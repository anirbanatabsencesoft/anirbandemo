﻿using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Audit;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Jobs;
using AbsenceSoft.Logic.Necessitys;
using AbsenceSoft.Logic.Pay;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Logic.Tasks;
using AbsenceSoft.Models;
using AbsenceSoft.Models.Cases;
using AbsenceSoft.Models.Contacts;
using AbsenceSoft.Models.Demands;
using AbsenceSoft.Models.Employees;
using AbsenceSoft.Models.Jobs;
using AbsenceSoft.Models.Notes;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using AbsenceSoft.Web;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Models.RiskProfiles;
using AbsenceSoft.Web.Filters;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.Controllers
{
    [Secure]
    public class CasesController : BaseController
    {
        [HttpGet, Title("Cases")]
        [Route("Cases", Name = "Cases")]
        [Secure("EditCase", "ViewCase")]
        public ViewResult Index()
        {
            var caseStatuses = (from CaseStatus ct in Enum.GetValues(typeof(CaseStatus))
                                select new { Id = (int)ct, Text = ct.ToString() }).ToList();
            caseStatuses.Insert(0, new { Id = -1, Text = "Filter" });
            ViewBag.CaseStatuses = new SelectList(caseStatuses, "Id", "Text");

            var caseTypes = (from CaseType ct in Enum.GetValues(typeof(CaseType))
                             select new { Id = (int)ct, Text = ct.ToString() }).ToList();
            caseTypes.Insert(0, new { Id = -1, Text = "Filter" });
            ViewBag.CaseTypes = new SelectList(caseTypes, "Id", "Text");

            return View(new EmployeeCasesModel());
        }

        #region Pay
        [HttpGet, Title("Pay")]
        [Route("Cases/{caseId}/Pay", Name = "Pay")]
        [Secure("EditCase")]
        public ActionResult Pay(string caseId)
        {
            Case theCase = Case.GetById(caseId);

            if (theCase == null)
                return new HttpStatusCodeResult(404, "Case not found");

            var viewModel = new PayService(CurrentUser).GetCasePayModel(theCase);

            return View(viewModel);
        }

        [Secure("EditCase"), HttpPost]
        [Route("Cases/Pay/UpdatePayPeriodStatus", Name = "UpdatePayPeriodStatus")]
        [ValidateApiAntiForgeryToken]
        public JsonResult UpdatePayPeriodStatus(CasePayPeriodModel payPeriod)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(payPeriod.CaseId))
                    return JsonError(new Exception("Case Id was not supplied"), "Case Id was not supplied", 404);

                if (payPeriod.PayPeriodId == Guid.Empty)
                    return JsonError(new Exception("Pay period was not supplied"), "Pay period was not supplied", 404);

                Case theCase = Case.GetById(payPeriod.CaseId);

                if (theCase == null)
                    return JsonError(new Exception("Case not found"), "Case not found", 404);

                var payService = new PayService(CurrentUser);

                theCase = payService.UpdatePayPeriodStatus(theCase, payPeriod.PayPeriodId, payPeriod.Status);
                theCase = new CaseService(false).Using(c => c.UpdateCase(theCase));
                var model = payService.GetCasePayModel(theCase);

                return Json(model);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCase"), HttpPost]
        [Route("Cases/Pay/ChangeDetailBenefitAmount", Name = "ChangeDetailBenefitAmount")]
        [ValidateApiAntiForgeryToken]
        public JsonResult ChangeDetailBenefitAmount(CasePayDetailModel payDetail)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(payDetail.CaseId))
                    return JsonError(new Exception("Case Id was not supplied"), "Case Id was not supplied", 404);

                if (payDetail.PayDetailId == Guid.Empty)
                    return JsonError(new Exception("Pay period detail identifier was not supplied"), "Pay period detail identifier was not supplied", 404);

                Case theCase = Case.GetById(payDetail.CaseId);

                if (theCase == null)
                    return JsonError(new Exception("Case not found"), "Case not found", 404);

                var payService = new PayService(CurrentUser);

                theCase = payService.ChangeBenefitAmount(theCase, payDetail.PayDetailId, payDetail.TotalPaid);
                theCase = new CaseService(false).Using(c => c.UpdateCase(theCase));
                var model = payService.GetCasePayModel(theCase);

                return Json(model);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCase"), HttpPost]
        [Route("Cases/Pay/ChangeDetailBenefitPercentage", Name = "ChangeDetailBenefitPercentage")]
        [ValidateApiAntiForgeryToken]
        public JsonResult ChangeDetailBenefitPercentage(CasePayDetailModel payDetail)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(payDetail.CaseId))
                    return JsonError(new Exception("Case Id was not supplied"), "Case Id was not supplied", 404);

                if (payDetail.PayDetailId == Guid.Empty)
                    return JsonError(new Exception("Pay period detail identifier was not supplied"), "Pay period detail identifier was not supplied", 404);

                Case theCase = Case.GetById(payDetail.CaseId);

                if (theCase == null)
                    return JsonError(new Exception("Case not found"), "Case not found", 404);

                var payService = new PayService(CurrentUser);

                theCase = payService.ChangeBenefitPercentage(theCase, payDetail.PayDetailId, payDetail.BenefitPercentage);
                theCase = new CaseService(false).Using(c => c.UpdateCase(theCase));
                var model = payService.GetCasePayModel(theCase);

                return Json(model);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCase"), HttpPost]
        [Route("Cases/Pay/ChangeApplyOffsetsByDefault", Name = "ChangeApplyOffsetByDefault")]
        [ValidateApiAntiForgeryToken]
        public JsonResult ChangeDetailIsOffset(CasePayModel payModel)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(payModel.CaseId))
                    return JsonError(new Exception("Case Id was not supplied"), "Case Id was not supplied", 404);

                Case theCase = Case.GetById(payModel.CaseId);

                if (theCase == null)
                    return JsonError(new Exception("Case not found"), "Case not found", 404);

                var payService = new PayService(CurrentUser);

                theCase = payService.ChangeApplyOffsetsByDefault(theCase, payModel.ApplyOffsetsByDefault);
                theCase = new CaseService(false).Using(c => c.UpdateCase(theCase));
                var model = payService.GetCasePayModel(theCase);

                return Json(model);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCase"), HttpPost]
        [Route("Cases/Pay/ChangeDetailIsOffset", Name = "ChangeDetailIsOffset")]
        [ValidateApiAntiForgeryToken]
        public JsonResult ChangeDetailIsOffset(CasePayDetailModel payDetail)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(payDetail.CaseId))
                    return JsonError(new Exception("Case Id was not supplied"), "Case Id was not supplied", 404);

                if (payDetail.PayDetailId == Guid.Empty)
                    return JsonError(new Exception("Pay period detail identifier was not supplied"), "Pay period detail identifier was not supplied", 404);

                Case theCase = Case.GetById(payDetail.CaseId);

                if (theCase == null)
                    return JsonError(new Exception("Case not found"), "Case not found", 404);

                var payService = new PayService(CurrentUser);

                theCase = payService.ChangeDetailIsOffset(theCase, payDetail.PayDetailId, payDetail.IsOffset);
                theCase = new CaseService(false).Using(c => c.UpdateCase(theCase));
                var model = payService.GetCasePayModel(theCase);

                return Json(model);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCase"), HttpPost]
        [Route("Cases/Pay/ChangeBenefitPercentage", Name = "ChangeBenefitPercentage")]
        [ValidateApiAntiForgeryToken]
        public JsonResult ChangeBenefitPercentage(PayUpdate casePay)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(casePay.CaseId))
                    return JsonError(new Exception("Case Id was not supplied"), "Case Id was not supplied", 404);

                if (string.IsNullOrWhiteSpace(casePay.PolicyId))
                    return JsonError(new Exception("Policy code was not supplied"), "Policy code was not supplied", 404);

                Case theCase = Case.GetById(casePay.CaseId);

                if (theCase == null)
                    return JsonError(new Exception("Case not found"), "Case not found", 404);

                var payService = new PayService(CurrentUser);

                theCase = payService.ChangeBenefitPercentage(theCase, casePay.PolicyId, casePay.Amount);
                theCase = new CaseService(false).Using(c => c.UpdateCase(theCase));
                var model = payService.GetCasePayModel(theCase);

                return Json(model);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCase"), HttpPost]
        [Route("Cases/Pay/UpdateBasePay", Name = "UpdateBasePay")]
        [ValidateApiAntiForgeryToken]
        public JsonResult UpdateBasePay(PayUpdate newBasePay)
        {
            try
            {
                if (newBasePay == null || string.IsNullOrWhiteSpace(newBasePay.CaseId))
                    return JsonError(new Exception("Case Id was not supplied"), "Case Id was not supplied", 404);

                Case theCase = Case.GetById(newBasePay.CaseId);

                if (theCase == null)
                    return JsonError(new Exception("Case not found"), "Case not found", 404);

                var payService = new PayService(CurrentUser);

                theCase = payService.ChangeEmployeeBasePay(theCase, Convert.ToDouble(newBasePay.Amount));
                theCase = new CaseService(false).Using(c => c.UpdateCase(theCase));

                var model = payService.GetCasePayModel(theCase);
                return Json(model);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to update employees base pay");
            }
        }

        [Secure("EditCase"), HttpPost]
        [Route("Cases/Pay/UpdatePaySchedule", Name = "UpdatePaySchedule")]
        [ValidateApiAntiForgeryToken]
        public JsonResult UpdatePaySchedule(UpdatePayScheduleModel newPaySchedule)
        {
            try
            {
                if (newPaySchedule.CaseId == null)
                    return JsonError(new Exception("Pay Schedule Id was not supplied"), "Pay Schedule Id was not supplied", 404);

                if (string.IsNullOrEmpty(newPaySchedule.PayScheduleId))
                    return JsonError(new Exception("Pay Schedule Id was not supplied"), "Pay Schedule Id was not supplied", 404);

                PayService ps = new PayService(CurrentUser);
                Case theCase = Case.GetById(newPaySchedule.CaseId);
                theCase = ps.UpdatePaySchedule(theCase, newPaySchedule.PayScheduleId);
                theCase = new CaseService(false).Using(c => c.UpdateCase(theCase));
                CasePayModel payModel = ps.GetCasePayModel(theCase);

                return Json(payModel);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to update employees base pay");
            }
        }

        [Secure("EditCase"), HttpPost]
        [Route("Cases/Pay/WaiveWaitingPeriod", Name = "WaiveWaitingPeriod")]
        [ValidateApiAntiForgeryToken]
        public JsonResult WaiveWaitingPeriod(WaiveWaitingPeriodModel model)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(model.CaseId))
                    return JsonError(new Exception("Case Id was not supplied"), "Case Id was not supplied", 404);

                PayService ps = new PayService(CurrentUser);
                Case theCase = Case.GetById(model.CaseId);
                theCase = ps.SetWaitingPeriod(theCase, model.Waive);
                theCase = new CaseService(false).Using(c => c.UpdateCase(theCase));
                CasePayModel payModel = ps.GetCasePayModel(theCase);

                return Json(payModel);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to waive the waiting period");
            }
        }

        [Secure("EditCase"), HttpPost]
        [Route("Cases/Pay/UpdatePayPeriod", Name = "UpdatePayPeriod")]
        [ValidateApiAntiForgeryToken]
        public JsonResult UpdatePayPeriod(CasePayPeriodModel payPeriod)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(payPeriod.CaseId))
                    return JsonError(new Exception("Case Id was not supplied"), "Case Id was not supplied", 404);

                PayService ps = new PayService(CurrentUser);
                Case theCase = Case.GetById(payPeriod.CaseId);
                theCase = ps.UpdatePayPeriodDates(theCase, payPeriod.PayPeriodId, payPeriod.PayDate, payPeriod.PayThru);
                theCase = new CaseService(false).Using(c => c.UpdateCase(theCase));
                CasePayModel payModel = ps.GetCasePayModel(theCase);

                return Json(payModel);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to waive the waiting period");
            }
        }

        #endregion Pay

        #region View

        [Secure("EditCase", "ViewCase"), HttpGet]
        [Route("Cases/{caseId}/GetDaysAwayFromWork", Name = "GetDaysAwayFromWork")]
        public JsonResult GetDaysAwayFromWork(string caseId, string dateOfInjury)
        {
            int calculatedGetDaysAwayFromWork = 0;

            DateTime? injuryDateTime = null;
            if (!string.IsNullOrEmpty(dateOfInjury) && double.TryParse(dateOfInjury, out double injuryDays))
            {
                injuryDateTime = (new DateTime(1970, 1, 1).ToUtcDateTime()).AddMilliseconds(injuryDays).ToMidnight();
            }

            Case theCase = Case.GetById(caseId);
            if (theCase != null)
            {
                using (var service = new CaseService())
                {
                    calculatedGetDaysAwayFromWork = service.CalculateDaysAwayFromWork(theCase, injuryDateTime);
                }
            }
            return Json(new { CalculatedGetDaysAwayFromWork = calculatedGetDaysAwayFromWork });
        }

        [Secure("EditCase", "ViewCase"), HttpGet, Title("Inquiry")]
        [Route("Inquiries/{caseId}/View", Name = "ViewInquiry")]
        public ActionResult ViewInquiry(string caseId, bool dsnccmc = false)
        {
            return CaseView(caseId, dsnccmc);
        }

        [Secure("EditCase", "ViewCase"), HttpGet, Title("Case")]
        [Route("Cases/{caseId}/View", Name = "ViewCase")]
        public ActionResult ViewCase(string caseId, bool dsnccmc = false) // dsnccmc = deleteShowNewCaseCommunicationsModalCookie
        {
            return CaseView(caseId, dsnccmc);
        }

        private ActionResult CaseView(string caseId, bool dsnccmc, string message = null)
        {
            if (String.IsNullOrEmpty(caseId))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Case identifier was not provided");

            var _case = Case.GetById(caseId);
            if (_case == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound, "Case was not found");

            if (!CurrentUser.HasEmployeeOrganizationAccess(_case.Employee))
            {
                return RedirectToRoute("Home");
            }

            var todoItemStatuses = (from ToDoItemStatus ct in Enum.GetValues(typeof(ToDoItemStatus))
                                    select new { Id = (int)ct, Text = ct.ToString() }).ToList();
            todoItemStatuses.Insert(0, new { Id = 0, Text = "Filter" });
            ViewBag.WorkflowItemStatuses = new SelectList(todoItemStatuses, "Id", "Text");

            var model = new ViewCaseModel();
            model.Id = _case.Id;
            model.CreatedDate = _case.CreatedDate;
            model.ModifiedDate = _case.ModifiedDate;
            model.CaseNumber = _case.CaseNumber;
            model.Status = _case.Status.ToString().SplitCamelCaseString();
            model.CaseStatus = _case.Status;
            model.StartDate = _case.StartDate;
            model.EndDate = _case.EndDate;
            model.Description = _case.Description;
            model.ShortSummary = _case.Narrative;
            model.IsIntermittent = _case.IsIntermittent;
            model.Reason = _case.Reason == null ? null : _case.Reason.Name;
            model.SubReason = _case.Reason == null ? null : _case.Reason.Category;
            model.LeaveType = this.GetLeaveType(_case);
            model.EmployeeName = _case.Employee.FullName;
            model.EmployeeJob = new EmployeeJobViewModel(_case.Employee.GetCurrentJob());
            model.EmployeeJob.IHasJobHistory = (_case.Employee.GetJobs().Count > 0);
            model.EmployeeJob.EmployeeNumber = _case.Employee.EmployeeNumber;
            model.EmployeeJob.EmployerId = _case.EmployerId;
            model.FTWeeklyWorkHours = Current.Employer().FTWeeklyWorkHours;

            //Get the JobActivity Level from associated Employee and apply to the case
            if (_case != null && _case.Employee != null && _case.Disability != null)
                model.EmployeeJob.Activity = _case.Disability.EmployeeJobActivity ?? _case.Employee.JobActivity;

            if (_case.Employee != null)
                model.EmployeeId = _case.Employee.Id;

            model.EmployerId = _case.EmployerId;
            using (var caseService = new CaseService())
            {
                var adjudicationDenialReasons = caseService.GetAllDenialReasons(DenialReasonTarget.Policy).Select(ct => new { ID = ct.Code, Name = ct.Description });
                ViewBag.AdjudicationDenialReasons = new SelectList(adjudicationDenialReasons, "ID", "Name");
            }
            if (_case.SpouseCaseId != null && _case.SpouseCase != null)
            {
                model.SpouseCaseId = _case.SpouseCaseId;
                model.SpouseCaseNumber = _case.SpouseCase.CaseNumber;
                model.SpouseFirstName = _case.SpouseCase.Employee.FirstName;
                model.SpouseLastName = _case.SpouseCase.Employee.LastName;
            }

            model.IsAccommodation = _case.IsAccommodation;
            model.IsSTD = _case.IsSTD;
            if (model.Disability == null)
                model.Disability = new DisabilityModel();

            if (_case.Disability != null)
            {
                if (_case.Disability.PrimaryDiagnosis != null)
                    model.Disability.PrimaryDiagnosis = new DiagnosisCodeModel() { Id = _case.Disability.PrimaryDiagnosis.Id, Description = _case.Disability.PrimaryDiagnosis.UIDescription };
                //model.Disability.SecondaryDiagnosis = new DiagnosisCodeModel() { Id = _case.Disability.PrimaryDiagnosis.Id, Description = _case.Disability.PrimaryDiagnosis.UIDescription };
                model.Disability.MedicalComplications = _case.Disability.MedicalComplications;
                model.Disability.Hospitalization = _case.Disability.Hospitalization;
                model.Disability.GeneralHealthCondition = _case.Disability.GeneralHealthCondition;
                model.Disability.ConditionStartDate = _case.Disability.ConditionStartDate;

                //hcp
                EmployeeContact employeeContact = _case.CasePrimaryProvider ?? new CaseService().GetCasePrimaryProviderContact(_case);
                if (employeeContact != null && employeeContact.Contact != null && _case.CasePrimaryProvider != null)
                {
                    model.Disability.HCPContactId = employeeContact.Id.ToString();
                    model.Disability.HCPContactTypeCode = employeeContact.ContactTypeCode;
                    model.Disability.HCPCompanyName = employeeContact.Contact.CompanyName;
                    model.Disability.HCPFirstName = employeeContact.Contact.FirstName;
                    model.Disability.HCPLastName = employeeContact.Contact.LastName;
                    model.Disability.HCPPhone = employeeContact.Contact.WorkPhone;
                    model.Disability.HCPFax = employeeContact.Contact.Fax;
                    model.Disability.HCPContactPersonName = employeeContact.Contact.ContactPersonName;

                    if (employeeContact.Contact.IsPrimary == null)
                        employeeContact.Contact.IsPrimary = false;

                    model.Disability.IsPrimary = employeeContact.Contact.IsPrimary.Value;
                }
            }

            model.ADAFeatureEnabled = _case.Employer.HasFeature(Feature.ADA);
            model.STDFeatureEnabled = _case.Employer.HasFeature(Feature.ShortTermDisability);
            model.GuidelinesDataEnabled = _case.Employer.HasFeature(Feature.GuidelinesData);
            model.STDPayFeatureEnabled = _case.Employer.HasFeature(Feature.ShortTermDisablityPay);
            model.NoFeaturesEnabled = !(model.ADAFeatureEnabled && model.STDFeatureEnabled);
            model.LOAFeatureEnabled = _case.Employer.HasFeature(Feature.LOA);
            model.RiskProfileFeatureEnabled = CurrentCustomer.HasFeature(Feature.RiskProfile);
            model.ShowIsWorkRelated = CurrentCustomer.Metadata.GetRawValue<bool>("ShowIsWorkRelated") || CurrentCustomer.HasFeature(Feature.WorkRelatedReporting);
            model.ShowContactSection = CurrentCustomer.Metadata.GetRawValue<bool>("ShowContactSection");
            model.ShowOtherQuestions = CurrentCustomer.Metadata.GetRawValue<bool>("ShowOtherQuestions");
            model.ShowAccomCategory = CurrentCustomer.Metadata.GetRawValue<bool>("ShowAccomCategory");
            model.ShowAccomAdditionalInfo = CurrentCustomer.Metadata.GetRawValue<bool>("ShowAccomAdditionalInfo");
            model.WorkRestrictionFeatureEnabled = CurrentCustomer.HasFeature(Feature.WorkRestriction);
            model.AccommodationTypeCategoriesFeatureEnabled = CurrentCustomer.HasFeature(Feature.AccommodationTypeCategories);
            //CurrentCustomer.HasFeature(Feature.BusinessIntelligenceReport);

            using (var demandService = new DemandService(CurrentUser))
            {
                model.WorkRestrictions = demandService.GetJobRestrictionsForEmployee(_case.Employee.Id, _case.Id).Select(r => new WorkRestrictionViewModel(r)).ToList();
            }
            using (var necessityService = new NecessityService(CurrentUser))
            {
                model.EmployeeNecessities = necessityService.GetEmployeeNecessities(_case.Employee).Where(n => n.CaseId == _case.Id).ToList();
            }

            model.WorkRelatedInfo = new WorkRelatedViewModel(_case);
            using (var caseService = new CaseService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                model.Relapses = caseService.GetRelapses(_case.Id);
            }
            _case.GenerateCaseView(CurrentUser);
            _case.Employee.GenerateEmployeeView(CurrentUser);

            if (!String.IsNullOrEmpty(message))
            {
                model.MessageOnViewLoad = message;
            }
            ViewBag.EmployerId = _case.EmployerId;
            return View("View", model);
        }

        [Secure("EditCase", "ViewCase"), HttpGet]
        [Route("Cases/{reasonTarget}/DenialReasons", Name = "CaseDenialReasons")]
        public JsonResult DenialReasonsData(string reasonTarget, string caseId = null)
        {
            try
            {
                var denialReasons = new List<DenialReason>();
                using (var service = new CaseService())
                {
                    if (caseId != null)
                    {
                        var theCase = service.GetCaseById(caseId);
                        service.CurrentEmployer = theCase != null ? theCase.Employer : Current.Employer();
                    }

                    if (DenialReasonTarget.Accommodation.ToString().Equals(reasonTarget))
                        denialReasons.AddRange(service.GetAllDenialReasons(DenialReasonTarget.Accommodation));
                    else if (DenialReasonTarget.Policy.ToString().Equals(reasonTarget))
                        denialReasons.AddRange(service.GetAllDenialReasons(DenialReasonTarget.Policy));
                    else
                        denialReasons.AddRange(service.GetAllDenialReasons(DenialReasonTarget.None));
                }
                return Json(denialReasons);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCase", "ViewCase"), HttpGet]
        [Route("Cases/{caseId}/PolicyData", Name = "CasePolicyData")]
        public JsonResult PolicyData(string caseId)
        {
            try
            {
                LeaveOfAbsence leaveOfAbsence = null;
                using (var service = new EligibilityService())
                {
                    leaveOfAbsence = service.GetLeaveOfAbsence(caseId);
                }
                var returnModel = new EditCaseModel() { Id = caseId };
                this.PopulateCasePoliciesData(returnModel, leaveOfAbsence, null);

                if (returnModel.AvailableManualPolicies.Count > 0)
                {
                    AppliedPolicyModel FMLAPolicyModel = returnModel.AvailableManualPolicies.Where(m => m.PolicyCode == "FMLA").FirstOrDefault();
                    AppliedPolicyModel STDPolicyModel = returnModel.AvailableManualPolicies.Where(m => m.PolicyCode == "STD").FirstOrDefault();
                    AppliedPolicyModel COMPMEDPolicyModel = returnModel.AvailableManualPolicies.Where(m => m.PolicyCode == "COMP-MED").FirstOrDefault();
                    AppliedPolicyModel USERRAPolicyModel = returnModel.AvailableManualPolicies.Where(m => m.PolicyCode == "USERRA").FirstOrDefault();

                    bool IsStdModelAvailabel = false;
                    bool IsFmlaModelAvailabel = false;
                    bool IsCompMedModelAvailabel = false;
                    bool IsUserraModelAvailabel = false;

                    if (STDPolicyModel != null)
                        IsStdModelAvailabel = true;
                    if (FMLAPolicyModel != null)
                        IsFmlaModelAvailabel = true;
                    if (COMPMEDPolicyModel != null)
                        IsCompMedModelAvailabel = true;
                    if (USERRAPolicyModel != null)
                        IsUserraModelAvailabel = true;

                    if (IsStdModelAvailabel)
                    {
                        returnModel.AvailableManualPolicies.Remove(STDPolicyModel);
                        returnModel.AvailableManualPolicies.Insert(0, STDPolicyModel);
                    }
                    if (IsFmlaModelAvailabel)
                    {
                        returnModel.AvailableManualPolicies.Remove(FMLAPolicyModel);
                        if (IsStdModelAvailabel)
                        {
                            returnModel.AvailableManualPolicies.Insert(1, FMLAPolicyModel);
                        }
                        else
                        {
                            returnModel.AvailableManualPolicies.Insert(0, FMLAPolicyModel);
                        }
                    }

                    if (IsCompMedModelAvailabel)
                    {
                        returnModel.AvailableManualPolicies.Remove(COMPMEDPolicyModel);
                        if (!IsStdModelAvailabel && !IsFmlaModelAvailabel)
                        {
                            returnModel.AvailableManualPolicies.Insert(0, COMPMEDPolicyModel);
                        }
                        if (IsStdModelAvailabel && IsFmlaModelAvailabel)
                        {
                            returnModel.AvailableManualPolicies.Insert(2, COMPMEDPolicyModel);
                        }
                        if ((IsStdModelAvailabel && !IsFmlaModelAvailabel) || (!IsStdModelAvailabel && IsFmlaModelAvailabel))
                        {
                            returnModel.AvailableManualPolicies.Insert(1, COMPMEDPolicyModel);
                        }

                    }
                    if (IsUserraModelAvailabel)
                    {
                        returnModel.AvailableManualPolicies.Remove(USERRAPolicyModel);
                        if (!IsStdModelAvailabel && !IsFmlaModelAvailabel && !IsCompMedModelAvailabel)
                        {
                            returnModel.AvailableManualPolicies.Insert(0, USERRAPolicyModel);
                        }
                        if (IsStdModelAvailabel && IsFmlaModelAvailabel && IsCompMedModelAvailabel)
                        {
                            returnModel.AvailableManualPolicies.Insert(3, USERRAPolicyModel);
                        }
                        if ((IsStdModelAvailabel && !IsFmlaModelAvailabel && !IsCompMedModelAvailabel) || (!IsStdModelAvailabel && IsFmlaModelAvailabel && !IsCompMedModelAvailabel) || (!IsStdModelAvailabel && !IsFmlaModelAvailabel && IsCompMedModelAvailabel))
                        {
                            returnModel.AvailableManualPolicies.Insert(1, USERRAPolicyModel);
                        }
                        if ((IsStdModelAvailabel && IsFmlaModelAvailabel && !IsCompMedModelAvailabel) || (IsStdModelAvailabel && !IsFmlaModelAvailabel && IsCompMedModelAvailabel) || (!IsStdModelAvailabel && IsFmlaModelAvailabel && IsCompMedModelAvailabel))
                        {
                            returnModel.AvailableManualPolicies.Insert(2, USERRAPolicyModel);
                        }
                    }
                }

                return Json(returnModel);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCase"), HttpPost]
        [Route("Cases/{caseId}/Edit", Name = "EditCase")]
        [ValidateApiAntiForgeryToken]
        public JsonResult Edit(EditCaseModel model)
        {
            try
            {
                var leaveOfAbsence = this.ToLeaveOfAbsence(model);
                var returnModel = new EditCaseModel()
                {
                    Id = model.Id
                };
                this.PopulateCasePoliciesData(returnModel, leaveOfAbsence, model);

                return Json(returnModel);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCase"), HttpPost]
        [Route("Cases/{caseId}/AddManualPolicy", Name = "AddManualPolicy")]
        [ValidateApiAntiForgeryToken]
        public JsonResult AddManualPolicy(string caseId, string policyCode)
        {
            try
            {
                using (var eligibilityService = new EligibilityService())
                using (var caseService = new CaseService())
                using (var employeeService = new EmployeeService())
                {
                    Case theCase = caseService.GetCaseById(caseId);
                    if (theCase == null)
                        return JsonError(new AbsenceSoftException("Case Not Found"));

                    Employee theEmployee = employeeService.GetEmployee(theCase.Employee.Id);
                    if (theEmployee == null)
                        return JsonError(new AbsenceSoftException("Employee Not Found"));

                    theCase = eligibilityService.AddManualPolicy(theCase, policyCode, null);

                    // Only update calcs IF they have a work schedule
                    if (theEmployee.WorkSchedules.Count > 0)
                    {
                        theCase = caseService.RunCalcs(theCase);
                    }
                    // Trigger the Mannual Policy and WorkFlow
                    theCase = caseService.UpdateCase(theCase, CaseEventType.CaseCreated);
                    return Json(new { success = true });
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCase"), HttpPost]
        [Route("Cases/{caseId}/DeletePolicy", Name = "DeletePolicy")]
        [ValidateApiAntiForgeryToken]
        public JsonResult DeletePolicy(DeleteCasePolicy model, string caseId)
        {
            try
            {
                var _case = Case.GetById(caseId);
                if (_case == null)
                {
                    throw new ArgumentOutOfRangeException("caseId", caseId, "Case not found.");
                }

                var segments = _case.Segments;
                if (segments.Any())
                {
                    for (int i = 0; i <= segments.Count - 1; i++)
                    {
                        if (segments[i].AppliedPolicies.Any(m => m.ManuallyAdded == true && m.Policy.Code == model.Code))
                        {
                            segments[i].AppliedPolicies.Remove(segments[i].AppliedPolicies.Where(m => m.ManuallyAdded == true && m.Policy.Code == model.Code).FirstOrDefault());
                        }
                    }
                }

                new CaseService().Using(c => c.UpdateCase(_case));

                return Json(null);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCase", "ViewCase"), HttpGet]
        [Route("Cases/{caseId}/LeaveOfAbsence", Name = "GetLeaveOfAbsence")]
        public JsonResult GetLeaveOfAbsence(string caseId)
        {
            try
            {
                LeaveOfAbsence leaveOfAbsence = null;
                using (var service = new EligibilityService())
                {
                    leaveOfAbsence = service.GetLeaveOfAbsence(caseId);
                }

                var caseViewModel = this.ToCaseViewModel(leaveOfAbsence);

                return Json(caseViewModel);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCase", "ViewCase"), HttpGet]
        [Route("Cases/{caseId}/Review", Name = "GetESSCaseForReview")]
        public JsonResult GetESSCaseForReview(string caseId)
        {
            Case cAse = Case.GetById(caseId);
            if (cAse != null)
            {
                CaseEssReviewModel reviewCase = new CaseEssReviewModel();
                reviewCase.CaseId = cAse.Id;
                reviewCase.EmployeeId = cAse.Employee.Id;
                reviewCase.EmployerId = cAse.EmployerId;
                reviewCase.IsContactInformationCorrect = cAse.Metadata.GetRawValue<bool>("IsContactInformationCorrect");
                reviewCase.NewAddress = (string.IsNullOrWhiteSpace(cAse.Employee.Info.Address.Address1) ? string.Empty : cAse.Employee.Info.Address.Address1) + (string.IsNullOrWhiteSpace(cAse.Employee.Info.Address.Address2) ? string.Empty : " " + cAse.Employee.Info.Address.Address2);
                reviewCase.NewCity = cAse.Employee.Info.Address.City;
                reviewCase.NewState = cAse.Employee.Info.Address.State;
                reviewCase.NewZipcode = cAse.Employee.Info.Address.PostalCode;
                reviewCase.NewWorkPhone = cAse.Employee.Info.WorkPhone;
                reviewCase.NewEmail = cAse.Employee.Info.Email;
                reviewCase.LeaveFor = cAse.Metadata.GetRawValue<string>("LeaveFor");
                reviewCase.CaseTypeId = (int?)cAse.Segments[0].Type;
                reviewCase.StartDate = cAse.StartDate;
                reviewCase.EndDate = cAse.EndDate;
                reviewCase.AbsenceReasonId = cAse.Reason.Id;
                reviewCase.Status = cAse.Status;
                reviewCase.IsAccommodation = cAse.IsAccommodation;
                if (cAse.Contact != null)
                {
                    reviewCase.EmployeeContactId = cAse.Contact.Id;
                    reviewCase.EmployeeRelationshipCode = cAse.Contact.ContactTypeCode;
                    reviewCase.EmployeeRelationshipFirstName = cAse.Contact.Contact.FirstName;
                    reviewCase.EmployeeRelationshipLastName = cAse.Contact.Contact.LastName;
                }

                reviewCase.IsWorkScheduleCorrect = cAse.Metadata.GetRawValue<bool>("IsWorkScheduleCorrect");

                switch (cAse.Reason.Code)
                {
                    case "EHC":
                    case "FHC":
                        reviewCase.IsWorkRelated = cAse.Metadata.GetRawValue<bool>("IsWorkRelated");
                        reviewCase.ShortDescription = cAse.Metadata.GetRawValue<string>("ShortDescription");
                        break;
                    case "PREGMAT":
                        reviewCase.ActualDeliveryDate = cAse.FindCaseEvent(CaseEventType.DeliveryDate).EventDate;
                        reviewCase.ExpectedDeliveryDate = cAse.FindCaseEvent(CaseEventType.DeliveryDate).EventDate;

                        reviewCase.WillUseBonding = cAse.Metadata.GetRawValue<bool>("WillUseBonding");
                        if (reviewCase.WillUseBonding.HasValue && reviewCase.WillUseBonding.Value == true)
                        {
                            reviewCase.BondingStartDate = cAse.FindCaseEvent(CaseEventType.BondingStartDate).EventDate;
                            reviewCase.BondingEndDate = cAse.FindCaseEvent(CaseEventType.BondingEndDate).EventDate;
                        }
                        break;
                    case "MILITARY":
                    case "EXIGENCY":
                    case "CEREMONY":
                    case "RESERVETRAIN":
                        reviewCase.MilitaryStatusId = (int?)cAse.Employee.MilitaryStatus;

                        break;
                    case "ADOPT":
                        reviewCase.AdoptionDate = cAse.FindCaseEvent(CaseEventType.AdoptionDate).EventDate;
                        break;
                }

                return Json(new { Success = true, CaseInfo = reviewCase });
            }

            return Json(new { Success = false });
        }

        [Secure("EditCase", "ViewCase"), HttpGet]
        [Route("Cases/{caseId}/TimeTrackingInfo", Name = "TimeTrackingInfoForCaseJSON")]
        public JsonResult TimeTrackingInfoForCaseJSON(CaseTimeTrackerModel caseTimeTrackerModel, EligibilityStatus[] includeStatus = null)
        {
            try
            {
                if (String.IsNullOrEmpty(caseTimeTrackerModel.CaseId))
                {
                    throw new ArgumentNullException("caseId");
                }

                using (var service = new CaseService())
                {
                    return Json(this.ToModel(service.GetEmployeePolicySummaryByCaseIdBasedOnAsOfDate(caseTimeTrackerModel.CaseId, includeStatus, caseTimeTrackerModel.AsOfDate)));
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCase", "ViewEmployeeTimeTracker"), HttpGet]
        [Route("Employees/{employeeId}/TimeTrackingInfo", Name = "TimeTrackingInfoForEmployeeJSON")]
        public JsonResult TimeTrackingInfoForEmployeeJSON(string employeeId)
        {
            try
            {
                if (String.IsNullOrEmpty(employeeId))
                {
                    throw new ArgumentNullException("employeeId");
                }

                using (var service = new CaseService())
                {
                    return Json(this.ToModel(service.GetEmployeePolicySummary(employeeId)));
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }


        [Secure("EditCase"), HttpGet]
        [Route("Cases/{caseId}/STDInfo", Name = "STDInfoJSON")]
        public JsonResult STDInfoJSON(string caseId)
        {
            try
            {
                if (String.IsNullOrEmpty(caseId))
                {
                    throw new ArgumentNullException("caseId");
                }

                var _case = Case.GetById(caseId);
                if (_case == null)
                {
                    throw new ArgumentOutOfRangeException("caseId", caseId, "Case not found.");
                }

                var model = new DisabilityModel();

                if (_case.Disability != null)
                {
                    //model.Disability.PrimaryDiagnosis = new DiagnosisCodeModel() { Id = _case.Disability.PrimaryDiagnosis.Id, Description = _case.Disability.PrimaryDiagnosis.UIDescription };
                    //model.Disability.SecondaryDiagnosis = new DiagnosisCodeModel() { Id = _case.Disability.PrimaryDiagnosis.Id, Description = _case.Disability.PrimaryDiagnosis.UIDescription };
                    model.MedicalComplications = _case.Disability.MedicalComplications;
                    model.Hospitalization = _case.Disability.Hospitalization;
                    model.GeneralHealthCondition = _case.Disability.GeneralHealthCondition;
                    model.ConditionStartDate = _case.Disability.ConditionStartDate;
                    model.PrimaryDiagnosis = (_case.Disability != null && _case.Disability.PrimaryDiagnosis != null) ? new DiagnosisCodeModel() { Id = _case.Disability.PrimaryDiagnosis.Id.ToString(), Description = _case.Disability.PrimaryDiagnosis.UIDescription } : null;
                    model.SecondaryDiagnosis = (_case.Disability != null && _case.Disability.SecondaryDiagnosis != null) ? MakeSecondaryCodes(_case.Disability.SecondaryDiagnosis) : null;
                    model.PrimaryPathId = _case.Disability.PrimaryPathId;
                    model.PrimaryPathText = _case.Disability.PrimaryPathText;
                    model.PrimaryPathMaxDays = _case.Disability.PrimaryPathMaxDays.HasValue ? _case.Disability.PrimaryPathMaxDays.Value.ToString() : null;
                    model.PrimaryPathMinDays = _case.Disability.PrimaryPathMinDays.HasValue ? _case.Disability.PrimaryPathMinDays.Value.ToString() : null;
                    model.PrimaryPathDaysUIText = _case.Disability.PrimaryPathDaysUIText;

                    //hcp
                    EmployeeContact employeeContact = _case.CasePrimaryProvider ?? new CaseService().GetCasePrimaryProviderContact(_case);
                    if (employeeContact != null && employeeContact.Contact != null)
                    {
                        model.HCPContactId = employeeContact.Id.ToString();
                        model.HCPFirstName = employeeContact.Contact.FirstName;
                        model.HCPLastName = employeeContact.Contact.LastName;
                        model.HCPPhone = employeeContact.Contact.WorkPhone;
                        model.HCPFax = employeeContact.Contact.Fax;
                        model.HCPContactPersonName = employeeContact.Contact.ContactPersonName;
                    }
                }

                return Json(model);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCase", "ViewCase"), HttpGet]
        [Route("Cases/GetRoles", Name = "GetRoles")]
        public JsonResult GetRoles(ToDoItemType type)
        {
            try
            {
                return Json(new ToDoService().Using(m => m.GetToDoRoles(CurrentUser, type).ToList()));
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCase", "ViewCase"), HttpPost]
        [Route("Cases/ToDoUserAssignmentSuggestionList", Name = "ToDoUserAssignmentSuggestionList")]
        [ValidateApiAntiForgeryToken]
        public JsonResult ToDoUserAssignmentSuggestionList(ListCriteria criteria)
        {
            try
            {
                return Json(new ToDoService().Using(m => m.ToDoUserAssignmentSuggestionList(CurrentUser, criteria)));
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCase", "ViewCase"), HttpPost]
        [Route("Cases/CreateManualTodo", Name = "CreateManualTodo")]
        [ValidateApiAntiForgeryToken]
        public JsonResult CreateManualTodo(CreateManualCaseTodoModel createManualCaseTodo)
        {
            try
            {
                Case cAse = Case.GetById(createManualCaseTodo.CaseId);
                return Json(new ToDoService().Using(m => m.CreateManualTodoItem(cAse, CurrentUser, createManualCaseTodo.ToDoItemType, createManualCaseTodo.Title, createManualCaseTodo.Description, createManualCaseTodo.DueDate, ToDoItemPriority.Normal, createManualCaseTodo.AssignToId)));
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpPost, Secure("DeleteNotes")]
        [Route("Cases/IntProc/Note/Delete", Name = "DeleteCaseIntProcNote")]
        public ActionResult DeleteNote(DeleteCaseIntProcNoteViewModel viewModel)
        {
            try
            {
                if (String.IsNullOrEmpty(viewModel.CaseId))
                {
                    throw new ArgumentNullException("caseId");
                }
                if (String.IsNullOrEmpty(viewModel.QuestionId))
                {
                    throw new ArgumentNullException("QuestionId");
                }

                CaseNote caseNote = null;
                using (AccommodationService svc = new AccommodationService())
                    caseNote = svc.GetAccommIntProcCaseNote(viewModel.QuestionId, viewModel.CaseId);

                if (caseNote != null)
                    CaseNote.Delete(caseNote.Id);

                return Json(null);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [HttpGet, Secure("ViewCaseWorkflows")]
        [Route("Cases/{caseId}/Workflows", Name = "CaseWorkflows")]
        public ActionResult ViewCaseWorkflows(string caseId)
        {
            ViewBag.CaseId = caseId;
            ViewBag.CustomerId = CurrentCustomer.Id;
            if (CurrentEmployer != null)
                ViewBag.EmployerId = CurrentEmployer.Id;
            ViewBag.Status = "";
            return View();
        }

        [HttpGet, Secure("ViewCaseWorkflows")]
        [Route("Inquiries/{caseId}/Workflows", Name = "InquiryWorkflows")]
        public ActionResult ViewInquiryWorkflows(string caseId)
        {
            ViewBag.CaseId = caseId;
            ViewBag.CustomerId = CurrentCustomer.Id;
            if (CurrentEmployer != null)
                ViewBag.EmployerId = CurrentEmployer.Id;
            ViewBag.Status = "Inquiry";
            return View("ViewCaseWorkflows");
        }


        #endregion

        #region Child Actions

        // this is comment out in the controller, and it is theonly use of GetCasePolicySummary which has been removed becasue
        // we do not totally for an individual case
        //[ChildActionOnly]
        //public PartialViewResult TimeTrackingInfoForCase(string caseId)
        //{
        //    if (String.IsNullOrEmpty(caseId))
        //    {
        //        throw new ArgumentNullException("caseId");
        //    }

        //    using (var service = new CaseService())
        //    {
        //        return PartialView("_TimeTrackingInfo", this.ToModel(service.GetCasePolicySummary(caseId)));
        //    }
        //}

        [ChildActionOnly]
        public PartialViewResult AccommodationTypeCategories(bool isCreate)
        {

            ViewBag.IsCreate = isCreate;
            using (var accommodationService = new AccommodationService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                var rootCategories = accommodationService.GetAccommodationTypes().Select(at => new AccommodationTypeCategoryViewModel(at));
                return PartialView(rootCategories);
            }



        }

        private List<AccommodationTypeCategoryViewModel> GetAccommodationTypeCategories(AccommodationTypeCategoryViewModel category, List<AccommodationTypeCategoryViewModel> categories)
        {
            string parentName = null;
            if (category != null)
                parentName = category.Name;

            List<AccommodationTypeCategoryViewModel> childCategories = categories.Where(c => c.ParentName == parentName).ToList();
            foreach (var cat in childCategories)
            {
                cat.Children = GetAccommodationTypeCategories(cat, categories);
            }

            return childCategories;
        }



        [ChildActionOnly]
        public PartialViewResult TimeTrackingInfoForEmployee(string employeeId)
        {
            if (String.IsNullOrEmpty(employeeId))
            {
                throw new ArgumentNullException("employeeId");
            }

            using (var service = new CaseService())
            {
                return PartialView("_TimeTrackingInfo", this.ToModel(service.GetEmployeePolicySummary(employeeId)));
            }
        }

        [ChildActionOnly]
        public PartialViewResult ParentCaseInfo(string caseId, string status)
        {
            if (String.IsNullOrEmpty(caseId))
            {
                throw new ArgumentNullException("caseId");
            }

            var parentCase = Case.GetById(caseId);
            if (parentCase == null)
            {
                throw new ArgumentOutOfRangeException("caseId", caseId, "Case not found.");
            }

            var model = new CaseParentedModel()
            {
                CaseId = parentCase.Id,
                Status = status,
                CaseNumber = parentCase.CaseNumber,
                CaseReason = parentCase.Reason != null ? parentCase.Reason.Name : string.Empty,
                LeaveStartDate = parentCase.StartDate,
                LeaveEndDate = parentCase.EndDate,
                EmployeeFirstName = parentCase.Employee.FirstName,
                EmployeeLastName = parentCase.Employee.LastName,
                EmployeeId = parentCase.Employee.Id,
                EmployerName = parentCase.Employee.EmployerName,
                IsTPA = parentCase.Customer.HasFeature(Feature.MultiEmployerAccess)
            };

            return PartialView("_ParentCaseInfo", model);
        }

        #endregion

        #region Create

        [Secure(Permission.CreateCasePermissionId), HttpGet, Title("Create Inquiry")]
        [Route("Employees/{employeeId}/Inquiry/Create", Name = "CreateInquiry")]
        public ActionResult CreateInquiry(string employeeId)
        {
            return CreateGet(employeeId, true, true);
        }

        [Secure(Permission.CreateCasePermissionId), HttpGet, Title("Create Inquiry")]
        [Route("Employees/{employeeId}/Inquiry/Create/{caseId}", Name = "CreateSavedInquiry")]
        public ActionResult CreateInquiry(string employeeId, string caseId)
        {
            return CreateGet(employeeId, true, false);
        }

        [Secure(Permission.CreateCasePermissionId), HttpGet, Title("Create Case")]
        [Route("Employees/{employeeId}/Cases/Create", Name = "CreateCase")]
        [Route("Employees/{caseId}/Case/Review/{toDoItemId}", Name = "EmployeeCaseReview")]
        public ActionResult Create(string employeeId)
        {
            return CreateGet(employeeId, false, false);
        }

        private ActionResult CreateGet(string employeeId, bool isInquiry, bool isEssReview)
        {
            // Added for showing employee case review 
            string caseId = RouteData.Values["caseId"] as string;
            string toDoItemId = RouteData.Values["toDoItemId"] as string;

            if (string.IsNullOrWhiteSpace(caseId) && string.IsNullOrWhiteSpace(employeeId))
                throw new ArgumentNullException("Not enough information provided");

            Employee emp;
            Case cAse = new Case();
            if (!string.IsNullOrEmpty(caseId))
            {
                cAse = Case.GetById(caseId);
                emp = Employee.GetById(cAse.Employee.Id);
            }
            else if (!string.IsNullOrWhiteSpace(employeeId))
            {
                emp = Employee.GetById(employeeId);
            }
            else
            {
                throw new AbsenceSoftException("An Employee is required to create a new case. If editing an existing inquiry, a Case is required.");
            }

            var hasAdminReasons = AbsenceReason.AsQueryable()
                        .Any(ar => (ar.EmployerId == null || ar.EmployerId == emp.EmployerId) && ar.Category == AbsenceReasonCategory.Administrative);

            CreateCaseModel viewModel = null;

            if (string.IsNullOrWhiteSpace(caseId))
            {
                viewModel = new CreateCaseModel()
                {
                    Status = (isInquiry ? "Inquiry" : "Open"),
                    IsInquiry = isInquiry,
                    HasAdministrativeReasons = hasAdminReasons,
                    EmployeeId = employeeId,
                    EmployerId = emp.EmployerId,
                    ADAFeatureEnabled = emp.Employer.HasFeature(Feature.ADA),
                    STDFeatureEnabled = emp.Employer.HasFeature(Feature.ShortTermDisability),
                    GuidelinesDataEnabled = emp.Employer.HasFeature(Feature.GuidelinesData),
                    STDPayFeatureEnabled = emp.Employer.HasFeature(Feature.ShortTermDisablityPay),
                    LOAFeatureEnabled = emp.Employer.HasFeature(Feature.LOA),
                    AccommodationTypeCategoriesFeatureEnabled = emp.Employer.HasFeature(Feature.AccommodationTypeCategories),
                    ShowIsWorkRelated = CurrentCustomer.Metadata.GetRawValue<bool>("ShowIsWorkRelated") || CurrentCustomer.HasFeature(Feature.WorkRelatedReporting),
                    ShowContactSection = CurrentCustomer.Metadata.GetRawValue<bool>("ShowContactSection"),
                    ShowOtherQuestions = CurrentCustomer.Metadata.GetRawValue<bool>("ShowOtherQuestions"),
                    EmployeePrimaryEmail = emp.Info.Email,
                    EmployeePrimaryPhone = emp.Info.WorkPhone,
                    EmployeeSecondaryPhone = emp.Info.CellPhone,
                    EmployeePersonalEmail = emp.Info.AltEmail,
                    EmployeeSecondaryEmail = emp.Info.SecondaryEmail,
                    WorkRelatedInfo = new WorkRelatedViewModel(cAse),
                    EmployeeOfficeLocation = emp.Info.OfficeLocation,
                    EmployeeHasFTESchedule = emp.WorkSchedules.Any(w => w.ScheduleType == ScheduleType.FteVariable),
                    FTWeeklyWorkTime = Current.Employer().FTWeeklyWorkHours.ToString(),
                    Job = new EmployeeJobViewModel(emp.GetCurrentJob())
                    {
                        EmployeeNumber = emp.EmployeeNumber,
                        EmployeeId = emp.Id,
                        EmployerId = emp.EmployerId,
                        IHasJobHistory = (emp.GetJobs().Count > 0)
                    }

                };

                var isSysAdmin = Current.User().Roles.Any(a => Role.SystemAdministrator.Id == a);
                if (!isSysAdmin)
                {
                    var permissions = Data.Security.User.Permissions.ProjectedPermissions;
                    if (permissions.Any(p => p == Permission.CreateCaseWizardOnly.Id))
                    {
                        var url = Url.Action("WizardCreate", new { employeeId = emp.Id });
                        url += String.Format("/#/employee-new-accomm-create/{0}/{1}", emp.Id, isInquiry.ToString().ToLower());
                        return Redirect(url);
                    }
                }
                viewModel.WorkSchedule = new ScheduleViewModel();
                viewModel.WorkSchedule.BuildTimesLists();
                return View("Create", viewModel);
            }
            else
            {
                if (isEssReview)
                {
                    ViewBag.ToDoItemId = toDoItemId;
                    viewModel = new CreateCaseModel()
                    {
                        Status = (isInquiry ? "Inquiry" : "Open"),
                        IsInquiry = isInquiry,
                        HasAdministrativeReasons = hasAdminReasons,
                        CaseId = caseId,
                        IsESSCaseReview = true,
                        IsAccommodation = cAse.IsAccommodation,
                        EmployeeId = emp.Id,
                        EmployerId = emp.EmployerId,
                        ADAFeatureEnabled = emp.Employer.HasFeature(Feature.ADA),
                        STDFeatureEnabled = emp.Employer.HasFeature(Feature.ShortTermDisability),
                        AccommodationTypeCategoriesFeatureEnabled = emp.Employer.HasFeature(Feature.AccommodationTypeCategories),
                        LOAFeatureEnabled = emp.Employer.HasFeature(Feature.LOA),
                        WorkRelatedInfo = new WorkRelatedViewModel(cAse)
                    };
                }
                else
                {
                    viewModel = new CreateCaseModel()
                    {
                        Status = (isInquiry ? "Inquiry" : "Open"),
                        IsInquiry = isInquiry,
                        HasAdministrativeReasons = hasAdminReasons,
                        CaseId = cAse.Id,
                        EmployeeId = cAse.Employee.Id,
                        EmployerId = cAse.EmployerId,
                        ShortDescription = cAse.Description,
                        Summary = cAse.Narrative,
                        StartDate = cAse.StartDate,
                        EndDate = cAse.EndDate,
                        CaseReporter = new CreateContactViewModel(cAse.CaseReporter),
                        CaseTypeId = (int)(Enum.Parse(typeof(CaseType), cAse.CaseType)),
                        ADAFeatureEnabled = cAse.Employer.HasFeature(Feature.ADA),
                        STDFeatureEnabled = cAse.Employer.HasFeature(Feature.ShortTermDisability),
                        GuidelinesDataEnabled = cAse.Employer.HasFeature(Feature.GuidelinesData),
                        STDPayFeatureEnabled = cAse.Employer.HasFeature(Feature.ShortTermDisablityPay),
                        AccommodationTypeCategoriesFeatureEnabled = cAse.Employer.HasFeature(Feature.AccommodationTypeCategories),
                        LOAFeatureEnabled = cAse.Employer.HasFeature(Feature.LOA),
                        ShowIsWorkRelated = cAse.Customer.Metadata.GetRawValue<bool>("ShowIsWorkRelated") || CurrentCustomer.HasFeature(Feature.WorkRelatedReporting),
                        ShowContactSection = cAse.Customer.Metadata.GetRawValue<bool>("ShowContactSection"),
                        ShowOtherQuestions = cAse.Customer.Metadata.GetRawValue<bool>("ShowOtherQuestions"),
                        EmployeePrimaryEmail = cAse.Employee.Info.Email,
                        EmployeePrimaryPhone = cAse.Employee.Info.WorkPhone,
                        EmployeeSecondaryPhone = cAse.Employee.Info.CellPhone,
                        EmployeePersonalEmail = cAse.Employee.Info.AltEmail,
                        EmployeeSecondaryEmail = cAse.Employee.Info.SecondaryEmail,
                        WorkRelatedInfo = new WorkRelatedViewModel(cAse),

                        Job = new EmployeeJobViewModel(cAse.Employee.GetCurrentJob())
                        {
                            EmployeeNumber = cAse.Employee.EmployeeNumber,
                            EmployeeId = cAse.Employee.Id,
                            EmployerId = cAse.Employee.EmployerId,
                            IHasJobHistory = (cAse.Employee.GetJobs().Count > 0)
                        }
                    };
                }

                return View("Create", viewModel);
            }
        }

        [Secure(Permission.CreateCasePermissionId, Permission.CreateCaseWizardOnlyPermissionId)]
        [HttpGet, Title("Create Case Wizard")]
        [Route("Employees/{employeeId}/WizardCreate")]
        public ActionResult WizardCreate(String employeeId)
        {
            var model = new
            {
                EmployeeId = employeeId,
                ReturnUrl = Url.Action("ViewEmployee", "Employees", new { employeeId = employeeId }),
                ReturnMessage = "Return to Employee Record"
            };
            return View(model);
        }

        [Secure("CreateCase"), HttpGet, Title("Create Case")]
        [Route("Employees/{employeeId}/GetEmployeeFeatureInfo", Name = "EmployeeFeatureInfo")]
        public JsonResult GetEmployeeFeatureInfo(string employeeId)
        {
            var emp = Employee.GetById(employeeId);
            return Json(new
            {
                ADAFeatureEnabled = emp.Employer.HasFeature(Feature.ADA),
                STDFeatureEnabled = emp.Employer.HasFeature(Feature.ShortTermDisability),
                GuidelinesDataEnabled = emp.Employer.HasFeature(Feature.GuidelinesData),
                LOAFeatureEnabled = emp.Employer.HasFeature(Feature.LOA),
                ShowIsWorkRelated = CurrentCustomer.Metadata.GetRawValue<bool>("ShowIsWorkRelated") || CurrentCustomer.HasFeature(Feature.WorkRelatedReporting),
                ShowContactSection = CurrentCustomer.Metadata.GetRawValue<bool>("ShowContactSection"),
                ShowOtherQuestions = CurrentCustomer.Metadata.GetRawValue<bool>("ShowOtherQuestions"),
            });
        }


        [Secure("CreateCase"), HttpGet, Title("Create Case")]
        [Route("Employees/{employeeId}/Cases/Accom", Name = "CreateAccomCase")]
        public ViewResult CreateAccomCase(string employeeId)
        {
            Employee emp = Employee.GetById(employeeId);
            var viewModel = new CreateCaseModel()
            {
                EmployeeId = employeeId,
                EmployerId = emp.EmployerId,
                ADAFeatureEnabled = emp.Employer.HasFeature(Feature.ADA),
                STDFeatureEnabled = emp.Employer.HasFeature(Feature.ShortTermDisability),
                IsAccommodation = true

            };

            return View("Create", viewModel);
        }

        [Secure("CreateCase"), HttpPost, Title("Create Case")]//, Secure(Permission.CreateCase)]
        [Route("Employees/{employeeId}/Cases/Create", Name = "CreateCasePost")]
        [ValidateApiAntiForgeryToken]
        public JsonResult Create(CreateCaseModel model)
        {
            try
            {
                var leaveOfAbsence = this.ToLeaveOfAbsence(model);

                //if (model.CustomFields != null && model.CustomFields.Count > 0)
                //    leaveOfAbsence.Case.CustomFields = model.CustomFields;

                Case savedCase = new CaseService().Using(c => c.UpdateCase(leaveOfAbsence.Case, CaseEventType.CaseCreated));
                // ^^ Do not call Case.Save() directly here, use the service like above. ^^

                List<VariableScheduleTime> variableTime = new List<VariableScheduleTime>();
                if (model.WorkSchedule.ActualTimes.Count > 0 && model.WorkSchedule.ScheduleType == ScheduleType.Variable)
                {
                    variableTime = model.WorkSchedule.ApplyActualTimesToDataModel(variableTime);
                }
                // Store variable time
                if (variableTime != null && variableTime.Any())
                {
                    new CaseService().Using(c => c.StoreVariableTime(variableTime, savedCase));
                    new EligibilityService().Using(e => e.RunEligibility(savedCase));
                    savedCase.Save();
                }
                if (model.Status != "Inquiry" && model.CustomFields != null && model.CustomFields.Count > 0)
                    leaveOfAbsence.Case.WfOnCustomFieldChangeEvent(CurrentUser);

                // Save extra field settings
                if (model.ShowOtherQuestions)
                {
                    if (!string.IsNullOrWhiteSpace(model.AccommodationRequest.GeneralHealthCondition))
                    {
                        CaseNote note = new CaseNote();
                        note.Category = NoteCategoryEnum.InteractiveProcess;
                        note.CreatedBy = CurrentUser;
                        note.CaseId = leaveOfAbsence.Case.Id;
                        note.CustomerId = leaveOfAbsence.Case.CustomerId;
                        note.EmployerId = leaveOfAbsence.Case.EmployerId;
                        note.Public = false;
                        note.ModifiedBy = CurrentUser;
                        note.Notes = "Summary Of Situation & Employee Ask - " + model.AccommodationRequest.GeneralHealthCondition;
                        note.Save();
                    }

                    if (!string.IsNullOrWhiteSpace(model.CurrentSchedule))
                    {
                        CaseNote note = new CaseNote();
                        note.Category = NoteCategoryEnum.CaseSummary;
                        note.CreatedBy = CurrentUser;
                        note.CaseId = leaveOfAbsence.Case.Id;
                        note.CustomerId = leaveOfAbsence.Case.CustomerId;
                        note.EmployerId = leaveOfAbsence.Case.EmployerId;
                        note.Public = false;
                        note.ModifiedBy = CurrentUser;
                        note.Notes = "What is the employee's current schedule? - " + model.CurrentSchedule;
                        note.Save();
                    }

                }

                if (model.AccommodationRequest != null && model.AccommodationRequest.AdditionalQuestions != null)
                {
                    var @case = leaveOfAbsence.Case;
                    MakeNotes(@case.Id, @case.CustomerId, @case.EmployerId, model.AccommodationRequest.AdditionalQuestions);
                }


                // Add new contact to employee's contact 
                string absenceReasonCode = string.Empty;
                if (leaveOfAbsence.Case.Status != CaseStatus.Inquiry)
                {
                    absenceReasonCode = AbsenceReason.GetById(model.AbsenceReasonId).Code;
                }

                if (!string.IsNullOrWhiteSpace(absenceReasonCode) && !String.IsNullOrEmpty(model.EmployeeRelationshipCode))
                {
                    //check absence reason flag whether it require relationship or not
                    AbsenceReason ar = AbsenceReason.GetById(model.AbsenceReasonId);
                    if ((ar.Flags & AbsenceReasonFlag.RequiresRelationship) == AbsenceReasonFlag.RequiresRelationship
                        && string.IsNullOrWhiteSpace(model.EmployeeContactId))
                    {
                        if (model.EmployeeRelationshipFirstName != null && model.EmployeeRelationshipLastName != null)
                        {
                            var employeeContact = new EmployeeContact()
                            {
                                EmployerId = leaveOfAbsence.Case.EmployerId,
                                CustomerId = CurrentCustomer.Id,
                                EmployeeId = model.EmployeeId,
                                EmployeeNumber = leaveOfAbsence.Case.Employee.EmployeeNumber,
                                ContactTypeCode = model.EmployeeRelationshipCode,
                                ContactTypeName = ContactType.GetByCode(model.EmployeeRelationshipCode).Name,
                                MilitaryStatus = model.MilitaryStatusId.HasValue ? (MilitaryStatus)model.MilitaryStatusId.Value : MilitaryStatus.Civilian,
                                Contact = new Contact()
                                {
                                    FirstName = model.EmployeeRelationshipFirstName,
                                    LastName = model.EmployeeRelationshipLastName,
                                    DateOfBirth = model.EmployeeRelationshipDateOfBirth
                                }
                            };

                            employeeContact.Save();
                            leaveOfAbsence.Case.Contact.Id = employeeContact.Id;
                            new CaseService().Using(c => c.UpdateCase(leaveOfAbsence.Case));
                        }
                    }
                }

                //create HCP contact if entered as part of STD intake
                if (((model.IsSTD.HasValue && model.IsSTD.Value) || model.GuidelinesDataEnabled) && !string.IsNullOrEmpty(model.Disability.HCPCompanyName) && !string.IsNullOrEmpty(model.Disability.HCPFirstName))
                {
                    var hcpContact = new EmployeeContact()
                    {
                        EmployerId = leaveOfAbsence.Case.EmployerId,
                        CustomerId = CurrentCustomer.Id,
                        EmployeeId = model.EmployeeId,
                        EmployeeNumber = leaveOfAbsence.Case.Employee.EmployeeNumber,
                        ContactTypeCode = model.Disability.HCPContactTypeCode,
                        ContactTypeName = ContactType.GetByCode(model.Disability.HCPContactTypeCode).Name,
                        MilitaryStatus = MilitaryStatus.Civilian,
                        Contact = new Contact()
                        {
                            CompanyName = model.Disability.HCPCompanyName,
                            FirstName = model.Disability.HCPFirstName,
                            LastName = model.Disability.HCPLastName,
                            WorkPhone = model.Disability.HCPPhone,
                            Fax = model.Disability.HCPFax,
                            IsPrimary = true
                        }
                    };
                    hcpContact.Save();

                    // set rest of the employee HCP contact IsPrimary false
                    using (var contactTypeService = new ContactTypeService())
                    {
                        contactTypeService.UpdateMedicalDesignationTypeContactsIsPrimary(CurrentUser.CustomerId, hcpContact.Contact.Id, model.EmployeeId);
                    }
                }

                //create HCP contact if entered as part of ADA intake                
                if (model.IsAccommodation.HasValue && model.IsAccommodation.Value && !string.IsNullOrEmpty(model.AccommodationRequest.HCPFirstName) && !string.IsNullOrEmpty(model.AccommodationRequest.HCPLastName))
                {
                    var hcpContact = new EmployeeContact()
                    {
                        EmployerId = leaveOfAbsence.Case.EmployerId,
                        CustomerId = CurrentCustomer.Id,
                        EmployeeId = model.EmployeeId,
                        EmployeeNumber = leaveOfAbsence.Case.Employee.EmployeeNumber,
                        ContactTypeCode = model.AccommodationRequest.HCPContactTypeCode,
                        ContactTypeName = model.AccommodationRequest.HCPContactTypeCode != null ? ContactType.GetByCode(model.AccommodationRequest.HCPContactTypeCode).Name : "",
                        MilitaryStatus = MilitaryStatus.Civilian,
                        Contact = new Contact()
                        {
                            CompanyName = model.AccommodationRequest.HCPCompanyName,
                            FirstName = model.AccommodationRequest.HCPFirstName,
                            LastName = model.AccommodationRequest.HCPLastName,
                            WorkPhone = model.AccommodationRequest.HCPPhone,
                            Fax = model.AccommodationRequest.HCPFax,
                            IsPrimary = true
                        }
                    };
                    hcpContact.Save();

                    // set rest of the employee HCP contact IsPrimary false
                    using (var contactTypeService = new ContactTypeService())
                    {
                        contactTypeService.UpdateMedicalDesignationTypeContactsIsPrimary(CurrentUser.CustomerId, hcpContact.Contact.Id, model.EmployeeId);
                    }
                }

                if (model.Job != null)
                {
                    if (model.Job.EmployeeNumber == null)
                        model.Job.EmployeeNumber = leaveOfAbsence.Employee.EmployeeNumber;

                    EmployeeJob job = leaveOfAbsence.Employee.GetCurrentJob();
                    job = model.Job.ApplyToDataModel(job, leaveOfAbsence.Employee.ServiceDate);
                    using (var jobService = new JobService(CurrentCustomer, CurrentEmployer, CurrentUser))
                    using (var employeeService = new EmployeeService())
                    {
                        jobService.UpdateEmployeeJob(job);
                        bool updateEmployee = false;
                        if (!string.IsNullOrEmpty(model.Job.Title))
                        {
                            leaveOfAbsence.Employee.JobTitle = model.Job.Title;
                            updateEmployee = true;
                        }

                        // they didn't select an activity level override, but the job is configured with one
                        if (job != null && job.Job != null && model.Job.Activity == null && job.Job.Activity != null)
                        {
                            leaveOfAbsence.Employee.JobActivity = job.Job.Activity;
                            updateEmployee = true;
                        }
                        // they selected an activity level
                        else if (model.Job.Activity != null)
                        {
                            leaveOfAbsence.Employee.JobActivity = model.Job.Activity;
                            updateEmployee = true;
                        }

                        if (updateEmployee)
                        {
                            employeeService.Update(leaveOfAbsence.Employee);
                        }
                    }
                }

                var returnModel = this.ToCommunicationModalModel(leaveOfAbsence);

                return Json(returnModel);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("CreateCase"), HttpPost, Title("Create Case")]
        [Route("ESS/{employeeId}/Cases/Create", Name = "CreateEmployeeSelfServiceCase")]
        [ValidateApiAntiForgeryToken]
        public JsonResult CreateEmployeeSelfServiceCase(CaseEssCreateModel model)
        {
            try
            {
                // Get changed properties details and create to do
                bool HasAnyPropertyChanged = false;
                EmployeeModel oldValues = new EmployeeModel();
                EmployeeModel newValues = new EmployeeModel();

                Employee emp = Employee.GetById(model.EmployeeId);
                var isEmployeeEditor = CurrentUser.HasEmployerAccess(emp.EmployerId, Permission.EditEmployee);
                Employee emp1 = Employee.GetById(model.EmployeeId);
                if (CurrentUser != null && !CurrentUser.HasEmployerAccess(emp.EmployerId, Permission.CreateCase))
                    throw new AbsenceSoftException("User does not have access to create a case for an employee with this employer");

                if (isEmployeeEditor || IsEmployeeSelfService)
                {
                    // if user has opted that contact information is incorrect change the emp info for case
                    // also save change employee information

                    if (model.IsContactInformationCorrect.HasValue && !model.IsContactInformationCorrect.Value)
                    {

                        // Create to do for employee information varification
                        EditEmployeeModel empOriginal = new EditEmployeeModel(emp1);

                        emp.Info.AltEmail = model.NewAltEmail;
                        emp.Info.Email = model.NewEmail;

                        emp.Info.WorkPhone = model.NewWorkPhone;
                        emp.Info.Address.Address1 = model.NewAddress;
                        emp.Info.Address.Address2 = string.Empty;
                        emp.Info.Address.City = model.NewCity;
                        emp.Info.Address.State = model.NewState;
                        emp.Info.Address.PostalCode = model.NewZipcode;
                        emp.Info.CellPhone = model.NewCellPhone;
                        emp.Info.HomePhone = model.NewHomePhone;
                        emp.Info.AltPhone = model.NewAltPhone;
                        emp.Info.AltAddress.Address1 = model.NewAltAddress;
                        emp.Info.AltAddress.Address2 = string.Empty;
                        emp.Info.AltAddress.City = model.NewAltCity;
                        emp.Info.AltAddress.State = model.NewAltState;
                        emp.Info.AltAddress.PostalCode = model.NewAltZipcode;

                        EditEmployeeModel empChanged = new EditEmployeeModel(emp);
                        GetPropertyInfos(empOriginal, empChanged, ref oldValues, ref newValues, ref HasAnyPropertyChanged);
                    }
                }

                //CaseEssReviewedModel reviewModel = new CaseEssReviewedModel(model);
                Case cAse = this.ToEmployeeLeaveRequest(model);

                if (model.Status == CaseStatus.Inquiry)
                {
                    cAse.Status = CaseStatus.Inquiry;
                }

                if (HasAnyPropertyChanged)
                {
                    // Save changed details
                    emp.Save();
                    emp.WfOnEmployeeChanged(CurrentUser, m => m
                        .SetRawValue("EmployeeInfoChanged", true)
                        .Add("OldValues", oldValues.ToBsonDocument())
                        .Add("NewValues", newValues.ToBsonDocument()));
                }
                if (model.SendECommunication.HasValue)
                {
                    cAse.SendECommunication = model.SendECommunication;
                    cAse.ECommunicationRequestDate = DateTime.UtcNow;
                }

                // MAJOR HACK FOR PARROT :-( :-( :-( (that's 3 sad faces!)
                if (model.IsEmployeeAtWork.HasValue)
                    cAse.Metadata.SetRawValue("IsEmployeeAtWork", model.IsEmployeeAtWork.Value);
                if (model.HasPerformanceIssue.HasValue)
                    cAse.Metadata.SetRawValue("HasPerformanceIssue", model.HasPerformanceIssue.Value);
                if (model.HasWorkConcerns.HasValue)
                    cAse.Metadata.SetRawValue("HasWorkConcerns", model.HasWorkConcerns.Value);
                if (model.IsWorkScheduleCorrect.HasValue)
                    cAse.Metadata.SetRawValue("IsWorkScheduleWrong", model.IsWorkScheduleCorrect == false);
                if (!string.IsNullOrEmpty(model.EmployeeAskForDesc))
                {
                    cAse.Metadata.SetRawValue("EmployeeAskForDesc", model.EmployeeAskForDesc);
                    /// More Hacks.  Parrot Wizard uses a different field for "Summary of Situation & Employee Ask" than regular case intake, but
                    /// they SHOULD be the same field
                    if (cAse.AccommodationRequest == null)
                        cAse.AccommodationRequest = new AccommodationRequest()
                        {
                            Status = CaseStatus.Open
                        };
                    cAse.AccommodationRequest.GeneralHealthCondition = model.EmployeeAskForDesc;
                }

                if (!string.IsNullOrEmpty(model.WorkplaceModificationDesc))
                    cAse.Metadata.SetRawValue("WorkplaceModificationDesc", model.WorkplaceModificationDesc);
                if (!string.IsNullOrEmpty(model.EmpOtherLeaveCaseNumber))
                    cAse.Metadata.SetRawValue("EmpOtherLeaveCaseNumber", model.EmpOtherLeaveCaseNumber);
                if (!string.IsNullOrEmpty(model.PerformanceIssueDesc))
                    cAse.Metadata.SetRawValue("PerformanceIssueDesc", model.PerformanceIssueDesc);
                if (!string.IsNullOrEmpty(model.CurrentSchedule))
                    cAse.Metadata.SetRawValue("CurrentSchedule", model.CurrentSchedule);


                cAse = new CaseService().Using(c => c.UpdateCase(cAse, CaseEventType.CaseCreated));

                //create HCP contact if entered as part of ADA intake                
                if (model.IsAccommodation.HasValue && model.IsAccommodation.Value && !string.IsNullOrEmpty(model.AccommodationRequest.HCPFirstName) && !string.IsNullOrEmpty(model.AccommodationRequest.HCPLastName))
                {
                    var hcpContact = new EmployeeContact()
                    {
                        EmployerId = cAse.EmployerId,
                        CustomerId = CurrentCustomer.Id,
                        EmployeeId = model.EmployeeId,
                        EmployeeNumber = cAse.Employee.EmployeeNumber,
                        ContactTypeCode = "PROVIDER",
                        ContactTypeName = "Provider",
                        MilitaryStatus = MilitaryStatus.Civilian,
                        Contact = new Contact()
                        {
                            FirstName = model.AccommodationRequest.HCPFirstName,
                            LastName = model.AccommodationRequest.HCPLastName,
                            WorkPhone = model.AccommodationRequest.HCPPhone,
                            Fax = model.AccommodationRequest.HCPFax,
                            IsPrimary = true
                        }
                    };
                    hcpContact.Save();

                    // set rest of the employee HCP contact IsPrimary false
                    using (var contactTypeService = new ContactTypeService())
                    {
                        contactTypeService.UpdateMedicalDesignationTypeContactsIsPrimary(CurrentUser.CustomerId, hcpContact.Contact.Id, model.EmployeeId);
                    }
                }

                if (model.Job != null)
                {
                    if (model.Job.EmployeeNumber == null)
                        model.Job.EmployeeNumber = cAse.Employee.EmployeeNumber;

                    EmployeeJob job = cAse.Employee.GetCurrentJob();
                    job = model.Job.ApplyToDataModel(job, cAse.Employee.ServiceDate);
                    using (var jobService = new JobService(CurrentCustomer, CurrentEmployer, CurrentUser))
                    using (var employeeService = new EmployeeService())
                    {
                        jobService.UpdateEmployeeJob(job);
                        bool updateEmployee = false;
                        if (!string.IsNullOrEmpty(model.Job.Title))
                        {
                            cAse.Employee.JobTitle = model.Job.Title;
                            updateEmployee = true;
                        }
                        // they didn't select an activity level override, but the job is configured with one
                        if (job != null && job.Job != null && model.Job.Activity == null && job.Job.Activity != null)
                        {
                            cAse.Employee.JobActivity = job.Job.Activity;
                            updateEmployee = true;
                        }
                        // they selected an activity level
                        else if (model.Job.Activity != null)
                        {
                            cAse.Employee.JobActivity = model.Job.Activity;
                            updateEmployee = true;
                        }

                        if (updateEmployee)
                        {
                            employeeService.Update(cAse.Employee);
                        }
                    }
                }

                // Add new contact to employee's contact 
                string absenceReasonCode = AbsenceReason.GetById(model.AbsenceReasonId).Code;

                if (!string.IsNullOrWhiteSpace(absenceReasonCode) && !String.IsNullOrEmpty(model.EmployeeRelationshipCode))
                {
                    string[] absenceReasonCodes = new string[] { "EXIGENCY", "MILITARY", "RESERVETRAIN", "CEREMONY", "FHC", "PARENTAL", "DOM", "ADOPT", "BEREAVEMENT" };

                    if (Array.IndexOf(absenceReasonCodes, absenceReasonCode) >= 0 && string.IsNullOrWhiteSpace(model.EmployeeContactId))
                    {
                        if (model.EmployeeRelationshipFirstName != null && model.EmployeeRelationshipLastName != null)
                        {
                            var employeeContact = new EmployeeContact()
                            {
                                CustomerId = CurrentCustomer.Id,
                                EmployeeId = model.EmployeeId,
                                EmployeeNumber = cAse.Employee.EmployeeNumber,
                                ContactTypeCode = model.EmployeeRelationshipCode,
                                ContactTypeName = ContactType.GetByCode(model.EmployeeRelationshipCode).Name,
                                MilitaryStatus = model.MilitaryStatusId.HasValue ? (MilitaryStatus)model.MilitaryStatusId.Value : MilitaryStatus.Civilian,
                                Contact = new Contact()
                                {
                                    FirstName = model.EmployeeRelationshipFirstName,
                                    LastName = model.EmployeeRelationshipLastName
                                }
                            };

                            employeeContact.Save();
                        }
                    }
                    else if (Array.IndexOf(absenceReasonCodes, absenceReasonCode) >= 0 && !string.IsNullOrWhiteSpace(model.EmployeeContactId))
                    {
                        EmployeeContact empContact = EmployeeContact.AsQueryable().FirstOrDefault(m => m.Id == model.EmployeeContactId);
                        if (empContact != null)
                        {
                            empContact.Contact.FirstName = model.EmployeeRelationshipFirstName;
                            empContact.Contact.LastName = model.EmployeeRelationshipLastName;
                            empContact.Save();
                        }
                    }
                }

                // Fetch HR information for the employee
                var HRContact = EmployeeContact.AsQueryable().Where(m => m.EmployeeId == cAse.Employee.Id && m.ContactTypeCode == "HR").ToList();
                if (HRContact.Count <= 0)
                {
                    HRContact = EmployeeContact.AsQueryable().Where(m => m.EmployeeId == cAse.Employee.Id && m.ContactTypeCode == "SUPERVISOR").ToList();
                }

                CaseEssCreatedModel returnModel = new CaseEssCreatedModel()
                {
                    CaseId = cAse.Id,
                    CaseNumber = cAse.CaseNumber,
                    EmployeeId = cAse.Employee.Id,
                    EmployerAddress = cAse.Employer.Contact.Address,
                    EmployerName = cAse.Employer.Name,
                    HRContactName = HRContact.Count > 0 ? HRContact[0].Contact.FirstName + " " + HRContact[0].Contact.LastName : string.Empty,
                    HRContactPhone = HRContact.Count > 0 ? HRContact[0].Contact.WorkPhone : string.Empty
                };

                return Json(returnModel);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }



        [Secure("EditCase"), HttpPost]
        [Route("Cases/{caseId}/CommunicationsModalData", Name = "CaseCommunicationsModalData")]
        [ValidateApiAntiForgeryToken]
        public JsonResult CommunicationsModalData(String caseId)
        {
            try
            {
                LeaveOfAbsence leaveOfAbsence = null;
                using (var service = new EligibilityService())
                {
                    leaveOfAbsence = service.GetLeaveOfAbsence(caseId);
                }
                var returnModel = this.ToCommunicationModalModel(leaveOfAbsence);
                return Json(returnModel);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCase"), HttpPost]
        [Route("Employees/{employeeId}/Cases/CalculateEligibility", Name = "CalculateEligibilityForCase")]
        [ValidateApiAntiForgeryToken]
        public JsonResult CalculateEligibility(CreateCaseModel model)
        {
            // TODO: ensure valid before trying to create case or it will error our

            try
            {
                //Setting IsEnquiry as true because it is only calculating eligilbility
                model.IsInquiry = true;
                var leaveOfAbsence = this.ToLeaveOfAbsence(model);
                var returnModel = new CreateCaseModel();
                this.PopulateCasePoliciesData(returnModel, leaveOfAbsence, model);

                if (leaveOfAbsence.Case.SpouseCaseId != null && leaveOfAbsence.Case.SpouseCase != null)
                {
                    model.SpouseCaseId = leaveOfAbsence.Case.SpouseCaseId;
                    model.SpouseCaseNumber = leaveOfAbsence.Case.SpouseCase.CaseNumber;
                    model.SpouseFirstName = leaveOfAbsence.Case.SpouseCase.Employee.FirstName;
                    model.SpouseLastName = leaveOfAbsence.Case.SpouseCase.Employee.LastName;
                }

                returnModel.IsSTD = leaveOfAbsence.Case.IsSTD;
                returnModel.IsAccommodation = leaveOfAbsence.Case.IsAccommodation;
                returnModel.STDFeatureEnabled = model.STDFeatureEnabled;
                returnModel.GuidelinesDataEnabled = model.GuidelinesDataEnabled;
                returnModel.ADAFeatureEnabled = model.ADAFeatureEnabled;

                returnModel.AvailableManualPolicies.OrderBy(m => m.PolicyName);

                if (returnModel.AvailableManualPolicies.Count > 0)
                {
                    AppliedPolicyModel FMLAPolicyModel = returnModel.AvailableManualPolicies.Where(m => m.PolicyCode == "FMLA").FirstOrDefault();
                    AppliedPolicyModel STDPolicyModel = returnModel.AvailableManualPolicies.Where(m => m.PolicyCode == "STD").FirstOrDefault();
                    AppliedPolicyModel COMPMEDPolicyModel = returnModel.AvailableManualPolicies.Where(m => m.PolicyCode == "COMP-MED").FirstOrDefault();
                    AppliedPolicyModel USERRAPolicyModel = returnModel.AvailableManualPolicies.Where(m => m.PolicyCode == "USERRA").FirstOrDefault();

                    bool IsStdModelAvailabel = false;
                    bool IsFmlaModelAvailabel = false;
                    bool IsCompMedModelAvailabel = false;
                    bool IsUserraModelAvailabel = false;

                    if (STDPolicyModel != null)
                        IsStdModelAvailabel = true;
                    if (FMLAPolicyModel != null)
                        IsFmlaModelAvailabel = true;
                    if (COMPMEDPolicyModel != null)
                        IsCompMedModelAvailabel = true;
                    if (USERRAPolicyModel != null)
                        IsUserraModelAvailabel = true;

                    if (IsStdModelAvailabel)
                    {
                        returnModel.AvailableManualPolicies.Remove(STDPolicyModel);
                        returnModel.AvailableManualPolicies.Insert(0, STDPolicyModel);
                    }
                    if (IsFmlaModelAvailabel)
                    {
                        returnModel.AvailableManualPolicies.Remove(FMLAPolicyModel);
                        if (IsStdModelAvailabel)
                        {
                            returnModel.AvailableManualPolicies.Insert(1, FMLAPolicyModel);
                        }
                        else
                        {
                            returnModel.AvailableManualPolicies.Insert(0, FMLAPolicyModel);
                        }
                    }

                    if (IsCompMedModelAvailabel)
                    {
                        returnModel.AvailableManualPolicies.Remove(COMPMEDPolicyModel);
                        if (!IsStdModelAvailabel && !IsFmlaModelAvailabel)
                        {
                            returnModel.AvailableManualPolicies.Insert(0, COMPMEDPolicyModel);
                        }
                        if (IsStdModelAvailabel && IsFmlaModelAvailabel)
                        {
                            returnModel.AvailableManualPolicies.Insert(2, COMPMEDPolicyModel);
                        }
                        if ((IsStdModelAvailabel && !IsFmlaModelAvailabel) || (!IsStdModelAvailabel && IsFmlaModelAvailabel))
                        {
                            returnModel.AvailableManualPolicies.Insert(1, COMPMEDPolicyModel);
                        }

                    }
                    if (IsUserraModelAvailabel)
                    {
                        returnModel.AvailableManualPolicies.Remove(USERRAPolicyModel);
                        if (!IsStdModelAvailabel && !IsFmlaModelAvailabel && !IsCompMedModelAvailabel)
                        {
                            returnModel.AvailableManualPolicies.Insert(0, USERRAPolicyModel);
                        }
                        if (IsStdModelAvailabel && IsFmlaModelAvailabel && IsCompMedModelAvailabel)
                        {
                            returnModel.AvailableManualPolicies.Insert(3, USERRAPolicyModel);
                        }
                        if ((IsStdModelAvailabel && !IsFmlaModelAvailabel && !IsCompMedModelAvailabel) || (!IsStdModelAvailabel && IsFmlaModelAvailabel && !IsCompMedModelAvailabel) || (!IsStdModelAvailabel && !IsFmlaModelAvailabel && IsCompMedModelAvailabel))
                        {
                            returnModel.AvailableManualPolicies.Insert(1, USERRAPolicyModel);
                        }
                        if ((IsStdModelAvailabel && IsFmlaModelAvailabel && !IsCompMedModelAvailabel) || (IsStdModelAvailabel && !IsFmlaModelAvailabel && IsCompMedModelAvailabel) || (!IsStdModelAvailabel && IsFmlaModelAvailabel && IsCompMedModelAvailabel))
                        {
                            returnModel.AvailableManualPolicies.Insert(2, USERRAPolicyModel);
                        }
                    }
                }

                return Json(returnModel);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("CreateCase"), HttpGet, Title("Select Employee")]
        [Route("Cases/Create/SelectEmployee", Name = "SelectEmployeeForCreateCase")]
        public ActionResult SelectEmployee()
        {
            return View();
        }

        #endregion

        #region Cancel

        [Secure("CancelCase")]
        [Route("Cases/{caseId}/Cancel", Name = "CancelCase")] // ?reason={reason}
        public JsonResult CancelCase(string caseId, CaseCancelReason reason)
        {
            try
            {
                using (var svc = new CaseService())
                {
                    Case theCase = svc.CancelCase(caseId, reason);
                    svc.UpdateCase(theCase);
                    return Json(theCase);
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        #endregion

        #region ReOpen
        [Secure("EditCase")]
        [Route("Cases/{caseId}/Reopen", Name = "ReopenCase")]
        public JsonResult ReopenCase(string caseId)
        {
            try
            {
                var myCase = Case.GetById(caseId);
                if (myCase == null)
                {
                    return JsonError(new Exception("Case was not found or no Id was supplied"), "Case was not found or no Id was supplied", 404);
                }

                return Json(new CaseService().Using(c => c.ReopenCase(myCase)));
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCase")]
        [Route("Cases/{caseId}/Relapse", Name = "RelapseCase")]
        public ActionResult RelapseCase(ReOpenChangeModel reOpenChangeModel)
        {
            try
            {
                var myCase = Case.GetById(reOpenChangeModel.CaseId);
                if (reOpenChangeModel.CaseType == CaseType.None)
                {
                    reOpenChangeModel.CaseType = myCase.Segments.OrderByDescending(s => s.StartDate).FirstOrDefault().Type;
                }

                //get latest employee details
                var relapseEmployee = Employee.GetById(myCase.Employee.Id);
                Relapse relapse = new Relapse
                {
                    CaseId = reOpenChangeModel.CaseId,
                    StartDate = reOpenChangeModel.StartDate,
                    EndDate = reOpenChangeModel.EndDate,
                    CaseType = reOpenChangeModel.CaseType,
                    Employee = relapseEmployee,
                    EmployerId = CurrentEmployer.Id,
                    CustomerId = CurrentCustomer.Id
                };
                Schedule workSchedule = null;
                if (reOpenChangeModel.WorkSchedule != null)
                {
                    DateTime date = (DateTime)(reOpenChangeModel.WorkSchedule.StartDate != null ? reOpenChangeModel.WorkSchedule.StartDate : DateTime.Now.ToMidnight());
                    date = date.GetFirstDayOfWeek();
                    reOpenChangeModel.WorkSchedule.Times.ForEach(x => { x.SampleDate = date; date = date.AddDays(1); });
                    workSchedule = reOpenChangeModel.WorkSchedule.ApplyToDataModel(workSchedule);
                }
                LeaveOfAbsence loa = new CaseService().Using(c => c.ReopenCase(myCase, relapse));
                loa.WorkSchedule = new List<Schedule> { workSchedule };
                RevisedCaseModel relapseCaseModel = GetRevisedCaseModel(loa);
                relapseCaseModel.WorkSchedule.ActualTimes = reOpenChangeModel.WorkSchedule.ActualTimes;
                relapseCaseModel.FTWeeklyWorkTime = Current.Employer().FTWeeklyWorkHours.ToString();
                return View("RelapseCase", relapseCaseModel);
            }
            catch (Exception ex)
            {
                Log.Error("Error reopening the case for Relapse", ex);
                return CaseView(reOpenChangeModel.CaseId, false,
                    $"Error reopening the case, {ex.Message}");
            }
        }

        /// <summary>
        /// Build "revisedCaseModel" object to be displayed inUI
        /// </summary>
        /// <param name="leaveOfAbsence">Updated object having details of furthur Leave request</param>
        /// <returns></returns>
        private RevisedCaseModel GetRevisedCaseModel(LeaveOfAbsence leaveOfAbsence)
        {

            Case revisedCase = leaveOfAbsence.Case;
            Employee emp = Employee.GetById(revisedCase.Employee.Id);

            var caseInfo = Json(ToCaseViewModel(leaveOfAbsence));

            var hasAdminReasons = AbsenceReason.AsQueryable()
                        .Any(ar => (ar.EmployerId == null || ar.EmployerId == emp.EmployerId) && ar.Category == AbsenceReasonCategory.Administrative);

            RevisedCaseModel viewModel = null;

            viewModel = new RevisedCaseModel()
            {
                Status = Enum.GetName(typeof(CaseStatus), revisedCase.Status),
                IsInquiry = false,
                HasAdministrativeReasons = hasAdminReasons,
                CaseId = revisedCase.Id,
                EmployeeId = revisedCase.Employee.Id,
                EmployerId = revisedCase.EmployerId,
                ShortDescription = revisedCase.Description,
                Summary = revisedCase.Narrative,
                StartDate = leaveOfAbsence.Relapse?.StartDate, //New start date of requested leave
                EndDate = leaveOfAbsence.Relapse?.EndDate,     //New End date of requested leave
                ExistingStartDate = revisedCase.StartDate,
                ExistingEndDate = revisedCase.EndDate,

                CaseTypeId = (int)(Enum.Parse(typeof(CaseType), leaveOfAbsence.ActiveSegment.Type.ToString())),
                ADAFeatureEnabled = revisedCase.Employer.HasFeature(Feature.ADA),
                STDFeatureEnabled = revisedCase.Employer.HasFeature(Feature.ShortTermDisability),
                GuidelinesDataEnabled = revisedCase.Employer.HasFeature(Feature.GuidelinesData),
                STDPayFeatureEnabled = revisedCase.Employer.HasFeature(Feature.ShortTermDisablityPay),
                AccommodationTypeCategoriesFeatureEnabled = revisedCase.Employer.HasFeature(Feature.AccommodationTypeCategories),
                LOAFeatureEnabled = revisedCase.Employer.HasFeature(Feature.LOA),
                ShowIsWorkRelated = revisedCase.Customer.Metadata.GetRawValue<bool>("ShowIsWorkRelated") || CurrentCustomer.HasFeature(Feature.WorkRelatedReporting),
                ShowContactSection = revisedCase.Customer.Metadata.GetRawValue<bool>("ShowContactSection"),
                ShowOtherQuestions = revisedCase.Customer.Metadata.GetRawValue<bool>("ShowOtherQuestions"),
                EmployeePrimaryEmail = revisedCase.Employee.Info.Email,
                EmployeePrimaryPhone = revisedCase.Employee.Info.WorkPhone,
                EmployeeSecondaryPhone = revisedCase.Employee.Info.CellPhone,
                EmployeePersonalEmail = revisedCase.Employee.Info.AltEmail,
                EmployeeSecondaryEmail = revisedCase.Employee.Info.AltEmail,
                WorkRelatedInfo = new WorkRelatedViewModel(revisedCase),

                Job = new EmployeeJobViewModel(revisedCase.Employee.GetCurrentJob())
                {
                    EmployeeNumber = revisedCase.Employee.EmployeeNumber,
                    EmployeeId = revisedCase.Employee.Id,
                    EmployerId = revisedCase.Employee.EmployerId,
                    IHasJobHistory = (revisedCase.Employee.GetJobs().Count > 0)
                },
                Reason = revisedCase.Reason,
                IsSTD = revisedCase.IsSTD,
                CaseType = revisedCase.CaseType,
                CaseNumber = revisedCase.CaseNumber,
                RiskProfile = revisedCase.RiskProfile == null ? null : new RiskProfileViewModel(revisedCase.RiskProfile),
                StatusString = revisedCase.Status == CaseStatus.Requested ? CaseStatus.Open.ToString().SplitCamelCaseString() : revisedCase.Status.ToString().SplitCamelCaseString(),
                AssignedToName = revisedCase.AssignedToName,
                ShortSummary = revisedCase.Narrative,
                Contact = revisedCase.Contact,
                AccommodationCategory = leaveOfAbsence.AccommodationCategory(),
                cdt = revisedCase.CreatedDate,
                mdt = revisedCase.ModifiedDate,
                ClosedDt = revisedCase.GetClosedDate(),
                ClosureReasonDetails = revisedCase.ClosureReasonDetails,
                CancelDt = revisedCase.GetCancelledDate(),
                CaseEvents = revisedCase.CaseEvents.Select(ce => new CaseEventViewModel(ce)).ToList(),
                Email = revisedCase.Employee.Info.AltEmail,
                PhoneNumber = revisedCase.Employee.Info.AltPhone,
                Address = revisedCase.Employee.Info.AltAddress == null ? null : revisedCase.Employee.Info.AltAddress.ToString(),
                Address1 = revisedCase.Employee.Info.AltAddress == null ? null : revisedCase.Employee.Info.AltAddress.Address1,
                Address2 = revisedCase.Employee.Info.AltAddress == null ? null : revisedCase.Employee.Info.AltAddress.Address2,
                City = revisedCase.Employee.Info.AltAddress == null ? null : revisedCase.Employee.Info.AltAddress.City,
                State = revisedCase.Employee.Info.AltAddress == null ? null : revisedCase.Employee.Info.AltAddress.State,
                Country = revisedCase.Employee.Info.AltAddress == null ? null : revisedCase.Employee.Info.AltAddress.Country,
                PostalCode = revisedCase.Employee.Info.AltAddress == null ? null : revisedCase.Employee.Info.AltAddress.PostalCode,
                SendECommunication = revisedCase.SendECommunication,
                Relapse = leaveOfAbsence.Relapse,
                RelapseCase = revisedCase,
                EmployeeHasFTESchedule = emp.WorkSchedules.Any(w => w.ScheduleType == ScheduleType.FteVariable)
            };

            viewModel.CaseReporter = new CreateContactViewModel(revisedCase.CaseReporter);

            var caseAssignees = CaseAssignee.AsQueryable().Where(ca => ca.CaseId == revisedCase.Id).Select(ca => new { CaseTypeAssigneeCode = ca.Code, ca.UserId }).ToList();
            List<string> assigneeUserIds = caseAssignees.Select(ca => ca.UserId).ToList();
            List<string> caseAssigneeTypeCodes = caseAssignees.Select(ca => ca.CaseTypeAssigneeCode).ToList();
            List<User> assigneeUsers = Data.Security.User.AsQueryable().Where(user => assigneeUserIds.Contains(user.Id)).ToList();
            List<CaseAssigneeType> caseAssigmentTypes = CaseAssigneeType.AsQueryable().Where(cat => caseAssigneeTypeCodes.Contains(cat.Code)).ToList();
            var caseAssigneelist = new List<dynamic>();
            foreach (var ca in caseAssignees)
            {
                dynamic caseAssignee = new System.Dynamic.ExpandoObject();
                caseAssignee.CaseAssigneeTypeCode = ca.CaseTypeAssigneeCode;
                var cat = caseAssigmentTypes.FirstOrDefault(r => ca.CaseTypeAssigneeCode == r.Code && r.CustomerId == leaveOfAbsence.Case.CustomerId);
                if (cat == null)
                    continue;
                caseAssignee.CaseAssigneeTypeName = cat.Name;
                User user = string.IsNullOrWhiteSpace(ca.UserId) ? null : assigneeUsers.FirstOrDefault(u => u.Id == ca.UserId);
                caseAssignee.UserId = user?.Id;
                caseAssignee.UserName = user?.DisplayName;
                caseAssigneelist.Add(caseAssignee);
            }

            viewModel.CaseAutoAssignedTo = caseAssigneelist;
            viewModel.AppliedPolicies.AddRange(MapToModel(leaveOfAbsence.Case, leaveOfAbsence.ActiveSegment.AppliedPolicies.Where(o => !o.ManuallyAdded && !o.Policy.IsDeleted).ToList()));
            viewModel.ManuallyAppliedPolicies.AddRange(MapToModel(leaveOfAbsence.Case, leaveOfAbsence.ActiveSegment.AppliedPolicies.Where(o => o.ManuallyAdded && !o.Policy.IsDeleted).ToList()));

            viewModel = PopulateAvailableManualPolicies(viewModel, leaveOfAbsence);

            var adjudications = GetCaseAdjudications(leaveOfAbsence.Case);
            if (adjudications != null && adjudications.Any())
            {
                Action<AppliedPolicyModel> getDetail = new Action<AppliedPolicyModel>(p =>
                {
                    var sum = adjudications.FirstOrDefault(a => a.PolicyCode == p.PolicyCode);
                    if (sum != null)
                    {
                        if (sum.Detail.Any())
                        {
                            p.StartDate = sum.Detail.Min(d => d.StartDate);
                            p.EndDate = sum.Detail.Max(d => d.EndDate);
                        }
                        p.Adjudications = sum.Detail;
                        p.Messages.AddRangeIfNotExists(sum.Messages);

                        // Allow this if the user has permissions to do it, AND, this policy has dependent policies that are consecutive to it
                        p.AllowChangeEndDate = Data.Security.User.Permissions.ProjectedPermissions.Contains(Permission.AdjudicateCase.Id) &&
                            leaveOfAbsence.Case.Segments.SelectMany(s => s.AppliedPolicies).Where(s => s.Policy.ConsecutiveTo.Contains(p.PolicyCode)).Any();
                    }
                });
                viewModel.AppliedPolicies.ForEach(getDetail);
                viewModel.ManuallyAppliedPolicies.ForEach(getDetail);
            }

            Schedule workSchedule = new Schedule();
            if (leaveOfAbsence.WorkSchedule != null && leaveOfAbsence.WorkSchedule.Count > 0)
            {
                var activeSched = leaveOfAbsence.WorkSchedule.FirstOrDefault(s => DateTime.UtcNow.DateInRange(s.StartDate, s.EndDate));
                if (activeSched == null)
                    activeSched = leaveOfAbsence.WorkSchedule.OrderBy(d => d.StartDate).FirstOrDefault();
                if (activeSched != null)
                    workSchedule = activeSched;
            }
            viewModel.WorkSchedule = new ScheduleViewModel(workSchedule);

            using (EmployerService svc = new EmployerService(CurrentCustomer, leaveOfAbsence.Case.Employer, CurrentUser))
            {
                viewModel.CustomFields = svc.GetCustomFields(EntityTarget.Case).ToList();
                foreach (CustomField fld in viewModel.CustomFields)
                {
                    CustomField field = leaveOfAbsence.Case.CustomFields.Where(x => x.Id.Equals(fld.Id)).FirstOrDefault();
                    if (field != null)
                        fld.SelectedValue = field.SelectedValue;
                }
            }

            return viewModel;
        }
        /// <summary>
        /// Fetch a list of Manual Policies that user can select in UI and apply for it
        /// </summary>
        /// <param name="returnModel"> Object tobe used in View</param>
        /// <param name="leaveOfAbsence">Details of Case, existing and new Leave information</param>
        /// <returns></returns>
        private RevisedCaseModel PopulateAvailableManualPolicies(RevisedCaseModel returnModel, LeaveOfAbsence leaveOfAbsence)
        {
            using (var service = new EligibilityService())
            {
                returnModel.AvailableManualPolicies.AddRange(service.GetAvailableManualPolicies(leaveOfAbsence.Case).Select(o => new AppliedPolicyModel()
                {
                    PolicyId = o.Id,
                    PolicyName = o.Name,
                    PolicyCode = o.Code,
                    PolicyType = o.PolicyType.ToString()
                }));
            }
            if (returnModel.AvailableManualPolicies.Count > 0)
            {
                AppliedPolicyModel FMLAPolicyModel = returnModel.AvailableManualPolicies.Where(m => m.PolicyCode == "FMLA").FirstOrDefault();
                AppliedPolicyModel STDPolicyModel = returnModel.AvailableManualPolicies.Where(m => m.PolicyCode == "STD").FirstOrDefault();
                AppliedPolicyModel COMPMEDPolicyModel = returnModel.AvailableManualPolicies.Where(m => m.PolicyCode == "COMP-MED").FirstOrDefault();
                AppliedPolicyModel USERRAPolicyModel = returnModel.AvailableManualPolicies.Where(m => m.PolicyCode == "USERRA").FirstOrDefault();

                bool IsStdModelAvailabel = false;
                bool IsFmlaModelAvailabel = false;
                bool IsCompMedModelAvailabel = false;
                bool IsUserraModelAvailabel = false;

                if (STDPolicyModel != null)
                    IsStdModelAvailabel = true;
                if (FMLAPolicyModel != null)
                    IsFmlaModelAvailabel = true;
                if (COMPMEDPolicyModel != null)
                    IsCompMedModelAvailabel = true;
                if (USERRAPolicyModel != null)
                    IsUserraModelAvailabel = true;

                if (IsStdModelAvailabel)
                {
                    returnModel.AvailableManualPolicies.Remove(STDPolicyModel);
                    returnModel.AvailableManualPolicies.Insert(0, STDPolicyModel);
                }
                if (IsFmlaModelAvailabel)
                {
                    returnModel.AvailableManualPolicies.Remove(FMLAPolicyModel);
                    if (IsStdModelAvailabel)
                    {
                        returnModel.AvailableManualPolicies.Insert(1, FMLAPolicyModel);
                    }
                    else
                    {
                        returnModel.AvailableManualPolicies.Insert(0, FMLAPolicyModel);
                    }
                }

                if (IsCompMedModelAvailabel)
                {
                    returnModel.AvailableManualPolicies.Remove(COMPMEDPolicyModel);
                    if (!IsStdModelAvailabel && !IsFmlaModelAvailabel)
                    {
                        returnModel.AvailableManualPolicies.Insert(0, COMPMEDPolicyModel);
                    }
                    if (IsStdModelAvailabel && IsFmlaModelAvailabel)
                    {
                        returnModel.AvailableManualPolicies.Insert(2, COMPMEDPolicyModel);
                    }
                    if ((IsStdModelAvailabel && !IsFmlaModelAvailabel) || (!IsStdModelAvailabel && IsFmlaModelAvailabel))
                    {
                        returnModel.AvailableManualPolicies.Insert(1, COMPMEDPolicyModel);
                    }

                }
                if (IsUserraModelAvailabel)
                {
                    returnModel.AvailableManualPolicies.Remove(USERRAPolicyModel);
                    if (!IsStdModelAvailabel && !IsFmlaModelAvailabel && !IsCompMedModelAvailabel)
                    {
                        returnModel.AvailableManualPolicies.Insert(0, USERRAPolicyModel);
                    }
                    if (IsStdModelAvailabel && IsFmlaModelAvailabel && IsCompMedModelAvailabel)
                    {
                        returnModel.AvailableManualPolicies.Insert(3, USERRAPolicyModel);
                    }
                    if ((IsStdModelAvailabel && !IsFmlaModelAvailabel && !IsCompMedModelAvailabel) || (!IsStdModelAvailabel && IsFmlaModelAvailabel && !IsCompMedModelAvailabel) || (!IsStdModelAvailabel && !IsFmlaModelAvailabel && IsCompMedModelAvailabel))
                    {
                        returnModel.AvailableManualPolicies.Insert(1, USERRAPolicyModel);
                    }
                    if ((IsStdModelAvailabel && IsFmlaModelAvailabel && !IsCompMedModelAvailabel) || (IsStdModelAvailabel && !IsFmlaModelAvailabel && IsCompMedModelAvailabel) || (!IsStdModelAvailabel && IsFmlaModelAvailabel && IsCompMedModelAvailabel))
                    {
                        returnModel.AvailableManualPolicies.Insert(2, USERRAPolicyModel);
                    }
                }
            }
            return returnModel;
        }

        /// <summary>
        /// Process the user request of reopening a leave request
        /// </summary>
        /// <param name="caseModel">Object having user request details provided in View</param>
        /// <returns></returns>
        [Secure(Permission.CreateCasePermissionId), HttpPost, Title("Create Case")]
        [Route("Employees/Case/UpdateRelapse", Name = "UpdateRelapseCase")]
        [ValidateApiAntiForgeryToken]
        public JsonResult UpdateRelapseCase(RevisedCaseModel caseModel)
        {
            try
            {
                Case revisedCase = ToCase(caseModel);

                using (CaseService svc = new CaseService())
                {
                    revisedCase = svc.UpdateRelapse(revisedCase, caseModel.Relapse);
                    List<VariableScheduleTime> variableTime = new List<VariableScheduleTime>();
                    if (caseModel.WorkSchedule.ActualTimes.Count > 0 && caseModel.WorkSchedule.ScheduleType == ScheduleType.Variable)
                    {
                        variableTime = caseModel.WorkSchedule.ApplyActualTimesToDataModel(variableTime);
                        svc.StoreVariableTime(variableTime, revisedCase);
                    }
                }

                var result = new { Result = "Success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        /// <summary>
        /// Conver user request( View Data) to Case Object to be saved in database
        /// </summary>
        /// <param name="model">Object having Details of user leave request</param>
        /// <returns></returns>
        private Case ToCase(RevisedCaseModel model)
        {

            using (var service = new CaseService())
            {

                model.RelapseCase.Status = (CaseStatus)Enum.Parse(typeof(CaseStatus), model.Status);
                // Employee contact information
                // currently get saved in 
                if (!string.IsNullOrWhiteSpace(model.EmployeePrimaryPhone))
                {
                    model.RelapseCase.Employee.Info.WorkPhone = model.EmployeePrimaryPhone;
                }

                if (!string.IsNullOrWhiteSpace(model.EmployeeSecondaryPhone))
                {
                    model.RelapseCase.Employee.Info.CellPhone = model.EmployeeSecondaryPhone;
                }

                if (!string.IsNullOrWhiteSpace(model.EmployeePrimaryEmail))
                {
                    model.RelapseCase.Employee.Info.Email = model.EmployeePrimaryEmail;
                    Employee e = Employee.GetById(model.RelapseCase.Employee.Id);
                    e.Info.Email = model.EmployeePrimaryEmail;
                    e.Save();
                }

                if (!string.IsNullOrWhiteSpace(model.EmployeePersonalEmail))
                {
                    model.RelapseCase.Employee.Info.AltEmail = model.EmployeePersonalEmail;
                    Employee e = Employee.GetById(model.RelapseCase.Employee.Id);
                    e.Info.AltEmail = model.EmployeePersonalEmail;
                    e.Save();
                }

                if (model.NewAddress != null)
                {
                    model.RelapseCase.Employee.Info.Address = model.NewAddress;
                }

                if (model.NewAltAddress != null)
                {
                    model.RelapseCase.Employee.Info.AltAddress = model.NewAltAddress;
                }

                if (!string.IsNullOrWhiteSpace(model.ContactPreference))
                {
                    model.RelapseCase.Metadata.SetRawValue("ContactPreference", model.ContactPreference);
                }

                if (!string.IsNullOrWhiteSpace(model.TimePreference))
                {
                    model.RelapseCase.Metadata.SetRawValue("TimePreference", model.TimePreference);
                }

                if (model.SendECommunication.HasValue)
                {
                    model.RelapseCase.SendECommunication = model.SendECommunication;
                    model.RelapseCase.ECommunicationRequestDate = DateTime.UtcNow;
                }

                // Employee mailing address
                if (model.ShowContactSection && model.IsContactInformationCorrect.HasValue && !model.IsContactInformationCorrect.Value)
                {
                    model.RelapseCase.Employee.Info.Address = model.NewAddress;
                }

                if (model.ShowContactSection && model.IsAltAddressCorrect.HasValue && !model.IsAltAddressCorrect.Value)
                {
                    model.RelapseCase.Employee.Info.AltAddress = model.NewAltAddress;
                }

                if (model.CustomFields != null && model.CustomFields.Count > 0)
                    model.RelapseCase.CustomFields = model.CustomFields;

            }
            // override the policy rules per the model
            if (model.RelapseCase.Segments.Any())
            {
                foreach (var policyModel in model.AppliedPolicies)
                {
                    var caseSegment = model.RelapseCase.Segments.OrderByDescending(d => d.StartDate).First();
                    var policy = caseSegment.AppliedPolicies.FirstOrDefault(o => o.Policy.Code == policyModel.PolicyCode);
                    if (policy != null)
                    {
                        foreach (var ruleGroupModel in policyModel.RuleGroups)
                        {
                            var ruleGroup = policy.RuleGroups.FirstOrDefault(o => o.RuleGroup.Id == ruleGroupModel.Id);
                            if (ruleGroup != null)
                            {
                                foreach (var ruleModel in ruleGroupModel.Rules)
                                {
                                    var rule = ruleGroup.Rules.FirstOrDefault(o => o.Rule.Id == ruleModel.Id);
                                    if (rule != null)
                                    {
                                        if (ruleModel.Overridden)
                                        {
                                            rule.Overridden = true;
                                            rule.OverrideFromResult = ruleModel.Result;
                                            rule.Result = ruleModel.OverrideResult;
                                            rule.OverrideNotes = ruleModel.OverrideNotes;
                                            rule.OverrideValue = ruleModel.OverrideValue;
                                        }
                                        else
                                        {
                                            rule.Overridden = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            var segment = model.RelapseCase.Segments.OrderByDescending(d => d.StartDate).First();
            segment.AppliedPolicies.RemoveAll(p => p.ManuallyAdded == true);
            // add manual policies per the model
            if (model.ManuallyAppliedPolicies.Any())
            {
                foreach (var manuallyAppliedPolicy in model.ManuallyAppliedPolicies)
                {
                    var manualPolicy = Policy.GetByCode(manuallyAppliedPolicy.PolicyCode, model.RelapseCase.CustomerId, model.RelapseCase.EmployerId);

                    string absenceReasonCode = model.RelapseCase.Reason.Code;
                    PolicyAbsenceReason reason = manualPolicy.AbsenceReasons.FirstOrDefault(a => a.Reason.Code == absenceReasonCode);

                    if (segment.AppliedPolicies.Select(p => p.Policy.Code).Any(p => p == manualPolicy.Code))
                        continue;

                    AppliedPolicy aPolicy = new AppliedPolicy();
                    aPolicy.StartDate = segment.StartDate;
                    aPolicy.EndDate = segment.EndDate.Value;
                    aPolicy.Policy = manualPolicy;
                    aPolicy.PolicyReason = reason;
                    if (aPolicy.PolicyReason != null)
                        aPolicy.PolicyReason.PeriodType = aPolicy.PolicyReason.PeriodType ?? model.RelapseCase.Employer.FMLPeriodType;
                    aPolicy.Status = EligibilityStatus.Eligible;
                    aPolicy.ManuallyAdded = true;
                    aPolicy.ManuallyAddedNote = manuallyAppliedPolicy.ManuallyAppliedPolicyNotes;
                    segment.AppliedPolicies.Add(aPolicy);

                }
            }

            if (model.WorkSchedule != null)
            {
                Schedule workSchedule = null;
                workSchedule = model.WorkSchedule.ApplyToDataModel(workSchedule);
                model.RelapseCase.Segments.FirstOrDefault(s => s.StartDate <= workSchedule.StartDate && s.EndDate >= workSchedule.StartDate).LeaveSchedule = new List<Schedule> { workSchedule };
            }
            return model.RelapseCase;
        }

        /// <summary>
        /// Calculate eligibility of Leave request on selective user modifications
        /// </summary>
        /// <param name="model">Object having Information on user changes for Leave request</param>
        /// <returns></returns>
        [Secure("EditCase"), HttpPost]
        [Route("Employees/{employeeId}/Cases/CalculateRelapseEligibility", Name = "CalculateRelapseEligibilityForCase")]
        [ValidateApiAntiForgeryToken]
        public JsonResult CalculateRelapseEligibility(RevisedCaseModel model)
        {
            try
            {
                Case relapseCase = ToCase(model);
                LeaveOfAbsence leaveOfAbsence;

                using (var eligiblityService = new EligibilityService())
                {
                    leaveOfAbsence = eligiblityService.RunEligibility(relapseCase, model.Relapse, model.RelapseDateChanged, true);
                }

                var returnModel = GetRevisedCaseModel(leaveOfAbsence);
                returnModel.IsAccommodation = leaveOfAbsence.Case.IsAccommodation;

                return Json(returnModel);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }



        #endregion


        #region Inquiry Accept, Close

        [Secure("EditCase", "ConvertInquiryToCase")]
        [Route("Inquiries/{caseId}/Accept", Name = "AcceptCase")]
        public JsonResult AcceptCase(string caseId)
        {
            try
            {
                var myCase = Case.GetById(caseId);
                if (myCase == null)
                    return JsonError(new Exception("Case was not found or no Id was supplied for Inquiry Accept"), "Case was not found or no Id was supplied for Inquiry Accept", 404);

                return Json(new CaseService().Using(c => c.AcceptInquiryCase(myCase)));
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCase")]
        [Route("Inquiries/{caseId}/Close", Name = "CloseCase")]
        public JsonResult CloseCase(string caseId)
        {
            try
            {
                var myCase = Case.GetById(caseId);
                if (myCase == null)
                    return JsonError(new Exception("Case was not found or no Id was supplied for Inquiry Close"), "Case was not found or no Id was supplied for Inquiry Close", 404);

                return Json(new CaseService().Using(c => c.CloseInquiryCase(myCase)));
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        #endregion

        #region EmployeeInfo

        [Secure("EditCase"), HttpPost]
        [Route("Cases/{caseId}/EmployeeInfo", Name = "UpdateEmployeeInfo")]
        [ValidateApiAntiForgeryToken]
        public JsonResult UpdateEmployeeInfo(EmployeeInfo info)
        {
            try
            {
                string caseId = RouteData.Values["caseId"] as string;
                Case theCase = Case.GetById(caseId);
                if (theCase == null)
                    return JsonError(null, "Case for employee info was not found");
                theCase.Employee.Info = info;
                theCase = new CaseService().Using(c => c.UpdateCase(theCase));
                return Json(info);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        #endregion


        [Secure("AdjudicateCase"), HttpPost]
        [Route("Cases/{caseId}/Adjudicate", Name = "AdjudicateCase")]
        [ValidateApiAntiForgeryToken]
        public JsonResult AdjudicateCase(CaseAdjudicationModel model)
        {
            try
            {
                using (var caseService = new CaseService())
                {
                    if (model.Case == null)
                    {
                        model.Case = caseService.GetCaseById(model.CaseId);
                    }
                    AdjudicationStatus? adjudicationStatus = null;
                    string adjudicationDenialReasonCode = null;
                    string adjudicationDenialReasonName = null;
                    string denialExplanation = null;

                    // Order of operations are sketchy here, just in case, do this really quick
                    model.Case = caseService.RunCalcs(model.Case);
                    DateTime? startDate = null, endDate = null;
                    foreach (CaseAdjudicationPolicyModel capm in model.Policies.Where(c => c.ApplyStatus))
                    {
                        //approved
                        if (model.ApprovedStartDate.HasValue && model.ApprovedEndDate.HasValue)
                        {
                            startDate = model.ApprovedStartDate.Value;
                            endDate = model.ApprovedEndDate.Value;
                            adjudicationStatus = AdjudicationStatus.Approved;
                            model.Case = caseService.ApplyDetermination(model.Case, capm.PolicyCode, model.ApprovedStartDate.Value, model.ApprovedEndDate.Value, adjudicationStatus.Value);
                        }
                        //denied
                        if (model.DeniedStartDate.HasValue && model.DeniedEndDate.HasValue)
                        {
                            startDate = model.DeniedStartDate.Value;
                            endDate = model.DeniedEndDate.Value;
                            adjudicationStatus = AdjudicationStatus.Denied;
                            if (!string.IsNullOrWhiteSpace(capm.AdjudicationDenialReasonCode))
                            {
                                adjudicationDenialReasonCode = capm.AdjudicationDenialReasonCode;
                                adjudicationDenialReasonName = capm.AdjudicationDenialReasonName;
                                denialExplanation = capm.DenialExplanation;
                            }
                            else
                            {
                                adjudicationDenialReasonCode = model.AdjudicationDenialReasonCode;
                                adjudicationDenialReasonName = model.AdjudicationDenialReasonName;
                                denialExplanation = model.DenialExplanation;
                            }
                            model.Case = caseService.ApplyDetermination(model.Case, capm.PolicyCode, model.DeniedStartDate.Value, model.DeniedEndDate.Value, adjudicationStatus.Value, adjudicationDenialReasonCode, denialExplanation, adjudicationDenialReasonName);
                        }
                        //pending
                        if (model.PendingStartDate.HasValue && model.PendingEndDate.HasValue)
                        {
                            startDate = model.PendingStartDate.Value;
                            endDate = model.PendingEndDate.Value;
                            adjudicationStatus = AdjudicationStatus.Pending;
                            model.Case = caseService.ApplyDetermination(model.Case, capm.PolicyCode, model.PendingStartDate.Value, model.PendingEndDate.Value, adjudicationStatus.Value);
                        }
                    }
                    model.Case.OnSavedNOnce((o, c) => c.Entity.WfOnCaseAdjudicated(adjudicationStatus.Value, null, null, null, null, startDate, endDate));
                    caseService.UpdateCase(model.Case);
                    caseService.EnqueueRecalcFutureCases(model.Case);
                }

                return Json(model.Case);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }




        [Secure("AdjudicateCase"), HttpPost]
        [Route("Cases/{caseId}/SetPolicyEndDate", Name = "SetPolicyEndDate")]
        [ValidateApiAntiForgeryToken]
        public JsonResult SetPolicyEndDate(SetPolicyEndDateModel model)
        {
            try
            {
                Case theCase = Case.GetById(model.CaseId);
                if (theCase == null)
                    throw new ArgumentException("The case was not found");

                using (CaseService svc = new CaseService())
                {
                    theCase = svc.SetPolicyEndDate(theCase, model.PolicyCode, model.EndDate);
                    theCase = svc.UpdateCase(theCase);
                }

                return Json(theCase);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("AdjudicateCase"), HttpPost]
        [Route("Cases/{caseId}/AdjudicateAllPendingTimeOnCase/{status}", Name = "AdjudicateAllPendingTimeOnCase")]
        [ValidateApiAntiForgeryToken]
        public JsonResult AdjudicateAllPendingTimeOnCase(string caseId, int status)
        {
            try
            {
                AdjudicationStatus caseStatus = (AdjudicationStatus)status;
                if (caseStatus != AdjudicationStatus.Approved || caseStatus != AdjudicationStatus.Denied)
                    return JsonError(new Exception("Incorrect Status"), "Status must be Approved or Denied", 401);

                var myCase = Case.GetById(caseId);
                if (myCase == null)
                    return JsonError(new Exception("Case was not found or no Id was supplied"), "Case was not found or no Id was supplied", 404);

                return Json(new CaseService().Using(c => c.AdjudicateAllPendingTimeOnCase(myCase, caseStatus)));
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        private class _CaseAdjudicationEvalGroup
        {
            public _CaseAdjudicationEvalGroup(CasePolicySummaryModel m) { model = m; }
            public CasePolicySummaryModel model { get; set; }
            public DateTime start { get; set; }
            public DateTime end { get; set; }
        }

        [Secure("AdjudicateCase"), HttpGet]
        [Route("Cases/{caseId}/Adjudicate", Name = "CaseAdjudications")]
        public JsonResult CaseAdjudications(string caseId)
        {
            try
            {

                Case c = Case.GetById(caseId);
                if (c == null)
                    return JsonError(new Exception("Not Found"), "Case could not be found", 404);

                var model = GetCaseAdjudications(c);

                return Json(model);

            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCase"), HttpPost]
        [Route("Cases/{caseId}/ReviseCase", Name = "ReviseCase")]
        public ActionResult ReviseCase(CaseChangeModel model)
        {
            try
            {
                var existingCase = Case.GetById(model.CaseId);
                Case changedCase = ChangeTheCase(existingCase, model);

                LeaveOfAbsence leave = new LeaveOfAbsence() { Case = changedCase };
                leave.ActiveSegment = changedCase.Segments.OrderByDescending(c => c.StartDate).FirstOrDefault(s => s.StartDate == model.StartDate);
                leave.CaseHistory = Case.AsQueryable().Where(c => c.Id != changedCase.Id && c.Employee.Id == changedCase.Employee.Id).ToList();
                leave.Employee = Employee.GetById(changedCase.Employee.Id); ;
                leave.Employer = leave.Employee.Employer;
                leave.Customer = leave.Employer.Customer;
                leave.Communications = Communication.AsQueryable().Where(c => c.CaseId == changedCase.Id).ToList();
                leave.Tasks = ToDoItem.AsQueryable().Where(t => t.CaseId == changedCase.Id).ToList();
                leave.WorkSchedule = Employee.GetById(changedCase.Employee.Id).WorkSchedules;

                RevisedCaseModel reviseCaseModel = GetRevisedCaseModel(leave);
                reviseCaseModel.WorkSchedule.ActualTimes = model.WorkSchedule.ActualTimes;
                reviseCaseModel.FTWeeklyWorkTime = Current.Employer().FTWeeklyWorkHours.ToString();
                reviseCaseModel.StartDate = model.StartDate;
                reviseCaseModel.EndDate = model.EndDate;
                reviseCaseModel.ChangeCaseData = model;
                return View("RelapseCase", reviseCaseModel);
            }
            catch (Exception ex)
            {
                Log.Error("Error changing the case from Administrative type to other type", ex);
                return CaseView(model.CaseId, false,
                    $"Error changing the case from Administrative type to other type, {ex.Message}");
            }
        }

        [Secure("EditCase"), HttpPost]
        [Route("Cases/{caseId}/Change", Name = "ChangeCase")]
        [ValidateApiAntiForgeryToken]
        public JsonResult ChangeCase(CaseChangeModel model)
        {
            var existingCase = Case.GetById(model.CaseId);

            var validationResult = ValidateChangeCaseData(model);

            if (validationResult != null)
            {
                return validationResult;
            }

            try
            {
                Case changedCase = ChangeTheCase(existingCase, model);

                using (CaseService svc = new CaseService())
                {
                    changedCase = svc.UpdateCase(changedCase);
                }

                return Json(model);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        private Case ChangeTheCase(Case existingCase, CaseChangeModel model)
        {
            if (model.IsEndDateNew)
            {
                existingCase.ExtendedDate = DateTime.UtcNow;
            }

            Schedule workSchedule = null;
            if (model.WorkSchedule != null)
            {
                workSchedule = model.WorkSchedule.ApplyToDataModel(workSchedule);
            }
            List<VariableScheduleTime> variableTime = new List<VariableScheduleTime>();
            if (model.WorkSchedule.ActualTimes.Count > 0 && model.WorkSchedule.ScheduleType == ScheduleType.Variable)
            {
                variableTime = model.WorkSchedule.ApplyActualTimesToDataModel(variableTime);
            }
            if (model.CaseType == CaseType.Administrative)
            {

            }
            using (CaseService svc = new CaseService())
            {
                existingCase = svc.ChangeCase(existingCase, model.StartDate, model.EndDate, model.IsStartDateNew, model.IsEndDateNew, model.CaseType, model.ModifyDecisions, model.ModifyAccommodations, workSchedule, variableTime);
                if (existingCase.WorkRelated != null)
                {
                    existingCase.WorkRelated.DaysAwayFromWork = svc.CalculateDaysAwayFromWork(existingCase);
                }
            }

            return existingCase;
        }

        [Secure("EditCase"), HttpPost]
        [Route("Cases/{caseId}/ValidateChangeCaseData", Name = "ValidateChangeCaseData")]
        [ValidateApiAntiForgeryToken]
        public JsonResult ValidateChangeCaseData(CaseChangeModel model)
        {
            var existingCase = Case.GetById(model.CaseId);

            if (existingCase == null)
                return JsonError(new Exception("Case was not found or no Id was supplied"), "Case was not found or no Id was supplied", 404);

            //All date validation need to be done against the Case Segment which the user needs to update. 
            //StartDate and EndDate of currentSegment will need to be used for date range validations
            var currentSegment = existingCase.Segments.OrderByDescending(s => s.StartDate).FirstOrDefault();

            DateTime currentStartDate = existingCase.StartDate;
            DateTime currentEndDate = existingCase.EndDate.HasValue ? (DateTime)existingCase.EndDate : DateTime.MinValue;

            // This is to varify if case is relapse or not
            if (existingCase.HasRelapse())
            {
                currentStartDate = currentSegment.StartDate;
                currentEndDate = (DateTime)currentSegment.EndDate;
            }

            // If it looks like they're extending the case, then they probably are; set the appropriate flags
            if (!model.IsStartDateNew && currentStartDate > model.StartDate)
                return JsonError(new AbsenceSoftException("The start date specified is before the start date of the case and new start date is not specified."),
                    "The start date specified is before the start date of the case and new start date is not specified.",
                    (int)HttpStatusCode.BadRequest);

            if (!model.IsEndDateNew && currentEndDate < model.EndDate)
                return JsonError(new AbsenceSoftException("The end date specified is after the end date of the case and new end date is not specified."),
                    "The end date specified is after the end date of the case and new end date is not specified.",
                    (int)HttpStatusCode.BadRequest);

            if ((!model.IsStartDateNew && model.StartDate < currentStartDate) || (!model.IsEndDateNew && model.EndDate > currentEndDate))
                return JsonError(new Exception("The dates requested fall outside of the bounds of the case. If this is intentional, please check the appropriate box(es)."),
                    "The dates requested fall outside of the bounds of the case. If this is intentional, please check the appropriate box(es).",
                    (int)HttpStatusCode.BadRequest);

            return null;
        }

        [HttpGet, Route("Cases/{caseId}/ReducedVariableSchedule")]
        public JsonResult GetReducedVariableSchedule(string caseId)
        {
            try
            {
                List<TimeModel> times = new CaseService().Using(c => c.GetReducedVariableSchedule(caseId))
                    .Select(t => new TimeModel()
                    {
                        TotalMinutes = ((int)t.Time.TotalMinutes).ToFriendlyTime(),
                        SampleDate = t.Time.SampleDate
                    }).OrderBy(a => a.SampleDate).ToList();
                return Json(times);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCase"), HttpPost]
        [Route("Cases/{caseId}/UpdateCustomFields", Name = "UpdateCaseCustomFields")]
        [ValidateApiAntiForgeryToken]
        public JsonResult UpdateCaseCustomFields(EditCaseCustomFieldsModel model)
        {
            string caseId = RouteData.Values["caseId"].ToString();
            if (string.IsNullOrWhiteSpace(caseId))
                throw new ArgumentNullException("Case id null");

            Case cAse = Case.GetById(caseId);
            if (cAse == null)
            {
                throw new Exception("Case not found");
            }

            try
            {
                cAse.CustomFields = model.CustomFields;
                new CaseService().Using(m => m.UpdateCase(cAse));
                if (model.CustomFields != null && model.CustomFields.Count > 0)
                    cAse.WfOnCustomFieldChangeEvent(CurrentUser);

                return Json(new { Success = true });
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCase"), HttpPost]
        [Route("Cases/{caseId}/UpdateAdditionalInfo", Name = "UpdateAdditionalInfo")]
        [ValidateApiAntiForgeryToken]
        public JsonResult UpdateCaseCustomFields(EditAdditionalInfoModel model)
        {
            string caseId = RouteData.Values["caseId"].ToString();
            if (string.IsNullOrWhiteSpace(caseId))
                throw new ArgumentNullException("Case id null");

            Case cAse = Case.GetById(caseId);
            if (cAse == null)
            {
                throw new Exception("Case not found");
            }

            try
            {
                cAse.Metadata.SetRawValue("ContactPreference", model.ContactPreference);
                cAse.Metadata.SetRawValue("TimePreference", model.TimePreference);
                if (model.SendECommunication.HasValue)
                {
                    cAse.SendECommunication = model.SendECommunication;
                    cAse.ECommunicationRequestDate = DateTime.UtcNow;
                }
                cAse.Employee.Info.AltEmail = model.Email;
                cAse.Employee.Info.AltPhone = model.PhoneNumber;
                model.ApplyAddressInfoToCase(cAse);
                cAse.OnSavedNOnce((o, c) => c.Entity.WfOnContactInfoChangeEvent(Current.User()));
                new CaseService().Using(m => m.UpdateCase(cAse));
                return Json(new { Success = true });
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }


        [Secure("EditCaseSummaryDescription"), HttpPost]
        [Route("Cases/{caseId}/UpdateCaseInfo", Name = "UpdateCaseInfo")]
        [ValidateApiAntiForgeryToken]
        public JsonResult UpdateCaseInfo(string caseId, EditCaseInfoModel model)
        {

            using (var caseService = new CaseService())
            {
                try
                {
                    Case cAse = caseService.GetCaseById(caseId);
                    if (cAse == null)
                    {
                        return JsonError(new Exception("Case not found"));
                    }

                    cAse.Narrative = model.ShortSummary;
                    cAse.Description = model.Description;

                    caseService.UpdateCase(cAse);
                    return Json(new { Success = true });
                }
                catch (Exception ex)
                {
                    return JsonError(ex);
                }
            }

        }

        [Secure("EditCaseReporterAndPhone"), HttpPost]
        [Route("Cases/{caseId}/UpdateCaseReporterInfo", Name = "UpdateCaseReporterInfo")]
        [ValidateApiAntiForgeryToken]
        public JsonResult UpdateCaseReporterInfo(string caseId, EditCaseReporterInfoModel model)
        {

            using (var caseService = new CaseService())
            {
                try
                {
                    Case cAse = caseService.GetCaseById(caseId);
                    if (cAse == null)
                    {
                        return JsonError(new Exception("Case not found"));
                    }

                    if (model.AuthorizedSubmitter != null && !string.IsNullOrWhiteSpace(model.AuthorizedSubmitter.Id))
                    {
                        using (var employerService = new EmployerService(Current.Customer(), Current.Employer(), Current.User()))
                        {
                            cAse.AuthorizedSubmitter = employerService.GetContactById(model.AuthorizedSubmitter.Id);
                            cAse.CaseReporter = null;
                        }
                    }
                    else
                    {
                        cAse.CaseReporter = CreateContact(model.CaseReporter, cAse.Employee.Id, cAse.EmployerId, cAse.CustomerId);
                        // Record the case reporter
                        CaseReporter reporter = caseService.GetCaseReporters(cAse.Id).OrderByDescending(o => o.CreatedDate).FirstOrDefault();
                        if (reporter == null || cAse.CaseReporter.Id != reporter.Reporter.Id)
                        {
                            new CaseReporter
                            {
                                CaseId = caseId,
                                CustomerId = cAse.CustomerId,
                                EmployerId = cAse.EmployerId,
                                Reporter = cAse.CaseReporter
                            }.Save();
                        }
                        cAse.AuthorizedSubmitter = null;
                    }

                    caseService.UpdateCase(cAse);

                    return Json(new { Success = true });
                }
                catch (Exception ex)
                {
                    return JsonError(ex);
                }
            }

        }

        [Secure("EditCaseEvents"), HttpPost]
        [Route("Cases/{caseId}/UpdateAdditionalCaseEvents", Name = "UpdateAdditionalCaseEvents")]
        [ValidateApiAntiForgeryToken]
        public JsonResult UpdateAdditionalCaseEvents(string caseId, UpdateCaseEventsViewModel updateCaseEvents)
        {
            try
            {
                using (var caseService = new CaseService())
                {
                    Case theCase = caseService.GetCaseById(caseId);
                    foreach (var caseEvent in updateCaseEvents.Events)
                    {
                        if (caseEvent.Delete)
                        {
                            theCase.ClearCaseEvent(caseEvent.EventType);
                        }
                        else
                        {
                            theCase.SetCaseEvent(caseEvent.EventType, caseEvent.EventDate);
                        }
                    }

                    caseService.RunCalcs(theCase);
                    caseService.UpdateCase(theCase);
                    theCase.WfOnCaseEventsUpdated();
                }

                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("ReAssignCase"), HttpPost]
        [Route("Cases/Reassign", Name = "ReassignCases")]
        [ValidateApiAntiForgeryToken]
        public JsonResult ReassignCases(ReassignCasesModel model)
        {
            string userId = model.UserId;
            if (model == null || !model.Cases.Any())
                return Json(model);

            try
            {
                User user;
                using (AuthenticationService svc = new AuthenticationService())
                {
                    user = svc.GetUserById(userId);
                    if (user == null)
                        return JsonError(new Exception("User was not found or no Id was supplied"), "User was not found or no Id was supplied", 404);
                }

                List<string> caseIds = model.Cases
                    .Where(c => c.ApplyStatus && !string.IsNullOrWhiteSpace(c.CaseId))
                    .Select(c => c.CaseId)
                    .ToList();

                List<Case> cases = Case.AsQueryable().Where(c => caseIds.Contains(c.Id)).ToList();
                using (CaseService svc = new CaseService(CurrentCustomer, CurrentEmployer, CurrentUser))
                {
                    cases.ForEach(c => svc.ReassignCase(c, user, model.CaseAssigneeTypeCode));
                }

                return Json(model);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("ReAssignCase"), HttpPost]
        [Route("Cases/ReassignCaseAssignee", Name = "ReassignCaseAssignee")]
        [ValidateApiAntiForgeryToken]
        public JsonResult ReassignCaseAssignee(ReassignCasesModel model)
        {
            string userId = model.UserId;
            if (model == null || !model.Cases.Any())
            {
                return Json(model);
            }

            try
            {
                User user = null;
                using (AuthenticationService svc = new AuthenticationService())
                {
                    if (!string.IsNullOrWhiteSpace(userId))
                    {
                        user = svc.GetUserById(userId);
                        if (user == null)
                        {
                            return JsonError(new Exception("User was not found or no Id was supplied"), "User was not found or no Id was supplied", 404);
                        }
                    }
                }

                foreach (ReassignCase rc in model.Cases)
                {
                    if (rc.ApplyStatus)
                    {
                        if (!rc.SuppressThresholdValidation)
                        {
                            int noOfCasesAssigned = CaseAssignee.AsQueryable().Where(ca => ca.Code == model.CaseAssigneeTypeCode && ca.CaseId != rc.CaseId && ca.UserId == model.UserId && ca.Status != CaseStatus.Closed && ca.Status != CaseStatus.Cancelled).ToList().Count();
                            int? assigneeTypeThresholdCount = CaseAssigneeType.GetByCode(model.CaseAssigneeTypeCode, CurrentCustomer.Id).ThresholdCaseCount;
                            if (assigneeTypeThresholdCount.HasValue && assigneeTypeThresholdCount > noOfCasesAssigned)
                                rc.SuppressThresholdValidation = true;
                            else if (!assigneeTypeThresholdCount.HasValue) rc.SuppressThresholdValidation = true;
                            else
                            {
                                rc.SuppressThresholdValidation = false;

                            }
                        }
                        if (rc.SuppressThresholdValidation)
                        {
                            using (CaseService svc = new CaseService(CurrentCustomer, CurrentEmployer, CurrentUser))
                            {
                                var theCase = svc.GetCaseById(rc.CaseId);
                                svc.ReassignCase(theCase, user, model.CaseAssigneeTypeCode);
                            }
                        }
                    }
                }

                return Json(model);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        #region Calendars



        /// <summary>
        /// 
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="employeeId"></param>
        /// <param name="rangeStart"></param>
        /// <param name="rangeEnd">EXCLUSIVE end of range</param>
        /// <returns></returns>
        [Secure("EditCase", "ViewCase"), HttpPost]
        [Route("Cases/{caseId}/Calendar", Name = "CaseCalendar")]
        [ValidateApiAntiForgeryToken]
        public JsonResult CaseCalendarForCase(CaseCalendarRequestModel request)
        {
            try
            {
                if (!request.Start.HasValue
                    || !request.End.HasValue)
                {
                    throw new ArgumentException("rangeStart and rangeEnd are required");
                }
                // normalize the range to UTC
                request.Start = DateTime.SpecifyKind(request.Start.Value.Date, DateTimeKind.Utc);
                request.End = DateTime.SpecifyKind(request.End.Value.Date, DateTimeKind.Utc);

                var response = new CaseCalendarReponseModel();

                // get the case
                var _case = Case.GetById(request.CaseId);
                if (_case == null)
                {
                    throw new Exception("Case not found for caseId: " + request.CaseId);
                }
                response.HasAnyResults = this.HasAnyCaseCalendarDates(_case);
                if (response.HasAnyResults)
                {
                    // note, this still could come back empty becasue THIS method restricts by date range
                    this.PopulateCaseCalendarModel(_case, response.Dates, request.Start.Value, request.End.Value);
                }

                response.Dates = response.Dates.OrderBy(m => m.Date).ToList();

                return Json(response);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="employeeId"></param>
        /// <param name="rangeStart"></param>
        /// <param name="rangeEnd">EXCLUSIVE end of range</param>
        /// <returns></returns>
        [Secure("EditCase", "ViewEmployeeAbsenceHistory"), HttpPost]
        [Route("Employees/{employeeId}/Calendar", Name = "EmployeeCaseCalendar")]
        [ValidateApiAntiForgeryToken]
        public JsonResult CaseCalendarForEmployee(CaseCalendarRequestModel request)
        {
            try
            {
                if (!request.Start.HasValue
                    || !request.End.HasValue)
                {
                    throw new ArgumentException("rangeStart and rangeEnd are required");
                }
                // normalize the range to UTC
                request.Start = DateTime.SpecifyKind(request.Start.Value.Date, DateTimeKind.Utc);
                request.End = DateTime.SpecifyKind(request.End.Value.Date, DateTimeKind.Utc);

                var response = new CaseCalendarReponseModel();

                // get the case
                var caseList = Case.AsQueryable().Where(o => o.Employee.Id == request.EmployeeId).ToList();
                response.HasAnyResults = this.HasAnyCaseCalendarDates(caseList);
                if (response.HasAnyResults)
                {
                    // note, this still could come back empty becasue THIS method restricts by date range
                    foreach (var _case in caseList)
                    {
                        this.PopulateCaseCalendarModel(_case, response.Dates, request.Start.Value, request.End.Value);
                    }
                }

                response.Dates = response.Dates.OrderBy(m => m.Date).ToList();

                return Json(response);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpPost, Secure("EditCase")]
        [Route("Cases/{caseId}/SaveMissedTime", Name = "SaveMissedTime")]
        public JsonResult SaveMissedTime(string caseId, string time, DateTime? date)
        {
            try
            {
                if (string.IsNullOrEmpty(time))
                {
                    return JsonError(null, "Time was not provided.");
                }

                int totalMinutes = time.ParseFriendlyTime();

                if (string.IsNullOrEmpty(caseId))
                {
                    return JsonError(null, "Case Id was not provided.");
                }

                if (totalMinutes > 1440)
                {
                    return JsonError(null, "Time is invalid. Please enter correct time value.");
                }

                if (date == null)
                    return JsonError(null, "Requested date is in an invalid format.");

                using (var caseService = new CaseService())
                {
                    caseService.SaveMissedTimeOverrides(caseId, totalMinutes, date);
                }
                return Json(new { Success = true });
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error Saving Missed Time.");
            }
        }
        #endregion

        #region Case lists

        [ChildActionOnly]
        public PartialViewResult CaseHistory(String employeeId, string caseId)
        {
            var model = new CaseHistoryModel();
            model.EmployeeId = employeeId;
            if (!String.IsNullOrWhiteSpace(employeeId))
            {
                var query = Case.AsQueryable().Where(o => o.Employee.Id == employeeId && o.Id != caseId).OrderBy(o => o.StartDate);
                foreach (var _case in query)
                {
                    model.Cases.Add(new CaseHistoryItemModel()
                    {
                        CaseId = _case.Id,
                        CaseNumber = _case.CaseNumber,
                        EmployeeName = _case.Employee.FirstName + " " + _case.Employee.LastName,
                        ReasonId = _case.Reason != null ? _case.Reason.Id : string.Empty,
                        Reason = _case.Reason != null ? _case.Reason.Name : string.Empty,
                        StartDate = _case.StartDate.ToUIString(),
                        EndDate = _case.EndDate.HasValue ? _case.EndDate.ToUIString() : string.Empty,
                        Status = _case.GetCaseStatus().ToString().SplitCamelCaseString(),
                        ModifiedDate = _case.ModifiedDate.ToUIString()
                    });
                }
            }
            return PartialView(model);
        }

        [HttpGet, Secure]
        [Route("Case/Employee/{employeeId}/CaseHistory")]
        public JsonResult GetCaseHistory(String employeeId, string caseId)
        {
            var model = new CaseHistoryModel();
            model.EmployeeId = employeeId;
            if (!String.IsNullOrWhiteSpace(employeeId))
            {
                var query = Case.AsQueryable().Where(o => o.Employee.Id == employeeId && o.Id != caseId).OrderBy(o => o.StartDate);
                foreach (var _case in query)
                {
                    model.Cases.Add(new CaseHistoryItemModel()
                    {
                        CaseId = _case.Id,
                        CaseNumber = _case.CaseNumber,
                        CaseType = _case.CaseType,
                        EmployeeName = _case.Employee.FirstName + " " + _case.Employee.LastName,
                        ReasonId = _case.Reason != null ? _case.Reason.Id : string.Empty,
                        Reason = _case.Reason != null ? _case.Reason.Name : string.Empty,
                        StartDate = _case.StartDate.ToUIString(),
                        EndDate = _case.EndDate.HasValue ? _case.EndDate.ToUIString() : string.Empty,
                        Status = _case.GetCaseStatus().ToString().SplitCamelCaseString(),
                        ModifiedDate = _case.ModifiedDate.ToUIString(),
                        Description = _case.Description,
                        Summary = _case.Narrative,
                        CreatedDate = _case.CreatedDate.ToUIString()
                    });
                }
            }
            return Json(model);
        }

        [HttpGet, Secure]
        [Route("Employee/{employeeId}/GetCases")]
        public JsonResult GetEmployeeCases(string employeeId)
        {
            var caseHistory = new CaseHistoryModel()
            {
                EmployeeId = employeeId
            };
            if (!string.IsNullOrWhiteSpace(employeeId))
            {
                var query = Case.AsQueryable().Where(o => o.Employee.Id == employeeId && (o.Status == CaseStatus.Open || o.Status == CaseStatus.Inquiry || o.Status == CaseStatus.Requested || o.Status == CaseStatus.Closed)).OrderBy(o => o.StartDate);
                caseHistory.Cases = query.Select(c => new CaseHistoryItemModel()
                {
                    CaseId = c.Id,
                    CaseNumber = c.CaseNumber,
                    CaseType = c.CaseType,
                    EmployeeName = c.Employee.FirstName + " " + c.Employee.LastName,
                    ReasonId = c.Reason != null ? c.Reason.Id : string.Empty,
                    Reason = c.Reason != null ? c.Reason.Name : string.Empty,
                    StartDate = c.StartDate.ToUIString(),
                    EndDate = c.EndDate.HasValue ? c.EndDate.ToUIString() : string.Empty,
                    Status = c.GetCaseStatus().ToString().SplitCamelCaseString(),
                    ModifiedDate = c.ModifiedDate.ToUIString(),
                    Description = c.Description,
                    Summary = c.Narrative,
                    CreatedDate = c.CreatedDate.ToUIString()
                }).ToList();

            }
            return Json(caseHistory);
        }

        [HttpPost, Secure("ViewCase")]
        [Route("Cases/List", Name = "CasesList")]
        [ValidateApiAntiForgeryToken]
        public JsonResult List(ListCriteria criteria)
        {
            try
            {
                using (var svc = new CaseService())
                {
                    return Json(svc.CaseList(criteria));
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error getting case list");
            }
        }

        [HttpPost, Secure("EditCase", "ViewCase")]
        [Route("Cases/LastViewedList", Name = "CasesLastViewedList")]
        [ValidateApiAntiForgeryToken]
        public JsonResult LastViewedList(ListCriteria criteria)
        {
            try
            {
                using (var svc = new CaseService())
                {
                    return Json(svc.CaseLastViewedList(criteria));
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error getting case last viewed list");
            }
        }
        #endregion

        #region CreateOrModifyTimeOffRequest

        [HttpGet, Secure("EditCase")]
        [Route("Cases/{caseId}/GetTimeOffRequest", Name = "GetTimeOffRequest")]
        public JsonResult GetTimeOffRequest(string caseId, string requestDate)
        {
            try
            {
                DateTime date;
                if (!DateTime.TryParse(requestDate, out date))
                    return JsonError(null, "Requested date was in an invalid format", 401);
                if (string.IsNullOrWhiteSpace(caseId))
                    return JsonError(null, "CaseId was not provided");
                Case c = Case.GetById(caseId);
                if (c == null)
                    return JsonError(null, "Case not found", 404);

                //Need policy summary data
                List<PolicySummary> ps = null;
                using (var service = new CaseService())
                {
                    ps = service.GetEmployeePolicySummaryByCaseId(caseId, null);
                }

                var seg = c.Segments
                    .Where(s => s.Type == CaseType.Intermittent && s.Status != CaseStatus.Cancelled && date.DateInRange(s.StartDate, s.EndDate)).FirstOrDefault();
                if (seg == null)
                    return JsonError(null, "Invalid request, requested date is not part of an intermittent portion of this case", 401);

                List<IntermittentRequestViewModel> modelList = new List<IntermittentRequestViewModel>();


                List<IntermittentTimeRequest> rList = seg.UserRequests.Where(u => u.RequestDate.Date == date.Date).ToList();
                if (rList != null)
                {
                    foreach (var r in rList)
                    {
                        IntermittentRequestViewModel model = new IntermittentRequestViewModel();
                        model.CaseId = caseId;
                        model.RequestDate = date;


                        if (r != null)
                        {
                            model.Id = r.Id;
                            model.Notes = r.Notes;
                            model.RequestDate = r.RequestDate;
                            model.IntermittentType = r.IntermittentType;
                            // model.DenialReasonCode = r.Detail.FirstOrDefault().DenialReasonCode;

                            model.StartTimeOfEvent = new TimeOfDayViewModel(r.StartTimeForLeave ?? new TimeOfDay(0, 0));
                            model.EndTimeOfEvent = new TimeOfDayViewModel(r.EndTimeForLeave ?? new TimeOfDay(0, 0));

                            List<IntermittentRequestDetailViewModel> list = new List<IntermittentRequestDetailViewModel>();
                            foreach (IntermittentTimeRequestDetail d in r.Detail)
                            {
                                IntermittentRequestDetailViewModel vm = new IntermittentRequestDetailViewModel();
                                vm.PolicyCode = d.PolicyCode;
                                vm.PendingTime = d.Pending;
                                vm.ApprovedTime = d.Approved;
                                vm.DeniedTime = d.Denied;
                                vm.DenialReasonCode = d.DenialReasonCode;
                                vm.DenialReasonOther = d.DeniedReasonOther;
                                PolicySummary summary = null;
                                if (ps != null && ps.Count() > 0 && ps.Any(x => x.PolicyCode == d.PolicyCode))
                                {
                                    summary = ps.FirstOrDefault(x => x.PolicyCode == d.PolicyCode);
                                    if (summary != null)
                                    {
                                        vm.PolicyName = summary.PolicyName;
                                        vm.Available = summary.TimeRemaining < 0 ? "reasonable amount" : String.Format("{0:N2} {1}", summary.TimeRemaining, summary.Units.ToString().ToLowerInvariant());
                                    }

                                }
                                list.Add(vm);
                            }
                            model.Details = list;
                            //check to see if the all of the time, denied reason, and denied reason other match
                            //so they can be combined in the UI
                            model.IsTimeEqual = IsTORTimeEqual(model);

                            model.WarningsApproved = false;
                            model.IsEditMode = true;
                            modelList.Add(model);
                        }
                    }
                }




                return Json(modelList);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpPost, Secure("EditCase")]
        [ValidateApiAntiForgeryToken]
        public JsonResult PreValidateTimeOffRequest(IntermittentRequestViewModel viewModel)
        {
            List<ValidationMessage> messages = new List<ValidationMessage>();
            try
            {
                Case c = Case.GetById(viewModel.CaseId);
                if (c == null)
                    messages.Add(new ValidationMessage() { ValidationType = ValidationType.Error, Message = "Case not found" });

                LeaveOfAbsence los = new LeaveOfAbsence();
                los.Employee = c.Employee;
                double fteTimeForWeek = los.TimeLimitForTheDay(viewModel.RequestDate);

                if (viewModel.StartTimeOfEvent.Hour == null && viewModel.EndTimeOfEvent.Hour != null)
                    messages.Add(new ValidationMessage() { ValidationType = ValidationType.Error, Message = "Start Time is required" });

                else if (viewModel.EndTimeOfEvent.Hour == null && viewModel.StartTimeOfEvent.Hour != null)
                    messages.Add(new ValidationMessage() { ValidationType = ValidationType.Error, Message = "End Time is required" });

                int timeRequest = viewModel.ApprovedTime + viewModel.DeniedTime + viewModel.PendingTime;

                if (timeRequest <= 0)
                {
                    if ((viewModel.Details?.Max(d => d.ApprovedTime + d.PendingTime + d.DeniedTime) ?? 0) <= 0)
                    {
                        messages.Add(new ValidationMessage() { ValidationType = ValidationType.Error, Message = "Time Request is required" });
                    }
                }

                if (!messages.Any())
                {
                    var seg = c.Segments
                        .Where(s => s.Type == CaseType.Intermittent && s.Status != CaseStatus.Cancelled && viewModel.RequestDate.DateInRange(s.StartDate, s.EndDate)).FirstOrDefault();
                    if (seg == null)
                        messages.Add(new ValidationMessage() { ValidationType = ValidationType.Error, Message = "Invalid request, requested date is not part of an intermittent portion of this case" });

                    if (!messages.Any())
                    {
                        IntermittentTimeRequest r = seg.UserRequests.FirstOrDefault(u => u.RequestDate.Date == viewModel.RequestDate.Date) ?? new IntermittentTimeRequest()
                        {
                            RequestDate = viewModel.RequestDate,
                        };
                        r.IntermittentType = viewModel.IntermittentType;
                        r.Notes = viewModel.Notes;
                        r.EmployeeEntered = Data.Security.User.Current.Employers.First(e => e.EmployerId == c.EmployerId && e.IsActive).EmployeeId == c.Employee.Id;

                        var IntermittentRequestData = seg.UserRequests.FirstOrDefault(u => u.Id == viewModel.Id);
                        if (IntermittentRequestData != null)
                        {
                            r.Id = IntermittentRequestData.Id;
                            r.IsEditMode = true;
                        }

                        string startLeaveTimeRequest = viewModel.StartTimeOfEvent != null ? viewModel.StartTimeOfEvent.ApplyModel(r.StartTimeForLeave).ToString() : null;
                        string endLeaveTimeRequest = viewModel.EndTimeOfEvent != null ? viewModel.EndTimeOfEvent.ApplyModel(r.EndTimeForLeave).ToString() : null;
                        DateTime startTime = DateTime.Parse($"{viewModel.RequestDate.ToShortDateString()} {startLeaveTimeRequest}");
                        DateTime? endTime = DateTime.Parse($"{viewModel.RequestDate.ToShortDateString()} {endLeaveTimeRequest}");

                        if (startTime != endTime)
                        {
                            if (seg.UserRequests.Any(u => u.RequestDate.Date.CompareTo(viewModel.RequestDate.Date) == 0 && DateTime.Parse($"{u.RequestDate.ToShortDateString()} {u.StartTimeForLeave}").DateInRange(startTime, endTime) ||
                                 DateTime.Parse($"{u.RequestDate.ToShortDateString()} {u.EndTimeForLeave}").DateInRange(startTime, endTime)))
                            {
                                messages.Add(new ValidationMessage() { ValidationType = ValidationType.Warning, Message = "Time of request overlaps existing time off request: Case " + c.CaseNumber + " TOR on " + r.RequestDate.ToString("MM/dd/yyyy") + " from " + startLeaveTimeRequest + " – " + endLeaveTimeRequest });
                            }
                        }

                        if (!messages.Any())
                        {

                            viewModel.Details = viewModel.Details ?? new List<IntermittentRequestDetailViewModel>(0);
                            // Uh oh, there are no details, shame on that UI, we'll just build them out for each of the policies 'n' stuff
                            if (viewModel.Details.Count == 0)
                            {
                                viewModel.Details = seg.AppliedPolicies.Where(p => p.Status == EligibilityStatus.Eligible).Select(p => new IntermittentRequestDetailViewModel()
                                {
                                    PolicyCode = p.Policy.Code,
                                    ApprovedTime = viewModel.ApprovedTime,
                                    PendingTime = viewModel.PendingTime,
                                    DeniedTime = viewModel.DeniedTime,
                                    DenialReasonCode = viewModel.DenialReasonCode,
                                    DenialReasonOther = viewModel.DenialReasonOther
                                }).ToList();
                            }

                            r.TotalMinutes = viewModel.Details.Max(d => d.ApprovedTime + d.PendingTime + d.DeniedTime);

                            //need to handle the details for each policy seperately
                            int policyTotalMinutes = 0;
                            List<IntermittentTimeRequestDetail> list = new List<IntermittentTimeRequestDetail>();
                            foreach (IntermittentRequestDetailViewModel vmDetail in viewModel.Details)
                            {
                                policyTotalMinutes = vmDetail.ApprovedTime + vmDetail.DeniedTime + vmDetail.PendingTime;
                                IntermittentTimeRequestDetail detail = new IntermittentTimeRequestDetail();
                                detail.PolicyCode = vmDetail.PolicyCode;
                                //HACK: TODO: Validate with Seth and Dave this is accurate here... Not sure.
                                // For pending, we know mistakes happen, automatically place any "unaccounted for" time in pending and give the
                                //  user the benefit of the doubt instead of throwing nasty error messages.
                                detail.Pending = (policyTotalMinutes - (vmDetail.ApprovedTime + vmDetail.DeniedTime));
                                detail.Approved = vmDetail.ApprovedTime;
                                detail.Denied = vmDetail.DeniedTime;
                                detail.DenialReasonCode = vmDetail.DenialReasonCode;
                                detail.DeniedReasonOther = vmDetail.DenialReasonOther;
                                list.Add(detail);
                            }
                            r.Detail = list;

                            using (var service = new CaseService())
                                messages = service.PreValidateTimeOffRequest(c.Id, new List<IntermittentTimeRequest>(1) { r });
                        }
                    }
                }
                // If we don't have any messages till this step then it could be a denial request to deny nothing i.e. there is no associated Pending or Approved
                if (!messages.Any())
                {
                    if (viewModel.PendingTime <= 0 && viewModel.ApprovedTime <= 0 && viewModel.DeniedTime > 0)
                    {
                        messages.Add(new ValidationMessage() { ValidationType = ValidationType.Warning, Message = "Do want to open a new ITOR and enter all the time into Denied?" });
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error pre-validating time off request", ex);
                messages.Add(new ValidationMessage() { ValidationType = ValidationType.Error, Message = "Error Pre-Validating Time Off Request" });
            }

            return Json(new
            {
                Messages = messages.ToArray(),
                HasWarnings = messages.Any()
            });
        }

        [HttpPost, Secure("EditCase")]
        [ValidateApiAntiForgeryToken]
        public JsonResult CreateOrModifyTimeOffRequest(IntermittentRequestViewModel viewModel)
        {
            try
            {
                Case c = Case.GetById(viewModel.CaseId);
                if (c == null)
                    return JsonError(new Exception("Case not found"));

                var seg = c.Segments
                    .Where(s => s.Type == CaseType.Intermittent && s.Status != CaseStatus.Cancelled && viewModel.RequestDate.DateInRange(s.StartDate, s.EndDate)).FirstOrDefault();


                if (seg == null)
                    return JsonError(new Exception("Invalid request, requested date is not part of an intermittent portion of this case"));
                bool isEditTOR = true;
                var IntermittentRequestData = seg.UserRequests.FirstOrDefault(u => u.Id == viewModel.Id);
                if (IntermittentRequestData == null)
                {
                    isEditTOR = false;
                    IntermittentRequestData = seg.UserRequests.FirstOrDefault(u => u.RequestDate == viewModel.RequestDate);
                }

                string IType = string.Empty;
                string IRequestDate = string.Empty;
                string INote = string.Empty;
                string Note = viewModel?.Notes;
                string ITotalTime = string.Empty;
                int IApproved = 0;
                int IDenied = 0;
                int IPending = 0;
                string IStartTime = string.Empty;
                string IEndTime = string.Empty;

                if (IntermittentRequestData != null)
                {
                    IType = IntermittentRequestData.IntermittentType.ToString();
                    INote = IntermittentRequestData.Notes ?? INote;
                    IRequestDate = IntermittentRequestData.RequestDate.ToString();
                    ITotalTime = IntermittentRequestData.TotalTime;
                    IApproved = IntermittentRequestData.Detail != null && IntermittentRequestData.Detail.Count() > 0 ? IntermittentRequestData.Detail[0].Approved : 0;
                    IDenied = IntermittentRequestData.Detail != null && IntermittentRequestData.Detail.Count() > 0 ? IntermittentRequestData.Detail[0].Denied : 0;
                    IPending = IntermittentRequestData.Detail != null && IntermittentRequestData.Detail.Count() > 0 ? IntermittentRequestData.Detail[0].Pending : 0;
                    IStartTime = Convert.ToString(IntermittentRequestData.StartTimeForLeave);
                    IEndTime = Convert.ToString(IntermittentRequestData.EndTimeForLeave);
                }
                var iattribute = IntermittentRequestData?.IntermittentType?.GetAttribute<DisplayAttribute>();
                List<IntermittentTimeRequest> intermittentTimeRequests = new List<IntermittentTimeRequest>();

                using (var service = new CaseService())
                {
                    //We need to loop through the list of Applied policy to generate the maximum no of Request date
                    //as different policy can have different intermittent settings. 
                    foreach (var pol in seg.AppliedPolicies)
                    {
                        if (pol.PolicyReason.IntermittentRestrictionsMinimum.HasValue && viewModel.ApprovedTime > 0)
                        {
                            intermittentTimeRequests = service.GenerateIntermittentRequests(c, pol.PolicyReason, viewModel.RequestDate, intermittentTimeRequests, isEditTOR);
                        }
                        else
                        {
                            if (!intermittentTimeRequests.Exists(x => x.RequestDate == viewModel.RequestDate.Date))
                            {
                                IntermittentTimeRequest intermittentTimeRequest = new IntermittentTimeRequest()
                                {
                                    RequestDate = viewModel.RequestDate,
                                    CreatedDate = DateTime.UtcNow
                                };
                                if (isEditTOR)
                                {
                                    intermittentTimeRequest.Id = viewModel.Id;
                                }
                                intermittentTimeRequest.IsEditMode = isEditTOR;
                                intermittentTimeRequests.Add(intermittentTimeRequest);
                            }
                            if (IntermittentRequestData != null && IntermittentRequestData.RequestDate != viewModel.RequestDate)
                            {
                                seg.UserRequests.RemoveAll(a => a.RequestDate == IntermittentRequestData.RequestDate && (!a.IntermittentType.HasValue || a.IntermittentType == IntermittentRequestData.IntermittentType));
                            }
                        }
                    }


                    foreach (var r in intermittentTimeRequests)
                    {

                        if (viewModel.Id == null || viewModel.Id == Guid.Empty)
                        {
                            viewModel.Id = r.Id;
                        }
                        r.IntermittentType = viewModel.IntermittentType;
                        r.Notes = viewModel.Notes;
                        // When request from Employee/HR/Sup login, set request made by employee true 
                        // and set TotalMinutes 
                        if (this.IsEmployeeSelfService && new AuthenticationService().Using(m => m.IsEmployee(CurrentUser, CurrentCustomer)))
                        {
                            r.EmployeeEntered = true;
                            r.TotalMinutes = viewModel.PendingTime;
                        }
                        else
                        {
                            r.EmployeeEntered = false;// CurrentUser.EmployeeId == c.Employee.Id;
                        }

                        viewModel.Details = viewModel.Details ?? new List<IntermittentRequestDetailViewModel>(0);

                        // Uh oh, there are no details, shame on that UI, we'll just build them out for each of the policies 'n' stuff
                        if (viewModel.Details.Count == 0)
                        {
                            viewModel.Details = seg.AppliedPolicies.Where(p => p.Status == EligibilityStatus.Eligible).Select(p => new IntermittentRequestDetailViewModel()
                            {
                                PolicyCode = p.Policy.Code,
                                ApprovedTime = viewModel.ApprovedTime,
                                PendingTime = viewModel.PendingTime,
                                DeniedTime = viewModel.DeniedTime,
                                DenialReasonCode = viewModel.DenialReasonCode,
                                DenialReasonOther = viewModel.DenialReasonOther
                            }).ToList();
                        }

                        r.StartTimeForLeave = viewModel.StartTimeOfEvent != null ? viewModel.StartTimeOfEvent.ApplyModel(r.StartTimeForLeave) : null;
                        r.EndTimeForLeave = viewModel.EndTimeOfEvent != null ? viewModel.EndTimeOfEvent.ApplyModel(r.EndTimeForLeave) : null;

                        string startLeaveTimeRequest = viewModel.StartTimeOfEvent != null ? viewModel.StartTimeOfEvent.ApplyModel(r.StartTimeForLeave).ToString() : null;
                        string endLeaveTimeRequest = viewModel.EndTimeOfEvent != null ? viewModel.EndTimeOfEvent.ApplyModel(r.EndTimeForLeave).ToString() : null;
                        r.StartTimeForLeave = new TimeOfDay(startLeaveTimeRequest);
                        r.EndTimeForLeave = new TimeOfDay(endLeaveTimeRequest);
                        r.TotalMinutes = viewModel.Details.Max(d => d.ApprovedTime + d.PendingTime + d.DeniedTime);
                        //need to handle the details for each policy seperately
                        //We just need to create the new intermittent request only for the policy which has 
                        //intermittent restrictions on.

                        //TOR Should be Applied only in Approved/Denied Policy , there could be scenario where one policy is consecutive  to
                        //another so until first policy exhaust next one will not start, in those scenario first policy will remain Approved .
                        //and the dependent policy in null state but both the policy will remain eligible. So if a user apply for any TOR it should 
                        //deduct only from first policy as it is in Approved State
                        var caseAdjudications = GetCaseAdjudications(c);
                        //Define a local function which will check for policy is approved or in null state
                        CasePolicySummaryModel CheckPolicyDeterminationAppoved(string policyCode)
                        {
                            var policyDetail = caseAdjudications.Where(p => p.PolicyCode == policyCode && p.Detail.Any()).FirstOrDefault();
                            if (policyDetail != null)
                            {
                                return policyDetail;
                            }
                            return null;
                        }

                        List<IntermittentTimeRequestDetail> list = new List<IntermittentTimeRequestDetail>();
                        foreach (IntermittentRequestDetailViewModel vmDetail in viewModel.Details)
                        {

                            //Don't add TOR for Policy which are in null state
                            if (CheckPolicyDeterminationAppoved(vmDetail.PolicyCode) != null)
                            {
                                int approvedTime = vmDetail.ApprovedTime;
                                IntermittentTimeRequestDetail intermittentTimeRequestDetail = null;
                                //One case can have multipile applied Policy and everyone may not have IntermittentRestrictionsMinimum set
                                //but all of them will have same policy reason.

                                var appliedPolicy = seg.AppliedPolicies.FirstOrDefault(pol => pol.Policy.Code == vmDetail.PolicyCode);

                                if (approvedTime > 0 && appliedPolicy != null && appliedPolicy.PolicyReason.IntermittentRestrictionsMinimum.HasValue)
                                {
                                    //if the Policy is configured to use IntermittentRestrictions then do the calculations
                                    approvedTime = service.GetTorTime(c, appliedPolicy.PolicyReason, approvedTime);
                                    intermittentTimeRequestDetail = GetUserRequestDetail(vmDetail, approvedTime, r.IsMaxOccurenceReached);
                                }
                                else if (r.RequestDate == viewModel.RequestDate)
                                {
                                    intermittentTimeRequestDetail = GetUserRequestDetail(vmDetail, approvedTime, r.IsMaxOccurenceReached);
                                }

                                if (intermittentTimeRequestDetail != null)
                                {
                                    //intermittentTimeRequestDetail.DenialReasonCode = !string.IsNullOrEmpty(intermittentTimeRequestDetail.DenialReasonCode) ? intermittentTimeRequestDetail.DenialReasonCode : viewModel.DenialReasonCode;
                                    list.Add(intermittentTimeRequestDetail);
                                    if (approvedTime > vmDetail.ApprovedTime)
                                    {
                                        r.IsIntermittentRestriction = true;
                                    }
                                }
                            }
                        }


                        r.Detail = list;
                        c = service.CreateOrModifyTimeOffRequest(c, intermittentTimeRequests);


                        IQueryable<CaseNote> noteQuery =
                               from n in CaseNote.AsQueryable()
                               where Query.EQ("RequestDate", new BsonDateTime(IntermittentRequestData != null ? IntermittentRequestData.RequestDate : viewModel.RequestDate)).Inject() && n.CaseId == viewModel.CaseId && n.IsItorRemoved == false
                               select n;



                        List<SavedCategory> savedCategories = new List<SavedCategory>();

                        var torCategoy = Enum.GetValues(typeof(NoteCategoryEnum)).Cast<NoteCategoryEnum>().FirstOrDefault(a => a == NoteCategoryEnum.TimeOffRequest);

                        using (NotesService noteService = new NotesService(CurrentUser))
                        {
                            savedCategories = noteService.CreateSavedNoteCategories(torCategoy.ToString().ToUpper(), EntityTarget.Case);
                        }

                        CaseNote note = noteQuery.FirstOrDefault() ?? new CaseNote()
                        {
                            Category = NoteCategoryEnum.TimeOffRequest,
                            Categories = savedCategories,
                            CreatedBy = CurrentUser,
                            CustomerId = c.CustomerId,
                            EmployerId = c.EmployerId,
                            Public = false,
                            Case = c,
                            IsItorRemoved = false
                        };
                        note.ModifiedBy = CurrentUser;
                        var attribute = r.IntermittentType.GetAttribute<DisplayAttribute>();
                        string hApproved = string.Empty;
                        string hDenied = string.Empty;
                        string hPending = string.Empty;
                        string Instatement = string.Empty;

                        List<string> myCollection = new List<string>();

                        hApproved = r.Detail?.FirstOrDefault().Approved > 0 ? "" + ((TimeSpan.FromMinutes((double)r.Detail?.FirstOrDefault().Approved)).ToString(@"hh\:mm")) + " hours Approved " : "";
                        hPending = r.Detail?.FirstOrDefault().Pending > 0 ? "" + ((TimeSpan.FromMinutes((double)r.Detail?.FirstOrDefault().Pending)).ToString(@"hh\:mm")) + " hours Pending " : "";
                        hDenied = r.Detail?.FirstOrDefault().Denied > 0 ? "" + ((TimeSpan.FromMinutes((double)r.Detail?.FirstOrDefault().Denied)).ToString(@"hh\:mm")) + " hours Denied " : "";
                        if (hApproved != "")
                        {
                            myCollection.Add(hApproved);
                        }
                        if (hPending != "")
                        {
                            myCollection.Add(hPending);
                        }
                        if (hDenied != "")
                        {
                            myCollection.Add(hDenied);
                        }

                        Instatement = String.Join(", ", myCollection);

                        string PhApproved = string.Empty;
                        string PhDenied = string.Empty;
                        string PhPending = string.Empty;
                        string PInstatement = string.Empty;
                        bool isTotalChanged = false;
                        List<string> mypCollection = new List<string>();

                        PhApproved = IApproved > 0 ? TimeSpan.FromMinutes((double)IApproved).ToString(@"hh\:mm") + " hours Approved " : "";
                        PhPending = IPending > 0 ? TimeSpan.FromMinutes((double)IPending).ToString(@"hh\:mm") + " hours Pending " : "";
                        PhDenied = IDenied > 0 ? TimeSpan.FromMinutes((double)IDenied).ToString(@"hh\:mm") + " hours Denied " : "";

                        if (PhApproved != "")
                        {
                            mypCollection.Add(PhApproved);
                        }
                        if (PhPending != "")
                        {
                            mypCollection.Add(PhPending);
                        }
                        if (PhDenied != "")
                        {
                            mypCollection.Add(PhDenied);
                        }

                        PInstatement = String.Join(", ", mypCollection);

                        isTotalChanged = ITotalTime != r.TotalTime.ToString() ? true : (ITotalTime == r.TotalTime.ToString() && (IStartTime != r.StartTimeForLeave.ToString() || IEndTime != r.EndTimeForLeave.ToString() || Instatement != PInstatement) ? true : false);

                        string NewRequest = "Intermittent Time Off Request:\n";
                        NewRequest += "New Request \n";
                        NewRequest += "" + attribute.Name + "\n";
                        NewRequest += "" + r.RequestDate.ToShortDateString() + "\n";
                        NewRequest += Instatement != "" ? "" + r.TotalTime + " (" + Instatement + ") \n" : "";
                        NewRequest += "" + r.StartTimeForLeave.ToString() + " - " + r.EndTimeForLeave.ToString() + " \n";
                        NewRequest += !string.IsNullOrWhiteSpace(Note) ? "" + Note + "\n" : "";

                        string ChangeRequest = "Intermittent Time Off Request:\n";
                        ChangeRequest += "Change Request\n";
                        ChangeRequest += !string.IsNullOrEmpty(IType) && IType != r.IntermittentType.ToString() ? "Changed From " + iattribute.Name + " to  " + attribute.Name + "\n" : "" + attribute.Name + "\n";
                        ChangeRequest += !string.IsNullOrEmpty(IRequestDate) && IRequestDate != r.RequestDate.ToString() ? "Change From " + (Convert.ToDateTime(IRequestDate)).ToShortDateString() + " to  " + r.RequestDate.ToShortDateString() + "\n" : "" + r.RequestDate.ToShortDateString() + "\n";
                        ChangeRequest += !string.IsNullOrEmpty(ITotalTime) && isTotalChanged ? "Changed From " + ITotalTime + " (" + PInstatement + ") to " + r.TotalTime + "  (" + Instatement + ")\n" : "" + r.TotalTime + "  (" + Instatement + ") \n";
                        ChangeRequest += (!string.IsNullOrEmpty(IStartTime)) && (!string.IsNullOrEmpty(IEndTime)) && (IStartTime != r.StartTimeForLeave.ToString() || IEndTime != r.EndTimeForLeave.ToString()) ? ("Changed From " + IStartTime + "- " + IEndTime + " to  " + r.StartTimeForLeave.ToString() + " - " + r.EndTimeForLeave.ToString() + " \n") : "" + r.StartTimeForLeave.ToString() + " - " + r.EndTimeForLeave.ToString() + " \n";
                        ChangeRequest += !string.IsNullOrWhiteSpace(Note) && !string.IsNullOrWhiteSpace(INote) && INote != Note ? "Changed From " + INote + " to " + Note + "\n" : !string.IsNullOrWhiteSpace(Note) ? Note : "";


                        if (!noteQuery.Any())
                        {
                            note.Notes = NewRequest;
                        }
                        else if (noteQuery.Any() && !isEditTOR)
                        {
                            note.Notes = note.Notes + "\n";
                            note.Notes += " " + "\n";
                            note.Notes += NewRequest;
                        }
                        else
                        {
                            string[] notearray = !string.IsNullOrEmpty(note.Notes) ? note.Notes.Split(new[] { "Intermittent" }, StringSplitOptions.None) : null;
                            string changenote = string.Empty;
                            foreach (var noteitem in notearray)
                            {
                                var t = "Intermittent" + " " + noteitem;
                                if (!string.IsNullOrEmpty(noteitem) && !noteitem.Contains(IStartTime + " - " + IEndTime))
                                {
                                    changenote = changenote + t;
                                }
                            }

                            if (!string.IsNullOrEmpty(changenote))
                            {
                                note.Notes = ChangeRequest + "\n";
                                note.Notes += " " + "\n";
                                note.Notes += changenote;
                            }
                            else
                            {
                                note.Notes = ChangeRequest;
                            }
                        }
                        note.Metadata.SetRawValue("RequestId", r.Id.ToString());
                        note.Metadata.SetRawValue("RequestDate", r.RequestDate);
                        note.Save();

                        c = service.UpdateCase(c);
                    }
                }

                viewModel.Id = intermittentTimeRequests.FirstOrDefault(i => i.RequestDate == viewModel.RequestDate
                && i.StartTimeForLeave.ToString() == viewModel.StartTimeOfEvent.ApplyModel(null).ToString())?.Id ?? viewModel.Id;
                return Json(viewModel);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error Saving Time Off Request");
            }
        }

        [HttpPost, Secure("EditCase")]
        [ValidateApiAntiForgeryToken]
        public JsonResult DeleteTimeOffRequest(IntermittentRequestViewModel viewModel)
        {
            try
            {
                Case theCase = Case.GetById(viewModel.CaseId);
                if (theCase == null)
                    return JsonError(new Exception("Case not found"));

                var seg = theCase.Segments
                    .Where(s => s.Type == CaseType.Intermittent && s.Status != CaseStatus.Cancelled && viewModel.RequestDate.DateInRange(s.StartDate, s.EndDate)).FirstOrDefault();

                if (seg == null)
                    return JsonError(new Exception("Invalid request, requested date is not part of an intermittent portion of this case"));

                IQueryable<CaseNote> noteQuery =
                       from n in CaseNote.AsQueryable()
                       where Query.EQ("RequestDate", new BsonDateTime(viewModel.RequestDate)).Inject() && n.CaseId == viewModel.CaseId && n.IsItorRemoved == false
                       select n;

                List<SavedCategory> savedCategories = new List<SavedCategory>();

                var torCategoy = Enum.GetValues(typeof(NoteCategoryEnum)).Cast<NoteCategoryEnum>().FirstOrDefault(a => a == NoteCategoryEnum.TimeOffRequest);

                using (NotesService noteService = new NotesService(CurrentUser))
                {
                    savedCategories = noteService.CreateSavedNoteCategories(torCategoy.ToString().ToUpper(), EntityTarget.Case);
                }

                CaseNote note = new CaseNote()
                {
                    Category = NoteCategoryEnum.TimeOffRequest,
                    Categories = savedCategories,
                    CreatedBy = CurrentUser,
                    CustomerId = theCase.CustomerId,
                    EmployerId = theCase.EmployerId,
                    Public = false,
                    Case = theCase,
                    IsItorRemoved = true
                };
                note.ModifiedBy = CurrentUser;
                string chknote = noteQuery?.FirstOrDefault().Notes;
                string[] notearray = !string.IsNullOrEmpty(chknote) ? chknote.Split(new[] { "Intermittent" }, StringSplitOptions.None) : null;
                string removedNote = string.Empty;
                if (notearray != null)
                {
                    foreach (var noteitem in notearray)
                    {
                        var t = "Intermittent" + " " + noteitem;
                        if (!string.IsNullOrEmpty(noteitem))
                        {
                            t = t.Replace("Change", "Removed");
                            t = t.Replace("New", "Removed");
                            removedNote = removedNote + t;
                        }
                    }
                }

                note.Notes = removedNote;
                note.IsItorRemoved = true;
                note.Metadata.SetRawValue("RequestId", viewModel.Id.ToString());
                note.Metadata.SetRawValue("RequestDate", viewModel.RequestDate);
                foreach (var n in noteQuery)
                {
                    n.IsItorRemoved = true;
                    n.Save();
                }
                note.Save();

                using (var service = new CaseService())
                {
                    seg.UserRequests.RemoveAll(a => a.RequestDate.CompareTo(viewModel.RequestDate) == 0);
                    theCase = service.RunCalcs(theCase);
                    theCase = service.UpdateCase(theCase);
                }

                return Json(viewModel);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error Deleting Time Off Request");
            }
        }

        [HttpPost, Secure("EditCase")]
        [ValidateApiAntiForgeryToken]
        public JsonResult RemoveTimeOffRequest(IntermittentRequestViewModel viewModel)
        {
            try
            {
                Case theCase = Case.GetById(viewModel.CaseId);
                if (theCase == null)
                    return JsonError(new Exception("Case not found"));

                var seg = theCase.Segments
                    .Where(s => s.Type == CaseType.Intermittent && s.Status != CaseStatus.Cancelled && viewModel.RequestDate.DateInRange(s.StartDate, s.EndDate)).FirstOrDefault();

                if (seg == null)
                    return JsonError(new Exception("Invalid request, requested date is not part of an intermittent portion of this case"));

                var IntermittentRequestData = seg.UserRequests.FirstOrDefault(u => u.Id == viewModel.Id);

                if (IntermittentRequestData == null)
                {
                    IntermittentRequestData = seg.UserRequests.FirstOrDefault(u => u.StartTimeForLeave.ToString() == viewModel.StartTimeOfEvent.ApplyModel(null).ToString() &&
                    u.EndTimeForLeave.ToString() == viewModel.EndTimeOfEvent.ApplyModel(null).ToString() && u.RequestDate.CompareTo(viewModel.RequestDate) == 0);
                }
                if (IntermittentRequestData == null)
                    return JsonError(new Exception("Intermittent Request not found"));

                seg.UserRequests.RemoveAll(a => a.Id == IntermittentRequestData.Id && (!a.IntermittentType.HasValue || a.IntermittentType == viewModel.IntermittentType));

                string hApproved = string.Empty;
                string hDenied = string.Empty;
                string hPending = string.Empty;
                string Instatement = string.Empty;

                List<string> myCollection = new List<string>();

                hApproved = IntermittentRequestData.Detail?.FirstOrDefault().Approved > 0 ? "" + ((TimeSpan.FromMinutes((double)IntermittentRequestData.Detail?.FirstOrDefault().Approved)).ToString(@"hh\:mm")) + " hours Approved " : "";
                hPending = IntermittentRequestData.Detail?.FirstOrDefault().Pending > 0 ? "" + ((TimeSpan.FromMinutes((double)IntermittentRequestData.Detail?.FirstOrDefault().Pending)).ToString(@"hh\:mm")) + " hours Pending " : "";
                hDenied = IntermittentRequestData.Detail?.FirstOrDefault().Denied > 0 ? "" + ((TimeSpan.FromMinutes((double)IntermittentRequestData.Detail?.FirstOrDefault().Denied)).ToString(@"hh\:mm")) + " hours Denied " : "";

                if (hApproved != "")
                {
                    myCollection.Add(hApproved);
                }
                if (hPending != "")
                {
                    myCollection.Add(hPending);
                }
                if (hDenied != "")
                {
                    myCollection.Add(hDenied);
                }

                Instatement = String.Join(", ", myCollection);

                var attribute = viewModel.IntermittentType.GetAttribute<DisplayAttribute>();

                using (var service = new CaseService())
                {
                    IQueryable<CaseNote> noteQuery =
                           from n in CaseNote.AsQueryable()
                           where Query.EQ("RequestDate", new BsonDateTime(viewModel.RequestDate)).Inject() && n.CaseId == viewModel.CaseId && n.IsItorRemoved == false
                           select n;



                    List<SavedCategory> savedCategories = new List<SavedCategory>();

                    var torCategoy = Enum.GetValues(typeof(NoteCategoryEnum)).Cast<NoteCategoryEnum>().FirstOrDefault(a => a == NoteCategoryEnum.TimeOffRequest);

                    using (NotesService noteService = new NotesService(CurrentUser))
                    {
                        savedCategories = noteService.CreateSavedNoteCategories(torCategoy.ToString().ToUpper(), EntityTarget.Case);
                    }

                    CaseNote note = new CaseNote()
                    {
                        Category = NoteCategoryEnum.TimeOffRequest,
                        Categories = savedCategories,
                        CreatedBy = CurrentUser,
                        CustomerId = theCase.CustomerId,
                        EmployerId = theCase.EmployerId,
                        Public = false,
                        Case = theCase,
                        IsItorRemoved = true
                    };
                    note.ModifiedBy = CurrentUser;

                    string startLeaveTimeRequest = viewModel.StartTimeOfEvent != null ? viewModel.StartTimeOfEvent.ApplyModel(IntermittentRequestData?.StartTimeForLeave).ToString() : null;
                    string endLeaveTimeRequest = viewModel.EndTimeOfEvent != null ? viewModel.EndTimeOfEvent.ApplyModel(IntermittentRequestData?.EndTimeForLeave).ToString() : null;

                    string RemovedRequest = "Intermittent Time Off Request:\n";
                    RemovedRequest += "Removed Request \n";
                    RemovedRequest += "" + attribute.Name + "\n";
                    RemovedRequest += "" + viewModel.RequestDate.ToShortDateString() + "\n";
                    RemovedRequest += "" + IntermittentRequestData?.TotalTime + " (" + Instatement + ") \n";
                    RemovedRequest += "" + startLeaveTimeRequest + " - " + endLeaveTimeRequest + " \n";
                    RemovedRequest += viewModel?.Notes != "" ? "" + viewModel?.Notes + " \n" : "";

                    string chknote = noteQuery?.FirstOrDefault()?.Notes;

                    string[] notearray = !string.IsNullOrEmpty(chknote) ? chknote.Split(new[] { "Intermittent" }, StringSplitOptions.None) : null;

                    string removedNote = string.Empty;

                    if (notearray != null)
                    {
                        foreach (var noteitem in notearray)
                        {
                            var t = "Intermittent" + " " + noteitem;
                            if (!string.IsNullOrEmpty(noteitem) && !noteitem.Contains(startLeaveTimeRequest + " - " + endLeaveTimeRequest))
                            {
                                removedNote = removedNote + t;
                            }
                        }
                        CaseNote previousnote = noteQuery.FirstOrDefault();
                        if (!string.IsNullOrEmpty(removedNote))
                        {
                            previousnote.Notes = removedNote;
                            previousnote.Save();
                        }
                        else
                        {
                            previousnote.IsItorRemoved = true;
                            previousnote.Save();
                        }
                    }

                    note.Notes = RemovedRequest;
                    note.Metadata.SetRawValue("RequestId", viewModel.Id.ToString());
                    note.Metadata.SetRawValue("RequestDate", viewModel.RequestDate);
                    note.Save();
                    theCase = service.RunCalcs(theCase);
                    theCase = service.UpdateCase(theCase);
                }

                return Json(viewModel);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error Deleting Time Off Request");
            }
        }


        private IntermittentTimeRequestDetail GetUserRequestDetail(IntermittentRequestDetailViewModel vmDetail, int approvedTime, bool isMaxOccurenceReached)
        {
            int policyTotalMinutes = approvedTime + vmDetail.DeniedTime + vmDetail.PendingTime;
            IntermittentTimeRequestDetail detail = new IntermittentTimeRequestDetail();
            detail.PolicyCode = vmDetail.PolicyCode;
            //HACK: TODO: Validate with Seth and Dave this is accurate here... Not sure.
            // For pending, we know mistakes happen, automatically place any "unaccounted for" time in pending and give the
            //  user the benefit of the doubt instead of throwing nasty error messages.
            detail.Pending = (policyTotalMinutes - (approvedTime + vmDetail.DeniedTime));
            //if MaxOccurenceReached flag is set for any date , we should mark the Denined reason and also ApprovedTime will be set to denied
            if (isMaxOccurenceReached)
            {
                detail.Denied = approvedTime;
                detail.DenialReasonCode = AdjudicationDenialReason.MaxOccurrenceReached;
            }
            else
            {
                detail.Approved = approvedTime;
                detail.Denied = vmDetail.DeniedTime;
                detail.DenialReasonCode = vmDetail.DenialReasonCode;
                detail.DeniedReasonOther = vmDetail.DenialReasonOther;
            }
            return detail;
        }

        #endregion CreateOrModifyTimeOffRequest

        #region Certifications

        [HttpGet, Secure("EditCase", "ViewCase")]
        [Route("Cases/{caseId}/GetCertifications", Name = "GetCertifications")]
        public JsonResult GetCertifications(string caseId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(caseId))
                    return JsonError(null, "CaseId was not provided");

                Case myCase = Case.GetById(caseId);
                if (myCase == null)
                    return JsonError(null, "Case not found", 404);

                CertificationModel model = new CertificationModel();
                model.CaseId = caseId;

                foreach (Certification cert in myCase.Certifications)
                {
                    string note = CaseNote.AsQueryable().ToList(n => n.CertId == cert.Id).LastOrDefault()?.Notes;

                    CertificationDetailModel cd = new CertificationDetailModel();
                    cd.Id = cert.Id;
                    cd.Duration = cert.Duration;
                    cd.DurationType = cert.DurationType;
                    if (cert.EndDate != default(DateTime))
                    {
                        cd.EndDate = cert.EndDate;
                    }
                    cd.Frequency = cert.Frequency;
                    cd.FrequencyType = cert.FrequencyType;
                    cd.FrequencyUnitType = cert.FrequencyUnitType;
                    cd.Occurances = cert.Occurances;
                    if (cert.StartDate != default(DateTime))
                    {
                        cd.StartDate = cert.StartDate;
                    }
                    cd.Notes = note;
                    cd.IntermittentType = cert.IntermittentType;
                    model.Certifications.Add(cd);
                }

                model.HasAnyResults = model.Certifications.Any();

                return Json(model);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error Loading Certifications");
            }
        }

        [HttpPost, Secure("EditCase")]
        [Route("Cases/{caseId}/CreateOrModifyCertification", Name = "CreateOrModifyCertification")]
        [ValidateApiAntiForgeryToken]
        public JsonResult CreateOrModifyCertification(string caseId, CertificationModel model)
        {
            try
            {
                CaseService caseService = new CaseService();
                Case myCase = caseService.GetCaseById(caseId);

                if (myCase == null)
                    return JsonError(null, "Case not found", 404);

                StringBuilder errors = new StringBuilder();
                foreach (CertificationDetailModel certification in model.Certifications)
                {
                    // Validate the input
                    if (!model.IsCertificateIncomplete)
                    {
                        if (certification.Frequency == null)
                            errors.AppendLine("Frequency is required");
                        if (certification.Occurances == null)
                            errors.AppendLine("Frequency episode occurance is required");
                        if (certification.FrequencyType == null)
                            errors.AppendLine("Frequency episode occurance type is required");
                        if (certification.Duration == null)
                            errors.AppendLine("Duration is required");
                        if (certification.DurationType == null)
                            errors.AppendLine("Duration episode occurance is required");
                        if (errors.Length > 0)
                            continue;
                    }

                    Certification cert = null;
                    bool isNewCert = false;
                    if (certification.Id != null && myCase.Certifications.Any(x => x.Id == certification.Id))
                        cert = myCase.Certifications.First(x => x.Id == certification.Id);
                    else
                    {
                        cert = new Certification();
                        isNewCert = true;
                    }

                    cert.Duration = certification.Duration ?? 0d;
                    cert.DurationType = certification.DurationType ?? Unit.Days;
                    DateTime minDate = DateTime.MinValue;
                    if (certification.EndDate != null && certification.EndDate != minDate)
                    {
                        cert.EndDate = certification.EndDate.Value;
                    }
                    cert.Frequency = certification.Frequency ?? 0;
                    cert.FrequencyType = certification.FrequencyType ?? Unit.Days;
                    cert.FrequencyUnitType = certification.FrequencyUnitType ?? DayUnitType.Workday; //hard coding for now - don't think this can be set thru the UI?
                    cert.Occurances = certification.Occurances ?? 0;
                    if (certification.StartDate != null && certification.StartDate != minDate)
                    {
                        cert.StartDate = certification.StartDate.Value;
                    }
                    cert.IntermittentType = certification.IntermittentType;
                    cert.IsCertificationIncomplete = model.IsCertificateIncomplete;

                    using (var service = new CaseService())
                    {
                        myCase = service.CreateOrModifyCertification(myCase, cert, isNewCert);

                        if (!string.IsNullOrWhiteSpace(certification.Notes))
                        {
                            var noteQuery =
                                from n in CaseNote.AsQueryable()
                                where Query.EQ("CertId", new BsonString(cert.Id.ToString())).Inject()
                                select n;

                            List<SavedCategory> savedCategories = new List<SavedCategory>();
                            var certCategoy = Enum.GetValues(typeof(NoteCategoryEnum)).Cast<NoteCategoryEnum>().FirstOrDefault(a => a == NoteCategoryEnum.Certification);

                            if (certCategoy != null)
                            {
                                using (var noteService = new NotesService(CurrentUser))
                                {
                                    savedCategories = noteService.CreateSavedNoteCategories(certCategoy.ToString().ToUpper(), EntityTarget.Case);
                                }
                            }

                            CaseNote note = noteQuery.FirstOrDefault() ?? new CaseNote()
                            {
                                Category = NoteCategoryEnum.Certification,
                                Categories = savedCategories,
                                CreatedBy = CurrentUser,
                                CustomerId = myCase.CustomerId,
                                EmployerId = myCase.EmployerId,
                                Public = false,
                                Case = myCase
                            };
                            note.ModifiedBy = CurrentUser;
                            note.Notes = certification.Notes;
                            note.CertId = cert.Id;
                            note.Metadata.SetRawValue("CertId", cert.Id.ToString());
                            note.Save();
                        }
                    }
                }
                if (errors.Length > 0)
                    return JsonError(null, errors.ToString(), 401);

                return Json(new { Success = true });
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error Upating Certification");
            }
        }

        [HttpDelete, Secure("EditCase")]
        [Route("Cases/{caseId}/DeleteCertification/{certificationId}", Name = "DeleteCertification")]
        public JsonResult DeleteCertification()
        {
            try
            {
                string certId = RouteData.Values["certificationId"] as string;
                string caseId = RouteData.Values["caseId"] as string;
                if (string.IsNullOrEmpty(certId))
                    return JsonError(null, "Certification Id was not provided");

                if (string.IsNullOrEmpty(caseId))
                    return JsonError(null, "Case Id was not provided");

                Guid certGuid = new Guid(certId);
                Case myCase = Case.GetById(caseId);
                if (myCase == null)
                    return JsonError(null, "Case not found", 404);

                if (myCase.Certifications.Any(x => x.Id == certGuid))
                {
                    myCase.Certifications.Remove(myCase.Certifications.First(x => x.Id == certGuid));
                    myCase.Save();
                }
                else
                    return JsonError(null, string.Format("No certification found matching id: {0}", certId));

                return Json(new { Success = true });
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error Deleting Certification");
            }
        }


        #endregion

        #region helper methods

        private bool IsTORTimeEqual(IntermittentRequestViewModel model)
        {
            bool returnValue = true;
            int approvedTime = 0;
            int deniedTime = 0;
            int pendingTime = 0;
            string denialReasonCode = null;
            string denialReasonOther = string.Empty;

            if (model.Details.Any())
            {
                approvedTime = model.Details.First().ApprovedTime;
                deniedTime = model.Details.First().DeniedTime;
                pendingTime = model.Details.First().PendingTime;
                if (deniedTime > 0)
                {
                    denialReasonCode = model.Details.First().DenialReasonCode;
                    if (model.Details.First().DenialReasonCode == AdjudicationDenialReason.Other)
                        denialReasonOther = model.Details.First().DenialReasonOther;
                }

                foreach (IntermittentRequestDetailViewModel d in model.Details)
                {
                    if (approvedTime != d.ApprovedTime ||
                        deniedTime != d.DeniedTime ||
                        pendingTime != d.PendingTime ||
                        (deniedTime > 0 && denialReasonCode != d.DenialReasonCode) ||
                        (denialReasonCode == AdjudicationDenialReason.Other && denialReasonOther != d.DenialReasonOther)
                       )
                    {
                        returnValue = false;
                    }
                }
            }

            return returnValue;
        }

        [NonAction]
        public String GetLeaveType(Case _case)
        {
            return _case.CaseType;
        }

        private bool HasAnyCaseCalendarDates(List<Case> cases)
        {
            bool result = false;
            foreach (Case _case in cases)
            {
                result = this.HasAnyCaseCalendarDates(_case);
                if (result)
                {
                    break;
                }
            }
            return result;
        }

        private bool HasAnyCaseCalendarDates(Case _case)
        {
            // only non-cancelled cases
            if (_case.Status != CaseStatus.Cancelled)
            {
                // get the segments that have any dates overlapping the range
                // and are not cancelled
                var segments = (from obj in _case.Segments
                                where obj.Status != CaseStatus.Cancelled
                                select obj).ToList();

                foreach (var segment in segments)
                {
                    // get the applied policies between the date range
                    // TODO: include all EligibilityStatues?
                    var policies = (from obj in segment.AppliedPolicies
                                    where obj.Status == EligibilityStatus.Eligible
                                    select obj).ToList();

                    foreach (var policy in policies)
                    {
                        // get all the applied policy usages (no date range check here) 
                        var policyUsages = (from obj in policy.Usage
                                            select obj).ToList();

                        foreach (var policyUsage in policyUsages)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private void PopulateCaseCalendarModel(Case _case, List<CaseCalendarDateModel> modelList, DateTime startDate, DateTime endDate)
        {
            // only non-cancelled cases
            List<int> years = Enumerable.Range(0, (_case.EndDate.Value.Year - _case.StartDate.Year) + 1).Select(i => _case.StartDate.Year + i).ToList();
            years.Add(years.Min() - 1);
            if (_case.Status != CaseStatus.Cancelled)
            {
                HolidayService hs = new HolidayService();
                List<DateTime> holidays = hs.GetHolidayList(years, _case.Employer);
                // get the segments that have any dates overlapping the range
                // and are not cancelled
                var segments = _case.Segments.Where(s => s.Status != CaseStatus.Cancelled && s.StartDate.DateRangesOverLap(s.EndDate, startDate, endDate)).ToList();

                foreach (var segment in segments)
                {
                    // get the applied policies between the date range
                    // TODO: include all EligibilityStatues?
                    var policies = segment.AppliedPolicies.Where(ap => ap.Status == EligibilityStatus.Eligible && ap.StartDate.DateRangesOverLap(ap.EndDate, startDate, endDate)).ToList();

                    foreach (var policy in policies)
                    {
                        // get all the applied policy usages that are between the date range
                        var policyUsages = (from obj in policy.Usage
                                            where obj.DateUsed >= startDate
                                                            && obj.DateUsed <= endDate
                                            select obj).ToList();

                        foreach (var policyUsage in policyUsages)
                        {
                            //we only want to show data on the calendar for intermittent
                            //if there is a TOR for that day
                            if (segment.Type == CaseType.Intermittent &&
                                !segment.UserRequests.Any(x => x.RequestDate == policyUsage.DateUsed))
                            {
                                continue;
                            }
                            if (!holidays.Any(m => m.Date == policyUsage.DateUsed))
                            {
                                // we now have a usage for a single DATE so...
                                // get the top-level calendar model for THIS DATE, or create it

                                var calendarDateModel = modelList.SingleOrDefault(o => o.Date == policyUsage.DateUsed);
                                if (calendarDateModel == null)
                                {
                                    calendarDateModel = new CaseCalendarDateModel() { Date = policyUsage.DateUsed };
                                    modelList.Add(calendarDateModel);
                                }
                                switch (segment.Type)
                                {
                                    case CaseType.Consecutive:
                                        calendarDateModel.HasConsecutiveSegment = true;
                                        break;
                                    case CaseType.Intermittent:
                                        calendarDateModel.HasIntermittentSegment = true;
                                        break;
                                }
                                switch (policyUsage.Determination)
                                {
                                    case AdjudicationStatus.Approved:
                                        calendarDateModel.HasApprovedUsage = true;
                                        break;
                                    case AdjudicationStatus.Denied:
                                        calendarDateModel.HasDeniedUsage = true;
                                        break;
                                    case AdjudicationStatus.Pending:
                                        calendarDateModel.HasPendingUsage = true;
                                        break;
                                }
                                // get the next-level Case-related model, or create it
                                var caseModel = calendarDateModel.Cases.SingleOrDefault(o => o.CaseId == _case.Id);
                                if (caseModel == null)
                                {
                                    caseModel = new CaseCalendarDateModel.Case()
                                    {
                                        CaseId = _case.Id,
                                        CaseNumber = _case.CaseNumber,
                                        ReasonName = _case.Reason.Name,
                                        // NOTE: this is not necessarily accurate since the model is skipping over segments...
                                        CaseType = segment.Type,
                                        MissedTimeOverride = _case.MissedTimeOverride
                                    };
                                    calendarDateModel.Cases.Add(caseModel);
                                }
                                // get the next-level Policy related model, or create it
                                var policyModel = caseModel.Policies.SingleOrDefault(o => o.PolicyCode == policy.Policy.Code &&
                                    o.Determination == AdjudicationStatus.Pending &&
                                    o.Determination == policyUsage.Determination);
                                if (policyModel == null)
                                {

                                    policyModel = new CaseCalendarDateModel.Case.Policy()
                                    {
                                        PolicyCode = policy.Policy.Code,
                                        PolicyName = policy.Policy.Name,
                                        // we can use the determination from the usage since it will
                                        // be the ONLY usage for THIS policy for THIS day (for this case... obviously)
                                        Determination = policyUsage.Determination,
                                        DeterminationText = policyUsage.Determination.ToString().SplitCamelCaseString(),
                                        StartDate = policy.StartDate,
                                        EndDate = policy.EndDate.Value,
                                    };
                                    GetUsageValueAndText(policy, policyUsage, policyModel);
                                    caseModel.Policies.Add(policyModel);


                                }

                            }
                        }
                    }

                    var usersRequests = segment.UserRequests.Where(ap => ap.RequestDate.DateRangesOverLap(ap.RequestDate, startDate, endDate)).ToList();
                    foreach (var usersRequest in usersRequests)
                    {
                        // we now have a usersRequest for a single DATE so...
                        // get the top-level calendar model for THIS DATE, or create it 
                        var calendarDateModel = modelList.SingleOrDefault(o => o.Date == usersRequest.RequestDate);
                        if (calendarDateModel == null)
                        {
                            calendarDateModel = new CaseCalendarDateModel() { Date = usersRequest.RequestDate };
                            modelList.Add(calendarDateModel);
                        }

                        // get the next-level Case-related model
                        var caseModel = calendarDateModel.Cases.SingleOrDefault(o => o.CaseId == _case.Id);

                        // get the next-level userRequests related model, or create it
                        var userRequests = caseModel?.UserRequests.SingleOrDefault(o => o.RequestDate == startDate);
                        if (userRequests == null)
                        {
                            userRequests = new CaseCalendarDateModel.Case.UserRequest()
                            {
                                RequestDate = usersRequest.RequestDate,
                                IntermittentType = usersRequest.IntermittentType,
                                IntermittentTypeText = usersRequest.IntermittentType == IntermittentType.Incapacity ? "Incapacity" : "Office Visit",
                                TotalMinutes = usersRequest.TotalMinutes
                            };
                            caseModel?.UserRequests.Add(userRequests);
                        }
                    }
                }
            }

            // populate holidays
            if (_case.Employer != null && _case.Employer.Holidays != null && _case.Employer.Holidays.Any())
            {
                HolidayService hs = new HolidayService();
                foreach (int year in years)
                {
                    foreach (Holiday holy in _case.Employer.Holidays)
                    {
                        DateTime hd;
                        if (hs.MakeDate(holy, year, out hd))
                        {
                            if (!modelList.Any(m => m.Date.Date == hd.Date))
                            {
                                string name = holy.Name;
                                if (holy.OrClosestWeekday && hd.IsWeekend())
                                {
                                    hd = hd.ClosestWeekday();
                                    name = string.Concat(holy.Name, " (Observed)");
                                }
                                if (!modelList.Any(m => m.Date.Date == hd.Date))
                                {
                                    modelList.Add(new CaseCalendarDateModel()
                                    {
                                        Date = hd,
                                        IsHoliday = true,
                                        HolidayName = name
                                    });
                                }
                            }
                        }
                    }
                }
            }
        }

        private void GetUsageValueAndText(AppliedPolicy policy, AppliedPolicyUsage policyUsage, CaseCalendarDateModel.Case.Policy policyModel)
        {
            if (policy.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays || (policy.PolicyReason.EntitlementType.HasValue && policy.PolicyReason.EntitlementType.Value.CalendarType()))
            {
                policyModel.HoursUsed = 1;
                policyModel.UsedTypeText = "Day";
            }
            else
            {
                policyModel.HoursUsed = Math.Round(policyUsage.MinutesUsed / (double)60, 2);
                policyModel.UsedTypeText = "Hours";
            }

        }
        private List<PolicySummaryModel> ToModel(List<PolicySummary> policySummaries)
        {
            List<PolicySummaryModel> modelList = new List<PolicySummaryModel>();

            foreach (var policySummary in policySummaries)
            {
                var policySummaryModel = new PolicySummaryModel();
                policySummaryModel.PolicyName = policySummary.PolicyName;
                policySummaryModel.PolicyCode = policySummary.PolicyCode;

                var timeRemainingUnitsText = policySummary.Units.ToString().ToLowerInvariant();
                if (policySummary.TimeRemaining == 1)
                {
                    // remove the s since it is singular
                    timeRemainingUnitsText = timeRemainingUnitsText.Remove(timeRemainingUnitsText.Length - 1);
                }
                policySummaryModel.TimeRemainingText = String.Format("{0:N2} {1}", policySummary.TimeRemaining, timeRemainingUnitsText);
                if (policySummary.TimeRemaining < 0 && policySummary.HoursRemaining == "reasonable amount")
                    policySummaryModel.TimeRemainingText = "reasonable amount";
                else if (policySummary.TimeRemaining < 0)
                    policySummaryModel.TimeRemainingText = String.Format("{0:N2} {1}", 0D, timeRemainingUnitsText);

                var timeUsedUnitsText = policySummary.Units.ToString().ToLowerInvariant();
                if (policySummary.TimeUsed == 1)
                {
                    // remove the s since it is singular
                    timeUsedUnitsText = timeUsedUnitsText.Remove(timeUsedUnitsText.Length - 1);
                }
                policySummaryModel.TimeUsedText = String.Format("{0:N2} {1}", policySummary.TimeUsed, timeUsedUnitsText);

                policySummaryModel.HoursUsedText = policySummary.HoursUsed;
                policySummaryModel.HoursRemainingText = policySummary.HoursRemaining;

                if (!string.IsNullOrWhiteSpace(policySummaryModel.HoursRemainingText) && policySummaryModel.HoursRemainingText != "reasonable amount")
                {
                    int min = policySummaryModel.HoursRemainingText.ParseFriendlyTime();
                    if (min < 0)
                        policySummaryModel.HoursRemainingText = "0h";
                }
                policySummaryModel.ExcludeFromTimeConversion = policySummary.ExcludeFromTimeConversion;
                modelList.Add(policySummaryModel);
            }
            return modelList;
        }

        private LeaveOfAbsence ToLeaveOfAbsence(CasePoliciesDataModel model)
        {
            Case cAse = null;
            using (var service = new CaseService())
            {
                if (model is CreateCaseModel)
                {
                    var createCaseModel = (CreateCaseModel)model;

                    // create a case
                    //is we don't have a work schedule then don't pass it in else do
                    Schedule workSchedule = null;
                    if (createCaseModel.WorkSchedule != null)
                    {
                        workSchedule = createCaseModel.WorkSchedule.ApplyToDataModel(workSchedule);
                    }


                    var caseStatus = (CaseStatus)Enum.Parse(typeof(CaseStatus), createCaseModel.Status, true);
                    string absenceReasonCode = string.Empty;
                    if (caseStatus != CaseStatus.Inquiry)
                    {
                        absenceReasonCode = AbsenceReason.GetById(createCaseModel.AbsenceReasonId).Code;
                    }

                    if (caseStatus == CaseStatus.Inquiry && !String.IsNullOrEmpty(createCaseModel.CaseId))
                    {
                        cAse = Case.GetById(createCaseModel.CaseId);
                        cAse.Description = createCaseModel.ShortDescription;
                        cAse.Narrative = createCaseModel.Summary;
                        cAse.StartDate = createCaseModel.StartDate.Value.ToMidnight();
                        cAse.EndDate = createCaseModel.EndDate.HasValue ? createCaseModel.EndDate : null;
                    }
                    else if (!String.IsNullOrEmpty(createCaseModel.CaseId))
                    {
                        cAse = service.CreateCase(caseStatus, createCaseModel.EmployeeId, createCaseModel.StartDate.Value, createCaseModel.EndDate, (CaseType)createCaseModel.CaseTypeId.Value, absenceReasonCode, description: createCaseModel.ShortDescription, narrative: createCaseModel.Summary, caseId: createCaseModel.CaseId);
                    }
                    else if (createCaseModel.WorkSchedule == null)
                        cAse = service.CreateCase(caseStatus, createCaseModel.EmployeeId, createCaseModel.StartDate.Value, createCaseModel.EndDate, (CaseType)createCaseModel.CaseTypeId.Value, absenceReasonCode, description: createCaseModel.ShortDescription, narrative: createCaseModel.Summary);
                    else
                        cAse = service.CreateCase(caseStatus, createCaseModel.EmployeeId, createCaseModel.StartDate.Value, createCaseModel.EndDate, (CaseType)createCaseModel.CaseTypeId.Value, absenceReasonCode, workSchedule, description: createCaseModel.ShortDescription, narrative: createCaseModel.Summary);

                    EmployeeContact contact = null;
                    if (!String.IsNullOrWhiteSpace(createCaseModel.EmployeeRelationshipCode))
                    {
                        cAse.Contact = new EmployeeContact()
                        {
                            Id = createCaseModel.EmployeeContactId,
                            EmployeeId = cAse.Employee.Id,
                            EmployeeNumber = cAse.Employee.EmployeeNumber,
                            EmployerId = cAse.EmployerId,
                            CustomerId = cAse.CustomerId,
                            ContactTypeCode = createCaseModel.EmployeeRelationshipCode,
                            ContactTypeName = ContactType.GetByCode(createCaseModel.EmployeeRelationshipCode).Name,
                            Contact = new Contact()
                            {
                                FirstName = createCaseModel.EmployeeRelationshipFirstName,
                                LastName = createCaseModel.EmployeeRelationshipLastName,
                                DateOfBirth = createCaseModel.EmployeeRelationshipDateOfBirth
                            },
                            MilitaryStatus = (MilitaryStatus)(createCaseModel.MilitaryStatusId ?? 0)
                        };
                        contact = cAse.Contact;
                    }
                    if (createCaseModel.AuthorizedSubmitter != null
                        && !string.IsNullOrWhiteSpace(createCaseModel.AuthorizedSubmitter.Id))
                    {
                        using (var employerService = new EmployerService(Current.Customer(), Current.Employer(), Current.User()))
                        {
                            cAse.AuthorizedSubmitter = employerService.GetContactById(createCaseModel.AuthorizedSubmitter.Id);
                        }
                    }

                    /// Only create the case reporter if we're creating the case (not just translating for eligibility)
                    /// And they didn't select an authorized submitter
                    if (!createCaseModel.IsInquiry && createCaseModel.AuthorizedSubmitter == null)
                    {
                        cAse.CaseReporter = CreateContact(createCaseModel.CaseReporter, createCaseModel.EmployeeId, cAse.EmployerId, cAse.CustomerId);
                    }

                    // Employee contact information
                    // currently get saved in 
                    if (!string.IsNullOrWhiteSpace(createCaseModel.EmployeePrimaryPhone))
                    {
                        cAse.Employee.Info.WorkPhone = createCaseModel.EmployeePrimaryPhone;
                    }

                    if (!string.IsNullOrWhiteSpace(createCaseModel.EmployeeSecondaryPhone))
                    {
                        cAse.Employee.Info.CellPhone = createCaseModel.EmployeeSecondaryPhone;
                        cAse.Employee.Info.AltPhone = createCaseModel.EmployeeSecondaryPhone;
                    }



                    if (!string.IsNullOrWhiteSpace(createCaseModel.EmployeePrimaryEmail))
                    {
                        cAse.Employee.Info.Email = createCaseModel.EmployeePrimaryEmail;
                        Employee e = Employee.GetById(cAse.Employee.Id);
                        e.Info.Email = createCaseModel.EmployeePrimaryEmail;
                        e.Save();
                    }

                    if (!string.IsNullOrWhiteSpace(createCaseModel.EmployeePersonalEmail))
                    {
                        cAse.Employee.Info.AltEmail = createCaseModel.EmployeePersonalEmail;
                        Employee e = Employee.GetById(cAse.Employee.Id);
                        e.Info.AltEmail = createCaseModel.EmployeePersonalEmail;
                        e.Save();
                    }

                    if (!string.IsNullOrWhiteSpace(createCaseModel.EmployeeSecondaryEmail))
                    {
                        cAse.Employee.Info.SecondaryEmail = createCaseModel.EmployeeSecondaryEmail;
                        cAse.Employee.Info.AltEmail = createCaseModel.EmployeeSecondaryEmail;
                        Employee e = Employee.GetById(cAse.Employee.Id);
                        e.Info.SecondaryEmail = createCaseModel.EmployeeSecondaryEmail;
                        e.Save();
                    }

                    if (createCaseModel.NewAddress != null)
                    {
                        cAse.Employee.Info.Address = createCaseModel.NewAddress;
                    }

                    if (createCaseModel.NewAltAddress != null)
                    {
                        cAse.Employee.Info.AltAddress = createCaseModel.NewAltAddress;
                    }

                    if (!string.IsNullOrWhiteSpace(createCaseModel.ContactPreference))
                    {
                        cAse.Metadata.SetRawValue("ContactPreference", createCaseModel.ContactPreference);
                    }

                    if (!string.IsNullOrWhiteSpace(createCaseModel.TimePreference))
                    {
                        cAse.Metadata.SetRawValue("TimePreference", createCaseModel.TimePreference);
                    }

                    if (createCaseModel.SendECommunication.HasValue)
                    {
                        cAse.SendECommunication = createCaseModel.SendECommunication;
                        cAse.ECommunicationRequestDate = DateTime.UtcNow;
                    }

                    // Employee mailing address
                    if (createCaseModel.ShowContactSection && createCaseModel.IsContactInformationCorrect.HasValue && !createCaseModel.IsContactInformationCorrect.Value)
                    {
                        cAse.Employee.Info.Address = createCaseModel.NewAddress;
                    }

                    if (createCaseModel.ShowContactSection && createCaseModel.IsAltAddressCorrect.HasValue && !createCaseModel.IsAltAddressCorrect.Value)
                    {
                        cAse.Employee.Info.AltAddress = createCaseModel.NewAltAddress;
                    }

                    // Save extra field settings
                    if (createCaseModel.ShowOtherQuestions)
                    {
                        if (createCaseModel.IsEmpSafeToBeAtWork.HasValue)
                        {
                            cAse.Metadata.SetRawValue("IsEmpSafeToBeAtWork", createCaseModel.IsEmpSafeToBeAtWork.Value);
                        }

                        if (createCaseModel.IsEmployeeAtWork.HasValue)
                        {
                            cAse.Metadata.SetRawValue("IsEmployeeAtWork", createCaseModel.IsEmployeeAtWork.Value);
                        }

                        if (createCaseModel.IsEmployeeOnLeave.HasValue)
                        {
                            cAse.Metadata.SetRawValue("IsEmployeeOnLeave", createCaseModel.IsEmployeeOnLeave.Value);

                            if (createCaseModel.IsEmployeeOnLeave.Value && !string.IsNullOrWhiteSpace(createCaseModel.EmpOtherLeaveCaseNumber))
                            {
                                cAse.Metadata.SetRawValue("OtherLeaveCaseNumber", createCaseModel.EmpOtherLeaveCaseNumber);
                            }
                        }


                        if (createCaseModel.DidEmployeeSelfIdentify.HasValue)
                        {
                            cAse.Metadata.SetRawValue("DidEmployeeSelfIdentify", createCaseModel.DidEmployeeSelfIdentify.Value);

                            if (createCaseModel.DidEmployeeSelfIdentify.Value && createCaseModel.DateIdentified.HasValue)
                                cAse.Metadata.SetRawValue("DateIdentified", createCaseModel.DateIdentified.Value);
                        }


                        if (createCaseModel.HasPerformanceIssue.HasValue)
                        {
                            cAse.Metadata.SetRawValue("HasPerformanceIssue", createCaseModel.HasPerformanceIssue.Value);

                            if (createCaseModel.HasPerformanceIssue.Value)
                            {
                                if (!string.IsNullOrWhiteSpace(createCaseModel.PerformanceIssueDesc))
                                    cAse.Metadata.SetRawValue("PerformanceIssueDesc", createCaseModel.PerformanceIssueDesc);
                            }
                        }


                        if (createCaseModel.HasWorkConcerns.HasValue)
                        {
                            cAse.Metadata.SetRawValue("HasWorkConcerns", createCaseModel.HasWorkConcerns.Value);

                            if (createCaseModel.HasWorkConcerns.Value)
                            {
                                if (!string.IsNullOrWhiteSpace(createCaseModel.WorkConcernsDesc))
                                    cAse.Metadata.SetRawValue("WorkConcernsDesc", createCaseModel.WorkConcernsDesc);
                            }
                        }
                    }

                    //disability
                    DisabilityInfo di = new DisabilityInfo()
                    {
                        PrimaryDiagnosis = null,
                        SecondaryDiagnosis = null,
                        MedicalComplications = createCaseModel.Disability.MedicalComplications.HasValue ? createCaseModel.Disability.MedicalComplications.Value : false,
                        Hospitalization = createCaseModel.Disability.Hospitalization.HasValue ? createCaseModel.Disability.Hospitalization.Value : false,
                        GeneralHealthCondition = createCaseModel.Disability.GeneralHealthCondition,
                        ConditionStartDate = createCaseModel.Disability.ConditionStartDate,
                        PrimaryPathText = createCaseModel.Disability.PrimaryPathText,
                        PrimaryPathDaysUIText = createCaseModel.Disability.PrimaryPathDaysUIText,
                    };

                    di.PrimaryPathMinDays = null;
                    if (!string.IsNullOrEmpty(createCaseModel.Disability.PrimaryPathMinDays))
                        di.PrimaryPathMinDays = Convert.ToInt32(createCaseModel.Disability.PrimaryPathMinDays);

                    di.PrimaryPathMaxDays = null;
                    if (!string.IsNullOrEmpty(createCaseModel.Disability.PrimaryPathMaxDays))
                        di.PrimaryPathMaxDays = Convert.ToInt32(createCaseModel.Disability.PrimaryPathMaxDays);

                    cAse.Disability = di;

                    //work related
                    if (createCaseModel.IsWorkRelated.HasValue && createCaseModel.IsWorkRelated.Value && createCaseModel.WorkRelatedInfo != null)
                    {
                        cAse.WorkRelated = createCaseModel.WorkRelatedInfo.ApplyModel(cAse);
                        if (cAse.WorkRelated != null && cAse.WorkRelated.Provider != null && !string.IsNullOrWhiteSpace(cAse.WorkRelated.Provider.ContactTypeCode))
                        {
                            cAse.WorkRelated.Provider.CustomerId = cAse.CustomerId;
                            cAse.WorkRelated.Provider.EmployerId = cAse.EmployerId;
                            cAse.WorkRelated.Provider.EmployeeId = cAse.Employee.Id;
                            cAse.WorkRelated.Provider.Save();
                        }
                        else
                            cAse.WorkRelated.Provider = null;
                    }
                    else
                        cAse.WorkRelated = null;

                    //create accomm
                    if (createCaseModel.IsAccommodation.HasValue && createCaseModel.IsAccommodation.Value)
                    {
                        cAse.IsAccommodation = true;
                        cAse.SetCaseEvent(CaseEventType.AccommodationAdded, DateTime.UtcNow);
                        AccommodationRequest request = new AccommodationRequest();
                        request.GeneralHealthCondition = createCaseModel.AccommodationRequest.GeneralHealthCondition;
                        request.Description = createCaseModel.AccommodationRequest.Description;
                        request.Status = CaseStatus.Open;

                        List<Accommodation> accommList = new List<Accommodation>();
                        Accommodation accomm = new Accommodation();
                        if (createCaseModel.AccommodationRequest.Accommodations[0].Type == null)
                            createCaseModel.AccommodationRequest.Accommodations[0].Type =
                                AccommodationType.GetByCode("OTHER", CurrentCustomer.Id, createCaseModel.EmployerId) ??
                                AccommodationType.GetByCode("OTR", CurrentCustomer.Id, createCaseModel.EmployerId) ??
                                AccommodationType.GetByCode("OTHER") ??
                                AccommodationType.GetByCode("OTR");

                        if (createCaseModel.AccommodationRequest.Accommodations[0].Type != null)
                        {
                            accomm.Type = createCaseModel.AccommodationRequest.Accommodations[0].Type;
                            accomm.Duration = createCaseModel.AccommodationRequest.Accommodations[0].Duration;
                            accomm.Description = createCaseModel.AccommodationRequest.Accommodations[0].Type.Code == "OTHER" ? createCaseModel.AccommodationRequest.Accommodations[0].Description : string.Empty;
                            accomm.Usage = MakeUsage(createCaseModel.AccommodationRequest.Accommodations[0], cAse, true);
                            accomm.IsWorkRelated = createCaseModel.AccommodationRequest.Accommodations[0].IsWorkRelated;
                            accomm.StartDate = createCaseModel.AccommodationRequest.StartDate;
                            accomm.EndDate = createCaseModel.AccommodationRequest.EndDate;
                            accommList.Add(accomm);
                        }

                        //HACK: This is duplicated for case wizard route
                        if (createCaseModel.AccommodationRequest.AdditionalQuestions != null)
                        {
                            request.AdditionalInfo = request.AdditionalInfo ?? new AccommodationInteractiveProcess();
                            foreach (var question in createCaseModel.AccommodationRequest.AdditionalQuestions)
                            {
                                foreach (var step in question.InteractiveProcessSteps)
                                {

                                    request.AdditionalInfo.Steps.Add(new AccommodationInteractiveProcess.Step
                                    {
                                        QuestionId = question.Name,
                                        ReportLabel = step.ReportLabel,
                                        SAnswer = step.SAnswer,
                                        Answer = step.Answer,
                                        Question = new AccommodationQuestion
                                        {
                                            Question = step.QuestionText
                                        }
                                    });

                                }
                            }
                        }


                        cAse.AccommodationRequest = request;
                        var accommodationRequest = new AccommodationService().CreateOrModifyAccommodationRequest(cAse, request.GeneralHealthCondition, accommList);
                        if (cAse.Id != null)
                            cAse.Save();

                    }

                    if (createCaseModel.IsWorkRelated.HasValue)
                        cAse.Metadata.SetRawValue("IsWorkRelated", createCaseModel.IsWorkRelated.Value);

                    if (cAse.Reason != null)
                    {
                        switch (cAse.Reason.Code)
                        {
                            case "EHC":
                                break;
                            case "PATERNITY":
                                if (createCaseModel.ExpectedDeliveryDate.HasValue)
                                    cAse.SetCaseEvent(CaseEventType.DeliveryDate, createCaseModel.ExpectedDeliveryDate.Value);
                                break;
                            case "PREGMAT":
                            case "BONDING":
                                if (createCaseModel.ExpectedDeliveryDate.HasValue)
                                    cAse.SetCaseEvent(CaseEventType.DeliveryDate, createCaseModel.ExpectedDeliveryDate.Value);
                                if (createCaseModel.ActualDeliveryDate.HasValue)
                                    cAse.SetCaseEvent(CaseEventType.DeliveryDate, createCaseModel.ActualDeliveryDate.Value);
                                if (createCaseModel.WillUseBonding.HasValue)
                                {
                                    cAse.Metadata.SetRawValue("WillUseBonding", createCaseModel.WillUseBonding.Value);
                                    if (createCaseModel.WillUseBonding.Value)
                                    {
                                        if (createCaseModel.BondingStartDate.HasValue)
                                            cAse.SetCaseEvent(CaseEventType.BondingStartDate, createCaseModel.BondingStartDate.Value);
                                        if (createCaseModel.BondingEndDate.HasValue)
                                            cAse.SetCaseEvent(CaseEventType.BondingEndDate, createCaseModel.BondingEndDate.Value);
                                    }
                                }
                                break;
                            case "ADOPT":
                                if (createCaseModel.AdoptionDate.HasValue)
                                    cAse.SetCaseEvent(CaseEventType.AdoptionDate, createCaseModel.AdoptionDate.Value);
                                break;
                            case "MILITARY":
                            case "EXIGENCY":
                            case "CEREMONY":
                            case "RESERVETRAIN":
                                if (contact != null)
                                {
                                    contact.MilitaryStatus = createCaseModel.MilitaryStatusId.HasValue ? (MilitaryStatus)createCaseModel.MilitaryStatusId.Value : MilitaryStatus.Civilian;
                                }
                                else if (createCaseModel.MilitaryStatusId.HasValue)
                                {
                                    cAse.Employee.MilitaryStatus = (MilitaryStatus)createCaseModel.MilitaryStatusId.Value;
                                }
                                break;
                        }
                    }
                    cAse.IsInquiry = createCaseModel.IsInquiry;
                    if (createCaseModel.CustomFields != null && createCaseModel.CustomFields.Count > 0)
                        cAse.CustomFields = createCaseModel.CustomFields;
                }
                else if (model is EditCaseModel)
                {
                    var editCaseModel = (EditCaseModel)model;
                    cAse = Case.GetById(editCaseModel.Id);
                }
                else
                {
                    throw new NotSupportedException("Only CreateCaseModel and EditCaseModel are supported");
                }

                // prepopulate the policies
                //
                if (cAse.Status != CaseStatus.Inquiry)
                {
                    using (var eligService = new EligibilityService())
                    {
                        eligService.RunEligibility(cAse);

                    }
                }
            }
            // override the policy rules per the model
            if (cAse.Segments.Any())
            {
                foreach (var policyModel in model.AppliedPolicies)
                {
                    var segment = cAse.Segments.First();
                    var policy = segment.AppliedPolicies.FirstOrDefault(o => o.Policy.Code == policyModel.PolicyCode);
                    if (policy != null)
                    {
                        foreach (var ruleGroupModel in policyModel.RuleGroups)
                        {
                            var ruleGroup = policy.RuleGroups.FirstOrDefault(o => o.RuleGroup.Id == ruleGroupModel.Id);
                            if (ruleGroup != null)
                            {
                                foreach (var ruleModel in ruleGroupModel.Rules)
                                {
                                    var rule = ruleGroup.Rules.FirstOrDefault(o => o.Rule.Id == ruleModel.Id);
                                    if (rule != null)
                                    {
                                        if (ruleModel.Overridden)
                                        {
                                            rule.Overridden = true;
                                            rule.OverrideFromResult = ruleModel.Result;
                                            rule.Result = ruleModel.OverrideResult;
                                            rule.OverrideNotes = ruleModel.OverrideNotes;
                                            rule.OverrideValue = ruleModel.OverrideValue;
                                        }
                                        else
                                        {
                                            rule.Overridden = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // add manual policies per the model
            if (model.ManuallyAppliedPolicies.Any())
            {
                foreach (var manuallyAppliedPolicy in model.ManuallyAppliedPolicies)
                {
                    using (var service = new EligibilityService())
                    {
                        cAse = service.AddManualPolicy(cAse, manuallyAppliedPolicy.PolicyCode, manuallyAppliedPolicy.ManuallyAppliedPolicyNotes);
                    }
                }
            }

            //If the type  is Inquiry , and  user clicked the <Convert to Case> Button but not the Create Case. In those cases 
            //we are not going to save it 
            bool IsInquiry = false;
            if (model is CreateCaseModel)
            {
                IsInquiry = ((CreateCaseModel)model).IsInquiry;
            }
            if (cAse.Id != null && !IsInquiry)
            {
                cAse.Save();
            }

            // re-run eligibility on the newly updated policy data
            LeaveOfAbsence leaveOfAbsence = null;
            using (var service = new EligibilityService())
            {
                leaveOfAbsence = service.RunEligibility(cAse);
            }
            //If the type  is Inquiry , and  user clicked the <Convert to Case> Button but not the Create Case. In those cases 
            //we are not going to save it 
            if (cAse.Id != null && !IsInquiry)
            {
                cAse.Save();
            }

            return leaveOfAbsence;
        }

        private Case ToEmployeeLeaveRequest(BaseCaseEss model, bool IsReviewed = false)
        {
            var emp = Employee.GetById(model.EmployeeId);
            var reason = AbsenceReason.GetById(model.AbsenceReasonId);
            if (reason == null)
                throw new AbsenceSoftException("Invalid reason for leave");
            if (!string.IsNullOrWhiteSpace(reason.EmployerId) && reason.EmployerId != emp.EmployerId)
                throw new AbsenceSoftException("Leave reason is not valid for this employer");

            if (model.IsContactInformationCorrect.HasValue && !model.IsContactInformationCorrect.Value)
            {
                emp.Info.Email = model.NewEmail;
                emp.Info.AltEmail = model.NewAltEmail;
                emp.Info.WorkPhone = model.NewWorkPhone;
                emp.Info.CellPhone = model.NewCellPhone;
                emp.Info.HomePhone = model.NewHomePhone;

                emp.Info.Address.Address1 = model.NewAddress;
                emp.Info.Address.Address2 = null;
                emp.Info.Address.City = model.NewCity;
                emp.Info.Address.State = model.NewState;
                emp.Info.Address.PostalCode = model.NewZipcode;

                emp.Info.AltAddress.Address1 = model.NewAltAddress;
                emp.Info.AltAddress.Address2 = null;
                emp.Info.AltAddress.City = model.NewAltCity;
                emp.Info.AltAddress.State = model.NewAltState;
                emp.Info.AltAddress.PostalCode = model.NewAltZipcode;
            }


            Case @case = null;
            if (model is CaseEssReviewedModel)
            {
                var review = model as CaseEssReviewedModel;
                @case = Case.GetById(review.CaseId);
            }
            @case = @case ?? new Case();

            @case.EmployerId = model.EmployeeId;
            @case.Employee = emp;
            @case.EmployerId = emp.EmployerId;
            @case.CustomerId = emp.CustomerId;
            @case.StartDate = model.StartDate.Value.ToMidnight();
            @case.EndDate = model.EndDate.ToMidnight();
            @case.Reason = reason;
            @case.Metadata.SetRawValue("ContactPreference", model.ContactPreference);
            @case.Metadata.SetRawValue("TimePreference", model.TimePreference);

            @case.Segments = new List<CaseSegment>() {
                new CaseSegment()
                {
                    StartDate = @case.StartDate,
                    EndDate = @case.EndDate,
                    Type = (CaseType)model.CaseTypeId,
                    Status = (model.Status ==  CaseStatus.Inquiry ? model.Status : @case.Status),
                }
            };
            //Get Absence manager 
            //AbsenceSoft.Data.Security.User caseManager = new AuthenticationService().Using(k => k.GetCaseManager(CurrentCustomer));
            //cAse.AssignedTo = caseManager;

            //ToDo: Case Manager Logic should come here for AssignedTo User
            //cAse.AssignedTo = AbsenceSoft.Data.Security.User.AsQueryable().FirstOrDefault(m => m.Id == "000000000000000000000000");

            // if it's accommodation case          
            if (!IsReviewed)
            {
                if ((model.IsAccommodation ?? false) && model.AccommodationRequest != null)
                {
                    @case.IsAccommodation = true;

                    AccommodationRequest request = new AccommodationRequest();
                    request.GeneralHealthCondition = model.AccommodationRequest.GeneralHealthCondition;
                    request.Description = model.AccommodationRequest.Description ?? model.AccommodationRequest.OtherDescription;
                    request.Status = CaseStatus.Open;

                    request.Accommodations = new List<Accommodation>();

                    if (model.AccommodationRequest != null)
                    {
                        AccommodationViewModel ac = (model.AccommodationRequest.Accommodations ?? new List<AccommodationViewModel>(0)).FirstOrDefault();
                        if (ac == null)
                            ac = new AccommodationViewModel() { Duration = AccommodationDuration.Temporary, IsWorkRelated = model.IsWorkRelated };
                        if (model.AccommodationRequest.Type != null)
                            ac.Type = model.AccommodationRequest.Type;

                        if (ac.Type != null)
                            request.Accommodations.Add(new Accommodation()
                            {
                                Type = ac.Type,
                                Duration = ac.Duration,
                                Description = ac.Type.Code == "OTHER" ? request.Description : string.Empty,
                                Usage = MakeUsage(ac, @case, true),
                                IsWorkRelated = ac.IsWorkRelated,

                            });
                    }
                    AddAdditionalQuestionsToRequest(model, request);
                    @case.AccommodationRequest = request;
                    var accommodationRequest = new AccommodationService().CreateOrModifyAccommodationRequest(@case, request.GeneralHealthCondition, request.Accommodations);
                    if (@case.Id != null)
                        @case.Save();
                }
                else
                {
                    @case.IsAccommodation = false;
                }

            }
            else
            {
                if (model.IsAccommodation.HasValue && model.IsAccommodation.Value)
                {
                    @case.IsAccommodation = true;
                }
                else
                {
                    @case.IsAccommodation = false;
                }
            }


            // Set extra parameters in metadata
            @case.Metadata.SetRawValue("LeaveFor", model.LeaveFor);
            @case.Metadata.SetRawValue("IsContactInformationCorrect", model.IsContactInformationCorrect);
            @case.Metadata.SetRawValue("IsWorkScheduleCorrect", model.IsWorkScheduleCorrect);

            // Create case contact
            EmployeeContact contact = null;
            if (!String.IsNullOrEmpty(model.EmployeeRelationshipCode))
            {
                @case.Contact = new EmployeeContact()
                {
                    Id = model.EmployeeContactId,
                    EmployeeId = @case.Employee.Id,
                    EmployeeNumber = @case.Employee.EmployeeNumber,
                    EmployerId = @case.EmployerId,
                    CustomerId = @case.CustomerId,
                    ContactTypeCode = model.EmployeeRelationshipCode,
                    ContactTypeName = ContactType.GetByCode(model.EmployeeRelationshipCode).Name,
                    Contact = new Contact()
                    {
                        FirstName = model.EmployeeRelationshipFirstName,
                        LastName = model.EmployeeRelationshipLastName,
                    },
                    MilitaryStatus = (MilitaryStatus)(model.MilitaryStatusId ?? 0)
                };
                contact = @case.Contact;
            }

            switch (@case.Reason.Code)
            {
                case "EHC":
                case "FHC":
                    if (model.IsWorkRelated.HasValue)
                    {
                        @case.Metadata.SetRawValue("IsWorkRelated", model.IsWorkRelated.Value);
                    }
                    @case.Metadata.SetRawValue("ShortDescription", model.ShortDescription);
                    break;
                case "PREGMAT":
                    if (model.ExpectedDeliveryDate.HasValue)
                        @case.SetCaseEvent(CaseEventType.DeliveryDate, model.ExpectedDeliveryDate.Value);
                    if (model.ActualDeliveryDate.HasValue)
                        @case.SetCaseEvent(CaseEventType.DeliveryDate, model.ActualDeliveryDate.Value);
                    if (model.WillUseBonding.HasValue)
                    {
                        @case.Metadata.SetRawValue("WillUseBonding", model.WillUseBonding.Value);
                        if (model.WillUseBonding.Value)
                        {
                            if (model.BondingStartDate.HasValue)
                                @case.SetCaseEvent(CaseEventType.BondingStartDate, model.BondingStartDate.Value);
                            if (model.BondingEndDate.HasValue)
                                @case.SetCaseEvent(CaseEventType.BondingEndDate, model.BondingEndDate.Value);
                        }
                    }
                    break;
                case "MILITARY":
                case "EXIGENCY":
                case "CEREMONY":
                case "RESERVETRAIN":
                    if (contact != null)
                    {
                        contact.MilitaryStatus = model.MilitaryStatusId.HasValue ? (MilitaryStatus)model.MilitaryStatusId.Value : MilitaryStatus.Civilian;
                    }
                    else if (model.MilitaryStatusId.HasValue)
                    {
                        @case.Employee.MilitaryStatus = (MilitaryStatus)model.MilitaryStatusId.Value;
                    }
                    break;
                case "ADOPT":
                    if (model.AdoptionDate.HasValue)
                    {
                        @case.SetCaseEvent(CaseEventType.AdoptionDate, model.AdoptionDate.Value);
                    }
                    break;
            }

            if (IsReviewed)
            {
                // prepopulate the policies
                using (var eligService = new EligibilityService())
                {
                    eligService.RunEligibility(@case);
                }

                // override the policy rules per the model
                if (@case.Segments.Any())
                {
                    foreach (var policyModel in model.AppliedPolicies)
                    {
                        var segment = @case.Segments.First();
                        var policy = segment.AppliedPolicies.SingleOrDefault(o => o.Policy.Code == policyModel.PolicyCode);
                        if (policy != null)
                        {
                            policy.StartDate = segment.StartDate;
                            policy.EndDate = segment.EndDate;
                            foreach (var ruleGroupModel in policyModel.RuleGroups)
                            {
                                var ruleGroup = policy.RuleGroups.SingleOrDefault(o => o.RuleGroup.Id == ruleGroupModel.Id);
                                if (ruleGroup != null)
                                {
                                    foreach (var ruleModel in ruleGroupModel.Rules)
                                    {
                                        var rule = ruleGroup.Rules.SingleOrDefault(o => o.Rule.Id == ruleModel.Id);
                                        if (rule != null)
                                        {
                                            if (ruleModel.Overridden)
                                            {
                                                rule.Overridden = true;
                                                rule.OverrideFromResult = ruleModel.Result;
                                                rule.Result = ruleModel.OverrideResult;
                                                rule.OverrideNotes = ruleModel.OverrideNotes;
                                                rule.OverrideValue = ruleModel.OverrideValue;
                                            }
                                            else
                                            {
                                                rule.Overridden = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // add manual policies per the model
                if (model.ManuallyAppliedPolicies.Any())
                {
                    foreach (var manuallyAppliedPolicy in model.ManuallyAppliedPolicies)
                    {
                        using (var service = new EligibilityService())
                        {
                            @case = service.AddManualPolicy(@case, manuallyAppliedPolicy.PolicyCode, manuallyAppliedPolicy.ManuallyAppliedPolicyNotes);
                        }
                    }
                }

                // prepopulate the policies
                using (var eligService = new EligibilityService())
                {
                    eligService.RunEligibility(@case);
                }
            }

            return @case;
        }

        private static void AddAdditionalQuestionsToRequest(BaseCaseEss model, AccommodationRequest request)
        {
            if (model.AccommodationRequest.AdditionalQuestions != null)
            {
                request.AdditionalInfo = request.AdditionalInfo ?? new AccommodationInteractiveProcess();
                foreach (var question in model.AccommodationRequest.AdditionalQuestions)
                {
                    foreach (var step in question.InteractiveProcessSteps)
                    {

                        request.AdditionalInfo.Steps.Add(new AccommodationInteractiveProcess.Step
                        {
                            QuestionId = question.Name,
                            ReportLabel = step.ReportLabel,
                            SAnswer = step.SAnswer,
                            Answer = step.Answer,
                            Question = new AccommodationQuestion
                            {
                                Question = step.QuestionText
                            }
                        });
                    }
                }
            }
        }

        private void GetPropertyInfos(object empOriginal, object empInfoNew, ref EmployeeModel oldValues, ref EmployeeModel newValues, ref bool HasAnyPropertyChanged, string parent = null)
        {
            try
            {
                Type t = empOriginal.GetType();
                PropertyInfo[] props = t.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prp in props)
                {


                    if (prp.PropertyType.Module.ScopeName != "CommonLanguageRuntimeLibrary")
                    {
                        // fix me: you have to pass parent + "." + t.Name instead of t.Name if parent != null
                        if (prp.GetValue(empOriginal) != null && prp.GetValue(empInfoNew) != null)
                            GetPropertyInfos(prp.GetValue(empOriginal), prp.GetValue(empInfoNew), ref oldValues, ref newValues, ref HasAnyPropertyChanged, t.Name);

                    }
                    else
                    {

                        PropertyInfo prop = empInfoNew.GetType().GetProperty(prp.Name);
                        if (prop != null && (prop.GetValue(empInfoNew) != null || prp.GetValue(empOriginal) != null))
                        {
                            //if (!Convert.ChangeType(prop.GetValue(emp), prop.PropertyType).Equals(Convert.ChangeType(prp.GetValue(o), prp.PropertyType)))
                            if (!(prop.GetValue(empInfoNew)).Equals(prp.GetValue(empOriginal)))
                            {


                                PropertyInfo oldValueProp = oldValues.GetType().GetProperty(prp.Name);
                                if (oldValueProp != null)
                                {
                                    // Set to true
                                    HasAnyPropertyChanged = true;
                                    oldValueProp.SetValue(oldValues, prp.GetValue(empOriginal));
                                }

                                // Store new value of the property
                                PropertyInfo newValueProp = newValues.GetType().GetProperty(prop.Name);
                                if (newValueProp != null)
                                {
                                    // Set to true
                                    HasAnyPropertyChanged = true;
                                    newValueProp.SetValue(newValues, prop.GetValue(empInfoNew));
                                }
                            }


                        }

                        //var value = prp.GetValue(o);
                        //var stringValue = (value != null) ? value.ToString() : "";
                        //var info = t.Name + "." + prp.Name + ": " + stringValue;
                        //if (String.IsNullOrWhiteSpace(parent))
                        //    yield return info;
                        //else
                        //    yield return parent + "." + info;
                    }

                }

            }//try
            catch (NullReferenceException ex)
            {
                throw ex;
            }
        }

        private bool AffectedBySpouseCase(AppliedPolicyModel policy, Case spouseCase)
        {
            if (!policy.CombinedForSpouses || spouseCase == null)
                return false;

            return spouseCase.Segments.SelectMany(s => s.AppliedPolicies).Any(p => p.Policy.Code == policy.PolicyCode && p.Status != EligibilityStatus.Ineligible/* && p.Usage.Any(u => u.Determination == AdjudicationStatus.Approved)*/);
        }

        private void PopulateCasePoliciesData(CasePoliciesDataModel policyDataModel, LeaveOfAbsence leaveOfAbsence, CasePoliciesDataModel postedBackModel)
        {
            using (var service = new EligibilityService())
            {
                policyDataModel.AvailableManualPolicies.AddRange(service.GetAvailableManualPolicies(leaveOfAbsence.Case).Select(o => new AppliedPolicyModel()
                {
                    PolicyId = o.Id,
                    PolicyName = o.Name,
                    PolicyCode = o.Code,
                    PolicyType = o.PolicyType.ToString()
                }));
            }
            policyDataModel.AppliedPolicies.AddRange(this.MapToModel(leaveOfAbsence.Case, leaveOfAbsence.Case.Segments.SelectMany(s => s.AppliedPolicies.Where(o => !o.ManuallyAdded && !o.Policy.IsDeleted)).ToList()));
            // fetch the list of Manual policies from Case Segment
            var lstAppplied = (leaveOfAbsence.Case.Segments ?? new List<CaseSegment>(0))
               .SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>(0))
               .Where(o => o.ManuallyAdded && !o.Policy.IsDeleted).ToList();
            policyDataModel.ManuallyAppliedPolicies.AddRange(this.MapToModel(leaveOfAbsence.Case, lstAppplied));

            // Set the See Spouse Case Flag
            if (!string.IsNullOrWhiteSpace(leaveOfAbsence.Case.SpouseCaseId))
            {
                policyDataModel.AppliedPolicies.ForEach(p => { if (p.CombinedForSpouses) p.SeeSpouseCaseFlag = AffectedBySpouseCase(p, leaveOfAbsence.Case.SpouseCase); });
                policyDataModel.ManuallyAppliedPolicies.ForEach(p => { if (p.CombinedForSpouses) p.SeeSpouseCaseFlag = AffectedBySpouseCase(p, leaveOfAbsence.Case.SpouseCase); });
            }

            if (postedBackModel != null)
            {
                // persist the expand/collapse data from the original posted-back model to the new returnModel
                foreach (var policy in postedBackModel.AppliedPolicies)
                {
                    if (policy.UserIsHandlingExpandCollapse)
                    {
                        var returnPolicy = policyDataModel.AppliedPolicies.SingleOrDefault(o => o.PolicyCode == policy.PolicyCode);
                        if (returnPolicy != null)
                        {
                            returnPolicy.UserIsHandlingExpandCollapse = true;
                            returnPolicy.Expanded = policy.Expanded;
                        }
                    }
                }
                foreach (var policy in postedBackModel.ManuallyAppliedPolicies)
                {
                    if (policy.UserIsHandlingExpandCollapse)
                    {
                        var returnPolicy = policyDataModel.ManuallyAppliedPolicies.SingleOrDefault(o => o.PolicyCode == policy.PolicyCode);
                        if (returnPolicy != null)
                        {
                            returnPolicy.UserIsHandlingExpandCollapse = true;
                            returnPolicy.Expanded = policy.Expanded;
                        }
                    }
                }
            }

            var adjudications = GetCaseAdjudications(leaveOfAbsence.Case);
            if (adjudications != null && adjudications.Any())
            {
                Action<AppliedPolicyModel> getDetail = new Action<AppliedPolicyModel>(p =>
                {
                    var sum = adjudications.FirstOrDefault(a => a.PolicyCode == p.PolicyCode);
                    if (sum != null)
                    {
                        if (sum.Detail.Any())
                        {
                            p.StartDate = sum.Detail.Min(d => d.StartDate);
                            p.EndDate = sum.Detail.Max(d => d.EndDate);
                        }
                        p.Adjudications = sum.Detail;
                        p.Messages.AddRangeIfNotExists(sum.Messages);

                        // Allow this if the user has permissions to do it, AND, this policy has dependent policies that are consecutive to it
                        p.AllowChangeEndDate = Data.Security.User.Permissions.ProjectedPermissions.Contains(Permission.AdjudicateCase.Id) &&
                            leaveOfAbsence.Case.Segments.SelectMany(s => s.AppliedPolicies).Where(s => s.Policy.ConsecutiveTo.Contains(p.PolicyCode)).Any();
                    }
                });
                policyDataModel.AppliedPolicies.ForEach(getDetail);
                policyDataModel.ManuallyAppliedPolicies.ForEach(getDetail);
            }
        }

        private dynamic ToCommunicationModalModel(LeaveOfAbsence leaveOfAbsence)
        {
            var leaveOfAbsenceModel = new
            {
                Case = new
                {
                    Id = leaveOfAbsence.Case.Id,
                    CaseNumber = leaveOfAbsence.Case.CaseNumber
                },
                Communications = leaveOfAbsence.Communications.Select(o => new
                {
                    Id = o.Id
                })
            };
            return leaveOfAbsenceModel;
        }

        private List<AppliedPolicyModel> MapToModel(Case myCase, List<AppliedPolicy> appliedPolicies)
        {
            var list = new List<AppliedPolicyModel>();
            var sortedPolicies = appliedPolicies.OrderBy(p => string.Format("{0}_{1:yyyyMMdd}_{2}",
                    (int)p.Policy.PolicyType, p.StartDate, p.Policy.ConsecutiveTo.Count));
            foreach (var appliedPolicy in sortedPolicies.GroupBy(p => p.Policy.Code))
            {
                var lastEffectivePolicy = appliedPolicy.OrderByDescending(p => p.StartDate).FirstOrDefault();
                var appliedPolicyModel = new AppliedPolicyModel()
                {
                    PolicyId = lastEffectivePolicy.Policy.Id,
                    PolicyName = lastEffectivePolicy.Policy.Name,
                    PolicyCode = lastEffectivePolicy.Policy.Code,
                    PolicyType = lastEffectivePolicy.Policy.PolicyType.ToString(),
                    StartDate = appliedPolicy.Min(p => p.StartDate),
                    EndDate = appliedPolicy.Max(p => p.EndDate),
                    EndDateText = appliedPolicy.Max(p => p.EndDate).ToUIString(),
                    StartDateText = appliedPolicy.Min(p => p.StartDate).ToUIString(),
                    Status = lastEffectivePolicy.Status,
                    ExhaustionDate = appliedPolicy.Min(p => p.FirstExhaustionDate),
                    CombinedForSpouses = lastEffectivePolicy.PolicyReason.CombinedForSpouses,
                    Paid = lastEffectivePolicy.PolicyReason.Paid && lastEffectivePolicy.Status == EligibilityStatus.Eligible && lastEffectivePolicy.Usage.Any(u => u.Determination == AdjudicationStatus.Approved)
                };

                // Should only be displaying and allowing the edit of Eligibility rules, NOT selection, pay or time
                foreach (var ruleGroup in lastEffectivePolicy.RuleGroups.Where(r => r.RuleGroup != null && r.RuleGroup.RuleGroupType == PolicyRuleGroupType.Eligibility))
                {
                    var ruleGroupModel = new AppliedRuleGroupModel()
                    {
                        Id = ruleGroup.RuleGroup.Id,
                        Description = ruleGroup.RuleGroup.Description
                    };
                    appliedPolicyModel.RuleGroups.Add(ruleGroupModel);
                    foreach (var rule in ruleGroup.Rules)
                    {
                        var ruleModel = new AppliedRuleModel()
                        {
                            Id = rule.Rule.Id,
                            Description = rule.Rule.Description,
                            ActualValueString = (rule.ActualValue == null ? "-nothing-" : (rule.ActualValueString ?? rule.ActualValue.ToString())) + ((rule.Rule.LeftExpression.StartsWith("NumberOfMonthsFromDeliveryDate()")) ? " months" : ""),
                            Result = rule.Overridden ? rule.OverrideFromResult : rule.Result,
                            Overridden = rule.Overridden,
                            OverrideNotes = rule.OverrideNotes,
                            OverrideValue = rule.OverrideValue,
                            OverrideResult = rule.Result
                        };
                        appliedPolicyModel.HasOverriddenRule = appliedPolicyModel.HasOverriddenRule || rule.Overridden;

                        ruleGroupModel.Rules.Add(ruleModel);
                    }
                }

                // expand if FAILED or OVERRIDDEN
                appliedPolicyModel.Expanded = (lastEffectivePolicy.Status == EligibilityStatus.Ineligible || lastEffectivePolicy.Status == EligibilityStatus.Pending || appliedPolicyModel.HasOverriddenRule);

                // If there are NO eligiblity rules but it's eligible (and not manually added), we need to display "why" so add a single message to that effect
                if (!lastEffectivePolicy.ManuallyAdded && !appliedPolicyModel.RuleGroups.Any() && appliedPolicyModel.Status == EligibilityStatus.Eligible)
                    appliedPolicyModel.AlternateEligibilityMessage = new EligibilityService().Using(s => s.GetWhySelected(myCase, lastEffectivePolicy));

                list.Add(appliedPolicyModel);
            }
            return list;
        }

        protected List<CasePolicySummaryModel> GetCaseAdjudications(Case c)
        {
            List<CasePolicySummaryModel> model = new List<CasePolicySummaryModel>();

            foreach (var sm in c.Summary.Policies)
            {
                model.Add(new CasePolicySummaryModel()
                {
                    Policy = sm.PolicyName,
                    PolicyCode = sm.PolicyCode,
                    Messages = sm.Messages,
                    Detail = sm.Detail.Select(d => new CasePolicySummaryDetailModel()
                    {
                        CaseType = d.CaseType.ToString().SplitCamelCaseString(),
                        DenialExplanation = d.DenialReasonOther,
                        DenialReason = d.DenialReasonName,
                        EndDate = d.EndDate,
                        EndDateText = d.EndDate.ToUIString(),
                        StartDate = d.StartDate,
                        StartDateText = d.StartDate.ToUIString(),
                        Status = d.Determination.ToString().SplitCamelCaseString()
                    }).ToList()
                });
            }

            return model;
        }

        private dynamic ToCaseViewModel(LeaveOfAbsence leaveOfAbsence)
        {
            EmployeeContact employeeContact = leaveOfAbsence.Case.CasePrimaryProvider ?? new CaseService().GetCasePrimaryProviderContact(leaveOfAbsence.Case);
            PriorHours prior = (leaveOfAbsence.Case.Employee.PriorHours ?? new List<PriorHours>(0)).OrderByDescending(d => d.AsOf).FirstOrDefault();
            List<CustomField> fields = null;
            bool hasEmployeeWorkSchedule = false;
            ScheduleModel workSchedule = new ScheduleModel();
            var caseAssignees = CaseAssignee.AsQueryable().Where(ca => ca.CaseId == leaveOfAbsence.Case.Id).Select(ca => new { CaseTypeAssigneeCode = ca.Code, ca.UserId }).ToList();
            List<string> assigneeUserIds = caseAssignees.Select(ca => ca.UserId).ToList();
            List<string> caseAssigneeTypeCodes = caseAssignees.Select(ca => ca.CaseTypeAssigneeCode).ToList();
            List<User> assigneeUsers = Data.Security.User.AsQueryable().Where(user => assigneeUserIds.Contains(user.Id)).ToList();
            List<CaseAssigneeType> caseAssigmentTypes = CaseAssigneeType.AsQueryable().Where(cat => caseAssigneeTypeCodes.Contains(cat.Code)).ToList();
            List<Relapse> Relapses = null;
            using (var service = new CaseService())
            {
                Relapses = service.GetRelapses(leaveOfAbsence.Case.Id).OrderBy(r => r.StartDate).ToList();
            }
            var caseAssigneelist = new List<dynamic>();
            foreach (var ca in caseAssignees)
            {
                dynamic caseAssignee = new System.Dynamic.ExpandoObject();
                caseAssignee.CaseAssigneeTypeCode = ca.CaseTypeAssigneeCode;
                var cat = caseAssigmentTypes.FirstOrDefault(r => ca.CaseTypeAssigneeCode == r.Code && r.CustomerId == leaveOfAbsence.Case.CustomerId);
                if (cat == null)
                    continue;
                caseAssignee.CaseAssigneeTypeName = cat.Name;
                User user = string.IsNullOrWhiteSpace(ca.UserId) ? null : assigneeUsers.FirstOrDefault(u => u.Id == ca.UserId);
                caseAssignee.UserId = user?.Id;
                caseAssignee.UserName = user?.DisplayName;
                caseAssigneelist.Add(caseAssignee);
            }

            if (leaveOfAbsence.WorkSchedule != null && leaveOfAbsence.WorkSchedule.Count > 0)
            {
                var activeSched = leaveOfAbsence.WorkSchedule.FirstOrDefault(s => DateTime.UtcNow.DateInRange(s.StartDate, s.EndDate));
                if (activeSched == null)
                    activeSched = leaveOfAbsence.WorkSchedule.OrderBy(d => d.StartDate).FirstOrDefault();
                if (activeSched != null)
                {
                    if (activeSched.ScheduleType == ScheduleType.FteVariable)
                    {
                        activeSched.Times = leaveOfAbsence.MaterializeSchedule(activeSched.StartDate.AddDays(DayOfWeek.Sunday - activeSched.StartDate.DayOfWeek), activeSched.EndDate ?? activeSched.StartDate.AddDays(DayOfWeek.Sunday - activeSched.StartDate.DayOfWeek + 6), policyBehavior: PolicyBehavior.Ignored);
                    }
                    workSchedule = activeSched.ToModel();
                }
            }

            using (EmployerService svc = new EmployerService(CurrentCustomer, leaveOfAbsence.Case.Employer, CurrentUser))
            {
                fields = svc.GetCustomFields(EntityTarget.Case).ToList();
                foreach (CustomField fld in fields)
                {
                    CustomField field = leaveOfAbsence.Case.CustomFields.Where(x => x.Id.Equals(fld.Id)).FirstOrDefault();
                    if (field != null)
                        fld.SelectedValue = field.SelectedValue;
                }
            }

            using (EmployeeService svc = new EmployeeService())
            {
                hasEmployeeWorkSchedule = (svc.GetEmployee(leaveOfAbsence.Employee.Id).WorkSchedules.Count() > 0);
            }

            // ContactTypeName fix for cases created before ContactTypeCode property existed on AuhorizedSubmitter model.
            var authorizedSubmitter = leaveOfAbsence.Case.AuthorizedSubmitter;
            if (authorizedSubmitter != null
                && !string.IsNullOrWhiteSpace(authorizedSubmitter.ContactTypeCode))
            {
                using (var employerService = new EmployerService(Current.Customer(), Current.Employer(), Current.User()))
                {
                    // Get current Employer Contact from database that represents up to date Authorized Submitter.
                    var employerContact = employerService.GetContactById(authorizedSubmitter.Id);
                    if (employerContact != null
                        && !string.IsNullOrWhiteSpace(authorizedSubmitter.ContactTypeCode))
                    {
                        // If the contact type name property has a value, go ahead and just assign it (may have historical signifigance).
                        if (!string.IsNullOrWhiteSpace(authorizedSubmitter.ContactTypeName))
                        {
                            authorizedSubmitter.ContactTypeName = authorizedSubmitter.ContactTypeName;
                        }
                        else
                        {
                            // Grab employer contact type for customer by customer id and contact type code value.
                            var employerContactType = EmployerContactType.AsQueryable()
                                .FirstOrDefault(p => p.CustomerId == CurrentUser.CustomerId
                                    && p.Code == authorizedSubmitter.ContactTypeCode);

                            // If an instance was found populate the contact type name property.
                            if (employerContactType != null)
                                authorizedSubmitter.ContactTypeName = employerContactType.Name;
                        }
                    }
                }
            }

            var getCaseClosureReason = new Func<Case, string>((field) =>
            {
                if (string.IsNullOrEmpty(field.ClosureReasonDetails))
                {
                    return string.Empty;
                }
                if (field.ClosureReasonDetails == "OTHER")
                {
                    return field.ClosureOtherReasonDetails;
                }
                else
                {
                    using (var caseService = new CaseService(CurrentCustomer, CurrentEmployer, CurrentUser))
                    {
                        var caseCategory = caseService.GetCaseCategoryByCode(field.ClosureReasonDetails);
                        if (caseCategory != null)
                        {
                            return caseCategory.Name;
                        }
                    }
                }
                return string.Empty;
            });

            bool hasProvider = employeeContact != null && employeeContact.Contact != null;
            dynamic leaveOfAbsenceModel = new
            {
                HasEmployeeWorkSchedule = hasEmployeeWorkSchedule,
                Case = new
                {
                    leaveOfAbsence.Case.CaseNumber,
                    StartDate = leaveOfAbsence.Case.StartDate.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'12':'mm':'ss'Z'"),
                    EndDate = leaveOfAbsence.Case.EndDate.ToString("yyyy'-'MM'-'dd'T'12':'mm':'ss'Z'"),
                    leaveOfAbsence.Case.Reason,
                    leaveOfAbsence.Case.CaseType,
                    AuthorizedSubmitter = (ICaseReporterModel)leaveOfAbsence.Case.AuthorizedSubmitter,
                    CaseReporter = (ICaseReporterModel)leaveOfAbsence.Case.AuthorizedSubmitter ?? (ICaseReporterModel)leaveOfAbsence.Case.CaseReporter,
                    HasIntermittent = leaveOfAbsence.Case.Segments.Any(s => s.Type == CaseType.Intermittent),
                    HasCertification = leaveOfAbsence.Case.Certifications.Any(),
                    leaveOfAbsence.Case.Status,
                    StatusString = leaveOfAbsence.Case.Status == CaseStatus.Requested ? CaseStatus.Open.ToString().SplitCamelCaseString() : leaveOfAbsence.Case.Status.ToString().SplitCamelCaseString(),
                    leaveOfAbsence.Case.Description,
                    ShortSummary = leaveOfAbsence.Case.Narrative,
                    leaveOfAbsence.Case.Contact,
                    MetaData = leaveOfAbsence.Case.Metadata,
                    leaveOfAbsence.Case.SendECommunication,
                    ClosedDt = leaveOfAbsence.Case.GetClosedDate(),
                    CancelDt = leaveOfAbsence.Case.GetCancelledDate(),
                    ClosureReasonDetails = getCaseClosureReason(leaveOfAbsence.Case),
                    CaseEvents = leaveOfAbsence.Case.CaseEvents.Select(ce => new CaseEventViewModel(ce)).ToList(),
                    cdt = leaveOfAbsence.Case.GetMostRecentEventDate(CaseEventType.CaseCreated) ?? leaveOfAbsence.Case.CreatedDate,
                    mdt = leaveOfAbsence.Case.ModifiedDate,
                    leaveOfAbsence.Case.IsAccommodation,
                    leaveOfAbsence.Case.AssignedToName,
                    CaseAutoAssignedTo = caseAssigneelist,
                    AccommodationCategory = leaveOfAbsence.AccommodationCategory(),
                    CustomFields = fields,
                    EmployeeJob = new EmployeeJobViewModel(leaveOfAbsence.Case.Employee.GetCurrentJob()),
                    RiskProfile = leaveOfAbsence.Case.RiskProfile == null ? null : new RiskProfileViewModel(leaveOfAbsence.Case.RiskProfile),
                    PhoneNumber = leaveOfAbsence.Case.Employee.Info.AltPhone,
                    Email = leaveOfAbsence.Case.Employee.Info.AltEmail,
                    Address = leaveOfAbsence.Case.Employee.Info.AltAddress == null ? null : leaveOfAbsence.Case.Employee.Info.AltAddress.ToString(),
                    Address1 = leaveOfAbsence.Case.Employee.Info.AltAddress == null ? null : leaveOfAbsence.Case.Employee.Info.AltAddress.Address1,
                    Address2 = leaveOfAbsence.Case.Employee.Info.AltAddress == null ? null : leaveOfAbsence.Case.Employee.Info.AltAddress.Address2,
                    City = leaveOfAbsence.Case.Employee.Info.AltAddress == null ? null : leaveOfAbsence.Case.Employee.Info.AltAddress.City,
                    State = leaveOfAbsence.Case.Employee.Info.AltAddress == null ? null : leaveOfAbsence.Case.Employee.Info.AltAddress.State,
                    Country = leaveOfAbsence.Case.Employee.Info.AltAddress == null ? null : leaveOfAbsence.Case.Employee.Info.AltAddress.Country,
                    PostalCode = leaveOfAbsence.Case.Employee.Info.AltAddress == null ? null : leaveOfAbsence.Case.Employee.Info.AltAddress.PostalCode,
                    Employee = new
                    {
                        leaveOfAbsence.Case.Employee.Id,
                        leaveOfAbsence.Case.Employee.FullName,
                        leaveOfAbsence.Case.Employee.EmployeeNumber,
                        leaveOfAbsence.Case.Employee.EmployerId,
                        leaveOfAbsence.Case.Employee.EmployerName,
                        leaveOfAbsence.Case.Employee.HireDate,
                        PriorHoursWorked = prior == null ? (double?)null : (double?)prior.HoursWorked,
                        PriorHoursWorkedAsOf = prior == null ? (DateTime?)null : (DateTime?)prior.AsOf,
                        leaveOfAbsence.Case.Employee.JobTitle,
                        leaveOfAbsence.Case.Employee.DoB,
                        leaveOfAbsence.Case.Employee.Gender,
                        leaveOfAbsence.Case.Employee.Info,
                        leaveOfAbsence.Case.Employee.WorkState,
                        leaveOfAbsence.Case.Employee.PayType,
                        Salary = leaveOfAbsence.Case.Employee.Salary.ToString(),
                        IsExempt = leaveOfAbsence.Case.Employee.IsExempt,
                        RehireDate = leaveOfAbsence.Case.Employee.RehireDate,
                        Meets50In75MileRule = leaveOfAbsence.Case.Employee.Meets50In75MileRule,
                        IsKeyEmployee = leaveOfAbsence.Case.Employee.IsKeyEmployee,
                        ServiceDate = leaveOfAbsence.Case.Employee.ServiceDate,
                        SpouseEmployeeNumber = leaveOfAbsence.Case.Employee.SpouseEmployeeNumber,
                        MilitaryStatus = leaveOfAbsence.Case.Employee.MilitaryStatus,
                        WorkSchedules = leaveOfAbsence.Case.Employee.WorkSchedules == null ? new List<ScheduleModel>() : leaveOfAbsence.Case.Employee.WorkSchedules.Select(w => w.ToModel()).ToList(),
                        WorkSchedule = workSchedule
                    },
                    Disability = new
                    {
                        PrimaryDiagnosis = (leaveOfAbsence.Case.Disability != null && leaveOfAbsence.Case.Disability.PrimaryDiagnosis != null) ? new DiagnosisCodeModel() { Id = leaveOfAbsence.Case.Disability.PrimaryDiagnosis.Id.ToString(), Description = leaveOfAbsence.Case.Disability.PrimaryDiagnosis.UIDescription } : null,
                        SecondaryDiagnosis = (leaveOfAbsence.Case.Disability != null && leaveOfAbsence.Case.Disability.SecondaryDiagnosis != null) ? MakeSecondaryCodes(leaveOfAbsence.Case.Disability.SecondaryDiagnosis) : null,
                        leaveOfAbsence.Case.Disability.MedicalComplications,
                        leaveOfAbsence.Case.Disability.Hospitalization,
                        leaveOfAbsence.Case.Disability.GeneralHealthCondition,
                        ConditionStartDate = leaveOfAbsence.Case.Disability.ConditionStartDate.ToUIString(),
                        HCPContactId = hasProvider ? employeeContact.Id.ToString() : string.Empty,
                        HCPFirstName = hasProvider ? employeeContact.Contact.FirstName : string.Empty,
                        HCPLastName = hasProvider ? employeeContact.Contact.LastName : string.Empty,
                        HCPPhone = hasProvider ? employeeContact.Contact.WorkPhone : string.Empty,
                        HCPFax = hasProvider ? employeeContact.Contact.Fax : string.Empty,
                        HCPContactPersonName = hasProvider ? employeeContact.Contact.ContactPersonName : string.Empty,
                        leaveOfAbsence.Case.Disability.PrimaryPathId,
                        leaveOfAbsence.Case.Disability.PrimaryPathText,
                        leaveOfAbsence.Case.Disability.PrimaryPathMinDays,
                        leaveOfAbsence.Case.Disability.PrimaryPathMaxDays,
                        leaveOfAbsence.Case.Disability.PrimaryPathDaysUIText,
                    },
                    CaseRelapses = Relapses,
                    Segments = leaveOfAbsence.Case.Segments.Select(s => new
                    {
                        SegmentType = s.Type,
                        SegmentStartDate = s.StartDate,
                        SegmentEndDate = s.EndDate
                    })

                }
            };

            return leaveOfAbsenceModel;
        }

        private List<DiagnosisCodeModel> MakeSecondaryCodes(List<DiagnosisCode> codes)
        {
            List<DiagnosisCodeModel> returnValue = new List<DiagnosisCodeModel>();
            foreach (DiagnosisCode dc in codes)
            {
                returnValue.Add(new DiagnosisCodeModel() { Id = dc.Id, Description = string.Format("{0} - {1}", dc.Code, dc.Name) });
            }
            return returnValue;
        }

        #endregion

        #region Employee Existing Contacts
        [HttpGet, Secure("EditCase", "CreateCase")]
        [Route("Case/Employee/{employeeId}/GetEmployeeContacts", Name = "GetEmployeeContacts")]
        public JsonResult GetEmployeeContacts(string employeeId, string absenceReasonCode)
        {
            try
            {
                List<EmployeeContact> employeePersonalContacts = new List<EmployeeContact>();
                using (var employeeService = new EmployeeService())
                {
                    employeePersonalContacts = employeeService.GetEmployeeContactsByAbsenceReasonCode(employeeId, absenceReasonCode);
                }

                IEnumerable<dynamic> employeeContacts = employeePersonalContacts.Select(o => new
                {
                    Id = o.Id,
                    Text = o.ContactTypeName.SplitCamelCaseString(),
                    FirstName = o.Contact.FirstName,
                    LastName = o.Contact.LastName,
                    RelationShipId = o.ContactTypeCode,
                    DateOfBirth = o.Contact.DateOfBirth.HasValue ? o.Contact.DateOfBirth.Value.ToString() : string.Empty
                });

                return Json(employeeContacts);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpGet, Secure("EditCase", "CreateCase")]
        [Route("Case/Employee/{employeeId}/GetAllEmployeeContacts", Name = "GetAllEmployeeContacts")]
        public JsonResult GetAllEmployeeContacts(string employeeId)
        {
            try
            {
                List<EmployeeContact> employeePersonalContactType = EmployeeContact.AsQueryable().Where(m => m.EmployeeId == employeeId).ToList();
                IEnumerable<dynamic> employeeContacts = employeePersonalContactType.Select(o => new
                {
                    Id = o.Id,
                    Text = o.ContactTypeName.SplitCamelCaseString(),
                    FirstName = o.Contact.FirstName,
                    LastName = o.Contact.LastName,
                    Label = string.Format("{0} {1} ({2})", o.Contact.FirstName, o.Contact.LastName, o.ContactTypeName.SplitCamelCaseString()),
                    ContactTypeCode = o.ContactTypeCode,
                    Email = o.Contact.Email,
                    WorkPhone = o.Contact.WorkPhone,
                    DOB = string.Empty
                });

                return Json(employeeContacts);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpGet, Secure("EditCase", "CreateCase")]
        [Route("Case/Employer/{employerId}/GetAllEmployerContacts", Name = "GetAllEmployerContacts")]
        public JsonResult GetAllEmployerContacts(string employerId)
        {
            try
            {
                List<EmployerContact> employerContacts;
                using (var employerService = new EmployerService())
                {
                    employerContacts = employerService.GetEmployerContacts(employerId);
                }

                var allEmployerContacts = employerContacts.Select(
                    o => new
                    {
                        Id = o.Id,
                        FullName = o.Contact.FullName,
                        FirstName = o.Contact.FirstName,
                        LastName = o.Contact.LastName,
                        Label = string.Format("{0} {1} ({2})", o.Contact.FirstName, o.Contact.LastName, o.ContactTypeName.SplitCamelCaseString()),
                        ContactTypeCode = o.ContactTypeCode,
                        Email = o.Contact.Email,
                        Phone = o.Contact.WorkPhone,
                        DOB = string.Empty
                    });

                return Json(allEmployerContacts);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpGet, Secure("EditCase", "CreateCase")]
        [Route("Case/Employee/{employeeId}/IsContactTypeDuplicate", Name = "IsContactTypeDuplicate")]
        public JsonResult IsContactTypeDuplicate(string employeeId, string contactTypeCode)
        {
            try
            {
                if (contactTypeCode == "SPOUSE" || contactTypeCode == "DOMESTICPARTNER" || contactTypeCode == "CIVILUNIONPARTNER" || contactTypeCode == "SAMESEXSPOUSE" || contactTypeCode == "COMMITTEDRELATIONSHIP")
                {
                    List<EmployeeContact> employeeContacts = EmployeeContact.AsQueryable().Where(m => m.EmployeeId == employeeId && m.ContactTypeCode == contactTypeCode).ToList();

                    if (employeeContacts.Count > 0)
                    {
                        return Json(new { IsDuplicate = true });
                    }
                    else
                    {
                        return Json(new { IsDuplicate = false });
                    }
                }
                else
                {
                    return Json(new { IsDuplicate = false });
                }

            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCase", "CreateCase"), HttpGet]
        [Route("Case/EmployeeContactTypes", Name = "EmployeeContactTypes")]
        public JsonResult EmployeeContactTypes(string absenceReasonCode)
        {
            try
            {
                List<ContactType> contactTypes = new List<ContactType>();
                using (var employeeService = new EmployeeService())
                {
                    contactTypes = employeeService.GetEmployeeContactTypesByAbsenceReasonCode(absenceReasonCode);
                }

                if (absenceReasonCode == "FHC")
                {
                    contactTypes.Remove(contactTypes.SingleOrDefault(c => c.Code == "SPOUSE"));
                    contactTypes.Remove(contactTypes.SingleOrDefault(c => c.Code == "PARENT"));
                    contactTypes.Remove(contactTypes.SingleOrDefault(c => c.Code == "CHILD"));

                    var employeeContactTypes = contactTypes.Select(o => new
                    {
                        Id = o.Code,
                        Text = o.Name,
                        DesignationType = o.ContactCategory.ToString()
                    }).OrderBy(k => k.Text).ToList();

                    employeeContactTypes.Insert(0, new { Id = "SPOUSE", Text = "Spouse", DesignationType = ContactTypeDesignationType.Personal.ToString() });
                    employeeContactTypes.Insert(1, new { Id = "PARENT", Text = "Parent", DesignationType = ContactTypeDesignationType.Personal.ToString() });
                    employeeContactTypes.Insert(2, new { Id = "CHILD", Text = "Child", DesignationType = ContactTypeDesignationType.Personal.ToString() });

                    return Json(employeeContactTypes);
                }
                else
                {
                    var employeeContactTypes = contactTypes.Select(o => new
                    {
                        Id = o.Code,
                        Text = o.Name.SplitCamelCaseString(),
                        DesignationType = o.ContactCategory.ToString()
                    });
                    return Json(employeeContactTypes);
                }

            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [HttpGet, Secure("EditCase")]
        [Route("Case/Employee/{employeeId}/IsEmployeeContactDuplicate", Name = "IsEmployeeContactDuplicate")]
        public JsonResult IsEmployeeContactDuplicate(string employeeId, string contactTypeCode, string firstName, string lastName)
        {
            try
            {
                List<EmployeeContact> employeeContacts = EmployeeContact.AsQueryable().Where(m => m.EmployeeId == employeeId && m.ContactTypeCode == contactTypeCode && m.Contact.FirstName == firstName && m.Contact.LastName == lastName).ToList();

                if (employeeContacts.Count > 0)
                {
                    return Json(new { IsDuplicate = true });
                }
                else
                {
                    return Json(new { IsDuplicate = false });
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        private EmployeeContact CreateContact(CreateContactViewModel contact, string employeeId, string employerId, string customerId)
        {
            if (contact == null)
                return null;

            if (string.IsNullOrEmpty(contact.EmployeeContactId))
            {
                EmployeeContact newContact = new EmployeeContact()
                {
                    EmployeeId = employeeId,
                    CustomerId = customerId,
                    EmployerId = employerId,
                    ContactTypeCode = contact.ContactTypeCode,
                    ContactTypeName = contact.ContactTypeName,
                    Contact = new Contact()
                    {
                        FirstName = contact.FirstName,
                        LastName = contact.LastName,
                        Email = contact.Email,
                        WorkPhone = contact.WorkPhone
                    }

                };

                return newContact.Save();
            }
            else
            {
                return EmployeeContact.GetById(contact.EmployeeContactId);
            }
        }

        #endregion

        #region ICD9

        [HttpGet]
        [Route("Cases/ICD9Search", Name = "ICD9Search")]
        //[Secure("EditCase")]
        public JsonResult ICD9Search(string searchTerm)
        {
            try
            {
                List<ListResult> results = new List<ListResult>();
                ListResult lr = null;

                List<DiagnosisCode> codes = null;
                using (var svc = new DiagnosisService())
                {
                    codes = svc.Search(searchTerm);
                }

                foreach (DiagnosisCode cd in codes)
                {
                    lr = new ListResult().Set("id", cd.Id).Set("text", string.Format("{0} - {1}", cd.Code, cd.Description));
                    results.Add(lr);
                }

                return Json(results);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error getting ICD9 list");
            }
        }

        [Secure("EditCase"), HttpPost]
        [Route("Cases/{Id}/GuidelinesDataSummary", Name = "GuidelinesDataSummary")]
        [ValidateApiAntiForgeryToken]
        public JsonResult DiagnosisSummary(String Id)
        {
            try
            {
                return Json(DiagnosisCode.GetById(Id));
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error getting Diagnosis Summary");
            }
        }

        [Secure("EditCase"), HttpPost]
        [Route("Cases/{diagnosisCode}/GuidelinesData", Name = "GuidelinesData")]
        [ValidateApiAntiForgeryToken]
        public JsonResult GuidelinesData(string employerId, string diagnosisCode)
        {
            if (String.IsNullOrWhiteSpace(employerId))
                return JsonError(new Exception(), "Missing employerId");

            try
            {
                Employer employer = Employer.GetById(employerId);
                DiagnosisGuidlinesModel model = new DiagnosisGuidlinesModel();

                //Check if feature is enabled
                if (employer == null || !employer.HasFeature(Feature.GuidelinesData))
                    return Json(model);

                model.Guidelines = new DiagnosisGuidelinesService().GetDiagnosisGuidelines(diagnosisCode);
                List<string> rtwBestPracticeList = new List<string>();
                if (model.Guidelines != null)
                {
                    foreach (RtwBestPractice rtw in model.Guidelines.RtwBestPractices)
                    {
                        var item = string.Format("{0} {1}", rtw.RtwPath, rtw.Days);
                        rtwBestPracticeList.Add(item);
                    }
                }
                model.RTWBestPracticeList = rtwBestPracticeList;
                return Json(model);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error getting Guidelines Data");
            }
        }



        [Secure("EditSTDDetail"), HttpPost]
        [Route("Cases/{caseId}/UpdateHealthCondition", Name = "UpdateHealthCondition")]
        [ValidateApiAntiForgeryToken]
        public JsonResult UpdateHealthCondition(DisabilityConditionModel model)
        {
            if (model.CaseId == null)
                return JsonError(new Exception(), "Missing CaseId");

            try
            {
                Case myCase = Case.GetById(model.CaseId);
                if (myCase == null)
                    return JsonError(new Exception(), "Case Not Found");

                myCase.Disability.ConditionStartDate = model.ConditionStartDate;
                myCase.Disability.GeneralHealthCondition = model.GeneralHealthCondition;
                myCase.Disability.Hospitalization = model.Hospitalization.HasValue ? model.Hospitalization.Value : false;
                myCase.Disability.MedicalComplications = model.MedicalComplications.HasValue ? model.MedicalComplications.Value : false;
                myCase.Disability.EmployeeJobActivity = model.ActivityLevel ?? myCase.Employee.JobActivity;

                myCase.Save();

                #region provider contact

                if ((string.IsNullOrEmpty(model.HCPContactId) || model.HCPContactId == "new") && !string.IsNullOrEmpty(model.HCPContactType) && (!string.IsNullOrEmpty(model.HCPCompanyName) || !string.IsNullOrEmpty(model.HCPFirstName)))
                {
                    //new provider contact
                    var employeeContact = new EmployeeContact()
                    {
                        EmployerId = myCase.EmployerId,
                        CustomerId = CurrentCustomer.Id,
                        EmployeeId = myCase.Employee.Id,
                        EmployeeNumber = myCase.Employee.EmployeeNumber,
                        ContactTypeCode = model.HCPContactType,
                        ContactTypeName = ContactType.GetByCode(model.HCPContactType).Name,
                        MilitaryStatus = MilitaryStatus.Civilian,
                        Contact = new Contact()
                        {
                            CompanyName = model.HCPCompanyName,
                            FirstName = model.HCPFirstName,
                            LastName = model.HCPLastName,
                            WorkPhone = model.HCPPhone,
                            Fax = model.HCPFax,
                            IsPrimary = model.IsPrimary,
                            ContactPersonName = model.HCPContactPersonName
                        }
                    };

                    employeeContact = employeeContact.Save();

                    // set rest of the employee HCP contact IsPrimary false
                    if (model.IsPrimary)
                    {
                        using (var contactTypeService = new ContactTypeService())
                        {
                            contactTypeService.UpdateMedicalDesignationTypeContactsIsPrimary(CurrentUser.CustomerId, employeeContact.Contact.Id, myCase.Employee.Id);
                        }
                        myCase.CasePrimaryProvider = employeeContact;
                        new CaseService().UpdateCase(myCase);
                    }

                }
                else if (!string.IsNullOrEmpty(model.HCPContactId) && model.HCPContactId != "new" && !string.IsNullOrEmpty(model.HCPContactType) && (!string.IsNullOrEmpty(model.HCPCompanyName) || !string.IsNullOrEmpty(model.HCPFirstName)))
                {
                    //update the existing contact
                    EmployeeContact providerContact = new CaseService().GetCaseProviderContact(model.HCPContactId);
                    if (providerContact != null)
                    {
                        providerContact.ContactTypeCode = model.HCPContactType;
                        providerContact.ContactTypeName = ContactType.GetByCode(model.HCPContactType).Name;
                        providerContact.Contact = new Contact()
                        {
                            CompanyName = model.HCPCompanyName,
                            FirstName = model.HCPFirstName,
                            LastName = model.HCPLastName,
                            WorkPhone = model.HCPPhone,
                            Fax = model.HCPFax,
                            IsPrimary = model.IsPrimary,
                            ContactPersonName = model.HCPContactPersonName
                        };
                        providerContact = providerContact.Save();

                        // set rest of the employee HCP contact IsPrimary false
                        if (model.IsPrimary)
                        {
                            using (var contactTypeService = new ContactTypeService())
                            {
                                contactTypeService.UpdateMedicalDesignationTypeContactsIsPrimary(CurrentUser.CustomerId, providerContact.Contact.Id, myCase.Employee.Id);
                            }

                            myCase.CasePrimaryProvider = providerContact;
                            new CaseService().UpdateCase(myCase);
                        }
                    }
                }
                else
                {
                    // no primary selected so set all related contacts to NOT primary
                    using (var contactTypeService = new ContactTypeService())
                    {
                        contactTypeService.UpdateMedicalDesignationTypeContactsIsPrimary(CurrentUser.CustomerId, null, myCase.Employee.Id);
                    }
                }

                #endregion

                return Json(model);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCase"), HttpPost]
        [Route("Cases/{caseId}/UpdateDiagnosis", Name = "UpdateDiagnosis")]
        [ValidateApiAntiForgeryToken]
        public JsonResult UpdateDiagnosis(DisabilityModel model)
        {
            if (model.CaseId == null)
                return JsonError(new Exception(), "Missing CaseId");

            if (!string.IsNullOrEmpty(model.PrimaryDiagnosisId) &&
                model.SecondaryDiagnosisIds.Any() &&
                model.SecondaryDiagnosisIds.Contains(model.PrimaryDiagnosisId))
            {
                return JsonError(new Exception(), "Secondary Diagnosis Code cannot match Primary Diagnosis Code");
            }

            try
            {
                Case myCase = Case.GetById(model.CaseId);
                if (myCase == null)
                    return JsonError(new Exception(), "Case Not Found");

                if (!string.IsNullOrEmpty(model.PrimaryDiagnosisId))
                {
                    var dc = DiagnosisCode.GetById(model.PrimaryDiagnosisId);
                    myCase.Disability.PrimaryDiagnosis = dc;
                }
                else
                    myCase.Disability.PrimaryDiagnosis = null;

                List<DiagnosisCode> secondaryCodes = new List<DiagnosisCode>();
                if (model.SecondaryDiagnosisIds != null && model.SecondaryDiagnosisIds.Any())
                {
                    foreach (string id in model.SecondaryDiagnosisIds)
                    {
                        var dc = DiagnosisCode.GetById(id);
                        secondaryCodes.Add(dc);
                    }
                    myCase.Disability.SecondaryDiagnosis = secondaryCodes;
                }
                else
                    myCase.Disability.SecondaryDiagnosis = null;

                myCase.Disability.PrimaryPathId = model.PrimaryPathId;
                myCase.Disability.PrimaryPathDaysUIText = model.PrimaryPathDaysUIText;
                myCase.Disability.PrimaryPathText = model.PrimaryPathText;
                if (!string.IsNullOrWhiteSpace(model.PrimaryPathMaxDays))
                    myCase.Disability.PrimaryPathMaxDays = Convert.ToInt32(model.PrimaryPathMaxDays);
                else
                    myCase.Disability.PrimaryPathMaxDays = null;

                if (!string.IsNullOrWhiteSpace(model.PrimaryPathMinDays))
                    myCase.Disability.PrimaryPathMinDays = Convert.ToInt32(model.PrimaryPathMinDays);
                else
                    myCase.Disability.PrimaryPathMinDays = null;

                myCase.Save();

                model.PrimaryDiagnosis = (myCase.Disability != null && myCase.Disability.PrimaryDiagnosis != null) ? new DiagnosisCodeModel() { Id = myCase.Disability.PrimaryDiagnosis.Id.ToString(), Description = myCase.Disability.PrimaryDiagnosis.UIDescription } : null;
                model.SecondaryDiagnosis = (myCase.Disability != null && myCase.Disability.SecondaryDiagnosis != null) ? MakeSecondaryCodes(myCase.Disability.SecondaryDiagnosis) : null;

                return Json(model);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        private List<AccommodationUsage> MakeUsage(AccommodationViewModel model, Case theCase, bool isNew)
        {
            List<AccommodationUsage> usage = new List<AccommodationUsage>();
            if (isNew)
            {
                AccommodationUsage au = new AccommodationUsage();
                if (model.Duration == AccommodationDuration.Permanent || model.Duration == 0)
                {
                    au.StartDate = theCase.StartDate;
                    au.EndDate = null;
                }

                if (model.Duration == AccommodationDuration.Temporary)
                {
                    au.StartDate = model.StartDate ?? theCase.StartDate;
                    au.EndDate = model.EndDate;
                }

                usage.Add(au);
                return usage;
            }
            else
            {
                usage = MakeUsageModel(model);
                using (AccommodationService svc = new AccommodationService())
                    usage = svc.BoxUsage(usage, model.StartDate ?? theCase.StartDate, model.EndDate);
                return usage;
            }
        }// MakeUsage

        private List<AccommodationUsage> MakeUsageModel(AccommodationViewModel accommodation)
        {
            List<AccommodationUsage> auList = new List<AccommodationUsage>();
            foreach (AccommodationUsageViewModel auvm in accommodation.AccommodationUsage)
            {
                AccommodationUsage au = new AccommodationUsage()
                {
                    StartDate = auvm.StartDate,
                    EndDate = auvm.EndDate,
                    Determination = auvm.AdjudicationStatus,
                    DenialReasonCode = auvm.DenialReasonCode,
                    DenialReasonOther = auvm.DenialReasonOther
                };
                auList.Add(au);
            }
            return auList;
        }

        #endregion

        #region "ESS Case Status Update"
        [Secure]
        [Route("Case/Reviewed", Name = "CaseReviewed")]
        public JsonResult CaseReviewed(CaseEssReviewedModel model)
        {
            try
            {
                if (model.CaseId == null)
                    throw new NullReferenceException("CaseId null");

                Case cAse = Case.GetById(model.CaseId);
                if (cAse == null)
                    throw new NullReferenceException("Case not found");

                if (model.Status == CaseStatus.Open)
                {
                    string assId = cAse.AssignedToId;
                    string assName = cAse.AssignedToName;
                    cAse = this.ToEmployeeLeaveRequest(model, true);
                    cAse.AssignedToId = assId;
                    cAse.AssignedToName = assName;
                    cAse.Segments.ForEach(m => m.Status = CaseStatus.Open);
                    if (cAse.AccommodationRequest != null && cAse.AccommodationRequest.Status == CaseStatus.Requested)
                    {
                        cAse.AccommodationRequest.Status = CaseStatus.Open;
                        if (cAse.AccommodationRequest.Accommodations != null)
                            cAse.AccommodationRequest.Accommodations.Where(a => a.Status == CaseStatus.Requested).ForEach(a => a.Status = CaseStatus.Open);
                    }
                    cAse.Status = CaseStatus.Open;
                    cAse = new CaseService().Using(c => c.UpdateCase(cAse, CaseEventType.CaseCreated));
                }
                else
                    new CaseService().Using(c => c.CancelCase(model.CaseId));

                return Json(new { Success = true });
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }
        #endregion

        #region Work Related (OSHA)

        [HttpGet, Secure("EditCase")]
        [Route("Cases/{caseId}/WorkRelated/Edit", Name = "WorkRelatedInfoEdit")]
        public ActionResult WorkRelatedInfoEdit(string caseId)
        {
            Case c = Case.GetById(caseId);
            if (c == null)
                return HttpNotFound();

            ViewBag.EmployeeId = c.Employee.Id;
            ViewBag.CaseEndDate = c.EndDate;

            WorkRelatedViewModel model = new WorkRelatedViewModel(c);
            if (c.CaseType.Contains(CaseType.Consecutive.ToString()) || c.CaseType.Contains(CaseType.Reduced.ToString()) || c.CaseType.Contains(CaseType.Intermittent.ToString()))
            {
                using (var caseService = new CaseService(CurrentCustomer, CurrentEmployer, CurrentUser))
                {
                    if (c.CaseType.Contains(CaseType.Consecutive.ToString()))
                    {
                        model.DaysAwayFromWork = caseService.CalculateDaysAwayFromWork(c);
                    }
                    if (c.CaseType.Contains(CaseType.Reduced.ToString()) || c.CaseType.Contains(CaseType.Intermittent.ToString()))
                    {
                        model.CalculatedDaysOnJobTransferOrRestriction = caseService.CalculateDaysOnJobTransferOrRestriction(c);
                    }
                }
            }
            return View("WorkRelatedInfoEdit", model);
        }

        [HttpGet, Secure("EditCase")]
        [Route("Cases/{caseId}/EmployeeRestrictions/{employeeId}", Name = "GetEmployeeRestrictions")]
        public JsonResult GetEmployeeRestrictions(string employeeId, string caseId)
        {
            List<EmployeeRestriction> restrictions = new List<EmployeeRestriction>();

            try
            {
                using (var demandService = new DemandService(CurrentUser))
                {
                    restrictions = demandService.GetJobRestrictionsForEmployee(employeeId, caseId).Select(r => r).ToList();

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

            return Json(restrictions, JsonRequestBehavior.AllowGet);
        }

        #region Special Hack Filters
        public class IgnoreworkRelatedSharpsModelErrorAttribute : IgnoreModelErrorAttribute
        {
            public IgnoreworkRelatedSharpsModelErrorAttribute() : base("WorkRelatedViewModel.SharpsInfo") { }
            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                var m = filterContext.Controller.ViewData.Model as WorkRelatedViewModel;
                if (m != null && m.Sharps != true)
                {
                    m.SharpsInfo = null;
                    return;
                }
                base.OnActionExecuting(filterContext);
            }
        }
        public class IgnoreworkRelatedProviderModelErrorAttribute : IgnoreModelErrorAttribute
        {
            public IgnoreworkRelatedProviderModelErrorAttribute() : base("WorkRelatedViewModel.ProviderContact") { }
            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                var m = filterContext.Controller.ViewData.Model as WorkRelatedViewModel;
                if (m != null && m.ProviderContact != null && string.IsNullOrWhiteSpace(m.ProviderContact.ContactTypeCode))
                {
                    m.ProviderContact = null;
                    return;
                }
                base.OnActionExecuting(filterContext);
            }
        }
        #endregion Special Hack Filters

        [HttpPost, Secure("EditCase")]
        [Route("Cases/{caseId}/WorkRelated/Edit", Name = "WorkRelatedInfoSave")]
        [IgnoreworkRelatedSharpsModelErrorAttribute]
        [IgnoreworkRelatedProviderModelErrorAttribute]
        public ActionResult WorkRelatedInfoSave(WorkRelatedViewModel model)
        {
            //By Pass Validations related to Sharps if not selected.
            //if (!(model.Sharps ?? false))
            //{
            //    if (ModelState.ContainsKey("SharpsInfo.TypeOfSharp")) ModelState["SharpsInfo.TypeOfSharp"].Errors.Clear();
            //    if (ModelState.ContainsKey("SharpsInfo.BrandOfSharp")) ModelState["SharpsInfo.BrandOfSharp"].Errors.Clear();
            //    if (ModelState.ContainsKey("SharpsInfo.ModelOfSharp")) ModelState["SharpsInfo.ModelOfSharp"].Errors.Clear();
            //    if (ModelState.ContainsKey("SharpsInfo.BodyFluidInvolved")) ModelState["SharpsInfo.BodyFluidInvolved"].Errors.Clear();
            //    if (ModelState.ContainsKey("SharpsInfo.HaveEngineeredInjuryProtection")) ModelState["SharpsInfo.HaveEngineeredInjuryProtection"].Errors.Clear();
            //    if (ModelState.ContainsKey("SharpsInfo.JobClassification")) ModelState["SharpsInfo.JobClassification"].Errors.Clear();
            //    if (ModelState.ContainsKey("SharpsInfo.LocationAndDepartment")) ModelState["SharpsInfo.LocationAndDepartment"].Errors.Clear();
            //    if (ModelState.ContainsKey("SharpsInfo.Procedure")) ModelState["SharpsInfo.Procedure"].Errors.Clear();
            //    if (ModelState.ContainsKey("SharpsInfo.InjuryLocation")) ModelState["SharpsInfo.InjuryLocation"].Errors.Clear();
            //}
            if (!ModelState.IsValid)
                return View("WorkRelatedInfoEdit", model);

            Case c = Case.GetById(model.CaseId);
            if (c == null)
                return HttpNotFound();

            c.WorkRelated = model.ApplyModel(c);
            if (c.WorkRelated != null && c.WorkRelated.Provider != null && !string.IsNullOrWhiteSpace(c.WorkRelated.Provider.ContactTypeCode))
            {
                c.WorkRelated.Provider.CustomerId = c.CustomerId;
                c.WorkRelated.Provider.EmployerId = c.EmployerId;
                c.WorkRelated.Provider.EmployeeId = c.Employee.Id;
                c.WorkRelated.CalculatedDaysOnJobTransferOrRestriction = model.CalculatedDaysOnJobTransferOrRestriction;
                c.WorkRelated.Provider.Save();
            }
            else
                c.WorkRelated.Provider = null;

            c.OnSavedNOnce((o, r) => r.Entity.WfOnWorkRelatedReportingChanged(CurrentUser));
            new CaseService().Using(s => s.UpdateCase(c));

            return RedirectToRoute("ViewCase", new { caseId = model.CaseId });
        }

        [HttpGet, Secure("EditCase")]
        [Route("Cases/{caseId}/WorkRelated/Forms/OSHA/301", Name = "GenerateOSHAForm301")]
        public ActionResult GenerateOSHAForm301(string caseId)
        {
            Case c = Case.GetById(caseId);
            if (c == null)
                return HttpNotFound();

            Attachment att = null;
            using (var svc = new WorkRelatedService())
                att = svc.GenerateOSHAForm301(c);

            if (att == null || att.File == null || att.File.Length == 0)
                return RedirectToRoute("ViewCase", new { caseId = caseId });

            return File(att.File, "application/pdf", "OSHA Form 301.pdf");
        }

        [HttpGet, Secure("EditCase")]
        [Route("Cases/{caseId}/WorkRelated/Forms/OSHA/100", Name = "GenerateForm100")]
        public ActionResult GenerateForm100(string caseId)
        {
            Case c = Case.GetById(caseId);
            if (c == null)
            {
                return HttpNotFound();
            }

            Attachment att = null;
            using (var svc = new WorkRelatedService())
            {
                att = svc.GenerateForm100(c);
            }

            if (att == null || att.File == null || att.File.Length == 0)
            {
                return RedirectToRoute("ViewCase", new { caseId = caseId });
            }

            return File(att.File, "application/pdf", "Form 100.pdf");
        }
        #endregion Work Related (OSHA)

        private void MakeNote(CaseEssCreateModel model, Case @case, string question, string requiredAnswer)
        {
            if (!string.IsNullOrWhiteSpace(requiredAnswer))
            {
                MakeNote(model, @case, m => true, "{0}? - {1}", question, requiredAnswer);
            }
        }

        private void MakeNote(CaseEssCreateModel model, Case @case, bool? flag, string question, string requiredAnswer)
        {
            if (flag.HasValue && flag.Value && !string.IsNullOrWhiteSpace(requiredAnswer))
            {
                MakeNote(model, @case, m => true, "{0}? - {1}", question, requiredAnswer);
            }
        }

        private void MakeNotes(string caseId, string customerId, string employerId, List<IntProcStepGroupViewModel> additionalQuestions)
        {
            foreach (var q in additionalQuestions)
            {
                foreach (var step in q.InteractiveProcessSteps)
                {
                    new CaseNote
                    {
                        Category = NoteCategoryEnum.CaseSummary,
                        CreatedBy = CurrentUser,
                        CaseId = caseId,
                        CustomerId = customerId,
                        EmployerId = employerId,
                        Public = false,
                        ModifiedBy = CurrentUser,
                        Notes = string.Format("{0}? - {1}", step.ReportLabel, step.SAnswer)
                    }.Save();
                }
            }
        }

        private void MakeNote(
            CaseEssCreateModel model,
            Case @case, Func<CaseEssCreateModel, bool> condition,
            string format,
            params object[] args
        )
        {
            if (!condition(model)) return;

            new CaseNote
            {
                Category = NoteCategoryEnum.CaseSummary,
                CreatedBy = CurrentUser,
                CaseId = @case.Id,
                CustomerId = @case.CustomerId,
                EmployerId = @case.EmployerId,
                Public = false,
                ModifiedBy = CurrentUser,
                Notes = String.Format(format, args)
            }.Save();
        }


        [Secure("JobAssignment")]
        [HttpPost, Route("Jobs/Assignment/Status", Name = "SetJobStatus")]
        [ValidateAntiForgeryToken]
        public ActionResult SetJobStatus(EmployeeJobViewModel employeeJobViewModel)
        {
            var status = employeeJobViewModel.Status;
            //EmployeeJobViewModel otherModel = new EmployeeJobViewModel(EmployeeJob.GetById(status.EmployeeJobId));
            //otherModel.Status = status;

            if (!ModelState.IsValid)
            {
                var job = EmployeeJob.GetById(status.EmployeeJobId);
                if (job != null)
                {
                    employeeJobViewModel.Code = job.JobCode;
                    employeeJobViewModel.Name = job.JobName;
                    employeeJobViewModel.Title = job.JobTitle;
                    if (job.Dates != null && !job.Dates.IsNull)
                    {
                        employeeJobViewModel.StartDate = job.Dates.StartDate;
                        employeeJobViewModel.EndDate = job.Dates.EndDate;
                    }
                    employeeJobViewModel.OfficeLocation = job.OfficeLocationCode;
                    employeeJobViewModel.EmployeeNumber = job.EmployeeNumber;
                    employeeJobViewModel.EmployerId = job.EmployerId;
                    employeeJobViewModel.IHasJobHistory = true;
                    employeeJobViewModel.TheJob = new JobViewModel(job.Job);

                    if (Request.IsAjaxRequest())
                        return PartialView("WorkRestrictionEmployeeJob", employeeJobViewModel);
                }
            }

            if (status != null && ModelState.IsValid)
            {
                using (var jobService = new JobService(CurrentUser))
                {
                    switch (status.Status)
                    {
                        case AdjudicationStatus.Approved:
                            jobService.ApproveEmployeeJob(status.EmployeeJobId);
                            break;
                        case AdjudicationStatus.Denied:
                            jobService.DenyEmployeeJob(status.EmployeeJobId, status.DenialReason.Value, status.DenialReasonOther);
                            break;
                        case AdjudicationStatus.Pending:
                        default:
                            jobService.PendEmployeeJob(status.EmployeeJobId);
                            break;
                    }
                }
            }

            return ReloadViaJavaScript();
        }

        [HttpGet]
        [Route("Cases/{employeeId}/Jobs", Name = "Jobs")]
        public JsonResult Jobs(string employeeId)
        {
            Employee emp = Employee.GetById(employeeId);
            EmployeeOrganization empOrg = emp.GetOfficeLocation();
            string currentLocation = empOrg == null ? null : empOrg.Code;
            List<Job> customerjobs = Job.AsQueryable().Where(j => j.EmployerId == emp.EmployerId && j.CustomerId == Customer.Current.Id).Where(j => string.IsNullOrWhiteSpace(currentLocation) || j.OrganizationCode == null || j.OrganizationCode == currentLocation).OrderBy(j => j.Name).ToList();
            //customerjobs.Insert(0,(new Job() { Code = "", Name = "Select a Job" }));
            var jobs = customerjobs.Select(job => new { Code = job.Code, Name = job.Name });
            return Json(jobs);
        }


        #region Comorbidity Guideline
        [Secure("EditCase"), HttpPost]
        [Route("Cases/CoMorbidityGuidelines", Name = "CoMorbidityGuidelines")]
        [ValidateApiAntiForgeryToken]
        public JsonResult GetCoMorbidityGuidelines(CoMorbidityModel model)
        {
            if (String.IsNullOrWhiteSpace(model.EmployeeId) || String.IsNullOrWhiteSpace(model.CaseId))
                return JsonError(new Exception(), "Missing parameter");

            try
            {
                Employee emp = Employee.GetById(model.EmployeeId);

                if (model.Args?.PS != null)
                {
                    model.Args.PS.CPTProcedureCodeString = model.Args.CPTProcedureCodeString;
                    model.Args.PS.HCPCSCodeString = model.Args.HCPCSCodeString;
                    model.Args.PS.ICDProcedureCodeString = model.Args.ICDProcedureCodeString;
                    model.Args.PS.NDCProcedureCodeString = model.Args.NDCProcedureCodeString;
                }
                CoMorbidityGuideline result = new CoMorbidityGuideline();

                //Check if feature is enabled
                if (emp == null || emp.Employer == null || !emp.Employer.HasFeature(Feature.GuidelinesData))
                    return Json(result);

                result = new DiagnosisGuidelinesService().GetCoMorbidityGuideline(model.CaseId, model.Args);
                return Json(result);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error getting Co-Morbidity Guidelines Data");
            }
        }


        #endregion

        [HttpGet, Secure("ViewCase")]
        [Route("Cases/{caseId}/GetCalculatedDaysOnJobTransferOrRestriction", Name = "GetCalculatedDaysOnJobTransferOrRestriction")]
        public JsonResult GetCalculatedDaysOnJobTransferOrRestriction(string caseId, string dateOfInjury = null)
        {
            int calculatedDaysOnJobTransferOrRestriction = 0;
            DateTime? injuryDateTime = null;
            if (!string.IsNullOrEmpty(dateOfInjury) && double.TryParse(dateOfInjury, out double injuryDays))
            {
                injuryDateTime = (new DateTime(1970, 1, 1).ToUtcDateTime()).AddMilliseconds(injuryDays).ToMidnight();
            }
            Case c = Case.GetById(caseId);
            if (c == null)
                return Json(calculatedDaysOnJobTransferOrRestriction);

            WorkRelatedViewModel model = new WorkRelatedViewModel(c);
            if (c.CaseType.Contains(CaseType.Reduced.ToString()) || c.CaseType.Contains(CaseType.Intermittent.ToString()))
            {
                using (var caseService = new CaseService(CurrentCustomer, CurrentEmployer, CurrentUser))
                {
                    calculatedDaysOnJobTransferOrRestriction = caseService.CalculateDaysOnJobTransferOrRestriction(c, injuryDateTime);
                }
            }
            return Json(new { CalculatedDaysOnJobTransferOrRestriction = calculatedDaysOnJobTransferOrRestriction });
        }
    }
}
