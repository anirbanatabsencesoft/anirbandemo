﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Models.Teams;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Controllers
{
    [Secure("AssignTeams")]
    public class TeamController : BaseController
    {
        #region My Teams
        [Secure, HttpGet, Route("MyTeam")]
        public ActionResult MyTeam()
        {
            List<TeamSave> teamSave = GetTeamsForUser();
            return View(teamSave);
        }

        [Secure, HttpPost, Route("MyTeam/RemoveTeamMember")]
        [ValidateApiAntiForgeryToken]
        public JsonResult RemoveMyTeamMember(TeamMember member)
        {
            try
            {
                using (TeamService svc = new TeamService(CurrentUser))
                {
                    List<TeamMember> teamMembers = svc.GetTeamMembers(member.TeamId);
                    if (!teamMembers.Any(tm => tm.UserId == CurrentUser.Id && tm.IsTeamLead))
                        return JsonError(new Exception("You are not the team lead of this team"), "You are not the team lead of this team");

                    TeamMember tmToRemove = teamMembers.FirstOrDefault(tm => tm.Id == member.Id);
                    if (tmToRemove == null)
                        return JsonError(new Exception("The user is not a part of the specified team"), "The user is not a part of the specified team");

                    svc.RemoveTeamMember(tmToRemove);
                    return Json(GetTeamsForUser());
                }
            }
            catch (AbsenceSoftException asex)
            {
                return JsonError(asex, asex.Message);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "An unexpected error occurred while removing the team member");
            }
        }
        
        [Secure, HttpPost, Route("MyTeam/AddTeamMember")]
        [ValidateApiAntiForgeryToken]
        public JsonResult AddMyTeamMember(TeamMember member)
        {
            try
            {
                using (TeamService svc = new TeamService(CurrentUser))
                {
                    List<TeamMember> teamMembers = svc.GetTeamMembers(member.TeamId);
                    if (!teamMembers.Any(tm => tm.UserId == CurrentUser.Id && tm.IsTeamLead))
                        return JsonError(new Exception("You are not the team lead of this team"), "You are not the team lead of this team");

                    TeamMember potentialTeamMember = teamMembers.FirstOrDefault(tm => tm.UserId == member.UserId);
                    if (potentialTeamMember != null)
                        return Json(GetTeamsForUser());

                    potentialTeamMember = new TeamMember()
                    {
                        TeamId = member.TeamId,
                        UserId = member.UserId
                    };

                    svc.SaveTeamMember(potentialTeamMember);
                    return Json(GetTeamsForUser());
                }
            }
            catch (AbsenceSoftException asex)
            {
                return JsonError(asex, asex.Message);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "An unexpected error occurred while adding the team member");
            }
        }

        [Secure, HttpGet, Route("MyTeam/Assignees")]
        public JsonResult MyTeamAssignees()
        {
            try
            {
                return Json(new TeamService(CurrentUser).GetUsersForTeamAssignmentByTeamLead());
            }
            catch (Exception ex)
            {
                return JsonError(ex, "An unexpected error occurred while retrieving the list of possible users");
            }
        }

        private List<TeamSave> GetTeamsForUser()
        {
            List<TeamSave> teamSave = new List<TeamSave>();
            using (TeamService svc = new TeamService(CurrentUser))
            {
                List<Team> teams = svc.GetTeamsForUser();
                foreach (Team item in teams)
                {
                    List<TeamMember> teamMembers = svc.GetTeamMembers(item.Id);
                    item.CurrentUserIsTeamLead = teamMembers.Any(t => t.UserId == CurrentUser.Id && t.IsTeamLead);
                    TeamSave myTeam = new TeamSave()
                    {
                        Team = item,
                        TeamMembers = teamMembers
                    };

                    teamSave.Add(myTeam);
                }
            }

            return teamSave;
        }

        #endregion

        #region Administration
        [HttpGet, Route("Team/List/{employerId?}", Name ="ViewTeams")]
        public ActionResult Index(string employerId)
        {
            List<Team> teams = new TeamService(CurrentUser).GetTeamsForCustomer(CurrentCustomer.Id);
            ViewBag.CustomerId = CurrentCustomer.Id;
            return View(teams);
        }

        

        [HttpGet, Route("Team/{customerId}/Edit/{teamId?}", Name = "EditTeam")]
        public ActionResult EditTeam(string customerId, string teamId)
        {
            Team t = null;
            if (!string.IsNullOrEmpty(teamId))
            {
                t = new TeamService(CurrentUser).GetTeamById(teamId);
            }
            else
            {
                t = new Team()
                {
                    CustomerId = customerId
                };
            }

            return View(t);
        }

        [HttpGet, Route("Team/{customerId}/Members/{teamId}", Name = "TeamMembers")]
        public ActionResult TeamMembers(string customerId, string teamId)
        {
            try
            {
                List<TeamMember> teamMembers = new TeamService(CurrentUser).GetTeamMembers(teamId);
                return Json(teamMembers);
            }
            catch (AbsenceSoftException asex)
            {
                return JsonError(asex, asex.Message);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "An unexpected error occurred while retrieving current team members");
            }
        }

        [HttpPost, Route("Team/Save/")]
        [ValidateApiAntiForgeryToken]
        public JsonResult TeamSave(TeamSave team)
        {
            try
            {
                if (!ModelState.IsValid)
                    return JsonError(null, string.Join(", ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage)));

                team.Team = new TeamService(CurrentUser).SaveTeam(team.Team);
                team.TeamMembers.ForEach(t =>
                {
                    t.TeamId = team.Team.Id;
                    t.CustomerId = team.Team.CustomerId;
                });
                team.TeamMembers = new TeamService(CurrentUser).SaveTeamMembers(team.Team.Id, team.TeamMembers);
                return Json(team);
            }
            catch (AbsenceSoftException asEx)
            {
                return JsonError(asEx, asEx.Message);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "An error occurred while trying to save the team");
            }
        }

        [HttpDelete, Route("Team/{customerId}/Delete/{teamId}")]
        [ValidateApiAntiForgeryToken]
        public JsonResult TeamDelete(string customerId, string teamId)
        {
            try
            {
                using (TeamService svc = new TeamService(CurrentUser))
                {
                    Team t = svc.GetTeamById(teamId);
                    svc.DeleteTeam(t);
                    return Json(true);
                }
                
            }
            catch (AbsenceSoftException asEx)
            {
                return JsonError(asEx, asEx.Message);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "An error occurred while trying to delete the team");
            }
        }



        #endregion

    }
}
