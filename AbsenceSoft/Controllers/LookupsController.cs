﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Models;
using AbsenceSoft.Models.Cases;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Tasks;
using MongoDB.Bson;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Models.Administration;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    public class LookupsController : BaseController
    {
        public const string ServiceOptionIndicatorKey = "ServiceOptionIndicator";

        [Secure, HttpGet]
        [Route("Lookups/AbsenceReasons/", Name = "AbsenceReasons")]
        public JsonResult AbsenceReasons(string employeeId, CaseType? caseType)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(employeeId))
                    throw new AbsenceSoftException("employeeId is not specified");

                List<AbsenceReason> absences;
                var list = new List<AbsenceReasonModel>();
                var reOrderedList = new List<AbsenceReasonModel>();
                //Get employee/employer specific
                using (CaseService service = new CaseService())
                {
                    absences = service.GetAbsenceReasons(employeeId, caseType);
                }

                // first get non-hierarchical ones
                //
                absences.Where(o => o.Category == null)
                    .ForEach(o => list.Add(new AbsenceReasonModel()
                    {
                        Id = o.Id,
                        Code = o.Code,
                        HelpText = o.HelpText,
                        Name = o.Name,
                        IsCategory = false,
                        IsChild = false,
                        IsForFamilyMember = (o.Flags & AbsenceReasonFlag.RequiresRelationship) == AbsenceReasonFlag.RequiresRelationship,
                        IsHiddenInESSSelect = (o.Flags & AbsenceReasonFlag.HideInEmployeeSelfServiceSelection) == AbsenceReasonFlag.HideInEmployeeSelfServiceSelection,
                        IsToShowAccomodationAlways = (o.Flags & AbsenceReasonFlag.ShowAccommodation) == AbsenceReasonFlag.ShowAccommodation
                    }
                    ));

                // now get the categories
                absences.Where(o => o.Category != null).Select(o => o.Category).Distinct()
                    .ForEach(category => list.Add(new AbsenceReasonModel()
                    {
                        HelpText = category,
                        Name = category,
                        Category = category,
                        IsCategory = true,
                        IsChild = false
                    }));

                // now get the categories' children
                //
                list.Where(o => o.Id == null).ForEach(o =>
                    o.Children.AddRange(
                        absences.Where(child => child.Category == o.Category).Select(child => new AbsenceReasonModel()
                        {
                            Id = child.Id,
                            Code = child.Code,
                            HelpText = child.HelpText,
                            Name = child.Name,
                            Category = o.Category,
                            IsCategory = false,
                            IsChild = true,
                            IsForFamilyMember = (child.Flags & AbsenceReasonFlag.RequiresRelationship) == AbsenceReasonFlag.RequiresRelationship,
                            IsHiddenInESSSelect = (child.Flags & AbsenceReasonFlag.HideInEmployeeSelfServiceSelection) == AbsenceReasonFlag.HideInEmployeeSelfServiceSelection,
                            IsToShowAccomodationAlways = (child.Flags & AbsenceReasonFlag.ShowAccommodation) == AbsenceReasonFlag.ShowAccommodation
                        })));

                var maxIndex = list.Count - 1;

                AbsenceReasonModel item = list.Where(m => m.Name == "Adoption/Foster Care").FirstOrDefault();
                if (item != null)
                {
                    if (maxIndex >= 3)
                    {
                        list.Remove(item);
                        list.Insert(3, item);
                    }
                }

                item = list.Where(m => m.Name == "Military").FirstOrDefault();
                if (item != null)
                {
                    if (maxIndex >= 4)
                    {
                        list.Remove(item);
                        list.Insert(4, item);
                    }
                }

                item = list.Where(m => m.Name == AbsenceReasonCategory.Administrative).FirstOrDefault();
                if (item != null)
                {
                    if (maxIndex >= 6)
                    {
                        list.Remove(item);
                        list.Insert(6, item);
                    }
                }

                return Json(list);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [Secure, HttpGet]
        [Route("Lookups/MilitaryStatuses", Name = "MilitaryStatuses")]
        public JsonResult MilitaryStatuses()
        {
            try
            {
                var values = Enum.GetValues(typeof(MilitaryStatus)).Cast<MilitaryStatus>().Select(o => new
                {
                    Id = o,
                    Text = o.ToString().SplitCamelCaseString()
                }).ToList();
                return Json(values);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [Secure, HttpGet]
        [Route("Lookups/EmployeeRelationships", Name = "EmployeeRelationships")]
        public JsonResult EmployeeRelationships(bool includeSelf)
        {
            try
            {
                var contactTypeService = new ContactTypeService();
                IEnumerable<dynamic> contactTypes = null;
                if (includeSelf)
                {
                    contactTypes = contactTypeService.GetContactTypes(AbsenceSoft.Data.Security.User.Current.CustomerId, null, ContactTypeDesignationType.Self, ContactTypeDesignationType.Personal, ContactTypeDesignationType.Administrative).Select(o => new
                    {
                        Id = o.Code,
                        Text = o.Name.SplitCamelCaseString(),
                        DesignationType = o.ContactCategory.ToString()
                    });
                }
                else
                {
                    contactTypes = contactTypeService.GetContactTypes(AbsenceSoft.Data.Security.User.Current.CustomerId, null, ContactTypeDesignationType.Personal).Select(o => new
                    {
                        Id = o.Code,
                        Text = o.Name.SplitCamelCaseString(),
                        DesignationType = o.ContactCategory.ToString()
                    });
                }
                return Json(contactTypes);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [HttpGet]
        [Route("Lookups/PolicyBehaviorTypes", Name = "PolicyBehaviorTypes")]
        public JsonResult PolicyBehaviorTypes()
        {
            try
            {
                var values = Enum.GetValues(typeof(PolicyBehavior)).Cast<PolicyBehavior>().Select(o => new
                {
                    Id = o,
                    Text = o.ToString().SplitCamelCaseString()
                }).ToList();
                return Json(values);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        /// <summary>
        /// Returns JSON of current Features defined in Enum  
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Lookups/Features", Name = "FeatureTypes")]
        public JsonResult FeatureTypes()
        {
            try
            {
                var values = Enum.GetValues(typeof(Feature)).Cast<Feature>().Select(o => new
                {
                    Id = (int)o,
                    Text = o.ToString().SplitCamelCaseString()
                }).ToList();
                return Json(values);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }


        [HttpGet]
        [Route("Lookups/FMLPeriodTypes", Name = "FMLPeriodTypes")]
        public JsonResult FMLPeriodTypes()
        {
            try
            {
                // Where clause added task #775
                var values = Enum.GetValues(typeof(PeriodType)).Cast<PeriodType>().Select(o => new
                {
                    Id = o,
                    Text = o.ToString().SplitCamelCaseString()
                }).ToList();
                return Json(values);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [Secure, HttpGet]
        [Route("Lookups/WorkflowItemStatuses", Name = "WorkflowItemStatuses")]
        public JsonResult WorkflowItemStatuses()
        {
            try
            {
                var todoItemStatuses = (from ToDoItemStatus ct in Enum.GetValues(typeof(ToDoItemStatus))
                                        select new { Id = (int)ct, Text = ct.ToString() }).ToList();
                todoItemStatuses.Insert(0, new { Id = 0, Text = "All" });

                return Json(todoItemStatuses);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [Secure, HttpGet]
        [Route("Lookups/CaseCancelReason", Name = "CaseCancelReason")]
        public JsonResult CaseCancelItems()
        {
            try
            {
                var CaseCancelItems = (from CaseCancelReason ct in Enum.GetValues(typeof(CaseCancelReason))
                                       select new { Id = (int)ct, Text = ct.ToString() }).ToList();
                return Json(CaseCancelItems);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [Secure, HttpGet]
        [Route("Lookups/CaseClosureCategory", Name = "CaseClosureCategory")]
        public JsonResult CaseClosureItems(string CaseId = null)
        {
            try
            {
                using (var casesService = new CaseService())
                {
                    var theCase = casesService.GetCaseById(CaseId);
                    Employer tempemp = theCase != null ? theCase.Employer : Current.Employer();
                    casesService.CurrentEmployer = tempemp;
                    var categories = casesService.GetAllCaseCategories(false);
                    var options = new List<SelectListItem> { new SelectListItem {
                        Value = "OTHER",
                        Text = "Other",

                    }
                 };
                    BuildCaseCategorySelectList(options, categories, null, CaseId);
                    return Json(options);
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        private static List<SelectListItem> BuildCaseCategorySelectList(List<SelectListItem> currentOptions, List<CaseCategory> categories, string prefix = null, string caseId = null)
        {
            string reasonId = null;
            if (caseId != null)
            {
                var caseService = new CaseService();
                var theCase = caseService.GetCaseById(caseId);
                if (theCase != null && theCase.Reason != null)
                {
                    reasonId = theCase.Reason.Id;
                }

            }
            foreach (var category in categories)
            {
                if (category.ReasonIds == null || category.ReasonIds.Where(item => category.ReasonIds.Contains(reasonId)).ToList().Count > 0)
                {
                    currentOptions.Add(
                        new SelectListItem
                        {
                            Value = category.Code,
                            Text = string.Format("{0}{1}", prefix, category.Name),

                        });
                }
            }

            return currentOptions;
        }
        [Secure, HttpGet]
        [Route("Lookups/ContactTypes", Name = "ContactTypes")]
        public JsonResult ContactTypeItems()
        {
            try
            {
                List<ContactType> list = new ContactTypeService().Using(s => s.GetContactTypes(AbsenceSoft.Data.Security.User.Current.CustomerId, null)).ToList();

                //Filter what contacts user can see by user's permission(s)
                var userPermissions = AbsenceSoft.Data.Security.User.Permissions.GetPermissions(AbsenceSoft.Data.Security.User.Current, Employer.CurrentId);
                List<ContactType> allowedContacts = new List<ContactType>();

                if (userPermissions.Contains(AbsenceSoft.Data.Security.Permission.ViewAdministrativeContact.Id))
                    allowedContacts.AddRange(list.Where(c => c.ContactCategory == ContactTypeDesignationType.Administrative));

                if (userPermissions.Contains(AbsenceSoft.Data.Security.Permission.ViewPersonalContact.Id))
                    allowedContacts.AddRange(list.Where(c => c.ContactCategory == ContactTypeDesignationType.Personal || c.ContactCategory == ContactTypeDesignationType.Self));

                if (userPermissions.Contains(AbsenceSoft.Data.Security.Permission.ViewMedicalContact.Id))
                    allowedContacts.AddRange(list.Where(c => c.ContactCategory == ContactTypeDesignationType.Medical));

                var ContactTypeItems = allowedContacts.Select(c => new { Id = c.Code, Text = c.Name }).ToList();

                if (userPermissions.Contains(AbsenceSoft.Data.Security.Permission.ViewMedicalContact.Id))
                {
                    ContactTypeItems.Remove(new { Id = "PROVIDER", Text = "Provider" });
                    ContactTypeItems.Insert(1, new { Id = "PROVIDER", Text = "Provider" });
                }

                return Json(ContactTypeItems.OrderBy(o => o.Text));
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure, HttpGet]
        [Route("Lookups/AdminContactTypes", Name = "AdminContactTypes")]
        public JsonResult AdministrativeContactTypes()
        {
            try
            {
                using (var contactTypeService = new ContactTypeService(CurrentUser))
                {
                    List<ContactType> list = contactTypeService.GetContactTypes(CurrentCustomer.Id, null, ContactTypeDesignationType.Administrative).ToList();
                    if (CurrentCustomer.HasFeature(Feature.EmployerContact))
                    {
                        list.AddRange(contactTypeService.GetContactTypesForCustomer().Select(ect => new ContactType()
                        {
                            Id = ect.Id,
                            Code = ect.Code,
                            Name = ect.Name
                        }).ToList());
                    }

                    return Json(list.OrderBy(o => o.Name));
                }

            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure, HttpGet]
        [Route("Lookups/GetPacketTypes", Name = "GetPacketTypes")]
        public JsonResult GetPacketTypes(string employerId)
        {
            try
            {
                var employer = Employer.AsQueryable().FirstOrDefault(m => m.Id == employerId);
                var LOAFeatureEnabled = true;
                if (employer != null)
                {
                    LOAFeatureEnabled = employer.HasFeature(Feature.LOA);
                }
                else
                {
                    LOAFeatureEnabled = CurrentUser.Employers.Any(m => m.Employer.HasFeature(Feature.LOA));
                }

                List<Template> templatesQueryable = null;
                List<Template> lstTemplate = null;
                if (Customer.Current != null && !string.IsNullOrEmpty(employerId))
                {
                    lstTemplate = Template.AsQueryable()
                        .Where(t => (t.CustomerId == null || t.CustomerId == Customer.Current.Id)
                            && (t.EmployerId == employerId || t.EmployerId == null)
                            && (t.IsLOASpecific == false || t.IsLOASpecific == LOAFeatureEnabled))
                        .ToList();

                }
                else if (Customer.Current != null)
                {
                    lstTemplate = Template.AsQueryable()
                        .Where(t => (t.CustomerId == null || t.CustomerId == Customer.Current.Id) &&
                            (t.IsLOASpecific == false || t.IsLOASpecific == LOAFeatureEnabled))
                        .ToList();
                }
                else
                {
                    lstTemplate = Template.AsQueryable()
                        .Where(t => t.CustomerId == null && (t.EmployerId == null || t.EmployerId == employerId)
                        && (t.IsLOASpecific == false || t.IsLOASpecific == LOAFeatureEnabled))
                        .OrderBy(m => m.Name)
                        .ThenByDescending(m => m.EmployerId)
                        .Distinct(m => m.Code)
                        .OrderBy(m => m.Name)
                        .ToList();
                }

                //get the custom templates first those are not suppressed
                templatesQueryable = lstTemplate.Where(p => p.IsCustom == true && p.IsSuppressed(Current.Customer(), employer) == false).ToList();
                //get suppressed list
                var lstSuppressed = lstTemplate.Where(p => p.IsCustom == true && p.IsSuppressed(Current.Customer(), employer) == true).ToList();
                //then the core template
                var lstCore = lstTemplate.Where(p => p.IsCustom == false && p.IsSuppressed(Current.Customer(), employer) == false).ToList();
                //merge core template if it doesn't exists
                foreach (var item in lstCore)
                {
                    if (templatesQueryable.Any(p => p.Code == item.Code) == false && lstSuppressed.Any(p => p.Code == item.Code) == false)
                        templatesQueryable.Add(item);
                }                

                //sort by name
                templatesQueryable = templatesQueryable
                        .OrderBy(m => m.Name)
                        .ThenByDescending(m => m.EmployerId)
                        .Distinct(m => m.Code)
                        .OrderBy(m => m.Name)
                        .ToList();                

                var permissions = AbsenceSoft.Data.Security.User.Permissions.ProjectedPermissions.Where(p => p.StartsWith("Comm_")).Select(p => p.Substring(5)).ToList();
                var templates = templatesQueryable.Where(r => permissions.Contains(r.Code)).Select(t => new { Value = t.Code, Text = t.Name }).ToList();

                return Json(templates);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [Secure, HttpGet]
        [Route("Lookups/GetPacketTypesForTodo", Name = "GetPacketTypesForTodo")]
        public JsonResult GetPacketTypesForTodo(string employerId)
        {
            try
            {
                var types = new ToDoService().Using(m => m.GetToDoTypes(employerId, CurrentUser));
                types.Remove(ToDoItemType.Manual);
                List<KeyValuePair<string, string>> lstType = new List<KeyValuePair<string, string>>();

                foreach (KeyValuePair<ToDoItemType, string> item in types)
                {
                    var typeAddInList = new KeyValuePair<string, string>(item.Key.ToString(), item.Value);
                    lstType.Add(typeAddInList);
                }

                lstType.Insert(0, new KeyValuePair<string, string>(ToDoItemType.Manual.ToString(), ToDoItemType.Manual.ToString().SplitCamelCaseString()));
                var dictionary = lstType.ToDictionary(x => x.Key, x => x.Value);
                return Json(new { Response = dictionary });
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [Secure, HttpGet]
        [Route("Lookups/GetNoteCategories", Name = "GetNoteCategories")]
        public JsonResult GetNoteCategories()
        {
            try
            {
                var noteCategories = (from NoteCategoryEnum ct in Enum.GetValues(typeof(NoteCategoryEnum))
                                      select new { Id = (int)ct, Text = ct.ToString() }).ToList();

                noteCategories.Insert(0, new { Id = -1, Text = "Filter" });

                return Json(noteCategories);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [Secure, HttpGet]
        [Route("Lookups/GetAttachmentTypes", Name = "GetAttachmentTypes")]
        public JsonResult GetAttachmentTypes()
        {
            try
            {
                var attachmentTypes = (from AttachmentType ct in Enum.GetValues(typeof(AttachmentType))
                                       select new { Id = (int)ct, Text = ct.ToString() }).ToList();

                attachmentTypes.Insert(0, new { Id = -1, Text = "Filter" });

                return Json(attachmentTypes);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [Secure, HttpGet]
        [Route("Lookups/GetEmailRepliesTypes", Name = "GetEmailRepliesTypes")]
        public JsonResult GetEmailRepliesTypes()
        {
            try
            {
                var emailRepliesType = (from EmailReplies ct in Enum.GetValues(typeof(EmailReplies))
                                        select new { Id = (int)ct, Text = ct.ToString() }).ToList();

                return Json(emailRepliesType);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        #region Policy Configuration

        [HttpGet]
        [Route("Lookups/PolicyRuleGroupTypes", Name = "PolicyRuleGroupTypes")]
        public JsonResult PolicyRuleGroupTypes()
        {
            try
            {
                var enumValues = Enum.GetValues(typeof(PolicyRuleGroupType)).Cast<PolicyRuleGroupType>();
                if (!CurrentCustomer.HasFeature(Feature.PolicyCrosswalk))
                    enumValues = enumValues.Where(prgt => prgt != PolicyRuleGroupType.Crosswalk);

                var values = enumValues.Select(ev => new
                {
                    Id = ev,
                    Text = ev.ToString().SplitCamelCaseString()
                }).ToList();

                return Json(values);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [HttpGet]
        [Route("Lookups/PolicyShowTypes", Name = "PolicyShowTypes")]
        public JsonResult PolicyShowTypes()
        {
            try
            {
                var values = Enum.GetValues(typeof(PolicyShowType)).Cast<PolicyShowType>().Select(o => new
                {
                    Id = o,
                    Text = o.ToString().SplitCamelCaseString()
                }).ToList();
                return Json(values);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [HttpGet]
        [Route("Lookups/CaseEventDateTypes", Name = "CaseEventDateTypes")]
        public JsonResult CaseEventDateTypes()
        {
            try
            {
                var values = Enum.GetValues(typeof(CaseEventDateType)).Cast<CaseEventDateType>().Select(o => new
                {
                    Id = o,
                    Text = o.ToString().SplitCamelCaseString()
                }).ToList();
                return Json(values);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [HttpGet]
        [Route("Lookups/CaseEventTypes", Name = "CaseEventTypes")]
        public JsonResult CaseEventTypes()
        {
            try
            {
                var values = Enum.GetValues(typeof(CaseEventType)).Cast<CaseEventType>().Select(o => new
                {
                    Id = o,
                    Text = o.ToString().SplitCamelCaseString()
                }).ToList();
                return Json(values);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [HttpGet]
        [Route("Lookups/PolicyTypes", Name = "PolicyTypes")]
        public JsonResult PolicyTypes()
        {
            try
            {
                var values = Enum.GetValues(typeof(PolicyType)).Cast<PolicyType>().Select(o => new
                {
                    Id = o,
                    Text = o.ToString().SplitCamelCaseString()
                }).ToList();
                return Json(values);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [HttpGet]
        [Route("Lookups/RuleGroupSuccessTypes", Name = "RuleGroupSuccessTypes")]
        public JsonResult RuleGroupSuccessTypes()
        {
            try
            {
                var values = Enum.GetValues(typeof(RuleGroupSuccessType)).Cast<RuleGroupSuccessType>().Select(o => new
                {
                    Id = o,
                    Text = o.ToString().SplitCamelCaseString()
                }).ToList();
                return Json(values);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [HttpGet]
        [Route("Lookups/EntitlementTypes", Name = "EntitlementTypes")]
        public JsonResult EntitlementTypes()
        {
            try
            {
                var values = Enum.GetValues(typeof(EntitlementType)).Cast<EntitlementType>().Select(o => new
                {
                    Id = o,
                    Text = o.ToString().SplitCamelCaseString()
                }).ToList();
                return Json(values);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [HttpGet]
        [Route("Lookups/PolicyEliminationTypes", Name = "PolicyEliminationTypes")]
        public JsonResult PolicyEliminationTypes()
        {
            try
            {
                var values = Enum.GetValues(typeof(PolicyEliminationType)).Cast<PolicyEliminationType>().Select(o => new
                {
                    Id = o,
                    Text = o.ToString().SplitCamelCaseString()
                }).ToList();
                return Json(values);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [HttpGet]
        [Route("Lookups/IntermittentRestrictionsTypes", Name = "IntermittentRestrictionsTypes")]
        public JsonResult IntermittentRestrictionsTypes()
        {
            try
            {
                var values = Enum.GetValues(typeof(IntermittentRestrictions)).Cast<IntermittentRestrictions>().Select(o => new
                {
                    Id = o,
                    Text = o.ToString().SplitCamelCaseString()
                }).ToList();
                return Json(values);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }




        [HttpGet]
        [Route("Lookups/CaseTypes", Name = "CaseTypes")]
        public JsonResult CaseTypes()
        {
            try
            {
                var values = Enum.GetValues(typeof(CaseType)).Cast<CaseType>().Select(o => new
                {
                    Id = o,
                    Text = o.ToString().SplitCamelCaseString()
                }).ToList();
                return Json(values);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }



        [HttpGet]
        [Route("Lookups/PolicyEvents", Name = "PolicyEvents")]
        public JsonResult PolicyEvents()
        {
            try
            {
                var values = Enum.GetValues(typeof(PolicyEvent)).Cast<PolicyEvent>().Select(o => new
                {
                    Id = o,
                    Text = o.ToString().SplitCamelCaseString()
                }).ToList();
                return Json(values);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        #endregion

        [HttpGet]
        [Route("Lookups/GenderTypes", Name = "GenderTypes")]
        public JsonResult GenderTypes()
        {
            try
            {
                var values = Enum.GetValues(typeof(Gender)).Cast<Gender>().Select(o => new
                {
                    Id = o,
                    Text = o.ToString().SplitCamelCaseString()
                }).ToList();
                return Json(values);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [HttpGet]
        [Route("Lookups/UnitTypes", Name = "UnitTypes")]
        public JsonResult UnitTypes()
        {
            try
            {
                var values = Enum.GetValues(typeof(Unit)).Cast<Unit>().Select(o => new
                {
                    Id = o,
                    Text = o.ToString().SplitCamelCaseString()
                }).ToList();
                return Json(values);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure, HttpGet]
        [Route("Lookups/GetEmployersForUser", Name = "GetEmployersForUser")]
        public JsonResult GetEmployersForUser()
        {
            using (var svc = new EmployerService())
            {
                return Json(svc.GetEmployersForUser(CurrentUser));
            }
        }

        [Secure, HttpGet]
        [Route("Lookups/GetEmployersForUserPlusUser", Name = "GetEmployersForUserPlusUser")]
        public JsonResult GetEmployersForUserPlusUser()
        {
            using (var svc = new EmployerService())
            {
                var employers = new List<Employer>
                {
                    new Employer
                    {
                        Id = "0",
                        Name = "User Access",
                        IsDeleted = false
                    }
                };
                var employersForUser = svc.GetEmployersForUser(CurrentUser)
                    .Where(p => !p.IsDeleted)
                    .OrderBy(p => p.Name.ToLowerInvariant());
                employers.AddRange(employersForUser);
                return Json(employers);
            }
        }

        [Secure, HttpGet]
        [Route("Lookups/GetRolesUser", Name = "GetRolesUser")]
        public JsonResult GetRolesUser(string userId)
        {
            var user = Data.Security.User.GetById(userId);
            return Json(user.Roles);
        }

        [Secure, HttpGet]
        [Route("Lookups/GetMultiEmployerAccessFeature", Name = "GetMultiEmployerAccessFeature")]
        public JsonResult GetMultiEmployerAccessFeature()
        {
            return Json(new { HasMultiEmployerAccess = CurrentCustomer.HasFeature(Feature.MultiEmployerAccess) });
        }

        [Secure, HttpGet]
        [Route("Lookups/GetEmployeeSelfServiceFeature", Name = "GetEmployeeSelfServiceFeature")]
        public JsonResult GetEmployeeSelfServiceFeature()
        {
            return Json(new { HasEmployeeSelfService = CurrentCustomer.HasFeature(Feature.EmployeeSelfService) });
        }

        [Secure, HttpGet]
        [Route("Lookups/GetEmployersForCustomer", Name = "GetEmployersForCustomer")]
        public JsonResult GetEmployersForCustomer()
        {
            using (var svc = new EmployerService())
            {
                return Json(svc.GetEmployersForCustomer(CurrentUser.CustomerId));
            }
        }

        [Secure, HttpGet]
        [Route("Lookups/CaseAssigneeTypes", Name = "CaseAssigneeTypes")]
        public JsonResult CaseAssigneeTypes()
        {
            using (var svc = new CaseService())
            {
                return Json(svc.GetCaseAssigneeTypesForCustomer());
            }
        }

        [Secure, HttpGet]
        [Route("Lookups/EmployerContactTypes", Name = "EmployerContactTypes")]
        public JsonResult EmployerContactTypes()
        {
            using (var svc = new ContactTypeService(CurrentUser))
            {
                return Json(svc.GetContactTypesForCustomer());
            }
        }

        [HttpGet]
        [Route("Lookups/JobClassifications", Name = "JobClassifications^")]
        public JsonResult JobClassifications()
        {
            try
            {
                var values = Enum.GetValues(typeof(JobClassification)).Cast<JobClassification>().Select(o => new
                {
                    Id = o,
                    Text = o.ToString().SplitCamelCaseString()
                }).ToList();
                return Json(values);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [HttpGet, Secure]
        [Route("Lookups/OfficeLocation", Name = "GetOfficeLocations")]
        public JsonResult GetOfficeLocations(string workState, DateTime? startDate, DateTime? endDate, bool isAccommodation)
        {
            try
            {
                return Json(
                    Organization.AsQueryable()
                        .Where(c => c.CustomerId == CurrentCustomer.Id
                            && c.TypeCode == OrganizationType.OfficeLocationTypeCode).ToList().Select(s => new
                            {
                                Text = s.Name,
                                Value = s.Code
                            })
                );
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [HttpGet, Secure]
        [Route("Lookups/WorkState", Name = "GetWorkState")]
        public JsonResult GetWorkState(DateTime? startDate, DateTime? endDate, bool isAccommodation)
        {
            try
            {
                if (startDate.HasValue && endDate.HasValue)
                {
                    if (isAccommodation)
                    {
                        var values = Case.AsQueryable(CurrentUser)
                               .Where(e => e.Employee.WorkState != null && e.Employee.WorkState != "" && e.IsAccommodation == true &&
                                   ((e.StartDate >= startDate && e.StartDate <= endDate) || (e.EndDate >= startDate && e.EndDate <= endDate)))
                               .Select(e => e.Employee.WorkState)
                               .Distinct()
                               .ToList()
                               .OrderBy(e => e)
                               .Select(e => new
                               {
                                   Text = e,
                                   Value = e
                               }).ToList();

                        return Json(values);
                    }
                    else
                    {
                        var values = Case.AsQueryable(CurrentUser)
                              .Where(e => e.Employee.WorkState != null && e.Employee.WorkState != "" &&
                                  ((e.StartDate >= startDate && e.StartDate <= endDate) || (e.EndDate >= startDate && e.EndDate <= endDate)))
                              .Select(e => e.Employee.WorkState)
                              .Distinct()
                              .ToList()
                              .OrderBy(e => e)
                              .Select(e => new
                              {
                                  Text = e,
                                  Value = e
                              }).ToList();

                        return Json(values);
                    }
                }
                else
                {
                    if (isAccommodation)
                    {
                        var values = Case.AsQueryable(CurrentUser)
                             .Where(e => e.Employee.WorkState != null && e.Employee.WorkState != "" && e.IsAccommodation == true)
                             .Select(e => e.Employee.WorkState)
                             .Distinct()
                             .ToList()
                             .OrderBy(e => e)
                             .Select(e => new
                             {
                                 Text = e,
                                 Value = e
                             }).ToList();

                        return Json(values);
                    }
                    else
                    {
                        var values = Case.AsQueryable(CurrentUser)
                             .Where(e => e.Employee.WorkState != null && e.Employee.WorkState != "")
                             .Select(e => e.Employee.WorkState)
                             .Distinct()
                             .ToList()
                             .OrderBy(e => e)
                             .Select(e => new
                             {
                                 Text = e,
                                 Value = e
                             }).ToList();

                        return Json(values);
                    }
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [HttpPost, Secure]
        [Route("Lookups/WorkStateForStatusReport", Name = "GetWorkStateForStatusReport")]
        [ValidateApiAntiForgeryToken]
        public JsonResult GetWorkStateForStatusReport(ListCriteria criteria)
        {
            try
            {
                return Json(new CaseService().Using(m => m.GetWorkStateForStatusReport(criteria, CurrentUser)));
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [HttpGet]
        [Route("Lookups/PayPeriodTypes", Name = "PayPeriodTypes")]
        public JsonResult PayPeriodTypes()
        {
            try
            {
                var values = Enum.GetValues(typeof(PayPeriodType)).Cast<PayPeriodType>().Select(o => new
                {
                    Id = o,
                    Text = o.ToString().SplitCamelCaseString()
                }).ToList();
                return Json(values);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpGet]
        [Route("Lookups/AccommodationTypes/{employerId?}", Name = "AccommodationTypes")]
        public JsonResult AccommodationTypes(string employerId, CaseType? caseType)
        {
            try
            {
                using (var accommodationService = new AccommodationService(CurrentCustomer, CurrentEmployer, CurrentUser))
                {
                    var filteredAccomTypes = accommodationService.GetAccommodationTypes()
                        .Where(a => !a.PreventNew).ToList();
                    return Json(filteredAccomTypes);
                }

            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpGet]
        [Route("Lookups/GetAllCaseStatus", Name = "GetAllCaseStatus")]
        public JsonResult GetAllCaseStatus()
        {
            var caseStatuses = (from CaseStatus ct in Enum.GetValues(typeof(CaseStatus))
                                select new { Id = (int)ct, Text = ct.ToString() }).ToList();
            caseStatuses.Add(new { Id = -1, Text = "Filter" });
            caseStatuses.Insert(0, new { Id = -2, Text = "Open + Requested" });

            return Json(caseStatuses);
        }
        [HttpGet]
        [Route("Lookups/GetJobDenialReasons", Name = "GetJobDenialReasons")]
        public JsonResult GetJobDenialReasons()
        {
            var jobDenialReasons = (from JobDenialReason jdr in Enum.GetValues(typeof(JobDenialReason))
                                    select new { Id = ((int)jdr).ToString(), Text = jdr.ToString() }).ToList();
            //jobDenialReasons.Insert(0, new { Id = "", Text = "" });

            return Json(jobDenialReasons);
        }
        [HttpGet]
        [Route("Lookups/{userId}/GetOrganizationTypes", Name = "GetOrganizationTypes")]
        public JsonResult GetOrganizationTypes(string userId)
        {
            using (var svc = new OrganizationService(CurrentUser))
            {
                var types = svc.GetOrganizationTypes(userId)
                    ?? new List<OrganizationType>();
                if (types.All(type => type.Code.ToUpper().Trim() != "OFFICELOCATION"))
                {
                    types.Insert(0, new OrganizationType
                    {
                        Code = "OFFICELOCATION",
                        Name = "Office Location"
                    });
                }
                return Json(types);
            }
        }

        [HttpGet]
        [Route("Lookups/{employerId}/GetEmployerOrganizationTypes", Name = "GetEmployerOrganizationTypes")]
        public JsonResult GetEmployerOrganizationTypes(string employerId)
        {
            using (var svc = new OrganizationService(CurrentUser))
            {
                List<OrganizationType> orgTypes = svc.GetEmployerOrganizationTypes(employerId);
                if (!orgTypes.Where(type => type.Code.ToUpper().Trim() == "OFFICELOCATION").Any())
                {
                    orgTypes.Insert(0, new OrganizationType() { Code = "OFFICELOCATION", Name = "Office Location" });
                }
                return Json(orgTypes.Select(orgType => new { Code = orgType.Code, Name = orgType.Name }));
            }
        }
        [HttpGet]
        [Route("Lookups/{employerId}/GetEmployerOrganizations", Name = "GetEmployerOrganizations")]
        public JsonResult GetEmployerOrganizations(string employerId)
        {
            return Json(Organization.AsQueryable().Where(org => org.EmployerId == employerId && org.CustomerId == CurrentCustomer.Id).OrderBy(org => org.Name).Select(org => new { Code = org.Code, Name = org.Name }));
        }

        [HttpGet]
        [Route("Lookups/GetCustomerServiceIndicatorOptions", Name = "GetCustomerServiceIndicatorOptions")]
        public JsonResult GetCustomerServiceIndicatorOptions()
        {
            try
            {

                List<CustomerServiceOption> customerServiceIndicatorOptions;
                using (var customerServiceOptionService = new CustomerServiceOptionService())
                {
                    customerServiceIndicatorOptions = customerServiceOptionService.GetCustomerServiceOptions(ServiceOptionIndicatorKey);
                }

                var customerServiceOptions = customerServiceIndicatorOptions.Select(
                    p => new CustomerServiceOptionModel
                    {
                        Id = p.Id,
                        Value = p.Value,
                        Order = p.Order
                    });

                return Json(customerServiceOptions);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpGet, Route("Lookups/ListCountries", Name ="ListCountries")]
        public JsonResult GetCountries()
        {
            using (var administrationService = new AdministrationService(CurrentUser))
            {
                var countries = administrationService.GetCountriesFromXML(includeRegions: false);
                return Json(countries.Select(c => new CountryViewModel(c)).ToList());
            }
        }

        [HttpGet, Route("Lookups/ListCountriesAndRegions", Name = "ListCountriesAndRegions")]
        public JsonResult GetCountriesAndRegions()
        {
            using (var administrationService = new AdministrationService(CurrentUser))
            {
                var countries = administrationService.GetCountriesFromXML(includeRegions: true);
                return Json(countries.Select(c => new CountryViewModel(c)).ToList());
            }
        }

        [HttpGet]
        [Route("Lookups/{employerId}/GetEmployerOfficeLocations", Name = "GetEmployerOfficeLocations")]
        public JsonResult GetEmployerOfficeLocations(string employerId)
        {
            return Json(Organization.AsQueryable().Where(org => org.EmployerId == employerId && org.CustomerId == CurrentCustomer.Id && org.TypeCode == OrganizationType.OfficeLocationTypeCode).Select(org => new { Code = org.Code, Name = org.Name }).ToList().OrderBy(org => org.Name));
        }

        [HttpGet]
        [Route("Lookups/{employerId}/GetOshaFormFieldOptions/{oshaFormFieldCode}", Name = "GetOshaFormFieldOptions")]
        public JsonResult GetOshaFormFieldOptions(string employerId, string oshaFormFieldCode)
        {
            using (var oshaService = new OshaFormFieldService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                OshaFormField oshaFormField = oshaService.GetOshaFormFieldByCode(oshaFormFieldCode);
                if (oshaFormField.Options != null)
                {
                    return Json(oshaFormField.Options.Select(option => new { Code = option, Name = option }));
                }
            }
            return null;
        }

        [HttpGet]
        [Route("Lookups/EventDateTypes", Name = "EventDateTypes")]
        public JsonResult EventDateTypes()
        {
            try
            {
                var values = Enum.GetValues(typeof(EventDateType)).Cast<EventDateType>().Select(o => new
                {
                    Id = o,
                    Text = o.ToString().SplitCamelCaseString()
                }).ToList();
                return Json(values);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }
    }
}