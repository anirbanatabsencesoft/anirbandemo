﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Models.Contacts;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    [Secure]
    public class ContactsController : BaseController
    {
        [Title("Employee Contacts"), Secure("EditContact", "ViewContact")]
        [Route("Employees/{employeeId}/Contacts", Name = "EmployeeContacts")]
        public ViewResult EmployeeContactsIndex(string employeeId)
        {
            if (String.IsNullOrEmpty(employeeId))
            {
                throw new ArgumentNullException("employeeId");
            }

            var model = new EmployeeParentedModel()
            {
                EmployeeId = employeeId
            };

            var emp = Employee.GetById(employeeId);
            var LOAFeatureEnabled = emp.Employer.HasFeature(Feature.LOA);

            var contactTypeService = new ContactTypeService();

            List<ContactType> allowedContacts = new List<ContactType>();

            if (emp != null)
            {
                //Get all contacts for employee
                var list = contactTypeService.GetContactTypesForEmployee(emp);

                //Filter what contacts user can see by user's permission(s)
                var userPermissions = AbsenceSoft.Data.Security.User.Permissions.GetPermissions(AbsenceSoft.Data.Security.User.Current, Employer.CurrentId);

                if (userPermissions.Contains(AbsenceSoft.Data.Security.Permission.ViewAdministrativeContact.Id))
                    allowedContacts.AddRange(list.Where(c => c.ContactCategory == ContactTypeDesignationType.Administrative));

                if (userPermissions.Contains(AbsenceSoft.Data.Security.Permission.ViewPersonalContact.Id) && LOAFeatureEnabled)
                    allowedContacts.AddRange(list.Where(c => c.ContactCategory == ContactTypeDesignationType.Personal || c.ContactCategory == ContactTypeDesignationType.Self));

                if (userPermissions.Contains(AbsenceSoft.Data.Security.Permission.ViewMedicalContact.Id))
                    allowedContacts.AddRange(list.Where(c => c.ContactCategory == ContactTypeDesignationType.Medical));
            }
            else
            {
                allowedContacts = contactTypeService.GetContactTypes(null, AbsenceSoft.Data.Security.User.Current.CustomerId, null);
            }

            ViewBag.ContactTypes = new SelectList(allowedContacts.Select(c => new { ID = c.Code, Name = c.Name }).ToList(), "ID", "Name");

            return View("Index", model);
        }

        [Title("Case Contacts"), Secure("EditContact")]
        [Route("Cases/{caseId}/Contacts", Name = "CaseContacts")]
        public ViewResult CaseContactsIndex(string caseId)
        {
            if (String.IsNullOrEmpty(caseId))
            {
                throw new ArgumentNullException("caseId");
            }

            var model = new CaseParentedModel()
            {
                CaseId = caseId
            };


            var cs = Case.GetById(caseId);
            var contactTypeService = new ContactTypeService();

            if (cs != null && cs.Employee != null)
                ViewBag.ContactTypes = new SelectList(contactTypeService.GetContactTypesForEmployee(cs.Employee).Select(c => new { ID = c.Code, Name = c.Name }).ToList(), "ID", "Name");
            else
                ViewBag.ContactTypes = new SelectList(contactTypeService.GetContactTypes(AbsenceSoft.Data.Security.User.Current.CustomerId, null).Select(c => new { ID = c.Code, Name = c.Name }).ToList(), "ID", "Name");

            return View("Index", model);
        }

        [HttpPost, Secure("CreateContact")]
        [Route("Employees/{employeeId}/Contact", Name = "CreateEmployeeContact")]
        [ValidateApiAntiForgeryToken]
        public ActionResult CreateForEmployee(CreateContactViewModel viewModel)
        {
            Employee employee = Employee.GetById(viewModel.Id);          
            if (employee == null)
                throw new Exception("Employee not found");
            EmployeeContact contact = new EmployeeContact();
            using (var caseService = new CaseService())
            using (var empService = new EmployeeService())
            {
                contact = empService.GetContact(viewModel.EmployeeContactId);
                DateTime? prevDOB = contact?.Contact?.DateOfBirth;
                contact = viewModel.ApplyModel(contact);
                DateTime? currentDOB = contact?.Contact?.DateOfBirth;
                bool updateForEligibility = prevDOB != currentDOB ? true : false;
                contact.CustomerId = employee.CustomerId;
                contact.EmployerId = employee.EmployerId;
                empService.SaveContact(contact);
                caseService.UpdateContactOnAssociatedCases(contact, updateForEligibility);
            }
            return Content("employee");
        }

        [HttpGet, Secure("EditContact")]
        [Route("Employees/{employeeId}/Contacts/{contactId}", Name = "GetEmployeeContact")]
        public JsonResult GetEmployeeContact(string employeeId, string contactId)
        {
            try
            {
                using (var employeeService = new EmployeeService())
                {
                    EmployeeContact contact = employeeService.GetContact(contactId);
                    if (contact == null || contact.EmployeeId != employeeId)
                        return JsonError(null, "Contact not found for employee", 404);

                    CreateContactViewModel viewModel = new CreateContactViewModel(contact);
                    return Json(viewModel);
                }

            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpPost, Secure("DeleteContact")]
        [Route("Employees/{contactId}/DeleteContact", Name = "DeleteEmployeeContact")]
        [ValidateApiAntiForgeryToken]
        public void DeleteEmployeeContact(string contactId)
        {
            EmployeeContact employeeContact = EmployeeContact.GetById(contactId);
            if (employeeContact != null)
            {
                employeeContact.Delete();
            }
        }

        [HttpGet]
        [Route("Employees/{employeeId}/Contacts/Medical", Name = "EmployeeMedicalContactsList")]
        public JsonResult ListForEmployeeMedicalContacts(string employeeId)
        {
            try
            {
                //Filter what contacts user can see by user's permission(s)
                var userPermissions = AbsenceSoft.Data.Security.User.Permissions.GetPermissions(AbsenceSoft.Data.Security.User.Current, Employer.CurrentId);
                if (userPermissions.Contains(AbsenceSoft.Data.Security.Permission.ViewMedicalContact.Id))
                {
                    using (var service = new EmployeeService())
                    {
                        var results = service.GetMedicalContacts(CurrentUser.CustomerId, employeeId)
                            .Select(e => new
                            {
                                ContactId = e.Id,
                                ContactTypeCode = e.ContactTypeCode,
                                ContactTypeName = e.ContactTypeName,
                                WorkPhone = string.IsNullOrEmpty(e.Contact.WorkPhone) ? string.Empty : e.Contact.WorkPhone,
                                CellPhone = string.IsNullOrEmpty(e.Contact.CellPhone) ? string.Empty : e.Contact.CellPhone,
                                HomePhone = string.IsNullOrEmpty(e.Contact.HomePhone) ? string.Empty : e.Contact.HomePhone,
                                Fax = string.IsNullOrEmpty(e.Contact.Fax) ? string.Empty : e.Contact.Fax,
                                Address = ((e.Contact.Address != null) && (e.Contact.Address.Address1) == string.Empty) ? string.Empty : (e.Contact.Address.Address1) + (!string.IsNullOrEmpty(e.Contact.Address.City) ? ", " + e.Contact.Address.City : string.Empty) + (!string.IsNullOrEmpty(e.Contact.Address.State) ? ", " + e.Contact.Address.State : string.Empty) + (!string.IsNullOrEmpty(e.Contact.Address.PostalCode) ? ", " + e.Contact.Address.PostalCode : string.Empty) + " " + (e.Contact.Address.Country),
                                Email = e.Contact.Email,
                                Name = e.Contact.FirstName + (!string.IsNullOrEmpty(e.Contact.LastName) ? " " + e.Contact.LastName : string.Empty),
                                Title = string.IsNullOrEmpty(e.Contact.Title) ? string.Empty : e.Contact.Title,
                                Company = string.IsNullOrEmpty(e.Contact.CompanyName) ? string.Empty : e.Contact.CompanyName,
                                FirstName = e.Contact.FirstName,
                                LastName = e.Contact.LastName,
                                IsPrimary = e.Contact.IsPrimary,
                                ContactPersonName = e.Contact.ContactPersonName
                            });
                        return Json(results, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    AbsenceSoft.Common.Log.Warn("User '{1}' has no Permissions for Medical Contacts, Employee Id: '{0}'", employeeId, CurrentUser);
                    return Json(new object[0], JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpGet] //, Secure("EditContact", "ViewContact")
        [Route("Employees/{employeeId}/Contacts/CurrentUserList", Name = "EmployeeCurrentUserList")]
        public JsonResult CurrentUserListForEmployee(string employeeId, string contactTypeCode)
        {
            try
            {
                var contactTypeService = new ContactTypeService();
                var contactTypeList = contactTypeService.GetContactTypes(AbsenceSoft.Data.Security.User.Current.CustomerId, null);

                var emp = Employee.GetById(employeeId);
                if (emp == null) return JsonError(new Exception("Employee not found"), "Employee not found", 500);
                var LOAFeatureEnabled = emp.Employer.HasFeature(Feature.LOA);

                using (var service = new EmployeeService())
                {
                    if (string.IsNullOrEmpty(contactTypeCode))
                    {
                        //Get all contacts for employee
                        var list = service.GetContacts(employeeId);

                        //Filter what contacts user can see by user's permission(s)
                        var userPermissions = AbsenceSoft.Data.Security.User.Permissions.GetPermissions(AbsenceSoft.Data.Security.User.Current, Employer.CurrentId);
                        List<string> allowedContacts = new List<string>();

                        if (userPermissions.Contains(AbsenceSoft.Data.Security.Permission.ViewAdministrativeContact.Id))
                            allowedContacts.AddRange(contactTypeList.Where(c => c.ContactCategory == ContactTypeDesignationType.Administrative).Select(a => a.Code));

                        if (userPermissions.Contains(AbsenceSoft.Data.Security.Permission.ViewPersonalContact.Id) && LOAFeatureEnabled)
                            allowedContacts.AddRange(contactTypeList.Where(c => c.ContactCategory == ContactTypeDesignationType.Personal || c.ContactCategory == ContactTypeDesignationType.Self).Select(a => a.Code));

                        if (userPermissions.Contains(AbsenceSoft.Data.Security.Permission.ViewMedicalContact.Id))
                            allowedContacts.AddRange(contactTypeList.Where(c => c.ContactCategory == ContactTypeDesignationType.Medical).Select(a => a.Code));

                        var results = list.Where(ec => allowedContacts.Contains(ec.ContactTypeCode) || ec.CreatedById == AbsenceSoft.Data.Security.User.Current.Id)
                            .Select(e => new
                            {
                                ContactId = e.Id,
                                ContactTypeCode = e.ContactTypeCode,
                                ContactTypeName = e.ContactTypeName,
                                WorkPhone = string.IsNullOrEmpty(e.Contact.WorkPhone) ? string.Empty : e.Contact.WorkPhone,
                                CellPhone = string.IsNullOrEmpty(e.Contact.CellPhone) ? string.Empty : e.Contact.CellPhone,
                                HomePhone = string.IsNullOrEmpty(e.Contact.HomePhone) ? string.Empty : e.Contact.HomePhone,
                                Fax = string.IsNullOrEmpty(e.Contact.Fax) ? string.Empty : e.Contact.Fax,
                                Address = ((e.Contact.Address != null) && (e.Contact.Address.Address1) == string.Empty) ? string.Empty : (e.Contact.Address.Address1) + (!string.IsNullOrEmpty(e.Contact.Address.City) ? ", " + e.Contact.Address.City : string.Empty) + (!string.IsNullOrEmpty(e.Contact.Address.State) ? ", " + e.Contact.Address.State : string.Empty) + (!string.IsNullOrEmpty(e.Contact.Address.PostalCode) ? ", " + e.Contact.Address.PostalCode : string.Empty) + " " + (e.Contact.Address.Country),
                                Email = e.Contact.Email,
                                Company = string.IsNullOrEmpty(e.Contact.CompanyName) ? string.Empty : e.Contact.CompanyName,
                                Name = e.Contact.FirstName + (!string.IsNullOrEmpty(e.Contact.LastName) ? " " + e.Contact.LastName : string.Empty),
                                Title = string.IsNullOrEmpty(e.Contact.Title) ? string.Empty : e.Contact.Title,
                                FirstName = e.Contact.FirstName,
                                LastName = e.Contact.LastName,
                                IsPrimary = e.Contact.IsPrimary,
                                ContactPersonName = e.Contact.ContactPersonName
                            }).ToList();

                        // results.Add(list.Where(p => p.CreatedById == AbsenceSoft.Data.Security.User.Current.Id).ToList());

                        return Json(results, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        //Filter what contacts user can see by user's permission(s)
                        var userPermissions = AbsenceSoft.Data.Security.User.Permissions.GetPermissions(AbsenceSoft.Data.Security.User.Current, Employer.CurrentId);
                        List<string> allowedContacts = new List<string>();

                        if (userPermissions.Contains(AbsenceSoft.Data.Security.Permission.ViewAdministrativeContact.Id))
                            allowedContacts.AddRange(contactTypeList.Where(c => c.ContactCategory == ContactTypeDesignationType.Administrative).Select(a => a.Code));

                        if (userPermissions.Contains(AbsenceSoft.Data.Security.Permission.ViewPersonalContact.Id) && LOAFeatureEnabled)
                            allowedContacts.AddRange(contactTypeList.Where(c => c.ContactCategory == ContactTypeDesignationType.Personal || c.ContactCategory == ContactTypeDesignationType.Self).Select(a => a.Code));

                        if (userPermissions.Contains(AbsenceSoft.Data.Security.Permission.ViewMedicalContact.Id))
                            allowedContacts.AddRange(contactTypeList.Where(c => c.ContactCategory == ContactTypeDesignationType.Medical).Select(a => a.Code));

                        var results = service.GetContacts(employeeId, contactTypeCode).Where(ec => allowedContacts.Contains(ec.ContactTypeCode)).Select(e => new
                        {
                            ContactId = e.Id,
                            ContactTypeCode = e.ContactTypeCode,
                            ContactTypeName = e.ContactTypeName,
                            WorkPhone = string.IsNullOrEmpty(e.Contact.WorkPhone) ? string.Empty : e.Contact.WorkPhone,
                            CellPhone = string.IsNullOrEmpty(e.Contact.CellPhone) ? string.Empty : e.Contact.CellPhone,
                            HomePhone = string.IsNullOrEmpty(e.Contact.HomePhone) ? string.Empty : e.Contact.HomePhone,
                            Fax = string.IsNullOrEmpty(e.Contact.Fax) ? string.Empty : e.Contact.Fax,
                            Address = ((e.Contact.Address != null) && (e.Contact.Address.Address1) == string.Empty) ? string.Empty : (e.Contact.Address.Address1) + (!string.IsNullOrEmpty(e.Contact.Address.City) ? ", " + e.Contact.Address.City : string.Empty) + (!string.IsNullOrEmpty(e.Contact.Address.State) ? ", " + e.Contact.Address.State : string.Empty) + (!string.IsNullOrEmpty(e.Contact.Address.PostalCode) ? ", " + e.Contact.Address.PostalCode : string.Empty) + " " + (e.Contact.Address.Country),
                            Email = e.Contact.Email,
                            Name = e.Contact.FirstName + (!string.IsNullOrEmpty(e.Contact.LastName) ? " " + e.Contact.LastName : string.Empty),
                            Title = string.IsNullOrEmpty(e.Contact.Title) ? string.Empty : e.Contact.Title,
                            Company = string.IsNullOrEmpty(e.Contact.CompanyName) ? string.Empty : e.Contact.CompanyName,
                            FirstName = e.Contact.FirstName,
                            LastName = e.Contact.LastName,
                            IsPrimary = e.Contact.IsPrimary,
                            ContactPersonName = e.Contact.ContactPersonName
                        });

                        return Json(results, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpGet] //, Secure("EditContact", "ViewContact")
        [Route("Employees/{employeeId}/Contacts/List", Name = "EmployeeContactsList")]
        public JsonResult ListForEmployee(string employeeId, string contactTypeCode)
        {
            try
            {
                var contactTypeService = new ContactTypeService();
                var contactTypeList = contactTypeService.GetContactTypes(AbsenceSoft.Data.Security.User.Current.CustomerId, null);

                var emp = Employee.GetById(employeeId);
                if (emp == null) return JsonError(new Exception("Employee not found"), "Employee not found", 500);
                var LOAFeatureEnabled = emp.Employer.HasFeature(Feature.LOA);

                using (var service = new EmployeeService())
                {
                    if (string.IsNullOrEmpty(contactTypeCode))
                    {
                        //Get all contacts for employee
                        var list = service.GetContacts(employeeId);

                        //Filter what contacts user can see by user's permission(s)
                        var userPermissions = AbsenceSoft.Data.Security.User.Permissions.GetPermissions(AbsenceSoft.Data.Security.User.Current, Employer.CurrentId);
                        List<string> allowedContacts = new List<string>();

                        if (userPermissions.Contains(AbsenceSoft.Data.Security.Permission.ViewAdministrativeContact.Id))
                            allowedContacts.AddRange(contactTypeList.Where(c => c.ContactCategory == ContactTypeDesignationType.Administrative).Select(a => a.Code));

                        if (userPermissions.Contains(AbsenceSoft.Data.Security.Permission.ViewPersonalContact.Id) && LOAFeatureEnabled)
                            allowedContacts.AddRange(contactTypeList.Where(c => c.ContactCategory == ContactTypeDesignationType.Personal || c.ContactCategory == ContactTypeDesignationType.Self).Select(a => a.Code));

                        if (userPermissions.Contains(AbsenceSoft.Data.Security.Permission.ViewMedicalContact.Id))
                            allowedContacts.AddRange(contactTypeList.Where(c => c.ContactCategory == ContactTypeDesignationType.Medical).Select(a => a.Code));

                        var results = list.Where(ec => allowedContacts.Contains(ec.ContactTypeCode))
                            .Select(e => new
                            {
                                ContactId = e.Id,
                                ContactTypeCode = e.ContactTypeCode,
                                ContactTypeName = e.ContactTypeName,
                                WorkPhone = string.IsNullOrEmpty(e.Contact.WorkPhone) ? string.Empty : e.Contact.WorkPhone,
                                CellPhone = string.IsNullOrEmpty(e.Contact.CellPhone) ? string.Empty : e.Contact.CellPhone,
                                HomePhone = string.IsNullOrEmpty(e.Contact.HomePhone) ? string.Empty : e.Contact.HomePhone,
                                Fax = string.IsNullOrEmpty(e.Contact.Fax) ? string.Empty : e.Contact.Fax,
                                Address = ((e.Contact.Address != null) && (e.Contact.Address.Address1) == string.Empty) ? string.Empty : (e.Contact.Address.Address1) + (!string.IsNullOrEmpty(e.Contact.Address.City) ? ", " + e.Contact.Address.City : string.Empty) + (!string.IsNullOrEmpty(e.Contact.Address.State) ? ", " + e.Contact.Address.State : string.Empty) + (!string.IsNullOrEmpty(e.Contact.Address.PostalCode) ? ", " + e.Contact.Address.PostalCode : string.Empty) + " " + (e.Contact.Address.Country),
                                Email = e.Contact.Email,
                                Company = string.IsNullOrEmpty(e.Contact.CompanyName) ? string.Empty : e.Contact.CompanyName,
                                Name = e.Contact.FirstName + (!string.IsNullOrEmpty(e.Contact.LastName) ? " " + e.Contact.LastName : string.Empty),
                                Title = string.IsNullOrEmpty(e.Contact.Title) ? string.Empty : e.Contact.Title,
                                FirstName = e.Contact.FirstName,
                                LastName = e.Contact.LastName,
                                IsPrimary = e.Contact.IsPrimary,
                                ContactPersonName = e.Contact.ContactPersonName
                            });

                        return Json(results, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        //Filter what contacts user can see by user's permission(s)
                        var userPermissions = AbsenceSoft.Data.Security.User.Permissions.GetPermissions(AbsenceSoft.Data.Security.User.Current, Employer.CurrentId);
                        List<string> allowedContacts = new List<string>();

                        if (userPermissions.Contains(AbsenceSoft.Data.Security.Permission.ViewAdministrativeContact.Id))
                            allowedContacts.AddRange(contactTypeList.Where(c => c.ContactCategory == ContactTypeDesignationType.Administrative).Select(a => a.Code));

                        if (userPermissions.Contains(AbsenceSoft.Data.Security.Permission.ViewPersonalContact.Id) && LOAFeatureEnabled)
                            allowedContacts.AddRange(contactTypeList.Where(c => c.ContactCategory == ContactTypeDesignationType.Personal || c.ContactCategory == ContactTypeDesignationType.Self).Select(a => a.Code));

                        if (userPermissions.Contains(AbsenceSoft.Data.Security.Permission.ViewMedicalContact.Id))
                            allowedContacts.AddRange(contactTypeList.Where(c => c.ContactCategory == ContactTypeDesignationType.Medical).Select(a => a.Code));

                        var results = service.GetContacts(employeeId, contactTypeCode).Where(ec => allowedContacts.Contains(ec.ContactTypeCode)).Select(e => new
                        {
                            ContactId = e.Id,
                            ContactTypeCode = e.ContactTypeCode,
                            ContactTypeName = e.ContactTypeName,
                            WorkPhone = string.IsNullOrEmpty(e.Contact.WorkPhone) ? string.Empty : e.Contact.WorkPhone,
                            CellPhone = string.IsNullOrEmpty(e.Contact.CellPhone) ? string.Empty : e.Contact.CellPhone,
                            HomePhone = string.IsNullOrEmpty(e.Contact.HomePhone) ? string.Empty : e.Contact.HomePhone,
                            Fax = string.IsNullOrEmpty(e.Contact.Fax) ? string.Empty : e.Contact.Fax,
                            Address = ((e.Contact.Address != null) && (e.Contact.Address.Address1) == string.Empty) ? string.Empty : (e.Contact.Address.Address1) + (!string.IsNullOrEmpty(e.Contact.Address.City) ? ", " + e.Contact.Address.City : string.Empty) + (!string.IsNullOrEmpty(e.Contact.Address.State) ? ", " + e.Contact.Address.State : string.Empty) + (!string.IsNullOrEmpty(e.Contact.Address.PostalCode) ? ", " + e.Contact.Address.PostalCode : string.Empty) + " " + (e.Contact.Address.Country),
                            Email = e.Contact.Email,
                            Name = e.Contact.FirstName + (!string.IsNullOrEmpty(e.Contact.LastName) ? " " + e.Contact.LastName : string.Empty),
                            Title = string.IsNullOrEmpty(e.Contact.Title) ? string.Empty : e.Contact.Title,
                            Company = string.IsNullOrEmpty(e.Contact.CompanyName) ? string.Empty : e.Contact.CompanyName,
                            FirstName = e.Contact.FirstName,
                            LastName = e.Contact.LastName,
                            IsPrimary = e.Contact.IsPrimary,
                            ContactPersonName = e.Contact.ContactPersonName
                        });

                        return Json(results, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpGet, Secure("EditContact")]
        [Route("Employees/{employeeId}/Case/{caseId}/Contacts/QuickFind", Name = "EmployeeContactQuickFind")]
        public JsonResult ContactQuickFind(string employeeId, string caseId, string searchTerm)
        {
            //TODO: re-factor it to fetch correct data
            Employee emp = Employee.GetById(employeeId);
            if (emp == null)
                throw new AbsenceSoftException("Invalid Employee Id");

            ListCriteria criteria = new ListCriteria();
            criteria.Set("text", searchTerm);
            criteria.Set("EmployeeId", employeeId);
            criteria.Set("EmployerId", emp.EmployerId);
            try
            {
                using (var employerService = new EmployerService(CurrentCustomer, CurrentEmployer, CurrentUser))
                using (var service = new EmployeeService())
                using (var caseService = new CaseService())
                {
                    var data = service.ContactQuickFind(criteria);

                    var addEmp = false;
                    var contacts = new List<ContactsQuickFindViewModel>();
                    var employee = new ContactsQuickFindViewModel();
                    employee.text = "Employee";
                    var _employee = service.GetEmployee(employeeId);
                    var _myCase = caseService.GetCaseById(caseId);
                    if (!string.IsNullOrWhiteSpace(_employee.Info.AltEmail) && _employee.Info.AltEmail != _employee.Info.Email)
                    {
                        addEmp = true;
                        employee.AddEmailChild("Pers Email-Preferred", _employee.Info.AltEmail, _employee);
                    }
                    if (!string.IsNullOrWhiteSpace(_employee.Info.Email))
                    {
                        addEmp = true;
                        employee.AddEmailChild("Work Email", _employee.Info.Email, _employee);
                    }                    
                    if (!string.IsNullOrWhiteSpace(_myCase.Employee.Info.AltEmail) && _myCase.Employee.Info.AltEmail != _employee.Info.AltEmail)
                    {
                        addEmp = true;
                        employee.AddEmailChild("Alt Email", _myCase.Employee.Info.AltEmail, _employee);
                    }
                    if (!string.IsNullOrWhiteSpace(_myCase.Employee.Info.SecondaryEmail))
                    {
                        addEmp = true;
                        employee.AddEmailChild("Alt Email", _myCase.Employee.Info.SecondaryEmail, _employee);
                    }
                    if (addEmp)
                        contacts.Add(employee);
                    if (data != null && data.Results != null && data.Results.Any())
                    {
                        var employeeContact = new ContactsQuickFindViewModel("Employee Contacts")
                        {
                            children = data.Results.Select(ec =>
                            new ContactsChildrenQuickFindViewModel()
                            {
                                text = ec.Get<string>("text"),
                                FirstName = ec.Get<string>("firstName"),
                                LastName = ec.Get<string>("lastName"),
                                Email = ec.Get<string>("email"),
                                TypeCode = ec.Get<string>("typeId"),
                                Fax = ec.Get<string>("fax"),
                                id = ec.Get<string>("id"),
                            }).OrderBy(t=>t.TypeCode).ThenBy(e=>e.text).ToList()
                        };
                        contacts.Add(employeeContact);
                    }
                    /// Add in the Employer Contacts
                    criteria.PageSize = int.MaxValue;
                    criteria.Set("ExcludeInactive", true);
                    var employerContacts = employerService.ListEmployerContacts(criteria);
                    if (employerContacts != null && employerContacts.Results != null && employerContacts.Results.Any())
                    {
                        var employerContact = new ContactsQuickFindViewModel("Employer Contacts")
                        {
                            children = employerContacts.Results.Select(ec =>
                            new ContactsChildrenQuickFindViewModel()
                            {
                                text = ec.Get<string>("text"),
                                FirstName = ec.Get<string>("FirstName"),
                                LastName = ec.Get<string>("LastName"),
                                Email = ec.Get<string>("Email"),
                                id = ec.Get<string>("Id")
                            }).OrderBy(e=>e.text).ToList()
                        };

                        contacts.Add(employerContact);
                    }
                    return Json(contacts, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error getting employee quick-find list");
            }
        }


        [Title("Contact"), Secure("EditContact")]
        [Route("Contacts/{contactId}/View", Name = "ViewContact")]
        public ViewResult ViewContact(string contactId)
        {
            return View("View");
        }
    }
}