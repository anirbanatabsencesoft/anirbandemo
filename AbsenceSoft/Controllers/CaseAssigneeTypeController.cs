﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Models.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    public class CaseAssigneeTypeController : BaseController
    {
        [HttpGet, Route("CaseAssigneeType/List/{employerId?}", Name = "ViewCaseAssigneeTypes")]
        public ActionResult CaseAssigneeTypeList(string employerId)
        {
            return View();
        }

        [HttpPost, Route("CaseAssigneeType/List", Name = "ListCaseAssigneeTypes")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListCaseAssigneeTypes(ListCriteria criteria)
        {
            using (var caseService = new CaseService())
            {
                var assigneeTypes = caseService.ListCaseAssigneeTypes(criteria);
                return PartialView(assigneeTypes);
            }
        }

        [HttpGet, Route("CaseAssigneeType/{customerId}/Edit/{caseAssigneeTypeId?}", Name = "CaseAssigneeTypeEdit")]
        public ActionResult CaseAssigneeTypeEdit(string customerId, string caseAssigneeTypeId = null)
        {
            CaseAssigneeTypeViewModel caseAssigneeTypeViewModel = null;
            if (caseAssigneeTypeId != null)
            {
                CaseAssigneeType caseAssigneeType = CaseAssigneeType.GetById(caseAssigneeTypeId);
                caseAssigneeTypeViewModel = new CaseAssigneeTypeViewModel(caseAssigneeType);
            }
            else
            {
                caseAssigneeTypeViewModel = new CaseAssigneeTypeViewModel();
            }

            return View(caseAssigneeTypeViewModel);
        }

        [HttpPost, Route("CaseAssigneeType/{customerId}/Save", Name = "CaseAssigneeTypeSave")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveCaseAssigneeType(string customerId, CaseAssigneeTypeViewModel caseAssigneeTypeViewModel)
        {
            if (!ModelState.IsValid)
                return PartialView(caseAssigneeTypeViewModel);

            CaseAssigneeType caseAssigneeType = null;
            if (!string.IsNullOrEmpty(caseAssigneeTypeViewModel.Id))
            {
                if (CaseAssigneeType.AsQueryable().Where(c => c.Id != caseAssigneeTypeViewModel.Id && c.Code == caseAssigneeTypeViewModel.Code).Any())
                {
                    ModelState.AddModelError(string.Empty, "Case Assignee Type Code is already existing");
                    if (Request.IsAjaxRequest())
                        return PartialView("CaseAssigneeTypeEdit", caseAssigneeTypeViewModel);

                    return View("CaseAssigneeTypeEdit", caseAssigneeTypeViewModel);
                }
                else
                {
                    caseAssigneeType = CaseAssigneeType.GetById(caseAssigneeTypeViewModel.Id);
                }
            }
            else
            {
                if (CaseAssigneeType.GetByCode(caseAssigneeTypeViewModel.Code, customerId) != null)
                {
                    ModelState.AddModelError(string.Empty, "Case Assignee Type Code is already existing");
                    if (Request.IsAjaxRequest())
                        return PartialView("CaseAssigneeTypeEdit", caseAssigneeTypeViewModel);

                    return View("CaseAssigneeTypeEdit", caseAssigneeTypeViewModel);
                }
                else
                {
                    caseAssigneeType = new CaseAssigneeType();
                }
            }

            caseAssigneeType = caseAssigneeTypeViewModel.ApplyToDataModel(caseAssigneeType);
            caseAssigneeType.Save();

            return RedirectViaJavaScript(Url.Action("CaseAssigneeTypeList", new { customerId = customerId }));
        }

        [HttpDelete, Route("CaseAssigneeType/{customerId}/Delete/{caseAssigneeTypeId}", Name = "DeleteCaseAssigneeType")]
        public ActionResult DeleteCaseAssigneeType(string customerId, string caseAssigneeTypeId)
        {
            try
            {
                CaseAssigneeType caseAssigneeType = CaseAssigneeType.GetById(caseAssigneeTypeId);
                caseAssigneeType.Delete();
                return RedirectViaJavaScript(Url.Action("CaseAssigneeTypeList", new { customerId = Customer.Current.Id }));
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to delete Case Assignee Type");
            }
        }
    }
}