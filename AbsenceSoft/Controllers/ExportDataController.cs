﻿using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Models;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Cases;
using System.IO;
using CsvHelper;
using Ionic.Zip;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Models.Notes;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Common;
using AbsenceSoft.Data.DataExport;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    public class ExportDataController : BaseController
    {
        //
        // GET: /ExportData/
        [Secure("ExportData"), HttpGet]
        [Route("ExportData/DoExport", Name = "ExportData")]
        public ActionResult Index()
        {
            return View();
        }

        [Secure("ExportData"), HttpPost]
        [Route("ExportData/{employerId}/DoExport", Name = "CreateExportData")]
        [ValidateApiAntiForgeryToken]
        public JsonResult DoExport(string employerId)
        {
            employerId = employerId ?? Employer.CurrentId;
            using (var export = new ExportService()) 
            {
                var exportEntity = export.CreateExportRecord(CurrentUser, employerId);
                export.Update(exportEntity);

                DateTime startTime = DateTime.Now;

                try
                {
                    exportEntity.Status = ExportStatus.Processing;
                    export.Update(exportEntity);

                    using (var emaployeeService = new EmployeeService())
                    {
                        using (var caseService = new CaseService())
                        {
                            using (var communicationService = new CommunicationService())
                            {
                                using (var attachmentService = new AttachmentService())
                                {
                                    var communicationListResult = communicationService.ExportCommunication(employerId);
                                    var caseListResult = caseService.ExportCaseList(employerId);
                                    var employeeListResults = emaployeeService.ExportEmployeeList(employerId);

                                    var attachmentListResult = attachmentService.ExportAttachments(employerId);

                                    string pathUptoCsvDirectory = Path.Combine(System.IO.Path.GetTempPath(), "AbsenceSoft", "temp");
                                    if (Directory.Exists(pathUptoCsvDirectory))
                                    {
                                        Directory.Delete(pathUptoCsvDirectory, true);
                                    }

                                    string csvSaveDirectoryPath = Path.Combine(pathUptoCsvDirectory, exportEntity.Id);
                                    if (!Directory.Exists(csvSaveDirectoryPath))
                                    {
                                        Directory.CreateDirectory(csvSaveDirectoryPath);
                                    }
                                    else
                                    {
                                        Directory.Delete(csvSaveDirectoryPath, true);
                                        Directory.CreateDirectory(csvSaveDirectoryPath);
                                    }

                                    string zipSaveDirectoryPath = Path.Combine(System.IO.Path.GetTempPath(), "AbsenceSoft", "tempZip");
                                    if (!Directory.Exists(zipSaveDirectoryPath))
                                    {
                                        Directory.CreateDirectory(zipSaveDirectoryPath);
                                    }
                                    else
                                    {
                                        Directory.Delete(zipSaveDirectoryPath, true);
                                        Directory.CreateDirectory(zipSaveDirectoryPath);
                                    }

                                    string zipSavefilePath = Path.Combine(zipSaveDirectoryPath, exportEntity.Id + ".zip");

                                    #region employee_list.csv

                                    try
                                    {
                                        string[] employeeListData = new string[26];
                                        employeeListData[0] = "EmployeeNumber";
                                        employeeListData[1] = "FirstName";
                                        employeeListData[2] = "LastName";
                                        employeeListData[3] = "Gender";
                                        employeeListData[4] = "Ssn";
                                        employeeListData[5] = "Status";
                                        employeeListData[6] = "PriorHoursWorked";
                                        employeeListData[7] = "ServiceDate";
                                        employeeListData[8] = "HireDate";
                                        employeeListData[9] = "WorkState";
                                        employeeListData[10] = "Meets50In75MileRule";
                                        employeeListData[11] = "IsKeyEmployee";
                                        employeeListData[12] = "IsExempt";
                                        employeeListData[13] = "MilitaryStatus";
                                        employeeListData[14] = "SupervisorEmployee";
                                        employeeListData[15] = "Email";
                                        employeeListData[16] = "Address1";
                                        employeeListData[17] = "Address2";
                                        employeeListData[18] = "City";
                                        employeeListData[19] = "State";
                                        employeeListData[20] = "PostalCode";
                                        employeeListData[21] = "Country";
                                        employeeListData[22] = "CreatedBy";
                                        employeeListData[23] = "CreatedDate";
                                        employeeListData[24] = "ModifiedBy";
                                        employeeListData[25] = "ModifiedDate";

                                        export.DoExport(Path.Combine(csvSaveDirectoryPath, "employee_master.csv"), employeeListData);

                                        foreach (Employee employee in employeeListResults)
                                        {
                                            employeeListData[0] = Convert.ToString(employee.EmployeeNumber);
                                            employeeListData[1] = Convert.ToString(employee.FirstName);
                                            employeeListData[2] = Convert.ToString(employee.LastName);
                                            employeeListData[3] = Convert.ToString(employee.Gender);

                                            if (employee.Ssn != null)
                                                employeeListData[4] = Convert.ToString(employee.Ssn.PlainText);
                                            else
                                                employeeListData[4] = string.Empty;

                                            employeeListData[5] = Convert.ToString(employee.Status);
                                            employeeListData[6] = Convert.ToString((employee.PriorHours ?? new List<PriorHours>(0)).OrderByDescending(p => p.AsOf).Take(1).Sum(p => p.HoursWorked));
                                            employeeListData[7] = Convert.ToString(employee.ServiceDate);
                                            employeeListData[8] = Convert.ToString(employee.HireDate);
                                            employeeListData[9] = Convert.ToString(employee.WorkState);
                                            employeeListData[10] = Convert.ToString(employee.Meets50In75MileRule);
                                            employeeListData[11] = Convert.ToString(employee.IsKeyEmployee);
                                            employeeListData[12] = Convert.ToString(employee.IsExempt);
                                            employeeListData[13] = Convert.ToString(employee.MilitaryStatus);
                                            var ct = EmployeeContact.AsQueryable().FirstOrDefault(e => e.EmployeeId == employee.Id && e.ContactTypeCode == "SUPERVISOR" && e.RelatedEmployeeNumber != null);
                                            employeeListData[14] = Convert.ToString(ct == null || ct.RelatedEmployee == null ? "" : ct.RelatedEmployee.EmployeeNumber);
                                            employeeListData[15] = Convert.ToString(employee.Info.Email);
                                            employeeListData[16] = Convert.ToString(employee.Info.Address.Address1);
                                            employeeListData[17] = Convert.ToString(employee.Info.Address.Address2);
                                            employeeListData[18] = Convert.ToString(employee.Info.Address.City);
                                            employeeListData[19] = Convert.ToString(employee.Info.Address.State);
                                            employeeListData[20] = Convert.ToString(employee.Info.Address.PostalCode);
                                            employeeListData[21] = Convert.ToString(employee.Info.Address.Country);
                                            employeeListData[22] = Convert.ToString(employee.CreatedBy);
                                            employeeListData[23] = Convert.ToString(employee.CreatedDate);
                                            employeeListData[24] = Convert.ToString(employee.ModifiedBy);
                                            employeeListData[25] = Convert.ToString(employee.ModifiedDate);

                                            export.DoExport(Path.Combine(csvSaveDirectoryPath, "employee_master.csv"), employeeListData);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Directory.Delete(csvSaveDirectoryPath, true);
                                        Log.Error("Error when export employee_master.csv due to :" + ex.Message, ex);
                                        exportEntity.Status = ExportStatus.Failed;
                                        exportEntity.Message = "Error when export employee_master.csv due to :" + ex.Message;
                                        export.Update(exportEntity);
                                        throw new ArgumentException("Error while exporting employee list data due to :" + ex.Message);
                                    }

                                    #endregion

                                    #region employee_schedules.csv

                                    try
                                    {
                                        string[] scheduleData = new string[6];
                                        scheduleData[0] = "EmployeeNumber";
                                        scheduleData[1] = "ScheduleType";
                                        scheduleData[2] = "Times";
                                        scheduleData[3] = "StartDate";
                                        scheduleData[4] = "EndDate";
                                        scheduleData[5] = "TextRepresentation";

                                        export.DoExport(Path.Combine(csvSaveDirectoryPath, "employee_schedule.csv"), scheduleData);

                                        foreach (Employee employee in employeeListResults)
                                        {
                                            foreach (Schedule sch in employee.WorkSchedules)
                                            {
                                                string timeString = string.Empty;

                                                foreach (Time time in sch.Times)
                                                {
                                                    timeString += Convert.ToString(time.TotalMinutes) + "|" + Convert.ToString(time.SampleDate) + ";";
                                                }

                                                scheduleData[0] = Convert.ToString(employee.EmployeeNumber);
                                                scheduleData[1] = Convert.ToString(sch.ScheduleType);
                                                scheduleData[2] = Convert.ToString(timeString);
                                                scheduleData[3] = Convert.ToString(sch.StartDate);
                                                scheduleData[4] = Convert.ToString(sch.EndDate);
                                                scheduleData[5] = sch.ToString();

                                                export.DoExport(Path.Combine(csvSaveDirectoryPath, "employee_schedule.csv"), scheduleData);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Directory.Delete(csvSaveDirectoryPath, true);
                                        Log.Error("Error when export employee_schedule.csv due to :" + ex.Message, ex);
                                        exportEntity.Status = ExportStatus.Failed;
                                        exportEntity.Message = "Error when export employee_schedule.csv due to :" + ex.Message;
                                        export.Update(exportEntity);
                                        throw new ArgumentException("Error while exporting employee schedule data due to :" + ex.Message);
                                    }

                                    #endregion

                                    #region employee_notes.csv

                                    try
                                    {
                                        string[] noteData = new string[5];
                                        noteData[0] = "EmployeeNumber";
                                        noteData[1] = "CreatedBy";
                                        noteData[2] = "CreatedDate";
                                        noteData[3] = "Category";
                                        noteData[4] = "Notes";

                                        export.DoExport(Path.Combine(csvSaveDirectoryPath, "employee_notes.csv"), noteData);

                                        var notes = EmployeeNote.AsQueryable(AbsenceSoft.Data.Security.User.Current, Permission.ExportData).Where(o => o.EmployerId == employerId).ToList();
                                        var n = notes.Select(
                                        o => new NoteModel()
                                        {
                                            EmployeeNumber = Convert.ToString(o.Employee.EmployeeNumber),
                                            Notes = Convert.ToString(o.Notes),
                                            CreatedDate = Convert.ToString(o.CreatedDate),
                                            Category = o.Category.HasValue ? (int)o.Category.Value : new int?(),
                                            CategoryText = o.Category.HasValue ? o.Category.Value.ToString().SplitCamelCaseString() : null,
                                            CreatedBy = Convert.ToString(o.CreatedBy)

                                        }).OrderByDescending(o => o.CreatedDate).ToList();

                                        foreach (NoteModel resultItem in n)
                                        {
                                            noteData[0] = Convert.ToString(resultItem.EmployeeNumber);
                                            noteData[1] = Convert.ToString(resultItem.CreatedBy);
                                            noteData[2] = Convert.ToString(resultItem.CreatedDate);
                                            noteData[3] = Convert.ToString(resultItem.CategoryText);
                                            noteData[4] = Convert.ToString(resultItem.Notes);

                                            export.DoExport(Path.Combine(csvSaveDirectoryPath, "employee_notes.csv"), noteData);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Directory.Delete(csvSaveDirectoryPath, true);
                                        Log.Error("Error when export employee_notes.csv due to :" + ex.Message, ex);
                                        exportEntity.Status = ExportStatus.Failed;
                                        exportEntity.Message = "Error when export employee_notes.csv due to :" + ex.Message;
                                        export.Update(exportEntity);
                                        throw new ArgumentException("Error while exporting employee note data due to :" + ex.Message);
                                    }

                                    #endregion

                                    #region cases_notes.csv

                                    try
                                    {
                                        string[] caseNoteData = new string[6];

                                        caseNoteData[0] = "EmployeeNumber";
                                        caseNoteData[1] = "CaseNumber";
                                        caseNoteData[2] = "CreatedBy";
                                        caseNoteData[3] = "CreatedDate";
                                        caseNoteData[4] = "Category";
                                        caseNoteData[5] = "Notes";

                                        export.DoExport(Path.Combine(csvSaveDirectoryPath, "cases_notes.csv"), caseNoteData);
                                        var casesNotes = CaseNote.AsQueryable().ToList(o => o.Case.Employer.Id == employerId).Select(
                                        o => new NoteModel()
                                        {
                                            EmployeeNumber = Convert.ToString(o.Case.Employee.EmployeeNumber),
                                            CaseNumber = Convert.ToString(o.Case.CaseNumber),
                                            Notes = Convert.ToString(o.Notes),
                                            CreatedDate = Convert.ToString(o.CreatedDate),
                                            Category = o.Category.HasValue ? (int)o.Category.Value : new int?(),
                                            CategoryText = o.Category.HasValue ? o.Category.Value.ToString().SplitCamelCaseString() : null,
                                            CreatedBy = Convert.ToString((o.CreatedBy ?? AbsenceSoft.Data.Security.User.System).Email)

                                        }).OrderByDescending(o => o.CreatedDate);

                                        foreach (NoteModel resultItem in casesNotes)
                                        {
                                            caseNoteData[0] = Convert.ToString(resultItem.EmployeeNumber);
                                            caseNoteData[1] = Convert.ToString(resultItem.CaseNumber);
                                            caseNoteData[2] = Convert.ToString(resultItem.CreatedBy);
                                            caseNoteData[3] = Convert.ToString(resultItem.CreatedDate);
                                            caseNoteData[4] = Convert.ToString(resultItem.CategoryText);
                                            caseNoteData[5] = Convert.ToString(resultItem.Notes);

                                            export.DoExport(Path.Combine(csvSaveDirectoryPath, "cases_notes.csv"), caseNoteData);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Directory.Delete(csvSaveDirectoryPath, true);
                                        Log.Error("Error when export case_note.csv due to :" + ex.Message, ex);
                                        exportEntity.Status = ExportStatus.Failed;
                                        exportEntity.Message = "Error when export case_note.csv due to :" + ex.Message;
                                        export.Update(exportEntity);
                                        throw new ArgumentException("Error while exporting case note data due to :" + ex.Message);
                                    }

                                    #endregion

                                    #region case_master.csv

                                    try
                                    {
                                        string[] caseListData = new string[43];
                                        caseListData[0] = "CaseNumber";
                                        caseListData[1] = "EmployeeNumber";
                                        caseListData[2] = "IsWorkRelated";
                                        caseListData[3] = "IsIntermittent";
                                        caseListData[4] = "StartDate";
                                        caseListData[5] = "EndDate";
                                        caseListData[6] = "SpouseCaseNumber";
                                        caseListData[7] = "CaseEventType";
                                        caseListData[8] = "FirstName";
                                        caseListData[9] = "LastName";
                                        caseListData[10] = "Gender";
                                        caseListData[11] = "Ssn";
                                        caseListData[12] = "Status";
                                        caseListData[13] = "PriorHoursWorked";
                                        caseListData[14] = "ServiceDate";
                                        caseListData[15] = "HireDate";
                                        caseListData[16] = "WorkState";
                                        caseListData[17] = "Meets50In75MileRule";
                                        caseListData[18] = "IsKeyEmployee";
                                        caseListData[19] = "IsExempt";
                                        caseListData[20] = "MilitaryStatus";
                                        caseListData[21] = "Email";
                                        caseListData[22] = "Address1";
                                        caseListData[23] = "Address2";
                                        caseListData[24] = "City";
                                        caseListData[25] = "State";
                                        caseListData[26] = "PostalCode";
                                        caseListData[27] = "Country";
                                        caseListData[28] = "Status";
                                        caseListData[29] = "AssignedToEmail";
                                        caseListData[30] = "ClosureReason";
                                        caseListData[31] = "Description";
                                        caseListData[32] = "Narrative";
                                        caseListData[33] = "Reason Category";
                                        caseListData[34] = "Reason Code";
                                        caseListData[35] = "Reason Name";
                                        caseListData[36] = "Reason Help Text";
                                        caseListData[37] = "Reason Metadata";
                                        caseListData[38] = "CreatedDate";
                                        caseListData[39] = "CreatedBy";
                                        caseListData[40] = "ModifiedDate";
                                        caseListData[41] = "ModifiedBy";

                                        export.DoExport(Path.Combine(csvSaveDirectoryPath, "case_master.csv"), caseListData);

                                        foreach (Case cAse in caseListResult)
                                        {
                                            caseListData[0] = Convert.ToString(cAse.CaseNumber);
                                            caseListData[1] = Convert.ToString(cAse.Employee.EmployeeNumber);
                                            caseListData[2] = Convert.ToString(cAse.Metadata.GetRawValue<bool>("IsWorkRelated"));
                                            caseListData[3] = Convert.ToString(cAse.IsIntermittent);
                                            caseListData[4] = Convert.ToString(cAse.StartDate);
                                            caseListData[5] = Convert.ToString(cAse.EndDate);

                                            if (cAse.SpouseCase != null)
                                                caseListData[6] = Convert.ToString(cAse.SpouseCase.CaseNumber);
                                            else
                                                caseListData[6] = string.Empty;

                                            if (cAse.CaseEvents.Count != 0)
                                            {
                                                foreach (CaseEvent ce in cAse.CaseEvents)
                                                {
                                                    caseListData[7] = Convert.ToString(ce.EventType);
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                caseListData[7] = string.Empty;
                                            }

                                            caseListData[8] = Convert.ToString(cAse.Employee.FirstName);
                                            caseListData[9] = Convert.ToString(cAse.Employee.LastName);
                                            caseListData[10] = Convert.ToString(cAse.Employee.Gender);

                                            if(cAse.Employee.Ssn != null)
                                            {
                                                caseListData[11] = Convert.ToString(cAse.Employee.Ssn.PlainText);
                                            }
                                            else
                                            {
                                                caseListData[11] = Convert.ToString(cAse.Employee.Ssn);
                                            }
                                            
                                            caseListData[12] = Convert.ToString(cAse.Employee.Status);
                                            caseListData[13] = Convert.ToString((cAse.Employee.PriorHours ?? new List<PriorHours>(0)).OrderByDescending(p => p.AsOf).Take(1).Sum(p => p.HoursWorked));
                                            caseListData[14] = Convert.ToString(cAse.Employee.ServiceDate);
                                            caseListData[15] = Convert.ToString(cAse.Employee.HireDate);
                                            caseListData[16] = Convert.ToString(cAse.Employee.WorkState);
                                            caseListData[17] = Convert.ToString(cAse.Employee.Meets50In75MileRule);
                                            caseListData[18] = Convert.ToString(cAse.Employee.IsKeyEmployee);
                                            caseListData[19] = Convert.ToString(cAse.Employee.IsExempt);
                                            caseListData[20] = Convert.ToString(cAse.Employee.MilitaryStatus);
                                            caseListData[21] = Convert.ToString(cAse.Employee.Info.Email);
                                            caseListData[22] = Convert.ToString(cAse.Employee.Info.Address.Address1);
                                            caseListData[23] = Convert.ToString(cAse.Employee.Info.Address.Address2);
                                            caseListData[24] = Convert.ToString(cAse.Employee.Info.Address.City);
                                            caseListData[25] = Convert.ToString(cAse.Employee.Info.Address.State);
                                            caseListData[26] = Convert.ToString(cAse.Employee.Info.Address.PostalCode);
                                            caseListData[27] = Convert.ToString(cAse.Employee.Info.Address.Country);
                                            caseListData[28] = Convert.ToString(cAse.Status);
                                            caseListData[29] = Convert.ToString(cAse.AssignedTo.Email);
                                            caseListData[30] = Convert.ToString(cAse.ClosureReason);
                                            caseListData[31] = Convert.ToString(cAse.Description);
                                            caseListData[32] = Convert.ToString(cAse.Narrative);
                                            caseListData[33] = Convert.ToString(cAse.Reason.Category);
                                            caseListData[34] = Convert.ToString(cAse.Reason.Code);
                                            caseListData[35] = Convert.ToString(cAse.Reason.Name);
                                            caseListData[36] = Convert.ToString(cAse.Reason.HelpText);
                                            caseListData[37] = string.Empty;
                                            caseListData[38] = Convert.ToString(cAse.CreatedDate);
                                            caseListData[39] = Convert.ToString(cAse.CreatedBy);
                                            caseListData[40] = Convert.ToString(cAse.ModifiedDate);
                                            caseListData[41] = Convert.ToString(cAse.ModifiedBy);

                                            export.DoExport(Path.Combine(csvSaveDirectoryPath, "case_master.csv"), caseListData);

                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Directory.Delete(csvSaveDirectoryPath, true);
                                        Log.Error("Error when export case_master.csv due to :" + ex.Message, ex);
                                        exportEntity.Status = ExportStatus.Failed;
                                        exportEntity.Message = "Error when export case_master.csv due to :" + ex.Message;
                                        export.Update(exportEntity);
                                        throw new ArgumentException("Error while exporting case list data due to :" + ex.Message);
                                    }


                                    #endregion

                                    #region case_detail.csv

                                    try
                                    {
                                        var PolicyData = caseListResult.Select(m => new ExportPolicyData()
                                        {
                                            Segments = m.Segments,
                                            CaseNumber = m.CaseNumber,
                                            CaseType = m.CaseType,
                                            EmployeeNumber = m.Employee.EmployeeNumber

                                        }).ToList();

                                        string[] policyData = new string[9];
                                        policyData[0] = "CaseNumber";
                                        policyData[1] = "EmployeeNumber";
                                        policyData[2] = "CaseType";
                                        policyData[3] = "PolicyName";
                                        policyData[4] = "UsageDate";
                                        policyData[5] = "EligibilityStatus";
                                        policyData[6] = "AdjudicationStatus";
                                        policyData[7] = "DenialReason";
                                        policyData[8] = "HoursUsed";

                                        export.DoExport(Path.Combine(csvSaveDirectoryPath, "case_detail.csv"), policyData);

                                        foreach (ExportPolicyData resultItem in PolicyData)
                                        {
                                            foreach (CaseSegment seg in resultItem.Segments)
                                            {
                                                foreach (AppliedPolicy ap in seg.AppliedPolicies)
                                                {
                                                    if (resultItem.CaseType == CaseType.Intermittent.ToString())
                                                    {
                                                        if (ap.Usage.Count != 0)
                                                        {
                                                            for (int i = 0; i < ap.Usage.Count; i++)
                                                            {
                                                                policyData[0] = Convert.ToString(resultItem.CaseNumber);
                                                                policyData[1] = Convert.ToString(resultItem.EmployeeNumber);
                                                                policyData[2] = Convert.ToString(resultItem.CaseType);
                                                                policyData[3] = Convert.ToString(ap.Policy.Name);
                                                                policyData[4] = Convert.ToString(ap.Usage[i].DateUsed);
                                                                policyData[5] = Convert.ToString(ap.Status);
                                                                policyData[6] = Convert.ToString(ap.Usage[i].Determination);
                                                                if (ap.Usage[i].DenialReasonCode == AdjudicationDenialReason.Other)
                                                                {
                                                                    policyData[7] = Convert.ToString(ap.Usage[i].DenialExplanation);
                                                                }
                                                                else
                                                                {
                                                                    policyData[7] = Convert.ToString(ap.Usage[i].DenialReasonName);
                                                                }
                                                                policyData[8] = Convert.ToString((ap.Usage[i].MinutesUsed / (double)60));

                                                                export.DoExport(Path.Combine(csvSaveDirectoryPath, "case_detail.csv"), policyData);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            policyData[0] = Convert.ToString(resultItem.CaseNumber);
                                                            policyData[1] = Convert.ToString(resultItem.EmployeeNumber);
                                                            policyData[2] = Convert.ToString(resultItem.CaseType);
                                                            policyData[3] = Convert.ToString(ap.Policy.Name);
                                                            policyData[4] = string.Empty;
                                                            policyData[5] = Convert.ToString(ap.Status);
                                                            policyData[6] = string.Empty;
                                                            policyData[7] = string.Empty;
                                                            policyData[8] = string.Empty;

                                                            export.DoExport(Path.Combine(csvSaveDirectoryPath, "case_detail.csv"), policyData);
                                                        }
                                                    }
                                                    else if (resultItem.CaseType == CaseType.Consecutive.ToString() || resultItem.CaseType == CaseType.Reduced.ToString())
                                                    {
                                                        if (seg.Absences.Count != 0)
                                                        {
                                                            foreach (Time tm in seg.Absences)
                                                            {
                                                                    policyData[0] = Convert.ToString(resultItem.CaseNumber);
                                                                    policyData[1] = Convert.ToString(resultItem.EmployeeNumber);
                                                                    policyData[2] = Convert.ToString(resultItem.CaseType);
                                                                    policyData[3] = Convert.ToString(ap.Policy.Name);
                                                                    policyData[4] = Convert.ToString(tm.SampleDate);
                                                                    policyData[5] = Convert.ToString(ap.Status);
                                                                
                                                                    if (ap.Usage.Count != 0)
                                                                    {
                                                                        for (int i = 0; i < ap.Usage.Count; i++)
                                                                        {
                                                                            policyData[6] = Convert.ToString(ap.Usage[i].Determination);
                                                                            if (ap.Usage[i].DenialReasonCode == AdjudicationDenialReason.Other)
                                                                            {
                                                                                policyData[7] = Convert.ToString(ap.Usage[i].DenialExplanation);
                                                                            }
                                                                            else
                                                                            {
                                                                                policyData[7] = Convert.ToString(ap.Usage[i].DenialReasonName);
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        policyData[6] = string.Empty;
                                                                        policyData[7] = string.Empty;
                                                                    }

                                                                    policyData[8] = Convert.ToString((tm.TotalMinutes / (double)60));

                                                                    export.DoExport(Path.Combine(csvSaveDirectoryPath, "case_detail.csv"), policyData);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            policyData[0] = Convert.ToString(resultItem.CaseNumber);
                                                            policyData[1] = Convert.ToString(resultItem.EmployeeNumber);
                                                            policyData[2] = Convert.ToString(resultItem.CaseType);
                                                            policyData[3] = Convert.ToString(ap.Policy.Name);
                                                            policyData[4] = string.Empty;
                                                            policyData[5] = Convert.ToString(ap.Status);
                                                            policyData[6] = string.Empty;
                                                            policyData[7] = string.Empty;
                                                            policyData[8] = string.Empty;
                                                            export.DoExport(Path.Combine(csvSaveDirectoryPath, "case_detail.csv"), policyData);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Directory.Delete(csvSaveDirectoryPath, true);
                                        Log.Error("Error when export case_detail.csv due to :" + ex.Message, ex);
                                        exportEntity.Status = ExportStatus.Failed;
                                        exportEntity.Message = "Error when export case_detail.csv due to :" + ex.Message;
                                        export.Update(exportEntity);
                                        throw new ArgumentException("Error while exporting case detail data due to :" + ex.Message);
                                    }

                                    #endregion

                                    #region communication.csv

                                    try
                                    {
                                        string[] communicationData = new string[12];
                                        communicationData[0] = "CommunicationId";
                                        communicationData[1] = "CaseNumber";
                                        communicationData[2] = "EmployeeNumber";
                                        communicationData[3] = "Template";
                                        communicationData[4] = "Name";
                                        communicationData[5] = "Subject";
                                        communicationData[6] = "Body";
                                        communicationData[7] = "CommunicationType";
                                        communicationData[8] = "Recipients";
                                        communicationData[9] = "CCRecipients";
                                        communicationData[10] = "PaperWorks";
                                        communicationData[11] = "SentDate";

                                        export.DoExport(Path.Combine(csvSaveDirectoryPath, "case_communication.csv"), communicationData);

                                        foreach (Communication com in communicationListResult)
                                        {
                                            communicationData[0] = Convert.ToString(com.Id);
                                            communicationData[1] = Convert.ToString(com.Case.CaseNumber);
                                            communicationData[2] = Convert.ToString(com.Employee.EmployeeNumber);

                                            string templateKey = com.Template;
                                            Template template = Template.GetByCode(templateKey, com.CustomerId, com.EmployerId);
                                            if (template != null)
                                                communicationData[3] = Convert.ToString(template.Name);
                                            else
                                                communicationData[3] = string.Empty;

                                            communicationData[4] = Convert.ToString(com.Name);
                                            communicationData[5] = Convert.ToString(com.Subject);
                                            communicationData[6] = Convert.ToString(com.Body);
                                            communicationData[7] = Convert.ToString(com.CommunicationType);
                                            if (com.Recipients.Count != 0)
                                            {
                                                communicationData[8] = string.Empty;
                                                if (com.Recipients.Count > 1)
                                                {
                                                    foreach (Contact cont in com.Recipients)
                                                    {
                                                        communicationData[8] += Convert.ToString(cont.Title) + "|" + Convert.ToString(cont.FirstName) + "|" + Convert.ToString(cont.MiddleName) + "|" + Convert.ToString(cont.LastName) + "|" + Convert.ToString(cont.Email) + "|" + Convert.ToString(cont.CellPhone) + "|" + Convert.ToString(cont.WorkPhone) + "|" + Convert.ToString(cont.HomePhone) + "|" + Convert.ToString(cont.Fax) + "|" + Convert.ToString(cont.Address.Address1) + "|" + Convert.ToString(cont.Address.Address2) + "|" + Convert.ToString(cont.Address.City) + "|" + Convert.ToString(cont.Address.State) + "|" + Convert.ToString(cont.Address.PostalCode) + "|" + Convert.ToString(cont.Address.Country) + ";";
                                                    }
                                                }
                                                else
                                                {
                                                    foreach (Contact cont in com.Recipients)
                                                    {
                                                        communicationData[8] = Convert.ToString(cont.Title) + "|" + Convert.ToString(cont.FirstName) + "|" + Convert.ToString(cont.MiddleName) + "|" + Convert.ToString(cont.LastName) + "|" + Convert.ToString(cont.Email) + "|" + Convert.ToString(cont.CellPhone) + "|" + Convert.ToString(cont.WorkPhone) + "|" + Convert.ToString(cont.HomePhone) + "|" + Convert.ToString(cont.Fax) + "|" + Convert.ToString(cont.Address.Address1) + "|" + Convert.ToString(cont.Address.Address2) + "|" + Convert.ToString(cont.Address.City) + "|" + Convert.ToString(cont.Address.State) + "|" + Convert.ToString(cont.Address.PostalCode) + "|" + Convert.ToString(cont.Address.Country) + ";";
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                communicationData[8] = string.Empty;
                                            }

                                            if (com.CCRecipients.Count != 0)
                                            {
                                                communicationData[9] = string.Empty;
                                                if (com.Recipients.Count > 1)
                                                {
                                                    foreach (Contact ccont in com.CCRecipients)
                                                    {
                                                        communicationData[9] += Convert.ToString(ccont.Title) + "|" + Convert.ToString(ccont.FirstName) + "|" + Convert.ToString(ccont.MiddleName) + "|" + Convert.ToString(ccont.LastName) + "|" + Convert.ToString(ccont.Email) + "|" + Convert.ToString(ccont.CellPhone) + "|" + Convert.ToString(ccont.WorkPhone) + "|" + Convert.ToString(ccont.HomePhone) + "|" + Convert.ToString(ccont.Fax) + "|" + Convert.ToString(ccont.Address.Address1) + Convert.ToString(ccont.Address.Address2) + Convert.ToString(ccont.Address.City) + Convert.ToString(ccont.Address.State) + Convert.ToString(ccont.Address.PostalCode) + Convert.ToString(ccont.Address.Country) + ";";
                                                    }
                                                }
                                                else
                                                {
                                                    foreach (Contact ccont in com.CCRecipients)
                                                    {
                                                        communicationData[9] = Convert.ToString(ccont.Title) + "|" + Convert.ToString(ccont.FirstName) + "|" + Convert.ToString(ccont.MiddleName) + "|" + Convert.ToString(ccont.LastName) + "|" + Convert.ToString(ccont.Email) + "|" + Convert.ToString(ccont.CellPhone) + "|" + Convert.ToString(ccont.WorkPhone) + "|" + Convert.ToString(ccont.HomePhone) + "|" + Convert.ToString(ccont.Fax) + "|" + Convert.ToString(ccont.Address.Address1) + Convert.ToString(ccont.Address.Address2) + Convert.ToString(ccont.Address.City) + Convert.ToString(ccont.Address.State) + Convert.ToString(ccont.Address.PostalCode) + Convert.ToString(ccont.Address.Country) + ";";
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                communicationData[9] = string.Empty;
                                            }

                                            if (com.Paperwork.Count != 0)
                                            {
                                                communicationData[10] = string.Empty;
                                                foreach (CommunicationPaperwork pw in com.Paperwork)
                                                {
                                                    communicationData[10] += Convert.ToString(pw.Paperwork.Name) + "|" + Convert.ToString(pw.Paperwork.FileName) + "|" + pw.Paperwork.RequiresReview + "|" + Convert.ToString(pw.Paperwork.ReturnDateAdjustment) + "|" + Convert.ToString(pw.Paperwork.CreatedBy) + "|" + Convert.ToString(pw.Paperwork.CreatedDate) + "|" + Convert.ToString(pw.Paperwork.ModifiedBy) + "|" + Convert.ToString(pw.Paperwork.ModifiedDate) + "|" +
                                                                             Convert.ToString(pw.DueDate) + "|" + Convert.ToString(pw.Status) + "|" + Convert.ToString(pw.StatusDate) + "|" +
                                                                             Convert.ToString(pw.Attachment.Id) + "|" + Convert.ToString(pw.Attachment.FileName) + "|" + Convert.ToString(pw.Attachment.AttachmentType) + "|" + Convert.ToString(pw.Attachment.ContentType) + "|" + Convert.ToString(pw.Attachment.Description) + "|" + Convert.ToString(pw.Attachment.CreatedBy) + "|" + Convert.ToString(pw.Attachment.CreatedDate) + "|" + Convert.ToString(pw.Attachment.ModifiedBy) + "|" + Convert.ToString(pw.Attachment.ModifiedDate) + ";";
                                                }
                                            }
                                            else
                                            {
                                                communicationData[10] = string.Empty;
                                            }

                                            communicationData[11] = Convert.ToString(com.SentDate);

                                            export.DoExport(Path.Combine(csvSaveDirectoryPath, "case_communication.csv"), communicationData);
                                        }

                                    }
                                    catch (Exception ex)
                                    {
                                        Directory.Delete(csvSaveDirectoryPath, true);
                                        Log.Error("Error when export communication.csv due to :" + ex.Message, ex);
                                        exportEntity.Status = ExportStatus.Failed;
                                        exportEntity.Message = "Error when export communication.csv due to :" + ex.Message;
                                        export.Update(exportEntity);
                                        throw new ArgumentException("Error while exporting communication data due to :" + ex.Message);
                                    }

                                    #endregion

                                    #region case_certification.csv

                                    try
                                    {
                                        string[] certificationData = new string[10];
                                        certificationData[0] = "CaseNumber";
                                        certificationData[1] = "EmployeeNumber";
                                        certificationData[2] = "StartDate";
                                        certificationData[3] = "EndDate";
                                        certificationData[4] = "Occurances";
                                        certificationData[5] = "Frequency";
                                        certificationData[6] = "FrequencyType";
                                        certificationData[7] = "Duration";
                                        certificationData[8] = "DurationType";
                                        certificationData[9] = "TextRepresentation";

                                        export.DoExport(Path.Combine(csvSaveDirectoryPath, "case_certification.csv"), certificationData);

                                        foreach (Case cas in caseListResult)
                                        {
                                            foreach (Certification cert in cas.Certifications)
                                            {
                                                certificationData[0] = Convert.ToString(cas.CaseNumber);
                                                certificationData[1] = Convert.ToString(cas.Employee.EmployeeNumber);
                                                certificationData[2] = Convert.ToString(cert.StartDate);
                                                certificationData[3] = Convert.ToString(cert.EndDate);
                                                certificationData[4] = Convert.ToString(cert.Occurances);
                                                certificationData[5] = Convert.ToString(cert.Frequency);
                                                certificationData[6] = Convert.ToString(cert.FrequencyType);
                                                certificationData[7] = Convert.ToString(cert.Duration);
                                                certificationData[8] = Convert.ToString(cert.DurationType);
                                                certificationData[9] = cert.ToString();

                                                export.DoExport(Path.Combine(csvSaveDirectoryPath, "case_certification.csv"), certificationData);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Directory.Delete(csvSaveDirectoryPath, true);
                                        Log.Error("Error when export case_certification.csv due to :" + ex.Message, ex);
                                        exportEntity.Status = ExportStatus.Failed;
                                        exportEntity.Message = "Error when export case_certification.csv due to :" + ex.Message;
                                        export.Update(exportEntity);
                                        throw new ArgumentException("Error while exporting certification data due to :" + ex.Message);
                                    }

                                    #endregion

                                    #region case_attachment.csv

                                    try
                                    {
                                        string[] attachmentData = new string[8];
                                        attachmentData[0] = "AttachmentId";
                                        attachmentData[1] = "CustomerNumber";
                                        attachmentData[2] = "EmployeeNumber";
                                        attachmentData[3] = "CaseNumber";
                                        attachmentData[4] = "AttachmentType";
                                        attachmentData[5] = "ContentType";
                                        attachmentData[6] = "ContentLength";
                                        attachmentData[7] = "Description";

                                        export.DoExport(Path.Combine(csvSaveDirectoryPath, "case_attachments.csv"), attachmentData);

                                        foreach (Attachment atmt in attachmentListResult)
                                        {
                                            attachmentData[0] = Convert.ToString(atmt.Id);
                                            attachmentData[1] = "";
                                            attachmentData[2] = Convert.ToString(atmt.Employee.EmployeeNumber);
                                            attachmentData[3] = Convert.ToString(atmt.Case.CaseNumber);
                                            attachmentData[4] = Convert.ToString(atmt.AttachmentType);
                                            attachmentData[5] = Convert.ToString(atmt.ContentType);
                                            attachmentData[6] = Convert.ToString(atmt.ContentLength);
                                            attachmentData[7] = Convert.ToString(atmt.Description);


                                            export.DoExport(Path.Combine(csvSaveDirectoryPath, "case_attachments.csv"), attachmentData);

                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Directory.Delete(csvSaveDirectoryPath, true);
                                        Log.Error("Error when export case_attachments.csv due to :" + ex.Message, ex);
                                        exportEntity.Status = ExportStatus.Failed;
                                        exportEntity.Message = "Error when export case_attachments.csv due to :" + ex.Message;
                                        export.Update(exportEntity);
                                        throw new ArgumentException("Error while exporting case attachments data due to :" + ex.Message);
                                    }

                                    #endregion

                                    #region Download all attachment

                                    try
                                    {
                                        string attachmentsSavePath = Path.Combine(csvSaveDirectoryPath, "Attachments");
                                        if (!Directory.Exists(attachmentsSavePath))
                                        {
                                            Directory.CreateDirectory(attachmentsSavePath);
                                        }
                                        else
                                        {
                                            Directory.Delete(attachmentsSavePath, true);
                                            Directory.CreateDirectory(attachmentsSavePath);
                                        }
                                        foreach (Attachment atmt in attachmentListResult)
                                        {
                                            Attachment attachment = attachmentService.DownloadAttachment(atmt);
                                            string ext = Path.GetExtension(attachment.FileName);
                                            attachment.FileName = attachment.Id + ext;
                                            System.IO.File.WriteAllBytes(Path.Combine(attachmentsSavePath, attachment.FileName), attachment.File);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Directory.Delete(csvSaveDirectoryPath, true);
                                        Log.Error("Error when downloading attachments due to :" + ex.Message, ex);
                                        exportEntity.Status = ExportStatus.Failed;
                                        exportEntity.Message = "Error when downloading attachments due to :" + ex.Message;
                                        export.Update(exportEntity);
                                        throw new ArgumentException("Error when downloading attachments due to :" + ex.Message);
                                    }

                                    #endregion

                                    #region Add AllFiles To Zip

                                    try
                                    {
                                        Guid id = Guid.NewGuid();
                                        //string password = AbsenceSoft.Data.Security.User.Current.CustomerId + "_" + id.ToString("N");
                                        string password = "AbsenceSoft";
                                        bool isZip = export.Zip(csvSaveDirectoryPath, password, exportEntity.Id, zipSavefilePath);
                                        Directory.Delete(csvSaveDirectoryPath, true);
                                        if (!isZip)
                                        {
                                            exportEntity.Status = ExportStatus.Failed;
                                            exportEntity.Message = "Operation Failed Due To Internal Server Error";
                                            export.Update(exportEntity);
                                            throw new Exception("Operation Failed Due To Internal Server Error");
                                        }
                                        else
                                        {
                                            exportEntity.FileReference = export.UploadFileToAmazon(zipSavefilePath, exportEntity.CustomerId, exportEntity.EmployerId);
                                            DateTime endTime = DateTime.Now;
                                            TimeSpan totalTime = endTime - startTime;
                                            exportEntity.Status = ExportStatus.Complete;
                                            exportEntity.Message = "Zip created successfully";
                                            exportEntity.ZipFilePassword = CryptoString.GetPlainText(password);
                                            exportEntity.CompletedIn = totalTime.TotalMinutes;
                                            export.Update(exportEntity);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        if (System.IO.File.Exists(zipSavefilePath))
                                        {
                                            System.IO.File.Delete(zipSavefilePath);
                                        }
                                        Log.Error("Error while zipping all csvs due to" + ex.Message, ex);
                                        exportEntity.Status = ExportStatus.Failed;
                                        exportEntity.Message = "Error while zipping all csvs due to :" + ex.Message;
                                        export.Update(exportEntity);
                                        throw new ArgumentException(ex.Message);
                                    }

                                    #endregion

                                    return Json("Data Exported Successfully");
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("Error before start export data due to :" + ex.Message, ex);
                    exportEntity.Status = ExportStatus.Failed;
                    exportEntity.Message = "Error while zipping all csvs due to :" + ex.Message;
                    export.Update(exportEntity);
                    throw new ArgumentException("Export Data Failed due to :" + ex.Message);
                }
            }
        }

        [Secure("ExportData"), HttpPost]
        [Route("ExportData/GetExportLink", Name = "GetExportLink")]
        [ValidateApiAntiForgeryToken]
        public JsonResult GetExportLink(string key)
        {
            using (var exportService = new ExportService())
            {
                string url = exportService.GetExportLink(key);
                return Json(new { url = url });
            }
        }

        [Secure("ExportData"), HttpPost]
        [Route("ExportData/{employerId}/GetTotalLinks", Name = "GetTotalLinks")]
        [ValidateApiAntiForgeryToken]
        public JsonResult GetTotalLinks(string key, string employerId)
        {
            using (var exportService = new ExportService())
            {
                List<DataExport> data = exportService.GetTotalLinks(employerId);
                return Json(data, JsonRequestBehavior.DenyGet);
            }
        }
    }
}