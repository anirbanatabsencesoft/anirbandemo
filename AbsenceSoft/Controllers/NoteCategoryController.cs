﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Tasks;
using AbsenceSoft.Models.Notes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Common;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    [Secure]
    public class NoteCategoryController : BaseController
    {
        [Secure]
        [HttpGet, Title("View Note Categories"), Route("NoteCategory/List/{employerId?}", Name ="ViewNoteCategories")]
        public ActionResult ViewNoteCategories(string employerId, EntityTarget? target)
        {
            using (var notesService = new NotesService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                // If we're making an Ajax request, it's for a drop down list and we don't want to include suppressed entities
                bool isAjax = Request.IsAjax();
                List<NoteCategory> noteCategories = notesService.GetAllNoteCategories(target, false, !isAjax);
                if(isAjax)
                    return Json(noteCategories);

                return View(noteCategories.Select(nc => new NoteCategoryViewModel(nc)).ToList());
            }
        }

        [Secure("EditNoteCategories")]
        [HttpGet, Route("NoteCategory/EditNoteCategory/{employerId?}", Name = "EditNoteCategory")]
        public ActionResult EditNoteCategory(string employerId, string id = null)
        {
            using (var noteService = new NotesService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                NoteCategoryViewModel category = new NoteCategoryViewModel(noteService.GetNoteCategoryById(id));
                return View(category);
            }
        }

        [Secure("EditNoteCategories")]
        [HttpPost, Route("NoteCategory/Save", Name = "NoteCategorySave")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveNoteCategory(NoteCategoryViewModel category)
        {
            if (!ModelState.IsValid)
                return PartialView("EditNoteCategory", category);

            if (ModelState.IsValid)
            {
                try
                {
                    using (NotesService svc = new NotesService(CurrentCustomer, CurrentEmployer, CurrentUser))
                    {
                        NoteCategory savedCategory = svc.GetNoteCategoryById(category.Id);

                        savedCategory = category.ApplyToDataModel(savedCategory);
                        savedCategory = svc.SaveNoteCategory(savedCategory);
                    }

                    return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewNoteCategories"));
                }
                catch (AbsenceSoftException ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }

            if (Request.IsAjaxRequest())
                return PartialView("NoteCategoryEdit", category);

            return View("NoteCategoryEdit", category);
        }

        [Secure("EditNoteCategories")]
        [HttpDelete, Route("NoteCategory/DeleteNoteCategory/{employerId?}", Name = "DeleteNoteCategory")]
        public ActionResult DeleteNoteCategory(string employerId, string id)
        {
            using (var noteService = new NotesService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                NoteCategory category = noteService.GetNoteCategoryById(id);
                if (category.IsCustom && !noteService.NoteCategoryHasChildren(category))
                    noteService.DeleteNoteCategory(category);

                return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewNoteCategories"));
            }
        }

        [Secure("EditNoteCategories")]
        [HttpPost, Route("NoteCategory/ToggleNoteCategorySuppression/{employerId?}", Name ="ToggleNoteCategorySuppression")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ToggleNoteCategorySuppression(NoteCategoryViewModel noteCategory)
        {
            using (var noteService = new NotesService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                var savedNoteCategory = noteService.ToggleNoteCategoryByCode(noteCategory.Code);
                noteCategory.IsDisabled = savedNoteCategory.IsSuppressed(CurrentCustomer, CurrentEmployer);
                return Json(noteCategory);
            }
        }


        [HttpGet, Route("NoteTemplate/List/{employerId?}", Name = "ViewNoteTemplates")]
        public ActionResult NoteTemplateList(string employerId)
        {
            return View();
        }

        [HttpGet, Route("NoteTemplatesByCategory/List/{noteCategoryCode}")]
        public ActionResult NoteTemplatesByCategory(string noteCategoryCode)
        {
            using (var noteService = new NotesService(CurrentUser))
            {
                List<NoteTemplate> noteTemplates = noteService.GetNoteTemplatesByCategory(noteCategoryCode);
                return Json(noteTemplates);
            }
        }

        [HttpPost, Route("NoteTemplate/List/", Name="ListNoteTemplates")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListNoteTemplates(ListCriteria criteria)
        {
            using (var noteService = new NotesService(CurrentUser))
            {
                var noteTemplates = noteService.GetNoteTemplates(criteria);
                return PartialView(noteTemplates);
            }
        }

        [Secure("EditNoteCategories")]
        [HttpGet, Route("NoteTemplate/{customerId}/Edit/{templateId?}", Name="NoteTemplateEdit")]
        public ActionResult NoteTemplateEdit(string customerId, string templateId = null)
        {
            NoteTemplateViewModel template = new NoteTemplateViewModel();
            if (!string.IsNullOrEmpty(templateId))
                template = new NoteTemplateViewModel(NoteTemplate.GetById(templateId));

            return View(template);
        }

        [Secure("EditNoteCategories")]
        [HttpPost, Route("NoteTemplate/Save", Name = "NoteTemplateSave")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveNoteTemplate(NoteTemplateViewModel templateViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var notesService = new NotesService(CurrentUser))
                    {
                        NoteTemplate savedTemplate = new NoteTemplate()
                        {
                            CustomerId = CurrentCustomer.Id
                        };
                        if (!string.IsNullOrEmpty(templateViewModel.Id))
                            savedTemplate = NoteTemplate.GetById(templateViewModel.Id);

                        savedTemplate = templateViewModel.ApplyToDataModel(savedTemplate);
                        savedTemplate = notesService.SaveNoteTemplate(savedTemplate);
                    }

                    return RedirectViaJavaScript(Url.RouteUrl("ViewNoteTemplates"));
                }
                catch (AbsenceSoftException ex)
                {
                    ModelState.AddModelError(string.Empty, ex);
                }
            }

            if (Request.IsAjaxRequest())
                return PartialView("NoteTemplateEdit", templateViewModel);

            return View("NoteTemplateEdit", templateViewModel);
        }


        [Secure("EditNoteCategories")]
        [HttpDelete, Route("NoteTemplate/{customerId}/Delete/{noteTemplateId}", Name="DeleteNoteTemplate")]
        public ActionResult DeleteNoteTemplate(string customerId, string noteTemplateId)
        {
            try
            {
                new NotesService(CurrentUser).DeleteNoteTemplate(customerId, noteTemplateId);
                return RedirectViaJavaScript(Url.Action("NoteTemplateList", new { customerId = CurrentCustomer.Id }));
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to delete Note Category");
            }
        }

        
    }
}