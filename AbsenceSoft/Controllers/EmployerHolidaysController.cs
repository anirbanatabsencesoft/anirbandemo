﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Processing.CaseRecalc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using AtUser = AbsenceSoft.Data.Security.User;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Models.Administration;

namespace AbsenceSoft.Controllers
{
    [RoutePrefix("Administration/Employer/{employerId}/Holiday/{holidayId?}")]
    [Secure(Permission.EditEmployerInfoPermission)]
    [Route("Administration/GetEmployerInfo", Name = "GetEmployerInfo")]
    public class EmployerHolidaysController : BaseController
    {
        [HttpGet, Route]
        public ActionResult Index(string employerId)
        {
            return ExecuteOnEmployer(employerId, false, e =>
            {
                var model = new
                {
                    EmployerName = e.SignatureName,
                    Items = (e.Holidays ?? new List<Holiday>()).Select(h => new EmployerHolidayViewModel(h)).ToList()
                };
                return Json(model);
            });
        }

        [HttpPut, Route]
        [ValidateModelState]
        public ActionResult Upsert(string employerId, EmployerHolidayViewModel holiday)
        {
            if (holiday.Month == Data.Enums.Month.February && holiday.DayOfMonth == 29)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Leap year is not allowed for a holiday");
            }

            if ( !string.IsNullOrEmpty(holiday.Date)) 
            {
                DateTime? startDate = null;
                DateTime? endDate = null;
                DateTime holidayDate = DateTime.MinValue;
                DateTime dt;
                DateTime Hdt;
                if (DateTime.TryParse(holiday.Date, out dt))
                {
                    if (DateTime.TryParse(holiday.SpecificDate, out Hdt))
                    {
                        holidayDate = Hdt;
                        holiday.Date = holiday.SpecificDate;
                    }
                    else
                    {
                        holidayDate = dt.ToMidnight();
                    }
                }
                if (DateTime.TryParse(holiday.StartDate, out dt))
                    startDate = dt.ToMidnight();
                if (DateTime.TryParse(holiday.EndDate, out dt))
                    endDate = dt.ToMidnight();

                if (!holidayDate.DateInRange(startDate ?? DateTime.MinValue, endDate))
                {
                    return new StatusAndDataActionResult(HttpStatusCode.InternalServerError, new
                    {
                        error = new
                        {
                            Message = string.Format("Holiday Date '{0}' should be between Effective Date Start and Effective Date End", holidayDate.ToShortDateString())
                        }
                    });
                }
            }

            return ExecuteOnEmployer(employerId, true, e =>
            {
                var target = e.Holidays.SingleOrDefault(h => h.Id == holiday.Id);
                if (target == null)
                {
                    target = new Holiday();
                    e.Holidays.Add(target);
                }
                holiday.ToHoliday(target);
                
                e.Save();
                return Json(new EmployerHolidayViewModel(target));
            });
        }

        [HttpDelete]
        [ValidateModelState, Route]
        public ActionResult Destroy(string employerId, Guid? holidayId)
        {
            return ExecuteOnEmployer(employerId, true, e =>
            {
                var target = e.Holidays.SingleOrDefault(h => h.Id == holidayId);
                if (target == null)
                {
                    return new HttpNotFoundResult();
                }
                e.Holidays.Remove(target);
                e.Save();
                return Json(target);
            });
        }

        private ActionResult ExecuteOnEmployer(string employerId, bool isHolidayChange, Func<Employer, ActionResult> action)
        {
            if (string.IsNullOrWhiteSpace(employerId))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            employerId = employerId.ToLowerInvariant();
            var employer = Employer.GetById(employerId);
            return this.ExecuteWithEmployerAccess(employerId, Permission.EditEmployerInfo, () =>
            {
                if (isHolidayChange)
                {
                    EnqueueCaseRecalc(employerId);
                }
                return action(employer);
            });
        }

        private static void EnqueueCaseRecalc(string employerId)
        {
            using (InstrumentationContext context = new InstrumentationContext("EmployerHolidayChanged.QueueCaseRecalc"))
            {
                using (QueueService svc = new QueueService())
                {
                    var data = new CaseRecalcData(
                        CaseEventType.EmployerHolidayScheduleChanged,
                        Case.Query.EQ(c => c.EmployerId, employerId),
                        AtUser.Current.Id
                    );
                    svc.Add(MessageQueues.AtCommonQueue, MessageType.CaseRecalcRequest, data);
                }
            }
        }
    }
}