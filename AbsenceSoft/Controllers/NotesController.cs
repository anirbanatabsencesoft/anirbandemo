﻿using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Tasks;
using AbsenceSoft.Models;
using AbsenceSoft.Models.Contacts;
using AbsenceSoft.Models.Notes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    [Secure]
    public class NotesController : BaseController
    {
        [HttpGet, Route("Inquiries/{caseId}/CaseActivity", Name = "InquiryActivity")]
        public ActionResult InquiryActivity(string caseId)
        {
            ViewBag.Status = "Inquiry";
            return CaseActivityGet(caseId);
        }

        [HttpGet, Route("Cases/{caseId}/CaseActivity", Name = "CaseActivity")]
        public ActionResult CaseActivity(string caseId)
        {
            ViewBag.Status = "";
            return CaseActivityGet(caseId);
        }

        private ActionResult CaseActivityGet(string caseId)
        {
            try
            {
                ListCriteria criteria = new ListCriteria
                {
                    PageSize = 1000
                };
                criteria.ExtensionData.Add("CaseId", caseId);
                criteria.ExtensionData.Add("Id", caseId);

                ListResults attachments = new AttachmentService().AttachmentList(criteria);
                ListResults todos = new ToDoService().ToDoItemList(CurrentUser, criteria);
                ListResults communications = new CommunicationService().CommunicationList(criteria);
                ListResults assignments = new CaseService().GetCaseAssignmentList(criteria);
                ListResults caseActivityResults = new NotesService().GetCaseNoteList(criteria);
                ListResults caseReporters = new CaseService().GetCaseReporterList(criteria);

                List<ListResult> caseActivity = caseActivityResults.Results.ToList();
                foreach (ListResult note in caseActivity)
                {
                    note.ExtensionData["Type"] = "Note";
                    note.ExtensionData["Description"] = note.ExtensionData.ContainsKey("Notes") ? note.ExtensionData["Notes"] : "";
                }

                caseActivity.AddRange(assignments.Results.Select(a => new ListResult()
                    .Set("CreatedDate", a.ExtensionData.ContainsKey("CreatedDate") ? a.ExtensionData["CreatedDate"] : "")
                    .Set("Category", a.ExtensionData.ContainsKey("Category") ? a.ExtensionData["Category"] : "")
                    .Set("CreatedBy", a.ExtensionData.ContainsKey("ModifiedBy") ? a.ExtensionData["ModifiedBy"] : "")
                    .Set("Type", "Assignment")
                    .Set("Description", a.ExtensionData.ContainsKey("Description") ? a.ExtensionData["Description"] : "")
                    .Set("ModifiedDate", a.ExtensionData.ContainsKey("ModifiedDate") ? a.ExtensionData["ModifiedDate"] : "")
                    .Set("StatusId", "")));

                caseActivity.AddRange(attachments.Results.Select(a => new ListResult()
                    .Set("CreatedDate", a.ExtensionData.ContainsKey("CreatedDate") ? a.ExtensionData["CreatedDate"] : "")
                    .Set("Category", a.ExtensionData.ContainsKey("AttachmentTypeName") ? a.ExtensionData["AttachmentTypeName"] : "")
                    .Set("CreatedBy", a.ExtensionData.ContainsKey("AttachedBy") ? a.ExtensionData["AttachedBy"] : "")
                    .Set("Type", "Attachment")
                    .Set("Description", a.ExtensionData.ContainsKey("Description") ? a.ExtensionData["Description"] : "")
                    .Set("ModifiedDate", "")
                    .Set("StatusId", "")));
                
                caseActivity.AddRange(todos.Results.Where(w => w.ExtensionData.ContainsKey("StatusId") == false || Convert.ToInt64(w.ExtensionData["StatusId"]) != Convert.ToInt64(ToDoItemStatus.Cancelled)).Select(t => new ListResult()
                    .Set("CreatedDate", t.ExtensionData.ContainsKey("CreatedDate") ? t.ExtensionData["CreatedDate"] : "")
                    .Set("Category", "N/A")
                    .Set("CreatedBy", t.ExtensionData.ContainsKey("ModifiedByName") ? t.ExtensionData["ModifiedByName"] : "")
                    .Set("Type", "ToDo")
                    .Set("Description", t.ExtensionData.ContainsKey("Title") ? t.ExtensionData["Title"] : "")
                    .Set("CaseCloseReason", t.ExtensionData.ContainsKey("ReasonCause") ? t.ExtensionData["ReasonCause"] : "")
                    .Set("ModifiedDate", t.ExtensionData.ContainsKey("ModifiedDate") ? t.ExtensionData["ModifiedDate"] : "")
                    .Set("StatusId", t.ExtensionData.ContainsKey("StatusId") ? t.ExtensionData["StatusId"] : "")));

                caseActivity.AddRange(communications.Results.Select(c => new ListResult()
                    .Set("CreatedDate", c.ExtensionData.ContainsKey("SentDate") ? c.ExtensionData["SentDate"] : "")
                    .Set("Category", "N/A")
                    .Set("CreatedBy", c.ExtensionData.ContainsKey("CreatedByName") ? c.ExtensionData["CreatedByName"] : "")
                    .Set("Type", "Communication")
                    .Set("Description", c.ExtensionData.ContainsKey("Subject") ? c.ExtensionData["Subject"] : "")
                    .Set("ModifiedDate", "")
                    .Set("StatusId", "")));

                caseActivity.AddRange(caseReporters.Results.Select(a => new ListResult()
                    .Set("CreatedDate", a.ExtensionData.ContainsKey("CreatedDate") ? a.ExtensionData["CreatedDate"] : "")
                    .Set("Category", a.ExtensionData.ContainsKey("Category") ? a.ExtensionData["Category"] : "")
                    .Set("CreatedBy", a.ExtensionData.ContainsKey("ModifiedBy") ? a.ExtensionData["ModifiedBy"] : "")
                    .Set("Type", "CaseReporter")
                    .Set("Description", a.ExtensionData.ContainsKey("Description") ? a.ExtensionData["Description"] : "")
                    .Set("ModifiedDate", a.ExtensionData.ContainsKey("ModifiedDate") ? a.ExtensionData["ModifiedDate"] : "")
                    .Set("StatusId", "")
                    .Set("CommunicationType", a.ExtensionData.ContainsKey("CommunicationType") ? a.ExtensionData["CommunicationType"] : "")));

                caseActivityResults.Results = caseActivity;
                caseActivityResults.Total = caseActivity.Count;
                ViewBag.CaseId = caseId;
                return View("CaseActivity", caseActivityResults);
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Error fetching case activity report for {0}", caseId), ex);
                throw;
            }
        }

        [Title("Inquiry Notes")]
        [Route("Inquiries/{caseId}/Notes", Name = "InquiryNotes")]
        [Secure("EditNotes", "CreateNotes", "ViewNotes")]
        public ViewResult InquiryNotesIndex(string caseId)
        {
            ViewBag.Status = "Inquiry";
            return CaseNotesIndexGet(caseId);
        }

        [Title("Case Notes")]
        [Route("Cases/{caseId}/Notes", Name = "CaseNotes")]
        [Secure("EditNotes", "CreateNotes", "ViewNotes")]
        public ViewResult CaseNotesIndex(string caseId)
        {
            ViewBag.Status = "";
            return CaseNotesIndexGet(caseId);
        }

        private ViewResult CaseNotesIndexGet(string caseId)
        {
            if (String.IsNullOrEmpty(caseId))
            {
                throw new ArgumentNullException("caseId");
            }

            Case theCase = Case.GetById(caseId);

            var model = new CaseParentedModel()
            {
                CaseId = caseId,
                Status = ViewBag.Status
            };
            ViewBag.CreationDate = DateTime.UtcNow.ToUnixDate();
            if (theCase != null)
            {
                ViewBag.EmployeeFullName = theCase.Employee.FirstName + " " + theCase.Employee.LastName;
                ViewBag.CaseNumber = theCase.CaseNumber;
                ViewBag.EmployerId = theCase.EmployerId;
                ViewBag.EmployeeId = theCase.Employee.Id;
            }

            return View("Index", model);
        }

        [Title("Employee Notes")]
        [Route("Employees/{employeeId}/Notes", Name = "EmployeeNotes")]
        [Secure("EditNotes", "CreateNotes", "ViewNotes")]
        public ViewResult EmployeeNotesIndex(string employeeId)
        {
            if (String.IsNullOrEmpty(employeeId))
            {
                throw new ArgumentNullException("employeeId");
            }

            using (var service = new EmployeeService())
            {
                var emp = service.GetEmployee(employeeId);
                if (emp != null)
                {
                    ViewBag.EmployeeFullName = emp.FirstName + " " + emp.LastName;
                    ViewBag.EmployerId = employeeId;
                    ViewBag.EmployeeId = emp.EmployerId;
                }
            }

            var model = new EmployeeParentedModel()
            {
                EmployeeId = employeeId
            };
            ViewBag.CreationDate = DateTime.Now.ToUIString();
            return View("Index", model);
        }

        [HttpPost, Secure("DeleteNotes")]
        [Route("Cases/Note/Delete", Name = "DeleteNote")]
        [ValidateApiAntiForgeryToken]
        public ActionResult DeleteNote(DeleteCaseNoteViewModel viewModel)
        {
            try
            {
                if (String.IsNullOrEmpty(viewModel.Id))
                {
                    throw new ArgumentNullException("noteId");
                }


                CaseNote.Delete(viewModel.Id);
                return Json(null);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }


        [HttpPost, Secure("DeleteNotes")]
        [Route("Employee/Note/Delete", Name = "EmployeeNote")]
        [ValidateApiAntiForgeryToken]
        public ActionResult EmployeeNoteDelete(DeleteCaseNoteViewModel viewModel)
        {
            try
            {
                if (String.IsNullOrEmpty(viewModel.Id))
                {
                    throw new ArgumentNullException("noteId");
                }


                EmployeeNote.Delete(viewModel.Id);
                return Json(null);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }



        [HttpPost, Secure("CreateNotes", "EditNotes", "CreateMyNotes")]
        [Route("Cases/{caseId}/Notes/Create", Name = "CreateCaseNote")]
        [ValidateApiAntiForgeryToken]
        public ActionResult CreateCaseNote(CreateNoteViewModel viewModel)
        {
            if (String.IsNullOrEmpty(viewModel.CaseId))
                throw new ArgumentNullException("caseId");
            if (String.IsNullOrEmpty(viewModel.Notes))
                throw new ArgumentNullException("Notes");

            var caseNote = new CaseNote();

            if (!string.IsNullOrWhiteSpace(viewModel.Id))
                caseNote = CaseNote.GetById(viewModel.Id);

            caseNote.CaseId = viewModel.CaseId;
            caseNote.Notes = HttpUtility.HtmlEncode(viewModel.Notes);
            caseNote.Public = viewModel.Public;
            
            if (viewModel.Category.HasValue)
            {
                caseNote.Category = (NoteCategoryEnum)viewModel.Category.Value;
            }
            else
            {
                foreach (NoteCategoryEnum val in Enum.GetValues(typeof(NoteCategoryEnum)))
                {
                    if (viewModel.CategoryCode != null && val.ToString().ToLower() == viewModel.CategoryCode.ToLower())
                    {
                        caseNote.Category = val;
                        break;
                    }
                }
            }
            using (var noteService = new NotesService(CurrentUser))
            {
                caseNote.Categories = noteService.CreateSavedNoteCategories(viewModel.CategoryCode, EntityTarget.Case);    
            }
            caseNote.EmployerId = caseNote.Case.EmployerId;
            caseNote.CustomerId = CurrentCustomer.Id;
            caseNote.TemplateId = viewModel.TemplateId != null && viewModel.TemplateId.Trim().Length > 0 ? viewModel.TemplateId : null;

            caseNote.Save();
            caseNote.WfOnCaseNoteCreated(CurrentUser);

            return Content("case");
        }

        [HttpPost, Secure("CreateNotes", "EditNotes", "CreateMyNotes")]
        [Route("Employees/{employeeId}/Notes/Create", Name = "CreateEmployeeNote")]
        [ValidateApiAntiForgeryToken]
        public ActionResult CreateEmployeeNote(CreateNoteViewModel viewModel)
        {
            if (String.IsNullOrEmpty(viewModel.EmployeeId))
            {
                throw new ArgumentNullException("employeeId");
            }
            if (String.IsNullOrEmpty(viewModel.Notes))
            {
                throw new ArgumentNullException("Notes");
            }

            var employeeNote = new EmployeeNote();

            if (!string.IsNullOrWhiteSpace(viewModel.Id))
            {
                employeeNote = EmployeeNote.GetById(viewModel.Id);
            }


            employeeNote.EmployeeId = viewModel.EmployeeId;
            employeeNote.EmployeeNumber = employeeNote.Employee.EmployeeNumber;
            employeeNote.Notes = HttpUtility.HtmlEncode(viewModel.Notes);
            employeeNote.Public = viewModel.Public;
            employeeNote.Category = viewModel.Category.HasValue ? (NoteCategoryEnum)viewModel.Category.Value : new NoteCategoryEnum?();
            using (var noteService = new NotesService(CurrentUser))
            {
                employeeNote.Categories = noteService.CreateSavedNoteCategories(viewModel.CategoryCode, EntityTarget.Employee);    
            }
            
            employeeNote.EmployerId = employeeNote.Employee.EmployerId;
            employeeNote.CustomerId = CurrentCustomer.Id;
            employeeNote.TemplateId = viewModel.TemplateId != null && viewModel.TemplateId.Trim().Length > 0 ? viewModel.TemplateId : null;

            employeeNote.Save();

            return Content("employee");
        }

        [HttpGet, Secure("EditNotes")]
        [Route("Employees/{employeeNoteId}/GetEmployeeNoteById", Name = "GetEmployeeNoteById")]
        public ActionResult GetEmployeeNoteById(string employeeNoteId)
        {
            if (String.IsNullOrEmpty(employeeNoteId))
            {
                throw new ArgumentNullException("employeeNoteId");
            }

            var employeeNotes = EmployeeNote.GetById(employeeNoteId);

            NoteModel model = new NoteModel()
            {
                EmployeeId = employeeNotes.EmployeeId,
                Category = (int?)employeeNotes.Category,
                CategoryCode = employeeNotes.Categories != null && employeeNotes.Categories.Count > 0 ? employeeNotes.Categories.FirstOrDefault().CategoryCode : string.Empty,
                CategoryText = employeeNotes.CategoryString,
                CreatedBy = (employeeNotes.CreatedBy ?? AbsenceSoft.Data.Security.User.System).DisplayName,
                CreatedDate = employeeNotes.CreatedDate.ToString(),
                NoteId = employeeNotes.Id,
                Public = employeeNotes.Public,
                Notes = HttpUtility.HtmlDecode(employeeNotes.Notes),
                TemplateId = employeeNotes.TemplateId
            };
            return Json(model);
        }

        [HttpGet, Secure("EditNotes")]
        [Route("Cases/{caseNoteId}/GetCaseNoteById", Name = "GetCaseNoteById")]
        public ActionResult GetCaseNoteById(string caseNoteId)
        {
            if (String.IsNullOrEmpty(caseNoteId))
            {
                throw new ArgumentNullException("caseNoteId");
            }

            using (var noteService = new NotesService())
            {

                var caseNote = noteService.GetCaseNoteById(caseNoteId);

                NoteModel model = new NoteModel()
                {
                    CaseNumber = caseNote.Case.CaseNumber,
                    Category = (int?)caseNote.Category,
                    CategoryCode = caseNote.Categories != null && caseNote.Categories.Count > 0 ? caseNote.Categories.FirstOrDefault().CategoryCode : string.Empty,
                    CategoryText = caseNote.CategoryString,
                    CreatedBy = (caseNote.CreatedBy ?? AbsenceSoft.Data.Security.User.System).DisplayName,
                    CreatedDate = caseNote.CreatedDate.ToString(),
                    NoteId = caseNote.Id,
                    Public = caseNote.Public,
                    Notes = HttpUtility.HtmlDecode(caseNote.Notes),
                    TemplateId = caseNote.TemplateId
                };

                return Json(model);
            }
        }

        /// <summary>
        /// Gets a list of employees notes to display on Notes/index page.
        /// </summary>
        /// <param name="idFilter">Filter notes by selected employee id</param>
        /// <param name="CreatedDateFilter">Filter by notes created date</param>
        /// <param name="CategoryFilter">Filter by note category</param>
        /// <param name="NotesFilter">Filter by keywork in notes (contains query)</param>
        /// <param name="sortBy">The field name to sort by, plain text</param>
        /// <param name="sort">Either 1 (asc) or -1 (desc) for the sort order for the sort field</param>
        /// <param name="page">The page number that the user is on in the paging/scrolling of results</param>
        /// <param name="size">The size of each page</param>
        /// <returns></returns>
        [Secure("ViewNotes", "EditNotes", "CreateNotes"), HttpPost]
        [Route("Employees/Notes/List", Name = "EmployeeNotesList")]
        [ValidateApiAntiForgeryToken]
        public JsonResult EmployeeNotesList(ListCriteria criteria)
        {
            try
            {
                using (var service = new NotesService())
                {
                    var results = service.EmployeeNoteList(criteria);
                    return Json(results);
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error getting employee list");
            }
        }

        /// <summary>
        /// Gets a list of case notes to display on Notes/index page.
        /// </summary>
        /// <param name="idFilter">Filter notes by selected case id</param>
        /// <param name="CreatedDateFilter">Filter by notes created date</param>
        /// <param name="CategoryFilter">Filter by note category</param>
        /// <param name="NotesFilter">Filter by keywork in notes (contains query)</param>
        /// <param name="sortBy">The field name to sort by, plain text</param>
        /// <param name="sort">Either 1 (asc) or -1 (desc) for the sort order for the sort field</param>
        /// <param name="page">The page number that the user is on in the paging/scrolling of results</param>
        /// <param name="size">The size of each page</param>
        /// <returns></returns>
        [Secure("ViewNotes", "EditNotes", "CreateNotes"), HttpPost]
        [Route("Cases/Notes/List", Name = "CaseNotesList")]
        [ValidateApiAntiForgeryToken]
        public JsonResult CaseNotesList(ListCriteria criteria)
        {
            try
            {
                using (var service = new NotesService())
                {
                    var results = service.GetCaseNoteList(criteria);

                    return Json(results);
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error getting employee list");
            }
        }


        [Secure("ViewNotes", "EditNotes", "CreateNotes"), HttpGet]
        [Route("Notes/RenderTemplate")]
        public JsonResult RenderNoteTemplate(string entityId, string templateId, EntityTarget target)
        {
            using (var notesService = new NotesService(CurrentUser))
            {
                NoteTemplate renderedTemplate = notesService.RenderTemplate(entityId, templateId, target);
                return Json(renderedTemplate);
            }
        }
    }
}