﻿using AbsenceSoft.Common.Properties;
using AbsenceSoft.Data.Processing;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Logic.Processing;
using AbsenceSoft.Logic.Processing.EL;
using System;
using System.Linq;
using System.Web.Mvc;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    public class FilesController : BaseController
    {
        public class UploadPolicyRequestModel { public string fileName { get; set; } public string employerId { get; set; } }

        
        [Title("Eligibility File Upload"), Secure("UploadEligibilityFile")]
        [Route("Files/EligibilityFileUpload/{employerId?}", Name = "EligibilityFileUpload")]
        public ActionResult EligibilityFileUpload(string employerId)
        {
            ViewBag.EmployerId = employerId;
            return View();
        }

     
        [Title("Eligibility File Detail"), Secure("UploadEligibilityFile")]
        [Route("Files/{fileId}/EligibilityFileDetails", Name = "EligibilityFileDetails")]
        public ActionResult EligibilityFileDetails(string fileId)
        {
            EligibilityUpload upload = new EligibilityUpload()
            {
                Id = fileId
            };
            return View(upload);
        }

     

        [Secure("UploadEligibilityFile"), HttpGet]
        [Route("Files/Eligibility/Policy", Name = "GetEligibilityDataFileUploadPolicy")]
        public JsonResult GetEligibilityDataFileUploadPolicy(UploadPolicyRequestModel model)
        {
            try
            {
                using (FileService svc = new FileService())
                {
                    UploadPolicy policy = svc.GetProcessingFileUploadPolicy(model.fileName, CurrentUser.CustomerId, model.employerId);
                    return Json(policy);
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error getting appropriate upload permissions or URL, please try again.");
            }
        } // GetEligibilityDataFileUploadPolicy

        [Secure("UploadEligibilityFile"), HttpPut]
        [Route("Files/Eligibility", Name = "CreateEligibilityDataFileUpload")]
        [ValidateApiAntiForgeryToken]
        public JsonResult CreateEligibilityDataFileUpload(EligibilityUpload upload)
        {
            try
            {
                return Json(new EligibilityUploadService(CurrentUser).Using(s => s.CreateEligibilityDataFileUpload(upload)));
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error creating eligibility data file upload.");
            }
        } // GetEligibilityDataFileUploadPolicy


        [Secure("UploadEligibilityFile"), HttpGet]
        [Route("Files/Eligibility/List/{employerId?}", Name = "ListEligibilityDataFiles")]
        public ActionResult ListEligibilityDataFiles(string employerId)
        {
         
            return Json(new { Uploads = new EligibilityUploadService(CurrentCustomer , CurrentEmployer,CurrentUser).Using(s => s.ListEligibilityDataFiles()) });
        } // ListEligibilityDataFiles

        [Secure("UploadEligibilityFile"), HttpGet]
        [Route("Files/Eligibility/ListLinkDataFiles", Name = "ListLinkDataFiles")]
        public ActionResult ListLinkDataFiles(string linkedEligibilityUploadId)
        {
            return Json(new { Uploads = new EligibilityUploadService(CurrentUser).Using(s => s.ListLinkEligibilityDataFiles (linkedEligibilityUploadId)) });
        } // ListLinkDataFiles

        [Secure("UploadEligibilityFile"), HttpGet]
        [Route("Files/Eligibility/{eligibilityUploadId}", Name = "GetEligibilityDataFile")]
        public ActionResult GetEligibilityDataFile(string eligibilityUploadId)
        {
            EligibilityUpload upload = EligibilityUpload.GetById(eligibilityUploadId);
            if (upload == null)
                return new HttpNotFoundResult();

            if (!CurrentUser.HasEmployerAccess(upload.EmployerId, Permission.UploadEligibilityFile))
                return new HttpUnauthorizedResult();

            return Json(upload);
        } // GetEligibilityDataFile

        [Secure("UploadEligibilityFile"), HttpGet]
        [Route("Files/Eligibility/{eligibilityUploadId}/Errors", Name = "ListEligibilityDataFileErrors")]
        public ActionResult ListEligibilityDataFileErrors(string eligibilityUploadId, int? page, int? size)
        {
            var pageSize = size ?? int.MaxValue;
            var skip = (((page ?? 1) - 1) * pageSize);
            if (skip < 0) skip = 0;
            return Json(new
            {
                Errors = new EligibilityUploadService().Using(s => s.ListEligibilityDataFileErrors(eligibilityUploadId).Skip(skip).Take(pageSize))
            });
        }


        [Secure("UploadEligibilityFile"), HttpGet]
        [Route("Files/Eligibility/{eligibilityUploadId}/Errors/{errorId}", Name = "GetEligibilityDataFileError")]
        public ActionResult GetEligibilityDataFileError(string eligibilityUploadId, string errorId)
        {
            var error = EligibilityUploadResult.AsQueryable().FirstOrDefault(u => u.EligibilityUploadId == eligibilityUploadId && u.Id == errorId);
            if (error == null)
                return JsonError(null, "Error not found", 404);
            return Json(error);
        } // GetEligibilityDataFileError


        [Secure("UploadEligibilityFile"), HttpGet]
        [Route("Files/Eligibility/{eligibilityUploadId}/Download", Name = "DownloadEligibilityDataFile")]
        public ActionResult DownloadEligibilityDataFile(string eligibilityUploadId)
        {
            EligibilityUpload upload = EligibilityUpload.GetById(eligibilityUploadId);
            if (upload == null)
                return new HttpNotFoundResult();

            if (!CurrentUser.HasEmployerAccess(upload.EmployerId, Permission.UploadEligibilityFile))
                return new HttpUnauthorizedResult();

            using (FileService svc = new FileService())
                return Redirect(svc.GetS3DownloadUrl(upload.FileKey, Settings.Default.S3BucketName_Processing, upload.FileName));
        } // DownloadEligibilityDataFile

        [Secure("UploadEligibilityFile"), HttpDelete]
        [Route("Files/Eligibility/{eligibilityUploadId}/Delete", Name = "DeleteEligibilityDataFile")]
        [ValidateApiAntiForgeryToken]
        public JsonResult DeleteEligibilityDataFile(string eligibilityUploadId)
        {
            try
            {
                new EligibilityUploadService(CurrentUser).Using(s => s.DeleteEligibilityDataFile(eligibilityUploadId));
                return Json(new { Success = true });
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        } // DeleteEligibilityDataFile

        [Secure("UploadEligibilityFile"), HttpDelete]
        [Route("Files/Eligibility/{eligibilityUploadId}/Cancel", Name = "CancelEligibilityDataFile")]
        [ValidateApiAntiForgeryToken]
        public JsonResult CancelEligibilityDataFile(string eligibilityUploadId)
        {
            try
            {
                new EligibilityUploadService(CurrentUser).Using(s => s.CancelEligibilityDataFile(eligibilityUploadId));
                return Json(new { Success = true });
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        } // CancelEligibilityDataFile

        [Secure]
        [Route("Files/Errors/{eligibilityUploadId}/ToCSV", Name = "FileErrorsToCSV")]
        public ActionResult ExportToCSV(string eligibilityUploadId)
        {
            EligibilityUpload upload = EligibilityUpload.GetById(eligibilityUploadId);

            EligibilityUploadService svc = new EligibilityUploadService();

            byte[] output = svc.GenerateCSV(eligibilityUploadId);
            string csvFileName =  upload.FileName + "_Errors.csv";
            return File(output, "text/comma-separated-values", csvFileName);
        }




        [Serializable]
        public class FTPServerAutomationCreateELFileRequest
        {
            public string Id { get; set; }
            public string CustomerId { get; set; }
            public string EmployerId { get; set; }
            public string FileName { get; set; }
            public long Size { get; set; }
            public string FileKey { get; set; }
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("Files/Eligibility/31755D83C5DF4E4AAE3ACD31787DD612/FTPServerAutomationCreateELFile/POST", Name = "FTPServerAutomationCreateELFile")]
        public ActionResult FTPServerAutomationCreateELFile(FTPServerAutomationCreateELFileRequest body)
        {
            if (Request.Headers["AS-Auth-Worthy"] != "oI8yourAbsenceToo")
                return new HttpUnauthorizedResult("Nope, sorry, no dice for you");
            try
            {
                EligibilityUpload upload = new EligibilityUpload()
                {
                    CustomerId = body.CustomerId,
                    EmployerId = body.EmployerId,
                    FileKey = body.FileKey,
                    FileName = body.FileName,
                    Size = body.Size,
                    Status = Data.Enums.ProcessingStatus.Pending,
                    MimeType = "text/csv"
                };

                using (EligibilityUploadService svc = new EligibilityUploadService(CurrentUser))
                    upload = svc.CreateEligibilityDataFileUpload(upload);

                body.Id = upload.Id;
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
            return Json(body);
        }


        [HttpPost]
        [AllowAnonymous]
        [Route("Files/Eligibility/31755D83C5DF4E4AAE3ACD31787DD612/ReQueue/POST", Name = "ReQueueELFile")]
        public ActionResult ReQueueEligibilityUpload(FTPServerAutomationCreateELFileRequest body)
        {
            if (Request.Headers["AS-Auth-Worthy"] != "oI8yourAbsenceToo")
                return new HttpUnauthorizedResult("Nope, sorry, no dice for you");
            try
            {
                var uploadId = body.Id;
                var upload = EligibilityUpload.GetById(uploadId);

                using (EligibilityUploadService svc = new EligibilityUploadService(CurrentUser))
                    upload = svc.CreateEligibilityDataFileUpload(upload);
                
                body.Id = upload.Id;
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
            return Json(body);
        }
    }
}