﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Models.Organization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    [Secure("EditOrganizations")]
    public class OrganizationTypeController : BaseController
    {
        // GET: Organization
        [HttpGet, Route("OrganizationType/{employerId}/List", Name = "OrganizationTypeList")]
        public ActionResult OrganizationTypeList(string employerId)
        {
            return View();
        }

        [HttpPost, Route("OrganizationType/List", Name="ListOrganizationTypes")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListOrganizationTypes(ListCriteria criteria)
        {
            using (var orgService = new OrganizationService(CurrentUser))
            {
                var organizationTypes = orgService.ListOrganizationTypes(criteria);
                return PartialView(organizationTypes);
            }
        }

        [HttpGet, Route("OrganizationType/{employerId}/Edit/{organizationTypeId?}", Name = "OrganizationTypeEdit")]
        public ActionResult OrganizationTypeEdit(string employerId, string organizationTypeId = null)
        {
            OrganizationTypeViewModel orgTypeViewModel = null;
            if (organizationTypeId != null)
            {
                OrganizationType orgType = OrganizationType.GetById(organizationTypeId);
                orgTypeViewModel = new OrganizationTypeViewModel(orgType);
            }
            else
            {
                orgTypeViewModel = new OrganizationTypeViewModel();
            }

            return View(orgTypeViewModel);
        }

        [HttpPost, Route("OrganizationType/{employerId}/Save", Name="OrganizationTypeSave")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveOrganizationType(string employerId, OrganizationTypeViewModel orgTypeViewModel)
        {
            if (!ModelState.IsValid)
                return PartialView(orgTypeViewModel);

            OrganizationType orgType = null;
            if (!string.IsNullOrEmpty(orgTypeViewModel.Id))
            {
                orgType = OrganizationType.GetById(orgTypeViewModel.Id);
            }
            else
            {
                orgType = new OrganizationType();
            }

            orgType = orgTypeViewModel.ApplyToDataModel(orgType);
            orgType.Save();

            return RedirectViaJavaScript(Url.Action("OrganizationTypeList", new { employerId = Employer.Current.Id }));
        }

        [HttpDelete, Route("OrganizationType/{employerId}/Delete/{organizationTypeId}", Name = "DeleteOrganizationType")]
        public ActionResult DeleteOrganizationType(string employerId, string organizationTypeId)
        {
            try
            {
                OrganizationType org = OrganizationType.GetById(organizationTypeId);
                org.Delete();
                return RedirectViaJavaScript(Url.Action("OrganizationTypeList", new { employerId = Employer.Current.Id }));
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to delete Organization Type");
            }
        }

    }
}