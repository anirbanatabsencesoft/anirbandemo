﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Models.AccommodationTypes;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Controllers
{
    [Secure("EditAccommodationTypes")]
    public class AccommodationTypeController : BaseController
    {
        [Title("View Accommodation Types")]
        [HttpGet, Route("AccommodationTypes/List/{employerId?}", Name = "ViewAccommodationTypes")]
        public ActionResult ViewAccommodationTypes(string employerId)
        {
            return View();
        }

        [HttpPost, Route("AccommodationTypes/ListAccommodationTypes", Name = "ListAccommodationTypes")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListAccommodationTypes(ListCriteria criteria)
        {
            using (var accommodationService = new AccommodationService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                ListResults results = accommodationService.AccommodationTypeList(criteria);
                return PartialView(results);
            }
        }

        [Title("Edit Accommodation Type")]
        [HttpGet, Route("AccommodationTypes/EditAccommodationType/{employerId?}", Name = "EditAccommodationType")]
        public ActionResult EditAccommodationType(string employerId, string id)
        {
            using (var accommodationService = new AccommodationService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                AccommodationTypeViewModel viewModel = new AccommodationTypeViewModel(accommodationService.GetAccommodationTypeById(id));
                return View(viewModel);
            }
        }

        [Title("Edit Accommodation Type")]
        [HttpPost, Route("AccommodationTypes/SaveAccommodationType", Name = "SaveAccommodationType")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveAccommodationType(AccommodationTypeViewModel accommodationType)
        {
            if (!ModelState.IsValid)
                return PartialView("EditAccommodationType", accommodationType);

            using (var accommodationService = new AccommodationService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                AccommodationType type = accommodationType.ApplyToDataModel(accommodationService.GetAccommodationTypeById(accommodationType.Id));
                accommodationService.SaveAccommodationType(type);
            }

            return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewAccommodationTypes"));
        }

        [HttpDelete, Route("AccommodationTypes/DeleteAccommodationType/{employerId?}", Name = "DeleteAccommodationType")]
        public ActionResult DeleteAccommodationType(string employerId, string id)
        {
            using (var accommodationService = new AccommodationService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                AccommodationType type = accommodationService.GetAccommodationTypeById(id);
                if (type.IsCustom)
                    accommodationService.DeleteAccommodationType(type);

                return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewAccommodationTypes"));
            }
        }

        [HttpPost, Route("AccommodationTypes/ToggleAccommodationTypeSuppression/{employerId?}", Name = "ToggleAccommodationTypeSuppression")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ToggleAccommodationTypeSuppression(AccommodationTypeViewModel accommodationType)
        {
            using (var accommodationService = new AccommodationService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                var savedAccommodationType = accommodationService.ToggleAccommodationTypeByCode(accommodationType.Code);
                accommodationType.IsDisabled = savedAccommodationType.IsDisabled;
                return Json(accommodationType);
            }
        }

    }
}