﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Models.Cases;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;
using System.Web.Mvc;

namespace AbsenceSoft.Controllers
{
    [Secure("EditOshaFormFields")]
    public class OshaFormFieldController : BaseController
    {
        [Title("View OSHA Form Field")]
        [HttpGet, Route("OshaFormField/List/{employerId?}", Name = "ViewOshaFormField")]   
        public ActionResult ViewOshaFormField(string employerId)
        {
            ViewBag.EmployerId = employerId;
            return View();
        }

        [HttpPost, Route("OshaFormField/List", Name = "ListOshaFormField")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListOshaFormField(ListCriteria criteria)
        {
            using (var oshaService = new OshaFormFieldService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                ListResults results = oshaService.GetOshaFormFieldList(criteria);
                return PartialView(results);
            }
        }

        [Title("Edit OSHA Form Field")]
        [HttpGet, Route("OshaFormField/Edit/{employerId?}", Name = "EditOshaFormField")]
        public ActionResult EditOshaFormField(string employerId, string id)
        {
            ViewBag.EmployerId = employerId;
            using (var oshaService = new OshaFormFieldService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                OshaFormFieldViewModel viewModel = new OshaFormFieldViewModel(oshaService.GetOshaFormFieldById(id));
                return View(viewModel);
            }
        }

        [Title("Save OSHA Form Field")]
        [HttpPost, Route("EditOshaFormField/Save", Name = "SaveOshaFormField")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveOshaFormField(OshaFormFieldViewModel Osha)
        {
            if (!ModelState.IsValid)
                return PartialView("EditOshaFormField", Osha);

            using (var oshaService = new OshaFormFieldService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                OshaFormField type = Osha.ApplyToDataModel(oshaService.GetOshaFormFieldById(Osha.Id));
                oshaService.SaveOshaFormField(type);
            }

            return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewOshaFormField"));
        }
    }
}