﻿using AbsenceSoft.Common.Properties;
using AbsenceSoft.Common.Security;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Reporting;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Web;
using AbsenceSoft.Web.Attributes;
using AT.Api.Core.Common;
using AT.Common.Core;
using AT.Data.Reporting.Arguments;
using AT.Data.Reporting.Base;
using AT.Entities.Reporting.Classes;
using AT.Entities.Reporting.Enums;
using AT.Logic.Cache;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace AbsenceSoft.Controllers
{
    /// <summary>
    /// Controller to handle all JS API operations
    /// </summary>
    public class ApiExecutionController : BaseController
    {
        #region JS Framework endpoints

        /// <summary>
        /// Get the token of the authenticated user
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("token/get")]
        public async Task<string> GetAuthenticationToken()
        {
            return await Task.FromResult(Utilities.GetTokenFromClaims());
        }


        /// <summary>
        /// Get the current user
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("current/user")]
        public async Task<object> GetCurrentUser()
        {
            var user = Current.User();
            return await Task.FromResult(FormattedJSON(new {Id = user.Id, Email = user.Email, CustomerId = user.CustomerId, EmployerId = user.EmployerId, EmployeeId = user.EmployeeId }));
        }

        /// <summary>
        /// Get the current employer for the user
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("current/employer")]
        public async Task<object> GetCurrentEmployer()
        {
            var employer = Current.Employer();
            if (employer != null)
            {
                return await Task.FromResult(FormattedJSON(new { Id = employer.Id, Name = employer.Name }));
            }
            return await Task.FromResult<object>(null);
        }

        /// <summary>
        /// Get the current customer of the user
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("current/customer")]
        public async Task<object> GetCurrentCustomer()
        {
            var customer = Current.Customer();
            return await Task.FromResult(FormattedJSON(new { Id= customer.Id, customer.Name}));
        }

        /// <summary>
        /// Get the teams for current user
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("current/team")]
        public async Task<object[]> GetTeamForCurrentUser()
        {
            using (TeamService svc = new TeamService(CurrentUser))
            {
                var team = svc.GetTeamsForUser();
                if (team != null && team.Count > 0)
                {
                    return await Task.FromResult(team.Select(t => FormattedJSON(new { Id = t.Id, t.Name })).ToArray());
                }
                return await Task.FromResult<object[]>(null);
            }
        }

        /// <summary>
        /// Returns the host of the component
        /// </summary>
        /// <param name="componentName"></param>
        /// <returns></returns>
        [HttpGet, Route("host/{component}"), OutputCache(Duration = 86400, VaryByParam = "component", Location = OutputCacheLocation.Client, NoStore = true)]
        public async Task<string> GetHost(string component)
        {
            return await Task.FromResult(EndPointManager.Instance.GetHostForComponent(component));
        }

        /// <summary>
        /// Calls a service endpoint
        /// </summary>
        /// <param name="component"></param>
        /// <param name="endPoint"></param>
        /// <param name="data"></param>
        /// <param name="method"></param>
        /// <returns></returns>
        [HttpPost, Route("service/execute")]
        public async Task<dynamic> CallService(ExecuteArgs args)
        {
            var token = Utilities.GetTokenFromClaims();
            if (!string.IsNullOrWhiteSpace(token)) {
                ServiceFactory factory = new ServiceFactory(token);
                return await factory.CallServiceAsync(args.Component, args.EndPoint, args.PostedData, args.Method);
            }
            else
            {
                return await Task.FromResult<object>(null);
            }
        }

        [HttpGet, Route("schedule/download")]
        [Secure]
        public FileResult DownloadScheduledReport(string file)
        {
            //decrypt the file
            var filePath = Crypto.Decrypt(file.Replace(' ', '+'));            
            byte[] resultFile=null;
            string fileType = "application/octet-stream";
            string fileName = "NoData.txt";
            if (!string.IsNullOrWhiteSpace(filePath))
            {
                var ext = filePath.Split('.');
                if (ext.Length >= 2)
                {
                    fileName = string.Format("{0:yyyy-MM-dd} - {1}{2}", DateTime.Today, "Report.", ext[1]);
                }
                using (FileService fileService = new FileService())
                {
                    resultFile = fileService.DownloadFile(filePath, Settings.Default.S3BucketName_Report_Files);
                }
                return File(resultFile, fileType, string.Format("{0:yyyy-MM-dd} - {1}", DateTime.Today, fileName));
            }
        
            return File(new byte[] { 0x20 }, fileType, string.Format("{0:yyyy-MM-dd} - {1}", DateTime.Today, fileName));
        }

        [HttpPost, Route("service/adhoc/download",Name = "DownloadAdhocReport")]
        [Secure(Data.Enums.Feature.ReportingTwoBeta, "ExportReportDefinition" )]
        public async Task<FileResult> DownloadAdhocReport(string adhocId)
        {
            //get the report definition
           var endPoint = @"reports/adhoc/user/savedreport/?id=" + adhocId;
            var args= new ExecuteArgs();
            args.Component = "Reporting";
            args.EndPoint = endPoint;
            args.Method = "Get";
            JObject report =await CallService(args);
            var result = report.GetValue("data").ToJSON();
            //encrypt the report definition
            result= AT.Common.Security.CryptoStatic.Encrypt(result);
            //Return file
            var bytes = Encoding.UTF8.GetBytes(result);
            var fileName = report.GetValue("data")?.Value<string>("name");
            return File(bytes, "text/plain", fileName == null ? "Export": fileName + ".report");
        }

        [HttpPost, Route("service/adhoc/upload", Name = "UploadAdhocReport")]
        [Secure(Data.Enums.Feature.ReportingTwoBeta, "ImportReportDefinition")]
        public async Task UploadAdhocReport()
        {
            var file = Request.Files.Count>0? Request.Files[0]:null;
            if (file?.ContentLength > 0)
            {
                byte[] uploadedFile = new byte[file.InputStream.Length];
                file.InputStream.Read(uploadedFile, 0, uploadedFile.Length);
                var result = AT.Common.Security.CryptoStatic.Decrypt(uploadedFile);
                //Convert to column definition
                ReportDefinition adhocReport = JsonConvert.DeserializeObject<ReportDefinition>(result);
                ReportColumnDefinition rcd = MapToReportColumnDefinition(adhocReport);
                SaveReportArgs argsData = new SaveReportArgs();
                argsData.ColumnDefinition = rcd;
                //Call to save
                var endPoint = @"reports/adhoc/save";
                var args = new ExecuteArgs();
                args.Component = "Reporting";
                args.EndPoint = endPoint;
                args.Method = "Post";
                args.PostedData = argsData.ToJSON();
                JObject report = await CallService(args);              
            }           
        }
        public JavaScriptResult Display(string message)
        {
            var script = " noty({ type: 'success', text: '"+ message + "' });";
            return JavaScript(script);
        }

        private ReportColumnDefinition MapToReportColumnDefinition(ReportDefinition newDefinition)
        {
            ReportColumnDefinition reportColumnDefinition = new ReportColumnDefinition();
            reportColumnDefinition.Name = newDefinition.Name;
            reportColumnDefinition.AdHocReportType = newDefinition.ReportType.AdHocReportType;            
            reportColumnDefinition.Active = true;
            if (reportColumnDefinition.Fields == null)
            {
                reportColumnDefinition.Fields = newDefinition.Fields.Select(f => new AdHocReportField { Name = f.Name, Order = f.Order }).ToList();
            }
            if (reportColumnDefinition.Filters == null)
            {
                reportColumnDefinition.Filters = newDefinition.Filters?.Select(f => new AdHocReportFilter { FieldName = f.FieldName, Value = f.Value.ToString(), OperatorType = (OperatorType)int.Parse(f.Operator) }).ToList();
            }

            return reportColumnDefinition;
        }

        /// <summary>
        /// Call the Service to get the file path
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        [HttpGet, Route("service/download")]
        public FileResult CallFileService(ExecuteArgs args)
        {
            byte[] resultFile= null; 
            var token = Utilities.GetTokenFromClaims();
            string output = string.Empty;
            string fileType = "application/octet-stream";
            string fileName = "NoData.txt";
            if (!string.IsNullOrWhiteSpace(token))
            {
                ServiceFactory factory = new ServiceFactory(token);
                output = factory.CallFileService(args.Component, args.EndPoint, args.PostedData, args.Method);
                if (!string.IsNullOrWhiteSpace(output))
                {
                    var ext= output.Split('.');
                    if(ext.Length>=2)
                    {
                        fileName = string.Format("{0:yyyy-MM-dd} - {1}{2}", DateTime.Today, "Report.",ext[1]);
                    }                 

                    using (FileService fileService = new FileService())
                    {
                        resultFile = fileService.DownloadFile(output, Settings.Default.S3BucketName_Report_Files);
                    }
                    return File(resultFile, fileType, string.Format("{0:yyyy-MM-dd} - {1}", DateTime.Today, fileName));
                }
            }
            return File(new byte[] { 0x20 }, fileType, string.Format("{0:yyyy-MM-dd} - {1}", DateTime.Today, fileName));
        }

        /// <summary>
        /// Class to encapsulate parameters
        /// </summary>
        public class ExecuteArgs
        {
            /// <summary>
            /// The name of the component we are trying to access
            /// </summary>
            public string Component { get; set; }

            /// <summary>
            /// The endpoint
            /// </summary>
            public string EndPoint { get; set; }

            /// <summary>
            /// The string Data
            /// </summary>
            public string PostedData { get; set; }

            /// <summary>
            /// The Http Method
            /// </summary>
            public string Method { get; set; }

        }

        /// <summary>
        /// Converts and object to formatted JSON
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private static object FormattedJSON(object obj)
        {
            try
            {
                return JsonConvert.SerializeObject(obj, new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });
            }
            catch
            {
                return null;
            }
        }
        #endregion
    }
}