﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Models.PolicyCrosswalk;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Controllers
{
    
    public class PolicyCrosswalkController : BaseController
    {
        [Secure("EditPolicyCrosswalkTypes")]
        [Title("View Policy Crosswalk Types")]
        [HttpGet, Route("PolicyCrosswalk/List/{employerId?}", Name = "ViewPolicyCrosswalkTypes")]
        public ActionResult ViewPolicyCrosswalkTypes(string employerId)
        {
            return View();
        }

        [Secure("EditPolicyCrosswalkTypes")]
        [HttpPost, Route("PolicyCrosswalk/ListPolicyCrosswalkTypes", Name ="ListPolicyCrosswalkTypes")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListPolicyCrosswalkTypes(ListCriteria criteria)
        {
            using (var policyService = new PolicyService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                ListResults results = policyService.PolicyCrosswalkList(criteria);
                return PartialView(results);
            }
        }

        [Secure("EditPolicyCrosswalkTypes")]
        [Title("Edit Policy Crosswalk Type")]
        [HttpGet, Route("PolicyCrosswalk/EditPolicyCrosswalkType/{employerId?}", Name = "EditPolicyCrosswalkType")]
        public ActionResult EditPolicyCrosswalkType(string employerId, string id)
        {
            using (var policyService = new PolicyService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                PolicyCrosswalkTypeViewModel viewModel = new PolicyCrosswalkTypeViewModel(policyService.GetPolicyCrosswalkTypeById(id));
                return View(viewModel);
            }
        }

        [Secure("EditPolicyCrosswalkTypes")]
        [Title("Edit Policy Crosswalk Type")]
        [HttpPost, Route("PolicyCrosswalk/SavePolicyCrosswalkType", Name = "SavePolicyCrosswalkType")]
        [ValidateAntiForgeryToken]
        public ActionResult SavePolicyCrosswalkType(PolicyCrosswalkTypeViewModel policyCrosswalkType)
        {
            if (!ModelState.IsValid)
                return PartialView("EditPolicyCrosswalkType", policyCrosswalkType);

            using (var policyService = new PolicyService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                PolicyCrosswalkType type = policyCrosswalkType.ApplyToDataModel(policyService.GetPolicyCrosswalkTypeById(policyCrosswalkType.Id));
                policyService.SavePolicyCrosswalkType(type);
            }

            return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewPolicyCrosswalkTypes"));
        }

        [Secure("EditPolicyCrosswalkTypes")]
        [HttpDelete, Route("PolicyCrosswalk/DeletePolicyCrosswalkType/{employerId?}", Name = "DeletePolicyCrosswalkType")]
        public ActionResult DeletePolicyCrosswalkType(string employerId, string id)
        {
            using (var policyService = new PolicyService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                PolicyCrosswalkType type = policyService.GetPolicyCrosswalkTypeById(id);
                if (type.IsCustom)
                    policyService.DeletePolicyCrosswalkType(type);

                return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewPolicyCrosswalkTypes"));
            }
        }

        [Secure("EditPolicies")]
        [HttpGet, Route("PolicyCrosswalk/GetAllCrosswalkTypes/{employerId?}", Name ="GetAllCrosswalkTypes")]
        public ActionResult GetAllPolicyCrosswalkTypes(string employerId)
        {
            using (var policyService = new PolicyService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                List<PolicyCrosswalkTypeViewModel> allTypes = policyService.GetAllPolicyCrosswalkTypes()
                    .Select(pct => new PolicyCrosswalkTypeViewModel(pct)).ToList();

                return Json(allTypes, JsonRequestBehavior.AllowGet);
            }
        }
    }
}