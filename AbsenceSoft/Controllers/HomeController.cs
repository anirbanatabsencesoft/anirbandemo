﻿using AbsenceSoft;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Models.Home;
using System;
using System.Linq;
using System.Web.Mvc;
using AbsenceSoft.Models.Signup;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Web.Attributes;
using AT.Logic.Authentication;
using System.Web;
using System.Threading.Tasks;
using AT.Entities.Authentication;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using System.IdentityModel.Tokens;
using AbsenceSoft.Data.Audit;
using AbsenceSoft.Web;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    public class HomeController : BaseController
    {
        private ApplicationSignInManager _signInManager;
        public HomeController()
        {
            
        }
        public HomeController(ApplicationSignInManager applicationSignInManager)
        {
            _signInManager = applicationSignInManager;
        }

        /// <summary>
        /// Gets the new object of ApplicationSignInManager.
        /// </summary>
        /// <value>
        /// The sign in manager.
        /// </value>
        protected ApplicationSignInManager SignInManager
        {
            get
            {
                if (_signInManager == null)
                    _signInManager = new ApplicationSignInManager(HttpContext.GetOwinContext().Authentication);
                return _signInManager;
            }
        }

        [Secure, Title("Dashboard")]
        [Route("", Name = "Home")]
        public ActionResult Index()
        {
            var todoItemStatuses = (from ToDoItemStatus ct in Enum.GetValues(typeof(ToDoItemStatus))
                                    select new { Id = (int)ct, Text = ct.ToString() }).ToList();
            todoItemStatuses.Insert(0, new { Id = 0, Text = "Filter" });
            ViewBag.WorkflowItemStatuses = new SelectList(todoItemStatuses, "Id", "Text");
            if (AbsenceSoft.Data.Security.User.Current != null && AbsenceSoft.Data.Security.User.Current.Employers.Count > 0)
            {
                ViewBag.CurrentUserEmployeeId = AbsenceSoft.Data.Security.User.Current.Employers[0].EmployeeId;
                if (AbsenceSoft.Data.Security.User.Current.Employers[0].Employee != null)
                {
                    ViewBag.CurrentUserEmployeeName = AbsenceSoft.Data.Security.User.Current.Employers[0].Employee.FullName;
                    ViewBag.CurrentUserEmployeeNumber = AbsenceSoft.Data.Security.User.Current.Employers[0].Employee.EmployeeNumber;
                }
            }
            else
                ViewBag.CurrentUserEmployeeId = null;
            return View();
        }

        [ChildActionOnly, Route("Logo/{employerId?}", Name = "Logo")]
        public ActionResult Logo(string employerId, bool includeHomeLink = true)
        {
            LogoModel logo = new LogoModel()
            {
                IncludeHomeLink = includeHomeLink
            };
            
            /// if we have current employer and they have more than one
            /// we display the employer logo
            if (CurrentEmployer != null && Employer.AsQueryable().Count(e => e.CustomerId == CurrentCustomer.Id) > 1 && !string.IsNullOrEmpty(CurrentEmployer.LogoId))
            {
                using (var employerFileService = new EmployerFileService())
                {
                    logo.LogoUrl = employerFileService.DownloadFile(CurrentEmployer.Id, CurrentEmployer.LogoId);
                }
                logo.LogoName = CurrentEmployer.Name;
            }
            else if (CurrentCustomer != null && !string.IsNullOrEmpty(CurrentCustomer.LogoId))
            {
                using (var customerFileService = new CustomerFileService())
                {
                    logo.LogoUrl = customerFileService.DownloadFile(CurrentCustomer.Id, CurrentCustomer.LogoId);
                }
            }

            if (string.IsNullOrEmpty(logo.LogoUrl))
                logo.LogoUrl = Server.MapPath("~/Content/Images/AbsenceSoft-logo-200.png"); ;

            if (string.IsNullOrEmpty(logo.LogoName))
                logo.LogoName = CurrentCustomer.Name;

            return PartialView("_Logo", logo);
        }

        [Title("Login"), HttpGet]
        [Route("Login", Name = "Login")]
        public ActionResult Login()
        {
            if (User != null && User.Identity != null)
            {
                if ((User.Identity is ClaimsIdentity identity))
                {
                    var token = identity.FindFirstValue("token");
                    if (token != null)
                    {
                        JwtSecurityToken jwtToken = new JwtSecurityToken(token);
                        if (Request != null && jwtToken.ValidTo.ToUniversalTime().ToUnixDate() >= DateTime.UtcNow.ToUniversalTime().ToUnixDate())
                        {
                            return RedirectToAction("Logout");
                        }
                    }
                }
            }
                        

#if DEMO || STAGE
            ViewBag.ShowLowerEnvironmentWarning = true;
#else
            ViewBag.ShowLowerEnvironmentWarning = false;
#endif

            bool isShow = false;

            if (Request != null)
                if (!string.IsNullOrEmpty(Request.QueryString["showLogoutWarning"]))
                    isShow = Convert.ToBoolean(Request.QueryString["showLogoutWarning"]);

            if(CustomerLoginPageIsDisabled(CurrentCustomer))
            {
                return View("DisabledLoginPage");
            }

            if (isShow)
            {
                ViewBag.Message = "Due to inactivity, you have been logged out for security reasons.";
            }
            else if (Request != null)
            {
                ViewBag.Message = Request.QueryString["error"] ?? "";
            }

            using (var ssoService = new SsoService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                LoginModel lvm = new LoginModel(ssoService.GetProfile(true));
                return View(lvm);
            }
        }

        private bool CustomerLoginPageIsDisabled(Customer customer)
        {
            return customer != null && customer.SecuritySettings != null && customer.SecuritySettings.DisableLoginPage;
        }

     

        [HttpPost, Title("Login")]
        [Route("Login", Name = "LoginPost")]
        public async Task<ActionResult> Login(LoginModel model)
        {
            if (Request != null) ViewBag.TermsConditions = (Request.Form["chkLogin"] == null ? false : true);
            if (ModelState.IsValid)
            {
                try
                {
                    var justChangedPassword = (ViewBag.JustChangedPassword == null? false : Convert.ToBoolean(ViewBag.JustChangedPassword));
                    if (!justChangedPassword && Request != null && (ViewBag.TermsConditions == null || (!ViewBag.TermsConditions)))
                    {     
                        ViewBag.LoginError = "Please accept the terms and conditions above to proceed.";
                        return View(model);
                    }
                    
                    SignInStatus result = await SignInManager.SignInAsync(model.Email, model.Password, ApplicationType.Portal);
                  
                    AbsenceSoft.Data.Security.User user = null;
                    using (var service = new AdministrationService())
                    {
                        user = service.GetUserByEmail(model.Email.ToLowerInvariant());
                    }

                    if (user!=null && CustomerLoginPageIsDisabled(user.Customer))
                        return View("DisabledLoginPage");                    
                 
                    if (result == SignInStatus.Ok)
                    {
                        AbsenceSoft.Web.AuditLoginHelper.AuditLogin(user, model.Email); 
                        using (var service = new AuthenticationService())
                        {
                            //loads contactsOf if EmployeeSelfService Role assigned. 
                            service.ValidEmployeeSelfServiceLogin(user);

                            // Save user for LastActivity Date
                            if (user != null)
                            {
                                user.LastActivityDate = DateTime.Now;
                                user.Save();
                            }                           
                        }
                        string url = Request.GetRelativeReturnUrl();

                        if (string.IsNullOrWhiteSpace(url))
                            return RedirectToRoute("Home");
                        else
                            return Redirect(url);
                    }
                    else if (result == SignInStatus.Error)
                    {
                        AbsenceSoft.Web.AuditLoginHelper.AuditLoginFailure(user, model.Email, LoginType.Failed);
                        ViewBag.LoginError = "Your login attempt has failed. The username and/or password may be incorrect. If you feel your account is properly registered with us, click on \"Forgot Password\" below to retrieve your account details.";
                        return View(model);
                    }
                    else if (result == SignInStatus.Locked)
                    {
                        AbsenceSoft.Web.AuditLoginHelper.AuditLoginFailure(user, model.Email, LoginType.Locked);
                        // ProcessFailedLoginAttempt
                        using (AuthenticationService auth = new AuthenticationService())
                        {
                            // Send create account email to user
                            auth.ResetLockedPassword(user); 
                        }
                        ViewBag.LoginError = "Your account has been locked for security reasons. An email has been sent to your designated email address with instructions on how to unlock your account. If you are still unable to log in, please contact your AbsenceSoft Support Team.";
                        return View(model);
                    }
                    else if (result == SignInStatus.Expired)
                    {
                        AbsenceSoft.Web.AuditLoginHelper.AuditLoginFailure(user, model.Email, LoginType.Expired);
                        return RedirectToAction("ChangePassword", "Home");
                    }
                    else if (result == SignInStatus.Disabled)
                    {
                        AbsenceSoft.Web.AuditLoginHelper.AuditLoginFailure(user, model.Email, LoginType.Disabled);

                        AccountSummaryVM modelAS = new AccountSummaryVM()
                        {
                            Name = user.FirstName + " " + user.LastName,
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            CompanyName = user.Customer.Name,
                            Address1 = user.Customer.Contact.Address.Address1,
                            Address2 = user.Customer.Contact.Address.Address2,
                            City = user.Customer.Contact.Address.City,
                            State = user.Customer.Contact.Address.State,
                            PostalCode = user.Customer.Contact.Address.PostalCode,
                            Country = user.Customer.Contact.Address.Country,
                            PhoneNumber = user.Customer.Contact.WorkPhone,
                            Email = user.Email,                         
                            CustomerId = user.CustomerId,
                            IsLogin = true
                        };
                        return View("AccountSummary", modelAS);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("cannot login the user", ex);
                }
            }

            return View(model);
        }

        [Route("Logout", Name = "Logout")]
        public async Task<ActionResult> Logout()
        {
            try
            {
                // Save user for LastActivity Date
                var user = Data.Security.User.Current;
                if (user != null)
                {
                    user.LastActivityDate = DateTime.Now;
                    user.Save();
                }
                AbsenceSoft.Web.AuditLoginHelper.AuditLogout(CurrentUser, CurrentUser.Email);

                await SignInManager.SignOutAsync();                

                if (Request.Cookies.AllKeys.Contains("_siteLocation"))
                {
                    Response.AppendCookie(new HttpCookie("_siteLocation", "false") { Expires = DateTime.Now.AddDays(-1), Path = "/" });
                }
            }
            catch (Exception ex)
            {
                Log.Error("cannot logout the user", ex);
            }

            return Redirect("~/logout.html");
        }

        [HttpGet, Route("ChangePassword")]
        public ActionResult ChangePassword()
        {
            var model = new ChangePasswordModel();
            var user = Data.Security.User.Current;
            if (user != null)
            {
                model.UserId = user.Email;
            }
            using (var svc = new AuthenticationService())
            {
                svc.Logout(HttpContext);
            }
            return View(model);
        }

        [HttpPost, Route("ChangePassword")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordModel model, FormCollection form)
        {

            if (!ModelState.IsValid)
            {
                model.NewPassword = null;
                model.ConfirmNewPassword = null;
                ModelState.AddModelError(string.Empty, "Form submission errors were found. Please review and complete all fields and try again.");
                return View(model);
            }
            try
            {
                using (var adminSvc = new AdministrationService())
                {
                    var user = Data.Security.User.AsQueryable()
                        .SingleOrDefault(u => u.Email == model.UserId);
                    if (user == null)
                    {
                        throw new AbsensesoftAuthenticationError();
                    }
                    if (!adminSvc.CheckPassword(user, model.CurrentPassword))
                    {
                        throw new AbsensesoftAuthenticationError();
                    }

                    if (!adminSvc.ChangePassword(user, model.NewPassword))
                    {
                        throw new Exception("Changed password failed for UserId: " + user.Id);
                    }

                    user.MustChangePassword = false;
                    user.Save();

                    ViewBag.JustChangedPassword = true;

                    return await Login(new LoginModel
                    {
                        Email = model.UserId,
                        Password = model.NewPassword
                    });
                }
            }
            catch (AbsenceSoftException ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View(model);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                ModelState.AddModelError(string.Empty, ErrorMessages.UnhandledException);
                return View(model);
            }
        }

        [Title("Terms and Conditions")]
        public ViewResult TermsAndConditions()
        {
            return View();
        }


        #region Forgot Password

        [HttpPost, Route("ForgotPassword", Name = "ForgotPassword")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ForgotPassword(string emailAddress)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
                return Json(new { success = false, error = "Email address is required to reset password" });

            try
            {
                return Json(new { success = new AuthenticationService().Using(s => s.ForgotPassword(emailAddress)) });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, error = ex.Message });
            }
        }

        [HttpGet, Route("ResetPassword/{resetKey}", Name = "ResetPassword")]
        public ActionResult ResetPassword(string resetKey)
        {
            if (string.IsNullOrWhiteSpace(resetKey))
                AddError("Reset key was not provided or was in an invalid format");

            return View();
        }

        [HttpPost, Route("ResetPassword/{resetKey}", Name = "ResetPasswordSubmit")]        
        public ActionResult ResetPasswordSubmit(string resetKey, string newPassword, string confirmPassword)
        {
            if (string.IsNullOrWhiteSpace(resetKey))
                return JsonError(new Exception("Reset key is required and was not provided or was not in the correct format"), "Reset key is required and was not provided or was not in the correct format", 400);

            if (string.IsNullOrWhiteSpace(newPassword) || string.IsNullOrWhiteSpace(confirmPassword))
                return JsonError(new Exception("Password and confirm password are required to reset your password"), "Password and confirm password are required to reset your password", 400);

            if (newPassword != confirmPassword)
                return JsonError(new Exception("Passwords must match"), "Passwords must match", 400);

            return this.Try(() =>
            {
                new AuthenticationService().Using(s => s.ResetPassword(resetKey, newPassword));
            });
        }

        [HttpGet, Route("SuccessResetPassword", Name = "SuccessResetPassword")]
        public ActionResult SuccessResetPassword()
        {
            return View();
        }

        #endregion


        [Title("Unauthorized")]
        public ActionResult Unauthorized()
        {
            return View();
        }
    }
}