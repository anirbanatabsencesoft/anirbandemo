﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Models;
using AbsenceSoft.Models.Attachments;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    [Secure]
    public class AttachmentsController : BaseController
    {
        [Title("Inquiry Attachments"), Secure("ViewAttachment")]
        [Route("Inquiries/{caseId}/Attachments", Name = "InquiryAttachments")]
        public ViewResult InquiryIndex(string caseId)
        {
            ViewBag.Status = "Inquiry";
            return Index(caseId);
        }

        [Title("Case Attachments"), Secure("ViewAttachment")]
        [Route("Cases/{caseId}/Attachments", Name = "CaseAttachments")]
        public ViewResult CaseIndex(string caseId)
        {
            ViewBag.Status = "";
            return Index(caseId);
        }

        private ViewResult Index(string caseId)
        {
            var attachmentTypes = (from AttachmentType at in Enum.GetValues(typeof(AttachmentType))
                                   select new { Id = (int)at, Text = at.ToString() }).ToList();
            ViewBag.AttachmentTypes = new SelectList(attachmentTypes, "Id", "Text");

            if (String.IsNullOrEmpty(caseId))
            {
                throw new ArgumentNullException("caseId");
            }
            Case myCase = Case.GetById(caseId);
            if (myCase == null)
            {
                throw new ArgumentException("Case not found");
            }
            var model = new CaseParentedModel()
            {
                CaseId = caseId,
                Status = ViewBag.Status,
                EmployeeId = myCase.Employee.Id,
                EmployerId = myCase.EmployerId
            };
            ViewBag.CreationDate = DateTime.Now.ToUIString();
            return View("Index", model);
        }

        [Secure("ViewAttachment"), HttpGet]
        [Route("Cases/{caseId}/Attachments/List", Name = "CaseAttachmentsList")]
        [ValidateApiAntiForgeryToken]
        public JsonResult List(string caseId)
        {
            try 
            {

            if (String.IsNullOrEmpty(caseId))
                throw new ArgumentNullException("caseId");

            var attachments = Attachment.AsQueryable().ToList(o => o.CaseId == caseId).Select(
                o => new
                {
                    Id = o.Id,
                    FileId = o.FileId,
                    FileName = o.FileName,
                    AttachmentType = o.AttachmentType,
                    AttachmentTypeName = o.AttachmentType.ToString(), // make sure this field is returned in the list, otherwise it will not display the list properly in view.
                    ContentLength = o.ContentLength,
                    ContentType = o.ContentType,
                    Description = o.Description,
                    CreatedDate = o.CreatedDate
                }).OrderBy(o => o.AttachmentType).ThenByDescending(o => o.CreatedDate);

                    return this.Json(attachments, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        
        }

        // NOTE: You can post the form just like any other form, but if you include the File input type in the form,
        //       MVC will automatically map the form controls to the ViewModel and the posted file to the posted file base
        //       parameter for you.
        [HttpPost, Secure("AttachFile")]
        [Route("Cases/{caseId}/Attachments/Create", Name = "CreateCaseAttachment")]
        [ValidateApiAntiForgeryToken]
        public JsonResult CreateCaseAttachment(CreateAttachmentViewModel viewModel, HttpPostedFileBase caseAttachments)
        {
            try 
            { 

            if (String.IsNullOrEmpty(viewModel.Id))
                throw new ArgumentNullException("caseId");

            if (caseAttachments == null || caseAttachments.ContentLength <= 0)
                throw new ArgumentNullException("file");

            Case myCase = Case.GetById(viewModel.Id);
            if (myCase == null)
                throw new ArgumentException("Case not found");

            var attachment = new AttachmentService().Using(a => a.CreateAttachment(myCase.Employee.Id, myCase.Id, viewModel.AttachmentType, viewModel.Description, caseAttachments, viewModel.Public));

            // We need this information to be returned.
            return this.Json(new
            {
                Message = "Success",
                Attachment = new
                {
                    Id = attachment.Id,
                    FileId = attachment.FileId,
                    FileName = attachment.FileName,
                    CaseId = attachment.CaseId,
                    AttachmentType = attachment.AttachmentType,
                    AttachmentTypeName = attachment.AttachmentType.ToString(),
                    ContentLength = attachment.ContentLength,
                    ContentType = attachment.ContentType,
                    Description = attachment.Description,
                    CreatedDate = DateTime.Now,
                    AttachmentId = viewModel.AttachmentId, // our javascript code depends on this field for unique identification of newly added attachment.
                    AttachedBy = CurrentUser.DisplayName
                }
            });

            // OK, the attachment above will be the actual file update result, which is super cool.
            //  From here, in the UI, you should create a CommunicationPaperwork object for any attachments, you have to prompt the applicable
            //  fields though for the CommunicationPaperwork models, there will be a field, AttachmentId, for this attachment's "Id".
            //  But there may also be things they need to do for this attachment, such as expect a response back (due date, etc.)
            //return this.Json(new { Message = "Success", Attachment = attachment });

            }
                catch (Exception ex)
                {
                    return JsonError(ex);
                }
            }


        [Secure("ViewAttachment")]
        [Route("Cases/{caseId}/Attachments/{attachmentId}", Name = "DownloadAttachment")]
        public ActionResult DownloadAttachment(string caseId, string attachmentId)
        {
            using (AttachmentService svc = new AttachmentService())
            {
                var attachment = svc.GetAttachment(attachmentId);
                if (attachment == null || attachment.CaseId != caseId || string.IsNullOrWhiteSpace(attachment.FileId))
                    return HttpNotFound("Attachment or file not found for this case");
                
                attachment = svc.DownloadAttachment(attachment);

                if (attachment.File == null || attachment.File.LongLength == 0)
                    return HttpNotFound("Attachment or file not found for this case");

                return File(attachment.File, attachment.ContentType, attachment.FileName);
            }
        }

        [Secure("DeleteAttachment"), HttpDelete]
        [Route("Cases/{caseId}/Attachments/{attachmentId}", Name = "DeleteAttachment")]
        [ValidateApiAntiForgeryToken]
        public JsonResult DeleteAttachment(string caseId, string attachmentId)
        {
            try
            {
                using (AttachmentService svc = new AttachmentService())
                {
                    var attachment = svc.GetAttachment(attachmentId);
                    if (attachment == null || attachment.CaseId != caseId)
                        return JsonError(null, "Attachment or file not found for this case", 404);

                    attachment.Delete();

                    return Json(new { success = true, message = "Attachment successfully deleted" });
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpPost, Secure("EditAttachment")]
        [Route("Cases/{caseId}/ModifyCaseAttachmentRequest", Name = "ModifyCaseAttachmentRequest")]
        [ValidateApiAntiForgeryToken]
        public JsonResult ModifyCaseAttachmentRequest(string caseId, string attachmentId, string fileName, string description)
        {
            try
            {
                using (AttachmentService svc = new AttachmentService())
                {
                    var attachment = svc.GetAttachment(attachmentId);
                    if (attachment == null || attachment.CaseId != caseId || String.IsNullOrEmpty(fileName))
                        return JsonError(null, "Attachment or file not found for this case", 404);

                    var origExt = Path.GetExtension(attachment.FileName);
                    var newExt = Path.GetExtension(fileName);
                    if(origExt!= newExt)
                    {
                        fileName = String.Concat(fileName, origExt);
                    }
                    attachment.FileName = fileName;
                    attachment.Description = description;
                    attachment.Save();

                    return Json(new { success = true, message = "Attachment successfully updated" });
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("ViewAttachment"), HttpPost]
        [Route("Attachments/List", Name = "AttachmentsList")]
        [ValidateApiAntiForgeryToken]
        public JsonResult List(ListCriteria criteria)
        {
            try
            {
                //string CaseId = RouteData.Values["CaseId"].ToString();
                //criteria.Set("CaseId", CaseId);
                AttachmentService obj = new AttachmentService();
                return Json(obj.AttachmentList(criteria), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }


        }

        [Secure("ViewAttachment")]
        [Route("Cases/Paperwork/{caseId}/Attachments/{attachmentId}", Name = "DownloadPaperworkAttachment")]
        public ActionResult DownloadPaperworkAttachment(string caseId, string attachmentId)
        {
            using (AttachmentService svc = new AttachmentService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                var attachment = svc.GetAttachmentById(caseId, attachmentId);
                if (attachment == null || attachment.CaseId != caseId || string.IsNullOrWhiteSpace(attachment.FileId))
                    return HttpNotFound("Attachment or file not found for this case");

                attachment = svc.DownloadAttachment(attachment);

                if (attachment.File == null || attachment.File.LongLength == 0)
                    return HttpNotFound("Attachment or file not found for this case");

                return File(attachment.File, attachment.ContentType, attachment.FileName);
            }
        }
    }
}