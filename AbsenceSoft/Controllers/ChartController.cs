﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Chart;
using AbsenceSoft.Web.Attributes;
using System.Collections.Generic;
using System.Web.Mvc;

namespace AbsenceSoft.Controllers
{
    [Secure]
    public class ChartController : BaseController
    {
        public JsonResult GetCaseLoadData(VisibilityListType viewType)
        {
            using (var chartService = new ChartService())
            {
                List<object[]> data = chartService.GetCaseLoadData(viewType);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetOpenToDosData(VisibilityListType viewType)
        {
            using (var chartService = new ChartService())
            {
                List<object[]> data = chartService.GetOpenToDosData(viewType);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetOverdueToDosData(VisibilityListType viewType)
        {
            using (var chartService = new ChartService())
            {
                List<object[]> data = chartService.GetOverdueToDosData(viewType);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }
    }
}