﻿using AbsenceSoft.Data;
using AbsenceSoft.Logic;
using AbsenceSoft.Models;
using AbsenceSoft.Filters;
using System.Web.Mvc;
using System.Text;
using AbsenceSoft.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbsenceSoft.Models.Cases;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Models.Contacts;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Data.Notes;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using AbsenceSoft.Logic.Tasks;
using MongoDB.Driver;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    [Secure]
    public class AccommodationsController : BaseController
    {
        [HttpGet, Secure("ADADetail")]
        [Route("Accommodations/Employee/{employeeId}/GetEmployeeAccommodations", Name = "GetEmployeeAccommodations")]
        public JsonResult GetEmployeeAccommodations(string employeeId)
        {
            try
            {
                var requests = new AccommodationService().GetEmployeeAccommodations(employeeId);
                if (requests.Any())
                    return Json(MakeViewModel(requests));
                else
                    return Json(new AccommodationRequestViewModel() { HasAnyRequests = false });
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error Getting Accommodations for Employee");
            }
        }

        [HttpGet, Secure("ADADetail", "ViewCase")]
        [Route("Accommodations/{caseId}/GetCaseAccommodations", Name = "GetCaseAccommodations")]
        public JsonResult GetCaseAccommodations(string caseId)
        {
            try
            {
                if (string.IsNullOrEmpty(caseId))
                    return JsonError(new Exception("CaseId not found"));

                Case c = Case.GetById(caseId);
                if (c == null)
                    return JsonError(new Exception("Case not found"));

                return Json(MakeAccommReqViewModel(c));
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error Getting Accommodations for Case");
            }
        }

        public AccommodationRequestViewModel MakeAccommReqViewModel(Case c)
        {
            AccommodationRequestViewModel viewModel = new AccommodationRequestViewModel();
            viewModel.CaseId = c.Id;

            if (c.IsAccommodation && c.AccommodationRequest != null)
            {
                viewModel.Status = c.AccommodationRequest.Status;
                viewModel.StartDate = c.AccommodationRequest.StartDate;
                viewModel.EndDate = c.AccommodationRequest.EndDate;
                viewModel.GeneralHealthCondition = c.AccommodationRequest.GeneralHealthCondition;
                viewModel.Description = c.AccommodationRequest.Description;
                viewModel.CancelReason = c.AccommodationRequest.CancelReason;
                viewModel.OtherDescription = c.AccommodationRequest.OtherReasonDesc;

                // Backfill any missing steps
                if (c.AccommodationRequest.AdditionalInfo != null)
                {
                    var addtlInfoSteps = new AccommodationService().Using(w => w.GetEmployerAccommodationInteractiveProcess(c.CustomerId, c.EmployerId, null, false)).Steps
                        .Where(step => !c.AccommodationRequest.AdditionalInfo.Steps.Any(s => s.QuestionId == step.QuestionId))
                        .ToList();
                    if (addtlInfoSteps.Any())
                        c.AccommodationRequest.AdditionalInfo.Steps.AddRange(addtlInfoSteps);
                }
                else
                    c.AccommodationRequest.AdditionalInfo = new AccommodationService().Using(w => w.GetEmployerAccommodationInteractiveProcess(c.CustomerId, c.EmployerId, null, false));
                viewModel.AdditionalQuestions = MakeSteps(c.AccommodationRequest.AdditionalInfo, c.AccommodationRequest.Id, c.Id);

                foreach (Accommodation a in c.AccommodationRequest.Accommodations.OrderByDescending(m => m.CreatedDate))
                {
                    // Backfill any missing steps
                    if (a.InteractiveProcess != null)
                    {
                        var steps = new AccommodationService().Using(w => w.GetEmployerAccommodationInteractiveProcess(c.CustomerId, c.EmployerId, a.Type.Code)).Steps
                            .Where(step => !a.InteractiveProcess.Steps.Any(s => s.QuestionId == step.QuestionId))
                            .ToList();
                        if (steps.Any())
                            a.InteractiveProcess.Steps.AddRange(steps);
                    }
                    else
                        a.InteractiveProcess = new AccommodationService().Using(w => w.GetEmployerAccommodationInteractiveProcess(c.CustomerId, c.EmployerId, a.Type.Code));

                    AccommodationViewModel avm = new AccommodationViewModel();
                    avm.Id = a.Id.ToString();
                    avm.StartDate = a.StartDate;
                    avm.EndDate = a.EndDate;
                    avm.Type = a.Type;
                    avm.Duration = a.Duration;
                    avm.Description = a.Description;
                    avm.Resolution = a.Resolution;
                    avm.Resolved = a.Resolved;
                    avm.ResolvedDate = a.ResolvedDate;
                    avm.Granted = a.Granted;
                    avm.GrantedDate = a.GrantedDate;
                    avm.Cost = a.Cost;
                    avm.AccommodationUsage = MakeUsageViewModel(a);
                    avm.InteractiveProcessGroups = MakeSteps(a.InteractiveProcess, a.Id, c.Id);
                    avm.Status = a.Status;
                    avm.CancelReason = a.CancelReason;
                    avm.OtherReasonDesc = a.OtherReasonDesc;
                    avm.IsWorkRelated = a.IsWorkRelated;
                    viewModel.Accommodations.Add(avm);
                }
            }
            viewModel.HasAnyRequests = (c.AccommodationRequest != null);
            viewModel.HasAnyAccommodations = viewModel.Accommodations.Any();

            EmployeeContact employeeContact = c.CasePrimaryProvider ?? new CaseService().GetCasePrimaryProviderContact(c);
            if (employeeContact != null && employeeContact.Contact != null)
            {
                viewModel.HCPContactId = employeeContact.Id.ToString();
                viewModel.HCPContactTypeCode = employeeContact.ContactTypeCode;
                viewModel.HCPCompanyName = employeeContact.Contact.CompanyName;
                viewModel.HCPFirstName = employeeContact.Contact.FirstName;
                viewModel.HCPLastName = employeeContact.Contact.LastName;
                viewModel.HCPPhone = employeeContact.Contact.WorkPhone;
                viewModel.HCPFax = employeeContact.Contact.Fax;
            }

            return viewModel;
        }

        [HttpPost, Secure("ADADetail")]
        [Route("Accommodations/{caseId}/CreateOrModifyAccommodationRequest", Name = "CreateOrModifyAccommodationRequest")]
        [ValidateApiAntiForgeryToken]
        public JsonResult CreateOrModifyAccommodationRequest(AccommodationRequestViewModel request)
        {
            try
            {
                if (string.IsNullOrEmpty(request.CaseId))
                    return JsonError(new Exception("CaseId not found"));

                Case c = Case.GetById(request.CaseId);
                if (c == null)
                    return JsonError(new Exception("Case not found"));
                
                List<Accommodation> accommList = new List<Accommodation>();

                if (c.AccommodationRequest != null && c.AccommodationRequest.Accommodations != null)
                {
                    foreach (Accommodation item in c.AccommodationRequest.Accommodations)
                    {
                        if (request.Accommodations.Count == 0)
                        {
                            item.Status = request.Status;
                            item.CancelReason = request.CancelReason.HasValue ? request.CancelReason.Value : (AccommodationCancelReason?)null; ;
                            if (request.CancelReason == AccommodationCancelReason.Other)
                                item.OtherReasonDesc = request.OtherDescription;
                            else
                                item.OtherReasonDesc = string.Empty;
                        }
                        accommList.Add(item);
                    }
                }

                //create HCP contact if entered as part of STD intake
                if (!string.IsNullOrEmpty(request.HCPContactId) && !request.HCPContactId.Equals("-1"))
                {
                    EmployeeContact provider = EmployeeContact.GetById(request.HCPContactId);

                    // if contact exists update details
                    if (provider != null)
                    {
                        provider.Contact.FirstName = request.HCPFirstName;
                        provider.Contact.LastName = request.HCPLastName;
                        provider.Contact.WorkPhone = request.HCPPhone;
                        provider.Contact.Fax = request.HCPFax;
                        provider.Contact.IsPrimary = request.IsPrimary;
                        provider.Save();

                        // set rest of the employee HCP contact IsPrimary false
                        if (request.IsPrimary)
                        {
                            using (var contactTypeService = new ContactTypeService())
                            {
                                contactTypeService.UpdateMedicalDesignationTypeContactsIsPrimary(CurrentUser.CustomerId, provider.Contact.Id, c.Employee.Id);
                            }
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(request.HCPFirstName) && !string.IsNullOrEmpty(request.HCPLastName))
                    {
                        var hcpContact = new EmployeeContact()
                        {
                            EmployerId = c.EmployerId,
                            CustomerId = CurrentCustomer.Id,
                            EmployeeId = c.Employee.Id,
                            EmployeeNumber = c.Employee.EmployeeNumber,
                            ContactTypeCode = request.HCPContactTypeCode,
                            
                            MilitaryStatus = MilitaryStatus.Civilian,
                            Contact = new Contact()
                            {
                                FirstName = request.HCPFirstName,
                                LastName = request.HCPLastName,
                                WorkPhone = request.HCPPhone,
                                Fax = request.HCPFax,
                                IsPrimary = request.IsPrimary
                            }
                        };
                        ContactType contactType = ContactType.GetByCode(request.HCPContactTypeCode);
                        if (contactType != null)
                        {
                            hcpContact.ContactTypeName = contactType.Code;
                        }
                        else
                        {
                            hcpContact.ContactTypeName = "Provider";
                            hcpContact.ContactTypeCode = "PROVIDER";
                        }
                            

                        //contact created as part of STD intake is primary provider
                        hcpContact.Save();

                        // set rest of the employee HCP contact IsPrimary false
                        if (request.IsPrimary)
                        {
                            using (var contactTypeService = new ContactTypeService())
                            {
                                contactTypeService.UpdateMedicalDesignationTypeContactsIsPrimary(CurrentUser.CustomerId, hcpContact.Contact.Id, c.Employee.Id);
                            }
                        }
                    }
                }

                AccommodationRequest theRequest = c.AccommodationRequest == null ? new AccommodationRequest() : c.AccommodationRequest;
                theRequest.GeneralHealthCondition = request.GeneralHealthCondition;
                theRequest.Description = request.Description;
                if (request.AdditionalQuestions != null && request.AdditionalQuestions.Count > 0)
                {
                    AccommodationInteractiveProcess AdditionalInfo = new AccommodationInteractiveProcess();

                    foreach (IntProcStepViewModel question in request.AdditionalQuestions.SelectMany(p => p.InteractiveProcessSteps))
                    {
                        AccommodationInteractiveProcess.Step additionalQue = c.AccommodationRequest.AdditionalInfo.Steps.Where(x => x.QuestionId.Equals(question.QuestionId)).FirstOrDefault() ??
                            c.AccommodationRequest.AdditionalInfo.Steps.AddFluid(new AccommodationInteractiveProcess.Step()
                            {
                                QuestionId = question.QuestionId,
                                Order = question.Order
                            });
                        additionalQue.Answer = question.Answer;
                        additionalQue.SAnswer = question.SAnswer;
                        additionalQue.Date = question.Date;
                        additionalQue.Time = question.Time;
                        additionalQue.Provider = question.Provider;
                        additionalQue.DateOfInitialDecision = question.DateOfInitialDecision;
                        additionalQue.DecisionOverturned = question.DecisionOverturned;

                        EmployeeContact newContact = new EmployeeContact();

                        if (question.Contact != null)
                        {
                            if (!string.IsNullOrEmpty(question.Contact.Id) && !question.Contact.Id.Equals("-1"))
                            {
                                newContact = EmployeeContact.GetById(question.Contact.Id);
                                //additionalQue.Contact = contact;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(question.Contact.Contact.FirstName) && !string.IsNullOrEmpty(question.Contact.Contact.FirstName))
                                {
                                    var contactType = new ContactType();
                                    if (!String.IsNullOrEmpty(question.Contact.ContactTypeCode))
                                        contactType = ContactType.GetByCode(question.Contact.ContactTypeCode);

                                    newContact = new EmployeeContact()
                                    {
                                        EmployerId = c.EmployerId,
                                        CustomerId = CurrentCustomer.Id,
                                        EmployeeId = c.Employee.Id,
                                        EmployeeNumber = c.Employee.EmployeeNumber,
                                        ContactTypeCode = contactType.Code,
                                        ContactTypeName = contactType.Name,
                                        MilitaryStatus = MilitaryStatus.Civilian,
                                        Contact = new Contact()
                                        {
                                            FirstName = question.Contact.Contact.FirstName,
                                            LastName = question.Contact.Contact.LastName,
                                        }
                                    };

                                    newContact.Save();
                                }
                            }
                        }


                        //create case note for int process question notes
                        if (!string.IsNullOrEmpty(question.Note))
                        {
                            using (var service = new CaseService())
                            {
                                CaseNote note = new CaseNote();
                                note.Category = NoteCategoryEnum.InteractiveProcess;
                                note.CreatedBy = CurrentUser;
                                note.CustomerId = c.CustomerId;
                                note.EmployerId = c.EmployerId;
                                note.Public = false;
                                note.Case = c;
                                note.ModifiedBy = CurrentUser;
                                note.Notes = question.QuestionText + " - " + question.Note;
                                note.Metadata.SetRawValue("AccommQuestionId", question.QuestionId);
                                note.Metadata.SetRawValue("AccommId", theRequest.Id);
                                note.Metadata.SetRawValue("Contact", newContact.ContactTypeName + ": " + newContact.Contact.FirstName + " " + newContact.Contact.LastName);
                                note.Metadata.SetRawValue("Answer", question.Answer);
                                note.Metadata.SetRawValue("SAnswer", question.SAnswer);
                                note.Save();
                                note.WfOnCaseNoteCreated(CurrentUser);
                            }
                        }

                        AdditionalInfo.Steps.Add(additionalQue);

                    }
                    theRequest.AdditionalInfo = AdditionalInfo;
                }

                // if AccommodationRequest.Status is "Cancelled", but all "Accommodations" under this request is open
                // then change AccommodationRequest Status to open
                if (request.Accommodations.Count > 0)
                {
                    if (!request.Accommodations.Any(m => m.Status == CaseStatus.Cancelled) && request.Status == CaseStatus.Cancelled)
                    {
                        theRequest.Status = CaseStatus.Open;
                        theRequest.CancelReason = (AccommodationCancelReason?)null;
                        theRequest.OtherReasonDesc = string.Empty;
                    }
                    else
                    {
                        theRequest.Status = request.Status;
                        theRequest.CancelReason = request.CancelReason.HasValue ? request.CancelReason : (AccommodationCancelReason?)null;
                        if (request.CancelReason.HasValue && request.CancelReason.Value == AccommodationCancelReason.Other)
                            theRequest.OtherReasonDesc = request.OtherDescription;
                        else
                            theRequest.OtherReasonDesc = string.Empty;
                    }
                }
                else
                {
                    if (!accommList.Any(m => m.Status == CaseStatus.Cancelled) && request.Status == CaseStatus.Cancelled)
                    {
                        theRequest.Status = CaseStatus.Open;
                        theRequest.CancelReason = (AccommodationCancelReason?)null;
                        theRequest.OtherReasonDesc = string.Empty;
                    }
                    else
                    {
                        theRequest.Status = request.Status;
                        theRequest.CancelReason = request.CancelReason.HasValue ? request.CancelReason : (AccommodationCancelReason?)null;
                        if (request.CancelReason.HasValue && request.CancelReason.Value == AccommodationCancelReason.Other)
                            theRequest.OtherReasonDesc = request.OtherDescription;
                        else
                            theRequest.OtherReasonDesc = string.Empty;
                    }
                }

                List<Accommodation> added = new List<Accommodation>();
                List<Accommodation> changed = new List<Accommodation>();
                List<Accommodation> deleted = new List<Accommodation>();

                List<Accommodation> newAccommList = new List<Accommodation>();
                Accommodation accomm = null;
                bool isNew = false;
                foreach (AccommodationViewModel avm in request.Accommodations)
                {
                    if (!string.IsNullOrEmpty(avm.Id) && c.AccommodationRequest.Accommodations.Any(x => x.Id.ToString().Equals(avm.Id)))
                    {
                        //check if we have a duplicate info.
                        var existingAccommodation = c.AccommodationRequest.Accommodations.FirstOrDefault(x => x.Type.Name.Equals(avm.Type.Name) && x.StartDate >= avm.StartDate && x.EndDate <= avm.EndDate && x.Id.ToString() != avm.Id);

                        if (existingAccommodation != null)
                        {
                            return JsonError(new Exception("Accommodation Request with the same date range is already existing."));
                        }

                        accomm = c.AccommodationRequest.Accommodations.First(x => x.Id.ToString().Equals(avm.Id));
                        if (accomm != null)
                            accommList.Remove(changed.AddFluid(accomm));
                        isNew = false;
                    }                    
                    else
                    {
                        if (string.IsNullOrEmpty(avm.Id) && c.AccommodationRequest != null && c.AccommodationRequest.Accommodations != null)
                        {
                            string errorMsg = "An accommodation request with the same date range already exists";

                            //chk if the new reqt is for temp duration, since it will have both start & end date
                            if ( avm.Duration == AccommodationDuration.Temporary )
                            {
                                if (c.AccommodationRequest.Accommodations.Any(x => x.Type.Name.Equals(avm.Type.Name) && ( avm.StartDate.Value.DateInRange(x.StartDate.Value, x.EndDate) || avm.EndDate.Value.DateInRange(x.StartDate.Value, x.EndDate) ))) // AT-3940, scene 1,4
                                {
                                    return JsonError(new AbsenceSoftException(errorMsg));
                                }
                            }
                            else // perm duration
                            {
                                if ( c.AccommodationRequest.Accommodations.Any(x => x.Type.Name.Equals(avm.Type.Name) && avm.StartDate.Value.DateInRange(x.StartDate.Value, x.EndDate))) // AT-3940, scene 1 & 3
                                {
                                    return JsonError(new AbsenceSoftException(errorMsg));
                                }
                                else if (c.AccommodationRequest.Accommodations.Any(x => x.Type.Name.Equals(avm.Type.Name) && ( avm.StartDate <= x.StartDate || x.EndDate >= avm.StartDate ) )) // Handling scenario where the new start date is earlier than current records. 
                                {
                                    return JsonError(new AbsenceSoftException(errorMsg));
                                }
                            }
                        }

                        accomm = added.AddFluid(new Accommodation());
                        isNew = true;
                    }
                     
                    accomm.StartDate = avm.StartDate;
                    accomm.EndDate = avm.EndDate;
                    accomm.Type = avm.Type;
                    accomm.Duration = avm.Duration;
                    accomm.Description = avm.Type.Code == "OTHER" ? avm.Description : string.Empty;
                    accomm.Cost = avm.Cost;
                    accomm.Resolution = avm.Resolution;
                    accomm.IsWorkRelated = avm.IsWorkRelated;
                    accomm.Resolved = avm.Resolved;
                    accomm.ResolvedDate = avm.ResolvedDate;
                    accomm.Granted = avm.Granted;
                    accomm.GrantedDate = avm.GrantedDate;
                    accomm.Usage = MakeUsage(avm, c, isNew);
                    accomm.Status = avm.Status;
                    accomm.CancelReason = avm.CancelReason.HasValue ? avm.CancelReason.Value : (AccommodationCancelReason?)null;
                    if (avm.CancelReason == AccommodationCancelReason.Other)
                        accomm.OtherReasonDesc = avm.OtherReasonDesc;
                    else
                        accomm.OtherReasonDesc = string.Empty;

                    //Add a case note for the accommodation interactive 
                    //process steps
                    if (!isNew)
                    {
                        if (avm.InteractiveProcessGroups != null)
                        {
                            foreach (IntProcStepViewModel s in avm.InteractiveProcessGroups.SelectMany(p => p.InteractiveProcessSteps))
                            {
                                AbsenceSoft.Data.Cases.AccommodationInteractiveProcess.Step theStep =
                                    accomm.InteractiveProcess.Steps.Where(x => x.QuestionId.Equals(s.QuestionId)).FirstOrDefault() ??
                                    accomm.InteractiveProcess.Steps.AddFluid(new AccommodationInteractiveProcess.Step()
                                    {
                                        QuestionId = s.QuestionId,
                                        Order = s.Order
                                    });

                                theStep.Answer = s.Answer;
                                theStep.SAnswer = s.SAnswer;
                                theStep.CompletedDate = DateTime.Now;
                                theStep.DateOfInitialDecision = s.DateOfInitialDecision;
                                theStep.DecisionOverturned = s.DecisionOverturned;
                                theStep.Date = s.Date;
                                theStep.Time = s.Time;
                                theStep.Provider = s.Provider;

                                EmployeeContact newContact = new EmployeeContact();

                                if (s.Contact != null)
                                {
                                    if (!string.IsNullOrEmpty(s.Contact.Id) && !s.Contact.Id.Equals("-1"))
                                    {
                                        newContact = EmployeeContact.GetById(s.Contact.Id);
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(s.Contact.Contact.FirstName) && !string.IsNullOrEmpty(s.Contact.Contact.FirstName))
                                        {
                                            var contactType = new ContactType();
                                            if (!String.IsNullOrEmpty(s.Contact.ContactTypeCode))
                                                contactType = ContactType.GetByCode(s.Contact.ContactTypeCode);

                                            newContact = new EmployeeContact()
                                            {
                                                EmployerId = c.EmployerId,
                                                CustomerId = CurrentCustomer.Id,
                                                EmployeeId = c.Employee.Id,
                                                EmployeeNumber = c.Employee.EmployeeNumber,
                                                ContactTypeCode = contactType.Code,
                                                ContactTypeName = contactType.Name,
                                                MilitaryStatus = MilitaryStatus.Civilian,
                                                Contact = new Contact()
                                                {
                                                    FirstName = s.Contact.Contact.FirstName,
                                                    LastName = s.Contact.Contact.LastName,
                                                }
                                            };

                                            newContact.Save();
                                        }
                                    }
                                }

                                //create case note for int process question notes
                                if (!string.IsNullOrEmpty(s.Note))
                                {
                                    using (var service = new CaseService())
                                    {
                                        if (string.IsNullOrWhiteSpace(s.QuestionId))
                                            return JsonError(new Exception("Accommodation Interactive Process QuestionId not found"));

                                        if (accomm.Id == null)
                                            return JsonError(new Exception("AccommodationId not found"));

                                        CaseNote note = new CaseNote();
                                        note.Category = NoteCategoryEnum.InteractiveProcess;
                                        note.CreatedBy = CurrentUser;
                                        note.CustomerId = c.CustomerId;
                                        note.EmployerId = c.EmployerId;
                                        note.Public = false;
                                        note.Case = c;
                                        note.ModifiedBy = CurrentUser;
                                        note.Notes = s.QuestionText + " - " + s.Note;
                                        note.Metadata.SetRawValue("AccommQuestionId", s.QuestionId);
                                        note.Metadata.SetRawValue("AccommId", accomm.Id);
                                        note.Metadata.SetRawValue("Answer", s.Answer);
                                        note.Metadata.SetRawValue("SAnswer", s.SAnswer);
                                        note.Save();
                                        note.WfOnCaseNoteCreated(CurrentUser);
                                    }
                                }
                            }

                            using (AccommodationService svc = new AccommodationService())
                            {
                                if (avm.ApprovedStartDate.HasValue)
                                    c = svc.ApplyDetermination(c, avm.ApprovedStartDate.Value, avm.ApprovedEndDate, AdjudicationStatus.Approved, null, string.Empty, string.Empty, accomm.Id);

                                //denied
                                if (avm.DeniedStartDate.HasValue)
                                    c = svc.ApplyDetermination(c, avm.DeniedStartDate.Value, avm.DeniedEndDate, AdjudicationStatus.Denied, avm.DenialReasonCode, avm.DenialExplanation, avm.DenialReasonName, accomm.Id);

                                //pending
                                if (avm.PendingStartDate.HasValue)
                                    c = svc.ApplyDetermination(c, avm.PendingStartDate.Value, avm.PendingEndDate, AdjudicationStatus.Pending, null, string.Empty, null, accomm.Id);
                            }
                        }// if is not new
                    }
                    accommList.Add(accomm);
                    if (isNew)
                    {
                        newAccommList.Add(accomm);
                        c.SetCaseEvent(CaseEventType.AccommodationAdded, DateTime.UtcNow);
                    }
                        
                }

                // Add any deleted
                deleted.AddRange(theRequest.Accommodations.Where(a => !accommList.Any(l => l.Id == a.Id)));

                
                c.AccommodationRequest = theRequest;
                var accommodationRequest = new AccommodationService().CreateOrModifyAccommodationRequest(c, theRequest.GeneralHealthCondition, newAccommList);
                theRequest.Accommodations = accommList;
                //if this is an accomm only case update the case dates
                //to match the request
                if (!c.Segments.SelectMany(s => s.AppliedPolicies).Any() && c.Segments.All(s => s.Type == CaseType.Administrative))
                {
                    c.StartDate = accommodationRequest.StartDate ?? c.StartDate;
                    c.EndDate = accommodationRequest.EndDate;
                    var seg = c.Segments.FirstOrDefault();
                    if (seg != null)
                    {
                        seg.Status = c.Status;
                        seg.StartDate = c.StartDate;
                        seg.EndDate = c.EndDate;
                        if (c.Segments.Count > 1)
                            c.Segments.RemoveRange(1, c.Segments.Count - 1);
                    }
                }

                if (c.Id != null)
                {
                    c.OnSavedNOnce((o, e) =>
                    {
                        added.ForEach(a => a.WfOnAccommodationCreated(c, CurrentUser));
                        changed.ForEach(a => a.WfOnAccommodationChanged(c, CurrentUser));
                        deleted.ForEach(a => a.WfOnAccommodationDeleted(c, CurrentUser));
                    });
                    new CaseService().Using(s => s.UpdateCase(c));
                }
                WorkRelatedViewModel model = new WorkRelatedViewModel(c);
                if (c.CaseType.Contains(CaseType.Reduced.ToString()) || c.CaseType.Contains(CaseType.Intermittent.ToString()))
                {
                    using (var caseService = new CaseService(CurrentCustomer, CurrentEmployer, CurrentUser))
                    {
                        model.CalculatedDaysOnJobTransferOrRestriction = caseService.CalculateDaysOnJobTransferOrRestriction(c);
                        c.WorkRelated = model.ApplyModel(c);
                        if (c.WorkRelated != null)
                        {
                            c.WorkRelated.Provider.Save();
                            new CaseService().Using(s => s.UpdateCase(c));
                        }
                    }
                }                              
                return Json(accommodationRequest);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error Creating Accommodation Request");
            }
        }

        [HttpPost, Secure("ADADetail")]
        [Route("Accommodations/ApplyDetermination", Name = "ApplyDetermination")]
        [ValidateApiAntiForgeryToken]
        public JsonResult ApplyDetermination(AccommodationDeterminationViewModel determination)
        {
            try
            {
                if (string.IsNullOrEmpty(determination.CaseId))
                    return JsonError(new Exception("CaseId not found"));

                Case theCase = Case.GetById(determination.CaseId);
                if (theCase == null)
                    return JsonError(new Exception("Case not found"));
                using (AccommodationService svc = new AccommodationService())
                {
                    foreach (AccommodationAdjudicationPolicyModel aapm in determination.Accommodations)
                    {
                        if (aapm.ApplyStatus)
                        {
                            var accomm = theCase.AccommodationRequest.Accommodations.FirstOrDefault(a => a.Id == new Guid(aapm.AccommodationId));
                            if (accomm != null)
                            {
                                // approved
                                if (determination.ApprovedStartDate.HasValue)
                                    theCase = svc.ApplyDetermination(theCase, determination.ApprovedStartDate.Value, determination.ApprovedEndDate, AdjudicationStatus.Approved, null, string.Empty, null, new Guid(aapm.AccommodationId));
                                // denied
                                if (determination.DeniedStartDate.HasValue)
                                    theCase = svc.ApplyDetermination(theCase, determination.DeniedStartDate.Value, determination.DeniedEndDate, AdjudicationStatus.Denied, determination.DenialReasonCode, determination.DenialExplanation, determination.DenialReasonName, new Guid(aapm.AccommodationId));
                                // pending
                                if (determination.PendingStartDate.HasValue)
                                    theCase = svc.ApplyDetermination(theCase, determination.PendingStartDate.Value, determination.PendingEndDate, AdjudicationStatus.Pending, null, string.Empty, null, new Guid(aapm.AccommodationId));
                            }
                        }
                    }

                    if (determination.Resolved.HasValue || determination.Granted.HasValue)
                    {
                        foreach (var accommodation in theCase.AccommodationRequest.Accommodations)
                        {
                            accommodation.Resolved = determination.Resolved.HasValue ? determination.Resolved.Value : accommodation.Resolved;
                            accommodation.ResolvedDate = determination.ResolvedDate;
                            accommodation.Resolution = determination.Resolution;
                            accommodation.Granted = determination.Granted.HasValue ? determination.Granted.Value : accommodation.Granted;
                            accommodation.GrantedDate = determination.GrantedDate;
                        }
                        theCase.AccommodationRequest = svc.CreateOrModifyAccommodationRequest(theCase, theCase.AccommodationRequest.GeneralHealthCondition, theCase.AccommodationRequest.Accommodations);
                    }

                    theCase = new CaseService().Using(s => s.UpdateCase(theCase));
                    return Json(theCase);
                }

            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error Applying Determination for Accommodation Request");
            }
        }

        [Secure]
        [Route("Accommodation/{caseId}/Request/{accomReqId}/Cancel", Name = "CancelAccommodationRequest")]
        public JsonResult CancelAccommodationRequest(string caseId, string accomReqId, AccommodationCancelReason cancelReason, string otherDesc)
        {
            try
            {
                if (string.IsNullOrEmpty(caseId))
                    return JsonError(new Exception("CaseId not found"));

                if (string.IsNullOrEmpty(accomReqId))
                    return JsonError(new Exception("Accommodation Request Id not found"));

                Case myCase = Case.GetById(caseId);
                if (myCase == null)
                    return JsonError(new Exception("Case not found"));

                new AccommodationService().Using(c => c.CancelAccommodationRequest(myCase, accomReqId, cancelReason, otherDesc));

                return Json(new { Success = true });
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }


        #region Helpers
        private List<IntProcStepGroupViewModel> MakeSteps(AccommodationInteractiveProcess ip, Guid accommId, string caseId)
        {
            List<IntProcStepGroupViewModel> steps = new List<IntProcStepGroupViewModel>();
            if (ip == null || ip.Steps == null) return steps;
            List<string> groupNames = ip.Steps.Where(p => p.Question != null).Select(p => p.Question.Group).Where(g => !string.IsNullOrWhiteSpace(g)).Distinct().ToList();
            foreach (var group in groupNames)
            {
                IntProcStepGroupViewModel g = new IntProcStepGroupViewModel()
                {
                    Name = group,
                    InteractiveProcessSteps = ip.Steps.Where(s => s.Question != null && s.Question.Group == group).Select(s => new IntProcStepViewModel()
                    {
                        QuestionId = s.QuestionId,
                        QuestionText = s.Question.Question,
                        Answer = s.Answer,
                        Order = s.Order,
                        Notes = GetAccommIntProcCaseNotes(s.QuestionId, accommId, caseId),
                        NoteVisible = false,
                        QuestionVisible = false,
                        SAnswer = s.SAnswer,
                        Date = s.Date,
                        Choices = s.Choices,
                        Provider = s.Provider,
                        Time = s.Time,
                        PromptDate = s.Question.PromptDate,
                        PromptDateOfInitialDecision = s.Question.PromptDateOfInitialDecision,
                        DateOfInitialDecision = s.DateOfInitialDecision,
                        PromptDateDecisionOverturned = s.Question.PromptDateDecisionOverturned,
                        PromptProvider = s.Question.PromptProvider,
                        PromptTime = s.Question.PromptTime,
                        IsYesNo = s.IsYesNo
                    }).ToList()
                };

                steps.Add(g);
            }

            if (ip.Steps.Any(s => s.Question != null && string.IsNullOrWhiteSpace(s.Question.Group)))
            {
                steps.Add(new IntProcStepGroupViewModel()
                {
                    InteractiveProcessSteps = ip.Steps.Where(s => s.Question != null && string.IsNullOrWhiteSpace(s.Question.Group)).Select(s => new IntProcStepViewModel()
                    {
                        QuestionId = s.QuestionId,
                        QuestionText = s.Question.Question,
                        Answer = s.Answer,
                        Order = s.Order,
                        Notes = GetAccommIntProcCaseNotes(s.QuestionId, accommId, caseId),
                        NoteVisible = false,
                        QuestionVisible = false,
                        SAnswer = s.SAnswer,
                        Date = s.Date,
                        Choices = s.Choices,
                        Provider = s.Provider,
                        Time = s.Time,
                        PromptDate = s.Question.PromptDate,
                        PromptDateOfInitialDecision = s.Question.PromptDateOfInitialDecision,
                        DateOfInitialDecision = s.DateOfInitialDecision,
                        PromptDateDecisionOverturned = s.Question.PromptDateDecisionOverturned,
                        PromptProvider = s.Question.PromptProvider,
                        PromptTime = s.Question.PromptTime,
                        IsYesNo = s.IsYesNo
                    }).ToList()
                });
            }


            return steps;
        }

        private List<IntProcNote> GetAccommIntProcCaseNotes(string questionId, Guid accommId, string caseId)
        {
            List<IntProcNote> notes = new List<IntProcNote>();
            List<CaseNote> caseNotes = null;
            using (AccommodationService svc = new AccommodationService())
                caseNotes = svc.GetAccommIntProcCaseNotes(accommId, caseId);

            IntProcNote ipn = null;
            foreach (CaseNote n in caseNotes)
            {
                string id = n.Metadata.GetRawValue<string>("AccommQuestionId");
                string Contact = n.Metadata.GetRawValue<string>("Contact");
                bool? Answer = n.Metadata.GetRawValue<bool?>("Answer");
                string SAnswer = n.Metadata.GetRawValue<string>("SAnswer");

                if (!string.IsNullOrEmpty(id) && id.Equals(questionId))
                {
                    ipn = new IntProcNote()
                    {
                        CreateDate = String.Format("{0:G}", n.CreatedDate),
                        Note = n.Notes,
                        ContactInfo = string.IsNullOrWhiteSpace(Contact) ? "" : "Contact - " + Contact,
                        Answer = Answer,
                        SAnswer = SAnswer
                    };
                    notes.Add(ipn);
                }
            }
            return notes;
        }

        private List<AccommodationUsage> MakeUsageModel(AccommodationViewModel accommodation)
        {
            List<AccommodationUsage> auList = new List<AccommodationUsage>();
            foreach (AccommodationUsageViewModel auvm in accommodation.AccommodationUsage)
            {
                AccommodationUsage au = new AccommodationUsage()
                {
                    //Following 2 line commented to fix AT-1825-System issues- reporting data in case page
                    //StartDate = accommodation.StartDate != null ? accommodation.StartDate.Value : auvm.StartDate,
                    //EndDate = accommodation.EndDate,
                    //Following 2 line inserted to fix AT-1825-System issues- reporting data in case page
                    StartDate = auvm.StartDate,
                    EndDate = auvm.EndDate,
                    Determination = auvm.AdjudicationStatus,
                    DenialReasonCode = auvm.DenialReasonCode,
                    DenialReasonName = auvm.DenialReasonName,
                    DenialReasonOther = auvm.DenialReasonOther
                };
                auList.Add(au);
            }
            return auList;
        }

        private List<AccommodationUsageViewModel> MakeUsageViewModel(Accommodation accommodation)
        {
            List<AccommodationUsageViewModel> auvmList = new List<AccommodationUsageViewModel>();
            foreach (AccommodationUsage au in accommodation.Usage)
            {
                AccommodationUsageViewModel auvm = new AccommodationUsageViewModel()
                {
                    StartDate = au.StartDate,
                    EndDate = au.EndDate,
                    AdjudicationStatus = au.Determination,
                    DenialReasonCode = au.DenialReasonCode,
                    DenialReasonName = au.DenialReasonName,
                    DenialReasonOther = au.DenialReasonOther
                };
                auvmList.Add(auvm);
            }
            return auvmList;
        }

        private List<AccommodationViewModel> MakeAccommodationsViewModel(AccommodationRequest req)
        {
            List<AccommodationViewModel> avmList = new List<AccommodationViewModel>();
            foreach (Accommodation a in req.Accommodations)
            {
                AccommodationViewModel avm = new AccommodationViewModel()
                {
                    Id = a.Id.ToString(),
                    StartDate = a.StartDate,
                    EndDate = a.EndDate,
                    Type = a.Type,
                    Duration = a.Duration,
                    Resolution = a.Resolution,
                    Resolved = a.Resolved,
                    Granted = a.Granted,
                    Cost = a.Cost,
                    AccommodationUsage = MakeUsageViewModel(a)
                };
                avmList.Add(avm);
            }
            return avmList;
        }

        private List<AccommodationRequestViewModel> MakeViewModel(List<AccommodationRequest> requests)
        {
            List<AccommodationRequestViewModel> vmList = new List<AccommodationRequestViewModel>();

            foreach (AccommodationRequest ar in requests)
            {
                AccommodationRequestViewModel arvm = new AccommodationRequestViewModel()
                {
                    Status = ar.Status,
                    StartDate = ar.StartDate,
                    EndDate = ar.EndDate,
                    GeneralHealthCondition = ar.GeneralHealthCondition,
                    Description = ar.Description,
                    Accommodations = MakeAccommodationsViewModel(ar)
                };
                vmList.Add(arvm);
            }
            return vmList;
        }

        private List<AccommodationUsage> MakeUsage(AccommodationViewModel model, Case theCase, bool isNew)
        {
            List<AccommodationUsage> usage = new List<AccommodationUsage>();
            if (isNew)
            {
                AccommodationUsage au = new AccommodationUsage();
                if (model.Duration == AccommodationDuration.Permanent)
                {
                    au.StartDate = model.StartDate.Value;
                    au.EndDate = null;
                }

                if (model.Duration == AccommodationDuration.Temporary)
                {
                    au.StartDate = model.StartDate ?? theCase.StartDate;
                    au.EndDate = model.EndDate;
                }

                usage.Add(au);
                return usage;
            }
            else
            {
                usage = MakeUsageModel(model);
                using (AccommodationService svc = new AccommodationService())
                    usage = svc.BoxUsageBeforeDetermination(usage, model.StartDate ?? theCase.StartDate, model.EndDate);
                return usage;
            }
        }// MakeUsage

        #endregion
    }
}
