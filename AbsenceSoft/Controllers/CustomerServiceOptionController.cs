﻿using System.Web.Mvc;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Models.Administration;
using AbsenceSoft.Web;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    public class CustomerServiceOptionController : BaseController
    {
        public const string ServiceOptionIndicatorKey = "ServiceOptionIndicator";

        [Secure("EditCustomerServiceOptions")]
        [HttpGet, Route("CustomerServiceOption/List", Name = "ViewCustomerServiceOptions")]
        public ActionResult ViewCustomerServiceOptions()
        {
            return View();
        }

        [Secure("EditCustomerServiceOptions")]
        [HttpPost, Route("CustomerServiceOption/List", Name = "ListCustomerServiceOptions")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListCustomerServiceOptions(ListCriteria criteria)
        {
            using (var customerServiceOptionService = new CustomerServiceOptionService(Current.User()))
            {
                var customerServiceOptions = customerServiceOptionService.ListCustomerServiceOptions(ServiceOptionIndicatorKey, criteria);
                return PartialView(customerServiceOptions);
            }
        }

        [Secure("EditCustomerServiceOptions")]
        [Route("CustomerServiceOption/EditCustomerServiceOption/{id?}", Name = "EditCustomerServiceOption")]
        public ActionResult EditCustomerServiceOption(string id)
        {
            CustomerServiceOptionModel customerServiceOptionModel = null;
            if (string.IsNullOrEmpty(id))
            {
                customerServiceOptionModel = new CustomerServiceOptionModel();
            }
            else
            {
                using (var customerServiceOptionService = new CustomerServiceOptionService(Current.User()))
                {
                    var customerServiceOption = customerServiceOptionService.GetCustomerServiceOptionById(ServiceOptionIndicatorKey, id);
                    customerServiceOptionModel = new CustomerServiceOptionModel
                    {
                        Id = customerServiceOption.Id,
                        Value = customerServiceOption.Value,
                        Order = customerServiceOption.Order
                    };
                }
            }

            return View(customerServiceOptionModel);
        }

        [Secure("EditCustomerServiceOptions")]
        [HttpPost, Route("CustomerServiceOption/SaveCustomerServiceOption", Name = "SaveCustomerServiceOption")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveCustomerServiceOption(CustomerServiceOptionModel customerServiceOptionModel)
        {
            if (!ModelState.IsValid)
                return PartialView("EditCustomerServiceOption", customerServiceOptionModel);

            using (var customerServiceOptionService = new CustomerServiceOptionService(Current.User()))
            {
                customerServiceOptionService.UpsertCustomerServiceOptionValue(ServiceOptionIndicatorKey, customerServiceOptionModel.Value, customerServiceOptionModel.Order);
            }

            return RedirectViaJavaScript(Url.RouteUrl("ViewCustomerServiceOptions"));
        }

        [Secure("EditCustomerServiceOptions")]
        [HttpDelete, Route("CustomerServiceOption/Delete/{id}", Name = "DeleteCustomerServiceOption")]
        public ActionResult DeleteCustomerServiceOption(string id)
        {
            using (var customerServiceOptionService = new CustomerServiceOptionService(Current.User()))
            {
                customerServiceOptionService.DeleteCustomerServiceOptionValue(ServiceOptionIndicatorKey, id);
                return RedirectViaJavaScript(Url.RouteUrl("ViewCustomerServiceOptions"));
            }
        }
    }
}