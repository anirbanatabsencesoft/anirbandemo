﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    [Secure]
    public class DocumentsController:BaseController
    {
        [HttpGet, Route("Documents/{documentId}/Download", Name ="DownloadDocument")]
        public ActionResult Download(string documentId)
        {
            Document d = Document.GetById(documentId);
            if (d == null)
                return null;


            /// this doc belongs to a customer, but the logged in user doesn't match
            if (!string.IsNullOrEmpty(d.CustomerId) && d.CustomerId != CurrentUser.CustomerId)
                return null;

            /// this doc belongs to an employer but the logged in user doesn't match
            if (!string.IsNullOrEmpty(d.EmployerId) && !CurrentUser.HasEmployerAccess(d.EmployerId))
                return null;

            return Redirect(d.DownloadUrl());
        }

        [HttpGet, Route("Documents/{employerId}/GetDocumentUploadPolicy")]
        public JsonResult GetEmployerDocumentUploadPolicy(string employerId, string fileName)
        {
            if(!CurrentUser.HasEmployerAccess(employerId))
                return null;

            Employer e = Employer.GetById(employerId);
            return Json(new FileService().GetEmployerUploadPolicy(fileName, e.CustomerId, employerId));
        }

        [HttpPost, Route("Documents/Save")]
        [ValidateApiAntiForgeryToken]
        public JsonResult Save(Document doc)
        {
            if (!CurrentUser.HasEmployerAccess(doc.EmployerId))
                return Json(doc);

            return Json(new FileService().SaveUploadedDocument(doc));
        }
    }
}