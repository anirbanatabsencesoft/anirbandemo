﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Models.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    [Secure]
    public class CaseCategoryController : BaseController
    {
        [Secure]
        [HttpGet, Title("View Case Categories"), Route("CaseCategory/List/{employerId?}", Name = "ViewCaseCategories")]
        public ActionResult ViewCaseCategories(string employerId)
        {
            using (var caseService = new CaseService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                // If we're making an Ajax request, it's for a drop down list and we don't want to include suppressed entities
                bool isAjax = Request.IsAjax();
                List<CaseCategory> caseCategories = caseService.GetAllCaseCategories(!isAjax);
                if (isAjax)
                    return Json(caseCategories);

                return View(caseCategories.Select(nc => new CaseCategoryViewModel(nc)).ToList());
            }
        }

        [Secure]
        [HttpGet, Route("CaseCategory/EditCaseCategory/{employerId?}", Name = "EditCaseCategory")]
        public ActionResult EditCaseCategory(string employerId, string id = null)
        {
            using (var caseService = new CaseService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                CaseCategoryViewModel category = new CaseCategoryViewModel(caseService.GetCaseCategoryById(id));
                return View(category);
            }
        }
        [Secure]
        [HttpPost, Route("CaseCategory/Save", Name = "CaseCategorySave")]
        [ValidateApiAntiForgeryToken]
        public ActionResult SaveCaseCategory(CaseCategoryViewModel category)
        {
            if (!ModelState.IsValid)
                return PartialView("EditCaseCategory", category);

            if (ModelState.IsValid)
            {
                try
                {
                    using (CaseService svc = new CaseService(CurrentCustomer, CurrentEmployer, CurrentUser))
                    {
                        CaseCategory savedCategory = svc.GetCaseCategoryById(category.Id);

                        savedCategory = category.ApplyToDataModel(savedCategory);
                        savedCategory = svc.SaveCaseCategory(savedCategory);
                    }

                    return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewCaseCategories"));
                }
                catch (AbsenceSoftException ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }

            if (Request.IsAjaxRequest())
                return PartialView("CaseCategoryEdit", category);

            return View("CaseCategoryEdit", category);
        }

        [Secure("DeleteCaseCloseReason")]
        [HttpDelete, Route("CaseCategory/DeleteCaseCategory/{employerId?}", Name = "DeleteCaseCategory")]
        public ActionResult DeleteCaseCategory(string employerId, string id)
        {
            using (var caseService = new CaseService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                CaseCategory category = caseService.GetCaseCategoryById(id);
                if (category.IsCustom)
                    caseService.DeleteCaseCategory(category);

                return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewCaseCategories"));
            }
        }

        [Secure("EditCaseCloseReason")]
        [HttpPost, Route("CaseCategory/ToggleCaseCategorySuppression/{employerId?}", Name = "ToggleCaseCategorySuppression")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ToggleCaseCategorySuppression(CaseCategoryViewModel caseCategory)
        {
            using (var caseService = new CaseService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                
                var savedCaseCategory = caseService.ToggleCaseCategoryById(caseCategory.Id);
                caseCategory.IsDisabled = savedCaseCategory.Suppressed;
                return Json(caseCategory);
            }
        }
    }
}