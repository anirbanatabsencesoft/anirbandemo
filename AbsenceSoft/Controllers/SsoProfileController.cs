﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Models.Sso;
using AbsenceSoft.Web.Attributes;
using ComponentSpace.SAML2.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Controllers
{
    [Secure(Feature.SingleSignOn, "EditSSOSettings")]
    public class SsoProfileController : BaseController
    {
        
        [Route("SsoSettings/{employerId?}", Name = "SsoSettings"), Title("SSO Settings")]
        public ActionResult SsoSettings(string employerId, bool? saved)
        {
            using (var ssoService = new SsoService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                SsoProfileViewModel model = new SsoProfileViewModel(ssoService.GetProfile());
                model.EmployerId = employerId;
                return View(model);
            }
        }

        [Route("SsoSettings/Save/{employerId?}", Name = "SaveSsoSettings"), Title("SSO Settings")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSettings(string employerId, SsoProfileViewModel model, HttpPostedFileBase partnerCertificate)
        {
            using (var administrationService = new AdministrationService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                if (!administrationService.CustomerHasFeature(Feature.EmployeeSelfService))
                {
                    model.IsEss = false;
                    model.SelfServiceLoginSourceName = null;
                    model.IsPortal = true;
                }
            }

            if (ModelState.IsValid)
            {
                using (var ssoService = new SsoService(CurrentCustomer, CurrentEmployer, CurrentUser))
                {
                    SsoProfile profile = model.ApplyToDataModel(ssoService.GetProfile(), partnerCertificate);
                    try
                    {
                        ssoService.SaveProfile(profile);
                    }
                    catch (CryptographicException)
                    {
                        ModelState.AddModelError("", "There was a cryptography error when attempting to update the configuration.  Your changes have been rolled back.");
                    }
                    catch (SAMLConfigurationException)
                    {
                        ModelState.AddModelError("", "The profile was successfully updated, but there was an error when configuring the SAML.");
                    }
                    model.Saved = true;
                }
            }

            return View("SsoSettings", model);
        }

        [HttpGet, Route("SsoSettings/ExportSPMetadata/{employerId?}", Name = "ExportSPMetadata")]
        public ActionResult ExportSPMetadata(string employerId, bool ess, bool regenerateCertificate)
        {
            using (var ssoService = new SsoService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                StringBuilder fileName = new StringBuilder();
                fileName.AppendFormat("{0}", CurrentCustomer.Name);
                if (CurrentEmployer != null)
                    fileName.AppendFormat(" - {0}", CurrentEmployer.Name);
                fileName.Append(" SP Metadata.xml");

                return File(ssoService.ExportSPMetadata(ess, regenerateCertificate), "text/xml; charset=UTF-8", fileName.ToString());
            }
        }

        [HttpPost, Route("SsoSettings/ImportIdPMetadata/{employerId?}", Name ="ImportIdpMetadata"), Title("SSO Settings")]
        [ValidateAntiForgeryToken]
        public ActionResult ImportIdPMetadata(string employerId, HttpPostedFileBase idpMetadata)
        {
            using (var ssoService = new SsoService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                SsoProfileViewModel model = null;
                try
                {
                    model = new SsoProfileViewModel(ssoService.ImportIdPMetadata(idpMetadata));
                    model.Saved = true;
                }
                
                catch (ArgumentException aEx)
                {
                    model = new SsoProfileViewModel(ssoService.GetProfile());
                    model.Saved = false;
                    ModelState.AddModelError("", aEx.Message);
                }
                catch (SAMLConfigurationException)
                {
                    model = new SsoProfileViewModel(ssoService.GetProfile());
                    model.Saved = true;
                    ModelState.AddModelError("", "The profile was successfully updated with the IdP Metadata, but there was an error when configuring the SAML.");
                }
                
                return View("SsoSettings", model);

            }
        }
    }
}