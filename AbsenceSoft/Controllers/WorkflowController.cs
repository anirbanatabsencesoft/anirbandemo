﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Workflows;
using AbsenceSoft.Models.Workflow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Common;
using AbsenceSoft.Web.Filters;
using AbsenceSoft.Logic.Customers;

namespace AbsenceSoft.Controllers
{
    [Secure]
    public class WorkflowController : BaseController
    {

        [Title("Workflows")]
        [Secure(Feature.WorkflowConfiguration, "EditWorkflow")]
        [HttpGet, Route("Workflow/List/{employerId?}", Name = "ViewWorkflows")]
        public ActionResult ViewWorkflows(string employerId)
        {
            ViewBag.EmployerId = employerId;
            return View();
        }

        [Secure(Feature.WorkflowConfiguration, "EditWorkflow")]
        [HttpPost, Route("Workflow/List", Name = "ListWorkflows")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListWorkflows(ListCriteria criteria)
        {
            using (var workflowService = new WorkflowService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                ListResults results = workflowService.WorkflowList(criteria);
                return PartialView(results);
            }
        }

        [Secure(Feature.WorkflowConfiguration, "EditWorkflow")]
        [HttpGet, Route("Workflow/ListAll/{employerId?}", Name = "ListAllWorkflows")]
        public ActionResult ListWorkflows()
        {
            using (var workflowService = new WorkflowService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                ListResults results = workflowService.WorkflowList(new ListCriteria() { PageNumber = 1, PageSize = int.MaxValue, SortBy = "Name", SortDirection = SortDirection.Ascending });
                return Json(results.Results);
            }
        }

        [Title("Edit Workflows")]
        [Secure(Feature.WorkflowConfiguration, "EditWorkflow")]
        [HttpGet, Route("Workflow/EditWorkflow/{employerId?}", Name = "EditWorkflow")]
        public ActionResult EditWorkflow(string employerId, string id)
        {
            using (var workflowService = new WorkflowService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                WorkflowViewModel wf = new WorkflowViewModel(workflowService.GetWorkflowById(id));
                ViewBag.EmployerId = employerId;
                return View(wf);
            }
        }

        [Title("Design Workflow")]
        [Secure(Feature.WorkflowConfiguration, "EditWorkflow")]
        [HttpGet, Route("Workflow/DesignWorkflow/{employerId?}", Name = "DesignWorkflow")]
        public ActionResult DesignWorkflow(string employerId, string id)
        {
            using (var workflowService = new WorkflowService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                WorkflowDesignViewModel wf = new WorkflowDesignViewModel(workflowService.GetWorkflowById(id));
                ViewBag.EmployerId = employerId;
                return View(wf);
            }
        }

        [HttpGet, Route("Workflow/Expressions/{employerId?}"), Secure(Feature.WorkflowConfiguration, "EditWorkflow")]
        public ActionResult GetWorkflowExpressions(EventType eventType, string employerId)
        {
            try
            {
                using (var svc = new WorkflowService(CurrentCustomer, CurrentEmployer, CurrentUser))
                {
                    return Json(svc.GetExpressionsByEventType(eventType));
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable retrieve workflow rule expressions");
            }
        }

        [HttpPost, Route("Workflow/Rules/"), Secure(Feature.WorkflowConfiguration, "EditWorkflow")]
        [ValidateApiAntiForgeryToken]
        public ActionResult GetWorkflowRules(WorkflowRuleExpressions expressions)
        {
            try
            {
                if (expressions == null)
                    throw new ArgumentNullException("expressions");

                if (expressions.RuleExpressions == null)
                    expressions.RuleExpressions = new List<WorkflowRuleViewModel>(0);

                var ruleExpressions = new List<RuleExpression>(expressions.RuleExpressions.Count);
                using (var employerService = new EmployerService())
                {
                    var employer = employerService.GetById(expressions.EmployerId);
                    using (var workflowService = new WorkflowService(CurrentCustomer, employer, CurrentUser))
                    {
                        var allExpressions = workflowService.GetExpressionsByEventType(expressions.EventType);

                        foreach (var ruleExpression in expressions.RuleExpressions)
                        {
                            var matchingExpression = allExpressions
                                .First(rg => rg.Title == ruleExpression.Group).Expressions
                                .FirstOrDefault(r => r.Name == ruleExpression.Name);

                            if (matchingExpression != null && matchingExpression.IsCustom)
                            {
                                ruleExpression.Parameters = matchingExpression.Parameters;
                                var appliedExpression = ruleExpression.ApplyToDataModel(matchingExpression);
                                ruleExpressions.Add(appliedExpression.Clone());
                            }
                            else if (matchingExpression != null && !matchingExpression.IsCustom)
                            {
                                var appliedExpression = ruleExpression.ApplyToDataModel(matchingExpression);
                                ruleExpressions.Add(appliedExpression.Clone());
                            }
                        }

                        return Json(workflowService.RuleExpressionsToRules(ruleExpressions, expressions.EventType));
                    }
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable retrieve workflow rules");
            }
        }

        [HttpPost, Route("Workflow/Expressions/"), Secure(Feature.WorkflowConfiguration, "EditWorkflow")]
        [ValidateApiAntiForgeryToken]
        public ActionResult GetWorkflowExpressions(WorkflowRuleExpressions rules, string EmployerId = null)
        {
            try
            {
                if (rules == null)
                    throw new ArgumentNullException("rules");

                if (rules.Rules == null)
                    rules.Rules = new List<Rule>(0);
                using (var employerService = new EmployerService())
                {
                    var employer = employerService.GetById(EmployerId);
                    using (var svc = new WorkflowService(CurrentCustomer, employer, CurrentUser))
                    {
                        var expressions = rules.Rules
                            .Select(r => svc.GetExpression(r))
                            .Where(r => r != null)
                            .Select(e => new WorkflowRuleViewModel(e, rules.EventType, employer));
                        return Json(expressions);
                    }
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable retrieve workflow rules");
            }
        }

        [Secure(Feature.WorkflowConfiguration, "EditWorkflow")]
        [HttpPost, Route("Workflow/SaveWorkflow", Name = "SaveWorkflow")]
        [ValidateApiAntiForgeryToken]
        public ActionResult SaveWorkflow(WorkflowViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return Json(viewModel);

            using (var employerService = new EmployerService())
            {
                var employer = employerService.GetById(viewModel.EmployerId);
                using (var workflowService = new WorkflowService(CurrentCustomer, employer, CurrentUser))
                {
                    Workflow workflow = viewModel.ApplyToDataModel(workflowService.GetWorkflowById(viewModel.Id));
                    workflowService.Update(workflow);
                    return Json(new WorkflowViewModel(workflow));
                }
            }
        }

        [Secure(Feature.WorkflowConfiguration, "EditWorkflow")]
        [HttpPost, Route("Workflow/SaveWorkflowDesign", Name = "SaveWorkflowDesign")]
        [ValidateApiAntiForgeryToken]
        public ActionResult SaveWorkflowDesign(WorkflowDesignViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return Json(viewModel);

            using (var employerService = new EmployerService())
            {
                var employer = employerService.GetById(viewModel.EmployerId);
                using (var workflowService = new WorkflowService(CurrentCustomer, employer, CurrentUser))
                {
                    Workflow workflow = viewModel.ApplyToDataModel(workflowService.GetWorkflowById(viewModel.Id));
                    workflowService.Update(workflow);
                    return Json(new WorkflowDesignViewModel(workflow));
                }
            }
        }

        [HttpPost, Route("Workflow/Copy/{employerId?}", Name = "CopyWorkflow"), Secure(Feature.WorkflowConfiguration, "EditWorkflow")]
        [ValidateApiAntiForgeryToken]
        public ActionResult CopyWorkflow(WorkflowCopyModel copy)
        {
            try
            {
                using (WorkflowService svc = new WorkflowService(CurrentCustomer, CurrentEmployer, CurrentUser))
                {
                    Workflow workflowToCopy = Workflow.GetByCode(copy.Code, copy.CustomerId, copy.EmployerId);
                    return RedirectViaJavaScript(Url.AdministrationRouteUrl("EditWorkflow", new { id = svc.Copy(workflowToCopy, copy.NewCode, copy.NewName, copy.NewDescription, copy.EmployerId).Id }));                    
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to copy workflow");
            }
        }

        [Secure(Feature.WorkflowConfiguration, "EditWorkflow")]
        [HttpDelete, Route("Workflow/DeleteWorkflow/{employerId?}", Name = "DeleteWorkflow")]
        public ActionResult DeleteWorkflow(string employerId, string workflowId)
        {
            try
            {
                using (WorkflowService svc = new WorkflowService(CurrentCustomer, CurrentEmployer, CurrentUser))
                {
                    Workflow wf = svc.GetWorkflowById(workflowId);
                    svc.Delete(wf);
                    return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewWorkflows"));
                } 
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to delete specified workflow");
            }
        }

       
        [Secure(Feature.WorkflowConfiguration, "EditWorkflow")]
        [HttpPost, Route("Workflow/ToggleWorkflowSuppression/{employerId?}", Name = "ToggleWorkflowSuppression")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ToggleWorkflowSuppression(WorkflowViewModel workflowModel)
        {
            using (var workflowService = new WorkflowService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                var template = workflowService.ToggleWorkflowByCode(workflowModel.Code);
                workflowModel.Suppressed = template.Suppressed;
                return Json(workflowModel);
            }
        }

        [HttpGet, Route("Workflow/Designer/Toolbar/{eventType?}"), ActionName("WorkflowToolbar"), Secure(Feature.WorkflowConfiguration, "EditWorkflow")]
        public ActionResult WorkflowToolbar(EventType? eventType)
        {

            using (var workflowService = new WorkflowService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                List<Activity> activities = null;
                if (eventType == null)
                    activities = workflowService.GetAllActivities();
                else
                    activities = workflowService.GetActivitiesByEventType(eventType.Value);

                var grouped = activities.GroupBy(p => p.Category);
                List<ActivityCategory> categories = grouped.Select(p => new ActivityCategory()
                {
                    CategoryName = p.Key,
                    Activities = p.ToList()
                }).OrderBy(x=>x.CategoryName).ToList();
                if (Request.IsAjax())
                    return Json(categories);


                return PartialView(categories);
            }
        }

        [HttpGet, Route("Workflow/Activities"), ActionName("Activities"), Secure(Feature.WorkflowConfiguration, "EditWorkflow")]
        public ActionResult GetWorkflowActivities()
        {
            using (var workflowService = new WorkflowService(CurrentUser))
            {
                return Json(workflowService.GetAllActivities());
            }
        }

        [HttpPost, Route("WorkflowInstances/List"), ActionName("WorkflowInstancesList")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListWorkflowInstances(ListCriteria criteria)
        {
            using (var workflowService = new WorkflowService(CurrentUser))
            {
                return Json(workflowService.ListWorkflowInstances(criteria));
            }
        }

        [HttpPost, Route("WorkflowInstance/Pause"), ActionName("WorkflowInstancePause")]
        [ValidateApiAntiForgeryToken]
        public ActionResult PauseWorkflowInstance(WorkflowInstance instance)
        {
            using (var workflowService = new WorkflowService(CurrentUser))
            {
                instance = WorkflowInstance.GetById(instance.Id);
                instance = workflowService.Pause(instance);
                return Json(workflowService.UpdateWorkflowInstance(instance));
            }
        }

        [HttpPost, Route("WorkflowInstance/Resume"), ActionName("WorkflowInstanceResume")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ResumeWorkflowInstance(WorkflowInstance instance)
        {
            using (var workflowService = new WorkflowService(CurrentUser))
            {
                instance = WorkflowInstance.GetById(instance.Id);
                instance = workflowService.Resume(instance);
                return Json(workflowService.UpdateWorkflowInstance(instance));
            }
        }

        [HttpPost, Route("WorkflowInstance/Cancel"), ActionName("WorkflowInstanceCancel")]
        [ValidateApiAntiForgeryToken]
        public ActionResult CancelWorkflowInstance(WorkflowInstance instance)
        {
            using (var workflowService = new WorkflowService(CurrentUser))
            {
                instance = WorkflowInstance.GetById(instance.Id);
                instance = workflowService.Cancel(instance);
                return Json(workflowService.UpdateWorkflowInstance(instance));
            }
        }

        [HttpGet, Route("WorkflowInstance/Start"), ActionName("WorkflowInstanceCriteria")]
        public ActionResult GetWorkflowInstanceCriteria(StartCaseWorkflow workflow)
        {
            using (var manualWorkflowService = new ManualWorkflowService(CurrentUser))
            {
                List<WorkflowCriteria> criteria = manualWorkflowService.GetCriteria(workflow.WorkflowId, null, workflow.CaseId);
                return Json(criteria);
            }
        }

        [HttpPost, Route("WorkflowInstance/Start"), ActionName("WorkflowInstanceStart")]
        [ValidateApiAntiForgeryToken]
        public ActionResult StartWorkflowInstance(StartCaseWorkflow workflow)
        {
            using (var manualWorkflowService = new ManualWorkflowService(CurrentUser))
            {
                using (var workflowService = new WorkflowService(CurrentUser))
                {
                    Event e = manualWorkflowService.BuildEvent(workflow.WorkflowId, null, workflow.CaseId, workflow.Criteria);
                    Workflow wf = workflowService.GetWorkflowById(workflow.WorkflowId);
                    WorkflowInstance instance = workflowService.CreateInstance(wf, e);
                    if (instance != null)
                    {
                        workflowService.UpdateWorkflowInstance(instance);
                        workflowService.Run(instance);
                    }
                    return Json(instance);
                }
            }
        }


    }
}