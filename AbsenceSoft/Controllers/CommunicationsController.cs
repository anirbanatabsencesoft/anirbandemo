﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Tasks;
using AbsenceSoft.Models;
using AbsenceSoft.Models.Communications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Cases;
using MongoDB.Bson;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;
using AbsenceSoft.Common;

namespace AbsenceSoft.Controllers
{
    [Secure]
    public class CommunicationsController : BaseController
    {
        [Title("Inquiry Communications"), Secure("SendCommunication")]
        [Route("Inquiries/{caseId}/Communications", Name = "InquiryCommunications")]
        public ViewResult InquiryCommunicationsIndex(string caseId)
        {
            ViewBag.Status = "Inquiry";
            return CaseCommunicationsIndexGet(caseId);
        }

        [Title("Case Communications"), Secure("SendCommunication")]
        [Route("Cases/{caseId}/Communications", Name = "CaseCommunications")]
        public ViewResult CaseCommunicationsIndex(string caseId)
        {
            ViewBag.Status = "";
            return CaseCommunicationsIndexGet(caseId);
        }

        private ViewResult CaseCommunicationsIndexGet(string caseId)
        {
            if (String.IsNullOrEmpty(caseId))
            {
                throw new ArgumentNullException("caseId");
            }

            var model = new CaseParentedModel()
            {
                CaseId = caseId,
                Status = ViewBag.Status
            };

            if (!String.IsNullOrWhiteSpace(caseId))
            {
                Case thisCase = Case.GetById(caseId);
                model.EmployerId = thisCase.EmployerId;
            }

            return View("Index", model);
        }

        [Title("Review Communication")]
        [ActionName("View")]
        [Route("Communications/{communicationId}/View/{caseId}", Name = "ViewCommunication")]
        [Secure("SendCommunication")]
        public ViewResult CommunicationView(string communicationId, string caseId)
        {
            if (String.IsNullOrEmpty(communicationId))
            {
                throw new ArgumentNullException("communicationId");
            }


            var model = new BaseCommunicationModel()
            {
                CommunicationId = communicationId,
                CaseId = caseId
            };

            return View("View", model);
        }

        [Route("Communications/{communicationId}/ViewCommunicationJson", Name = "ViewCommunicationJson")]
        [Secure("SendCommunication")]
        public JsonResult ViewCommunicationJson(string communicationId)
        {
            try
            {
                if (String.IsNullOrEmpty(communicationId))
                    throw new ArgumentNullException("communicationId");

                Communication model = Communication.GetById(communicationId);
                if (model == null)
                    throw new ArgumentNullException("Communication ID not found.");

                return Json(model, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }


        //Note: optional parameters in query string to be specified last {packetType?}
        //Note: added WorkFlowItemCommunications in route to populate toDoItemId parameter, otherwise it always set packetType parameter
        [Title("Create Communication"), HttpGet]
        [Route("Cases/{caseId}/Communications/Create/{templateKey?}", Name = "CreateCaseCommunication")]
        [Route("Todos/{todoItemId}/Communications/Create", Name = "CreateCaseWorkFlowItemCommunication")]
        [Secure("SendCommunication")]
        public ViewResult Create(string draftId)
        {
            string caseId = RouteData.Values["caseId"] as string;
            string templateKey = RouteData.Values["templateKey"] as string;
            string toDoItemId = RouteData.Values["todoItemId"] as string;

            if (string.IsNullOrWhiteSpace(caseId) && string.IsNullOrWhiteSpace(toDoItemId))
                throw new ArgumentNullException("caseId");

            if (!string.IsNullOrEmpty(templateKey))
                templateKey = templateKey.ToUpperInvariant();

            if (!string.IsNullOrEmpty(toDoItemId) && string.IsNullOrEmpty(draftId))
            {
                draftId = (Communication.AsQueryable().Where(t => t.ToDoItemId == toDoItemId && t.IsDraft && !t.IsDeleted).OrderByDescending(o => o.CreatedDate).Select(i => i.Id).FirstOrDefault() ?? "");
            }
            CreateCummunicationViewModel model = new CreateCummunicationViewModel()
            {
                CaseId = caseId,
                TemplateKey = templateKey,
                ToDoItemId = toDoItemId,
                CommunicationId = draftId
            };

            Case theCase = Case.GetById(caseId);
            if (theCase != null)
            {
                model.EmployeeId = theCase.Employee.Id;
            }
            else
            {
                ToDoItem todo = ToDoItem.GetById(toDoItemId);
                if (todo != null)
                {
                    model.EmployeeId = todo.EmployeeId;
                    model.CaseId = todo.CaseId;
                    model.TemplateKey = todo.Metadata.GetRawValue<string>("Template");
                }
            }

            ViewBag.CreationDate = DateTime.UtcNow;
            return View(model);
        }

        [Title("Create Communication"), HttpPost]
        [Route("Cases/{caseId}/Communications/Create", Name = "CreateCaseCommunicationPost")]
        [Route("Todos/{todoItemId}/Communications/Create", Name = "CreateCaseWorkFlowItemCommunicationPost")]
        [Secure("SendCommunication")]
        [ValidateApiAntiForgeryToken]
        public JsonResult Create(CreateCummunicationViewModel model)
        {
            string errorMsg = null;
            try
            {
                if (ModelState.IsValid)
                {
                    Communication comm = model.BuildCommunication();


                    if (model.Recipients.Count > 0)
                    {
                        if (model.Recipients.Any(m => m.Email == null || m.Email == string.Empty))
                        {
                            throw new AbsenceSoftException("Email(To) address is incorrect or not available.");
                        }
                    }
                    if (model.CCRecipients.Count > 0)
                    {
                        if (model.CCRecipients.Any(m => m.Email == null || m.Email == string.Empty))
                        {
                            throw new AbsenceSoftException("Email(CC) address is incorrect or not available.");
                        }
                    }

                  

                    ToDoItem wf = null;
                    if (!string.IsNullOrWhiteSpace(model.ToDoItemId))
                        wf = ToDoItem.GetById(model.ToDoItemId);

                    try
                    {
                        comm = new CommunicationService().Using(c => c.SendCommunication(comm, wf, model.TemplateKey));
                        return comm != null ? Json(new CreateCummunicationViewModel(comm)) : null;
                    }
                    catch (Exception ex)
                    {
                        errorMsg = string.Concat("Error ID: ", RandomString.Generate(8, true, true, true, false), " - ", ex.Message);
                        Log.Error(errorMsg, ex);
                        return JsonError(ex, errorMsg);

                    }
                }
                // No good, invalid model state, return back our original view.
                return Json(model);
            }
            catch (Exception ex)
            {
                errorMsg = string.Concat("Error ID: ", RandomString.Generate(8, true, true, true, false), " - ", ex.Message);
                Log.Error(errorMsg,ex);
                return JsonError(ex, errorMsg);
            }
        }

        [Secure("SendCommunication")]
        [HttpPost, Route("Communications/SaveAsDraft")]
        [ValidateApiAntiForgeryToken]
        public JsonResult SaveAsDraft(CreateCummunicationViewModel model)
        {
            using (var communicationService = new CommunicationService())
            {
                Communication comm = model.BuildCommunication(communicationService.GetCommunication(model.CommunicationId));
                comm.IsDraft = true;
                comm = communicationService.SaveCommunication(comm);
                return Json(comm);
            }
        }

        [Secure("SendCommunication")]
        [HttpPost, Route("Communications/DeleteDrafts")]
        [ValidateApiAntiForgeryToken]
        public JsonResult DeleteDrafts(CommunicationListViewModel comms)
        {
            using (var communicationService = new CommunicationService())
            {
                if (comms != null && comms.Communications != null && comms.Communications.Count() > 0)
                {
                    communicationService.BulkDraftDelete(comms.Communications.Select(c => c.CommunicationId).ToArray());
                }
                return Json(true);
            }
        }

        [Title("Resend Communication"), HttpGet]
        [Route("Communications/Resend/{communicationId}", Name = "ResendCaseCommunication")]
        [Secure("SendCommunication")]
        public ActionResult Resend(string communicationId)
        {
            if (string.IsNullOrWhiteSpace(communicationId))
                throw new ArgumentNullException("communicationId");

            Communication comm = Communication.GetById(communicationId);
            if (comm == null)
                return new HttpStatusCodeResult(404, "Communication was not found");

            CreateCummunicationViewModel model = new Models.Communications.CreateCummunicationViewModel()
            {
                CommunicationId = communicationId,
                IsResend = true,

                TemplateKey = comm.Template,
                CaseId = comm.CaseId,
                EmployeeId = comm.EmployeeId
            };
            model.SendECommunication = comm.Case.SendECommunication;
            if (model.SendECommunication.HasValue && (!model.SendECommunication.Value) && (!string.IsNullOrWhiteSpace(communicationId)))
            {
                model.CommunicationTypeValue = (int)CommunicationType.Mail;
            }
            ViewBag.CreationDate = DateTime.UtcNow;

            return View("Create", model);
        }



        [Route("Cases/Communications/GetCommunicationList", Name = "GetCommunicationList")]
        [Secure("SendCommunication")]
        [HttpGet]
        public JsonResult GetCommunicationList(ListCriteria criteria, string CaseId)
        {
            try
            {
                criteria.Set("CaseId", CaseId);
                CommunicationService obj = new CommunicationService();
                return Json(obj.CommunicationList(criteria), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }


        [Route("communications/list", Name = "CommunicationList")]
        [Secure("SendCommunication")]
        [HttpPost]
        [ValidateApiAntiForgeryToken]
        public JsonResult GetCommunicationList(ListCriteria criteria)
        {
            try
            {
                //string CaseId = RouteData.Values["CaseId"].ToString();
                //criteria.Set("CaseId", CaseId);
                CommunicationService obj = new CommunicationService();
                var result = obj.CommunicationList(criteria);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Route("Todos/{toDoItemId}/ChangeStatus", Name = "ChangeStatus")]
        [Secure("SendCommunication")]
        public JsonResult ChangeStatus(string toDoItemId)
        {
            try
            {
                if (!string.IsNullOrEmpty(toDoItemId))
                {
                    var workFlowItem = ToDoItem.GetById(toDoItemId);
                    workFlowItem.Status = ToDoItemStatus.Cancelled;
                    workFlowItem.Save();
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpGet]
        [Route("Cases/{caseId}/Communications/CreateCummunicationViewModel/{templateKey?}")]
        [Route("Todos/{toDoItemId}/Communications/CreateCummunicationViewModel/{templateKey?}")]
        [Route("Communications/{communicationId}/CreateCummunicationViewModel/Resend")]
        [Secure("SendCommunication")]
        public JsonResult CreateCummunicationViewModel(string templateKey)
        {
            try
            {
                string caseId = RouteData.Values["caseId"] as string;
                templateKey = templateKey ?? RouteData.Values["templateKey"] as string;
                string toDoItemId = RouteData.Values["toDoItemId"] as string;
                string communicationId = RouteData.Values["communicationId"] as string;

                if ((string.IsNullOrEmpty(communicationId) || string.IsNullOrWhiteSpace(communicationId)) && string.IsNullOrEmpty(caseId) && !string.IsNullOrEmpty(templateKey) && !string.IsNullOrEmpty(toDoItemId))
                {
                    ToDoItem todo = ToDoItem.GetById(toDoItemId);
                    if (todo != null)
                        caseId = todo.CaseId;
                }
                if (string.IsNullOrWhiteSpace(caseId) && string.IsNullOrWhiteSpace(toDoItemId) && string.IsNullOrWhiteSpace(communicationId))
                    throw new ArgumentNullException("caseId");

                CreateCummunicationViewModel model = new CreateCummunicationViewModel() { CaseId = caseId };
                using (var communicationService = new CommunicationService(CurrentCustomer, CurrentEmployer, CurrentUser))
                {

                    // Is it a resend? Hack, I know, but whatever, our UI sucks.
                    if (!string.IsNullOrWhiteSpace(communicationId))
                    {
                        Communication comm = Communication.GetById(communicationId);
                        if (comm == null)
                            return JsonError(new Exception("Communication was not found"), statusCode: 404);

                        comm = comm.Clone();
                        comm.Clean();
                        comm.Recipients.ForEach(r => r.Clean());
                        comm.CCRecipients.ForEach(r => r.Clean());
                        comm.Paperwork.ForEach(r => r.Clean());
                        model = new CreateCummunicationViewModel(comm);
                        if (model.SendECommunication.HasValue && (!model.SendECommunication.Value) && (!string.IsNullOrWhiteSpace(communicationId)))
                        {
                            model.CommunicationTypeValue = (int)CommunicationType.Mail;
                        }
                        return Json(model, JsonRequestBehavior.AllowGet);
                    }
                    else if (!string.IsNullOrEmpty(templateKey) && string.IsNullOrWhiteSpace(communicationId))
                    {
                        model = new CreateCummunicationViewModel(communicationService.CreateCommunication(caseId, templateKey));
                        model.Subject = string.IsNullOrWhiteSpace(model.Subject) ? model.CommunicationName : model.Subject;
                        model.TemplateKey = templateKey;
                        return Json(model, JsonRequestBehavior.AllowGet);
                    }

                    ToDoItem wf = null;
                    if (!string.IsNullOrEmpty(toDoItemId))
                        wf = ToDoItem.GetById(toDoItemId);


                    if (wf != null)
                    {
                        model = new CreateCummunicationViewModel(communicationService.CreateCommunication(wf));
                        model.ToDoItemId = toDoItemId;
                        templateKey = templateKey ?? wf.Metadata.GetRawValue<string>("Template");
                    }
                    else if (!string.IsNullOrEmpty(templateKey))
                    {
                        model = new CreateCummunicationViewModel(communicationService.CreateCommunication(caseId, templateKey));
                    }

                    model.Subject = string.IsNullOrWhiteSpace(model.Subject) ? model.CommunicationName : model.Subject;
                    model.TemplateKey = templateKey;

                    /// Set the values from ToDoItem that communication needs when we're getting a template
                    if (wf != null && model.SendECommunication.HasValue == false)
                    {
                        int defaultSendMethod = wf.Metadata.GetRawValue<int?>("CommunicationType") ?? 0;
                        model.CommunicationTypeValue = defaultSendMethod;
                    }

                    return Json(model, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        /// <summary>
        /// Downloads the communication packet as a single file. Well, it attempts to anyway.
        /// </summary>
        /// <param name="caseId">The case id.</param>
        /// <param name="communicationId">The communication id.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("Communications/DownloadPacket/{communicationId}")]
        [Secure("SendCommunication")]
        public ActionResult DownloadCommunicationPacket(string communicationId)
        {
            Communication comm = Communication.AsQueryable(CurrentUser, Permission.SendCommunication)
                .Where(c => c.Id == communicationId).FirstOrDefault();
            if (comm == null)
                return HttpNotFound();

            Document doc = null;
            using (CommunicationService svc = new CommunicationService())
                doc = svc.CreateCommunicationPackage(comm);

            if (doc == null || doc.File == null || doc.File.LongLength <= 0L)
                return HttpNotFound();

            return File(doc.File, doc.ContentType ?? "application/pdf");
        }
    }
}