﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Models.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Web.Filters;
using AbsenceSoft.Models.AccommodationTypes;

namespace AbsenceSoft.Controllers
{
    [Secure]
    public class DenialReasonController : BaseController
    {
        [Title("View Denial Reasons")]
        [HttpGet, Route("DenialReason/List/{employerId?}", Name = "ViewDenialReasons")]
        public ActionResult ViewDenialReasons(string employerId)
        {
            ViewBag.EmployerId = employerId;
            return View();
        }

        [HttpPost, Route("DenialReason/List", Name = "List")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListDenialReasons(ListCriteria criteria)
        {
            using (var caseService = new CaseService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                ListResults results = caseService.DenialReasonList(criteria);
                return PartialView(results);
            }
        }

        [Title("Edit Denial Reason")]
        [HttpGet, Route("DenialReason/Edit/{employerId?}", Name = "Edit")]
        public ActionResult EditDenialReason(string employerId, string id)
        {
            using (var caseService = new CaseService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                DenialReasonViewModel viewModel = new DenialReasonViewModel(caseService.GetDenialReasonById(id));
                return View(viewModel);
            }
        }

        [Title("Edit Denial Reason")]
        [HttpPost, Route("DenialReason/Save", Name = "SaveDenialReasons")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveDenialReason(DenialReasonViewModel denialReason)
        {
            if (!ModelState.IsValid)
                return PartialView("EditDenialReason", denialReason);

            using (var caseService = new CaseService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                DenialReason reason = denialReason.ApplyToDataModel(caseService.GetDenialReasonById(denialReason.Id));
                caseService.SaveDenialReason(reason);
            }

            return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewDenialReasons"));
        }

        [HttpDelete, Route("DenialReason/Delete/{employerId?}", Name = "Delete")]
        public ActionResult DeleteDenialReason(string employerId, string id)
        {
            using (var caseService = new CaseService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                DenialReason reason = caseService.GetDenialReasonById(id);
                if (reason.IsCustom)
                    caseService.DeleteDenialReason(reason);

                return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewDenialReasons"));
            }
        }

        [HttpPost, Route("DenialReason/Toggle/{employerId?}", Name = "Toggle")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ToggleDenialReasonSuppression(DenialReasonViewModel denialReason)
        {
            using (var caseService = new CaseService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                var savedDenialReason = caseService.ToggleDenialReasonByCode(denialReason.Code);
                return Json(denialReason);
            }
        }

    }
}