﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Necessitys;
using AbsenceSoft.Models.Demands;
using AbsenceSoft.Models.Employees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Common;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    [Secure]
    public class NecessityController : BaseController
    {
        #region Admin

        /// <summary>
        /// Necessities the list.
        /// </summary>
        /// <param name="employerId">The employer identifier.</param>
        /// <returns></returns>
        [Secure("EditNecessities"), Title("Necessities")]
        [HttpGet, Route("Necessity/List/{employerId?}", Name ="ViewNecessities")]
        public ActionResult ViewNecessities(string employerId)
        {
            ViewBag.EmployerId = employerId;
            return View();
        }

        /// <summary>
        /// Lists the necessities.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        [Secure("EditNecessities")]
        [HttpPost, Route("Necessity/List", Name = "ListNecessities")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListNecessities(ListCriteria criteria)
        {
            using (var necService = new NecessityService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                var necessities = necService.NecessityList(criteria);
                ViewBag.EmployerId = criteria.Get<string>("EmployerId");
                return PartialView(necessities);
            }
        }

        /// <summary>
        /// Necessities the edit.
        /// </summary>
        /// <param name="employerId">The employer identifier.</param>
        /// <param name="id">The necessity identifier.</param>
        /// <returns></returns>
        [Secure("EditNecessities"), Title("Edit Necessity")]
        [HttpGet, Route("Necessity/EditNecessity/{employerId?}", Name = "EditNecessity")]
        public ActionResult NecessityEdit(string employerId, string id = null)
        {
            using (var necessityService = new NecessityService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                NecessityViewModel necessity = new NecessityViewModel(necessityService.GetNecessity(id));
                ViewBag.EmployerId = employerId;
                return View(necessity);
            }
        }

        /// <summary>
        /// Saves the necessity.
        /// </summary>
        /// <param name="employerId">The employer identifier.</param>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [Secure("EditNecessities")]
        [HttpPost, Route("Necessity/SaveNecessity", Name = "NecessitySave")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveNecessity(NecessityViewModel model)
        {
            if (!ModelState.IsValid)
                return PartialView(model);

            using (var svc = new NecessityService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                Necessity necessity = model.ApplyToDataModel(svc.GetNecessity(model.Id));
                svc.SaveNecessity(necessity);
            }
                

            return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewNecessities"));
        }

        /// <summary>
        /// Deletes the necessity.
        /// </summary>
        /// <param name="employerId">The employer identifier.</param>
        /// <param name="id">The necessity identifier.</param>
        /// <returns></returns>
        [Secure("EditNecessities")]
        [HttpDelete, Route("Necessity/Delete/{employerId?}", Name = "DeleteNecessity")]
        public ActionResult DeleteNecessity(string employerId, string id)
        {
            try
            {
                using (var svc = new NecessityService(CurrentCustomer, CurrentEmployer, CurrentUser))
                {
                    svc.RemoveNecessity(id);
                }
                    
                return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewNecessities"));
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to delete Necessity");
            }
        }

        #endregion Admin

        #region Employee

        /// <summary>
        /// Gets the employee necessity.
        /// </summary>
        /// <param name="caseId">The case identifier.</param>
        /// <param name="employeeNecessityId">The employee necessity identifier.</param>
        /// <returns></returns>
        /// <exception cref="AbsenceSoftException">Unable to find the specified case to apply the work restriction to.</exception>
        [Secure("AddEmployeeNecessity", "EditEmployeeNecessity")]
        [HttpGet, Route("Necessity/{caseId}/EmployeeNecessity/{employeeNecessityId?}", Name = "EmployeeNecessity")]
        public ActionResult GetEmployeeNecessity(string caseId, string employeeNecessityId)
        {
            Case theCase = Case.GetById(caseId);
            if (theCase == null)
                throw new AbsenceSoftException("Unable to find the specified case to apply the work restriction to.");

            EmployeeNecessityViewModel viewModel = new EmployeeNecessityViewModel(EmployeeNecessity.GetById(employeeNecessityId));
            viewModel.CaseId = theCase.Id;
            viewModel.EmployeeId = theCase.Employee.Id;

            return PartialView("EmployeeNecessity", viewModel);
        }

        /// <summary>
        /// Saves the employee necessity.
        /// </summary>
        /// <param name="caseId">The case identifier.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns></returns>
        /// <exception cref="AbsenceSoftException">Unable to find the specified case to apply the work restriction to.</exception>
        [Secure("AddEmployeeNecessity", "EditEmployeeNecessity")]
        [HttpPost, Route("Necessity/{caseId}/EmployeeNecessityEntry", Name = "EmployeeNecessitySave")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveEmployeeNecessity(string caseId, EmployeeNecessityViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return PartialView("EmployeeNecessity", viewModel);

            Case theCase = Case.GetById(viewModel.CaseId);
            if (theCase == null)
                throw new AbsenceSoftException("Unable to find the specified case to apply the work restriction to.");

            using (var svc = new NecessityService(CurrentUser))
                svc.ChangeEmployeeNecessity(theCase.Employee.Id,
                    viewModel.NecessityId,viewModel.Id,
                    viewModel.StartDate.HasValue ? viewModel.StartDate.Value.AsRange(viewModel.EndDate) : DateRange.Null,
                    theCase.Id);

            return RedirectViaJavaScript(Url.RouteUrl("ViewCase", new { caseId = viewModel.CaseId }));
        }

        /// <summary>
        /// Deletes the employee necessity.
        /// </summary>
        /// <param name="caseId">The case identifier.</param>
        /// <param name="employeeNecessityId">The employee necessity identifier.</param>
        /// <returns></returns>
        [Secure("DeleteEmployeeNecessity")]
        [HttpDelete, Route("Necessity/{caseId}/EmployeeNecessity/{employeeNecessityId}/Delete", Name = "DeleteEmployeeNecessity")]
        public ActionResult DeleteEmployeeNecessity(string caseId, string employeeNecessityId)
        {
            try
            {
                using (var svc = new NecessityService(CurrentUser))
                    svc.RemoveEmployeeNecessity(employeeNecessityId);
                return RedirectViaJavaScript(Url.RouteUrl("ViewCase", new { caseId = caseId }));
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to delete Employee Necessity");
            }
        }

        #endregion Employee
    }
}