﻿using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Communications;
using AbsenceSoft.Models.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Common;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    [Secure("EditCommunicationTemplate")]
    public class TemplatesController : BaseController
    {

        #region Communication Templates
        [Title("Communication Templates")]
        [HttpGet, Route("Templates/ListCommunications/{employerId?}", Name = "ViewCommunications")]
        public ActionResult ViewCommunications(string employerId)
        {
            ViewBag.EmployerId = employerId;
            return View();
        }

        [HttpPost, Route("Templates/ListCommunications/{employerId?}", Name = "ListCommunications")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListCommunications(string employerId, ListCriteria criteria)
        {
            using (var templateService = new TemplateService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                ListResults results = templateService.CommunicationsTemplateList(criteria);
                return PartialView(results);
            }
        }

        [Title("Edit Communications")]
        [HttpGet, Route("Templates/EditCommunications/{employerId?}", Name = "EditCommunications")]
        public ActionResult EditCommunications(string employerId, string id)
        {
            using (var templateService = new TemplateService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                CommunicationTemplateViewModel viewModel = new CommunicationTemplateViewModel(templateService.GetCommunicationTemplate(id));
                ViewBag.EmployerId = employerId;
                return View(viewModel);
            }
        }

        [HttpPost, Route("Templates/SaveCommunications/{employerId?}", Name = "SaveCommunications")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveCommunications(string employerId, CommunicationTemplateViewModel communicationTemplate, HttpPostedFileBase document)
        {
            if (!ModelState.IsValid)
                return View("EditCommunications", communicationTemplate);

            using (var templateService = new TemplateService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                // New Communication template
                if (string.IsNullOrEmpty(communicationTemplate.Id) && templateService.GetTemplateByCode(communicationTemplate.Code) != null)
                {
                    ModelState.AddModelError(string.Empty, "Communication Template Code is already existing");
                    return View("EditCommunications", communicationTemplate);
                }
            }

            if (document != null && document.ContentLength > 0)
            {
                using (var fileService = new FileService(CurrentCustomer, CurrentEmployer, CurrentUser))
                {
                    Document uploadedDoc = fileService.UploadAdministrationDocument(document);
                    communicationTemplate.DocumentId = uploadedDoc.Id;
                    communicationTemplate.DocumentName = uploadedDoc.FileName;
                }
            }

            using (var templateService = new TemplateService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                Template template = communicationTemplate.ApplyToDataModel(templateService.GetCommunicationTemplate(communicationTemplate.Id));
                templateService.SaveCommunicationTemplate(template);
                Permission.AddCommunicationToAllRoles(CurrentCustomer.Id, template.Code);
            }

            return RedirectToRoute("ViewCommunications");
        }

        [HttpDelete, Route("Templates/DeleteCommunications/{employerId?}", Name = "DeleteCommunications")]
        public ActionResult DeleteCommunications(string employerId, string id)
        {
            using (var templateService = new TemplateService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                Template template = templateService.GetCommunicationTemplate(id);
                if (template.IsCustom)
                    templateService.DeleteCommunicationTemplate(template);

                return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewCommunications"));
            }
        }

        [HttpPost, Route("Templates/ToggleEnablingCommunication/{employerId?}", Name = "ToggleEnablingCommunication")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ToggleEnablingCommunication(CommunicationTemplateViewModel communicationTemplate)
        {
            using (var templateService = new TemplateService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                var template = templateService.ToggleCommunicationTemplateSuppressionById(communicationTemplate.Id);
                communicationTemplate.Suppressed = template.Suppressed;
                return Json(communicationTemplate);
            }
        }

        [FileDownload]
        [HttpGet, Route("Templates/Catalog/{employerId?}", Name ="DownloadTemplateCatalog")]
        public ActionResult DownloadTemplateCatalog(string employerId)
        {
            using (var templateService = new TemplateService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                return File(templateService.DownloadTemplateCatalog(), "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "Catalog.docx");
            }
        }

        #endregion

        #region Paperwork Templates

        [Title("Paperwork Templates")]
        [HttpGet, Route("Templates/ListPaperwork/{employerId?}", Name = "ViewPaperwork")]
        public ActionResult ViewPaperwork(string employerId)
        {
            ViewBag.EmployerId = employerId;
            return View();
        }

        [HttpPost, Route("Templates/ListPaperwork/{employerId?}", Name = "ListPaperwork")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListPaperwork(string employerId, ListCriteria criteria)
        {
            using (var templateService = new TemplateService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                ListResults results = templateService.PaperworkTemplateList(criteria);
                return PartialView(results);
            }
        }

        [Title("Edit Paperwork")]
        [HttpGet, Route("Templates/EditPaperwork/{employerId?}", Name = "EditPaperwork")]
        public ActionResult EditPaperwork(string employerId, string id)
        {
            using (var templateService = new TemplateService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                PaperworkTemplateViewModel viewModel = new PaperworkTemplateViewModel(templateService.GetPaperworkTemplate(id));                
                ViewBag.EmployerId = employerId; 
                return View(viewModel);
            }
        }

        [HttpPost, Route("Templates/SavePaperwork/{employerId?}", Name = "SavePaperwork")]
        [ValidateAntiForgeryToken]
        public ActionResult SavePaperwork(string employerId, PaperworkTemplateViewModel paperworkTemplate, HttpPostedFileBase document)
        {
            if (!ModelState.IsValid)
                return View("EditPaperwork", paperworkTemplate);

            using (var templateService = new TemplateService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                // New Paperwork template
                if (string.IsNullOrEmpty(paperworkTemplate.Id) && templateService.GetPaperworkByCode(paperworkTemplate.Code) != null)
                {
                    ModelState.AddModelError(string.Empty, "Paperwork Template Code is already existing");
                    return View("EditPaperwork", paperworkTemplate);
                }
            }

            if (document != null && document.ContentLength > 0) // this is new template creation & edit scenario when new document is uploaded
            {
                if (!FileTypeMatchesDocumentType(paperworkTemplate.DocType, System.IO.Path.GetExtension(document.FileName).ToLower()))
                {
                    ModelState.AddModelError("DOCTYPEMISMATCH", "The uploaded file does not match the document type selected. Please upload correct file or change the document type to match uploaded file.");
                    return View("EditPaperwork", paperworkTemplate);
                }

                using (var fileService = new FileService(CurrentCustomer, CurrentEmployer, CurrentUser))
                {
                    Document uploadedDoc = fileService.UploadAdministrationDocument(document);
                    paperworkTemplate.FileId = uploadedDoc.Id;
                    paperworkTemplate.FileName = uploadedDoc.FileName;
                }
            }

            // This is edit template case, user might change document type, but not uploading the new file.
            if ( !string.IsNullOrEmpty(paperworkTemplate.Id))
            {
                if(!string.IsNullOrWhiteSpace(paperworkTemplate.FileName))
                {
                    if (!FileTypeMatchesDocumentType(paperworkTemplate.DocType, System.IO.Path.GetExtension(paperworkTemplate.FileName).ToLower()))
                    {
                        ModelState.AddModelError("DOCTYPEMISMATCH", "The uploaded file does not match the document type selected. Please upload correct file or change the document type to match uploaded file.");
                        return View("EditPaperwork", paperworkTemplate);
                    }
                }
            }

            using (var templateService = new TemplateService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                Paperwork paperwork = paperworkTemplate.ApplyToDataModel(templateService.GetPaperworkTemplate(paperworkTemplate.Id));
                templateService.SavePaperworkTemplate(paperwork);
            }

            return RedirectToRoute("ViewPaperwork");
        }

        private bool FileTypeMatchesDocumentType(DocumentType docType, string fileExtension)
        {
            if ( docType == DocumentType.MSWordDocument && !(fileExtension == ".doc" || fileExtension == ".docx"))
                return false;

            if ( docType == DocumentType.Pdf && fileExtension != ".pdf")
                return false;

            return true;
        }

        [HttpDelete, Route("Templates/DeletePaperwork/{employerId?}", Name = "DeletePaperwork")]
        public ActionResult DeletePaperwork(string employerId, string id)
        {
            using (var templateService = new TemplateService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                Paperwork paperwork = templateService.GetPaperworkTemplate(id);
                if (paperwork.IsCustom)
                    templateService.DeletePaperworkTemplate(paperwork);

                return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewPaperwork"));
            }
        }



        #endregion
    }
}