﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Models.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Controllers
{
    public class CaseAssignmentController : BaseController
    {

        #region Case Assignment Type Administration

        [HttpGet, Route("CaseAssignment/{customerId}/Types/{employerId?}")]
        public ActionResult Index(string customerId, string employerId = null)
        {
            List<CaseAssignmentType> customerTypes = new CaseAssignmentService(CurrentUser).GetCaseAssignmentTypes(customerId, employerId);
            if (Request.IsAjaxRequest())
                return Json(customerTypes);
            
            ViewBag.CustomerId = customerId;
            return View(customerTypes);
        }

        [HttpGet, Route("CaseAssignment/{customerId}/TypeEdit/{caseAssignmentTypeId?}", Name="EditCaseAssignmentType")]
        public ActionResult EditCaseAssignmentType(string customerId, string caseAssignmentTypeId = null)
        {
            CaseAssignmentType type = null;
            
            if (string.IsNullOrEmpty(caseAssignmentTypeId))
                type = new CaseAssignmentType()
                {
                    CustomerId = customerId
                };
            else
                type = new CaseAssignmentService(CurrentUser).GetCaseAssignmentType(customerId, caseAssignmentTypeId);

            return View(type);
        }

        [HttpPost, Route("CaseAssignment/Save")]
        public ActionResult SaveCaseAssignmentType(CaseAssignmentType type)
        {
            try
            {
                type = new CaseAssignmentService(CurrentUser).UpdateCaseAssignmentType(type);
                return Json(type);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to save case assignment type");
                
            }
        }

        [HttpDelete, Route("CaseAssignment/{customerId}/Delete/{caseAssignmentTypeId}")]
        public ActionResult DeleteCaseAssignmentRole(string customerId, string caseAssignmentTypeId)
        {
            try
            {
                using (CaseAssignmentService svc = new CaseAssignmentService(CurrentUser))
                {
                    CaseAssignmentType type = svc.GetCaseAssignmentType(customerId, caseAssignmentTypeId);
                    if (type != null)
                        svc.DeleteCaseAssignmentType(type);

                    return Json(true);
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to delete case assignment type");
            }
        }

        #endregion

        #region Case Assignment 
        
        [HttpGet, Route("CaseAssignment/{caseId}/AssignableUsers")]
        public JsonResult CaseAssignmentAssignableUsers(string caseId)
        {
            try
            {
                Case theCase = Case.GetById(caseId);
                if (theCase == null)
                    return JsonError(new Exception("Unable to find case"), "Unable to find specified case");

                return Json(new CaseAssignmentService(CurrentUser).GetUsersForCase(theCase));
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to retrieve users to be assigned to the case");
            }
        }

        [HttpPost, Route("CaseAssignment/UpdateCaseAssignment")]
        public JsonResult CaseAssignmentUpdateAssignment(BulkUpdateCaseAssignment updateCaseAssignment)
        {
            try
            {
                Case theCase = Case.GetById(updateCaseAssignment.CaseId);
                if(theCase == null)
                    return JsonError(new Exception("Unable to find case"), "Unable to find specified case");

                ///Lets do the primary assignee first
                theCase.AssignedToId = updateCaseAssignment.UserId;

                using (CaseAssignmentService svc = new CaseAssignmentService(CurrentUser))
                {
                    foreach (Assignee assignee in updateCaseAssignment.Assignees)
                    {
                        /// if they already have an assignment, we need to see if something changed or not
                        Assignee currentAssignment = theCase.AssignedUsers.Where(au => au.Id == assignee.Id).FirstOrDefault();
                        if (currentAssignment != null)
                        {
                            /// If the person and role are the same, nothing changed, nothing to do
                            if (currentAssignment.AssignedToId == assignee.AssignedToId && currentAssignment.AssignmentType.Id == assignee.AssignmentType.Id)
                            {
                                continue;
                            }
                            else
                            {
                                /// let's just make it easy, we're going to unassign them and then assign the new user
                                svc.RemoveUserFromCase(theCase, currentAssignment.AssignedToId);
                            }
                        }

                        svc.AssignUserToCaseType(theCase, assignee.AssignedToId, assignee.AssignmentType.Id);
                    }    
                }

                new CaseService().Using(p => p.UpdateCase(theCase));

                return Json(true);
            }
            catch (AbsenceSoftException asEx)
            {
                return JsonError(asEx, asEx.Message);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to add user to case");
            }
        }

        [HttpPost, Route("CaseAssignment/AssignUser")]
        public JsonResult CaseAssignmentAssignUser(AssignUserToCase newAssignee)
        {
            try
            {
                Case theCase = Case.GetById(newAssignee.CaseId);
                if(theCase == null)
                    return JsonError(new Exception("Unable to find case"), "Unable to find specified case");

                theCase = new CaseAssignmentService(CurrentUser).AssignUserToCaseType(theCase, newAssignee.UserId, newAssignee.CaseAssignmentRoleId);
                new CaseService().Using(p => p.UpdateCase(theCase));
                return Json(theCase);
            }
            catch (AbsenceSoftException asEx)
            {
                return JsonError(asEx, asEx.Message);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to add user to case");
            }
        }

        [HttpDelete, Route("CaseAssignment/{caseId}/DeleteUser/{userId}")]
        public JsonResult CaseAssignmentDeleteUser(string caseId, string userId)
        {
            try
            {
                Case theCase = Case.GetById(caseId);
                if(theCase == null)
                    return JsonError(new Exception("Unable to find case"), "Unable to find specified case");

                theCase = new CaseAssignmentService(CurrentUser).RemoveUserFromCase(theCase, userId);
                new CaseService().Using(p => p.UpdateCase(theCase));
                return Json(theCase);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to remove user from case");
            }
        }

        #endregion
    }
}