﻿using AbsenceSoft.Logic.Common;
using AbsenceSoft.Models.Health;
using Microsoft.VisualBasic.Devices;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;

namespace AbsenceSoft.Controllers
{
    public class HealthController : Controller
    {
        // GET: Health
        public ActionResult Index(string id)
        {
            HealthCheckViewModel model = new HealthCheckViewModel();
            model.ShowExceptionDetail = string.Equals(id, "1ad3f7a60c1447878cd88a175722e15e", StringComparison.InvariantCultureIgnoreCase);

            Parallel.Invoke(
                () => {
                    CheckServerDiskSpace(model);
                    CheckServerMemory(model);
                    CheckServerCPU(model);
                },
                () => CheckNetwork(model),
                () => CheckDatabase(model),
                () => CheckDataWarehouse(model),
                async () => await CheckS3(model),
                async () => await CheckSqs(model),
                () => CheckSmtp(model));

            if (model.ServerOK && !model.ServerMessage.Any())
                model.ServerMessage.Add("Server is OK");
            if (model.DatabaseOK && !model.DatabaseMessage.Any())
                model.DatabaseMessage.Add("Database is OK");
            if (model.DataWarehouseOK && !model.DataWarehouseMessage.Any())
                model.DataWarehouseMessage.Add("Data Warehouse is OK");
            if (model.DnsOK && !model.DnsMessage.Any())
                model.DnsMessage.Add("DNS / Network is OK");
            if (model.S3OK && !model.S3Message.Any())
                model.S3Message.Add("S3 is OK");
            if (model.SmtpOK && !model.SmtpMessage.Any())
                model.SmtpMessage.Add("SES / SMTP is OK");
            if (model.SqsOK && !model.SqsMessage.Any())
                model.SqsMessage.Add("SQS is OK");

            if (!model.IsOK)
                Response.StatusCode = 500;

            return View(model);
        }

        private void CheckServerDiskSpace(HealthCheckViewModel model)
        {
            bool driveOk = false;
            try
            {
                DriveInfo info = DriveInfo.GetDrives().FirstOrDefault(d => d.IsReady && d.Name.ToLowerInvariant().StartsWith("c"));
                if (info != null)
                {
                    var free = (Double)info.TotalFreeSpace / (Double)info.TotalSize;
                    if (free > .1D)
                        driveOk = true;
                    else
                        model.ServerMessage.AddFormat("Available disk space is only at {0:P2} and should have at least 10% free", free);
                }
                else
                {
                    driveOk = true;
                    model.ServerMessage.Add("WARNING: Server primary drive could not be found to measure available disk space");
                }
            }
            catch (Exception ex)
            {
                if (model.ShowExceptionDetail)
                    ex.ToString().Split('\n').ForEach(model.ServerMessage.Add);
                else
                    model.ServerMessage.Add("Error detecting available disk space");
            }
            model.ServerOK = model.ServerOK & driveOk;
        }

        private void CheckServerMemory(HealthCheckViewModel model)
        {
            bool memOk = false;
            try
            {
                var info = new ComputerInfo();
                if (info != null)
                {
                    var free = (Double)info.AvailablePhysicalMemory / (Double)info.TotalPhysicalMemory;
                    if (free > .10D)
                        memOk = true;
                    else
                        model.ServerMessage.AddFormat("Available RAM is only at {0:P2} and should have at least 10% free", free);
                }
                else
                {
                    memOk = true;
                    model.ServerMessage.Add("WARNING: Server info could not be found to measure available memory");
                }
            }
            catch (Exception ex)
            {
                if (model.ShowExceptionDetail)
                    ex.ToString().Split('\n').ForEach(model.ServerMessage.Add);
                else
                    model.ServerMessage.Add("Error detecting available memory");
            }
            model.ServerOK = model.ServerOK & memOk;
        }

        private void CheckServerCPU(HealthCheckViewModel model)
        {
            //http://stackoverflow.com/questions/4679962/what-is-the-correct-performance-counter-to-get-cpu-and-memory-usage-of-a-process/4680030#4680030
            bool cpuOk = false;
            try
            {
                PerformanceCounter total_cpu = new PerformanceCounter("Processor", "% Processor Time", "_Total", true);
                if (total_cpu != null)
                {
                    var used = total_cpu.NextValue();
                    if (used <= 90f)
                        cpuOk = true;
                    else
                        model.ServerMessage.AddFormat("CPU usage is currently at {0:P2} and should be no more than 90%", used);
                }
                else
                {
                    cpuOk = true;
                    model.ServerMessage.Add("WARNING: '% Processor Time' _Total performance counter could not be accessed");
                }
            }
            catch (Exception ex)
            {
                if (model.ShowExceptionDetail)
                    ex.ToString().Split('\n').ForEach(model.ServerMessage.Add);
                else
                    model.ServerMessage.Add("Error detecting available memory");
            }
            model.ServerOK = model.ServerOK & cpuOk;
        }

        private void CheckNetwork(HealthCheckViewModel model)
        {
            bool networkOk = false;
            try
            {
                var info = new ServerComputer();
                if (info != null)
                {
                    if (!info.Network.IsAvailable)
                        model.DnsMessage.Add("Network is not available");
                    else
                    {
                        networkOk = info.Network.IsAvailable & info.Network.Ping("www.google.com");
                        if (!networkOk)
                            model.DnsMessage.Add("Unable to reach external DNS server or could not resolve/reach outside services (PING Failed)");
                    }
                }
                else
                {
                    networkOk = true;
                    model.DnsMessage.Add("WARNING: Server info could not be found to measure network health");
                }
            }
            catch (Exception ex)
            {
                if (model.ShowExceptionDetail)
                    ex.ToString().Split('\n').ForEach(model.DnsMessage.Add);
                else
                    model.DnsMessage.Add("Error detecting network health");
            }
            model.DnsOK = networkOk;
        }

        private void CheckDatabase(HealthCheckViewModel model)
        {
            bool dbOk = false;
            try
            {
                AbsenceSoft.Data.Security.User.AsQueryable().Where(u => u.Id == AbsenceSoft.Data.Security.User.DefaultUserId).Count();
                dbOk = true;
            }
            catch (Exception ex)
            {
                if (model.ShowExceptionDetail)
                    ex.ToString().Split('\n').ForEach(model.DatabaseMessage.Add);
                else
                    model.DatabaseMessage.Add("Error detecting database connection");
            }
            model.DatabaseOK = model.DatabaseOK & dbOk;
        }

        private void CheckDataWarehouse(HealthCheckViewModel model)
        {
            var dwOk = false;
            try
            {
                var cmd = new AbsenceSoft.Common.Postgres.CommandRunner("DataWarehouse");
                int i = cmd.ExecuteSingle<int>("SELECT 321 FROM mat_view_customer LIMIT 1");
                if (i == 321)
                    dwOk = true;
                else
                    model.DataWarehouseMessage.AddFormat("Did not get expected results from data warehouse test; expected 321 but got {0}", i);
            }
            catch (Exception ex)
            {
                if (model.ShowExceptionDetail)
                    ex.ToString().Split('\n').ForEach(model.DataWarehouseMessage.Add);
                else
                    model.DataWarehouseMessage.Add("Error detecting database connection");
            }
            model.DataWarehouseOK = model.DataWarehouseOK & dwOk;
        }

        private async Task CheckS3(HealthCheckViewModel model)
        {
            var s3Ok = false;
            try
            {
                using (var fs = new FileService())
                {
                    if (await fs.IsAliveAsync())
                        s3Ok = true;
                    else
                        model.S3Message.Add("AWS S3 and/or the Files bucket was not available or was not able to connect to that resource/region.");
                }
            }
            catch (Exception ex)
            {
                if (model.ShowExceptionDetail)
                    ex.ToString().Split('\n').ForEach(model.S3Message.Add);
                else
                    model.S3Message.Add("Error checking AWS S3 service health and bucket availability");
            }
            model.S3OK = model.S3OK & s3Ok;
        }

        private async Task CheckSqs(HealthCheckViewModel model)
        {
            var sqsOk = false;
            try
            {
                using (var fs = new QueueService())
                {
                    if (await fs.IsAliveAsync())
                        sqsOk = true;
                    else
                        model.SqsMessage.Add("AWS SQS and/or the common queue was not available or was not able to connect to that resource/region.");
                }
            }
            catch (Exception ex)
            {
                if (model.ShowExceptionDetail)
                    ex.ToString().Split('\n').ForEach(model.SqsMessage.Add);
                else
                    model.SqsMessage.Add("Error checking AWS SQS service health and queue availability");
            }
            model.SqsOK = model.SqsOK & sqsOk;
        }

        private void CheckSmtp(HealthCheckViewModel model)
        {
            //http://stackoverflow.com/questions/372742/can-i-test-smtpclient-before-calling-client-send
            var smtpOk = false;
            try
            {
                var config = WebConfigurationManager.OpenWebConfiguration("~/web.config");
                var mailSettings = config.GetSectionGroup("system.net/mailSettings") as System.Net.Configuration.MailSettingsSectionGroup;
                if (mailSettings != null)
                {
                    if (mailSettings.Smtp.DeliveryMethod == System.Net.Mail.SmtpDeliveryMethod.Network)
                    {
                        var host = mailSettings.Smtp.Network.Host;
                        var port = mailSettings.Smtp.Network.Port;

                        IPHostEntry hostEntry = Dns.GetHostEntry(host);
                        IPEndPoint endPoint = new IPEndPoint(hostEntry.AddressList[0], port);
                        using (Socket tcpSocket = new Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp))
                        {
                            //try to connect and test the rsponse for code 220 = success
                            tcpSocket.Connect(endPoint);
                            int resp = CheckResponse(tcpSocket);
                            if (resp != 220)
                                model.SmtpMessage.AddFormat("Error making socket connection to mail server, response returned was {0}", resp);
                            else
                            {
                                // send HELO and test the response for code 250 = proper response
                                SendData(tcpSocket, string.Format("HELO {0}\r\n", Dns.GetHostName()));
                                resp = CheckResponse(tcpSocket);
                                if (resp != 250)
                                    model.SmtpMessage.AddFormat("Error saying HELO, response returned was {0}", resp);
                                else
                                    // if we got here it's that we can connect to the smtp server
                                    smtpOk = true;
                            }
                        }
                    }
                    else
                    {
                        smtpOk = true;
                        model.SmtpMessage.Add("INFO: SMTP is not configured to use SES or a network protocol");
                    }
                }
                else
                {
                    smtpOk = true;
                    model.SmtpMessage.Add("WARNING: Unable to get SMTP mail settings from configuration");
                }
            }
            catch (Exception ex)
            {
                if (model.ShowExceptionDetail)
                    ex.ToString().Split('\n').ForEach(model.SmtpMessage.Add);
                else
                    model.SmtpMessage.Add("Error checking AWS SES / SMTP service health and availability");
            }
            model.SmtpOK = model.SmtpOK & smtpOk;
        }

        private static void SendData(Socket socket, string data)
        {
            byte[] dataArray = Encoding.ASCII.GetBytes(data);
            socket.Send(dataArray, 0, dataArray.Length, SocketFlags.None);
        }

        private static int CheckResponse(Socket socket)
        {
            while (socket.Available == 0)
                System.Threading.Thread.Sleep(10);
            byte[] responseArray = new byte[1024];
            socket.Receive(responseArray, 0, socket.Available, SocketFlags.None);
            string responseData = Encoding.ASCII.GetString(responseArray);
            int responseCode = Convert.ToInt32(responseData.Substring(0, 3));
            return responseCode;
        }
    }
}