﻿using AbsenceSoft.Data.Security;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Models.CustomContent;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Controllers
{
    public class CustomContentController : BaseController
    {

        [Secure("EditCustomContent")]
        [HttpGet, Route("CustomContent/List", Name="ViewCustomContent")]
        public ActionResult ViewCustomContent(string customerId)
        {
            return View();
        }

        [Secure("EditCustomContent")]
        [HttpPost, Route("CustomContent/List", Name = "ListCustomContent")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListCustomContent(ListCriteria criteria = null)
        {
            using (var contentService = new ContentService(CurrentUser))
            {
                var customContent = contentService.GetContent(criteria);
                return PartialView(customContent);
            }
        }

        [Secure("EditCustomContent")]
        [HttpGet, Route("CustomContent/EditCustomContent/{id?}", Name = "EditCustomContent")]
        public ActionResult EditCustomContent(string id)
        {
            CustomContentViewModel customContentViewModel = null;
            if (!string.IsNullOrEmpty(id))
            {
                using(var contentService = new ContentService(CurrentUser)){
                    customContentViewModel = new CustomContentViewModel(contentService.GetById(id));   
                }
            }
            else
            {
                customContentViewModel = new CustomContentViewModel();
            }

            return View(customContentViewModel);
        }

        [Secure("EditCustomContent")]
        [HttpPost, Route("CustomContent/SaveCustomContent/", Name = "SaveCustomContent")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveCustomContent(CustomContentViewModel customContent)
        {
            if (!ModelState.IsValid)
                return PartialView("EditCustomContent", customContent);

            using (var contentService = new ContentService(CurrentCustomer, null, CurrentUser))
            {
                var content = contentService.GetById(customContent.Id);
                content = customContent.ApplyToDataModel(content);
                contentService.SaveContent(content);
                return RedirectViaJavaScript(Url.RouteUrl("ViewCustomContent"));
            }
        }

        [Secure("EditCustomContent")]
        [HttpDelete, Route("CustomContent/{customerId}/DeleteCustomContent/{id}", Name = "DeleteCustomContent")]
        public ActionResult DeleteCustomContent(string customerId, string id)
        {
            using (var contentService = new ContentService(CurrentCustomer, null, CurrentUser))
            {
                var content = contentService.GetById(id);
                contentService.DeleteContent(content);
                return RedirectViaJavaScript(Url.RouteUrl("ViewCustomContent"));
            }
        }
    }
}