﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Tasks;
using AbsenceSoft.Models.Demands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AbsenceSoft.Common;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Models.Cases;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    [Secure]
    public class DemandController : BaseController
    {
        #region Demand methods
        
        [Secure("EditPhysicalDemands")]
        [Route("Demand/List/{employerId?}", Name = "ViewPhysicalDemands")]
        public ActionResult ViewPhysicalDemands(string employerId)
        {
            return View();
        }

        [Secure("EditPhysicalDemands")]
        [HttpPost, Route("Demand/List/{employerId?}", Name = "ListDemands")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListDemands(string employerId, ListCriteria criteria)
        {
            using (var demandService = new DemandService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                var demandList = demandService.GetDemandList(criteria);
                return PartialView(demandList);
            }
        }

        [Secure("EditPhysicalDemands")]
        [HttpGet, Route("Demand/ListAll/{employerId?}", Name = "ListAllDemands")]
        public ActionResult ListAllDemands(string employerId)
        {
            using (var demandService = new DemandService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                return Json(demandService.GetDemands());
            }
        }

        [Secure("EditPhysicalDemands")]
        [Route("Demand/EditPhysicalDemand/{employerId?}", Name = "EditPhysicalDemand")]
        public ActionResult EditPhysicalDemand(string employerId, string id)
        {
            using (var demandService = new DemandService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                DemandViewModel viewModel = new DemandViewModel(demandService.GetDemandById(id));
                return View(viewModel);
            }
        }

        [Secure("EditPhysicalDemands")]
        [HttpPost, Route("Demand/SavePhysicalDemand/{employerId?}", Name = "SavePhysicalDemand")]
        [ValidateAntiForgeryToken]
        public ActionResult SavePhysicalDemand(string employerId, DemandViewModel demandViewModel)
        {
            if (!ModelState.IsValid)
                return PartialView("EditPhysicalDemand", demandViewModel);

            using (var demandService = new DemandService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                Demand demand = demandViewModel.ApplyToDataModel(demandService.GetDemandById(demandViewModel.Id));
                demandService.SaveDemand(demand);
            }

            return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewPhysicalDemands"));
        }

        [Secure("EditPhysicalDemands")]
        [HttpDelete, Route("Demand/DeletePhysicalDemand/{employerId?}", Name = "DeleteDemand")]
        public ActionResult DeleteDemand(string employerId, string id)
        {
            using (var demandService = new DemandService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                var demand = Demand.GetById(id);
                demandService.DeleteDemand(demand);
                return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewPhysicalDemands"));
            }
        }

        #endregion

        #region Demand Type Methods

        [Secure("EditPhysicalDemands")]
        [Route("Demand/ListTypes/{employerId?}", Name = "ViewDemandTypes")]
        public ActionResult ViewDemandTypes(string employerId)
        {
            return View();
        }

        [Secure("EditPhysicalDemands")]
        [HttpPost, Route("Demand/ListTypes/{employerId?}", Name = "ListDemandTypes")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListDemandTypes(string employerId, ListCriteria criteria)
        {
            using (var demandService = new DemandService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                var demandTypeList = demandService.GetDemandTypeList(criteria);
                return PartialView(demandTypeList);
            }
        }

        [Secure("EditPhysicalDemands")]
        [Route("Demand/EditDemandType/{employerId?}", Name = "EditDemandType")]
        public ActionResult EditDemandType(string employerId, string id)
        {
            var demandTypeViewModel = string.IsNullOrEmpty(id)
                ? new DemandTypeViewModel()
                : new DemandTypeViewModel(DemandType.GetById(id));

            return View(demandTypeViewModel);
        }

        [Secure("EditPhysicalDemands")]
        [HttpPost, Route("Demand/SaveDemandType/{employerId?}", Name = "SaveDemandType")]
        [ValidateAntiForgeryToken]
        public ActionResult SavePhysicalDemandType(string employerId, DemandTypeViewModel demandTypeViewModel)
        {
            if (!ModelState.IsValid)
                return PartialView("EditDemandType", demandTypeViewModel);

            //validate whether same code exists for a demand type or not. Validate against a customer.
            if (string.IsNullOrEmpty(demandTypeViewModel.Id))
            {
                var dtExists = DemandType.GetByCode(demandTypeViewModel.Code, customerId: CurrentCustomer.Id, includeSuppressed: true);
                if(dtExists != null)
                {
                    ModelState.AddModelError(string.Empty, "Duplicate code exists!");
                    return PartialView("EditDemandType", demandTypeViewModel);
                }
            }

            var demandType = string.IsNullOrEmpty(demandTypeViewModel.Id)
                ? new DemandType()
                : DemandType.GetById(demandTypeViewModel.Id);

            demandType = demandTypeViewModel.ApplyToDataModel(demandType);
            using (var demandService = new DemandService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                demandService.SaveDemandType(demandType);
            }

            return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewDemandTypes"));
        }

        [Secure("EditPhysicalDemands")]
        [HttpDelete, Route("Demand/DeleteDemandType/{employerId?}", Name = "DeleteDemandType")]
        public ActionResult DeleteDemandType(string employerId, string id)
        {
            using (var demandService = new DemandService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                var type = DemandType.GetById(id);
                demandService.DeleteDemandType(type);
            }

            return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewDemandTypes"));
        }

        #endregion

        #region Applied Demand methods

        [Secure("AddWorkRestrictionDetail", "EditWorkRestrictionDetail", "DeleteWorkRestrictionDetail")]
        [HttpGet, Route("Demand/{caseId}/WorkRestrictionEntry", Name = "WorkRestrictionEntry")]
        public ActionResult WorkRestrictionEntry(string caseId, string todoItemId)
        {
            var theCase = Case.GetById(caseId);
            if (theCase == null)
                throw new AbsenceSoftException("Unable to find the specified case to apply the work restriction to.");

            var viewModel = new WorkRestrictionEntryViewModel
            {
                CaseId = caseId,
                ToDoItemId = todoItemId,
                WorkRestrictions = new List<WorkRestrictionViewModel>()
            };

            using (var demandService = new DemandService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                var demands = demandService.GetDemands();
                var demandTypes = demandService.GetDemandTypes();
                var restrictions = demandService.GetJobRestrictionsForEmployee(theCase.Employee.Id, theCase.Id);
                foreach (var demand in demands)
                {
                    var matchingRestrictions = restrictions.Where(r => r.Restriction.DemandId == demand.Id).ToList();
                    var newestRestriction = matchingRestrictions.OrderByDescending(r => r.CreatedDate).FirstOrDefault();
                    var workRestriction = new WorkRestrictionViewModel(newestRestriction, demand, demandTypes)
                    {
                        IsWorkRestrictionEntry = true,
                        DuplicateRestrictions = newestRestriction == null
                            ? new List<WorkRestrictionViewModel>()
                            : matchingRestrictions.Where(r => r.Id != newestRestriction.Id)
                                .Select(r => new WorkRestrictionViewModel(r, demand, demandTypes))
                                .ToList()
                    };

                    if (workRestriction.StartDate == null)
                        workRestriction.StartDate = DateTime.UtcNow.ToMidnight();
                    if (string.IsNullOrEmpty(workRestriction.EmployeeId))
                        workRestriction.EmployeeId = theCase.Employee.Id;
                    if (string.IsNullOrEmpty(workRestriction.CaseId))
                        workRestriction.CaseId = theCase.Id;


                    viewModel.WorkRestrictions.Add(workRestriction);
                }
            }

            if (ControllerContext.IsChildAction)
                return PartialView(viewModel);

            return View(viewModel);
        }

        [NonAction]
        public ActionResult WorkRestrictionEntryForm(WorkRestrictionEntryViewModel viewModel)
        {
            return View("WorkRestrictionEntryForm", viewModel);
        }

        [Secure("AddWorkRestrictionDetail", "EditWorkRestrictionDetail", "DeleteWorkRestrictionDetail")]
        [HttpPost, Route("Demand/{caseId}/WorkRestrictionEntry", Name = "WorkRestrictionEntrySave")]
        [ValidateAntiForgeryToken]
        public ActionResult WorkRestrictionEntrySave(WorkRestrictionEntryViewModel viewModel)
        {
            Case theCase = Case.GetById(viewModel.CaseId);
            if (theCase == null)
                throw new AbsenceSoftException("Unable to find the specified case to apply the work restriction to.");

            using (var demandService = new DemandService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                if (!ModelState.IsValid)
                {
                    List<ModelState> list = ModelState.Values.Where(state => state.Errors.Count > 0).ToList();
                    List<DemandType> demandTypes = demandService.GetDemandTypes();
                    foreach (var restriction in viewModel.WorkRestrictions)
                    {
                        restriction.BuildValuesModel(null, demandTypes);
                    }
                    return PartialView("WorkRestrictionEntryForm", viewModel);
                }

                List<WorkRestrictionViewModel> restrictionsWithValues = viewModel.WorkRestrictions.Where(wr => wr.HasValues).ToList();
                foreach (var restriction in restrictionsWithValues)
                {
                    EmployeeRestriction savedRestriction = EmployeeRestriction.GetById(restriction.Id);
                    savedRestriction = restriction.ApplyToDataModel(savedRestriction, true);
                    demandService.SaveJobRestriction(savedRestriction);
                }
            }

            if (!string.IsNullOrWhiteSpace(viewModel.ToDoItemId))
            {
                ToDoItem wfi = ToDoItem.GetById(viewModel.ToDoItemId);
                if (wfi != null)
                    new ToDoService().Using(s => s.CompleteToDo(wfi, CurrentUser, true));
            }
            
            return RedirectToRoute("ViewCase", new { caseId = viewModel.CaseId });
        }

        [Secure("AddWorkRestrictionDetail", "EditWorkRestrictionDetail")]
        [HttpGet, Route("Demand/{caseId}/WorkRestriction/{workRestrictionId?}", Name = "WorkRestriction")]
        public ActionResult WorkRestriction(string caseId, string workRestrictionId)
        {
            WorkRestrictionViewModel viewModel = null;
            Case theCase = Case.GetById(caseId);
            if (theCase == null)
                throw new AbsenceSoftException("Unable to find the specified case to apply the work restriction to.");

            if (workRestrictionId == null)
            {
                viewModel = new WorkRestrictionViewModel(theCase.Employee.Id);
            }
            else
            {
                var workRestriction = EmployeeRestriction.GetById(workRestrictionId);
                viewModel = new WorkRestrictionViewModel(workRestriction);
            }

            return PartialView(viewModel);
        }

        [Secure]
        [HttpGet, Route("Demand/{demandId}/Values", Name = "DemandValues")]
        public ActionResult AppliedDemandValues(string demandId)
        {
            using (var demandService = new DemandService(CurrentUser))
            {
                WorkRestrictionViewModel viewModel = new WorkRestrictionViewModel()
                {
                    DemandId = demandId
                };
                viewModel.BuildValuesModel();
                return PartialView(viewModel);

            }
        }

        [HttpGet]
        public ActionResult GetDemands(string searchTerm, int pageSize, int pageNum)
        {    
            var demands = Demand.AsQueryable()
                .Where(d => d.CustomerId == (CurrentCustomer ?? new Customer()).Id
                    && d.Name.ToLower().Contains(searchTerm.ToLower()))
                .OrderBy(d => d.Order)
                .ToList();

            var demandsDropDown = demands.Select(
                e => new
                {
                    id = e.Id,
                    text = e.Name,
                }).ToList();

            return new JsonResult
            {
                Data = new
                {
                    Total = demandsDropDown.Count,
                    Results =  demandsDropDown
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Adds or updates the work restriction on a specific case
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [Secure("AddWorkRestrictionDetail", "EditWorkRestrictionDetail")]
        [HttpPost, Route("Demand/{caseId}/SaveWorkRestriction", Name = "SaveWorkRestriction")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveWorkRestriction(string caseId, WorkRestrictionViewModel viewModel)
        {
            Case theCase = Case.GetById(caseId);
            if (theCase == null)
                throw new AbsenceSoftException("Unable to find the specified case to apply the work restriction to.");

            //Following validation is relaxed to fix AT-2872
            //if (ModelState.IsValid)
            //{
            //    using (var demandService = new DemandService(CurrentUser))
            //    {
            //        List<EmployeeRestriction> restrictions = demandService.GetJobRestrictionsForEmployee(theCase.Employee.Id, theCase.Id);
            //        DateRange dr = new DateRange(viewModel.StartDate.Value, viewModel.EndDate);
            //        if (restrictions.Any(r => r.Id != viewModel.Id && r.Restriction.DemandId == viewModel.DemandId && r.Restriction.Dates.DateRangesOverLap(dr)))
            //        {
            //            ModelState.AddModelError("DemandId", "Duplicate work restriction exists in the same or overlapped date range.");
            //        }
            //    }
            //}

            if (ModelState.IsValid)
            {
                var misingDemandOptionValue = false;

                foreach (var item in viewModel.Values)
                {
                    switch(item.Type)
                    {
                        case DemandValueType.Text:
                            if (string.IsNullOrWhiteSpace(item.Text))
                                misingDemandOptionValue = true;
                            break;
                        case DemandValueType.Boolean:
                            if (!item.Applicable.HasValue)
                                misingDemandOptionValue = true;
                            break;
                        case DemandValueType.Value:
                            if (!item.DemandItemId.HasValue)
                                misingDemandOptionValue = true;
                            break;

                    }

                    if (misingDemandOptionValue)
                        break;
                }

                if (misingDemandOptionValue)
                {
                    ModelState.AddModelError("DemandItemIds", "Must fill the value(s) for the demand item(s).");          
                }
            }

            if (!ModelState.IsValid)
            {
                viewModel.BuildValuesModel();
                return PartialView("WorkRestriction", viewModel);
            }

            using (var demandService = new DemandService(CurrentUser))
            {
                EmployeeRestriction workRestriction = EmployeeRestriction.GetById(viewModel.Id);
                demandService.SaveJobRestriction(viewModel.ApplyToDataModel(workRestriction));
            }
            WorkRelatedViewModel model = new WorkRelatedViewModel(theCase);
            if (theCase.CaseType.Contains(CaseType.Reduced.ToString()) || theCase.CaseType.Contains(CaseType.Intermittent.ToString()))
            {
                using (var caseService = new CaseService(CurrentCustomer, CurrentEmployer, CurrentUser))
                {
                    model.CalculatedDaysOnJobTransferOrRestriction = caseService.CalculateDaysOnJobTransferOrRestriction(theCase);
                    theCase.WorkRelated = model.ApplyModel(theCase);
                    if (theCase.WorkRelated != null)
                    {
                        theCase.WorkRelated.Provider.Save();
                        new CaseService().Using(s => s.UpdateCase(theCase));
                    }
                }
            }
            return RedirectViaJavaScript(Url.RouteUrl("ViewCase", new { caseId = caseId }));
        }

        [Secure("DeleteWorkRestrictionDetail")]
        [HttpDelete, Route("Demand/{caseId}/DeleteWorkRestriction/{workRestrictionId}", Name = "DeleteWorkRestriction")]
        public ActionResult DeleteWorkRestriction()
        {
            try
            {
                string workRestrictionId = RouteData.Values["workRestrictionId"] as string;
                string caseId = RouteData.Values["caseId"] as string;
                Case theCase = Case.GetById(caseId);
                if (theCase == null)
                    throw new AbsenceSoftException("Unable to find the specified case to delete the work restriction from");

                using (var demandService = new DemandService(CurrentUser))
                {
                    demandService.DeleteJobRestriction(workRestrictionId);
                }

                return Json(new { Success = true });
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error Deleting Work Restriction");
            }

        }


        #endregion


    }
}