﻿using AbsenceSoft.Models.BulkDownload;
using System.Web.Mvc;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Logic.Processing.EL;
using AbsenceSoft.Data.Processing;
using AbsenceSoft.Data.Enums;
using System.Collections.Generic;
using System.Linq;
using AbsenceSoft;
using AbsenceSoft.Web.Filters;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Models.Employees;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;

namespace AbsenceSoft.Controllers
{
    [Secure(Feature.CaseBulkDownload)]
    public class BulkDownloadController : BaseController
    {
        [Secure("CaseBulkDownload")]
        [Title("Bulk Download")]
        [HttpGet, Route("BulkDownload/{caseId}", Name ="CaseBulkDownload")]
        public ActionResult ViewBulkDownloads(string caseId)
        {
            using (var caseExportService = new CaseExportService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                CaseBulkDownloadViewModel caseDownloads = new CaseBulkDownloadViewModel()
                {
                    CaseId = caseId,
                    BulkDownloads = caseExportService.GetCaseBulkDownloads(caseId)
                        .Select(cb => new BulkDownloadViewModel(cb)).ToList()
                };

                return View(caseDownloads);
            }
        }

        [Secure("CaseBulkDownload")]
        [Title("Bulk Download")]
        [HttpGet, Route("BulkDownloadForEmployee/{employeeId}")]
        public ActionResult BulkDownloadsForEmployee(string employeeId)
        {
            using (var employeeService = new EmployeeService())
            {
                var employee = employeeService.GetEmployee(employeeId);
                return View(new EmployeeViewModel(employee));
            }
        }

        [Secure("CaseBulkDownload")]
        [HttpPost, Route("BulkDownload/StatusRequest")]
        [ValidateApiAntiForgeryToken]
        public JsonResult GetCaseBulkDownloadStatus(CaseBulkDownloadViewModel downloads)
        {
            using (var caseExportService = new CaseExportService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                return Json(caseExportService.GetCaseBulkDownloadsByIds(downloads.CaseId, downloads.BulkDownloads.Select(c => c.Id)));
            }
        }

        [Secure("CaseBulkDownload")]
        [HttpGet, Route("BulkDownload/{employeeId}/StatusRequest")]
        [ValidateApiAntiForgeryToken]
        public JsonResult GetCaseBulkDownloadStatus(string employeeId)
        {
            using (var caseExportService = new CaseExportService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                return Json(caseExportService.GetCaseBulkDownloadsForEmployee(employeeId),JsonRequestBehavior.AllowGet);
            }
        }

        [Secure("CaseBulkDownload")]
        [HttpPost, Route("BulkDownload/CaseBulkDownload")]
        [ValidateApiAntiForgeryToken]
        public JsonResult CaseBulkDownload(BulkDownloadViewModel model)
        {
            using (var caseExportService = new CaseExportService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                var download = new BulkDownloadViewModel(caseExportService.CreateCaseExportRecord(model.CaseId, model.DownloadType));
                return Json(download);
            }
        }

        [Secure("CaseBulkDownload")]
        [HttpPost, Route("BulkDownload/BulkDownloadCases")]
        [ValidateApiAntiForgeryToken]
        public JsonResult BulkDownloadCases(BulkDownloadViewModel model)
        {
            using (var caseExportService = new CaseExportService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                var download = new BulkDownloadViewModel(caseExportService.CreateCaseExportRecordByEmployeeId(model.EmployeeId, model.CaseIds, model.DownloadType));
                return Json(download);
            }
        }

        [Secure("CaseBulkDownload")]
        [HttpGet, Route("BulkDownload/HasFeatureAndPermissions")]
        [ValidateApiAntiForgeryToken]
        public JsonResult HasFeatureAndPermissionsEnabled()
        {
            using (var adminService = new AdministrationService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                var hasPermission = adminService.CustomerHasFeature(Feature.CaseBulkDownload) && adminService.UserHasPermission(Permission.CaseBulkDownload);
                return Json( new { HasPermission = hasPermission }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}