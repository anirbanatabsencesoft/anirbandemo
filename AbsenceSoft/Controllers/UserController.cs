﻿using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Models.Users;
using AbsenceSoft.Web.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Models.Signup;
using AbsenceSoft.Filters;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    [Secure]
    public class UserController : BaseController
    {
        [Secure("EditUser")]
        [Title("View Users")]
        [HttpGet, Route("User/List", Name ="ViewUsers")]
        public ActionResult ViewUsers()
        {
            using (var authService = new AuthenticationService())
            {
                UserListViewModel userList = new UserListViewModel()
                {
                    Roles = authService.GetRoles(CurrentCustomer.Id)
                };
                
                return View(userList);
            }


        }

        [Secure("EditUser")]
        [HttpPost, Route("User/List", Name ="ListUsers")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListUsers(ListCriteria criteria)
        {
            using (var adminService = new AdministrationService(CurrentUser))
            {
                ListResults users = adminService.UsersList(criteria);
                return PartialView(users);
            }
        }

        [Secure("DisableUser"), HttpPost]
        [Route("User/{userId}/DisableUser", Name = "DisableUser")]
        [ValidateApiAntiForgeryToken]
        public JsonResult DisableUser(string userId)
        {
            try
            {
                using (var authenticationService = new AuthenticationService())
                using (var administrationService = new AdministrationService())
                {
                    var user = authenticationService.GetUserById(userId);
                    if (user == null)
                        return JsonError(null, "Unable to find the user");

                    if (user != null)
                        administrationService.DisableUser(user);

                    return Json(new { Success = true });
                }
                
            }
            catch (Exception ex)
            {
                return JsonError(ex, ex.Message);
            }
        }


        [Secure("ManageUserAccount"), HttpPost]
        [Route("User/{userId}/UnLockUser", Name = "UnLockUser")]
        [ValidateApiAntiForgeryToken]
        public JsonResult UnLockUser(string userId)
        {
            try
            {                
                using (var administrationService = new AdministrationService())
                {
                    administrationService.UnLockUser(userId);
                    return Json(new { Success = true });
                }

            }
            catch (Exception ex)
            {
                return JsonError(ex, ex.Message);
            }
        }

        [Secure("ManageUserAccount"), HttpPost]
        [Route("User/{userId}/ResetUserPassword", Name = "ManageUserAccount")]
        [ValidateApiAntiForgeryToken]
        public JsonResult ResetUserPassword(string userId)
        {
            try
            {                
                using (var authenticationService = new AuthenticationService())
                {
                    var user = authenticationService.GetUserById(userId);
                    if (user == null)
                    {
                        return JsonError(null, "Unable to find the user");
                    }
                    string resetLink = authenticationService.GetPasswordResetLink(user);
                    return Json(new { Success = true, link = resetLink });
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, ex.Message);
            }
        }


        [Secure("EnableUser"), HttpPost]
        [Route("User/EnableUser", Name = "EnableUser")]
        [ValidateApiAntiForgeryToken]
        public ActionResult EnableUser(CreateUserModel model)
        {
            try
            {
                using (var administrationService = new AdministrationService())
                {
                    administrationService.EnableUser(model.Id, model.EndDate);

                    return Json(new { Success = true });
                }

            }
            catch (Exception ex)
            {
                return JsonError(ex, ex.Message);
            }
        }
    }
}