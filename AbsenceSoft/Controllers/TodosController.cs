﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Logic.Tasks;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using AbsenceSoft.Models;
using AbsenceSoft.Models.Administration;
using AbsenceSoft.Models.Demands;
using AbsenceSoft.Models.Todos;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace AbsenceSoft.Controllers
{
    [Secure]
    public class TodosController : BaseController
    {
        private ToDoService _tds { get; set; }
        private ToDoService ToDo
        {
            get
            {
                if (_tds == null)
                {
                    _tds = new ToDoService();
                }

                return _tds;
            }
        }

        private CaseService _cs { get; set; }
        private CaseService Case
        {
            get
            {
                if (_cs == null)
                {
                    _cs = new CaseService();
                }

                return _cs;
            }
        }


        [Title("To-Do's")]
        [Route("Todos", Name = "Todos")]
        [Secure("CompleteToDo", "ViewTodo")]
        public ViewResult Index()
        {
            var todoItemStatuses = (from ToDoItemStatus ct in Enum.GetValues(typeof(ToDoItemStatus))
                                    select new { Id = (int)ct, Text = ct.ToString() }).ToList();
            todoItemStatuses.Insert(0, new { Id = 0, Text = "Filter" });
            ViewBag.WorkflowItemStatuses = new SelectList(todoItemStatuses, "Id", "Text");
            return View();
        }

        //Filter table
        [HttpPost]//, Ajax]
        [Route("Todos/List", Name = "WorkflowList")]
        [Secure("CompleteToDo", "ViewTodo")]
        [ValidateApiAntiForgeryToken]
        public JsonResult ToDosList(ListCriteria criteria) //string employeeFilter, string textFilter, string dueFilter, string statusFilter, string assignedToNameFilter, string sortBy, int sortDirection = 1, bool doPaging = false, int pageNumber = 1, int pageSize = 10)
        {
            try
            {
                using (var svc = new ToDoService())
                {
                    return Json(svc.ToDoItemList(CurrentUser, criteria));
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error getting case list");
            }
        }

        [HttpGet]
        [Route("Todos/GetNextToDo", Name = "WorkflowGetNextToDo")]
        [Secure("CompleteToDo")]
        public JsonResult GetNextToDo(string caseId)
        {
            try
            {
                var criteria = new ListCriteria
                {
                    PageNumber = 1,
                    PageSize = 1,
                    SortBy = "DueDate",
                    SortDirection = SortDirection.Ascending
                };
                criteria.Set("CaseId", caseId);

                //filter where status <> completed or cancelled
                criteria.Set("NextToDoFilter", "NextToDoFilter");
                using (var svc = new ToDoService())
                {
                    var data = svc.ToDoItemList(CurrentUser, criteria);
                    if (data.Results.Any())
                    {
                        return Json(data.Results.First(), JsonRequestBehavior.AllowGet);
                    }
                    return Json("", JsonRequestBehavior.AllowGet);

                }

            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }


        }

        [Title("View To-Do")]
        [Route("Todos/{todoItemId}/View", Name = "ViewWorkflow")]
        [Secure("CompleteToDo", "ViewTodo")]
        public ViewResult ToDoView()
        {
            return View();
        }

        [Title("Create To-Do")]
        [Route("Cases/{caseId}/Todos/Create", Name = "CreateCaseWorkflow")]
        [Secure("CreateToDo")]
        public ViewResult Create()
        {
            return View();
        }

        [Route("Todos/{todoItemId}/Paperwork/Review", Name = "PaperworkReviewView")]
        [Secure("CompleteToDo")]
        public ViewResult PaperworkReviewView(string toDoItemId)
        {
            var todoItem = ToDoItem.GetById(toDoItemId);
            var model = new PaperWorkTodoReviewModel(todoItem);

            // Get case
            var theCase = AbsenceSoft.Data.Cases.Case.GetById(todoItem.CaseId);
            if (theCase != null)
            {
                model.CaseStartDate = theCase.StartDate;
                model.CaseEndDate = theCase.EndDate;
            }

            // load paperwork fields
            string commId = todoItem.Metadata.GetRawValue<string>("CommunicationId");
            var pId = todoItem.Metadata.GetRawValue<Guid?>("PaperworkId");
            Communication comm = Communication.GetById(commId);

            using (AuthenticationService svc = new AuthenticationService(CurrentUser))
            {
                var usersList = svc.GetUsersList(CurrentUser.Customer.Id, null);
                List<UsersListVM> lstUsers = new List<UsersListVM>();

                usersList.ForEach(x => lstUsers.Add(new UsersListVM()
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email,
                    IsLoggedInUser = x.Id == CurrentUser.Id ? true : false,
                    DisplayName = x.DisplayName,
                    Roles = x.Roles,
                    RolesName = Role.Query.Find(Role.Query.In(m => m.Id, x.Roles)).Select(m => m.Name).ToList()
                })
                );
                model.UserList = lstUsers;
                model.ReAssign = new ReassignTodosModel
                {
                    Todos = new List<ReassignTodo>()
                };
                ReassignTodo reassigntodo = new ReassignTodo
                {
                    TodoId = toDoItemId
                };
                model.ReAssign.Todos.Add(reassigntodo);
            }

            if (comm != null && pId.HasValue)
            {
                CommunicationPaperwork pw = comm.Paperwork.FirstOrDefault(p => p.Id == pId);
                if (pw != null && pw.Fields != null && pw.Fields.Count() > 0)
                {
                    foreach (var currentField in pw.Fields)
                    {
                        List<WorkRestrictionViewModel> workRestrictionViewModels = null;
                        if (currentField.Type == PaperworkFieldType.WorkRestrictions)
                        {
                            if (theCase == null)
                            {
                                throw new AbsenceSoftException("Unable to find the specified case to apply the work restriction to.");
                            }

                            workRestrictionViewModels = new List<WorkRestrictionViewModel>();
                            using (var demandService = new DemandService(CurrentCustomer, CurrentEmployer, CurrentUser))
                            {
                                List<Demand> demands = demandService.GetDemands();
                                List<DemandType> demandTypes = demandService.GetDemandTypes();
                                List<EmployeeRestriction> restrictions = demandService.GetJobRestrictionsForEmployee(theCase.Employee.Id, theCase.Id);
                                foreach (var demand in demands)
                                {
                                    EmployeeRestriction matchingRestriction = restrictions.FirstOrDefault(r => r.Restriction.DemandId == demand.Id);
                                    WorkRestrictionViewModel workRestriction = new WorkRestrictionViewModel(matchingRestriction, demand, demandTypes);
                                    if (workRestriction.StartDate == null)
                                    {
                                        workRestriction.StartDate = DateTime.UtcNow.ToMidnight();
                                    }

                                    if (string.IsNullOrEmpty(workRestriction.EmployeeId))
                                    {
                                        workRestriction.EmployeeId = theCase.Employee.Id;
                                    }

                                    if (string.IsNullOrEmpty(workRestriction.CaseId))
                                    {
                                        workRestriction.CaseId = theCase.Id;
                                    }

                                    workRestrictionViewModels.Add(workRestriction);
                                }
                            }
                        }

                        model.Fields.Add(new PaperworkFieldModel()
                        {
                            Field = currentField,
                            WorkRestrictions = workRestrictionViewModels
                        });


                    }
                }
            }

            // get cert 
            if (theCase != null
                && theCase.Certifications != null
                && theCase.Certifications.Any()
                && theCase.Segments.Any(s => s.Type == CaseType.Intermittent))
            {

                Certification lastCertificate = theCase.Certifications.OrderByDescending(c => c.EndDate).OrderByDescending(c => c.StartDate).OrderByDescending(c => c.CertificationCreateDate).FirstOrDefault();

                if (lastCertificate != null)
                {
                    if (lastCertificate.IsCertificationIncomplete)
                    {

                        string certNote;
                        certNote = CaseNote.AsQueryable().ToList(n => n.CertId == lastCertificate.Id).FirstOrDefault()?.Notes;


                        if (lastCertificate.StartDate != default(DateTime))
                        {
                            model.CertificationDetail.StartDate = lastCertificate.StartDate;
                        }
                        if (lastCertificate.EndDate != default(DateTime))
                        {
                            model.CertificationDetail.EndDate = lastCertificate.EndDate;
                        }
                        if (lastCertificate.Duration != 0)
                        {
                            model.CertificationDetail.Duration = lastCertificate.Duration;
                        }
                        if (lastCertificate.Frequency != 0)
                        {
                            model.CertificationDetail.Frequency = lastCertificate.Frequency;
                        }
                        model.CertificationDetail.DurationType = lastCertificate.DurationType;
                        model.CertificationDetail.FrequencyType = lastCertificate.FrequencyType;
                        if (lastCertificate.Occurances != 0)
                        {
                            model.CertificationDetail.Occurances = lastCertificate.Occurances;
                        }
                        model.CertificationDetail.Notes = certNote;
                        model.CertificationDetail.Id = lastCertificate.Id;
                    }
                    else
                    {
                        model.HasCertifications = "true";
                    }
                }
            }

            return View("Paperwork/Review/Index", model);
        }

        [HttpPost]
        [Route("Todos/{todoItemId}/Paperwork/Review", Name = "PaperworkReviewSave")]
        [Secure("CompleteToDo")]
        [ValidateAntiForgeryToken]
        public ActionResult PaperworkReviewSave(string todoItemId, PaperWorkTodoReviewModel model, bool? checkToValidate)
        {

            var todoItem = ToDoItem.GetById(todoItemId);
            if (todoItem == null)
            {
                throw new AbsenceSoftException("invalid todo id");
            }

            var theCase = AbsenceSoft.Data.Cases.Case.GetById(todoItem.CaseId);
            // TODO: fix server side validation (complex due to optional form sections, complex child objects, dynamic lists, etc)
            ViewBag.DateRangeError = "";
            if (model.CertificationDetail != null && (model.CertificationDetail.StartDate != DateTime.MinValue && model.CertificationDetail.EndDate != DateTime.MinValue) && (model.CertificationDetail.StartDate < theCase.StartDate || model.CertificationDetail.EndDate > theCase.EndDate))
            {
                ViewBag.DateRangeError = "Error: Cert date range must fall within the date range of the case";
                return View("Paperwork/Review/Index", model);
            }

            // save cert 
            if (model.CaseType == "Intermittent"
                && model.CertificationDetail != null)
            {
                if (theCase == null)
                {
                    throw new AbsenceSoftException("Unable to find the specified case to apply the certification detials to.");
                }

                if ((theCase.Certifications == null
                    || !theCase.Certifications.Any())
                    && (model.CertificationDetail.Frequency == null
                    || model.CertificationDetail.Occurances == null))
                {
                    if (!(checkToValidate ?? false))
                    {
                        throw new AbsenceSoftException("Certification must be provided on first Intermittent paperwork approval, if a certification has not been provided in advance.");
                    }
                }
            }

            // save paperwork fields
            if (model.Fields != null && model.Fields.Count() > 0)
            {
                var commId = todoItem.Metadata.GetRawValue<string>("CommunicationId");
                var pId = todoItem.Metadata.GetRawValue<Guid?>("PaperworkId");
                var comm = Communication.GetById(commId);
                if (comm != null && pId.HasValue)
                {
                    var pw = comm.Paperwork.FirstOrDefault(p => p.Id == pId);
                    if (pw != null)
                    {
                        if (theCase == null)
                        {
                            throw new AbsenceSoftException("Unable to find the specified case to apply the work restriction to.");
                        }

                        using (var demandService = new DemandService(CurrentUser))
                        {
                            foreach (var currentField in model.Fields)
                            {
                                if (currentField.WorkRestrictions == null)
                                {
                                    continue;
                                }

                                var restrictionsWithValues = currentField.WorkRestrictions.Where(wr => wr.HasValues).ToList();
                                foreach (var restriction in restrictionsWithValues)
                                {
                                    var savedRestriction = EmployeeRestriction.GetById(restriction.Id);
                                    savedRestriction = restriction.ApplyToDataModel(savedRestriction, true);
                                    demandService.SaveJobRestriction(savedRestriction);
                                }
                            }
                        }

                        pw.Fields = model.Fields.Select(f => f.Field).ToList();
                        comm.Save();
                    }
                }
            }

            // save cert 
            if (model.CaseType == "Intermittent"
                && model.CertificationDetail != null
                && ((checkToValidate ?? false) || (model.CertificationDetail.Frequency != null
                && model.CertificationDetail.Occurances != null)))
            {
                if (theCase == null)
                {
                    throw new AbsenceSoftException("Unable to find the specified case to apply the certification detials to.");
                }

                using (var caseService = new CaseService())
                {

                    bool isNewCertification = true;
                    var certification = new Certification
                    {
                        Occurances = model.CertificationDetail.Occurances ?? 0,
                        Frequency = model.CertificationDetail.Frequency ?? 0,
                        FrequencyType = model.CertificationDetail.FrequencyType ?? Unit.Days,
                        FrequencyUnitType = model.CertificationDetail.FrequencyUnitType ?? DayUnitType.Workday,
                        Duration = model.CertificationDetail.Duration ?? 0d,
                        DurationType = model.CertificationDetail.DurationType ?? Unit.Days,
                        IsCertificationIncomplete = (checkToValidate ?? false),
                        IntermittentType = model.CertificationDetail.IntermittentType
                    };

                    if (model.CertificationDetail.Id != null)
                    {
                        certification.Id = (Guid)model.CertificationDetail.Id;
                        isNewCertification = false;
                    }
                    DateTime minDate = DateTime.MinValue;

                    if (model.CertificationDetail.StartDate != null && model.CertificationDetail.StartDate != minDate)
                    {
                        certification.StartDate = ((DateTime)model.CertificationDetail.StartDate).ToMidnight();
                    }
                    if (model.CertificationDetail.EndDate != null && model.CertificationDetail.EndDate != minDate)
                    {
                        certification.EndDate = ((DateTime)model.CertificationDetail.EndDate).ToMidnight();
                    }


                    theCase = caseService.CreateOrModifyCertification(theCase, certification, isNewCertification);

                    if (!string.IsNullOrWhiteSpace(model.CertificationDetail.Notes))
                    {
                        var noteQuery =
                            from n in CaseNote.AsQueryable()
                            where Query.EQ("CertId", new BsonString(certification.Id.ToString())).Inject()
                            select n;

                        var note = noteQuery.FirstOrDefault()
                            ?? new CaseNote
                            {
                                Category = NoteCategoryEnum.Certification,
                                CreatedBy = CurrentUser,
                                CustomerId = theCase.CustomerId,
                                EmployerId = theCase.EmployerId,
                                Public = false,
                                Case = theCase
                            };

                        note.ModifiedBy = CurrentUser;
                        note.Notes = model.CertificationDetail.Notes;
                        note.CertId = certification.Id;
                        note.Metadata.SetRawValue("CertId", certification.Id.ToString());
                        note.Save();
                    }
                }
            }

            // save todo
            switch (model.ActionType)
            {
                case "complete": todoItem.WfOnToDoItemCompleted(CurrentUser, PaperworkReviewActivity.ApprovedOutcomeValue, true); break;
                case "incomplete": todoItem.WfOnToDoItemCompleted(CurrentUser, PaperworkReviewActivity.IncompleteOutcomeValue, true, (checkToValidate ?? false)); break;
                case "denied": todoItem.WfOnToDoItemCompleted(CurrentUser, PaperworkReviewActivity.DeniedOutcomeValue, true); break;
                default: throw new AbsenceSoftException("unknown action type");
            }

            return RedirectToAction("ViewCase", "Cases", new { caseId = todoItem.CaseId });
        }

        [Route("Todos/{todoItemId}/WorkRestrictions/Entry", Name = "WorkRestrictionsEntry")]
        [Secure("CompleteToDo")]
        public ViewResult WorkRestrictionsEntry(string todoItemId)
        {
            var todoItem = ToDoItem.GetById(todoItemId);
            var model = new EnterWorkRestrictionsViewModel(todoItem);
            return View("WorkRestrictions/Entry/Index", model);
        }

        [Route("Todos/{toDoItemId}/ChangeDueDate", Name = "ChangeDueDate")]
        [Secure("CompleteToDo")]
        public JsonResult ChangeDueDate(string toDoItemId, DateTime dueDate, string reason)
        {
            try
            {
                var item = ToDo.ChangeDueDate(toDoItemId, dueDate, reason);
                return Json(new { Success = true });
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error changing due date");
            }
        }

        [HttpPost]
        [Route("Todos/ChangeStatus", Name = "ChangeToDoStatus")]
        [Secure("CancelToDo")]
        [ValidateApiAntiForgeryToken]
        public JsonResult ChangeStatus(BaseTodoModel todoItem)
        {
            try
            {
                var item = ToDo.ChangeStatus(todoItem.Id, (ToDoItemStatus)todoItem.Status, todoItem.CancelReason);
                return Json(new { Success = true });
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error changing to-do status");
            }
        }

        [HttpPost]
        [Route("Todos/{toDoItemId}/ReOpen", Name = "ReOpenToDo")]
        [Secure("CompleteToDo")]
        [ValidateApiAntiForgeryToken]
        public JsonResult ReOpen(string toDoItemId)
        {
            try
            {
                var item = ToDo.ReOpen(toDoItemId);
                return Json(new { Success = true });
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error reopening to-do");
            }
        }


        [HttpGet, Route("Todos/{toDoItemId}/PaperworkDue", Name = "PaperworkDueTodo")]
        [Secure("CompleteToDo")]
        public ActionResult PaperworkDue(string toDoItemId)
        {
            return Json(new PaperworkDueModel(ToDoItem.GetById(toDoItemId)));
        }

        [HttpPost, Route("Todos/{toDoItemId}/PaperworkDue", Name = "PaperworkDueTodoComplete")]
        [Secure("CompleteToDo")]
        [ValidateApiAntiForgeryToken]
        public ActionResult PaperworkDueComplete(PaperworkDueModel model)
        {
            // load the todo item
            ToDoItem wfi = ToDoItem.GetById(model.Id);
            if (wfi == null)
            {
                return Json(false);
            }

            wfi.Metadata.SetRawValue("NewDueDate", model.NewDueDate);
            wfi.Metadata.SetRawValue("ExtendGracePeriod", model.ExtendGracePeriod);
            wfi.Metadata.SetRawValue("PaperworkReceived", model.PaperworkReceived);
            wfi.Metadata.SetRawValue("CloseCase", model.CloseCase);
            wfi.Metadata.SetRawValue("ReturnAttachmentId", model.ReturnAttachmentId);
            wfi.WorkflowInstance ?.Metadata.SetRawValue("CloseCase", model.CloseCase);
            if (model.ExtendGracePeriod)
            {
                wfi.WfOnToDoItemExtended(model.NewDueDate, model.DueDate, CurrentUser,
                model.PaperworkReceived ? PaperworkDueActivity.PaperworkReceivedOutcomeValue : PaperworkDueActivity.PaperworkNotReceivedOutcomeValue,
                true);
            }
            else
            {
                wfi.WfOnToDoItemCompleted(CurrentUser,
                    model.PaperworkReceived ? PaperworkDueActivity.PaperworkReceivedOutcomeValue : PaperworkDueActivity.PaperworkNotReceivedOutcomeValue,
                    true);
            }

            return Json(model);
        }

        [HttpGet, Route("Todos/{toDoItemId}/PaperworkReview", Name = "PaperworkReviewTodo")]
        [Secure("CompleteToDo")]
        public ActionResult PaperworkReview(string toDoItemId)
        {
            return Json(new PaperWorkReviewModel(ToDoItem.GetById(toDoItemId)));
        }

        [HttpPost, Route("Todos/{toDoItemId}/PaperworkReview", Name = "PaperworkReviewTodoComplete")]
        [Secure("CompleteToDo")]
        [ValidateApiAntiForgeryToken]
        public ActionResult PaperworkReviewComplete(PaperWorkReviewModel model)
        {
            try
            {
                ToDoItem wfi = ToDoItem.GetById(model.Id);
                if (wfi == null)
                {
                    // some sort of error?
                    return Json(model);
                }

                string outcome = null;
                if (model.PaperworkApproved)
                {
                    outcome = PaperworkReviewActivity.ApprovedOutcomeValue;
                }

                if (model.PaperworkDenied)
                {
                    outcome = PaperworkReviewActivity.DeniedOutcomeValue;
                }

                if (model.PaperworkIncomplete)
                {
                    outcome = PaperworkReviewActivity.IncompleteOutcomeValue;
                }

                if (outcome != null)
                {
                    wfi.WfOnToDoItemCompleted(CurrentUser, outcome, true);
                }

                return Json(model);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error completing paperwork review");
            }
        }

        [HttpGet, Route("Todos/{toDoItemId}/ReturnToWork", Name = "ReturnToWorkTodo")]
        [Secure("CompleteToDo")]
        public ActionResult ReturnToWork(string toDoItemId)
        {
            return Json(new ReturnToWorkModel(ToDoItem.GetById(toDoItemId)));
        }

        [HttpPost, Route("Todos/{toDoItemId}/ReturnToWork", Name = "ReturnToWorkTodoComplete")]
        [Secure("CompleteToDo")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ReturnToWorkComplete(ReturnToWorkModel model)
        {
            ToDoItem wfi = ToDoItem.GetById(model.Id);
            if (wfi == null)
            {
                // some sort of error?
                return Json(model);
            }




            string outcome = null;
            if (model.Recertify)
            {
                outcome = ReturnToWorkActivity.RecertifyOutcomeValue;
            }
            else if (model.WillReturnToWork)
            {
                outcome = ReturnToWorkActivity.ReturnToWorkOutcomeValue;
            }
            else if (model.WillNotReturnToWork)
            {
                outcome = ReturnToWorkActivity.NotReturningToWorkOutcomeValue;
            }

            wfi.Metadata.SetRawValue("HasWorkRestrictions", model.HasWorkRestrictions);
            if (wfi.WorkflowInstance != null)
            {
                wfi.WorkflowInstance.Case.HasWorkRestrictions = model.HasWorkRestrictions;
            }
            //Add New Attribute "ReturnToWork" for Jira Work Item-EZW-3 : RTW not reflecting properly 
            if (wfi.DueDate != model.ReturnToWorkDate && wfi.ItemType == ToDoItemType.ReturnToWork)
            {
                wfi.Metadata.SetRawValue("ReturnToWork", model.ReturnToWorkDate);
            }
            wfi.WfOnToDoItemCompleted(CurrentUser, outcome, true);
            if (wfi.DueDate != model.ReturnToWorkDate && wfi.ItemType == ToDoItemType.ReturnToWork)
            {
                if (wfi.Case.CaseEvents != null && wfi.Case.CaseEvents.Count > 0 && model.ReturnToWorkDate != null && model.ReturnToWorkDate.HasValue)
                {
                    wfi.Case.CaseEvents[0].EventDate = model.ReturnToWorkDate.Value;
                }
                var caseService = new CaseService();
                caseService.UpdateCase(wfi.Case);
                Case theCase = caseService.GetCaseById(wfi.CaseId);
                theCase.WfOnCaseEventsUpdated();
            }
            return Json(model);
        }

        [HttpGet, Route("Todos/{toDoItemId}/CaseClosed", Name = "CaseClosedTodo")]
        [Secure("CompleteToDo")]
        public ActionResult CaseClosed(string toDoItemId)
        {
            try
            {
                var model = new CaseClosedModel(ToDoItem.GetById(toDoItemId));
                model.ValidationMessages = ToDo.ValidatePendingSegmentsForCaseClosed(model.CaseId);
                model.HasAnyResults = model.ValidationMessages.Any();
                model.EnabledCaseClosureCategories = CurrentCustomer.HasFeature(Feature.CaseClosureCategories);
                return Json(model);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error getting case closed to-do item");
            }
        }

        [HttpPost, Route("Todos/{toDoItemId}/CaseClosed", Name = "CaseClosedTodoComplete")]
        [Secure("CompleteToDo")]
        [ValidateApiAntiForgeryToken]
        public ActionResult CaseClosed(CaseClosedModel model)
        {
            try
            {
                ToDoItem wfi = ToDoItem.GetById(model.Id);
                if (wfi == null)
                {
                    return Json(false);
                }

                wfi.Metadata.SetRawValue("Reason", CaseClosureReason.ReturnToWork);
                wfi.Metadata.SetRawValue("ReasonCause", (model.Reason == "Other") ? model.OtherReasonText : model.Reason); // We don't have any other way to pass this value to CloseCaseActivity
                wfi.Metadata.SetRawValue("OtherReasonText", model.OtherReasonText); // We don't have any other way to pass this value to CloseCaseActivity
                wfi.Metadata.SetRawValue("CaseClosedDate", model.CaseClosedDate);
                wfi.Status = ToDoItemStatus.Complete;
                wfi.Save();
                wfi.WfOnToDoItemCompleted(CurrentUser);

                /// They may have added a manual close case todo 
                if (!wfi.IsWorkflow())
                {
                    Case theCase = AbsenceSoft.Data.Cases.Case.GetById(wfi.CaseId);
                    Case.CaseClosed(theCase, null, null, model.Reason, model.OtherReasonText);
                }


                return Json(model);
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Error completing case closed Todo item, {0}", model == null || model.Id == null ? "UNKNOWN" : model.Id), ex);
                return JsonError(ex, string.Format("Error completing close case ToDo item, {0}", ex.Message));
            }
        }

        [HttpGet, Route("Todos/{toDoItemId}/CaseReview", Name = "CaseReview")]
        [Secure("CompleteToDo")]
        public ActionResult CaseReview(string toDoItemId)
        {
            return Json(new BaseTodoModel(ToDoItem.GetById(toDoItemId)));
        }

        [HttpPost, Route("Todos/{toDoItemId}/CaseReview", Name = "CaseReviewCompleted")]
        [Secure("CompleteToDo")]
        [ValidateApiAntiForgeryToken]
        public ActionResult CaseReviewCompleted(BaseTodoModel model)
        {
            ToDoItem wfi = ToDoItem.GetById(model.Id);
            if (wfi == null)
            {
                return Json(false);
            }

            wfi.WfOnToDoItemCompleted(CurrentUser, Activity.CompleteOutcomeValue, true);

            return Json(true);
        }


        [HttpGet, Route("Todos/{toDoItemId}/VerifyReturnToWork", Name = "VerifyReturnToWork")]
        [Secure("CompleteToDo")]
        public ActionResult VerifyReturnToWork(string toDoItemId)
        {
            return Json(new VerifyReturnToWorkModel(ToDoItem.GetById(toDoItemId)));
        }

        [HttpPost, Route("Todos/{toDoItemId}/VerifyReturnToWork", Name = "VerifyReturnToWorkCompleted")]
        [Secure("CompleteToDo")]
        [ValidateApiAntiForgeryToken]
        public ActionResult VerifyReturnToWorkCompleted(VerifyReturnToWorkModel model)
        {
            ToDoItem wfi = ToDoItem.GetById(model.Id);
            if (wfi == null)
            {
                return Json(false);
            }

            string outcome = null;
            if (model.EmployeeDidNotReturn)
            {
                outcome = VerifyReturnToWorkActivity.RtwVerifiedNoOutcome;
            }
            else if (model.EmployeeReturned)
            {
                wfi.Metadata.SetRawValue("ReturnToWorkDate", model.VerifyReturnToWorkDate);
                outcome = VerifyReturnToWorkActivity.RtwVerifiedYesOutcome;
            }

            wfi.WfOnToDoItemCompleted(CurrentUser, outcome, true);

            //This is required for updating Actual RTW in case of a manual todo 
            if (!wfi.IsWorkflow() && model.EmployeeReturned)
            {
                wfi.Case.SetCaseEvent(CaseEventType.ReturnToWork, (DateTime)model.VerifyReturnToWorkDate);
                wfi.Case.Save();
            }
            return Json(true);
        }


        [HttpGet, Route("Todos/{toDoItemId}/ErgonomicAssessmentScheduled", Name = "ErgonomicAssessmentScheduled")]
        [Secure("CompleteToDo")]
        public ActionResult ErgonomicAssessmentScheduled(string toDoItemId)
        {
            return Json(new ErgonomicAssessmentScheduledModel(ToDoItem.GetById(toDoItemId)));
        }

        [HttpPost, Route("Todos/{toDoItemId}/ErgonomicAssessmentScheduledCompleted", Name = "ErgonomicAssessmentScheduledCompleted")]
        [Secure("CompleteToDo")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ErgonomicAssessmentScheduledCompleted(ErgonomicAssessmentScheduledModel model)
        {
            ToDoItem wfi = ToDoItem.GetById(model.Id);
            if (wfi == null)
            {
                return Json(false);
            }

            if (model.ErgonomicAssessmentScheduledDate.HasValue)
            {
                wfi.Metadata.SetRawValue("ErgonomicAssessmentDate", model.ErgonomicAssessmentScheduledDate.Value);
                wfi.WfOnToDoItemCompleted(CurrentUser, Activity.CompleteOutcomeValue, true);
            }

            return Json(true);
        }

        [HttpGet, Route("Todos/{toDoItemId}/CompleteErgonomicAssessment", Name = "CompleteErgonomicAssessment")]
        [Secure("CompleteToDo")]
        public ActionResult CompleteErgonomicAssessment(string toDoItemId)
        {
            return Json(new CompleteErgonomicAssessmentModel(ToDoItem.GetById(toDoItemId)));
        }

        [HttpPost, Route("Todos/{toDoItemId}/CompleteErgonomicAssessmentCompleted", Name = "CompleteErgonomicAssessmentCompleted")]
        [Secure("CompleteToDo")]
        [ValidateApiAntiForgeryToken]
        public ActionResult CompleteErgonomicAssessmentCompleted(CompleteErgonomicAssessmentModel model)
        {
            ToDoItem wfi = ToDoItem.GetById(model.Id);
            if (wfi == null)
            {
                return Json(false);
            }

            string outcome = null;

            if (model.ResponseText == "Yes")
            {
                outcome = CompleteErgonomicAssessmentActivity.ErgonomicAssessmentCompletedOutcome;
            }
            else if (model.ResponseText == "No")
            {
                outcome = CompleteErgonomicAssessmentActivity.ErgonomicAssessmentNotCompletedOutcome;
            }
            else
            {
                wfi.DueDate = model.AssessmentDate.HasValue ? model.AssessmentDate.Value : wfi.DueDate;
            }

            if (outcome != null)
            {
                wfi.WfOnToDoItemCompleted(CurrentUser, outcome, true);
            }
            else
            {
                wfi.Save();
            }

            return Json(true);
        }

        [HttpGet, Route("Todos/{toDoItemId}/EquipmentSoftwareOrdered", Name = "EquipmentSoftwareOrdered")]
        [Secure("CompleteToDo")]
        public ActionResult EquipmentSoftwareOrdered(string toDoItemId)
        {
            return Json(new EquipmentSoftwareOrderedModel(ToDoItem.GetById(toDoItemId)));
        }

        [HttpPost, Route("Todos/{toDoItemId}/EquipmentSoftwareOrderedCompleted", Name = "EquipmentSoftwareOrderedCompleted")]
        [Secure("CompleteToDo")]
        [ValidateApiAntiForgeryToken]
        public ActionResult EquipmentSoftwareOrderedCompleted(EquipmentSoftwareOrderedModel model)
        {
            ToDoItem wfi = ToDoItem.GetById(model.Id);
            if (wfi == null)
            {
                return Json(false);
            }

            ToDo.CompleteToDo(wfi, CurrentUser, model.Completed);
            return Json(true);
        }

        [HttpGet, Route("Todos/{toDoItemId}/EquipmentSoftwareInstalled", Name = "EquipmentSoftwareInstalled")]
        [Secure("CompleteToDo")]
        public ActionResult EquipmentSoftwareInstalled(string toDoItemId)
        {
            return Json(new EquipmentSoftwareInstalledModel(ToDoItem.GetById(toDoItemId)));
        }

        [HttpPost, Route("Todos/{toDoItemId}/EquipmentSoftwareInstalledCompleted", Name = "EquipmentSoftwareInstalledCompleted")]
        [Secure("CompleteToDo")]
        [ValidateApiAntiForgeryToken]
        public ActionResult EquipmentSoftwareInstalledCompleted(EquipmentSoftwareInstalledModel model)
        {
            ToDoItem wfi = ToDoItem.GetById(model.Id);
            if (wfi == null)
            {
                return Json(false);
            }

            ToDo.CompleteToDo(wfi, CurrentUser, model.Completed);

            return Json(true);
        }


        [HttpGet, Route("Todos/{toDoItemId}/HRISScheduleChange", Name = "HRISScheduleChange")]
        [Secure("CompleteToDo")]
        public ActionResult HRISScheduleChange(string toDoItemId)
        {
            return Json(new HRISScheduleChangeModel(ToDoItem.GetById(toDoItemId)));
        }

        [HttpPost, Route("Todos/{toDoItemId}/HRISScheduleChangeCompleted", Name = "HRISScheduleChangeCompleted")]
        [Secure("CompleteToDo")]
        [ValidateApiAntiForgeryToken]
        public ActionResult HRISScheduleChangeCompleted(HRISScheduleChangeModel model)
        {
            ToDoItem wfi = ToDoItem.GetById(model.Id);
            if (wfi == null)
            {
                return Json(false);
            }

            ToDo.CompleteToDo(wfi, CurrentUser, model.Completed);

            return Json(true);
        }

        [HttpGet, Route("Todos/{toDoItemId}/OtherAccommodation", Name = "OtherAccommodation")]
        [Secure("CompleteToDo")]
        public ActionResult OtherAccommodation(string toDoItemId)
        {
            return Json(new OtherAccommodationModel(ToDoItem.GetById(toDoItemId)));
        }

        [HttpPost, Route("Todos/{toDoItemId}/OtherAccommodationCompleted", Name = "OtherAccommodationCompleted")]
        [Secure("CompleteToDo")]
        [ValidateApiAntiForgeryToken]
        public ActionResult OtherAccommodationCompleted(OtherAccommodationModel model)
        {
            ToDoItem wfi = ToDoItem.GetById(model.Id);
            if (wfi == null)
            {
                return Json(false);
            }

            ToDo.CompleteToDo(wfi, CurrentUser, model.Completed);

            return Json(true);
        }

        [HttpGet, Route("Todos/{toDoItemId}/STDCaseManagerContactsEE", Name = "STDCaseManagerContactsEE")]
        [Secure("CompleteToDo")]
        public ActionResult STDCaseManagerContactsEE(string toDoItemId)
        {
            return Json(new BaseIsCompleteModel(ToDoItem.GetById(toDoItemId)));
        }

        [HttpGet, Route("Todos/{toDoItemId}/STDContactHCP", Name = "STDContactHCP")]
        [Secure("CompleteToDo")]
        public ActionResult STDContactHCP(string toDoItemId)
        {
            ToDoItem item = ToDoItem.GetById(toDoItemId);

            AbsenceSoft.Data.Customers.EmployeeContact employeeContact = item.Case.CasePrimaryProvider ?? new CaseService().GetCasePrimaryProviderContact(item.Case);
            if (employeeContact != null)
            {
                return Json(new STDContactHCPModel(item)
                {
                    HCPName = string.Format("{0} {1}", employeeContact.Contact.FirstName, employeeContact.Contact.LastName),
                    HCPPhone = employeeContact.Contact.WorkPhone,
                    HCPContactName = employeeContact.Contact.Metadata.GetRawValue<String>("ContactPersonName")
                });
            }

            return Json(item);
        }

        [HttpGet, Route("Todos/{toDoItemId}/STDDiagnosis", Name = "STDDiagnosis")]
        [Secure("CompleteToDo")]
        public ActionResult STDDiagnosis(string toDoItemId)
        {
            return Json(new BaseIsCompleteModel(ToDoItem.GetById(toDoItemId)));
        }

        [HttpGet, Route("Todos/{toDoItemId}/STDDesignatePrimaryHCP", Name = "STDDesignatePrimaryHCP")]
        [Secure("CompleteToDo")]
        public ActionResult STDDesignatePrimaryHCP(string toDoItemId)
        {
            return Json(new BaseIsCompleteModel(ToDoItem.GetById(toDoItemId)));
        }

        [HttpGet, Route("Todos/{toDoItemId}/STDGenHealthCond", Name = "STDGenHealthCond")]
        [Secure("CompleteToDo")]
        public ActionResult STDGenHealthCond(string toDoItemId)
        {
            ToDoItem item = ToDoItem.GetById(toDoItemId);

            if (item.Case.Disability != null)
            {
                return Json(new STDGenHealthCondModel(item)
                {
                    GenHealthCond = item.Case.Disability.GeneralHealthCondition,
                    ConditionStartDate = item.Case.Disability.ConditionStartDate
                });
            }

            return Json(item);
        }

        [HttpGet, Route("Todos/{toDoItemId}/STDFollowUp", Name = "STDFollowUp")]
        [Secure("CompleteToDo")]
        public ActionResult STDFollowUp(string toDoItemId)
        {
            return Json(new BaseIsCompleteModel(ToDoItem.GetById(toDoItemId)));
        }

        [HttpPost, Route("Todos/{toDoItemId}/STDDiagnosisComplete", Name = "STDDiagnosisComplete")]
        [Secure("CompleteToDo")]
        [ValidateApiAntiForgeryToken]
        public ActionResult STDDiagnosisComplete(BaseIsCompleteModel model)
        {
            ToDoItem wfi = ToDoItem.GetById(model.Id);
            if (wfi == null)
            {
                return Json(false);
            }

            ToDo.CompleteToDo(wfi, CurrentUser, model.Completed);

            return Json(true);
        }

        [HttpPost, Route("Todos/{toDoItemId}/STDGenHealthCondComplete", Name = "STDGenHealthCondComplete")]
        [Secure("CompleteToDo")]
        [ValidateApiAntiForgeryToken]
        public ActionResult STDGenHealthCondComplete(STDGenHealthCondModel model)
        {
            ToDoItem wfi = ToDoItem.GetById(model.Id);
            if (wfi == null)
            {
                return Json(false);
            }

            if (model.Completed)
            {
                wfi.Metadata.SetRawValue("GeneralHealthCondition", model.GenHealthCond);
                wfi.Metadata.SetRawValue("ConditionStartDate", model.ConditionStartDate);
                wfi.WfOnToDoItemCompleted(CurrentUser, Activity.CompleteOutcomeValue, true);
            }

            if (!string.IsNullOrWhiteSpace(model.GenHealthCond))
            {
                wfi.Case.Disability.GeneralHealthCondition = model.GenHealthCond;
                wfi.Case.Disability.ConditionStartDate = model.ConditionStartDate;

                wfi.Case.Save();
            }
            else
            {
                return Json(false);
            }

            return Json(true);
        }

        [HttpPost, Route("Todos/{toDoItemId}/STDFollowUpComplete", Name = "STDFollowUpComplete")]
        [Secure("CompleteToDo")]
        [ValidateApiAntiForgeryToken]
        public ActionResult STDFollowUpComplete(STDFollowUpModel model)
        {
            ToDoItem wfi = ToDoItem.GetById(model.Id);
            if (wfi == null)
            {
                return Json(false);
            }

            wfi.Metadata.SetRawValue("RTW", model.Completed);
            wfi.Metadata.SetRawValue("ContactEmployeeDueDate", model.ContactEmployeeDueDate);
            wfi.Metadata.SetRawValue("ContactHCPDueDate", model.ContactHCPDueDate);
            wfi.ResultText = model.Completed ? StdFollowUpActivity.StdLeaveCompleteOutcome : StdFollowUpActivity.StdLeaveNotCompleteOutcome;
            wfi.WfOnToDoItemCompleted(CurrentUser, null, true);

            return Json(true);
        }

        [HttpGet, Route("Todos/{toDoItemId}/CaseAttachmentReview", Name = "CaseAttachmentReviewTodo")]
        [Secure("CompleteToDo")]//TODO: NEED TO ADD: Permission.ToDos(ToDoItemType.ReviewCaseAttachment).First().Name
        public ActionResult CaseAttachmentReview(string toDoItemId)
        {
            return Json(new CaseAttachmentReviewModel(ToDoItem.GetById(toDoItemId)));
        }

        [HttpGet, Route("Todos/{toDoItemId}/CaseNoteReview", Name = "CaseNoteReviewTodo")]
        [Secure("CompleteToDo")]//TODO: NEED TO ADD: Permission.ToDos(ToDoItemType.ReviewCaseAttachment).First().Name
        public ActionResult CaseNoteReview(string toDoItemId)
        {
            return Json(new CaseNoteReviewModel(ToDoItem.GetById(toDoItemId)));
        }

        [HttpGet, Route("Todos/{toDoItemId}/QuestionToDo", Name = "QuestionToDo")]
        [Secure("CompleteToDo")]//TODO: NEED TO ADD: Permission.ToDos(ToDoItemType.ReviewCaseAttachment).First().Name
        public ActionResult QuestionToDo(string toDoItemId)
        {
            return Json(ToDoItem.GetById(toDoItemId));
        }

        [HttpPost, Route("Todos/{toDoItemId}/SaveQuestionToDo", Name = "SaveQuestionToDo")]
        [Secure("CompleteToDo")]
        [ValidateApiAntiForgeryToken]
        public ActionResult SaveQuestionToDo(ToDoItem model)
        {
            ToDoItem wfi = ToDoItem.GetById(model.Id);
            if (wfi == null)
            {
                return Json(false);
            }

            string outcome = null;
            if (!string.IsNullOrEmpty(model.ResultText))
            {
                outcome = model.ResultText;
            }
            wfi.WfOnToDoItemCompleted(CurrentUser, outcome, true);
            return Json(true);
        }



        [HttpPost, Route("Todos/{toDoItemId}/CompleteToDo", Name = "CompleteToDo")]
        [Secure("CompleteToDo")]
        [ValidateApiAntiForgeryToken]
        public ActionResult CompleteToDo(BaseIsCompleteModel model)
        {
            ToDoItem wfi = ToDoItem.GetById(model.Id);
            if (wfi == null)
            {
                return Json(false);
            }

            ToDo.CompleteToDo(wfi, CurrentUser, model.Completed, model.ResultText);

            return Json(true);
        }


        [HttpGet, Route("Todos/{toDoItemId}/EmployeeInformationReview", Name = "EmployeeInformationReviewTodo")]
        [Secure("CompleteToDo")]//TODO: NEED TO ADD: Permission.ToDos(ToDoItemType.ReviewCaseNote).First().Name
        public ActionResult EmployeeInformationReview(string toDoItemId)
        {
            return Json(new EmployeeInformationReviewModel(ToDoItem.GetById(toDoItemId)));
        }

        [HttpPost, Route("Todos/{toDoItemId}/EmployeeInformationReview", Name = "EmployeeInformationReviewComplete")]
        [Secure("CompleteToDo")]//TODO: NEED TO ADD: Permission.ToDos(ToDoItemType.ReviewCaseNote).First().Name
        [ValidateApiAntiForgeryToken]
        public ActionResult EmployeeInformationReview(EmployeeInformationReviewModel model)
        {
            ToDoItem wfi = ToDoItem.GetById(model.Id);
            if (wfi == null)
            {
                return Json(false);
            }

            wfi.Metadata.SetRawValue("OldValues", model.OldValues.ToBsonDocument());
            wfi.Metadata.SetRawValue("NewValues", model.NewValues.ToBsonDocument());
            wfi.WfOnToDoItemCompleted(CurrentUser, Activity.CompleteOutcomeValue, true);

            return Json(true);
        }

        [HttpGet, Route("Todos/{toDoItemId}/EmployeeCaseReview", Name = "EmployeeCaseReviewTodo")]
        [Secure("CompleteToDo")]//TODO: NEED TO ADD: Permission.ToDos(ToDoItemType.ReviewCaseNote).First().Name
        public ActionResult EmployeeCaseReview(string toDoItemId)
        {
            return Json(new EmployeeCaseReviewModel(ToDoItem.GetById(toDoItemId)));
        }

        [HttpPost, Route("Todos/{toDoItemId}/EmployeeCaseReviewed", Name = "EmployeeCaseReviewComplete")]
        [Secure("CompleteToDo")]//TODO: NEED TO ADD: Permission.ToDos(ToDoItemType.ReviewCaseNote).First().Name
        [ValidateApiAntiForgeryToken]
        public ActionResult EmployeeCaseReviewComplete(string toDoItemId)
        {
            ToDoItem wfi = ToDoItem.GetById(toDoItemId);
            if (wfi == null)
            {
                return Json(false);
            }

            wfi.WfOnToDoItemCompleted(CurrentUser, Activity.CompleteOutcomeValue, true);

            return Json(true);
        }

        [HttpGet, Route("Todos/{toDoItemId}/WorkScheduleReview", Name = "WorkScheduleReviewTodo")]
        [Secure("CompleteToDo")]//TODO: NEED TO ADD: Permission.ToDos(ToDoItemType.ReviewCaseNote).First().Name
        public ActionResult WorkScheduleReview(string toDoItemId)
        {
            return Json(new EmployeeWorkScheduleReviewModel(ToDoItem.GetById(toDoItemId)));
        }

        [HttpPost, Route("Todos/{toDoItemId}/WorkScheduleReviewed", Name = "WorkScheduleReviewComplete")]
        [Secure("CompleteToDo")]//TODO: NEED TO ADD: Permission.ToDos(ToDoItemType.ReviewCaseNote).First().Name
        [ValidateApiAntiForgeryToken]
        public ActionResult WorkScheduleReviewComplete(EmployeeWorkScheduleReviewModel model)
        {
            ToDoItem wfi = ToDoItem.GetById(model.Id);
            if (wfi == null)
            {
                return Json(false);
            }

            if (model.Update)
            {
                if (model.EmployeeInfo.WorkSchedule != null)
                {
                    DateTime schedStart = model.EmployeeInfo.WorkSchedule.StartDate;
                    if (model.EmployeeInfo.WorkSchedule.ScheduleType == ScheduleType.Weekly || model.EmployeeInfo.WorkSchedule.ScheduleType == ScheduleType.Variable)
                    {
                        schedStart = schedStart.AddDays(((int)schedStart.DayOfWeek * -1));
                    }

                    if (model.EmployeeInfo.WorkSchedule.Times.Any())
                    {
                        int i = 0;
                        foreach (var time in model.EmployeeInfo.WorkSchedule.Times)
                        {
                            time.TotalMinutes = string.IsNullOrWhiteSpace(time.TotalMinutes) ? "0" : time.TotalMinutes;
                            time.SampleDate = schedStart.AddDays(i);
                            i++;
                        }
                    }
                }
            }

            wfi.Metadata.SetRawValue("WorkSchedule", model.EmployeeInfo.WorkSchedule.ToSchedule());
            wfi.WfOnToDoItemCompleted(CurrentUser, Activity.CompleteOutcomeValue, true);

            return Json(false);
        }


        [HttpGet, Route("Todos/{toDoItemId}/WCManagerCaseReview", Name = "WCManagerCaseReviewTodo")]
        [Secure("CompleteToDo")]//TODO: NEED TO ADD: Permission.ToDos(ToDoItemType.ReviewCaseNote).First().Name
        public ActionResult WCManagerCaseReview(string toDoItemId)
        {
            return Json(ToDoItem.GetById(toDoItemId));
        }

        [HttpPost, Route("Todos/{toDoItemId}/WCManagerCaseReviewed", Name = "WCManagerCaseReviewComplete")]
        [Secure("CompleteToDo")]//TODO: NEED TO ADD: Permission.ToDos(ToDoItemType.ReviewCaseNote).First().Name
        [ValidateApiAntiForgeryToken]
        public ActionResult WCManagerCaseReviewComplete(ToDoItem model)
        {
            //ToDoItem wfi = ToDoItem.GetById(model.Id);
            if (model == null)
            {
                return Json(false);
            }

            ToDo.CompleteToDo(model, CurrentUser, true, "CASE REVIEWED");

            return Json(false);
        }


        [HttpGet, Route("Todos/{toDoItemId}/InitiateNewLeaveReview", Name = "InitiateNewLeaveReview")]
        [Secure("CompleteToDo")]//TODO: NEED TO ADD: Permission.ToDos(ToDoItemType.ReviewCaseNote).First().Name
        public ActionResult InitiateNewLeaveReview(string toDoItemId)
        {
            return Json(ToDoItem.GetById(toDoItemId));
        }

        [HttpPost, Route("Todos/{toDoItemId}/InitiateNewLeaveReviewComplete", Name = "InitiateNewLeaveReviewComplete")]
        [Secure("CompleteToDo")]//TODO: NEED TO ADD: Permission.ToDos(ToDoItemType.ReviewCaseNote).First().Name
        [ValidateApiAntiForgeryToken]
        public ActionResult InitiateNewLeaveReviewComplete(ToDoItem model)
        {
            //ToDoItem wfi = ToDoItem.GetById(model.Id);
            if (model == null)
            {
                return Json(false);
            }

            ToDo.CompleteToDo(model, CurrentUser, true, "NEW LEAVE TODO REVIEWED");

            return Json(false);
        }

        [HttpGet, Route("Todos/{toDoItemId}/GetManualTodo", Name = "GetManualTodo")]
        [Secure("CompleteToDo")]//TODO: NEED TO ADD: Permission.ToDos(ToDoItemType.ReviewCaseNote).First().Name
        public ActionResult GetManualTodo(string toDoItemId)
        {
            return Json(ToDoItem.GetById(toDoItemId));
        }

        [HttpGet, Route("Todos/{todoItemId}/DetermineRequest", Name = "GetIntermittentRequest")]
        [Secure("CompleteToDo")]
        public ActionResult GetIntermittentRequest(string toDoItemId)
        {
            ToDoItem determineRequestToDo = ToDoItem.GetById(toDoItemId);
            Guid intermittentRequestId = determineRequestToDo.Metadata.GetRawValue<Guid>("IntermittentTimeRequestId");
            IntermittentTimeRequest matchingRequest = determineRequestToDo.Case.Segments.SelectMany(s => s.UserRequests).FirstOrDefault(ur => ur.Id == intermittentRequestId);
            return Json(matchingRequest);
        }


        [Secure("CompleteToDo"), HttpPost]
        [Route("Todos/Reassign", Name = "ReassignTodos")]
        [ValidateApiAntiForgeryToken]
        public JsonResult ReassignTodos(ReassignTodosModel model)
        {
            string userId = model.UserId;
            if (model == null || !model.Todos.Any())
            {
                return Json(model);
            }

            try
            {
                using (AuthenticationService svc = new AuthenticationService())
                {
                    var user = svc.GetUserById(userId);
                    if (user == null)
                    {
                        return JsonError(new Exception("User was not found or no Id was supplied"), "User was not found or no Id was supplied", 404);
                    }
                }

                foreach (ReassignTodo rt in model.Todos)
                {
                    if (rt.ApplyStatus)
                    {
                        var t = ToDoItem.GetById(rt.TodoId);
                        if (t == null)
                        {
                            return JsonError(new Exception("Todo was not found or no Id was supplied"), "Todo was not found or no Id was supplied", 404);
                        }

                        t.AssignedToId = userId;
                        t.Save();
                    }
                }
                return Json(model);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("CompleteToDo"), HttpPost]
        [Route("Todos/ReassignPaperWorkReview", Name = "ReassignTodoPaperWorkReview")]
        [ValidateApiAntiForgeryToken]
        public JsonResult ReassignTodoPaperWorkReview(PaperWorkTodoReviewModel model, string TodoId)
        {
            string userId = string.Empty;
            if (model == null || string.IsNullOrEmpty(TodoId))
            {
                return Json(model);
            }

            if (model.ReAssign != null)
            {
                userId = model.ReAssign.UserId;
            }

            try
            {
                using (AuthenticationService svc = new AuthenticationService())
                {
                    var user = svc.GetUserById(userId);
                    if (user == null)
                    {
                        return JsonError(new Exception("User was not found or no Id was supplied"), "User was not found or no Id was supplied", 404);
                    }
                }

                var t = ToDoItem.GetById(TodoId);
                if (t == null)
                {
                    return JsonError(new Exception("Todo was not found or no Id was supplied"), "Todo was not found or no Id was supplied", 404);
                }

                t.AssignedToId = userId;
                t.Save();

                return Json(new { result = "Redirect", RedirectUrl = Url.Action("ViewCase", "Cases", new { caseId = model.CaseId }) });

            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        private string GetToDoItemPaperworkTemplateCode(ToDoItem item)
        {
            string returnValue = string.Empty;
            string commId = item.Metadata.GetRawValue<string>("CommunicationId");
            string pId = item.Metadata.GetRawValue<string>("PaperworkId");
            Communication comm = Communication.GetById(commId);

            if (comm != null && !string.IsNullOrWhiteSpace(pId))
            {
                returnValue = comm.Metadata.GetRawValue<string>("Template");

            }
            return returnValue;
        }

    }// public class TodosController : BaseController
}