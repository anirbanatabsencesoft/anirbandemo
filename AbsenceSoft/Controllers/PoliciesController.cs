﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Common;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using MongoDB.Driver.Linq;
using System.Text;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Models.Policies;
using AbsenceSoft.Models.Cases;
using AbsenceSoft.Data.Pay;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Pay;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    [Secure]
    public class PoliciesController : BaseController
    {
        [HttpGet, Title("Policies")]
        [Route("Policies/{employerId}/{policyId?}", Name = "Policies")]
        //[Secure("PolicyConfig")]
        public ViewResult Index(string employerId, string policyId)
        {
            if (!string.IsNullOrWhiteSpace(employerId))
                ViewBag.EmployerId = employerId;
            if (!string.IsNullOrWhiteSpace(policyId))
                ViewBag.PolicyId = policyId;

            return View();
        }

        [HttpGet]
        [Route("Policies/{policyId}/GetPolicyById/{employerId}", Name = "GetPolicyById")]
        public JsonResult GetPolicyById(string policyId, string employerId)
        {
            if (string.IsNullOrEmpty(policyId))
                return JsonError(null, "Missing Policy Id");

            try
            {
                using (var svc = new PolicyService(CurrentUser.CustomerId, employerId))
                {
                    // fix up the percentages for display / edit
                    Policy p = svc.GetPolicyById(policyId);

                    PolicyToPercentages(p);
                    return Json(p);
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error Getting Policy");
            }
        }


        [HttpGet]
        [Route("Policies/{code}/GetPolicyByCode", Name = "GetPolicyByCode")]
        public JsonResult GetPolicyByCode(string code, string employerId)
        {
            if (string.IsNullOrEmpty(code))
                return JsonError(null, "Missing Policy Code");

            if (string.IsNullOrEmpty(employerId))
                return JsonError(null, "Missing Policy EmployerId");

            try
            {
                return Json(Policy.GetByCode(code, CurrentCustomer.Id, employerId));
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error Getting Policy");
            }
        }

        [HttpGet]
        [Route("Policies/{policyId}/UpsertPolicy/{employerId}", Name = "EditPolicy")]
        [Route("Policy/{employerId}/Create", Name = "CreatePolicy")]
        public ViewResult UpsertPolicy(string policyId, string employerId)
        {
            if (!string.IsNullOrEmpty(policyId))
                ViewBag.PolicyId = policyId;

            if (!string.IsNullOrEmpty(employerId))
                ViewBag.EmployerId = employerId;

            return View("Upsert");
        }

        [HttpDelete, Route("Policy/{employerId}/Delete/{policyCode}")]
        [ValidateApiAntiForgeryToken]
        public JsonResult DeletePolicy(string employerId, string policyCode)
        {
            try
            {
                new PolicyService(CurrentUser.CustomerId, employerId).DeleteOrRevertPolicy(policyCode);
                return Json(true);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error deleting policy");
            }
        }


        [HttpPost]
        [Route("Policies/RuleToRuleExpression", Name = "RuleToRuleExpression")]
        [ValidateApiAntiForgeryToken]
        public JsonResult RuleToRuleExpression(PolicyRules model)
        {
            try
            {
                // Validate the input
                StringBuilder errors = new StringBuilder();

                if (string.IsNullOrEmpty(model.EmployerId))
                    errors.Append("Could not locate Customer account<br />");

                if (model.Rules == null)
                    errors.Append("Could not find rules<br />");

                if (errors.Length > 0)
                    return JsonError(null, errors.ToString());

                //default null values?
                List<RuleExpression> ruleExpressions = new List<RuleExpression>();
                using (PolicyService svc = new PolicyService(CurrentCustomer.Id, model.EmployerId))
                {
                    var expressions = svc.GetRuleExpressions();
                    foreach (var rule in model.Rules)
                    {
                        ruleExpressions.Add(svc.GetExpression(rule, expressions));
                    }

                    return Json(ruleExpressions);
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error Upating Policy");
            }
        }

        [HttpPost, Route("Policies/RuleExpressionToRule", Name = "RuleExpressionToRule")]
        [ValidateApiAntiForgeryToken]
        public JsonResult RuleExpressionToRule(PolicyRuleExpressions model)
        {
            try
            {
                StringBuilder errors = new StringBuilder();

                if (string.IsNullOrEmpty(model.EmployerId))
                    errors.Append("Could not locate Customer account<br />");

                if (model.Expressions == null)
                    errors.Append("Could not find expressions<br />");

                if (errors.Length > 0)
                    return JsonError(null, errors.ToString());
                List<Rule> rules = new List<Rule>();
                using (PolicyService svc = new PolicyService(CurrentCustomer.Id, model.EmployerId))
                {
                    foreach (var expression in model.Expressions)
                        rules.Add(svc.GetRule(expression));
                }
                return Json(rules);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error validating expression");
            }

        }

        [HttpPost, Route("Policies/{id}/CheckPolicyForConflicts", Name = "CheckPolicyForConflicts")]
        [ValidateApiAntiForgeryToken]
        public JsonResult CheckPolicyForConflicts(Policy model)
        {
            try
            {
                List<string> potentialConflicts = new List<string>();

                if (!string.IsNullOrEmpty(model.WorkState)
                    && model.AbsenceReasons.Any(ar => !string.IsNullOrEmpty(ar.WorkState) && model.WorkState != ar.WorkState))
                    potentialConflicts.Add("Work State");

                if (model.Gender != null
                    && model.AbsenceReasons.Any(ar => ar.Gender != null && model.Gender != ar.Gender))
                    potentialConflicts.Add("Gender");

                List<Rule> policyRules = model.RuleGroups.SelectMany(r => r.Rules).ToList();
                List<Rule> absenceReasonRules = model.AbsenceReasons.SelectMany(ar => ar.RuleGroups).SelectMany(rg => rg.Rules).ToList();

                foreach (Rule rule in policyRules)
                {
                    if (IsTimeOfServiceRule(rule) && absenceReasonRules.Any(r => IsTimeOfServiceRule(r)))
                        potentialConflicts.Add(rule.Name);
                    else if (IsHoursWorkedRule(rule) && absenceReasonRules.Any(r => IsHoursWorkedRule(r)))
                        potentialConflicts.Add(rule.Name);
                    else if (IsPayRule(rule) && absenceReasonRules.Any(r => IsPayRule(r)))
                        potentialConflicts.Add(rule.Name);
                    else if (IsRelatedPersonRule(rule) && absenceReasonRules.Any(r => IsRelatedPersonRule(r)))
                        potentialConflicts.Add(rule.Name);
                    else if (absenceReasonRules.Any(r => r.LeftExpression == rule.LeftExpression))
                        potentialConflicts.Add(rule.Name);
                }

                return Json(potentialConflicts);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to check for conflicts in policy");
            }
        }

        public bool IsTimeOfServiceRule(Rule rule)
        {
            return rule.LeftExpression.StartsWith("Employee.ServiceDate")
                || rule.LeftExpression.StartsWith("Employee.HireDate")
                || rule.LeftExpression.StartsWith("Employee.RehireDate")
                || rule.LeftExpression.StartsWith("TotalMonthsWorked")
                || rule.LeftExpression.StartsWith("Has1YearOfService")
                || rule.LeftExpression.StartsWith("HasMinLengthOfService");
        }

        public bool IsHoursWorkedRule(Rule rule)
        {
            return rule.LeftExpression.StartsWith("TotalHoursWorkedLast12Months")
                || rule.LeftExpression.StartsWith("HoursWorkedPerWeek")
                || rule.LeftExpression.StartsWith("AverageHoursWorkedPerWeekOverPeriod")
                || rule.LeftExpression.StartsWith("HasLesserOfHalfFTHoursOrNHoursPerWeek");
        }

        public bool IsPayRule(Rule rule)
        {
            return rule.LeftExpression.StartsWith("Employee.PayType")
                || rule.LeftExpression.StartsWith("Employee.Salary")
                || rule.LeftExpression.StartsWith("EffectiveAnnualPayFromLeaveStartDate");
        }

        public bool IsJobRule(Rule rule)
        {
            return rule.LeftExpression.StartsWith("Employee.WorkType")
                || rule.LeftExpression.StartsWith("Employee.JobTitle")
                || rule.LeftExpression.StartsWith("Employee.Job.Classification")
                || rule.LeftExpression.StartsWith("Employee.Job.CensusJobCategoryCode")
                || rule.LeftExpression.StartsWith("Employee.Job.SOCCode");
        }

        public bool IsRelatedPersonRule(Rule rule)
        {
            return rule.LeftExpression.StartsWith("CaseRelationshipType")
                || rule.LeftExpression.StartsWith("Case.Contact.ContactTypeOther")
                || rule.LeftExpression.StartsWith("Case.Contact.YearsOfAge")
                || rule.LeftExpression.StartsWith("Case.Contact.MilitaryStatus");

        }


        [HttpPost]
        [Route("Policies/{id}/UpsertPolicyPost", Name = "UpsertPolicyPost")]
        [ValidateApiAntiForgeryToken]
        public JsonResult UpsertPolicyPost(Policy model)
        {
            try
            {
                //
                // HACK: Remove metadata from RuleGroups. UI is feeding extra
                // garbage into Metadata and adding many lines of multidimentional arrays to Mongo
                // for policy and case data.  This doesn't appear to affect calculations, but it does
                // make viewing Mongo data a pain.
                var ruleGroups = model.RuleGroups ?? new List<PolicyRuleGroup>();
                ruleGroups.ForEach(rg => rg.Metadata.Clear());
                ruleGroups = (model.AbsenceReasons ?? new List<PolicyAbsenceReason>()).SelectMany(m => m.RuleGroups).ToList();
                ruleGroups.ForEach(ar => ar.Metadata.Clear());

                // Validate the input
                StringBuilder errors = new StringBuilder();

                if (string.IsNullOrEmpty(model.CustomerId))
                    model.CustomerId = CurrentCustomer.Id;

                if (string.IsNullOrEmpty(model.EmployerId))
                    errors.Append("Could not locate Customer account<br />");

                if (string.IsNullOrEmpty(model.Name))
                    errors.Append("Policy Name is required<br />");
                if (string.IsNullOrEmpty(model.Code))
                    errors.Append("Policy Code is required<br />");
                if (model.EffectiveDate == DateTime.MinValue)
                    errors.Append("Effective Date is required<br />");

                if (errors.Length > 0)
                    return JsonError(null, errors.ToString());


                // convert payment percentages back into percentages
                foreach (PolicyAbsenceReason ar in model.AbsenceReasons)
                {
                    if (ar.Reason == null)
                    {
                        ar.Reason = AbsenceReason.GetByCode(ar.ReasonCode, model.CustomerId, model.EmployerId);
                    }
                    foreach (PaymentInfo pi in ar.PaymentTiers)
                    {
                        pi.PaymentPercentage /= 100;
                        pi.MaxPaymentPercentage /= 100;
                    }
                }

                //default null values?

                using (PolicyService svc = new PolicyService(model.CustomerId, model.EmployerId))
                {

                    var policy = svc.UpsertPolicy(model);
                    PolicyToPercentages(policy);

                    PolicyViewModel pvm = new PolicyViewModel();
                    pvm.EmployerPeriodType = Employer.GetById(model.EmployerId).FMLPeriodType;
                    pvm.Policy = policy;

                    return Json(pvm);
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error Upating Policy");
            }
        }

        private void PolicyToPercentages(Policy p)
        {
            foreach (PolicyAbsenceReason ar in p.AbsenceReasons)
            {
                foreach (PaymentInfo pi in ar.PaymentTiers)
                {
                    pi.MaxPaymentPercentage *= 100;
                    pi.PaymentPercentage *= 100;
                }
            }
        }

        [HttpPost]
        [Route("Policies/List", Name = "PoliciesList")]
        [ValidateApiAntiForgeryToken]
        public JsonResult List(ListCriteria criteria)
        {
            string employerId = criteria.Get<string>("EmployerId");
            try
            {
                using (var svc = new PolicyService(CurrentUser.CustomerId, employerId))
                {
                    return Json(svc.PolicyList(criteria));
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error getting policy list");
            }
        }

        [HttpGet, Route("Policy/{employerId}/Duplicate/{code}")]
        public JsonResult IsDuplicate(string employerId, string code, string policyId)
        {
            ListCriteria criteria = new ListCriteria();
            criteria.Set("EmployerId", employerId);
            criteria.Set("Code", code);
            try
            {
                using (var svc = new PolicyService(CurrentUser.CustomerId, employerId))
                {
                    var results = svc.PolicyList(criteria);
                    if (results.Results.Count() > 0)
                    {
                        string foundId = results.Results.First().Get<string>("Id");
                        return Json(foundId != policyId);
                    }
                    return Json(false);
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error checking for duplicate policies");
            }
        }

        [HttpPost]
        [Route("Policies/AbsReasonList", Name = "AbsReasonList")]
        [ValidateApiAntiForgeryToken]
        public JsonResult AbsReasonList(ListCriteria criteria)
        {
            try
            {
                using (var svc = new PolicyService(CurrentCustomer, CurrentEmployer, CurrentUser))
                {
                    return Json(svc.PolicyAbsReasonList(criteria));
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error getting policy absence reason list");
            }
        }

        [HttpPost]
        [Route("Policies/{policyId}/ToggleEnabledPolicy/{employerId}", Name = "ToggleEnabledPolicy")]
        [ValidateApiAntiForgeryToken]
        public JsonResult ToggleEnabledPolicy(string policyId, string employerId)
        {
            //Policy model = new Policy();

            if (string.IsNullOrEmpty(policyId))
                return JsonError(null, "Missing Policy Id");

            var policy = Policy.GetById(policyId);
            if (policy == null)
                return JsonError(null, "Missing Policy");

            //employerId = employerId;

            try
            {
                bool enable = false;
                if (!string.IsNullOrWhiteSpace(employerId))
                {
                    var emp = Employer.GetById(employerId);
                    if (emp != null && emp.SuppressPolicies.Any() && emp.SuppressPolicies.Contains(policy.Code.ToUpperInvariant()))
                        enable = true;
                }

                using (var svc = new PolicyService(CurrentUser.CustomerId, employerId))
                {
                    if (enable)
                        svc.EnablePolicy(policy.Code);
                    else
                        svc.DisablePolicy(policy.Code);
                }
                return Json(policy);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error Upating Policy");
            }
        }

        [HttpPost]
        //[Secure("PolicyConfig")]
        [Route("Policies/{Id}/RemoveAbsReason", Name = "RemoveAbsReason")]
        [ValidateApiAntiForgeryToken]
        public JsonResult RemoveAbsReason(RemoveAbsReasonModel model)
        {
#warning 4/2/2015 TPA-UI: Need to provide the employerId here, probably in the URL RouteData, but either way needs to be provided for this operation

            if (string.IsNullOrEmpty(model.PolicyId))
                return JsonError(null, "Missing Policy Id");

            if (string.IsNullOrEmpty(model.AbsReasonCode))
                return JsonError(null, "Missing Absence Reason Code");           

            try
            {

                var policy = Policy.GetById(model.PolicyId);
                if (policy == null)
                    return JsonError(null, "Missing Policy");

                if (policy.AbsenceReasons.Any(a => a.ReasonCode == model.AbsReasonCode))
                    policy.AbsenceReasons.Remove(policy.AbsenceReasons.Where(ab => ab.ReasonCode == model.AbsReasonCode).First());

                policy.Save();
                return Json(policy);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error Removing Policy Absence Reason");
            }
        }

        [Secure("EditEmployee"), HttpGet, Title("Policies")]
        [Route("Policies/Search/{employerId}", Name = "SearchPolicies")]
        public JsonResult Search(string employerId)
        {
            ListCriteria criteria = new ListCriteria();
            criteria.Set("EmployerId", employerId);
            try
            {
                using (var service = new PolicyService(CurrentCustomer.Id, CurrentUser.Employers[0].Id.ToString()))
                {
                    var data = service.PolicySearch(criteria);
                    var results = data.Results;
                    if (data.Results != null && data.Results.Any())
                    {
                        return Json(results, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { id = "", text = "" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error getting policies search");
            }
        }

        [HttpGet]
        [Route("Policies/GetRuleExpressions", Name = "GetRuleExpressions")]
        public JsonResult GetRuleExpressions(string employerId)
        {
            try
            {
                using (var svc = new PolicyService(CurrentUser.CustomerId, employerId))
                {
                    return Json(svc.GetRuleExpressions());
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error getting rule expressions");
            }
        }

        [HttpGet, Route("Policies/{employerId}/PayOrder")]
        public ActionResult PolicyPayOrder(string employerId)
        {
            ViewBag.EmployerId = employerId;
            return View();
        }

        [HttpGet, Route("Policies/{employerId}/PoliciesPayOrder")]
        public JsonResult PoliciesPayOrder(string employerId)
        {
            try
            {
                List<PolicyPayOrder> policiesPayOrder = new PayService(CurrentUser).GetEmployerPolicyPayOrder(employerId);
                return Json(policiesPayOrder);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to load current policies pay order");
            }
        }

        [HttpPost, Route("Policies/PoliciesPayOrder")]
        [ValidateApiAntiForgeryToken]
        public JsonResult PoliciesPayOrder(EmployerPolicyPayOrder policyPayOrder)
        {
            try
            {
                new PayService(CurrentUser).SaveEmployerPolicyPayOrder(policyPayOrder.PolicyPayOrder);
                return Json(policyPayOrder.PolicyPayOrder);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to save policy pay oder");
            }
        }

    }
}