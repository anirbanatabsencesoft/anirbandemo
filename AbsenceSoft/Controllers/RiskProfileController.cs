﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.RiskProfiles;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.RiskProfiles;
using AbsenceSoft.Models.RiskProfiles;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Controllers
{
    [Secure]
    public class RiskProfileController : BaseController
    {
        [Secure("EditRiskProfiles")]
        [Title("Risk Profiles")]
        [HttpGet, Route("RiskProfiles/List/{employerId?}", Name ="ViewRiskProfiles")]
        public ActionResult ViewRiskProfiles(string employerId)
        {
            return View();
        }

        [Secure("EditRiskProfiles")]
        [HttpPost, Route("RiskProfiles/List", Name ="ListRiskProfiles")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListRiskProfiles(ListCriteria criteria)
        {
            using (var riskProfileService = new RiskProfileService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                /// They can't change the order, so we'll enforce it to be sort by order ascending
                /// In order to support drag and drop in the UI, we also want to bring back all risk profiles
                criteria.SortBy = "Order";
                criteria.SortDirection = Data.Enums.SortDirection.Ascending;
                criteria.PageSize = int.MaxValue;
                ListResults results = riskProfileService.RiskProfileList(criteria);
                return PartialView(results);
            }
        }

        [Secure("EditRiskProfiles")]
        [HttpGet, Route("RiskProfiles/Expressions/{employerId?}", Name = "RiskProfileExpressions")]
        public ActionResult RiskProfileExpressions(string employerId)
        {
            using (var riskProfileService = new RiskProfileService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                return Json(riskProfileService.GetRuleExpressions());
            }
        }

        [Secure("EditRiskProfiles")]
        [Title("Edit Risk Profile")]
        [HttpGet, Route("RiskProfiles/EditRiskProfile/{employerId?}", Name ="EditRiskProfile")]
        public ActionResult EditRiskProfile(string employerId, string id)
        {
            using (var riskProfileService = new RiskProfileService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                RiskProfileViewModel viewModel = new RiskProfileViewModel(riskProfileService.GetRiskProfile(id));
                return View(viewModel);
            }
        }

        [Secure("EditRiskProfiles")]
        [HttpGet, Route("RiskProfiles/RiskProfileExpressions/{employerId?}", Name = "EditRiskProfileExpressions")]
        public ActionResult RiskProfileExpressions(string employerId, string id)
        {
            using (var riskProfileService = new RiskProfileService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                var riskProfile = riskProfileService.GetRiskProfile(id);
                var expressions = riskProfileService.GetRuleExpressions();
                RiskProfileViewModel viewModel = new RiskProfileViewModel(riskProfile, expressions);
                return Json(viewModel);
            }
        }

        [Secure("EditRiskProfiles")]
        [Title("EditRiskProfile")]
        [HttpPost, Route("RiskProfiles/SaveRiskProfile/{employerId?}", Name = "SaveRiskProfile")]
        [ValidateApiAntiForgeryToken]
        public ActionResult SaveRiskProfile(string employerId, RiskProfileViewModel riskProfile)
        {
            try
            {
                using (var riskProfileService = new RiskProfileService(CurrentCustomer, CurrentEmployer, CurrentUser))
                {
                    RiskProfile profile = riskProfile.ApplyToDataModel(riskProfileService.GetRiskProfile(riskProfile.Id));
                    return Json(riskProfileService.SaveRiskProfile(profile));
                }
            }
            catch(Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditRiskProfiles")]
        [HttpDelete, Route("RiskProfiles/DeleteRiskProfile/{employerId?}", Name = "DeleteRiskProfile")]
        public ActionResult DeleteRiskProfile(string employerId, string id)
        {
            using (var riskProfileService = new RiskProfileService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                RiskProfile profile = riskProfileService.GetRiskProfile(id);
                if (profile.IsCustom)
                    riskProfileService.DeleteRiskProfile(profile);

                return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewRiskProfiles"));
            }
        }

        [Secure("EditRiskProfiles")]
        [HttpPost, Route("RiskProfiles/UpdateRiskProfilesOrder/{employerId?}", Name ="UpdateRiskProfilesOrder")]
        [ValidateApiAntiForgeryToken]
        public ActionResult UpdateRiskProfilesOrder(string employerId, UpdateRiskProfilesOrder newOrder)
        {
            using (var riskProfileService = new RiskProfileService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                foreach (var riskProfile in newOrder.RiskProfiles.Where(rp => rp.Order.HasValue))
                {
                    riskProfileService.UpdateRiskProfileOrder(riskProfile.Id, riskProfile.Order.Value);
                }
            }

            return Json(true);
        }

        [Secure]
        [HttpPost, Route("RiskProfiles/CalculateRiskProfile/{employerId?}", Name ="CalculateRiskProfile")]
        [ValidateApiAntiForgeryToken]
        public ActionResult CalculateRiskProfile(string employerId, string employeeId, string caseId)
        {
            using (var riskProfileService = new RiskProfileService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                var calculatedProfile = riskProfileService.CalculateAndSaveRiskProfile(employeeId, caseId);
                if (calculatedProfile == null || calculatedProfile.RiskProfile == null)
                    return Json(new JsonNetResult() { Data = false });

                return Json(calculatedProfile.RiskProfile);
            }
            
        }
    }
}