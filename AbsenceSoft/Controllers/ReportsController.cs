﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Filters;
using AbsenceSoft.Models.Reports;
using AbsenceSoft.Models.OshaApi;
using AbsenceSoft.Reporting;
using System;
using System.Web.Script.Serialization;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web;
using AbsenceSoft.Logic.Reporting;
using AbsenceSoft.Logic;
using AbsenceSoft.Data.Reporting;
using Newtonsoft.Json;
using System.Text;
using MongoDB.Driver;
using MongoDB.Bson;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.WorkRelated;
using AbsenceSoft.Data.Communications;
using System.IO.Pipes;
using System.Threading.Tasks;
using AbsenceSoft.Models.Organization;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    [Secure("RunCaseManagerReport", "RunBIReport", "RunAdHocReport", "RunOshaReport")]
    public class ReportsController : BaseController
    {
        private int DefaultAdHocReportRowLimit
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["DefaultAdHocReportRowLimit"]); }
        }

        // GET: Report
        [Title("Reports"), Route("reports", Name = "ReportHome")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route("reports/home")]
        public PartialViewResult AbsenceReportsHome()
        {
            return PartialView("_ReportHome");
        }

        [HttpGet]
        [Route("reports/absencereports/{category}")]
        public PartialViewResult AbsenceReports(string category)
        {
            List<BaseReport> reports = new ReportService().GetReportsByMainCategory(category);
            Dictionary<string, List<BaseReport>> categoryReports = new Dictionary<string, List<BaseReport>>();
            foreach (BaseReport rep in reports)
            {
                if (!categoryReports.ContainsKey(rep.Category))
                {
                    categoryReports.Add(rep.Category, new List<BaseReport>());
                }
                categoryReports[rep.Category].Add(rep);
            }
            ViewBag.Category = category;
            return PartialView("_ReportList", categoryReports);
        }


        [Secure("RunAdHocReport")]
        [HttpGet, Route("Reports/AdHocReports", Name = "AdHocReports"), Title("Ad-Hoc Reports")]
        public ActionResult AdHocReports()
        {
            return View();
        }

        [Secure("RunAdHocReport")]
        [HttpPost, Route("AdHocReport/Run", Name = "RunAdHocReport")]  
        [ValidateApiAntiForgeryToken]
        public ActionResult RunAdHocReport(AdHocReportViewModel report)
        {
            using (var reportingService = new AdHocReportingService(Current.User()))
            {
                ReportDefinition definition = report.CreateReportDefinition();
                AdHocReportResultViewModel results = new AdHocReportResultViewModel(reportingService.RunReport(definition, null, null, DefaultAdHocReportRowLimit));
                return PartialView("_AdHocReportResultsAndInformation", results);
            }

        }

        [Secure("RunAdHocReport")]
        [HttpPost, Route("AdHocReport/Save", Name = "SaveAdHocReport")]
        [ValidateApiAntiForgeryToken]
        public ActionResult SaveAdHocReport(AdHocReportViewModel report)
        {
            if (!ModelState.IsValid)
                return Json(report);

            using (var reportingService = new AdHocReportingService(Current.User()))
            {
                ReportDefinition definition = report.CreateReportDefinition();
                return Json(reportingService.SaveReportDefinition(definition, report.Name));
            }
        }

        [Secure("RunAdHocReport")]
        [HttpPost, Route("AdHocReport/SaveAs", Name = "SaveAsAdHocReport")]
        [ValidateApiAntiForgeryToken]
        public ActionResult SaveAsAdHocReport(AdHocReportViewModel report)
        {
            if (!ModelState.IsValid)
                return Json(report);

            using (var reportingService = new AdHocReportingService(Current.User()))
            {
                ReportDefinition definition = report.CreateReportDefinition();
                return Json(reportingService.SaveAsReportDefinition(definition, report.Name));
            }
        }
        /// <summary>
        /// Osha 300A Form Submission
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("Submit300A", Name = "Submit300A")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Submit300A(Osha300AFormPreviewModel model)
        {
            using (var efs = new OshaEFilingService())
            {
                List<SubmissionResponse> responses = new List<SubmissionResponse>();
                for(int i=0;i<model.Osha300AFormPreviewList.Count;i++)
                {
                    SubmissionResponse response = await efs.Submit300A(model.Osha300AFormPreviewList[i]);
                    if (response != null)
                    {
                        responses.Add(response);
                    }
                }

                return View("Submit300A", new ApiResultViewModel(responses));
            }
        }
 
        [Secure("RunAdHocReport")]
        [HttpPost, Route("AdHocReport/Export", Name = "ExportAdHocReport")]
        [ValidateAntiForgeryToken]
        public ActionResult ExportAdHocReport(string jsonReportData)
        {
            Response.Clear();
            Response.Buffer = false;
            Response.BufferOutput = false;
            Response.ContentType = "text/comma-separated-values";

            using (var reportingService = new AdHocReportingService(Current.User()))
            {
                AdHocReportViewModel report = JsonConvert.DeserializeObject<AdHocReportViewModel>(jsonReportData);
                ReportDefinition definition = report.CreateReportDefinition();

                StringBuilder fileName = new StringBuilder();
                fileName.AppendFormat("{0:yyyy-MM-dd}", DateTime.Now);
                if (!string.IsNullOrEmpty(report.Name))
                {
                    // when the report is retrieved from most recent
                    // then it will have a name, else needs a name
                    string tmpreportname = report.Name.Trim();
                    if (tmpreportname.IndexOf("Untitled") != -1)
                        tmpreportname = definition.ReportType.Name;
                    fileName.AppendFormat(" - {0}", tmpreportname);
                }
                System.IO.Path.GetInvalidPathChars().ForEach(c => fileName.Replace(c, '_'));
                fileName.Append(".csv");
                Response.AddHeader("Content-Disposition", string.Format("attachment; filename=\"{0}\"", fileName));
                Response.Flush();

                reportingService.ExportReportAsCsv(definition, CurrentUser, s =>
                {
                    Response.Write(s);
                    Response.Flush();
                });
            }

            Response.End();
            return new EmptyResult();
        }

        [Secure("RunAdHocReport")]
        [HttpGet, Route("Reports/AdHocReport/{reportTypeId}/{reportId?}", Name = "AdHocReport"), Title("Ad-Hoc Report")]
        public ActionResult AdHocReport(string reportTypeId, string reportId = null)
        {
            using (var reportingService = new AdHocReportingService(Current.User()))
            {
                AdHocReportViewModel report = null;
                if (string.IsNullOrWhiteSpace(reportId))
                {
                    ReportType reportType = reportingService.GetReportTypeById(reportTypeId);
                    report = new AdHocReportViewModel(reportType);
                }
                else
                {
                    ReportDefinition savedReport = reportingService.GetReportById(reportId);
                    report = new AdHocReportViewModel(savedReport);
                    report.Results = new AdHocReportResultViewModel(reportingService.RunReport(savedReport, null, null, DefaultAdHocReportRowLimit));
                }
                return View(report);
            }
        }

        [ChildActionOnly]
        [Secure("RunAdHocReport")]
        [HttpGet, Route("Reports/ReportCategories", Name = "ReportCategories")]
        public ActionResult ReportCategories(string reportTypeId, string reportId)
        {
            using (var reportingService = new AdHocReportingService(Current.User()))
            {
                ReportTypeViewModel reportType = new ReportTypeViewModel(reportingService.GetReportTypeById(reportTypeId))
                {
                    Categories = GetReportCategoriesForReportType(reportTypeId)
                };
                if (string.IsNullOrEmpty(reportId))
                    return PartialView("_CategorySidebar", reportType);

                ReportDefinition definition = reportingService.GetReportById(reportId);
                if (definition == null || definition.Fields == null)
                    return PartialView("_CategorySidebar", reportType);

                PopulateIncludedFields(reportType.Categories, definition);

                return PartialView("_CategorySidebar", reportType);

            }
        }

        [ChildActionOnly]
        [Secure("RunAdHocReport")]
        [HttpGet, Route("Reports/ReportHeader", Name = "ReportHeader")]
        public ActionResult ReportHeader(string reportId)
        {
            if (string.IsNullOrEmpty(reportId))
                throw new ArgumentNullException("reportId");

            using (var reportingService = new AdHocReportingService(Current.User()))
            {
                ReportDefinition definition = reportingService.GetReportById(reportId);
                List<AdHocReportCategory> categories = GetReportCategoriesForReportType(definition.ReportType.Id);
                PopulateIncludedFields(categories, definition);
                return PartialView("_ReportHeader", categories);
            }
        }

        [ChildActionOnly]
        [Secure("RunAdHocReport")]
        [HttpGet, Route("Reports/ReportFilters", Name = "ReportFilters")]
        public ActionResult ReportFilters(string reportTypeId, string reportId)
        {
            using (var reportingService = new AdHocReportingService(Current.User()))
            {
                AdHocReportFiltersViewModel filters = new AdHocReportFiltersViewModel()
                {
                    Categories = GetReportCategoriesForReportType(reportTypeId)
                };

                if (string.IsNullOrEmpty(reportId))
                    return PartialView("_Filters", filters);

                ReportDefinition definition = reportingService.GetReportById(reportId);
                if (definition == null || definition.Filters == null)
                    return PartialView("_Filters", filters);


                foreach (var filter in definition.Filters)
                {
                    ReportFilter metadataFilter = reportingService.GetFieldFilter(reportTypeId, filter.FieldName);
                    if (metadataFilter != null)
                    {
                        metadataFilter.FieldName = filter.FieldName;
                        metadataFilter.Operator = filter.Operator;
                        metadataFilter.Value = filter.Value;
                        filters.Filters.Add(new AdHocReportFilterViewModel(metadataFilter));
                    }
                }

                return PartialView("_Filters", filters);
            }
        }

        [Secure("RunAdHocReport")]
        [HttpGet, Route("Reports/FilterField", Name = "FilterField")]
        public ActionResult FilterField(string fieldName, string reportTypeId)
        {
            using (var reportingService = new AdHocReportingService(Current.User()))
            {
                AdHocReportFilterViewModel filter = new AdHocReportFilterViewModel(reportingService.GetFieldFilter(reportTypeId, fieldName));
                return PartialView("_Filter", filter);
            }
        }

        private List<AdHocReportCategory> GetReportCategoriesForReportType(string reportTypeId)
        {
            using (var reportingService = new AdHocReportingService(Current.User()))
            {
                List<ReportField> fields = reportingService.GetFields(reportTypeId);
                List<AdHocReportCategory> categories = new List<AdHocReportCategory>();
                foreach (var field in fields)
                {
                    var category = categories.FirstOrDefault(c => c.Name == field.Category) ?? categories.AddFluid(new AdHocReportCategory() { Name = field.Category, Fields = new List<AdHocReportField>() });
                    category.Fields.AddIfNotExists(new AdHocReportField(field), f => f.Name == field.Name);
                }
                return categories;
            }
        }

        private void PopulateIncludedFields(List<AdHocReportCategory> categories, ReportDefinition definition)
        {
            foreach (var category in categories)
            {
                foreach (var field in category.Fields)
                {
                    var savedField = definition.Fields.FirstOrDefault(f => f.Name == field.Name);
                    if (savedField == null)
                        continue;

                    field.IncludedInReport = true;
                    field.Order = savedField.Order;
                }
            }
        }

        [ChildActionOnly]
        [Secure("RunAdHocReport")]
        [HttpGet, Route("Reports/SavedReports")]
        public ActionResult SavedReports()
        {
            using (var reportingService = new AdHocReportingService(Current.User()))
            {
                AdHocReportListViewModel savedReports = new AdHocReportListViewModel()
                {
                    Title = "Saved Reports",
                    NoReportsMessage = "There are no saved reports",
                    NumberOfColumns = 2,
                    Reports = reportingService.GetSavedReportDefinitions()
                    .Select(r => new AdHocReportViewModel(r)).ToList()
                };
                return PartialView("_AdHocReportList", savedReports);
            }
        }

        [ChildActionOnly]
        [Secure("RunAdHocReport")]
        [HttpGet, Route("Reports/Sidebar")]
        public ActionResult ReportSidebar(string activeId)
        {
            using (var reportingService = new AdHocReportingService(Current.User()))
            {

                List<ReportTypeViewModel> types = reportingService.GetReportTypes()
                    .Select(rt => new ReportTypeViewModel(rt, rt.Id == activeId)).ToList();
                return PartialView("_Sidebar", types);
            }
        }

        [ChildActionOnly]
        [Secure("RunAdHocReport")]
        [HttpGet, Route("Reports/AdHocReportTypes")]
        public ActionResult AdHocReportTypes()
        {
            using (var reportingService = new AdHocReportingService(Current.User()))
            {
                List<ReportTypeViewModel> types = reportingService.GetReportTypes()
                    .Select(rt => new ReportTypeViewModel(rt)).ToList();
                return PartialView("_AdHocReportTypes", types);
            }
        }


        [ChildActionOnly]
        [Secure("RunAdHocReport")]
        [HttpGet, Route("Reports/MostRecentAdHocReports", Name = "MostRecentAdHocReports")]
        public ActionResult MostRecentAdHocReports()
        {
            using (var reportingService = new AdHocReportingService())
            {
                AdHocReportListViewModel mostRecentAdHocReports = new AdHocReportListViewModel()
                {
                    Title = "Most Recent Reports",
                    NoReportsMessage = "There are no recently run reports",
                    NumberOfColumns = 3,
                    Reports = reportingService.GetMostRecentlyRanReports(Current.Customer())
                    .Select(s => new AdHocReportViewModel(s)).ToList()
                };

                return PartialView("_AdHocReportList", mostRecentAdHocReports);
            }
        }

        [HttpGet]
        [Route("Reports/{reportId}/View")]
        public ActionResult ViewReport(string reportId)
        {
            ReportService reportService = new ReportService();
            BaseReport theReport = reportService.GetReport(reportId);
            ReportCriteria criteria = reportService.GetCriteria(reportId, CurrentUser);
            ReportViewModel report = new ReportViewModel(theReport, criteria);
            return View("Report", report);
        }

        [FileDownload, HttpPost]
        [Route("Reports/{reportId}/View", Name = "RunReport")]
        [ValidateAntiForgeryToken]
        public ActionResult RunReport(ReportViewModel viewModel, string command)
        {
            ReportService reportService = new ReportService();
            viewModel.BaseReport = reportService.GetReport(viewModel.ReportId);
            viewModel.Criteria = viewModel.ApplyToDataModel();
            if (command == "Export To CSV")
            {
                return Export(ExportType.CSV, viewModel.BaseReport, viewModel.Criteria);
            }
            else if (command == "Export To PDF")
            {
                return Export(ExportType.PDF, viewModel.BaseReport, viewModel.Criteria);
            }
            else if (command == "Run Report")
            {
                viewModel.Results = reportService.RunReport(viewModel.ReportId, viewModel.Criteria, CurrentUser);
                viewModel.IsReportRun = true;
                if (Request.IsAjax())
                    return Json(viewModel);

                return View("Report", viewModel);
            }

            throw new ArgumentException(string.Format("Command {0} is unsupported", command));
        }

        [HttpGet]
        [Route("Reports/Criteria", Name = "ReportCriteria")]
        public JsonResult GetCriteria(string reportId)
        {
            ReportCriteria reportCriteria = new ReportService().GetCriteria(reportId, CurrentUser);
            return Json(reportCriteria);
        }

        [HttpGet]
        [Route("Reports/GetReportList", Name = "GetReportList")]
        public JsonResult GetReportList(string category)
        {
            List<BaseReport> reports = new ReportService().GetReportsByMainCategory(category);
            return Json(reports);
        }

        [HttpGet]
        [Route("Reports/GetReportById", Name = "GetReportById")]
        public JsonResult GetReportById(string Id)
        {
            BaseReport report = new ReportService().GetReport(Id);
            return Json(report);
        }

        [HttpGet]
        [Route("Reports/GetOshaReportableYears/{employerId}", Name = "GetOshaReportableYears")]
        public JsonResult GetOshaReportableYearsByEmployer(string employerId)
        {
            IEnumerable<int> years = null;

            using (var svc = new WorkRelatedService(CurrentCustomer,CurrentEmployer,CurrentUser))
            {
                years = svc.GetOshaReportingYearsForEmployer().ToList();
            }

            return Json(years);
        }

        [HttpGet]
        [Route("Reports/GetOshaReportableLocations/{employerId}", Name = "GetOshaReportableLocations")]
        public JsonResult GetOshaReportableLocationsByEmployer(string employerId)
        {
            IEnumerable<Organization> locations = null;

            using (var svc = new WorkRelatedService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                locations = svc.GetOshaReportingLocationsForEmployer().ToList();
            }

            return Json(locations);
        }

        [Secure("RunOshaReport"), HttpGet, Route("Reports/OshaReports", Name = "OshaReports"), Title("OSHA Reports")]
        public ActionResult OshaReports()
        {
            var vm = new OshaReportViewModel();
            if (CurrentUser.Customer != null &&
                CurrentUser.Customer.HasFeature(Feature.MultiEmployerAccess) &&
                CurrentUser.Employers.Count > 1)
            {
                vm.EmployerId = string.Empty; // Do not default the employer, even though there will be multiple.
            }
            else
            {
                vm.EmployerId = CurrentUser.EmployerId; // this will be in a hidden input. Not a drop down, cuz there is only 1.
            }

            return View(vm);
        }

        [Secure("RunOshaReport"), HttpPost, Route("Reports/OshaReports"), Title("OSHA Reports")]
        [ValidateAntiForgeryToken]
        public ActionResult OshaReports(OshaReportViewModel oshaReportViewModel)
        {
            byte[] file = null;
            byte[] output = null;
            string fileName = string.Empty;
            oshaReportViewModel.Results = string.Empty;
            Osha300AFormPreviewModel previewModel = new Osha300AFormPreviewModel();

            if (!this.ModelState.IsValid)
            {

                if (Request.IsAjax())
                    return PartialView(oshaReportViewModel);
                else
                    return View(oshaReportViewModel);
            }

            try
            {
                List<byte[]> pdfs = new List<byte[]>();

                if (oshaReportViewModel.SelectedLocation == null)
                    oshaReportViewModel.SelectedLocation = new List<string>(){""};

                using (var svc = new WorkRelatedService())
                {
                    foreach (string location in oshaReportViewModel.SelectedLocation)
                    {
                        foreach (int year in oshaReportViewModel.SelectedYear)
                        {
                            switch (oshaReportViewModel.Form)
                            {
                                case "300":
                                    fileName = string.Format("{0} OSHA Report Form 300.pdf", year);
                                    file = svc.GenerateOSHAForm300(year, oshaReportViewModel.EmployerId, location);
                                    break;
                                case "300a":
                                    fileName = string.Format("{0} OSHA Report Form 300a.pdf", year);
                                    file = svc.GenerateOSHAForm300a(year, oshaReportViewModel.EmployerId, location);
                                    break;
                                case "300aefile":
                                    Osha300AFormPreview preview = svc.GenerateOshaForm300aPreview(year, oshaReportViewModel.EmployerId, location);
                                    if (preview != null)
                                    {
                                        previewModel.Osha300AFormPreviewList.Add(preview);
                                    }
                                    else
                                    {
                                        oshaReportViewModel.Results = "There are no OSHA reportable cases available for this Establishment.";
                                        return View(oshaReportViewModel);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            if (file != null)
                                pdfs.Add(file);
                        }
                    }

                    if (previewModel.Osha300AFormPreviewList.Any())
                    {
                        return View("Osha300APreview", previewModel);
                    }

                    if (pdfs.Any())
                        output= svc.GeneratePDF(pdfs.ToArray());
                }
            }
            catch (Exception ex)
            {
                // Show the error to the user.
                oshaReportViewModel.Results = ex.GetBaseException().Message;
                Log.Error(ex);
                if (Request.IsAjax())
                    return PartialView(oshaReportViewModel);
                else
                    return View(oshaReportViewModel);
            }

            if (output == null || output.Length == 0)
            {
                oshaReportViewModel.Results = "No Results Found.";
                if (Request.IsAjax())
                    return PartialView(oshaReportViewModel);
                else
                    return View(oshaReportViewModel);
            }

            return File(output, "application/pdf", fileName);
        }

        /// <summary>
        /// Handles the common logic of the export report
        /// </summary>
        /// <param name="exportType"></param>
        /// <param name="report"></param>
        /// <param name="criteria"></param>
        /// <returns></returns>
        private FileResult Export(ExportType exportType, BaseReport report, ReportCriteria criteria)
        {
            ReportService reportService = new ReportService();
            string fileType = null;
            string fileName = string.Format("{0:yyyy-MM-dd} - {1} {2}", DateTime.Today, report.Category, report.Name);
            byte[] output = null;
            switch (exportType)
            {
                case ExportType.PDF:
                    fileType = "text/pdf";
                    fileName = string.Concat(fileName, ".pdf");
                    output = reportService.GeneratePDF(report.Id, criteria, Current.User());
                    break;
                case ExportType.CSV:
                    fileType = "text/comma-separated-values";
                    fileName = string.Concat(fileName, ".csv");
                    output = reportService.GenerateCSV(report.Id, criteria, Current.User());
                    break;
            }


            return File(output, fileType, fileName);
        }
    }
}
