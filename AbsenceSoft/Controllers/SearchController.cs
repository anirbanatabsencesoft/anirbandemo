﻿using AbsenceSoft.Filters;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Models.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Web.Attributes;

namespace AbsenceSoft.Controllers
{
    [Secure]
    public class SearchController : BaseController
    {
        [Title("Search")]
        [Route("Search", Name = "Search")]
        [Secure("SearchCases")]
        public ActionResult Index(string searchText, string[] employers, string lastName, DateTime? dateOfBirth, string sortBy, int? page, int? pageSize, long? searchCount)
        {
            if (!page.HasValue)
            {
                page = 1;
            }

            if (!pageSize.HasValue)
            {
                pageSize = 30;
            }

            if (!searchCount.HasValue)
            {
                searchCount = 0;
            }

            ListCriteria criteria = new ListCriteria()
            {
                PageSize = pageSize.Value,
                PageNumber = page.Value,
                SortBy = sortBy
            };

            criteria.Set("SearchCount", searchCount.Value);
            criteria.Set("SearchText", searchText);
            criteria.Set("Employers", employers);
            criteria.Set("LastName", lastName);
            criteria.Set("DateOfBirth", dateOfBirth);

            using (var svc = new SearchService())
            {
                SearchResults results = svc.EmployeeCaseSearch(criteria);
                results.Criteria.Set("SearchCount", results.Total);
                SearchViewModel model = new SearchViewModel(results);
                
                if (Request.IsAjaxRequest())
                {
                    return PartialView("SearchList", model);
                }

                return View(model);
            }
        }
    }
}