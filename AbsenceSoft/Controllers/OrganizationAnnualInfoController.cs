﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Models.OrganizationAnnualInfos;
using AbsenceSoft.Web;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace AbsenceSoft.Controllers
{

    public class OrganizationAnnualInfoController : BaseController
    {

        /// <summary>
        /// Organizations the annual information bulk upload.
        /// </summary>
        /// <param name="upload">The upload.</param>
        /// <returns></returns>
        [Secure("EditOrganizations")]
        [HttpPost, Route("OrganizationAnnualInfo/{employerId}/BulkUpload", Name = "OrganizationAnnualInfoBulkUploadPost")]
        [ValidateApiAntiForgeryToken]
        public ActionResult OrganizationAnnualInfoBulkUpload(BulkUploadAnnualInfo upload)
        {
            try
            {
                using (var organizationAnnualInfoService = new OrganizationAnnualInfoService(CurrentCustomer, CurrentEmployer, CurrentUser))
                {
                    List<OrganizationAnnualInfo> validAnnualinfo = upload.AnnualInfos
                        .Where(c => c.IsValidForBulkUpload)
                        .Select(c => c.ApplyToDataModel()).ToList();

                    if (validAnnualinfo.Count == 0)
                    {
                        upload.Message = "No valid annual info found to upload.";
                        upload.Success = false;
                    }
                    else
                    {
                        organizationAnnualInfoService.BulkUploadAnnualInfo(validAnnualinfo);
                        upload.Success = true;
                        int invalidContacts = upload.AnnualInfos.Count - validAnnualinfo.Count;
                        StringBuilder successMessage = new StringBuilder();
                        successMessage.AppendFormat("{0} annual info have been successfully uploaded.", validAnnualinfo.Count);
                        if (invalidContacts > 0)
                        {
                            successMessage.AppendFormat(" {0} annual info had missing data and could not be uploaded.", invalidContacts);
                            upload.PartialSuccess = true;
                        }
                        upload.Message = successMessage.ToString();
                    }
                }
            }
            catch (AbsenceSoftException aex)
            {
                upload.Success = false;
                upload.Message = aex.Message;
            }

            return Json(upload);
        }

        /// <summary>
        /// Annuals the information entry.
        /// </summary>
        /// <param name="yearStart">The year start.</param>
        /// <returns></returns>
        [Secure("EditOrganizations")]
        [HttpGet, Route("OrganizationAnnualInfo/{employerId}/AnnualInfo", Name = "AnnualInfoEntry")]
        public ActionResult AnnualInfoEntry(int numberOfYearsBack = 0,bool saved =false)
        {
            
           using (var organizationAnnualInfoService = new OrganizationAnnualInfoService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                var years = organizationAnnualInfoService.GetYears(numberOfYearsBack).OrderBy(p => p).ToList();
                int startYear = 0, endYear = 0;
                if (years.Any())
                {
                    startYear = years[0];
                    endYear = years[years.Count - 1];
                }

                var savedAnnualInfo = organizationAnnualInfoService.GetSavedAnnualInfo(startYear,endYear);
                var organizationEntryViewModel = new OrganizationAnnualInfoEntryViewModel(savedAnnualInfo, years, numberOfYearsBack);
                if (saved)
                    organizationEntryViewModel.Success = true;
                return View(organizationEntryViewModel);
                
            }
        }


        /// <summary>
        /// Annuals the information entry save.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns></returns>
        [Secure("EditOrganizations")]
        [HttpPost, Route("OrganizationAnnualInfo/{employerId}/AnnualInfo", Name = "AnnualInfoEntrySave")]
        [ValidateAntiForgeryToken]
        public ActionResult AnnualInfoEntrySave(OrganizationAnnualInfoEntryViewModel viewModel)
        {
            try
            {
                using (var organizationAnnualInfoService = new OrganizationAnnualInfoService(CurrentCustomer, CurrentEmployer, CurrentUser))
                {
                    List<OrganizationAnnualInfo> validAnnualInfos = new List<OrganizationAnnualInfo>();
                    foreach (OrganizationAnnualInfoLocationViewModel location in viewModel.OrganizationAnnualInfoLocations)
                    {
                        validAnnualInfos.AddRange(location.OrganizationAnnualInfoYears.Select(c => c.ApplyToDataModel()).ToList());
                    }

                    if (validAnnualInfos.Count == 0)
                    {
                        viewModel.Message = "No valid annual info found against selected organization";
                        viewModel.Success = false;
                    }
                    else
                    {
                        organizationAnnualInfoService.BulkUploadAnnualInfo(validAnnualInfos);
                        viewModel.Success = true;
                        StringBuilder successMessage = new StringBuilder();
                        successMessage.AppendFormat("{0} Annual info have been successfully uploaded.", validAnnualInfos.Count);
                        viewModel.Message = successMessage.ToString();
                    }
                }
            }
            catch (AbsenceSoftException aex)
            {
                viewModel.Success = false;
                viewModel.Message = aex.Message;
            }
            string lUrl = Url.AdministrationRouteUrl("AnnualInfoEntry")+"?numberOfYearsBack=0&saved=True";
            return RedirectViaJavaScript(lUrl);


           

    

        }
    

    }
}