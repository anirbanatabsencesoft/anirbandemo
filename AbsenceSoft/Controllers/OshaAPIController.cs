﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Models.Reports;
using AbsenceSoft.Models.OshaApi;
using AbsenceSoft.Web.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AbsenceSoft.Logic.WorkRelated;
using AbsenceSoft.Data.Customers;


namespace AbsenceSoft.Controllers
{
    public class OshaAPIController : BaseController
    {
        /// <summary>
        /// Get Employer API Token
        /// </summary>
        /// <param name="employerId"></param>
        /// <returns></returns>
        [Secure("EditOshaApiToken")]
        [Route("OshaEFile/{employerId?}", Name = "OshaEFile"), Title("OSHA E-File")]
        public ActionResult OshaEFile(string employerId)
        {
            using (var oshaEfs = new OshaEFilingService())
            {
                OshaApiTokenViewModel model = new OshaApiTokenViewModel(oshaEfs.GetToken(employerId));
                model.EmployerId = employerId;
                return View(model);
            }
        }

        /// <summary>
        /// Save API Token to Employer table
        /// </summary>
        /// <param name="employerId"></param>
        /// <param name="model"></param>
        /// <returns></returns>

        [Secure("EditOshaApiToken")]
        [HttpPost, Route("OshaEFile/Save/{employerId?}", Name = "Save"), Title("Save Token")]
        public ActionResult Save(string employerId, OshaApiTokenViewModel model)
        {
            using (var administrationService = new AdministrationService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                if (administrationService.CustomerHasFeature(Feature.WorkRelatedReporting))
                {
                    if (ModelState.IsValid)
                    {
                        Employer employer = Employer.GetById(employerId);
                        try
                        {
                            employer.OshaApiToken = model.ApiToken;
                            employer.Save();
                            model.Saved = true;
                        }
                        catch (Exception ex)
                        {
                            model.Saved = false;
                            ModelState.AddModelError("", "There was an error when attempting to update the API Token.");
                        }

                    }
                }
            }
            return PartialView("OshaEFile", model);
        }
    }

}