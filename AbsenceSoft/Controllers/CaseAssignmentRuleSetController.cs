﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Models.Cases;
using AbsenceSoft.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Xml;

namespace AbsenceSoft.Controllers
{
    public class CaseAssignmentRuleSetController : BaseController
    {
        [HttpGet, Route("CaseAssignmentRuleSet/List/{employerId?}", Name = "ViewCaseAssignmentRules")]
        public ActionResult CaseAssignmentRuleSetList(string employerId)
        {
            return View();
        }

        [HttpPost, Route("CaseAssignmentRuleSet/List", Name = "ListCaseAssignmentRuleSets")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListCaseAssignmentRuleSets(ListCriteria criteria)
        {
            using (var caseService = new CaseService())
            {
                var assignmentRuleSetTypes = caseService.ListCaseAssignmentRuleSets(criteria);
                IEnumerable<ListResult> orderedRuleSets = assignmentRuleSetTypes.Results.OrderBy(dict => dict["CaseAssignmentRuleOrder"]).ToList();

                var i = 0;
                orderedRuleSets = orderedRuleSets.Select(dict => 
                {
                    if (dict.Get<int>("CaseAssignmentRuleOrder") != i++)
                    {
                        dict.Set("CaseAssignmentRuleOrder", i - 1);                        
                    }
                    return dict;
                });

                assignmentRuleSetTypes.Results = orderedRuleSets;

                return PartialView(assignmentRuleSetTypes);
            }
        }

        [HttpPost, Route("CaseAssignmentRuleSet/SaveRulesetOrder", Name = "SaveRulesetOrder")]
        [ValidateApiAntiForgeryToken]
        public JsonResult SaveRulesetOrder(List<CaseAssignmentRuleSetOrderViewModel> model)
        {            
            model.ForEach(m => {
                var caseAssignmentRuleSet = CaseAssignmentRuleSet.GetByCode(m.Code, CurrentCustomer.Id);
                caseAssignmentRuleSet.CaseAssignmentRuleOrder = m.CaseAssignmentRuleOrder;
                caseAssignmentRuleSet.Save();
            });
            return Json(new { success = true });
        }

        [HttpGet, Route("CaseAssignmentRuleSet/{customerId}/Edit/{caseAssignmentRuleSetId?}", Name = "CaseAssignmentRuleSetEdit")]
        public ActionResult CaseAssignmentRuleSetEdit(string customerId, string caseAssignmentRuleSetId = null)
        {
            CaseAssignmentRuleSetViewModel caseAssignmentRuleSetViewModel = null;
            if (caseAssignmentRuleSetId != null)
            {
                CaseAssignmentRuleSet caseAssignmentRuleSet = CaseAssignmentRuleSet.GetById(caseAssignmentRuleSetId);
                caseAssignmentRuleSetViewModel = new CaseAssignmentRuleSetViewModel(caseAssignmentRuleSet);
            }
            else
            {
                caseAssignmentRuleSetViewModel = new CaseAssignmentRuleSetViewModel(customerId);
            }
            GetRuleConfigurationOptions(caseAssignmentRuleSetViewModel);
            return View(caseAssignmentRuleSetViewModel);
        }

        [HttpPost, Route("CaseAssignmentRuleSet/{customerId}/Save", Name = "CaseAssignmentRuleSetSave")]
        [ValidateApiAntiForgeryToken]
        public ActionResult SaveCaseAssignmentRuleSet(string customerId, CaseAssignmentRuleSetViewModel caseAssignmentRuleSetViewModel)
        {
            if (!ModelState.IsValid)
                return PartialView(caseAssignmentRuleSetViewModel);            

            CaseAssignmentRuleSet caseAssignmentRuleSet = null;           
            if (!string.IsNullOrEmpty(caseAssignmentRuleSetViewModel.Id))
            {
                if (CaseAssignmentRuleSet.AsQueryable().Where(c => c.Id != caseAssignmentRuleSetViewModel.Id && c.Code == caseAssignmentRuleSetViewModel.Code).Any())
                {
                    caseAssignmentRuleSetViewModel.GetCaseAssigneeTypes();
                    ModelState.AddModelError(string.Empty, "Case Assignment Rule Set Code is already existing");
                    if (Request.IsAjaxRequest())
                        return PartialView("CaseAssignmentRuleSetEdit", caseAssignmentRuleSetViewModel);

                    return View("CaseAssignmentRuleSetEdit", caseAssignmentRuleSetViewModel);
                }
                else
                {
                    caseAssignmentRuleSet = CaseAssignmentRuleSet.GetById(caseAssignmentRuleSetViewModel.Id);
                }
            }
            else
            {
                if (CaseAssignmentRuleSet.GetByCode(caseAssignmentRuleSetViewModel.Code, customerId) != null)
                {
                    caseAssignmentRuleSetViewModel.GetCaseAssigneeTypes();
                    ModelState.AddModelError(string.Empty, "Case Assignment Rule Set Code is already existing");
                    if (Request.IsAjaxRequest())
                        return PartialView("CaseAssignmentRuleSetEdit", caseAssignmentRuleSetViewModel);

                    return View("CaseAssignmentRuleSetEdit", caseAssignmentRuleSetViewModel);
                }
                else
                {
                    caseAssignmentRuleSet = new CaseAssignmentRuleSet();
                }
            }

            caseAssignmentRuleSet = caseAssignmentRuleSetViewModel.ApplyToDataModel(caseAssignmentRuleSet);
            if (string.IsNullOrWhiteSpace(caseAssignmentRuleSetViewModel.Id))
            {
                if (CaseAssignmentRuleSet.AsQueryable().Any())
                {
                    int nextRulesetOrder = CaseAssignmentRuleSet.AsQueryable().Where(c => c.CustomerId == customerId && c.IsDeleted == false).Max(o => o.CaseAssignmentRuleOrder);
                    nextRulesetOrder = nextRulesetOrder == 0 ? 0 : nextRulesetOrder + 1;
                    caseAssignmentRuleSet.CaseAssignmentRuleOrder = nextRulesetOrder;
                }
                else
                {
                    caseAssignmentRuleSet.CaseAssignmentRuleOrder = 0;
                }
            }
            caseAssignmentRuleSet.Save();

            return RedirectViaJavaScript(Url.Action("CaseAssignmentRuleSetList", new { customerId }));
        }

        [HttpDelete, Route("CaseAssignmentRuleSet/{customerId}/Delete/{caseAssignmentRuleSetId}", Name = "DeleteCaseAssignmentRuleSet")]
        public ActionResult DeleteCaseAssignmentRuleSet(string customerId, string caseAssignmentRuleSetId)
        {
            try
            {
                CaseAssignmentRuleSet caseAssignmentRuleSet = CaseAssignmentRuleSet.GetById(caseAssignmentRuleSetId);
                int nextRulesetOrder = caseAssignmentRuleSet.CaseAssignmentRuleOrder;
                caseAssignmentRuleSet.Delete();
                var caseAssignmentRuleSets = CaseAssignmentRuleSet.AsQueryable().Where(c => c.CustomerId == customerId && c.CaseAssignmentRuleOrder > nextRulesetOrder && c.IsDeleted == false).ToList();
                
                if (caseAssignmentRuleSets.Any())
                {
                    caseAssignmentRuleSets.ForEach(c => 
                    {
                        c.CaseAssignmentRuleOrder = c.CaseAssignmentRuleOrder - 1;
                    });
                    
                    caseAssignmentRuleSets.ForEach(c => { c.Save(); });
                }

                return RedirectViaJavaScript(Url.Action("CaseAssignmentRuleSetList", new { customerId = Customer.Current.Id }));
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to delete Case Assignment Rule Set");
            }
        }

        [HttpGet, Route("CaseAssignmentRuleSet/CreateCaseAssignmentRule", Name = "CreateCaseAssignmentRule")]
        public ActionResult CreateCaseAssignmentRule()
        {
            CaseAssignmentRuleViewModel model = new CaseAssignmentRuleViewModel();
            List<CaseAssignmentRuleViewModel> list = new List<CaseAssignmentRuleViewModel> { model };
            GetRuleConfigurationOptions(new CaseAssignmentRuleSetViewModel() { CaseAssignmentRules = list });
            return PartialView("CaseAssignmentRule", model);
        }
        [NonAction]
        public Dictionary<string, string> GetRuleTypes()
        {

            Dictionary<string, string> ruleTypes = new Dictionary<string, string>()
            {
                { string.Empty, "Select One"},
                { "1", "Employer"},
                { "2", "Work State"},
                { "3", "Case Type"},
                { "4", "Policy"},
                { "5", "Policy Type"},
                { "6", "Alpha Split"},
                { "7", "Rotary"},
                { "8", "Self"},
                { "9", "Unassigned"}

            };

            if (CurrentCustomer.HasFeature(Feature.PolicyCrosswalk))
            {
                ruleTypes.Add("10", "Policy Crosswalk");
            }
            if (CurrentCustomer.HasFeature(Feature.AdminOrganizations))
            {
                ruleTypes.Add("11", "Office Location");
            }
            if (CurrentCustomer.HasFeature(Feature.ADA))
            {
                ruleTypes.Add("12", "Accommodation Type");
            }

            return ruleTypes;
        }

        private void GetRuleConfigurationOptions(CaseAssignmentRuleSetViewModel ruleSetModel)
        {
            Dictionary<string, string> employers = new Dictionary<string, string>();
            List<CustomField> customFields = new List<CustomField>();
            using (EmployerService empService = new EmployerService())
            {
                empService.GetEmployersForCustomer(CurrentCustomer.Id).OrderBy(emp => emp.Name).ForEach(emp => employers.Add(emp.Id, emp.Name));
                customFields = empService.GetCustomFieldsForCustomer();
            }

            List<Policy> policyList = Policy.AsQueryable().Where(policy => policy.CustomerId == CurrentCustomer.Id || policy.CustomerId == null).ToList();
            Dictionary<string, string> policies = new Dictionary<string, string>();
            policyList.OrderBy(policy => policy.Code)
                .ForEach(policy =>
                policies.AddIf(policy.Code, policy.Code, policies.ContainsKey(policy.Code) == false));

            Dictionary<string, string> caseTypes = Enum.GetValues(typeof(CaseType))
               .Cast<CaseType>()
               .ToDictionary(t => ((int)t).ToString(), t => t.ToString());

            Dictionary<string, string> policyTypes = Enum.GetValues(typeof(PolicyType))
               .Cast<PolicyType>()
               .ToDictionary(t => ((int)t).ToString(), t => t.ToString());

            Dictionary<string, string> caseAssigmentTypes = Enum.GetValues(typeof(CaseAssigmentType))
               .Cast<CaseAssigmentType>()
               .ToDictionary(t => ((int)t).ToString(), t => t.ToString());
            caseAssigmentTypes = (new Dictionary<string, string> { { string.Empty, "Select One" } }).Concat(caseAssigmentTypes).ToDictionary(k => k.Key, v => v.Value);

            List<Team> caseAssigmentTeams = Team.AsQueryable().Where(team => team.CustomerId == CurrentCustomer.Id).ToList();
            Dictionary<string, string> teams = new Dictionary<string, string>
            {
                { string.Empty, "Select One" }
            };
            caseAssigmentTeams
                .OrderBy(team => team.Name)
                .ForEach(team => teams.AddIf(team.Id, team.Name, string.IsNullOrWhiteSpace(team.Name) == false && !teams.ContainsKey(team.Id)));

            List<Data.Security.User> caseAssigmentUsers;
            using (CaseService caseSvc = new CaseService(CurrentCustomer, null, CurrentUser))
            {
                caseAssigmentUsers = caseSvc.GetPossibleCaseAssigneeUsers();
            }
            Dictionary<string, string> users = new Dictionary<string, string>
            {
                { string.Empty, "Select One" }
            };
            caseAssigmentUsers
                .OrderBy(user => user.DisplayName)
                .ForEach(user => users.AddIf(user.Id, user.DisplayName, !users.ContainsKey(user.Id)));

            Dictionary<string, string> workStates = new Dictionary<string, string>();
            XmlDocument doc = new XmlDocument();
            doc.Load(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/states.xml"));
            foreach (XmlNode node in doc.SelectNodes("//state"))
            {
                string value = node.Attributes["code"].Value;
                workStates.Add(value, value);
            }
            List<OfficeLocationViewModel> officeLocations = new List<OfficeLocationViewModel>();

            if (CurrentCustomer.HasFeature(Feature.AdminOrganizations))
            {
                var organizations = Organization.AsQueryable().Where(o => o.TypeCode == OrganizationType.OfficeLocationTypeCode).ToList();
                officeLocations = organizations.Select(o => new OfficeLocationViewModel(o)).ToList();
            }
            Dictionary<string, string> ruleTypes = GetRuleTypes();
            if (customFields.Count>0)
            {
                foreach (var item in customFields)
                {
                    if (!ruleTypes.ContainsKey(item.Code))
                    {
                        ruleTypes.Add(item.Code, item.Name);
                    }
                }
            }

            Dictionary<string, string> accomTypes = new Dictionary<string, string>();
            if (CurrentCustomer.HasFeature(Feature.ADA))
            {
                using (AccommodationService accomService = new AccommodationService(CurrentCustomer, CurrentEmployer, CurrentUser))
                {
                    var accomodationTypes = accomService.GetBaseAccommodationTypes();
                    if (accomodationTypes.Any())
                    {
                        foreach (var item in accomodationTypes)
                        {
                            if (item.Code !=null && !accomTypes.ContainsKey(item.Code))
                            {
                                accomTypes.Add(item.Code, $"{item.Name} ({item.Code})");
                            }
                        }
                    }
                }
            }

            foreach (CaseAssignmentRuleViewModel model in ruleSetModel.CaseAssignmentRules)
            {
                model.Employers = employers;
                model.WorkStates = workStates;
                model.Policies = policies;
                model.PolicyTypes = policyTypes;
                model.CaseTypes = caseTypes;
                model.AssignmentTypes = caseAssigmentTypes;
                model.AssignmentTeams = teams;
                model.AssignmentUsers = users;
                model.RuleTypes = ruleTypes;
                if (CurrentCustomer.HasFeature(Feature.AdminOrganizations))
                {
                    model.OfficeLocations = officeLocations;

                }
                model.CustomFields = customFields;
                model.AccommodationTypes = accomTypes;
            }
            ruleSetModel.Employers = employers;
            ruleSetModel.WorkStates = workStates;
            ruleSetModel.Policies = policies;
            ruleSetModel.PolicyTypes = policyTypes;
            ruleSetModel.CaseTypes = caseTypes;
            ruleSetModel.AssignmentTypes = caseAssigmentTypes;
            ruleSetModel.AssignmentTeams = teams;
            ruleSetModel.AssignmentUsers = users;
            ruleSetModel.RuleTypes = ruleTypes;
            if (CurrentCustomer.HasFeature(Feature.AdminOrganizations))
            {
                ruleSetModel.OfficeLocations = officeLocations;

            }
            ruleSetModel.CustomFields = customFields;
            ruleSetModel.AccommodationTypes = accomTypes;
        }
    }
}
