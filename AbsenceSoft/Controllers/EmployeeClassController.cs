﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Models.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Web.Filters;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Data.Customers;

namespace AbsenceSoft.Controllers
{
    [Secure]
    public class EmployeeClassController : BaseController
    {
        [Title("View Employee Classes")]
        [HttpGet, Route("EmployeeClass/List/{employerId?}", Name = "ViewEmployeeClasses")]
        public ActionResult ViewEmployeeClasses(string employerId)
        {
            ViewBag.EmployerId = employerId;
            return View();
        }

        [HttpPost, Route("EmployeeClass/List", Name = "ListEmployeeClasses")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListEmployeeClasses(ListCriteria criteria)
        {
            using (var employeeClassService = new EmployeeClassService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                ListResults results = employeeClassService.EmployeeClassList(criteria);
                return PartialView(results);
            }
        }

        [Title("Edit Employee Class")]
        [HttpGet, Route("EmployeeClass/Edit/{employerId?}", Name = "EditEmployeeClass")]
        public ActionResult EditEmployeeClass(string employerId, string id)
        {
            using (var employeeClassService = new EmployeeClassService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                EmployeeClassViewModel viewModel = new EmployeeClassViewModel(employeeClassService.GetEmployeeClassById(id));
                return View(viewModel);
            }
        }

        [Title("Edit Employee Class")]
        [HttpPost, Route("EmployeeClass/Save", Name = "SaveEmployeeClasses")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveEmployeeClass(EmployeeClassViewModel employeeClass)
        {
            if (!ModelState.IsValid)
                return PartialView("EditEmployeeClass", employeeClass);

            using (var employeeClassService = new EmployeeClassService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                EmployeeClass eClass = employeeClass.ApplyToDataModel(employeeClassService.GetEmployeeClassById(employeeClass.Id));
                employeeClassService.SaveEmployeeClass(eClass);
            }

            return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewEmployeeClasses"));
        }

        [HttpDelete, Route("EmployeeClass/Delete/{employerId?}", Name = "DeleteEmployeeClass")]
        public ActionResult DeleteEmployeeClass(string employerId, string id)
        {
            using (var employeeClassService = new EmployeeClassService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                EmployeeClass eClass = employeeClassService.GetEmployeeClassById(id);
                if (eClass.IsCustom)
                    employeeClassService.DeleteEmployeeClass(eClass);

                return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewEmployeeClasses"));
            }
        }

        [HttpPost, Route("EmployeeClass/ToggleEmployeeClassSuppression/{employerId?}", Name = "ToggleEmployeeClassSuppression")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ToggleEmployeeClassSuppression(EmployeeClassViewModel employeeClass)
        {
            if(String.IsNullOrWhiteSpace(employeeClass.Code))
            {
                return Json(employeeClass);
            }
            using (var employeeClassService = new EmployeeClassService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                var savedEmployeeClass = employeeClassService.ToggleEmployeeClassByCode(employeeClass.Code);
                return Json(employeeClass);
            }
        }
    }
}