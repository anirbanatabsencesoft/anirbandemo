﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Jobs;
using AbsenceSoft.Models.Employees;
using AbsenceSoft.Models.Jobs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    [Secure(Feature.JobConfiguration)]
    public class JobsController : BaseController
    {
        // GET: Job
        [Secure("EditJobs")]
        [HttpGet, Route("Jobs/{employerId}/List", Name = "ListJobs")]
        public ActionResult ListJobs(string employerId)
        {
            return View();
        }

        [Secure("EditJobs")]
        [HttpPost, Route("Jobs/List", Name="JobsList")]
        [ValidateApiAntiForgeryToken]
        public ActionResult JobsList(ListCriteria criteria)
        {
            using (var jobService = new JobService(CurrentUser))
            {
                var jobs = jobService.JobList(criteria);
                return PartialView(jobs);
            }
        }

        [Secure("EditJobs")]
        [HttpGet, Route("Jobs/{employerId}/Edit/{jobId?}", Name = "EditJob")]
        public ActionResult EditJob(string employerId, string jobId)
        {
            JobViewModel job = null;
            if (!string.IsNullOrEmpty(jobId))
            {
                job = new JobViewModel(Job.GetById(jobId));
            }
            else
            {
                job = new JobViewModel();
                job.BuildRequirements(employerId);
            }


            return View(job);
        }

        [Secure("EditJobs")]
        [HttpPost, Route("Jobs/{employerId}/Save", Name = "SaveJob")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveJob(string employerId, JobViewModel theJob)
        {
            if (!ModelState.IsValid)
            {
                theJob.RebuildViewModel();
                return PartialView("EditJob", theJob);
            }
                

            using (var jobService = new JobService(CurrentUser))
            {
                Job job = theJob.ApplyToDataModel(jobService.GetJob(theJob.Id));
                jobService.SaveJob(job);
            }
            

            return RedirectViaJavaScript(Url.RouteUrl("ListJobs", new { employerId = employerId }));
        }

        [Secure("EditJobs")]
        [HttpDelete, Route("Jobs/{employerId}/Delete/{jobId}", Name = "DeleteJob")]
        public ActionResult DeleteJob(string employerId, string jobId)
        {
            using (var jobService = new JobService(CurrentUser))
            {
                jobService.DeleteJob(jobId);
            }

            return RedirectViaJavaScript(Url.RouteUrl("ListJobs", new { employerId = employerId }));
        }

        [Secure("ViewJobs")]
        [HttpGet, Route("Jobs/{employerId}/View/{code}/{employeeId}", Name="ViewJob")]
        public ActionResult ViewJob(string employerId, string code, string employeeId)
        {
            ViewBag.EmployeeId = employeeId;
            JobViewModel job = new JobViewModel(Job.GetByCode(code, CurrentCustomer.Id, employerId));
            return View(job);
        }

        [Secure("JobAssignment")]
        [HttpGet, Route("Jobs/{employeeId}/Assignment/{caseId?}", Name="JobAssignment")]
        public ActionResult JobAssignment(string employeeId, string caseId)
        {
            using (var jobService = new JobService(CurrentUser))
            using (var demandService = new DemandService(CurrentUser))
            {
                Employee emp = Employee.GetById(employeeId);
                EmployeeJob job = jobService.GetCurrentEmployeeJob(emp);
                List<JobEvaluationResult> evaluatedJobs = demandService.EvaluateJobsForEmployee(emp, caseId);
                JobAssignmentViewModel jobAssignment = new JobAssignmentViewModel(employeeId, caseId, job, evaluatedJobs);
                return View(jobAssignment);
            }
        }

        [Secure("JobAssignment")]
        [HttpPost, Route("Jobs/Assignment/", Name="SetJob")]
        [ValidateAntiForgeryToken]
        public ActionResult SetJob(NewJobViewModel newJob)
        {
            if(!ModelState.IsValid)
                return PartialView(newJob);

            using (var jobService = new JobService(CurrentUser))
            {
                jobService.ChangeEmployeeJobs(newJob.EmployeeId, newJob.JobCode, newJob.StartDate.HasValue ? new DateRange(newJob.StartDate.Value, newJob.EndDate) : DateRange.Null, newJob.JobTitle, newJob.CaseId);
            }

            if (!string.IsNullOrEmpty(newJob.CaseId))
                return RedirectViaJavaScript(Url.RouteUrl("ViewCase", new { caseId = newJob.CaseId }));

            return RedirectViaJavaScript(Url.RouteUrl("ViewEmployee", new { employeeId = newJob.EmployeeId }));
        }
    }
}