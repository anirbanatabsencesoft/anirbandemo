﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Customers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Filters;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    public class CustomersController : BaseController
    {
        /// <summary>
        /// Gets a customer image, like a logo, etc. which is generally publicly availble
        /// for landing pages, etc. BUT, the customer URL must match, if one exists.
        /// If the mime type of the file is not an image type, returns a 404.
        /// </summary>
        /// <param name="customerId">The customer id that the file belongs to</param>
        /// <param name="fileId">The id of the file representing the image</param>
        /// <returns>The image file or a 404 w/ appropriate message</returns>
        [Route("Customers/{customerId}/Image/{fileId}", Name = "CustomerImage")]
        public ActionResult CustomerImage(string customerId, string fileId)
        {
            if (string.IsNullOrWhiteSpace(customerId) || string.IsNullOrWhiteSpace(fileId))
                return new HttpNotFoundResult("Image not found");

            Customer cust = Customer.GetById(customerId);

            if (cust != null)
            {
                if (fileId != cust.LogoId)
                {
                    fileId = cust.LogoId;
                }
            }

            using (CustomerFileService svc = new CustomerFileService())
            {
                try
                {
                    string url = svc.DownloadFile(customerId, fileId);
                    if (!string.IsNullOrWhiteSpace(url))
                        return Redirect(url);
                    else
                        return GetDefaultLogoImage();
                }
                catch (AbsenceSoftException softEx)
                {
                    return new HttpNotFoundResult("Image not found; " + softEx.Message);
                }
            }
        }


        /// <summary>
        /// Gets a customer file, the current user's customer Id MUST MATCH; otherwise
        /// will return a 404.
        /// </summary>
        /// <param name="customerId">The customer id that the file belongs to</param>
        /// <param name="fileId">The id of the file representing the file</param>
        /// <returns>The file or a 404 w/ appropriate message</returns>
        [Route("Customers/{customerId}/File/{fileId}", Name = "CustomerFile")]
        public ActionResult CustomerFile(string customerId, string fileId)
        {
            using (CustomerFileService svc = new CustomerFileService())
            {
                try
                {
                    string url = svc.DownloadFile(customerId, fileId);
                    if (!string.IsNullOrWhiteSpace(url))
                        return Redirect(url);
                }
                catch (AbsenceSoftException softEx)
                {
                    return new HttpNotFoundResult("Image not found; " + softEx.Message);
                }
            }

            return new HttpNotFoundResult("File not found");
        }


        /// <summary>
        /// Uploads a file for a customer using the HttpPostedFileBase and returns a JSON result with
        /// the resulting file information, Id, etc. for displiay purposes back in UI land.
        /// </summary>
        /// <param name="customerId">The customer id that the file belongs to</param>
        /// <param name="file">The file to upload and save</param>
        /// <returns>A JSON result with the final result and file information</returns>
        //[HttpPost]
        [Route("Customers/{customerId}/File/Create", Name = "UploadCustomerFile")]
        [ValidateApiAntiForgeryToken]
        public JsonResult UploadCustomerFile(string customerId, HttpPostedFileBase customerLogo)
        {
            if (string.IsNullOrWhiteSpace(customerId))
                throw new ArgumentNullException("customerId");
            if (customerLogo == null || customerLogo.ContentLength <= 0)
                throw new ArgumentNullException("file");

            Customer cust = Customer.Current;
            if (cust == null)
                throw new ArgumentException("Current customer not found");
            if (cust.Id != customerId)
                throw new AbsenceSoftException("Customer mismatch");

            return Json(new CustomerFileService().Using(s => s.UploadFile(customerId, customerLogo)));
        }

        private FilePathResult GetDefaultLogoImage()
        {
            string filepath = Server.MapPath("~/Content/Images/~/Content/Images/AbsenceSoft-logo-200.png");
            return File(filepath, "image/png");
        }

        private FileContentResult GetCustomerNameImage(string name)
        {
            byte[] file = null;
            using (Bitmap img = new Bitmap(364, 87))
            {
                img.MakeTransparent();
                using (Graphics g = Graphics.FromImage(img))
                {
                    g.SmoothingMode = SmoothingMode.AntiAlias;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    g.TextRenderingHint = TextRenderingHint.AntiAlias;

                    RectangleF rect = new RectangleF(0, 0, 364, 87);
                    SolidBrush brush = new SolidBrush(Color.FromArgb(255, 131, 0));
                    Font helveticaNeue = new Font("Helvetica Neue", emSize: 1);

                    g.DrawString(name, helveticaNeue, brush, rect);

                    g.Flush();
                }
                using (MemoryStream stream = new MemoryStream())
                {
                    img.Save(stream, ImageFormat.Png);
                    stream.Position = 0;
                    file = stream.ToArray();
                }
            }
            return File(file, "image/png");
        }

		[HttpGet]
        [Route("Customers/GetEmployerFeatures", Name = "GetEmployerFeatures")]
		public JsonResult GetEmployerFeatures(string employerId)
		{
			using (var svc = new EmployerService())
			{
				var list = svc.GetFeatures(CurrentUser.CustomerId, employerId);

                if (CurrentUser.Employers.Count > 1)
                {
                    list.Remove(Data.Enums.Feature.MultiEmployerAccess);
                    list.Add(Data.Enums.Feature.MultiEmployerAccess, true);
                }

				return Json(list);
			}
		}
    }
}