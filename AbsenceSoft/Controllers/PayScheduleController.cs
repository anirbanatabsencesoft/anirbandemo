﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    [Secure]
    public class PayScheduleController : BaseController
    {
        // GET: PaySchedule
        [HttpGet, Route("PaySchedule/{employerId}", Name = "ViewPaySchedules")]
        public ActionResult Index(string employerId)
        {
            ViewBag.EmployerId = employerId;
            return View();
        }

        [HttpGet, Route("PaySchedule/{employerId}/List")]
        public JsonResult PayScheduleList(string employerId)
        {
            try
            {
                List<PaySchedule> employerPaySchedules = new PayScheduleService().PayScheduleForEmployer(employerId);
                Employer e = Employer.GetById(employerId);
                PaySchedule defaultPaySchedule = employerPaySchedules.FirstOrDefault(eps => eps.Id == e.DefaultPayScheduleId);
                if (defaultPaySchedule != null)
                    defaultPaySchedule.Default = true;

                return Json(employerPaySchedules);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to retrieve pay schedules for employer");
            }

        }

        [HttpGet, Route("PaySchedule/{employerId}/Edit/{payScheduleId?}")]
        public ActionResult EditPaySchedule(string employerId, string payScheduleId)
        {
            ViewBag.EmployerId = employerId;
            ViewBag.PayScheduleId = payScheduleId;
            return View();
        }

        [HttpGet, Route("PaySchedule/{employerId}/Get/{payScheduleId}")]
        public JsonResult GetPaySchedule(string employerId, string payScheduleId)
        {
            try
            {
                List<PaySchedule> employerPaySchedules = new PayScheduleService().PayScheduleForEmployer(employerId);
                PaySchedule scheduleToEdit = employerPaySchedules.FirstOrDefault(eps => eps.Id == payScheduleId);
                if (scheduleToEdit != null)
                    scheduleToEdit.Default = scheduleToEdit.Employer.DefaultPayScheduleId == scheduleToEdit.Id;
                return Json(scheduleToEdit);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to retrieve pay schedule for employer");
            }

        }

        [HttpPost, Route("PaySchedule/Save/")]
        [ValidateApiAntiForgeryToken]
        public JsonResult SavePaySchedule(PaySchedule schedule)
        {
            try
            {
                PayScheduleService pss = new PayScheduleService();
                pss.PayscheduleUpdatesert(schedule);
                return Json(schedule);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to save pay schedule");
            }
        }

        [HttpDelete, Route("PaySchedule/{employerId}/Delete/{payScheduleId}")]
        [ValidateApiAntiForgeryToken]
        public JsonResult DeletePaySchedule(string employerId, string payScheduleId)
        {
            try
            {
                PayScheduleService pss = new PayScheduleService();
                PaySchedule schedule = pss.PayScheduleForEmployer(employerId).FirstOrDefault(ps => ps.Id == payScheduleId);

                if (schedule == null)
                    throw new AbsenceSoftException("Unable to find specified pay schedule");

                pss.PayScheduleDelete(schedule);
                return Json(true);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to delete pay schedule");
            }
        }

    }
}