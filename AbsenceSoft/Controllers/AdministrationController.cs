﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Models.Signup;
using AbsenceSoft.Models.Administration;
using AbsenceSoft.Logic.Communications;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Security;
using System.Linq;
using AbsenceSoft.Logic;
using System.Web;
using AbsenceSoft.Models.Communications;
using AbsenceSoft.Logic.Users;
using AbsenceSoft.Web;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Common;
using AbsenceSoft.Web.Helpers;
using System.Security.Claims;
using System.IdentityModel.Tokens;
using Microsoft.AspNet.Identity;
using AT.Logic.Authentication;
using System.Threading.Tasks;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    public class AdministrationController : BaseController
    {
        public const string ServiceOptionIndicatorKey = "ServiceOptionIndicator";
        private ApplicationSignInManager _signInManager;
        public AdministrationController(ApplicationSignInManager applicationSignInManager)
        {
            _signInManager = applicationSignInManager;
        }

        public AdministrationController()
        {

        }
        /// <summary>
        /// Gets the new object of ApplicationSignInManager.
        /// </summary>
        /// <value>
        /// The sign in manager.
        /// </value>
        protected ApplicationSignInManager SignInManager
        {
            get
            {
                if (_signInManager == null)
                    _signInManager = new ApplicationSignInManager(HttpContext.GetOwinContext().Authentication);
                return _signInManager;
            }
        }


        [Secure, Title("Administration")]
        [Route("Administration/{employerId?}", Name = "AdministrationIndex")]
        public ActionResult Index(string employerId)
        {
            ViewBag.HideConfiguration = false;
            if (string.IsNullOrEmpty(employerId))
            {
                return View("CustomerIndex", new CustomerViewModel(CurrentCustomer));
            }

            if (CurrentEmployer == null)
            {
                ViewBag.HideConfiguration = true;
                return View("EmployerIndex", new EmployerViewModel());
            }

            if (!CurrentUser.HasEmployerAccess(employerId))
            {
                return Redirect(Url.RouteUrl("AdministrationIndex", null, false));
            }

            return View("EmployerIndex", new EmployerViewModel(CurrentEmployer));
        }

        [Secure("EditCustomerInfo"), Title("Settings")]
        [HttpGet, Route("Administration/SecuritySettings", Name = "SecuritySettings")]
        public ActionResult SecuritySettings()
        {
            SecuritySettingsViewModel settings = new SecuritySettingsViewModel(CurrentCustomer);
            return View(settings);
        }

        [Secure("EditCustomerInfo"), Title("Settings")]
        [HttpPost, Route("Administration/SaveSecuritySettings", Name = "SaveSecuritySettings")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSecuritySettings(SecuritySettingsViewModel settings)
        {
            Customer currentCustomer = settings.ApplyToDataModel(CurrentCustomer);
            using (var customerService = new CustomerService())
            {
                customerService.Update(currentCustomer);
                Current.Customer(currentCustomer);
            }
            return RedirectViaJavaScript(Url.RouteUrl("AdministrationIndex"));
        }

        [Secure, ChildActionOnly]
        [Route("AdministrationHeader", Name = "AdministrationHeader")]
        public ActionResult AdministrationHeader()
        {
            using (var administrationService = new AdministrationService(Current.User()))
            {
                //fetch the permissions once then pass into the bunch of HasFeatureAndPermissions to minimize a bunch of DB calls
                List<string> permissions = administrationService.GetUserPermissionsList();

                List<AdministrationLinkViewModel> administrationLinks = new List<AdministrationLinkViewModel>();

                if (administrationService.HasFeatureAndPermission(Feature.WorkflowConfiguration, Permission.EditWorkflow, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("Workflows", "ViewWorkflows"));
                }

                if (administrationService.HasFeatureAndPermission(Feature.AdminPaySchedules, Permission.EditPaySchedules, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("Pay Schedules", "ViewPaySchedules")
                    {
                        DisplayForCustomer = false
                    });
                }

                if (administrationService.HasFeatureAndPermission(Feature.AdminPhysicalDemands, Permission.EditPhysicalDemands, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("Physical Demands", "ViewPhysicalDemands"));
                    administrationLinks.Add(new AdministrationLinkViewModel("Demand Types", "ViewDemandTypes"));
                }

                if (administrationService.HasFeatureAndPermission(Feature.AdminCustomFields, Permission.EditCustomFields, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("Custom Fields", "ViewCustomFields"));
                }

                if (administrationService.HasFeatureAndPermission(Feature.PolicyConfiguration, Permission.EditAbsenceReason, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("Absence Reasons", "ViewAbsenceReasons"));
                }

                if (administrationService.HasFeatureAndPermission(Feature.PolicyConfiguration, Permission.EditPolicies, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("Policies", "Policies")
                    {
                        DisplayForCustomer = false
                    });
                }

                if (administrationService.HasFeatureAndPermission(Feature.CommunicationConfiguration, Permission.EditCommunicationTemplate, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("Communications", "ViewCommunications"));
                    administrationLinks.Add(new AdministrationLinkViewModel("Paperwork/Attachments", "ViewPaperwork"));
                }

                if (administrationService.HasFeatureAndPermission(Feature.AdminNecessities, Permission.EditNecessities, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("Necessities", "ViewNecessities"));
                }

                if (administrationService.HasFeatureAndPermission(Feature.IPRestrictions, Permission.EditIPRanges, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("IP Ranges", "IpRangeList")
                    {
                        DisplayForEmployer = false
                    });
                }

                if (administrationService.HasFeatureAndPermission(Feature.AdminNoteCategories, Permission.EditNoteCategory, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("Note Categories", "ViewNoteCategories"));
                    administrationLinks.Add(new AdministrationLinkViewModel("Note Templates", "ViewNoteTemplates"));
                }
                if (administrationService.HasFeatureAndPermission(Feature.CaseClosureCategories, Permission.CreateCaseCloseReason, permissions) || administrationService.HasFeatureAndPermission(Feature.CaseClosureCategories, Permission.EditCaseCloseReason, permissions) || administrationService.HasFeatureAndPermission(Feature.CaseClosureCategories, Permission.DeleteCaseCloseReason, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("Case Close Reasons", "ViewCaseCategories"));
                }
                if (administrationService.HasFeatureAndPermission(Feature.AdminConsultations, Permission.EditConsultations, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("Consultations", "ViewConsultations"));
                }

                if (administrationService.HasFeatureAndPermission(Feature.AdminAssignTeams, Permission.AssignTeams, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("Case Assignee Types", "ViewCaseAssigneeTypes")
                    {
                        DisplayForEmployer = false
                    });
                    administrationLinks.Add(new AdministrationLinkViewModel("Case Assignment Rules", "ViewCaseAssignmentRules")
                    {
                        DisplayForEmployer = false
                    });
                    administrationLinks.Add(new AdministrationLinkViewModel("Teams", "ViewTeams")
                    {
                        DisplayForEmployer = false
                    });
                }

                if (administrationService.HasFeatureAndPermission(Feature.EmployerContact, Permission.EditEmployerContacts, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("Employer Contact Upload", "EmployerContactBulkUpload"));
                    administrationLinks.Add(new AdministrationLinkViewModel("Employer Contact Types", "ViewEmployerContactTypes"));
                    administrationLinks.Add(new AdministrationLinkViewModel("Employer Contacts", "ViewEmployerContacts")
                    {
                        DisplayForCustomer = false
                    });
                }
                if (administrationService.HasFeatureAndPermission(Feature.EmployerServiceOptions, Permission.EditCustomerServiceOptions, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("Service Types", "ViewCustomerServiceOptions"));
                }
                if (administrationService.HasFeatureAndPermission(Feature.CustomContent, Permission.EditCustomContent, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("Custom Content", "ViewCustomContent"));
                }

                if (administrationService.HasFeatureAndPermission(Feature.AdminOrganizations, Permission.EditOrganizations, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("Organizations", "ManageOrganizations")
                    {
                        DisplayForCustomer = false
                    });

                    administrationLinks.Add(new AdministrationLinkViewModel("Organization Types", "OrganizationTypeList")
                    {
                        DisplayForCustomer = false
                    });
                    if (administrationService.HasFeatureAndPermission(Feature.OshaReporting, Permission.EditOrganizations, permissions))
                    {
                        administrationLinks.Add(new AdministrationLinkViewModel("Organization Annual Info", "AnnualInfoEntry")
                        {
                            DisplayForCustomer = false
                        });
                    }
                }

                if (administrationService.HasFeatureAndPermission(Feature.AdminDataUpload, Permission.UploadEligibilityFile, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("Data Upload", "EligibilityFileUpload"));
                }

                if (administrationService.HasFeatureAndPermission(Feature.JobConfiguration, Permission.EditJobs, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("Jobs", "ListJobs")
                    {
                        DisplayForCustomer = false
                    });
                }

                if (administrationService.HasFeatureAndPermission(Feature.ADA, Permission.EditAccommodationType, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("Accommodation Types", "ViewAccommodationTypes"));
                }

                if (administrationService.HasFeatureAndPermission(Feature.RiskProfile, Permission.EditRiskProfiles, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("Risk Profiles", "ViewRiskProfiles")
                    {
                        DisplayForEmployer = false
                    });
                }

                if (administrationService.HasFeatureAndPermission(Feature.PolicyCrosswalk, Permission.EditPolicyCrosswalkType, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("Policy Crosswalk", "ViewPolicyCrosswalkTypes"));
                }

                if (administrationService.UserHasPermission(Permission.EditEmployerInfo) && CurrentEmployer != null)
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("Holidays", "")
                    {
                        DisplayForCustomer = false,
                        LinkUrl = string.Format("/admin/{0}/#!/admin/employer/{0}/holidays", CurrentEmployer.Id)
                    });
                }
                // Osha e-file configuration option
                if (administrationService.HasFeatureAndPermission(Feature.WorkRelatedReporting, Permission.EditOshaApiToken, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("OSHA E-File", "OshaEFile")
                    {
                        DisplayForCustomer = false
                    });
                }

                if (administrationService.HasFeatureAndPermission(Feature.WorkRelatedReporting, Permission.EditOshaFormField, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("OSHA Reporting", "ViewOshaFormField"));
                }

                if (administrationService.HasFeatureAndPermission(Feature.SingleSignOn, Permission.EditSSOSettings, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("SSO Settings", "SSOSettings"));
                }

                //Edit Denial Reasons
                if (administrationService.HasFeatureAndPermission(Feature.AdminDenialReason, Permission.EditDenialReason, permissions))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("Denial Reasons", "ViewDenialReasons"));
                }

                //Edit Employee Class
                if (administrationService.UserHasPermission(Permission.EditEmployeeClass))
                {
                    administrationLinks.Add(new AdministrationLinkViewModel("Employee Class", "ViewEmployeeClasses"));
                }

                //Enforce alphabetical, in case it gets messed up
                administrationLinks = administrationLinks.OrderBy(c => c.LinkText).ToList();
                return PartialView("_AdministrationHeader", administrationLinks);
            }
        }

        [Secure, ChildActionOnly]
        [Route("AdministrationSidebar", Name = "AdministrationSidebar")]
        public ActionResult AdministrationSidebar()
        {
            using (var customerService = new CustomerService(CurrentUser))
            {
                List<EmployerInfo> employers = customerService.GetEmployersForCurrentCustomer()
                    .Where(e => CurrentUser.HasEmployerAccess(e.Id))
                    .OrderBy(e => e.Name)
                    .Select(e => new EmployerInfo(e)).ToList();

                return PartialView("_AdministrationSidebar", employers);
            }
        }

        [Secure, Title("View All Employers")]
        [Route("Administration/ViewAllEmployers", Name = "ViewAllEmployers")]
        public ActionResult ViewAllEmployers()
        {
            using (var customerService = new CustomerService(CurrentUser))
            {
                List<EmployerInfo> employers = customerService.GetEmployersForCurrentCustomer()
                    .Where(e => CurrentUser.HasEmployerAccess(e.Id))
                    .OrderBy(e => e.Name)
                    .Select(e => new EmployerInfo(e)).ToList();

                return View(employers);
            }
        }


        [Secure("EditCustomerInfo")]
        [HttpPost, Route("Administration/SaveCustomer", Name = "SaveCustomerChanges")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveCustomerChanges(CustomerViewModel customer, HttpPostedFileBase customerLogo)
        {
            if (!ModelState.IsValid)
                return View("CustomerIndex", customer);

            ///Verify that they uploaded a jpg or png
            if (customerLogo != null && customerLogo.ContentLength > 0)
            {
                if (customerLogo.IsImage())
                {
                    customer.LogoId = UpdateCustomerLogo(customerLogo);
                }
                else
                {
                    ModelState.AddModelError("customerLogo", "The uploaded logo is not an image.");
                    return View("CustomerIndex", customer);
                }
            }

            Customer currentCustomer = customer.ApplyToDataModel(Current.Customer());
            using (var customerService = new CustomerService())
            {
                customerService.Update(currentCustomer);
                Current.Customer(currentCustomer);
            }

            return RedirectToRoute("AdministrationIndex");
        }

        [Secure("EditEmployerInfo"), Title("Administration")]
        [HttpPost, Route("Administration/SaveEmployer/{employerId?}", Name = "SaveEmployerChanges")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveEmployerChanges(string employerId, EmployerViewModel employer, HttpPostedFileBase employerLogo)
        {
            employer.Id = employerId;
            if (!ModelState.IsValid)
                return View("EmployerIndex", employer);

            /// If they did not upload an image, don't care, nothing to do
            /// But if they did, it has to be an image
            if (employerLogo != null && employerLogo.ContentLength > 0 && !employerLogo.IsImage())
            {
                ModelState.AddModelError("employerLogo", "The uploaded logo is not an image.");
                return View("EmployerIndex", employer);
            }

            using (var employerService = new EmployerService())
            {
                Employer currentEmployer = employer.ApplyToDataModel(CurrentEmployer ?? employerService.CreateEmployer(CurrentCustomer));
                currentEmployer.CustomerId = CurrentCustomer.Id;
                employerService.Update(currentEmployer);
                Current.Employer(currentEmployer);

                /// Update the logo
                /// Do it after the save in case they created a new employer
                if (employerLogo != null && employerLogo.ContentLength > 0)
                {
                    /// If have multiple employers, go ahead and upload it for the specific employer
                    if (Employer.AsQueryable().Count(e => e.CustomerId == CurrentCustomer.Id) > 1)
                    {
                        currentEmployer.LogoId = UpdateEmployerLogo(employerLogo);
                        employerService.Update(currentEmployer);
                        Current.Employer(currentEmployer);
                    }
                    else
                    {
                        /// Otherwise we upload it for the customer, since that's what we displayed
                        UpdateCustomerLogo(employerLogo, true);
                    }
                }

                /// update the selected service option
                if (!string.IsNullOrWhiteSpace(employer.CustomerServiceOptionId) && CurrentCustomer.HasFeature(Feature.EmployerServiceOptions))
                {
                    using (var serviceOptionService = new EmployerServiceOptionService(CurrentCustomer, CurrentEmployer, CurrentUser))
                    {
                        serviceOptionService.UpsertEmployerServiceOption(CurrentEmployer.Id, "ServiceOptionIndicator", employer.CustomerServiceOptionId);
                    }
                }

                return RedirectToRoute("AdministrationIndex", new { employerId = CurrentEmployer.Id });
            }
        }

        /// <summary>
        /// Uploads a file for the current customer and optionally updates them
        /// </summary>
        /// <param name="logo"></param>
        /// <param name="updateCustomer"></param>
        /// <returns></returns>
        private string UpdateCustomerLogo(HttpPostedFileBase logo, bool updateCustomer = false)
        {
            using (var customerFileService = new CustomerFileService())
            using (var customerService = new CustomerService(CurrentUser))
            {
                Customer currentCustomer = CurrentCustomer;
                currentCustomer.LogoId = customerFileService.UploadFile(CurrentCustomer.Id, logo).Id;
                Current.Customer(currentCustomer);

                if (updateCustomer)
                    customerService.Update(currentCustomer);

                return currentCustomer.LogoId;
            }
        }

        /// <summary>
        /// Uploads a file for the current employer
        /// </summary>
        /// <param name="logo"></param>
        /// <returns></returns>
        private string UpdateEmployerLogo(HttpPostedFileBase logo)
        {
            using (var employerFileService = new EmployerFileService())
            {
                return employerFileService.UploadFile(CurrentEmployer.Id, logo).Id;
            }
        }


        [Secure, HttpGet]//Requires Permission?
        public ActionResult GetPermissions(string employerId)
        {
            return Json(AbsenceSoft.Data.Security.User.Permissions.EmployerPermissions(employerId ?? Employer.CurrentId));
        }

        /// <summary>
        /// Administration > Edit User Info
        /// </summary>
        /// <param name="model">The create user model populated</param>
        /// <returns>JSON: same model w/ or without errors + login cookie if successful</returns>
        [Secure("EditUser"), HttpPost]
        [Route("Administration/UpdateUserInfo", Name = "UpdateUserInfo")]
        [ValidateApiAntiForgeryToken]
        public ActionResult UpdateUserInfo(CreateUserModel model)
        {
            if (model == null)
            {
                return Json(new CreateUserModel() { Error = "Request malformed or corrupt. Please try again." });
            }

            model.Error = "";
            if (string.IsNullOrWhiteSpace(model.Id))
                model.Error = "Not valid request\n";
            if (string.IsNullOrWhiteSpace(model.Email))
                model.Error = "Email address is required\n";
            if (string.IsNullOrWhiteSpace(model.FirstName))
                model.Error += "First Name is required";
            if (string.IsNullOrWhiteSpace(model.LastName))
                model.Error += "Last Name is required";

            if (string.IsNullOrWhiteSpace(model.Error))
            {
                try
                {
                    using (var authenticationService = new AuthenticationService())
                    using (var notificationService = new NotificationService(CurrentCustomer, CurrentEmployer, CurrentUser))
                    {
                        var user = authenticationService.GetUserById(model.Id);
                        user = model.ApplyToDataModel(user);
                        authenticationService.UpdateUser(user);
                        notificationService.SaveToDoNotificationForUser(user.Id, model.ToDoNotificationFrequency);
                    }
                }
                catch (Exception ex)
                {
                    model.Error += ex.Message;
                }
            }

            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                model.Error = model.Error.Trim();
            }

            return Json(model);
        }//POST: Account

        [Secure, HttpPost]
        [Route("UpdatePassword", Name = "UpdatePassword")]
        [ValidateApiAntiForgeryToken]
        public ActionResult UpdatePassword(ChangePasswordModel changePassword)
        {
            if (ModelState.IsValid)
            {
                using (AdministrationService svc = new AdministrationService())
                {
                    try
                    {
                        svc.ChangePassword(CurrentUser, changePassword.NewPassword);
                        return ReloadViaJavaScript();
                    }
                    catch (AbsenceSoftException ex)
                    {
                        ModelState.AddModelError(string.Empty, ex.Message);
                    }
                }
            }

            return PartialView("_ChangePasswordForm", changePassword);
        }

        [Secure("EditCustomerInfo"), HttpGet]
        public ActionResult EditCompanyInfo()
        {
            if (CurrentUser.Customer == null)
                return Redirect("~/");

            bool isTpaCustomerWithEmployerServiceOptions;
            using (var employerServiceOptionService = new EmployerServiceOptionService(Current.User()))
            {
                isTpaCustomerWithEmployerServiceOptions = employerServiceOptionService.IsTpaCustomerWithEmployerServiceOptions();
            }

            var company = new CreateCustomerModel
            {
                Id = CurrentUser.Customer.Id,
                Name = CurrentUser.Customer.Name,
                SubDomain = CurrentUser.Customer.Url,
                Address1 = CurrentUser.Customer.Contact.Address.Address1,
                Address2 = CurrentUser.Customer.Contact.Address.Address2,
                City = CurrentUser.Customer.Contact.Address.City,
                State = CurrentUser.Customer.Contact.Address.State,
                Country = CurrentUser.Customer.Contact.Address.Country,
                PostalCode = CurrentUser.Customer.Contact.Address.PostalCode,
                Phone = CurrentUser.Customer.Contact.WorkPhone,
                Fax = CurrentUser.Customer.Contact.Fax,
                IsTpaCustomerWithEmployerServiceOptions = isTpaCustomerWithEmployerServiceOptions
            };

            return View(company);
        }

        [Secure("EditRoleDefinition")]
        [HttpGet]
        [Route("Administration/EditRoles", Name = "EditRoles")]
        public ActionResult EditRoles()
        {
            if (CurrentUser == null || string.IsNullOrEmpty(CurrentUser.CustomerId))
                return View();

            EditRolesModel model = new EditRolesModel();
            model.CustomerId = CurrentUser.CustomerId;
            model.AllRoles = new List<RoleModel>();
            model.AllPermissions = new List<PermissionModel>();
            model.AllCategories = new List<PermissionCategoryModel>();
            model.IsESSDataVisibilityInPortalFeatureEnabled = CurrentCustomer.HasFeature(Feature.ESSDataVisibilityInPortal);

            //roles
            using (AuthenticationService svc = new AuthenticationService())
            {
                List<Role> roles = svc.GetRoles(CurrentUser.CustomerId, false).OrderBy(x => x.Name).ToList();
                roles.ForEach(o => model.AllRoles.Add(new RoleModel()
                {
                    Id = o.Id,
                    Name = o.Name,
                    Type = o.Type,
                    AlwaysAssign = o.AlwaysAssign,
                    DefaultRole = o.DefaultRole
                }
                ));
            }
            return View(model);
        }

        [Secure("EditCustomerInfo")]
        [HttpPost, Route("Administration/AddRole", Name = "AddRole")]
        [ValidateApiAntiForgeryToken]
        public ActionResult AddRole(EditRolesModel model)
        {
            if (model == null)
                return Json(model);

            try
            {
                bool duplicateRole = Role.AsQueryable().Where(p => p.Name == model.RoleName && p.IsDeleted == false && p.CustomerId == model.CustomerId).Count() > 0;
                if (duplicateRole)
                    throw new AbsenceSoftException("Role Name duplicates an existing role.  Please change the name.");

                var deletedRole = Role.Repository.AsQueryable().FirstOrDefault(r => r.CustomerId == model.CustomerId && r.Name == model.RoleName && r.IsDeleted);

                Role newRole = new Role()
                {
                    CustomerId = model.CustomerId,
                    Name = model.RoleName,
                    Permissions = MakePermissions(model.Permissions),
                    Type = model.Type,
                    ContactTypes = model.ContactTypes ?? new List<string>(0),
                    RevokeAccessIfNoContactsOfTypes = model.RevokeAccessIfNoContactsOfTypes,
                    DefaultRole = model.DefaultRole,
                    AlwaysAssign = model.AlwaysAssign,
                };


                // We're instead going to "undelete" the deleted role by using the old roleId
                if (deletedRole != null)
                {
                    newRole.Id = deletedRole.Id;
                }


                newRole.Save();
                model.RoleId = newRole.Id;

                using (AuthenticationService svc = new AuthenticationService())
                {
                    model.AllRoles = svc.GetRoles(CurrentUser.CustomerId, false)
                        .OrderBy(x => x.Name)
                        .Select(r => new RoleModel() { Id = r.Id, Name = r.Name, Type = r.Type }).ToList();
                }


                model.Success = true;
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

            return Json(model);
        }

        [Secure("EditCustomerInfo")]
        [HttpPost, Route("Administration/EditRole", Name = "EditRole")]
        [ValidateApiAntiForgeryToken]
        public ActionResult EditRole(EditRolesModel model)
        {
            if (model == null)
                return Json(model);

            if (string.IsNullOrEmpty(model.RoleId) || model.Permissions == null)
                return Json(model);

            try
            {
                var role = Role.GetById(model.RoleId);
                role.Type = model.Type;
                role.ContactTypes = model.ContactTypes ?? new List<string>(0);
                role.RevokeAccessIfNoContactsOfTypes = model.RevokeAccessIfNoContactsOfTypes;
                role.Permissions = MakePermissions(model.Permissions);
                role.AlwaysAssign = model.AlwaysAssign;
                role.DefaultRole = model.DefaultRole;
                role.Save();
                model.Success = true;
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

            return Json(model);
        }

        [Secure("EditCustomerInfo")]
        [HttpDelete, Route("Administration/DeleteRole/{roleId}")]
        public ActionResult DeleteRole(string roleId)
        {
            try
            {
                Role r = Role.GetById(roleId);
                if (r != null)
                    using (var svc = new AdministrationService())
                    {
                        svc.DeleteRole(r);
                    }

                return Json(true);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to delete role");
            }
        }

        private List<string> MakePermissions(List<PermissionModel> model)
        {
            return model.Where(pm => pm.Apply).Select(pm => pm.Id).Distinct().ToList();
        }

        /// <summary>
        /// Retrieves a role with active permissions.
        /// </summary>
        /// <param name="roleId">
        /// The role to retrieve.  Leave empty or null to retrieve
        /// an empty permissions template
        /// </param>
        /// <returns></returns>
        [Secure, HttpGet, Route("Administration/GetRoleAndActivePermissions/{roleId?}", Name = "GetRoleAndActivePermissions")]
        public ActionResult GetRoleAndActivePermissions(string roleId)
        {
            try
            {
                using (var service = new AuthenticationService())
                {
                    var role = !String.IsNullOrWhiteSpace(roleId)
                        ? Role.GetById(roleId)
                        : new Role
                        {
                            CustomerId = CurrentCustomer.Id,
                            Permissions = Permission.AllCustomCommunications(CurrentCustomer.Id).Select(p => p.Id).ToList()
                        };
                    var allpermissions = Role.SystemAdministrator.GetPermissions();
                    /// Short term hack: Removing permission if they don't have ESS

                    // Short term hack: Removing permission if they are not AMZ
                    if (CurrentCustomer.Id != "546e5097a32aa00d60e3210a")
                    {
                        allpermissions.Remove(Permission.CreateCaseWizardOnly);
                    }

                    var activePermissions =
                        from ap in allpermissions
                        join rp in role.Permissions on ap.Id equals rp into rpRight
                        from optionalRp in rpRight.DefaultIfEmpty()
                        select new PermissionModel
                        {
                            Category = ap.Category,
                            HelpText = ap.HelpText,
                            Id = ap.Id,
                            Name = ap.Name,
                            RoleTypes = ap.RoleTypes,
                            Apply = optionalRp != null
                        };

                    var model = new
                    {
                        role.RevokeAccessIfNoContactsOfTypes,
                        role.Type,
                        role.CreatedBy,
                        role.CreatedById,
                        role.CreatedDate,

                        role.ModifiedBy,
                        role.ModifiedById,
                        role.ModifiedDate,

                        role.ContactTypes,
                        role.CustomerId,
                        role.Metadata,
                        role.Id,
                        role.Name,
                        role.DefaultRole,
                        role.AlwaysAssign,

                        Permissions = activePermissions.Distinct(a => a.Id),
                    };
                    return Json(model);
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure, HttpGet]//Requires Permission?
        public ActionResult GetRole(string roleId)
        {
            try
            {
                using (var service = new AuthenticationService())
                {
                    return Json(Role.GetById(roleId));
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure, HttpGet]//Requires Permission?
        [Route("Administration/GetRoles", Name = "AdminGetRoles")]
        public ActionResult GetRoles()
        {
            if (CurrentUser == null || string.IsNullOrEmpty(CurrentUser.CustomerId))
            {
                return Redirect("~/");
            }

            try
            {
                //roles
                using (AuthenticationService svc = new AuthenticationService())
                {
                    return Json(svc.GetRoles(CurrentUser.CustomerId).OrderBy(x => x.Name).ToList());
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }


        [ChildActionOnly]
        public PartialViewResult CompanyInfo()
        {
            return PartialView();
        }


        [Secure("EditCustomerInfo"), HttpPost]
        [ValidateApiAntiForgeryToken]
        public ActionResult EditCompanyInfo(CreateCustomerModel model)
        {
            if (!ModelState.IsValid)
            {
                model.Error = ModelState.ToHumanReadable();
                return Json(model);
            }

            if (string.IsNullOrWhiteSpace(model.Country))
                model.Country = "US";

            try
            {
                using (var svc = new CustomerService())
                {
                    var add = new Address
                    {
                        Address1 = model.Address1,
                        Address2 = string.IsNullOrWhiteSpace(model.Address2)
                            ? null
                            : model.Address2,
                        City = model.City,
                        State = model.State,
                        PostalCode = model.PostalCode,
                        Country = model.Country
                    };

                    var cust = CurrentUser.Customer;
                    cust.Id = model.Id;
                    cust.Name = model.Name;
                    cust.Contact.Address = add;
                    cust.Contact.WorkPhone = model.Phone;
                    cust.Contact.Fax = model.Fax;

                    svc.Update(cust);
                }
            }
            catch (Exception ex)
            {
                model.Error += ex.Message;
                return Json(model);
            }

            return Json(model);
        }

        [Secure("EditEmployerInfo"), HttpGet]
        public ActionResult EditEmployerInfo()
        {
            if (!string.IsNullOrWhiteSpace(CurrentUser.CustomerId) && CurrentCustomer != null)
            {
                // Stage a new employer to fill out our defaults here
                // HACK: Get the first employer that is active for this user :-( Need to change based on warning above.
                var erAccess = CurrentUser.Employers.FirstOrDefault(e => e.IsActive);
                var employer = (erAccess != null ? erAccess.Employer : null) ?? new EmployerService().Using(e => e.CreateEmployer(CurrentCustomer));
                bool isTpaCustomerWithEmployerServiceOptions;
                var customerServiceOptionId = string.Empty;
                using (var employerServiceOptionService = new EmployerServiceOptionService(Current.User()))
                {
                    isTpaCustomerWithEmployerServiceOptions = employerServiceOptionService.IsTpaCustomerWithEmployerServiceOptions();
                    var employerServiceOption = employerServiceOptionService.GetEmployerServiceOption(employer.Id, ServiceOptionIndicatorKey);
                    if (employerServiceOption != null)
                        customerServiceOptionId = employerServiceOption.CusomerServiceOptionId;
                }

                var model = new UpdateEmployerModel
                {
                    Id = employer.Id,
                    Address1 = employer.Contact.Address.Address1,
                    Address2 = employer.Contact.Address.Address2,
                    City = employer.Contact.Address.City,
                    State = employer.Contact.Address.State,
                    Country = employer.Contact.Address.Country,
                    PostalCode = employer.Contact.Address.PostalCode,
                    Phone = employer.Contact.WorkPhone,
                    Fax = employer.Contact.Fax,
                    ReferenceCode = employer.ReferenceCode,
                    //SignatureName = employer.Name,
                    SignatureName = employer.SignatureName,
                    MaxEmployees = employer.MaxEmployees,
                    AllowIntermittentForBirthAdoptionOrFosterCare = employer.AllowIntermittentForBirthAdoptionOrFosterCare,
                    IgnoreScheduleForPriorHoursWorked = employer.IgnoreScheduleForPriorHoursWorked,
                    IgnoreAverageMinutesWorkedPerWeek = employer.IgnoreAverageMinutesWorkedPerWeek,
                    EnableSpouseAtSameEmployerRule = employer.EnableSpouseAtSameEmployerRule,
                    FTWeeklyWorkHours = employer.FTWeeklyWorkHours ?? 40,
                    FMLPeriodType = employer.FMLPeriodType,
                    Enable50In75MileRule = employer.Enable50In75MileRule,
                    IsPubliclyTradedCompany = employer.IsPubliclyTradedCompany ?? false,
                    Holidays = employer.Holidays ?? new List<Holiday>(),
                    IsTpaCustomerWithEmployerServiceOptions = isTpaCustomerWithEmployerServiceOptions,
                    CustomerServiceOptionId = customerServiceOptionId
                };
                // Return the view with the populated model here.
                return View(model);
            }

            return Redirect("~/");

        }

        [ChildActionOnly]
        public PartialViewResult EmployerInfo()
        {
            return PartialView();
        }

        /// <summary>
        /// POST: Employer
        /// </summary>
        /// <param name="model">The employer update model</param>
        /// <returns>JSON: the same model passed in with the Id populated, or Error populated</returns>
        [HttpPost, Secure("EditEmployerInfo")]
        [ValidateApiAntiForgeryToken]
        public ActionResult UpdateEmployer(UpdateEmployerModel model)
        {
            if (model == null)
                return Json(new UpdateEmployerModel() { Error = "Request malformed or corrupt. Please try again." });

            using (var svc = new EmployerService())
            {

                var employer = Employer.GetById(model.Id) ?? svc.CreateEmployer(CurrentUser.Customer);

                model.Error = "";
                if (string.IsNullOrWhiteSpace(model.SignatureName))
                    model.Error = "Signature Name is required\n";
                if (string.IsNullOrWhiteSpace(model.Address1))
                    model.Error = "Address is required\n";
                if (string.IsNullOrWhiteSpace(model.City))
                    model.Error = "City is required\n";
                if (string.IsNullOrWhiteSpace(model.State))
                    model.Error = "State is required\n";
                if (string.IsNullOrWhiteSpace(model.PostalCode))
                    model.Error = "PostalCode is required\n";
                if (string.IsNullOrWhiteSpace(model.Country))
                    model.Country = "US";
                if (string.IsNullOrWhiteSpace(model.Phone))
                    model.Error = "Phone is required\n";

                model.ReferenceCode = string.IsNullOrWhiteSpace(model.ReferenceCode)
                    ? null
                    : model.ReferenceCode.Trim();

                if (string.IsNullOrWhiteSpace(model.Error))
                {
                    try
                    {
                        employer.Holidays = model.Holidays;
                        employer.Name = model.SignatureName;
                        employer.Url = model.Url;
                        employer.MaxEmployees = model.MaxEmployees;
                        employer.Contact.Address = new Address
                        {
                            Address1 = model.Address1,
                            Address2 = model.Address2,
                            City = model.City,
                            State = model.State,
                            PostalCode = model.PostalCode,
                            Country = model.Country
                        };
                        employer.Contact.WorkPhone = model.Phone;
                        employer.Contact.Fax = model.Fax;
                        employer.ReferenceCode = model.ReferenceCode;
                        employer.AllowIntermittentForBirthAdoptionOrFosterCare = model.AllowIntermittentForBirthAdoptionOrFosterCare;
                        employer.IgnoreScheduleForPriorHoursWorked = model.IgnoreScheduleForPriorHoursWorked;
                        employer.IgnoreAverageMinutesWorkedPerWeek = model.IgnoreAverageMinutesWorkedPerWeek;
                        employer.Enable50In75MileRule = model.Enable50In75MileRule;
                        employer.EnableSpouseAtSameEmployerRule = model.EnableSpouseAtSameEmployerRule;
                        employer.FMLPeriodType = model.FMLPeriodType;
                        employer.FTWeeklyWorkHours = model.FTWeeklyWorkHours;
                        employer.IsPubliclyTradedCompany = model.IsPubliclyTradedCompany;
                        employer.ModifiedById = CurrentUser.Id;
                        employer.ResetDayOfMonth = model.ResetDayOfMonth;
                        employer.ResetMonth = model.ResetMonth;
                        svc.Update(employer);

                        // Add new employer to Current user's employer list
                        if (string.IsNullOrWhiteSpace(model.Id))
                        {
                            using (var auth = new AuthenticationService())
                            {
                                // Set the user's employer id
                                CurrentUser.Employers.Add(
                                    new EmployerAccess
                                    {
                                        Employer = employer,
                                        AutoAssignCases = true
                                    });

                                // Set our update employer done
                                CurrentUser.Done(UserFlag.UpdateEmployer);
                                // Update the current user
                                auth.UpdateUser(CurrentUser);
                            }
                        }

                        model.Id = employer.Id;

                        try
                        {
                            using (var employerServiceOptionService = new EmployerServiceOptionService(Current.User()))
                            {
                                employerServiceOptionService.UpsertEmployerServiceOption(
                                    employer.Id, ServiceOptionIndicatorKey, model.CustomerServiceOptionId);
                            }
                        }
                        catch (Exception ex)
                        {
                            if (ex.Message.Contains("WriteConcern")
                                && ex.Message.Contains("duplicate key error")
                                && ex.Message.Contains("CustomerId_1_EmployerId_1_Key_1"))
                            {
                                throw new AbsenceSoftException(
                                    string.Format("Go to Manage Users section, and check user account: {0}, to see if they have been granted Employer Access for employer: {1}. The service option can not be assign at this time due to employer access permissions.",
                                        Current.User().DisplayName,
                                        employer.SignatureName));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        model.Error += ex.Message;
                    }
                }
            }

            model.Error = model.Error.Trim();
            if (string.IsNullOrWhiteSpace(model.Error))
                model.Error = null;

            return Json(model);
        }//POST: Employer

        [Secure("CloseAccount")]
        [HttpDelete, Route("Administration/{employerId}/DeleteAccount", Name = "DeleteAccount")]
        public ActionResult DeleteAccount(string employerId)
        {
            using (var administrationService = new AdministrationService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                administrationService.DeleteCurrentEmployer();
                if (administrationService.IsLastEmployer())
                    new AuthenticationService().Using(s => s.Logout(HttpContext));

                return RedirectViaJavaScript(Url.RouteUrl("AdministrationIndex", false));
            }
        }

        [Secure("CloseAccount")]
        [HttpGet, Route("Administration/{employerId}/RestoreAccount", Name = "RestoreAccount")]
        public ActionResult RestoreAccount(string employerId)
        {
            using (var administrationService = new AdministrationService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                administrationService.RestoreCurrentEmployer();
                return RedirectViaJavaScript(Url.RouteUrl("AdministrationIndex", false));
            }
        }

        /// <summary>
        /// Step 1 GET
        /// </summary>
        /// <returns>The Add user view</returns>
        [HttpGet, Secure("EditUser")]
        public ActionResult AddUserInfo()
        {
            return View(new CreateUserModel());
        }

        /// <summary>
        /// Step 1 POST
        /// </summary>
        /// <param name="model">The create user model populated</param>
        /// <returns>JSON: same model w/ or without errors + login cookie if successful</returns>
        [Secure("EditUser")]
        [HttpPost, Route("Administration/AddUserInfo", Name = "AddUserInfo")]
        [ValidateApiAntiForgeryToken]
        public ActionResult AddUserInfo(AddNewUserModel model)
        {
            if (model == null)
                return Json(new CreateUserModel() { Error = "Request malformed or corrupt. Please try again." });
            model.Error = "";
            if (string.IsNullOrWhiteSpace(model.Email))
                model.Error = "Email address is required\n";
            if (string.IsNullOrWhiteSpace(model.FirstName))
                model.Error += "First Name is required";
            if (string.IsNullOrWhiteSpace(model.LastName))
                model.Error += "Last Name is required";

            // Generate random password for new user
            string password = AbsenceSoft.RandomString.Generate(8, true, true, true, false);
            model.Password = password;

            User user = new User();
            // Set user to must change password so he has to set
            user.MustChangePassword = true;

            if (string.IsNullOrWhiteSpace(model.Error))
            {
                try
                {
                    List<string> roles = new List<string>();
                    foreach (RoleModel rm in model.ApplyRoles)
                        if (rm.Apply)
                            roles.Add(rm.Id);

                    using (AuthenticationService authenticationService = new AuthenticationService())
                    using (NotificationService notificationService = new NotificationService(CurrentCustomer, CurrentEmployer, CurrentUser))
                    {
                        List<Role> defaultRoles = authenticationService.GetRoles(CurrentCustomer.Id).Where(r => r.Type == (model.IsSelfServiceUser ? RoleType.SelfService : RoleType.Portal) && (r.DefaultRole || r.AlwaysAssign)).ToList();

                        if (roles.Count == 0)
                        {
                            foreach (Role rm in defaultRoles.Where(r => r.DefaultRole))
                                roles.AddIfNotExists(rm.Id);
                        }

                        foreach (Role rm in defaultRoles.Where(r => r.AlwaysAssign))
                            roles.AddIfNotExists(rm.Id);

                        user = authenticationService.CreateUser(model.Email, model.Password, model.FirstName, model.LastName, roles.ToArray());
                        user.UserFlags = UserFlag.CreateCustomer | UserFlag.UpdateEmployer | UserFlag.ProductTour | UserFlag.FirstEmployee;
                        user.ContactInfo = user.ContactInfo ?? new Contact();
                        user.ContactInfo.WorkPhone = model.Phone;
                        user.ContactInfo.Fax = model.Fax;
                        user.JobTitle = model.JobTitle;
                        user.Title = model.Title;
                        user.EndDate = model.EndDate;
                        user.UserType = model.IsSelfServiceUser ? UserType.SelfService : UserType.Portal;
                        authenticationService.UpdateUser(user);
                        model.Id = user.Id;

                        // Assign new user current user's customer
                        using (CustomerService customerSvc = new CustomerService())
                        {
                            // Assign customer to new user 
                            user.Customer = CurrentCustomer;
                            user.CustomerId = CurrentCustomer.Id;
                            model.CompanyName = CurrentCustomer.Name;
                        }

                        // Update user
                        user.Done(UserFlag.CreateCustomer);
                        authenticationService.UpdateUser(user);


                        //Assign new user current user's employer
                        foreach (string employerId in model.Employers)
                        {
                            EmployerAccess emp = new EmployerAccess();
                            emp.EmployerId = employerId;
                            emp.AutoAssignCases = model.AutoAssignCases;
                            user.Employers.Add(emp);
                        }

                        if (!string.IsNullOrWhiteSpace(model.AssignedEmployeeEmployerId)
                                && !string.IsNullOrWhiteSpace(model.AssignedEmployeeId) && user.Employers != null)
                        {
                            EmployerAccess emp = user.Employers.FirstOrDefault(e => e.EmployerId == model.AssignedEmployeeEmployerId);
                            if (emp != null)
                            {
                                emp.EmployeeId = model.AssignedEmployeeId;
                            }
                        }
                        else if (user.Employers != null)
                        {
                            foreach (EmployerAccess empAccess in user.Employers)
                            {
                                empAccess.EmployeeId = null;
                            }
                        }


                        // Set our update employer done
                        user.Done(UserFlag.UpdateEmployer);
                        authenticationService.UpdateUser(user);

                        notificationService.SaveToDoNotificationForUser(user.Id, model.ToDoNotificationFrequency);

                        // Send email to new user
                        authenticationService.AddNewUserMail(user);
                    }
                }
                catch (Exception ex)
                {
                    model.Error += ex.Message;
                }
            }

            model.Error = model.Error.Trim();
            if (string.IsNullOrWhiteSpace(model.Error))
                model.Error = null;

            return Json(model);
        }//POST: Account

        [Secure("EditUser"), HttpPost]
        [Route("Administration/UpdateEmployerAccess", Name = "UpdateEmployerAccess")]
        [ValidateApiAntiForgeryToken]
        public JsonResult UpdateEmployerAccess(EmployerAccessListModel model)
        {
            try
            {
                if (model == null || !model.EmployerAccessList.Any())
                    return Json(null);

                var employerAccessWithUserId = model.EmployerAccessList.FirstOrDefault(p => !string.IsNullOrWhiteSpace(p.UserId));
                if (employerAccessWithUserId == null)
                    return JsonError(null, "User could not be located, please refresh the page and try again.");

                var user = new AuthenticationService().Using(svc => svc.GetUserById(employerAccessWithUserId.UserId));
                if (user == null)
                    return JsonError(null, "User could not be located, please refresh the page and try again.");

                if (user.Employers == null || !user.Employers.Any())
                    return JsonError(null, "User employers could not be located, please refresh the page and try again.");

                var moreThanOneEmployer = user.Employers.Count > 1;
                var isTpa = user.Customer.HasFeature(Feature.MultiEmployerAccess) || moreThanOneEmployer;
                var isEss = user.Customer.HasFeature(Feature.EmployeeSelfService);
                var showEmployerAccess = isTpa || isEss;

                HashSet<string> customerRoles;
                using (var svc = new AuthenticationService())
                {
                    customerRoles = new HashSet<string>(svc.GetRoles(CurrentUser.CustomerId).Select(p => p.Id), StringComparer.InvariantCultureIgnoreCase);
                }

                foreach (var employerAccessModel in model.EmployerAccessList)
                {
                    // Only grant current acceptable roles.
                    employerAccessModel.Roles = employerAccessModel.Roles.Where(role => customerRoles.Contains(role)).ToList();

                    if (string.IsNullOrWhiteSpace(employerAccessModel.EmployerId))
                        continue;

                    if (employerAccessModel.EmployerId == "0")
                    {
                        // Adjust user only as there is no relevant employer id.
                        user.Roles = employerAccessModel.Roles;
                        continue;
                    }

                    var employerAccess = user.Employers == null
                        ? null
                        : user.Employers.FirstOrDefault(m => m.EmployerId == employerAccessModel.EmployerId);

                    if (employerAccess != null)
                        employerAccess.Roles = employerAccessModel.Roles;

                    if (user.Employers != null
                        && user.Employers.Count == 1
                        && showEmployerAccess == false)
                    {
                        // Keep user permissions in sync with employer.
                        user.Roles = employerAccessModel.Roles;
                    }
                }

                user.Save();
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

            return Json(model);
        }

        [Secure("EditCustomerInfo"), HttpGet, Title("Edit Company Info")]
        [Route("Administration/GetCompanyInfo", Name = "GetCompanyInfo")]
        public JsonResult GetCompanyInfo()
        {
            try
            {
                var customer = CurrentUser.Customer;
                var contact = customer.Contact;
                var address = customer.Contact.Address;

                bool isTpaCustomerWithEmployerServiceOptions;
                using (var employerServiceOptionService = new EmployerServiceOptionService(Current.User()))
                {
                    isTpaCustomerWithEmployerServiceOptions = employerServiceOptionService.IsTpaCustomerWithEmployerServiceOptions();
                }

                var model = new CreateCustomerModel
                {
                    Id = customer.Id,
                    Name = customer.Name,
                    SubDomain = customer.Url,
                    Address1 = address.Address1,
                    Address2 = address.Address2,
                    City = address.City,
                    State = address.State,
                    Country = address.Country,
                    PostalCode = address.PostalCode,
                    Phone = contact.WorkPhone,
                    Fax = contact.Fax,
                    LogoId = customer.LogoId,
                    IsTpaCustomerWithEmployerServiceOptions = isTpaCustomerWithEmployerServiceOptions
                };

                return Json(model);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditCustomerInfo", "ViewCustomerAndEmployerInfo"), HttpGet]//EditEmployerInfo?
        [Route("Administration/GetBasicInfo", Name = "GetBasicInfo")]
        public JsonResult GetBasicInfo()
        {
            try
            {
                bool isTpaCustomerWithEmployerServiceOptions;
                Dictionary<string, string> employerServiceOptionIdDictionary;
                using (var employerServiceOptionService = new EmployerServiceOptionService(Current.User()))
                {
                    isTpaCustomerWithEmployerServiceOptions = employerServiceOptionService.IsTpaCustomerWithEmployerServiceOptions();
                    employerServiceOptionIdDictionary = employerServiceOptionService.GetCustomerServiceOptionIdDictionary(ServiceOptionIndicatorKey);
                }

                var customer = CurrentUser.Customer;
                var basicInfo = new AdministrationBasicInfo
                {
                    CompanyId = CurrentUser.CustomerId,
                    CompanyName = customer.Name,
                    UserName = CurrentUser.FirstName + " " + CurrentUser.LastName,
                    CoURL = customer.Url,
                    CoAddressOne = customer.Contact.Address.Address1,
                    CoAddressTwo = customer.Contact.Address.Address2,
                    CoCityStateZip = string.Format(
                        "{0}, {1} {2} {3}",
                        customer.Contact.Address.City,
                        customer.Contact.Address.State,
                        customer.Contact.Address.Country,
                        customer.Contact.Address.PostalCode),
                    IsTpaCustomerWithEmployerServiceOptions = isTpaCustomerWithEmployerServiceOptions
                };

                var employers = Employer.AsQueryable().Where(m => m.CustomerId == CurrentUser.Customer.Id).ToList();
                basicInfo.EmployersInfo = new List<EmployerInfo>();

                foreach (var employer in employers)
                {
                    string customerServiceOptionId;
                    employerServiceOptionIdDictionary.TryGetValue(employer.Id, out customerServiceOptionId);

                    basicInfo.EmployersInfo.Add(
                        new EmployerInfo
                        {
                            EmployerId = employer.Id,
                            EmployerName = employer.Name,
                            EmpAddressOne = employer.Contact.Address.Address1,
                            EmpAddressTwo = employer.Contact.Address.Address2,
                            EmpCityStateZip = string.Format(
                                "{0}, {1} {2} {3}",
                                employer.Contact.Address.City,
                                employer.Contact.Address.State,
                                employer.Contact.Address.Country,
                                employer.Contact.Address.PostalCode),
                            CusomerServiceOptionId = customerServiceOptionId,
                            ReferenceCode = employer.ReferenceCode
                        });
                }

                basicInfo.UserEmail = CurrentUser.Email;

                return Json(basicInfo);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditEmployerInfo", "ViewCustomerAndEmployerInfo"), HttpPost]
        [Route("Administration/GetEmployersList", Name = "GetEmployersList")]
        [ValidateApiAntiForgeryToken]
        public JsonResult GetEmployersList(ListCriteria criteria)
        {
            try
            {
                using (var svc = new AdministrationService())
                {
                    return Json(svc.EmployersList(CurrentUser, criteria));
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error getting employer list");
            }
        }

        [HttpGet, Secure("EditUser")]
        [Route("Administration/{userId}/ManageUser", Name = "ManageUser")]
        public ActionResult ManageUser(string userId)
        {
            dynamic model = new System.Dynamic.ExpandoObject();
            model.User = new { };
            model.IsAddNew = true;

            if (!string.IsNullOrWhiteSpace(userId) && userId.Trim() != "000000000000000000000000")
            {
                using (var authenticationService = new AuthenticationService())
                using (var notificationService = new NotificationService(CurrentCustomer, CurrentEmployer, CurrentUser))
                {
                    User user = authenticationService.GetUserById(userId);
                    NotificationConfiguration configuration = notificationService.GetToDoNotificationForUser(userId);
                    NotificationFrequencyType? toDoNotificationFrequency = null;
                    if (configuration != null)
                    {
                        toDoNotificationFrequency = configuration.FrequencyType;
                    };

                    model.IsAddNew = false;
                    List<string> roles = new List<string>();
                    roles.AddRange(user.Roles);

                    List<Role> allRoles;
                    using (var svc = new AuthenticationService())
                    {
                        allRoles = svc.GetRoles(user.CustomerId).OrderBy(x => x.Name).ToList();
                    }
                    var isLoggedInUser = CurrentUser.Id == userId;

                    model.User = new
                    {
                        Id = user.Id,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Title = user.Title,
                        DisplayName = user.DisplayName,
                        JobTitle = user.JobTitle,
                        EndDate = user.EndDate,
                        Phone = (user.ContactInfo == null ? null : user.ContactInfo.WorkPhone),
                        Fax = (user.ContactInfo == null ? null : user.ContactInfo.Fax),
                        Roles = roles.Distinct().ToList(),
                        RolesName = allRoles.Where(m => roles.Contains(m.Id))
                                    .Select(m => m.Type == RoleType.SelfService ? string.Format("{0} (ESS)", m.Name) : m.Name)
                                    .ToList(),
                        IsSelfServiceUser = user.UserType == UserType.SelfService,
                        IsLoggedInUser = isLoggedInUser,
                        ToDoNotificationFrequency = toDoNotificationFrequency,
                        Email = user.Email,
                        Status = user.Status,
                        IsDisableable = (!user.IsDisabled && user.HasPermission(Permission.DisableUser) && !isLoggedInUser),
                        IsResetPwdAllowed = user.HasPermission(Permission.ManageUserAccount),
                        IsEnableable = (user.IsDisabled && user.HasPermission(Permission.EnableUser)),
                        IsEditable = (!user.IsDeleted && !user.IsDisabled),
                        Employers = user.Employers.Where(e => e.IsActive == true && e.Employer != null).Select(m => new { Id = m.EmployerId, Name = m.Employer.Name, Roles = m.Roles }).OrderBy(emp => emp.Name),
                        IsLocked = user.IsLocked
                    };
                }
            }

            return View(model);
        }


        [HttpGet, Secure("EditEmployerInfo")]
        [Route("Administration/GetEmployerInfo", Name = "GetEmployerInfo")]
        //[Route("Administration/Employer/{employerId}", Name = "GetEmployerInfo")]
        public JsonResult GetEmployerInfo(string employerId)
        {
            try
            {
                bool isTpaCustomerWithEmployerServiceOptions;
                var customerServiceOptionId = string.Empty;
                using (var employerServiceOptionService = new EmployerServiceOptionService(Current.User()))
                {
                    isTpaCustomerWithEmployerServiceOptions = employerServiceOptionService.IsTpaCustomerWithEmployerServiceOptions();
                    var employerServiceOption = employerServiceOptionService.GetEmployerServiceOption(employerId, ServiceOptionIndicatorKey);
                    if (employerServiceOption != null)
                        customerServiceOptionId = employerServiceOption.CusomerServiceOptionId;
                }

                // employer is null set employer from Current customer
                // other wise get employer info using employerid and set employer
                if (string.IsNullOrWhiteSpace(employerId))
                {
                    var employer = new EmployerService().Using(e => e.CreateEmployer(CurrentCustomer));
                    var model = new UpdateEmployerModel
                    {
                        Id = employer.Id,
                        MaxEmployees = employer.MaxEmployees,
                        AllowIntermittentForBirthAdoptionOrFosterCare = employer.AllowIntermittentForBirthAdoptionOrFosterCare,
                        IgnoreScheduleForPriorHoursWorked = employer.IgnoreScheduleForPriorHoursWorked,
                        IgnoreAverageMinutesWorkedPerWeek = employer.IgnoreAverageMinutesWorkedPerWeek,
                        EnableSpouseAtSameEmployerRule = employer.EnableSpouseAtSameEmployerRule,
                        FTWeeklyWorkHours = employer.FTWeeklyWorkHours ?? 40,
                        FMLPeriodType = employer.FMLPeriodType,
                        Enable50In75MileRule = employer.Enable50In75MileRule,
                        IsPubliclyTradedCompany = employer.IsPubliclyTradedCompany ?? false,
                        Holidays = employer.Holidays ?? new List<Holiday>(),
                        IsTpaCustomerWithEmployerServiceOptions = isTpaCustomerWithEmployerServiceOptions,
                        CustomerServiceOptionId = customerServiceOptionId
                    };

                    // Return the view with the populated model here.
                    return Json(model);
                }
                else
                {
                    // Stage a new employer to fill out our defaults here
                    var employer = AbsenceSoft.Data.Customers.Employer.GetById(employerId);
                    var model = new UpdateEmployerModel
                    {
                        Id = employer.Id,
                        Address1 = employer.Contact.Address.Address1,
                        Address2 = employer.Contact.Address.Address2,
                        City = employer.Contact.Address.City,
                        State = employer.Contact.Address.State,
                        Country = employer.Contact.Address.Country,
                        PostalCode = employer.Contact.Address.PostalCode,
                        Phone = employer.Contact.WorkPhone,
                        Fax = employer.Contact.Fax,
                        ReferenceCode = employer.ReferenceCode,
                        //SignatureName = employer.Name,
                        SignatureName = employer.Name ?? employer.Customer.Contact.FirstName + " " + employer.Customer.Contact.LastName,
                        Url = employer.Url,
                        LogoId = employer.LogoId,
                        MaxEmployees = employer.MaxEmployees,
                        AllowIntermittentForBirthAdoptionOrFosterCare = employer.AllowIntermittentForBirthAdoptionOrFosterCare,
                        IgnoreScheduleForPriorHoursWorked = employer.IgnoreScheduleForPriorHoursWorked,
                        IgnoreAverageMinutesWorkedPerWeek = employer.IgnoreAverageMinutesWorkedPerWeek,
                        EnableSpouseAtSameEmployerRule = employer.EnableSpouseAtSameEmployerRule,
                        FTWeeklyWorkHours = employer.FTWeeklyWorkHours ?? 40,
                        FMLPeriodType = employer.FMLPeriodType,
                        Enable50In75MileRule = employer.Enable50In75MileRule,
                        IsPubliclyTradedCompany = employer.IsPubliclyTradedCompany ?? false,
                        Holidays = employer.Holidays ?? new List<Holiday>(),
                        DefaultPayScheduleId = employer.DefaultPayScheduleId,
                        IsTpaCustomerWithEmployerServiceOptions = isTpaCustomerWithEmployerServiceOptions,
                        CustomerServiceOptionId = customerServiceOptionId
                    };
                    // Return the view with the populated model here.
                    return Json(model);
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        /// <summary>
        /// Step 2, GET: Logo
        /// </summary>
        /// <returns>A file result or HTTP Not found result if no logo or logo was not found or other error occurred</returns>
        [HttpGet]
        [Route("Employer/Logo", Name = "GetEmployerLogo")]
        public ActionResult GetEmployerLogo(string employerId)
        {
            if (string.IsNullOrWhiteSpace(employerId))
                return Json(new { IsSuccess = false, Error = "employer id not provided" });

            var employer = Employer.AsQueryable().FirstOrDefault(m => m.Id == employerId);
            if (employer == null)
                return Json(new { IsSuccess = false, Error = "Employer not found" });

            if (string.IsNullOrWhiteSpace(employerId) || string.IsNullOrWhiteSpace(employer.LogoId))
                return new HttpNotFoundResult("Image not found");

            using (EmployerFileService svc = new EmployerFileService())
            {
                try
                {
                    string url = svc.DownloadFile(employerId, employer.LogoId);
                    if (!string.IsNullOrWhiteSpace(url))
                        return Redirect(url);
                    else
                        return GetDefaultLogoImage();
                }
                catch (AbsenceSoftException softEx)
                {
                    return new HttpNotFoundResult("Image not found; " + softEx.Message);
                }
            }
        }//GET: Logo

        private FilePathResult GetDefaultLogoImage()
        {
            string filepath = Server.MapPath("~/Content/Images/AbsenceSoft-logo-200.png");
            return File(filepath, "image/png");
        }

        /// <summary>
        /// DELETE: Logo
        /// </summary>
        /// <returns>A JSON result</returns>
        [HttpDelete, Secure("EditCustomerInfo")]
        [Route("Employer/DeleteLogo")]
        public ActionResult DeleteLogo(string employerId)
        {
            if (string.IsNullOrWhiteSpace(employerId))
                return Json(new { IsSuccess = false, Error = "employer id not provided" });

            var employer = Employer.AsQueryable().FirstOrDefault(m => m.Id == employerId);
            if (employer == null)
                return Json(new { IsSuccess = false, Error = "Employer not found" });

            if (string.IsNullOrWhiteSpace(employer.LogoId))
                return Json(new { IsSuccess = true });
            try
            {
                string fileId = employer.LogoId;
                employer.LogoId = null;
                new EmployerService().Using(c => c.Update(employer));
                new EmployerFileService().Using(s => s.DeleteFile(fileId));
                return Json(new { IsSuccess = true });
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, Error = ex.Message });
            }
        }//DELETE: Logo


        [HttpGet, Secure(Permission.ReAssignTodoPermissionId)]
        [Route("Administration/GetUsersList", Name = "GetUsersList")]
        public JsonResult GetUsersList(string employerId)
        {
            try
            {
                using (AuthenticationService svc = new AuthenticationService(CurrentUser))
                {
                    var usersList = svc.GetUsersList(CurrentUser.Customer.Id, employerId);
                    List<UsersListVM> lstUsers = new List<UsersListVM>();

                    usersList.ForEach(x => { 
                        bool hasSelfService = Role.Query.Find(Role.Query.In(m => m.Id, x.Roles)).Where(r => r.Type == RoleType.SelfService).Select(r => r.Name).Any();
                        int hasOtherRole = Role.Query.Find(Role.Query.In(m => m.Id, x.Roles)).Where(r => r.Type != RoleType.SelfService).Select(r => r.Name).Count();
                        if (hasSelfService)
                        {
                            if (hasOtherRole > 0 || x.Roles.Contains("000000000000000000000000"))
                            {
                                lstUsers.Add(new UsersListVM()
                                {
                                    Id = x.Id,
                                    FirstName = x.FirstName,
                                    LastName = x.LastName,
                                    Email = x.Email,
                                    IsLoggedInUser = x.Id == CurrentUser.Id ? true : false,
                                    DisplayName = x.DisplayName,
                                    Roles = x.Roles,
                                    RolesName = Role.Query.Find(Role.Query.In(m => m.Id, x.Roles)).Select(m => m.Name).ToList()
                                });
                            }
                        }
                        else
                        {
                            lstUsers.Add(new UsersListVM()
                            {
                                Id = x.Id,
                                FirstName = x.FirstName,
                                LastName = x.LastName,
                                Email = x.Email,
                                IsLoggedInUser = x.Id == CurrentUser.Id ? true : false,
                                DisplayName = x.DisplayName,
                                Roles = x.Roles,
                                RolesName = Role.Query.Find(Role.Query.In(m => m.Id, x.Roles)).Select(m => m.Name).ToList()
                            });
                        }
                    }
                    );
                    return Json(lstUsers);
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpGet, Secure]
        [Route("Administration/{employerId}/GetEECustomFields", Name = "GetEECustomFields")]
        public ActionResult GetEECustomFields(string employerId, EntityTarget page)
        {
            using (EmployerService svc = new EmployerService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                return Json(svc.GetCustomFields(page, true));
            }
        }


        [Secure, HttpGet]
        [Route("Administration/{userId}/GetAssignedEmployeeForUser", Name = "GetAssignedEmployeeForUser")]
        public JsonResult GetAssignedEmployeeForUser(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                return JsonError(new Exception("user id is not provided"));

            User user = new AuthenticationService().GetUserById(userId);
            if (user == null)
                return JsonError(new Exception("user is not valid"));

            EmployerAccess empAccess = user.Employers.FirstOrDefault(empl => empl.EmployeeId != null);

            if (empAccess != null)
            {
                Employee emp = empAccess.Employee;
                if (emp != null)
                {
                    return Json(new
                    {
                        Id = emp.Id,
                        Name = emp.FullName,
                        EmployeeNumber = emp.EmployeeNumber,
                        EmployerName = emp.EmployerName,
                        EmployerId = emp.EmployerId,
                    });
                }
            }
            return null;
        }

        [DoNotResetCookie]
        [HttpGet]
        [Route("Administration/GetUserIsAuthenticatedOrNot", Name = "GetUserIsAuthenticatedOrNot")]
        public JsonResult IsUserAuthenticated()
        {
            var authDetails = new UserIsAuthenticatedViewModel();
            if (!(User.Identity is ClaimsIdentity identity))
            {
                return Json(authDetails, JsonRequestBehavior.AllowGet);
            };
            var token = identity.FindFirstValue("token");
            if (token == null)
            {
                return Json(authDetails, JsonRequestBehavior.AllowGet);
            };
            JwtSecurityToken jwtToken = new JwtSecurityToken(token);
            //checking the JWT validity as identity.IsAuthenticated is behaving stragely when opened with multiple tab in a browser
            authDetails.IsUserAuthenticated = jwtToken.ValidTo.ToUniversalTime().ToUnixDate() >= DateTime.UtcNow.ToUniversalTime().ToUnixDate();
            authDetails.WillExpire = Data.Customers.SecuritySettings.ShowTimeoutDialog(HttpContext, jwtToken.ValidTo);
            authDetails.WillExpireSameDayOrWithinNextMeridiem = Data.Customers.SecuritySettings.WillExpireSameDayOrWithinNextMeridiem(HttpContext, jwtToken.ValidTo);
            authDetails.ExpirationTime = jwtToken.ValidTo.ToUniversalTime().ToUnixDate();
            return Json(authDetails, JsonRequestBehavior.AllowGet);
        }

        [Secure, HttpGet]
        [Route("Administration/RenewSession", Name = "RenewSession")]
        public async Task<JsonResult> RenewSession()
        {
            if (User.Identity.IsAuthenticated && User.Identity is ClaimsIdentity identity)
            {
                var token = identity.FindFirstValue("token");
                if (token != null)
                {
                    await SignInManager.SignInRenewAysnc(token, AT.Entities.Authentication.ApplicationType.Portal);
                }
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [Secure, HttpGet]
        [Route("admin/{employerId?}")]
        public ActionResult EmployerAdminIndex(string employerId)
        {
            return View();
        }

        private List<AbsenceSoft.Models.Administration.ListItem> MakeListVM(List<AbsenceSoft.Data.Customers.ListItem> listModel)
        {
            List<AbsenceSoft.Models.Administration.ListItem> list = new List<Models.Administration.ListItem>();
            foreach (AbsenceSoft.Data.Customers.ListItem li in listModel)
            {
                list.Add(new Models.Administration.ListItem() { Key = li.Key, Value = li.Value });
            }
            return list;
        }

        private List<CustomFieldModel> MakeFieldsVM(string employerId)
        {
            List<CustomField> fields = null;
            using (EmployerService svc = new EmployerService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                fields = svc.GetCustomFields();
            }

            List<CustomFieldModel> list = new List<CustomFieldModel>();
            if (fields != null)
            {
                foreach (CustomField cf in fields)
                {
                    list.Add(new CustomFieldModel()
                    {
                        EmployerId = employerId,
                        Id = cf.Id,
                        Name = cf.Name,
                        Description = cf.Description,
                        Label = cf.Label,
                        HelpText = cf.HelpText,
                        DataType = (int)cf.DataType,
                        ValueType = (int)cf.ValueType,
                        ListValues = MakeListVM(cf.ListValues),
                        IsRequired = cf.IsRequired,
                        IsCollectedAtIntake = cf.IsCollectedAtIntake,
                        FieldOrder = cf.FileOrder,
                        Target = (int)cf.Target
                    });
                }
            }
            return list;
        }
    }
}