﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Models.Signup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    public class SignupController : BaseController
    {
        /// <summary>
        /// Step 1 GET
        /// </summary>
        /// <returns>The account view result</returns>
        [HttpGet]
        public ActionResult Account()
        {
#if RELEASE
            ViewBag.TrackConversion = true;
#else
            ViewBag.TrackConversion = false;
#endif
            return View(new CreateUserModel());
        }//GET: Account

        /// <summary>
        /// Step 1 GET: CheckEmail
        /// </summary>
        /// <param name="email">The email address to check for duplicates in the DB</param>
        /// <returns>JSON: { Exists: Boolean }</returns>
        [HttpGet]
        public JsonResult CheckEmail(string email)
        {
            try
            {
                using (var customerService = new CustomerService(CurrentUser))
                {
                    string lowEmail = (email ?? string.Empty).ToLowerInvariant();
                    var u = Data.Security.User.AsQueryable().SingleOrDefault(q => q.Email == lowEmail);
                    string domainContent = customerService.MatchesDomainContent(email);
                    return Json(new
                    {
                        Exists = u != null,
                        IsDeleted = u != null && u.IsDeleted,
                        IsDisabled = u != null && u.IsDisabled,
                        MatchesDomain = !string.IsNullOrEmpty(domainContent),
                        MatchesDomainContent = domainContent
                    });
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "There was an error validating the email address, please refresh the page and try again.", 500);
            }
        }//GET: CheckEmail

        /// <summary>
        /// Step 1 GET: CheckEmailCustId
        /// </summary>
        /// <param name="email">The email address & custId to check for duplicates in the DB</param>
        /// <returns>JSON: { Exists: Boolean }</returns>
        [HttpGet]
        public JsonResult CheckEmailCustomerId(string email)
        {
            try
            {
                string lowEmail = (email ?? string.Empty).ToLowerInvariant();
                var employee = Data.Security.User.AsQueryable().Where(u => u.Email == lowEmail && !u.IsDeleted).Where(u => u.CustomerId == CurrentCustomer.Id).Any();
                return Json(new { Exists = (employee != false) });
            }
            catch (Exception ex)
            {
                return JsonError(ex, "There was an error validating the email address, please refresh the page and try again.", 500);
            }
        }//GET: CheckEmail

        /// <summary>
        /// GET: CheckOnlyByEmail
        /// </summary>
        /// <param name="email">The email address to check existence in the DB</param>
        /// <returns>JSON: { Exists: Boolean }</returns>
        [HttpGet]
        public JsonResult CheckEmployeeEmail(string email)
        {
            try
            {
                using (EmployeeService svc = new EmployeeService())
                    return Json(new { Exists = svc.GetEmployeeOnlyByEmail(email) });
            }
            catch (Exception ex)
            {
                return JsonError(ex, "There was an error validating the email address, please refresh the page and try again.", 500);
            }
        }//GET: CheckEmail

        /// <summary>
        /// Step 1 POST
        /// </summary>
        /// <param name="model">The create user model populated</param>
        /// <returns>JSON: same model w/ or without errors + login cookie if successful</returns>
        /// 
        [HttpPost]
        [ValidateApiAntiForgeryToken]
        public ActionResult Account(CreateUserModel model)
        {
            if (model == null)
                return Json(new CreateUserModel() { Error = "Request malformed or corrupt. Please try again." });

            model.Error = "";
            using (var customerService = new CustomerService(CurrentUser))
            {
                if (string.IsNullOrWhiteSpace(model.Email))
                    model.Error = "Email address is required\n";
                if (string.IsNullOrWhiteSpace(model.Password))
                    model.Error += "Password is required\n";
                if (string.IsNullOrWhiteSpace(model.ConfirmPassword))
                    model.Error += "Password confirmation is required\n";
                if (model.Password != model.ConfirmPassword)
                    model.Error += "Password confirmation does not match the password provided. Passwords must match\n";
                if (string.IsNullOrWhiteSpace(model.FirstName))
                    model.Error += "First Name is required";
                if (string.IsNullOrWhiteSpace(model.LastName))
                    model.Error += "First Name is required";
                if (string.IsNullOrWhiteSpace(model.Name))
                    model.Error += "Name is required\n";
                if (string.IsNullOrWhiteSpace(model.SubDomain))
                    model.Error += "Customer URL is required\n";
                if (string.IsNullOrWhiteSpace(model.Phone))
                    model.Error += "Phone is required\n";
                if (string.IsNullOrWhiteSpace(model.Address1))
                    model.Error += "Address is required\n";
                if (string.IsNullOrWhiteSpace(model.City))
                    model.Error += "City is required\n";
                if (string.IsNullOrWhiteSpace(model.State))
                    model.Error += "State is required\n";
                if (string.IsNullOrWhiteSpace(model.PostalCode))
                    model.Error += "PostalCode is required\n";
                if (string.IsNullOrWhiteSpace(model.Country))
                    model.Country = "US";

                string matchesDomainContent = customerService.MatchesDomainContent(model.Email);
                if (!string.IsNullOrEmpty(matchesDomainContent))
                    model.Error += string.Format("{0}\n", matchesDomainContent);
            }
            if (string.IsNullOrWhiteSpace(model.Error))
            {
                var user = new User();
                try
                {
                    // Add User Data
                    using (AuthenticationService svc = new AuthenticationService())
                    {
                        user = svc.CreateUser(model.Email, model.Password, model.FirstName, model.LastName, Role.SystemAdministrator.Id);
                        user.UserFlags = UserFlag.CreateCustomer | UserFlag.UpdateEmployer | UserFlag.ProductTour | UserFlag.FirstEmployee;

                        svc.UpdateUser(user);
                        model.Id = user.Id;
                        //FormsAuthentication.SetAuthCookie(user.Id, true);
                    }

                    using (CustomerService svc = new CustomerService())
                    {
                        var add = new Address()
                        {
                            Address1 = model.Address1,
                            Address2 = model.Address2,
                            City = model.City,
                            State = model.State,
                            PostalCode = model.PostalCode,
                            Country = model.Country
                        };
                        var cust = svc.CreateCustomer(model.Name, model.SubDomain, model.Phone, model.Fax, add, user);
                        svc.Update(cust);
                        model.CustomerId = cust.Id;
                        user.Roles = new List<string>() { Role.SystemAdministrator.Id };
                        user.CustomerId = cust.Id;
                    }

                    using (AuthenticationService svc = new AuthenticationService())
                    {
                        // Create first role
                        user.Done(UserFlag.CreateCustomer);
                        svc.UpdateUser(user);
                    }

                    // As per new sign up process, disable user account
                    using (AuthenticationService auth = new AuthenticationService())
                    {
                        //User user = CurrentUser;
                        user.DisabledDate = DateTime.Now;
                        auth.UpdateUser(user);

                        // Send create account email to user
                        auth.CreateAccountMail(user);

                        // Send user need access email to AbsenceTracker team
                        auth.UserNeedAccessMail(user);
                    }

                }
                catch (Exception ex)
                {
                    model.Error += ex.Message;
                    if (!string.IsNullOrWhiteSpace(model.Id))
                        Data.Customers.Customer.Delete(model.Id);
                    model.Id = null;
                }
            }

            model.Error = model.Error.Trim();
            if (string.IsNullOrWhiteSpace(model.Error))
                model.Error = null;

            return Json(model);
        }//POST: Account

        /// <summary>
        /// Step 2 GET
        /// </summary>
        /// <returns>The customer view result</returns>
        [HttpGet, Secure("EditCustomerInfo")]
        public ActionResult Customer()
        {
            if (string.IsNullOrWhiteSpace(CurrentUser.CustomerId) && CurrentUser.Must(UserFlag.CreateCustomer))
                return View(new CreateCustomerModel());

            return Redirect("~/");
        }//GET: Customer

        /// <summary>
        /// Step 2 GET: CheckDomain
        /// </summary>
        /// <param name="subDomain">The sub-domain to check for usage on</param>
        /// <returns>JSON: { Available: Boolean }</returns>
        [HttpGet]
        public ActionResult CheckDomain(string subDomain)
        {
            try
            {
                return Json(new { Available = !(new CustomerService().Using(c => c.IsSubDomainInUse(subDomain))) });
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error checking availability, please try again.", 500);
            }
        }//GET: CheckDomain

        /// <summary>
        /// Step 2 POST
        /// </summary>
        /// <param name="model">The create customer model populated</param>
        /// <returns>The same create customer model populated with errors if any</returns>
        [HttpPost, Secure("EditCustomerInfo")]
        [ValidateApiAntiForgeryToken]
        public ActionResult Customer(CreateCustomerModel model)
        {
            if (model == null)
                return Json(new CreateCustomerModel { Error = "Request malformed or corrupt. Please try again." });

            if (!string.IsNullOrWhiteSpace(CurrentUser.CustomerId) || !CurrentUser.Must(UserFlag.CreateCustomer))
            {
                model.Error = "This action is not valid, a customer has already been created. Please go to your home page.";
                if (!CurrentUser.Must(UserFlag.CreateCustomer))
                    return Json(model);

                CurrentUser.Done(UserFlag.CreateCustomer);
                CurrentUser.Save();
                return Json(model);
            }

            model.Error = "";
            if (string.IsNullOrWhiteSpace(model.Name))
                model.Error = "Name is required\n";
            if (string.IsNullOrWhiteSpace(model.SubDomain))
                model.Error = "Customer URL is required\n";
            if (string.IsNullOrWhiteSpace(model.Phone))
                model.Error = "Phone is required\n";
            if (string.IsNullOrWhiteSpace(model.Address1))
                model.Error = "Address is required\n";
            if (string.IsNullOrWhiteSpace(model.City))
                model.Error = "City is required\n";
            if (string.IsNullOrWhiteSpace(model.State))
                model.Error = "State is required\n";
            if (string.IsNullOrWhiteSpace(model.PostalCode))
                model.Error = "PostalCode is required\n";
            if (string.IsNullOrWhiteSpace(model.Country))
                model.Country = "US";
            //if (string.IsNullOrWhiteSpace(model.Fax))
            //    model.Error = "Fax is required";

            if (string.IsNullOrWhiteSpace(model.Error))
            {
                try
                {
                    using (var svc = new CustomerService())
                    {
                        var add = new Address
                        {
                            Address1 = model.Address1,
                            Address2 = model.Address2,
                            City = model.City,
                            State = model.State,
                            PostalCode = model.PostalCode,
                            Country = model.Country
                        };
                        var cust = svc.CreateCustomer(model.Name, model.SubDomain, model.Phone, model.Fax, add, CurrentUser);
                        svc.Update(cust);
                        model.Id = cust.Id;
                        CurrentUser.Roles = new List<string> { Role.SystemAdministrator.Id };
                        CurrentUser.CustomerId = cust.Id;
                    }
                    using (var svc = new AuthenticationService())
                    {
                        // Create first role
                        CurrentUser.Done(UserFlag.CreateCustomer);
                        svc.UpdateUser(CurrentUser);
                    }
                }
                catch (Exception ex)
                {
                    model.Error += ex.Message;
                    if (!string.IsNullOrWhiteSpace(model.Id))
                        Data.Customers.Customer.Delete(model.Id);
                    model.Id = null;
                }
            }

            model.Error = model.Error.Trim();
            if (string.IsNullOrWhiteSpace(model.Error))
                model.Error = null;

            return Json(model);
        }//POST: Customer

        /// <summary>
        /// Step 2, GET: Logo
        /// </summary>
        /// <returns>A file result or HTTP Not found result if no logo or logo was not found or other error occurred</returns>
        [HttpGet]
        public ActionResult Logo()
        {
            var c = CurrentCustomer ?? new Customer();
            return new CustomersController().CustomerImage(c.Id, c.LogoId);
        }//GET: Logo

        /// <summary>
        /// Uploads a file for a customer using the HttpPostedFileBase and returns a JSON result with
        /// the resulting file information, Id, etc. for displiay purposes back in UI land.
        /// </summary>
        /// <param name="customerLogo">The file to upload and save</param>
        /// <returns>A JSON result with the final result and file information</returns>
        [HttpPost, Secure("EditCustomerInfo")]
        [ValidateApiAntiForgeryToken]
        public ActionResult Logo(HttpPostedFileBase customerLogo)
        {
            if (customerLogo == null || customerLogo.ContentLength <= 0)
                return Json(new { IsSuccess = false, Error = "No file was uploaded or provided" });

            Customer cust = CurrentCustomer;
            if (cust == null)
                return Json(new { IsSuccess = false, Error = "Current customer not found" });

            try
            {
                var uploadedFile = new CustomerFileService().Using(s => s.UploadFile(cust.Id, customerLogo));
                cust.LogoId = uploadedFile.Id;
                new CustomerService().Using(c => c.Update(cust));
                string url = Url.Action("Logo", "Signup");
                return Json(new { LogoURL = url, IsSuccess = true });
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, Error = ex.Message });
            }
        }//Logo

        /// <summary>
        /// DELETE: Logo
        /// </summary>
        /// <returns>A JSON result</returns>
        [HttpDelete, Secure("EditCustomerInfo")]    //ActionName("Logo")
        public ActionResult DeleteLogo()
        {
            Customer cust = CurrentCustomer;
            if (cust == null)
                return Json(new { IsSuccess = false, Error = "Current customer not found" });

            if (string.IsNullOrWhiteSpace(cust.LogoId))
                return Json(new { IsSuccess = true });
            try
            {
                string fileId = cust.LogoId;
                cust.LogoId = null;
                new CustomerService().Using(s => s.Update(cust));
                new CustomerFileService().Using(s => s.DeleteFile(fileId));
                return Json(new { IsSuccess = true });
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, Error = ex.Message });
            }
        }//DELETE: Logo


        /// <summary>
        /// GET: Employer
        /// </summary>
        /// <returns>The employer view result</returns>
        [HttpGet, Secure("EditEmployerInfo")]
        public ActionResult Employer()
        {
            if (CurrentUser.Must(UserFlag.UpdateEmployer) && !string.IsNullOrWhiteSpace(CurrentUser.CustomerId) && CurrentCustomer != null)
            {
                // Stage a new employer to fill out our defaults here
#warning TPA-UI: Need this model to edit a specific employer, not the "current" Employer as there is no such thing anymore.
                // HACK: Get the first employer that is active for this user :-( Need to change based on warning above.
                var erAccess = CurrentUser.Employers.FirstOrDefault(e => e.IsActive);
                var employer = (erAccess != null ? erAccess.Employer : null) ?? new EmployerService().Using(e => e.CreateEmployer(CurrentCustomer));
                var model = new UpdateEmployerModel()
                {
                    Id = employer.Id,
                    UserId = CurrentUser.Id,
                    Address1 = employer.Contact.Address.Address1,
                    Address2 = employer.Contact.Address.Address2,
                    City = employer.Contact.Address.City,
                    State = employer.Contact.Address.State,
                    Country = employer.Contact.Address.Country,
                    PostalCode = employer.Contact.Address.PostalCode,
                    Phone = employer.Contact.WorkPhone,
                    Fax = employer.Contact.Fax,
                    ReferenceCode = employer.ReferenceCode,
                    //SignatureName = employer.Name,
                    SignatureName = employer.Customer.Contact.FirstName + " " + employer.Customer.Contact.LastName,
                    Url = employer.Url,
                    MaxEmployees = employer.MaxEmployees,
                    AllowIntermittentForBirthAdoptionOrFosterCare = employer.AllowIntermittentForBirthAdoptionOrFosterCare,
                    IgnoreScheduleForPriorHoursWorked = employer.IgnoreScheduleForPriorHoursWorked,
                    IgnoreAverageMinutesWorkedPerWeek = employer.IgnoreAverageMinutesWorkedPerWeek,
                    EnableSpouseAtSameEmployerRule = employer.EnableSpouseAtSameEmployerRule,
                    FTWeeklyWorkHours = employer.FTWeeklyWorkHours ?? 40,
                    FMLPeriodType = employer.FMLPeriodType,
                    Enable50In75MileRule = employer.Enable50In75MileRule,
                    IsPubliclyTradedCompany = employer.IsPubliclyTradedCompany ?? false,
                    Holidays = employer.Holidays ?? new List<Holiday>()
                };
                // Return the view with the populated model here.
                return View(model);
            }

            return Redirect("~/");
        }//GET: Employer

        /// <summary>
        /// Step 2 GET: CheckDomain - check employer domain availability
        /// </summary>
        /// <param name="subDomain">The sub-domain to check for usage on</param>
        /// <returns>JSON: { Available: Boolean }</returns>
        [HttpGet, Secure("EditEmployerInfo")]
        public ActionResult CheckEmployerDomain(string subDomain, string employerId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(employerId))
                    employerId = null;

                return Json(new { Available = !(new EmployerService().Using(c => c.IsSubDomainInUse(subDomain, employerId))) });
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error checking availability, please try again.", 500);
            }
        }//GET: CheckDomain

        /// <summary>
        /// POST: Employer
        /// </summary>
        /// <param name="model">The employer update model</param>
        /// <returns>JSON: the same model passed in with the Id populated, or Error populated</returns>
        [HttpPost, Secure("EditEmployerInfo")]
        [ValidateApiAntiForgeryToken]
        public ActionResult Employer(UpdateEmployerModel model)
        {
            if (model == null)
                return Json(new UpdateEmployerModel() { Error = "Request malformed or corrupt. Please try again." });

            if (string.IsNullOrWhiteSpace(CurrentUser.CustomerId))
            {
                model.Error = "This action is not valid, a customer has not yet been created. Please create the customer first.";
                if (!CurrentUser.Must(UserFlag.CreateCustomer))
                {
                    CurrentUser.UserFlags |= UserFlag.CreateCustomer;
                    CurrentUser.Save();
                }
                return Json(model);
            }

            model.Error = "";
            if (string.IsNullOrWhiteSpace(model.SignatureName))
                model.Error = "Signature Name is required\n";
            if (string.IsNullOrWhiteSpace(model.Address1))
                model.Error = "Address is required\n";
            if (string.IsNullOrWhiteSpace(model.City))
                model.Error = "City is required\n";
            if (string.IsNullOrWhiteSpace(model.State))
                model.Error = "State is required\n";
            if (string.IsNullOrWhiteSpace(model.PostalCode))
                model.Error = "PostalCode is required\n";
            if (string.IsNullOrWhiteSpace(model.Country))
                model.Country = "US";
            if (string.IsNullOrWhiteSpace(model.Phone))
                model.Error = "Phone is required\n";
            if (string.IsNullOrWhiteSpace(model.Url))
                model.Error = "Employer URL is required\n";
            //if (string.IsNullOrWhiteSpace(model.Fax))
            //   model.Error = "Fax is required";

            model.ReferenceCode = string.IsNullOrWhiteSpace(model.ReferenceCode)
                    ? null
                    : model.ReferenceCode.Trim();

            if (string.IsNullOrWhiteSpace(model.Error))
            {
                try
                {
                    using (EmployerService svc = new EmployerService())
                    {
                        var employer = AbsenceSoft.Data.Customers.Employer.GetById(model.Id) ?? svc.CreateEmployer(CurrentUser.Customer);
                        employer.Holidays = model.Holidays;
                        employer.Name = model.SignatureName;
                        employer.Url = model.Url;
                        employer.MaxEmployees = model.MaxEmployees;
                        employer.Contact.Address = new Address()
                        {
                            Address1 = model.Address1,
                            Address2 = model.Address2,
                            City = model.City,
                            State = model.State,
                            PostalCode = model.PostalCode,
                            Country = model.Country
                        };
                        employer.Contact.WorkPhone = model.Phone;
                        employer.Contact.Fax = model.Fax;
                        employer.ReferenceCode = model.ReferenceCode;
                        employer.AllowIntermittentForBirthAdoptionOrFosterCare = model.AllowIntermittentForBirthAdoptionOrFosterCare;
                        employer.IgnoreScheduleForPriorHoursWorked = model.IgnoreScheduleForPriorHoursWorked;
                        employer.IgnoreAverageMinutesWorkedPerWeek = model.IgnoreAverageMinutesWorkedPerWeek;
                        employer.Enable50In75MileRule = model.Enable50In75MileRule;
                        employer.EnableSpouseAtSameEmployerRule = model.EnableSpouseAtSameEmployerRule;
                        employer.FMLPeriodType = model.FMLPeriodType;
                        employer.FTWeeklyWorkHours = model.FTWeeklyWorkHours;
                        employer.IsPubliclyTradedCompany = model.IsPubliclyTradedCompany;
                        employer.ModifiedById = CurrentUser.Id;
                        employer.ResetDayOfMonth = model.ResetDayOfMonth;
                        employer.ResetMonth = model.ResetMonth;

                        svc.Update(employer);
                        model.Id = employer.Id;

                        if (CurrentUser.Must(UserFlag.UpdateEmployer))
                        {
                            using (AuthenticationService auth = new AuthenticationService())
                            {
                                // Set the user's employer id
                                CurrentUser.Employers.Add(new EmployerAccess()
                                {
                                    Employer = employer,
                                    AutoAssignCases = true
                                });
                                // Set our update employer done
                                CurrentUser.Done(UserFlag.UpdateEmployer);
                                // Update the current user
                                auth.UpdateUser(CurrentUser);
                            }
                        }

                        // As per new sign up process, disable user account
                        //using (AuthenticationService auth = new AuthenticationService())
                        //{
                        //    User user = CurrentUser;
                        //    user.DisabledDate = DateTime.Now;
                        //    auth.UpdateUser(user);

                        //    // Send create account email to user
                        //    auth.CreateAccountMail(user);

                        //    // Send user need access email to AbsenceTracker team
                        //    auth.UserNeedAccessMail(user);
                        //}

                        //// As per new sign up process user cannot login after singup
                        //// he needs account verification
                        //// so signing out 
                        //new AuthenticationService().Using(s => s.Logout());

                        //// Sign out the user and expire their authentication ticket
                        //FormsAuthentication.SignOut();

                        //// Abandon the session (hopefully this doesn't break if we turn session off)
                        //if (Session != null)
                        //    Session.Abandon();
                    }
                }
                catch (Exception ex)
                {
                    model.Error += ex.Message;
                }
            }

            model.Error = model.Error.Trim();
            if (string.IsNullOrWhiteSpace(model.Error))
                model.Error = null;

            return Json(model);
        }//POST: Employer

        [HttpGet]
        [Route("Signup/Verify/{customerId}/{resetKey}/approve", Name = "ApprovePage")]
        public ActionResult ApproveUser(string customerId, string resetKey)
        {
            if (string.IsNullOrWhiteSpace(resetKey) || string.IsNullOrWhiteSpace(customerId))
            {
                return View("ApproveDenyUser", new ApproveDenyUser()
                {
                    IsSuccess = false,
                    Message = "Request is not in correct format"
                });
            }

            return View("ApproveDenyUser", new ApproveDenyUser()
            {
                IsSuccess = true,
                Message = null,
                IsApprove = true,
                CustomerId = customerId,
                ResetKey = resetKey
            });
        }

        [HttpPost, Route("Signup/Verify/{customerId}/{resetKey}/approve", Name = "ApproveUser")]
        [ValidateApiAntiForgeryToken]
        public JsonResult ApproveUser()
        {
            string customerId = RouteData.Values["customerId"] as string;
            string resetKey = RouteData.Values["resetKey"] as string;

            if (string.IsNullOrWhiteSpace(resetKey))
                return Json(new { success = false, error = "Reset key is required and is not provided or is not in the correct format" });
            if (string.IsNullOrWhiteSpace(customerId))
                return Json(new { success = false, error = "CustomerId is required and is not provided or is not in the correct format" });

            try
            {
                using (var svc = new AuthenticationService())
                {
                    if (!svc.IsValidResetKey(customerId, resetKey))
                    {
                        return Json(new { success = false, error = "Invalid Request. Either this user request is already been attented or may be the URL you entered is incorrect / something missing." });
                    }
                }
                
                return Json(new { success = new AuthenticationService().Using(s => s.ApproveUser(customerId, resetKey)) });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, error = ex.Message });
            }
        }

        [HttpGet]
        [Route("Signup/Verify/{customerId}/{resetKey}/deny", Name = "DenyPage")]
        public ActionResult DenyUser(string customerId, string resetKey)
        {
            if (string.IsNullOrWhiteSpace(resetKey) || string.IsNullOrWhiteSpace(customerId))
            {
                return View("ApproveDenyUser", new ApproveDenyUser()
                {
                    IsSuccess = false,
                    Message = "Request is not in correct format"
                });
            }

            return View("ApproveDenyUser", new ApproveDenyUser()
            {
                IsSuccess = true,
                Message = null,
                IsApprove = false,
                CustomerId = customerId,
                ResetKey = resetKey
            });
        }

        [HttpPost, Route("Signup/Verify/{customerId}/{resetKey}/deny", Name = "DenyUser")]
        [ValidateApiAntiForgeryToken]
        public JsonResult DenyUser()
        {
            string customerId = RouteData.Values["customerId"] as string;
            string resetKey = RouteData.Values["resetKey"] as string;

            if (string.IsNullOrWhiteSpace(resetKey))
                return Json(new { success = false, error = "Reset key is required and is not provided or is not in the correct format" });
            if (string.IsNullOrWhiteSpace(customerId))
                return Json(new { success = false, error = "CustomerId is required and is not provided or is not in the correct format" });
            using (var svc = new AuthenticationService())
            {
                if (!svc.IsValidResetKey(customerId, resetKey))
                {
                    return Json(new { success = false, error = "Invalid Request. Either this user request is already been attented or may be the URL you entered is incorrect / something missing." });
                }
            }

            try
            {
                return Json(new { success = new AuthenticationService().Using(s => s.DenyUser(customerId, resetKey)) });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, error = ex.Message });
            }
        }

        [HttpGet]
        [Route("AccountSummary/{userId}", Name = "AccountSummary")]
        public ActionResult AccountSummary(string userId)
        {
            using (var svc = new AuthenticationService())
            {
                User user = svc.GetUserById(userId);

                // if user does not exist in the system ... redirect to home page
                if (user == null || user.IsDeleted)
                {
                    return RedirectToRoute("Home");
                }

                // if user is already been approved, don't display account summary page
                if (user.DisabledDate == null)
                {
                    return RedirectToRoute("Home");
                }

#if RELEASE
                return Redirect(AbsenceSoft.Common.Properties.Settings.Default.CreateAccountCompleteUrl);
#else
                // Otherwise show account summary page
                AccountSummaryVM model = new AccountSummaryVM()
                {
                    Name = user.FirstName + " " + user.LastName,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    CompanyName = user.Customer.Name,
                    Address1 = user.Customer.Contact.Address.Address1,
                    Address2 = user.Customer.Contact.Address.Address2,
                    City = user.Customer.Contact.Address.City,
                    State = user.Customer.Contact.Address.State,
                    PostalCode = user.Customer.Contact.Address.PostalCode,
                    Country = user.Customer.Contact.Address.Country,
                    PhoneNumber = user.Customer.Contact.WorkPhone,
                    Email = user.Email,
                    //BillingAddress1 = user.Employer.Contact.Address.Address1,
                    //BillingAddress2 = user.Employer.Contact.Address.Address2,
                    //BillingCity = user.Employer.Contact.Address.City,
                    //BillingState = user.Employer.Contact.Address.State,
                    //BillingPostalCode = user.Employer.Contact.Address.PostalCode,
                    //BillingCountry = user.Employer.Contact.Address.Country,
                    //Size = user.Employer.MaxEmployees.HasValue ? (user.Employer.MaxEmployees.Value == 999 ? "1 to 999" : "1000 to 4999") : "5000 Above",
                    CustomerId = user.CustomerId,
                    IsLogin = false
                };

                return View(model);
#endif
            }
        }
    }
}