﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Models.IpRange;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AbsenceSoft.Web.Attributes;

namespace AbsenceSoft.Controllers
{
    [Secure("EditIPRanges")]
    public class IpRangeController : BaseController
    {
        private IpRangeService Service()
        {
            return new IpRangeService(CurrentCustomer, null, CurrentUser);
        }

        #region Admin

        /// <summary>
        /// IpRanges the list.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
       // [Secure("EditNecessities")]
        [HttpGet, Route("IpRange/List", Name = "IpRangeList")]
        public ActionResult IpRangeList()
        {
            string customerId = CurrentCustomer.Id;
            ViewBag.CustomerId = customerId;
  
            using (var svc = Service())
            {
                var ipRanges = svc.GetAllIpRangesForCustomer().Select(p => new IpRangeViewModel(p)).ToList();
                if (ipRanges == null)
                    ipRanges = new List<IpRangeViewModel>();

                var model = new IpRangeListViewModel(ipRanges);
                // Warn the user if there are IP Ranges, they are not in them, and it's not yet enabled.
                model.WarnYourNotInThisList =
                    !CurrentCustomer.EnableIpFilter
                    && !svc.IsIpInRanges(Request.ClientIPAddress())
                    && svc.CheckExistingIpRanges();

                return View(model);
            }
        }

        [HttpGet]
        [Route("IpRange/Create", Name = "EditIpRange")]
        public ActionResult IpRangeEdit(string ipRangeId)
        {
            using (var svc = Service())
            {
                var model = new IpRangeViewModel(svc.GetIpRangeById(ipRangeId));
                model.CustomerId = CurrentCustomer.Id;
                return View(model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveIpRange(IpRangeViewModel model)
        {
            ViewBag.CustomerId = CurrentCustomer.Id;
            
            if (!ModelState.IsValid)
                return View("IpRangeEdit", model);

            using (var svc = Service())
            {
                svc.UpsertIpRange(model.IpRangeId, model.Name, model.Ip1, model.Ip2, model.Ip3, model.Ip41, model.Ip42, model.IsEnabled);
            }

            return Redirect(Url.Action("IpRangeList"));
        }

        [HttpDelete, Route("IpRange/Delete/{ipRangeId}", Name = "DeleteIpRange")]
        public ActionResult DeleteIpRange(string ipRangeId)
        {
            try
            {
                using (var svc = Service())
                    svc.RemoveIpRange(ipRangeId);
                return RedirectViaJavaScript(Url.Action("IpRangeList"));
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to delete IP Range");
            }
        }

        [HttpPost, Route("IpRange/SaveIpActivation", Name = "SaveIpActivation")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveIpActivation(bool enableIpFilter)
        {
            IpRangeConfirmationViewModel model = new IpRangeConfirmationViewModel();
            model.Success = true;

            try
            {
                if (CurrentCustomer.EnableIpFilter != enableIpFilter)
                {
                    CurrentCustomer.EnableIpFilter = enableIpFilter;
                    CurrentCustomer.Save();
                    
                    if (enableIpFilter)
                        model.Message = "IP Range Filtering has been enabled.";
                    else
                        model.Message = "IP Range Filtering has been disabled.";
                }
                else
                {
                    model.Warning = string.Format("No changes were made. IP Range Filtering was already {0}.", enableIpFilter ? "enabled" : "disabled");
                }
            }
            catch (Exception ex)
            {
                model.Success = false;
                model.Message = string.Format("Error saving IP Range Activation Setting for Customer, Error ID: {0}", Guid.NewGuid());
                Log.Error(model.Message, ex);
            }

            return PartialView("_ConfirmationMessage", model);
        }

        #endregion Admin

    }
}