﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Models;
using AbsenceSoft.Models.EmployerContacts;
using AbsenceSoft.Web;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Controllers
{
    public class EmployerContactController : BaseController
    {

        #region Bulk Upload
        [Secure("EditEmployerContacts")]
        [HttpGet, Route("EmployerContact/BulkUpload", Name="EmployerContactBulkUpload")]
        public ActionResult BulkUpload()
        {
            return View();
        }
        
        [Secure("EditEmployerContacts")]
        [HttpPost, Route("EmployerContact/BulkUpload", Name="EmployerContactBulkUploadPost")]
        [ValidateApiAntiForgeryToken]
        public ActionResult EmployerContactBulkUpload(BulkUploadEmployerContact upload)
        {
            try
            {
                using (var employerService = new EmployerService(Current.Customer(), Current.Employer(), Current.User()))
                {
                    List<EmployerContact> validContacts = upload.Contacts
                        .Where(c => c.IsValidForBulkUpload)
                        .Select(c => c.ApplyToDataModel()).ToList();

                    if (validContacts.Count == 0)
                    {
                        upload.Message = "No valid contacts found to upload.";
                        upload.Success = false;
                    }
                    else
                    {
                        employerService.BulkUploadContacts(validContacts);
                        upload.Success = true;
                        int invalidContacts = upload.Contacts.Count - validContacts.Count;
                        StringBuilder successMessage = new StringBuilder();
                        successMessage.AppendFormat("{0} contacts have been successfully uploaded.", validContacts.Count);
                        if (invalidContacts > 0)
                        {
                            successMessage.AppendFormat(" {0} contacts had missing data and could not be uploaded.", invalidContacts);
                            upload.PartialSuccess = true;
                        }
                        upload.Message = successMessage.ToString();
                    }
                }
            }
            catch (AbsenceSoftException aex)
            {
                upload.Success = false;
                upload.Message = aex.Message;
            }

            return Json(upload);
        }

        #endregion

        #region Employer Contacts

        [Secure("EditEmployerContacts")]
        [HttpGet, Route("EmployerContacts/{employerId}/List", Name = "ViewEmployerContacts")]
        public ActionResult ViewEmployerContacts(string employerId)
        {
            ViewBag.EmployerId = employerId;
            return View();
        }

        [Secure("EditEmployerContacts")]
        [HttpPost, Route("EmployerContacts/List", Name = "ListEmployerContacts")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListContacts(ListCriteria criteria)
        {
            using (var employerService = new EmployerService(Current.User()))
            {
                var contacts = employerService.ListEmployerContacts(criteria);
                return PartialView(contacts);
            }
            
        }

        [Secure("EditEmployerContacts")]
        [Route("EmployerContact/{employerId}/EditEmployerContact/{id?}", Name = "EditEmployerContact")]
        public ActionResult EditEmployerContact(string employerId, string id)
        {
            ContactViewModel contact = null;
            if (string.IsNullOrEmpty(id))
            {
                contact = new ContactViewModel()
                {
                    EmployerId = employerId
                };
            }
            else
            {
                using (var employerService = new EmployerService(Current.User()))
                {
                    contact = new ContactViewModel(employerService.GetContactById(id));
                }
            }

            return View(contact);
        }

        [Secure("EditEmployerContacts")]
        [HttpPost, Route("EmployerContact/{employerId}/SaveEmployerContact", Name = "SaveEmployerContact")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveEmployerContact(string employerId, ContactViewModel contact)
        {
            if (!ModelState.IsValid)
                return View("EditEmployerContact", contact);

            using (var employerService = new EmployerService(Current.Customer(), Current.Employer(), Current.User()))
            {
                EmployerContact contactData = employerService.GetContactById(contact.Id);
                contactData = contact.ApplyToDataModel(contactData);
                employerService.SaveContact(contactData);
            }

            return RedirectViaJavaScript(Url.RouteUrl("ViewEmployerContacts", new { employerId = employerId }));
        }

        [Secure("EditEmployerContacts")]
        [HttpDelete, Route("EmployerContact/{employerId}/Delete/{id}", Name = "DeleteEmployerContact")]
        public ActionResult DeleteEmployerContact(string employerId, string id)
        {
            using (var employerService = new EmployerService(Current.User()))
            {
                EmployerContact contact = employerService.GetContactById(id);
                employerService.DeleteContact(contact);
                return RedirectViaJavaScript(Url.RouteUrl("ViewEmployerContacts"));
            }
        }

        #endregion

        #region Employer Contact Types

        [Secure("EditEmployerContacts")]
        [HttpGet, Route("EmployerContactTypes/List", Name="ViewEmployerContactTypes")]
        public ActionResult ViewEmployerContactTypes()
        {
            return View();
        }

        [Secure("EditEmployerContacts")]
        [HttpPost, Route("EmployerContactTypes/List", Name="ListEmployerContactTypes")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListEmployerContactTypes(ListCriteria criteria)
        {
            using (var contactTypeService = new ContactTypeService(Current.User()))
            {
                var contactTypes = contactTypeService.ListEmployerContactTypes(criteria);
                return PartialView(contactTypes);
            }
        }

        [Secure("EditEmployerContacts")]
        [Route("EmployerContactTypes/EditContactType/{id?}", Name="EditEmployerContactType")]
        public ActionResult EditEmployerContactType(string id)
        {
            EmployerContactTypeViewModel contactType = null;
            if (string.IsNullOrEmpty(id))
            {
                contactType = new EmployerContactTypeViewModel();
            }
            else
            {
                using (var contactTypeService = new ContactTypeService(Current.User()))
                {
                    contactType = new EmployerContactTypeViewModel(contactTypeService.GetContactTypeById(id));
                }
            }

            return View(contactType);
        }

        [Secure("EditEmployerContacts")]
        [HttpPost, Route("EmployerContactTypes/SaveEmployerContactType", Name="SaveEmployerContactType")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveEmployerContactType(EmployerContactTypeViewModel contactType)
        {            
            if (!ModelState.IsValid)
                return View("EditEmployerContactType", contactType);

            string currentContactTypeCode = string.Empty;

            using (var employerService = new EmployerService(Current.User()))
            using (var contactTypeService = new ContactTypeService(Current.User()))
            {
                EmployerContactType contactTypeData = contactTypeService.GetContactTypeById(contactType.Id);

                // set current Code to update employer contacts
                if (contactTypeData != null)
                {
                    currentContactTypeCode = contactTypeData.Code;
                }

                // save contact type
                contactTypeData = contactType.ApplyToDataModel(contactTypeData);                
                contactTypeService.SaveContactType(contactTypeData);

                if (!string.IsNullOrWhiteSpace(Current.User().EmployerId) && !string.IsNullOrEmpty(currentContactTypeCode))
                {
                    // get existing employer contacts associated with this contact type code
                    var existingContacts = employerService.GetEmployerContactsByContactType(Current.User().EmployerId, currentContactTypeCode);

                    if (existingContacts != null)
                    {
                        foreach (var existingContact in existingContacts)
                        {
                            // Update employer contact's contact type code and name
                            EmployerContact contactData = employerService.GetContactById(existingContact.Id);
                            contactData.ContactTypeCode = contactType.Code;
                            contactData.ContactTypeName = contactType.Name;
                            contactData.Save();
                        }
                    }
                }                
            }

            return RedirectViaJavaScript(Url.RouteUrl("ViewEmployerContactTypes"));
        }

        [Secure("EditEmployerContacts")]
        [HttpDelete, Route("EmployerContactTypes/Delete/{id}", Name = "DeleteEmployerContactType")]
        public ActionResult DeleteEmployerContactType(string customerId, string id)
        {
            using (var contactTypeService = new ContactTypeService(Current.User()))
            {
                EmployerContactType contact = contactTypeService.GetContactTypeById(id);
                contactTypeService.DeleteContactType(contact);
                return RedirectViaJavaScript(Url.RouteUrl("ViewEmployerContactTypes"));
            }
        }

        #endregion
    }
}