﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Common;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using MongoDB.Driver.Linq;
using System.Text;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Models.Policies;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    [Secure]
    public class AbsenceReasonsController : BaseController
    {
        [HttpGet, Title("Absence Reasons")]
        [Route("AbsenceReasons/List/{employerId?}", Name = "ViewAbsenceReasons")]
        [Secure(Feature.PolicyConfiguration, "EditAbsenceReasons")]
        public ViewResult ViewAbsenceReasons(string employerId)
        {
            ViewBag.EmployerId = employerId;
            return View();
        }

        [Secure(Feature.PolicyConfiguration, "EditAbsenceReasons")]
        [HttpPost, Route("AbsenceReasons/List", Name = "ListAbsenceReasons")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListAbsenceReasons(ListCriteria criteria)
        {
            using (var policyService = new PolicyService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                ListResults results = policyService.AbsenceReasonList(criteria);
                return PartialView(results);
            }
        }

        [Secure(Feature.PolicyConfiguration, "EditAbsenceReasons")]
        [Title("Edit Absence Reason")]
        [HttpGet, Route("AbsenceReasons/EditAbsenceReason/{employerId?}", Name = "EditAbsenceReason")]
        public ActionResult EditAbsenceReason(string employerId, string code)
        {
            ViewBag.EmployerId = employerId;
            using (var absenceReasonService = new PolicyService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                AbsenceReasonViewModel viewModel = new AbsenceReasonViewModel(absenceReasonService.GetAbsenceReason(code));
                return View(viewModel);
            }
        }

        [Secure(Feature.PolicyConfiguration, "EditAbsenceReasons")]
        [HttpPost, Route("AbsenceReasons/SaveAbsenceReason", Name = "SaveAbsenceReason")]
        public ActionResult SaveAbsenceReasons(AbsenceReasonViewModel absenceReason)
        {
            if (!ModelState.IsValid)
                return PartialView("EditAbsenceReasons", absenceReason);

            using (var policyService = new PolicyService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                AbsenceReason reason = absenceReason.ApplyToDataModel(policyService.GetAbsenceReason(absenceReason.Code));
                policyService.SaveAbsenceReason(reason);
            }

            return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewAbsenceReasons"));
        }

        [Secure(Feature.PolicyConfiguration, "EditAbsenceReasons")]
        [HttpDelete, Route("AbsenceReasons/DeleteAbsenceReason/{employerId?}", Name = "DeleteAbsenceReason")]
        public ActionResult DeleteAbsenceReason(string employerId, string code)
        {
            ViewBag.EmployerId = employerId;
            using (var policyService = new PolicyService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                AbsenceReason absenceReason = policyService.GetAbsenceReason(code);
                if (absenceReason.IsCustom)
                    policyService.DeleteReason(absenceReason);

                return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewAbsenceReasons"));
            }
        }

        [Secure(Feature.PolicyConfiguration, "EditAbsenceReasons")]
        [HttpPost, Route("AbsenceReasons/ToggleAbsenceReasonSuppression/{employerId?}", Name = "ToggleAbsenceReasonSuppression")]
        public ActionResult ToggleAbsenceReasonSuppression(AbsenceReasonViewModel absenceReason)
        {
            using (var policyService = new PolicyService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                var savedAbsenceReason = policyService.ToggleAbsenceReasonByCode(absenceReason.Code);
                absenceReason.IsDisabled = savedAbsenceReason.IsDisabled;
                return Json(absenceReason);
            }
        }

        [HttpGet]
        //[Secure("PolicyConfig")]
        [Route("PolicyAbsenceReasons/{code}/GetAbsReasonByCode/{employerId}", Name = "GetAbsReasonByCode")]
        public JsonResult GetAbsReasonByCode(string code, string employerId)
        {
            if (string.IsNullOrEmpty(code))
                return JsonError(null, "Missing Absence Reason Code");

            try
            {
                return Json(AbsenceReason.GetByCode(code, CurrentCustomer.Id, employerId));
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error Getting Absence Reason");
            }
        }


        [HttpGet]
        //[Secure("PolicyConfig")]
        [Route("PolicyAbsenceReasons/{reasonId}/GetAbsReasonById/{employerId}", Name = "GetAbsReasonById")]
        public JsonResult GetAbsReasonById(string reasonId, string employerId)
        {
            if (string.IsNullOrEmpty(reasonId))
                return JsonError(null, "Missing Absence Reason Id");

            try
            {
                return Json(AbsenceReason.GetById(reasonId));
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error Getting Absence Reason");
            }
        }


        [HttpGet]
        //[Secure("PolicyConfig")]
        [Route("PolicyAbsenceReasons/{reasonId}/UpsertAbsReason/{employerId}", Name = "UpsertAbsReason")]
        [Route("PolicyAbsenceReasons/{employerId}/Create", Name = "CreateAbsReason")]
        public ViewResult UpsertAbsReason(string reasonId, string employerId)
        {
            if (!string.IsNullOrEmpty(reasonId))
                ViewBag.ReasonId = reasonId;

            if (!string.IsNullOrEmpty(employerId))
                ViewBag.EmployerId = employerId;

            return View("Upsert");
        }

        [HttpPost]
        //[Secure("PolicyConfig")]
        [Route("PolicyAbsenceReasons/{id}/UpsertAbsReasonPost", Name = "UpsertAbsReasonPost")]
        [ValidateApiAntiForgeryToken]
        public JsonResult UpsertAbsReasonPost(AbsenceReason model)
        {
            if (string.IsNullOrEmpty(model.Code))
                return JsonError(null, "Missing Absence Reason Code");

            try
            {
                using (var policyService = new PolicyService(CurrentCustomer.Id, null))
                {
                    return Json(policyService.SaveAbsenceReason(model));
                }

            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error Upating Absence Reason");
            }
        }

        [HttpPost]
        [Route("PolicyAbsenceReasons/List", Name = "PolicyAbsenceReasonsList")]
        [ValidateApiAntiForgeryToken]
        public JsonResult List(ListCriteria criteria)
        {
            try
            {
                using (var svc = new PolicyService(CurrentUser.CustomerId, criteria.Get<string>("EmployerId")))
                {
                    return Json(svc.AbsenceReasonList(criteria));
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error getting absence reason list");
            }
        }

        [HttpPost]
        //[Secure("PolicyConfig")]
        [Route("PolicyAbsenceReasons/{reasonId}/ToggleEnabled/{employerId}", Name = "ToggleEnabled")]
        [ValidateApiAntiForgeryToken]
        public JsonResult ToggleEnabled(string reasonId, string employerId)
        {
            //Policy model = new Policy();
            if (string.IsNullOrEmpty(reasonId))
                return JsonError(null, "Missing Absence Reason Id");

            var reason = AbsenceReason.GetById(reasonId);
            if (reason == null)
                return JsonError(null, "Missing Reason");

            try
            {
                bool enable = false;
                if (!string.IsNullOrWhiteSpace(employerId))
                {
                    var emp = Employer.GetById(employerId ?? Employer.CurrentId);
                    if (emp != null && emp.SuppressReasons.Any() && emp.SuppressReasons.Contains(reason.Code.ToUpperInvariant()))
                        enable = true;
                }

                using (var svc = new PolicyService(CurrentUser.CustomerId, employerId ?? Employer.CurrentId))
                {
                    if (enable)
                        svc.EnableReason(reason.Code);
                    else
                        svc.DisableReason(reason.Code);
                }
                return Json(reason);
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error Upating Absence Reason");
            }
        }

    }
}