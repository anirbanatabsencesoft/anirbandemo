﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Filters
{
    /// <summary>
    /// Represents an attribute that is used to restrict an action method so that the method handles only Ajax requests.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class AjaxAttribute : ActionMethodSelectorAttribute
    {
        /// <summary>
        /// Gets or sets a value indicating whether request must be an Ajax request or not.
        /// </summary>
        /// <value><c>true</c> if request must be an Ajax request; otherwise, <c>false</c>.</value>
        public bool MustBe { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AjaxAttribute"/> class.
        /// </summary>
        public AjaxAttribute() : this(true)
        {
            // does nothing
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AjaxAttribute"/> class.
        /// </summary>
        /// <param name="mustBe">if set to <c>true</c> then request must be an Ajax request.</param>
        public AjaxAttribute(bool mustBe)
        {
            this.MustBe = mustBe;
        }

        /// <summary>
        /// Determines whether the action method selection is valid for the specified controller context.
        /// </summary>
        /// <param name="controllerContext">The controller context.</param>
        /// <param name="methodInfo">Information about the action method.</param>
        /// <returns>
        /// true if the action method selection is valid for the specified controller context; otherwise, false.
        /// </returns>
        public override bool IsValidForRequest(ControllerContext controllerContext, MethodInfo methodInfo)
        {
            if (controllerContext == null)
                throw new ArgumentNullException("controllerContext");

            return controllerContext.HttpContext.Request.IsAjaxRequest() == this.MustBe;
        }
    }
}