﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace AbsenceSoft.Filters
{
    public class HandlePasswordChangeRequiredAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAjax()) return;

            string currentAction = filterContext.RouteData.GetRequiredString("action");
            string currentController = filterContext.RouteData.GetRequiredString("controller");
            if (currentAction.ToLowerInvariant() == "changepassword" && currentController.ToLowerInvariant() == "home") return;

            var user = Data.Security.User.Current;
            if (user == null) return;

            if (user.Metadata.GetRawValue<bool>("SSO")) return;

            if (!user.MustChangePassword) return;

            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
            {
                Action = "ChangePassword",
                Controller = "Home"
            }));


        }
    }
}