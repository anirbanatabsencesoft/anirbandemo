﻿using AbsenceSoft.Web.Attributes;
using AT.Logic.Authentication;
using Microsoft.AspNet.Identity;
using System;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Filters
{
    public class RenewSessionGlobalFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            // no action required.
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //checking for secure attribute
            bool hasSecureAttribute = filterContext.ActionDescriptor
                .GetCustomAttributes(typeof(SecureAttribute), false).Any();
            if (hasSecureAttribute && filterContext.ActionDescriptor.ActionName!= "RenewSession")
            {
                if (filterContext.HttpContext.User.Identity.IsAuthenticated && filterContext.HttpContext.User.Identity is ClaimsIdentity identity)
                {
                    var token = identity.FindFirstValue("token");
                    if (token != null)
                    {
                        // checks the willExpire
                        JwtSecurityToken jwtToken = new JwtSecurityToken(token);                        
                        var duration = jwtToken.ValidTo.Subtract(jwtToken.ValidFrom);
                        if (duration <= TimeSpan.Zero) return;
                        var halfWay = duration.TotalMinutes / 2;
                        var remaining= jwtToken.ValidTo.Subtract(DateTime.UtcNow);
                        if (remaining <= TimeSpan.Zero) return;                       
                        bool willExpire = remaining.TotalMinutes<= halfWay && remaining.TotalMinutes>2;
                        if (willExpire)
                        {
                            //renews the session
                            var SignInManager = new ApplicationSignInManager(filterContext.HttpContext.GetOwinContext().Authentication);
                            SignInManager.SignInRenewAysnc(token, AT.Entities.Authentication.ApplicationType.Portal).GetAwaiter();
                        }
                    }
                }
            }
        }


    }
}