﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Web;
using System;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Filters
{
    /// <summary>
    /// Represents a filter that prevents users from accessing resources that don't belong to a specific IP address range defined
    /// by the customer.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class IpRangeFilterAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IpRangeFilterAttribute"/> class.
        /// </summary>
        public IpRangeFilterAttribute() : base()
        {
            this.Order = 0;
        }

        /// <summary>
        /// Called when authorization is required.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!CheckDeviceIp(filterContext.HttpContext.Request.ClientIPAddress()) && !filterContext.HttpContext.Request.RawUrl.Contains("/Unauthorized"))
            {
                filterContext.HttpContext.Response.Redirect("/Home/Unauthorized", true);                
            }
        }
        
        /// <summary>
        /// Checks the device ip.
        /// </summary>
        /// <param name="ipAddress">The ip address.</param>
        /// <returns></returns>
        private bool CheckDeviceIp(string ipAddress)
        {
            // Just to be sure the customer isn't null, even though we've probably already checked othersise.
            var currentCustomer = Current.Customer();
            if (currentCustomer == null || !currentCustomer.HasFeature(Feature.IPRestrictions) || !currentCustomer.EnableIpFilter)
                return true;

            using (var ipRangeService = new IpRangeService(currentCustomer, Current.Employer(), Current.User()))
            {
                if (!ipRangeService.CheckExistingIpRanges())
                    return true;

                if (string.IsNullOrWhiteSpace(ipAddress))
                    return true;

                return ipRangeService.IsIpInRanges(ipAddress);
            }
        }
    }
}