﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class IgnoreModelErrorAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// The keys string
        /// </summary>
        private string keysString;

        /// <summary>
        /// Initializes a new instance of the <see cref="IgnoreModelErrorAttribute" /> class.
        /// </summary>
        /// <param name="keys">The keys.</param>
        /// <param name="ignore">The ignore function delegate.</param>
        public IgnoreModelErrorAttribute(string keys) : base()
        {
            this.keysString = keys;
        }

        /// <summary>
        /// Called by the ASP.NET MVC framework before the action method executes.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ModelStateDictionary modelState = filterContext.Controller.ViewData.ModelState;
            string[] keyPatterns = keysString.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < keyPatterns.Length; i++)
            {
                string keyPattern = keyPatterns[i]
                        .Trim()
                        .Replace(@".", @"\.")
                        .Replace(@"[", @"\[")
                        .Replace(@"]", @"\]")
                        .Replace(@"\[\]", @"\[[0-9]+\]")
                        .Replace(@"*", @"[A-Za-z0-9]+");
                IEnumerable<string> matchingKeys = modelState.Keys.Where(x => Regex.IsMatch(x, keyPattern));
                foreach (string matchingKey in matchingKeys)
                    modelState[matchingKey].Errors.Clear();
            }
        }
    }
}