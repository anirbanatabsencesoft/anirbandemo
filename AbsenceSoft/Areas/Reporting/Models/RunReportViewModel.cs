﻿using AbsenceSoft.Reporting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Areas.Reporting.Models
{
    public class RunReportViewModel
    {
        public RunReportViewModel()
        {

        }

        public RunReportViewModel(BaseReport baseReport)
            :this()
        {

        }

        public string Category { get; set; }

        public string Name { get; set; }

        public string FullName {
            get
            {
                return $"{Category} {Name}";
            }
        }
    }
}