﻿using AT.Logic.Reporting.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Areas.Reporting.Models
{
    public class ReportViewModel
    {
        public ReportViewModel()
        {

        }

        public ReportViewModel(BaseReport report)
            :this()
        {
            Name = report.Name ?? "Unknown";
            Id = report.Id;
        }

        public string Name { get; set; }

        public string Id { get; set; }
    }
}