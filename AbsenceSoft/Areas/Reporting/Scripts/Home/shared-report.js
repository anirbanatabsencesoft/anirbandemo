﻿(function (AbsenceSoft, $) {
    'use strict';
    AbsenceSoft.SharedReport = AbsenceSoft.SharedReport || {};
    var utils = AbsenceSoft.Utilities;
    //define the module
    AbsenceSoft.SharedReport.Module = "reporting";
    
    //Create the Report Card dynamically 
    AbsenceSoft.SharedReport.CreateReportCard = function (data, howmany) {
        var divHtml = document.createElement('div');        
        for (var i = 0; i < howmany; i++) {            
            var block = $("#SharedReportCard").clone().attr('id', 'sharedreportcard' + data[i].id);
            var newHtml = AbsenceSoft.SharedReport.getHtml(block, data[i]);
            $(divHtml).append(newHtml);
        }
        $("#SharedReport").html(divHtml);
        $(".SharedReport").removeClass("hide").addClass("show");
    };

    //get the html from template
    AbsenceSoft.SharedReport.getHtml = function (oldHtml, data) {
        var reportData = JSON.parse(data.criteriaJson);
        $(oldHtml).find('.shared-report-title').text(data.reportName);
        $(oldHtml).find('.shared-report-description').text(reportData.reportDefinition.name);
        $(oldHtml).find('.shared-report-card').prop('id', 'cardflip' + data.id);
        $(oldHtml).find('.shared-title').attr('data-target', '#cardflip' + data.id);
        $(oldHtml).find('.shared-title-flipIcon').attr('data-target', '#cardflip' + data.id);        
        if (data.lastRequestId)
        {
            $(oldHtml).find('.shared-report-go').prop('href', '/reporting/run/?reportId=' + reportData.reportId + '&sharedreportid=' + data.id + '&requestid=' + data.lastRequestId + '&type=shared'); 
            $(oldHtml).find('.shared-tile-navigation').prop('href', '/reporting/run/?reportId=' + reportData.reportId + '&sharedreportid=' + data.id + '&requestid=' + data.lastRequestId + '&type=shared'); 
            
        }
        else
        {
            $(oldHtml).find('.shared-report-go').prop('href', '/reporting/run/?reportId=' + reportData.reportId + '&sharedreportid=' + data.id + '&type=shared'); 
            $(oldHtml).find('.shared-tile-navigation').prop('href', '/reporting/run/?reportId=' + reportData.reportId + '&sharedreportid=' + data.id + '&type=shared'); 
        }
               
        return oldHtml;
    };

    //get all shared report
    AbsenceSoft.SharedReport.getSharedReports = function () {
        var endPoint = "reports/shared/get";
        try {
            AbsenceSoft.HttpUtils.Get(AbsenceSoft.SharedReport.Module, endPoint, function (result) {
                if (!utils.isNullOrWhitespace(result)) {
                    var resultData = JSON.parse(result);
                    if (resultData && resultData.success === true && resultData.length !== 0) {
                        if ($("#NoOfCards").val() === "all") {
                            AbsenceSoft.SharedReport.CreateReportCard(resultData.data, resultData.data.length);
                        }
                        else {
                            if (resultData.data.length > 4) {
                                $(".ActiveIfCount").show();
                            }
                            else {
                                $(".ActiveIfCount").hide();
                            }
                            if (resultData.data.length > 0) {
                                var howmany = (resultData.data.length < 4) ? resultData.data.length : 4;
                                AbsenceSoft.SharedReport.CreateReportCard(resultData.data, howmany);
                            }
                        }
                    }
                    else {
                        console.log(resultData.message);
                    }
                }
            });
        }              
        catch (ex) {
            console.log(ex);
        }
    };
   
    $(document).ready(function () {
        if ($(location).attr('href').indexOf("reporting/home") > -1) {
            AbsenceSoft.SharedReport.getSharedReports();
        }
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);