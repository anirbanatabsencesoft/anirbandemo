﻿(function (AbsenceSoft, $) {
    'use strict';
    AbsenceSoft.PopularReport = AbsenceSoft.PopularReport || {};

    //define the module
    AbsenceSoft.PopularReport.Module = "reporting";
    var utils = AbsenceSoft.Utilities;
    var reportType = {
        Operation: 'operation', BI: 'bi', Adhoc: 'adhoc', Forensic: 'forensic'
    };

    //Create the Report Card dynamically 
    AbsenceSoft.PopularReport.CreateReportCard = function (data, type) {
        var divHtml = document.createElement('div');
        var howmany = data.length > 4 ? 4 : data.length;
        for (var i = 0; i < howmany; i++) {
            var block = $("#" + type + "-report-card").clone().attr('id', type + data[i].id);
            var newHtml = AbsenceSoft.PopularReport.getHtml(block, data[i], type,i);
            $(divHtml).append(newHtml);
        }
        $("#" + type + "-report-popular").html(divHtml);
        $("." + type + "-report-container").removeClass("hide").addClass("show");
    };

    //get the html from template
    AbsenceSoft.PopularReport.getHtml = function (oldHtml, data, type,i) {        
        $(oldHtml).find('.'+type+'-report-title').text(data.name);      
        $(oldHtml).find('.' + type + '-report-description').text(data.reportDescription);
        $(oldHtml).find('.' + type +'-report-card').prop('id', 'cardflip' + data.id);
        $(oldHtml).find('.' + type +'-title').attr('data-target', '#cardflip' + data.id);
        $(oldHtml).find('.' + type +'-title-flipIcon').attr('data-target', '#cardflip' + data.id);
        $(oldHtml).find('.' + type + '-report-go').prop('href', '/reporting/run/?reportId=' + data.id + '&type=' + type);
        $(oldHtml).find('.' + type + '-tile-navigation').prop('href', '/reporting/run/?reportId=' + data.id + '&type=' + type);
        if (type === "bi") {
            if (i == 0)
                $(oldHtml).find('.fa-line-chart').removeClass('fa-line-chart').addClass('fa fa-line-chart');
            if(i==1)
                $(oldHtml).find('.fa-line-chart').removeClass('fa-line-chart').addClass('fa-area-chart');
            else if (i == 2)
                $(oldHtml).find('.fa-line-chart').removeClass('fa-line-chart').addClass('fa fa-list-alt');
            else if (i == 3)
                $(oldHtml).find('.fa-line-chart').removeClass('fa-line-chart').addClass('fa fa-bar-chart');
        }
        else if (type === "operation") {
            if(i==0)
                $(oldHtml).find('.fa-line-chart').removeClass('fa-line-chart').addClass('fa-area-chart').css("color", "#08607a");
           else if (i == 1)
                $(oldHtml).find('.fa-line-chart').removeClass('fa-line-chart').addClass('fa fa-list').css("color", "#08607a");
            else if (i == 2)
                $(oldHtml).find('.fa-line-chart').removeClass('fa-line-chart').addClass('fa fa-pie-chart').css("color", "#08607a");
            else if (i == 3)
                $(oldHtml).find('.fa-line-chart').removeClass('fa-line-chart').addClass('fa fa-envelope').css("color", "#08607a");
        }
        else if (type === "forensic") {
            if (i == 0)
                $(oldHtml).find('.fa-line-chart').removeClass('fa-line-chart').addClass('fa fa-indent').css("color", "#21a1c6");
            else if (i == 1)
                $(oldHtml).find('.fa-line-chart').removeClass('fa-line-chart').addClass('fa fa-align-left').css("color", "#21a1c6");
            else if (i == 2)
                $(oldHtml).find('.fa-line-chart').removeClass('fa-line-chart').addClass('fa-user-secret').css("color", "#21a1c6");
        }
        
        return oldHtml;
    };

   

//call respective http
    var callHttp = function (url, type) {
        try {
            AbsenceSoft.HttpUtils.Get("reporting", url, function (result) {
                if (!utils.isNullOrWhitespace(result)) {
                    var d = JSON.parse(result);
                    if (d.success == true) {
                        if (d.data.length > 0) {
                            AbsenceSoft.PopularReport.CreateReportCard(d.data, type);
                        }
                    }
                }
            });
        }
        catch (ex) {
            console.log(ex);
        }
    }

    //get popular reports
    AbsenceSoft.PopularReport.getPopularReports = function (name) {
        var url = "";
        if (name == "Operations Report") {
             url = "reports/operation/get/maincategory/" + name;
            callHttp(url, reportType.Operation);
        } else if (name == "Business Intelligence Reports") {
             url = "reports/operation/get/maincategory/" + name;
            callHttp(url, reportType.BI);
        } else if (name == "Forensic Report") {
             url = "reports/operation/get/maincategory/" + name;
            callHttp(url, reportType.Forensic);
        }
    };

    $(document).ready(function () {
        if ($(location).attr('href').indexOf("reporting/home") > -1) {
            AbsenceSoft.PopularReport.getPopularReports("Operations Report");
            AbsenceSoft.PopularReport.getPopularReports("Business Intelligence Reports");
            AbsenceSoft.PopularReport.getPopularReports("Forensic Report");
        }
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);