﻿(function (AbsenceSoft, $) {
    'use strict';
    AbsenceSoft.SavedReport = AbsenceSoft.SavedReport || {};
    var utils = AbsenceSoft.Utilities;
    //define the module
    var module = "reporting";
    
    //Create the Report Card dynamically 
    var createReportCard = function (data, howmany) {
        var divHtml = document.createElement('div');        
        for (var i = 0; i < howmany; i++) {            
            var block = $("#SharedReportCard").clone().attr('id', 'savedreportcard' + data[i].id);
            var newHtml = getHtml(block, data[i]);
            $(divHtml).append(newHtml);
        }
        $("#saved-report").html(divHtml);
        $(".saved-report-container").removeClass("hide").addClass("show");
    };

    //get the html from template
    var getHtml = function (oldHtml, data) {
        var reportData = JSON.parse(data.criteriaJson);
        $(oldHtml).find('.shared-report-title').text(data.reportName);
        $(oldHtml).find('.shared-report-description').text(reportData.reportDefinition.name);
        $(oldHtml).find('.shared-report-card').prop('id', 'cardflip' + data.id);
        $(oldHtml).find('.shared-title').attr('data-target', '#cardflip' + data.id);
        $(oldHtml).find('.shared-title-flipIcon').attr('data-target', '#cardflip' + data.id);        
        if (data.lastRequestId)
        {
            $(oldHtml).find('.shared-report-go').prop('href', '/reporting/run/?reportId=' + reportData.reportId + '&sharedreportid=' + data.id + '&requestid=' + data.lastRequestId + '&type=saved'); 
            $(oldHtml).find('.shared-tile-navigation').prop('href', '/reporting/run/?reportId=' + reportData.reportId + '&sharedreportid=' + data.id + '&requestid=' + data.lastRequestId + '&type=saved'); 
        }
        else
        {
            $(oldHtml).find('.shared-report-go').prop('href', '/reporting/run/?reportId=' + reportData.reportId + '&sharedreportid=' + data.id + '&type=saved'); 
            $(oldHtml).find('.shared-tile-navigation').prop('href', '/reporting/run/?reportId=' + reportData.reportId + '&sharedreportid=' + data.id + '&type=saved'); 
        }
               
        return oldHtml;
    };

    //get all shared report
    var getSavedReports = function () {
        var endPoint = "reports/operation/saved";
        try {
            AbsenceSoft.HttpUtils.Get(module, endPoint, function (result) {
                if (!utils.isNullOrWhitespace(result)) {
                    var resultData = JSON.parse(result);
                    if (resultData && resultData.success === true && resultData.length !== 0) {
                        if ($("#NoOfCardsForSavedReports").val() === "all") {
                            createReportCard(resultData.data, resultData.data.length);
                        }
                        else {
                            if (resultData.data.length > 4) {
                                $(".ActiveIfCount").show();
                            }
                            else {
                                $(".ActiveIfCount").hide();
                            }
                            if (resultData.data.length > 0) {
                                var howmany = (resultData.data.length < 4) ? resultData.data.length : 4;
                                createReportCard(resultData.data, howmany);
                            }
                        }
                    }

                    else {
                        console.log(resultData.message);
                    }
                }
            });
        }               
        catch (ex) {
            console.log(ex);
        }
    };
   
    $(document).ready(function () {
        if ($(location).attr('href').indexOf("reporting/home") > -1) {
            getSavedReports();
        }
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);