﻿(function (AbsenceSoft, $) {
    AbsenceSoft.ReportHome = AbsenceSoft.ReportHome || {};

    var utils = AbsenceSoft.Utilities;
    var shownCategories = [];
    var operationReportData = "";
    var biReportData = "";
    var adhocReportData = "";
    var forensicReportData = "";

    var reportType = {
        Operation: 'operation', BI: 'bi', Adhoc: 'adhoc', Forensic: 'forensic'
    };

    var menuBeingShown = function () {
        var category = $(this);
        if (category.attr('name').toUpperCase() === "OSHA REPORT") {
            $('#oshaflyout').hide();
            var rootPath = "/reports/oshareports";
            window.location.assign(rootPath);
        } else if (category.attr('name').toUpperCase() === "ADHOC REPORT") {
            $('#adhocflyout').hide();
            var rootPath = "/reporting/adhoc";
            window.location.assign(rootPath);
        } else{
            loadCategory(category);
            hideOtherMenus(category);
        }
    };

    var hideAllMenus = function () {
        $('.report-flyout.in').collapse('hide');
        $('.report-menu-toggles').removeClass('active');
    };

    var hideOtherMenus = function (category) {
        var id = category.attr('id');
        $('.report-flyout.in').not('#' + id).collapse('hide');
    };

    //load the categories on click
    var loadCategory = function (category) {
        var name = category.attr('name');
        if (shownCategories.indexOf(name) > -1) {
            return;
        }

        var contentArea = category.find('.report-category-content');
        contentArea.html('<p class="load-save text-white">Loading<span>.</span><span>.</span><span>.</span></p>');

        if (name == "Operations Report") {
            var url = "reports/operation/get/maincategory/" + name;
            callHttp(category, operationReportData, url, reportType.Operation);

        } else if (name == "Business Intelligence Reports") {
            var url = "reports/operation/get/maincategory/" + name;
            callHttp(category, biReportData, url, reportType.BI);

        //} else if (name == "AdHoc Report") {
        //    var url = "reports/adhoc/getall";
        //    callHttp(category, adhocReportData, url, reportType.Adhoc);

        } else if (name == "Forensic Report") {
            var url = "reports/operation/get/maincategory/" + name;
            callHttp(category, forensicReportData, url, reportType.Forensic);
        }
        else {
            contentArea.html('<ul class="report-list"><li class="single-report-item">No record(s) found</li></ul>');
        }
    };

    //call respective http
    var callHttp = function (category, data, url, type) {
        var contentArea = category.find('.report-category-content');
        var searchText = category.find(".report-category-search");
        searchText.keyup(function () {
            generateReportList(contentArea, data, type, $(this).val());
        });

        if (utils.isNullOrWhitespace(data)) {
            AbsenceSoft.HttpUtils.Get("reporting", url, function (result) {
                if (!utils.isNullOrWhitespace(result)) {
                    var d = JSON.parse(result);
                    if (d.success == true) {
                        data = d.data;
                        generateReportList(contentArea, data, type);
                    }
                }
            });
        }
        else {
            generateReportList(contentArea, data, type, searchText.val());
        }
    }

    //generate the report list
    var generateReportList = function (contentArea, data, type, searchText) {
        var html = generateHtml(data, searchText, type);
        contentArea.empty();
        if (html && data.length > 0) {
            contentArea.html(html);
        } else {
            contentArea.html('<ul class="report-list"><li class="single-report-item">No record(s) found</li></ul>');
        }
    }

    //generate the HTML for report list
    var generateHtml = function (data, searchText, type) {
        var html = '<ul class="report-list">';
        var d = [];
        if (utils.isNullOrWhitespace(searchText)) {
            d = data;   
        }
        else {
            d = _.toArray(_.filter(data, function (o) {
                return !utils.isNullOrWhitespace(o.name) && o.name.toLowerCase().lastIndexOf(searchText.toLowerCase()) > -1;
            }));
        }

        if (d && d.length > 0) {
            //get categories
            var categories = _.toArray(_.sortBy((_.toArray(_.groupBy(d, 'category'))), function (o) { return o.length; }));
            if (categories.length > 0) {
                for (var i = 0; i < categories.length; i++) {
                    var c = categories[i];
                    if (c.length == 1) {
                        var title = c[0].name ? c[0].name : c[0].category;
                        if (!utils.isNullOrWhitespace(title) && !utils.isNullOrWhitespace(c[0].id)) {
                            if (type === "adhoc") {
                                tmp += '<li id="' + r.id + '" class="single-report-item"><a href="/reports/adhocreport/' + c[0].id + '">' + r.name + '</a></li>';
                            }
                            else {
                                html += '<li id="' + c[0].id + '" class="single-report-item"><a href="/reporting/run/?reportId=' + c[0].id + '&type=' + type + '">' + title + '</a></li>';
                            }
                        }
                    } else {
                        var tmp = "";
                        if (!utils.isNullOrWhitespace(c[0].category)) {
                            tmp += '<li class="category-item">' + c[0].category + '</li>';
                            tmp += '<ul class="category-container">';
                        }
                        var hasChild = 0;

                        //list reports
                        for (var j = 0; j < c.length; j++) {
                            var r = c[j];
                            var title = r.name ? r.name : r.category;
                            if (!utils.isNullOrWhitespace(r.id) && !utils.isNullOrWhitespace(title)) {
                                if (!utils.isNullOrWhitespace(c[0].category)) {
                                    if (type === "adhoc") {
                                        tmp += '<li id="' + r.id + '" class="single-report-item"><a href="/reports/adhocreport/' + r.id + '">' + r.name + '</a></li>';
                                    }
                                    else {
                                        tmp += '<li id="' + r.id + '" class="report-item"><a href="/reporting/run/?reportId=' + r.id + '&type=' + type + '">' + r.name + '</a></li>';
                                    }
                                }
                                else {
                                    if (type === "adhoc") {
                                        tmp += '<li id="' + r.id + '" class="single-report-item"><a href="/reports/adhocreport/' + r.id + '">' + r.name + '</a></li>';
                                    }
                                    else {
                                        tmp += '<li id="' + r.id + '" class="single-report-item"><a href="/reporting/run/?reportId=' + r.id + '&type=' + type + '">' + r.name + '</a></li>';
                                    }
                                }
                                hasChild += 1;
                            }
                        }
                        if (!utils.isNullOrWhitespace(c[0].category)) {
                            tmp += '</ul>';
                        }

                        if (hasChild > 0) {
                            html += tmp;
                        }
                    }
                }
            }
        }
        html += '</ul>';
        return html;
    }


    var addActiveClass = function () {
        var isActive = $(this).hasClass('active');
        $('.report-menu-toggles').removeClass('active');

        if (!isActive) {
            $(this).addClass('active');
        }
    };

    $(document).ready(function () {
        if ($('.report-flyout').length < 1)
            return;

        $('.report-flyout').on('show.bs.collapse', menuBeingShown);
        $('.report-menu-toggles').click(addActiveClass);

        $("#report-settings-box").hide();
        $("#report-settings-btn").click(function () {         
            $("#report-settings-box").toggle();
        });

        $(function () {
            $('#settings').tooltip();
        });
       
      
    });

    $(document).on('click', function (e) {
        var openedMenu = $('.report-flyout.in');
        if (openedMenu != null && openedMenu.length > 0 && !$(e.target).closest('.report-flyout.in').length) {
            $(openedMenu).collapse('toggle');
        }
    });


})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);