﻿(function (AbsenceSoft, $) {
    "use strict";

    // Enum that will tell what kind of chart is required
    var ChartEnum = {
        html: 1,
        chart: 2
    };

    //Entry point for plugin
    $.fn.drawChart = function (options) {
        var opt = $.extend({}, $.fn.defaults, options);
        if (opt.chartType == ChartEnum.html) {
            return createGrid(this, opt);
        }
        if (opt.chartType == ChartEnum.chart) {
            return createChart(this, opt);
        }
        return this;
    };


    // Create the Chart 
    function createChart(obj, opt) {
        google.charts.load('current', { packages: ['corechart'] });
        google.charts.setOnLoadCallback(
            function () {
                DrawChart(obj, opt);
            });
    }

    // Call back for draw chart
    function DrawChart(obj, option) {
        var seriesData = option.data;
        console.log(seriesData);
        /// early exit if there is no data for the chart to display
        /// Google pitches a fit if we send empty data
        if (seriesData.length <= 1) {           
            return obj.html("No results found.");
        }

        var data = google.visualization.arrayToDataTable(seriesData);

        var options = option.chart;
        var chartOptions = {
            'height': options.height,
            'legend': { position: 'bottom', alignment: 'center', maxLines: 5, textStyle: { fontSize: 12 } },
            'is3D': options.is3D,
            'isStacked': options.isStacked,
            'bar': { groupWidth: '60' }
        }
        var mainDiv = $("<div>").prop("id", "theChart");
        var chartWrapper = new google.visualization.ChartWrapper({
            'chartType': options.type,
            'containerId': $(obj).attr('id'),
            'options': chartOptions
        });

        chartWrapper.setDataTable(data);
        chartWrapper.draw();
        return obj;
    }


    //Converts the word Pascal case
    function camelCaseToSpace(input) {
        return input
            .replace(/([A-Z])/g, " $1")
            .toLowerCase()
            .replace(/\b[a-z]/g, function (txtVal) {
                return txtVal.toUpperCase();
            });
    }

    //Convert the object to link
    function convertToCorrectFormat(input) {
        if (!input) {
            return '';
        }
        if ($.type(input) == "object") {
            return '<a href="' + input.Link + '">' + input.Value + '</a>';
        }
        
        return input;
    }

    // Get the columns name
    function getColumns(data) {
        var tempCol = [];
        for (var key in data) {
            if (tempCol.indexOf(key) === -1) {
                tempCol.push(key);
            }
        }
        return tempCol;
    }

    // Create the grid based on data
    function createTabularData(data, colsName) {
        var ta = $("<table>").addClass("table table-striped table-bordered table-hover table-condensed table-20");
        var thead = $("<thead>");
        var tr = $("<tr>").addClass("head-border");
        for (var i3 = 0; i3 < colsName.length; i3++) {
            $(tr).append(
                '<th class="data-border">' + camelCaseToSpace(colsName[i3]) + "</th>"
            );
        }
        $(thead).append(tr);
        $(ta).append(thead);
        var tb = $("<tbody>");
        for (var i4 = 0; i4 < data.length; i4++) {
            var tr1 = $("<tr>").addClass("head-border");
            for (var j = 0; j < colsName.length; j++) {
                $(tr1).append(
                    "<td class='data-border'>" +
                    convertToCorrectFormat(data[i4][colsName[j]]) +
                    "</td>"
                );
            }
            $(tb).append(tr1);
        }
        $(ta).append(tb);
        return ta;
    }

    //Create the Grid from plug in
    function createGrid(obj, opt) {
        var mainDiv = $("<div>").addClass(
            "text-primary report-group-label"
        );
        var headerDiv = $("<div>");
        var col = [];
        var data = opt.data;
        if (!opt.isGroup) {
            col = getColumns(data[0]);
            $(mainDiv).append(createTabularData(data, col));
        } else {
            col = getColumns(data[0].Items[0]);
            for (var i = 0; i < data.length; i++) {
                var label = $("<label>")
                    .text(data[i].Label)
                    .prop("id", "groupSection" + i);
                $(mainDiv).append(label);
                $(headerDiv).append(
                    '<a class="report-toc" href="#groupSection' +
                    i +
                    '">' +
                    data[i].Label +
                    "</a> | "
                );
                $(mainDiv).append(createTabularData(data[i].Items, col));
            }
            $(mainDiv).prepend(headerDiv);
        }
        return obj.html(mainDiv);
    }

    //Plug in default settings
    $.fn.defaults = {
        chartType: ChartEnum.html,
        isGroup: false
    };
})((window.AbsenceSoft = window.AbsenceSoft || {}), jQuery);
