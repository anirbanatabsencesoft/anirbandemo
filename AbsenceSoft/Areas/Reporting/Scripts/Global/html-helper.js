﻿(function (AbsenceSoft, $) {
    'use strict';
    AbsenceSoft.HtmlHelper = AbsenceSoft.HtmlHelper || {};

    //generate html
    AbsenceSoft.HtmlHelper.GenerateHtml = function (d) {
        switch (d.controlType) {
            case 0:
                return generateTextBox(d, true, false, false, false);
            case 1:
                return generateTextBox(d, false, false, false, false);
            case 2:
                return generateSelectList(d, false, false, true);
            case 3:
                return generateTextArea(d);
            case 4:
                return generateTextBox(d, false, true, false, false);
            case 5:
            case 6:            
                return generateTextBox(d, false, false, true, false);
            case 7:
                return generateRadioButtonList(d, false);
            case 8:
                return generateCheckbox(d);
            case 9:           
                return generateSelectList(d, true, true, true);
            case 10:
                return generateTextBox(d, false, false, false, true);
            case 12:
                return generateSelectList(d, true, false, true);
            case 11:
                return generateSelectList(d, false, false, false);
        }
    }

    //generate text box
    var generateTextBox = function (d, isReadonly, isDate, isNumeric, isHidden) {
        var type = 'text';
        if (isNumeric == true) {
            type = 'number';
        } else if (isHidden == true) {
            type = 'hidden';
        }
        var html = '<input type="' + type + '" name="' + d.fieldName + '" id="' + d.fieldName + '"';
        if (isReadonly == true) {
            html += ' readonly="true"';
        }
        if (isDate == true) {
            html += ' data-provide="datepicker-inline"'
        }

        html += ' />';
        return html;
    }

    //generate select
    var generateSelectList = function (d, isMultiple, ischeckList, isshowAll) {
        var html = '<select  name="' + d.fieldName + '" id="' + d.fieldName + '" ';
        if (isMultiple == true) {
            html += ' multiple';
        }

        if (ischeckList == true) {
            html += ' class="form-control select2-multiple"';
        }
        html += '>';

        if (isMultiple != true && isshowAll == true) {
            html += '<option value="">All</option>';
        }

        for (var i = 0; i < d.options.length; i++) {
            html += '<option value="' + d.options[i].value + '">' + d.options[i].text + '</option>';
        }

        html += '</select>'
        return html;
    }

    //text area
    var generateTextArea = function (d) {
        return '<textarea  name="' + d.fieldName + '" id="' + d.fieldName + '"></textarea>';
    }

    //generate checkbox
    var generateCheckbox = function (d) {
        return '<input type="check"  name="' + d.fieldName + '" id="' + d.fieldName + '" />';
    }  

    //generate radio button list
    var generateRadioButtonList = function (d, isCheckboxList) {
        var html = "";
        if (d.options.length > 0) {
            html = '<div class="btn-group" data-toggle="buttons">';

            if (isCheckboxList != true) {
                html += '<label class="btn report-criteria-btn criteria-selected active" onclick="AbsenceSoft.BuildCriteria.makeActive(this)"><input type="radio" name="' + d.fieldName + '" value="">All</label>';
            }

            for (var i = 0; i < d.options.length; i++) {
                html += '<label class="btn report-criteria-btn"';
                if (isCheckboxList == true) {
                    html += ' onclick="AbsenceSoft.BuildCriteria.makeMultiActive(this)">';
                } else {
                    html += ' onclick="AbsenceSoft.BuildCriteria.makeActive(this)">';
                }
                html += '<input type="radio" name="' + d.fieldName + '" value="' + d.options[i].value + '"> ' + d.options[i].text;
                html += '</label>';
            }
            html += '</div>';
        }
        return html;
    }

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);