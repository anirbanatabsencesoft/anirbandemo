﻿(function (AbsenceSoft, $) {
    AbsenceSoft.Utilities = AbsenceSoft.Utilities || {};

    //check for null or undefined
    AbsenceSoft.Utilities.isNullOrWhitespace = function (obj) {
        try {
            if (obj == null) {
                return true;
            } else if (typeof (obj) == 'undefined') {
                return true;
            } else if (AbsenceSoft.Utilities.isJson(obj)) {
                return false;
            } else if (obj.trim() == '') {
                return true;
            }
        }
        catch (e) {
            return false;
        }
    }

    //check whether is json
    AbsenceSoft.Utilities.isJson = function (str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    //get the query string param
    AbsenceSoft.Utilities.getQueryParamFromUrl = function (name) {
        if (name && window.location.search) {
            var match = RegExp('[?&]' + name.toLowerCase() + '=([^&]*)').exec(window.location.search.toLowerCase());
            return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
        }
        return "";
    }

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);