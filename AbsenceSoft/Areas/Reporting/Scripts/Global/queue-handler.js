﻿(function (AbsenceSoft, $) {
    'use strict';
    AbsenceSoft.Queue = AbsenceSoft.Queue || {};

    //local variables
    var availableTasks = [];
    var executedTaskIds = [];
    var currentUserId = "";
    var runningTasks = {};
    var taskStatus = {};
    var taskMessage = {};
    var taskNames = {};
    var queueInterval = null;
    var requestReportCancelEndPoint = 'reports/cancel';  
    var reportId = "";
    AbsenceSoft.Queue.notyTimeout = 3000;

    //default settings for timer as 2 secs
    var lookbackTime = 2000;

    //start the queue manager
    var startQueue = function () {    
        if (!currentUserId) {
            //get the current user id
            $.get("/current/user", function (data) {
                var d = JSON.parse(data);
                currentUserId = d.id;
                AbsenceSoft.QueueStorage.GetInitialHistoryData(currentUserId);
                AbsenceSoft.QueueStorage.GetInitialQueueData(currentUserId);
            });
        }

        if (currentUserId) {
            showHistory();
            availableTasks = AbsenceSoft.QueueStorage.GetAll(currentUserId);

            if (availableTasks.length > 0) {
                //check whether the tasks are already running or not
                var exeTasks = $.grep(availableTasks, function (e) {
                    if (e.requestId) {
                        return executedTaskIds.includes(e.requestId) === false;
                    }
                });

                if (exeTasks) {
                    $(exeTasks).each(function (i, e) {
                        if (e.requestId) {
                            startTask(e);
                            executedTaskIds.push(e.requestId);
                        }
                    });
                }
            }
        }
    }

    //start a new task to check the queue process status
    var startTask = function (e) {
        if (e.requestId) {
            runningTasks[e.requestId] = setInterval(function () { checkStatus(e) }, lookbackTime);
        }
    }

    //check status
    var checkStatus = function (e) {
        if (e.requestId) {
            AbsenceSoft.HttpUtils.Get("reporting", "reports/status/" + e.requestId, function (data) {
                var d = JSON.parse(data);
                if (d.success === true) {
                    taskStatus[e.requestId] = d.data.status;
                    taskMessage[e.requestId] = e.msg;
                    taskNames[e.requestId] = e.name;
                    setStatus();

                    if (d.data.status == 6303)
                    {
                        removeCurrentTask(e.requestId);
                        $.noty.closeAll();                     
                        var type = "ui";
                        var path = "";
                        
                        //notify
                        var checkFormat = AbsenceSoft.Session.GetById(currentUserId, e.requestId);
                        if (checkFormat && checkFormat.data.requestedExportFormat < 3) {
                            var reportFormat = checkFormat.data.requestedExportFormat;
                            var aText = '<b>Report generation completed.<b><br/>';
                            var aLink = '<a href="#" onclick="return AbsenceSoft.Queue.CreateLink(' + e.requestId + ',' + reportFormat + ')">Download Report</a>';
                            noty({ type: 'success', text: aText + aLink, layout: 'topRight', timeout: AbsenceSoft.Queue.notyTimeout });
                            type = reportFormat;
                        }
                        else if (checkFormat && checkFormat.data.reportType==="adhoc")
                        {
                            var reporttypeid = checkFormat.data.requestedData.reportDefinition.ReportType.Id;
                            noty({ type: 'success', text: '<b>Report generation completed.<b><br/><a href="/reporting/adhoc/adhocreport?reporttypeid=' + reporttypeid+'&requestid=' + e.requestId + '&type=adhoc">Show Result</a>', layout: 'topRight', timeout: AbsenceSoft.Queue.notyTimeout });                            
                        }
                        else 
                        {
                            noty({ type: 'success', text: '<b>Report generation completed.<b><br/><a href="/reporting/run/?requestid=' + e.requestId + '&type=result">Show Result</a>', layout: 'topRight', timeout: AbsenceSoft.Queue.notyTimeout });
                        }
                        
                    } else if (d.data.status == 6305) {
                        removeCurrentTask(e.requestId);
                        $.noty.closeAll();

                        var type = "ui";
                        var path = "";

                        noty({ type: 'error', text: '<b>Report generation failed. Please try again later.</b>', layout: 'topRight', timeout: AbsenceSoft.Queue.notyTimeout });
                      
                    }
                    //add to history - for success and failed reports only 
                    if (d.data.status == 6303 || d.data.status == 6305 ) {
                        AbsenceSoft.QueueStorage.AddToHistory(currentUserId, e.requestId, e.name, AbsenceSoft.Queue.getStatus(taskStatus[e.requestId]), AbsenceSoft.Queue.ShowMessage(taskMessage[e.requestId]), reportId, type, path);
                    }
                }
            });
        }
    }

    //remove the current task and task id
    var removeCurrentTask = function (id) {
        clearInterval(runningTasks[id]);
        delete runningTasks[id];
        AbsenceSoft.QueueStorage.Remove(currentUserId, id);       
    }

    //set the HTML status
    var setStatus = function () {
        var html = '<table class="table table-hover .table-striped .table-bordered">';
        html += '<thead><tr><td>Report</td><td>Current Status</td><td>Query</td><td>Last Checked</td><td>Action</td></tr></thead>';
        html += '<tbody>';
        Object.keys(taskStatus).forEach(function (id) {
            var idhtm = taskNames[id];            
            if (taskStatus[id] === 6303) {   
                var rData = AbsenceSoft.Session.GetById(currentUserId, id);
                if (rData && rData.data.requestedExportFormat<3)
                 {
                    var reportFormat = rData.data.requestedExportFormat;
                    var reportName = reportFormat == 0 ? taskNames[id] + '-PDF' : taskNames[id] + '-CSV';
                    idhtm = '<a href="#" onclick="return AbsenceSoft.Queue.CreateLink(' + id + ',' + reportFormat + ')">' + reportName + '</a>';
                }
                else if (rData && rData.data.reportType === "adhoc") {
                    var reporttypeid = rData.data.requestedData.reportDefinition.ReportType.Id;
                    idhtm = '<a href="/reporting/adhoc/adhocreport?reporttypeid='+ reporttypeid+'&requestid='+ id +'&type=adhoc">' + taskNames[id] + '</a>';
                   
                }
                else {
                    idhtm = '<a href="/reporting/run/?requestid=' + id + '&type=result">' + taskNames[id] + '</a>';
                }
            }
            html += '<tr><td>' + idhtm + '</td><td>' + AbsenceSoft.Queue.getStatus(taskStatus[id]) + '</td><td>' + AbsenceSoft.Queue.ShowMessage(taskMessage[id]) + '</td><td>' + moment().format('MMMM Do YYYY, h:mm:ss a') + '</td><td><a onclick="AbsenceSoft.Queue.Remove(' + id + ', this)">Remove</a></td></tr>';
        });
        html += '</tbody></table>';

        $("#queue-current-status").html(html);
    }

    //show modal dialog
    AbsenceSoft.Queue.ShowStatusModal = function () {
        $("#queueStatusModal").modal();
    }

    //show modal dialog
    AbsenceSoft.Queue.CreateLink = function (requestId, reportFormat) {
        window.open(window.location.origin + '/service/download/?Component=' + AbsenceSoft.ReportOperations.Module + '&EndPoint=reports/getfilepath/' + requestId + "/" + reportFormat + '&Method=GET', "_self");      
    }

    //show message
    AbsenceSoft.Queue.ShowMessage = function (msg) {
        if (msg) {
            return msg;
        } else {
            return '';
        }
    }

    //remove a queue
    AbsenceSoft.Queue.Remove = function (id, obj) {
        //stop queue first
        clearInterval(queueInterval);

        //do operations
        removeCurrentTask(id);
        delete taskStatus[id];
        delete taskNames[id];
        delete taskMessage[id];
        $(obj).parent().parent().remove();
        AbsenceSoft.Session.Remove(currentUserId, id);
        //start queue again
        queueInterval = setInterval(startQueue, lookbackTime);
    }


    //remove a queue
    AbsenceSoft.Queue.CancelRequest = function (id, obj) {

        var requestObject = "reportRequestId";//reportRequestId:" + id;
        AbsenceSoft.HttpUtils.Post(AbsenceSoft.ReportOperations.Module, requestReportCancelEndPoint + "/" + id, requestObject, function (data) {
            //add the data to queue manager
            var d = JSON.parse(data);
            if (d.success === true) {
                noty({ type: 'success', text: '<b>Report request cancelled.</b>', layout: 'topRight', timeout: AbsenceSoft.Queue.notyTimeout });
                //do operations
                removeCurrentTask(id);
                delete taskStatus[id];
                delete taskNames[id];
                delete taskMessage[id];
                $(obj).parent().parent().remove();
                AbsenceSoft.Session.Remove(currentUserId, id);
            }
            else {
                noty({ type: 'error', text: '<b>Could not cancel Report request.</b>', layout: 'topRight', timeout: AbsenceSoft.Queue.notyTimeout });
            }
        });

        //start queue again
        queueInterval = setInterval(startQueue, lookbackTime);
    }

    //get the status text from enum
    AbsenceSoft.Queue.getStatus = function (i) {
        switch (i) {
            case 6301:
                return 'Pending';
            case 6302:
                return 'Processing';
            case 6303:
                return 'Completed';
            case 6304:
                return 'Canceled';
            case 6305:
                return 'Failed';
            case 6306:
                return 'Scheduled';
            default:
                return "Unknown status";
        }
    }

    //show the earlier history
    var showHistory = function () {
        var history = AbsenceSoft.QueueStorage.GetHistory(currentUserId);
        var html = '<table class="table table-hover .table-striped .table-bordered">';
        html += '<thead><tr><td>Request Id</td><td>Current Status</td><td>Comments</td><td>Last Updated</td></tr></thead>';
        html += '<tbody>';
        if (history.length > 0) {
            $(history).each(function (i, h) {
                var id = h.requestId;
                var reportId = h.reportId;
                var idhtm = h.name;
                var isFile = h.type != "ui";
                var hcomments = h.comments == null ? "" : h.comments;
                if (h.status === 6303) {
                    if (isFile) {
                        var reportFormat = h.type.toUpperCase();
                        var reportName = h.name + '-' + reportFormat.toUpperCase();
                        idhtm = '<a href="#" onclick="return AbsenceSoft.Queue.CreateLink(' + id + ',' + (reportFormat == "pdf" ? 0 : 1) + ')">' + reportName + '</a>';
                    }
                    else {
                        idhtm = '<a href="/reporting/run/?requestid=' + id + '&reportid=' + reportId + '&type=result">' + h.name + '</a>';
                    }
                }
                if (h.status >= 6301 && h.status <= 6306) {
                    html += '<tr><td>' + idhtm + '</td><td>' + AbsenceSoft.Queue.getStatus(h.status) + '</td><td>' + hcomments + '</td><td>' + h.lastUpdated + '</td></tr>';
                }
            });
        } else {
            html += '<tr><td colspan="4">No record(s) found!</td></tr>';
        }
        html += '</tbody></table>';

        $("#queue-history").html(html);
    }
     
    //clear history
    AbsenceSoft.Queue.clearHistory = function () {
        var ids = _.map(AbsenceSoft.QueueStorage.GetHistory(currentUserId), 'id');
        AbsenceSoft.Session.RemoveList(currentUserId, ids);
        AbsenceSoft.QueueStorage.ClearHistory(currentUserId);
        var html = '<table class="table table-hover .table-striped .table-bordered">';
        html += '<thead><tr><td>Request Id</td><td>Current Status</td><td>Comments</td><td>Last Checked</td></tr></thead>';
        html += '<tbody>';
        html += '<tr><td colspan="4">No record(s) found!</td></tr>';
        html += '</tbody></table>';
        $("#queue-history").html(html);
    }

    var CheckUserIsAuthenticated = function ()
    {
        $.ajax({
            url: '/Administration/GetUserIsAuthenticatedOrNot',
            success: setQueueTask,
            error: function (err) { console.log(err); }
        });
    }

    var setQueueTask = function (data)
    {
        if (data.IsUserAuthenticated) {
            queueInterval = setInterval(startQueue, lookbackTime);
            $("#clear-history").click(AbsenceSoft.Queue.clearHistory);
        }
    }

    //initialize
    $(document).ready(function () {
        if ($(location).attr('href').toLowerCase().indexOf("login") === -1) {
            CheckUserIsAuthenticated();
            reportId = AbsenceSoft.Utilities.getQueryParamFromUrl("reportid");
        }
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);