﻿(function (AbsenceSoft, $) {
    'use strict';
    AbsenceSoft.QueueStorage = AbsenceSoft.QueueStorage || {};

    //get initial history data
    AbsenceSoft.QueueStorage.GetInitialQueueData = function (userid) {
        var ids = [];
        AbsenceSoft.HttpUtils.Get("reporting", "reports/request/history?limit=20&queuedonly=true", function (result) {
            try {
                var data = JSON.parse(result).data;
                if (data && data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        var d = data[i];
                        ids.push({ "requestId": d.id, "reportId": d.reportId, "name": d.reportName, "status": d.status, "comments": d.criteriaPlainEnglish, "lastUpdated": moment(d.lastModified).format('MMMM Do YYYY, h:mm:ss a'), "type": "ui", "path": "" });
                    }
                    sessionStorage.setItem(userid + "_REPORT_QUEUE", JSON.stringify(ids));
                }
            }
            catch (ex) {
                console.log('Unable to retrieve queue data');
            }
        });
    }

    //Add to queue id to store
    AbsenceSoft.QueueStorage.Add = function (userid, requestId, name, status, msg, reportId, type, path) {
        var ids = AbsenceSoft.QueueStorage.GetAll(userid);
        ids.push({ "requestId": requestId, "reportId": reportId, "name": name, "status": status, "comments": msg, "lastUpdated": moment().format('MMMM Do YYYY, h:mm:ss a'), "type": type, "path": path });
        sessionStorage.setItem(userid + "_REPORT_QUEUE", JSON.stringify(ids));
    }

    //Get all available ids queued
    AbsenceSoft.QueueStorage.GetAll = function (userid) {
        var sd = sessionStorage.getItem(userid + "_REPORT_QUEUE");
        if (sd) {
            try
            {
                return JSON.parse(sd);
            }
            catch (e) {  }
        }
        return [];
    }

    //remove the id from queue once notified
    AbsenceSoft.QueueStorage.Remove = function (userid, id) {
        var ids = AbsenceSoft.QueueStorage.GetAll(userid);
        ids = $.grep(ids, function (e, i) {
            return e.id != id;
        });
        sessionStorage.setItem(userid + "_REPORT_QUEUE", JSON.stringify(ids));
    }


    //get initial history data
    AbsenceSoft.QueueStorage.GetInitialHistoryData = function (userid) {
        AbsenceSoft.QueueStorage.ClearHistory(userid);
        var h = [];
        AbsenceSoft.HttpUtils.Get("reporting", "reports/request/history?limit=10&queuedonly=false", function (result) {
            try {
                var data = JSON.parse(result).data;
                if (data && data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        var d = data[i];
                        var type = "ui";
                        var path = "path";
                        if (d.resultCsvFilePath) {
                            type = "csv";
                            path = d.resultCsvFilePath;
                        } else if (d.resultPdfFilePath) {
                            type = "pdf";
                            path = d.resultPdfFilePath;
                        }

                        h.push({ "requestId": d.id, "reportId": d.reportId, "name": d.reportName, "status": d.status, "comments": d.criteriaPlainEnglish, "lastUpdated": moment(d.lastModified).format('MMMM Do YYYY, h:mm:ss a'), "type": type, "path": path });
                    }
                    sessionStorage.setItem(userid + "_REQUEST_HISTORY", JSON.stringify(h));
                }
            }
            catch (ex) {
                console.log('Unable to retrieve history');
            }
        });
    }

    //add data to request history
    AbsenceSoft.QueueStorage.AddToHistory = function (userid, requestId, name, status, msg, reportId, type, path) {
        var h = AbsenceSoft.QueueStorage.GetHistory(userid);
        h.push({ "requestId": requestId, "reportId": reportId, "name": name, "status": status, "comments": msg, "lastUpdated": moment().format('MMMM Do YYYY, h:mm:ss a'), "type": type, "path": path });
        sessionStorage.setItem(userid + "_REQUEST_HISTORY", JSON.stringify(h));
    }

    //get the history data
    AbsenceSoft.QueueStorage.GetHistory = function (userid) {
        var historyString = sessionStorage.getItem(userid + "_REQUEST_HISTORY");
        if (historyString) {
            try {
                return JSON.parse(historyString);
            }
            catch (e) { }
        }
        return [];
    }

    //clear history
    AbsenceSoft.QueueStorage.ClearHistory = function (userid) {
        sessionStorage.setItem(userid + "_REQUEST_HISTORY", JSON.stringify([]));
    }

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);