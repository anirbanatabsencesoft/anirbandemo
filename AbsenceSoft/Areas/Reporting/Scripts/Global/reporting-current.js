﻿(function (AbsenceSoft, $) {
    'use strict';

    AbsenceSoft.Current = AbsenceSoft.Current || { };

    //expose variables
    AbsenceSoft.Current.User = { };
    AbsenceSoft.Current.Employer = { };
    AbsenceSoft.Current.Customer = { };
    AbsenceSoft.Current.Contacts = { };
    AbsenceSoft.Current.Roles = { };
    AbsenceSoft.Current.Teams = {};
    AbsenceSoft.Current.Users = {};

    //get all users
    var getAllUsers = function () {
        $.get('/Administration/GetUsersList', function (data) {
            AbsenceSoft.Current.Users = data;
        });
    }
    
    //get current data
    var getCurrentUser = function () {
        $.get("/current/user", function (data) {
            AbsenceSoft.Current.User = JSON.parse(data);
            if (AbsenceSoft.Current.User.employeeId != null && typeof (AbsenceSoft.Current.User.employeeId) != 'undefined') {
                $.get("/Case/Employee/" + user.employeeId + "/GetAllEmployeeContacts", function (contacts) {
                    AbsenceSoft.Current.Contacts = contacts
                });
            }
        });
    }

    var getCurrentEmployer = function () {
        $.get("/current/employer", function (data) {
            AbsenceSoft.Current.Employer = data;
        });
    }

    var getCurrentCustomer = function () {
        $.get("/current/customer", function (data) {
            AbsenceSoft.Current.Customer = JSON.parse(data);
        });
    }

    var getCurrentRoles = function () {
        $.get("/Administration/GetRoles", function (data) {
            AbsenceSoft.Current.Roles = data;
        });
    }

    var getCurrentTeams = function () {
        $.get("/current/team", function (data) {
            AbsenceSoft.Current.Teams = data;
        });
    }
    
    //call on ready
    AbsenceSoft.Current.getCurrentData = function () {
        getCurrentUser();
        getCurrentEmployer();
        getCurrentCustomer();
        getCurrentRoles();
        getCurrentTeams();
        getAllUsers();
    };

    
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);