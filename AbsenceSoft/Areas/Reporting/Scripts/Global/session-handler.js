﻿(function (AbsenceSoft, $) {
    'use strict';
    AbsenceSoft.Session = AbsenceSoft.Session || {};

    //Add
    AbsenceSoft.Session.Add = function (userid, requestid, data) {
        var ids = AbsenceSoft.Session.GetAll(userid);
        ids.push({ id: requestid, data: data });
        localStorage.setItem(userid + "_REPORT_DATA", JSON.stringify(ids));      
    }

    //Get all available ids queued
    AbsenceSoft.Session.GetAll = function (userid) {
        var sd = localStorage.getItem(userid+"_REPORT_DATA");
        if (sd) {
            try {
                return JSON.parse(sd);
            }
            catch (e) { }
        }
        return [];
    }
    //Get
    AbsenceSoft.Session.GetById = function (userid, requestid) {
        var ids = AbsenceSoft.Session.GetAll(userid);
        return _.find(ids, 'id', parseInt(requestid));
    }

    AbsenceSoft.Session.RemoveList = function (userid, lst) {       
        $.each(lst,function (index,id) {
            AbsenceSoft.Session.Remove(userid, id);
        });
    }

    //remove
    AbsenceSoft.Session.Remove = function (userid, id) {
        var ids = AbsenceSoft.Session.GetAll(userid);
        ids = $.grep(ids, function (e, i) {
            return e.id != id;
        });
        localStorage.setItem(userid + "_REPORT_DATA", JSON.stringify(ids));
    }

    //clear
    AbsenceSoft.Session.Clear = function () {
        localStorage.clear();
    }

    // set criteria data for report for particular report
    AbsenceSoft.Session.AddCriteria = function (reportid, criteriaData) {
        sessionStorage.setItem(reportid + "_REPORT_CRITERA", JSON.stringify({ criteriaData: criteriaData, expiry: moment() }));
    }

    // get criteria data for report for particular report
    AbsenceSoft.Session.GetCriteria = function (reportid) {
        var sd = sessionStorage.getItem(reportid + "_REPORT_CRITERA");
        if (sd) {
            try {
                return JSON.parse(sd);
            }
            catch (e) { }
        }
        return null;       
    }


    // set criteria data for report for particular report
    AbsenceSoft.Session.SetReportData = function (reportid, criteriaData) {
        sessionStorage.setItem(reportid + "_REPORT_DATA", JSON.stringify({ reportData: criteriaData, expiry: moment() }));
    }

    // get criteria data for report for particular report
    AbsenceSoft.Session.GetReportData = function (reportid) {
        var sd = sessionStorage.getItem(reportid + "_REPORT_DATA");
        if (sd) {
            try {
                return JSON.parse(sd);
            }
            catch (e) { }
        }
        return null;
    }


    $(document).ready(function () {
        if ($(location).attr('href').indexOf("/login") > -1) {
            sessionStorage.clear();
        }
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);