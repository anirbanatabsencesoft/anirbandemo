﻿(function (AbsenceSoft, $) {
    AbsenceSoft.AdhocReport2_0 = AbsenceSoft.AdhocReport2_0 || {};
    var sourceElement = null;
    AbsenceSoft.AdhocReport2_0.Module = "reporting";

    var toggleCategories = function () {
        $(this).find('span').toggleClass('fa-chevron-circle-down fa-chevron-circle-up');
        $('.adhoc-report-category').not(this).find('span').removeClass('fa-chevron-circle-up').addClass('fa-chevron-circle-down');
        $('.ad-menu-list.in').collapse('hide');
    }

    var toggleFieldSelection = function () {
        AbsenceSoft.AdhocReport2_0.MarkDirty();
        var that = $(this);
        var key = that.find('input');
        var keyVal = key.val();
        var isCurrentlyIncluded = key.hasClass('include');
        that.find('span').toggleClass('ad-menu-x');
        key.toggleClass('include');

        if (isCurrentlyIncluded) {
            var header = $('#AdHocReportDataGrid thead tr')
                .find(':input[value="' + keyVal + '"]')
                .parents('.adhoc-field');

            var nthChild = getHeaderIndex(header) + 1;
            $('#AdHocReportDataGrid tbody tr td').remove(':nth-child(' + nthChild + ')');
            header.remove();
        } else {
            var newField = '<th class="adhoc-field" draggable="true"><span class="glyphicon glyphicon-move "></span> <h6>' + that.text() +'</h6><input type="hidden" value="' + keyVal + '" /></th>';
            $('#AdHocReportDataGrid thead tr').append(newField);
            $('#AdHocReportDataGrid tbody tr').append('<td class="data-border"></th>');
            bindDragEvents();
        }
    }

    var getHeaderIndex = function (header) {
        return $('#AdHocReportDataGrid thead tr .adhoc-field').index(header);
    }

    var addColumnToGrid = function (field)
    {
        var newField = '<th class="adhoc-field" draggable="true"><h>' + field.displayName + '</h><input type="hidden" value="' + field.name + '" /></th>';
        $('#AdHocReportDataGrid thead tr').append(newField);
        $('#AdHocReportDataGrid tbody tr').append('<td class="data-border"></th>');
        bindDragEvents();
    }

    var filterFields = function () {
        var searchTerm = $(this).val().trim().toLowerCase();

        $('.format-tree-primary-branch').each(function (i, element) {
            var branch = $(element);
            if (branch.find('p').text().toLowerCase().indexOf(searchTerm) == -1) {
                branch.addClass('hide');
            } else {
                branch.removeClass('hide');
            }
        });

        $('.adhoc-report-category-container').each(function (i, element) {
            var category = $(element);
            var notHiddenFields = category.find('.format-tree-primary-branch').not('.hide');
            if (notHiddenFields.length > 0) {
                category.removeClass('hide');
            } else {
                category.addClass('hide');
            }
        });
    }

    var dragStart = function (e) {
        $(this).addClass('adhoc-dragging');
        $('.adhoc-field').not(this).addClass('adhoc-drag-target');

        sourceElement = this;
        /// We're not using dataTransfer.setData or getData, because IE doesn't have proper support for it
        /// But we need to set something, or FF assumes we have nothing to transfer
        e.originalEvent.dataTransfer.setData('text/plain', $(this).html());
        e.originalEvent.dataTransfer.effectAllowed = 'move';
    }

    var dragEnd = function (e) {
        $(this).removeClass('adhoc-dragging');
        $('.adhoc-field').removeClass('adhoc-drag-target adhoc-dragging adhoc-drag-over');
        sourceElement = null;
    }

    var allowDrop = function (e) {
        if (e.preventDefault)
            e.preventDefault();

        e.originalEvent.dataTransfer.dropEffect = 'move';
        return false;
    }

    var dropped = function (e) {
        if (e.stopPropagation)
            e.stopPropagation();

        if (sourceElement != this) {
            var currentHTML = $(this).html();
            var sourceHTML = $(sourceElement).html();
            $(sourceElement).html(currentHTML);
            $(this).html(sourceHTML);

            var targetColumn = getHeaderIndex(this) + 1;
            var sourceColumn = getHeaderIndex(sourceElement) + 1;
            $('#AdHocReportDataGrid tbody tr td:nth-child(' + targetColumn + ')').each(function (i, targetCell) {
                /// get the target cell html
                var targetCellHTML = $(targetCell).html();

                /// get a reference to the source cell
                var sourceCell = $($('#AdHocReportDataGrid tbody tr td:nth-child(' + sourceColumn + ')')[i]);

                /// set the target cell html to the source cell html
                $(targetCell).html(sourceCell.html());

                /// set the source cell html to the target cell html
                sourceCell.html(targetCellHTML);
            });
        }

        return false;
    }

    var handleDragEnter = function (e) {
        $(this).addClass('adhoc-drag-over');
    }

    var handleDragLeave = function (e) {
        $(this).removeClass('adhoc-drag-over');
    }

    var removeActiveClassOnCategories = function () {
        $('.adhoc-category-button').removeClass('active');
    }

    var resetActive = function (ele) {
        removeActiveClassOnCategories();     
        $('a[href$="'+ele+'"]:first').addClass('active');      
    }



    //get all Adhoc report
    AbsenceSoft.AdhocReport2_0.GetAdhocReportsCategory = function () {

        var reporttypeid = AbsenceSoft.Utilities.getQueryParamFromUrl("reporttypeid");
        var reportid = AbsenceSoft.Utilities.getQueryParamFromUrl("reportid");
        var endPoint = "reports/adhoc/categories?reportTypeId=" + reporttypeid;
        if (reportid) {
            endPoint = "reports/adhoc/categories?reportTypeId=" + reporttypeid + "&reportId=" + reportid;
        }
        try {
            AbsenceSoft.HttpUtils.Get(AbsenceSoft.AdhocReport2_0.Module, endPoint, function (result) {
                if (!AbsenceSoft.Utilities.isNullOrWhitespace(result)) {
                    var resultData = JSON.parse(result);
                    if (resultData && resultData.success === true && resultData.length !== 0) {
                        AbsenceSoft.AdhocReport2_0.LoadCategoryMenu(resultData.data);   
                        AbsenceSoft.AdhocReport2_0.SetReportCategory();                        
                    }
                    else {
                        console.log(resultData.message);
                    }
                }
            });
        }
        catch (ex) {
            console.log(ex);
        }

    }

    AbsenceSoft.AdhocReport2_0.SetReportCategory = function () {
        var reporttypeid = AbsenceSoft.Utilities.getQueryParamFromUrl("reporttypeid");
        var endPoint = "reports/adhoc/reporttype/get/id/" + reporttypeid;
        try {
            AbsenceSoft.HttpUtils.Get(AbsenceSoft.AdhocReport2_0.Module, endPoint, function (result) {
                if (!AbsenceSoft.Utilities.isNullOrWhitespace(result)) {
                    var resultData = JSON.parse(result);
                    if (resultData && resultData.success === true && resultData.length !== 0) {
                        var reportCategory;
                        AbsenceSoft.AdhocReport2_0.const.forEach(function (obj) {
                            if (resultData.data.adHocReportType == obj.Id) {
                                reportCategory = '#' + obj.Name + 'ReportCategory';
                                $(".report-type-title").html(resultData.data.name + ' Report');
                                $("#adhockey").val(resultData.data.key);
                                resetActive(reportCategory);
                            }
                        });
                        $('.return-ad-btn').prop('href', reportCategory);
                    }
                    else {
                        console.log(resultData.message);
                    }
                }
            });
        }
        catch (ex) {
            console.log(ex);
        }
    }

    var SetReportInfo = function (rid) {
        var endPoint = 'reports/adhoc/user/savedreport?id=' + rid;
        try {
            AbsenceSoft.HttpUtils.Get(AbsenceSoft.AdhocReport2_0.Module, endPoint, function (result) {
                if (!AbsenceSoft.Utilities.isNullOrWhitespace(result)) {
                    var resultData = JSON.parse(result);
                    if (resultData && resultData.success === true && resultData.length !== 0) {
                        $('#adhoc-report-name').html(resultData.data.name);
                      //  showReport(data, resultData.data.fields);
                    }
                    else {
                        console.log(resultData.message);
                    }
                }
            });
        }
        catch (ex) {
            console.log(ex);
        }

    }


    AbsenceSoft.AdhocReport2_0.LoadCategoryMenu = function (data) {
        var reportid = AbsenceSoft.Utilities.getQueryParamFromUrl("reportid");
        if (reportid) {
            SetReportInfo(reportid);
        }
        var ul = document.createElement('ul');
        $(ul).addClass("adhoc-report-category-container");
        $(data).each(function (index, catObj) {           
            var li = document.createElement('li');
            var label = document.createElement('label');
            $(label).addClass('adhoc-report-category');
            $(label).attr('data-toggle', 'collapse').attr('data-target', '#adHocReportCategory' + catObj.name.replace(/\s+/g, '').trim())
            var span = document.createElement('span');
            $(span).addClass('fa fa-chevron-circle-down pull-right ah-light-gray treemenu-margin');
            $(label).html(catObj.name);
            $(label).append(span);
            $(li).append(label);
            if (catObj.fields.length > 0) {
                var ulfield = document.createElement('ul');
                $(ulfield).addClass('ad-menu-list collapse sidetree-lists').prop('id', 'adHocReportCategory' + catObj.name.replace(/\s+/g, '').trim()).prop('style', 'overflow:hidden; overflow-y:scroll; max-height: 20.5em;');
                
                $(catObj.fields).each(function (index, field) {
                    var lifield = document.createElement('li');
                    var textHidden = document.createElement('input');
                    $(textHidden).prop('type', 'hidden').val(field.name);
                    var para = document.createElement('p');
                    $(para).html(field.displayName);
                    var spanfield = document.createElement('span');
                    $(spanfield).addClass('fa fa-plus-circle pull-right ah-bright-green');
                    if (field.includedInReport) {
                        $(textHidden).addClass('include');
                        $(spanfield).addClass('ad-menu-x');
                        if (reportid) {
                            addColumnToGrid(field);
                        }
                    }
                   
                    $(lifield).addClass('format-tree-primary-branch');
                    $(lifield).append(textHidden);
                    $(lifield).append(para);
                    $(lifield).append(spanfield);
                    $(ulfield).append(lifield);
                });
                $(li).append(ulfield);
            }
            $(ul).append(li);
        });
        
        $('.css-treeview').append(ul);
        $('.adhoc-report-category').click(toggleCategories);
        $('.format-tree-primary-branch').click(toggleFieldSelection);
        $('#categorySearch').keyup(filterFields);
        $('.return-ad-btn').click(AbsenceSoft.AdhocReport2_0.ShowCategorySubMenu());
        bindDragEvents();
    }

    var bindDragEvents = function () {
        $('.adhoc-field').unbind('dragstart').on('dragstart', dragStart);
        $('.adhoc-field').unbind('dragend').on('dragend', dragEnd);
        $('.adhoc-field').unbind('dragover').on('dragover', allowDrop);
        $('.adhoc-field').unbind('drop').on('drop', dropped);
        $('.adhoc-field').unbind('dragenter').on('dragenter', handleDragEnter);
        $('.adhoc-field').unbind('dragleave').on('dragleave', handleDragLeave);
    }

    $(document).ready(function () {
        if ($(location).attr('href').toLowerCase().indexOf("reporting/adhoc/adhocreport") != -1) {            
            AbsenceSoft.AdhocReport2_0.GetAdhocReportsCategory();
        }
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);