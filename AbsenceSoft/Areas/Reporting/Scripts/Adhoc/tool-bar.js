﻿(function (AbsenceSoft, $) {
    AbsenceSoft.AdhocReport2_0 = AbsenceSoft.AdhocReport2_0 || {};
    var isDirty = false;
    var isExport = false;
    var isLoading = false;
    var isSave = false;
    AbsenceSoft.AdhocReport2_0.Module = "reporting";
    var notyTimeout = 3000;

    var getFilters = function () {
        var filters = [];
        var uiFilters = $('#filters').find('.absencesoft-repeating').each(function (i) {
            var that = $(this);
            var fieldName = that.find('.report-fields-for-filtering').select2('val');
            var operator = that.find('.filter-operator').val();
            var value = that.find('.filter-value').val();
            /// Only include filters that they have made selections on all three values
            if (fieldName && operator && value) {
                filters.push({
                    FieldName: fieldName,
                    Operator: operator,
                    Value: value
                })
            }
        });

        return filters;
    }

    var getFields = function () {
        var fields = [];

        $('#AdHocReportDataGrid thead tr th').each(function (i, element) {
            var fieldName = $(this).find('input').val();
            fields.push({
                Name: fieldName,
                Order: i + 1
            });
        })
        return fields;
    }

    var dataIsValid = function (reportData) {
        return reportData.Fields.length > 0;
    }

    var buildReportData = function () {
        var reporttypeid = AbsenceSoft.Utilities.getQueryParamFromUrl("reporttypeid");
        var arg = {};
        arg.CustomerId = AbsenceSoft.Current.Customer.id;        
        arg.ReportRequestType = 1;
        arg.ExportType = 0;
       
        arg.reportDefinition = {
            UserId: AbsenceSoft.Current.User.id,
            Name: $('#adhoc-report-name').html() !== "Untitled Report" ? $('#adhoc-report-name').html() : "Untitled Report"+ Date.now().toString(),
            Description: $('#adhoc-report-name').html() !== "Untitled Report" ? $('#adhoc-report-name').html() : "Untitled Report" + Date.now().toString(),
            ReportType: {
                AdHocReportType: 1,
                Id: reporttypeid,
                Key: $("#adhockey").val()
            },
            Fields: getFields(),
            Filters: getFilters()
        };
        return arg;
    }

    var loadReport = function (data) {
        var requestId = AbsenceSoft.Utilities.getQueryParamFromUrl("requestId");
        var reportid = AbsenceSoft.Utilities.getQueryParamFromUrl("reportid");
        if (reportid) {
            var endPoint = 'reports/adhoc/user/savedreport?id=' + reportid;
            try {
                AbsenceSoft.HttpUtils.Get(AbsenceSoft.AdhocReport2_0.Module, endPoint, function (result) {
                    if (!AbsenceSoft.Utilities.isNullOrWhitespace(result)) {
                        var resultData = JSON.parse(result);
                        if (resultData && resultData.success === true && resultData.length !== 0) {
                            $('#adhoc-report-name').html(reportData.data.Name);
                            showReport(data, resultData.data.fields);
                        }
                        else {
                            console.log(resultData.message);
                        }
                    }
                });
            }
            catch (ex) {
                console.log(ex);
            }
        }
        else {
            var reportData = AbsenceSoft.Session.GetById(AbsenceSoft.Current.User.id, requestId);
            if (reportData && reportData.data) {
                $('#adhoc-report-name').html(reportData.data.requestedData.reportDefinition.Name);
                var reporttypeid = reportData.data.requestedData.reportDefinition.ReportType.Id;
                var fields = reportData.data.requestedData.reportDefinition.Fields;
                showReport(data, fields);
            }

        }
    }

    var showReport = function(data, fields)
    {
        $(fields).each(function (index, field) {
            $("input[value='" + field.Name + "']").addClass('include');
            $("input[value='" + field.Name + "']").next().next().addClass('ad-menu-x');
            var newField = '<th class="adhoc-field" draggable="true"><h>' + $("input[value='" + field.Name + "']").next().html() + '</h><input type="hidden" value="' + field.Name + '" /></th>';
            $('#AdHocReportDataGrid thead tr').append(newField);
            $('#AdHocReportDataGrid tbody tr').append('<td class="data-border"></th>');
            // bindDragEvents();
        });

        $(data.jsonData).each(function (index, item) {
            var tr = document.createElement('tr');
            _.forIn(item, function (value, key) {
                $(tr).append('<td class="data-border">' + value + '</td>');
            });
            $('#AdHocReportDataGrid tbody').append(tr);
        });
    }

    var saveReport = function () {     
        var reportId = AbsenceSoft.Utilities.getQueryParamFromUrl("reportid");
        if (reportId) {
            $('#adHocReportName').val($('#adHocReportTitle').text());
            isSave = true;
            $('#adHocReportSaveText').text('Save Report:');
            $('#adHocSaveReportAsDialog').modal('show');
        } else {
            saveAsReport();
        }
    }

    var saveAsReport = function () {
        isSave = false;       
        $('#adHocReportSaveText').text('Save Report As:');
        $('#adHocReportName').val('');
        $('#adHocSaveReportAsDialog').modal('show');
        
    }

    var resetReportName = function () {
        $('#reportName').val('');
        $('#adHocReportNameRequired').addClass('hide');
    }

    var saveReportAs = function () {
        var reporttypeid = AbsenceSoft.Utilities.getQueryParamFromUrl("reporttypeid");
        var reportName = $('#adHocReportName').val();
        if (reportName.trim() == '') {
            $('#adHocReportNameRequired').removeClass('hide');
            return;
        }

        var reportArguments = {
            Identifier: "reporting",
            ColumnDefinition: {
                AdHocReportType: 1,
                Id: reporttypeid,
                Active: true,
                Fields: getFields(),
               Name: reportName            
               //Filters: getFilters()
            }
        };

        var endPoint = "reports/adhoc/save";
        if (isSave) {
            endPoint = "reports/adhoc/saveas"
        }
     
        try {
            AbsenceSoft.HttpUtils.Post(AbsenceSoft.ReportOperations.Module, endPoint, reportArguments, function (result) {
                if (!AbsenceSoft.Utilities.isNullOrWhitespace(result)) {
                    var resultData = JSON.parse(result);
                    if (resultData && resultData.success === true && resultData.length !== 0) {
                        console.log("saved");
                    }
                    else {                      
                        if (!isSave) {
                            $('#adHocReportTitle').text("Untitled Report");  
                        }
                          noty({ type: 'error', text: '<b>!Error</b><br/>Report name already exist.', layout: 'topRight', timeout: notyTimeout });
                            return;                        
                        console.log(resultData.message);
                    }
                }
            });
        }
        catch (ex) {
            console.log(ex);
        }
        $('#saveAdHocReportAs').removeClass("disabled").removeAttr("disabled");
    }

    var handleUnload = function (event) {
        if (isDirty && !isExport) {
            var confirmationMessage = "You have unsaved changes to this report.  Are you sure you would like to leave the page?";
            event.returnValue = confirmationMessage;
            return confirmationMessage;
        }
    }
    
    AbsenceSoft.AdhocReport2_0.MarkDirty = function () {
        isDirty = true;
        isExport = false;
    };

    AbsenceSoft.AdhocReport2_0.buildReportData = function () {
        return buildReportData();
    }

    AbsenceSoft.AdhocReport2_0.loadReport = function (data) {
        return loadReport(data);
    }

    AbsenceSoft.AdhocReport2_0.getFilters = function () {
        var reporttypeid = AbsenceSoft.Utilities.getQueryParamFromUrl("reporttypeid");
        var reportid = AbsenceSoft.Utilities.getQueryParamFromUrl("reportid");
        var endPoint = "reports/adhoc/filters?reportTypeId=" + reporttypeid;
        if (reportid) {
            endPoint = "reports/adhoc/filters?reportTypeId=" + reporttypeid + "&reportId=" + reportid;
        }
        try {
            AbsenceSoft.HttpUtils.Get(AbsenceSoft.AdhocReport2_0.Module, endPoint, function (result) {
                if (!AbsenceSoft.Utilities.isNullOrWhitespace(result)) {
                    var resultData = JSON.parse(result);
                    if (resultData && resultData.success === true && resultData.length !== 0) {
                        if (resultData.data.filters.length > 0) {
                            //bind filters
                        }
                        else {
                            //bind
                        }
                        if (resultData.data.categories.length > 0) {
                            makeFileterList(resultData.data.categories);
                        }
                    }
                    else {
                        console.log(resultData.message);
                    }
                }
            });
        }
        catch (ex) {
            console.log(ex);
        }
    }

    var makeFileterList = function (categories) {
        var div = document.createElement('div');
        $(div).addClass('row absencesoft-repeating');
        var templatediv = document.createElement('div');
        $(templatediv).addClass('col-xs-2');
        var filterdiv = document.createElement('div');
        $(filterdiv).addClass('col-xs-9 filter-container');
        $(filterdiv).html('<div class="row"><div class="col-xs-2"><select class="form-control filter-operator" id="AdhocOperator" name="Operator"><option value="">Select a comparison</option>< option value= "=" > equals</option ><option value="<>">does not equal</option><option value="~|">begins with</option><option value="~~">contains</option><option value="|~">ends with</option></select ></div><div class="col-xs-10"><input class="form-control filter-value" id="Value" name="Value" type="text" value=""></div></div></div>');
        var buttondiv = document.createElement('div');
        $(buttondiv).addClass('col-xs-1');
        $(buttondiv).html('<button class="btn btn-icon btn-remove-filter remove-button pull-right"><span class="fa fa-minus"></span></button>');

        var select = document.createElement('select');
        $(select).addClass('report-fields-for-filtering form-control');
        var opt = $('<option value="">Select a field for filtering</option>');
        $(select).append(opt);
        $(categories).each(function (index, category) {
            var optgrp = $('<optgroup label=' + category.name + '>');            
            $(category.fields).each(function (index, field) {
                var optcat = '<option value="' + field.name + '">' + field.displayName + '</option>';                
                $(optgrp).append(optcat);
            });
            $(select).append(optgrp);
        });
        $(templatediv).html(select);
        $(div).append(templatediv);
        $(div).append(filterdiv);
        $(div).append(buttondiv);
        $('#filterTemplate').html(div);
    }
    var hideAddFilterTextAdhoc = function () {
        $('#addFiltersText').addClass('hide');
        $('#runFiltersText').removeClass('hide');
    }


    $(document).ready(function () {
        if ($(location).attr('href').toLowerCase().indexOf("reporting/adhoc") > -1) {
            AbsenceSoft.Current.getCurrentData();
            AbsenceSoft.AdhocReport2_0.getFilters();
            $('#saveAdHocReportAs').on('click', function () {
                $('#saveAdHocReportAs').addClass('disabled').attr('disabled', true);
                return saveReportAs();
            });          
            $('#adHocSaveReportAsDialog').on('hidden.bs.modal', resetReportName);
            $('#save-report').click(saveReport);          
            $('#saveas-report').click(saveAsReport);
            $('#addFilter-adhoc').click(hideAddFilterTextAdhoc);
            window.onbeforeunload = handleUnload;
        }
    });
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);