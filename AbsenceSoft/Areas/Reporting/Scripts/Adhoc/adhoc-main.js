﻿(function (AbsenceSoft, $) {
    'use strict';
    AbsenceSoft.AdhocReport2_0 = AbsenceSoft.AdhocReport2_0 || {};
    
    //define the module
    AbsenceSoft.AdhocReport2_0.Module = "reporting";
    var removeActiveClassOnCategories = function () {
        $('.adhoc-category-button').removeClass('active');
    }

    var resetActive = function () {
        removeActiveClassOnCategories();       
        $('a[href$="' + $(this).attr('href') + '"]:first').addClass('active'); 
        $($(this).attr('href')).addClass('hide');
    }

    AbsenceSoft.AdhocReport2_0.ShowCategorySubMenu = function () {
        removeActiveClassOnCategories();
        var link = $(this);
        $('.adhoc-report-type').addClass('hide');
        var elementToShow = link.attr('href');         
        $(".adhoc-category-button[href='" + elementToShow + "']").addClass('active');
        $(elementToShow).removeClass('hide');
        return false;
    }

    AbsenceSoft.AdhocReport2_0.LoadMenu = function (data) {      
        var categoarydiv = document.createElement('div'); 
        var reporttypediv = document.createElement('div'); 
        AbsenceSoft.AdhocReport2_0.const.forEach(function (obj) {     
            var id = obj.Name+'ReportCategory';
            var adhoc_template = $(".ad-cat-vscroll").clone();
            $(adhoc_template).find('.adhoc-category-button').prop('href', '#' + id);
            $(adhoc_template).find('.adhoc-icon-class').addClass(obj.IconClass);
            $(adhoc_template).find('.adhoc-report-name').html(obj.Name);
            $(categoarydiv).append(adhoc_template);

            var category_template = $("#adhoc-report-type").children().clone().attr('id', id);                
            $(category_template).find('.adhoc-report-type-href').prop('href', '#' + id);

            var divCatList = document.createElement('div');
            $(divCatList).addClass('col-sm-3  col-xs-6');
            var divCatInnerList = document.createElement('div');
            $(divCatInnerList).addClass('text-center adh-type-list');
            var icon = document.createElement('i');
            $(icon).addClass(obj.IconClass).attr('aria-hidden', 'true');
            $(divCatInnerList).append(icon);
            var catUl = document.createElement('ul');
            $(data).each(function (index, reportObj) {              
                if (reportObj.adHocReportType == obj.Id) { 
                    var li = document.createElement('li');
                    var liCat = document.createElement('li');
                    var a = document.createElement('a');
                    var aCat = document.createElement('a');
                    var span = document.createElement('span');
                    $(a).addClass('ad-category-current').prop('href', '/reporting/adhoc/adhocreport?reporttypeid=' + reportObj.id + '&type=adhoc').html(reportObj.name); 
                    $(liCat).html($(a).clone());
                    $(catUl).append(liCat);  
                    $(span).addClass('fa fa-arrow-circle-right pull-right');
                    $(a).append(span);
                    $(li).html(a);
                    $(category_template).find('.ad-close-menu').append(li);    
                    
                }               
            });
            $(divCatInnerList).append(catUl);
            $(divCatList).html(divCatInnerList);
            $(reporttypediv).append(category_template);
            $('.adhoc-cat-list').append(divCatList);
          
        });
       
        $("#side-navbar").html(categoarydiv);
       
        $("#adhoc-report-type").html(reporttypediv);
        $('.adhoc-category-button').click(AbsenceSoft.AdhocReport2_0.ShowCategorySubMenu);
        $('.ad-close-menu a').click(resetActive);
        $('.ad-category-current').click(resetActive);
    }

    //get all Adhoc report
    AbsenceSoft.AdhocReport2_0.getAdhocReports = function () {
        var endPoint = "reports/adhoc/getall";
        try {
            AbsenceSoft.HttpUtils.Get(AbsenceSoft.AdhocReport2_0.Module, endPoint, function (result) {
                if (!AbsenceSoft.Utilities.isNullOrWhitespace(result)) {
                    var resultData = JSON.parse(result);
                    if (resultData && resultData.success === true && resultData.length !== 0) {
                        AbsenceSoft.AdhocReport2_0.LoadMenu(resultData.data)
                    }
                    else {
                        console.log(resultData.message);
                    }
                }
            });
        }
        catch (ex) {
            console.log(ex);
        }
    };
    
    
    var createSavedReports = function (data, noOfColumns, noOfRecords, selector) {        
        var m = 0;
        for (var j = 0; j < noOfColumns; j++) {
            var div = document.createElement('div');
            $(div).addClass('col-xs-' + 12 / noOfColumns);
            var ul = document.createElement('ul');      
            for (var i = 0; i < noOfRecords; i++) {                
                m = (i * noOfColumns) + j;
                if (m > noOfRecords-1) {
                    break;
                }
                var li = document.createElement('li');
                var a = document.createElement('a');
                $(a).prop('href', '/reporting/adhoc/adhocreport?reporttypeid=' + data[m].reportId + '&reportid=' + data[m].id+'&type=adhoc').html(data[m].name); 
                $(li).html(a);
                $(ul).append(li);
            }
            $(div).append(ul);
            $(selector).append(div);
        }
    }

    
    AbsenceSoft.AdhocReport2_0.getMostRecentAdhocReports = function () {
        var endPoint = "reports/adhoc/user/savedreports";
        try {
            AbsenceSoft.HttpUtils.Get(AbsenceSoft.AdhocReport2_0.Module, endPoint, function (result) {
                if (!AbsenceSoft.Utilities.isNullOrWhitespace(result)) {
                    var resultData = JSON.parse(result);
                    if (resultData && resultData.success === true && resultData.length !== 0) {                       
                        createSavedReports(resultData.data, 3, 10, '.ad-hoc-most-recent');
                        createSavedReports(resultData.data, 2, resultData.data.length,'.adhoc-report-list-all');
                    }
                    else {
                        console.log(resultData.message);
                    }
                }
            });
        }
        catch (ex) {
            console.log(ex);
        }
    }

    $(document).ready(function () {
        if ($(location).attr('href').toLowerCase().indexOf("reporting/adhoc") > -1)
        {
            AbsenceSoft.AdhocReport2_0.getAdhocReports();
            AbsenceSoft.AdhocReport2_0.getMostRecentAdhocReports();
        }
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);