﻿(function (AbsenceSoft, $) {
    AbsenceSoft.AdhocReport2_0 = AbsenceSoft.AdhocReport2_0 || {};
    AbsenceSoft.AdhocReport2_0.const = [
        { "Id": 1, "Name": "Employee", "IconClass": "fa fa-users ah-bright-blue" } ,
        { "Id": 2, "Name": "Case", "IconClass": "fa fa-briefcase ah-orange" },
        { "Id": 3, "Name": "ToDo", "IconClass": "fa fa-list ah-bright-green" } ,
        { "Id": 4, "Name": "Customer", "IconClass": "fa fa-building ah-coral" }
    ];

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);