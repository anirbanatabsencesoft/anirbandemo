﻿(function (AbsenceSoft, $) {
    AbsenceSoft.Share = AbsenceSoft.Share || {};

    var selection = "";
    var notyTimeout = 3000;

    //on click ui logic
    AbsenceSoft.Share.showRecipientsInput = function () {
        $("#user").hide();
        $("#team").hide();
        $("#role").hide();

        //attach event
        $('input[name="sharedWith"]').click(AbsenceSoft.Share.showRespectiveInput);

        //fill the data
        AbsenceSoft.Share.fillUsers();
        AbsenceSoft.Share.fillRoles();
        AbsenceSoft.Share.fillTeams();

        //set selected
        AbsenceSoft.Share.setSelected();
        
        $(".select2-multiple").select2({ closeOnSelect: true });
    }

    //set selected values for dropdowns
    AbsenceSoft.Share.setSelected = function () {
        if (AbsenceSoft.BuildCriteria.shareId){
            $("#shared-report-id").val(AbsenceSoft.BuildCriteria.shareId);
        }
        if (AbsenceSoft.BuildCriteria.shareName) {
            $("#txtShareName").val(AbsenceSoft.BuildCriteria.shareName);
        }
        if (AbsenceSoft.BuildCriteria.selectedUserKeys && AbsenceSoft.BuildCriteria.selectedUserKeys.length > 0) {
            $("#selUser").val(AbsenceSoft.BuildCriteria.selectedUserKeys);
            $('input[name="sharedWith"][value="USER"]').click();
        }
        if (AbsenceSoft.BuildCriteria.selectedTeamKeys && AbsenceSoft.BuildCriteria.selectedTeamKeys.length > 0) {
            $("#selTeam").val(AbsenceSoft.BuildCriteria.selectedTeamKeys);
            $('input[name="sharedWith"][value="TEAM"]').click();
        }
        if (AbsenceSoft.BuildCriteria.selectedRoleNames && AbsenceSoft.BuildCriteria.selectedRoleNames.length > 0){
            $("#selRole").val(AbsenceSoft.BuildCriteria.selectedRoleNames);
            $('input[name="sharedWith"][value="ROLE"]').click();
        }
        //show hide
        AbsenceSoft.Share.showRespectiveInput();
    }

    //on clicking selection
    AbsenceSoft.Share.showRespectiveInput = function (sel, obj) {
        selection = $('input[name="sharedWith"]:checked').val();
        if (selection == "USER") {
            $("#user").show();
            $("#team").hide();
            $("#role").hide();
        } else if (selection == "TEAM") {
            $("#user").hide();
            $("#team").show();
            $("#selRole").hide();
        } else if (selection == "ROLE") {
            $("#user").hide();
            $("#team").hide();
            $("#role").show();
        }
    }

    //fill users
    AbsenceSoft.Share.fillUsers = function () {
        var users = AbsenceSoft.Current.Users;
        if (user && users.length > 0) {
            $.each(users, function (i, item) {
                $('#selUser').append($('<option>', {
                    value: item.Id,
                    text: item.DisplayName
                }));
            });
        }
    }

    //fill roles
    AbsenceSoft.Share.fillRoles = function () {
        var roles = AbsenceSoft.Current.Roles;
        if (roles && roles.length > 0) {
            $.each(roles, function (i, item) {
                $('#selRole').append($('<option>', {
                    value: item.Id,
                    text: item.Name
                }));
            });
        }
    }

    //fill team
    AbsenceSoft.Share.fillTeams = function () {
        var teams = AbsenceSoft.Current.Teams;
        if (teams && teams.length > 0) {
            $.each(teams, function (i, item) {
                $('#selTeam').append($('<option>', {
                    value: item.Id,
                    text: item.Name
                }));
            });
        }
    }

    AbsenceSoft.Share.fillAllRecipients = function () {
        AbsenceSoft.Share.fillUsers();
        AbsenceSoft.Share.fillRoles();
        AbsenceSoft.Share.fillTeams();
    }

    //save
    AbsenceSoft.Share.shareReport = function () {
        var arg = {};
        arg.id = $("#shared-report-id").val() || null;
        arg.ReportName = $("#txtShareName").val();

        if (!arg.ReportName) {
            noty({ type: 'error', text: '<b>!Error</b><br/>Report name is missing.', layout: 'topRight', timeout: notyTimeout });
            return;
        }

        arg.SharedWith = selection;
        arg.Order = 0;
        arg.TeamKeys = $("#selTeam").val();
        arg.RoleNames = $("#selRole").val();
        arg.UserKeys = $("#selUser").val();
        arg.Argument = AbsenceSoft.BuildCriteria.getRequestObject();
        if (AbsenceSoft.Utilities.getQueryParamFromUrl("requestid")) {
            var requestedReportId = AbsenceSoft.Utilities.getQueryParamFromUrl("requestid");
            arg.LastRequestId = requestedReportId;
        }
        
        var t = JSON.stringify(arg);
        AbsenceSoft.HttpUtils.Post("reporting", "reports/shared/save", arg, function (data) {
            var d = JSON.parse(data);
            if (d.success) {
                noty({ type: 'success', text: '<b>Success</b><br/>Report shared.', layout: 'topRight', timeout: notyTimeout });
                $("#report-settings").collapse('hide');
            } else {
                noty({ type: 'error', text: '<b>!Error</b><br/>' + d.message, layout: 'topRight', timeout: notyTimeout });
            }
        });
    }


    $(document).ready(function () {
        $("#btnsharereport").click(AbsenceSoft.Share.showRecipientsInput);
        $("#btnShareReport").click(AbsenceSoft.Share.shareReport);

      
        $('#sch-time-picker').datetimepicker({
            pick12HourFormat: true,
            pickDate: false
        });        

        var nowDate = new Date();
        var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate()+1, 0, 0, 0, 0);
       
        $('#start-date').datetimepicker({
            orientation: 'bottom auto',
            showTodayButton: true,
            format: "MM/dd/yyyy",
            pickDate: true,
            autoclose: true
        }).on('changeDate', function (ev) {
            $(this).datetimepicker('hide');
            });

        $('#end-date').datetimepicker({
            orientation: 'bottom auto',
            showTodayButton: true,
            format: "MM/dd/yyyy",
            pickDate: true,
            autoclose: true
        }).on('changeDate', function (ev) {
            $(this).datetimepicker('hide');
        });

        $('#sch-date-picker').datetimepicker({
            orientation: 'bottom auto',
            showTodayButton: true,
            format: "MM/dd/yyyy",
            pickDate: true,
            autoclose: true,
            startDate: today
        }).on('changeDate', function (ev) {
            $(this).datetimepicker('hide');
        });

        
       
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);