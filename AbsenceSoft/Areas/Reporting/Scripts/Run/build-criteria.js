﻿(function (AbsenceSoft, $) {
    AbsenceSoft.BuildCriteria = AbsenceSoft.BuildCriteria || {};
    var current = AbsenceSoft.Current;
    var utils = AbsenceSoft.Utilities;
    var type = "";
    var id = "";
    var groupBy = "";
    var sharedReportId = "";
    var isCriteriaLoaded = false;
    var isGroupLoaded = false;
    var interval = null;

    //global variables
    AbsenceSoft.BuildCriteria.criteriaData = "";
    AbsenceSoft.BuildCriteria.reportData = "";

    //global variables for shared with
    AbsenceSoft.BuildCriteria.shareName = "";
    AbsenceSoft.BuildCriteria.shareId = "";
    AbsenceSoft.BuildCriteria.selectedUserKeys = [];
    AbsenceSoft.BuildCriteria.selectedTeamKeys = [];
    AbsenceSoft.BuildCriteria.selectedRoleNames = [];
    AbsenceSoft.BuildCriteria.savedName = "";

    // check if session data is expired 
    var isExpired = function (expiryTime) {
        return parseInt(moment.duration(moment().diff(expiryTime))._milliseconds / 360000) > 60 ? true : false;
    }

    // Get the shared report details
    var loadSharedReport = function () {
        AbsenceSoft.HttpUtils.Get("reporting", "reports/shared/get?reportid=" + sharedReportId, function (result) {
            var d = $.parseJSON(result);
            if (d.success === true && d.data.length > 0) {
                AbsenceSoft.BuildCriteria.criteriaData = $.parseJSON(d.data[0].criteriaJson).reportDefinition.criteria;
                groupBy = $.parseJSON(d.data[0].criteriaJson).reportDefinition.groupType;
                AbsenceSoft.BuildCriteria.buildCriteriaParam();
                $(".select2-multiple").select2({ closeOnSelect: true });
                if (type === "shared") {
                    setSharingInfo(d.data[0]);
                }
                else {
                    setSavedInfo(d.data[0]);
                }
            }
        });
    }

    // get the report criteria and load
    var loadReportCriteria = function () {
        var report = AbsenceSoft.Session.GetReportData(id);
        if (report && !isExpired(report.expiry)) {
            AbsenceSoft.BuildCriteria.reportData = report.reportData;
            AbsenceSoft.BuildCriteria.buildReportParam();
        }
        else {
            //get report
            AbsenceSoft.HttpUtils.Get("reporting", "reports/operation/get/id/" + id, function (result) {
                var d = JSON.parse(result);
                if (d.success === true) {
                    AbsenceSoft.BuildCriteria.reportData = d.data;
                    AbsenceSoft.Session.SetReportData(id, d.data);
                    AbsenceSoft.BuildCriteria.buildReportParam();
                }
            });
        }
    }

    //Load the report information
    var loadReport = function () {
        var cData = AbsenceSoft.Session.GetCriteria(id);
        if (cData && !isExpired(cData.expiry)) {
            AbsenceSoft.BuildCriteria.criteriaData = cData.criteriaData;
            AbsenceSoft.BuildCriteria.buildCriteriaParam();
            $(".select2-multiple").select2({ closeOnSelect: true });
        }
        else {
            //get criteria
            AbsenceSoft.HttpUtils.Get("reporting", "reports/operation/criteria/get/reportid/" + id, function (result) {
                var d = JSON.parse(result);
                if (d.success === true) {
                    AbsenceSoft.BuildCriteria.criteriaData = d.data;
                    AbsenceSoft.Session.AddCriteria(id, d.data);
                    AbsenceSoft.BuildCriteria.buildCriteriaParam();
                    $(".select2-multiple").select2({ closeOnSelect: true });
                }
            });
        }
    }

    //get the report
    var getReportAndCriteria = function () {

        $("#reportCriteriaFilters").hide();
        $("#group-by").hide();

        if (type === "operation" || type === "bi" || type === "forensic") {
            loadReportCriteria();
            loadReport();
        }

        if (type === "shared" || type == "saved") {

            loadSharedReport();
            loadReportCriteria();
        }

        interval = setInterval(showLoadingTillCompleteLoad, 100);
    }

    //set the sharing info on settings tab
    var setSharingInfo = function (d) {
        AbsenceSoft.BuildCriteria.shareName = d.reportName;
        AbsenceSoft.BuildCriteria.shareId = d.id;
        switch (d.sharedWith) {
            case "USER":
                var userKeys = [];
                $(d.recipientList).each(function (i, e) {
                    userKeys.push(e.userKey);
                });
                AbsenceSoft.BuildCriteria.selectedUserKeys = userKeys;
                break;
            case "TEAM":
                var teamKeys = [];
                $(d.recipientList).each(function (i, e) {
                    teamKeys.push(e.teamKey);
                });
                AbsenceSoft.BuildCriteria.selectedTeamKeys = teamKeys;
                break;
            case "ROLE":
                var roleNames = [];
                $(d.recipientList).each(function (i, e) {
                    roleNames.push(e.roleName);
                });
                AbsenceSoft.BuildCriteria.selectedRoleNames = roleNames;
                break;
        }
    }


    var setSavedInfo = function (d) {
        AbsenceSoft.BuildCriteria.savedName = d.reportName;
        $("#saveReportName").val(AbsenceSoft.BuildCriteria.savedName);
    }


    //show the loading till the loading is complete
    var showLoadingTillCompleteLoad = function () {
        if (isCriteriaLoaded === true && isGroupLoaded === true) {
            clearInterval(interval);
            $("#reportCriteriaFilters").show();
            $("#group-by").show();
            $("#loaddiv").hide();
        }
    }


    //generate criteria
    AbsenceSoft.BuildCriteria.buildCriteriaParam = function () {
        if (!utils.isNullOrWhitespace(AbsenceSoft.BuildCriteria.criteriaData)) {
            var reqid = utils.getQueryParamFromUrl("requestid");
            var rData = AbsenceSoft.Session.GetById(current.User.id, reqid);
            if (AbsenceSoft.BuildCriteria.criteriaData.hasCriteria === true) {
                var html = "";

                var index = 0;
                var closedRow = false;

                for (var i = 0; i < AbsenceSoft.BuildCriteria.criteriaData.filters.length; i++) {
                    var ctrl = AbsenceSoft.HtmlHelper.GenerateHtml(AbsenceSoft.BuildCriteria.criteriaData.filters[i]);
                    if (!utils.isNullOrWhitespace(ctrl)) {
                        if (index == 0 || index % 4 == 0) {
                            html += '<div class="col-md-12 pad0 criteriarow">';
                            closedRow = false;
                        }
                        html += '<div class="col-xs-3">';
                        html += '<label>' + AbsenceSoft.BuildCriteria.criteriaData.filters[i].prompt + '</label>';

                        if ((type === "bi" || (rData && rData.data.reportType == 'bi')) && AbsenceSoft.BuildCriteria.criteriaData.filters[i].prompt === 'Duration') {
                            html += '<span class="iconPosition-lblduration"><button class="btn btn-link tooltip-margin " role="button" tabindex="0" data-toggle="tooltip" title="Reports are run for the selected duration and associated time frame selected"><span class="fa fa-question-circle"></span></button></span>';
                            html += ctrl.replace('<select  name="Duration" id="Duration" >', '<select  name="Duration" id="Duration" onchange="AbsenceSoft.BuildCriteria.showRespectiveDurationValue()" >');
                        }
                        else {
                            html += ctrl;
                        }
                        html += '</div>';

                        if (index != 0 && index % 3 == 0) {
                            html += '</div>';
                            closedRow = true;
                        }
                        index += 1;                       
                    }
                    
                }

                if (!closedRow) {
                    html += '</div>';
                }
                for (var i = 0; i < AbsenceSoft.BuildCriteria.criteriaData.filters.length; i++) {
                    if ((type === "bi" || (rData && rData.data.reportType == 'bi')) && AbsenceSoft.BuildCriteria.criteriaData.filters[i].prompt === 'Duration') {
                    html += '<div class="col-md-12 pad0 criteriarow">';
                    closedRow = false;
                    html += '<div class="col-xs-3">';  
                    html += '<label>Time Frame</label>';
                    html += AbsenceSoft.BuildCriteria.addDurationOptionsForBi();
                    html += '</div>';
                    html += '</div>'
                    closedRow = true;
                    }
                }
                $("#reportCriteriaFilters").html(html);
                $("#startenddate").show();
                if (AbsenceSoft.BuildCriteria.criteriaData.showAgeCriteria === true) {
                    $("#agefromto").show();
                } else {
                    $("#agefromto").hide();
                }

                if (type === 'bi' || (rData && rData.data.reportType == 'bi')) {
                    AbsenceSoft.BuildCriteria.showRespectiveDurationValue();
                    $('#reportCriteriaFilters').on('change', '#Duration', AbsenceSoft.BuildCriteria.showRespectiveDurationValue());
                    $("#dateRanges").hide();
                    isGroupLoaded = true;
                }

                if (rData && rData.data.availableGroupingFields) {
                    generateGroupByHtml(rData.data.availableGroupingFields);
                } else if (!utils.isJson(AbsenceSoft.BuildCriteria.reportData) && AbsenceSoft.BuildCriteria.reportData.availableGroupingFields) {
                    generateGroupByHtml(AbsenceSoft.BuildCriteria.reportData.availableGroupingFields);
                }
                else {
                    isGroupLoaded = true;
                }

                AbsenceSoft.BuildCriteria.setReportCriteria();


            }
        }
        resetHeight();
        isCriteriaLoaded = true;
    }

    AbsenceSoft.BuildCriteria.setReportCriteria = function () {

        for (var i = 0; i < AbsenceSoft.BuildCriteria.criteriaData.filters.length; i++) {
            var d = AbsenceSoft.BuildCriteria.criteriaData.filters[i];
            switch (d.controlType) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 11:
                    $("#" + d.fieldName).val(d.value);
                    break;
                case 7:
                    $("input[name='" + d.fieldName + "'][value='" + d.value + "']").click();
                    break;
                case 8:
                    $("input[name='" + d.fieldName + "'][value='" + d.value + "']").prop('checked', true);
                    break;
                case 9:
                case 12:
                    var controlIdValues = AbsenceSoft.BuildCriteria.criteriaData.filters.find(function (x) {
                        return x.fieldName === d.fieldName;
                    });
                    $('#' + d.fieldName + ' option').filter(function () {
                        if ($(this).val()) {
                            if (controlIdValues.values !== null) {
                                return controlIdValues.values.indexOf($(this).val()) > -1;
                            } else {
                                return controlIdValues.value === $(this).val();
                            }
                        }
                    }).prop('selected', true);
                    break;
            }
        }
        $("#startDate").val(moment(AbsenceSoft.BuildCriteria.criteriaData.startDate).format("MM/DD/YYYY"));
        $("#endDate").val(moment(AbsenceSoft.BuildCriteria.criteriaData.endDate).format("MM/DD/YYYY"));

        var reqid = utils.getQueryParamFromUrl("requestid");
        var rData = AbsenceSoft.Session.GetById(current.User.id, reqid);
        var groupBy = "none";
        if (rData && rData.data.groupby) {
            groupBy = rData.data.groupby;
        }
        $("input[name='groupby'][value='" + groupBy + "']").attr('checked', true).click();
        //  isGroupLoaded = true;
    }

    //generate the criteria
    AbsenceSoft.BuildCriteria.buildReportParam = function () {
        if (!utils.isNullOrWhitespace(AbsenceSoft.BuildCriteria.reportData)) {
            //heading
            AbsenceSoft.BuildCriteria.generateHeading(AbsenceSoft.BuildCriteria.reportData);

            //build group by          
            if (!utils.isNullOrWhitespace(AbsenceSoft.BuildCriteria.reportData.availableGroupingFields) && !isGroupLoaded) {
                generateGroupByHtml(AbsenceSoft.BuildCriteria.reportData.availableGroupingFields);
            }
            else {
                isGroupLoaded = true;
            }
            var enableCsvExport = AbsenceSoft.BuildCriteria.reportData.isExportableToCsv;
            var enablePdfExport = AbsenceSoft.BuildCriteria.reportData.isExportableToPdf;
            $('#export-csv').prop("disabled", !enableCsvExport);
            $('#export-pdf').prop("disabled", !enablePdfExport);
        }
    }

    AbsenceSoft.BuildCriteria.addDurationOptionsForBi = function () {
        var durationFiltersHTML = '<div id= "monthlyDuration"><select id="selectedMonth" class="form-control input-sm"><option value="1">January</option><option value="2">February</option><option value="3">March</option><option value="4">April</option><option value="5">May</option><option value="6">June</option><option value="7">July</option><option value="8">August</option><option value="9">September</option><option value="10">October</option><option value="11">November</option><option value="12">December</option></select><span class="iconPosition"><button class="btn btn-link tooltip-margin "  role="button" tabindex="0" data-toggle="tooltip" title="Monthly - data returned for month selected"><span class="fa fa-question-circle"></span></button></span></div>';
        durationFiltersHTML = durationFiltersHTML + '<div id="yearlyDuration"> <select id="selectedYear" class="form-control input-sm"><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option></select><span class="iconPosition"><button class="btn btn-link tooltip-margin"  role="button" tabindex="0"  data-toggle="tooltip" title="Yearly - data returned for year selected"><span class="fa fa-question-circle"></span></button></span></div><div id="quarterlyDuration"><select id="selectedQuarter" class="form-control input-sm"><option value="1">First Quarter</option><option value="2">Second Quarter</option><option value="3">Third Quarter</option><option value="4">Fourth Quarter</option></select><span class="iconPosition"><button class="btn btn-link tooltip-margin"  role="button" tabindex="0" data-toggle="tooltip" title="Quarterly - data returned for quarter selected"><span class="fa fa-question-circle"></span></button></span></div>';
        durationFiltersHTML = durationFiltersHTML + '<div id="daterangeDuration" ><div class="row"><div class="col-sm-5"><div class="input-group date editor"><input type="text" id="txtStartDate" class="form-control input-sm" data-provide="datepicker"></div></div><div class="col-sm-2 text-center"><label class="control-label">TO</label></div><div class="col-sm-5"><div class="input-group date editor"><input type="text" id="txtEndDate" class="form-control input-sm" data-provide="datepicker">';
        durationFiltersHTML = durationFiltersHTML + '</div></div></div><span class="iconPosition"><button class="btn btn-link tooltip-margin"  role="button" tabindex="0"  data-toggle="tooltip" title="Date Range - data returned for range entered by user" ><span class="fa fa-question-circle"></span></button><span class="iconPosition"></div><div id="rollingDuration"><div class="input-group date editor"><input type="text" id="rollingStartDate" class="form-control input-sm" data-provide="datepicker"></div><span class="iconPosition"><button class="btn btn-link tooltip-margin"  role="button" tabindex="0" data-toggle="tooltip" title=" Rolling 12 Months - data returned for 12 month period forward from date entered by user"><span class="fa fa-question-circle"></span></button><span class="iconPosition"></div>';

       
        return durationFiltersHTML;
    }

    AbsenceSoft.BuildCriteria.showRespectiveDurationValue = function () {
        AbsenceSoft.BuildCriteria.hideAllDurationValue();
        var selectedDuration = $('#Duration').val();
        switch (selectedDuration) {
            case "Monthly":
                $('#monthlyDuration').show();
                $(function () {
                    $('#monthlyDuration').tooltip();
                })
                break;
            case "Quarterly":
                $('#quarterlyDuration').show();
                $(function () {
                    $('#quarterlyDuration').tooltip();
                })
                break;
            case "Yearly":
                $('#yearlyDuration').show();
                $(function () {
                    $('#yearlyDuration').tooltip();
                })
                break;
            case "DateRange":
                $('#daterangeDuration').show();
                $(function () {
                    $('#daterangeDuration').tooltip();
                })
                break;
            case "Rolling":
                $('#rollingDuration').show();
                $(function () {
                    $('#rollingDuration').tooltip();
                })
                break;
        }
    }

    AbsenceSoft.BuildCriteria.hideAllDurationValue = function () {
        $('#monthlyDuration').hide();
        $('#yearlyDuration').hide();
        $('#quarterlyDuration').hide();
        $('#daterangeDuration').hide();
        $('#rollingDuration').hide();
    }

    AbsenceSoft.BuildCriteria.setDurationValue = function () {
        var durationValue = $('#Duration').val();
        switch (durationValue) {
            case 'Monthly':
                var selectedMonth = $('#selectedMonth').val();
                /// Months are 0 based, so month - 1 gets actual month
                startDate = new Date(startDate.getFullYear(), selectedMonth - 1, 1);
                /// Setting the date of the next month to 0 gets the last day of the month we want
                endDate = new Date(endDate.getFullYear(), selectedMonth, 0);
                break;
            case 'Quarterly':
                var selectedQuarter = $('#selectedQuarter').val();
                startDate = new Date(startDate.getFullYear(), selectedQuarter * 3 - 3, 1);
                endDate = new Date(startDate.getFullYear(), selectedQuarter * 3, 0);
                break;
            case 'Yearly':
                var selectedYear = $('#selectedYear').val();
                startDate = new Date(selectedYear, 0, 1);
                endDate = new Date(selectedYear, 11, 31);
                break;
            case 'DateRange':
                startDate = new Date(Date.parse($('#txtStartDate').val()));
                if (isNaN(startDate)) {
                    startDate = new Date();
                    $('#txtStartDate').val(startDate.formatMMDDYYYY());
                }

                endDate = new Date(Date.parse($('#txtEndDate').val()));
                if (isNaN(endDate)) {
                    endDate = startDate.addMonths(1);
                    $('#txtEndDate').val(endDate.formatMMDDYYYY());
                }

                if (endDate < startDate) {
                    AbsenceSoft.Reports.SetChartError("End Date must come before Start Date.");
                    return;
                }

                break;
            case 'Rolling':
                startDate = new Date(Date.parse($('#rollingStartDate').val()));
                if (isNaN(startDate)) {
                    startDate = new Date();
                    $('#rollingStartDate').val(startDate.formatMMDDYYYY());
                }
                // Rolling Year
                endDate = new Date(startDate.getFullYear() + 1, startDate.getMonth(), startDate.getDate());
                break;
            default:
                endDate = new Date();
                startDate = new Date(endDate.getFullYear() - 1, endDate.getMonth(), endDate.getDate());
                break;
        }
    }

    //generate name
    AbsenceSoft.BuildCriteria.generateHeading = function (d) {
        if (!utils.isNullOrWhitespace(d.category)) {
            $("#report-category").html(d.category);
        }
        if (!utils.isNullOrWhitespace(d.reportDescription)) {
            $("#report-name").html(d.reportDescription);
        }
    }

    //generate the group by buttons
    var generateGroupByHtml = function (d) {
        var wordLength = 0;
        var grpHtml = "";
        for (var key in d) {
            if (key !== 'custom') {
                if (wordLength < AbsenceSoft.Constant.const[key.toUpperCase()].length) {
                    wordLength = AbsenceSoft.Constant.const[key.toUpperCase()].length;
                }
            }
        }
        if (wordLength > 15) {
            grpHtml = '<div class="col-sm-6 col-xs-6"><div id="group-by" class="btn-group" style="width:100%;" data-toggle="buttons"><label>Group By</label>';
        }
        else {
            grpHtml = '<div class="col-sm-3 col-xs-3"><div id="group-by" class="btn-group" style="width:100%;" data-toggle="buttons"><label>Group By</label>';
        }

        var cnt = 0;
        for (var key in d) {
            if (key !== 'custom') {
                if (cnt === 0) {
                    grpHtml += '<label class="btn report-criteria-btn criteria-selected';
                    grpHtml += '" onclick="AbsenceSoft.BuildCriteria.makeActive(this);">'
                    grpHtml += '<input type="radio" name="groupby" value="' + key + '" ';
                    grpHtml += 'checked="checked"> ' + AbsenceSoft.Constant.const[key.toUpperCase()];
                    grpHtml += '</label>';
                }
                else {
                    grpHtml += '<label class="btn report-criteria-btn ';
                    grpHtml += '" onclick="AbsenceSoft.BuildCriteria.makeActive(this);">'
                    grpHtml += '<input type="radio" name="groupby" value="' + key + '"';
                    grpHtml += '> ' + AbsenceSoft.Constant.const[key.toUpperCase()];
                    grpHtml += '</label>';
                }
                cnt += 1;
            }
        }
        grpHtml += '</div></div>';
        //reset if no data
        if (cnt === 0) {
            grpHtml = "";
        }


        $("#reportCriteriaFilters").append(grpHtml);

        var interval = setInterval(function () {
            if (isCriteriaLoaded === true) {
                clearInterval(interval);
            }
        }, 100);

        resetHeight();

        isGroupLoaded = true;
    }

    //set the active class on select
    AbsenceSoft.BuildCriteria.makeActive = function (obj) {
        var btn = $(obj);
        btn.parent().find('.criteria-selected').each(function (i, e) { $(e).removeClass('criteria-selected') });
        btn.addClass('criteria-selected');
    }

    //set the muti-active
    AbsenceSoft.BuildCriteria.makeMultiActive = function (obj) {
        var btn = $(obj);
        var cls = 'criteria-selected';
        if (btn.hasClass(cls)) {
            btn.removeClass(cls);
        } else {
            btn.addClass();
        }
    }

    //Get request object
    AbsenceSoft.BuildCriteria.getRequestObject = function () {
        type = utils.getQueryParamFromUrl("type");
        if (type !== "adhoc") {
            setCriteriaFilterValues();
        }
       
        var arg = {};
        arg.CustomerId = AbsenceSoft.Current.Customer.id;
        arg.ReportId = AbsenceSoft.BuildCriteria.reportData.id;

        if (!arg.ReportId) {
            if (utils.getQueryParamFromUrl("requestid")) {
                var requestedReportId = utils.getQueryParamFromUrl("requestid");
                var rData = AbsenceSoft.Session.GetById(current.User.id, requestedReportId);
                arg.ReportId = rData.data.reportid;
            }
        }

        var reportype = 0;
        if (type === "adhoc") {
            reportype = 1;
        }
        else if (type === "bi") {
            setBiFilterCriteria($('#Duration').val(), arg);
            setCriteriaFilterValues();
        }

        arg.ReportRequestType = reportype;
        arg.ExportType = 0;
        var grpType = $('input[name="groupby"]:checked').val();
        if (!grpType || grpType === "none" || grpType === "") {
            grpType = null;
        }

        if (type === "adhoc") {
            arg = AbsenceSoft.AdhocReport2_0.buildReportData();
        }
        else
        {
            arg.reportDefinition = {
                Name: AbsenceSoft.BuildCriteria.reportData.name,
                Description: null,
                ReportType: null,
                Fields: null,
                Criteria: AbsenceSoft.BuildCriteria.criteriaData,
                GroupType: grpType,
                GroupByColumns: null
            };
        }
        

        arg.pageNumber = 1;
        arg.pageSize = 10;

        console.log(arg);
        return arg;
    }

    var getDurationFilterValue = function () {
        var durationValue = {};
        if ($("#Duration").val() === "Monthly") {
            durationValue["Monthly"] = $("#selectedMonth").val();
        }
        else if ($("#Duration").val() == "Quarterly") {
            durationValue["Quarterly"] = $("#selectedQuarter").val();
        }
        else if ($("#Duration").val() == "Yearly") {
            durationValue["Yearly"] = $("#selectedYear").val();
        }
        else if ($("#Duration").val() == "DateRange") {
            durationValue["DateRange"] = $("#txtStartDate").val() + " to " + $("#txtEndDate").val() ;
        }
        else if ($("#Duration").val() == "Rolling") {
            durationValue["Rolling"] = $("#rollingStartDate").val();
        }
        return durationValue;

    }

    var getYearsDropDownHtml = function (numberOfYearsToDisplay) {
        var today = new Date();
        var currentYear = today.getFullYear();
        var yearsDropDownHtml = '<div id="yearlyDuration"> <select id="selectedYear" class="form-control input-sm">';
        var optionYear;
        for (i = 0; i < numberOfYearsToDisplay; i++) {
            optionYear = currentYear - numberOfYearsToDisplay + i + 1;
            yearsDropDownHtml += '<option value="' + optionYear.toString() + '">' + optionYear.toString() + '</option>';
        }
        yearsDropDownHtml += '</select></div>';
        return yearsDropDownHtml;
    }
    var setBiFilterCriteria = function (durationValue) {
        var selStartDate = moment();
        var selEndDate = moment();
        switch (durationValue) {
            case 'Monthly':
                var selectedMonth = $('#selectedMonth').val();

                /// Months are 0 based, so month - 1 gets actual month                
                AbsenceSoft.BuildCriteria.criteriaData.startDate = new Date(moment().date(0).month(selectedMonth - 2).year((selectedMonth === "1") ? moment().year() - 1 : moment().year()).format("YYYY-MM-DD")).toUTCString();
                /// Setting the date of the next month to 0 gets the last day of the month we want       
                noOfDays = parseInt(moment(new Date(moment().date(1).month(selectedMonth - 1).year(moment().year()).format("YYYY-MM-DD"))).daysInMonth()) - 1;
                AbsenceSoft.BuildCriteria.criteriaData.endDate = new Date(moment().date(noOfDays).month(selectedMonth - 1).year(moment().year()).format("YYYY-MM-DD")).toUTCString();
                break;
            case 'Quarterly':
                var selectedQuarter = $('#selectedQuarter').val();

                AbsenceSoft.BuildCriteria.criteriaData.startDate = new Date(moment().year(), selectedQuarter * 3 - 3, 1).toUTCString();
                AbsenceSoft.BuildCriteria.criteriaData.endDate = new Date(moment().year(), selectedQuarter * 3, 0).toUTCString();
                break;
            case 'Yearly':
                var selectedYear = $('#selectedYear').val();
                AbsenceSoft.BuildCriteria.criteriaData.startDate = new Date(selectedYear, 0, 1).toUTCString();
                AbsenceSoft.BuildCriteria.criteriaData.endDate = new Date(selectedYear, 11, 31).toUTCString();
                break;
            case 'DateRange':
                if ($('#startDate').val() === '') {
                    selStartDate = new Date().toUTCString();
                    $('#startDate').val(selStartDate.formatMMDDYYYY());
                }

                if ($('#endDate').val() === '') {
                    selEndDate = selStartDate.addMonths(1);
                    $('#endDate').val(selEndDate.formatMMDDYYYY());
                }

                if (selEndDate < selStartDate) {
                    AbsenceSoft.Reports.SetChartError("Start Date must come before End Date.");
                    return;
                }
                AbsenceSoft.BuildCriteria.criteriaData.startDate = selStartDate.toUTCString();
                AbsenceSoft.BuildCriteria.criteriaData.endDate = selEndDate.toUTCString();
                break;
            case 'Rolling':
                if ($('#startDate').val() === '') {
                    selStartDate = moment(moment().year() + '-' + (moment().month() + 1) + '-' + '01');
                    selEndDate = moment((moment().year() + 1) + '-' + (moment().month() + 1) + '-' + '01');
                    $('#rollingStartDate').val(selStartDate.formatMMDDYYYY());
                }
                // Rolling Year

                AbsenceSoft.BuildCriteria.criteriaData.startDate = selStartDate.toUTCString();
                AbsenceSoft.BuildCriteria.criteriaData.endDate = selEndDate.toUTCString();
                break;
            default:
               // the dates are converting to utcstring on value set
                selEndDate = new Date();
                selStartDate = new Date(selEndDate.getFullYear() - 1, selEndDate.getMonth(), selEndDate.getDate());               
                AbsenceSoft.BuildCriteria.criteriaData.startDate = selStartDate.toUTCString();
                AbsenceSoft.BuildCriteria.criteriaData.endDate = selEndDate.toUTCString();
                break;
        }
        $('#startDate').val(AbsenceSoft.BuildCriteria.criteriaData.startDate);
        $('#endDate').val(AbsenceSoft.BuildCriteria.criteriaData.endDate);
    }

    //set the criteria values
    var setCriteriaFilterValues = function () {
        AbsenceSoft.BuildCriteria.criteriaData.startDate = $('#startDate').val();
        AbsenceSoft.BuildCriteria.criteriaData.endDate = $('#endDate').val();

        var english = "";
        english += "From '" + $('#startDate').val() + "' through '" + $('#endDate').val() + "'";

        if ($('#ageFrom').is(':visible') === true && $('#ageFrom').is(':hidden') === false) {

            if ($('#ageFrom').val().length > 0 && $('#ageTo').val().length > 0) {
                english += " where Age between " + $('#ageFrom').val() + " and " + $('#ageTo').val();
            }
            else {
                if ($('#ageFrom').val().length > 0) {
                    english += " where Age greater than " + $('#ageFrom').val();
                }

                if ($('#ageTo').val().length > 0) {
                    english += " where Age less than " + $('#ageTo').val();
                }
            }
        }

        for (var i = 0; i < AbsenceSoft.BuildCriteria.criteriaData.filters.length; i++) {
            var d = AbsenceSoft.BuildCriteria.criteriaData.filters[i];
            switch (d.controlType) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 11:
                    var valSet = $("#" + d.fieldName).val();
                    if (valSet.length > 0) {
                        d.value = valSet;
                        english = setEnglishVal(d, english);
                    }
                    else {
                        d.value = null;
                    }
                    break;
                case 7:
                    d.value = $('input[name="' + d.fieldName + '"]:checked').val();
                    english = setEnglishVal(d, english);
                    break;
                case 8:
                    d.values = $('input[name="' + d.fieldName + '"]:checked').val();
                    english = setEnglishVal(d, english);
                    break;
                case 9:
                case 12:
                    d.values = $('#' + d.fieldName).val();
                    english = setEnglishVal(d, english);
                    break;
            }
        }
        AbsenceSoft.BuildCriteria.criteriaData.plainEnglish = english;
    }

    var setEnglishVal = function (d, english) {
        if ((d.value && d.value != '') || (d.values && d.values.length > 0)) {
            if (english.indexOf('where') <= 0) {
                english += " where ";
            }
            else {
                english += ", ";
            }
            if (d.options && d.options.length > 0 && d.value) {
                var val = $.isNumeric(d.value) && d.value.length < 5 ? parseInt(d.value) : d.value;
                english += d.prompt + " is [" + (_.find(d.options, "value", val) ? _.find(d.options, "value", val).text : '') + "]";
            }
            else if (d.values && d.values.length > 0) {
                english += d.prompt + " is in ";

                var multiVals = '[';
                for (var item = 0; item < d.values.length; item++) {
                    var vals = $.isNumeric(d.values[item]) && d.values[item].length < 5 ? parseInt(d.values[item]) : d.values[item];
                    multiVals += _.find(d.options, "value", vals) ? _.find(d.options, "value", vals).text + "," : "";
                }
                var currentIndex = multiVals.lastIndexOf(",");
                multiVals = multiVals.substring(0, currentIndex);
                english += multiVals + ']';
            }
            else {
                english += d.prompt + " is [" + d.value + "]";
            }
        }
        return english;
    }

    var resetHeight = function () {
        $(".criteria-result").css({ height: 'auto' });
    }

    //return the group value by index
    AbsenceSoft.BuildCriteria.GetGroupByType = function (index) {
        var arr = ['custom', 'workState', 'reason', 'policy', 'organization', 'location', 'leave', 'duration', 'accommodationType', 'caseManager', 'condition', 'conditionByCondition', 'conditionByOrganization', 'durationByCondition', 'durationByOrganization', 'caseReport', 'caseReportByDay', 'caseReportByOrganization', 'needleSticks', 'injuryDetails', 'biReportTotalLeavesByReason', 'biReportTotalLeavesByLocation'];
        if (index < arr.length) {
            return arr[index];
        }
        return "";
    }

    //call it in document ready
    $(document).ready(function () {
        if ($(location).attr('href').indexOf("reporting/run") > -1) {
            $("#startenddate").hide();
            $("#agefromto").hide();
            $("#rerunreport").hide();

            type = utils.getQueryParamFromUrl("type");
            id = utils.getQueryParamFromUrl("reportId");
            sharedReportId = utils.getQueryParamFromUrl("sharedreportid");

            AbsenceSoft.Current.getCurrentData();
            getReportAndCriteria();
            $('#reportCriteria').collapse('show');

            $(".cancel-btn").click(function () {
                $("#report-settings").collapse('hide');
            });

            $('.reporting-main-content, .navbar-fixed-top').click(function (evt) {
                if (!$(evt.target).closest('#report-settings').length && $('#report-settings').is(':visible') === true && $('#report-settings').is(':hidden') === false) {
                    $("#report-settings").collapse('hide');
                }
            });

            $("#showBtn").click(function () {
                $("#reportCriteria").stop().slideToggle(300);
                $(this).find(".fa-chevron-up, .fa-chevron-down").toggle();
                
                if (type === 'result') {
                    $("#report-outer").css("display", "block");
                } else {
                    $("#report-outer").css("display", "none");
                }

            });

        }

        $(function () {
            $('#monthlyDuration').tooltip();
        })

        $('.input-append input').click(function () {
            var $this = $(this);
            var tileNum = $this.attr("id");
            $("#" + tileNum).closest("div").data("datetimepicker").show();
            $('.bootstrap-datetimepicker-widget ul').css("padding", "0");

        });


    });

    $(window).resize(function () {
        setHeight();
    });

    $(window).load(function () {      
        setHeight();
    });
    function setHeight() {
        var $theWindowSize = $(this).height();
        if ($theWindowSize < 645) {
            if (!$('#reportCriteria').hasClass('scroll-vertical')) {
                $('#reportCriteria').addClass('scroll-vertical');
            }
            if (!$('#schedulereport').hasClass('scroll-vertical')) {
                $('#schedulereport').addClass('scroll-vertical');
            }
        }
        else {
            $('#reportCriteria').removeClass('scroll-vertical');
            $('#schedulereport').removeClass('scroll-vertical');
        }
    }
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);