﻿(function (AbsenceSoft, $) {
    AbsenceSoft.Enums = AbsenceSoft.Enums || {};

    AbsenceSoft.Enums.enums = Object.freeze({
        ReportStatus: Object.freeze({
            Pending: 6301, Processing: 6302, Completed: 6303, Canceled: 6304, Failed: 6305, Scheduled: 6306
        }),
        ReportExportFormat: Object.freeze({ RUN: 3, CSV: 1, PDF: 0 }),
        ReportRecurrenceType: Object.freeze({ OneTime: 0, Daily: 1, Weekly: 2, Monthly: 3, Yearly: 4 })       
    });

    AbsenceSoft.Enums.CaseExportFormat = Object.freeze(
        {
            Pdf: 1,
            Zip: 2,
            GetDescription: function (caseExportFormat) {
                switch (caseExportFormat) {
                    case AbsenceSoft.Enums.CaseExportFormat.Pdf:
                        return "Pdf";
                    case AbsenceSoft.Enums.CaseExportFormat.Zip:
                        return "Zip";
                    default:
                        return;
                }
            }
        });

    AbsenceSoft.Enums.CaseExportProcessingStatus = Object.freeze(
        {
            Pending: 0,
            Processing: 1,
            Complete: 2,
            Failed: -1,
            GetDescription: function (caseExportProcessingStatus) {
                switch (caseExportProcessingStatus) {
                    case AbsenceSoft.Enums.CaseExportProcessingStatus.Pending:
                        return "Pending";
                    case AbsenceSoft.Enums.CaseExportProcessingStatus.Processing:
                        return "Processing";
                    case AbsenceSoft.Enums.CaseExportProcessingStatus.Complete:
                        return "Complete";
                    case AbsenceSoft.Enums.CaseExportProcessingStatus.Failed:
                        return "Failed";
                    default:
                        return;
                }
            }
        });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);