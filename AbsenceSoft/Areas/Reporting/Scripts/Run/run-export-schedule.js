﻿(function (AbsenceSoft, $) {
    AbsenceSoft.RunExportSchedule = AbsenceSoft.RunExportSchedule || {};

    //class references
    var utils = AbsenceSoft.Utilities;
    var current = AbsenceSoft.Current;

    //local variables
    var requestReportEndPoint = 'reports/request';  
    var scheduleReportEndPoint = 'reports/schedule';
    var getCompletedReportEndPoint = 'reports/results/';
    var notyTimeout = 3000;
    
    AbsenceSoft.RunExportSchedule.RequestedReportArgs = '';
   

    //return empty string on undefined
    AbsenceSoft.RunExportSchedule.returnEmptyIfUndefined = function (obj) {
        if (obj) {
            return obj;
        }
        return "";
    }

    //request a report execution
    AbsenceSoft.RunExportSchedule.requestReport = function (requestedExportFormat) {            
        //get the request objects
        AbsenceSoft.RunExportSchedule.RequestedReportArgs = AbsenceSoft.BuildCriteria.getRequestObject();
        AbsenceSoft.RunExportSchedule.RequestedReportArgs.Reportparam = AbsenceSoft.BuildCriteria.reportData;
        AbsenceSoft.RunExportSchedule.RequestedReportArgs.ExportFormat = requestedExportFormat;

        var type = AbsenceSoft.Utilities.getQueryParamFromUrl("type");
        
        //validate start and end date
        if (type!=='adhoc' && (!AbsenceSoft.RunExportSchedule.RequestedReportArgs.reportDefinition.Criteria.startDate || !AbsenceSoft.RunExportSchedule.RequestedReportArgs.reportDefinition.Criteria.endDate)) {
            noty({ type: 'error', text: '<b>!Error</b><br/>Please select the date range', layout: 'topRight', timeout: notyTimeout });
            return;
        }

        if (requestedExportFormat === AbsenceSoft.Enums.enums.ReportExportFormat.RUN) {
            AbsenceSoft.RunExportSchedule.RequestedReportArgs.PageNumber = 1;
            AbsenceSoft.RunExportSchedule.RequestedReportArgs.PageSize = 500;
        }
        else {
            AbsenceSoft.RunExportSchedule.RequestedReportArgs.PageNumber = 1;
            AbsenceSoft.RunExportSchedule.RequestedReportArgs.PageSize = 50000;
        }

        //check if request is for show report and set the arguments from storage
        if (utils.getQueryParamFromUrl("requestid")) {
            var requestId = utils.getQueryParamFromUrl("requestid");
            var gType = '';
            var rData = AbsenceSoft.Session.GetById(current.User.id, requestId);
            if (rData) {               
                AbsenceSoft.RunExportSchedule.RequestedReportArgs.Reportparam.id = rData.data.reportid;
                AbsenceSoft.RunExportSchedule.RequestedReportArgs.Reportparam.isExportableToCsv = rData.data.isExportableToCsv;
                AbsenceSoft.RunExportSchedule.RequestedReportArgs.Reportparam.isExportableToPdf = rData.data.isExportableToPdf;
                AbsenceSoft.RunExportSchedule.RequestedReportArgs.Reportparam.availableGroupingFields = rData.data.availableGroupingFields;  
                AbsenceSoft.RunExportSchedule.RequestedReportArgs.Reportparam.reportType = rData.data.reportType; 
            }           
        }

        //call service
        AbsenceSoft.HttpUtils.Post(AbsenceSoft.ReportOperations.Module, requestReportEndPoint, AbsenceSoft.RunExportSchedule.RequestedReportArgs, function (data) {
            //add the data to queue manager
            var d = JSON.parse(data);
            if (d.success === true) {
                if (requestedExportFormat === AbsenceSoft.Enums.enums.ReportExportFormat.RUN) {
                    noty({ type: 'success', text: '<b>We are processing your report<b/><br/>You can wait or can do some other task.<br/>We will notify once complete.', layout: 'topRight', timeout: notyTimeout });
                }
                else {
                    noty({ type: 'success', text: '<b>We are processing your report for exporting data.<b/><br/>You can wait or can do some other task.<br/>We will notify once complete.', layout: 'topRight', timeout: notyTimeout });
                }
               
                var plainEnglish = "Adhoc Report";
                if (AbsenceSoft.RunExportSchedule.RequestedReportArgs.reportDefinition.Criteria && AbsenceSoft.RunExportSchedule.RequestedReportArgs.reportDefinition.Criteria.plainEnglish && type !== 'adhoc') {
                    
                    plainEnglish = AbsenceSoft.RunExportSchedule.RequestedReportArgs.reportDefinition.Criteria.plainEnglish;                    
                }   
                
                var reportParamData = {
                    reportid: AbsenceSoft.RunExportSchedule.RequestedReportArgs.Reportparam.id,
                    groupby: AbsenceSoft.RunExportSchedule.RequestedReportArgs.reportDefinition.GroupType,
                    isExportableToCsv: AbsenceSoft.RunExportSchedule.RequestedReportArgs.Reportparam.isExportableToCsv,
                    isExportableToPdf: AbsenceSoft.RunExportSchedule.RequestedReportArgs.Reportparam.isExportableToPdf,
                    availableGroupingFields: AbsenceSoft.RunExportSchedule.RequestedReportArgs.Reportparam.availableGroupingFields,
                    requestedExportFormat: requestedExportFormat,
                    reportType: AbsenceSoft.RunExportSchedule.RequestedReportArgs.Reportparam.reportType ? AbsenceSoft.RunExportSchedule.RequestedReportArgs.Reportparam.reportType : utils.getQueryParamFromUrl("type"),
                    requestedData: type === 'adhoc' ? AbsenceSoft.RunExportSchedule.RequestedReportArgs : null
                };

                //add required report data to local storage
                AbsenceSoft.Session.Add(current.User.id, d.data.id, reportParamData);
                //add to queue manager
                AbsenceSoft.QueueStorage.Add(current.User.id, d.data.id, AbsenceSoft.RunExportSchedule.returnEmptyIfUndefined(AbsenceSoft.RunExportSchedule.RequestedReportArgs.reportDefinition.Name), AbsenceSoft.RunExportSchedule.returnEmptyIfUndefined(plainEnglish));
            } else {
                noty({ type: 'error', text: '<b>!Error</b><br/>' + d.message, layout: 'topRight', timeout: notyTimeout });
            }
        });
    }

    

    //show report
    AbsenceSoft.RunExportSchedule.showReport = function () {
        var id = utils.getQueryParamFromUrl("requestid");
        if (id) {
            AbsenceSoft.HttpUtils.Get(AbsenceSoft.ReportOperations.Module, getCompletedReportEndPoint + id, AbsenceSoft.RunExportSchedule.renderReport);
        }
    }

    //show the result
    AbsenceSoft.RunExportSchedule.renderReport = function (reportResult) {
        var rerportType = utils.getQueryParamFromUrl("type");
        var reportData = jQuery.parseJSON(reportResult);
        if (reportData.success) {
            if (rerportType === 'adhoc') {

                AbsenceSoft.AdhocReport2_0.loadReport(reportData.data);
                console.log(reportData.data);
            }
            else {

                //Set Header
                AbsenceSoft.BuildCriteria.generateHeading(reportData.data);

                //set criteria
                AbsenceSoft.BuildCriteria.criteriaData = reportData.data.criteria;
                AbsenceSoft.BuildCriteria.buildCriteriaParam();
                AbsenceSoft.BuildCriteria.reportData = { category: reportData.data.category, name: reportData.data.name };
                $(".select2-multiple").select2({ closeOnSelect: true });
                $("#reportCriteria").collapse('hide');

                if (reportData.data.category === 'Business Intelligence Report' || reportData.data.category === 'Productivity Comparison') {
                    $("#dateRanges").hide();
                }

                $('#filterCriteria').val(reportData.data.criteria.plainEnglish);
                if (reportData.data && reportData.data.jsonData && reportData.data.jsonData.length > 0) {
                    if (reportData.data.chart !== null) {
                        $('#reportResult').drawChart({
                            chartType: 2,
                            isGroup: false,
                            data: reportData.data.jsonData,
                            chart: reportData.data.chart
                        });
                    }
                    else if (reportData.data.jsonData[0].Key) {
                        $('#reportResult').drawChart({
                            chartType: 1,
                            isGroup: true,
                            data: reportData.data.jsonData
                        });
                    }
                    else {
                        $('#reportResult').drawChart({
                            chartType: 1,
                            isGroup: false,
                            data: reportData.data.jsonData
                        });
                    }
                }
                else {
                    $('#reportResult').html('No data found for selected criteria.')
                }

                var rData = AbsenceSoft.Session.GetById(current.User.id, reqid);
                if (rData) {
                    $('#export-csv').prop("disabled", !rData.data.isExportableToPdf);
                    $('#export-pdf').prop("disabled", !rData.data.isExportableToCsv);
                }
            }
        }
        else {
            $("#report-saved-alert").show();
            $("#report-saved-alert span").text('Could not get data for the request.');
        }
    }

    //schedule a report
    AbsenceSoft.RunExportSchedule.scheduleReport = function () {
        if ($.trim($("#schRepSelTeam").val()).length === 0 && $.trim($("#schRepSelRole").val()).length === 0 && $.trim($("#schRepSelUser").val()).length === 0) {
            noty({ type: 'error', text: '<b>!Error</b><br/> Please select User/ Role/ Team.', layout: 'topRight', timeout: notyTimeout });
            return;
        }
        else if ($.trim($('#sch-date-picker input').val()).length === 0 || $.trim($('#sch-time-picker input').val()).length === 0)
        {
            noty({ type: 'error', text: '<b>!Error</b><br/> Please select Schedule date and time.', layout: 'topRight', timeout: notyTimeout });
            return;
        }

        var requestInfo = AbsenceSoft.BuildCriteria.getRequestObject();       
        requestInfo.ExportFormat = $('#export-format option:selected').val()
        var sharingInfo = {
            "SharedWith": $('input[name="schRepSharedWith"]:checked').val(),
            "TeamKeys": $("#schRepSelTeam").val(),
            "RoleNames": $("#schRepSelRole").val(),
            "UserKeys": $("#schRepSelUser").val()
        }
        var schDt = moment($('#sch-date-picker input').val() + ' ' + $('#sch-time-picker input').val());        
        var ScheduleReportArgs = {
            "RequestInfo": requestInfo,
            "ScheduleInfo": {
                "RecurrenceType": $('#schedule-frequency option:selected').val(),
                "StartDate": schDt.utc().format(),
                "WeeklySelectedDays": {
                    "Sunday": ($('#schedule-day option:selected').text() === "Sunday"),
                    "Monday": ($('#schedule-day option:selected').text() === "Monday"),
                    "Tuesday": ($('#schedule-day option:selected').text() === "Tuesday"),
                    "Wednesday": ($('#schedule-day option:selected').text() === "Wednesday"),
                    "Thursday": ($('#schedule-day option:selected').text() === "Thursday"),
                    "Friday": ($('#schedule-day option:selected').text() === "Friday"),
                    "Saturday": ($('#schedule-day option:selected').text() === "Saturday")
                }
            },
            "SharingInfo": sharingInfo
        };

        try {
            AbsenceSoft.HttpUtils.Post(AbsenceSoft.ReportOperations.Module, scheduleReportEndPoint, ScheduleReportArgs, function (data) {
                var d = JSON.parse(data);
                if (d.success === true) {
                    noty({ type: 'success', text: 'Report scheduled successfully!', layout: 'topRight', timeout: notyTimeout });
                } else {
                    noty({ type: 'error', text: '<b>!Error</b><br/>' + d.message, layout: 'topRight', timeout: notyTimeout });
                }
            });
        }
        catch (ex) {
            noty({ type: 'error', text: '<b>!Error</b><br/>' + ex, layout: 'topRight', timeout: notyTimeout });
        }
    }

    //show hide recipients
    AbsenceSoft.RunExportSchedule.showRecipientsInput = function () {
        $("#schRepUser").hide();
        $("#schRepTeam").hide();
        $("#schRepRole").hide();

        //attach event
        $('input[name="schRepSharedWith"]').click(AbsenceSoft.RunExportSchedule.showRespectiveInput);

        //fill the data
        AbsenceSoft.RunExportSchedule.fillUsers();
        AbsenceSoft.RunExportSchedule.fillRoles();
        AbsenceSoft.RunExportSchedule.fillTeams();

        $(".select2-multiple").select2({ closeOnSelect: true });
    }

    //on clicking selection
    AbsenceSoft.RunExportSchedule.showRespectiveInput = function (sel, obj) {
        selection = $('input[name="schRepSharedWith"]:checked').val();
        if (selection === "USER") {
            $("#schRepUser").show();
            $("#schRepTeam").hide();
            $("#schRepRole").hide();
        } else if (selection === "TEAM") {
            $("#schRepUser").hide();
            $("#schRepTeam").show();
            $("#schRepRole").hide();
        } else if (selection === "ROLE") {
            $("#schRepUser").hide();
            $("#schRepTeam").hide();
            $("#schRepRole").show();
        }
    }

    //fill users
    AbsenceSoft.RunExportSchedule.fillUsers = function () {
        var users = AbsenceSoft.Current.Users;

        $.each(users, function (i, item) {
            $('#schRepSelUser').append($('<option>', {
                value: item.Id,
                text: item.DisplayName
            }));
        });
    }

    //fill roles
    AbsenceSoft.RunExportSchedule.fillRoles = function () {
        var roles = AbsenceSoft.Current.Roles;

        $.each(roles, function (i, item) {
            $('#schRepSelRole').append($('<option>', {
                value: item.Id,
                text: item.Name
            }));
        });
    }

    //fill team
    AbsenceSoft.RunExportSchedule.fillTeams = function () {
        var teams = AbsenceSoft.Current.Teams;
        if (teams && teams.length > 0) {
            $.each(teams, function (i, item) {
                try {
                    var d = $.parseJSON(item);
                    $('#schRepSelTeam').append($('<option>', {
                        value: d.id,
                        text: d.name
                    }));
                }
                catch (ex) {
                    console.log(ex);
                }
            });
        }
    }

    //bind on the respective page
    $(document).ready(function () {

        if ($(location).attr('href').toLowerCase().indexOf("reporting/run") > -1 || $(location).attr('href').toLowerCase().indexOf("reporting/adhoc") > -1) {
            //get the type and the id
           var type = utils.getQueryParamFromUrl("type");        
         
            //bind export buttons
            $('#export-csv').on('click', function () {
                AbsenceSoft.RunExportSchedule.requestReport(AbsenceSoft.Enums.enums.ReportExportFormat.CSV);
            });
            $('#export-pdf').on('click', function () {
                AbsenceSoft.RunExportSchedule.requestReport(AbsenceSoft.Enums.enums.ReportExportFormat.PDF);
            });

            if (type === 'result' || ((type === 'shared' || type==='adhoc') && reqid)) { 
                //show report
                AbsenceSoft.RunExportSchedule.showReport();
                $("#run-report-span").text("Re-Run Report");              
            } else {
                $("#run-report-span").text("On Page Results");
            }
             
            if (type === "bi") {
                $("#dateRanges").hide();
            }

            $('#run-report').on('click', function () {
                AbsenceSoft.RunExportSchedule.requestReport(AbsenceSoft.Enums.enums.ReportExportFormat.RUN);
            });

            $('#schedule-report').on('click', function () {
                AbsenceSoft.RunExportSchedule.scheduleReport();
            });

            //attach event to get recipeient list on click
            $("#schReport").click(function () {
                AbsenceSoft.RunExportSchedule.showRecipientsInput();
            });
        }
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);