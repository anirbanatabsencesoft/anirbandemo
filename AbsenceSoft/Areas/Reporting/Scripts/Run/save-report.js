﻿(function (AbsenceSoft, $) {
    AbsenceSoft.Save = AbsenceSoft.Save || {};

    var selection = "SELF";
    var notyTimeout = 3000;

    //save
    AbsenceSoft.Save.saveReport = function () {
        var arg = {};
        arg.ReportName = $("#saveReportName").val();

        if (!arg.ReportName) {
            noty({ type: 'error', text: '<b>!Error</b><br/>Report name is missing.', layout: 'topRight', timeout: notyTimeout });
            return;
        }

        arg.SharedWith = selection;
        arg.UserKeys = [AbsenceSoft.Current.User.id];
        arg.Argument = AbsenceSoft.BuildCriteria.getRequestObject();

        var t = JSON.stringify(arg);
        AbsenceSoft.HttpUtils.Post("reporting", "reports/shared/save", arg, function (data) {
            var d = JSON.parse(data);
            if (d.success) {
                noty({ type: 'success', text: '<b>Success</b><br/>Report saved.', layout: 'topRight', timeout: notyTimeout });
                $("#report-settings").collapse('hide');
                $("#saveReportName").val('');
            } else {
                noty({ type: 'error', text: '<b>!Error</b><br/>' + d.message, layout: 'topRight', timeout: notyTimeout });
            }
        });
    }


    $(document).ready(function () {
        $("#save-report").click(AbsenceSoft.Save.saveReport);
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);