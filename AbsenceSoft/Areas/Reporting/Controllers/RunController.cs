﻿using AbsenceSoft.Areas.Reporting.Models;
using AbsenceSoft.Controllers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Web.Attributes;
using AT.Logic.Reporting.Reports;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Areas.Reporting.Controllers
{
    [Secure(Feature.ReportingTwoBeta, "RunCaseManagerReport", "RunBIReport", "RunAdHocReport", "RunOshaReport")]
    public class RunController : BaseController
    {
        // GET: Reporting/Run
        public ActionResult Index(string reportId, string requestId, string sharedReportId, string type)
        {
            if ((string.IsNullOrWhiteSpace(reportId) || string.IsNullOrWhiteSpace(requestId) || string.IsNullOrWhiteSpace(sharedReportId)) && string.IsNullOrWhiteSpace(type))
            {
                return RedirectToAction("Index", "Home", new { area = "Reporting" });
            }

            // Wire up the API

            RunReportViewModel report = new RunReportViewModel()
            {
                Category = $"{type} Report"
            };

            ViewBag.ReportId = reportId;
            return View(report);
        }

        [Secure("RunAdHocReport")]       
        public ActionResult Adhoc()
        {
            return View();
        }
    }
}