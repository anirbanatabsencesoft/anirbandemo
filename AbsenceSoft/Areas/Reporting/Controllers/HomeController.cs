﻿using AbsenceSoft.Areas.Reporting.Models;
using AbsenceSoft.Controllers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Web.Attributes;
using AT.Api.Core.Common;
using AT.Api.Reporting.Controllers;
using AT.Common.Core;
using AT.Logic.Reporting.Reports;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Areas.Reporting.Controllers
{
    [Secure(Feature.ReportingTwoBeta, "RunCaseManagerReport", "RunBIReport", "RunAdHocReport", "RunOshaReport")]
    public class HomeController : BaseController
    {
        // GET: Reporting/Home
        [Title("Reporting 2.0")]
        public ActionResult Index()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult ReportingSidebar()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult AllSharedReport()
        {
            return View("AllSharedReport");
        }

        [HttpGet]
        public async Task<ActionResult> GetReportsForCategory(string categoryName)
        {
            // wire up the api
            //var reportingApi = new ServiceFactory(Utilities.GetTokenFromClaims());
            //var results = await reportingApi.ExecuteService<ReportingController, string, List<BaseReport>>(categoryName, rc => rc.GetReportsByMainCategory(categoryName));
            ApiExecutionController ctrl = new ApiExecutionController();
            var stringData = await ctrl.CallService(new ApiExecutionController.ExecuteArgs() { Component = "reporting", EndPoint = "reports/operation/get/maincategory/" + categoryName, Method = "GET", PostedData = "" });
            var jset = new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.All };
            var results = JsonConvert.DeserializeObject<ResultSet<List<AT.Logic.Reporting.Reports.BaseReport>>>(Convert.ToString(stringData), jset);

            List<ReportViewModel> reportList = new List<ReportViewModel>();
            // We aren't using the null coalesce here because we want to send back and empty list if nothing comes back
            //if (results.Data != null) {
            //    reportList = results.Data.Select(br => new ReportViewModel(br)).ToList();
            //}

            return PartialView("_ReportList", reportList);
        }
    }
}