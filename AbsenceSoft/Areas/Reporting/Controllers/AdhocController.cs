﻿using AbsenceSoft.Controllers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Web.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Areas.Reporting.Controllers
{
    [RouteArea("Reporting")]
    [Secure(Feature.ReportingTwoBeta, "RunCaseManagerReport", "RunBIReport", "RunAdHocReport", "RunOshaReport")]
    public class AdhocController : BaseController
    {
        // GET: Reporting/Adhoc
        [Title("Adhoc 2.0")]
        public ActionResult Index()
        {
            return View();
        }

       
        [HttpGet, Title("Ad-Hoc 2.0 Report")]
        public ActionResult AdhocReport()
        {
            return View();
        }

    }
}