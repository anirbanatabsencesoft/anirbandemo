﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson.Serialization;
using System;
using System.Linq;

namespace AbsenceSoft.Tests.ProdSupport
{
    [TestClass]
    public class AT3908Tests
    {
        [TestMethod]
        public void AT3908PolicySelectionIssue()
        {
            #region Employee JSON
            var json = @"
{ 
    ""_id"" : ObjectId(""57f544a83de0961428b9c26b""), 
    ""LastERow"" : ""vx5vWDCCF+2xgRRgGsdwyQ=="", 
    ""cdt"" : ISODate(""2016-10-05T18:21:28.258+0000""), 
    ""cby"" : ObjectId(""57d323ada32aa0dd6ca701d2""), 
    ""mdt"" : ISODate(""2016-12-27T19:59:17.732+0000""), 
    ""mby"" : ObjectId(""57d323ada32aa0dd6ca701d2""), 
    ""CustomerId"" : ObjectId(""000000000000000000000002""), 
    ""EmployerId"" : ObjectId(""000000000000000000000002""), 
    ""EmployeeNumber"" : ""12345"", 
    ""Info"" : {
        ""_id"" : ""049915c7-9db2-473e-8560-704c30ea460d"", 
        ""Email"" : ""somebody@antechmail.com"", 
        ""Address"" : {
            ""_id"" : ""f40334a1-0f8c-4cd1-b22e-9f65b095ce3f"", 
            ""Address1"" : ""123 Maureen Way"", 
            ""City"" : ""Westeros"", 
            ""State"" : ""RI"", 
            ""PostalCode"" : ""1985"", 
            ""Country"" : ""USA""
        }, 
        ""AltAddress"" : {
            ""_id"" : ""40cbe686-aac5-423a-96f2-64fb1cc9f1b0"", 
            ""Address1"" : null, 
            ""City"" : null, 
            ""State"" : null, 
            ""PostalCode"" : null, 
            ""Country"" : ""US""
        }, 
        ""HomePhone"" : ""1112223333""
    }, 
    ""FirstName"" : ""Dyneris"", 
    ""MiddleName"" : ""Stormborn"", 
    ""LastName"" : ""Targarian"", 
    ""Gender"" : NumberInt(70), 
    ""DoB"" : ISODate(""1982-05-16T00:00:00.000+0000""), 
    ""Ssn"" : null, 
    ""Status"" : NumberInt(76), 
    ""Salary"" : 120199.67, 
    ""WorkType"" : NumberInt(1), 
    ""JobTitle"" : ""Mother of Dragons"", 
    ""PriorHours"" : [
        {
            ""_id"" : ""d3bc4b12-52c9-4fae-b86c-86442b70787e"", 
            ""HoursWorked"" : 2080.0, 
            ""AsOf"" : ISODate(""2016-10-05T00:00:00.000+0000"")
        }
    ], 
    ""ServiceDate"" : ISODate(""2008-06-16T00:00:00.000+0000""), 
    ""HireDate"" : ISODate(""2008-06-16T00:00:00.000+0000""), 
    ""RehireDate"" : ISODate(""2012-10-08T00:00:00.000+0000""), 
    ""WorkState"" : ""RI"", 
    ""WorkCountry"" : ""US"", 
    ""WorkSchedules"" : [
        {
            ""_id"" : ""306664a2-f0b6-4830-9e81-f520b1580ec6"", 
            ""ScheduleType"" : NumberInt(0), 
            ""StartDate"" : ISODate(""2008-06-16T00:00:00.000+0000""), 
            ""EndDate"" : null, 
            ""Times"" : [
                {
                    ""_id"" : ""9cdbff44-a240-426e-9bc6-8bca29c8cddb"", 
                    ""TotalMinutes"" : null, 
                    ""SampleDate"" : ISODate(""2008-06-15T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""3b9d9634-e8a7-4fa1-912c-c4d4f4bf829a"", 
                    ""TotalMinutes"" : NumberInt(480), 
                    ""SampleDate"" : ISODate(""2008-06-16T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""3ae401f9-acd9-4171-9123-73c054f59f7a"", 
                    ""TotalMinutes"" : NumberInt(480), 
                    ""SampleDate"" : ISODate(""2008-06-17T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""127180a1-9ddc-4662-acba-ff39cac82434"", 
                    ""TotalMinutes"" : NumberInt(480), 
                    ""SampleDate"" : ISODate(""2008-06-18T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""e2b35517-28ca-47ec-9119-edc03625d7e7"", 
                    ""TotalMinutes"" : NumberInt(480), 
                    ""SampleDate"" : ISODate(""2008-06-19T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""8a569218-2f6b-4057-aad8-552f9b9e1e70"", 
                    ""TotalMinutes"" : NumberInt(480), 
                    ""SampleDate"" : ISODate(""2008-06-20T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""9480cb54-1d07-4c77-b635-bdb84e135da3"", 
                    ""TotalMinutes"" : null, 
                    ""SampleDate"" : ISODate(""2008-06-21T00:00:00.000+0000"")
                }
            ]
        }
    ], 
    ""Meets50In75MileRule"" : false, 
    ""IsKeyEmployee"" : false, 
    ""IsExempt"" : true, 
    ""MilitaryStatus"" : NumberInt(0), 
    ""CustomFields"" : [

    ], 
    ""EmployerName"" : ""Westeros""
}";
            #endregion Employee JSON

            var emp = BsonSerializer.Deserialize<Employee>(json).Save();

            using (CaseService cs = new CaseService(false))
            using (EligibilityService esvc = new EligibilityService())
            {
                DateTime startDate = new DateTime(2016, 9, 14).ToMidnight();
                DateTime endDate = new DateTime(2016, 12, 13).ToMidnight();

                Case newCase = cs.CreateCase(CaseStatus.Open, emp.Id, startDate, endDate, CaseType.Consecutive, "PREGMAT");
                newCase.SetCaseEvent(CaseEventType.DeliveryDate, new DateTime(2016, 9, 30).ToMidnight());
                newCase.Metadata.SetRawValue("WillUseBonding", true);
                newCase.Description = "Maternity for Dragons";
                LeaveOfAbsence loa = esvc.RunEligibility(newCase);
                newCase = loa.Case;

                // make sure there is an fmla policy
                AppliedPolicy fmla = newCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
                fmla.Should().NotBeNull("FMLA Policy not found");
                newCase.Delete();
            }
        }
    }
}
