﻿using AbsenceSoft;
using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Pay;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Tests.ProdSupport
{
    [TestClass]
    public class AT2325Tests
    {
        /// <summary>
        /// https://absencesoft.atlassian.net/browse/AT-2325
        /// FMLA extending when Holidays are in leave period.
        /// </summary>
        /// <remarks>This is bad mmm-kay, calcs that do this are bad mmm-kay.</remarks>
        [TestMethod]
        public void AT2325TestHolidayCalcs()
        {
            /*
             * AT-2325 - FMLA extending when Holidays are in leave period
             * This is a nasty one, however can be easily explained through this unit test.
             * 
             * This entire unit test should pass, key word being should. In a perfect world
             * this case below should ahve FMLA exhaust after 12 work weeks, which happens to
             * end on 3/24/2016 and exhaustion starts on 3/25/2016.
             * 
             * The actual thing the calcs are doing is adding 0.6 work weeks because it's not
             * counting the holidays as part of the work week denominator.
             * 
             * For instance, a M-F work week involves 5 work days. If I take an entire week, that's
             * exactly 1.0 work weeks. If I only take 1 day, it's 0.2 work weeks, if I take 2 days
             * then it's 0.4 work weeks, etc. The denominator for my work schedule is "5", so
             * 1/5, 2/5, etc. for however many work days I'm on leave against my work schedule.
             * 
             * Now, the way the law is written and understood by the courts (and our lawyer) is that
             * if the employee takes an entire week, the holiday counts against thier usage, if the
             * employee doesn't take the entire work week, the holiday does NOT count against
             * their usage.
             * 
             * For example, if I take leave M-F of a week and let's say Monday is a holiday, 
             * I've used exactly 1 work week, that holiday counts as part of the work week.
             * 
             * If I take leave M-W, and Monday is a holiday, I've used exactly 0.5 work weeks (2 * 0.25)
             * and this is because my effective work week is now only 4 working days, making my
             * denominator for work weeks 4 instead of 5 (because 1 holiday is subtracted).
             * 
             * Here inlies the issue. Our current calcs, to be speciific 
             * public List<AppliedPolicyUsageType> GetUsage(DateTime sampleDate) is the culprit
             * in AbsenceSoft.Logic.Cases.AppliedPolicyUsageHelper
             * probably around line 94; where it determines the BaseUnit and assigns it for that day
             * 
             * My guess is that it does not account for holidays, OR, if it does, why it would get 5
             * for that usage instead of 4; that's your job to figure out!
             * 
             * The final consumer of this denominator (BaseUnit) where it gets the wrong % of work week
             * is located in public double DaysUsage(EntitlementType et), of
             * AbsenceSoft.Data.Cases.AppliedPolicyUsage; line 143.
             */

            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            deleteTestCases(empId);
            var testPolicy = Policy.GetByCode("FMLA");
            testPolicy.PolicyBehaviorType = PolicyBehavior.ReducesWorkWeek;
            testPolicy.Save();

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode && r.CustomerId == null).First();

            Case.Delete(Case.Query.EQ(r => r.Employee.Id, empId));

            Employee emp = Employee.GetById(empId);
            emp.ServiceDate = new DateTime(2000, 1, 2).ToMidnight();
            emp.HireDate = new DateTime(2000, 1, 2).ToMidnight();
            emp.WorkSchedules = new List<Schedule>()
            {
                new Schedule()
                {
                    StartDate = emp.ServiceDate ?? new DateTime(2000, 1, 1).ToMidnight(),
                    EndDate = null,
                    ScheduleType = ScheduleType.Weekly,
                    Times = new List<Time>(7)
                    {
                        new Time() { SampleDate = new DateTime(2000, 1, 2).ToMidnight(), TotalMinutes = null },
                        new Time() { SampleDate = new DateTime(2000, 1, 3).ToMidnight(), TotalMinutes = 480 },
                        new Time() { SampleDate = new DateTime(2000, 1, 4).ToMidnight(), TotalMinutes = 480 },
                        new Time() { SampleDate = new DateTime(2000, 1, 5).ToMidnight(), TotalMinutes = 480 },
                        new Time() { SampleDate = new DateTime(2000, 1, 6).ToMidnight(), TotalMinutes = 480 },
                        new Time() { SampleDate = new DateTime(2000, 1, 7).ToMidnight(), TotalMinutes = 480 },
                        new Time() { SampleDate = new DateTime(2000, 1, 8).ToMidnight(), TotalMinutes = null }
                    }
                }
            };
            emp.Save();

            using (CaseService cs = new CaseService(false))
            using (EligibilityService esvc = new EligibilityService())
            {
                DateTime startDate = new DateTime(2015, 12, 31).ToMidnight();
                DateTime endDate = new DateTime(2016, 4, 8).ToMidnight();
                DateTime expectedExhaustion = new DateTime(2016, 3, 24).ToMidnight();

                Case newCase = cs.CreateCase(CaseStatus.Open, empId, startDate, endDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

                LeaveOfAbsence loa = esvc.RunEligibility(newCase);

                // make sure there is an fmla policy
                AppliedPolicy fmla = newCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
                fmla.Should().NotBeNull("FMLA Policy not found");

                // get rid of eveything but the fmla policy. we only need one to test and it'll make life easier
                Assert.AreEqual(newCase.Segments.Count, 1);
                newCase.Segments[0].AppliedPolicies = new List<AppliedPolicy>() { fmla };

                // this runs the calc too
                cs.ApplyDetermination(newCase, fmla.Policy.Code, startDate, endDate, AdjudicationStatus.Approved);

                // Re-Get the FMLA applied policy
                fmla = newCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");

                TestDateHas(fmla, 2015, 12, 31, 480, 0.25);
                TestDateHas(fmla, 2016, 1, 1, 0, 0); // New Year's Day
                TestDateHas(fmla, 2016, 1, 2, 0, 0);
                TestDateHas(fmla, 2016, 1, 3, 0, 0);
                TestDateHas(fmla, 2016, 1, 4, 480, 0.2);
                TestDateHas(fmla, 2016, 1, 5, 480, 0.2);
                TestDateHas(fmla, 2016, 1, 6, 480, 0.2);
                TestDateHas(fmla, 2016, 1, 7, 480, 0.2);
                TestDateHas(fmla, 2016, 1, 8, 480, 0.2);
                TestDateHas(fmla, 2016, 1, 9, 0, 0);
                TestDateHas(fmla, 2016, 1, 10, 0, 0);
                TestDateHas(fmla, 2016, 1, 11, 480, 0.2);
                TestDateHas(fmla, 2016, 1, 12, 480, 0.2);
                TestDateHas(fmla, 2016, 1, 13, 480, 0.2);
                TestDateHas(fmla, 2016, 1, 14, 480, 0.2);
                TestDateHas(fmla, 2016, 1, 15, 480, 0.2);
                TestDateHas(fmla, 2016, 1, 16, 0, 0);
                TestDateHas(fmla, 2016, 1, 17, 0, 0);
                TestDateHas(fmla, 2016, 1, 18, 0, 0); // MLK Day
                TestDateHas(fmla, 2016, 1, 19, 480, 0.25);
                TestDateHas(fmla, 2016, 1, 20, 480, 0.25);
                TestDateHas(fmla, 2016, 1, 21, 480, 0.25);
                TestDateHas(fmla, 2016, 1, 22, 480, 0.25);
                TestDateHas(fmla, 2016, 1, 23, 0, 0);
                TestDateHas(fmla, 2016, 1, 24, 0, 0);
                TestDateHas(fmla, 2016, 1, 25, 480, 0.2);
                TestDateHas(fmla, 2016, 1, 26, 480, 0.2);
                TestDateHas(fmla, 2016, 1, 27, 480, 0.2);
                TestDateHas(fmla, 2016, 1, 28, 480, 0.2);
                TestDateHas(fmla, 2016, 1, 29, 480, 0.2);
                TestDateHas(fmla, 2016, 1, 30, 0, 0);
                TestDateHas(fmla, 2016, 1, 31, 0, 0);
                TestDateHas(fmla, 2016, 2, 1, 480, 0.2);
                TestDateHas(fmla, 2016, 2, 2, 480, 0.2);
                TestDateHas(fmla, 2016, 2, 3, 480, 0.2);
                TestDateHas(fmla, 2016, 2, 4, 480, 0.2);
                TestDateHas(fmla, 2016, 2, 5, 480, 0.2);
                TestDateHas(fmla, 2016, 2, 6, 0, 0);
                TestDateHas(fmla, 2016, 2, 7, 0, 0);
                TestDateHas(fmla, 2016, 2, 8, 480, 0.2);
                TestDateHas(fmla, 2016, 2, 9, 480, 0.2);
                TestDateHas(fmla, 2016, 2, 10, 480, 0.2);
                TestDateHas(fmla, 2016, 2, 11, 480, 0.2);
                TestDateHas(fmla, 2016, 2, 12, 480, 0.2);
                TestDateHas(fmla, 2016, 2, 13, 0, 0);
                TestDateHas(fmla, 2016, 2, 14, 0, 0);
                TestDateHas(fmla, 2016, 2, 15, 0, 0); // President's Day
                TestDateHas(fmla, 2016, 2, 16, 480, 0.25);
                TestDateHas(fmla, 2016, 2, 17, 480, 0.25);
                TestDateHas(fmla, 2016, 2, 18, 480, 0.25);
                TestDateHas(fmla, 2016, 2, 19, 480, 0.25);
                TestDateHas(fmla, 2016, 2, 20, 0, 0);
                TestDateHas(fmla, 2016, 2, 21, 0, 0);
                TestDateHas(fmla, 2016, 2, 22, 480, 0.2);
                TestDateHas(fmla, 2016, 2, 23, 480, 0.2);
                TestDateHas(fmla, 2016, 2, 24, 480, 0.2);
                TestDateHas(fmla, 2016, 2, 25, 480, 0.2);
                TestDateHas(fmla, 2016, 2, 26, 480, 0.2);
                TestDateHas(fmla, 2016, 2, 27, 0, 0);
                TestDateHas(fmla, 2016, 2, 28, 0, 0);
                TestDateHas(fmla, 2016, 2, 29, 480, 0.2);
                TestDateHas(fmla, 2016, 3, 1, 480, 0.2);
                TestDateHas(fmla, 2016, 3, 2, 480, 0.2);
                TestDateHas(fmla, 2016, 3, 3, 480, 0.2);
                TestDateHas(fmla, 2016, 3, 4, 480, 0.2);
                TestDateHas(fmla, 2016, 3, 5, 0, 0);
                TestDateHas(fmla, 2016, 3, 6, 0, 0);
                TestDateHas(fmla, 2016, 3, 7, 480, 0.2);
                TestDateHas(fmla, 2016, 3, 8, 480, 0.2);
                TestDateHas(fmla, 2016, 3, 9, 480, 0.2);
                TestDateHas(fmla, 2016, 3, 10, 480, 0.2);
                TestDateHas(fmla, 2016, 3, 11, 480, 0.2);
                TestDateHas(fmla, 2016, 3, 12, 0, 0);
                TestDateHas(fmla, 2016, 3, 13, 0, 0);
                TestDateHas(fmla, 2016, 3, 14, 480, 0.2);
                TestDateHas(fmla, 2016, 3, 15, 480, 0.2);
                TestDateHas(fmla, 2016, 3, 16, 480, 0.2);
                TestDateHas(fmla, 2016, 3, 17, 480, 0.2);
                TestDateHas(fmla, 2016, 3, 18, 480, 0.2);
                TestDateHas(fmla, 2016, 3, 19, 0, 0);
                TestDateHas(fmla, 2016, 3, 20, 0, 0);
                TestDateHas(fmla, 2016, 3, 21, 480, 0.2);
                TestDateHas(fmla, 2016, 3, 22, 480, 0.2);
                TestDateHas(fmla, 2016, 3, 23, 480, 0.2);
                TestDateHas(fmla, 2016, 3, 24, 0, 0);
                TestDateHas(fmla, 2016, 3, 25, 0, 0);  // First Date of Exhaustion
                TestDateHas(fmla, 2016, 3, 26, 0, 0);// Exhausted 
                TestDateHas(fmla, 2016, 3, 27, 0, 0);
                TestDateHas(fmla, 2016, 3, 28, 0, 0);
                TestDateHas(fmla, 2016, 3, 29, 0, 0);
                TestDateHas(fmla, 2016, 3, 30, 0, 0);
                TestDateHas(fmla, 2016, 3, 31, 0, 0);
                TestDateHas(fmla, 2016, 4, 1, 0, 0);
                TestDateHas(fmla, 2016, 4, 2, 0, 0);
                TestDateHas(fmla, 2016, 4, 3, 0, 0);
                TestDateHas(fmla, 2016, 4, 4, 0, 0);
                TestDateHas(fmla, 2016, 4, 5, 0, 0);
                TestDateHas(fmla, 2016, 4, 6, 0, 0);
                TestDateHas(fmla, 2016, 4, 7, 0, 0);
                TestDateHas(fmla, 2016, 4, 8, 0, 0); // End of Leave

                // TODO: Get first exhaustion date
                var exh = fmla.FirstExhaustionDate;
                exh.Should().HaveValue();
                exh.Value.Should().Be(expectedExhaustion);
            }

            Case.Delete(Case.Query.EQ(r => r.Employee.Id, empId));
        }

        void TestDateHas(AppliedPolicy fmla, int year, int month, int day, int minutesUsed = 0x0, double workWeeksUsed = 0D)
        {
            var dateUsed = new DateTime(year, month, day).ToMidnight();
            var usage = fmla.Usage.FirstOrDefault(u => u.DateUsed == dateUsed);
            usage.Should().NotBeNull("Usage for date {0:yyyy-MM-dd} not found", dateUsed);
            usage.MinutesUsed.Should().Be(minutesUsed, "Minutes Used for date {0:yyyy-MM-dd} should be {1} but was {2}", dateUsed, minutesUsed, usage.MinutesUsed);
            var actualWorkWeeks = usage.DaysUsage(EntitlementType.WorkWeeks);
            actualWorkWeeks.Should().Be(workWeeksUsed, "Work Weeks Used for date {0:yyyy-MM-dd} should be {1} but was {2}", dateUsed, workWeeksUsed, actualWorkWeeks);
        }

        private void deleteTestCases(string empId)
        {
            Case.Repository.Collection.Remove(Case.Query.EQ(c => c.Employee.Id, empId));
            ToDoItem.Repository.Collection.Remove(ToDoItem.Query.EQ(c => c.EmployeeId, empId));
            Communication.Repository.Collection.Remove(Communication.Query.EQ(c => c.EmployeeId, empId));
            Attachment.Repository.Collection.Remove(Attachment.Query.EQ(c => c.EmployeeId, empId));
        }
    }
}
