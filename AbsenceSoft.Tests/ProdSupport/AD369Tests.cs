﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Processing;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Processing.EL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Tests.ProdSupport
{
    [TestClass]
    public class AD369Tests
    {
        const string CustomerId = "000000000000000000000001";
        const string EmployerId = "000000000000000000000001";

        [TestMethod]
        public void AD369ELLoadContactsHosedTest()
        {
            Employee.Repository.Delete("54cc83b8a32a9f0ad08635a2");
            Employee.Repository.Delete("54cc82b3a32a9f0ad0849ff3");
            EmployeeContact.Repository.Delete(c => c.EmployeeId == "54cc83b8a32a9f0ad08635a2");

            (ContactType.GetByCode("OMR", CustomerId, EmployerId) ?? new ContactType()
            {
                CustomerId = CustomerId,
                EmployerId = EmployerId,
                ContactCategory = AbsenceSoft.Data.Enums.ContactTypeDesignationType.Administrative,
                Code = "OMR",
                Name = "OMR"
            }).Save();
            (ContactType.GetByCode("OPS", CustomerId, EmployerId) ?? new ContactType()
            {
                CustomerId = CustomerId,
                EmployerId = EmployerId,
                ContactCategory = AbsenceSoft.Data.Enums.ContactTypeDesignationType.Administrative,
                Code = "OPS",
                Name = "OPS"
            }).Save();
            (ContactType.GetByCode("HRA", CustomerId, EmployerId) ?? new ContactType()
            {
                CustomerId = CustomerId,
                EmployerId = EmployerId,
                ContactCategory = AbsenceSoft.Data.Enums.ContactTypeDesignationType.Administrative,
                Code = "HRA",
                Name = "HRAssistant"
            }).Save();

            #region Employee JSON
            var json = @"{ 
    ""_id"" : ObjectId(""54cc83b8a32a9f0ad08635a2""), 
    ""LastHRRow2"" : ""uDTKwBvp1ecOydEXlt57eg=="", 
    ""LastELRow"" : ""X2sVXWcZZqXZI37ymp7qEA=="", 
    ""LastSupRow"" : ""UmEuAPpcEJZl1sn3C7ytkA=="", 
    ""_Release"" : ""8/6"", 
    ""LastCRow"" : ""kubBIM+WpSVTveP3z04BIQ=="", 
    ""LastERow"" : ""/UHP7HZlty2NeqePesqpiQ=="", 
    ""cdt"" : ISODate(""2015-01-31T07:26:48.812+0000""), 
    ""cby"" : ObjectId(""000000000000000000000000""), 
    ""mdt"" : ISODate(""2017-02-07T07:24:11.843+0000""), 
    ""mby"" : ObjectId(""000000000000000000000000""), 
    ""CustomerId"" : ObjectId(""000000000000000000000001""), 
    ""EmployerId"" : ObjectId(""000000000000000000000001""), 
    ""EmployeeNumber"" : ""180581"", 
    ""Info"" : {
        ""_id"" : ""64b708fc-7c65-415a-a51c-72ba676bbeef"", 
        ""Email"" : ""kemmerly@amazon.com"", 
        ""AltEmail"" : ""somebody@gmail.com"", 
        ""Address"" : {
            ""_id"" : ""60ab6d6b-920b-4f81-a7d0-3f3ced5a380a"", 
            ""Address1"" : ""123 ANY STREET"", 
            ""City"" : ""Somewhere"", 
            ""State"" : ""KS"", 
            ""PostalCode"" : ""67000"", 
            ""Country"" : ""US""
        }, 
        ""AltAddress"" : {
            ""_id"" : ""fee985bd-f6b0-4335-9a34-d298fc7a10d7"", 
            ""Address1"" : null, 
            ""City"" : null, 
            ""State"" : null, 
            ""PostalCode"" : null, 
            ""Country"" : ""US""
        }, 
        ""HomePhone"" : ""6200000000"", 
        ""CellPhone"" : ""6200000000"", 
        ""OfficeLocation"" : ""MCI7""
    }, 
    ""FirstName"" : ""Person"", 
    ""MiddleName"" : ""H"", 
    ""LastName"" : ""Lady"", 
    ""Gender"" : NumberInt(77), 
    ""DoB"" : ISODate(""1990-01-01T00:00:00.000+0000""), 
    ""Ssn"" : null, 
    ""Status"" : NumberInt(65), 
    ""WorkType"" : NumberInt(1), 
    ""CostCenterCode"" : ""7403"", 
    ""JobTitle"" : ""Technical Support Tech I"", 
    ""Department"" : ""Corporate Systems"", 
    ""PriorHours"" : [
        {
            ""_id"" : ""3b448c8a-d7d8-431e-a8fc-ac7da2134421"", 
            ""HoursWorked"" : 0.0, 
            ""AsOf"" : ISODate(""2016-09-24T00:00:00.000+0000"")
        }, 
        {
            ""_id"" : ""c74e6f36-21ab-45f5-b5ea-94e9d25d70ec"", 
            ""HoursWorked"" : 0.0, 
            ""AsOf"" : ISODate(""2016-10-29T00:00:00.000+0000"")
        }, 
        {
            ""_id"" : ""66c5f9c5-8814-40cf-846b-d1f3263bd2c0"", 
            ""HoursWorked"" : 0.0, 
            ""AsOf"" : ISODate(""2017-01-12T00:00:00.000+0000"")
        }, 
        {
            ""_id"" : ""4197fd81-1582-41de-a2da-261a44762a0e"", 
            ""HoursWorked"" : 0.0, 
            ""AsOf"" : ISODate(""2017-01-25T00:00:00.000+0000"")
        }, 
        {
            ""_id"" : ""b6c43850-892e-424a-b2aa-8985429dce68"", 
            ""HoursWorked"" : 0.0, 
            ""AsOf"" : ISODate(""2017-02-07T00:00:00.000+0000"")
        }
    ], 
    ""ServiceDate"" : ISODate(""2015-11-09T00:00:00.000+0000""), 
    ""HireDate"" : ISODate(""2010-04-04T00:00:00.000+0000""), 
    ""RehireDate"" : ISODate(""2015-11-09T00:00:00.000+0000""), 
    ""WorkState"" : ""KS"", 
    ""WorkCountry"" : ""US"", 
    ""WorkSchedules"" : [
        {
            ""_id"" : ""cd0ac601-7abe-4bd6-b777-a5802951db75"", 
            ""ScheduleType"" : NumberInt(0), 
            ""StartDate"" : ISODate(""2010-04-04T00:00:00.000+0000""), 
            ""EndDate"" : ISODate(""2016-09-23T00:00:00.000+0000""), 
            ""Times"" : [
                {
                    ""_id"" : ""7b6bf953-7843-412a-aa24-805c1ebb6728"", 
                    ""TotalMinutes"" : NumberInt(0), 
                    ""SampleDate"" : ISODate(""2010-04-04T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""a55b4c8a-1d7f-42dc-9ed4-bb301ba1ff92"", 
                    ""TotalMinutes"" : NumberInt(0), 
                    ""SampleDate"" : ISODate(""2010-04-05T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""a8030e96-b9de-491f-beba-0399577f1c53"", 
                    ""TotalMinutes"" : NumberInt(0), 
                    ""SampleDate"" : ISODate(""2010-04-06T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""2277a5b9-62f0-4ecb-96da-4bac60946430"", 
                    ""TotalMinutes"" : NumberInt(0), 
                    ""SampleDate"" : ISODate(""2010-04-07T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""2f88ed5d-841d-4cfb-91b6-d4d182c00c73"", 
                    ""TotalMinutes"" : NumberInt(0), 
                    ""SampleDate"" : ISODate(""2010-04-08T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""cf439cb5-79df-4dce-acd4-35b5f68f9be8"", 
                    ""TotalMinutes"" : NumberInt(0), 
                    ""SampleDate"" : ISODate(""2010-04-09T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""951a0214-ef7b-46e7-8365-d5718af7008a"", 
                    ""TotalMinutes"" : NumberInt(0), 
                    ""SampleDate"" : ISODate(""2010-04-10T00:00:00.000+0000"")
                }
            ]
        }, 
        {
            ""_id"" : ""902b89d1-deb3-497c-abfc-6a1f2fe53dd5"", 
            ""ScheduleType"" : NumberInt(0), 
            ""StartDate"" : ISODate(""2016-09-24T00:00:00.000+0000""), 
            ""EndDate"" : null, 
            ""Times"" : [
                {
                    ""_id"" : ""0e54c55e-e9b8-4b2b-b952-83fba7d883b2"", 
                    ""TotalMinutes"" : null, 
                    ""SampleDate"" : ISODate(""2016-09-18T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""33eb7eaa-9724-463b-9358-559a6153ac14"", 
                    ""TotalMinutes"" : NumberInt(0), 
                    ""SampleDate"" : ISODate(""2016-09-19T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""b02f49f2-189e-4ab9-b90c-06866ba38d9b"", 
                    ""TotalMinutes"" : NumberInt(0), 
                    ""SampleDate"" : ISODate(""2016-09-20T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""acb32ebc-cc6a-4416-9c60-076235ddae1e"", 
                    ""TotalMinutes"" : NumberInt(0), 
                    ""SampleDate"" : ISODate(""2016-09-21T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""24092fff-bd54-437d-906f-4d2c823457ee"", 
                    ""TotalMinutes"" : NumberInt(0), 
                    ""SampleDate"" : ISODate(""2016-09-22T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""712b5967-9674-4b3c-ba0f-1fd5f2db2a48"", 
                    ""TotalMinutes"" : NumberInt(0), 
                    ""SampleDate"" : ISODate(""2016-09-23T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""95252132-fac1-4a11-be0c-6819ebdef920"", 
                    ""TotalMinutes"" : null, 
                    ""SampleDate"" : ISODate(""2016-09-24T00:00:00.000+0000"")
                }
            ]
        }
    ], 
    ""Meets50In75MileRule"" : false, 
    ""IsKeyEmployee"" : false, 
    ""IsExempt"" : false, 
    ""MilitaryStatus"" : NumberInt(0), 
    ""CustomFields"" : [
        {
            ""_id"" : ObjectId(""55a6b04c3de0960bb0a99c13""), 
            ""cdt"" : ISODate(""2015-07-15T19:11:08.922+0000""), 
            ""cby"" : ObjectId(""000000000000000000000000""), 
            ""mdt"" : ISODate(""2015-07-15T19:11:08.922+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""EMPLOYEECLASS"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""EmployeeClass"", 
            ""Description"" : ""Employee Class"", 
            ""Label"" : ""EmployeeClass"", 
            ""HelpText"" : ""Employee Class"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [
                {
                    ""Key"" : ""B-Fixed Term Contractor - EU"", 
                    ""Value"" : ""B-Fixed Term Contractor - EU""
                }, 
                {
                    ""Key"" : ""F-Regular Full Time"", 
                    ""Value"" : ""F-Regular Full Time""
                }, 
                {
                    ""Key"" : ""H-Regular Part Time - 20 + Hours"", 
                    ""Value"" : ""H-Regular Part Time - 20 + Hours""
                }, 
                {
                    ""Key"" : ""I-Intern"", 
                    ""Value"" : ""I-Intern""
                }, 
                {
                    ""Key"" : ""N-Trainee European"", 
                    ""Value"" : ""N-Trainee European""
                }, 
                {
                    ""Key"" : ""P-Apprentice"", 
                    ""Value"" : ""P-Apprentice""
                }, 
                {
                    ""Key"" : ""R-Regular Reduced Time  30 + Hrs"", 
                    ""Value"" : ""R-Regular Reduced Time  30 + Hrs""
                }, 
                {
                    ""Key"" : ""S-Seasonal/Short-Term"", 
                    ""Value"" : ""S-Seasonal/Short-Term""
                }, 
                {
                    ""Key"" : ""T-Onsite Vendor"", 
                    ""Value"" : ""T-Onsite Vendor""
                }, 
                {
                    ""Key"" : ""V-Offsite Vendor"", 
                    ""Value"" : ""V-Offsite Vendor""
                }, 
                {
                    ""Key"" : ""W-3P Onsite Worker"", 
                    ""Value"" : ""W-3P Onsite Worker""
                }, 
                {
                    ""Key"" : ""X-Regular Flex Time - < 20 Hrs"", 
                    ""Value"" : ""X-Regular Flex Time - < 20 Hrs""
                }, 
                {
                    ""Key"" : ""M - Internal Staffing Solutions"", 
                    ""Value"" : ""M - Internal Staffing Solutions""
                }
            ], 
            ""SelectedValue"" : ""F"", 
            ""FileOrder"" : NumberInt(16), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""562992f03de0960f481aa0a1""), 
            ""cdt"" : ISODate(""2015-10-23T01:52:48.763+0000""), 
            ""cby"" : ObjectId(""000000000000000000000000""), 
            ""mdt"" : ISODate(""2015-11-11T06:08:08.602+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""HRASSOCIATE"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""HRAssociate"", 
            ""Description"" : ""Level"", 
            ""Label"" : ""Level"", 
            ""HelpText"" : ""Level"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""3"", 
            ""FileOrder"" : NumberInt(18), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""54db96b2a32aa0097c0177f6""), 
            ""cdt"" : ISODate(""2015-02-11T17:51:46.951+0000""), 
            ""cby"" : ObjectId(""546e5319a32aa00f7808626c""), 
            ""mdt"" : ISODate(""2016-07-12T03:13:15.330+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""JOBCODE"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Job Code"", 
            ""Description"" : ""Job Code"", 
            ""Label"" : ""Job Code"", 
            ""HelpText"" : ""Job Code"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""T06221"", 
            ""FileOrder"" : NumberInt(3), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""54db9722a32aa0097c0177fc""), 
            ""cdt"" : ISODate(""2015-02-11T17:53:38.046+0000""), 
            ""cby"" : ObjectId(""546e5319a32aa00f7808626c""), 
            ""mdt"" : ISODate(""2016-07-12T03:13:30.103+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""BUSINESSTITLE"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Business Title"", 
            ""Description"" : ""Business Title"", 
            ""Label"" : ""Business Title"", 
            ""HelpText"" : ""Business Title"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""IT Support Engineer I"", 
            ""FileOrder"" : NumberInt(4), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""54db96e9a32aa0097c0177f8""), 
            ""cdt"" : ISODate(""2015-02-11T17:52:41.398+0000""), 
            ""cby"" : ObjectId(""546e5319a32aa00f7808626c""), 
            ""mdt"" : ISODate(""2016-07-12T03:09:23.186+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""LOCATION"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Location"", 
            ""Description"" : ""Location"", 
            ""Label"" : ""Location"", 
            ""HelpText"" : ""Location"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""8051"", 
            ""FileOrder"" : NumberInt(5), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""54db9765a32aa0097c017802""), 
            ""cdt"" : ISODate(""2015-02-11T17:54:45.441+0000""), 
            ""cby"" : ObjectId(""546e5319a32aa00f7808626c""), 
            ""mdt"" : ISODate(""2016-07-12T03:09:10.628+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""LOCATIONTYPE"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Location Type"", 
            ""Description"" : ""Location Type"", 
            ""Label"" : ""Location Type"", 
            ""HelpText"" : ""Location Type"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""FC"", 
            ""FileOrder"" : NumberInt(6), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""551cca90a32aa00260295703""), 
            ""cdt"" : ISODate(""2015-04-02T04:50:24.760+0000""), 
            ""cby"" : ObjectId(""546e5319a32aa00f7808626c""), 
            ""mdt"" : ISODate(""2016-07-12T03:08:57.275+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""LASTDATEWORKED"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Last Date Worked"", 
            ""Description"" : ""Last Date Worked"", 
            ""Label"" : ""Last Date Worked"", 
            ""HelpText"" : ""Last Date Worked"", 
            ""DataType"" : NumberInt(8), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(7), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""578d4858a32aa1a51c7b406b""), 
            ""cdt"" : ISODate(""2016-07-18T21:21:28.286+0000""), 
            ""cby"" : ObjectId(""56cf2150a32aa00a783e5bc9""), 
            ""mdt"" : ISODate(""2016-07-21T15:29:21.624+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""TERMACTIONREASON"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Term Action Reason"", 
            ""Description"" : ""Term Action Reason"", 
            ""Label"" : ""Term Action Reason"", 
            ""HelpText"" : ""Term Action Reason"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(8), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""57757ee3a32aa01e08c57df9""), 
            ""cdt"" : ISODate(""2016-06-30T20:19:47.611+0000""), 
            ""cby"" : ObjectId(""56cf2150a32aa00a783e5bc9""), 
            ""mdt"" : ISODate(""2016-07-21T15:29:29.642+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""TERMFACTOR"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Term Factor"", 
            ""Description"" : ""Term Factor"", 
            ""Label"" : ""Term Factor"", 
            ""HelpText"" : ""Term Factor"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(9), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""57757f06a32aa01e08c57dfa""), 
            ""cdt"" : ISODate(""2016-06-30T20:20:22.762+0000""), 
            ""cby"" : ObjectId(""56cf2150a32aa00a783e5bc9""), 
            ""mdt"" : ISODate(""2016-07-12T03:08:11.800+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""FCLMAREA"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""FCLM Area"", 
            ""Description"" : ""FCLM Area"", 
            ""Label"" : ""FCLM Area"", 
            ""HelpText"" : ""FCLM Area"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""26"", 
            ""FileOrder"" : NumberInt(10), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""57757cfea32aa01e08c57df4""), 
            ""cdt"" : ISODate(""2016-06-30T20:11:42.177+0000""), 
            ""cby"" : ObjectId(""56cf2150a32aa00a783e5bc9""), 
            ""mdt"" : ISODate(""2016-07-12T03:08:01.395+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""FCLMJOB"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""FCLM JOB"", 
            ""Description"" : ""FCLM Job"", 
            ""Label"" : ""FCLM JOB"", 
            ""HelpText"" : ""FCLM Job"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""IT Support"", 
            ""FileOrder"" : NumberInt(11), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : true
        }, 
        {
            ""_id"" : ObjectId(""57757d3aa32aa01e08c57df5""), 
            ""cdt"" : ISODate(""2016-06-30T20:12:42.047+0000""), 
            ""cby"" : ObjectId(""56cf2150a32aa00a783e5bc9""), 
            ""mdt"" : ISODate(""2016-07-12T03:07:47.074+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""SHIFTPATTERN"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Shift Pattern"", 
            ""Description"" : ""Shift Pattern"", 
            ""Label"" : ""Shift Pattern"", 
            ""HelpText"" : ""Shift Pattern"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""AAAA"", 
            ""FileOrder"" : NumberInt(12), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : true
        }, 
        {
            ""_id"" : ObjectId(""57757dbea32aa01e08c57df7""), 
            ""cdt"" : ISODate(""2016-06-30T20:14:54.091+0000""), 
            ""cby"" : ObjectId(""56cf2150a32aa00a783e5bc9""), 
            ""mdt"" : ISODate(""2016-07-12T03:07:34.719+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""SHIFTPATTERNDESCRIPTION"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Shift Pattern Description"", 
            ""Description"" : ""Shift Pattern Description"", 
            ""Label"" : ""Shift Pattern Description"", 
            ""HelpText"" : ""Shift Pattern Description"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""FC Variable Sched"", 
            ""FileOrder"" : NumberInt(13), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""57757cbfa32aa01e08c57df3""), 
            ""cdt"" : ISODate(""2016-06-30T20:10:39.775+0000""), 
            ""cby"" : ObjectId(""56cf2150a32aa00a783e5bc9""), 
            ""mdt"" : ISODate(""2016-07-12T03:06:49.791+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""DEPARTMENT"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Department"", 
            ""Description"" : ""Department"", 
            ""Label"" : ""Department"", 
            ""HelpText"" : ""Department"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""1231"", 
            ""FileOrder"" : NumberInt(14), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""57757c74a32aa01e08c57df1""), 
            ""cdt"" : ISODate(""2016-06-30T20:09:24.923+0000""), 
            ""cby"" : ObjectId(""56cf2150a32aa00a783e5bc9""), 
            ""mdt"" : ISODate(""2016-07-12T03:06:24.738+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""DEPARTMENTDESCRIPTION"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Department Description"", 
            ""Description"" : ""Department Description"", 
            ""Label"" : ""Department Description"", 
            ""HelpText"" : ""Department Description"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""FC IT - Variable"", 
            ""FileOrder"" : NumberInt(15), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""57758027a32aa01e08c57dfd""), 
            ""cdt"" : ISODate(""2016-06-30T20:25:11.014+0000""), 
            ""cby"" : ObjectId(""56cf2150a32aa00a783e5bc9""), 
            ""mdt"" : ISODate(""2016-07-12T03:05:48.343+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""EMPLOYEECLASSDESCRIPTION"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Employee Class Description"", 
            ""Description"" : ""Employee Class Description"", 
            ""Label"" : ""Employee Class Description"", 
            ""HelpText"" : ""Employee Class Description"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""Regular Full Time"", 
            ""FileOrder"" : NumberInt(17), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""54db9705a32aa0097c0177f9""), 
            ""cdt"" : ISODate(""2015-02-11T17:53:09.105+0000""), 
            ""cby"" : ObjectId(""546e5319a32aa00f7808626c""), 
            ""mdt"" : ISODate(""2016-07-12T03:05:17.330+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""STEAMMANAGER"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""STeam Manager"", 
            ""Description"" : ""STeam Manager"", 
            ""Label"" : ""STeam Manager"", 
            ""HelpText"" : ""STeam Manager"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""015989"", 
            ""FileOrder"" : NumberInt(19), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""54db973ba32aa0097c0177fd""), 
            ""cdt"" : ISODate(""2015-02-11T17:54:03.333+0000""), 
            ""cby"" : ObjectId(""546e5319a32aa00f7808626c""), 
            ""mdt"" : ISODate(""2016-07-12T03:04:58.454+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""AMAZONLOGIN"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Amazon Login"", 
            ""Description"" : ""Amazon Login"", 
            ""Label"" : ""Amazon Login"", 
            ""HelpText"" : ""Amazon Login"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""KEMMERLY"", 
            ""FileOrder"" : NumberInt(20), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""577581b3a32aa01e08c57e00""), 
            ""cdt"" : ISODate(""2016-06-30T20:31:47.763+0000""), 
            ""cby"" : ObjectId(""56cf2150a32aa00a783e5bc9""), 
            ""mdt"" : ISODate(""2016-07-12T03:04:30.998+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""HREMPLOYEE"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""HR Employee ?"", 
            ""Description"" : ""HR Employee ?"", 
            ""Label"" : ""HR Employee ?"", 
            ""HelpText"" : ""HR Employee ?"", 
            ""DataType"" : NumberInt(4), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : false, 
            ""FileOrder"" : NumberInt(21), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""577d8a4bc5f3321004ad1cf6""), 
            ""cdt"" : ISODate(""2016-07-06T22:46:35.764+0000""), 
            ""cby"" : ObjectId(""56b398d8c5f32f15b85fa75d""), 
            ""mdt"" : ISODate(""2016-07-12T03:04:17.286+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""COMPANY"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Company"", 
            ""Description"" : ""Company"", 
            ""Label"" : ""Company"", 
            ""HelpText"" : ""Company"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""A07"", 
            ""FileOrder"" : NumberInt(22), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""577d8a6ec5f3321004ad1cf7""), 
            ""cdt"" : ISODate(""2016-07-06T22:47:10.364+0000""), 
            ""cby"" : ObjectId(""56b398d8c5f32f15b85fa75d""), 
            ""mdt"" : ISODate(""2016-07-12T03:04:04.556+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""COMPANYNAME"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Company Name"", 
            ""Description"" : ""Company Name"", 
            ""Label"" : ""Company Name"", 
            ""HelpText"" : ""Company Name"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""Amazon.com.ksdc LLC"", 
            ""FileOrder"" : NumberInt(23), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }
    ], 
    ""EmployerName"" : ""Amazon""
}";
            #endregion

            var emp = BsonSerializer.Deserialize<Employee>(json).Save();
            emp.CustomFields.ForEach(c => c.Save());

            #region OMR Employee JSON
            json = @"{ 
    ""_id"" : ObjectId(""54cc82b3a32a9f0ad0849ff3""), 
    ""LastHRRow2"" : ""QmbXPkT9++K+aGX/Ma62+A=="", 
    ""LastELRow"" : ""wX0Y+XU1k5tR7urNlPrDqA=="", 
    ""LastSupRow"" : ""wWhMaY8X1mS5P+qRQ4RJIA=="", 
    ""_Release"" : ""8/6"", 
    ""LastCRow"" : ""uA1A43SovqMGOostKawafg=="", 
    ""LastERow"" : ""uLiSy02EPqS+wdlP0GnxkQ=="", 
    ""cdt"" : ISODate(""2015-01-31T07:22:27.729+0000""), 
    ""cby"" : ObjectId(""000000000000000000000000""), 
    ""mdt"" : ISODate(""2017-02-13T07:09:39.019+0000""), 
    ""mby"" : ObjectId(""000000000000000000000000""), 
    ""CustomerId"" : ObjectId(""000000000000000000000001""), 
    ""EmployerId"" : ObjectId(""000000000000000000000001""), 
    ""EmployeeNumber"" : ""100189134"", 
    ""Info"" : {
        ""_id"" : ""8d9b4bc5-f28c-4c07-b1b4-e0ba7d9ed2c5"", 
        ""Email"" : ""ckleary@amazon.com"", 
        ""AltEmail"" : ""somebody@comcast.net"", 
        ""Address"" : {
            ""_id"" : ""71510985-f7cf-42be-81b8-581f73ab6bc7"", 
            ""Address1"" : ""123 Easy Street"", 
            ""City"" : ""Olathe"", 
            ""State"" : ""KS"", 
            ""PostalCode"" : ""60000"", 
            ""Country"" : ""US""
        }, 
        ""AltAddress"" : {
            ""_id"" : ""7d17c59e-5ebd-4b68-aef4-c8a20f52a8a2"", 
            ""Address1"" : null, 
            ""City"" : null, 
            ""State"" : null, 
            ""PostalCode"" : null, 
            ""Country"" : ""US""
        }, 
        ""WorkPhone"" : ""7506"", 
        ""CellPhone"" : ""9130000000"", 
        ""OfficeLocation"" : ""MCI7""
    }, 
    ""FirstName"" : ""Another"", 
    ""MiddleName"" : ""P"", 
    ""LastName"" : ""Person"", 
    ""Gender"" : NumberInt(70), 
    ""DoB"" : ISODate(""1966-10-14T00:00:00.000+0000""), 
    ""Ssn"" : null, 
    ""Status"" : NumberInt(65), 
    ""WorkType"" : NumberInt(1), 
    ""CostCenterCode"" : ""1299"", 
    ""JobTitle"" : ""Medical Representative I"", 
    ""Department"" : ""Distribution Center - Var HC"", 
    ""PriorHours"" : [
        {
            ""_id"" : ""6369af57-7ee9-43fa-b1c0-9d62888d2477"", 
            ""HoursWorked"" : 0.0, 
            ""AsOf"" : ISODate(""2016-09-24T00:00:00.000+0000"")
        }, 
        {
            ""_id"" : ""7f1d02d4-e620-47ee-8eb1-8bb865f59109"", 
            ""HoursWorked"" : 0.0, 
            ""AsOf"" : ISODate(""2016-10-29T00:00:00.000+0000"")
        }, 
        {
            ""_id"" : ""0276ba2d-ab70-4e7b-9fd5-0e8789b6999a"", 
            ""HoursWorked"" : 0.0, 
            ""AsOf"" : ISODate(""2017-01-19T00:00:00.000+0000"")
        }, 
        {
            ""_id"" : ""ac453d23-4dde-4ee4-957e-fc6beb4f10c3"", 
            ""HoursWorked"" : 0.0, 
            ""AsOf"" : ISODate(""2017-02-11T00:00:00.000+0000"")
        }, 
        {
            ""_id"" : ""a66f777f-381f-46db-89d9-1681c646d02e"", 
            ""HoursWorked"" : 0.0, 
            ""AsOf"" : ISODate(""2017-02-13T00:00:00.000+0000"")
        }
    ], 
    ""ServiceDate"" : ISODate(""2014-03-02T00:00:00.000+0000""), 
    ""HireDate"" : ISODate(""2014-03-02T00:00:00.000+0000""), 
    ""WorkState"" : ""KS"", 
    ""WorkCountry"" : ""US"", 
    ""WorkSchedules"" : [
        {
            ""_id"" : ""f1c74f29-7d57-4747-a313-328a7019d2c7"", 
            ""ScheduleType"" : NumberInt(0), 
            ""StartDate"" : ISODate(""2014-03-02T00:00:00.000+0000""), 
            ""EndDate"" : ISODate(""2016-09-23T00:00:00.000+0000""), 
            ""Times"" : [
                {
                    ""_id"" : ""a10ea090-60f6-4049-8f03-d73afd2cd5d9"", 
                    ""TotalMinutes"" : NumberInt(0), 
                    ""SampleDate"" : ISODate(""2014-03-02T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""41f5ab4c-ca38-437e-b210-7932618ca3a3"", 
                    ""TotalMinutes"" : NumberInt(0), 
                    ""SampleDate"" : ISODate(""2014-03-03T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""a190c507-ed39-4301-a257-1728a12b2ffa"", 
                    ""TotalMinutes"" : NumberInt(0), 
                    ""SampleDate"" : ISODate(""2014-03-04T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""90e5bd96-3951-4aec-a81f-1c10aa49cf3c"", 
                    ""TotalMinutes"" : NumberInt(0), 
                    ""SampleDate"" : ISODate(""2014-03-05T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""7133c409-b8b0-4afb-bd48-5e0883dbc036"", 
                    ""TotalMinutes"" : NumberInt(0), 
                    ""SampleDate"" : ISODate(""2014-03-06T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""d59aec22-f94f-4ef8-aa47-5f483e121e73"", 
                    ""TotalMinutes"" : NumberInt(0), 
                    ""SampleDate"" : ISODate(""2014-03-07T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""4f978e2f-18b0-4c8a-b547-db164798014c"", 
                    ""TotalMinutes"" : NumberInt(0), 
                    ""SampleDate"" : ISODate(""2014-03-08T00:00:00.000+0000"")
                }
            ]
        }, 
        {
            ""_id"" : ""2af51663-e3dc-4f04-94b9-98acad90a98f"", 
            ""ScheduleType"" : NumberInt(0), 
            ""StartDate"" : ISODate(""2016-09-24T00:00:00.000+0000""), 
            ""EndDate"" : null, 
            ""Times"" : [
                {
                    ""_id"" : ""44ab7bee-acf3-4605-b6f1-d302b776c123"", 
                    ""TotalMinutes"" : null, 
                    ""SampleDate"" : ISODate(""2016-09-18T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""e15858b9-ef0b-4df6-b649-3600b7196d21"", 
                    ""TotalMinutes"" : NumberInt(0), 
                    ""SampleDate"" : ISODate(""2016-09-19T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""03b7fd8a-3045-4443-9ab7-e7e09e5d3dab"", 
                    ""TotalMinutes"" : NumberInt(0), 
                    ""SampleDate"" : ISODate(""2016-09-20T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""896c23bf-0edc-4f94-940c-fc3e0ec70fdb"", 
                    ""TotalMinutes"" : NumberInt(0), 
                    ""SampleDate"" : ISODate(""2016-09-21T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""0d184b49-be7f-4aa6-8136-b686e4764967"", 
                    ""TotalMinutes"" : NumberInt(0), 
                    ""SampleDate"" : ISODate(""2016-09-22T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""674b4c55-d404-4613-9c8a-df7561d09bba"", 
                    ""TotalMinutes"" : NumberInt(0), 
                    ""SampleDate"" : ISODate(""2016-09-23T00:00:00.000+0000"")
                }, 
                {
                    ""_id"" : ""402a5a17-3262-4ad7-ab28-67aef4804697"", 
                    ""TotalMinutes"" : null, 
                    ""SampleDate"" : ISODate(""2016-09-24T00:00:00.000+0000"")
                }
            ]
        }
    ], 
    ""Meets50In75MileRule"" : false, 
    ""IsKeyEmployee"" : false, 
    ""IsExempt"" : false, 
    ""MilitaryStatus"" : NumberInt(0), 
    ""CustomFields"" : [
        {
            ""_id"" : ObjectId(""55a6b04c3de0960bb0a99c13""), 
            ""cdt"" : ISODate(""2015-07-15T19:11:08.922+0000""), 
            ""cby"" : ObjectId(""000000000000000000000000""), 
            ""mdt"" : ISODate(""2015-07-15T19:11:08.922+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""EMPLOYEECLASS"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""EmployeeClass"", 
            ""Description"" : ""Employee Class"", 
            ""Label"" : ""EmployeeClass"", 
            ""HelpText"" : ""Employee Class"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [
                {
                    ""Key"" : ""B-Fixed Term Contractor - EU"", 
                    ""Value"" : ""B-Fixed Term Contractor - EU""
                }, 
                {
                    ""Key"" : ""F-Regular Full Time"", 
                    ""Value"" : ""F-Regular Full Time""
                }, 
                {
                    ""Key"" : ""H-Regular Part Time - 20 + Hours"", 
                    ""Value"" : ""H-Regular Part Time - 20 + Hours""
                }, 
                {
                    ""Key"" : ""I-Intern"", 
                    ""Value"" : ""I-Intern""
                }, 
                {
                    ""Key"" : ""N-Trainee European"", 
                    ""Value"" : ""N-Trainee European""
                }, 
                {
                    ""Key"" : ""P-Apprentice"", 
                    ""Value"" : ""P-Apprentice""
                }, 
                {
                    ""Key"" : ""R-Regular Reduced Time  30 + Hrs"", 
                    ""Value"" : ""R-Regular Reduced Time  30 + Hrs""
                }, 
                {
                    ""Key"" : ""S-Seasonal/Short-Term"", 
                    ""Value"" : ""S-Seasonal/Short-Term""
                }, 
                {
                    ""Key"" : ""T-Onsite Vendor"", 
                    ""Value"" : ""T-Onsite Vendor""
                }, 
                {
                    ""Key"" : ""V-Offsite Vendor"", 
                    ""Value"" : ""V-Offsite Vendor""
                }, 
                {
                    ""Key"" : ""W-3P Onsite Worker"", 
                    ""Value"" : ""W-3P Onsite Worker""
                }, 
                {
                    ""Key"" : ""X-Regular Flex Time - < 20 Hrs"", 
                    ""Value"" : ""X-Regular Flex Time - < 20 Hrs""
                }, 
                {
                    ""Key"" : ""M - Internal Staffing Solutions"", 
                    ""Value"" : ""M - Internal Staffing Solutions""
                }
            ], 
            ""SelectedValue"" : ""F"", 
            ""FileOrder"" : NumberInt(16), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""562992f03de0960f481aa0a1""), 
            ""cdt"" : ISODate(""2015-10-23T01:52:48.763+0000""), 
            ""cby"" : ObjectId(""000000000000000000000000""), 
            ""mdt"" : ISODate(""2015-11-11T06:08:08.602+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""HRASSOCIATE"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""HRAssociate"", 
            ""Description"" : ""Level"", 
            ""Label"" : ""Level"", 
            ""HelpText"" : ""Level"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""4"", 
            ""FileOrder"" : NumberInt(18), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""54db96b2a32aa0097c0177f6""), 
            ""cdt"" : ISODate(""2015-02-11T17:51:46.951+0000""), 
            ""cby"" : ObjectId(""546e5319a32aa00f7808626c""), 
            ""mdt"" : ISODate(""2016-07-12T03:13:15.330+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""JOBCODE"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Job Code"", 
            ""Description"" : ""Job Code"", 
            ""Label"" : ""Job Code"", 
            ""HelpText"" : ""Job Code"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""P12210"", 
            ""FileOrder"" : NumberInt(3), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""54db9722a32aa0097c0177fc""), 
            ""cdt"" : ISODate(""2015-02-11T17:53:38.046+0000""), 
            ""cby"" : ObjectId(""546e5319a32aa00f7808626c""), 
            ""mdt"" : ISODate(""2016-07-12T03:13:30.103+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""BUSINESSTITLE"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Business Title"", 
            ""Description"" : ""Business Title"", 
            ""Label"" : ""Business Title"", 
            ""HelpText"" : ""Business Title"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""Onsite Medical Representative"", 
            ""FileOrder"" : NumberInt(4), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""54db96e9a32aa0097c0177f8""), 
            ""cdt"" : ISODate(""2015-02-11T17:52:41.398+0000""), 
            ""cby"" : ObjectId(""546e5319a32aa00f7808626c""), 
            ""mdt"" : ISODate(""2016-07-12T03:09:23.186+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""LOCATION"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Location"", 
            ""Description"" : ""Location"", 
            ""Label"" : ""Location"", 
            ""HelpText"" : ""Location"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""8051"", 
            ""FileOrder"" : NumberInt(5), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""54db9765a32aa0097c017802""), 
            ""cdt"" : ISODate(""2015-02-11T17:54:45.441+0000""), 
            ""cby"" : ObjectId(""546e5319a32aa00f7808626c""), 
            ""mdt"" : ISODate(""2016-07-12T03:09:10.628+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""LOCATIONTYPE"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Location Type"", 
            ""Description"" : ""Location Type"", 
            ""Label"" : ""Location Type"", 
            ""HelpText"" : ""Location Type"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""FC"", 
            ""FileOrder"" : NumberInt(6), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""551cca90a32aa00260295703""), 
            ""cdt"" : ISODate(""2015-04-02T04:50:24.760+0000""), 
            ""cby"" : ObjectId(""546e5319a32aa00f7808626c""), 
            ""mdt"" : ISODate(""2016-07-12T03:08:57.275+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""LASTDATEWORKED"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Last Date Worked"", 
            ""Description"" : ""Last Date Worked"", 
            ""Label"" : ""Last Date Worked"", 
            ""HelpText"" : ""Last Date Worked"", 
            ""DataType"" : NumberInt(8), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(7), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""578d4858a32aa1a51c7b406b""), 
            ""cdt"" : ISODate(""2016-07-18T21:21:28.286+0000""), 
            ""cby"" : ObjectId(""56cf2150a32aa00a783e5bc9""), 
            ""mdt"" : ISODate(""2016-07-21T15:29:21.624+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""TERMACTIONREASON"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Term Action Reason"", 
            ""Description"" : ""Term Action Reason"", 
            ""Label"" : ""Term Action Reason"", 
            ""HelpText"" : ""Term Action Reason"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(8), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""57757ee3a32aa01e08c57df9""), 
            ""cdt"" : ISODate(""2016-06-30T20:19:47.611+0000""), 
            ""cby"" : ObjectId(""56cf2150a32aa00a783e5bc9""), 
            ""mdt"" : ISODate(""2016-07-21T15:29:29.642+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""TERMFACTOR"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Term Factor"", 
            ""Description"" : ""Term Factor"", 
            ""Label"" : ""Term Factor"", 
            ""HelpText"" : ""Term Factor"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(9), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""57757f06a32aa01e08c57dfa""), 
            ""cdt"" : ISODate(""2016-06-30T20:20:22.762+0000""), 
            ""cby"" : ObjectId(""56cf2150a32aa00a783e5bc9""), 
            ""mdt"" : ISODate(""2016-07-12T03:08:11.800+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""FCLMAREA"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""FCLM Area"", 
            ""Description"" : ""FCLM Area"", 
            ""Label"" : ""FCLM Area"", 
            ""HelpText"" : ""FCLM Area"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(10), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""57757cfea32aa01e08c57df4""), 
            ""cdt"" : ISODate(""2016-06-30T20:11:42.177+0000""), 
            ""cby"" : ObjectId(""56cf2150a32aa00a783e5bc9""), 
            ""mdt"" : ISODate(""2016-07-12T03:08:01.395+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""FCLMJOB"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""FCLM JOB"", 
            ""Description"" : ""FCLM Job"", 
            ""Label"" : ""FCLM JOB"", 
            ""HelpText"" : ""FCLM Job"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(11), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : true
        }, 
        {
            ""_id"" : ObjectId(""57757d3aa32aa01e08c57df5""), 
            ""cdt"" : ISODate(""2016-06-30T20:12:42.047+0000""), 
            ""cby"" : ObjectId(""56cf2150a32aa00a783e5bc9""), 
            ""mdt"" : ISODate(""2016-07-12T03:07:47.074+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""SHIFTPATTERN"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Shift Pattern"", 
            ""Description"" : ""Shift Pattern"", 
            ""Label"" : ""Shift Pattern"", 
            ""HelpText"" : ""Shift Pattern"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""AAAA"", 
            ""FileOrder"" : NumberInt(12), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : true
        }, 
        {
            ""_id"" : ObjectId(""57757dbea32aa01e08c57df7""), 
            ""cdt"" : ISODate(""2016-06-30T20:14:54.091+0000""), 
            ""cby"" : ObjectId(""56cf2150a32aa00a783e5bc9""), 
            ""mdt"" : ISODate(""2016-07-12T03:07:34.719+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""SHIFTPATTERNDESCRIPTION"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Shift Pattern Description"", 
            ""Description"" : ""Shift Pattern Description"", 
            ""Label"" : ""Shift Pattern Description"", 
            ""HelpText"" : ""Shift Pattern Description"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""FC Variable Sched"", 
            ""FileOrder"" : NumberInt(13), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""57757cbfa32aa01e08c57df3""), 
            ""cdt"" : ISODate(""2016-06-30T20:10:39.775+0000""), 
            ""cby"" : ObjectId(""56cf2150a32aa00a783e5bc9""), 
            ""mdt"" : ISODate(""2016-07-12T03:06:49.791+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""DEPARTMENT"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Department"", 
            ""Description"" : ""Department"", 
            ""Label"" : ""Department"", 
            ""HelpText"" : ""Department"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""1200070"", 
            ""FileOrder"" : NumberInt(14), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""57757c74a32aa01e08c57df1""), 
            ""cdt"" : ISODate(""2016-06-30T20:09:24.923+0000""), 
            ""cby"" : ObjectId(""56cf2150a32aa00a783e5bc9""), 
            ""mdt"" : ISODate(""2016-07-12T03:06:24.738+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""DEPARTMENTDESCRIPTION"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Department Description"", 
            ""Description"" : ""Department Description"", 
            ""Label"" : ""Department Description"", 
            ""HelpText"" : ""Department Description"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""FC Support"", 
            ""FileOrder"" : NumberInt(15), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""57758027a32aa01e08c57dfd""), 
            ""cdt"" : ISODate(""2016-06-30T20:25:11.014+0000""), 
            ""cby"" : ObjectId(""56cf2150a32aa00a783e5bc9""), 
            ""mdt"" : ISODate(""2016-07-12T03:05:48.343+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""EMPLOYEECLASSDESCRIPTION"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Employee Class Description"", 
            ""Description"" : ""Employee Class Description"", 
            ""Label"" : ""Employee Class Description"", 
            ""HelpText"" : ""Employee Class Description"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""Regular Full Time"", 
            ""FileOrder"" : NumberInt(17), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""54db9705a32aa0097c0177f9""), 
            ""cdt"" : ISODate(""2015-02-11T17:53:09.105+0000""), 
            ""cby"" : ObjectId(""546e5319a32aa00f7808626c""), 
            ""mdt"" : ISODate(""2016-07-12T03:05:17.330+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""STEAMMANAGER"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""STeam Manager"", 
            ""Description"" : ""STeam Manager"", 
            ""Label"" : ""STeam Manager"", 
            ""HelpText"" : ""STeam Manager"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""015989"", 
            ""FileOrder"" : NumberInt(19), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""54db973ba32aa0097c0177fd""), 
            ""cdt"" : ISODate(""2015-02-11T17:54:03.333+0000""), 
            ""cby"" : ObjectId(""546e5319a32aa00f7808626c""), 
            ""mdt"" : ISODate(""2016-07-12T03:04:58.454+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""AMAZONLOGIN"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Amazon Login"", 
            ""Description"" : ""Amazon Login"", 
            ""Label"" : ""Amazon Login"", 
            ""HelpText"" : ""Amazon Login"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""CKLEARY"", 
            ""FileOrder"" : NumberInt(20), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""577581b3a32aa01e08c57e00""), 
            ""cdt"" : ISODate(""2016-06-30T20:31:47.763+0000""), 
            ""cby"" : ObjectId(""56cf2150a32aa00a783e5bc9""), 
            ""mdt"" : ISODate(""2016-07-12T03:04:30.998+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""HREMPLOYEE"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""HR Employee ?"", 
            ""Description"" : ""HR Employee ?"", 
            ""Label"" : ""HR Employee ?"", 
            ""HelpText"" : ""HR Employee ?"", 
            ""DataType"" : NumberInt(4), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : false, 
            ""FileOrder"" : NumberInt(21), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""577d8a4bc5f3321004ad1cf6""), 
            ""cdt"" : ISODate(""2016-07-06T22:46:35.764+0000""), 
            ""cby"" : ObjectId(""56b398d8c5f32f15b85fa75d""), 
            ""mdt"" : ISODate(""2016-07-12T03:04:17.286+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""COMPANY"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Company"", 
            ""Description"" : ""Company"", 
            ""Label"" : ""Company"", 
            ""HelpText"" : ""Company"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""A07"", 
            ""FileOrder"" : NumberInt(22), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""577d8a6ec5f3321004ad1cf7""), 
            ""cdt"" : ISODate(""2016-07-06T22:47:10.364+0000""), 
            ""cby"" : ObjectId(""56b398d8c5f32f15b85fa75d""), 
            ""mdt"" : ISODate(""2016-07-12T03:04:04.556+0000""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""CustomerId"" : ObjectId(""000000000000000000000001""), 
            ""EmployerId"" : ObjectId(""000000000000000000000001""), 
            ""Code"" : ""COMPANYNAME"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(1), 
            ""Name"" : ""Company Name"", 
            ""Description"" : ""Company Name"", 
            ""Label"" : ""Company Name"", 
            ""HelpText"" : ""Company Name"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""Amazon.com.ksdc LLC"", 
            ""FileOrder"" : NumberInt(23), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }
    ], 
    ""EmployerName"" : ""Amazon""
}";
            #endregion

            var omrEmp = BsonSerializer.Deserialize<Employee>(json).Save();
            omrEmp.CustomFields.ForEach(c => c.Save());

            #region Initial Employee Contacts
            BsonSerializer.Deserialize<EmployeeContact>(@"{ 
    ""_id"" : ObjectId(""5877298ba9a8f95646cef850""), 
    ""ContactPosition"" : NumberInt(3), 
    ""ContactTypeCode"" : ""OMR"", 
    ""CustomerId"" : ObjectId(""000000000000000000000001""), 
    ""EmployeeId"" : ObjectId(""54cc83b8a32a9f0ad08635a2""), 
    ""EmployerId"" : ObjectId(""000000000000000000000001""), 
    ""EmployeeNumber"" : ""180581"", 
    ""Contact"" : {
        ""_id"" : ""409589b4-7da2-40f0-9fa0-a28f7fc35f9d"", 
        ""Address"" : {
            ""_id"" : ""4650203a-944a-4d45-bb9a-b30a56d0e5ff"", 
            ""Address1"" : null, 
            ""Address2"" : null, 
            ""City"" : null, 
            ""State"" : null, 
            ""Country"" : ""US"", 
            ""PostalCode"" : null
        }, 
        ""LastName"" : ""Gilkey"", 
        ""FirstName"" : ""Ethan"", 
        ""WorkPhone"" : null, 
        ""HomePhone"" : null, 
        ""Fax"" : null, 
        ""CellPhone"" : ""636 2330487"", 
        ""Email"" : ""eggilkey@amazon.com"", 
        ""AltEmail"" : ""ethangilkey4@yahoo.com""
    }, 
    ""cby"" : ObjectId(""000000000000000000000000""), 
    ""cdt"" : ISODate(""2017-01-12T06:58:59.668+0000""), 
    ""mby"" : ObjectId(""000000000000000000000000""), 
    ""mdt"" : ISODate(""2017-01-12T06:58:59.668+0000""), 
    ""RelatedEmployeeNumber"" : null, 
    ""ContactTypeName"" : ""OMR"", 
    ""MilitaryStatus"" : NumberInt(0)
}").Save();
            BsonSerializer.Deserialize<EmployeeContact>(@"{ 
    ""_id"" : ObjectId(""5877298ba9a8f95646cef84f""), 
    ""ContactPosition"" : NumberInt(2), 
    ""ContactTypeCode"" : ""OMR"", 
    ""CustomerId"" : ObjectId(""000000000000000000000001""), 
    ""EmployeeId"" : ObjectId(""54cc83b8a32a9f0ad08635a2""), 
    ""EmployerId"" : ObjectId(""000000000000000000000001""), 
    ""EmployeeNumber"" : ""180581"", 
    ""Contact"" : {
        ""_id"" : ""0c261823-cf18-4d06-8397-9376f8c0ca3e"", 
        ""Address"" : {
            ""_id"" : ""f68e4559-4376-4d6d-91d7-2461782401bd"", 
            ""Address1"" : null, 
            ""Address2"" : null, 
            ""City"" : null, 
            ""State"" : null, 
            ""Country"" : ""US"", 
            ""PostalCode"" : null
        }, 
        ""LastName"" : ""Green"", 
        ""FirstName"" : ""Cody"", 
        ""WorkPhone"" : null, 
        ""HomePhone"" : null, 
        ""Fax"" : null, 
        ""CellPhone"" : ""816 8080775"", 
        ""Email"" : ""greencod@amazon.com"", 
        ""AltEmail"" : ""emtgreen5@gmail.com""
    }, 
    ""cby"" : ObjectId(""000000000000000000000000""), 
    ""cdt"" : ISODate(""2017-01-12T06:58:59.668+0000""), 
    ""mby"" : ObjectId(""000000000000000000000000""), 
    ""mdt"" : ISODate(""2017-02-13T07:33:20.412+0000""), 
    ""RelatedEmployeeNumber"" : null, 
    ""ContactTypeName"" : ""OMR"", 
    ""MilitaryStatus"" : NumberInt(0)
}").Save();
            #endregion

            ProcessFile("PROD_AZ_ELIG_ABSENCESOFT_20170216105948.csv",
                "O|180581|OMR|OMR|0|1|100189134|Leary|Constance|ckleary@amazon.com|ckleary66@comcast.net|913/538-9008||913/375-5936|",
                "O|180581|OMR|OMR|0|2||Green|Cody|greencod@amazon.com|emtgreen5@gmail.com|||816 8080775|");

            EmployeeContact ckleary = EmployeeContact.AsQueryable()
                .Where(c => c.CustomerId == CustomerId)
                .Where(c => c.EmployerId == EmployerId)
                .Where(c => c.EmployeeNumber == "180581")
                .Where(c => c.RelatedEmployeeNumber == "100189134")
                .Where(c => c.ContactTypeCode == "OMR")
                .FirstOrDefault();
            Assert.IsNotNull(ckleary, "Employee contact was not found which is bad mmm'kay");

            ProcessFile("PROD_AZ_ELIG_ABSENCESOFT_20170303110027.csv",
                "C|180581|T06221|IT Support Engineer I|8051|FC||||26|IT Support|AAAA|FC Variable Sched|1213|Amazon Pantry Fulfillment-Var|F|Regular Full Time|3|015989|KEMMERLY|N|A07|Amazon.com.ksdc LLC");

            ckleary = EmployeeContact.AsQueryable()
                .Where(c => c.CustomerId == CustomerId)
                .Where(c => c.EmployerId == EmployerId)
                .Where(c => c.EmployeeNumber == "180581")
                .Where(c => c.RelatedEmployeeNumber == "100189134")
                .Where(c => c.ContactTypeCode == "OMR")
                .FirstOrDefault();
            Assert.IsNotNull(ckleary, "Employee contact was not found which is bad mmm'kay");

            ProcessFile("PROD_AZ_ELIG_ABSENCESOFT_20170310110139.csv",
                "O|180581|OPS|OPS|0|1||Orbin|Michael|orbinm@amazon.com|mrr1mjo@hotmail.com|651/207-3015|651/207-3015||",
                "O|180581|OPS|OPS|0|2||Thomas|Joshua|jthomasm@amazon.com|joshthomas06@gmail.com|||937/367-8579|",
                "O|180581|OPS|OPS|0|3||Grimes|Todd|todgrime@amazon.com|Grimes.Todd@gmail.com|||816 8061872|",
                "O|180581|OPS|OPS|0|4||Yoder|Dean|dyoder@amazon.com|dlyoder@bsu.edu|||317/437-0294|",
                "O|180581|OPS|OPS|0|5||Ulmer|Cameron|ulmercam@amazon.com||||816.261.0436|");

            ckleary = EmployeeContact.AsQueryable()
                .Where(c => c.CustomerId == CustomerId)
                .Where(c => c.EmployerId == EmployerId)
                .Where(c => c.EmployeeNumber == "180581")
                .Where(c => c.RelatedEmployeeNumber == "100189134")
                .Where(c => c.ContactTypeCode == "OMR")
                .FirstOrDefault();
            Assert.IsNotNull(ckleary, "Employee contact was not found which is bad mmm'kay");

            ProcessFile("PROD_AZ_ELIG_ABSENCESOFT_20170316110833.csv",
                "C|180581|T06221|IT Support Engineer I|8051|FC||||26|IT Support|AAAA|FC Variable Sched|1231|FC IT - Variable|F|Regular Full Time|3|015989|KEMMERLY|N|A07|Amazon.com.ksdc LLC");

            ckleary = EmployeeContact.AsQueryable()
                .Where(c => c.CustomerId == CustomerId)
                .Where(c => c.EmployerId == EmployerId)
                .Where(c => c.EmployeeNumber == "180581")
                .Where(c => c.RelatedEmployeeNumber == "100189134")
                .Where(c => c.ContactTypeCode == "OMR")
                .FirstOrDefault();
            Assert.IsNotNull(ckleary, "Employee contact was not found which is bad mmm'kay");

            ProcessFile("PROD_AZ_ELIG_ABSENCESOFT_20170403114729.csv",
                "O|180581|OPS|OPS|0|1||Orbin|Michael|orbinm@amazon.com|mrr1mjo@hotmail.com|651/207-3015|651/207-3015||",
                "O|180581|OPS|OPS|0|2||Thomas|Joshua|jthomasm@amazon.com|joshthomas06@gmail.com|||937/367-8579|",
                "O|180581|OPS|OPS|0|3||Grimes|Todd|todgrime@amazon.com|Grimes.Todd@gmail.com|||816 8061872|",
                "O|180581|OPS|OPS|0|4||Yoder|Dean|dyoder@amazon.com|dlyoder@bsu.edu|||317/437-0294|",
                "O|180581|OPS|OPS|0|5||Ulmer|Cameron|ulmercam@amazon.com||||816/261-0436|");

            ckleary = EmployeeContact.AsQueryable()
                .Where(c => c.CustomerId == CustomerId)
                .Where(c => c.EmployerId == EmployerId)
                .Where(c => c.EmployeeNumber == "180581")
                .Where(c => c.RelatedEmployeeNumber == "100189134")
                .Where(c => c.ContactTypeCode == "OMR")
                .FirstOrDefault();
            Assert.IsNotNull(ckleary, "Employee contact was not found which is bad mmm'kay");

            ProcessFile("PROD_AZ_ELIG_ABSENCESOFT_20170406114757.csv",
                "O|180581|HRA|HRAssistant|0|1||Billy|Gary|garybill@amazon.com|garybillyjr@yahoo.com||913/605-0539||",
                "O|180581|HRA|HRAssistant|0|2||Sadir-Hartgrave|Deborah|sadirhd@amazon.com|sadir85@yahoo.com|||913 3330639|");

            ckleary = EmployeeContact.AsQueryable()
                .Where(c => c.CustomerId == CustomerId)
                .Where(c => c.EmployerId == EmployerId)
                .Where(c => c.EmployeeNumber == "180581")
                .Where(c => c.RelatedEmployeeNumber == "100189134")
                .Where(c => c.ContactTypeCode == "OMR")
                .FirstOrDefault();
            Assert.IsNotNull(ckleary, "Employee contact was not found which is bad mmm'kay");
        }

        void ProcessFile(string fileName, params string[] rows)
        {
            EligibilityUpload upload = new EligibilityUpload()
            {
                CustomerId = CustomerId,
                EmployerId = EmployerId,
                FileKey = "debug/000000000000000000000001/000000000000000000000001/" + fileName,
                FileName = fileName,
                Records = rows.Length,
                Status = AbsenceSoft.Data.Enums.ProcessingStatus.Processing,
                CreatedById = User.DefaultUserId,
                ModifiedById = User.DefaultUserId,
                MimeType = "text/plain",
                Processed = 0L,
                Errors = 0L
            }
            .SetCreatedDate(DateTime.UtcNow)
            .SetModifiedDate(DateTime.UtcNow)
            .Save();

            int i = 1;
            var results = rows.Select(r =>
            {
                EligibilityUploadResult result = new EligibilityUploadResult().SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow);
                result.RowNumber = i;
                result.RowSource = r;
                result.RowParts = r.Split('|');
                result.CreatedById = User.DefaultUserId;
                result.ModifiedById = User.DefaultUserId;
                result.EligibilityUploadId = upload.Id;
                i++;

                return result;
            }).ToList();

            using (var svc = new EligibilityUploadService())
                svc.ProcessRecords(upload, results);

            Assert.IsFalse(results.Any(r => r.IsError), string.Join(Environment.NewLine, results.Where(r => r.IsError).Select(r => r.Error)));
        }
    }
}
