﻿using System;
using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Policies;
using System.Collections.Generic;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic;
using AbsenceSoft.Common;
using AbsenceSoft;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Logic.Pay;
using MongoDB.Bson.Serialization;
using MongoDB.Bson;

namespace AbsenceSoft.Tests.ProdSupport
{
    [TestClass]
    public class AT2539Tests
    {
        /// <summary>
        /// https://absencesoft.atlassian.net/browse/AT-2539
        /// FTime Tracker showing 0.2 work weeks less after case closed.
        /// </summary>
        /// <remarks>This is bad mmm-kay, calcs that do this are bad mmm-kay.</remarks>
        [TestMethod]
        public void AT2539MissingUsageOnDeterminationAndReCalc()
        {
            /*
             * AT-2539 - Time Tracker showing 0.2 work weeks less after case closed
             */
            
            #region Policy PREGFAM
            var pregFam = BsonSerializer.Deserialize<Policy>(BsonDocument.Parse(@"{ 
    ""_id"" : ObjectId(""56c77a17c5f33116c8a8422b""), 
    ""IsDuplicate"" : false, 
    ""EmployerName"" : ""TEST"", 
    ""cdt"" : ISODate(""2016-02-19T20:24:55.287+0000""), 
    ""cby"" : ObjectId(""000000000000000000000000""), 
    ""mdt"" : ISODate(""2016-06-09T19:31:18.798+0000""), 
    ""mby"" : ObjectId(""000000000000000000000000""), 
    ""CustomerId"" : ObjectId(""000000000000000000000002""), 
    ""EmployerId"" : ObjectId(""000000000000000000000002""), 
    ""Code"" : ""PREGFAM"", 
    ""Name"" : ""Pregnancy Family Leave"", 
    ""Description"" : ""Pregnancy Family Leave"", 
    ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000+0000""), 
    ""PolicyType"" : NumberInt(5), 
    ""RuleGroups"" : [
        {
            ""_id"" : ""9f7fabdc-66a0-4541-bef1-9784d060c202"", 
            ""Name"" : ""Policy Eligibility"", 
            ""Description"" : ""Employee must have 12 months of Service"", 
            ""Rules"" : [
                {
                    ""_id"" : ""b20f2ecd-08f4-4d00-8afd-592e7f24701c"", 
                    ""Name"" : ""Worked 12 Months"", 
                    ""Description"" : ""The Employee must have 12 months of Service"", 
                    ""LeftExpression"" : ""Has1YearOfService()"", 
                    ""Operator"" : ""=="", 
                    ""RightExpression"" : ""true""
                }
            ], 
            ""SuccessType"" : NumberInt(0), 
            ""RuleGroupType"" : NumberInt(1), 
            ""Entitlement"" : null, 
            ""PaymentEntitlement"" : []
        }
    ], 
    ""AbsenceReasons"" : [
        {
            ""CaseTypeConsecutive"" : true, 
            ""CaseTypeIntermittent"" : false, 
            ""CaseTypeReduced"" : false, 
            ""CaseTypeAdministrative"" : false, 
            ""EntitlementTypeText"" : ""Work Weeks"", 
            ""_id"" : ""cddf0dd2-0e4b-423a-8cb4-8db2cb74b26b"", 
            ""ReasonCode"" : ""ADOPT"", 
            ""EntitlementType"" : NumberInt(1), 
            ""Entitlement"" : 5.0, 
            ""CombinedForSpouses"" : false, 
            ""PeriodType"" : NumberInt(5), 
            ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000+0000""), 
            ""CaseTypes"" : NumberInt(1), 
            ""Paid"" : false, 
            ""PaymentTiers"" : [], 
            ""RuleGroups"" : [], 
            ""ResidenceState"" : null, 
            ""ShowType"" : NumberInt(3), 
            ""PolicyEvents"" : [], 
            ""AllowOffset"" : false, 
            ""UseWholeDollarAmountsOnly"" : false
        }, 
        {
            ""CaseTypeConsecutive"" : true, 
            ""CaseTypeIntermittent"" : false, 
            ""CaseTypeReduced"" : false, 
            ""CaseTypeAdministrative"" : false, 
            ""EntitlementTypeText"" : ""Work Weeks"", 
            ""_id"" : ""0973de5f-89de-4020-96d9-05bbe9532420"", 
            ""ReasonCode"" : ""PREGMAT"", 
            ""EntitlementType"" : NumberInt(1), 
            ""Entitlement"" : 5.0, 
            ""CombinedForSpouses"" : false, 
            ""PeriodType"" : NumberInt(5), 
            ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000+0000""), 
            ""CaseTypes"" : NumberInt(1), 
            ""Paid"" : false, 
            ""PaymentTiers"" : [], 
            ""RuleGroups"" : [], 
            ""ResidenceState"" : null, 
            ""Gender"" : NumberInt(70), 
            ""ShowType"" : NumberInt(3), 
            ""PolicyEvents"" : [], 
            ""AllowOffset"" : false, 
            ""UseWholeDollarAmountsOnly"" : false
        }, 
        {
            ""CaseTypeConsecutive"" : true, 
            ""CaseTypeIntermittent"" : false, 
            ""CaseTypeReduced"" : false, 
            ""CaseTypeAdministrative"" : false, 
            ""EntitlementTypeText"" : ""Work Weeks"", 
            ""_id"" : ""5b9177ee-9552-4ba9-bf92-dd0cda99f1b3"", 
            ""ReasonId"" : ""56c77a48c5f33116c8a8422c"", 
            ""EntitlementType"" : NumberInt(1), 
            ""Entitlement"" : 5.0, 
            ""CombinedForSpouses"" : false, 
            ""PeriodType"" : NumberInt(5), 
            ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000+0000""), 
            ""CaseTypes"" : NumberInt(1), 
            ""Paid"" : false, 
            ""PaymentTiers"" : [], 
            ""RuleGroups"" : [], 
            ""ResidenceState"" : null, 
            ""ShowType"" : NumberInt(3), 
            ""PolicyEvents"" : [], 
            ""AllowOffset"" : false, 
            ""UseWholeDollarAmountsOnly"" : false
        }
    ], 
    ""WorkState"" : null, 
    ""ResidenceState"" : null, 
    ""Gender"" : null, 
    ""ConsecutiveTo"" : [
        ""PREGMED""
    ]
}")).Save();
            #endregion

            #region Policy PREGMED
            var pregMed = BsonSerializer.Deserialize<Policy>(BsonDocument.Parse(@"{ 
    ""_id"" : ObjectId(""56c77856c5f33116c8a8422a""), 
    ""IsDuplicate"" : false, 
    ""EmployerName"" : ""OppenheimerFunds"", 
    ""cdt"" : ISODate(""2016-02-19T20:17:26.150+0000""), 
    ""cby"" : ObjectId(""000000000000000000000000""), 
    ""mdt"" : ISODate(""2016-06-06T22:10:14.339+0000""), 
    ""mby"" : ObjectId(""000000000000000000000000""), 
    ""CustomerId"" : ObjectId(""000000000000000000000002""), 
    ""EmployerId"" : ObjectId(""000000000000000000000002""), 
    ""Code"" : ""PREGMED"", 
    ""Name"" : ""Pregnancy Medical Leave"", 
    ""Description"" : ""Pregnancy Medical Leave"", 
    ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000+0000""), 
    ""PolicyType"" : NumberInt(5), 
    ""RuleGroups"" : [], 
    ""AbsenceReasons"" : [
        {
            ""CaseTypeConsecutive"" : true, 
            ""CaseTypeIntermittent"" : false, 
            ""CaseTypeReduced"" : false, 
            ""CaseTypeAdministrative"" : false, 
            ""EntitlementTypeText"" : ""Work Weeks"", 
            ""_id"" : ""cd9c6548-f393-443d-bbdf-dd0b23a5098d"", 
            ""ReasonCode"" : ""PREGMAT"", 
            ""EntitlementType"" : NumberInt(1), 
            ""Entitlement"" : 8.0, 
            ""CombinedForSpouses"" : false, 
            ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000+0000""), 
            ""CaseTypes"" : NumberInt(1), 
            ""Paid"" : false, 
            ""PaymentTiers"" : [], 
            ""RuleGroups"" : [
                {
                    ""_id"" : ""62cf35c1-ca24-4aea-811c-0c87fd8d52ee"", 
                    ""Name"" : ""Worked 12 Months"", 
                    ""Description"" : ""Worked 12 Months"", 
                    ""Rules"" : [
                        {
                            ""_id"" : ""cf4d660d-af97-4137-861e-e9c771b016c1"", 
                            ""Name"" : ""Worked 12 Months"", 
                            ""Description"" : ""The Employee has worked at least 12 Months"", 
                            ""LeftExpression"" : ""Has1YearOfService()"", 
                            ""Operator"" : ""=="", 
                            ""RightExpression"" : ""true""
                        }
                    ], 
                    ""SuccessType"" : NumberInt(0), 
                    ""RuleGroupType"" : NumberInt(0), 
                    ""Entitlement"" : null, 
                    ""PaymentEntitlement"" : []
                }
            ], 
            ""ResidenceState"" : null, 
            ""Gender"" : NumberInt(70), 
            ""ShowType"" : NumberInt(3), 
            ""PolicyEvents"" : [], 
            ""AllowOffset"" : false, 
            ""UseWholeDollarAmountsOnly"" : false
        }
    ], 
    ""WorkState"" : null, 
    ""ResidenceState"" : null, 
    ""Gender"" : null, 
    ""ConsecutiveTo"" : []
}")).Save();
            #endregion

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == "PREGMAT" && r.CustomerId == null).First();

            Employee emp = Employee.GetById("000000000000000000000003");
            emp.Clean();
            emp.Id = ObjectId.GenerateNewId().ToString();
            emp.Gender = Gender.Female;
            emp.ServiceDate = new DateTime(2012, 6, 18).ToMidnight();
            emp.HireDate = new DateTime(2011, 6, 6).ToMidnight();
            emp.WorkSchedules = new List<Schedule>()
            {
                new Schedule()
                {
                    StartDate = emp.ServiceDate ?? new DateTime(2011, 6, 6).ToMidnight(),
                    EndDate = null,
                    ScheduleType = ScheduleType.Variable,
                    Times = new List<Time>(7)
                    {
                        new Time() { SampleDate = new DateTime(2011, 6, 5).ToMidnight(), TotalMinutes = null },
                        new Time() { SampleDate = new DateTime(2011, 6, 6).ToMidnight(), TotalMinutes = 450 },
                        new Time() { SampleDate = new DateTime(2011, 6, 7).ToMidnight(), TotalMinutes = 450 },
                        new Time() { SampleDate = new DateTime(2011, 6, 8).ToMidnight(), TotalMinutes = 450 },
                        new Time() { SampleDate = new DateTime(2011, 6, 9).ToMidnight(), TotalMinutes = 450 },
                        new Time() { SampleDate = new DateTime(2011, 6, 10).ToMidnight(), TotalMinutes = 450 },
                        new Time() { SampleDate = new DateTime(2011, 6, 11).ToMidnight(), TotalMinutes = null }
                    }
                }
            };
            emp.PriorHours.Add(new PriorHours()
            {
                AsOf = new DateTime(2016, 7, 8).ToMidnight(),
                HoursWorked = 1283.0D
            });
            emp.Save();

            using (CaseService cs = new CaseService(false))
            using (EligibilityService esvc = new EligibilityService())
            {
                DateTime startDate = new DateTime(2016, 3, 10).ToMidnight();
                DateTime endDate = new DateTime(2016, 6, 7).ToMidnight();

                Case newCase = cs.CreateCase(CaseStatus.Open, emp.Id, startDate, endDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

                LeaveOfAbsence loa = esvc.RunEligibility(newCase);

                // make sure there is an fmla policy
                var pols = newCase.Segments.SelectMany(s => s.AppliedPolicies).Where(a => a.Policy.Code == "PREGFAM" || a.Policy.Code == "PREGMED").ToList();
                pols.Should().NotBeEmpty("PREGFAM and PREGMED Policies not found");
                pols.Should().HaveCount(2);

                // get rid of eveything but the fmla policy. we only need one to test and it'll make life easier
                Assert.AreEqual(newCase.Segments.Count, 1);
                newCase.Segments[0].AppliedPolicies = pols;

                var casePolicies = (newCase.Segments ?? new List<CaseSegment>(0))
                    .SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>(0))
                    .Where(p => p.Status == EligibilityStatus.Eligible)
                    .Select(p => p.Policy.Code)
                    .ToArray();

                // Get the policy summary
                var summary = cs.GetEmployeePolicySummary(emp, new DateTime(2016, 7, 11).ToMidnight(), newCase, true, casePolicies).FirstOrDefault(s => s.PolicyCode == pregFam.Code);
                summary.Should().NotBeNull();
                summary.TimeUsed.Should().Be(0D);
                summary.TimeRemaining.Should().Be(5D);

                // this runs the calc too
                cs.ApplyDetermination(newCase, null, startDate, endDate, AdjudicationStatus.Approved);

                // Get the policy summary again
                summary = cs.GetEmployeePolicySummary(emp, new DateTime(2016, 7, 11).ToMidnight(), newCase, true, casePolicies).FirstOrDefault(s => s.PolicyCode == pregFam.Code);
                summary.Should().NotBeNull();
                summary.TimeUsed.Should().Be(4.8D);
                summary.TimeRemaining.Should().Be(.2D);

                // Get the policy EMPLOYEE summary
                summary = cs.GetEmployeePolicySummary(emp, new DateTime(2016, 7, 11).ToMidnight(), null, false).FirstOrDefault(s => s.PolicyCode == pregFam.Code);
                summary.Should().NotBeNull();
                summary.TimeUsed.Should().Be(4.8D);
                summary.TimeRemaining.Should().Be(.2D);

                cs.CaseClosed(newCase, endDate.AddDays(1), CaseClosureReason.ReturnToWork);

                // Get the policy summary for the last time after closure to see usage
                summary = cs.GetEmployeePolicySummary(emp, new DateTime(2016, 7, 11).ToMidnight(), newCase, true, casePolicies).FirstOrDefault(s => s.PolicyCode == pregFam.Code);
                summary.Should().NotBeNull();
                summary.TimeUsed.Should().Be(4.8D);
                summary.TimeRemaining.Should().Be(.2D);
            }

            pregFam.Delete();
            pregMed.Delete();
        }

        void TestDateHas(AppliedPolicy fmla, int year, int month, int day, int minutesUsed = 0x0, double workWeeksUsed = 0D)
        {
            var dateUsed = new DateTime(year, month, day).ToMidnight();
            var usage = fmla.Usage.FirstOrDefault(u => u.DateUsed == dateUsed);
            usage.Should().NotBeNull("Usage for date {0:yyyy-MM-dd} not found", dateUsed);
            usage.MinutesUsed.Should().Be(minutesUsed, "Minutes Used for date {0:yyyy-MM-dd} should be {1} but was {2}", dateUsed, minutesUsed, usage.MinutesUsed);
            var actualWorkWeeks = usage.DaysUsage(EntitlementType.WorkWeeks);
            actualWorkWeeks.Should().Be(workWeeksUsed, "Work Weeks Used for date {0:yyyy-MM-dd} should be {1} but was {2}", dateUsed, workWeeksUsed, actualWorkWeeks);
        }
    }
}
