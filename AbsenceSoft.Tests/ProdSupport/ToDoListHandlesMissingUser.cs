﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Tasks;
using AbsenceSoft.Data.ToDo;
using FluentAssertions;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic;

namespace AbsenceSoft.Tests.ProdSupport
{
    [TestClass]

    public class ToDoListHandlesMissingUser
    {
        [TestMethod]
        public void ToDoListHandlesMissingUserTest()
        {
        
            string caseCode = "EHC";
            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            using (CaseService svc = new CaseService())
            {
                var emp = "000000000000000000000001";
                Assert.IsNotNull(emp);
            

                User actualuser = User.GetById("000000000000000000000001");

                var newCase = svc.CreateCase(CaseStatus.Open, emp, DateTime.Today, DateTime.Today.AddDays(15), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code, description: "CaseServiceTest.CreateCase");
                Assert.IsNotNull(newCase);
                newCase = svc.UpdateCase(newCase);
                Assert.IsNotNull(newCase.Id);
           
                var tdi = new ToDoService().CreateCaseReviewTodo(newCase.Id, "TEST").Id;
                newCase.Status = CaseStatus.Cancelled;
                newCase.AssignedTo = actualuser;
                newCase.Save();
                newCase.WfOnCaseCanceled(actualuser);

                // Test ToDo status
                var todo = ToDoItem.GetById(tdi);
                todo.Should().NotBeNull();
                todo.Status.Should().Be(ToDoItemStatus.Cancelled);
                todo.ResultText.Should().Be("CASE CANCELLED");
                // Assign the Todo to a another user


                User assignToUser = User.GetById("000000000000000000000002");
                todo.AssignedToId = assignToUser.Id;
                todo.Save();

                //Soft Delete user 

                actualuser.IsDeleted = true;
                actualuser.Save();

                // Check for the todo that moved just 
                var criteria = new ListCriteria();
                criteria.PageNumber = 1;
                criteria.PageSize = 1;
                criteria.SortBy = "DueDate";
                criteria.SortDirection = SortDirection.Ascending;

                var todos = new ToDoService().ToDoItemList(assignToUser, criteria);

                todos.Should().NotBeNull();


                // remove the soft Delete from the user
                actualuser.IsDeleted = false;
                actualuser.Save();

            }
        }
    }
}
