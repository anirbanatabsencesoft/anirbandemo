﻿using System;
using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Logic.Cases;
using System.Collections.Generic;
using AbsenceSoft.Data;
using MongoDB.Bson.Serialization.Attributes;
using System.IO;
using System.Text;
using AbsenceSoft.Data.Cases;

namespace AbsenceSoft.Tests.Rendering
{
    [TestClass]
    public class DataCatalogGeneratorTests
    {
        [TestMethod]
        public void TestDataCatalogGenerator()
        {
            List<string> tokens = new List<string>();

            Action<string, Type> tokeniZer = null;
            tokeniZer = new Action<string, Type>((parentPropertyName, type) =>
            {

                var props = type.GetProperties(BindingFlags.FlattenHierarchy | BindingFlags.Instance | BindingFlags.Public).OrderBy(p => p.Name);
                foreach (var p in props)
                {
                    string newName = string.Concat(parentPropertyName ?? "", string.IsNullOrWhiteSpace(parentPropertyName) ? "" : ".", p.Name);
                    string prefix = "";
                    foreach (var c in (newName ?? ""))
                    {
                        if (c == ' ')
                            prefix += c;
                        else
                            break;
                    }
                  
                    if (p.PropertyType.IsGenericType && p.PropertyType.GetInterfaces().Any(i => i.GetGenericTypeDefinition() == typeof(IEnumerable<>)))
                    {
                    
                       if (p.IsDefined(typeof(BsonIgnoreAttribute)))
                        {
                            tokens.Add(string.Concat(prefix, "{{", newName.Trim(), ".*}}"));
                        }
                        else
                        {
                            // We have a collection of, let's enumerate
                            tokens.Add(string.Concat(prefix, "{{#each ", newName, "}}"));
                            tokeniZer(string.Concat(prefix, "    "), p.PropertyType.GetGenericArguments().First());
                            tokens.Add(string.Concat(prefix, "{{/each}}"));
                        }
                    }
                    else if (!p.PropertyType.Namespace.StartsWith("System") && p.PropertyType.BaseType != null &&
                        ((p.PropertyType.BaseType.IsGenericType && p.PropertyType.BaseType.GetGenericTypeDefinition() == typeof(BaseEntity<>))
                        || (p.PropertyType.IsClass && (!p.PropertyType.IsGenericType || p.PropertyType.GetGenericTypeDefinition() != typeof(Nullable<>)))))
                    {
                        if (p.IsDefined(typeof(BsonIgnoreAttribute)))
                            tokens.Add(string.Concat(prefix, "{{", newName.Trim(), ".*}}"));
                        else
                            // Complex type, object or class definition, we need to build the nested tokens
                            tokeniZer(newName, p.PropertyType);
                    }
                    else
                        tokens.Add(string.Concat(prefix, "{{", newName.Trim(), "}}"));
                }

            });
            tokeniZer(null, typeof(TemplateRenderModel));

            Assert.IsNotNull(tokens);

            StringBuilder doc = new StringBuilder();
            using (StringWriter writer = new StringWriter(doc))
                tokens.ForEach(writer.WriteLine);

            string documentation = doc.ToString();
            Assert.IsNotNull(documentation);
        }
    }
}
