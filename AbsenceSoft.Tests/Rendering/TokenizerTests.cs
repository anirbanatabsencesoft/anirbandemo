﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Rendering.Templating.MailMerge;
using AbsenceSoft.Tests.Rendering.TestResources;
using Aspose.Words;
using FluentAssertions;

namespace AbsenceSoft.Tests.Rendering
{
    [TestClass]
    public class TokenizerTests
    {
        [TestMethod]
        public void PlainTextWorksWithDoubleAngleBrackets()
        {
            string caseNumber = "123A";
            string employeeNumber = "12345";

            string token = "Case #«Case.CaseNumber» has been created for Employee «Employee.EmployeeNumber»";
            MailMergeData mailMergeData = new MailMergeData(){
                Case = new CaseMergeData(){
                    CaseNumber = caseNumber
                },
                Employee = new EmployeeMergeData(){
                    EmployeeNumber = employeeNumber
                }
            };

            string renderedToken = AbsenceSoft.Rendering.Template.RenderTemplate(token, mailMergeData);
            renderedToken.Should().Be(string.Format("Case #{0} has been created for Employee {1}", caseNumber, employeeNumber));
        }

        [TestMethod]
        public void RenderWordTemplate_ExecutesFine()
        {
            MailMergeData mailMergeData = new MailMergeData()
            {
                Policies = new List<PolicyMergeData>(),
                RelatedPerson = new ContactMergeData()
                {
                    TypeName = "Spouse"
                }
            };

            mailMergeData.Policies.Add(new PolicyMergeData()
            {
                Code = "FMLA",
                Name = "Family and Medical Leave",
                Type = PolicyType.FMLA,
                TimeRemainingText = "12",
                Determination = AdjudicationSummaryStatus.Pending,
                TimeUsedText = "12",
                StartDate = DateTime.UtcNow,
                EndDate = DateTime.UtcNow,
                IneligibleReason = "Test",
                DeniedReason = "Test",
                Status = EligibilityStatus.Ineligible
            });


            byte[] renderedToken = AbsenceSoft.Rendering.Template.RenderWordTemplate(RenderingResources.PolicyDocument, SaveFormat.Docx, mailMergeData);

            MemoryStream memoryStream = new MemoryStream(renderedToken);
            Document document = new Document(memoryStream);
            document.Save("MergedPolicyDocument.docx", SaveFormat.Docx);

            memoryStream.Should().NotBeNull();
            memoryStream.Length.Should().BeGreaterThan(0);
        }
    }
}
