﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using AbsenceSoft.Rendering.Templating.MailMerge;
using Aspose.Words.MailMerging;
using System.Collections.Generic;

namespace AbsenceSoft.Tests.Rendering
{

    #region Test Classes
    class TestSubclass
    {
        public string SecondLevelString { get; set; }
        public List<CustomFieldMergeData> CustomFields { get; set; }
    }
    class TestClass
    {
        public string FirstLevelString { get; set; }
        public TestSubclass Sub { get; set; }
        public Flags Flags { get; set; }
        public List<CustomFieldMergeData> CustomFields { get; set; }
    }
    #endregion

    [TestClass]
    public class MailMergeDataSourceTests
    {

        #region Test Setup
        IMailMergeDataSource ds;
        const string MyFlag = "MyFlag";
        static object[] data = new object[]
        {
            new TestClass
            {
                Flags = new Flags(new[] { MyFlag}),
                CustomFields = new List<CustomFieldMergeData>
                {
                    new CustomFieldMergeData
                    {
                        Name = "MyCustomField1",
                        Value = "Value"
                    }
                },
                FirstLevelString = "FirstLevelString",
                Sub = new TestSubclass
                {
                    SecondLevelString = "SecondLevelString",
                    CustomFields = new List<CustomFieldMergeData>
                    {
                        new CustomFieldMergeData
                        {
                            Name="MyCustomField2",
                            Value="MyCustomField2"
                        }
                    }
                }
            }
        };

        [TestInitialize]
        public void TestInitialize()
        {

            ds = new MailMergeDataSource(data, "test");
            ds.MoveNext();
        }
        #endregion

        [TestMethod]
        public void GetFirstLevelStringTest()
        {
            GetTestValue("FirstLevelString").Should().Be("FirstLevelString");
        }

        [TestMethod]
        public void GetSecondLevelStringTest()
        {
            GetTestValue("Sub.SecondLevelString").Should().Be("SecondLevelString");
        }

        [TestMethod]
        public void GetCustomValueFieldTest()
        {
            GetTestValue("MyCustomField1").Should().Be("Value");
        }

        [TestMethod]
        public void GetNestedCustomValueFieldTest()
        {
            GetTestValue("Sub.MyCustomField2").Should().Be("MyCustomField2");
        }

        [TestMethod]
        public void GetFlagPropertyTest()
        {
            GetTestValue("Flags.MyFlag").Should().Be(true);
        }

        [TestMethod]
        public void GetNonExistentFieldValue()
        {
            GetTestValue("IShouldNotExist", false).Should().Be(null);
        }

        #region Helpers
        private object GetTestValue(string propertyPath, bool shouldResolve = true)
        {
            object fieldValue;
            ds.GetValue(propertyPath, out fieldValue).Should().Be(shouldResolve);
            return fieldValue;
        }
        #endregion
    }
}
