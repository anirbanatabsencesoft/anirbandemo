﻿using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Rendering;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using FluentAssertions;
using AbsenceSoft.Logic.Cases;
using Aspose.Pdf.Facades;
using System.Linq;
using Aspose.Pdf.Forms;

namespace AbsenceSoft.Tests.Rendering
{
    [TestClass]
    public class PdfTests
    {
        private readonly IList<string> testFileList;
        private readonly string testFilepath;
        public PdfTests()
        {
            testFileList = new List<string>();
            testFilepath = Environment.CurrentDirectory;
        }

        [TestMethod]
        public void RenderMultiplePdfDocumentsTest()
        {
            string path = Environment.CurrentDirectory;
            File.WriteAllBytes(Path.Combine(path, "pdf_1.pdf"), Pdf.ConvertHtmlToPdf("<p>Hi, this is some html!</p>"));
            File.WriteAllBytes(Path.Combine(path, "pdf_2.pdf"), Pdf.ConvertHtmlToPdf("<html><head><style type='text/css'>body { font-size: 3em; }</style></head><body><p>Really Big Text!</p><p style=\"font-size:50%;\"><a href=\"rootMe.html\">Rooted Hyperlink</a></p></body></html>"));
            File.WriteAllBytes(Path.Combine(path, "pdf_3.pdf"), Pdf.ConvertHtmlToPdf("<p>Wow, another Pdf document, go figure.</p>"));
            File.Delete(Path.Combine(path, "pdf_1.pdf"));
            File.Delete(Path.Combine(path, "pdf_2.pdf"));
            File.Delete(Path.Combine(path, "pdf_3.pdf"));
        }

        [TestMethod]
        public void JoinMultiplePdfDocumentsTest()
        {
            string path = Environment.CurrentDirectory;
            var pdf1 = Pdf.ConvertHtmlToPdf("<p>Hi, this is some html!</p>");
            var pdf2 = Pdf.ConvertHtmlToPdf("<html><head><style type='text/css'>body { font-size: 3em; }</style></head><body><p>Really Big Text!</p><p style=\"font-size:50%;\"><a href=\"rootMe.html\">Rooted Hyperlink</a></p></body></html>");
            var pdf3 = Pdf.ConvertHtmlToPdf("<p>Wow, another Pdf document, go figure.</p>");
            File.WriteAllBytes(Path.Combine(path, "pdf_4.pdf"), Pdf.MergePdfDocuments(pdf1, pdf2, pdf3));
            File.Delete(Path.Combine(path, "pdf_4.pdf"));
        }

        [TestMethod]
        public void OHSAForm301PdfStamper1()
        {
            Case myCase = new Case()
            {
                Id = "999990000000000000000001",
                CaseNumber = "111111111",
                Employee = new Employee()
                {
                    Id = "999990000000000000000001",
                    FirstName = "Charles",
                    MiddleName = "Lee (Chucky)",
                    LastName = "Ray",
                    DoB = new DateTime(1979, 2, 15),
                    HireDate = new DateTime(2009, 8, 16),
                    Gender = Gender.Male,
                    Info = new EmployeeInfo()
                    {
                        Address = new Address()
                        {
                            Address1 = "123 Child's Play Ave.",
                            Address2 = "Apt 456",
                            City = "Denver",
                            State = "CO",
                            PostalCode = "80021",
                            Country = "US"
                        }
                    }
                },
                WorkRelated = new WorkRelatedInfo()
                {
                    Classification = WorkRelatedCaseClassification.Death,
                    DateOfDeath = new DateTime(2054, 8, 23),
                    DaysAwayFromWork = 8,
                    DaysOnJobTransferOrRestriction = 30,
                    EmergencyRoom = true,
                    HealthCarePrivate = false,
                    HospitalizedOvernight = true,
                    IllnessOrInjuryDate = new DateTime(2016, 1, 6),
                    Provider = new EmployeeContact()
                    {
                        Contact = new Contact()
                        {
                            CompanyName = "St. Mt. Helens Health Screwing Facility",
                            FirstName = "Bob",
                            LastName = "Misterfancypanceporsche",
                            Title = "Dr.",
                            Address = new Address()
                            {
                                Address1 = "8974 Orange Grove Street",
                                City = "Somewhere Else",
                                State = "GA",
                                PostalCode = "45698",
                                Country = "US"
                            }
                        },
                        ContactTypeCode = "PROVIDER",
                        ContactTypeName = "Provider"
                    },
                    Reportable = true,
                    Sharps = false,
                    TimeEmployeeBeganWork = new TimeOfDay(8, 0, false),
                    TimeOfEvent = new TimeOfDay(9, 30, false),
                    TypeOfInjury = WorkRelatedTypeOfInjury.Injury,
                    InjuryOrIllness = "Busted head and lacerated eyeball, minor cuts and abraisions, missing brain pieces, a lobbed off ear, some broken toes, a broken pinky finger and of course death, then undeath, then some zombie like state of being.",
                    ActivityBeforeIncident = "They were carrying scissors and it was bad and they were running with those scissors and it was all pretty nightmarish you could hear their feet pounding in the warehouse and we all just knew the were going to die",
                    WhatHappened = "Well, the scissors came up and poked out thier eyeball and went into the dude's brain and it was a bloody mess but he was OK and we all laughed 'cause that goofy just always doing stuff like that and it's funny",
                    WhatHarmedTheEmployee = "Scissors mainly, but also a mop, the floor, his own ignorance, a washcloth some water that was left out on the floor, poison, chemicals and a host of other deadly things.",
                    WhereOccurred = "In the library, with the candlestick by Col. Mustard and Mrs. Peacock"
                }
            }.SetCreatedDate(new DateTime(2016, 5, 30));

            var pdf = PopulateOSHA301(myCase);
            pdf.Should().NotBeNullOrEmpty();
            //string path = GenerateOSHA301(myCase, "OSHA-Form301 (1).pdf");
        }

        [TestMethod]
        public void OHSAForm301PdfStamper2()
        {
            Case myCase = new Case()
            {
                Id = "999990000000000000000002",
                CaseNumber = "222222222",
                Employee = new Employee()
                {
                    Id = "999990000000000000000002",
                    FirstName = "Jennifer",
                    LastName = "Tilly",
                    DoB = new DateTime(1979, 2, 15),
                    HireDate = new DateTime(2009, 8, 16),
                    Gender = Gender.Female,
                    Info = new EmployeeInfo()
                    {
                        Address = new Address()
                        {
                            Address1 = "123 Child's Play Ave.",
                            Address2 = "Apt 456",
                            City = "Denver",
                            State = "CO",
                            PostalCode = "80021",
                            Country = "US"
                        }
                    }
                },
                WorkRelated = new WorkRelatedInfo()
                {
                    Classification = WorkRelatedCaseClassification.Death,
                    DateOfDeath = new DateTime(2054, 8, 23),
                    DaysAwayFromWork = 8,
                    DaysOnJobTransferOrRestriction = 30,
                    EmergencyRoom = false,
                    HealthCarePrivate = false,
                    HospitalizedOvernight = false,
                    IllnessOrInjuryDate = new DateTime(2016, 1, 6),
                    Provider = new EmployeeContact()
                    {
                        Contact = new Contact()
                        {
                            CompanyName = "St. Mt. Helens Health Screwing Facility",
                            FirstName = "Bob",
                            LastName = "Misterfancypanceporsche",
                            Title = "Dr.",
                            Address = new Address()
                            {
                                Address1 = "8974 Orange Grove Street",
                                City = "Somewhere Else",
                                State = "GA",
                                PostalCode = "45698",
                                Country = "US"
                            }
                        },
                        ContactTypeCode = "PROVIDER",
                        ContactTypeName = "Provider"
                    },
                    Reportable = true,
                    Sharps = false,
                    TimeEmployeeBeganWork = new TimeOfDay(8, 0, false),
                    TimeOfEvent = null,
                    TypeOfInjury = WorkRelatedTypeOfInjury.Injury,
                    InjuryOrIllness = "Busted head and lacerated eyeball, minor cuts and abraisions, missing brain pieces, a lobbed off ear, some broken toes, a broken pinky finger and of course death, then undeath, then some zombie like state of being.",
                    ActivityBeforeIncident = "They were carrying scissors and it was bad and they were running with those scissors and it was all pretty nightmarish you could hear their feet pounding in the warehouse and we all just knew the were going to die",
                    WhatHappened = "Well, the scissors came up and poked out thier eyeball and went into the dude's brain and it was a bloody mess but he was OK and we all laughed 'cause that goofy just always doing stuff like that and it's funny",
                    WhatHarmedTheEmployee = "Scissors mainly, but also a mop, the floor, his own ignorance, a washcloth some water that was left out on the floor, poison, chemicals and a host of other deadly things.",
                    WhereOccurred = "In the library, with the candlestick by Col. Mustard and Mrs. Peacock"
                }
            }.SetCreatedDate(new DateTime(2016, 5, 30));

            var pdf = PopulateOSHA301(myCase);
            pdf.Should().NotBeNullOrEmpty();
            //string path = GenerateOSHA301(myCase, "OSHA-Form301 (2).pdf");
        }

        /// <summary>
        /// Generates the osh a301.
        /// </summary>
        /// <param name="myCase">My case.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        private string GenerateOSHA301(Case myCase, string fileName)
        {
            string path = Path.Combine(Environment.CurrentDirectory, fileName);
            if (File.Exists(path))
                File.Delete(path);

            var pdf = PopulateOSHA301(myCase);

            File.WriteAllBytes(path, pdf);
            //File.Delete(path);
            return path;
        }

        /// <summary>
        /// Populates the OSHA Form 301 document PDF and returns the resulting document.
        /// </summary>
        /// <param name="myCase">My case.</param>
        /// <returns></returns>
        private byte[] PopulateOSHA301(Case myCase)
        {
            var pdf = TestResources.RenderingResources.OSHA301;

            // Each stamp is 24pt high (per line)
            pdf = Pdf.StampPdf(pdf, new WorkRelatedService().Using(s => s.BuildOSHAForm301Stamps(myCase)));

            return pdf;
        }

        [TestMethod]
        public void TestPopulateCurrentServiceMemberInjuryIllnessWithData()
        {
            Dictionary<string, string> values = new Dictionary<string, string>
            {
                { "servicemember", "St. Mt. Helens Health Screwing Facility" },
                { "Name of Employee Requesting Leave to Care for the Current Servicemember", "Bob Misterfancypanceporsche" },
                { "Name of the Current Servicemember for whom employee is requesting leave to care", "Jennifer Tittly" },
                { "Spouse", "On" },
                { "Date", DateTime.Now.ToLongDateString() }
            };

            var resource = TestResources.RenderingResources.DOL_FMLA_Certification_CurrentServiceMemberInjuryIllness;
            string fileName = Path.Combine(testFilepath, "DOL_FMLA_Certification_CurrentServiceMemberInjuryIllness.pdf");
            File.WriteAllBytes(fileName, Pdf.FillPdf(resource, values));
            testFileList.Add(fileName);
            ValidatePdfFile(fileName);
            ValidatePopulatedField(fileName, values);
        }

        [TestMethod]
        public void TestPopulateEmployeeHealthCertificationWithData()
        {
            Dictionary<string, string> values = new Dictionary<string, string>
            {
                { "txtEmployerName", "St. Mt. Helens Health Screwing Facility" },
                { "txtEmployeeTitle", "Sr. Manager" },
                { "txtWorkSchedule", "Regular" },
                { "chxAttached", "Yes" },
                { "txtYourName", "Shyam Krishna" },
                { "txtProviderNameAddress", "8974 Orange Grove Street,Somewhere Else,GA" },
                { "txtPhoneAreaCode", "01" },
                { "txtPhoneNumber", "1234578951" },
                { "txtFaxAreaCode", "01" },
                { "txtFaxNumber", "2535455844" },
                { "chxOvernighStay", "Yes" },
                { "txtDateSigned", DateTime.Now.ToLongDateString() }
            };

            var resource = TestResources.RenderingResources.DOL_FMLA_Certification_EmployeeHealth;
            string fileName = Path.Combine(testFilepath, "DOL_FMLA_Certification_EmployeeHealth.pdf");
            File.WriteAllBytes(fileName, Pdf.FillPdf(resource, values));
            testFileList.Add(fileName);
            ValidatePdfFile(fileName);
            ValidatePopulatedField(fileName, values);
        }

        [TestMethod]
        public void TestPopulateFamilyHealthCertificationWithData()
        {
            Dictionary<string, string> values = new Dictionary<string, string>
            {
                { "txtNameContact", "St. Mt. Helens Health Screwing Facility" },
                { "txtNameContact2", "2535455844" },
                { "txtYourName", "Frank Dikinson" },
                { "txtNameOfFamilyMember", "Debi Dikinson" },
                { "txtRelationship", "Spouse" },
                { "chxOvernightStay", "Yes" },
                { "chxEpisodic", "No" },
                { "txtDateSigned", DateTime.Now.ToLongDateString() }
            };

            var resource = TestResources.RenderingResources.DOL_FMLA_Certification_FamilyHealth;
            string fileName = Path.Combine(testFilepath, "DOL_FMLA_Certification_FamilyHealth.pdf");
            File.WriteAllBytes(fileName, Pdf.FillPdf(resource, values));
            testFileList.Add(fileName);
            ValidatePdfFile(fileName);
            ValidatePopulatedField(fileName, values);
        }

        [TestMethod]
        public void TestPopulateQualifyingExigencyCertificationWithData()
        {
            Dictionary<string, string> values = new Dictionary<string, string>
            {
                { "Employer name", "St. Mt. Helens Health Screwing Facility" },
                { "Contact Information", "8974 Orange Grove Street" },
                { "Your Name", "Ken Skillman" },
                { "First", "Walter" },
                { "Middle", "D'cruse" },
                { "Last", "Sawnson" },
                { "Check Box1", "Yes" },
                { "Will you need to be absent from work for a single continuous period of time due to the qualifying exigency", "Yes_2" },
                { "Date", DateTime.Now.ToLongDateString() }
            };

            var resource = TestResources.RenderingResources.DOL_FMLA_Certification_QualifyingExigency;
            string fileName = Path.Combine(testFilepath, "DOL_FMLA_Certification_QualifyingExigency.pdf");
            File.WriteAllBytes(fileName, Pdf.FillPdf(resource, values));
            testFileList.Add(fileName);
            ValidatePdfFile(fileName);
            ValidatePopulatedField(fileName, values);
        }

        [TestMethod]
        public void TestPopulateVeteranInjuryIllnessCertificationWithData()
        {
            Dictionary<string, string> values = new Dictionary<string, string>
            {
                { "Name and address of employer this is the employer of the employee requesting leave to care for a veteran", "St. Mt. Helens Health Screwing Facility" },
                { "Name of employee requesting leave to care for a veteran", "Ken Skillman" },
                { "Name of veteran for whom employee is requesting leave", "Walter Jackson" },
                { "Parent", "On" },
                { "Please provide the veteran's military brach, rank and unit at the time of discharge", "Major General" },
                { "Is the veteran receiving medical treatment recuperation or therapy for an injury or illness", "Yes" },
                { "Describe the care to be provided to the veteran and an estimate of the leave needed to provide the care 1", "Full Day Rest" },
                { "Date of the Veteran's discharge", DateTime.Now.ToLongDateString() }
            };

            var resource = TestResources.RenderingResources.DOL_FMLA_Certification_VeteranInjuryIllness;
            string fileName = Path.Combine(testFilepath, "DOL_FMLA_Certification_VeteranInjuryIllness.pdf");
            File.WriteAllBytes(fileName, Pdf.FillPdf(resource, values));
            testFileList.Add(fileName);
            ValidatePdfFile(fileName);
            ValidatePopulatedField(fileName, values);
        }

        /// <summary>
        /// Validate PDF file is generated and not blank.
        /// </summary>
        /// <param name="fileName"></param>
        private void ValidatePdfFile(string fileName)
        {
            Assert.IsTrue(File.Exists(fileName));
            Assert.IsTrue(new FileInfo(fileName).Length != 0);
        }

        /// <summary>
        /// Validate the Populated fields
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="values"></param>
        private void ValidatePopulatedField(string filePath, Dictionary<string, string> values)
        {
            using (var form = new Aspose.Pdf.Facades.Form(filePath))
            {
                var fields = form.Document.Form.Fields;
                foreach (var item in values)
                {
                    var field = fields.FirstOrDefault(e => e.FullName == item.Key);
                    if (field != null && field is TextBoxField)
                    {
                        Assert.AreEqual(item.Value, field.Value);
                    }
                }
            }

        }


        /// <summary>
        /// Clean up all the files generated in test
        /// </summary>
        [TestCleanup]
        public void DeleteAllCreatedFiles()
        {
            foreach (var item in testFileList)
            {
                if (File.Exists(item))
                {
                   File.Delete(item);
                }
            }
        }
    }
}
