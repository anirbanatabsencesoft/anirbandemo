﻿using Aspose.Words;
using AbsenceSoft.Rendering;
using AbsenceSoft.Rendering.Templating.MailMerge;
using communicationData = AbsenceSoft.Data.Communications;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Customers;
using System.Text.RegularExpressions;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Enums;
using FluentAssertions;

namespace AbsenceSoft.Tests.Rendering
{
    [TestClass]
    public class WordCompilerTester
    {
        private readonly int BaseExpectedMergeFields = 602;
        [TestMethod]        
        public void CreateGenericWordDocumentTest()
        {
            int expectedMergeFields = BaseExpectedMergeFields+5
                + Template.GetExhaustedFlags().Count();
            Document d = null;
            using (Stream s = Template.GenerateTemplate())
            {
                s.Position = 0;
                d = new Document(s);
            }

            string[] fieldNames = d.MailMerge.GetFieldNames();

            // count for DeniedReasonCode in mailmerge
            expectedMergeFields += Template.GetDeniedReasonFlags(fieldNames).Count();

            Assert.IsTrue(fieldNames.Length == expectedMergeFields, string.Format("Expected {0} merge fields. Found {1}", expectedMergeFields, fieldNames.Length));
        }

        [TestMethod]
        public void CreateEmployerSpecificWordDocumentTest()
        {
            int expectedMergeFields = BaseExpectedMergeFields + 7   

                + Template.GetExhaustedFlags().Count();
            string employeeCustomLabel = "This is an employee custom field";
            string caseCustomLabel = "This is an employee custom field";
            List<CustomField> customFields = new List<CustomField>();
            CustomField employeeCustomField = new CustomField()
            {
                Name = employeeCustomLabel,
                Label = employeeCustomLabel,
                Target = AbsenceSoft.Data.Enums.EntityTarget.Employee
            };
            CustomField caseCustomField = new CustomField()
            {
                Name = caseCustomLabel,
                Label = caseCustomLabel,
                Target = AbsenceSoft.Data.Enums.EntityTarget.Case
            };
            customFields.Add(employeeCustomField);
            customFields.Add(caseCustomField);

            Document d = null;
            using (Stream s = Template.GenerateTemplate(customFields))
            {
                s.Position = 0;
                d = new Document(s);
            }

            string[] fieldNames = d.MailMerge.GetFieldNames();
            string employeeCustomFieldName = string.Format("Employee.{0}", employeeCustomLabel);
            string caseCustomFieldName = string.Format("Case.{0}", caseCustomLabel);
            Assert.IsTrue(fieldNames.Contains(employeeCustomFieldName), string.Format("Expected {0} in merge fields, but was not found", employeeCustomFieldName));
            Assert.IsTrue(fieldNames.Contains(caseCustomFieldName), string.Format("Expected {0} in merge fields, but was not found", caseCustomFieldName));

            // count for DeniedReasonCode in mailmerge
            expectedMergeFields += Template.GetDeniedReasonFlags(fieldNames).Count();

            Assert.IsTrue(fieldNames.Length == expectedMergeFields, string.Format("Expected {0} merge fields.  Found {1}", expectedMergeFields, fieldNames.Length));
        }

        [TestMethod]
        public void BlankDocumentHasNoMergeFields()
        {
            MailMergeData data = new MailMergeData();
            using (Stream s = Template.GenerateTemplate())
            {
                using (Stream final = Template.RenderWordTemplate(s, SaveFormat.Docx, data))
                {
                    Document doc = new Document(final);
                    Assert.IsTrue(doc.MailMerge.GetFieldNames().Count() == 0, "Merged document has empty mail merge fields");
                }
            }
        }

        [TestMethod]
        public void OutstandingPaperworkIsRepeatedTest()
        {
            MailMergeData data = new MailMergeData();
            List<PaperworkMergeData> outstandingPaperwork = new List<PaperworkMergeData>();
            string adaPaperworkRemaining = "ADA Accomodation Received";
            DateTime adaPaperworkDue = new DateTime(2015, 1, 1);
            PaperworkMergeData adaOutstandingPaperwork = new PaperworkMergeData()
            {
                Name = adaPaperworkRemaining,
                DueDate = adaPaperworkDue
            };

            string fmlaPaperworkRemaining = "FMLA Doctor's Approval";
            DateTime fmlaPaperworkDue = new DateTime(2015, 2, 2);

            PaperworkMergeData fmlaOutstandingPaperwork = new PaperworkMergeData()
            {
                Name = fmlaPaperworkRemaining,
                DueDate = fmlaPaperworkDue
            };

            outstandingPaperwork.Add(adaOutstandingPaperwork);
            outstandingPaperwork.Add(fmlaOutstandingPaperwork);
            data.OutstandingPaperwork = outstandingPaperwork;
            string compiledHtml = null;
            using (Stream s = Template.GenerateTemplate())
            {
                compiledHtml = Template.RenderTemplateToHTML(s, data);
            }
            Assert.IsNotNull(compiledHtml);
        }

        [TestMethod]
        public void ContactsAreRepeatedTest()
        {
            List<ContactMergeData> contacts = new List<ContactMergeData>();
            ContactMergeData supervisor = new ContactMergeData()
            {
                FirstName = "John",
                LastName = "Doe",
                Email = "johndoe@absencesoft.com",
                WorkPhone = "5555555555"
            };

            ContactMergeData hr = new ContactMergeData()
            {
                FirstName = "Jane",
                LastName = "Smith",
                Email = "janesmith@absencesoft.com",
                WorkPhone = "1111111111"
            };

            contacts.Add(supervisor);
            contacts.Add(hr);

            MailMergeData data = new MailMergeData();
            data.AdminContacts = contacts;
            string compiledHtml = null;
            using (Stream s = Template.GenerateTemplate())
            {
                compiledHtml = Template.RenderTemplateToHTML(s, data);
            }
            Assert.IsNotNull(compiledHtml);
        }

        [TestMethod]
        public void CustomFieldValuesAreFound()
        {
            Case c = null;
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            AbsenceReason ar = AbsenceReason.GetByCode(caseCode);
            using (var caseService = new CaseService())
            {
                c = caseService.CreateCase(CaseStatus.Open, empId, DateTime.UtcNow, DateTime.UtcNow.AddDays(14), CaseType.Consecutive, ar.Code);
            }

            Employee e = Employee.GetById(c.Employee.Id);
            CustomField cf = new CustomField()
            {
                Name = "PTO",
                SelectedValue = "Test"
            };
            e.CustomFields.Add(cf);

            MailMergeData data = new MailMergeData();
            data.CopyCaseData(c, e, null, null, null, null);
            data.CopyEmployeeData(e, null, null, null, c, null, null);
            MailMergeDataSource dataSource = new MailMergeDataSource(new List<MailMergeData> { data }, "MergeTest");
            dataSource.MoveNext();
            object customFieldValue = null;
            dataSource.GetValue("Employee.PTO", out customFieldValue);
            Assert.IsNotNull(customFieldValue);
            customFieldValue.Should().Be("Test");
        }

        [TestMethod]
        public void ExtraWhitespaceGetsNuked()
        {
            Case.Delete(Case.Query.EQ(r => r.Employee.Id, "000000000000000000000003"));
            Case c = null;
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            AbsenceReason ar = AbsenceReason.GetByCode(caseCode);
            LeaveOfAbsence loa = null;
            using (var caseService = new CaseService())
            using (var eligService = new EligibilityService())
            {
                c = caseService.CreateCase(CaseStatus.Open, empId, DateTime.UtcNow, DateTime.UtcNow.AddDays(14), CaseType.Consecutive, ar.Code);
                loa = eligService.RunEligibility(c);
                c = loa.Case;
            }

            MailMergeData data = new MailMergeData();
            data.CopyCaseData(c, c.Employee, null, null, null, null);

            using (Stream docStream = new MemoryStream(TestResources.RenderingResources.Eligibility_and_Rights_and_Responsibilities_Notice))
            {
                var html = AbsenceSoft.Rendering.Template.RenderTemplateToHTML(docStream, data);
                html.Should().NotBeNullOrWhiteSpace();
                //if (File.Exists(@"C:\Users\Chad Scharf\Desktop\doc.html"))
                //    File.Delete(@"C:\Users\Chad Scharf\Desktop\doc.html");
                //File.WriteAllText(@"C:\Users\Chad Scharf\Desktop\doc.html", html);
                var pdf = Pdf.ConvertHtmlToPdf(html);
                pdf.Should().NotBeNullOrEmpty();
                //if (File.Exists(@"C:\Users\Chad Scharf\Desktop\doc.pdf"))
                //    File.Delete(@"C:\Users\Chad Scharf\Desktop\doc.pdf");
                //File.WriteAllBytes(@"C:\Users\Chad Scharf\Desktop\doc.pdf", pdf);
            }
        }

        [TestMethod]
        public void ExtraWhitespaceGetsNukedDuex()
        {
            Case.Delete(Case.Query.EQ(r => r.Employee.Id, "000000000000000000000003"));
            Case c = null;
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            AbsenceReason ar = AbsenceReason.GetByCode(caseCode);
            LeaveOfAbsence loa = null;
            using (var caseService = new CaseService(false))
            using (var eligService = new EligibilityService())
            {
                c = caseService.CreateCase(CaseStatus.Open, empId, DateTime.UtcNow, DateTime.UtcNow.AddDays(14), CaseType.Consecutive, ar.Code);
                loa = eligService.RunEligibility(c);
                c = loa.Case;
                caseService.UpdateCase(c, CaseEventType.CaseCreated);
                c = caseService.ApplyDetermination(c, null, c.StartDate, c.EndDate.Value, AdjudicationStatus.Approved);
                c = caseService.UpdateCase(c);
            }

            MailMergeData data = new MailMergeData();
            data.CopyCaseData(c, c.Employee, null, null, null, null);

            using (Stream docStream = new MemoryStream(TestResources.RenderingResources.Designation_Notice_Leave_Approved))
            {
                var html = AbsenceSoft.Rendering.Template.RenderTemplateToHTML(docStream, data);
                html.Should().NotBeNullOrWhiteSpace();
                //if (File.Exists(@"C:\Users\Chad Scharf\Desktop\doc.html"))
                //    File.Delete(@"C:\Users\Chad Scharf\Desktop\doc.html");
                //File.WriteAllText(@"C:\Users\Chad Scharf\Desktop\doc.html", html);
                var pdf = Pdf.ConvertHtmlToPdf(html);
                pdf.Should().NotBeNullOrEmpty();
                //if (File.Exists(@"C:\Users\Chad Scharf\Desktop\doc.pdf"))
                //    File.Delete(@"C:\Users\Chad Scharf\Desktop\doc.pdf");
                //File.WriteAllBytes(@"C:\Users\Chad Scharf\Desktop\doc.pdf", pdf);
            }
        }


        [TestMethod, Ignore]
        public void TestTimeUsedAndTimeRemaining()
        {
            Case c = CreateCaseForMailMergeTest();

            var listPolicies = new CommunicationService().GetPolicyUtilizationSummary(c);
            MailMergeData data = new MailMergeData();
            data.CopyCaseData(c, null, null, null, null, null);
            data.UpdatePolicyUtilization(listPolicies);

            Assert.IsTrue(data.Case.FMLAPolicy.TimeUsedText.Contains("0.00 weeks"));
            Assert.IsTrue(data.Case.FMLAPolicy.TimeRemainingText.Contains("12.00 weeks"));
            Assert.IsTrue(data.Case.FMLAPolicy.DeniedReason.Contains(AdjudicationDenialReason.Exhausted.ToString()));
        }

        [TestMethod]
        public void TestPaperworkDueDateReminder()
        {
            Case c = CreateCaseForMailMergeTest();

            List<communicationData.CommunicationPaperwork> allDuePaperwork = new List<communicationData.CommunicationPaperwork>()
            {
                new communicationData.CommunicationPaperwork(){ DueDate = new DateTime(2017, 10, 15), Status = PaperworkReviewStatus.Pending, Paperwork = new communicationData.Paperwork(){ Name = "Test Due Paperwork 1" } },
                new communicationData.CommunicationPaperwork(){ DueDate = new DateTime(2017, 10, 30), Status = PaperworkReviewStatus.Incomplete, Paperwork = new communicationData.Paperwork(){ Name = "Test Due Paperwork 2" } },
                new communicationData.CommunicationPaperwork(){ DueDate = new DateTime(2017, 11, 14), Status = PaperworkReviewStatus.NotReceived, Paperwork = new communicationData.Paperwork(){ Name = "Test Due Paperwork 3" } }
            };

            MailMergeData data = new MailMergeData();
            data.CopyCaseData(c, null, null, null, null, null);
            data.CopyDuePaperworkData(allDuePaperwork, new DateTime(2017, 9, 15), new DateTime(2017, 9, 16));

            Assert.AreEqual(data.AllPendingPaperworkDue.Count, 3);
            Assert.AreEqual(data.AllPendingPaperworkDue[0].Name, "Test Due Paperwork 1");
            Assert.AreEqual(data.AllPendingPaperworkDue[0].DueDate, new DateTime(2017, 10, 15));
            Assert.AreEqual(data.AllPendingPaperworkDue[0].FirstCasePaperworkDueDate, new DateTime(2017, 10, 15));
            Assert.AreEqual(data.AllPendingPaperworkDue[0].LastCasePaperworkDueDate, new DateTime(2017, 9, 16));
            Assert.AreEqual(data.AllPendingPaperworkDue[1].Name, "Test Due Paperwork 2");
            Assert.AreEqual(data.AllPendingPaperworkDue[1].DueDate, new DateTime(2017, 10, 30));
            Assert.AreEqual(data.AllPendingPaperworkDue[1].FirstCasePaperworkDueDate, new DateTime(2017, 10, 30));
            Assert.AreEqual(data.AllPendingPaperworkDue[1].LastCasePaperworkDueDate, new DateTime(2017, 9, 16));
            Assert.AreEqual(data.AllPendingPaperworkDue[2].Name, "Test Due Paperwork 3");
            Assert.AreEqual(data.AllPendingPaperworkDue[2].DueDate, new DateTime(2017, 11, 14));
            Assert.AreEqual(data.AllPendingPaperworkDue[2].FirstCasePaperworkDueDate, new DateTime(2017, 11, 14));
            Assert.AreEqual(data.AllPendingPaperworkDue[2].LastCasePaperworkDueDate, new DateTime(2017, 9, 16));
        }


        private static Case CreateCaseForMailMergeTest()
        {
            string caseCode = "EHC";
            string empId = "000000000000000000000003";
            Policy policy = Policy.GetByCode("FMLA");
            policy.PolicyBehaviorType = PolicyBehavior.ReducesWorkWeek;
            policy.Save();
            PolicyAbsenceReason policyAbsenceReason = policy.AbsenceReasons.FirstOrDefault(a => a.Reason.Code == caseCode);
            policyAbsenceReason.PeriodType = PeriodType.CalendarYear;
            AbsenceReason ar = AbsenceReason.GetByCode(caseCode);
            Employee emp = Employee.GetById(empId);
            Case c = new Case()
            {
                Status = CaseStatus.Open,
                Employee = emp,
                EmployerId = emp.EmployerId,
                CustomerId = emp.CustomerId,
                Reason = ar,
                StartDate = DateTime.UtcNow,
                EndDate = DateTime.UtcNow.AddDays(14),
                Summary = new CaseSummary()
                {
                    Policies = new List<CaseSummaryPolicy>()
                    {
                       new CaseSummaryPolicy()
                       {
                           PolicyCode =policy.Code,
                           PolicyName = policy.Name,
                           Detail =  new List<CaseSummaryPolicyDetail> ()
                           {
                               new CaseSummaryPolicyDetail()
                               {
                                      Determination = AdjudicationSummaryStatus.Approved,
                                      StartDate = DateTime.UtcNow,
                                      EndDate = DateTime.UtcNow.AddDays(14),
                               }
                           }
                       }
                    }
                },
                Segments = new List<CaseSegment>()
                {
                     new CaseSegment()
                    {
                       StartDate = DateTime.UtcNow,
                       EndDate = DateTime.UtcNow.AddDays(14),
                       Type = CaseType.Consecutive,
                       Status = CaseStatus.Open,
                       AppliedPolicies =  new List<AppliedPolicy>()
                       {
                           new AppliedPolicy()
                           {
                               StartDate = DateTime.UtcNow,
                               EndDate = DateTime.UtcNow.AddDays(14),
                               Status =EligibilityStatus.Eligible,
                               Policy = policy,
                               PolicyReason = policyAbsenceReason,
                              Usage = new List<AppliedPolicyUsage>()
                              {
                                  new AppliedPolicyUsage()
                                  {
                                      DateUsed =DateTime.UtcNow,
                                      MinutesUsed =480,
                                      MinutesInDay=480,
                                      Determination =AdjudicationStatus.Approved,
                                      PolicyReasonCode =ar.Code
                                  },
                                    new AppliedPolicyUsage()
                                  {
                                      DateUsed =DateTime.UtcNow.AddDays(1),
                                      MinutesUsed =480,
                                      MinutesInDay=480,
                                      Determination =AdjudicationStatus.Denied,
                                      DenialReasonCode = AdjudicationDenialReason.Exhausted,
                                      PolicyReasonCode =ar.Code
                                  }
                              }
                           }
                       }
                     }
                }
            };
            return c;
        }


        [TestMethod]
        public void TestIneligibleReason()
        {
            string policyCode = "FMLA";
            string caseCode = "EHC";
            string ineligibleReason = "Test Ineligiblity Reason";
            Case c = CreateCase(policyCode, caseCode, CaseType.Consecutive, ineligibleReason);
            MailMergeData data = new MailMergeData();
            data.CopyCaseData(c, null, null, null, null, null);
            Assert.IsNotNull(data.Case.FMLAPolicy.IneligibleReason);
            Assert.AreEqual(data.Case.FMLAPolicy.IneligibleReason, ineligibleReason + " FAILED, was true");
        }

        [TestMethod]
        public void TestDenialReason()
        {
            string policyCode = "FMLA";
            string caseCode = "EHC";
            string testReason = "Test Reason";
            string ineligibleReason = "Test Ineligiblity Reason";
            Case c = CreateCase(policyCode, caseCode, CaseType.Intermittent, ineligibleReason);
            c.Segments[0].UserRequests = new List<IntermittentTimeRequest>()
            {
                new IntermittentTimeRequest()
                {
                    Detail = new List<IntermittentTimeRequestDetail>()
                    {

                        new IntermittentTimeRequestDetail()
                        {
                            PolicyCode = policyCode,
                            Pending = 0,
                            Approved = 0,
                            Denied = 480,
                            DenialReasonCode = AdjudicationDenialReason.Other,
                            DenialReasonName = AdjudicationDenialReason.OtherDescription,
                            DeniedReasonOther = testReason
                        },
                        new IntermittentTimeRequestDetail()
                        {
                            PolicyCode = policyCode,
                            Pending = 0,
                            Approved = 0,
                            Denied = 480,
                            DenialReasonCode = AdjudicationDenialReason.NotASeriousHealthCondition,
                            DenialReasonName = AdjudicationDenialReason.NotASeriousHealthConditionDescription,
                            DeniedReasonOther = null
                        },
                        new IntermittentTimeRequestDetail()
                        {
                            PolicyCode = policyCode,
                            Pending = 0,
                            Approved = 480,
                            Denied = 0,
                        }

                    }
                }
            };
            MailMergeData data = new MailMergeData();
            data.CopyCaseData(c, null, null, null, null, null);
            Assert.IsNotNull(data.TimeOffRequests);
            Assert.AreEqual(data.TimeOffRequests.Count, 3);
            Assert.AreEqual(data.TimeOffRequests[0].DeniedReasonName, AdjudicationDenialReason.Other.ToString() + " - " + testReason);
            Assert.AreEqual(data.TimeOffRequests[1].DeniedReasonName, AdjudicationDenialReason.NotASeriousHealthConditionDescription);
            Assert.IsNull(data.TimeOffRequests[2].DeniedReasonName);
        }

        /// <summary>
        /// CreateCase - Add required element into case but no delete
        /// </summary>
        private static Case CreateCase(string policyCode, string caseCode, CaseType caseType, string ineligibleReason)
        {
            Policy policy = Policy.GetByCode(policyCode);
            PolicyAbsenceReason policyAbsenceReason = policy.AbsenceReasons.FirstOrDefault(a => a.Reason.Code == caseCode);
            Case c = new Case()
            {
                Status = CaseStatus.Open,
                StartDate = DateTime.UtcNow,
                EndDate = DateTime.UtcNow.AddDays(14),
                Segments = new List<CaseSegment>()
                {
                     new CaseSegment()
                    {
                       StartDate = DateTime.UtcNow,
                       EndDate = DateTime.UtcNow.AddDays(14),
                       Type = caseType,
                       Status = CaseStatus.Open,
                       AppliedPolicies =  new List<AppliedPolicy>()
                       {
                           new AppliedPolicy()
                           {

                               StartDate = DateTime.UtcNow,
                               EndDate = DateTime.UtcNow.AddDays(14),
                               Status =EligibilityStatus.Ineligible,
                               Policy = policy,
                               PolicyReason = policyAbsenceReason,
                               RuleGroups = new List<AppliedRuleGroup>()
                               {
                                  new AppliedRuleGroup()
                                  {
                                       RuleGroup = policy.RuleGroups[0],
                                       Pass=false,
                                       Rules = new List<AppliedRule>()
                                       {
                                           new AppliedRule()
                                           {
                                                ActualValue ="true",
                                                Pass=false,
                                                Rule = new AbsenceSoft.Data.Rules.Rule(){ Description = ineligibleReason}
                                           }
                                       }
                                  }
                               }
                           }
                       }
                     }
                }
            };
            return c;
        }

        /// <summary>
        /// Test to check if employee's organization complete address gets captured 
        /// when CopyEmployee method is called 
        /// </summary>
        [TestMethod]
        public void SetOfficeLocationAddressTest()
        {
            string customerId = "000000000000000000000003";
            string employerId = "000000000000000000000003";
            string employeeNumber = "E0011";

            Employee employee = new Employee()
            {
                EmployerId = employerId,
                CustomerId = customerId,
                EmployeeNumber = employeeNumber,
                FirstName = "John",
                LastName = "Smith",
                Gender = Gender.Male,
                DoB = new DateTime(1950, 10, 20),
                HireDate = new DateTime(2010, 01, 02),
                ServiceDate = new DateTime(2010, 01, 15),
                OfficeLocationDisplayName = "CA001",
                Info = new EmployeeInfo() { Email = "john.smith@absencesoft.com" }
            }.Save();

            Organization organization = new Organization()
            {
                Name = "ORG",
                Path = "/HQ - Denver, CO/",
                Code = "org",
                TypeCode = "OFFICELOCATION",
                CustomerId = customerId,
                EmployerId = employerId,
                Address = new AbsenceSoft.Data.Address()
                {
                    Address1 = "Main Street 1",
                    Address2 = "Mind Space",
                    PostalCode = "10001",
                    City = "Manhattan",
                    State = "NY",
                    Country = "US"
                },
            }.Save();

            Case cAse = CreateCaseForMailMergeTest();
            cAse.Employee = employee;
            cAse.Save();

            EmployeeOrganization employeeOrganization = new EmployeeOrganization()
            {
                EmployerId = employerId,
                CustomerId = customerId,
                EmployeeNumber = cAse.Employee.EmployeeNumber,
                TypeCode = "OFFICELOCATION",
                Dates = new DateRange(new DateTime(2017, 01, 01)),
                Name = "CA001",
                Code = "10001",
                Organization = organization,
            }.Save();

            EmployeeMergeData employeeMergeData = new EmployeeMergeData();
            employeeMergeData.CopyEmployeeData(cAse.Employee, cAse);
            Assert.IsNotNull(employeeMergeData.OfficeLocationAddress);
            Assert.AreEqual(organization.Address.Address1, employeeMergeData.OfficeLocationAddress.Address1);
            Assert.AreEqual(organization.Address.Address2, employeeMergeData.OfficeLocationAddress.Address2);
            Assert.AreEqual(organization.Address.PostalCode, employeeMergeData.OfficeLocationAddress.PostalCode);
            Assert.AreEqual(organization.Address.City, employeeMergeData.OfficeLocationAddress.City);
            Assert.AreEqual(organization.Address.State, employeeMergeData.OfficeLocationAddress.State);
            Assert.AreEqual(organization.Address.Country, employeeMergeData.OfficeLocationAddress.Country);
        }
    }
}
