﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Tests
{
    internal static class TestUtil
    {
        public static object RunStaticMethod(Type type, string methodName, object[] methodParams)
        {
            BindingFlags eFlags = BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;
            return RunMethod(type, methodName, null, methodParams, eFlags);
        } //RunStaticMethod

        public static object RunInstanceMethod(Type type, string methodName, object instance, object[] methodParams)
        {
            BindingFlags eFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            return RunMethod(type, methodName, instance, methodParams, eFlags);
        } //RunInstanceMethod

        public static object RunMethod(this object instance, string methodName, params object[] methodParams)
        {
            BindingFlags eFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            return RunMethod(instance.GetType(), methodName, instance, methodParams, eFlags);
        } //RunMethod

        private static object RunMethod(Type t, string strMethod, object objInstance, object[] aobjParams, BindingFlags eFlags)
        {
            MethodInfo m;
            try
            {
                m = t.GetMethod(strMethod, eFlags);
                if (m == null)
                    throw new ArgumentException(string.Concat("There is no method '", strMethod, "' for type '", t.ToString(), "'."));

                object objRet = m.Invoke(objInstance, aobjParams);
                return objRet;
            }
            catch
            {
                throw;
            }
        } //end of method
    }
}
