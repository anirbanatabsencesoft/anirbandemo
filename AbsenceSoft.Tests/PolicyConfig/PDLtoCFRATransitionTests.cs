﻿using System;
using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Policies;
using System.Collections.Generic;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data;
using MongoDB.Driver;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.ToDo;

namespace AbsenceSoft.Tests.PolicyConfig
{
    [TestClass]
    public class PDLtoCFRATransitionTests
    {
        [TestMethod,Ignore]
        public void PDLtoCFRAExhaustionTest()
        {
            Employee emp = Employee.GetById("000000000000000000000024"); // Californian Pregnant Female
            emp.Should().NotBeNull();
            deleteTestCases(emp.Id);
            Case.Repository.Collection.Remove(Case.Query.EQ(c => c.Employee.Id, emp.Id), RemoveFlags.None);
            CaseService svc = new CaseService(false);
            EligibilityService elig = new EligibilityService();

            DateTime startDate = Date(2015, 1, 4); // Sunday, Jan 4
            DateTime endDate = Date(2015, 6, 6); // Saturday, Jun 6
            string reasonCode = AbsenceReason.GetByCode("PREGMAT", emp.CustomerId, emp.EmployerId).Code; // Pregnancy Maternity

            // Create the case
            Case testCase = svc.CreateCase(CaseStatus.Open, emp.Id, startDate, endDate, CaseType.Consecutive, reasonCode);
            // Run Eligibility/Policy Selection, etc.
            testCase = elig.RunEligibility(testCase).Case;
            // Remove all the noise other than the 2 policies we care about, PDL and CFRA
            testCase.Segments[0].AppliedPolicies.RemoveAll(ap => ap.Policy.Code != "CA-CPDL" && ap.Policy.Code != "CA-CFRA");
            // For each of the remaining policies, ensure they're eligible
            testCase.Segments[0].AppliedPolicies.ForEach(ap => {
                ap.Status = EligibilityStatus.Eligible;
                ap.RuleGroups.ForEach(g => g.Rules.ForEach(r =>
                {
                    if (r.Result != AppliedRuleEvalResult.Pass)
                    {
                        r.OverrideFromResult = r.Result;
                        r.Overridden = true;
                        r.Result = AppliedRuleEvalResult.Pass;
                        r.OverrideValue = r.Rule.RightExpression;
                        r.OverrideNotes = "UNIT TEST";
                    }
                }));
            });
            // Re-run eligibility which also re-runs calcs 'n' stuff
            testCase = elig.RunEligibility(testCase).Case;

            // Get our 2 applied policies
            AppliedPolicy pdl = testCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "CA-CPDL");
            AppliedPolicy cfra = testCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "CA-CFRA");
            pdl.Should().NotBeNull();
            cfra.Should().NotBeNull();

            pdl.FirstExhaustionDate.Should().HaveValue().And.Be(Date(2015, 5, 2));
            cfra.Usage.Min(u => u.DateUsed).Should().Be(Date(2015, 5, 2));
        }

        [TestMethod,Ignore]
        public void PDLtoCFRASetEndDateTest()
        {
            Employee emp = Employee.GetById("000000000000000000000024"); // Californian Pregnant Female
            emp.Should().NotBeNull();
            deleteTestCases(emp.Id);
            Case.Repository.Collection.Remove(Case.Query.EQ(c => c.Employee.Id, emp.Id), RemoveFlags.None);
            CaseService svc = new CaseService(false);
            EligibilityService elig = new EligibilityService();

            DateTime startDate = Date(2015, 1, 4); // Sunday, Jan 4
            DateTime endDate = Date(2015, 6, 6); // Saturday, Jun 6
            string reasonCode = AbsenceReason.GetByCode("PREGMAT", emp.CustomerId, emp.EmployerId).Code; // Pregnancy Maternity

            // Create the case
            Case testCase = svc.CreateCase(CaseStatus.Open, emp.Id, startDate, endDate, CaseType.Consecutive, reasonCode);
            // Run Eligibility/Policy Selection, etc.
            testCase = elig.RunEligibility(testCase).Case;
            // Remove all the noise other than the 2 policies we care about, PDL and CFRA
            testCase.Segments[0].AppliedPolicies.RemoveAll(ap => ap.Policy.Code != "CA-CPDL" && ap.Policy.Code != "CA-CFRA");
            // For each of the remaining policies, ensure they're eligible
            testCase.Segments[0].AppliedPolicies.ForEach(ap =>
            {
                ap.Status = EligibilityStatus.Eligible;
                ap.RuleGroups.ForEach(g => g.Rules.ForEach(r =>
                {
                    if (r.Result != AppliedRuleEvalResult.Pass)
                    {
                        r.OverrideFromResult = r.Result;
                        r.Overridden = true;
                        r.Result = AppliedRuleEvalResult.Pass;
                        r.OverrideValue = r.Rule.RightExpression;
                        r.OverrideNotes = "UNIT TEST";
                    }
                }));
            });
            // Re-run eligibility which also re-runs calcs 'n' stuff
            testCase = elig.RunEligibility(testCase).Case;

            // Get our 2 applied policies
            AppliedPolicy pdl = testCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "CA-CPDL");
            AppliedPolicy cfra = testCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "CA-CFRA");
            pdl.Should().NotBeNull();
            cfra.Should().NotBeNull();

            pdl.FirstExhaustionDate.Should().HaveValue().And.Be(Date(2015, 5, 2));
            cfra.Usage.Min(u => u.DateUsed).Should().Be(Date(2015, 5, 2));

            //******************** SET END DATE "A" ********************//
            /* 1. Set the end date to Thursday, April 23, exactly 2 weeks prior to exhaustion above
             * 2. Ensure the CFRA policy starts on the day after PDL's end date
             * 3. Ensure PDL is no longer exhausted, 'cause it's not, it just ended early
             */

            // Explicitly set the policy end date to Thu, April 23, exactly 2 weeks prior to exhaustion date
            testCase = svc.SetPolicyEndDate(testCase, pdl.Policy.Code, Date(2015, 4, 23));

            // Re-Get our 2 applied policies
            pdl = testCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "CA-CPDL");
            cfra = testCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "CA-CFRA");
            pdl.Should().NotBeNull();
            cfra.Should().NotBeNull();

            pdl.FirstExhaustionDate.Should().NotHaveValue();
            pdl.EndDate.Should().HaveValue().And.Be(Date(2015, 4, 23));
            pdl.Usage.Max(u => u.DateUsed).Should().Be(Date(2015, 4, 23));
            cfra.Usage.Should().HaveCount(c => c > 0).And.Subject.Min(u => u.DateUsed).Should().Be(Date(2015, 4, 24));
            var cfraSummary = testCase.Summary.Policies.FirstOrDefault(p => p.PolicyCode == "CA-CFRA");
            cfraSummary.Should().NotBeNull();
            cfraSummary.Detail.Should().HaveCount(1);
            cfraSummary.Detail[0].StartDate.Should().Be(Date(2015, 4, 24));


            //******************** SET END DATE "B" ********************//
            /* 1. Set the end date now to extend past the new end date
             * 2. need to "add" time now and shift new policy dates
             */

            // Explicitly set the policy end date to Thu, April 23, exactly 2 weeks prior to exhaustion date
            testCase = svc.SetPolicyEndDate(testCase, pdl.Policy.Code, Date(2015, 5, 8));

            // Re-Get our 2 applied policies
            pdl = testCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "CA-CPDL");
            cfra = testCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "CA-CFRA");
            pdl.Should().NotBeNull();
            cfra.Should().NotBeNull();

            pdl.FirstExhaustionDate.Should().HaveValue().And.Be(Date(2015, 5, 2));
            pdl.EndDate.Should().HaveValue().And.Be(Date(2015, 5, 8));
            pdl.Usage.Max(u => u.DateUsed).Should().Be(Date(2015, 5, 8));
            cfra.Usage.Should().HaveCount(c => c > 0).And.Subject.Min(u => u.DateUsed).Should().Be(Date(2015, 5, 2));
            cfraSummary = testCase.Summary.Policies.FirstOrDefault(p => p.PolicyCode == "CA-CFRA");
            cfraSummary.Should().NotBeNull();
            cfraSummary.Detail.Should().HaveCount(1);
            cfraSummary.Detail[0].StartDate.Should().Be(Date(2015, 5, 2));
        }

        private DateTime Date(int year, int month, int day)
        {
            return new DateTime(year, month, day, 0, 0, 0, 0, DateTimeKind.Utc);
        }

        [TestMethod]
        public void CFRAAvailabilityOnPDLDenialTest()
        {
            // CA emp, just for this test
            Employee emp = Employee.GetById("000000000000000000000024"); // Californian Pregnant Female
            emp.Should().NotBeNull();
            deleteTestCases(emp.Id);
            Case.Repository.Collection.Remove(Case.Query.EQ(c => c.Employee.Id, emp.Id), RemoveFlags.None);

            string reasonCode = "PREGMAT";
            string cfraPolicyCode = "CA-CFRA";
            string cpdlPolicyCode = "CA-CPDL";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == reasonCode).First();
            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                DateTime caseStartDate = new DateTime(2017, 4, 3, 0, 0, 0, DateTimeKind.Utc);
                DateTime caseEndDate = new DateTime(2017, 8, 15, 0, 0, 0, DateTimeKind.Utc);

                Case newCase = cs.CreateCase(CaseStatus.Open, emp.Id, caseStartDate, caseEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                newCase.SetCaseEvent(CaseEventType.DeliveryDate, new DateTime(2017, 5, 13, 0, 0, 0, DateTimeKind.Utc));
                newCase.Metadata.SetRawValue("WillUseBonding", true);
                newCase.SetCaseEvent(CaseEventType.BondingStartDate, new DateTime(2017, 6, 1, 0, 0, 0, DateTimeKind.Utc));
                newCase.SetCaseEvent(CaseEventType.BondingEndDate, new DateTime(2017, 6, 16, 0, 0, 0, DateTimeKind.Utc));
                LeaveOfAbsence loa = esvc.RunEligibility(newCase);

                AppliedPolicy cfraAppliedPolicy = newCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == cfraPolicyCode);
                DateTime expectedfirstUsageDate = new DateTime(2017, 6, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime actualfirstUsageDate = cfraAppliedPolicy.Usage.Select(u => u.DateUsed).Min();
                Assert.AreEqual(expectedfirstUsageDate, actualfirstUsageDate, "{0} Applied Policy first usage date should be {1}", cfraAppliedPolicy.Policy.Name, expectedfirstUsageDate.ToString("mm/dd/yyyy"));

                cs.SetPolicyEndDate(newCase, cpdlPolicyCode, new DateTime(2017, 7, 31, 0, 0, 0, DateTimeKind.Utc));

                DateTime denialStartDate = new DateTime(2017, 4, 3, 0, 0, 0, DateTimeKind.Utc);
                DateTime denialEndDate = new DateTime(2017, 4, 14, 0, 0, 0, DateTimeKind.Utc);
                cs.ApplyDetermination(newCase, cpdlPolicyCode, denialStartDate, denialEndDate, AdjudicationStatus.Denied, AdjudicationDenialReason.Other, "Other Test", AdjudicationDenialReason.OtherDescription);

                // find the policy again
                cfraAppliedPolicy = newCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == cfraPolicyCode);

                actualfirstUsageDate = cfraAppliedPolicy.Usage.Select(u => u.DateUsed).Min();
                expectedfirstUsageDate = new DateTime(2017, 8, 01, 0, 0, 0, DateTimeKind.Utc);

                Assert.AreEqual(expectedfirstUsageDate, actualfirstUsageDate, "{0} Applied Policy first usage date should be {1}", cfraAppliedPolicy.Policy.Name, expectedfirstUsageDate.ToString("mm/dd/yyyy"));
            }
        }
        private void deleteTestCases(string empId)
        {
            Case.Repository.Collection.Remove(Case.Query.EQ(c => c.Employee.Id, empId));
            ToDoItem.Repository.Collection.Remove(ToDoItem.Query.EQ(c => c.EmployeeId, empId));
            Communication.Repository.Collection.Remove(Communication.Query.EQ(c => c.EmployeeId, empId));
            Attachment.Repository.Collection.Remove(Attachment.Query.EQ(c => c.EmployeeId, empId));
        }

    }
}
