﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Logic.Cases;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Tests.PolicyConfig
{
    [TestClass]
    public class NyPflPolicyEligibilityTest
    {
        [TestMethod]
        public void NyPflPaidLeaveCalculationPayMinimumTest()
        {
            var absenceReasonCode = "FHC";
            DateTime startDate = new DateTime(2018, 1, 1);
            DateTime endDate = new DateTime(2018, 12, 31);
            var employee = GetEmployee(new DateTime(2016, 12, 1), 3300D);             
            CreateTestCustomNYPFLPolicy(employee);
            AbsenceReason absenceReason = AbsenceReason.GetByCode(absenceReasonCode);
            using (var eligibilityService = new EligibilityService())
            using (var caseService = new CaseService(false))
            {
                // create case
                Case myCase = caseService.CreateCase(CaseStatus.Open, employee.Id, startDate, endDate, CaseType.Consecutive, absenceReason.Code);
                myCase.Contact = new EmployeeContact()
                {
                    ContactTypeCode = "SPOUSE",
                    Contact = new AbsenceSoft.Data.Contact()
                    {
                        FirstName = "Test",
                        LastName = "Person"
                    },
                    ContactTypeName = "Spouse",
                    CustomerId = employee.CustomerId,
                    EmployerId = employee.EmployerId,
                    EmployeeId = employee.Id,
                    EmployeeNumber = employee.EmployeeNumber
                }.Save();
                eligibilityService.RunEligibility(myCase);
                caseService.RunCalcs(myCase);
                caseService.ApplyDetermination(myCase, "NYPFL", myCase.StartDate, myCase.EndDate.Value, AdjudicationStatus.Approved);
                caseService.RunCalcs(myCase);

                var policyCodes = myCase.Segments.SelectMany(s => s.AppliedPolicies).Select(p => p.Policy.Code).Distinct().ToList();
                policyCodes.Should().Contain("NYPFL");

                var maxApprovedDate = myCase.Summary.Policies.FirstOrDefault(p => p.PolicyCode == "NYPFL").Detail
                    .Where(d => d.Determination == AdjudicationSummaryStatus.Approved).Max(a => a.EndDate.Value);

                var details = myCase.Pay.PayPeriods.SelectMany(p => p.Detail).Where(d => d.PolicyCode == "NYPFL" && maxApprovedDate > d.StartDate).ToList();
                details.Should().NotBeEmpty();
//                details.Select(d => d.PayAmount.Value).All(d => d == 100M).Should().BeTrue();
            }
        }

        private static void CreateTestCustomNYPFLPolicy(Employee employee)
        {
            //Set effective date to less than 1/1/2018. Because test cases will fail when cases created before 1/1/2018
            var testNYPFLPolicy = Policy.GetByCode("NYPFL", employee.CustomerId, employee.EmployerId);
            if (testNYPFLPolicy.CustomerId == null)
            {
                testNYPFLPolicy = testNYPFLPolicy.Clone();
                testNYPFLPolicy.CustomerId = employee.CustomerId;
                testNYPFLPolicy.EmployerId = employee.EmployerId;
                testNYPFLPolicy.EffectiveDate = new DateTime(2017, 12, 1);
                testNYPFLPolicy.Save();
            }
            
        }

        [TestMethod]
        public void NyPflPaidLeaveCalculationPayMinimumShouldNotApplyTest()
        {            
            var absenceReasonCode = "FHC";
            DateTime startDate = new DateTime(2018, 1, 1);
            DateTime endDate = new DateTime(2018, 12, 31);
            var employee = GetEmployee(new DateTime(2016, 12, 1), 15600D);
            CreateTestCustomNYPFLPolicy(employee);
            AbsenceReason absenceReason = AbsenceReason.GetByCode(absenceReasonCode);
            using (var eligibilityService = new EligibilityService())
            using (var caseService = new CaseService(false))
            {
                // create case
                Case myCase = caseService.CreateCase(CaseStatus.Open, employee.Id, startDate, endDate, CaseType.Consecutive, absenceReason.Code);
                myCase.Contact = new EmployeeContact()
                {
                    ContactTypeCode = "SPOUSE",
                    Contact = new AbsenceSoft.Data.Contact()
                    {
                        FirstName = "Test",
                        LastName = "Person"
                    },
                    ContactTypeName = "Spouse",
                    CustomerId = employee.CustomerId,
                    EmployerId = employee.EmployerId,
                    EmployeeId = employee.Id,
                    EmployeeNumber = employee.EmployeeNumber
                }.Save();
                eligibilityService.RunEligibility(myCase);
                caseService.RunCalcs(myCase);
                caseService.ApplyDetermination(myCase, "NYPFL", myCase.StartDate, myCase.EndDate.Value, AdjudicationStatus.Approved);
                caseService.RunCalcs(myCase);

                var policyCodes = myCase.Segments.SelectMany(s => s.AppliedPolicies).Select(p => p.Policy.Code).Distinct().ToList();
                policyCodes.Should().Contain("NYPFL");

                var maxApprovedDate = myCase.Summary.Policies.FirstOrDefault(p => p.PolicyCode == "NYPFL").Detail
                    .Where(d => d.Determination == AdjudicationSummaryStatus.Approved).Max(a => a.EndDate.Value);

                var details = myCase.Pay.PayPeriods.SelectMany(p => p.Detail).Where(d => d.PolicyCode == "NYPFL" && maxApprovedDate > d.StartDate).ToList();
                details.Should().NotBeEmpty();
               // details.Select(d => d.PayAmount.Value).All(d => d == 100M).Should().BeTrue();
            }
        }

        [TestMethod]
        public void NyPflFullTimeEmployeeEligibilityTest()
        {
            var absenceReasonCode = "EHC";
            var policyCode = "NEWNYPFLPOLICY";
            Employee employee = Employee.GetById("000000000000000000000002");
            Case.Repository.Collection.Remove(Case.Query.EQ(c => c.Employee.Id, employee.Id), RemoveFlags.None);
            AbsenceReason absenceReason = AbsenceReason.GetByCode(absenceReasonCode);
            CreateNyPflTestPolicy(policyCode, absenceReason);
            using (var eligibilityService = new EligibilityService())
            {
                using (CaseService caseService = new CaseService())
                {
                    // create case
                    Case myCase = caseService.CreateCase(CaseStatus.Open, employee.Id, new DateTime(2017, 4, 1), new DateTime(2017, 4, 30), CaseType.Consecutive, absenceReason.Code);
                    myCase.Employee.EmployeeClassCode = WorkType.FullTime;
                    LeaveOfAbsence loa = null;
                    myCase.Employee.ServiceDate = new DateTime(2016, 12, 1);
                    TestEligibility(policyCode, eligibilityService, myCase, EligibilityStatus.Ineligible, ref loa);
                    myCase.Employee.ServiceDate = new DateTime(2016, 10, 1);
                    TestEligibility(policyCode, eligibilityService, myCase, EligibilityStatus.Eligible, ref loa);

                    SaveWorkRelatedCase(absenceReason, eligibilityService, caseService, myCase, new DateTime(2017, 2, 1), new DateTime(2017, 2, 28));

                    TestEligibility(policyCode, eligibilityService, myCase, EligibilityStatus.Ineligible, ref loa);
                    myCase = caseService.CreateCase(CaseStatus.Open, employee.Id, new DateTime(2017, 9, 1), new DateTime(2017, 9, 30), CaseType.Consecutive, absenceReason.Code);
                    myCase.Employee.EmployeeClassCode = WorkType.FullTime;
                    TestEligibility(policyCode, eligibilityService, myCase, EligibilityStatus.Eligible, ref loa);
                }
            }
        }

        [TestMethod]
        public void NyPflPartTimeEmployeeEligibilityTest()
        {
            var absenceReasonCode = "EHC";
            var policyCode = "NEWNYPFLPOLICY";
            Employee employee = Employee.GetById("000000000000000000000002");
            Case.Repository.Collection.Remove(Case.Query.EQ(c => c.Employee.Id, employee.Id), RemoveFlags.None);
            AbsenceReason absenceReason = AbsenceReason.GetByCode(absenceReasonCode);
            CreateNyPflTestPolicy(policyCode, absenceReason);
            using (var eligibilityService = new EligibilityService())
            {
                using (CaseService caseService = new CaseService())
                {
                    // create case
                    Case myCase = caseService.CreateCase(CaseStatus.Open, employee.Id, new DateTime(2016, 4, 1), new DateTime(2016, 4, 30), CaseType.Consecutive, absenceReason.Code);
                    myCase.Employee.EmployeeClassCode = WorkType.PartTime;
                    LeaveOfAbsence loa = null;
                    myCase.Employee.ServiceDate = new DateTime(2015, 12, 1);
                    SaveWorkRelatedCase(absenceReason, eligibilityService, caseService, myCase, new DateTime(2016, 1, 1), new DateTime(2016, 2, 1));
                    TestEligibility(policyCode, eligibilityService, myCase, EligibilityStatus.Ineligible, ref loa);
                    myCase = caseService.CreateCase(CaseStatus.Open, employee.Id, new DateTime(2017, 3, 1), new DateTime(2017, 3, 30), CaseType.Consecutive, absenceReason.Code);
                    myCase.Employee.EmployeeClassCode = WorkType.PartTime;
                    TestEligibility(policyCode, eligibilityService, myCase, EligibilityStatus.Eligible, ref loa);
                    Case testCase = caseService.CreateCase(CaseStatus.Open, employee.Id, new DateTime(2016, 5, 1), new DateTime(2016, 12, 30), CaseType.Consecutive, absenceReason.Code);
                    testCase = eligibilityService.RunEligibility(testCase).Case;
                    testCase.Save();
                    foreach (AppliedPolicy ap in testCase.Segments[0].AppliedPolicies)
                    {
                        caseService.ApplyDetermination(testCase, ap.Policy.Code, new DateTime(2016, 5, 1), new DateTime(2016, 12, 30), AdjudicationStatus.Approved);
                    }
                    TestEligibility(policyCode, eligibilityService, myCase, EligibilityStatus.Ineligible, ref loa);
                    myCase = caseService.CreateCase(CaseStatus.Open, employee.Id, new DateTime(2017, 9, 11), new DateTime(2017, 9, 25), CaseType.Consecutive, absenceReason.Code);
                    myCase.Employee.EmployeeClassCode = WorkType.PartTime;
                    TestEligibility(policyCode, eligibilityService, myCase, EligibilityStatus.Eligible, ref loa);
                }
            }
        }

        private void SaveWorkRelatedCase(AbsenceReason absenceReason, EligibilityService eligibilityService, CaseService caseService, Case myCase, DateTime startDate, DateTime endDate)
        {
            Case workRelatedCase = caseService.CreateCase(CaseStatus.Open, myCase.Employee.Id, startDate, endDate, CaseType.Consecutive, absenceReason.Code);
            workRelatedCase.Metadata.Set("IsWorkRelated", true);
            eligibilityService.RunEligibility(workRelatedCase);
            caseService.RunCalcs(workRelatedCase);
            workRelatedCase.Save();
        }

        private void TestEligibility(string policyCode, EligibilityService eligibilityService, Case myCase, EligibilityStatus eligibilityStatus,ref LeaveOfAbsence loa)
        {            
            loa = eligibilityService.RunEligibility(myCase);
            AppliedPolicy nypflAppliedPolicy = loa.Case.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(ap => ap.Policy.Code == policyCode);
            Assert.IsNotNull(nypflAppliedPolicy);
            Assert.AreEqual(eligibilityStatus, nypflAppliedPolicy.Status);
        }

        private Employee GetEmployee(DateTime serviceDate, double salary)
        {
            var employee = Employee.GetById("507000001234500000000002");
            if (employee == null)
            {
                employee = Employee.GetById("000000000000000000000002").Clone();
                employee.Id = "507000001234500000000002";
            }
            employee.EmployeeNumber = "AT-5070 - NYPFL";
            Case.Repository.Collection.Remove(Case.Query.EQ(c => c.Employee.Id, employee.Id), RemoveFlags.None);

            if (employee.Employer.DefaultPayScheduleId == null || employee.Employer.DefaultPaySchedule == null)
            {
                employee.Employer.DefaultPaySchedule = new PaySchedule()
                {
                    EmployerId = employee.EmployerId,
                    CustomerId = employee.CustomerId,
                    ProcessingDays = 3,
                    Name = "Default Pay Schedule",
                    PayPeriodType = PayPeriodType.BiWeekly,
                    DayStart = (int)DayOfWeek.Sunday,
                    DayEnd = (int)DayOfWeek.Saturday,
                    Default = true,
                    StartDate = new DateTime(2010, 1, 3).ToMidnight()
                }.Save();
                employee.Employer.Save();
            }

            employee.PayScheduleId = employee.Employer.DefaultPayScheduleId;
            employee.PayType = PayType.Salary;
            employee.Salary = salary;
            employee.EmployeeClassCode = WorkType.FullTime;
            employee.ServiceDate = serviceDate;
            employee.WorkState = "NY";
            employee.WorkCountry = "US";
            return employee.Save();
        }

        private void CreateNyPflTestPolicy(string policyCode, AbsenceReason absenceReason)
        {
            // create test policy that has eligibility rule group
            var testNYPFLPolicy = Policy.GetByCode(policyCode, "000000000000000000000002", "000000000000000000000002");
            if (testNYPFLPolicy == null)
            {
                testNYPFLPolicy = new Policy()
                {
                    CustomerId = "000000000000000000000002",
                    EmployerId = "000000000000000000000002",
                    Name = "NYPFL Test Policy",
                    Code = policyCode,
                    AbsenceReasons = new List<PolicyAbsenceReason>()
                    {
                        new PolicyAbsenceReason()
                        {
                            Reason = absenceReason,
                            EntitlementType = EntitlementType.WorkWeeks,
                            Entitlement = 12,
                            CaseTypes = CaseType.Consecutive
                        }
                    },
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        new PolicyRuleGroup()
                        {
                            RuleGroupType = PolicyRuleGroupType.Eligibility,
                            SuccessType = RuleGroupSuccessType.Or,
                            Rules = new List<Rule>()
                            {
                                new Rule()
                                {
                                    Id = Guid.NewGuid(),
                                    LeftExpression = "MeetsNewYorkPflMinLengthOfServiceFT(26, Unit.Weeks)",
                                    Operator = "==",
                                    RightExpression = "true",
                                    Name = "Test Rule FT",
                                    Description = "Test Description FT"
                                },
                                new Rule()
                                {
                                    Id = Guid.NewGuid(),
                                    LeftExpression = "MeetsNewYorkPflMinLengthOfServicePT(56, Unit.Weeks, 175)",
                                    Operator = "==",
                                    RightExpression = "true",
                                    Name = "Test Rule PT",
                                    Description = "Test Description PT"
                                }
                            }
                        }
                    }
                };
                testNYPFLPolicy.Save();
            }
        }

        private Employee GetEmployeeForPREGMATBonding()
        {
            var employee = Employee.GetById("000000000000000000000205");
            if (employee == null)
            {
                employee = Employee.GetById("000000000000000000000002").Clone();
                employee.Id = "000000000000000000000205";
            }
            employee.EmployeeNumber = "PS-499 - NYPFL";
            Case.Repository.Collection.Remove(Case.Query.EQ(c => c.Employee.Id, employee.Id), RemoveFlags.None);
            employee.Gender = Gender.Female;
            employee.EmployeeClassCode = WorkType.FullTime;
            employee.ServiceDate = new DateTime(2014, 1, 1);
            employee.WorkState = "NY";
            employee.WorkCountry = "US";
            return employee.Save();
        }

        [TestMethod]
        public void NyPflPREGMATShouldhaveBondingDate()
        {
            var absenceReasonCode = "PREGMAT";
            DateTime startDate = new DateTime(2018, 1, 1);
            DateTime endDate = new DateTime(2018, 6, 30);
            var employee = GetEmployeeForPREGMATBonding();
            AbsenceReason absenceReason = AbsenceReason.GetByCode(absenceReasonCode);
            using (var eligibilityService = new EligibilityService())
            using (var caseService = new CaseService(false))
            {
                // create case
                Case myCase = caseService.CreateCase(CaseStatus.Open, employee.Id, startDate, endDate, CaseType.Consecutive, absenceReason.Code);
                myCase.Metadata.SetRawValue("WillUseBonding", true);
                DateTime deliveryDate = new DateTime(2018, 2, 25);
                DateTime bondingStartDate = new DateTime(2018, 2, 1);
                DateTime bondingEndDate = new DateTime(2018, 6, 25);
                myCase.SetCaseEvent(CaseEventType.DeliveryDate, deliveryDate);
                myCase.SetCaseEvent(CaseEventType.BondingStartDate, bondingStartDate);
                myCase.SetCaseEvent(CaseEventType.BondingEndDate, bondingEndDate);
                myCase.Contact = new EmployeeContact()
                {
                    ContactTypeCode = "CHILD",
                    Contact = new AbsenceSoft.Data.Contact()
                    {
                        FirstName = "Test",
                        LastName = "Person"
                    },
                    ContactTypeName = "Child",
                    CustomerId = employee.CustomerId,
                    EmployerId = employee.EmployerId,
                    EmployeeId = employee.Id,
                    EmployeeNumber = employee.EmployeeNumber
                }.Save();

                eligibilityService.RunEligibility(myCase);
                caseService.RunCalcs(myCase);

                var policyCodes = myCase.Segments.SelectMany(s => s.AppliedPolicies).Select(p => p.Policy.Code).Distinct().ToList();
                policyCodes.Should().Contain("NYPFL");
                var nypfl = myCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "NYPFL");
                Assert.AreEqual(bondingStartDate, nypfl.StartDate);
                Assert.AreEqual(bondingEndDate, nypfl.EndDate);
            }
        }
    }
}
