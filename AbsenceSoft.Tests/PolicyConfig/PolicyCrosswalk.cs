﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Logic.Cases;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Tests.PolicyConfig
{
    [TestClass]
    public class PolicyCrosswalk
    {
        [TestMethod]
        public void CrosswalkCodeIsApplied()
        {

            string codeToApply = "CROSSWALKSUCCESS";
            var absenceReason = AbsenceReason.GetByCode("EHC");
            var policyCrosswalkType = new PolicyCrosswalkType()
            {
                Name = "Crosswalk Test",
                Code = "CROSSWALKTEST"
            };

            policyCrosswalkType.Save();

            // create test policy that has crosswalk rule group
            var testPolicy = new Policy()
            {
                Name = "Crosswalk",
                Code = "CROSSWALK",
                AbsenceReasons = new List<PolicyAbsenceReason>()
                {
                    new PolicyAbsenceReason()
                    {
                        Reason = absenceReason,
                        EntitlementType = EntitlementType.WorkWeeks,
                        Entitlement = 12,
                        CaseTypes = CaseType.Consecutive
                    }
                },
                RuleGroups = new List<PolicyRuleGroup>()
                {
                    new PolicyRuleGroup()
                    {
                        RuleGroupType = PolicyRuleGroupType.Selection
                    },
                    new PolicyRuleGroup()
                    {
                        RuleGroupType = PolicyRuleGroupType.Crosswalk,
                        CrosswalkTypeCode = policyCrosswalkType.Code,
                        CrosswalkCode = codeToApply
                    }
                }
            };

            testPolicy.Save();

            // create case 
            var employee = Employee.GetById("000000000000000000000002");
            Case.Repository.Collection.Remove(Case.Query.EQ(c => c.Employee.Id, employee.Id));

            DateTime startDate = new DateTime(2017, 1, 1);
            DateTime endDate = new DateTime(2017, 1, 31);
            using (var caseService = new CaseService())
            using (var eligibilityService = new EligibilityService())
            {
                Case myCase = caseService.CreateCase(CaseStatus.Open, employee.Id, startDate, endDate, CaseType.Consecutive, absenceReason.Code);
                myCase = eligibilityService.RunEligibility(myCase).Case;
                myCase = caseService.UpdateCase(myCase);

                foreach (var segment in myCase.Segments)
                {
                    foreach (var appliedPolicy in segment.AppliedPolicies)
                    {
                        if (appliedPolicy.Policy.Code == testPolicy.Code)
                        {
                            Assert.IsNotNull(appliedPolicy.PolicyCrosswalkCodes);
                            var matchingPolicyCode = appliedPolicy.PolicyCrosswalkCodes
                                .FirstOrDefault(pcc => pcc.TypeName == policyCrosswalkType.Name
                                    && pcc.TypeCode == policyCrosswalkType.Code
                                    && pcc.Code == codeToApply);

                            Assert.IsNotNull(matchingPolicyCode);
                        }
                        else
                        {
                            Assert.IsTrue(appliedPolicy.PolicyCrosswalkCodes == null || !appliedPolicy.PolicyCrosswalkCodes.Any(), "There should not be a policy crosswalk code applied here.s");
                        }
                    }
                }
            }

        }


        [TestMethod]
        [Ignore]
        public void CrosswalkCodeIsAppliedAndOverriddenFromAbsenceReason()
        {
            string codeToApply = "CROSSWALKSUCCESS";
            var absenceReason = AbsenceReason.GetByCode("EHC");
            var policyCrosswalkType = new PolicyCrosswalkType()
            {
                Name = "Crosswalk Test 2",
                Code = "CROSSWALKTEST2"
            };

            policyCrosswalkType.Save();

            // create test policy that has crosswalk rule group
            var testPolicy = new Policy()
            {
                Name = "Crosswalk Test Policy",
                Code = "UNTOUCHED",
                AbsenceReasons = new List<PolicyAbsenceReason>()
                {
                    new PolicyAbsenceReason()
                    {
                        Reason = absenceReason,
                        EntitlementType = EntitlementType.WorkWeeks,
                        Entitlement = 12,
                        CaseTypes = CaseType.Consecutive,
                        RuleGroups = new List<PolicyRuleGroup>()
                        {
                            new PolicyRuleGroup()
                            {
                                RuleGroupType = PolicyRuleGroupType.Crosswalk,
                                CrosswalkTypeCode = policyCrosswalkType.Code,
                                CrosswalkCode = codeToApply
                            }
                        }
                    }
                },
                RuleGroups = new List<PolicyRuleGroup>()
                {
                    new PolicyRuleGroup()
                    {
                        RuleGroupType = PolicyRuleGroupType.Selection
                    },
                    new PolicyRuleGroup()
                    {
                        RuleGroupType = PolicyRuleGroupType.Crosswalk,
                        CrosswalkTypeCode = policyCrosswalkType.Code,
                        CrosswalkCode = "FAILED"
                    }
                }
            };

            testPolicy.Save();

            // create case 
            var employee = Employee.GetById("000000000000000000000002");
            Case.Repository.Collection.Remove(Case.Query.EQ(c => c.Employee.Id, employee.Id));

            DateTime startDate = new DateTime(2017, 1, 1);
            DateTime endDate = new DateTime(2017, 1, 31);
            using (var caseService = new CaseService())
            using (var eligibilityService = new EligibilityService())
            {
                Case myCase = caseService.CreateCase(CaseStatus.Open, employee.Id, startDate, endDate, CaseType.Consecutive, absenceReason.Code);
                myCase = eligibilityService.RunEligibility(myCase).Case;
                myCase = caseService.UpdateCase(myCase);

                foreach (var segment in myCase.Segments)
                {
                    foreach (var appliedPolicy in segment.AppliedPolicies)
                    {
                        if (appliedPolicy.Policy.Code == testPolicy.Code)
                        {
                            Assert.IsNotNull(appliedPolicy.PolicyCrosswalkCodes);
                            var matchingPolicyCode = appliedPolicy.PolicyCrosswalkCodes
                                .FirstOrDefault(pcc => pcc.TypeName == policyCrosswalkType.Name
                                    && pcc.TypeCode == policyCrosswalkType.Code
                                    && pcc.Code == codeToApply);

                            Assert.IsNotNull(matchingPolicyCode);
                        }
                        else
                        {
                            Assert.IsTrue(appliedPolicy.PolicyCrosswalkCodes == null || !appliedPolicy.PolicyCrosswalkCodes.Any(), "There should not be a policy crosswalk code applied here.");
                        }
                    }
                }
            }

        }
    }
}
