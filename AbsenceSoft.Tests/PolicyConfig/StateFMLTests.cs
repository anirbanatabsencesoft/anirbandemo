﻿using System;
using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Policies;
using System.Collections.Generic;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Logic.Expressions;

namespace AbsenceSoft.Tests.PolicyConfig
{
    [TestClass]
    public class StateFMLTests
    {
#warning HACK HACK HACK, Temporary disable unit test to account for policy changes
        [TestMethod, Ignore]
        public void TestNJFMLPregnancyStartDateAfterDelivery()
        {
            string empId = "000000000000000000000188";                                   // NJ 1 Year, Female
            string reasonCode = "PREGMAT";                                               // Pregnancy / Maternity
            DateTime caseStart = new DateTime(2015, 1, 5, 0, 0, 0, DateTimeKind.Utc);    // Monday, Jan  5, 2015 (case start date)
            DateTime caseEnd = new DateTime(2015, 2, 27, 0, 0, 0, DateTimeKind.Utc);     // Friday, Feb 27, 2015 (case end date)
            DateTime deliveryDate = new DateTime(2015, 2, 2, 0, 0, 0, DateTimeKind.Utc); // Monday, Feb  2, 2015 (delivery date)
            string policyCode = "NJ-FML";                                                // New Jersey Family Leave
            Case newCase = null;
            LeaveOfAbsence loa = null;

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == reasonCode).First();

            var policy = Policy.GetByCode(policyCode);

            // Get the employee, and then ensure it's still a female and works in NJ
            Employee testEmp = Employee.GetById(empId);
            testEmp.Gender = Gender.Female;
            testEmp.WorkState = "NJ";
            // Correct Hire Date, Service Date and Work Schedule Start Date
            testEmp.HireDate = testEmp.ServiceDate = caseStart.AddYears(-2);
            testEmp.WorkSchedules[0].StartDate = testEmp.HireDate.Value;
            testEmp.Save();

            // Remove all existing cases for this employee to ensure they don't interfere with calcs, etc. (need predictability)
            List<Case> empCases = Case.AsQueryable().Where(c => c.Employee.Id == empId).ToList();
            empCases.ForEach(c => c.Delete());

            // Create the case
            using (CaseService cs = new CaseService())
                newCase = cs.CreateCase(CaseStatus.Open, empId, caseStart, caseEnd, CaseType.Consecutive, reason.Code);

            // Set the expected/actual delivery date (this applies either way, expected or actual set this event date)
            newCase.SetCaseEvent(CaseEventType.DeliveryDate, deliveryDate);

            // Should have only 1 segment
            Assert.AreEqual(newCase.Segments.Count, 1);

            // Run eligibility
            using (EligibilityService esvc = new EligibilityService())
            {
                loa = esvc.RunEligibility(newCase);
                newCase = loa.Case = esvc.AddManualPolicy(loa.Case, policy.Code, "ADDED");
                loa = esvc.RunEligibility(newCase);
            }

            // Get the New Jersey policy and ensure it exists and is eligible
            var njfml = newCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == policyCode);
            Assert.IsNotNull(njfml);
            Assert.AreEqual(EligibilityStatus.Eligible, njfml.Status);
            Assert.AreEqual(deliveryDate, njfml.StartDate);
            Assert.AreEqual(njfml.EndDate, caseEnd);

            Assert.AreEqual(26, njfml.Usage.Count);

            double minsUsed = njfml.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.MinutesUsed);
            double daysUsed = Math.Round(njfml.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.PercentOfDay), 4);
            double weeksUsed = Math.Round(njfml.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.DaysUsage(EntitlementType.WorkWeeks)), 4);

            Assert.AreEqual(9120, minsUsed);
            Assert.AreEqual(19, daysUsed);
            Assert.AreEqual(3.8, weeksUsed);

            var njSummary = newCase.Summary.Policies.FirstOrDefault(p => p.PolicyCode == policyCode);
            Assert.IsNotNull(njSummary);
        }

        [TestMethod]
        public void MinumumLengthOfServiceEligibilityForNYTest()
        {
            var policy = Policy.GetByCode("NY-STD");
            Assert.IsNotNull(policy);

            DateTime startDate = new DateTime(2017, 09, 26);
            DateTime endDate = new DateTime(2017, 09, 30);
            var reasonCode = AbsenceReason.GetByCode("EHC").Code;
            Case myCase = CreateCaseForNYWorkState(startDate, endDate, reasonCode);

            foreach (var segment in myCase.Segments)
            {
                foreach (var appliedPolicy in segment.AppliedPolicies)
                {
                    if (appliedPolicy.Policy.Code == policy.Code)
                    {
                        var appliedRuleGroup = appliedPolicy.RuleGroups.FirstOrDefault(rg => rg.Rules.All(r => r.Rule.Name == "Worked 4 Weeks"));
                        Assert.IsNotNull(appliedRuleGroup);
                        Assert.IsTrue(appliedRuleGroup.Pass);
                    }
                }
            }
        }

        /// <summary>
        /// Eligibility rule test to compare expected delivery date to start date
        /// </summary>
        [TestMethod]
        public void LengthOfServiceFromExpectedDeliveryDateTest()
        {
            AbsenceReason absenceReason = AbsenceReason.GetByCode("PREGMAT");
            string employeeId = "000000000000000000000002";
            string customerId = "000000000000000000000002";

            //create a rule
            Rule lengthOfServiceShouldBeGreaterThan12Months;
            using (var baseExpressionService = new BaseExpressionService())
            {
                var expressionGroups = baseExpressionService.GetRuleExpressions();
                var caseInformationExpressionGroup = expressionGroups.FirstOrDefault(eg => eg.Title == "Case Information");
                caseInformationExpressionGroup.Should().NotBeNull();
                var lengthOfServiceFromDeliveryDate = caseInformationExpressionGroup.Expressions.FirstOrDefault(e => e.Name == "LengthOfServiceFromExpectedDeliveryDate");
                lengthOfServiceFromDeliveryDate.Should().NotBeNull();
                lengthOfServiceFromDeliveryDate.RuleOperator = ">";
                lengthOfServiceFromDeliveryDate.Value = 12;
                lengthOfServiceShouldBeGreaterThan12Months = (Rule)baseExpressionService.GetRule(lengthOfServiceFromDeliveryDate);
                lengthOfServiceShouldBeGreaterThan12Months.Should().NotBeNull();
            }

            // create test policy that has eligibility rule group
            Policy policy = new Policy()
            {
                CustomerId = customerId,
                EmployerId = employeeId,
                Name = "Expected Delivery Date Policy",
                Code = "EDDP",
                AbsenceReasons = new List<PolicyAbsenceReason>()
                {
                    new PolicyAbsenceReason()
                    {
                        Reason = absenceReason,
                        EntitlementType = EntitlementType.WorkWeeks,
                        Entitlement = 12,
                        CaseTypes = CaseType.Consecutive
                    }
                },
                RuleGroups = new List<PolicyRuleGroup>()
                {
                    new PolicyRuleGroup()
                    {
                        RuleGroupType = PolicyRuleGroupType.Selection
                    },
                    new PolicyRuleGroup()
                    {
                        RuleGroupType = PolicyRuleGroupType.Eligibility,
                        Rules = new List<Rule>()
                        {
                            lengthOfServiceShouldBeGreaterThan12Months
                        }
                    }
                }
            };

            policy.Save();

            using (var eligibilityService = new EligibilityService())
            {
                using (CaseService caseService = new CaseService())
                {
                    Employee employee = Employee.GetById(employeeId);

                    Case.Repository.Collection.Remove(Case.Query.EQ(c => c.Employee.Id, employee.Id), MongoDB.Driver.RemoveFlags.None);

                    DateTime startDate = new DateTime(2018, 01, 01);
                    DateTime endDate = new DateTime(2018, 01, 31);
                    DateTime expectedDeliveryDate = new DateTime(2018, 01, 02);

                    // create case
                    Case _case = caseService.CreateCase(CaseStatus.Open, employee.Id, startDate, endDate, CaseType.Consecutive, absenceReason.Code);
                    _case.Employee.ServiceDate = new DateTime(2017, 08, 28);
                    _case.Employee.Gender = Gender.Female;
                    _case.Contact = new EmployeeContact()
                    {
                        ContactTypeCode = "SELF",
                        ContactTypeName = "SELF",
                        EmployerId = employeeId,
                        EmployeeNumber = "E001",
                        CustomerId = customerId
                    };

                    _case.SetCaseEvent(CaseEventType.DeliveryDate, expectedDeliveryDate);

                    _case = eligibilityService.RunEligibility(_case).Case;

                    foreach (var segment in _case.Segments)
                    {
                        foreach (var appliedPolicy in segment.AppliedPolicies)
                        {
                            if (appliedPolicy.Policy.Code == policy.Code)
                            {
                                foreach (var ruleGroup in appliedPolicy.RuleGroups)
                                {
                                    var rule = ruleGroup.Rules.FirstOrDefault(r => r.Rule.Name == "Expected Delivery Date");
                                    Assert.IsNotNull(rule);
                                    // This should be false as employee has not completed 1 year from expected delivery date 
                                    Assert.IsFalse(rule.Pass);
                                    Assert.AreEqual(rule.ActualValueString, "4.17");
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Test for elimination period not denying for NY Temp Disability policy
        /// </summary>
        [TestMethod]
        public void TestNYDisabilityPolicyEliminationPeriod()
        {
            var policy = Policy.GetByCode("NY-STD");
            Assert.IsNotNull(policy);
            DateTime startDate = new DateTime(2017, 10, 30);
            DateTime endDate = new DateTime(2017, 11, 30);
            var reasonCode = AbsenceReason.GetByCode("EHC").Code;

            Case myCase = CreateCaseForNYWorkState(startDate,endDate, reasonCode);
            Assert.IsNotNull(myCase.Summary);
            CaseSummaryPolicy cspSTD = myCase.Summary.Policies.FirstOrDefault(f => f.PolicyCode == "NY-STD");

            Assert.IsNotNull(cspSTD);
            Assert.IsNotNull(cspSTD.Detail);
            Assert.AreEqual(cspSTD.Detail.Count, 2);

            // Denied
            Assert.AreEqual(cspSTD.Detail[0].Determination, AdjudicationSummaryStatus.Denied);
            Assert.AreEqual(cspSTD.Detail[0].DenialReasonCode, AdjudicationDenialReason.EliminationPeriod);
            Assert.AreEqual(cspSTD.Detail[0].StartDate, new DateTime(2017, 10, 30, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspSTD.Detail[0].EndDate, new DateTime(2017, 11, 05, 0, 0, 0, DateTimeKind.Utc));

            // Pending
            Assert.AreEqual(cspSTD.Detail[1].Determination, AdjudicationSummaryStatus.Pending);
            Assert.AreEqual(cspSTD.Detail[1].StartDate, new DateTime(2017, 11, 06, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspSTD.Detail[1].EndDate, new DateTime(2017, 11, 30, 0, 0, 0, DateTimeKind.Utc));
        }

        /// <summary>
        /// Create a new case for NY work state
        /// </summary>
        /// <param name="startDate">The start date of the case's initial segment</param>
        /// <param name="endDate">The anticipated end date of the case's initial segment</param>
        /// <param name="reasonCode">The selected reason for the leave of absence</param>
        /// <returns></returns>
        private Case CreateCaseForNYWorkState(DateTime startDate,DateTime endDate,string reasonCode)
        {
            using (var eligibilityService = new EligibilityService())
            {
                using (CaseService caseService = new CaseService())
                {
                    var employee = Employee.GetById("000000000000000000000002");
                    Case.Repository.Collection.Remove(Case.Query.EQ(c => c.Employee.Id, employee.Id), MongoDB.Driver.RemoveFlags.None);
                    Case myCase = caseService.CreateCase(CaseStatus.Open, employee.Id, startDate, endDate, CaseType.Consecutive, reasonCode);
                    myCase.Employee.WorkState = "NY";
                    myCase.Employee.ServiceDate = new DateTime(2017, 08, 28);

                    myCase = eligibilityService.RunEligibility(myCase).Case;
                    return myCase;
                }
            }
        }

        [TestMethod]
        public void TestNYDisabilityPolicyPeriodType()
        {
            string policyCode = "NY-STD";
            var policy = Policy.GetByCode(policyCode);
            Assert.IsNotNull(policy);
            DateTime startDate = new DateTime(2017, 10, 30);
            DateTime endDate = new DateTime(2018, 10, 30);
            var reasonCode = AbsenceReason.GetByCode("EHC").Code;

            // Use existing PeriodType -- RollingBack
            Case myCase = CreateCaseForNYWorkState(startDate, endDate, reasonCode);
            CaseSummaryPolicy cspSTD = myCase.Summary.Policies.FirstOrDefault(f => f.PolicyCode == policyCode);
            var appliedPolicy = myCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);


            // Denied
            Assert.AreEqual(cspSTD.Detail[0].Determination, AdjudicationSummaryStatus.Denied);
            Assert.AreEqual(cspSTD.Detail[0].DenialReasonCode, AdjudicationDenialReason.EliminationPeriod);
            Assert.AreEqual(cspSTD.Detail[0].StartDate, new DateTime(2017, 10, 30, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspSTD.Detail[0].EndDate, new DateTime(2017, 11, 05, 0, 0, 0, DateTimeKind.Utc));

            // Pending
            Assert.AreEqual(cspSTD.Detail[1].Determination, AdjudicationSummaryStatus.Pending);
            Assert.AreEqual(cspSTD.Detail[1].StartDate, new DateTime(2017, 11, 06, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspSTD.Detail[1].EndDate, new DateTime(2018, 5, 4, 0, 0, 0, DateTimeKind.Utc));

            // Change the PeriodType -- RollingForward 
            CaseService cs = new CaseService();
            appliedPolicy.PolicyReason.PeriodType = PeriodType.RollingForward;
            cs.ApplyDetermination(myCase, policyCode, startDate, endDate, AdjudicationStatus.Approved);
            cs.GetProjectedUsage(myCase);

            cspSTD = myCase.Summary.Policies.FirstOrDefault(f => f.PolicyCode == policyCode);

            // Denied
            Assert.AreEqual(cspSTD.Detail[0].Determination, AdjudicationSummaryStatus.Denied);
            Assert.AreEqual(cspSTD.Detail[0].DenialReasonCode, AdjudicationDenialReason.EliminationPeriod);
            Assert.AreEqual(cspSTD.Detail[0].StartDate, new DateTime(2017, 10, 30, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspSTD.Detail[0].EndDate, new DateTime(2017, 11, 05, 0, 0, 0, DateTimeKind.Utc));

            // Approved
            Assert.AreEqual(cspSTD.Detail[1].Determination, AdjudicationSummaryStatus.Approved);
            Assert.AreEqual(cspSTD.Detail[1].StartDate, new DateTime(2017, 11, 06, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspSTD.Detail[1].EndDate, new DateTime(2018, 5, 4, 0, 0, 0, DateTimeKind.Utc));

        }
    }
}
