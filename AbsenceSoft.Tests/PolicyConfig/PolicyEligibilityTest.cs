﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic.Cases;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Driver;
using Newtonsoft.Json;

namespace AbsenceSoft.Tests.PolicyConfig
{
    [TestClass]
    public class PolicyEligibilityTest
    {
        [TestMethod]
        public void SubstitutePolicyTest()
        {
            var absenceReason = AbsenceReason.GetByCode("EHC");

            // create test policy that has eligibility rule group
            var testPolicy = new Policy()
            {
                CustomerId = "000000000000000000000002",
                EmployerId = "000000000000000000000002",
                Name = "Self or Spouse",
                Code = "NewPol",
                AbsenceReasons = new List<PolicyAbsenceReason>()
                {
                    new PolicyAbsenceReason()
                    {
                        Reason = absenceReason,
                        EntitlementType = EntitlementType.WorkWeeks,
                        Entitlement = 12,
                        CaseTypes = CaseType.Consecutive
                    }
                },
                RuleGroups = new List<PolicyRuleGroup>()
                {
                    new PolicyRuleGroup()
                    {
                        RuleGroupType = PolicyRuleGroupType.Selection
                    },
                    new PolicyRuleGroup()
                    {
                        RuleGroupType = PolicyRuleGroupType.Eligibility,
                        Rules = new List<Rule>()
                        {
                            new Rule()
                            {
                                Id = Guid.NewGuid(),
                                LeftExpression = "Substitute",
                                Operator = "=",
                                RightExpression = "'Fail It'",
                                Name = "Test Rule",
                                Description = "Test Description"
                            }
                        }
                    }
                }
            };
            testPolicy.Save();
            
            var substitutePolicy = new Policy()
            {
                CustomerId = "000000000000000000000002",
                EmployerId = "000000000000000000000002",
                Name = "Self or Spouse",
                Code = "SUBPOLICY",
                SubstituteFor = new List<String>() { "NEWPOL" },
                
                AbsenceReasons = new List<PolicyAbsenceReason>()
                {
                    new PolicyAbsenceReason()
                    {
                        Reason = absenceReason,
                        EntitlementType = EntitlementType.WorkWeeks,
                        Entitlement = 12,
                        CaseTypes = CaseType.Consecutive,
                        
                        PeriodType = PeriodType.RollingBack,
                        SubstituteFor = new List<String>(){ "NEWPOL" }
                    }
                },
                RuleGroups = new List<PolicyRuleGroup>()
                {
                    new PolicyRuleGroup()
                    {
                        RuleGroupType = PolicyRuleGroupType.Selection
                    },
                    new PolicyRuleGroup()
                    {
                        RuleGroupType = PolicyRuleGroupType.Eligibility,
                        Rules = new List<Rule>()
                        {
                            new Rule()
                            {
                                Id = Guid.NewGuid(),
                                LeftExpression = "Substitute",
                                Operator = "=",
                                RightExpression = "'Fail It'",
                                Name = "Test Rule",
                                Description = "Test Description"
                            }
                        }
                    }
                }
            };
            
            using (var eligibilityService = new EligibilityService())
            {
                using (CaseService caseService = new CaseService())
                {
                    // create case 
                    var employee = Employee.GetById("000000000000000000000002");
                    Case.Repository.Collection.Remove(Case.Query.EQ(c => c.Employee.Id, employee.Id), RemoveFlags.None);
                    DateTime startDate = new DateTime(2017, 1, 1);
                    DateTime endDate = new DateTime(2017, 1, 31);

                    Case myCase = caseService.CreateCase(CaseStatus.Open, employee.Id, startDate, endDate, CaseType.Consecutive, absenceReason.Code);
                    myCase = eligibilityService.RunEligibility(myCase).Case;

                    var appliedIneligiblePolicy = myCase.Segments[0].AppliedPolicies.Where(p => p.Policy.Code == "NEWPOL").FirstOrDefault();
                    Assert.AreEqual(appliedIneligiblePolicy.Status, EligibilityStatus.Ineligible);

                    var appliedSubstitutePolicy = myCase.Segments[0].AppliedPolicies.Where(p => p.Policy.Code == "SUBPOLICY").FirstOrDefault();
                    Assert.IsNull(appliedSubstitutePolicy);
                    substitutePolicy.Save();

                    myCase = eligibilityService.RunEligibility(myCase).Case;
                    appliedSubstitutePolicy = myCase.Segments[0].AppliedPolicies.Where(p => p.Policy.Code == "SUBPOLICY").FirstOrDefault();
                    Assert.IsNotNull(appliedSubstitutePolicy);
                }
            }

            testPolicy.Delete();
            substitutePolicy.Delete();
        }
        [TestMethod,Ignore]
        public void CheckContainsOperatorWorksFine()
        {
            var absenceReason = AbsenceReason.GetByCode("FHC");

            // create test policy that has eligibility rule group
            var testPolicy = new Policy()
            {
                CustomerId = "000000000000000000000002",
                EmployerId = "000000000000000000000002",
                Name = "Self or Spouse",
                Code = "NewPol",
                AbsenceReasons = new List<PolicyAbsenceReason>()
                {
                    new PolicyAbsenceReason()
                    {
                        Reason = absenceReason,
                        EntitlementType = EntitlementType.WorkWeeks,
                        Entitlement = 12,
                        CaseTypes = CaseType.Consecutive
                    }
                },
                RuleGroups = new List<PolicyRuleGroup>()
                {
                    new PolicyRuleGroup()
                    {
                        RuleGroupType = PolicyRuleGroupType.Selection
                    },
                    new PolicyRuleGroup()
                    {
                        RuleGroupType = PolicyRuleGroupType.Eligibility,
                        Rules = new List<Rule>()
                        {
                            new Rule()
                            {
                                Id = Guid.NewGuid(),
                                LeftExpression = "CaseRelationshipType",
                                Operator = "::",
                                RightExpression = "'SELF,SPOUSE,PARENT'",
                                Name = "Test Rule",
                                Description = "Test Description"
                            }
                        }
                    }
                }
            };

            testPolicy.Save();

            using (var eligibilityService = new EligibilityService())
            {
                using (CaseService caseService = new CaseService())
                {
                    // create case 
                    var employee = Employee.GetById("000000000000000000000002");
                    Case.Repository.Collection.Remove(Case.Query.EQ(c => c.Employee.Id, employee.Id), RemoveFlags.None);
                    DateTime startDate = new DateTime(2017, 1, 1);
                    DateTime endDate = new DateTime(2017, 1, 31);

                    Case myCase = caseService.CreateCase(CaseStatus.Open, employee.Id, startDate, endDate, CaseType.Consecutive, absenceReason.Code);
                    myCase.Contact = new EmployeeContact()
                    {
                        ContactTypeCode = "SPOUSE",
                        ContactTypeName = "SPOUSE",
                        EmployerId = "000000000000000000000002",
                        EmployeeNumber = "demo_001",
                        CustomerId = "000000000000000000000002"
                    };

                    myCase = eligibilityService.RunEligibility(myCase).Case;

                    foreach (var segment in myCase.Segments)
                    {
                        foreach (var appliedPolicy in segment.AppliedPolicies)
                        {
                            if (appliedPolicy.Policy.Code == testPolicy.Code)
                            {
                                foreach (var ruleGroup in appliedPolicy.RuleGroups)
                                {
                                    var testRule = ruleGroup.Rules.FirstOrDefault(a => a.Rule.Name == "Test Rule");
                                    Assert.AreEqual(testRule.ActualValueString, "SPOUSE");
                                    Assert.IsTrue(testRule.Pass);
                                }
                            }
                        }
                    }
                }
            }
        }

        [TestMethod]
        public void RemoveCPDLOnRelapse()
        {
            string empId = "000000000000000000000018";
            string caseCode = "PREGMAT";
            deleteTestCases(empId);
            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).FirstOrDefault();
            Assert.IsNotNull(reason);

            DateTime startDate = new DateTime(2018, 11, 10, 0, 0, 0, DateTimeKind.Utc);
            DateTime endDate = new DateTime(2019, 02, 25, 0, 0, 0, DateTimeKind.Utc);

            CaseService cs = new CaseService();
            Case newCase = cs.CreateCase(CaseStatus.Open, empId, startDate, endDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

            newCase.Metadata.SetRawValue("WillUseBonding", true);
            DateTime deliveryDate = new DateTime(2019, 1, 1);
            DateTime bondingStartDate = new DateTime(2019, 1, 5);
            DateTime bondingEndDate = new DateTime(2019, 1, 31);
            newCase.SetCaseEvent(CaseEventType.DeliveryDate, deliveryDate);
            newCase.SetCaseEvent(CaseEventType.BondingStartDate, bondingStartDate);
            newCase.SetCaseEvent(CaseEventType.BondingEndDate, bondingEndDate);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(newCase);

            cs.RunCalcs(newCase);

            // check CA-CPDL is applied or not
            AppliedPolicy cacpdl = newCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == "CA-CPDL");
            Assert.IsNotNull(cacpdl);
            Assert.AreEqual(cacpdl.EndDate, bondingStartDate.AddDays(-1));

            cs.GetProjectedUsage(newCase);

            DateTime approveStart = new DateTime(2018, 11, 10, 0, 0, 0, DateTimeKind.Utc);
            DateTime approveEnd = new DateTime(2019, 02, 25, 0, 0, 0, DateTimeKind.Utc);

            cs.ApplyDetermination(newCase, null, approveStart, approveEnd, AdjudicationStatus.Approved);

            string caseId = newCase.Id;
            Case completedCase = Case.Repository.GetById(caseId);

            if (completedCase != null)
            {
                //Close the Case 
                completedCase.Status = AbsenceSoft.Data.Enums.CaseStatus.Closed;
                completedCase.ModifiedById = "000000000000000000000002";
                completedCase.SetModifiedDate(DateTime.UtcNow);
                completedCase.Segments.ForEach(s => s.Status = CaseStatus.Closed);
                completedCase.SaveStatus();
            }
            Assert.AreEqual(CaseStatus.Closed, completedCase.Status);

            Relapse relapse = new Relapse
            {
                CaseId = completedCase.Id,
                StartDate = new DateTime(2019, 4, 1, 0, 0, 0, DateTimeKind.Utc),
                EndDate = new DateTime(2019, 4, 20, 0, 0, 0, DateTimeKind.Utc),
                CaseType = (CaseType)(Enum.Parse(typeof(CaseType), completedCase.CaseType.ToString())),
                Employee = completedCase.Employee
            };

            LeaveOfAbsence loaRelpase = cs.ReopenCase(completedCase, relapse);
            Assert.AreEqual(CaseStatus.Open, loaRelpase.Case.Status);

            Case reOpenedCase = cs.UpdateRelapse(loaRelpase.Case, loaRelpase.Relapse);
            Assert.AreEqual(2, loaRelpase.Case.Segments.Count);

            // check cpdl policies count
            List<AppliedPolicy> cpdlList = reOpenedCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).ToList(a => (a.Policy ?? new Policy()).Code == "CA-CPDL");
            Assert.IsNotNull(cpdlList);
            Assert.AreEqual(1, cpdlList.Count);

            // check cpdl policies detail count
            CaseSummaryPolicy cpdlSummary = reOpenedCase.Summary.Policies.Where(p => p.PolicyCode.Equals("CA-CPDL")).FirstOrDefault();
            Assert.IsNotNull(cpdlSummary);
            Assert.IsNotNull(cpdlSummary.Detail);
            Assert.AreEqual(1, cpdlSummary.Detail.Count);

            // clean out the other tests (if the bombed)
            deleteTestCases(empId);
        }

        /// <summary>
        /// Deletes the test cases and related data for an employee.
        /// </summary>
        /// <param name="empId">The emp identifier.</param>
        private void deleteTestCases(string empId)
        {
            Case.Repository.Collection.Remove(Case.Query.EQ(c => c.Employee.Id, empId));
            ToDoItem.Repository.Collection.Remove(ToDoItem.Query.EQ(c => c.EmployeeId, empId));
            Communication.Repository.Collection.Remove(Communication.Query.EQ(c => c.EmployeeId, empId));
            Attachment.Repository.Collection.Remove(Attachment.Query.EQ(c => c.EmployeeId, empId));
        }
    }
}
