﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Logic.Cases;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Driver;
using Newtonsoft.Json;

namespace AbsenceSoft.Tests.PolicyConfig
{
    [TestClass]
    public class PolicyBehaviorTest
    {
        [TestMethod]
        public void CheckFMLAPolicyBehaviourIgnoreString()
        {
            var policyAbsenceReason = new PolicyAbsenceReason()
            {
                PolicyBehaviorType = PolicyBehavior.Ignored
            };

            Assert.IsTrue(policyAbsenceReason.ToString().Contains("Holiday will be completely ignored"));
        }

        [TestMethod]
        public void CheckFMLAPolicyBehaviourIgnoredNoLostTimeString()
        {
            var policyAbsenceReason = new PolicyAbsenceReason()
            {
                PolicyBehaviorType = PolicyBehavior.IgnoredNoLostTime
            };

            Assert.IsTrue(policyAbsenceReason.ToString().Contains("Holiday will not decrease the employee's scheduled time, however it does decrease the total time lost on that given holiday"));
        }

        [TestMethod]
        public void CheckFMLAPolicyBehaviourReducesWorkWeekString()
        {
            var policyAbsenceReason = new PolicyAbsenceReason()
            {
                PolicyBehaviorType = PolicyBehavior.ReducesWorkWeek
            };

            Assert.IsTrue(policyAbsenceReason.ToString().Contains("Holiday will reduce the amount of time used by removing the scheduled work day from the work schedule"));
        }

        private AppliedPolicy CreateAppliedPolicy(PolicyBehavior? policyBehavior = null ,PolicyBehavior? policyReasonBehavior =null )
        {
            var policy = new Policy()
            {
                PolicyBehaviorType=policyBehavior
            };

            var policyAbsenceReason = new PolicyAbsenceReason()
            {
                PolicyBehaviorType = policyReasonBehavior
            };
          
            var appliedPolicy = new AppliedPolicy()
            {
                Policy = policy,
                PolicyReason = policyAbsenceReason
            };

            return appliedPolicy;
        }

        [TestMethod]
        public void CheckAppliedPolicyBehaviourWithNull()
        {
            var appliedPolicy = CreateAppliedPolicy();
            Assert.AreEqual(appliedPolicy.PolicyBehaviorType,PolicyBehavior.ReducesWorkWeek);
        }

        [TestMethod]
        public void CheckAppliedPolicyBehaviourWithValueForBoth()
        {
            var appliedPolicy = CreateAppliedPolicy(PolicyBehavior.Ignored,PolicyBehavior.IgnoredNoLostTime);
            Assert.AreEqual(appliedPolicy.PolicyBehaviorType, PolicyBehavior.IgnoredNoLostTime);
        }

        [TestMethod]
        public void CheckAppliedPolicyBehaviourWithPolicyHasValue()
        {
            var appliedPolicy = CreateAppliedPolicy(PolicyBehavior.Ignored);
            Assert.AreEqual(appliedPolicy.PolicyBehaviorType, PolicyBehavior.Ignored);
        }
    }
}
