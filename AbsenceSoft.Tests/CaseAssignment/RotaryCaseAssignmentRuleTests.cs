﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.CaseAssignmentRules;
using AbsenceSoft.Logic.Cases;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using System;
using System.Linq;
using static AbsenceSoft.Tests.Services.Cases.CaseServiceTest;

namespace AbsenceSoft.Tests.CaseAssignment
{
    [TestClass]
    public class RotaryCaseAssignmentRuleTests
    {
        [TestMethod]
        public void TestRotaryCaseAssignment()
        {
            using (CaseContext context = new CaseContext(CaseType.Administrative, AppliedRuleEvalResult.Pass))
            using (CaseService svc = new CaseService())
            {
                context.Portal();

                // mimic case create in case service
                new CaseAssignee()
                {
                    CaseId = context.Case.Id,
                    Code = null,
                    CustomerId = context.Case.CustomerId,
                    Status = context.Case.Status,
                    UserId = context.Case.AssignedToId
                }.Save();

                // Get case assignee list
                var assigneeList = User.AsQueryable().Where(u => u.CustomerId == "000000000000000000000002").ToList();

                new RotaryRule().ApplyCaseAssignmentRules(ref assigneeList, context.Case, null);

                // Rotary should only have a single user to assign
                assigneeList.Should().HaveCount(1);
                // That assignment should never be the user who was just assigned "this" case (in context)
                assigneeList.First().Id.Should().NotBe(context.User.Id, "rotary assignment should not have chosen the same user already assigned to the case with others in the list");

                // Reassign the case to this user
                var user1 = User.GetById(assigneeList.First().Id);
                user1.Should().NotBeNull();
                svc.ReassignCase(context.Case, user1, null);

                // Now, while still keeping the other case "alive", create another case
                using (CaseContext nextContext = new CaseContext(CaseType.Administrative, AppliedRuleEvalResult.Pass))
                {
                    nextContext.Portal();

                    // mimic case create in case service
                    new CaseAssignee()
                    {
                        CaseId = nextContext.Case.Id,
                        Code = null,
                        CustomerId = nextContext.Case.CustomerId,
                        Status = nextContext.Case.Status,
                        UserId = nextContext.Case.AssignedToId
                    }.Save();

                    // Rebuild the case assignee list, all users, same implicit database directed order
                    var otherAssigneeList = User.AsQueryable().Where(u => u.CustomerId == "000000000000000000000002").ToList();

                    new RotaryRule().ApplyCaseAssignmentRules(ref otherAssigneeList, nextContext.Case, null);

                    // Rotary should only have a single user to assign
                    otherAssigneeList.Should().HaveCount(1);
                    // That assignment should never be the user who was just assigned "this" case (in context)
                    otherAssigneeList.First().Id.Should().NotBe(nextContext.User.Id);

                    // Reassign the next case to this user
                    var user2 = User.GetById(otherAssigneeList.First().Id);
                    user2.Should().NotBeNull();
                    svc.ReassignCase(nextContext.Case, user2, null);

                    user2.Id.Should().NotBe(user1.Id, "rotary assignment should have assigned to the next user in the list");
                }
            }
        }

        [TestMethod]
        public void PS1311_SelectMedicalRotaryCaseAssignmentToIndividualTest()
        {
            using (CaseContext context = new CaseContext(CaseType.Administrative, AppliedRuleEvalResult.Pass))
            using (CaseService svc = new CaseService())
            {
                context.Portal();

                // mimic case create in case service
                new CaseAssignee()
                {
                    CaseId = context.Case.Id,
                    Code = null,
                    CustomerId = context.Case.CustomerId,
                    Status = context.Case.Status,
                    UserId = context.Case.AssignedToId
                }.Save();

                // Get case assignee list
                var assigneeList = User.AsQueryable().Where(u => u.CustomerId == "000000000000000000000002").ToList();
                // Determine our random victim to force assignment to (rotary of 1)
                var targetAssignee = assigneeList.FirstOrDefault(a => a.Id != context.Case.AssignedToId);

                new RotaryRule().ApplyCaseAssignmentRules(ref assigneeList, context.Case, new CaseAssignmentRule()
                {
                    Name = "PS-1311 Default Assignment",
                    Description = "PS-1311 Select Medical Test",
                    CaseAssignmentType = CaseAssigmentType.TeamMember,
                    CaseAssignmentId = targetAssignee.Id,
                    Order = 5, // order doens't matter here, but it's what it's the prod DB for this rule
                    RuleType = ((int)CaseAssignmentRuleType.RotaryRule).ToString() // Rotary = "7"
                });

                // Rotary should only have a single user to assign
                assigneeList.Should().HaveCount(1);
                // It should actually choose the target user configured
                assigneeList.First().Id.Should().Be(targetAssignee.Id, "rotary assignment should have chosen the 1 user configured to be chosen");
            }
        }
    }
}
