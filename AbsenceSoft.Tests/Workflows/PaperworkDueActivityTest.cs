﻿using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Tests.Workflows
{
    [TestClass]
    public class PaperworkDueActivityTest : WorkflowTestsBase
    {
        [TestMethod]
        public void PaperworkDueNotCompletedTest()
        {
            using (CaseContext c = new CaseContext().Http())
            using (CommunicationService s = new CommunicationService())
            {
                var newDate = new DateTime(2099, 8, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                var comm = s.CreateCommunication(c.Case.Id, "ELGNOTICE", CommunicationType.Mail);
                comm = s.SendCommunication(comm, null, "ELGNOTICE");

                var todos = c.ToDos();
                var due = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.PaperworkDue
                 && t.Title == "Paperwork 'Certification for FMLA Employee Health Condition' Due");
                due.Should().NotBeNull();
                Assert.IsNotNull(comm.FirstPaperworkDueDate);

                due.Metadata.SetRawValue("NewDueDate", newDate);
                due.Metadata.SetRawValue("ExtendGracePeriod", true);
                due.Metadata.SetRawValue("PaperworkNotReceived", true);
                Guid? paperworkId = due.Metadata.GetRawValue<Guid?>("PaperworkId");
                due.WfOnToDoItemExtended(newDate, due.DueDate, c.User, PaperworkDueActivity.PaperworkNotReceivedOutcomeValue, true);

                List<Communication> communications = Communication.AsQueryable().Where(cm => cm.CaseId == c.Case.Id && cm.Paperwork != null && cm.Paperwork.Any()).ToList();

                communications.ForEach(cm => cm.Paperwork.ForEach(p =>
                {
                    if (p.Id == paperworkId)
                    {
                        Assert.IsTrue(p.DueDateHistory.Exists(pd => pd.DueDate == newDate));
                    }
                }));
            }

        }
    }
}
