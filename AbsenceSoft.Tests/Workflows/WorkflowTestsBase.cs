﻿using AbsenceSoft.Common;
using AbsenceSoft.Common.Properties;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Tasks;
using AbsenceSoft.Logic.Workflows;
using AbsenceSoft.Logic.Workflows.Activities;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AbsenceSoft.Tests.Workflows
{
    [TestClass]
    public class WorkflowTestsBase
    {
        const string CUSTOMER_ID = "000000000000000000000002"; // Test
        const string EMPLOYER_ID = "000000000000000000000002"; // Test
        const string EMPLOYEE_ID = "000000000000000000000003"; // FMLA Spouse at Same Employer Female
        static readonly DateTime START_DATE = new DateTime(2015, 8, 31, 0, 0, 0, 0, DateTimeKind.Utc);
        static readonly DateTime END_DATE = new DateTime(2015, 9, 18, 0, 0, 0, 0, DateTimeKind.Utc);
        const string REASON_ID = "000000000000000001000000"; // Employee Health Condition
        const string REASON_CODE = "EHC"; // Employee Health Condition
        const string USER_ID = "000000000000000000000002"; // qa@absencesoft.com
        const string RELAPSE_ID = "5c2dba9dafe9394c80aa7487";

        [TestMethod]
        public void CancelCaseWorkflowTest()
        {
            using (CaseContext c = new CaseContext())
            {
                var tdi = new ToDoService().CreateCaseReviewTodo(c.Case.Id, "TEST").Id;
                c.Case.Status = CaseStatus.Cancelled;
                c.Case.CancelReason = CaseCancelReason.Other;
                c.Case.Segments[0].Status = CaseStatus.Cancelled;
                c.Case.Save();
                c.Case.WfOnCaseCanceled(c.User);
                // Test ToDo status
                var todo = ToDoItem.GetById(tdi);
                todo.Should().NotBeNull();
                todo.Status.Should().Be(ToDoItemStatus.Cancelled);
                todo.ResultText.Should().Be("CASE CANCELLED");
            }
        }

        [TestMethod]
        public void CaseExtendedWorkflowTest()
        {
            using (CaseContext c = new CaseContext())
            {
                c.Case.WfOnCaseExtended(c.User);
                var todos = c.ToDos();
                todos.Count().Should().BeGreaterOrEqualTo(0);
            }
        }

        [TestMethod]
        public void ReopenCancelledCaseWorkflowTest()
        {
            using (CaseContext c = new CaseContext())
            {
                var tdi = new ToDoService().CreateCaseReviewTodo(c.Case.Id, "TEST").Id;
                c.Case.Status = CaseStatus.Cancelled;
                c.Case.CancelReason = CaseCancelReason.Other;
                c.Case.Segments[0].Status = CaseStatus.Cancelled;
                c.Case.Save();
                c.Case.WfOnCaseCanceled(c.User);
                // Test ToDo status is cancelled with result text
                var todo = ToDoItem.GetById(tdi);
                todo.Should().NotBeNull();
                todo.Status.Should().Be(ToDoItemStatus.Cancelled);
                todo.ResultText.Should().Be("CASE CANCELLED");

                // Reopen the case
                c.Case.Status = CaseStatus.Open;
                c.Case.CancelReason = null;
                c.Case.Segments[0].Status = CaseStatus.Open;
                c.Case.Save();
                c.Case.WfOnCaseReopened(c.User);

                // Re-get the ToDo and check that the status is pending w/o result text
                todo = ToDoItem.GetById(tdi);
                todo.Should().NotBeNull();
                todo.Status.Should().Be(ToDoItemStatus.Pending);
                todo.ResultText.Should().BeNullOrWhiteSpace();
            }
        }

        [TestMethod]
        public void ReopenClosedCaseWorkflowTest()
        {
            using (CaseContext c = new CaseContext())
            {
                var tdi = new ToDoService().CreateCaseReviewTodo(c.Case.Id, "TEST").Id;
                c.Case.Status = CaseStatus.Closed;
                c.Case.Segments[0].Status = CaseStatus.Closed;
                c.Case.Save();
                c.Case.WfOnCaseClosed(c.User);
                // Test ToDo status is cancelled with result text
                var todo = ToDoItem.GetById(tdi);
                todo.Should().NotBeNull();
                todo.Status.Should().Be(ToDoItemStatus.Cancelled);
                todo.ResultText.Should().Be("CASE CLOSED");

                // Reopen the case
                c.Case.Status = CaseStatus.Open;
                c.Case.CancelReason = null;
                c.Case.Segments[0].Status = CaseStatus.Open;
                c.Case.Save();
                c.Case.WfOnCaseReopened(c.User);

                // Re-get the ToDo and check that the status is pending w/o result text
                todo = ToDoItem.GetById(tdi);
                todo.Should().NotBeNull();
                todo.Status.Should().Be(ToDoItemStatus.Pending);
                todo.ResultText.Should().BeNullOrWhiteSpace();
            }
        }

        [TestMethod]
        public void StdCaseOpenedTest()
        {
            using (CaseContext c = new CaseContext())
            {
                Customer cust = Customer.GetById(CUSTOMER_ID);
                cust.Features = cust.Features ?? new List<AppFeature>(1);
                if (!cust.HasFeature(Feature.ShortTermDisability))
                {
                    cust.Features.Add(new AppFeature() { Feature = Feature.ShortTermDisability, Enabled = true });
                    cust.Save();
                }

                // Set all policies as STD policies, just for lolz
                c.Case.Segments[0].AppliedPolicies.ForEach(p => p.Policy.PolicyType = PolicyType.STD);
                c.Case.Save();
                c.Case.WfOnCaseCreated(c.User);

                var todos = c.ToDos();
                var stdDiagnosis = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.STDDiagnosis);
                stdDiagnosis.Should().NotBeNull();
                stdDiagnosis.Title.Should().Be("STD Diagnosis");
                stdDiagnosis.WfOnToDoItemCompleted(c.User, Activity.CompleteOutcomeValue, true);

                todos = c.ToDos();

                var stdHealthCondition = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.STDGenHealthCond);
                stdHealthCondition.Title.Should().Be("STD General Health Condition");
                stdHealthCondition.Metadata.SetRawValue("GeneralHealthCondition", "SomeGeneralHealthCondition");
                stdHealthCondition.WfOnToDoItemCompleted(c.User, Activity.CompleteOutcomeValue, true);
                c.Refresh();
                c.Case.Disability.GeneralHealthCondition.Should().Be("SomeGeneralHealthCondition");
                todos = c.ToDos();

                var stdFollowup = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.STDFollowUp);
                stdFollowup.Title.Should().Be("STD Follow Up");
                stdFollowup.Status = ToDoItemStatus.Complete;
                stdFollowup.WfOnToDoItemCompleted(c.User, StdFollowUpActivity.StdLeaveNotCompleteOutcome, true);

                todos = c.ToDos();

                var stdContactsEE = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.STDCaseManagerContactsEE);
                stdContactsEE.Title.Should().Be("STD Contact Employee");

                var stdContactsHCP = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.STDContactHCP);
                stdContactsHCP.Title.Should().Be("STD Contact HCP");

                var caseReview = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.CaseReview);
                caseReview.Title.Should().Be("RTW Prompt");
                caseReview.HideUntil.Should().Be(END_DATE.AddDays(-3));

                // Now Deny all of the STD
                using (var svc = new CaseService(false))
                    c.Case.Segments[0].AppliedPolicies.ForEach(p => svc.ApplyDetermination(c.Case, p.Policy.Code, p.StartDate, p.EndDate.Value, AdjudicationStatus.Denied, AdjudicationDenialReason.Other, "Dude!", AdjudicationDenialReason.OtherDescription));
                c.Case.Segments[0].AppliedPolicies[0].WfOnPolicyAdjudicated(c.Case, AdjudicationStatus.Denied, AdjudicationDenialReason.Other, "Dude!", AdjudicationDenialReason.OtherDescription, c.User);

                todos = c.ToDos();

                caseReview = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.CaseReview);
                caseReview.Title.Should().Be("RTW Prompt");
                caseReview.Status.Should().Be(ToDoItemStatus.Cancelled);

                // Now Approve all of the STD
                using (var svc = new CaseService(false))
                    c.Case.Segments[0].AppliedPolicies.ForEach(p => svc.ApplyDetermination(c.Case, p.Policy.Code, p.StartDate, p.EndDate.Value, AdjudicationStatus.Approved));
                c.Case.Segments[0].AppliedPolicies[0].WfOnPolicyAdjudicated(c.Case, AdjudicationStatus.Approved, null, null, null, c.User);

                todos = c.ToDos();

                stdContactsEE = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.STDCaseManagerContactsEE);
                stdContactsEE.Title.Should().Be("STD Contact Employee");

                stdContactsHCP = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.STDContactHCP);
                stdContactsHCP.Title.Should().Be("STD Contact HCP");

                caseReview = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.CaseReview);
                caseReview.Title.Should().Be("RTW Prompt");
                caseReview.HideUntil.Should().Be(END_DATE.AddDays(-3));

                // Now Deny all of the STD
                using (var svc = new CaseService(false))
                    c.Case.Segments[0].AppliedPolicies.ForEach(p => svc.ApplyDetermination(c.Case, p.Policy.Code, p.StartDate, p.EndDate.Value, AdjudicationStatus.Denied, AdjudicationDenialReason.Other, "Dude!", AdjudicationDenialReason.OtherDescription));
                c.Case.Segments[0].AppliedPolicies[0].WfOnPolicyAdjudicated(c.Case, AdjudicationStatus.Denied, AdjudicationDenialReason.Other, "Dude!", AdjudicationDenialReason.OtherDescription, c.User);

                todos = c.ToDos();

                caseReview = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.CaseReview);
                caseReview.Title.Should().Be("RTW Prompt");
                caseReview.Status.Should().Be(ToDoItemStatus.Cancelled);

                // Now Pend all of the STD
                using (var svc = new CaseService(false))
                    c.Case.Segments[0].AppliedPolicies.ForEach(p => svc.ApplyDetermination(c.Case, p.Policy.Code, p.StartDate, p.EndDate.Value, AdjudicationStatus.Pending));
                c.Case.Segments[0].AppliedPolicies[0].WfOnPolicyAdjudicated(c.Case, AdjudicationStatus.Pending, null, null, null, c.User);

                todos = c.ToDos();

                stdContactsEE = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.STDCaseManagerContactsEE);
                stdContactsEE.Title.Should().Be("STD Contact Employee");

                stdContactsHCP = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.STDContactHCP);
                stdContactsHCP.Title.Should().Be("STD Contact HCP");

                caseReview = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.CaseReview);
                caseReview.Title.Should().Be("RTW Prompt");
                caseReview.HideUntil.Should().Be(END_DATE.AddDays(-3));
            }
            using (CaseContext c = new CaseContext(CaseType.Intermittent))
            {
                Customer cust = Customer.GetById(CUSTOMER_ID);
                cust.Features = cust.Features ?? new List<AppFeature>(1);
                if (!cust.HasFeature(Feature.ShortTermDisability))
                {
                    cust.Features.Add(new AppFeature() { Feature = Feature.ShortTermDisability, Enabled = true });
                    cust.Save();
                }

                // Set all policies as STD policies, just for lolz
                c.Case.Segments[0].AppliedPolicies.ForEach(p => p.Policy.PolicyType = PolicyType.STD);
                c.Case.Save();
                c.Case.WfOnCaseCreated(c.User);

                var todos = c.ToDos();
                var stdDiagnosis = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.STDDiagnosis);
                stdDiagnosis.Should().NotBeNull();
                stdDiagnosis.Title.Should().Be("STD Diagnosis");
                stdDiagnosis.WfOnToDoItemCompleted(c.User, Activity.CompleteOutcomeValue, true);

                todos = c.ToDos();

                var stdHealthCondition = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.STDGenHealthCond);
                stdHealthCondition.Title.Should().Be("STD General Health Condition");
                stdHealthCondition.Metadata.SetRawValue("GeneralHealthCondition", "SomeGeneralHealthCondition");
                stdHealthCondition.WfOnToDoItemCompleted(c.User, Activity.CompleteOutcomeValue, true);
                c.Refresh();
                c.Case.Disability.GeneralHealthCondition.Should().Be("SomeGeneralHealthCondition");
                todos = c.ToDos();

                var stdFollowup = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.STDFollowUp);
                stdFollowup.Title.Should().Be("STD Follow Up");
                stdFollowup.Status = ToDoItemStatus.Complete;
                stdFollowup.WfOnToDoItemCompleted(c.User, StdFollowUpActivity.StdLeaveNotCompleteOutcome, true);

                todos = c.ToDos();

                var stdContactsEE = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.STDCaseManagerContactsEE);
                stdContactsEE.Title.Should().Be("STD Contact Employee");

                var stdContactsHCP = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.STDContactHCP);
                stdContactsHCP.Title.Should().Be("STD Contact HCP");

                var caseReview = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.CaseReview);
                caseReview.Title.Should().Be("RTW Prompt");
                caseReview.HideUntil.Should().Be(END_DATE.AddDays(-3));

                // Now Deny all of the STD
                using (var svc = new CaseService(false))
                    c.Case.Segments[0].AppliedPolicies.ForEach(p => svc.ApplyDetermination(c.Case, p.Policy.Code, p.StartDate, p.EndDate.Value, AdjudicationStatus.Denied, AdjudicationDenialReason.Other, "Dude!", AdjudicationDenialReason.OtherDescription));
                c.Case.Segments[0].AppliedPolicies[0].WfOnPolicyAdjudicated(c.Case, AdjudicationStatus.Denied, AdjudicationDenialReason.Other, "Dude!", AdjudicationDenialReason.OtherDescription, c.User);

                todos = c.ToDos();

                caseReview = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.CaseReview);
                caseReview.Title.Should().Be("RTW Prompt");
                caseReview.Status.Should().Be(ToDoItemStatus.Cancelled);

                // Now Approve all of the STD
                using (var svc = new CaseService(false))
                    c.Case.Segments[0].AppliedPolicies.ForEach(p => svc.ApplyDetermination(c.Case, p.Policy.Code, p.StartDate, p.EndDate.Value, AdjudicationStatus.Approved));
                c.Case.Segments[0].AppliedPolicies[0].WfOnPolicyAdjudicated(c.Case, AdjudicationStatus.Approved, null, null, null, c.User);

                todos = c.ToDos();

                stdContactsEE = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.STDCaseManagerContactsEE);
                stdContactsEE.Title.Should().Be("STD Contact Employee");

                stdContactsHCP = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.STDContactHCP);
                stdContactsHCP.Title.Should().Be("STD Contact HCP");

                caseReview = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.CaseReview);
                caseReview.Title.Should().Be("RTW Prompt");
                caseReview.HideUntil.Should().Be(END_DATE.AddDays(-3));

            }
        }

        [TestMethod]
        public void CaseClosedWorkflowTest()
        {
            using (CaseContext c = new CaseContext())
            {
                var tdi = new ToDoService().CreateCaseReviewTodo(c.Case.Id, "TEST").Id;
                new CaseService(true).Using(s => s.CaseClosed(c.Case, END_DATE.AddDays(1), CaseClosureReason.ReturnToWork));
                // Test ToDo status
                var todo = ToDoItem.GetById(tdi);
                todo.Should().NotBeNull();
                todo.Status.Should().Be(ToDoItemStatus.Cancelled);
                todo.ResultText.Should().Be("CASE CLOSED");
                ToDoItem.AsQueryable().Where(t => t.CaseId == c.Case.Id && t.ItemType == ToDoItemType.Communication && t.Status == ToDoItemStatus.Pending).Count().Should().Be(1);
            }
        }

        [TestMethod]
        public void CaseDeterminationApprovedWorkflowTest()
        {
            using(CaseContext c = new CaseContext())
            {
                c.Case.WfOnCaseAdjudicated(AdjudicationStatus.Approved, null, null, null);
                var todos = c.ToDos();
                todos.Count().Should().BeGreaterOrEqualTo(1);
                var approved = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.Communication && t.Title == "Send Designation Notice Leave Approved");
                approved.Should().NotBeNull();
            }
        }

        [TestMethod]
        public void CaseDeterminationDeniedWorkflowTest()
        {
            using (CaseContext c = new CaseContext())
            {
                /// Deny the case
                new CaseService(false).Using(s => s.ApplyDetermination(c.Case, null, START_DATE, END_DATE, AdjudicationStatus.Denied, AdjudicationDenialReason.NotASeriousHealthCondition, null, AdjudicationDenialReason.NotASeriousHealthConditionDescription));

                c.Case.WfOnCaseAdjudicated(AdjudicationStatus.Denied, AdjudicationDenialReason.NotASeriousHealthCondition, null, AdjudicationDenialReason.NotASeriousHealthConditionDescription);
                var todos = c.ToDos();
                todos.Count().Should().BeGreaterOrEqualTo(1);
                var approved = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.Communication && t.Title == "Send Designation Notice Leave Denied");
                approved.Should().NotBeNull();
            }
        }

        [TestMethod]
        public void AccommodationRequestedWorkflowTest()
        {
            using (CaseContext c = new CaseContext())
            {
                Accommodation theAccommodation = BuildAccommodation(c.Case, "OTR");
                c.Case.Save();
                theAccommodation.WfOnAccommodationCreated(c.Case, c.User);

                var todos = c.ToDos();
                todos.Count().Should().BeGreaterOrEqualTo(1);
                var todo = todos.Where(t => t.ItemType == ToDoItemType.Communication);
                todo.Any(t => t.Title == "Send Accommodation Acknowledgement Letter to Employee").Should().BeTrue();
            }
        }

        [TestMethod]
        public void ErgonomicAssessmentRequestedAndCompletedWorkflowTest()
        {
            using (CaseContext c = new CaseContext())
            {
                Accommodation theAccommodation = BuildAccommodation(c.Case, "EA");
                c.Case.Save();
                theAccommodation.WfOnAccommodationCreated(c.Case, c.User);

                var todos = c.ToDos();
                var scheduleErgonomicAssessmentTodo = todos.First(t => t.ItemType != ToDoItemType.Communication);
                scheduleErgonomicAssessmentTodo.Title.Should().Be("Schedule Ergonomic Assessment");
                scheduleErgonomicAssessmentTodo.ItemType.Should().Be(ToDoItemType.ScheduleErgonomicAssessment);
                scheduleErgonomicAssessmentTodo.Metadata.SetRawValue("ErgonomicAssessmentDate", END_DATE.AddDays(-1));
                scheduleErgonomicAssessmentTodo.WfOnToDoItemCompleted(c.User, Activity.CompleteOutcomeValue, true);

                todos = c.ToDos();
                var completeErgonomicAssessmentTodo = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.CompleteErgonomicAssessment);
                completeErgonomicAssessmentTodo.Title.Should().Be("Complete Ergonomic Assessment");
                completeErgonomicAssessmentTodo.DueDate.Should().Be(END_DATE.AddDays(-1));
                completeErgonomicAssessmentTodo.WfOnToDoItemCompleted(c.User, CompleteErgonomicAssessmentActivity.ErgonomicAssessmentCompletedOutcome, true);
                todos = ToDoItem.AsQueryable().Where(t => t.CaseId == c.Case.Id).ToList();
                var reviewErgonomicAssessmentTodo = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.CaseReview);
                reviewErgonomicAssessmentTodo.Title.Should().Be("Perform Accommodation Determination");
            }
        }

        [TestMethod]
        public void ErgonomicAssessmentRequestedAndNotCompletedWorkflowTest()
        {
            using (CaseContext c = new CaseContext())
            {
                Accommodation theAccommodation = BuildAccommodation(c.Case, "EA");
                c.Case.Save();
                theAccommodation.WfOnAccommodationCreated(c.Case, c.User);

                var todos = c.ToDos();
                var scheduleErgonomicAssessmentTodo = todos.FirstOrDefault(t => t.ItemType != ToDoItemType.Communication);
                scheduleErgonomicAssessmentTodo.Should().NotBeNull();
                scheduleErgonomicAssessmentTodo.Title.Should().Be("Schedule Ergonomic Assessment");
                scheduleErgonomicAssessmentTodo.WfOnToDoItemCompleted(c.User, Activity.CompleteOutcomeValue, true);

                todos = ToDoItem.AsQueryable().Where(t => t.CaseId == c.Case.Id).ToList();
                var completeErgonomicAssessmentTodo = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.CompleteErgonomicAssessment);
                completeErgonomicAssessmentTodo.Title.Should().Be("Complete Ergonomic Assessment");
                completeErgonomicAssessmentTodo.WfOnToDoItemCompleted(c.User, CompleteErgonomicAssessmentActivity.ErgonomicAssessmentNotCompletedOutcome, true);
                todos = c.ToDos();
                todos.Count().Should().BeGreaterOrEqualTo(4);
                var reviewErgonomicAssessmentTodo = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.CaseReview);
                reviewErgonomicAssessmentTodo.Title.Should().Be("Perform Accommodation Determination, No ergonomic assessment");
            }
        }

        [TestMethod]
        public void EquipmentOrSoftwareRequestedWorkflowTest()
        {
            using (CaseContext c = new CaseContext())
            {
                Accommodation theAccommodation = BuildAccommodation(c.Case, "EOS");
                c.Case.Save();
                theAccommodation.WfOnAccommodationCreated(c.Case, c.User);
                var todos = c.ToDos();
                var orderEquipmentOrSoftwareToDo = todos.FirstOrDefault(t => t.ItemType != ToDoItemType.Communication);
                orderEquipmentOrSoftwareToDo.Title.Should().Be("Accommodation Equipment / Software Ordered");
                orderEquipmentOrSoftwareToDo.ItemType.Should().Be(ToDoItemType.EquipmentSoftwareOrdered);
                orderEquipmentOrSoftwareToDo.WfOnToDoItemCompleted(c.User, Activity.CompleteOutcomeValue, true);
                todos = c.ToDos();
                var completeErgonomicAssessmentTodo = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.EquipmentSoftwareInstalled);
                completeErgonomicAssessmentTodo.Should().NotBeNull();
                completeErgonomicAssessmentTodo.Title.Should().Be("Accommodation Equipment / Software Installed");
            }
        }

        [TestMethod]
        public void CasePassesWhenCaseHasAccommodationThatHasFirstTemporaryAndLastPermanentDurations()
        {
            using (var workflowService = new WorkflowService())
            {
                var expressionGroups = workflowService.GetExpressionsByEventType(EventType.CaseCreated);
                var caseGroup = expressionGroups.FirstOrDefault(eg => eg.Title == "Case Information");
                caseGroup.Should().NotBeNull();
                var firstCreatedAccommodationDurationExpression = caseGroup.Expressions.FirstOrDefault(e => e.Prompt == "First Created Accommodation Duration");
                firstCreatedAccommodationDurationExpression.Should().NotBeNull();
                firstCreatedAccommodationDurationExpression.Value = AccommodationDuration.Temporary;
                var caseShouldBeCreatedWhenFirstAccommodationIsTemporary = (Rule)workflowService.GetRule(firstCreatedAccommodationDurationExpression);
                var lastCreatedAccommodationDurationExpression = caseGroup.Expressions.FirstOrDefault(e => e.Prompt == "Last Created Accommodation Duration");
                lastCreatedAccommodationDurationExpression.Should().NotBeNull();
                lastCreatedAccommodationDurationExpression.Value = AccommodationDuration.Permanent;
                var caseShouldBeCreatedWhenLastAccommodationIsPermanent = (Rule)workflowService.GetRule(lastCreatedAccommodationDurationExpression);
                var rules = new List<Rule> { caseShouldBeCreatedWhenFirstAccommodationIsTemporary, caseShouldBeCreatedWhenLastAccommodationIsPermanent };

                var reviewTheCase = new WorkflowActivity
                {
                    Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"),
                    Name = "Review The Case",
                    ActivityId = new CaseReviewActivity().Id
                };

                reviewTheCase.Metadata.Set("Title", "TemporaryAndPermanentAccommodationEnvolvedCase");

                var workFlow = new Workflow
                {
                    Code = "TEMPORARYANDPERMANENTACCOMRELATEDCASECREATED",
                    Name = "Temporary And Permanent Accommodation Related Case Created",
                    Description = "Test workflow that should fire when a temporary accommodation and permanent related case is created",
                    TargetEventType = EventType.CaseCreated,
                    Criteria = rules,
                    Activities = new List<WorkflowActivity>
                    {
                        reviewTheCase
                    }
                };

                workFlow.Save();

                using (var caseContext = new CaseContext(CaseType.Intermittent))
                {
                    var theFirstAccommodation = new Accommodation
                    {
                        Type = new AccommodationType { Code = "EA", Name = "EA", Id = ObjectId.Empty.ToString() },
                        Usage = new List<AccommodationUsage>(1)
                        {
                            new AccommodationUsage { StartDate = START_DATE, EndDate = END_DATE, Determination = AdjudicationStatus.Pending }
                        },
                        Duration = AccommodationDuration.Temporary,
                        Status = CaseStatus.Open,
                        CreatedDate = DateTime.UtcNow
                    };
                    var theLastAccommodation = new Accommodation
                    {
                        Type = new AccommodationType { Code = "EA", Name = "EA", Id = ObjectId.Empty.ToString() },
                        Usage = new List<AccommodationUsage>(1)
                        {
                            new AccommodationUsage { StartDate = START_DATE, EndDate = END_DATE, Determination = AdjudicationStatus.Pending }
                        },
                        Duration = AccommodationDuration.Permanent,
                        Status = CaseStatus.Open,
                        CreatedDate = DateTime.UtcNow
                    };

                    var accommRequest = new AccommodationRequest
                    {
                        Accommodations = new List<Accommodation>(2) { theFirstAccommodation, theLastAccommodation },
                        CaseId = caseContext.Case.Id,
                        CaseNumber = caseContext.Case.CaseNumber,
                        Status = CaseStatus.Open
                    };
                    caseContext.Case.AccommodationRequest = accommRequest;
                    caseContext.Case.IsAccommodation = true;
                    caseContext.Case.Save();
                    caseContext.Case.WfOnCaseCreated();
                    theFirstAccommodation.WfOnAccommodationCreated(caseContext.Case, caseContext.User);
                    theLastAccommodation.WfOnAccommodationCreated(caseContext.Case, caseContext.User);

                    var todos = caseContext.ToDos();
                    todos.Should().NotBeEmpty();
                    var caseReviewTodo = todos.FirstOrDefault(p => p.ItemType == ToDoItemType.CaseReview);
                    caseReviewTodo.Should().NotBeNull();
                }

                workFlow.Delete();
            }
        }

        [TestMethod]
        public void CasePassesWhenCaseHasAccommodationsThatAreAllPermanent()
        {
            using (var workflowService = new WorkflowService())
            {
                var expressionGroups = workflowService.GetExpressionsByEventType(EventType.CaseCreated);
                var caseGroup = expressionGroups.FirstOrDefault(eg => eg.Title == "Case Information");
                caseGroup.Should().NotBeNull();

                var hasAccommodationsExpression = caseGroup.Expressions.FirstOrDefault(e => e.Prompt == "Does Case Have Accommodations");
                hasAccommodationsExpression.Should().NotBeNull();
                var caseShouldBeCreatedWhenItHasAccomodations = (Rule)workflowService.GetRule(hasAccommodationsExpression);

                var allCaseAccommodationDurationsArePermanentExpression = caseGroup.Expressions.FirstOrDefault(e => e.Prompt == "Is All Case Accommodation Durations Permanent");
                allCaseAccommodationDurationsArePermanentExpression.Should().NotBeNull();
                var caseShouldBeCreatedWhenAllAccommodationsIsPermanent = (Rule)workflowService.GetRule(allCaseAccommodationDurationsArePermanentExpression);

                var rules = new List<Rule> { caseShouldBeCreatedWhenItHasAccomodations, caseShouldBeCreatedWhenAllAccommodationsIsPermanent };

                var accommodationExpressionGroups = workflowService.GetExpressionsByEventType(EventType.AccommodationCreated);
                var accommodationExpressionGroup = accommodationExpressionGroups.FirstOrDefault(eg => eg.Title == "Accommodation");
                accommodationExpressionGroup.Should().NotBeNull();

                var accommodationDurationExpression = accommodationExpressionGroup.Expressions.FirstOrDefault(e => e.Prompt == "Accommodation Duration");
                accommodationDurationExpression.Should().NotBeNull();
                accommodationDurationExpression.Value = AccommodationDuration.Permanent;
                var accommodationShuldBeCreatedWhenItHasAccomodationThatIsPermenant = (Rule)workflowService.GetRule(accommodationDurationExpression);

                var accommodationCreatedRules = new List<Rule> { accommodationShuldBeCreatedWhenItHasAccomodationThatIsPermenant };

                var reviewTheCase = new WorkflowActivity
                {
                    Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"),
                    Name = "Review The Case",
                    ActivityId = new CaseReviewActivity().Id
                };

                reviewTheCase.Metadata.Set("Title", "PermanentAccommodationEnvolvedCase");

                var workFlow = new Workflow
                {
                    Code = "PERMANENTACCOMRELATEDCASECREATED",
                    Name = "Permanent Accommodation Related Case Created",
                    Description = "Test workflow that should fire when a permenant accommodation related case is created",
                    TargetEventType = EventType.CaseCreated,
                    Criteria = rules,
                    Activities = new List<WorkflowActivity>
                    {
                        reviewTheCase
                    }
                };

                workFlow.Save();

                var scheduleErgonomicAssessmentActivity = new WorkflowActivity
                {
                    Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A72"),
                    Name = "Schedule Ergonomic Assessment",
                    ActivityId = new ScheduleErgonomicAssessmentActivity().Id
                };

                scheduleErgonomicAssessmentActivity.Metadata.Set("Title", "ScheduleErgonomicAssessment");

                var permanentAccommodationWorkFlow = new Workflow
                {
                    Code = "PERMANENTACCOMRELATEDCREATED",
                    Name = "Permanent Accommodation Related Created",
                    Description = "Test workflow that should fire when a permentant accommodation related case is created",
                    TargetEventType = EventType.AccommodationCreated,
                    Criteria = accommodationCreatedRules,
                    Activities = new List<WorkflowActivity>
                    {
                        scheduleErgonomicAssessmentActivity
                    }
                };

                permanentAccommodationWorkFlow.Save();

                using (var caseContext = new CaseContext(CaseType.Intermittent))
                {
                    var theFirstAccommodation = new Accommodation
                    {
                        Type = new AccommodationType { Code = "EA", Name = "EA", Id = ObjectId.Empty.ToString() },
                        Usage = new List<AccommodationUsage>(1)
                        {
                            new AccommodationUsage { StartDate = START_DATE, EndDate = END_DATE, Determination = AdjudicationStatus.Pending }
                        },
                        Duration = AccommodationDuration.Permanent,
                        Status = CaseStatus.Open,
                        CreatedDate = DateTime.UtcNow
                    };
                    var theLastAccommodation = new Accommodation
                    {
                        Type = new AccommodationType { Code = "EA", Name = "EA", Id = ObjectId.Empty.ToString() },
                        Usage = new List<AccommodationUsage>(1)
                        {
                            new AccommodationUsage { StartDate = START_DATE, EndDate = END_DATE, Determination = AdjudicationStatus.Pending }
                        },
                        Duration = AccommodationDuration.Permanent,
                        Status = CaseStatus.Open,
                        CreatedDate = DateTime.UtcNow
                    };

                    var accommRequest = new AccommodationRequest
                    {
                        Accommodations = new List<Accommodation>(2) { theFirstAccommodation, theLastAccommodation },
                        CaseId = caseContext.Case.Id,
                        CaseNumber = caseContext.Case.CaseNumber,
                        Status = CaseStatus.Open
                    };
                    caseContext.Case.AccommodationRequest = accommRequest;
                    caseContext.Case.IsAccommodation = true;
                    caseContext.Case.Save();
                    caseContext.Case.WfOnCaseCreated();
                    theFirstAccommodation.WfOnAccommodationCreated(caseContext.Case, caseContext.User);
                    theLastAccommodation.WfOnAccommodationCreated(caseContext.Case, caseContext.User);

                    var todos = caseContext.ToDos();
                    todos.Should().NotBeEmpty();
                    var caseReviewTodo = todos.FirstOrDefault(p => p.ItemType == ToDoItemType.CaseReview);
                    caseReviewTodo.Should().NotBeNull();
                    var scheduleErgonomicAssessmentTodo = todos.FirstOrDefault(p => p.ItemType == ToDoItemType.ScheduleErgonomicAssessment);
                    scheduleErgonomicAssessmentTodo.Should().NotBeNull();
                }

                workFlow.Delete();
                permanentAccommodationWorkFlow.Delete();
            }
        }

        [TestMethod]
        public void CasePassesWhenCaseHasAccommodationsThatAreAllTemporary()
        {
            using (var workflowService = new WorkflowService())
            {
                var expressionGroups = workflowService.GetExpressionsByEventType(EventType.CaseCreated);
                var caseGroup = expressionGroups.FirstOrDefault(eg => eg.Title == "Case Information");
                caseGroup.Should().NotBeNull();

                var hasAccommodationsExpression = caseGroup.Expressions.FirstOrDefault(e => e.Prompt == "Does Case Have Accommodations");
                hasAccommodationsExpression.Should().NotBeNull();
                var caseShouldBeCreatedWhenItHasAccomodations = (Rule)workflowService.GetRule(hasAccommodationsExpression);

                var allCaseAccommodationDurationsAreTemporaryExpression = caseGroup.Expressions.FirstOrDefault(e => e.Prompt == "Is All Case Accommodation Durations Temporary");
                allCaseAccommodationDurationsAreTemporaryExpression.Should().NotBeNull();
                var caseShouldBeCreatedWhenAllAccommodationsIsTemporary = (Rule)workflowService.GetRule(allCaseAccommodationDurationsAreTemporaryExpression);

                var rules = new List<Rule> { caseShouldBeCreatedWhenItHasAccomodations, caseShouldBeCreatedWhenAllAccommodationsIsTemporary };

                var accommodationExpressionGroups = workflowService.GetExpressionsByEventType(EventType.AccommodationCreated);
                var accommodationExpressionGroup = accommodationExpressionGroups.FirstOrDefault(eg => eg.Title == "Accommodation");
                accommodationExpressionGroup.Should().NotBeNull();

                var accommodationDurationExpression = accommodationExpressionGroup.Expressions.FirstOrDefault(e => e.Prompt == "Accommodation Duration");
                accommodationDurationExpression.Should().NotBeNull();
                accommodationDurationExpression.Value = AccommodationDuration.Temporary;
                var accommodationShuldBeCreatedWhenItHasAccomodationThatIsTemporary = (Rule)workflowService.GetRule(accommodationDurationExpression);

                var accommodationCreatedRules = new List<Rule> { accommodationShuldBeCreatedWhenItHasAccomodationThatIsTemporary };

                var reviewTheCase = new WorkflowActivity
                {
                    Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"),
                    Name = "Review The Case",
                    ActivityId = new CaseReviewActivity().Id
                };

                reviewTheCase.Metadata.Set("Title", "TemporaryAccommodationEnvolvedCase");

                var workFlow = new Workflow
                {
                    Code = "TEMPORARYACCOMRELATEDCASECREATED",
                    Name = "Temporary Accommodation Related Case Created",
                    Description = "Test workflow that should fire when a temporary accommodation related case is created",
                    TargetEventType = EventType.CaseCreated,
                    Criteria = rules,
                    Activities = new List<WorkflowActivity>
                    {
                        reviewTheCase
                    }
                };

                workFlow.Save();

                var scheduleErgonomicAssessmentActivity = new WorkflowActivity
                {
                    Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A72"),
                    Name = "Schedule Ergonomic Assessment",
                    ActivityId = new ScheduleErgonomicAssessmentActivity().Id
                };

                scheduleErgonomicAssessmentActivity.Metadata.Set("Title", "ScheduleErgonomicAssessment");

                var temporaryAccommodationWorkFlow = new Workflow
                {
                    Code = "TEMPORARYACCOMRELATEDCREATED",
                    Name = "Temporary Accommodation Related Created",
                    Description = "Test workflow that should fire when a temporary accommodation related case is created",
                    TargetEventType = EventType.AccommodationCreated,
                    Criteria = accommodationCreatedRules,
                    Activities = new List<WorkflowActivity>
                    {
                        scheduleErgonomicAssessmentActivity
                    }
                };

                temporaryAccommodationWorkFlow.Save();

                using (var caseContext = new CaseContext(CaseType.Intermittent))
                {
                    var theFirstAccommodation = new Accommodation
                    {
                        Type = new AccommodationType { Code = "EA", Name = "EA", Id = ObjectId.Empty.ToString() },
                        Usage = new List<AccommodationUsage>(1)
                        {
                            new AccommodationUsage { StartDate = START_DATE, EndDate = END_DATE, Determination = AdjudicationStatus.Pending }
                        },
                        Duration = AccommodationDuration.Temporary,
                        Status = CaseStatus.Open,
                        CreatedDate = DateTime.UtcNow
                    };
                    var theLastAccommodation = new Accommodation
                    {
                        Type = new AccommodationType { Code = "EA", Name = "EA", Id = ObjectId.Empty.ToString() },
                        Usage = new List<AccommodationUsage>(1)
                        {
                            new AccommodationUsage { StartDate = START_DATE, EndDate = END_DATE, Determination = AdjudicationStatus.Pending }
                        },
                        Duration = AccommodationDuration.Temporary,
                        Status = CaseStatus.Open,
                        CreatedDate = DateTime.UtcNow
                    };

                    var accommRequest = new AccommodationRequest
                    {
                        Accommodations = new List<Accommodation>(2) { theFirstAccommodation, theLastAccommodation },
                        CaseId = caseContext.Case.Id,
                        CaseNumber = caseContext.Case.CaseNumber,
                        Status = CaseStatus.Open
                    };
                    caseContext.Case.AccommodationRequest = accommRequest;
                    caseContext.Case.IsAccommodation = true;
                    caseContext.Case.Save();
                    caseContext.Case.WfOnCaseCreated();
                    theFirstAccommodation.WfOnAccommodationCreated(caseContext.Case, caseContext.User);
                    theLastAccommodation.WfOnAccommodationCreated(caseContext.Case, caseContext.User);

                    var todos = caseContext.ToDos();
                    todos.Should().NotBeEmpty();
                    var caseReviewTodo = todos.FirstOrDefault(p => p.ItemType == ToDoItemType.CaseReview);
                    caseReviewTodo.Should().NotBeNull();
                    var scheduleErgonomicAssessmentTodo = todos.FirstOrDefault(p => p.ItemType == ToDoItemType.ScheduleErgonomicAssessment);
                    scheduleErgonomicAssessmentTodo.Should().NotBeNull();
                }

                workFlow.Delete();
                temporaryAccommodationWorkFlow.Delete();
            }
        }

        [TestMethod, Ignore]
        public void EmployeeInfoChanged()
        {
            using (CaseContext c = new CaseContext().SelfService())
            {
                c.Case.Employee.WfOnEmployeeChanged(c.User);

                var todos = c.ToDos();
                todos.Count().Should().BeGreaterOrEqualTo(1);
                var reviewEmployeeInfoTodo = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.ReviewEmployeeInformation);
                reviewEmployeeInfoTodo.Should().NotBeNull();
                reviewEmployeeInfoTodo.Title.Should().Be("Review Employee Information Changed By " + c.User.DisplayName);
            }
        }

        [TestMethod, Ignore]
        public void EmployeeWorkScheduleReview()
        {
            using (CaseContext c = new CaseContext().SelfService())
            {
                Schedule schedule = new Schedule();
                c.Case.Employee.WfOnWorkScheduleChanged(schedule, c.User);

                var todos = c.ToDos();
                todos.Count().Should().BeGreaterOrEqualTo(1);
                var reviewEmployeeWorkScheduleTodo = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.ReviewWorkSchedule);
                reviewEmployeeWorkScheduleTodo.Should().NotBeNull();
                reviewEmployeeWorkScheduleTodo.Title.Should().Be("Review Work Schedule Of TestFMLA, SpouseAtLocF");
            }
        }

        [TestMethod]
        public void CaseAttachmentCreated()
        {
            using (CaseContext c = new CaseContext().SelfService())
            {
                Attachment a = new Attachment();
                a.AttachmentType = AttachmentType.Communication;
                a.FileName = "Test Attachment";
                a.CaseId = c.Case.Id;
                a.CustomerId = c.Case.CustomerId;
                a.EmployerId = c.Case.EmployerId;
                a.EmployeeId = c.Case.Employee.Id;
                a.Save();
                a.WfOnAttachmentCreated(c.User);

                var todos = c.ToDos();
                todos.Count().Should().BeGreaterOrEqualTo(1);
                var reviewCaseAttachmentToDo = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.ReviewCaseAttachment);
                reviewCaseAttachmentToDo.Should().NotBeNull();
                reviewCaseAttachmentToDo.Title.Should().Be("Review Case Attachment 'Test Attachment', uploaded by TestFMLA, SpouseAtLocF");
            }
        }

        [TestMethod]
        public void AccommodationDeterminationTest()
        {
            using (CaseContext c = new CaseContext())
            {
                var accom = BuildAccommodation(c.Case, "LEV", "Leave");
                new AccommodationService().Using(s => s.ApplyDetermination(c.Case, START_DATE, END_DATE, AdjudicationStatus.Approved, null, null, null, accom.Id));
                c.Case.Save();
                var todos = c.ToDos();
                todos.Where(t => t.ItemType == ToDoItemType.Communication).Count().Should().BeGreaterOrEqualTo(4);
                todos.Where(t => t.Metadata.GetRawValue<string>("Template") == "ACCOM-APPROVED").Should().HaveCount(1);
                todos.Where(t => t.Metadata.GetRawValue<string>("Template") == "ACCOM-APPROVED-HR").Should().HaveCount(1);
                todos.Where(t => t.Metadata.GetRawValue<string>("Template") == "ACCOM-APPROVED-SUP").Should().HaveCount(1);
                todos.Where(t => t.Metadata.GetRawValue<string>("Template") == "MGRNOTICE").Should().HaveCount(1);
            }
            using (CaseContext c = new CaseContext())
            {
                var accom = BuildAccommodation(c.Case, "LEV", "Leave");
                new AccommodationService().Using(s => s.ApplyDetermination(c.Case, START_DATE, END_DATE, AdjudicationStatus.Denied, AccommodationAdjudicationDenialReason.NotAReasonableRequest, null, AccommodationAdjudicationDenialReason.NotAReasonableRequestDescription, accom.Id));
                c.Case.Save();
                var todos = c.ToDos();
                todos.Where(t => t.ItemType == ToDoItemType.Communication).Count().Should().BeGreaterOrEqualTo(2);
                todos.Where(t => t.Metadata.GetRawValue<string>("Template") == "ACCOM-DENIED").Should().HaveCount(1);
                todos.Where(t => t.Metadata.GetRawValue<string>("Template") == "MGRNOTICE").Should().HaveCount(1);
            }
            using (CaseContext c = new CaseContext())
            {
                var accom = BuildAccommodation(c.Case, "EOS", "Equipment Or Software");
                new AccommodationService().Using(s => s.ApplyDetermination(c.Case, START_DATE, END_DATE, AdjudicationStatus.Approved, null, null, null, accom.Id));
                c.Case.Save();
                var todos = c.ToDos();
                todos.Where(t => t.ItemType == ToDoItemType.Communication).Count().Should().BeGreaterOrEqualTo(4);
                todos.Where(t => t.Metadata.GetRawValue<string>("Template") == "ACCOM-APPROVED").Should().HaveCount(1);
                todos.Where(t => t.Metadata.GetRawValue<string>("Template") == "ACCOM-APPROVED-HR").Should().HaveCount(1);
                todos.Where(t => t.Metadata.GetRawValue<string>("Template") == "ACCOM-APPROVED-SUP").Should().HaveCount(1);
                todos.Where(t => t.Metadata.GetRawValue<string>("Template") == "MGRNOTICE").Should().HaveCount(1);
                todos.Where(t => t.ItemType == ToDoItemType.EquipmentSoftwareOrdered).Should().HaveCount(1);
            }
            using (CaseContext c = new CaseContext())
            {
                var accom = BuildAccommodation(c.Case, "SC", "Schedule Change");
                new AccommodationService().Using(s => s.ApplyDetermination(c.Case, START_DATE, END_DATE, AdjudicationStatus.Approved, null, null, null, accom.Id));
                c.Case.Save();
                var todos = c.ToDos();
                todos.Where(t => t.ItemType == ToDoItemType.Communication).Count().Should().BeGreaterOrEqualTo(4);
                todos.Where(t => t.Metadata.GetRawValue<string>("Template") == "ACCOM-APPROVED").Should().HaveCount(1);
                todos.Where(t => t.Metadata.GetRawValue<string>("Template") == "ACCOM-APPROVED-HR").Should().HaveCount(1);
                todos.Where(t => t.Metadata.GetRawValue<string>("Template") == "ACCOM-APPROVED-SUP").Should().HaveCount(1);
                todos.Where(t => t.Metadata.GetRawValue<string>("Template") == "MGRNOTICE").Should().HaveCount(1);
                todos.Where(t => t.ItemType == ToDoItemType.HRISScheduleChange).Should().HaveCount(1);
            }
            using (CaseContext c = new CaseContext())
            {
                var accom = BuildAccommodation(c.Case, "OTR", "Other");
                new AccommodationService().Using(s => s.ApplyDetermination(c.Case, START_DATE, END_DATE, AdjudicationStatus.Approved, null, null, null, accom.Id));
                c.Case.Save();
                var todos = c.ToDos();
                todos.Where(t => t.ItemType == ToDoItemType.Communication).Count().Should().BeGreaterOrEqualTo(4);
                todos.Where(t => t.Metadata.GetRawValue<string>("Template") == "ACCOM-APPROVED").Should().HaveCount(1);
                todos.Where(t => t.Metadata.GetRawValue<string>("Template") == "ACCOM-APPROVED-HR").Should().HaveCount(1);
                todos.Where(t => t.Metadata.GetRawValue<string>("Template") == "ACCOM-APPROVED-SUP").Should().HaveCount(1);
                todos.Where(t => t.Metadata.GetRawValue<string>("Template") == "MGRNOTICE").Should().HaveCount(1);
                todos.Where(t => t.ItemType == ToDoItemType.OtherAccommodation).Should().HaveCount(1);
            }
        }

        [TestMethod]
        public void CaseCreatedNeedsRecertificationTest()
        {
            using (CaseContext c = new CaseContext())
            {
                c.Case.WfOnCaseCreated(c.User);
                var todos = c.ToDos();
                todos.Where(t => t.ItemType == ToDoItemType.Communication && t.Metadata.GetRawValue<string>("Template") == "ELGNOTICE").Should().HaveCount(1, "should have ELGNOTICE todo");
                todos.Where(t => t.ItemType == ToDoItemType.Communication && t.Metadata.GetRawValue<string>("Template") == "HRNOTICE").Should().HaveCount(1, "should have HRNOTICE todo");
                todos.Where(t => t.ItemType == ToDoItemType.Communication && t.Metadata.GetRawValue<string>("Template") == "MGRNOTICE").Should().HaveCount(1, "should have MGRNOTICE todo");

                var rtw = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.ReturnToWork);
                rtw.Should().NotBeNull();
                var evt = c.Case.FindCaseEvent(CaseEventType.EstimatedReturnToWork);
                evt.Should().NotBeNull();

                rtw.WfOnToDoItemCompleted(c.User, ReturnToWorkActivity.RecertifyOutcomeValue, true);

                todos = c.ToDos();
                todos.Where(t => t.ItemType == ToDoItemType.Communication && t.Metadata.GetRawValue<string>("Template") == "RECERT").Should().HaveCount(1, "should have RECERT todo");
            }
        }

        [TestMethod]
        public void CaseCreatedNotReturningToWork()
        {
            using (CaseContext c = new CaseContext())
            {
                c.Case.WfOnCaseCreated(c.User);
                var todos = c.ToDos();
                var rtw = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.ReturnToWork);
                rtw.Should().NotBeNull();
                rtw.WfOnToDoItemCompleted(c.User, ReturnToWorkActivity.NotReturningToWorkOutcomeValue, true);

                todos = c.ToDos();
                var notReturningToWork = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.CloseCase && t.Title == "Close Case, Employee Will Not Return To Work");
                notReturningToWork.Should().NotBeNull();
            }
        }

        [TestMethod]
        public void CaseCreatedReturnedToWork()
        {
            using (CaseContext c = new CaseContext())
            {
                c.Case.WfOnCaseCreated(c.User);
                var todos = c.ToDos();
                var rtw = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.ReturnToWork);
                rtw.DueDate.Should().Be(END_DATE);
                rtw.WfOnToDoItemCompleted(c.User, ReturnToWorkActivity.ReturnToWorkOutcomeValue, true);

                todos = c.ToDos();
                var verifyRTW = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.VerifyReturnToWork);
                verifyRTW.Should().NotBeNull();
                verifyRTW.Title.Should().Be("Verify Return To Work");

                verifyRTW.Metadata.SetRawValue("ReturnToWorkDate", END_DATE);
                verifyRTW.WfOnToDoItemCompleted(c.User,VerifyReturnToWorkActivity.RtwVerifiedYesOutcome, true);

                c.Refresh();
                var evt = c.Case.FindCaseEvent(CaseEventType.ReturnToWork);
                evt.Should().NotBeNull();
                evt.EventDate.Should().Be(END_DATE,  "Return to Work is due on day case ends");

                todos = c.ToDos();
                var returnToWork = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.CloseCase && t.Title == "Employee Returned To Work, Close Case");
                returnToWork.Should().NotBeNull();

            }
        }

        [TestMethod]
        public void CaseCreatedIneligible()
        {
            using (CaseContext c = new CaseContext())
            {
                foreach (AppliedPolicy ap in c.Case.Segments.SelectMany(s => s.AppliedPolicies))
                {
                    ap.Status = EligibilityStatus.Ineligible;
                }
                c.Case.Save();
                c.Case.WfOnCaseCreated(c.User);
                var todos = c.ToDos();
                var sendIneligibilityPacket = todos.FirstOrDefault(tdi => tdi.Title == "Send Ineligibility Notice");
                sendIneligibilityPacket.Should().NotBeNull();
            }
        }

        [TestMethod]
        public void CaseCreatedDidNotReturnToWork()
        {
            using (CaseContext c = new CaseContext())
            {
                c.Case.WfOnCaseCreated(c.User);
                var todos = c.ToDos();
                var rtw = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.ReturnToWork);
                rtw.WfOnToDoItemCompleted(c.User, ReturnToWorkActivity.ReturnToWorkOutcomeValue, true);

                todos = c.ToDos();
                var verifyRTW = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.VerifyReturnToWork);
                verifyRTW.WfOnToDoItemCompleted(c.User, VerifyReturnToWorkActivity.RtwVerifiedNoOutcome, true);

                todos = c.ToDos();
                var didNotReturnToWork = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.CloseCase && t.Title == "Employee Did Not Return To Work, Close Case");
                didNotReturnToWork.Should().NotBeNull();

            }
        }

        [TestMethod]
        public void CaseCreatedIsWorkRelated()
        {
            string todoTitleToVerify = "IAmACaseReviewCreatedInResponseToAWorkRelatedCase";

            using (var workflowService = new WorkflowService())
            {
                var expressionGroups = workflowService.GetExpressionsByEventType(EventType.CaseCreated);
                var caseGroup = expressionGroups.FirstOrDefault(eg => eg.Title == "Case Information");
                caseGroup.Should().NotBeNull();
                var isWorkRelated = caseGroup.Expressions.FirstOrDefault(e => e.Prompt == "Is Work Related");
                isWorkRelated.Should().NotBeNull();
                isWorkRelated.Value = true;
                var caseCreatedShouldBeWorkRelated = (Rule)workflowService.GetRule(isWorkRelated);
                var rules = new List<Rule> { caseCreatedShouldBeWorkRelated };

                WorkflowActivity reviewTheCase = new WorkflowActivity()
                {
                    Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"),
                    Name = "Review The Case",
                    ActivityId = new CaseReviewActivity().Id
                };

                reviewTheCase.Metadata.Set("Title", todoTitleToVerify);

                Workflow wf = new Workflow()
                {
                    Code = "WORKRELATEDCREATED",
                    Name = "Work Related Created",
                    Description = "Test workflow that should fire when a work related case is sent",
                    TargetEventType = EventType.CaseCreated,
                    Criteria = rules,
                    CriteriaSuccessType = RuleGroupSuccessType.And,
                    Activities = new List<WorkflowActivity>()
                    {
                        reviewTheCase
                    }
                };

                wf.Save();

                using (CaseContext c = new CaseContext())
                {
                    c.Case.Metadata.Set("IsWorkRelated", true);
                    c.Case.Save();
                    c.Case.WfOnCaseCreated();
                    var todos = c.ToDos();
                    var reviewIsWorkRelated = todos.FirstOrDefault(t => t.Title == todoTitleToVerify);
                    reviewIsWorkRelated.Should().NotBeNull();
                }

                wf.Delete();
            }
        }

        [TestMethod]
        public void CaseCreatedAccommodationIsWorkRelated()
        {
            string todoTitleToVerify = "IAmACaseReviewCreatedInResponseToAWorkRelatedCase";

            using (var workflowService = new WorkflowService())
            {
                var expressionGroups = workflowService.GetExpressionsByEventType(EventType.CaseCreated);
                var caseGroup = expressionGroups.FirstOrDefault(eg => eg.Title == "Case Information");
                caseGroup.Should().NotBeNull();
                var isWorkRelated = caseGroup.Expressions.FirstOrDefault(e => e.Prompt == "Is Work Related");
                isWorkRelated.Should().NotBeNull();
                isWorkRelated.Value = true;
                var caseCreatedShouldBeWorkRelated = (Rule)workflowService.GetRule(isWorkRelated);
                var rules = new List<Rule> { caseCreatedShouldBeWorkRelated };

                WorkflowActivity reviewTheCase = new WorkflowActivity()
                {
                    Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"),
                    Name = "Review The Case",
                    ActivityId = new CaseReviewActivity().Id
                };

                reviewTheCase.Metadata.Set("Title", todoTitleToVerify);

                Workflow wf = new Workflow()
                {
                    Code = "WORKRELATEDCREATED",
                    Name = "Work Related Created",
                    Description = "Test workflow that should fire when a work related case is sent",
                    TargetEventType = EventType.CaseCreated,
                    Criteria = rules,
                    CriteriaSuccessType = RuleGroupSuccessType.And,
                    Activities = new List<WorkflowActivity>()
                    {
                        reviewTheCase
                    }
                };

                wf.Save();

                using (CaseContext c = new CaseContext())
                {
                    c.Case.AccommodationRequest = new AccommodationRequest()
                    {
                        Accommodations = new List<Accommodation>()
                        {
                            new Accommodation()
                            {
                                IsWorkRelated = true
                            }
                        }
                    };
                    c.Case.Save();
                    c.Case.WfOnCaseCreated();
                    var todos = c.ToDos();
                    var reviewIsWorkRelated = todos.FirstOrDefault(t => t.Title == todoTitleToVerify);
                    reviewIsWorkRelated.Should().NotBeNull();
                }

                wf.Delete();
            }
        }

        [TestMethod]
        public void EssCaseCreatedTest()
        {
            using (CaseContext c = new CaseContext())
            {
                c.SelfService();
                c.Case.Metadata.SetRawValue("IsWorkScheduleWrong", true);
                c.Case.Save();
                c.Case.WfOnCaseCreated(c.User);
                var todo = c.ToDos().FirstOrDefault(t => t.ItemType == ToDoItemType.ReviewWorkSchedule);
                todo.Should().NotBeNull("should have review work schedule todo");
                todo.Title.Should().Be(string.Format("Review Work Schedule of '{0}'", c.Case.Employee.FullName));
            }
        }

        [TestMethod]
        public void WorkRelatedCaseOSHAPaperworkDueTest()
        {
            using (CaseContext c = new CaseContext())
            {
                c.Case.Customer.AddFeature(Feature.WorkRelatedReporting, true);
                c.Case.Customer.Save();
                c.Case.Metadata.SetRawValue("IsWorkRelated", true);
                c.Case.WorkRelated = new WorkRelatedInfo()
                {
                    IllnessOrInjuryDate = new DateTime(2016, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)
                };
                c.Case.WfOnCaseCreated(c.User);
                var todos = c.ToDos();
                var osha = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.CaseReview && t.Title.Contains("OSHA Form 301"));
                osha.Should().NotBeNull();
                osha.DueDate.Should().HaveYear(2016).And.HaveMonth(1).And.HaveDay(8);
                osha.WfOnToDoItemCompleted(c.User, CaseReviewActivity.CompleteOutcomeValue, true);
            }
        }

        [TestMethod]
        public void PolicyExhaustedTest()
        {
            using (CaseContext c = new CaseContext())
            {
                new CaseService(false).Using(s => s.ChangeCase(c.Case, START_DATE, END_DATE.AddDays(365), false, true, CaseType.Consecutive, true, false));
                c.Case.Save();
                c.Case.Segments[0].AppliedPolicies[0].WfOnPolicyExhausted(c.Case, c.User);
                var todos = c.ToDos();
                todos.Count().Should().BeGreaterOrEqualTo(1);
                todos[0].Title.Should().Be("Send Eligibility Notice Leave Exhausted");
            }
            // Now test to see if our event will get called automagically.
            using (CaseContext c = new CaseContext())
            {
                new CaseService(true).Using(s => s.ChangeCase(c.Case, START_DATE, END_DATE.AddDays(365), false, true, CaseType.Consecutive, true, false));
                c.Case.Save();
                // There's going to be more than 1 of these guys now because we're firing workflow, which will include case extended.
                var todos = c.ToDos();
                var exh = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.Communication && t.Metadata.GetRawValue<string>("Template") == "ELGLEXH");
                exh.Should().NotBeNull();
                exh.Title.Should().Be("Send Eligibility Notice Leave Exhausted");

            }
        }

        [TestMethod]
        [Ignore]
        public void PaperworkSentTest()
        {
            // Ensure we're in an HTTP context, 'cause the communication service is stupid
            //
            // Received, Reviewed, Approved
            using (CaseContext c = new CaseContext().Http())
            using (CommunicationService s = new CommunicationService())
            {
                var comm = s.CreateCommunication(c.Case.Id, "ELGNOTICE", CommunicationType.Mail);
                comm = s.SendCommunication(comm, null, "ELGNOTICE");

                var todos = c.ToDos();
                var due = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.PaperworkDue
                && t.Title == "Paperwork 'Certification for FMLA Employee Health Condition' Due");
                due.Should().NotBeNull();
                due.Metadata.SetRawValue("PaperworkReceived", true);

                due.WfOnToDoItemCompleted(c.User, PaperworkDueActivity.PaperworkReceivedOutcomeValue, true);
                todos = c.ToDos();
                var review = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.PaperworkReview);
                review.Should().NotBeNull();
                review.Title.Should().Be("Review Paperwork: Certification for FMLA Employee Health Condition");

                review.WfOnToDoItemCompleted(c.User, PaperworkReviewActivity.ApprovedOutcomeValue, true);
                todos = c.ToDos();
                var cR = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.CaseReview && t.Title == "Paperwork Approved, Make Approval Decision");
                cR.Should().NotBeNull();
            }
            //
            // Received, Reviewed, Incomplete
            using (CaseContext c = new CaseContext().Http())
            using (CommunicationService s = new CommunicationService())
            {
                var comm = s.CreateCommunication(c.Case.Id, "ELGNOTICE", CommunicationType.Mail);
                comm = s.SendCommunication(comm, null, "ELGNOTICE");

                var todos = c.ToDos();
                var due = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.PaperworkDue
                && t.Title == "Paperwork 'Certification for FMLA Employee Health Condition' Due");
                due.Should().NotBeNull();
                due.Metadata.SetRawValue("PaperworkReceived", true);

                due.WfOnToDoItemCompleted(c.User, PaperworkDueActivity.PaperworkReceivedOutcomeValue, true);
                todos = c.ToDos();
                var review = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.PaperworkReview);
                review.Should().NotBeNull();
                review.Title.Should().Be("Review Paperwork: Certification for FMLA Employee Health Condition");

                review.WfOnToDoItemCompleted(c.User, PaperworkReviewActivity.IncompleteOutcomeValue, true);
                todos = c.ToDos();
                var inc = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.Communication && t.Metadata.GetRawValue<string>("Template") == "INCOMPCERT");
                inc.Should().NotBeNull();
            }
            //
            // Received, Reviewed, Denied
            using (CaseContext c = new CaseContext().Http())
            using (CommunicationService s = new CommunicationService())
            {
                var comm = s.CreateCommunication(c.Case.Id, "ELGNOTICE", CommunicationType.Mail);
                comm = s.SendCommunication(comm, null, "ELGNOTICE");

                var todos = c.ToDos();
                var due = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.PaperworkDue
                 && t.Title == "Paperwork 'Certification for FMLA Employee Health Condition' Due");
                due.Should().NotBeNull();

                due.Metadata.SetRawValue("PaperworkReceived", true);

                due.WfOnToDoItemCompleted(c.User, PaperworkDueActivity.PaperworkReceivedOutcomeValue, true);
                todos = c.ToDos();
                var review = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.PaperworkReview);
                review.Should().NotBeNull();
                review.Title.Should().Be("Review Paperwork: Certification for FMLA Employee Health Condition");

                review.WfOnToDoItemCompleted(c.User, PaperworkReviewActivity.DeniedOutcomeValue, true);
                todos = c.ToDos();
                var cR = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.CaseReview && t.Title == "Paperwork Denied, Make Approval Decision");
                cR.Should().NotBeNull();
            }
            // NOT Recieved just loops back to paperwork due unless they explicitly select to close the case
            using (CaseContext c = new CaseContext().Http())
            using (CommunicationService s = new CommunicationService())
            {
                var fakeDate = new DateTime(2999, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                var comm = s.CreateCommunication(c.Case.Id, "ELGNOTICE", CommunicationType.Mail);
                comm = s.SendCommunication(comm, null, "ELGNOTICE");

                var todos = c.ToDos();
                var due = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.PaperworkDue
                 && t.Title == "Paperwork 'Certification for FMLA Employee Health Condition' Due");
                due.Should().NotBeNull();
                due.DueDate.Should().Be(comm.FirstPaperworkDueDate.HasValue ? comm.FirstPaperworkDueDate.Value.AddDays(1) : due.DueDate);

                due.Metadata.SetRawValue("NewDueDate", fakeDate);
                due.Metadata.SetRawValue("ExtendGracePeriod", false);
                due.Metadata.SetRawValue("PaperworkReceived", false);
                due.Metadata.SetRawValue("CloseCase", false);
                due.WfOnToDoItemCompleted(c.User, PaperworkDueActivity.PaperworkNotReceivedOutcomeValue, true);
                todos = c.ToDos();
                due = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.Communication && t.Title == "Send Documentation Not Received Letter to Employee");
                due.Should().NotBeNull("Should have Send Doc Not Received todo");
            }

            // NOT Recieved, close case
            using (CaseContext c = new CaseContext().Http())
            using (CommunicationService s = new CommunicationService())
            {
                var fakeDate = new DateTime(2999, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                var comm = s.CreateCommunication(c.Case.Id, "ELGNOTICE", CommunicationType.Mail);
                comm = s.SendCommunication(comm, null, "ELGNOTICE");

                var todos = c.ToDos();
                var due = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.PaperworkDue
                && t.Title == "Paperwork 'Certification for FMLA Employee Health Condition' Due");
                due.Should().NotBeNull();
                due.DueDate.Should().Be(comm.FirstPaperworkDueDate.HasValue ? comm.FirstPaperworkDueDate.Value.AddDays(1) : due.DueDate);

                due.Metadata.SetRawValue("PaperworkReceived", false);
                due.Metadata.SetRawValue("ExtendGracePeriod", false);
                due.Metadata.SetRawValue("CloseCase", true);
                due.WfOnToDoItemCompleted(c.User, PaperworkDueActivity.PaperworkNotReceivedOutcomeValue, true);
                todos = c.ToDos();
                var todo = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.CloseCase);
                todo.Should().NotBeNull();
                todo.Title.Should().Be("Close Case, Paperwork 'Certification for FMLA Employee Health Condition' Not Received");

                comm = Communication.GetById(comm.Id);
                var pw = comm.Paperwork.FirstOrDefault(p => p.Id == todo.Metadata.GetRawValue<Guid?>("PaperworkId"));
                pw.Should().NotBeNull();
                pw.Status.Should().Be(PaperworkReviewStatus.NotReceived);
            }
        }

        [TestMethod]
        //[Ignore]
        public void PaperworkSentIsForApprovalDecisionFlagTest()
        {
            // Ensure we're in an HTTP context, 'cause the communication service is stupid
            //
            // Received, IsforApprovalDecision=true, Approved
            using (CaseContext c = new CaseContext().Http())
            using (CommunicationService s = new CommunicationService())
            {
                var comm = s.CreateCommunication(c.Case.Id, "ELGNOTICE", CommunicationType.Mail);
                var communicationPaperwork = new CommunicationPaperwork();
                var paperwork = new Paperwork();
                paperwork.DocType = DocumentType.None;

                paperwork.Criteria = new List<PaperworkCriteriaGroup>();
                paperwork.Fields = new List<PaperworkField>();
                communicationPaperwork.Paperwork = paperwork;
                comm.Paperwork.Add(communicationPaperwork);
                comm = s.SendCommunication(comm, null, "ELGNOTICE");
                var todos = c.ToDos();
                var due = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.PaperworkDue
                && t.Title == "Paperwork 'Certification for FMLA Employee Health Condition' Due");
                due.Should().NotBeNull();
                due.Metadata.SetRawValue("PaperworkReceived", true);

                due.WfOnToDoItemCompleted(c.User, PaperworkDueActivity.PaperworkReceivedOutcomeValue, true);
                todos = c.ToDos();
                var review = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.PaperworkReview);
                review.Should().NotBeNull();
                review.Title.Should().Be("Review Paperwork: Certification for FMLA Employee Health Condition");

                review.WfOnToDoItemCompleted(c.User, PaperworkReviewActivity.ApprovedOutcomeValue, true);
                todos = c.ToDos();
                var cR = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.CaseReview && t.Title == "Paperwork Approved, Make Approval Decision");
                cR.Should().NotBeNull();
            }
            //
            // Received, IsforApprovalDecision=false end workflow transition without making Approval decision
            using (CaseContext c = new CaseContext().Http())
            using (CommunicationService s = new CommunicationService())
            {
                var comm = s.CreateCommunication(c.Case.Id, "ELGNOTICE", CommunicationType.Mail);
                comm = s.SendCommunication(comm, null, "ELGNOTICE");

                var todos = c.ToDos();
                var due = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.PaperworkDue
                && t.Title == "Paperwork 'Certification for FMLA Employee Health Condition' Due");
                due.Should().NotBeNull();
                due.Metadata.SetRawValue("PaperworkReceived", true);

                due.WfOnToDoItemCompleted(c.User, PaperworkDueActivity.PaperworkReceivedOutcomeValue, true);
                todos = c.ToDos();
                var review = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.PaperworkReview);
                review.Should().NotBeNull();
                review.Title.Should().Be("Review Paperwork: Certification for FMLA Employee Health Condition");

            }
            //
        }

        [TestMethod]
        [Ignore]
        public void SpecificPaperworkSentTest()
        {
            string todoTitleToVerify = "IAmACaseReviewCreatedInResponseToSpecificPaperwork";

            using (var workflowService = new WorkflowService())
            {
                var expressionGroups = workflowService.GetExpressionsByEventType(EventType.PaperworkSent);
                var communicationsGroup = expressionGroups.FirstOrDefault(eg => eg.Title == "Communications");
                communicationsGroup.Should().NotBeNull();
                var isSpecificPaperwork = communicationsGroup.Expressions.FirstOrDefault(e => e.Prompt == "Paperwork");
                isSpecificPaperwork.Should().NotBeNull();
                isSpecificPaperwork.Value = Paperwork.AsQueryable().FirstOrDefault(t => t.Name == "Certification for FMLA Employee Health Condition" && t.CustomerId == null).Id;
                var paperworkShouldBeCertification = (Rule)workflowService.GetRule(isSpecificPaperwork);
                var rules = new List<Rule> { paperworkShouldBeCertification };

                WorkflowActivity reviewTheCase = new WorkflowActivity()
                {
                    Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"),
                    Name = "Automatically Approved Time Off Requests",
                    ActivityId = new CaseReviewActivity().Id
                };

                reviewTheCase.Metadata.Set("Title", todoTitleToVerify);

                Workflow wf = new Workflow()
                {
                    Code = "EMPHEALTHSENT",
                    Name = "Employee Health Notice Sent",
                    Description = "Test workflow that should fire when the employee health paperwork is sent",
                    TargetEventType = EventType.PaperworkSent,
                    Criteria = rules,
                    Activities = new List<WorkflowActivity>()
                    {
                        reviewTheCase
                    }
                };

                wf.Save();

                using (CaseContext c = new CaseContext().Http())
                using (CommunicationService s = new CommunicationService())
                {
                    var comm = s.CreateCommunication(c.Case.Id, "ELGNOTICE", CommunicationType.Mail);
                    comm = s.SendCommunication(comm, null, "ELGNOTICE");

                    var todos = c.ToDos();
                    var caseReviewTodo = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.CaseReview
                    && t.Title == todoTitleToVerify);
                    caseReviewTodo.Should().NotBeNull();
                }

                wf.Delete();
            }
        }

        [TestMethod]
        public void TestEmployeeTerminated()
        {
            Workflow wf =  new Workflow()
            {
                Code = "EmployeeTerminated",
                Name = "Employee Terminated",
                Description = "Test workflow that should fire when employee is terminated",
                TargetEventType = EventType.EmployeeTerminated
            };

            WorkflowActivity closeAllEmployeeToDos = new WorkflowActivity()
            {
                Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"),
                Name = "Close All ToDos",
                ActivityId = new UpdateToDosActivity().Id
            };

            closeAllEmployeeToDos.Metadata
                .SetRawValue("UpdateAll", true)
                .SetRawValue("WhereStatus", ToDoItemStatus.Overdue)
                .SetRawValue("Status", ToDoItemStatus.Cancelled);

            wf.Activities.Add(closeAllEmployeeToDos);
            wf.Save();

            using (CaseContext c = new CaseContext())
            {
                c.Case.WfOnCaseCreated();

                var todos = c.ToDos();
                todos.Count().Should().BeGreaterOrEqualTo(1);
                bool makeOverDueByDate = true;
                foreach (var todo in todos)
                {
                    if (makeOverDueByDate)
                        todo.DueDate = todo.DueDate.AddMonths(-1);
                    else
                        todo.Status = ToDoItemStatus.Overdue;
                    todo.Save();

                    makeOverDueByDate = !makeOverDueByDate;
                }

                var employee = Employee.GetById(c.Case.Employee.Id);
                employee.WfOnEmployeeTerminated();

                todos = c.ToDos();
                foreach (var todo in todos)
                    todo.Status.Should().Be(ToDoItemStatus.Cancelled);
            }
        }


        [TestMethod]
        public void IntermittentRequestDoesNotAutoApproveWhenNotCertified()
        {
            Workflow wf = BuildIntermittentRequestWorkflow();

            using (CaseContext c = new CaseContext(CaseType.Intermittent))
            using (CaseService caseService = new CaseService())
            {
                IntermittentTimeRequest tor = new IntermittentTimeRequest()
                {
                    TotalMinutes = 120,
                    RequestDate = c.Case.StartDate,
                    Detail = new List<IntermittentTimeRequestDetail>(){
                        new IntermittentTimeRequestDetail(){
                            PolicyCode = c.Case.Segments.First().AppliedPolicies.First().Policy.Code,
                            Pending = 120
                        }
                    }
                };

                caseService.CreateOrModifyTimeOffRequest(c.Case, new List<IntermittentTimeRequest>() { tor });
                caseService.UpdateCase(c.Case);
                var todos = c.ToDos();
                var determineRequestToDo = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.DetermineRequest);
                determineRequestToDo.Should().NotBeNull();
                determineRequestToDo.Status.Should().Be(ToDoItemStatus.Pending);
            }

            wf.Delete();
        }

        [TestMethod]
        public void IntermittentRequestCasePassesWhenCreatedByAuthorizedSubmitter()
        {
            using (var workflowService = new WorkflowService())
            {
                var expressionGroups = workflowService.GetExpressionsByEventType(EventType.CaseCreated);
                var caseGroup = expressionGroups.FirstOrDefault(eg => eg.Title == "Case Information");
                caseGroup.Should().NotBeNull();
                var isReportedByAuthorizedSubmitterExpression = caseGroup.Expressions.FirstOrDefault(e => e.Prompt == "Is Reported By Authorized Submitter");
                isReportedByAuthorizedSubmitterExpression.Should().NotBeNull();
                isReportedByAuthorizedSubmitterExpression.Value = true;
                var intermittentRequestShouldBeCreatedByAuthorizedSubmitter = (Rule)workflowService.GetRule(isReportedByAuthorizedSubmitterExpression);
                var rules = new List<Rule>{ intermittentRequestShouldBeCreatedByAuthorizedSubmitter };

                var workFlow = BuildIntermittentRequestWorkflow(rules);

                using (var caseContext = new CaseContext(CaseType.Intermittent))
                using (var caseService = new CaseService())
                {
                    // Create the workflow with a Case Review to-do when the case is created by an authorized submitter
                    using (var employerService = new EmployerService())
                    {
                        var employerContact = employerService.SaveContact(
                             new EmployerContact
                             {
                                 Contact = new Contact()
                                 {
                                     FirstName = "Testy",
                                     LastName = "De Guy",
                                     Email = "Okayyyy@absencesoft.com",
                                     WorkPhone = "9041234567",
                                     Fax = "9041234568",
                                     Address = new Address()
                                     {
                                         Country = "US"
                                     }
                                 },
                                 ContactTypeCode = "HR",
                                 ContactTypeName = "HR"
                             });

                        caseContext.Case.AuthorizedSubmitter = employerContact;
                    }

                    var tor = new IntermittentTimeRequest
                    {
                        TotalMinutes = 120,
                        RequestDate = caseContext.Case.StartDate,
                        Detail = new List<IntermittentTimeRequestDetail>(){
                            new IntermittentTimeRequestDetail(){
                                PolicyCode = caseContext.Case.Segments.First().AppliedPolicies.First().Policy.Code,
                                Pending = 120
                            }
                        }
                    };

                    caseService.CreateOrModifyTimeOffRequest(caseContext.Case, new List<IntermittentTimeRequest>() { tor });
                    caseService.UpdateCase(caseContext.Case);
                    var todos = caseContext.ToDos();
                    todos.Should().BeEmpty();
                }

                workFlow.Delete();
            }

        }

        [TestMethod]
        public void IntermittentRequestAutoApprovesWhenCertified()
        {
            var wf = BuildIntermittentRequestWorkflow();

            using (CaseContext c = new CaseContext(CaseType.Intermittent))
            using (CaseService caseService = new CaseService())
            {
                IntermittentTimeRequest tor = new IntermittentTimeRequest()
                {
                    TotalMinutes = 120,
                    RequestDate = c.Case.StartDate,
                    Detail = new List<IntermittentTimeRequestDetail>(){
                        new IntermittentTimeRequestDetail(){
                            PolicyCode = c.Case.Segments.First().AppliedPolicies.First().Policy.Code,
                            Pending = 120
                        }
                    }
                };
                var certification = new Certification()
                {
                    StartDate = START_DATE,
                    EndDate = END_DATE,
                    Duration = 4,
                    Frequency = 1,
                    Occurances = 1
                };

                caseService.CreateOrModifyCertification(c.Case, certification, true);
                caseService.CreateOrModifyTimeOffRequest(c.Case, new List<IntermittentTimeRequest>() { tor });
                caseService.UpdateCase(c.Case);

                var todos = c.ToDos();
                var determineRequestToDo = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.DetermineRequest);
                determineRequestToDo.Should().NotBeNull();
                determineRequestToDo.Status.Should().Be(ToDoItemStatus.Complete);
            }

            wf.Delete();
        }

        [TestMethod]
        public void IntermittentRequestWorkflowCertification()
        {
            using (var workflowService = new WorkflowService())
            {
                var expressionGroups = workflowService.GetExpressionsByEventType(EventType.TimeOffRequested);
                var intermittentGroup = expressionGroups.FirstOrDefault(eg => eg.Title == "Time Off Request");
                var passedCertification = intermittentGroup.Expressions.FirstOrDefault(e => e.Prompt == "Passed Certification");
                passedCertification.Value = true;
                Rule intermittentRequestShouldBeCertified = (Rule)workflowService.GetRule(passedCertification);
                List<Rule> rules = new List<Rule>();
                rules.Add(intermittentRequestShouldBeCertified);

                var wf = BuildIntermittentRequestWorkflow(rules);
                using (CaseContext c = new CaseContext(CaseType.Intermittent))
                using (CaseService caseService = new CaseService())
                {
                    IntermittentTimeRequest tor = new IntermittentTimeRequest()
                    {
                        TotalMinutes = 120,
                        RequestDate = c.Case.StartDate,
                        Detail = new List<IntermittentTimeRequestDetail>(){
                        new IntermittentTimeRequestDetail(){
                            PolicyCode = c.Case.Segments.First().AppliedPolicies.First().Policy.Code,
                            Pending = 120
                        }
                    }
                    };
                    var certification = new Certification()
                    {
                        StartDate = START_DATE,
                        EndDate = END_DATE,
                        Duration = 4,
                        Frequency = 1,
                        Occurances = 1
                    };

                    caseService.CreateOrModifyCertification(c.Case, certification, true);
                    caseService.CreateOrModifyTimeOffRequest(c.Case, new List<IntermittentTimeRequest>() { tor });
                    caseService.UpdateCase(c.Case);

                    var todos = c.ToDos();
                    var determineRequestToDo = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.DetermineRequest);
                    determineRequestToDo.Should().NotBeNull();
                    determineRequestToDo.Status.Should().Be(ToDoItemStatus.Complete);
                }

                wf.Delete();
            }
        }

        protected Workflow BuildIntermittentRequestWorkflow(List<Rule> rules = null)
        {
            if (rules == null)
                rules = new List<Rule>();

            Workflow wf = new Workflow()
            {
                Code = "IntermittentRequestCreated",
                Name = "Intermittent Request Created",
                Description = "Test Workflow that runs when an intermittent request is created",
                TargetEventType = EventType.TimeOffRequested,
                Criteria = rules
            };

            WorkflowActivity approveTOR = new WorkflowActivity()
            {
                Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"),
                Name = "Automatically Approved Time Off Requests",
                ActivityId = new DetermineRequestActivity().Id
            };

            approveTOR.Metadata
                .SetRawValue("ApproveAutomatically", true);

            wf.Activities.Add(approveTOR);
            return wf.Save();
        }

        [TestMethod]
        public void TestEmployeeRestriction()
        {
            Workflow wfOnWorkRestriction = new Workflow()
            {
                Name = "Work Restriction Created",
                Description = "Test Workflow for Work Restriction Created",
                TargetEventType = EventType.WorkRestrictionCreated,
                Code = "WRCREATED"
            };

            var reviewCase = new WorkflowActivity()
            {
                ActivityId = new CaseReviewActivity().Id,
                Name = "Review the case cause we added a work restriction"
            };

            wfOnWorkRestriction.Activities.Add(reviewCase);

            wfOnWorkRestriction.Save();

            using (var caseContext = new CaseContext())
            using (var demandService = new DemandService(User.System))
            {
                var employee = caseContext.Case.Employee;
                EmployeeRestriction restriction = new EmployeeRestriction()
                {
                    Employee = employee,
                    Case = caseContext.Case,
                    Restriction = new WorkRestriction()
                    {
                        Type = WorkRestrictionType.Fully
                    }
                };
                demandService.SaveJobRestriction(restriction);

                var todos = caseContext.ToDos();
                var caseReviewTodo = todos.FirstOrDefault(c => c.ItemType == ToDoItemType.CaseReview);
                caseReviewTodo.Should().NotBeNull();
            }

            wfOnWorkRestriction.Delete();
        }

        [TestMethod]
        public void WorkflowOnlyFiresWhenEmployeeJobIsTemporary()
        {
            string todoTitleToVerify = "IAmAEmployeeReviewCreatedInResponseToAnEmployeeJobChange";

            WorkflowActivity reviewEmployee = new WorkflowActivity()
            {
                ActivityId = new ReviewEmployeeInformationActivity().Id,
                Name = "Review the Employee Information",

            };

            reviewEmployee.Metadata.Set("Title", todoTitleToVerify);
            Workflow wf = new Workflow()
            {
                Code = "EMPJOBCHANGE",
                Name = "Employee Job Changed",
                Description = "Test Workflow that should fie when the employee job is changed and is temporary",
                TargetEventType = EventType.EmployeeJobChanged,
                Activities = new List<WorkflowActivity>()
                {
                    reviewEmployee
                }
            };
            using (var workflowService = new WorkflowService())
            {
                var expressionGroups = workflowService.GetExpressionsByEventType(EventType.EmployeeJobChanged);
                var jobGroup = expressionGroups.FirstOrDefault(eg => eg.Title == "Job Information");
                jobGroup.Should().NotBeNull();
                var isTemporaryJob = jobGroup.Expressions.FirstOrDefault(jg => jg.Prompt == "Is Temporary");
                isTemporaryJob.Should().NotBeNull();
                isTemporaryJob.Value = true;
                var shouldBeTemporary = (Rule)workflowService.GetRule(isTemporaryJob);
                wf.Criteria = new List<Rule> { shouldBeTemporary };
            }

            wf.Save();


            EmployeeJob theNewJob = new EmployeeJob()
            {
                Employee = Employee.GetById(EMPLOYEE_ID),
                CustomerId = CUSTOMER_ID,
                EmployerId = EMPLOYER_ID,
                Dates = new DateRange(DateTime.UtcNow, DateTime.UtcNow.AddDays(7))
            };

            theNewJob.WfOnEmployeeJobChanged(User.System);

            var todo = ToDoItem.AsQueryable().FirstOrDefault(tdi => tdi.Title == todoTitleToVerify && tdi.ItemType == ToDoItemType.ReviewEmployeeInformation);
            todo.Should().NotBeNull();
            wf.Delete();
        }

        [TestMethod]
        public void NoEligibilityNoticeForEligibleCaseCreatedTest()
        {
            using (CaseContext c = new CaseContext(CaseType.Consecutive))
            {
                c.Case.Segments.First().AppliedPolicies.First().Status = EligibilityStatus.Eligible;
                c.Case.Save();
                c.Case.WfOnCaseCreated(c.User);
                var todos = c.ToDos();
                var sendIneligibilityPacket = todos.FirstOrDefault(tdi => tdi.Title == "Send Ineligibility Notice");
                Assert.IsNull(sendIneligibilityPacket);
            }
        }


        [TestMethod]
        public void EligibilityNoticeForInEligibleCaseCreatedTest()
        {
            using (CaseContext c = new CaseContext(CaseType.Consecutive, AppliedRuleEvalResult.Fail))
            {
                foreach (AppliedPolicy ap in c.Case.Segments.SelectMany(s => s.AppliedPolicies))
                {
                    ap.Status = EligibilityStatus.Ineligible;
                }
                c.Case.Save();
                c.Case.WfOnCaseCreated(c.User);
                var todos = c.ToDos();
                var sendIneligibilityPacket = todos.FirstOrDefault(tdi => tdi.Title == "Send Ineligibility Notice");
                sendIneligibilityPacket.Should().NotBeNull();
            }
        }
        protected Accommodation BuildAccommodation(Case theCase, string code, string name = null)
        {
            Accommodation theAccommodation = new Accommodation()
            {
                Type = new AccommodationType() { Code = code, Name = name ?? code, Id = ObjectId.Empty.ToString() },
                Usage = new List<AccommodationUsage>(1)
                {
                    new AccommodationUsage() { StartDate = START_DATE, EndDate = END_DATE, Determination = AdjudicationStatus.Pending }
                },
                Duration = AccommodationDuration.Temporary,
                Status = CaseStatus.Open
            };
            AccommodationRequest accommRequest = new AccommodationRequest()
            {
                Accommodations = new List<Accommodation>(1) { theAccommodation },
                CaseId = theCase.Id,
                CaseNumber = theCase.CaseNumber,
                Status = CaseStatus.Open
            };
            theCase.AccommodationRequest = accommRequest;
            theCase.IsAccommodation = true;
            return theAccommodation;
        }

        public double? CalcDaysBetweenTwoDates(DateTime fromDate, DateTime toDate)
        {
            double? daysBetweenTwoDates = null;
            if (fromDate != null && toDate != null)
            {
                daysBetweenTwoDates = (toDate - fromDate).TotalDays;
            }

            return daysBetweenTwoDates;
        }

        [TestMethod]
        public void CalcDaysAwayFromWorkTest()
        {
            DateTime caseStartDate = new DateTime(2017, 9, 1);
            DateTime caseEndDate = new DateTime(2017, 9, 30);
            double? daysAwayFromWork = CalcDaysBetweenTwoDates(caseStartDate, caseEndDate);
            Assert.IsNotNull(daysAwayFromWork);
            Assert.IsTrue(daysAwayFromWork == 29);

        }

        [TestMethod]
        public void IsWorkRelatedCaseOverrideValueTest()
        {
            using (CaseContext c = new CaseContext(CaseType.Consecutive))
            {
                using (CaseService caseSvc = new CaseService(false))
                {
                    using (EligibilityService eligSvc = new EligibilityService())
                    {
                        c.Case.Metadata.Set("IsWorkRelated", true);
                        c.Case.WorkRelated = new WorkRelatedInfo();
                        c.Case.WorkRelated.OverrideDaysAwayFromWork = Convert.ToInt32(CalcDaysBetweenTwoDates(c.Case.StartDate, c.Case.EndDate == null ? END_DATE : Convert.ToDateTime(c.Case.EndDate)));
                        c.Case.WorkRelated.DaysOnJobTransferOrRestriction = c.Case.WorkRelated.OverrideDaysAwayFromWork;
                        c.Case.WorkRelated.DaysAwayFromWork = 3;
                        eligSvc.RunEligibility(c.Case);
                        caseSvc.RunCalcs(c.Case);
                        c.Case.Save();
                        Case myCase=  caseSvc.GetCaseById(c.Case.Id);
                        Assert.IsNotNull(myCase);
                        Assert.IsNotNull(myCase.WorkRelated);
                        Assert.IsTrue(myCase.WorkRelated.DaysAwayFromWork == 3);
                        Assert.IsTrue(myCase.WorkRelated.OverrideDaysAwayFromWork == 18);
                        Assert.IsTrue(myCase.WorkRelated.DaysOnJobTransferOrRestriction == 18);
                    }
                }

            }

            }


        protected class CaseContext : IDisposable
        {
            private Case _case = null;
            private User _user = null;
            public Case Case { get { return _case; } }
            public User User { get { return _user; } }
            public CaseContext(CaseType caseType = CaseType.Consecutive, AppliedRuleEvalResult ruleResult = AppliedRuleEvalResult.Pass)
            {
                _user = User.GetById("000000000000000000000002");
                _case = CreateCase(caseType, ruleResult);
            }
            public void Dispose()
            {
                try
                {
                    string caseId = null;
                    if (_case != null)
                    {
                        caseId = _case.Id;
                        _case.Delete();
                    }
                    _case = null;
                    _user = null;
                    ToDoItem.Delete(ToDoItem.Query.EQ(t => t.CaseId, caseId));
                    ToDoItem.Delete(ToDoItem.Query.EQ(t => t.EmployeeId, EMPLOYEE_ID));
                    //ActivityHistory.Delete(Query.Or(Query.EQ("CaseId", new BsonObjectId(new ObjectId(caseId))), Query.EQ("EmployeeId", new BsonObjectId(new ObjectId(EMPLOYEE_ID)))));
                    //WorkflowInstance.Delete(WorkflowInstance.Query.EQ(w => w.CaseId, caseId));
                    Communication.Delete(Communication.Query.EQ(c => c.CaseId, caseId));
                    var docs = Attachment.AsQueryable().Where(a => a.CaseId == caseId).ToList().Select(a => a.Document).ToList();
                    Attachment.Delete(Attachment.Query.EQ(a => a.CaseId, caseId));
                    Document.Delete(Document.Query.In(d => d.Id, docs.Select(d => d.Id)));
                    if (docs.Any())
                        using (FileService fs = new FileService())
                            foreach (var d in docs)
                                try
                                {
                                    fs.DeleteS3Object(d.Locator, Settings.Default.S3BucketName_Files);
                                }
                                catch { }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            public CaseContext Refresh()
            {
                _case = Case.GetById(_case.Id);
                return this;
            }
            public CaseContext Http()
            {
                if (HttpContext.Current != null)
                    return this;
                HttpContext context = HttpContext.Current = new HttpContext(new HttpRequest(null, "http://tempuri.org", null), new HttpResponse(null));
                context.Request.Browser = new HttpBrowserCapabilities() { Capabilities = new Dictionary<string, string> { { "supportsEmptyStringInCookieValue", "false" }, { "cookies", "false" } } };
                AbsenceSoft.Data.Customers.Customer.Current = _user.Customer;
                context.User = new GenericPrincipal(new GenericIdentity(_user.Id), new[] { ObjectId.Empty.ToString() });
                context.Items["__current_user__"] = _user;
                return this;
            }
            public CaseContext SelfService()
            {
                Http();
                HttpContext.Current.Items["__portal_context"] = "ESS";
                AbsenceSoft.Data.Customers.Employer.Current = Case.Employer;
                return this;
            }
            public CaseContext Portal()
            {
                Http();
                HttpContext.Current.Items.Remove("__portal_context");
                AbsenceSoft.Data.Customers.Employer.Current = null;
                return this;
            }
            public List<ToDoItem> ToDos()
            {
                // Avoid user query data access issues, especially in ESS contexts.
                var query = ToDoItem.Query.Find(
                    ToDoItem.Query.And(
                        ToDoItem.Query.EQ(t => t.CustomerId, CUSTOMER_ID),
                        ToDoItem.Query.EQ(t => t.EmployerId, EMPLOYER_ID),
                        ToDoItem.Query.EQ(t => t.EmployeeId, EMPLOYEE_ID),
                        ToDoItem.Query.In(t => t.CaseId, new BsonValue[] { BsonNull.Value, new BsonObjectId(new ObjectId(_case.Id)) })
                    //ToDoItem.Query.NE(t => t.Status, ToDoItemStatus.Complete),
                    //ToDoItem.Query.NE(t => t.Status, ToDoItemStatus.Cancelled)
                    )
                );
                return query.ToList();
            }
        }

        protected static Case CreateCase(CaseType caseType = CaseType.Consecutive, AppliedRuleEvalResult ruleResult = AppliedRuleEvalResult.Pass)
        {
            Case.Delete(Case.Query.EQ(c => c.Employee.Id, EMPLOYEE_ID));
            using (CaseService caseSvc = new CaseService(false))
            using (EligibilityService eligSvc = new EligibilityService())
            {
                Case c = caseSvc.CreateCase(CaseStatus.Open, EMPLOYEE_ID, START_DATE, END_DATE, caseType, REASON_CODE);
                c.AssignedToId = USER_ID;
                c.AssignedToName = USER_ID;
                c.CurrentRelapseId = RELAPSE_ID;
                eligSvc.RunEligibility(c);
                foreach (var policy in c.Segments[0].AppliedPolicies)
                {
                    policy.RuleGroups.ForEach(g => g.Rules.Where(r => r.Result != ruleResult).ForEach(r =>
                    {
                        r.Overridden = true;
                        r.OverrideFromResult = r.Result;
                        r.OverrideValue = r.Rule.LeftExpression;
                        r.OverrideNotes = "Manually Overridden by System for Test";
                        r.Result = ruleResult;
                    }));
                }
                eligSvc.RunEligibility(c);
                c.SetCaseEvent(CaseEventType.EstimatedReturnToWork, END_DATE);
                return c.Save();
            }
        }
    }
}
