﻿using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Workflows;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Tests.Workflows
{
    [TestClass]
    public class WorkflowConfigurationTests : WorkflowTestsBase
    {
        [TestMethod]
        public void WorkingDaysBeforeCaseEndDate()
        {
            string todoTitleToVerify = "TEN WORKING DAYS";
            WorkflowActivity dueInTenWorkingDaysBefore = new WorkflowActivity()
            {
                Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"),
                Name = "Review The Case",
                ActivityId = new CaseReviewActivity().Id
            };

            dueInTenWorkingDaysBefore.Metadata.Set("Title", todoTitleToVerify);
            dueInTenWorkingDaysBefore.Metadata.Set("Time", -10);
            dueInTenWorkingDaysBefore.Metadata.Set("TargetDueDate", CaseEventType.CaseEndDate);
            dueInTenWorkingDaysBefore.Metadata.Set("Units", ToDoResponseTime.WorkingDays);


            Workflow workingDays = new Workflow()
            {
                Code = "WORKINGDAYS",
                Name = "Working Days",
                Description = "Test workflow that should create a case review due 10 working days before case end date",
                TargetEventType = EventType.CaseCreated,
                CriteriaSuccessType = RuleGroupSuccessType.And,
                Activities = new List<WorkflowActivity>()
                    {
                        dueInTenWorkingDaysBefore
                    }
            };

            workingDays.Save();

            using (CaseContext c = new CaseContext())
            {
                c.Case.Save();
                c.Case.WfOnCaseCreated();

                var todos = c.ToDos();
                var workingDaysToDo = todos.FirstOrDefault(t => t.Title == todoTitleToVerify);
                workingDays.Should().NotBeNull();

                // Employee works M-F
                // Case ends on a Friday
                // There is a holiday on September 7th (Labor Day)
                DateTime targetDate = new DateTime(2015, 9, 3, 0, 0, 0, 0, DateTimeKind.Utc);
                Assert.AreEqual(targetDate, workingDaysToDo.DueDate);

            }

            workingDays.Delete();
        }

        [TestMethod]
        public void DaysInAccommodationRuleFailsTest()
        {
            string todoTitleToVerify = "IAmACaseReviewCreatedInResponseToADaysInAccommodationRule";
            Workflow wf = BuildAccommodationLengthWorkflow(todoTitleToVerify);

            using (CaseContext c = new CaseContext())
            {
                var accommodation = BuildAccommodation(c.Case, "LEV", "Leave");
                c.Case.Save();
                c.Case.WfOnCaseCreated();
                accommodation.WfOnAccommodationCreated(c.Case);
                var todos = c.ToDos();
                var noCaseReviewToDo = todos.FirstOrDefault(t => t.Title == todoTitleToVerify);
                noCaseReviewToDo.Should().BeNull();
            }
            wf.Delete();
        }

        [TestMethod]
        public void DaysInAccommodationRulePassesTest()
        {
            string todoTitleToVerify = "IAmACaseReviewCreatedInResponseToADaysInAccommodationRule";
            var wf = BuildAccommodationLengthWorkflow(todoTitleToVerify);
            using (CaseContext c = new CaseContext())
            {
                var accommodation = BuildAccommodation(c.Case, "LEV", "Leave");

                accommodation.Usage.First().StartDate = new DateTime(2016, 1, 1);
                accommodation.Usage.First().EndDate = new DateTime(2016, 3, 31);
                accommodation.DaysInAccommodation.Should().Be(90);
                c.Case.Save();
                c.Case.WfOnCaseCreated();
                accommodation.WfOnAccommodationCreated(c.Case);
                var todos = c.ToDos();
                var noCaseReviewToDo = todos.FirstOrDefault(t => t.Title == todoTitleToVerify);
                noCaseReviewToDo.Should().NotBeNull();
            }

            wf.Delete();
        }

        private Workflow BuildAccommodationLengthWorkflow(string todoTitleToVerify)
        {
            using (var workflowService = new WorkflowService())
            {
                var expressionGroups = workflowService.GetExpressionsByEventType(EventType.AccommodationCreated);
                var accommodationGroup = expressionGroups.FirstOrDefault(eg => eg.Title == "Accommodation");
                accommodationGroup.Should().NotBeNull();
                var daysInAccommodationExpression = accommodationGroup.Expressions.FirstOrDefault(e => e.Prompt == "Days In Accommodation");
                daysInAccommodationExpression.Should().NotBeNull();
                daysInAccommodationExpression.RuleOperator = ">=";
                daysInAccommodationExpression.Value = 90;
                var accommodationLengthShouldBeGreaterThanOrEqualTo90Days = (Rule)workflowService.GetRule(daysInAccommodationExpression);
                var rules = new List<Rule> { accommodationLengthShouldBeGreaterThanOrEqualTo90Days };

                WorkflowActivity reviewTheCase = new WorkflowActivity()
                {
                    Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"),
                    Name = "Review The Case",
                    ActivityId = new CaseReviewActivity().Id
                };

                reviewTheCase.Metadata.Set("Title", todoTitleToVerify);

                Workflow wf = new Workflow()
                {
                    Code = "ACCOMMODATIONLENGTH",
                    Name = "Accommodation Length",
                    Description = "Test workflow that should fire when an accommodation length is matched",
                    TargetEventType = EventType.AccommodationCreated,
                    Criteria = rules,
                    CriteriaSuccessType = RuleGroupSuccessType.And,
                    Activities = new List<WorkflowActivity>()
                    {
                        reviewTheCase
                    }
                };

                return wf.Save();
            }
        }

        [TestMethod]
        public void DaysInCaseRuleFailsTest()
        {
            string todoTitleToVerify = "IAmACaseReviewCreatedInResponseToADaysInCaseRule";
            Workflow wf = BuildCaseLengthWorkflow(todoTitleToVerify, 30, ">=");
            using (CaseContext c = new CaseContext())
            {
                c.Case.DaysInCase.Should().Be(19);
                c.Case.WfOnCaseCreated();
                var todos = c.ToDos();
                var noCaseReviewToDo = todos.FirstOrDefault(t => t.Title == todoTitleToVerify);
                noCaseReviewToDo.Should().BeNull();
            }
            wf.Delete();
        }

        [TestMethod]
        public void DaysInCaseRulePassesTest()
        {
            string todoTitleToVerify = "IAmACaseReviewCreatedInResponseToMoreThan10DaysInCaseRule";
            var wf = BuildCaseLengthWorkflow(todoTitleToVerify, 10, ">=");
            using (CaseContext c = new CaseContext())
            {
                c.Case.DaysInCase.Should().Be(19);
                c.Case.WfOnCaseCreated();
                var todos = c.ToDos();
                var reviewMyCase = todos.FirstOrDefault(t => t.Title == todoTitleToVerify);
                reviewMyCase.Should().NotBeNull();
            }

            wf.Delete();
        }

        [TestMethod]
        public void DaysInCaseRuleEqualsNineteen()
        {
            string todoTitleToVerify = "IAmACaseReviewCreatedInResponseToExactly19DaysInCaseRule";
            var wf = BuildCaseLengthWorkflow(todoTitleToVerify, 19, "==");
            using (CaseContext c = new CaseContext())
            {
                c.Case.DaysInCase.Should().Be(19);
                c.Case.WfOnCaseCreated();
                var todos = c.ToDos();
                var reviewMyCase = todos.FirstOrDefault(t => t.Title == todoTitleToVerify);
                reviewMyCase.Should().NotBeNull();
            }

            wf.Delete();
        }

        [TestMethod]
        public void CaseHasAnyPolicyType()
        {
            using (var workflowService = new WorkflowService())
            using (var caseContext = new CaseContext())
            {
                string todoTitleToVerify = "I am a case review where a case has a policy with a type of FMLA";
                var policyTypeExpression = VerifyExpression("Case Information", "Has Any Policy Type", true, "==");
                policyTypeExpression.Parameters.First().Value = PolicyType.FMLA;
                var rules = new List<Rule>()
                {
                    (Rule)workflowService.GetRule(policyTypeExpression)
                };

                WorkflowActivity reviewTheCase = new WorkflowActivity()
                {
                    Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"),
                    Name = "Review The Case",
                    ActivityId = new CaseReviewActivity().Id
                };

                reviewTheCase.Metadata.Set("Title", todoTitleToVerify);

                Workflow wf = new Workflow()
                {
                    Code = "POLICYTYPE",
                    Name = "Policy Type",
                    Description = "Test workflow that should fire when a policy type is matched",
                    TargetEventType = EventType.CaseCreated,
                    Criteria = rules,
                    CriteriaSuccessType = RuleGroupSuccessType.And,
                    Activities = new List<WorkflowActivity>()
                    {
                        reviewTheCase
                    }
                };

                wf.Save();

                caseContext.Case.Save();
                caseContext.Case.WfOnCaseCreated();

                var todos = caseContext.ToDos();
                var matchingTodo = todos.FirstOrDefault(t => t.Title == todoTitleToVerify);
                matchingTodo.Should().NotBeNull();

                wf.Delete();
            }
        }

        [TestMethod]
        public void AttachmentTypeExpression()
        {
            using (var workflowService = new WorkflowService())
            using (var caseContext = new CaseContext())
            {
                string todoTitleToVerify = "I am a case review where an attachment has a specific type";
                var attachmentTypeToTest = AttachmentType.Communication;
                var attachmentTypeExpression = VerifyExpression("Attachment Information", "Attachment Type", attachmentTypeToTest, "==", EventType.AttachmentCreated);
                var rules = new List<Rule>()
                {
                    (Rule)workflowService.GetRule(attachmentTypeExpression)
                };

                WorkflowActivity reviewTheCase = new WorkflowActivity()
                {
                    Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"),
                    Name = "Review The Case",
                    ActivityId = new CaseReviewActivity().Id
                };

                reviewTheCase.Metadata.Set("Title", todoTitleToVerify);

                Workflow wf = new Workflow()
                {
                    Code = "ATTACHMENTTYPE",
                    Name = "Attachment Type",
                    Description = "Test workflow that should fire when an attachment type is matched",
                    TargetEventType = EventType.AttachmentCreated,
                    Criteria = rules,
                    CriteriaSuccessType = RuleGroupSuccessType.And,
                    Activities = new List<WorkflowActivity>()
                    {
                        reviewTheCase
                    }
                };

                wf.Save();

                caseContext.Case.Save();

                var testAttachment = new Attachment()
                {
                    AttachmentType = attachmentTypeToTest,
                    CustomerId = caseContext.Case.CustomerId,
                    EmployerId = caseContext.Case.EmployerId,
                    CaseId = caseContext.Case.Id
                };

                testAttachment.Save();
                testAttachment.WfOnAttachmentCreated();

                var todos = caseContext.ToDos();
                var matchingTodo = todos.FirstOrDefault(t => t.Title == todoTitleToVerify);
                matchingTodo.Should().NotBeNull();
            }
        }

        [TestMethod]
        public void ToDoDoesNotAutoComplete()
        {
            using (var caseContext = new CaseContext())
            {
                string todoTitleToVerify = "I Should NOT Be Auto Completed";

                WorkflowActivity reviewTheCase = new WorkflowActivity()
                {
                    Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"),
                    Name = "Review The Case",
                    ActivityId = new CaseReviewActivity().Id
                };

                reviewTheCase.Metadata.Set("Title", todoTitleToVerify);
                reviewTheCase.Metadata.Set("Autocomplete", true);

                Workflow wf = new Workflow()
                {
                    Code = "NOTAUTOCOMPLETE",
                    Name = "Not Auto Complete",
                    Description = "Test workflow that should NOT fire and auto complete the todo",
                    TargetEventType = EventType.CaseCreated,
                    CriteriaSuccessType = RuleGroupSuccessType.And,
                    Activities = new List<WorkflowActivity>()
                    {
                        reviewTheCase
                    }
                };

                wf.Save();

                caseContext.Case.Save();
                caseContext.Case.WfOnCaseCreated();

                var todos = caseContext.ToDos();
                var matchingTodo = todos.FirstOrDefault(t => t.Title == todoTitleToVerify);
                matchingTodo.Should().NotBeNull();
                matchingTodo.Status.Should().Be(ToDoItemStatus.Pending);
                wf.Delete();
            }
        }

        [TestMethod]
        public void ToDoAutoCompletes()
        {
            using (var caseContext = new CaseContext())
            {
                string todoTitleToVerify = "I Should Be Auto Completed";
                string secondTodoTitleToVerify = "I Should Be Pending";

                WorkflowActivity reviewTheCase = new WorkflowActivity()
                {
                    Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"),
                    Name = "Review The Case",
                    ActivityId = new CaseReviewActivity().Id
                };

                reviewTheCase.Metadata.Set("Autocomplete", true);
                reviewTheCase.Metadata.Set("Title", todoTitleToVerify);

                WorkflowActivity reviewTheCaseAgain = new WorkflowActivity()
                {
                    Id = new Guid("E6D369AD-A247-4A7C-9171-C2C6142C3D68"),
                    Name = "Review The Case",
                    ActivityId = new CaseReviewActivity().Id
                };

                reviewTheCaseAgain.Metadata.Set("Title", secondTodoTitleToVerify);

                Workflow wf = new Workflow()
                {
                    Code = "AUTOCOMPLETE",
                    Name = "Auto Complete",
                    Description = "Test workflow that should fire and auto complete the todo",
                    TargetEventType = EventType.CaseCreated,
                    CriteriaSuccessType = RuleGroupSuccessType.And,
                    Activities = new List<WorkflowActivity>()
                    {
                        reviewTheCase,
                        reviewTheCaseAgain
                    }, 
                    Transitions = new List<Transition>()
                    {
                        new Transition()
                        {
                            Id = new Guid("931F9804-8029-4A43-812B-4B86857D10E3"),
                            SourceActivityId = reviewTheCase.Id,
                            TargetActivityId = reviewTheCaseAgain.Id,
                            ForOutcome = Activity.CompleteOutcomeValue
                        }
                    }
                };

                wf.Save();

                caseContext.Case.Save();
                caseContext.Case.WfOnCaseCreated(caseContext.User);

                var todos = caseContext.ToDos();
                var matchingTodo = todos.FirstOrDefault(t => t.Title == todoTitleToVerify);
                matchingTodo.Should().NotBeNull();
                matchingTodo.Status.Should().Be(ToDoItemStatus.Complete);

                matchingTodo = todos.FirstOrDefault(t => t.Title == secondTodoTitleToVerify);
                matchingTodo.Should().NotBeNull();
                matchingTodo.Status.Should().Be(ToDoItemStatus.Pending);

                wf.Delete();
            }
        }

        [TestMethod]
        public void ReviewCaseNoteAutoCompletes()
        {
            using (var caseContext = new CaseContext())
            {
                string todoTitleToVerify = "Case Note - I Should Be Auto Completed";

                WorkflowActivity reviewTheCase = new WorkflowActivity()
                {
                    Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"),
                    Name = "Review The Case Note",
                    ActivityId = new ReviewCaseNoteActivity().Id
                };

                reviewTheCase.Metadata.Set("Autocomplete", true);
                reviewTheCase.Metadata.Set("Title", todoTitleToVerify);

                Workflow wf = new Workflow()
                {
                    Code = "CASENOTEAUTOCOMPLETE",
                    Name = "Auto Complete",
                    Description = "Test workflow that should fire and auto complete the case note todo",
                    TargetEventType = EventType.CaseNoteCreated,
                    CriteriaSuccessType = RuleGroupSuccessType.And,
                    Activities = new List<WorkflowActivity>()
                    {
                        reviewTheCase,
                    }
                };

                wf.Save();

                caseContext.Case.Save();
                caseContext.Case.WfOnCaseCreated(caseContext.User);

                CaseNote theNote = new CaseNote();
                theNote.CaseId = caseContext.Case.Id;
                theNote.CustomerId = caseContext.Case.CustomerId;
                theNote.EmployerId = caseContext.Case.EmployerId;
                theNote.Notes = "Blah blah";
                theNote.Save();
                theNote.WfOnCaseNoteCreated(caseContext.User);

                var todos = caseContext.ToDos();
                var matchingTodo = todos.FirstOrDefault(t => t.Title == todoTitleToVerify);
                matchingTodo.Should().NotBeNull();
                matchingTodo.Status.Should().Be(ToDoItemStatus.Complete);
                wf.Delete();
            }
        }

        private RuleExpression VerifyExpression(string groupToVerify, string expressionToVerify, object value, string theOperator, EventType typeToCheck = EventType.CaseCreated, List<RuleExpressionParameter> parameters = null)
        {
            using (var workflowService = new WorkflowService())
            {
                var expressionGroups = workflowService.GetExpressionsByEventType(typeToCheck);
                var theGroup = expressionGroups.FirstOrDefault(eg => eg.Title == groupToVerify);
                theGroup.Should().NotBeNull();
                var theExpression = theGroup.Expressions.FirstOrDefault(e => e.Prompt == expressionToVerify);
                theExpression.Should().NotBeNull();
                theExpression.RuleOperator = theOperator;
                theExpression.Value = value;
                return theExpression;
            }
        }

        private Workflow BuildCaseLengthWorkflow(string todoTitleToVerify, int length, string theOperator)
        {

            var ruleExpression = VerifyExpression("Case Information", "Calendar Days In Case", length, theOperator);
            using (var workflowService = new WorkflowService())
            {
                var rules = new List<Rule>(){
                    (Rule)workflowService.GetRule(ruleExpression)
                };

                WorkflowActivity reviewTheCase = new WorkflowActivity()
                {
                    Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"),
                    Name = "Review The Case",
                    ActivityId = new CaseReviewActivity().Id
                };

                reviewTheCase.Metadata.Set("Title", todoTitleToVerify);

                Workflow wf = new Workflow()
                {
                    Code = "CASELENGTH",
                    Name = "Case Length",
                    Description = "Test workflow that should fire when a case length is matched",
                    TargetEventType = EventType.CaseCreated,
                    Criteria = rules,
                    CriteriaSuccessType = RuleGroupSuccessType.And,
                    Activities = new List<WorkflowActivity>()
                    {
                        reviewTheCase
                    }
                };

                return wf.Save();
            }


        }

        [TestMethod]
        public void EmployeeWorkStateExpression()
        {
            using (var workflowService = new WorkflowService())
            using (var caseContext = new CaseContext())
            {
                string todoTitleToVerify = "Case review where a Employee Work State is ID";
                var workStateExpression = VerifyExpression("Employee Information", "Work State (Non-US)", "ID", "==");
                var rules = new List<Rule>()
                {
                    (Rule)workflowService.GetRule(workStateExpression)
                };

                WorkflowActivity reviewTheCase = new WorkflowActivity()
                {
                    Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"),
                    Name = "Review The Case",
                    ActivityId = new CaseReviewActivity().Id
                };

                reviewTheCase.Metadata.Set("Title", todoTitleToVerify);

                Workflow wf = new Workflow()
                {
                    Code = "CASECREATE",
                    Name = "Case Create",
                    Description = "Test workflow that should fire when a employee work state is Idaho",
                    TargetEventType = EventType.CaseCreated,
                    Criteria = rules,
                    CriteriaSuccessType = RuleGroupSuccessType.And,
                    Activities = new List<WorkflowActivity>()
                    {
                        reviewTheCase
                    }
                };

                wf.Save();

                caseContext.Case.Save();
                caseContext.Case.WfOnCaseCreated();

                var todos = caseContext.ToDos();
                var matchingTodo = todos.FirstOrDefault(t => t.Title == todoTitleToVerify);
                matchingTodo.Should().NotBeNull();

                wf.Delete();
            }
        }

        [TestMethod]
        public void CaseIsRelapseExpression()
        {
            using (var workflowService = new WorkflowService())
            using (var caseContext = new CaseContext())
            {
                string todoTitleToVerify = "Case review where a case is relapse";
                var workStateExpression = VerifyExpression("Case Information", "Is Relapse", true, "==");
                var rules = new List<Rule>()
                {
                    (Rule)workflowService.GetRule(workStateExpression)
                };

                WorkflowActivity reviewTheCase = new WorkflowActivity()
                {
                    Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"),
                    Name = "Review The Case",
                    ActivityId = new CaseReviewActivity().Id
                };
                reviewTheCase.Metadata.Set("Title", todoTitleToVerify);

                Workflow wf = new Workflow()
                {
                    Code = "CASECREATE",
                    Name = "Case Create",
                    Description = "Test workflow that should fire when a case is relapsed",
                    TargetEventType = EventType.CaseCreated,
                    Criteria = rules,
                    CriteriaSuccessType = RuleGroupSuccessType.And,
                    Activities = new List<WorkflowActivity>()
                    {
                        reviewTheCase
                    }
                };

                wf.Save();
                caseContext.Case.Metadata.Set("IsRelapse", true);
                caseContext.Case.Save();
                caseContext.Case.WfOnCaseCreated();

                var todos = caseContext.ToDos();
                var matchingTodo = todos.FirstOrDefault(t => t.Title == todoTitleToVerify);
                matchingTodo.Should().NotBeNull();

                wf.Delete();
            }
        }

        [TestMethod]
        public void HasPolicyDenialReasonExpression()
        {
            using (var workflowService = new WorkflowService())
            using (var caseContext = new CaseContext())
            {
                string todoTitleToVerify = "Case review where a case Has Policy Denial Reason";
                var workStateExpression = VerifyExpression("Case Information", "Policy Denial Reason", true, "==");
                workStateExpression.Parameters = new List<RuleExpressionParameter>();
                workStateExpression.Parameters.Add(new RuleExpressionParameter { StringValue = "ELIMINATIONPERIOD", Value = "ELIMINATIONPERIOD", Name = "Test" });
                var rules = new List<Rule>()
                {
                    (Rule)workflowService.GetRule(workStateExpression)
                };

                WorkflowActivity reviewTheCase = new WorkflowActivity()
                {
                    Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"),
                    Name = "Review The Case",
                    ActivityId = new CaseReviewActivity().Id
                };
                reviewTheCase.Metadata.Set("Title", todoTitleToVerify);

                Workflow wf = new Workflow()
                {
                    Code = "CASEDETERMINE",
                    Name = "Case Determine",
                    Description = "Test workflow that should fire when a policy is adjudicated",
                    TargetEventType = EventType.CaseExtended,
                    Criteria = rules,
                    CriteriaSuccessType = RuleGroupSuccessType.And,
                    Activities = new List<WorkflowActivity>()
                    {
                        reviewTheCase
                    }
                };

                wf.Save();
                caseContext.Case.Save();
                caseContext.Case.WfOnCaseExtended();
                //caseContext.Case.WfOnCaseAdjudicated(AdjudicationStatus.Denied, "ELIMINATIONPERIOD", "Elimination Period", null, null);

                var todos = caseContext.ToDos();
                var matchingTodo = todos.FirstOrDefault(t => t.Title == todoTitleToVerify);
                matchingTodo.Should().NotBeNull();

                wf.Delete();
            }
        }

        [TestMethod]
        public void HasAccomodationDenialReasonExpression()
        {
            using (var workflowService = new WorkflowService())
            using (var caseContext = new CaseContext())
            {
                string todoTitleToVerify = "Case review where a case Has Accomodation Denial Reason";
                var workStateExpression = VerifyExpression("Case Information", "Accommodation Denial Reason", true, "==");
                workStateExpression.Parameters = new List<RuleExpressionParameter>();
                workStateExpression.Parameters.Add(new RuleExpressionParameter { StringValue = "CANNOTACCOMODATE", Value = "CANNOTACCOMODATE", Name = "Test" });
                var rules = new List<Rule>()
                {
                    (Rule)workflowService.GetRule(workStateExpression)
                };

                WorkflowActivity reviewTheCase = new WorkflowActivity()
                {
                    Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"),
                    Name = "Review The Case",
                    ActivityId = new CaseReviewActivity().Id
                };
                reviewTheCase.Metadata.Set("Title", todoTitleToVerify);

                Workflow wf = new Workflow()
                {
                    Code = "CASEDETERMINE",
                    Name = "Case Determine",
                    Description = "Test workflow that should fire when a accomodation is adjudicated",
                    TargetEventType = EventType.CaseExtended,
                    Criteria = rules,
                    CriteriaSuccessType = RuleGroupSuccessType.And,
                    Activities = new List<WorkflowActivity>()
                    {
                        reviewTheCase
                    }
                };

                wf.Save();                
                caseContext.Case.AccommodationRequest = new AbsenceSoft.Data.Cases.AccommodationRequest();
                caseContext.Case.AccommodationRequest.Accommodations = new List<AbsenceSoft.Data.Cases.Accommodation>(0) { new AbsenceSoft.Data.Cases.Accommodation() {
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(7),
                    Status = CaseStatus.Open,
                    IsWorkRelated = false,
                    Cost = 500,
                    Determination = AdjudicationStatus.Denied,
                    Usage = new List<AbsenceSoft.Data.Cases.AccommodationUsage>(0){ new AbsenceSoft.Data.Cases.AccommodationUsage() {
                        DenialReasonCode ="CANNOTACCOMODATE",
                        Determination = AdjudicationStatus.Denied,
                        DenialReasonName = "Cannot Accomodate",
                        StartDate =DateTime.Now,
                        EndDate = DateTime.Now.AddDays(7)
                    } }
                } };
                caseContext.Case.Save();
                caseContext.Case.WfOnCaseExtended();

                var todos = caseContext.ToDos();
                var matchingTodo = todos.FirstOrDefault(t => t.Title == todoTitleToVerify);
                matchingTodo.Should().NotBeNull();

                wf.Delete();
            }
        }

        [TestMethod]
        public void PolicyDeterminationDeniedWorkflowTest()
        {
            using (CaseContext c = new CaseContext())
            {
                /// Deny the case
                new Logic.Cases.CaseService(false).Using(s => s.ApplyDetermination(c.Case, null, c.Case.StartDate, c.Case.EndDate.Value, AdjudicationStatus.Denied, AdjudicationDenialReason.NotASeriousHealthCondition, null, AdjudicationDenialReason.NotASeriousHealthConditionDescription));

                c.Case.WfOnCaseAdjudicated(AdjudicationStatus.Denied, AdjudicationDenialReason.NotASeriousHealthCondition, null, AdjudicationDenialReason.NotASeriousHealthConditionDescription);
                var todos = c.ToDos();
                todos.Count().Should().BeGreaterOrEqualTo(1);
                var approved = todos.FirstOrDefault(t => t.ItemType == ToDoItemType.Communication && t.Title == "Send Designation Notice Leave Denied");
                approved.Should().NotBeNull();
            }
        }

        [TestMethod]
        public void AccommodationDeniedExpressionTest()
        {
            string todoTitleToVerify = "IAmACaseReviewCreatedInResponseToAccommodationDeniedRule";
            Workflow wf = BuildAccommodationDeterminationWorkflow(todoTitleToVerify, "Accommodation Denial Reason");

            using (CaseContext c = new CaseContext())
            {
                var accommodation = BuildAccommodation(c.Case, "LEV", "Leave");
                c.Case.Save();
                c.Case.WfOnCaseCreated();
                accommodation.WfOnAccommodationCreated(c.Case);
                accommodation.Usage.FirstOrDefault().Determination = AdjudicationStatus.Denied;
                accommodation.WfOnAccommodationAdjudicated(c.Case, AdjudicationStatus.Denied, AccommodationAdjudicationDenialReason.CannotAccommodate, null, null, null);
                var todos = c.ToDos();
                var noCaseReviewToDo = todos.FirstOrDefault(t => t.Title == todoTitleToVerify);
                noCaseReviewToDo.Should().BeNull();
            }
            wf.Delete();
        }

        private Workflow BuildAccommodationDeterminationWorkflow(string todoTitleToVerify, string prompt)
        {
            using (var workflowService = new WorkflowService())
            {
                var expressionGroups = workflowService.GetExpressionsByEventType(EventType.AccommodationAdjudicated);
                var accommodationGroup = expressionGroups.FirstOrDefault(eg => eg.Title == "Accommodation");
                accommodationGroup.Should().NotBeNull();
                var deniedReasonAccommodationExpression = accommodationGroup.Expressions.FirstOrDefault(e => e.Prompt == prompt);
                deniedReasonAccommodationExpression.Should().NotBeNull();
                deniedReasonAccommodationExpression.RuleOperator = "==";
                deniedReasonAccommodationExpression.Value = true;
                var accommodationDeniedShouldbeOrConatinsCannotAccomodate = (Rule)workflowService.GetRule(deniedReasonAccommodationExpression);
                var rules = new List<Rule> { accommodationDeniedShouldbeOrConatinsCannotAccomodate };

                WorkflowActivity reviewTheCase = new WorkflowActivity()
                {
                    Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"),
                    Name = "Review The Case",
                    ActivityId = new CaseReviewActivity().Id
                };

                reviewTheCase.Metadata.Set("Title", todoTitleToVerify);

                Workflow wf = new Workflow()
                {
                    Code = "ACCOMMODATIONADJUDICATED",
                    Name = "Accommodation Adjudicated",
                    Description = "Test workflow that should fire when an accommodation adjudication is matched",
                    TargetEventType = EventType.AccommodationAdjudicated,
                    Criteria = rules,
                    CriteriaSuccessType = RuleGroupSuccessType.And,
                    Activities = new List<WorkflowActivity>()
                    {
                        reviewTheCase
                    }
                };

                return wf.Save();
            }
        }

        [TestMethod]
        public void HasPolicyStatusExpression()
        {
            using (var workflowService = new WorkflowService())
            using (var caseContext = new CaseContext())
            {
                string todoTitleToVerify = "Case review where a case Has Policy Status";
                var workStateExpression = VerifyExpression("Policy Information", "Policy Status", true, "==");
                workStateExpression.Parameters = new List<RuleExpressionParameter>();
                workStateExpression.Parameters.Add(new RuleExpressionParameter { StringValue = ((int)PolicyType.STD).ToString(), Value = PolicyType.STD, Name = "Test" });
                workStateExpression.Parameters.Add(new RuleExpressionParameter { StringValue = "1", Value = "1", Name = "Test" });
                var rules = new List<Rule>()
                {
                    (Rule)workflowService.GetRule(workStateExpression)
                };

                WorkflowActivity reviewTheCase = new WorkflowActivity()
                {
                    Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"),
                    Name = "Review The Case",
                    ActivityId = new CaseReviewActivity().Id
                };
                reviewTheCase.Metadata.Set("Title", todoTitleToVerify);

                Workflow wf = new Workflow()
                {
                    Code = "CASEDETERMINE",
                    Name = "Case Determine",
                    Description = "Test workflow that should fire when a policy is adjudicated",
                    TargetEventType = EventType.CaseExtended,
                    Criteria = rules,
                    CriteriaSuccessType = RuleGroupSuccessType.And,
                    Activities = new List<WorkflowActivity>()
                    {
                        reviewTheCase
                    }
                };

                wf.Save();
                caseContext.Case.Save();
                caseContext.Case.WfOnCaseExtended();

                var todos = caseContext.ToDos();
                var matchingTodo = todos.FirstOrDefault(t => t.Title == todoTitleToVerify);
                matchingTodo.Should().NotBeNull();

                wf.Delete();
            }
        }

        [TestMethod]
        public void HasAccomodationStatusExpression()
        {
            using (var workflowService = new WorkflowService())
            using (var caseContext = new CaseContext())
            {
                string todoTitleToVerify = "Case review where a case Has Accomodation Denial Reason";
                var workStateExpression = VerifyExpression("Accommodation", "Accommodation Status", true, "==");
                workStateExpression.Parameters = new List<RuleExpressionParameter>();
                workStateExpression.Parameters.Add(new RuleExpressionParameter { StringValue = "EA", Value = "EA", Name = "Test" });
                workStateExpression.Parameters.Add(new RuleExpressionParameter { StringValue = "2", Value = "2", Name = "Test" });
                var rules = new List<Rule>()
                {
                    (Rule)workflowService.GetRule(workStateExpression)
                };

                WorkflowActivity reviewTheCase = new WorkflowActivity()
                {
                    Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"),
                    Name = "Review The Case",
                    ActivityId = new CaseReviewActivity().Id
                };
                reviewTheCase.Metadata.Set("Title", todoTitleToVerify);

                Workflow wf = new Workflow()
                {
                    Code = "CASEDETERMINE",
                    Name = "Case Determine",
                    Description = "Test workflow that should fire when a accomodation is adjudicated",
                    TargetEventType = EventType.AccommodationAdjudicated,
                    Criteria = rules,
                    CriteriaSuccessType = RuleGroupSuccessType.And,
                    Activities = new List<WorkflowActivity>()
                    {
                        reviewTheCase
                    }
                };

                wf.Save();
                caseContext.Case.AccommodationRequest = new AbsenceSoft.Data.Cases.AccommodationRequest();
                caseContext.Case.AccommodationRequest.Accommodations = new List<AbsenceSoft.Data.Cases.Accommodation>(0) { new AbsenceSoft.Data.Cases.Accommodation() {
                    StartDate = DateTime.Now.ToMidnight(),
                    EndDate = DateTime.Now.AddDays(7).ToMidnight(),
                    Status = CaseStatus.Open,
                    IsWorkRelated = false,
                    Cost = 500,
                    Type = new AbsenceSoft.Data.Cases.AccommodationType{ Code="EA", Name="Ergonomic Assessment" },
                    Determination = AdjudicationStatus.Denied,
                    Usage = new List<AbsenceSoft.Data.Cases.AccommodationUsage>(0){ new AbsenceSoft.Data.Cases.AccommodationUsage() {
                        DenialReasonCode ="CANNOTACCOMODATE",
                        Determination = AdjudicationStatus.Denied,
                        DenialReasonName = "Cannot Accomodate",
                        StartDate =DateTime.Now.ToMidnight(),
                        EndDate = DateTime.Now.AddDays(7).ToMidnight()
                    } }
                } };
                caseContext.Case.Save();
                caseContext.Case.WfOnAccommodationAdjudicated(caseContext.Case.AccommodationRequest.Accommodations[0], AdjudicationStatus.Denied, "", "", null);

                var todos = caseContext.ToDos();
                var matchingTodo = todos.FirstOrDefault(t => t.Title == todoTitleToVerify);
                matchingTodo.Should().NotBeNull();

                wf.Delete();
            }
        }

        [TestMethod]
        public void HasAccomodationTypeExpression()
        {
            using (var workflowService = new WorkflowService())
            using (var caseContext = new CaseContext())
            {
                string todoTitleToVerify = "Case review where a case Has Accomodation Type";
                var workStateExpression = VerifyExpression("Accommodation", "Has Accommodation Type", true, "==");
                workStateExpression.Parameters = new List<RuleExpressionParameter>();
                workStateExpression.Parameters.Add(new RuleExpressionParameter { StringValue = "EA", Value = "EA", Name = "Test" });
                var rules = new List<Rule>()
                {
                    (Rule)workflowService.GetRule(workStateExpression)
                };

                WorkflowActivity reviewTheCase = new WorkflowActivity()
                {
                    Id = new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"),
                    Name = "Review The Case",
                    ActivityId = new CaseReviewActivity().Id
                };
                reviewTheCase.Metadata.Set("Title", todoTitleToVerify);

                Workflow wf = new Workflow()
                {
                    Code = "CASEDETERMINE",
                    Name = "Case Determine",
                    Description = "Test workflow that should fire when a accomodation is adjudicated",
                    TargetEventType = EventType.CaseExtended,
                    Criteria = rules,
                    CriteriaSuccessType = RuleGroupSuccessType.And,
                    Activities = new List<WorkflowActivity>()
                    {
                        reviewTheCase
                    }
                };

                wf.Save();
                caseContext.Case.AccommodationRequest = new AbsenceSoft.Data.Cases.AccommodationRequest();
                caseContext.Case.AccommodationRequest.Accommodations = new List<AbsenceSoft.Data.Cases.Accommodation>(0) { new AbsenceSoft.Data.Cases.Accommodation() {
                    StartDate = DateTime.Now.ToMidnight(),
                    EndDate = DateTime.Now.AddDays(7).ToMidnight(),
                    Status = CaseStatus.Open,
                    IsWorkRelated = false,
                    Cost = 500,
                    Type = new AbsenceSoft.Data.Cases.AccommodationType{ Code="EA", Name="Ergonomic Assessment" },
                    Determination = AdjudicationStatus.Denied,
                    Usage = new List<AbsenceSoft.Data.Cases.AccommodationUsage>(0){ new AbsenceSoft.Data.Cases.AccommodationUsage() {
                        DenialReasonCode ="CANNOTACCOMODATE",
                        Determination = AdjudicationStatus.Denied,
                        DenialReasonName = "Cannot Accomodate",
                        StartDate =DateTime.Now.ToMidnight(),
                        EndDate = DateTime.Now.AddDays(7).ToMidnight()
                    } }
                } };
                caseContext.Case.Save();
                caseContext.Case.WfOnCaseExtended();

                var todos = caseContext.ToDos();
                var matchingTodo = todos.FirstOrDefault(t => t.Title == todoTitleToVerify);
                matchingTodo.Should().NotBeNull();

                wf.Delete();
            }
        }
    }
}