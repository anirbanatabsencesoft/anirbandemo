﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AbsenceSoft.Tests.Common
{
    [TestClass]
    public class DateTimeTest
    {
        [TestMethod]
        public void DateTimeTests()
        {
            // one unit test for all the DateTime methods

            // leap year test(s)
            DateTime dob = new DateTime(2004, 2, 29);
            int age = dob.AgeInYears(new DateTime(2011, 2, 28));
            Assert.IsTrue(age == 6);

            age = dob.AgeInYears(new DateTime(2011, 3, 1));
            Assert.IsTrue(age == 7);

            age = dob.AgeInYears(new DateTime(2012, 2, 28));
            Assert.IsTrue(age == 7);
            age = dob.AgeInYears(new DateTime(2012, 2, 29));
            Assert.IsTrue(age == 8);
            age = dob.AgeInYears(new DateTime(2012, 3, 1));
            Assert.IsTrue(age == 8);


            dob = new DateTime(2004, 2, 28);
            age = dob.AgeInYears(new DateTime(2011, 2, 28));
            Assert.IsTrue(age == 7);

            age = dob.AgeInYears(new DateTime(2011, 3, 1));
            Assert.IsTrue(age == 7);

        }

        [TestMethod]
        public void IsSameTimeAs_DifferentKind_ReturnsFalse()
        {
            //// Arrange
            var currentDateTime = DateTime.UtcNow;
            var refDate = new DateTime(currentDateTime.Ticks, DateTimeKind.Utc);
            var compareDate = new DateTime(currentDateTime.Ticks, DateTimeKind.Local);

            //// Act
            var result = refDate.IsSameTimeAs(compareDate);

            //// Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsSameTimeAs_NegativeDifferenceInDateTime_ReturnsFalse()
        {
            //// Arrange
            var currentDateTime = DateTime.UtcNow;
            var refDate = new DateTime(currentDateTime.Ticks, DateTimeKind.Utc);
            var compareDate = new DateTime(currentDateTime.AddMilliseconds(2000).Ticks, DateTimeKind.Utc);

            //// Act
            var result = refDate.IsSameTimeAs(compareDate);

            //// Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsSameTimeAs_DifferentDateTime_ReturnsFalse()
        {
            //// Arrange
            var currentDateTime = DateTime.UtcNow;
            var compareDate = new DateTime(currentDateTime.Ticks, DateTimeKind.Utc);
            var refDate = new DateTime(currentDateTime.AddMilliseconds(2000).Ticks, DateTimeKind.Utc);

            //// Act
            var result = refDate.IsSameTimeAs(compareDate);

            //// Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsSameTimeAs_DifferentDateTimeWithAcceptableDelta_ReturnsTrue()
        {
            //// Arrange
            var currentDateTime = DateTime.UtcNow;
            var refDate = new DateTime(currentDateTime.Ticks, DateTimeKind.Utc);
            var compareDate = new DateTime(currentDateTime.AddMilliseconds(2000).Ticks, DateTimeKind.Utc);

            //// Act
            var result = refDate.IsSameTimeAs(compareDate, 2000);

            //// Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsSameTimeAs_SameDateTime_ReturnsTrue()
        {
            //// Arrange
            var currentDateTime = DateTime.UtcNow;
            var refDate = new DateTime(currentDateTime.Ticks, DateTimeKind.Utc);
            var compareDate = new DateTime(currentDateTime.Ticks, DateTimeKind.Utc);

            //// Act
            var result = refDate.IsSameTimeAs(compareDate);

            //// Assert
            Assert.IsTrue(result);
        }
    }
}
