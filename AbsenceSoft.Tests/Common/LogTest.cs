﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Common;

namespace AbsenceSoft.Tests.Common
{
    [TestClass]
    public class LogTest
    {
        [TestMethod, TestCategory("Logging")]
        public void LogError()
        {
            try
            {
                Log.Error("TEST: LogError");
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod, TestCategory("Logging")]
        public void LogException()
        {
            try
            {
                Log.Error(new Exception("TEST: LogException"));
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod, TestCategory("Logging")]
        public void LogErrorWithException()
        {
            try
            {
                Log.Error("TEST: LogErrorWithException", new Exception("TEST: LogErrorWithException"));
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod, TestCategory("Logging")]
        public void LogInfo()
        {
            try
            {
                Log.Info("TEST: LogInfo");
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod, TestCategory("Logging")]
        public void LogWarning()
        {
            try
            {
                Log.Warn("TEST: LogWarning");
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod, TestCategory("Logging")]
        public void LogDebug()
        {
            try
            {
                Log.Debug("TEST: LogDebug");
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }
    }
}
