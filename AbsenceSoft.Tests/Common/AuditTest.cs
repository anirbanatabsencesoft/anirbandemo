﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using AbsenceSoft.Data.Audit;
using System.Linq;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Cases;

namespace AbsenceSoft.Tests.Common
{
    [TestClass]
    public class AuditTest
    {
        [TestMethod]
        public void AuditTestOnData()
        {
            // need an emp for the case
            Employee emp = Employee.AsQueryable().FirstOrDefault(fd=> fd.Id == "000000000000000000000075");
            Assert.IsNotNull(emp);

            // indexer test
            Case caseA = new Case()
            {
                EmployerId = emp.EmployerId,
                Employee = emp,
                CustomerId = emp.CustomerId
            };

            caseA.CustomFields.Add(new CustomField()
                {
                    DataType = AbsenceSoft.Data.Enums.CustomFieldType.Text,
                    IsRequired = false,
                    SelectedValue = "abc",
                    Description = "will it break?"
                });

            Case caseB = caseA.Clone<Case>();
            caseB.CustomFields[0].SelectedValue = 333;

            List<ChangeHistory> wth = ChangeHistory.GenereateAudit(caseA, caseB, Guid.NewGuid().ToString());

            Assert.IsNotNull(wth);
            Assert.AreEqual(wth.Count, 1);
            Assert.IsTrue(wth[0].Items.Count >= 1);
            var a = wth[0].Items.FirstOrDefault(i => i.ChangePath == "/Case/CustomFields[]");
            Assert.IsNotNull(a);
            Assert.AreEqual(a.Before, "abc");
            Assert.AreEqual(a.After, "333");

            // create a test parent object and populate it and it's child, clone it, change a couple values
            // and inspect the result object
            Guid testGuid = Guid.NewGuid(); // need a known value for the test

            AuditTestParent before = new AuditTestParent()
            {
                Id = Guid.NewGuid().ToString(),
                intValue1 = 1,
                intValue2 = 2,
                stringValue1 = "s1",
                stringValue2 = "s2",
                datetimeValue1 = new DateTime(2015, 2, 1, 0, 0, 0, DateTimeKind.Utc),
                IamIgnored = 1,
                ThisShouldBeIgnored = new List<string>() { "aa", "bb", "cc", "dd", "ee"}
            };
            before.TheChild.Id = Guid.NewGuid().ToString();
            before.TheChild.cIntValue1 = 3;
            before.TheChild.cIntValue2 = 4;
            before.TheChild.cStringValue1 = "cs1";
            before.TheChild.cStringValue2 = "cs2";
            before.TheList.Add(new AuditTestList() { foo = Guid.NewGuid(), lIntValue1 = 1, lStringValue1 = "10" });
            before.TheList.Add(new AuditTestList() { foo = testGuid, lIntValue1 = 2, lStringValue1 = "20" });
            before.TheList.Add(new AuditTestList() { foo = Guid.NewGuid(), lIntValue1 = 3, lStringValue1 = "30" });
            before.Override.IWasChanged = 5;
            before.Override.AnotherChange = 6;
            before.Override.NotTrackingMe = -1;
            before.AddStuff.IntValue1 = 6;
            before.MultipleKeys.Add(new AuditTestListMultipleKeys() { Key1 = 1, Key2 = "stringKey1", ChangedValue1 = "the quick", ChangedValue2 = "brown fox" });
            before.MultipleKeys.Add(new AuditTestListMultipleKeys() { Key1 = 1, Key2 = "stringKey2", ChangedValue1 = "now is the time", ChangedValue2 = "for all good men" });
            before.MultipleKeys.Add(new AuditTestListMultipleKeys() { Key1 = 2, Key2 = "stringKey1", ChangedValue1 = "now is the winter", ChangedValue2 = "of our discontent" });

            AuditTestParent after = before.Clone<AuditTestParent>();
            after.Override.ChangeHistoryReset();
            after.intValue1 = 10;
            after.stringValue1 = "s20";
            after.IamIgnored = 2;
            after.TheChild.cIntValue2 = 20;
            after.TheChild.cStringValue2 = "cs30";
            after.TheList[1].lIntValue1 = 77;
            after.Override.IWasChanged = 55;
            after.Override.AnotherChange = 66;
            after.AddStuff.IntValue1 = 999;
            after.MultipleKeys[1].ChangedValue1 = "to come to the aid";
            after.MultipleKeys[1].ChangedValue2 = "of their country";
            after.MultipleKeys[2].ChangedValue1 = "made glorious summer";
            after.MultipleKeys[2].ChangedValue2 = "by this son of York";

            List<ChangeHistory> chl = ChangeHistory.GenereateAudit(before, after, Guid.NewGuid().ToString());

            Assert.AreEqual(chl.Count, 1);
            ChangeHistory ch = chl[0];          // shortcut

            // nullable test check
            ChangeItem nullvs = ch.Items.FirstOrDefault(f => f.ChangePath == "/AuditTestParent" && f.PropertyName == "intValue1");
            Assert.IsNotNull(nullvs);
            Assert.AreEqual(nullvs.Before, "1");
            Assert.AreEqual(nullvs.After, "10");
            Assert.AreEqual(nullvs.Action, AuditAction.Save);
            Assert.AreEqual(nullvs.ClassType, "Int32");


            ChangeItem pvs = ch.Items.FirstOrDefault(f=> f.ChangePath == "/AuditTestParent" && f.PropertyName == "stringValue1");
            Assert.IsNotNull(pvs);
            Assert.AreEqual(pvs.Before,"s1");
            Assert.AreEqual(pvs.After,"s20");
            Assert.AreEqual(pvs.Action, AuditAction.Save);

            ChangeItem avs = ch.Items.FirstOrDefault(f => f.ChangePath ==  string.Format("/AuditTestParent/TheList[{0}]", testGuid) && f.PropertyName == "lIntValue1");
            Assert.IsNotNull(avs);
            Assert.AreEqual(avs.Before, "2");
            Assert.AreEqual(avs.After, "77");
            Assert.AreEqual(avs.AuditObjectId, testGuid.ToString());

            avs = ch.Items.FirstOrDefault(f => f.ChangePath == "/AuditTestParent/Override" && f.PropertyName == "IWasChanged");
            Assert.IsNotNull(avs);
            Assert.AreEqual(avs.Before, "5");
            Assert.AreEqual(avs.After, "55");

            avs = ch.Items.FirstOrDefault(f => f.ChangePath == "/AuditTestParent/MultipleKeys[2~stringKey1]" && f.PropertyName == "ChangedValue2");
            Assert.IsNotNull(avs);
            Assert.AreEqual(avs.AuditObjectId, "2~stringKey1");

            // this mimics an add. there should be 21 items, spot check 1
            chl = ChangeHistory.GenereateAudit(null, after, Guid.NewGuid().ToString());
            Assert.IsNotNull(chl);
            Assert.AreEqual(chl.Count, 1);
            ch = chl[0];
            //Assert.AreEqual(ch.Items.Count, 23);
            pvs = ch.Items.FirstOrDefault(f => f.ChangePath == "/AuditTestParent" && f.PropertyName == "stringValue1");
            Assert.AreEqual(pvs.Before, "null");
            Assert.AreEqual(pvs.After, "s20");
            Assert.AreEqual(pvs.Action, AuditAction.Create);

            // this mimics a delete. there should be 21 items, spot check 1
            chl = ChangeHistory.GenereateAudit(before, null, Guid.NewGuid().ToString());
            Assert.IsNotNull(chl);
            Assert.AreEqual(chl.Count, 1);
            ch = chl[0];
            //Assert.AreEqual(ch.Items.Count, 23);
            pvs = ch.Items.FirstOrDefault(f => f.ChangePath == "/AuditTestParent" && f.PropertyName == "stringValue1");
            Assert.AreEqual(pvs.Before, "s1");
            Assert.AreEqual(pvs.After, "null");
            Assert.AreEqual(pvs.Action, AuditAction.Delete);

        }
    }



    [Serializable]
    public class AuditTestParent
    {
        public string Id { get; set; }
        public int? intValue1 { get; set; }
        public int intValue2 { get; set; }

        public string stringValue1 { get; set; }
        public string stringValue2 { get; set; }

        public DateTime datetimeValue1 { get; set; }

        public AuditTestChild TheChild { get; set; }

        public List<AuditTestList> TheList { get; set; }
        public List<AuditTestList> NullList { get; set; }
        public List<AuditTestList> EmptyList { get; set; }

        public AuditOverrideTest Override { get; set; }
        public AuditTestAddStuff AddStuff { get; set; }

        public List<AuditTestListMultipleKeys> MultipleKeys { get; set; }

        [AuditPropertyNoAttribute]
        public int IamIgnored { get; set; }

        public List<string> ThisShouldBeIgnored { get; set; }

        public AuditTestParent()
        {
            TheChild = new AuditTestChild();
            TheList = new List<AuditTestList>();
            EmptyList = new List<AuditTestList>();
            Override = new AuditOverrideTest();
            AddStuff = new AuditTestAddStuff();
            MultipleKeys = new List<AuditTestListMultipleKeys>();
        }
    }

    [Serializable]
    public class AuditTestListMultipleKeys
    {
        [AuditClassPrimaryKey(1)]
        public string Key2 { get; set; }

        [AuditClassPrimaryKey(0)]
        public int Key1 { get; set; }

        public string ChangedValue1 { get; set; }
        public string ChangedValue2 { get; set; }
    }

    [Serializable]
    public class AuditTestChild
    {
        public string Id { get; set; }
        public int cIntValue1 { get; set; }
        public int cIntValue2 { get; set; }

        public string cStringValue1 { get; set; }
        public string cStringValue2 { get; set; }

        public DateTime cDatetimeValue1 { get; set; }
    }

    [Serializable]
    public class AuditTestList
    {
        // the default is to use the id field, this is just to test
        [AuditClassPrimaryKey(0)]
        public Guid foo { get; set; }
        public int lIntValue1 { get; set; }
        public string lStringValue1 { get; set; }
    }

    [Serializable]
    public class AuditOverrideTest : IAuditOverride
    {
        private AuditChangeHelper _helper { get; set; }

        public string Id { get; set; }

        private int _iWasChanged;
        public int IWasChanged
        {
            get
            {
                return _iWasChanged;
            }
            set
            {
                _helper.Change("IWasChanged", _iWasChanged.ToString(), value.ToString(), AuditAction.Save);
                _iWasChanged = value;
            }
        }

        private int _anotherChange;
        public int AnotherChange
        {
            get
            {
                return _anotherChange;
            }
            set
            {
                _helper.Change("AnotherChange", _anotherChange.ToString(), value.ToString(), AuditAction.Save);
                _anotherChange = value;
            }
        }

        public int NotTrackingMe { get; set; }

        public AuditOverrideTest()
        {
            Id = Guid.NewGuid().ToString();
            _helper = new AuditChangeHelper("AuditOverrideTest",Id);
        }
        

        public List<ChangeItem> GetAuditChangeItems(object before, object after)
        {
            return _helper.GetChanges();
        }

        public void ChangeHistoryReset()
        {
            _helper.Reset();
        }
    }                                           // public class AuditOverrideTest : IAuditOverride

    [Serializable]
    public class AuditTestAddStuff : IAuditInclude
    {
        public Guid Id { get; set; }
        public int IntValue1 { get; set; }

        public AuditTestAddStuff()
        {
            Id = Guid.NewGuid();
        }

        public List<ChangeItem> GetAuditChangeItems(object before, object after)
        {
            return new List<ChangeItem>() {
                new ChangeItem() { Action = AuditAction.Save, Before = "b", After= "bb", PropertyName="FooBurger", AuditObjectId = Id.ToString(), ClassType="wugawugawuga"},
                new ChangeItem() { Action = AuditAction.Save, Before = "z", After= "zz", PropertyName="BarDog", AuditObjectId = Id.ToString(), ClassType="wugawugawuga"},
                new ChangeItem() { Action = AuditAction.Save, Before = "h", After= "hh", PropertyName="SomeOtherStupidThing", AuditObjectId = Id.ToString(), ClassType="muwuhahahaha"}
            };
        }

        public void ChangeHistoryReset()
        {
            // does nothing
        }
    }


}
