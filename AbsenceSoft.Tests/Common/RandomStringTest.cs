﻿using AbsenceSoft.Common.Security;
using AbsenceSoft.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Tests.Common
{
    [TestClass]
    public class RandomStringTest
    {
        [TestMethod, TestCategory("ApiSecret")]
        public void GenerateSecretTest()
        {
            var random = RandomString.GenerateSecret();
            var secret = new CryptoString(random);
            secret = secret.Encrypt().Hash();

            Assert.IsNotNull(secret);
            
        }

    }
}
