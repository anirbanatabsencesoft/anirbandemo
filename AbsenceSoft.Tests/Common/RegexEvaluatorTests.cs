﻿using AbsenceSoft.Logic.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AbsenceSoft.Tests.Common
{
    [TestClass]
    public class RegexEvaluatorTests
    {
        [TestMethod]
        public void TestWrap()
        {
            var searchTerm = "tEsT";
            var row = "Mary Test (#10001) | DOB: 6/7/1986 | AbsenceSoftTest";
            var expected = "Mary <span>Test</span> (#10001) | DOB: 6/7/1986 | AbsenceSoft<span>Test</span>";
            var regex = new Regex(searchTerm, RegexOptions.IgnoreCase);
            var evaluateTitle = new MatchEvaluator(match => RegexEvaluators.Wrap(match, row, "<span>{0}</span>", exactMatch: false));
            var actual = regex.Replace(row, evaluateTitle);

            Assert.AreEqual(expected, actual);
        }
    }
}
