﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Enums;
using AbsenceSoft;

namespace AbsenceSoft.Tests.Common
{
    [TestClass]
    public class EnumTest
    {
        [TestMethod]
        public void TestSplitCamelCaseString()
        {
            Assert.AreEqual("HR", "HR".SplitCamelCaseString());
            Assert.AreEqual("Grandparent In Law", "GrandparentInLaw".SplitCamelCaseString());
            Assert.AreEqual("Legal Ward", "LegalWard".SplitCamelCaseString());
            Assert.AreEqual("XML Is Awesome Dude", "XMLIsAwesomeDude".SplitCamelCaseString());
            Assert.AreEqual("This Is JSON Man", "ThisIsJSONMan".SplitCamelCaseString());
        }
    }
}
