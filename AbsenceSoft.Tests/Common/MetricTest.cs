﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Common;

namespace AbsenceSoft.Tests.Common
{
    [TestClass]
    public class MetricTest
    {
        [TestMethod, TestCategory("Metrics")]
        public void Metrics()
        {
            Metric m = new Metric("MetricTest.Metrics").Save(DateTime.UtcNow.AddSeconds(1));
            Assert.IsNotNull(m);
            Assert.IsNotNull(m.Id);
            Assert.IsFalse(string.IsNullOrWhiteSpace(m.Id));
            Assert.IsTrue(m.Elapsed > 0);
        }
    }
}
