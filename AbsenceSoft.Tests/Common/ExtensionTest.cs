﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Tests.Common
{
    [TestClass]
    public class ExtensionTest
    {
        [TestMethod, TestCategory("Extensions")]
        public void FormatPhone1()
        {
            string testData = "+1.720.591.5632";

            string expected = "(720) 591-5632";
            string actual = testData.FormatPhone();

            Assert.AreEqual(expected, actual);
        }
        [TestMethod, TestCategory("Extensions")]
        public void FormatPhone2()
        {
            string testData = "0448052335544";

            string expected = "+44 8052335544";
            string actual = testData.FormatPhone();

            Assert.AreEqual(expected, actual);
        }
        [TestMethod, TestCategory("Extensions")]
        public void FormatPhone3()
        {
            string testData = "8105554444x123";

            string expected = "(810) 555-4444 x123";
            string actual = testData.FormatPhone();

            Assert.AreEqual(expected, actual);
        }
        [TestMethod, TestCategory("Extensions")]
        public void FormatPhone4()
        {
            string testData = "0448052335544x123456";

            string expected = "+44 8052335544 x123456";
            string actual = testData.FormatPhone();

            Assert.AreEqual(expected, actual);
        }
    }
}
