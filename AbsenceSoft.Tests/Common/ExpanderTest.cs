﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Tests.Common
{
    [TestClass]
    public class ExpanderTest
    {
        [TestMethod]
        public void ExpanderTest1()
        {
            ListCriteria crit = new ListCriteria();
            crit.PageNumber = 2;
            crit.PageSize = 50;
            crit.SortBy = "EmployeeName";
            crit.SortDirection = SortDirection.Ascending;
            crit.ExtensionData.Add("HiThere", "Dude");

            string json = JsonConvert.SerializeObject(crit);
            Assert.IsNotNull(json);
            Assert.IsTrue(json.Contains("\"HiThere\":\"Dude\""));

            ListCriteria crit2 = JsonConvert.DeserializeObject<ListCriteria>(json);
            Assert.IsNotNull(crit2);
            Assert.AreEqual("Dude", crit2.Get<string>("HiThere"));
        }
    }
}
