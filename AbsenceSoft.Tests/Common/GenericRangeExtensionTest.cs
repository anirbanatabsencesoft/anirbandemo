﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft;
using FluentAssertions;

namespace AbsenceSoft.Tests.Common
{
    [TestClass]
    public class GenericRangeExtensionTest
    {
        [TestMethod, TestCategory("Extensions")]
        public void IntInRange()
        {
            1.IsInRange(1, 1).Should().BeTrue();
            1.IsInRange(1, 2).Should().BeTrue();
            1.IsInRange(0, 1).Should().BeTrue();
            1.IsInRange(0, 2).Should().BeTrue();

            1.IsInRange(0, 0).Should().BeFalse();
            1.IsInRange(2, 2).Should().BeFalse();
            1.IsInRange(-1, 0).Should().BeFalse();
            1.IsInRange(2, 3).Should().BeFalse();
        }
    }
}
