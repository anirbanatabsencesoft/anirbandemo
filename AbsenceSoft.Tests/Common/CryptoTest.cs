﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Common.Security;

namespace AbsenceSoft.Tests.Common
{
    [TestClass]
    public class CryptoTest
    {
        [TestMethod, TestCategory("Encryption")]
        public void Encryption()
        {
            var testData = "ddtyrtyrtytyfjghgfjghjkfkluitruyityridhjhjfgjh";

            var cipher = Crypto.Encrypt(testData);
            var decryptedData = Crypto.Decrypt(cipher);

            Assert.AreEqual(testData, decryptedData);
        }

        [TestMethod, TestCategory("Encryption")]
        public void Hash()
        {
            var testData = "ddtyrtyrtytyfjghgfjghjkfkluitruyityridhjhjfgjh";

            var cipher = Crypto.Encrypt(testData);
            var decryptedData = Crypto.Decrypt(cipher);

            var hashData = Crypto.Sha256Hash(testData);
            var result = Crypto.CompareHash(hashData, testData);
            Assert.IsTrue(result);
        }
    }
}
