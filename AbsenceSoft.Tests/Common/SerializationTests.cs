﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Data;
using Newtonsoft.Json;
using FluentAssertions;
using AbsenceSoft.Common.Serializers;
using MongoDB.Bson;
using System.Collections.Generic;
using MongoDB.Bson.Serialization;
using MongoRepository;

namespace AbsenceSoft.Tests.Common
{
    [TestClass]
    public class SerializationTests
    {
        [TestMethod]
        public void TestMetadataDeserializationFromJSON()
        {
            string awesomeJSON = "{ \"Property\": \"And thennnnnnnnnnnnnnn?\", \"Movie\": \"Dude, Where's My Car?\", \"Genre\": \"Comedy\", \"Year\": 2000, \"Actors\": [\"Ashton Kutcher\", \"Jennifer Garner\"] }";

            SuperAwesomeSerializableExtensibleNonEntityThingy thingy = JsonConvert.DeserializeObject<SuperAwesomeSerializableExtensibleNonEntityThingy>(awesomeJSON, JsonSettings.SerializerSettings);

            thingy.Should().NotBeNull();
            thingy.Metadata.Should().NotBeNull();
            thingy.Metadata.GetRawValue<string>("Movie").Should().Be("Dude, Where's My Car?");
            thingy.Metadata.GetRawValue<string>("Genre").Should().Be("Comedy");
            thingy.Metadata.GetRawValue<int>("Year").Should().Be(2000);
            thingy.Metadata.GetRawValue<List<object>>("Actors").Should().BeEquivalentTo(new List<object>() { "Ashton Kutcher", "Jennifer Garner" });

            string outMyJSON = JsonConvert.SerializeObject(thingy, JsonSettings.SerializerSettings);

            outMyJSON.Should()
                .Contain("\"Property\":\"And thennnnnnnnnnnnnnn?\"").And
                .Contain("\"Movie\":\"Dude, Where's My Car?\"").And
                .Contain("\"Genre\":\"Comedy\"").And
                .Contain("\"Year\":2000").And
                .Contain("\"Actors\":[\"Ashton Kutcher\",\"Jennifer Garner\"]");
        }

        [Serializable]
        public class SuperAwesomeSerializableExtensibleNonEntityThingy : BaseNonEntity
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="SuperAwesomeSerializableExtensibleNonEntityThingy"/> class.
            /// </summary>
            public SuperAwesomeSerializableExtensibleNonEntityThingy() { }

            /// <summary>
            /// Gets or sets the property.
            /// </summary>
            /// <value>
            /// The property.
            /// </value>
            public string Property { get; set; }
        }

        [Serializable]
        public class TestThisDateRangeSerializerClass
        {
            public TestThisDateRangeSerializerClass()
            {
                Dates = new DateRange(new DateTime(2015, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc), null);
            }

            public DateRange Dates { get; set; }
        }

        [TestMethod]
        public void TestDateRangeJSONSerialization()
        {
            var tester = new TestThisDateRangeSerializerClass();
            var json = JsonConvert.SerializeObject(tester);
            json.Should().Contain("StartDate");
            tester = JsonConvert.DeserializeObject<TestThisDateRangeSerializerClass>(json);
            tester.Should().NotBeNull();
            tester.Dates.Should().NotBeNull();
            tester.Dates.IsNull.Should().BeFalse();
            tester.Dates = new DateRange(tester.Dates.StartDate, tester.Dates.StartDate.AddDays(30));
            json = JsonConvert.SerializeObject(tester);
            json.Should().Contain("EndDate");
            tester = JsonConvert.DeserializeObject<TestThisDateRangeSerializerClass>(json);
            tester.Should().NotBeNull();
            tester.Dates.Should().NotBeNull();
            tester.Dates.IsNull.Should().BeFalse();
            tester.Dates.IsPerpetual.Should().BeFalse();
        }

        [TestMethod]
        public void TestDateRangeBSONSerialization()
        {
            var tester = new TestThisDateRangeSerializerClass();
            var bson = tester.ToBsonDocument().ToString();
            bson.Should().Contain("StartDate");
            tester = BsonSerializer.Deserialize<TestThisDateRangeSerializerClass>(bson);
            tester.Should().NotBeNull();
            tester.Dates.Should().NotBeNull();
            tester.Dates.IsNull.Should().BeFalse();
            tester.Dates = new DateRange(tester.Dates.StartDate, tester.Dates.StartDate.AddDays(30));
            bson = tester.ToBsonDocument().ToString();
            bson.Should().Contain("EndDate");
            tester = BsonSerializer.Deserialize<TestThisDateRangeSerializerClass>(bson);
            tester.Should().NotBeNull();
            tester.Dates.Should().NotBeNull();
            tester.Dates.IsNull.Should().BeFalse();
            tester.Dates.IsPerpetual.Should().BeFalse();
        }
    }
}
