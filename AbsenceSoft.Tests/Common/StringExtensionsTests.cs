﻿using AbsenceSoft.Common.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AbsenceSoft.Tests.Common
{
    [TestClass]
    public class StringExtensionsTests
    {
        [TestMethod]
        public void IsValidUrl_NullUrl_ReturnsFalse()
        {
            //// Arrange
            string url = null;

            //// Act
            var validationResult = url.IsValidUrl();

            //// Assert
            Assert.IsFalse(validationResult);
        }

        [TestMethod]
        public void IsValidUrl_EmptyUrl_ReturnsFalse()
        {
            //// Arrange
            var url = string.Empty;

            //// Act
            var validationResult = url.IsValidUrl();

            //// Assert
            Assert.IsFalse(validationResult);
        }

        [TestMethod]
        public void IsValidUrl_WhitespaceUrl_ReturnsFalse()
        {
            //// Arrange
            var url = "  ";

            //// Act
            var validationResult = url.IsValidUrl();

            //// Assert
            Assert.IsFalse(validationResult);
        }

        [TestMethod]
        public void IsValidUrl_ValidAndInvlaidUrls_ReturnsCorrectValidationResult()
        {
            //// Arrange
            string url1 = "https://www.example.com";
            string url2 = "http://www.example.com";
            string url3 = "www.example.com";
            string url4 = "example.com";
            string url5 = "http://blog.example.com";
            string url6 = "http://www.example.com/product";
            string url7 = "http://www.example.com/products?id=1&page=2";
            string url8 = "http://www.example.com#up";
            string url9 = "http://255.255.255.255";
            string url10 = "http://invalid.com/perl.cgi?key= | http://web-site.com/cgi-bin/perl.cgi?key1=value1&key2";
            string url11 = "http://www.site.com:8008";

            //// Act
            //// Assert
            Assert.IsTrue(url1.IsValidUrl());
            Assert.IsTrue(url2.IsValidUrl());
            Assert.IsFalse(url3.IsValidUrl());
            Assert.IsFalse(url4.IsValidUrl());
            Assert.IsTrue(url5.IsValidUrl());
            Assert.IsTrue(url6.IsValidUrl());
            Assert.IsTrue(url7.IsValidUrl());
            Assert.IsTrue(url8.IsValidUrl());
            Assert.IsTrue(url9.IsValidUrl());
            Assert.IsFalse(url10.IsValidUrl());
            Assert.IsTrue(url11.IsValidUrl());
        }
    }
}
