﻿using System;
using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Policies;
using System.Collections.Generic;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic;
using AbsenceSoft.Common;
using AbsenceSoft;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Logic.Pay;
using static AbsenceSoft.Setup.Policies.PolicyBase;
using AbsenceSoft.Logic.CaseAssignmentRules;
using static AbsenceSoft.Logic.Cases.CaseService;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Common.Properties;
using System.Web;
using System.Security.Principal;
using MongoDB.Bson;

namespace AbsenceSoft.Tests.Services.Cases
{
    [TestClass]
    public class CaseServiceTest
    {
        const string CUSTOMER_ID = "000000000000000000000002"; // Test
        const string EMPLOYER_ID = "000000000000000000000002"; // Test
        const string EMPLOYEE_ID = "000000000000000000000003"; // FMLA Spouse at Same Employer Female
        static readonly DateTime START_DATE = new DateTime(2015, 8, 31, 0, 0, 0, 0, DateTimeKind.Utc);
        static readonly DateTime END_DATE = new DateTime(2015, 9, 18, 0, 0, 0, 0, DateTimeKind.Utc);
        const string REASON_ID = "000000000000000001000000"; // Employee Health Condition
        const string REASON_CODE = "EHC"; // Employee Health Condition
        const string USER_ID = "000000000000000000000002"; // qa@absencesoft.com

        [TestMethod]
        public void GetAbsenceReasons()
        {
            using (CaseService svc = new CaseService())
            {
                var emp = Employee.AsQueryable().Where(e => e.EmployeeNumber == "demo_001").FirstOrDefault();
                Assert.IsNotNull(emp);
                var reasons = svc.GetAbsenceReasons(emp.Id);
                Assert.IsNotNull(reasons);
                Assert.IsTrue(reasons.Any());
            }
        }

        [TestMethod]
        public void CreateCase()
        {
            using (CaseService svc = new CaseService())
            {
                var emp = Employee.AsQueryable().Where(e => e.EmployeeNumber == "demo_001").FirstOrDefault();
                Assert.IsNotNull(emp);
                var reasons = svc.GetAbsenceReasons(emp.Id);
                Assert.IsNotNull(reasons);
                Assert.IsTrue(reasons.Any());
                var r = reasons.First();

                Case.AsQueryable().Where(c => c.Employee.Id == emp.Id && c.Reason.Code == r.Code).ForEach(a => a.Delete());

                var newCase = svc.CreateCase(CaseStatus.Open, emp.Id, DateTime.Today, DateTime.Today.AddDays(15), AbsenceSoft.Data.Enums.CaseType.Consecutive, r.Code, description: "CaseServiceTest.CreateCase");
                Assert.IsNotNull(newCase);
                newCase = svc.UpdateCase(newCase);
                Assert.IsNotNull(newCase.Id);
            }
        }

        [TestMethod, ExpectedException(typeof(AbsenceSoftException))]
        public void CaseOverlapStartException()
        {
            using (CaseService svc = new CaseService())
            {
                string empId = "000000000000000000000001";
                DateTime startDate = new DateTime(2014, 4, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime endDate = new DateTime(2014, 5, 1, 0, 0, 0, DateTimeKind.Utc);
                string reasonCode = AbsenceReason.GetByCode("EHC").Code;

                var ogCase = svc.CreateCase(CaseStatus.Open, empId, startDate, endDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reasonCode).Save();
                try
                {
                    svc.CreateCase(CaseStatus.Open, empId, startDate.AddDays(-1), startDate.AddDays(1), AbsenceSoft.Data.Enums.CaseType.Consecutive, reasonCode);
                }
                finally
                {
                    ogCase.Delete();
                }
            }
        }

        [TestMethod, ExpectedException(typeof(AbsenceSoftException))]
        public void CaseOverlapEndException()
        {
            using (CaseService svc = new CaseService())
            {
                string empId = "000000000000000000000001";
                DateTime startDate = new DateTime(2014, 4, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime endDate = new DateTime(2014, 5, 1, 0, 0, 0, DateTimeKind.Utc);
                string reasonCode = AbsenceReason.GetByCode("EHC").Code;

                var ogCase = svc.CreateCase(CaseStatus.Open, empId, startDate, endDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reasonCode).Save();
                try
                {
                    svc.CreateCase(CaseStatus.Open, empId, endDate.AddDays(-1), endDate.AddDays(1), AbsenceSoft.Data.Enums.CaseType.Consecutive, reasonCode);
                }
                finally
                {
                    ogCase.Delete();
                }
            }
        }

        [TestMethod, ExpectedException(typeof(AbsenceSoftException))]
        public void CaseOverlapBothException()
        {
            using (CaseService svc = new CaseService())
            {
                string empId = "000000000000000000000001";
                DateTime startDate = new DateTime(2014, 4, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime endDate = new DateTime(2014, 5, 1, 0, 0, 0, DateTimeKind.Utc);
                string reasonCode = AbsenceReason.GetByCode("EHC").Code;

                var ogCase = svc.CreateCase(CaseStatus.Open, empId, startDate, endDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reasonCode).Save();
                try
                {
                    svc.CreateCase(CaseStatus.Open, empId, startDate.AddDays(-1), endDate.AddDays(1), AbsenceSoft.Data.Enums.CaseType.Consecutive, reasonCode);
                }
                finally
                {
                    ogCase.Delete();
                }
            }
        }

        [TestMethod, ExpectedException(typeof(AbsenceSoftException))]
        public void CaseOverlapMiddleException()
        {
            using (CaseService svc = new CaseService())
            {
                string empId = "000000000000000000000001";
                DateTime startDate = new DateTime(2014, 4, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime endDate = new DateTime(2014, 5, 1, 0, 0, 0, DateTimeKind.Utc);
                string reasonCode = AbsenceReason.GetByCode("EHC").Code;

                var ogCase = svc.CreateCase(CaseStatus.Open, empId, startDate, endDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reasonCode).Save();
                try
                {
                    svc.CreateCase(CaseStatus.Open, empId, startDate.AddDays(1), endDate.AddDays(-1), AbsenceSoft.Data.Enums.CaseType.Consecutive, reasonCode);
                }
                finally
                {
                    ogCase.Delete();
                }
            }
        }

        /// <summary>
        /// Tests that when Cases overlap but their segments do not that no exception is thrown
        /// </summary>
        [TestMethod]
        public void CaseSegmentNoOverlap()
        {



            using (CaseService svc = new CaseService())
            {
                string empId = "000000000000000000000001";
                DateTime consecutiveStartDate = new DateTime(2014, 4, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime consecutiveEndDate = new DateTime(2014, 5, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime intermittentStartDate = new DateTime(2014, 10, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime intermittentEndDate = new DateTime(2014, 11, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime newConsecutiveStartDate = new DateTime(2014, 2, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime newConsecutiveEndDate = new DateTime(2014, 3, 1, 0, 0, 0, DateTimeKind.Utc);
                string reasonCode = AbsenceReason.GetByCode("EHC").Code;

                var originalCase = svc.CreateCase(CaseStatus.Open, empId, consecutiveStartDate, consecutiveEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reasonCode).Save();
                var intermittentCase = svc.CreateCase(CaseStatus.Open, empId, intermittentStartDate, intermittentEndDate, CaseType.Intermittent, reasonCode).Save();
                svc.ChangeCase(intermittentCase, newConsecutiveStartDate, newConsecutiveEndDate, true, false, CaseType.Consecutive, false, false);
                svc.UpdateCase(intermittentCase);
                originalCase.Delete();
                intermittentCase.Delete();
            }
        }



        /// <summary>
        /// Tests that Intermittent Leave does not Exceed Allowance when it is set in between of reduced leave
        /// </summary>
        [TestMethod]
        public void IntermittentLeaveDoesNotExceedAllowance()
        {

            var testPolicy = new Policy()
            {
                Name = "AT-5114",
                Code = "5114",
                AbsenceReasons = new List<PolicyAbsenceReason>()
                {
                    new PolicyAbsenceReason()
                    {
                        Reason = Reasons.EmployeeHealthCondition,
                        //new code for manual policy addition will add manual policy if its case type contains segment case type.
                        CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced , 
                        EntitlementType = EntitlementType.WorkingHours,
                        Entitlement = 40
                    }
                }
            };

            testPolicy.Save();

            using (CaseService svc = new CaseService())
            using (EligibilityService eligSvc = new EligibilityService())
            {
                string empId = "000000000000000000000001";

                DateTime reducedStartDate = new DateTime(2017, 8, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime reducedEndDate = new DateTime(2017, 8, 31, 0, 0, 0, DateTimeKind.Utc);
                DateTime intermittentStartDate = new DateTime(2017, 08, 2, 0, 0, 0, DateTimeKind.Utc);
                DateTime intermittentEndDate = new DateTime(2017, 08, 2, 0, 0, 0, DateTimeKind.Utc);

                string reasonCode = AbsenceReason.GetByCode("EHC").Code;

                var newCase = svc.CreateCase(CaseStatus.Open, empId, reducedStartDate, reducedEndDate, CaseType.Reduced, reasonCode).Save();
                AbsenceSoft.Data.Schedule reduced = new AbsenceSoft.Data.Schedule()
                {
                    StartDate = new DateTime(2017, 8, 1, 0, 0, 0, DateTimeKind.Utc),
                    EndDate = new DateTime(2017, 8, 31, 0, 0, 0, DateTimeKind.Utc),
                    ScheduleType = AbsenceSoft.Data.Enums.ScheduleType.Weekly
                };
                foreach (AbsenceSoft.Data.Time t in newCase.Employee.WorkSchedules[0].Times)
                    reduced.Times.Add(new AbsenceSoft.Data.Time()
                    {
                        SampleDate = t.SampleDate,
                        TotalMinutes = 120
                    });

                newCase.Segments[0].LeaveSchedule = new List<AbsenceSoft.Data.Schedule>();
                newCase.Segments[0].LeaveSchedule.Add(reduced);

                newCase.Segments[0].Type = AbsenceSoft.Data.Enums.CaseType.Reduced;
                eligSvc.AddManualPolicy(newCase, "5114", "adding 5114 a manual policy");
                eligSvc.RunEligibility(newCase);
                newCase = svc.ApplyDetermination(newCase, null, reducedStartDate, reducedEndDate, AdjudicationStatus.Approved);
                newCase = svc.UpdateCase(newCase);

                var usage89 = newCase.Segments[0].AppliedPolicies[0].Usage.Where(u => u.DateUsed == Date.NewUtcDate(2017, 8, 8)).ToList();
                usage89.First().Determination.Should().Be(AdjudicationStatus.Approved, "Time for 08/8 should be Approved");

                svc.ChangeCase(newCase, intermittentStartDate, intermittentEndDate, false, false, CaseType.Intermittent, false, false);
                newCase = svc.ApplyDetermination(newCase, null, intermittentStartDate, intermittentEndDate, AdjudicationStatus.Approved);
                newCase = svc.UpdateCase(newCase);

                newCase = svc.CreateOrModifyTimeOffRequest(newCase, new List<IntermittentTimeRequest>(5)
                {
                    new IntermittentTimeRequest()
                    {
                        RequestDate = Date.NewUtcDate(2017, 8, 2),
                        TotalMinutes = 480,
                        Detail = newCase.Segments[0].AppliedPolicies.Select(p => new IntermittentTimeRequestDetail()
                        {
                            PolicyCode = p.Policy.Code,
                            Approved = 480,
                            Pending = 0,
                            Denied = 0
                        }).ToList()
                    }
                });

                newCase = svc.UpdateCase(newCase);

                var usage802 = newCase.Segments[1].AppliedPolicies[0].Usage.Where(u => u.DateUsed == Date.NewUtcDate(2017, 8, 2)).ToList();
                usage802.First().Determination.Should().Be(AdjudicationStatus.Approved, "Time for 08/02 should be Approved");

                usage89 = newCase.Segments[2].AppliedPolicies[0].Usage.Where(u => u.DateUsed == Date.NewUtcDate(2017, 8, 9)).ToList();
                usage89.First().Determination.Should().Be(AdjudicationStatus.Denied, "Time for 08/9 should be denied");


            }

        }



        [TestMethod]
        public void CaseSummaryDoesNotFillInPolicyGaps()
        {
            using (CaseService svc = new CaseService())
            using (EligibilityService eligSvc = new EligibilityService())
            {
                string empId = "000000000000000000000002";
                DateTime intermittentStartDate = new DateTime(2014, 10, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime intermittentEndDate = new DateTime(2014, 11, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime newConsecutiveStartDate = new DateTime(2014, 2, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime newConsecutiveEndDate = new DateTime(2014, 3, 1, 0, 0, 0, DateTimeKind.Utc);
                string reasonCode = AbsenceReason.GetByCode("EHC").Code;

                var intermittentCase = svc.CreateCase(CaseStatus.Open, empId, intermittentStartDate, intermittentEndDate, CaseType.Intermittent, reasonCode);
                eligSvc.AddManualPolicy(intermittentCase, "FMLA", "testing adding a manual policy");
                eligSvc.RunEligibility(intermittentCase);
                svc.ChangeCase(intermittentCase, newConsecutiveStartDate, newConsecutiveEndDate, true, false, CaseType.Consecutive, false, false);
                eligSvc.RunEligibility(intermittentCase);

                var policy = intermittentCase.Summary.Policies.First();
                var calculatedPolicyEndDate = policy.Detail.Where(f => f.CaseType == CaseType.Consecutive).Max(f => f.EndDate);

                Assert.AreEqual(newConsecutiveEndDate, calculatedPolicyEndDate,
                    string.Format("Case Summary for Policy {0} calculated incorrect end date", policy.PolicyName));
            }
        }


        /// <summary>
        /// Deletes the test cases and related data for an employee.
        /// </summary>
        /// <param name="empId">The emp identifier.</param>
        private void deleteTestCases(string empId)
        {
            Case.Repository.Collection.Remove(Case.Query.EQ(c => c.Employee.Id, empId));
            ToDoItem.Repository.Collection.Remove(ToDoItem.Query.EQ(c => c.EmployeeId, empId));
            Communication.Repository.Collection.Remove(Communication.Query.EQ(c => c.EmployeeId, empId));
            Attachment.Repository.Collection.Remove(Attachment.Query.EQ(c => c.EmployeeId, empId));
        }


        [TestMethod]
        public void FMLAExhaustion()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            CaseService cs = new CaseService();
            // make the case 13 weeks long
            Case newCase = cs.CreateCase(CaseStatus.Open, empId, new DateTime(2014, 3, 2, 0, 0, 0, DateTimeKind.Utc), new DateTime(2014, 5, 31, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
            //Case newCase = cs.CreateCase(empId, new DateTime(2014, 3, 2, 0, 0, 0, DateTimeKind.Utc), new DateTime(2014, 3, 4, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(newCase);

            // make sure there is an fmla policy
            AppliedPolicy fmla = newCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
            Assert.IsNotNull(fmla, "FMLA Policy not found");

            Assert.AreEqual(91, fmla.Usage.Count);        // 60 days approved, 4 days exhausted, 27 non-working days 

            double minsUsed = fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.MinutesUsed);
            double daysUsed = Math.Round(fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.PercentOfDay), 4);
            double weeksUsed = Math.Round(fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.DaysUsage(EntitlementType.WorkWeeks)), 4);

            Assert.IsTrue(minsUsed == 28800);
            Assert.IsTrue(daysUsed == 60);
            Assert.IsTrue(weeksUsed == 12);

            // the first exhausted day is Tuesday after the 12th weeks ends because the Monday is Memorial Day.
            AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);

            // fix for 0 time used processing has moved it back to that Saturday
            // 27-May-2014 is Tuesday and should be the first exhausted day
            Assert.AreEqual(new DateTime(2014, 5, 24, 0, 0, 0, DateTimeKind.Utc), apu.DateUsed);

        }


        [TestMethod]
        public void WiFmlEligibilityTest()
        {

            string customerId = "000000000000000000000002";
            string employerId = "000000000000000000000002";
            //We need to change the global rule of MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup to 15 from 50
            //, as in test db we don't have 50 employee for WI state so this rule will always fail and WI-FML status will remain
            //inelibilble , so clone the existing Core policy and make the changes once done , delete it

            var testWIFMLPolicy = Policy.GetByCode("WI-FML", customerId, employerId);
            if (testWIFMLPolicy.CustomerId  == null)
            {
                testWIFMLPolicy = testWIFMLPolicy.Clone();
                testWIFMLPolicy.CustomerId = customerId;
                testWIFMLPolicy.EmployerId = employerId;
                testWIFMLPolicy.RuleGroups[0].Rules[0].RightExpression = "15";
                testWIFMLPolicy.Save();
            }

            //add test employee



            Employee testEmployee = new Employee()
            {
                EmployeeNumber = "TestEmp002",
                CustomerId = customerId,
                EmployerId = employerId,
                FirstName = "Test",
                LastName = "Emp 002",
                IsExempt = false,
                IsKeyEmployee = false,
                WorkCountry="US",
                WorkState ="WI",
                PriorHours=new List<PriorHours>()
                {
                    new PriorHours() {HoursWorked=1200, AsOf = new DateTime(2016,01,01)}
                },

                ServiceDate = new DateTime(2016, 01, 01),
                WorkSchedules = new List<Schedule>() {
                    new Schedule() { StartDate = new DateTime(2016,01,01), ScheduleType = ScheduleType.Weekly, Times = TestWorkSchedules.MakeSchedule(new DateTime(2016,01,01),8)}
                }
            };
            testEmployee.Save();

            string empId = testEmployee.Id;
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            CaseService cs = new CaseService();
            // make the case 13 weeks long
            Case newCase = cs.CreateCase(CaseStatus.Open, empId, new DateTime(2018, 1, 1, 0, 0, 0, DateTimeKind.Utc), new DateTime(2018, 01, 24, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(newCase);

            // make sure there is an fmla policy
            AppliedPolicy wifml = newCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "WI-FML");
            Assert.IsNotNull(wifml, "WI-FML");

            //Last day of two weeks it should be pending          
            //AppliedPolicyUsage apuPending = wifml.Usage.FirstOrDefault(u => u.DateUsed == new DateTime(2018, 1, 12, 0, 0, 0, DateTimeKind.Utc));
            //Assert.AreEqual(true,apuPending.Determination == AdjudicationStatus.Pending);

            ////Two weeks completed so it should be Denied
            //AppliedPolicyUsage apu = wifml.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
            //Assert.AreEqual(new DateTime(2018, 1, 13, 0, 0, 0, DateTimeKind.Utc), apu.DateUsed);

            //do the Cleanup
            testWIFMLPolicy.Delete();
            testEmployee.Delete();


        }

        // I need some more info on how to set this up
        [TestMethod,Ignore]
        public void FMLASpouseSharedCalc()
        {
            // these 2 are already spouses
            string wife = "000000000000000000000003";              // the wife
            string husband = "000000000000000000000004";        // the husband

            string caseCode = "FHC";
            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(wife);
            deleteTestCases(husband);

            CaseService cs = new CaseService();
            // let's give the first employee 10 weeks off
            DateTime wifeStart = new DateTime(2014, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime wifeEnd = new DateTime(2014, 3, 14, 0, 0, 0, DateTimeKind.Utc);
            Case wifeCase = cs.CreateCase(CaseStatus.Open, wife, wifeStart, wifeEnd, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
            wifeCase.Contact = new EmployeeContact()
            {
                ContactTypeCode = "CHILD",
                ContactTypeName = "Child",
                CustomerId = wifeCase.CustomerId,
                EmployeeId = wifeCase.Employee.Id,
                YearsOfAge = 5,
                MilitaryStatus = MilitaryStatus.Civilian,
                Contact = new Contact()
                {
                    FirstName = "Little",
                    LastName = "Johnny",
                    Title = "Bratt"
                }
            };

            EligibilityService esvc = new EligibilityService();
            esvc.RunEligibility(wifeCase);

            wifeCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).Where(p => p.Policy.Code == "FMLA").ToList()
            .ForEach(ap => cs.ApplyDetermination(wifeCase, ap.Policy.Code, wifeStart, wifeEnd, AdjudicationStatus.Approved));


            // make sure there is an fmla policy
            AppliedPolicy fmla = wifeCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
            Assert.IsNotNull(fmla, "FMLA Policy not found");

            double minsUsed = fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.MinutesUsed);
            double daysUsed = Math.Round(fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.PercentOfDay), 4);
            double weeksUsed = Math.Round(fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.DaysUsage(EntitlementType.WorkWeeks)), 4);

            /// weeks used should be 10.5 cause holidays?
            Assert.IsTrue(weeksUsed == 10.5);
            Assert.IsTrue(daysUsed == 10 * 5);
            Assert.IsTrue(minsUsed == 10 * 5 * 8 * 60);

            // and save the wife case so it is there when we calc the husband
            wifeCase.Save();

            // now give the husband a day off
            DateTime husbandStart = new DateTime(2014, 3, 10, 0, 0, 0, DateTimeKind.Utc);
            DateTime husbandEnd = new DateTime(2014, 6, 1, 0, 0, 0, DateTimeKind.Utc);
            Case husbandCase = cs.CreateCase(CaseStatus.Open, husband, husbandStart, husbandEnd, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
            husbandCase.Contact = new EmployeeContact()
            {
                ContactTypeCode = "CHILD",
                ContactTypeName = "Child",
                CustomerId = wifeCase.CustomerId,
                EmployeeId = wifeCase.Employee.Id,
                YearsOfAge = 5,
                MilitaryStatus = MilitaryStatus.Civilian,
                Contact = new Contact()
                {
                    FirstName = "Little",
                    LastName = "Johnny",
                    Title = "Bratt"
                }
            };

            esvc.RunEligibility(husbandCase);

            fmla = husbandCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
            Assert.IsNotNull(fmla, "FMLA Policy not found");

            Assert.IsNotNull(fmla);

            minsUsed = fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.MinutesUsed);
            daysUsed = Math.Round(fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.PercentOfDay), 4);
            weeksUsed = Math.Round(fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.DaysUsage(EntitlementType.WorkWeeks)), 4);

            Assert.IsTrue(weeksUsed == 2.2);
            Assert.IsTrue(daysUsed == 11);
            Assert.IsTrue(minsUsed == 5280);          // 3 weeks, 5 days a week, 8 hours a day, 60 minutes an hour

            deleteTestCases(wife);
            deleteTestCases(husband);

        }

        [TestMethod]
        public void TestCalcs()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            CaseService cs = new CaseService();

            Case newCase = cs.CreateCase(CaseStatus.Open, empId, new DateTime(2014, 3, 2, 0, 0, 0, DateTimeKind.Utc), new DateTime(2014, 3, 15, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(newCase);

            // now, they are eligible and they had two weeks of regular leave 480 minutes a day for 10 days
            Assert.AreEqual(1, newCase.Segments.Count);
            Assert.IsTrue(newCase.Segments[0].AppliedPolicies.Count >= 1);
            var fmla = newCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "FMLA");
            Assert.IsNotNull(fmla, "FMLA Policy not on case");
            Assert.AreEqual(14, fmla.Usage.Count);

            double minsUsed = fmla.Usage.Where(u => u.UsesTime).Sum(u => u.MinutesUsed);
            double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.PercentOfDay), 4);
            double weeksUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.WorkWeeks)), 4);

            Assert.IsTrue(minsUsed == 4800);
            Assert.IsTrue(daysUsed == 10);
            Assert.IsTrue(weeksUsed == 2);

            // now let's mess with the schedule and use the rotating schedule (3/3 is in the second week of the pattern)
            //  Sun – 0 hrs, Mon – 0 hrs, Tues – 0 hrs, Weds – 12 hrs, Thurs – 12 hrs, Fri – 12 hrs, Sat – 0 hrs, 
            //  Sun – 0 hrs, Mon – 12 hrs, Tues – 12 hrs, Weds – 12 hrs, Thurs – 0 hrs, Fri – 0 hrs, Sat – 0 hrs
            // two weeks = 72 hours (72 * 60 = 4320)
            // testing gotcha - there employee is copied into the case, change it there
            newCase.Employee.WorkSchedules[0].StartDate = TestWorkSchedules.RotatingSchedule().Min(s => s.SampleDate);
            newCase.Employee.WorkSchedules[0].Times = TestWorkSchedules.RotatingSchedule();
            newCase.Employee.WorkSchedules[0].ScheduleType = ScheduleType.Rotating;
            newCase.Employee.Save();

            cs.GetProjectedUsage(newCase);
            fmla = newCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "FMLA");
            Assert.IsNotNull(fmla, "FMLA Policy not on case");

            newCase.Segments.Should().HaveCount(1);
            newCase.Segments[0].AppliedPolicies.Count.Should().BeGreaterOrEqualTo(1);
            fmla.Usage.Where(c => c.MinutesUsed > 0).Should().HaveCount(6, "FMLA usage where minutes used is > 0 should be 6");

            minsUsed = fmla.Usage.Where(u => u.UsesTime).Sum(u => u.MinutesUsed);
            daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.PercentOfDay), 4);
            weeksUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.WorkWeeks)), 4);

            minsUsed.Should().Be(4320);
            daysUsed.Should().Be(6);
            weeksUsed.Should().Be(2);// but still two weeks           

            // change the case type to a reduced schedule, and copy the current schedule but make them half days
            Schedule reduced = new Schedule()
            {
                StartDate = newCase.Employee.WorkSchedules[0].StartDate,
                EndDate = new DateTime(2014, 3, 15, 0, 0, 0, DateTimeKind.Utc),
                ScheduleType = ScheduleType.Rotating
            };
            foreach (Time t in newCase.Employee.WorkSchedules[0].Times)
                reduced.Times.Add(new Time()
                {
                    SampleDate = t.SampleDate,
                    TotalMinutes = t.TotalMinutes / 2
                });

            newCase.Segments[0].LeaveSchedule = new List<Schedule> { reduced };

            newCase.Segments[0].Type = CaseType.Reduced;

            cs.GetProjectedUsage(newCase);
            fmla = newCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "FMLA");
            Assert.IsNotNull(fmla, "FMLA Policy not on case");

            newCase.Segments.Should().HaveCount(1);
            newCase.Segments[0].AppliedPolicies.Count.Should().BeGreaterOrEqualTo(1);
            fmla.Usage.Where(c => c.MinutesUsed > 0).Should().HaveCount(6);

            minsUsed = fmla.Usage.Where(u => u.UsesTime).Sum(u => u.MinutesUsed);
            daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.PercentOfDay), 4);
            weeksUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.WorkWeeks)), 4);

            minsUsed.Should().Be(2160);
            daysUsed.Should().Be(3);
            weeksUsed.Should().Be(1);

            // for our next trick let's do an intermittent case with two half days
            // schedule is not used for intermittent, load up the absence collection
            newCase.Segments[0].LeaveSchedule = new List<Schedule>();

            //  third week of the pattern: Sun – 0 hrs, Mon – 12 hrs, Tues – 12 hrs, Weds – 12 hrs, Thurs – 0 hrs, Fri – 0 hrs, Sat – 0 hrs
            // monday - full day
            newCase.Segments[0].Absences = new List<Time>();
            List<Time> ta = newCase.Segments[0].Absences; // shortcut

            // remember abscenses are backwards. The total number of minutes are the minutes worked
            // that day (if not then we will have to change code accordingly) (which we just did so the above
            // statement is no longer true - feel free to delete this comment)
            ta.Add(new Time()
            {
                TotalMinutes = 720,
                SampleDate = new DateTime(2014, 03, 10, 0, 0, 0, DateTimeKind.Utc)
            });
            // wednesday half day
            ta.Add(new Time()
            {
                TotalMinutes = 360,
                SampleDate = new DateTime(2014, 03, 12, 0, 0, 0, DateTimeKind.Utc)
            });


            // now, put back the other rotating schedule case (check it to make sure we did that correctly)
            // and then get the spouse, set up a the exact same case for them
            newCase.Segments[0].Type = CaseType.Consecutive;
            newCase.Segments[0].LeaveSchedule = new List<Schedule>();
            newCase.Employee.WorkSchedules[0].Times = TestWorkSchedules.GetFullTimeList();
            newCase.Employee.Save();

            cs.GetProjectedUsage(newCase);
            fmla = newCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "FMLA");
            Assert.IsNotNull(fmla, "FMLA Policy not on case");

            minsUsed = fmla.Usage.Where(u => u.UsesTime).Sum(u => u.MinutesUsed);
            daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.PercentOfDay), 4);
            weeksUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.WorkWeeks)), 4);

            minsUsed.Should().Be(4800);
            daysUsed.Should().Be(10);
            weeksUsed.Should().Be(2);

            // now load the spouse
            string spouseEmpId = "000000000000000000000004";
            //string spouseCaseCode = "EHC";

            Employee spouseEmp = Employee.GetById(spouseEmpId);

            // case for the same dates
            Case spouseCase = cs.CreateCase(CaseStatus.Open, spouseEmpId, new DateTime(2014, 3, 2, 0, 0, 0, DateTimeKind.Utc), new DateTime(2014, 3, 15, 0, 0, 0, DateTimeKind.Utc), CaseType.Consecutive, reason.Code);

            LeaveOfAbsence spouseLoa = esvc.RunEligibility(spouseCase);

            // this leave isn't combined for spouses so the results should be the same
            minsUsed = spouseCase.Segments[0].AppliedPolicies.First(p => p.Policy.Code == "FMLA").Usage.Where(u => u.UsesTime).Sum(u => u.MinutesUsed);
            daysUsed = Math.Round(spouseCase.Segments[0].AppliedPolicies.First(p => p.Policy.Code == "FMLA").Usage.Where(u => u.UsesTime).Sum(u => u.PercentOfDay), 4);
            weeksUsed = Math.Round(spouseCase.Segments[0].AppliedPolicies.First(p => p.Policy.Code == "FMLA").Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.WorkWeeks)), 4);

            minsUsed.Should().Be(4800);
            daysUsed.Should().Be(10);
            weeksUsed.Should().Be(2);

            // save the employee case (otherwise the spouse case won't find it)
            newCase.Description = "a very tiny test case";
            newCase.Save();

            // and since I don't know how to create overlapping cases, I'll just force this one to overlap
            spouseCase.Segments[0].AppliedPolicies.First(p => p.Policy.Code == "FMLA").PolicyReason.CombinedForSpouses = true;

            // make sure we are set to a full time schedule
            spouseCase.Employee.WorkSchedules[0] = TestWorkSchedules.GetFullTimeSchedule();
            spouseCase.Employee.WorkSchedules[0].EndDate = null;
            spouseCase.Employee.Save();

            cs.GetProjectedUsage(spouseCase);

            // for now it should be the same. We will let them use the same time on the same policy
            // if that changes the tests below can be commented out and the LoadSpousTotals in CaseService needs to be changed
            minsUsed = spouseCase.Segments[0].AppliedPolicies.First(p => p.Policy.Code == "FMLA").Usage.Where(u => u.UsesTime).Sum(u => u.MinutesUsed);
            daysUsed = Math.Round(spouseCase.Segments[0].AppliedPolicies.First(p => p.Policy.Code == "FMLA").Usage.Where(u => u.UsesTime).Sum(u => u.PercentOfDay), 4);
            weeksUsed = Math.Round(spouseCase.Segments[0].AppliedPolicies.First(p => p.Policy.Code == "FMLA").Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.WorkWeeks)), 4);

            minsUsed.Should().Be(4800);
            daysUsed.Should().Be(10);
            weeksUsed.Should().Be(2);

            // and clean up after ourselves
            string idTest = newCase.Id;

            Case caseDateTest = Case.GetById(idTest);

            newCase.Delete();
        }

        [TestMethod]
        public void CaseClosed()
        {
            // now create a case for an employee and create the work items
            string empId = "000000000000000000000003";
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            Employee testEmp = Employee.GetById(empId);
            List<Case> empCases = Case.AsQueryable().Where(c => c.Employee.Id == empId).ToList();

            // if a previous run crashed, get rid of the test case
            foreach (Case c in empCases)
                if (c.Description == "case closed test")
                    c.Delete();

            // create a test case
            CaseService cs = new CaseService();

            Case newCase = cs.CreateCase(CaseStatus.Open, empId, new DateTime(2014, 1, 5, 0, 0, 0, DateTimeKind.Utc), new DateTime(2014, 2, 1, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(newCase);

            newCase.Segments[0].AppliedPolicies[0].PolicyReason.PeriodType = PeriodType.RollingBack;        // for some reason this one already is a rollback, but fix up the number of days
            newCase.Segments[0].AppliedPolicies[0].PolicyReason.Period = 100;                               // because right now the set makes it 12
            newCase.Segments[0].AppliedPolicies[0].Usage.ForEach(u => u.Determination = AdjudicationStatus.Approved);

            newCase.Description = "case closed test";
            newCase.Save();

            cs.CaseClosed(newCase, new DateTime(2014, 1, 25, 0, 0, 0, DateTimeKind.Utc), CaseClosureReason.ReturnToWork);

            var ps = cs.GetEmployeePolicySummary(newCase.Employee); // cs.GetCasePolicySummary(newCase.Id);
            double weeksUsed = ps.Sum(wu => wu.TimeUsed);

            /// They actually did use three weeks ? 
            Assert.AreEqual(3, weeksUsed);                    // 1 day not counted for MLK day

            newCase.Delete();

        }

        [TestMethod]
        public void EliminationPeriod()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            Employee testEmp = Employee.GetById(empId);
            List<Case> empCases = Case.AsQueryable().Where(c => c.Employee.Id == empId).ToList();

            // if a previous run crashed, get rid of the test case
            foreach (Case c in empCases)
                if (c.Description == "a very tiny test case" || c.Description == "another very tiny test case" || c.Description == "No more of these")
                    c.Delete();

            CaseService cs = new CaseService();

            Case newCase = cs.CreateCase(CaseStatus.Open, empId, new DateTime(2014, 3, 2, 0, 0, 0, DateTimeKind.Utc), new DateTime(2014, 3, 15, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(newCase);

            // now set the policy to have a 1 week elimination period
            var fmla = newCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "FMLA");
            PolicyAbsenceReason par = fmla.PolicyReason;
            par.EliminationType = EntitlementType.WorkWeeks;
            par.Elimination = 1;

            // reproject
            cs.GetProjectedUsage(newCase);

            // now, they are eligible and they had two weeks of regular leave 480 minutes a day for 10 days
            Assert.AreEqual(1, newCase.Segments.Count);
            Assert.IsTrue(newCase.Segments[0].AppliedPolicies.Count >= 1);
            fmla = newCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "FMLA");
            Assert.IsNotNull(fmla);
            Assert.AreEqual(14, fmla.Usage.Count);          // there is 1 row for every day

            double minsUsed = fmla.Usage.Where(u => u.UsesTime).Sum(u => u.MinutesUsed);
            double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.PercentOfDay), 4);
            double weeksUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.WorkWeeks)), 4);

            // fmla elimination period is inclusive, so we should still have all our time
            Assert.AreEqual(4800, minsUsed);
            Assert.AreEqual(10, daysUsed);
            Assert.AreEqual(2, weeksUsed);

            // some more fun, set a 3 week elimination period
            par.Elimination = 3;

            // reproject
            cs.GetProjectedUsage(newCase);
            Assert.AreEqual(1, newCase.Segments.Count);
            Assert.IsTrue(newCase.Segments[0].AppliedPolicies.Count >= 1);
            fmla = newCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "FMLA");
            Assert.AreEqual(14, fmla.Usage.Count);

            minsUsed = fmla.Usage.Where(u => u.UsesTime).Sum(u => u.MinutesUsed);
            daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.PercentOfDay), 4);
            weeksUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.WorkWeeks)), 4);

            // and because of the elimination period we should only have 0 time used
            Assert.AreEqual(0, minsUsed);
            Assert.AreEqual(0, daysUsed);
            Assert.AreEqual(0, weeksUsed);

            // sum the denied
            minsUsed = fmla.Usage.Where(u => !u.UsesTime).Sum(u => u.MinutesUsed);
            daysUsed = Math.Round(fmla.Usage.Where(u => !u.UsesTime).Sum(u => u.PercentOfDay), 4);
            weeksUsed = Math.Round(fmla.Usage.Where(u => !u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.WorkWeeks)), 4);

            // only the length of the case should be denied
            Assert.AreEqual(4800, minsUsed);
            Assert.AreEqual(10, daysUsed);
            Assert.AreEqual(2, weeksUsed);

        }

        [TestMethod]
        public void AppliedPolicySortTest()
        {
            Policy p1 = new Policy() { Name = "Grandparent A", Code = "Grandparent A", Id = "1" };
            Policy p11 = new Policy() { Name = "Parent A.1", Code = "Parent A.1", Id = "1.1", ConsecutiveTo = { "Grandparent A" } };
            Policy p12 = new Policy() { Name = "Parent A.2", Code = "Parent A.2", Id = "1.2", ConsecutiveTo = { "Grandparent A" } };
            Policy p111 = new Policy() { Name = "Child A.1.1", Code = "Child A.1.1", Id = "1.1.1", ConsecutiveTo = { "Parent A.1", "Child B.2.1" } };     // this is the poor confused child, should sort to the bottom

            Policy p2 = new Policy() { Name = "Grandparent B", Code = "Grandparent B", Id = "2" };
            Policy p21 = new Policy() { Name = "Parent B.1", Code = "Parent B.1", Id = "2.1", ConsecutiveTo = { "Grandparent B" } };
            Policy p22 = new Policy() { Name = "Parent B.2", Code = "Parent B.2", Id = "2.2", ConsecutiveTo = { "Grandparent B" } };
            Policy p221 = new Policy() { Name = "Child B.2.1", Code = "Child B.2.1", Id = "2.2.1", ConsecutiveTo = { "Parent B.2" } };

            // new create the applied policies for these
            List<AppliedPolicy> sortMe = new List<AppliedPolicy>() {
                new AppliedPolicy() { StartDate = new DateTime(2014,3,2,0,0,0, DateTimeKind.Utc), Policy = p1 },
                new AppliedPolicy() { StartDate = new DateTime(2014,3,2,0,0,0, DateTimeKind.Utc), Policy = p11 },
                new AppliedPolicy() { StartDate = new DateTime(2014,3,2,0,0,0, DateTimeKind.Utc), Policy = p12 },
                new AppliedPolicy() { StartDate = new DateTime(2014,3,2,0,0,0, DateTimeKind.Utc), Policy = p111 },
                new AppliedPolicy() { StartDate = new DateTime(2014,3,2,0,0,0, DateTimeKind.Utc), Policy = p2 },
                new AppliedPolicy() { StartDate = new DateTime(2014,3,2,0,0,0, DateTimeKind.Utc), Policy = p21 },
                new AppliedPolicy() { StartDate = new DateTime(2014,3,2,0,0,0, DateTimeKind.Utc), Policy = p22 },
                new AppliedPolicy() { StartDate = new DateTime(2014,3,2,0,0,0, DateTimeKind.Utc), Policy = p221 }
            };

            // use the note field to make this easy to see
            sortMe.ForEach(sm => sm.ManuallyAddedNote = sm.Policy.Code);

            CaseService cs = new CaseService();
            List<AppliedPolicy> imSorted = cs.AppliedPolicySort(sortMe);

            Assert.IsTrue(imSorted[7].ManuallyAddedNote == "Child A.1.1");

            // now test the suplemental to sort, we should get them sorted by date order and then children
            //Policy s1 = new Policy() { Name = "Parent S1", Code = "Parent S1", Id = "1" };
            //Policy s2 = new Policy() { Name = "Child S1.1", Code = "Child S1.1", Id = "2", SupplementalTo = {"Parent S1"} };

            //List<AppliedPolicy> sortSup = new List<AppliedPolicy>() {
            //    new AppliedPolicy() { StartDate = new DateTime(2014,3,2,0,0,0, DateTimeKind.Utc), Policy = s2 },
            //    new AppliedPolicy() { StartDate = new DateTime(2014,3,2,0,0,0, DateTimeKind.Utc), Policy = s1 }
            //};

            //sortSup.ForEach(sm => sm.ManuallyAddedNote = sm.Policy.Code);
            //List<AppliedPolicy> supsSorted = cs.AppliedPolicySort(sortSup);

            //Assert.AreEqual(supsSorted.Count, 2);         
            //Assert.IsTrue(supsSorted[1].ManuallyAddedNote == "Child S1.1");

        }                                   // public void ConsecutiveTooPolicySort()

        [TestMethod]
        public void ChangeCaseTest()
        {
            // first bit stolen from create case
            using (CaseService svc = new CaseService())
            {
                var emp = Employee.AsQueryable().Where(e => e.EmployeeNumber == "demo_001").FirstOrDefault();
                Assert.IsNotNull(emp);
                var reasons = svc.GetAbsenceReasons(emp.Id);
                Assert.IsNotNull(reasons);
                Assert.IsTrue(reasons.Any());
                var r = reasons.First();

                Case.AsQueryable().Where(c => c.Employee.Id == emp.Id && c.Reason.Code == r.Code).ForEach(a => a.Delete());

                // start with a three week case
                var newCase = svc.CreateCase(CaseStatus.Open, emp.Id, new DateTime(2014, 2, 2, 0, 0, 0, DateTimeKind.Utc), new DateTime(2014, 2, 22, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, r.Code, description: "CaseServiceTest.CreateCase");
                Assert.IsNotNull(newCase);
                newCase = svc.UpdateCase(newCase);
                Assert.IsNotNull(newCase.Id);

                // first add one that comes a week after this on
                svc.ChangeCase(newCase, new DateTime(2014, 3, 2, 0, 0, 0, DateTimeKind.Utc), new DateTime(2014, 3, 8, 0, 0, 0, DateTimeKind.Utc), true, true, CaseType.Consecutive, false, false);

                // Now, since we said change the start and end date, we should only have a single segment
                Assert.AreEqual(1, newCase.Segments.Count);
                Assert.AreEqual(new DateTime(2014, 3, 2, 0, 0, 0, DateTimeKind.Utc), newCase.StartDate);
                Assert.AreEqual(new DateTime(2014, 3, 8, 0, 0, 0, DateTimeKind.Utc), newCase.EndDate);

                // now make it start a week earlier
                svc.ChangeCase(newCase, new DateTime(2014, 1, 26, 0, 0, 0, DateTimeKind.Utc), new DateTime(2014, 2, 22, 0, 0, 0, DateTimeKind.Utc), true, true, CaseType.Consecutive, false, false);
                Assert.AreEqual(1, newCase.Segments.Count);
                Assert.AreEqual(new DateTime(2014, 1, 26, 0, 0, 0, DateTimeKind.Utc), newCase.StartDate);
                Assert.AreEqual(new DateTime(2014, 2, 22, 0, 0, 0, DateTimeKind.Utc), newCase.EndDate);

                // now make the second one end later
                svc.ChangeCase(newCase, new DateTime(2014, 3, 2, 0, 0, 0, DateTimeKind.Utc), new DateTime(2014, 3, 22, 0, 0, 0, DateTimeKind.Utc), true, true, CaseType.Consecutive, false, false);
                Assert.AreEqual(1, newCase.Segments.Count);
                Assert.AreEqual(new DateTime(2014, 3, 2, 0, 0, 0, DateTimeKind.Utc), newCase.StartDate);
                Assert.AreEqual(new DateTime(2014, 3, 22, 0, 0, 0, DateTimeKind.Utc), newCase.EndDate);

                // now the icky test, drop an intermittent case that overlaps the end of the first and the
                // beginning of the second. It should insert it as the second segment and then adjust the
                // other two segments accordingly
                svc.ChangeCase(newCase, new DateTime(2014, 2, 16, 0, 0, 0, DateTimeKind.Utc), new DateTime(2014, 3, 5, 0, 0, 0, DateTimeKind.Utc), true, false, CaseType.Intermittent, false, false);
                Assert.AreEqual(2, newCase.Segments.Count);
                Assert.AreEqual(new DateTime(2014, 2, 16, 0, 0, 0, DateTimeKind.Utc), newCase.StartDate);
                Assert.AreEqual(new DateTime(2014, 3, 22, 0, 0, 0, DateTimeKind.Utc), newCase.EndDate);
            }
        } // public void ChangeCase()

        [TestMethod]
        public void ChangeCaseOneDayTest()
        {
            // first bit stolen from create case
            using (CaseService svc = new CaseService())
            {
                var emp = Employee.AsQueryable().Where(e => e.EmployeeNumber == "demo_001").FirstOrDefault();
                Assert.IsNotNull(emp);
                var reasons = svc.GetAbsenceReasons(emp.Id);
                Assert.IsNotNull(reasons);
                Assert.IsTrue(reasons.Any());
                var r = reasons.First();

                Case.AsQueryable().Where(c => c.Employee.Id == emp.Id && c.Reason.Code == r.Code).ForEach(a => a.Delete());

                // start with a three week case
                var newCase = svc.CreateCase(CaseStatus.Open, emp.Id, new DateTime(2014, 2, 2, 0, 0, 0, DateTimeKind.Utc), new DateTime(2014, 2, 22, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, r.Code, description: "CaseServiceTest.CreateCase");
                Assert.IsNotNull(newCase);
                newCase = svc.UpdateCase(newCase);
                Assert.IsNotNull(newCase.Id);

                newCase = svc.ChangeCase(newCase, newCase.StartDate, newCase.StartDate, false, false, CaseType.Intermittent, false, false);
                Assert.AreEqual(2, newCase.Segments.Count);

                var orderedSegments = newCase.Segments.OrderBy(s => s.StartDate);
                var firstSegment = orderedSegments.First();
                Assert.AreEqual(newCase.StartDate, firstSegment.StartDate);
                Assert.AreEqual(newCase.StartDate, firstSegment.EndDate);

                var lastSegment = orderedSegments.Last();
                Assert.AreEqual(newCase.StartDate.AddDays(1), lastSegment.StartDate);
                Assert.AreEqual(newCase.EndDate, lastSegment.EndDate);
            }
        } // public void ChangeCase()

        [TestMethod]
        public void ChangeCaseEntireSegment()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            CaseService cs = new CaseService();

            DateTime startDate = new DateTime(2014, 3, 2, 0, 0, 0, DateTimeKind.Utc);
            DateTime endDate = new DateTime(2014, 3, 22, 0, 0, 0, DateTimeKind.Utc);

            Case firstCase = cs.CreateCase(CaseStatus.Open, empId, startDate, endDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(firstCase);

            // we don't care anything about this stupid case, just try to change the type to intermittent 
            // same dates and it should work
            cs.ChangeCase(firstCase, startDate, endDate, false, false, CaseType.Intermittent, false, false);

            // if that didn't throw an exception then yippee, let's keep going
            // approve it and then create a time off request
            // approve the leave for time off
            firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>())
                    .ForEach(ap => cs.ApplyDetermination(firstCase, ap.Policy.Code, startDate, endDate, AdjudicationStatus.Approved));

            AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == "FMLA");
            Assert.IsNotNull(firstCase);

            List<IntermittentTimeRequest> itr = new List<IntermittentTimeRequest>() {
                        new IntermittentTimeRequest()
                        {
                            RequestDate = new DateTime(2014, 3, 10, 0, 0, 0, DateTimeKind.Utc),
                            TotalMinutes = 480,
                            Detail = new List<IntermittentTimeRequestDetail>() {
                                { new IntermittentTimeRequestDetail()
                                    { Approved=480, PolicyCode = fmla.Policy.Code}
                                }},
                            Notes = "make the change case go boom!"
                        }
            };

            // add the approved day
            cs.CreateOrModifyTimeOffRequest(firstCase, itr);

            // not really necessary but, here goes
            cs.GetProjectedUsage(firstCase);

            // now it should throw an exception when we try to change the type
            Exception except = null;
            try
            {
                cs.ChangeCase(firstCase, startDate, endDate, false, false, CaseType.Consecutive, false, false);
            }
            catch (Exception e)
            {
                except = e;
            }

            // case change should throw an exception when we try to change it back
            Assert.IsNotNull(except);
        }

        [TestMethod]
        public void CaseEventTest()
        {
            // this first bit is stolen from test calcs so it better work (well, I made the time off longer)

            string empId = "000000000000000000000003";
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            CaseService cs = new CaseService();

            Case newCase = cs.CreateCase(CaseStatus.Open, empId, new DateTime(2014, 3, 2, 0, 0, 0, DateTimeKind.Utc), new DateTime(2014, 3, 22, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(newCase);

            // now, they are eligible and they had 3 weeks of regular leave 480 minutes a day for 15 days
            Assert.AreEqual(1, newCase.Segments.Count);
            Assert.IsTrue(newCase.Segments[0].AppliedPolicies.Count >= 1);
            var fmla = newCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "FMLA");
            Assert.IsNotNull(fmla);
            Assert.AreEqual(21, fmla.Usage.Count);

            double minsUsed = fmla.Usage.Where(u => u.UsesTime).Sum(u => u.MinutesUsed);
            double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.PercentOfDay), 4);
            double weeksUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.WorkWeeks)), 4);

            Assert.AreEqual(7200, minsUsed);
            Assert.AreEqual(15, daysUsed);
            Assert.AreEqual(3, weeksUsed);

            // tweak the policy to listen for something
            fmla.PolicyReason.PolicyEvents.Add(new PolicyEvent() { DateType = EventDateType.EndPolicyBasedOnEventDate, EventType = CaseEventType.HospitalReleaseDate });

            // now recalc usage and nothing should have changed
            cs.RunCalcs(newCase);
            fmla = newCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "FMLA");
            Assert.IsNotNull(fmla);

            // now, they are eligible and they had 3 weeks of regular leave 480 minutes a day for 15 days
            Assert.AreEqual(1, newCase.Segments.Count);
            Assert.IsTrue(newCase.Segments[0].AppliedPolicies.Count >= 1);
            Assert.AreEqual(21, fmla.Usage.Count);

            minsUsed = fmla.Usage.Where(u => u.UsesTime).Sum(u => u.MinutesUsed);
            daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.PercentOfDay), 4);
            weeksUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.WorkWeeks)), 4);

            Assert.AreEqual(7200, minsUsed);
            Assert.AreEqual(15, daysUsed);
            Assert.AreEqual(3, weeksUsed);

            // now add the event to the case
            newCase.SetCaseEvent(CaseEventType.HospitalReleaseDate, new DateTime(2014, 3, 15, 0, 0, 0, DateTimeKind.Utc));

            // and this time the case should only get two weeks of time
            cs.RunCalcs(newCase);
            fmla = newCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "FMLA");
            Assert.IsNotNull(fmla);

            minsUsed = fmla.Usage.Where(u => u.UsesTime).Sum(u => u.MinutesUsed);
            daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.PercentOfDay), 4);
            weeksUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.WorkWeeks)), 4);

            Assert.AreEqual(4800, minsUsed);
            Assert.AreEqual(10, daysUsed);
            Assert.AreEqual(2, weeksUsed);
        }//end: CaseEventTest()

        [TestMethod]
        public void CalcsCalendarTypes()
        {
            // once again, ripped from the TestCalcs, go through the first few steps and make sure everything
            // works right before we start mucking around with the policy types
            string empId = "000000000000000000000003";
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            CaseService cs = new CaseService();

            Case newCase = cs.CreateCase(CaseStatus.Open, empId, new DateTime(2014, 3, 2, 0, 0, 0, DateTimeKind.Utc), new DateTime(2014, 3, 15, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(newCase);

            // now, they are eligible and they had two weeks of regular leave 480 minutes a day for 10 days
            Assert.AreEqual(1, newCase.Segments.Count);
            Assert.IsTrue(newCase.Segments[0].AppliedPolicies.Count >= 1);
            var fmla = newCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "FMLA");
            Assert.IsNotNull(fmla);
            Assert.AreEqual(14, fmla.Usage.Count);

            double minsUsed = fmla.Usage.Where(u => u.UsesTime).Sum(u => u.MinutesUsed);
            double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.PercentOfDay), 4);
            double weeksUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.WorkWeeks)), 4);

            Assert.AreEqual(4800, minsUsed);
            Assert.AreEqual(10, daysUsed);
            Assert.AreEqual(2, weeksUsed);

            // now switch to a calendar type
            fmla.PolicyReason.EntitlementType = EntitlementType.CalendarDays;

            // (the default was 12, it'll never get to 14) set it to 30 days
            fmla.PolicyReason.Entitlement = 30;

            cs.GetProjectedUsage(newCase);
            fmla = newCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "FMLA");
            Assert.IsNotNull(fmla);

            //minsUsed = fmla.Usage.Where(u => u.UsesTime).Sum(u => u.MinutesUsed);
            //daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.PercentOfDay), 4);
            //weeksUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.WorkWeeks)), 4);
            daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);

            Assert.AreEqual(14, daysUsed);

            //fmla.PolicyReason.EntitlementType = EntitlementType.CalendarMonths;
            //cs.GetProjectedUsage(newCase);

            // not really days this time but a percentage of the month
            //CalendarMonths in AT is 1=30days so we need to change it 
            daysUsed = Math.Round(fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.DaysUsage(EntitlementType.CalendarMonths)), 4);
            Assert.AreEqual(Math.Round(14d / 30d, 4), Math.Round(daysUsed, 4));

            // only allow a week
            fmla.PolicyReason.Entitlement = 7;
            cs.GetProjectedUsage(newCase);
            fmla = newCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "FMLA");
            Assert.IsNotNull(fmla);
            daysUsed = Math.Round(fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
            Assert.AreEqual(7d, Math.Round(daysUsed, 4));
        }//end: TestCalcsCalendarTypes()


        [TestMethod]
        public void CasePerUseCapTest()
        {

            // this first bit is stolen from test calcs so it better work (well, I made the time off longer)
            string empId = "000000000000000000000003";
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            CaseService cs = new CaseService();

            Case newCase = cs.CreateCase(CaseStatus.Open, empId, new DateTime(2014, 3, 2, 0, 0, 0, DateTimeKind.Utc), new DateTime(2014, 3, 22, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(newCase);

            // now, they are eligible and they had 3 weeks of regular leave 480 minutes a day for 15 days
            Assert.AreEqual(1, newCase.Segments.Count);
            Assert.IsTrue(newCase.Segments[0].AppliedPolicies.Count >= 1);
            var fmla = newCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "FMLA");
            Assert.IsNotNull(fmla);
            Assert.AreEqual(21, fmla.Usage.Count);

            double minsUsed = fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.MinutesUsed);
            double daysUsed = Math.Round(fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.PercentOfDay), 4);
            double weeksUsed = Math.Round(fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.DaysUsage(EntitlementType.WorkWeeks)), 4);

            Assert.AreEqual(7200, minsUsed);
            Assert.AreEqual(15, daysUsed);
            Assert.AreEqual(3, weeksUsed);

            // now that we know that is still working, let's add a per use cap of one work week 
            fmla.PolicyReason.PerUseCapType = EntitlementType.WorkWeeks;
            fmla.PolicyReason.PerUseCap = 1;

            cs.GetProjectedUsage(newCase);
            fmla = newCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "FMLA");
            Assert.IsNotNull(fmla);

            minsUsed = fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.MinutesUsed);
            daysUsed = Math.Round(fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.PercentOfDay), 4);
            weeksUsed = Math.Round(fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.DaysUsage(EntitlementType.WorkWeeks)), 4);

            Assert.AreEqual(2400, minsUsed);
            Assert.AreEqual(5, daysUsed);
            Assert.AreEqual(1, weeksUsed);
        }//end: CaseEventTest()

        // So far, I've used this test to inspect the data manually. Need everything verified before I 
        // continue on the test
        [TestMethod]
        public void CaseItermittent()
        {
            // this test is now entirely wrong... needs to be re-worked for the new intermittent status column

            // this first bit is stolen from test calcs so it better work (well, I made the time off longer)
            string empId = "000000000000000000000186";
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            DateTime startDate = new DateTime(2015, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime endDate = new DateTime(2015, 1, 31, 0, 0, 0, 0, DateTimeKind.Utc);

            CaseService cs = new CaseService();
            Case newCase = cs.CreateCase(CaseStatus.Open, empId, startDate, endDate, AbsenceSoft.Data.Enums.CaseType.Intermittent, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(newCase);

            // check an intermittent status and make sure that it is intermittent pending
            Assert.IsTrue(newCase.Segments.Count == 1);
            AppliedPolicy fmla = newCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "FMLA");

            Assert.AreEqual(fmla.Usage.Count, 31);

            // set the determination on all the policies to allow an intermittent request
            //newCase.Segments[0].AppliedPolicies.ForEach(ap => cs.ApplyDetermination(newCase, ap.Policy.Code, startDate, endDate, AdjudicationStatus.Approved));
            cs.ApplyDetermination(newCase, fmla.Policy.Code, startDate, endDate, AdjudicationStatus.Approved);

            //cs.ApplyDetermination(newCase, newCase.Segments[0].AppliedPolicies[0].Policy.Code, startDate, endDate, AdjudicationStatus.IntermittentAllowed);
            //cs.ApplyDetermination(newCase, newCase.Segments[0].AppliedPolicies[1].Policy.Code, startDate, endDate, AdjudicationStatus.IntermittentAllowed);

            // make sure it got set
            fmla = newCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "FMLA");

            // spot check one
            Assert.IsTrue(fmla.Usage[0].Determination == AdjudicationStatus.Pending);
            Assert.IsTrue(fmla.Usage[0].IntermittentDetermination == IntermittentStatus.Allowed);

            // and check the summary
            CaseSummaryPolicy cspFMLA = newCase.Summary.Policies.FirstOrDefault(f => f.PolicyCode == "FMLA");
            Assert.AreEqual(cspFMLA.Detail.Count, 1);
            Assert.AreEqual(cspFMLA.Detail[0].StartDate, startDate);
            Assert.AreEqual(cspFMLA.Detail[0].EndDate, endDate);

            Certification cert = new Certification()
            {
                Duration = 8,
                DurationType = Unit.Hours,
                Frequency = 1,
                FrequencyType = Unit.Weeks,
                FrequencyUnitType = DayUnitType.Workday,
                StartDate = startDate,
                EndDate = endDate
            };

            cs.CreateOrModifyCertification(newCase, cert, true);

            // check the status on a usage object
            fmla = newCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "FMLA");
            Assert.IsTrue(fmla.Usage[0].Determination == AdjudicationStatus.Pending);
            Assert.IsTrue(fmla.Usage[0].IntermittentDetermination == IntermittentStatus.Allowed);

            // create a time off request for the 11th
            List<IntermittentTimeRequest> itr = new List<IntermittentTimeRequest>(1) {
                        new IntermittentTimeRequest()
                        {
                            RequestDate = new DateTime(2015, 1, 20, 0, 0, 0, DateTimeKind.Utc),
                            TotalMinutes = 480,
                            Detail = newCase.Segments[0].AppliedPolicies.Select(p => new IntermittentTimeRequestDetail()
                            {
                                Approved = 480,
                                PolicyCode = p.Policy.Code
                            }).ToList(),
                            Notes = "my notes"
                        }
             };

            cs.CreateOrModifyTimeOffRequest(newCase, itr);

            fmla = newCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == "FMLA");
            Assert.AreEqual(fmla.Usage.Count, 31);

            cspFMLA = newCase.Summary.Policies.FirstOrDefault(f => f.PolicyCode == "FMLA");

            Assert.AreEqual(cspFMLA.Detail.Count, 1);
            Assert.AreEqual(cspFMLA.Detail[0].StartDate, startDate);
            Assert.AreEqual(cspFMLA.Detail[0].EndDate, endDate);
        }

        [TestMethod]
        public void CaseCertificationChecks2()
        {
            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == "EHC" && r.EmployerId == null).First();
            DateTime startDate = new DateTime(2015, 4, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime endDate = new DateTime(2015, 4, 30, 0, 0, 0, DateTimeKind.Utc);
            Employee emp = Employee.GetById("000000000000000000000136");
            CaseService cs = new CaseService();
            Case theCase = cs.CreateCase(CaseStatus.Open, emp.Id, startDate, endDate, CaseType.Intermittent, reason.Code, null, "Test Certification Check", null);
            EligibilityService esvc = new EligibilityService();
            theCase = esvc.RunEligibility(theCase).Case;
            Certification cert = new Certification()
            {
                Occurances = 2,
                Duration = 8,
                DurationType = Unit.Hours,
                Frequency = 1,
                FrequencyType = Unit.Weeks,
                FrequencyUnitType = DayUnitType.Workday,
                StartDate = startDate,
                EndDate = endDate
            };
            theCase.Certifications.Add(cert);
            cs.ApplyDetermination(theCase, null, startDate, endDate, AdjudicationStatus.Approved);

            CertificationCheckResult crt;

            cs.CreateOrModifyTimeOffRequest(theCase, new List<IntermittentTimeRequest>()
            {
                new IntermittentTimeRequest() {
                    TotalMinutes = 480,
                    RequestDate = new DateTime(2015, 4, 2, 0, 0, 0, DateTimeKind.Utc),
                    Detail = theCase.Segments[0].AppliedPolicies.Select(p => new IntermittentTimeRequestDetail() { Approved = 480, PolicyCode = p.Policy.Code }).ToList()
                },
                new IntermittentTimeRequest() {
                    TotalMinutes = 240,
                    RequestDate = new DateTime(2015, 4, 6, 0, 0, 0, DateTimeKind.Utc),
                    Detail = theCase.Segments[0].AppliedPolicies.Select(p => new IntermittentTimeRequestDetail() { Approved = 240, PolicyCode = p.Policy.Code }).ToList()
                }
            });

            crt = cs.CheckIntermittentTimeRequest(theCase, new IntermittentTimeRequest()
            {
                TotalMinutes = 480,
                RequestDate = new DateTime(2015, 4, 7, 0, 0, 0, DateTimeKind.Utc),
                Detail = theCase.Segments[0].AppliedPolicies.Select(p => new IntermittentTimeRequestDetail() { Approved = 480, PolicyCode = p.Policy.Code }).ToList()
            });

            Assert.IsTrue(crt.Pass, "Certification should have passed");

            cs.CreateOrModifyTimeOffRequest(theCase, new List<IntermittentTimeRequest>()
            {
                new IntermittentTimeRequest() {
                    TotalMinutes = 480,
                    RequestDate = new DateTime(2015, 4, 7, 0, 0, 0, DateTimeKind.Utc),
                    Detail = theCase.Segments[0].AppliedPolicies.Select(p => new IntermittentTimeRequestDetail() { Approved = 480, PolicyCode = p.Policy.Code }).ToList()
                }
            });

            crt = cs.CheckIntermittentTimeRequest(theCase, new IntermittentTimeRequest()
            {
                TotalMinutes = 480,
                RequestDate = new DateTime(2015, 4, 8, 0, 0, 0, DateTimeKind.Utc),
                Detail = theCase.Segments[0].AppliedPolicies.Select(p => new IntermittentTimeRequestDetail() { Approved = 480, PolicyCode = p.Policy.Code }).ToList()
            });

            Assert.IsFalse(crt.Pass, "Certification should have failed");

            cs.CreateOrModifyTimeOffRequest(theCase, new List<IntermittentTimeRequest>()
            {
                new IntermittentTimeRequest() {
                    TotalMinutes = 480,
                    RequestDate = new DateTime(2015, 4, 8, 0, 0, 0, DateTimeKind.Utc),
                    Detail = theCase.Segments[0].AppliedPolicies.Select(p => new IntermittentTimeRequestDetail() { Approved = 480, PolicyCode = p.Policy.Code }).ToList()
                }
            });

            crt = cs.CheckIntermittentTimeRequest(theCase, new IntermittentTimeRequest()
            {
                TotalMinutes = 480,
                RequestDate = new DateTime(2015, 4, 13, 0, 0, 0, DateTimeKind.Utc),
                Detail = theCase.Segments[0].AppliedPolicies.Select(p => new IntermittentTimeRequestDetail() { Approved = 480, PolicyCode = p.Policy.Code }).ToList()
            });

            Assert.IsTrue(crt.Pass, "Certification should have passed");

            cs.CreateOrModifyTimeOffRequest(theCase, new List<IntermittentTimeRequest>()
            {
                new IntermittentTimeRequest() {
                    TotalMinutes = 480,
                    RequestDate = new DateTime(2015, 4, 13, 0, 0, 0, DateTimeKind.Utc),
                    Detail = theCase.Segments[0].AppliedPolicies.Select(p => new IntermittentTimeRequestDetail() { Approved = 480, PolicyCode = p.Policy.Code }).ToList()
                }
            });

            crt = cs.CheckIntermittentTimeRequest(theCase, new IntermittentTimeRequest()
            {
                TotalMinutes = 480,
                RequestDate = new DateTime(2015, 4, 15, 0, 0, 0, DateTimeKind.Utc),
                Detail = theCase.Segments[0].AppliedPolicies.Select(p => new IntermittentTimeRequestDetail() { Approved = 480, PolicyCode = p.Policy.Code }).ToList()
            });

            Assert.IsTrue(crt.Pass, "Certification should have passed");

            cs.CreateOrModifyTimeOffRequest(theCase, new List<IntermittentTimeRequest>()
            {
                new IntermittentTimeRequest() {
                    TotalMinutes = 480,
                    RequestDate = new DateTime(2015, 4, 15, 0, 0, 0, DateTimeKind.Utc),
                    Detail = theCase.Segments[0].AppliedPolicies.Select(p => new IntermittentTimeRequestDetail() { Approved = 480, PolicyCode = p.Policy.Code }).ToList()
                }
            });

            crt = cs.CheckIntermittentTimeRequest(theCase, new IntermittentTimeRequest()
            {
                TotalMinutes = 480,
                RequestDate = new DateTime(2015, 4, 17, 0, 0, 0, DateTimeKind.Utc),
                Detail = theCase.Segments[0].AppliedPolicies.Select(p => new IntermittentTimeRequestDetail() { Approved = 480, PolicyCode = p.Policy.Code }).ToList()
            });

            Assert.IsFalse(crt.Pass, "Certification should have failed");
        }


        [TestMethod]
        public void CaseCertificationChecks()
        {
            // this first bit is stolen from test calcs so it better work (well, I made the time off longer)
            string empId = "000000000000000000000003";
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            CaseService cs = new CaseService();

            // sunday
            DateTime startDate = new DateTime(2014, 3, 2, 0, 0, 0, DateTimeKind.Utc);

            // saturday 3 weeks later
            DateTime endDate = new DateTime(2014, 3, 29, 0, 0, 0, DateTimeKind.Utc);

            Case newCase = cs.CreateCase(CaseStatus.Open, empId, startDate, endDate, AbsenceSoft.Data.Enums.CaseType.Intermittent, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(newCase);

            // now that we know everthing is still working correctly, let's create a certification
            Certification newCert = new Certification()
            {
                Occurances = 3,
                Duration = 2,
                DurationType = Unit.Days,
                Frequency = 1,
                FrequencyType = Unit.Months,
                FrequencyUnitType = DayUnitType.Workday,
                StartDate = newCase.StartDate,
                EndDate = newCase.EndDate.Value
            };

            newCase.Certifications.Add(newCert);

            // approve the case for time off
            cs.ApplyDetermination(newCase, null, startDate, endDate, AdjudicationStatus.Approved);

            List<IntermittentTimeRequest> itr = new List<IntermittentTimeRequest>()
            {
                new IntermittentTimeRequest() {
                    TotalMinutes = 480,
                    RequestDate = new DateTime(2014,3,3,0,0,0, DateTimeKind.Utc),
                    Detail = new List<IntermittentTimeRequestDetail>()
                    {
                        new IntermittentTimeRequestDetail() { Approved = 480, PolicyCode = newCase.Segments[0].AppliedPolicies[0].Policy.Code}
                    }
                },
                new IntermittentTimeRequest() {
                    TotalMinutes = 480,
                    RequestDate = new DateTime(2014,3,4,0,0,0, DateTimeKind.Utc),
                    Detail = new List<IntermittentTimeRequestDetail>()
                    {
                        new IntermittentTimeRequestDetail() { Approved = 480, PolicyCode = newCase.Segments[0].AppliedPolicies[0].Policy.Code}
                    }
                }

            };

            cs.CreateOrModifyTimeOffRequest(newCase, itr);

            // this should pass
            CertificationCheckResult crt = cs.CheckIntermittentTimeRequest(newCase, new IntermittentTimeRequest()
            {
                TotalMinutes = 480,
                RequestDate = new DateTime(2014, 3, 6, 0, 0, 0, DateTimeKind.Utc),
                Detail = new List<IntermittentTimeRequestDetail>()
                {
                    new IntermittentTimeRequestDetail() { Approved = 480, PolicyCode = newCase.Segments[0].AppliedPolicies[0].Policy.Code}
                }
            });

            //cs.ApplyDetermination(newCase, ap.Policy.Code, new DateTime(2014,3,3,0,0,0, DateTimeKind.Utc),new DateTime(2014,3,3,0,0,0, DateTimeKind.Utc),

            List<IntermittentTimeRequest> itr2 = new List<IntermittentTimeRequest>()
            {
                new IntermittentTimeRequest() {
                    TotalMinutes = 480,
                    RequestDate = new DateTime(2014,3,10,0,0,0, DateTimeKind.Utc),
                    Detail = new List<IntermittentTimeRequestDetail>()
                    {
                        new IntermittentTimeRequestDetail() { Approved = 480, PolicyCode = newCase.Segments[0].AppliedPolicies[0].Policy.Code}
                    }
                },
                new IntermittentTimeRequest() {
                    TotalMinutes = 480,
                    RequestDate = new DateTime(2014,3,11,0,0,0, DateTimeKind.Utc),
                    Detail = new List<IntermittentTimeRequestDetail>()
                    {
                        new IntermittentTimeRequestDetail() { Approved = 480, PolicyCode = newCase.Segments[0].AppliedPolicies[0].Policy.Code}
                    }
                }

            };

            cs.CreateOrModifyTimeOffRequest(newCase, itr2);

            // this should pass too
            crt = cs.CheckIntermittentTimeRequest(newCase, new IntermittentTimeRequest()
            {
                TotalMinutes = 480,
                RequestDate = new DateTime(2014, 3, 6, 0, 0, 0, DateTimeKind.Utc),
                Detail = new List<IntermittentTimeRequestDetail>()
                {
                    new IntermittentTimeRequestDetail() { Approved = 480, PolicyCode = newCase.Segments[0].AppliedPolicies[0].Policy.Code}
                }
            });
            Assert.IsTrue(crt.Pass);

            List<IntermittentTimeRequest> itr3 = new List<IntermittentTimeRequest>()
            {
                new IntermittentTimeRequest() {
                    TotalMinutes = 480,
                    RequestDate = new DateTime(2014,3,13,0,0,0, DateTimeKind.Utc),
                    Detail = new List<IntermittentTimeRequestDetail>()
                    {
                        new IntermittentTimeRequestDetail() { Approved = 480, PolicyCode = newCase.Segments[0].AppliedPolicies[0].Policy.Code}
                    }
                },
                new IntermittentTimeRequest() {
                    TotalMinutes = 480,
                    RequestDate = new DateTime(2014,3,14,0,0,0, DateTimeKind.Utc),
                    Detail = new List<IntermittentTimeRequestDetail>()
                    {
                        new IntermittentTimeRequestDetail() { Approved = 480, PolicyCode = newCase.Segments[0].AppliedPolicies[0].Policy.Code}
                    }
                }

            };

            cs.CreateOrModifyTimeOffRequest(newCase, itr3);

            // please fail
            crt = cs.CheckIntermittentTimeRequest(newCase, new IntermittentTimeRequest()
            {
                TotalMinutes = 480,
                RequestDate = new DateTime(2014, 3, 18, 0, 0, 0, DateTimeKind.Utc),
                Detail = new List<IntermittentTimeRequestDetail>()
                {
                    new IntermittentTimeRequestDetail() { Approved = 480, PolicyCode = newCase.Segments[0].AppliedPolicies[0].Policy.Code}
                }
            });
            Assert.IsFalse(crt.Pass);
        }


        [TestMethod]
        public void CaseCertificationChecksCalendarBased()
        {
            // this first bit is stolen from test calcs so it better work (well, I made the time off longer)
            string empId = "000000000000000000000003";
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            CaseService cs = new CaseService();

            // sunday
            DateTime startDate = new DateTime(2015, 2, 1, 0, 0, 0, DateTimeKind.Utc);

            // saturday 3 weeks later
            DateTime endDate = new DateTime(2015, 2, 21, 0, 0, 0, DateTimeKind.Utc);

            Case newCase = cs.CreateCase(CaseStatus.Open, empId, startDate, endDate, AbsenceSoft.Data.Enums.CaseType.Intermittent, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(newCase);

            // this cert will allow one use per calendar week.
            // Sunday - Saturday. If they take a leave on Thursday
            // they can take a leave as soon as the folliowing Sunday
            Certification newCert = new Certification()
            {
                Occurances = 1,
                Duration = 1,
                DurationType = Unit.Days,
                Frequency = 1,
                FrequencyType = Unit.Weeks,
                FrequencyUnitType = DayUnitType.CalendarDay,
                StartDate = newCase.StartDate,
                EndDate = newCase.EndDate.Value
            };

            newCase.Certifications.Add(newCert);

            // approve the case for time off
            foreach (AppliedPolicy ap in newCase.Segments.SelectMany(s => s.AppliedPolicies))
            {
                cs.ApplyDetermination(newCase, ap.Policy.Code, startDate, endDate, AdjudicationStatus.Approved);
            }

            // request for Thurs the first week
            List<IntermittentTimeRequest> itr = new List<IntermittentTimeRequest>()
            {
                new IntermittentTimeRequest() {
                    TotalMinutes = 480,
                    RequestDate = new DateTime(2015,2,5,0,0,0, DateTimeKind.Utc),
                    Detail = new List<IntermittentTimeRequestDetail>()
                    {
                        new IntermittentTimeRequestDetail() { Approved = 480, PolicyCode = newCase.Segments[0].AppliedPolicies[0].Policy.Code}
                    }
                }
            };

            cs.CreateOrModifyTimeOffRequest(newCase, itr);

            // this should pass
            CertificationCheckResult crt = cs.CheckIntermittentTimeRequest(newCase, new IntermittentTimeRequest()
            {
                TotalMinutes = 480,
                RequestDate = new DateTime(2015, 2, 9, 0, 0, 0, DateTimeKind.Utc),
                Detail = new List<IntermittentTimeRequestDetail>()
                    {
                        new IntermittentTimeRequestDetail() { Approved = 480, PolicyCode = newCase.Segments[0].AppliedPolicies[0].Policy.Code}
                    }
            });

        }

        [TestMethod]
        public void STDPayTest()
        {
            // a CA emp
            string empId = "000000000000000000000017";
            string caseCode = "EHC";

            // Remove any prior cases for this employee to eliminate conflict thingies
            deleteTestCases(empId);

            Employee checkIt = Employee.GetById(empId);

            // Ensure the employee's schedule starts on a given date.
            checkIt.WorkSchedules.First().StartDate = new DateTime(2013, 6, 2, 0, 0, 0, DateTimeKind.Utc);
            checkIt.HireDate = new DateTime(2013, 6, 2, 0, 0, 0, DateTimeKind.Utc); // is a Sunday btw
            checkIt.ServiceDate = new DateTime(2013, 6, 2, 0, 0, 0, DateTimeKind.Utc);

            // give the employee a salary, say, 52k/year
            checkIt.Salary = 52000;
            checkIt.PayType = PayType.Salary;

            // Create a pay schedule for this employee, bi-weekly starting the 
            //  first day of the week of their service date.

            checkIt.PaySchedule = new PaySchedule()
            {
                CreatedById = User.DefaultUserId,
                ModifiedById = User.DefaultUserId,
                CustomerId = checkIt.CustomerId,
                EmployerId = checkIt.EmployerId,
                DayStart = (int)DayOfWeek.Sunday,
                DayEnd = (int)DayOfWeek.Saturday,
                Name = "Test Case Schedule for " + empId,
                PayPeriodType = PayPeriodType.BiWeekly,
                ProcessingDays = 2,
                StartDate = checkIt.ServiceDate.Value.GetFirstDayOfWeek()
            }.Save();

            checkIt.Save();

            // Employee health condition gets STD and CA-STD, yay!
            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            CaseService cs = new CaseService(false);
            PayService ps = new PayService(User.System);

            // make the case 13 weeks long
            DateTime startDate = new DateTime(2014, 3, 2, 0, 0, 0, DateTimeKind.Utc); // is a Sunday, btw
            DateTime endDate = new DateTime(2014, 5, 31, 0, 0, 0, DateTimeKind.Utc);

            // Create the case using a 13 week span for this employee, consecutive for EHC
            Case newCase = cs.CreateCase(CaseStatus.Open, empId, startDate, endDate, CaseType.Consecutive, reason.Code);
            // Run eligibility
            newCase = new EligibilityService().Using(e => e.RunEligibility(newCase)).Case;

            // Get the CA-STD and STD policies from the applied policies collections for all segments (btw, there should only be 1 segment)
            AppliedPolicy caStd = newCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "CA-STD");
            AppliedPolicy std = newCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "STD");

            // Ensure they are not null 'n' stuff, duh
            Assert.IsNotNull(caStd, "CA-STD should not be null/missing from case segments applied policies");
            Assert.IsNotNull(std, "STD should not be null/missing from case segments applied policies");

            // Fakey fakey our STD pay
            std.PolicyReason.PaymentTiers = new List<PaymentInfo>(2)
            {
                new PaymentInfo() { Duration = 6, Order = 1, PaymentPercentage = .8m, MaxPaymentPercentage = .8m },
                new PaymentInfo() { Duration = 20, Order = 2, PaymentPercentage = .5m, MaxPaymentPercentage = .6m }
            };

            // and also, the std policy has a 1 week eliminatin period, so remove that for this test
            std.PolicyReason.Elimination = null;
            std.PolicyReason.EliminationType = null;

            // Run calcs, which include payment stuff
            cs.GetProjectedUsage(newCase);

            // Approve all policies for the entire duration of the leave
            cs.ApplyDetermination(newCase, null, startDate, endDate, AdjudicationStatus.Approved);

            var caDetail = newCase.Pay.PayPeriods.SelectMany(p => p.Detail).Where(d => d.PolicyCode == "CA-STD").OrderBy(d => d.StartDate).ToList();
            var stdDetail = newCase.Pay.PayPeriods.SelectMany(p => p.Detail).Where(d => d.PolicyCode == "STD").OrderBy(d => d.StartDate).ToList();

            // Case start date happens to fall on the payroll start date, hooray! Should only be 7 pay periods I think (13 weeks, 6.5 pay periods, so really 7)
            Assert.AreEqual(7, newCase.Pay.PayPeriods.Count);

            // if there are 7 payment tiers then there should be at least 7 details
            Assert.AreEqual(7, caDetail.Count);

            // except for std, which because of the payment tier splitting mid way through
            // a pay period there should be 8
            Assert.AreEqual(8, stdDetail.Count);

            var caPercentages = caDetail.Select(d => d.PayPercentage.Value).ToArray();
            // After the 1 week waiting period, we expect 55% the rest of the pay periods for this policy.
            var caExpectedPercentages = new decimal[] { 0m, .55m, .55m, .55m, .55m, .55m, .55m, .55m };
            var caPayments = caDetail.Select(d => d.PayAmount.Value).ToArray();
            // In CA for STD we round up to the nearest whole dollar amount. These will already be even, but still
            //  in case you wanted to test for the rounding nonsense.
            var caExpectedPayments = new decimal[] { 0m, 1100m, 1100m, 1100m, 1100m, 1100m, 1100m, 1100m };

            // Loop and assert we have our expected results for each detail record.
            for (var i = 0; i < caDetail.Count; i++)
            {
                Assert.AreEqual(caExpectedPercentages[i], caPercentages[i]);
                Assert.AreEqual(caExpectedPayments[i], caPayments[i]);
            }

            var stdPercentages = stdDetail.Select(d => d.PayPercentage.Value).ToArray();
            // 3 pay periods @ 80%, then the remaining 4 @ 60%, given our Policy Pay Order, we expect up to 80%/60% total, 
            //  since CA is @ 55%, then we should compensate @ 25%/5% respectfully for this policy.
            var stdExpectedPercentages = new decimal[] { .8m, .25m, .25m, .25m, .05m, .05m, .05m, .05m };
            var stdPayments = stdDetail.Select(d => d.PayAmount.Value).ToArray();

            // even though there are 7 pay periods there are 8 detail rows because week 7 starts
            // std on a new payment tier and that is in the middle of a payroll cycle (the week of 4/14)
            var stdExpectedPayments = new decimal[] { 800m, 500m, 500m, 250m, 50m, 100m, 100m, 100m };

            // Loop and assert we have our expected results for each detail record.
            for (var i = 0; i < stdDetail.Count; i++)
            {
                Assert.AreEqual(stdExpectedPercentages[i], stdPercentages[i]);
                Assert.AreEqual(stdExpectedPayments[i], stdPayments[i]);
            }

            // now let's test the apply offset default flag

            std.PolicyReason.AllowOffset = true;
            (new CaseService()).RunCalcs(newCase);

            bool isOneOn = newCase.Pay.PayPeriods.Any(pp => pp.Detail.Any(det => det.IsOffset));
            Assert.IsTrue(isOneOn);

            // change the salary amount so that it will require rounding (rounding is turned on, but 52k a year
            // doesn need to be rounded)
            checkIt.Salary = 30000;
            checkIt.PayType = PayType.Salary;
            checkIt.Save();
            newCase.Employee.Salary = 30000;
            (new CaseService()).RunCalcs(newCase);

            caDetail = newCase.Pay.PayPeriods.SelectMany(p => p.Detail).Where(d => d.PolicyCode == "CA-STD").OrderBy(d => d.StartDate).ToList();
            stdDetail = newCase.Pay.PayPeriods.SelectMany(p => p.Detail).Where(d => d.PolicyCode == "STD").OrderBy(d => d.StartDate).ToList();

            // start with all the same checks as before (see above for comments on reasons)
            Assert.AreEqual(7, caDetail.Count);
            Assert.AreEqual(8, stdDetail.Count);
            caPercentages = caDetail.Select(d => d.PayPercentage.Value).ToArray();
            // After the 1 week waiting period, we expect 55% the rest of the pay periods for this policy.
            caExpectedPercentages = new decimal[] { 0m, .55m, .55m, .55m, .55m, .55m, .55m, .55m };
            caPayments = caDetail.Select(d => d.PayAmount.Value).ToArray();
            // now we want to test for that rounding non-sense
            caExpectedPayments = new decimal[] { 0m, 635m, 635m, 635m, 635m, 635m, 635m, 635m };

            // Loop and assert we have our expected results for each detail record.
            for (var i = 0; i < caDetail.Count; i++)
            {
                Assert.AreEqual(caExpectedPercentages[i], caPercentages[i]);
                Assert.AreEqual(caExpectedPayments[i], caPayments[i]);
            }

            stdPercentages = stdDetail.Select(d => d.PayPercentage.Value).ToArray();
            // 3 pay periods @ 80%, then the remaining 4 @ 60%, given our Policy Pay Order, we expect up to 80%/60% total, 
            //  since CA is @ 55%, then we should compensate @ 25%/5% respectfully for this policy.
            stdExpectedPercentages = new decimal[] { .8m, .25m, .25m, .25m, .05m, .05m, .05m, .05m };
            stdPayments = stdDetail.Select(d => d.PayAmount.Value).ToArray();

            // even though there are 7 pay periods there are 8 detail rows because week 7 starts
            // std on a new payment tier and that is in the middle of a payroll cycle (the week of 4/14)
            stdExpectedPayments = new decimal[] { 461.54m, 288.08m, 288.08m, 143.85m, 28.85m, 57.31m, 57.31m, 57.31m };

            // Loop and assert we have our expected results for each detail record.
            for (var i = 0; i < stdDetail.Count; i++)
            {
                Assert.AreEqual(stdExpectedPercentages[i], stdPercentages[i]);
                Assert.AreEqual(stdExpectedPayments[i], stdPayments[i]);
            }
        }                               // public void STDPayTestEasy()



        // i need test cases dates, etc. Though the rolling forward test does do this so maybe this test would be redundant
        [TestMethod]
        public void PeriodRollOver()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            CaseService cs = new CaseService();
            // make the case really long, long enough to exhaust i
            Case newCase = cs.CreateCase(CaseStatus.Open, empId, new DateTime(2014, 5, 1, 0, 0, 0, DateTimeKind.Utc), new DateTime(2014, 8, 31, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(newCase);

            cs.GetProjectedUsage(newCase);


            // make sure there is an fmla policy
            AppliedPolicy fmla = newCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
            Assert.IsNotNull(fmla, "FMLA Policy not found");

            Assert.IsTrue(fmla.Usage.Where(u => u.DateUsed == new DateTime(2014, 8, 29, 0, 0, 0, DateTimeKind.Utc) && u.Determination == AdjudicationStatus.Denied).Count() == 1);

            newCase.Description = "No more of these";
            newCase.Save();

            // 5/8 is a friday, 4/27 is a Monday
            Case case1 = cs.CreateCase(CaseStatus.Open, empId, new DateTime(2015, 4, 27, 0, 0, 0, DateTimeKind.Utc), new DateTime(2015, 5, 8, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
            esvc.RunEligibility(case1);

            cs.GetProjectedUsage(case1);

            fmla = case1.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
            Assert.IsNotNull(fmla, "FMLA Policy not found");

            // they should all be exahuasted because all the other usage is in the lookback period
            Assert.IsTrue(fmla.Usage.Where(u => u.Determination == AdjudicationStatus.Denied).Count() > 0);
            // mon 8/24 - fri 9/4 two weeks 
            // don't save that one, and make one out in the future overlapping the end of the last one
            Case case2 = cs.CreateCase(CaseStatus.Open, empId, new DateTime(2015, 8, 24, 0, 0, 0, DateTimeKind.Utc), new DateTime(2015, 9, 4, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
            esvc.RunEligibility(case2);

            cs.GetProjectedUsage(case2);

        }                               // public void PeriodRollOver()

        [TestMethod,Ignore]
        public void RollingForward()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            var testPolicy = Policy.GetByCode("FMLA");
            testPolicy.PolicyBehaviorType = PolicyBehavior.ReducesWorkWeek;
            testPolicy.Save();
            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            // same fmla exhaustion as last every other time, this time the first one is set to rolling forward. 
            // the results should be the same
            CaseService cs = new CaseService();
            // make the case 13 weeks long
            Case firstCase = cs.CreateCase(CaseStatus.Open, empId, new DateTime(2014, 3, 2, 0, 0, 0, DateTimeKind.Utc), new DateTime(2014, 5, 31, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
            //Case newCase = cs.CreateCase(empId, new DateTime(2014, 3, 2, 0, 0, 0, DateTimeKind.Utc), new DateTime(2014, 3, 4, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(firstCase);


            // make sure there is an fmla policy
            AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
            Assert.IsNotNull(fmla, "FMLA Policy not found");

            // switch it to rolling forward. It should calc the same as before
            fmla.PolicyReason.PeriodType = PeriodType.RollingForward;
            cs.ApplyDetermination(firstCase, "FMLA", firstCase.StartDate, firstCase.EndDate.Value, AdjudicationStatus.Approved);
            cs.GetProjectedUsage(firstCase);
            fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");

            Assert.AreEqual(91, fmla.Usage.Count);        // 60 days approved, 4 days exhausted, 27 non-working days 

            double minsUsed = fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.MinutesUsed);
            double daysUsed = Math.Round(fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.PercentOfDay), 4);
            double weeksUsed = Math.Round(fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.DaysUsage(EntitlementType.WorkWeeks)), 4);

            Assert.IsTrue(minsUsed == 28800);
            Assert.IsTrue(daysUsed == 60);
            Assert.IsTrue(weeksUsed == 12);

            // the first exhausted day is Tuesday after the 12th weeks ends because the Monday is Memorial Day.
            AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
            Assert.IsTrue(apu.DateUsed == new DateTime(2014, 5, 24, 0, 0, 0, DateTimeKind.Utc));

            firstCase.Description = "No more of these";
            firstCase.Save();

            // now.... this case will be for two weeks 2/23/15 which is the week before the last start date and that week should exhuast
            // through 3/6 which is a Friday and the whole week should allocate because with rolling forward the whole prior usage should drop off on 3/2
            // 2/24 is the elim period
            // 3/3 = 2 days used, should be elim period
            // 3/14 = 10 days used (with time)
            // rolling forward and the period change
            Case secondCase = cs.CreateCase(CaseStatus.Open, empId, new DateTime(2015, 2, 23, 0, 0, 0, DateTimeKind.Utc), new DateTime(2015, 2, 24, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
            esvc.RunEligibility(secondCase);
            fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
            Assert.IsNotNull(fmla, "FMLA Policy not found");

            // set the type and recalc
            fmla.PolicyReason.PeriodType = PeriodType.RollingForward;
            cs.GetProjectedUsage(secondCase);
            fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");


            // and do the tests
            AppliedPolicyUsage fmla_used = fmla.Usage.FirstOrDefault(f => f.DateUsed == new DateTime(2015, 2, 23, 0, 0, 0, DateTimeKind.Utc));
            Assert.IsNotNull(fmla_used);
            Assert.AreEqual(AdjudicationDenialReason.EliminationPeriod, fmla_used.DenialReasonCode);

            // change the dates and do it again, and now the begining should be exhausted and the end pending
            Case thirdCase = cs.CreateCase(CaseStatus.Open, empId, new DateTime(2015, 2, 23, 0, 0, 0, DateTimeKind.Utc), new DateTime(2015, 3, 14, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
            esvc.RunEligibility(thirdCase);

            // set the rolling forward and calc again
            fmla = thirdCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
            fmla.PolicyReason.PeriodType = PeriodType.RollingForward;
            cs.GetProjectedUsage(thirdCase);

            fmla = thirdCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
            fmla_used = fmla.Usage.FirstOrDefault(f => f.DateUsed == new DateTime(2015, 2, 23, 0, 0, 0, DateTimeKind.Utc));
            Assert.IsNotNull(fmla_used);
            Assert.AreEqual(AdjudicationDenialReason.Exhausted, fmla_used.DenialReasonCode);

            fmla_used = fmla.Usage.FirstOrDefault(f => f.DateUsed == new DateTime(2015, 3, 3, 0, 0, 0, DateTimeKind.Utc));
            Assert.IsNotNull(fmla_used);
            Assert.AreEqual(AdjudicationStatus.Denied, fmla_used.Determination);
        }

        [TestMethod]
        public void RollingForwardTakeAndIntermittent()
        {
            var testPolicy = Policy.GetByCode("FMLA");
            testPolicy.PolicyBehaviorType = PolicyBehavior.ReducesWorkWeek;
            testPolicy.Save();

            string empId = "000000000000000000000075";
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();



            deleteTestCases(empId);

            CaseService cs = new CaseService();
            // exhaust the case
            DateTime firstCaseStart = new DateTime(2014, 5, 1, 0, 0, 0, DateTimeKind.Utc);
            Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstCaseStart, new DateTime(2014, 7, 31, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(firstCase);

            // make sure there is an fmla policy
            Assert.IsNotNull(firstCase);
            Assert.IsNotNull(firstCase.Segments);
            Assert.IsTrue(firstCase.Segments.Count >= 1);

            AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == "FMLA");
            Assert.IsNotNull(fmla, "FMLA Policy not found");
            // set it all to rolling forward
            firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).ForEach(ap => ap.PolicyReason.PeriodType = PeriodType.RollingForward);

            // calc again
            cs.GetProjectedUsage(firstCase);

            fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == "FMLA");


            // it should exhaust, just approve all the dates up to there
            //firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).Where(p => p.Policy.Code == "FMLA").ToList()
            //    .ForEach(ap => cs.ApplyDetermination(firstCase, ap.Policy.Code, firstCaseStart, ap.FirstExhaustionDate.Value.AddDays(-1), AdjudicationStatus.Approved));

            //firstCase.Save();


            /////////////////////////////////////////////////////
            //////
            ///// second case
            /////
            ////////////////////////////////////////////////////
            DateTime secondCaseStartDate = new DateTime(2014, 8, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondCaseEndDate = new DateTime(2015, 1, 31, 0, 0, 0, DateTimeKind.Utc);
            Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondCaseStartDate, secondCaseEndDate, AbsenceSoft.Data.Enums.CaseType.Intermittent, reason.Code);

            esvc.RunEligibility(secondCase);

            // make sure there is an fmla policy
            fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == "FMLA");
            Assert.IsNotNull(fmla, "FMLA Policy not found");


            // set it all to rolling forward
            secondCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).ForEach(ap => ap.PolicyReason.PeriodType = PeriodType.RollingForward);

            // calc again
            cs.GetProjectedUsage(secondCase);

            // make sure there is an fmla policy
            fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == "FMLA");
            Assert.IsNotNull(fmla, "FMLA Policy not found");

            // it should exhaust, just approve all the dates up to there
            secondCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).Where(p => p.Policy.Code == "FMLA").ToList()
                .ForEach(ap => cs.ApplyDetermination(secondCase, ap.Policy.Code, secondCaseStartDate, secondCaseEndDate, AdjudicationStatus.Approved));

            cs.GetProjectedUsage(secondCase);

            fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == "FMLA");

            List<IntermittentTimeRequest> itr = new List<IntermittentTimeRequest>(1) {
                        new IntermittentTimeRequest()
                        {
                            RequestDate = secondCaseStartDate,
                            TotalMinutes = 240,
                            Detail = new List<IntermittentTimeRequestDetail>() {
                                { new IntermittentTimeRequestDetail()
                                    { Pending = 240, PolicyCode = fmla.Policy.Code}
                                }},
                            Notes = "my notes"
                        }};

            cs.CreateOrModifyTimeOffRequest(secondCase, itr);

            cs.GetProjectedUsage(secondCase);
            fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == "FMLA");

            Assert.IsNotNull(fmla, "FMLA Policy not found");

            // and now our time off request should be denied
            //AppliedPolicyUsage fmla_used = fmla.Usage.FirstOrDefault(f => f.DateUsed == new DateTime(2014, 8, 1, 0, 0, 0, DateTimeKind.Utc));
            //Assert.IsNotNull(fmla_used);
            //Assert.AreEqual(fmla_used.Determination, AdjudicationStatus.Denied);


            // clean up
            deleteTestCases(empId);
        }//end: RollingForwardTakeAndIntermittent()


        [TestMethod]
        public void MultiScheduleLeaveOfAbsence()
        {

            // this pulls from several tests. first build up an employee with multiple schedules
            string empId = "000000000000000000000037";
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            // the employee has a schedule that is weekly starts on 11/5/2011
            // first step will be to create a new rotating schedule
            // this one will be four 10 hour days and will be tues - fri

            EmployeeService es = new EmployeeService();
            Employee theEmp = Employee.GetById(empId);

            // make sure that they have one schedule already
            Assert.AreEqual(theEmp.WorkSchedules.Count, 1);

            // sched starts on a sunday
            Schedule newWs = new Schedule()
            {
                ScheduleType = ScheduleType.Rotating,
                StartDate = new DateTime(2012, 3, 4, 0, 0, 0, DateTimeKind.Utc),
                Times = new List<Time>()
                {
                    new Time() { SampleDate = new DateTime(2012,2,26,0,0,0,DateTimeKind.Utc), TotalMinutes = 0 },      /* the previous sunday, because sample date should not matter */
                    new Time() { SampleDate = new DateTime(2012,2,27,0,0,0,DateTimeKind.Utc), TotalMinutes = 0 },
                    new Time() { SampleDate = new DateTime(2012,2,28,0,0,0,DateTimeKind.Utc), TotalMinutes = 600 },
                    new Time() { SampleDate = new DateTime(2012,2,29,0,0,0,DateTimeKind.Utc), TotalMinutes = 600 },
                    new Time() { SampleDate = new DateTime(2012,3,1,0,0,0,DateTimeKind.Utc), TotalMinutes = 600 },
                    new Time() { SampleDate = new DateTime(2012,3,2,0,0,0,DateTimeKind.Utc), TotalMinutes = 600 },
                    new Time() { SampleDate = new DateTime(2012,3,3,0,0,0,DateTimeKind.Utc), TotalMinutes = 0 },
                }
            };

            // add the schedule
            es.SetWorkSchedule(theEmp, newWs, null);

            Assert.AreEqual(theEmp.WorkSchedules.Count, 2);
            Assert.AreEqual(theEmp.WorkSchedules[0].EndDate, new DateTime(2012, 3, 3, 0, 0, 0, DateTimeKind.Utc));

            // now make a newer schedule, let's do the same as before, but now we'll do three 12 hour days and one 4 hour day
            Schedule thirdSchedule = new Schedule()
            {
                ScheduleType = ScheduleType.Rotating,
                StartDate = new DateTime(2014, 6, 1, 0, 0, 0, DateTimeKind.Utc),
                Times = new List<Time>()
                {
                    new Time() { SampleDate = new DateTime(2012,2,26,0,0,0,DateTimeKind.Utc), TotalMinutes = 0 },      /* the previous sunday, because sample date should not matter */
                    new Time() { SampleDate = new DateTime(2012,2,27,0,0,0,DateTimeKind.Utc), TotalMinutes = 720 },
                    new Time() { SampleDate = new DateTime(2012,2,28,0,0,0,DateTimeKind.Utc), TotalMinutes = 720 },
                    new Time() { SampleDate = new DateTime(2012,2,29,0,0,0,DateTimeKind.Utc), TotalMinutes = 720 },
                    new Time() { SampleDate = new DateTime(2012,3,1,0,0,0,DateTimeKind.Utc), TotalMinutes = 240 },
                    new Time() { SampleDate = new DateTime(2012,3,2,0,0,0,DateTimeKind.Utc), TotalMinutes = 0 },
                    new Time() { SampleDate = new DateTime(2012,3,3,0,0,0,DateTimeKind.Utc), TotalMinutes = 0 },
                }
            };

            es.SetWorkSchedule(theEmp, thirdSchedule, null);

            Assert.AreEqual(theEmp.WorkSchedules.Count, 3);
            Assert.AreEqual(theEmp.WorkSchedules[1].EndDate, new DateTime(2014, 5, 31, 0, 0, 0, DateTimeKind.Utc));

            // save the employee because the services reload it from the id
            es.Update(theEmp);

            // now that we have our schedules created. Let's set up a case that crosses them
            CaseService cs = new CaseService();

            // just a really basic test fisrt
            // the last Friday in May, through the first Saturday in June. Which is 14 days.
            DateTime firstCaseStart = new DateTime(2014, 5, 25, 0, 0, 0, DateTimeKind.Utc);
            Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstCaseStart, new DateTime(2014, 6, 7, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(firstCase);

            cs.GetProjectedUsage(firstCase);

            Assert.IsNotNull(firstCase);
            Assert.IsNotNull(firstCase.Segments);
            Assert.IsTrue(firstCase.Segments.Count >= 1);

            AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == "FMLA");
            Assert.IsNotNull(fmla, "FMLA Policy not found");
            Assert.AreEqual(14, fmla.Usage.Count);

            // give our schedule, wed 5/30 should have 10 hours and Tues 6/3 should have 12 hours
            DateTime findDate = new DateTime(2014, 5, 30, 0, 0, 0, DateTimeKind.Utc);
            AppliedPolicyUsage wed530 = fmla.Usage.FirstOrDefault(f => f.DateUsed == findDate);
            Assert.IsNotNull(wed530);
            Assert.AreEqual(wed530.MinutesUsed, 600);

            findDate = new DateTime(2014, 6, 3, 0, 0, 0, DateTimeKind.Utc);
            AppliedPolicyUsage tues63 = fmla.Usage.FirstOrDefault(f => f.DateUsed == findDate);
            Assert.IsNotNull(tues63);
            Assert.AreEqual(tues63.MinutesUsed, 720);


            // clean up for the next test that uses this emp
            deleteTestCases(empId);

            // all done

        }                           // MultiScheduleLeaveOfAbsence

        [TestMethod]
        public void CaseSummary()
        {
            // again, chunk stolen from TestCalcs
            string empId = "000000000000000000000003";
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            CaseService cs = new CaseService();

            // create a case that will trip over both Veterns Day and Thanksgiving
            Case newCase = cs.CreateCase(CaseStatus.Open, empId, new DateTime(2014, 11, 1, 0, 0, 0, DateTimeKind.Utc), new DateTime(2014, 12, 5, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(newCase);

            Assert.IsNotNull(newCase.Summary);

            // start with the std policy
            CaseSummaryPolicy cspSTD = newCase.Summary.Policies.FirstOrDefault(f => f.PolicyCode == "STD");
            Assert.IsNotNull(cspSTD);
            Assert.IsNotNull(cspSTD.Detail);

            // on this first try there should be one row with the same start and end date and
            // std has an elimination period, check for that then the regular days
            Assert.AreEqual(cspSTD.Detail.Count, 2);
            Assert.AreEqual(cspSTD.Detail[0].StartDate, new DateTime(2014, 11, 1, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspSTD.Detail[0].EndDate, new DateTime(2014, 11, 7, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspSTD.Detail[0].Determination, AdjudicationSummaryStatus.Denied);
            Assert.AreEqual(cspSTD.Detail[0].DenialReasonCode, AdjudicationDenialReason.EliminationPeriod);

            Assert.AreEqual(cspSTD.Detail[1].StartDate, new DateTime(2014, 11, 8, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspSTD.Detail[1].EndDate, new DateTime(2014, 12, 5, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspSTD.Detail[1].Determination, AdjudicationSummaryStatus.Pending);

            // now deny all of the std
            cs.ApplyDetermination(newCase, cspSTD.PolicyCode, new DateTime(2014, 10, 1, 0, 0, 0, DateTimeKind.Utc), new DateTime(2014, 12, 5, 0, 0, 0, DateTimeKind.Utc), AdjudicationStatus.Denied, AdjudicationDenialReason.NotASeriousHealthCondition, null, AdjudicationDenialReason.NotASeriousHealthConditionDescription);

            cspSTD = newCase.Summary.Policies.FirstOrDefault(f => f.PolicyCode == "STD");
            Assert.AreEqual(cspSTD.Detail.Count, 1);
            Assert.AreEqual(cspSTD.Detail[0].StartDate, new DateTime(2014, 11, 1, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspSTD.Detail[0].EndDate, new DateTime(2014, 12, 5, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspSTD.Detail[0].Determination, AdjudicationSummaryStatus.Denied);
            Assert.AreEqual(cspSTD.Detail[0].DenialReasonCode, AdjudicationDenialReason.NotASeriousHealthCondition);


            // find the fmla policy sumary
            CaseSummaryPolicy cspFMLA = newCase.Summary.Policies.FirstOrDefault(f => f.PolicyCode == "FMLA");
            Assert.IsNotNull(cspFMLA);
            Assert.IsNotNull(cspFMLA.Detail);

            // on this first try there should be one row with the same start and end date and
            // all set to pending
            Assert.AreEqual(cspFMLA.Detail.Count, 1);
            Assert.AreEqual(cspFMLA.Detail[0].StartDate, new DateTime(2014, 11, 1, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspFMLA.Detail[0].EndDate, new DateTime(2014, 12, 5, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspFMLA.Detail[0].Determination, AdjudicationSummaryStatus.Pending);

            // first off the std piece should still be the same
            cspSTD = newCase.Summary.Policies.FirstOrDefault(f => f.PolicyCode == "STD");
            Assert.AreEqual(cspSTD.Detail.Count, 1);
            Assert.AreEqual(cspSTD.Detail[0].StartDate, new DateTime(2014, 11, 1, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspSTD.Detail[0].EndDate, new DateTime(2014, 12, 5, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspSTD.Detail[0].Determination, AdjudicationSummaryStatus.Denied);
            Assert.AreEqual(cspSTD.Detail[0].DenialReasonCode, AdjudicationDenialReason.NotASeriousHealthCondition);

            // now set part of the case to approved
            cs.ApplyDetermination(newCase, cspFMLA.PolicyCode, new DateTime(2014, 11, 1, 0, 0, 0, DateTimeKind.Utc), new DateTime(2014, 12, 1, 0, 0, 0, DateTimeKind.Utc), AdjudicationStatus.Approved);

            // first off the std piece should still be the same
            cspSTD = newCase.Summary.Policies.FirstOrDefault(f => f.PolicyCode == "STD");
            Assert.AreEqual(cspSTD.Detail.Count, 1);
            Assert.AreEqual(cspSTD.Detail[0].StartDate, new DateTime(2014, 11, 1, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspSTD.Detail[0].EndDate, new DateTime(2014, 12, 5, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspSTD.Detail[0].Determination, AdjudicationSummaryStatus.Denied);
            Assert.AreEqual(cspSTD.Detail[0].DenialReasonCode, AdjudicationDenialReason.NotASeriousHealthCondition);

            // it has been recalced and regened so grab it again
            cspFMLA = newCase.Summary.Policies.FirstOrDefault(f => f.PolicyCode == "FMLA");
            Assert.IsNotNull(cspFMLA);
            Assert.IsNotNull(cspFMLA.Detail);

            // this time there should be two sets, one approved, one pending
            Assert.AreEqual(cspFMLA.Detail.Count, 2);
            Assert.AreEqual(cspFMLA.Detail[0].StartDate, new DateTime(2014, 11, 1, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspFMLA.Detail[0].EndDate, new DateTime(2014, 12, 1, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspFMLA.Detail[0].Determination, AdjudicationSummaryStatus.Approved);

            Assert.AreEqual(cspFMLA.Detail[1].StartDate, new DateTime(2014, 12, 2, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspFMLA.Detail[1].EndDate, new DateTime(2014, 12, 5, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspFMLA.Detail[1].Determination, AdjudicationSummaryStatus.Pending);

            // now deny them, we should get the same results except with a different status
            cs.ApplyDetermination(newCase, cspFMLA.PolicyCode, new DateTime(2014, 11, 1, 0, 0, 0, DateTimeKind.Utc), new DateTime(2014, 12, 1, 0, 0, 0, DateTimeKind.Utc), AdjudicationStatus.Denied, AdjudicationDenialReason.NotASeriousHealthCondition, null, AdjudicationDenialReason.NotASeriousHealthConditionDescription);

            // just here to use the debugger (comment in or out as needed)
            //cs.CalcCaseSummary(newCase);

            // it has been recalced and regened so grab it again
            cspFMLA = newCase.Summary.Policies.FirstOrDefault(f => f.PolicyCode == "FMLA");
            Assert.IsNotNull(cspFMLA);
            Assert.IsNotNull(cspFMLA.Detail);

            // there should still be two sets, one denied, one pending
            Assert.AreEqual(cspFMLA.Detail.Count, 2);
            Assert.AreEqual(cspFMLA.Detail[0].StartDate, new DateTime(2014, 11, 1, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspFMLA.Detail[0].EndDate, new DateTime(2014, 12, 1, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspFMLA.Detail[0].Determination, AdjudicationSummaryStatus.Denied);

            Assert.AreEqual(cspFMLA.Detail[1].StartDate, new DateTime(2014, 12, 2, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspFMLA.Detail[1].EndDate, new DateTime(2014, 12, 5, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(cspFMLA.Detail[1].Determination, AdjudicationSummaryStatus.Pending);


        }

        /// <summary>
        /// Test for ticket(s) #1260, 
        /// </summary>

        [TestMethod,Ignore]
        public void CaseStartsExhausted()
        {
            var testPolicy = Policy.GetByCode("FMLA");
            testPolicy.PolicyBehaviorType = PolicyBehavior.ReducesWorkWeek;
            testPolicy.Save();


            string empId = "000000000000000000000075";


            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            CaseService cs = new CaseService();

            DateTime firstStartDate = new DateTime(2014, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2014, 4, 2, 0, 0, 0, DateTimeKind.Utc);


            // exhaust the case
            Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(firstCase);

            // make sure there is an fmla policy
            Assert.IsNotNull(firstCase);
            Assert.IsNotNull(firstCase.Segments);
            Assert.IsTrue(firstCase.Segments.Count >= 1);

            // approve the case
            firstCase.Segments.SelectMany(s => s.AppliedPolicies).ForEach(ap =>
            {
                cs.ApplyDetermination(firstCase, ap.Policy.Code, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
            });

            AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == "FMLA");
            Assert.IsNotNull(fmla, "FMLA Policy not found");

            // case should exhaust on 3/29 (I guess it's debatable when it is exhausted since Saturday isn't a work day, but Friday 3/28 was last of the available time)
            // thanks to the zero days, this keeps getting changed back and forth. Time ran out on Friday. Saturday and Sunday's aren't work days....so back to monday the 31th

            /// Should case exhaust on 3/26 due to holidays now?
            DateTime FirstExhasutdate = new DateTime(2014, 3, 26, 0, 0, 0, DateTimeKind.Utc);

            AppliedPolicyUsage firstCaseExhaustUsage = fmla.Usage.FirstOrDefault(fd => fd.DateUsed == FirstExhasutdate && fd.Determination == AdjudicationStatus.Denied && fd.DenialReasonCode == AdjudicationDenialReason.Exhausted);


            Assert.IsNotNull(firstCaseExhaustUsage);
            Assert.AreEqual(firstCaseExhaustUsage.Determination, AdjudicationStatus.Denied);
            Assert.AreEqual(firstCaseExhaustUsage.DenialReasonCode, AdjudicationDenialReason.Exhausted);

            // commit the change so it's in the db to pick up on the next date range
            firstCase.Save();

            // create the second case
            //DateTime secondStartDate = new DateTime(2015, 3, 1, 0, 0, 0, DateTimeKind.Utc);
            //DateTime secondEndDate = new DateTime(2015, 4, 1, 0, 0, 0, DateTimeKind.Utc);

            // 11/12 is a Sunday, even though it is not a workday it should show exhausted
            DateTime secondStartDate = new DateTime(2014, 12, 01, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondEndDate = new DateTime(2015, 2, 4, 0, 0, 0, DateTimeKind.Utc);

            Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondStartDate, secondEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

            LeaveOfAbsence loa2 = esvc.RunEligibility(secondCase);

            // test the second case
            Assert.IsNotNull(secondCase);
            Assert.IsNotNull(secondCase.Segments);
            Assert.IsTrue(secondCase.Segments.Count >= 1);


            AppliedPolicy fmla2 = secondCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == "FMLA");
            Assert.IsNotNull(fmla2, "FMLA Policy not found");

            // the first case should start off exhausted
            Assert.IsTrue(fmla2.Usage.Count > 0);
            Assert.AreEqual(fmla2.Usage[0].DateUsed, secondStartDate);
            Assert.AreEqual(fmla2.Usage[0].Determination, AdjudicationStatus.Approved);
            //Assert.AreEqual(fmla2.Usage[0].DenialReasonCode, AdjudicationDenialReason.Exhausted);

            //// december 30th should still be exhausted but Jan 2, should be pending
            //// (jan 1 is pending because it is a holiday)
            //AppliedPolicyUsage dec30 = fmla2.Usage.FirstOrDefault(u => u.DateUsed == new DateTime(2014, 12, 30, 0, 0, 0, DateTimeKind.Utc) && u.Determination == AdjudicationStatus.Denied);
            //Assert.IsNotNull(dec30);

            //AppliedPolicyUsage jan6 = fmla2.Usage.FirstOrDefault(u => u.DateUsed == new DateTime(2015, 1, 6, 0, 0, 0, DateTimeKind.Utc) && u.Determination == AdjudicationStatus.Pending);
            //Assert.IsNotNull(jan6);
        }

        /// <summary>
        /// Test for ticket # AT-5495
        /// </summary>
        [TestMethod]
        public void LastDayPolicyExhaustion()
        {
            Policy policy = Policy.GetByCode("FMLA");
            policy.Save();

            string employeeId = "000000000000000000000075";
            string caseCode = "EHC";

            AbsenceReason absenceReason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(employeeId);

            CaseService caseService = new CaseService();

            DateTime caseStartDate = new DateTime(2017, 10, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime caseEndDate = new DateTime(2017, 12, 23, 0, 0, 0, DateTimeKind.Utc);

            Case _case = caseService.CreateCase(CaseStatus.Open, employeeId, caseStartDate, caseEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, absenceReason.Code);

            EligibilityService eligibilityService = new EligibilityService();
            LeaveOfAbsence leaveOfAbsence = eligibilityService.RunEligibility(_case);

            // make sure there is an fmla policy
            Assert.IsNotNull(_case);
            Assert.IsNotNull(_case.Segments);
            Assert.IsTrue(_case.Segments.Count >= 1);

            // approve the case
            _case.Segments.SelectMany(s => s.AppliedPolicies).ForEach(ap =>
            {
                caseService.ApplyDetermination(_case, ap.Policy.Code, caseStartDate, caseEndDate, AdjudicationStatus.Approved);
            });

            AppliedPolicy fmla = _case.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == "FMLA");
            Assert.IsNotNull(fmla, "FMLA Policy not found");

            // case was getting exhausted on 12/23 i.e. saturday even when there is time remaining 
            var isExhausted = _case.Summary.Policies.FirstOrDefault(e => e.PolicyCode == "FMLA").Detail.Any(d => d.DenialReasonCode == AdjudicationDenialReason.Exhausted);
            Assert.IsFalse(isExhausted);
        }

        // test an intermittent and consecutive case going on at the same time
        // well... fisrt an intermittent case, then close the case and open a consecutive case that runs
        // on top of the intermittent case (ticket 1259)
        [TestMethod]
        public void ConsecutiveAndIntermittentCase()
        {
            var testPolicy = Policy.GetByCode("FMLA");
            testPolicy.PolicyBehaviorType = PolicyBehavior.ReducesWorkWeek;
            testPolicy.Save();


            string empId = "000000000000000000000075";
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            // clean out the other tests (if the bombed)
            deleteTestCases(empId);

            CaseService cs = new CaseService();

            DateTime intStartDate = new DateTime(2014, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime intEndDate = new DateTime(2014, 12, 31, 0, 0, 0, DateTimeKind.Utc);

            // exhaust the case
            Case intCase = cs.CreateCase(CaseStatus.Open, empId, intStartDate, intEndDate, AbsenceSoft.Data.Enums.CaseType.Intermittent, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(intCase);

            // approve the leave for time off
            intCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).Where(p => p.Policy.Code == "FMLA").ToList()
                        .ForEach(ap => cs.ApplyDetermination(intCase, ap.Policy.Code, intStartDate, intEndDate, AdjudicationStatus.Approved));

            cs.GetProjectedUsage(intCase);
            intCase.Save();

            // get the fmla policy
            Assert.IsTrue(intCase.Segments.Count > 0);
            Assert.IsNotNull(intCase.Segments[0].AppliedPolicies);
            Assert.IsTrue(intCase.Segments[0].AppliedPolicies.Count > 0);

            AppliedPolicy fmla = intCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == "FMLA");

            //// create the time off requests
            //List<IntermittentTimeRequest> itr = new List<IntermittentTimeRequest>() {
            //            new IntermittentTimeRequest()
            //            {
            //                RequestDate = new DateTime(2014, 1, 9, 0, 0, 0, DateTimeKind.Utc),
            //                TotalMinutes = 480,
            //                Detail = new List<IntermittentTimeRequestDetail>() {
            //                    { new IntermittentTimeRequestDetail()
            //                        { Approved=480, PolicyCode = fmla.Policy.Code}
            //                    }},
            //                Notes = "my notes 1"
            //            },
            //            new IntermittentTimeRequest()
            //            {
            //                RequestDate = new DateTime(2014, 1, 10, 0, 0, 0, DateTimeKind.Utc),
            //                TotalMinutes = 480,
            //                Detail = new List<IntermittentTimeRequestDetail>() {
            //                    { new IntermittentTimeRequestDetail()
            //                        { Approved = 480, PolicyCode = fmla.Policy.Code}
            //                    }},
            //                Notes = "my notes 2"
            //            },
            //            new IntermittentTimeRequest()
            //            {
            //                RequestDate = new DateTime(2014, 3, 10, 0, 0, 0, DateTimeKind.Utc),
            //                TotalMinutes = 480,
            //                Detail = new List<IntermittentTimeRequestDetail>() {
            //                    { new IntermittentTimeRequestDetail()
            //                        { Approved = 480, PolicyCode = fmla.Policy.Code}
            //                    }},
            //                Notes = "my notes 3"
            //            },
            //            new IntermittentTimeRequest()
            //            {
            //                RequestDate = new DateTime(2014, 3, 11, 0, 0, 0, DateTimeKind.Utc),
            //                TotalMinutes = 480,
            //                Detail = new List<IntermittentTimeRequestDetail>() {
            //                    { new IntermittentTimeRequestDetail()
            //                        { Approved = 480, PolicyCode = fmla.Policy.Code}
            //                    }},
            //                Notes = "my notes 4"
            //            },
            //            new IntermittentTimeRequest()
            //            {
            //                RequestDate = new DateTime(2014, 3, 12, 0, 0, 0, DateTimeKind.Utc),
            //                TotalMinutes = 480,
            //                Detail = new List<IntermittentTimeRequestDetail>() {
            //                    { new IntermittentTimeRequestDetail()
            //                        { Approved = 480, PolicyCode = fmla.Policy.Code}
            //                    }},
            //                Notes = ""
            //            },
            //            new IntermittentTimeRequest()
            //            {
            //                RequestDate = new DateTime(2014, 4, 8, 0, 0, 0, DateTimeKind.Utc),
            //                TotalMinutes = 480,
            //                Detail = new List<IntermittentTimeRequestDetail>() {
            //                    { new IntermittentTimeRequestDetail()
            //                        { Approved = 480, PolicyCode = fmla.Policy.Code}
            //                    }},
            //                Notes = "This will force the 2015 leave to exhaust on 3-30-2015 instead of 3-31-2015 because it will not be earned back before the end of the case"
            //            }

            //};

            //// approve the time
            //intCase.Segments.SelectMany(s => s.AppliedPolicies).ForEach(ap =>
            //{
            //    cs.ApplyDetermination(intCase, ap.Policy.Code, intStartDate, intEndDate, AdjudicationStatus.Approved);
            //});

            //// create a time off request
            //cs.CreateOrModifyTimeOffRequest(intCase, itr);
            //cs.GetProjectedUsage(intCase);

            //intCase.Save();

            //fmla = intCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == "FMLA");
            //DateTime leaveDate = new DateTime(2014, 3, 12, 0, 0, 0, DateTimeKind.Utc);
            //AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == leaveDate);
            //Assert.IsNotNull(apu);
            //Assert.AreEqual(apu.Determination, AdjudicationStatus.Approved);

            /////////////////////////////////////////////////////
            //
            //  consecutive case
            //
            // should still exhaust on 4/2 because they will earn back their days
            // as the case progresses
            /////////////////////////////////////////////////////
            DateTime consStartDate = new DateTime(2015, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime conEndDate = new DateTime(2015, 4, 4, 0, 0, 0, DateTimeKind.Utc);

            // exhaust the case
            Case conCase = cs.CreateCase(CaseStatus.Open, empId, consStartDate, conEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
            loa = esvc.RunEligibility(conCase);

            // make sure there is an fmla policy
            Assert.IsNotNull(conCase);
            Assert.IsNotNull(conCase.Segments);
            Assert.IsTrue(conCase.Segments.Count >= 1);

            fmla = conCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == "FMLA");
            Assert.IsNotNull(fmla, "FMLA Policy not found");
            DateTime FirstExhasutdate = new DateTime(2015, 3, 25, 0, 0, 0, DateTimeKind.Utc);
            // first exhaustion date should be 3/25?

            //AppliedPolicyUsage firstCaseExhaustUsage = fmla.Usage.FirstOrDefault(fd => fd.DateUsed == FirstExhasutdate);
            //Assert.AreEqual(AdjudicationStatus.Pending, fmla.Usage[0].Determination);

            // and then clean up from the tests
            deleteTestCases(empId);
        }

        [TestMethod]
        public void IntermittentCloseWhileTimePending()
        {
            string empId = "000000000000000000000075";
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            // clean out the other tests (if the bombed)
            deleteTestCases(empId);

            CaseService cs = new CaseService();

            DateTime intStartDate = new DateTime(2014, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime intEndDate = new DateTime(2014, 12, 31, 0, 0, 0, DateTimeKind.Utc);

            // exhaust the case
            Case intCase = cs.CreateCase(CaseStatus.Open, empId, intStartDate, intEndDate, AbsenceSoft.Data.Enums.CaseType.Intermittent, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(intCase);

            // approve the leave for time off
            intCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).Where(p => p.Policy.Code == "FMLA").ToList()
                        .ForEach(ap => cs.ApplyDetermination(intCase, ap.Policy.Code, intStartDate, intEndDate, AdjudicationStatus.Approved));

            cs.GetProjectedUsage(intCase);
            intCase.Save();

            // get the fmla policy
            Assert.IsTrue(intCase.Segments.Count > 0);
            Assert.IsNotNull(intCase.Segments[0].AppliedPolicies);
            Assert.IsTrue(intCase.Segments[0].AppliedPolicies.Count > 0);

            AppliedPolicy fmla = intCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == "FMLA");

            // create the time off requests
            //List<IntermittentTimeRequest> itr = new List<IntermittentTimeRequest>() {
            //            new IntermittentTimeRequest()
            //            {
            //                RequestDate = new DateTime(2014, 1, 9, 0, 0, 0, DateTimeKind.Utc),
            //                TotalMinutes = 480,
            //                Detail = new List<IntermittentTimeRequestDetail>() {
            //                    { new IntermittentTimeRequestDetail()
            //                        { Approved=480, PolicyCode = fmla.Policy.Code }
            //                    }},
            //                Notes = "my notes 1"
            //            },
            //            new IntermittentTimeRequest()
            //            {
            //                RequestDate = new DateTime(2014, 1, 10, 0, 0, 0, DateTimeKind.Utc),
            //                TotalMinutes = 480,
            //                Detail = new List<IntermittentTimeRequestDetail>() {
            //                    { new IntermittentTimeRequestDetail()
            //                        { Approved = 480, PolicyCode = fmla.Policy.Code }
            //                    }},
            //                Notes = "my notes 2"
            //            },
            //            new IntermittentTimeRequest()
            //            {
            //                RequestDate = new DateTime(2014, 3, 10, 0, 0, 0, DateTimeKind.Utc),
            //                TotalMinutes = 480,
            //                Detail = new List<IntermittentTimeRequestDetail>() {
            //                    { new IntermittentTimeRequestDetail()
            //                        { Pending = 480, PolicyCode = fmla.Policy.Code }
            //                    }},
            //                Notes = "my notes 3"
            //            },
            //            new IntermittentTimeRequest()
            //            {
            //                RequestDate = new DateTime(2014, 3, 11, 0, 0, 0, DateTimeKind.Utc),
            //                TotalMinutes = 480,
            //                Detail = new List<IntermittentTimeRequestDetail>() {
            //                    { new IntermittentTimeRequestDetail()
            //                        { Approved = 480, PolicyCode = fmla.Policy.Code }
            //                    }},
            //                Notes = "my notes 4"
            //            },
            //            new IntermittentTimeRequest()
            //            {
            //                RequestDate = new DateTime(2014, 3, 12, 0, 0, 0, DateTimeKind.Utc),
            //                TotalMinutes = 480,
            //                Detail = new List<IntermittentTimeRequestDetail>() {
            //                    { new IntermittentTimeRequestDetail()
            //                        { Approved = 480, PolicyCode = fmla.Policy.Code }
            //                    }},
            //                Notes = ""
            //            },
            //            new IntermittentTimeRequest()
            //            {
            //                RequestDate = new DateTime(2014, 4, 8, 0, 0, 0, DateTimeKind.Utc),
            //                TotalMinutes = 480,
            //                Detail = new List<IntermittentTimeRequestDetail>() {
            //                    { new IntermittentTimeRequestDetail()
            //                        { Approved = 480, PolicyCode = fmla.Policy.Code }
            //                    }},
            //                Notes = "This will force the 2015 leave to exhaust on 3-30-2015 instead of 3-31-2015 because it will not be earned back before the end of the case"
            //            }

            //};

            //intCase.Segments.SelectMany(s => s.AppliedPolicies).ForEach(ap =>
            //{
            //    cs.ApplyDetermination(intCase, ap.Policy.Code, intStartDate, intEndDate, AdjudicationStatus.Approved);
            //});

            //// create a time off request
            //cs.CreateOrModifyTimeOffRequest(intCase, itr);
            //cs.GetProjectedUsage(intCase);

            //intCase.Save();

            //// now try to close the case with 3/10 pending. it should not allow it
            //Exception execpt = null;
            //try
            //{
            //    cs.CaseClosed(intCase, new DateTime(2014, 5, 1, 0, 0, 0, DateTimeKind.Utc), CaseClosureReason.ReturnToWork);
            //}
            //catch (Exception e)
            //{
            //    execpt = e;
            //}
            //Assert.IsNotNull(execpt);

            // clean out the other tests (if the bombed)
            deleteTestCases(empId);

        }

        [TestMethod,Ignore]
        public void OverApprovePregTest()
        {

            var testPolicy = Policy.GetByCode("CA-CPDL");
            testPolicy.PolicyBehaviorType = PolicyBehavior.ReducesWorkWeek;
            testPolicy.Save();


            string empId = "000000000000000000000018";
            string caseCode = "PREGMAT";
            deleteTestCases(empId);
            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).FirstOrDefault();
            Assert.IsNotNull(reason);

            DateTime startDate = new DateTime(2015, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime endDate = new DateTime(2015, 9, 1, 0, 0, 0, DateTimeKind.Utc);

            CaseService cs = new CaseService();
            Case newCase = cs.CreateCase(CaseStatus.Open, empId, startDate, endDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(newCase);

            AppliedPolicy cacpdl = newCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == "CA-CPDL");
            Assert.IsNotNull(cacpdl);

            // we only want to test ca-cpdl so get rid of the others
            newCase.Segments[0].AppliedPolicies = newCase.Segments[0].AppliedPolicies.Where(fd => fd.Policy.Code == "CA-CPDL").ToList();
            Assert.AreEqual(newCase.Segments[0].AppliedPolicies.Count, 1);

            DateTime approveStart = new DateTime(2015, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime approveEnd = new DateTime(2015, 6, 1, 0, 0, 0, DateTimeKind.Utc);

            cs.ApplyDetermination(newCase, cacpdl.Policy.Code, approveStart, approveEnd, AdjudicationStatus.Approved);

            cs.GetProjectedUsage(newCase);

            //cacpdl = newCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == "CA-CPDL");
            // check the summary there should be two rows, the approve one ends on 5/05 and the denied starts on 5/6
            Assert.AreEqual(newCase.Summary.Policies.Count, 1);
            Assert.AreEqual(newCase.Summary.Policies[0].Detail.Count, 2);
            Assert.AreEqual(newCase.Summary.Policies[0].Detail[0].EndDate, new DateTime(2015, 4, 30, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(newCase.Summary.Policies[0].Detail[0].Determination, AdjudicationSummaryStatus.Approved);

            Assert.AreEqual(newCase.Summary.Policies[0].Detail[1].StartDate, new DateTime(2015, 5, 1, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(newCase.Summary.Policies[0].Detail[1].Determination, AdjudicationSummaryStatus.Denied);

            // clean out the other tests (if the bombed)
            deleteTestCases(empId);


        }

        /// <summary>
        /// test 
        /// </summary>
        [TestMethod,Ignore]
        public void TORsInvalidTest()
        {
            string empId = "000000000000000000000075";
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            // clean out the other tests (if the bombed)
            deleteTestCases(empId);

            CaseService cs = new CaseService();

            DateTime intStartDate = new DateTime(2014, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime intEndDate = new DateTime(2014, 12, 31, 0, 0, 0, DateTimeKind.Utc);

            // exhaust the case
            Case intCase = cs.CreateCase(CaseStatus.Open, empId, intStartDate, intEndDate, AbsenceSoft.Data.Enums.CaseType.Intermittent, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(intCase);

            intCase.Segments[0].AppliedPolicies.RemoveAll(p => p.Policy.Code == "FMLA-FC"); // ensure we're only dealing with FMLA  
            intCase = cs.RunCalcs(intCase);
            intCase = cs.UpdateCase(intCase, CaseEventType.CaseCreated);
            // approve the leave for time off
            intCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).Where(p => p.Policy.Code == "FMLA").ToList()
                        .ForEach(ap => cs.ApplyDetermination(intCase, ap.Policy.Code, intStartDate, intEndDate, AdjudicationStatus.Approved));

            cs.GetProjectedUsage(intCase);

            // get the fmla policy
            Assert.IsTrue(intCase.Segments.Count > 0);
            Assert.IsNotNull(intCase.Segments[0].AppliedPolicies);
            Assert.IsTrue(intCase.Segments[0].AppliedPolicies.Count > 0);

            AppliedPolicy fmla = intCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == "FMLA");

            // approve it for time off
            intCase.Segments.SelectMany(s => s.AppliedPolicies).ForEach(ap =>
            {
                cs.ApplyDetermination(intCase, ap.Policy.Code, intStartDate, intEndDate, AdjudicationStatus.Approved);
            });

            // create a valid time off request to make sure everything is working
            List<IntermittentTimeRequest> itr = new List<IntermittentTimeRequest>() {
                        new IntermittentTimeRequest()
                        {
                            RequestDate = new DateTime(2014, 1, 9, 0, 0, 0, DateTimeKind.Utc),
                            TotalMinutes = 480,
                            Detail = new List<IntermittentTimeRequestDetail>() {
                                { new IntermittentTimeRequestDetail()
                                    { Approved=480, PolicyCode = fmla.Policy.Code }
                                }},
                            Notes = "should work"
                        }
            };

            cs.CreateOrModifyTimeOffRequest(intCase, itr);

            // yeah, that worked, now let's do one on a holidy
            itr = new List<IntermittentTimeRequest>() {
                        new IntermittentTimeRequest()
                        {
                            RequestDate = new DateTime(2014, 1, 1, 0, 0, 0, DateTimeKind.Utc),
                            TotalMinutes = 480,
                            Detail = new List<IntermittentTimeRequestDetail>() {
                                { new IntermittentTimeRequestDetail()
                                    { Approved=480, PolicyCode = fmla.Policy.Code }
                                }},
                            Notes = "Holiday go boom!"
                        }
            };

            Exception excep = null;
            try
            {
                // create a time off request
                cs.CreateOrModifyTimeOffRequest(intCase, itr);
            }
            catch (Exception e)
            {
                excep = e;
            }

            Assert.IsNotNull(excep);

            // now let's try a Sunday cuz this person does not work on a Sunday
            excep = null;
            itr = new List<IntermittentTimeRequest>() {
                        new IntermittentTimeRequest()
                        {
                            RequestDate = new DateTime(2014, 3, 9, 0, 0, 0, DateTimeKind.Utc),
                            TotalMinutes = 480,
                            Detail = new List<IntermittentTimeRequestDetail>() {
                                { new IntermittentTimeRequestDetail()
                                    { Approved=480, PolicyCode = fmla.Policy.Code }
                                }},
                            Notes = "Sunday is fun day"
                        }
            };
            try
            {
                // create a time off request
                cs.CreateOrModifyTimeOffRequest(intCase, itr);
            }
            catch (Exception e)
            {
                excep = e;
            }

            Assert.IsNotNull(excep);

            // now mess with the dude's schedule and set it to start a couple years in the future
            intCase.Employee.WorkSchedules[0].StartDate = new DateTime(2015, 4, 7, 0, 0, 0, DateTimeKind.Utc);
            intCase.Employee.HireDate = intCase.Employee.WorkSchedules[0].StartDate;
            intCase.Employee.ServiceDate = intCase.Employee.WorkSchedules[0].StartDate;
            intCase.Employee.Save();

            // and try the day that previously worked and should continue to work because the case dates and approved policy
            //  are still there and we have to take what the customer gives us no matter what.
            itr = new List<IntermittentTimeRequest>() {
                        new IntermittentTimeRequest()
                        {
                            RequestDate = new DateTime(2014, 1, 9, 0, 0, 0, DateTimeKind.Utc),
                            TotalMinutes = 480,
                            Detail = new List<IntermittentTimeRequestDetail>() {
                                { new IntermittentTimeRequestDetail()
                                    { Approved=480, PolicyCode = fmla.Policy.Code }
                                }},
                            Notes = "should work"
                        }
            };

            excep = null;
            try
            {
                // create a time off request
                cs.CreateOrModifyTimeOffRequest(intCase, itr);
            }
            catch (Exception e)
            {
                excep = e;
            }

            // This should 
            Assert.IsNull(excep, excep?.ToString());
        }

        [TestMethod]
        public void CaseApproveChangeScheduleCalc()
        {
            /* so if the method name sounds confusing here is what we are testing for:
             * On a consecutive case create it, save it, approve the time
             * then change the schedule in the middle of the case and without
             * approving the time run the calcs. If a full day was approved before
             * then the new full day should still be approved.
           */

            string empId = "000000000000000000000003";
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            CaseService cs = new CaseService();

            DateTime startDate = new DateTime(2015, 3, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime endDate = new DateTime(2015, 5, 5, 0, 0, 0, DateTimeKind.Utc);

            Case newCase = cs.CreateCase(CaseStatus.Open, empId, startDate, endDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(newCase);

            // make sure there is an fmla policy
            AppliedPolicy fmla = newCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
            Assert.IsNotNull(fmla, "FMLA Policy not found");

            // get rid of eveything but the fmla policy. we only need one to test and it'll make life easier
            Assert.AreEqual(newCase.Segments.Count, 1);
            newCase.Segments[0].AppliedPolicies = new List<AppliedPolicy>() { fmla };

            // this runs the calc too
            cs.ApplyDetermination(newCase, fmla.Policy.Code, startDate, endDate, AdjudicationStatus.Approved);

            // now check the policy summary and make sure there is one run
            Assert.AreEqual(newCase.Summary.Policies.Count, 1);
            Assert.AreEqual(newCase.Summary.Policies[0].Detail.Count, 1);
            Assert.AreEqual(newCase.Summary.Policies[0].Detail[0].StartDate, startDate);
            Assert.AreEqual(newCase.Summary.Policies[0].Detail[0].EndDate, endDate);

            // we're lazy, clone the current schedule
            Schedule newSched = newCase.Employee.WorkSchedules[0].Clone();
            Schedule backupSched = newCase.Employee.WorkSchedules[0].Clone();

            // cut the schedule in half, end the previous schedule and add this one
            newSched.StartDate = startDate.AddDays(18);
            newSched.Times.ForEach(t => t.TotalMinutes = t.TotalMinutes == 0 ? 0 : t.TotalMinutes / 2);
            newCase.Employee.WorkSchedules[0].EndDate = startDate.AddDays(17);
            newCase.Employee.WorkSchedules.Add(newSched);

            newCase.Employee.Save();


            // now... run the calcs and the summary should be the same
            cs.RunCalcs(newCase);
            Assert.AreEqual(newCase.Summary.Policies.Count, 1);
            Assert.AreEqual(newCase.Summary.Policies[0].Detail.Count, 1);
            Assert.AreEqual(newCase.Summary.Policies[0].Detail[0].StartDate, startDate);
            Assert.AreEqual(newCase.Summary.Policies[0].Detail[0].EndDate, endDate);

            // put back the schedule so we don't mess up other tests
            newCase.Employee.WorkSchedules = new List<Schedule>() { backupSched };
            newCase.Employee.Save();

            deleteTestCases(empId);

        }

#warning HACK HACK HACK, Temporary disable unit test to account for policy changes
        [TestMethod, Ignore]
        public void CalendarYearEntitlementTest()
        {
            /* use WI-FML. This leave entitles a family member two weeks
             * per calendar year. This test will use up the two weeks of leave 
             * in may of one year, and then in January of next year create
             * and approve another leave
             */

            // specieal WI emp, just for this test
            string empId = "000000000000000000000366";
            string caseCode = "FHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            // clean up, in case of resuse
            deleteTestCases(empId);

            Employee theEmp = Employee.GetById(empId);

            // now the employee needs a contact, create a child that we can use for this case
            EmployeeContact theKid = new EmployeeContact()
            {
                Contact = new Contact()
                {
                    Address = new Address()
                    {
                        Country = "US"
                    },
                    FirstName = "WI",
                    LastName = "TestKid"
                },
                ContactTypeCode = "CHILD",
                ContactTypeName = "Child",
                YearsOfAge = 5,
                MilitaryStatus = MilitaryStatus.Civilian,
                EmployeeId = empId,
                EmployerId = theEmp.EmployerId,
                CustomerId = theEmp.CustomerId
            };

            theKid.Save();

            CaseService cs = new CaseService();

            // set up a 3 week leave, the 3rd week will be denied exhuasted
            DateTime firstStartDate = new DateTime(2014, 3, 2, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2014, 3, 22, 0, 0, 0, DateTimeKind.Utc);

            Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
            firstCase.Contact = theKid;

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(firstCase);

            AppliedPolicy wifFmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "WI-FML");
            wifFmla.Status = EligibilityStatus.Eligible;
            if (wifFmla == null)
            {
                esvc.AddManualPolicy(firstCase, "WI-FML", "Dude, add it");
                loa = esvc.RunEligibility(firstCase);
                wifFmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "WI-FML");
            }
            Assert.IsNotNull(wifFmla, "WI-FML Policy not found");

            // approve the leave then check the results
            cs.ApplyDetermination(firstCase, wifFmla.Policy.Code, firstStartDate, firstEndDate, AdjudicationStatus.Approved);

            // find the policy again
            wifFmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "WI-FML");
            Assert.IsNotNull(wifFmla, "WI-FML Policy not found");

            // check that the first day is approved
            AppliedPolicyUsage firstDay = wifFmla.Usage.FirstOrDefault(fd => fd.DateUsed == firstStartDate);
            Assert.IsNotNull(firstDay);
            Assert.AreEqual(firstDay.Determination, AdjudicationStatus.Approved);

            // the last day of the two weeks is the 14th, so even though there are no
            // work hours the 15th should be the first exhausted day
            DateTime firstExhaustedDate = new DateTime(2014, 3, 15, 0, 0, 0, DateTimeKind.Utc);
            AppliedPolicyUsage fed = wifFmla.Usage.FirstOrDefault(fd => fd.DateUsed == firstExhaustedDate);

            Assert.IsNotNull(fed);
            Assert.AreEqual(fed.Determination, AdjudicationStatus.Denied);
            Assert.IsTrue(string.IsNullOrWhiteSpace(fed.DenialReasonCode));
            Assert.AreEqual(fed.DenialReasonCode, AdjudicationDenialReason.Exhausted);

            // save the case and create the next one
            firstCase.Save();

            // because this case occures in the next calendar year it should be approved
            DateTime secondStartDate = new DateTime(2015, 1, 11, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondEndDate = new DateTime(2015, 1, 24, 0, 0, 0, DateTimeKind.Utc);

            Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondStartDate, secondEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
            secondCase.Contact = theKid;

            LeaveOfAbsence loaSecond = esvc.RunEligibility(secondCase);

            wifFmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "WI-FML");
            if (wifFmla == null)
            {
                esvc.AddManualPolicy(secondCase, "WI-FML", "Dude, add it");
                loaSecond = esvc.RunEligibility(secondCase);
                wifFmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "WI-FML");
            }
            Assert.IsNotNull(wifFmla, "WI-FML Policy not found");

            // make the rest of this easier to test and remove all but the WI policy
            //secondCase.Segments[0].AppliedPolicies = new List<AppliedPolicy>() { wifFmla }; (ok, really it's easier to debug if you uncomment this line)

            cs.ApplyDetermination(secondCase, wifFmla.Policy.Code, secondStartDate, secondEndDate, AdjudicationStatus.Approved);

            // just check the first day 
            firstDay = wifFmla.Usage.FirstOrDefault(fd => fd.DateUsed == secondStartDate);
            Assert.IsNotNull(firstDay);
            Assert.AreEqual(firstDay.Determination, AdjudicationStatus.Approved);

            // now... wipe out the the first case (never save the second) and create
            // a case that is 4 weeks long, starts the last week of the year goes three weeks
            // into the new year. The three weeks of the leave should be approved and the last week
            // should be denied 
            deleteTestCases(empId);


            DateTime thirdStartDate = new DateTime(2014, 12, 21, 0, 0, 0, DateTimeKind.Utc);
            DateTime thirdEndDate = new DateTime(2015, 1, 24, 0, 0, 0, DateTimeKind.Utc);

            Case thirdCase = cs.CreateCase(CaseStatus.Open, empId, thirdStartDate, thirdEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
            thirdCase.Contact = theKid;

            LeaveOfAbsence loaThird = esvc.RunEligibility(thirdCase);

            wifFmla = thirdCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "WI-FML");
            if (wifFmla == null)
            {
                esvc.AddManualPolicy(thirdCase, "WI-FML", "Dude, add it");
                loaThird = esvc.RunEligibility(thirdCase);
                wifFmla = thirdCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "WI-FML");
            }
            Assert.IsNotNull(wifFmla, "WI-FML Policy not found");

            // make the rest of this easier to test and remove all but the WI policy
            //thirdCase.Segments[0].AppliedPolicies = new List<AppliedPolicy>() { wifFmla }; (ok, really it's easier to debug if you uncomment this line)

            cs.ApplyDetermination(thirdCase, wifFmla.Policy.Code, thirdStartDate, thirdEndDate, AdjudicationStatus.Approved);

            // just check the first day 
            firstDay = wifFmla.Usage.FirstOrDefault(fd => fd.DateUsed == thirdStartDate);
            Assert.IsNotNull(firstDay);
            Assert.AreEqual(firstDay.Determination, AdjudicationStatus.Approved);

            // first work day of the new year
            firstDay = wifFmla.Usage.FirstOrDefault(fd => fd.DateUsed == new DateTime(2015, 1, 2, 0, 0, 0, DateTimeKind.Utc));
            Assert.IsNotNull(firstDay);
            Assert.AreEqual(firstDay.Determination, AdjudicationStatus.Approved);

            // since 1/1 is a holiday 1/2 is the first work day. this makes two calendar weeks end on the 16th
            Assert.AreEqual(wifFmla.FirstExhaustionDate, new DateTime(2015, 1, 16, 0, 0, 0, DateTimeKind.Utc));

            // clean up for the next test
            deleteTestCases(empId);


        }



        [TestMethod]
        public void AT648VariableScheduleNoTimeDetermiationWithZeroUsageTest()
        {
            // specieal WI emp, just for this test
            string empId = "000000000000000000000366";
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            // clean up, in case of resuse
            deleteTestCases(empId);

            Employee theEmp = Employee.GetById(empId);
            var sched = theEmp.WorkSchedules.First();
            sched.ScheduleType = ScheduleType.Variable;
            theEmp.Save();

            VariableScheduleTime.Delete(VariableScheduleTime.Query.EQ(v => v.EmployeeId, empId));
            new VariableScheduleTime()
            {
                EmployeeId = empId,
                CustomerId = theEmp.CustomerId,
                EmployerId = theEmp.EmployerId,
                Time = new Time()
                {
                    SampleDate = new DateTime(2015, 12, 20, 0, 0, 0, 0, DateTimeKind.Utc),
                    TotalMinutes = 480
                }
            }.Save();

            CaseService cs = new CaseService();

            // set up a 3 week leave, the 3rd week will be denied exhuasted
            DateTime firstStartDate = new DateTime(2014, 3, 2, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2014, 3, 22, 0, 0, 0, DateTimeKind.Utc);

            Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(firstCase);

            AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
            Assert.IsNotNull(fmla, "FMLA Policy not found");
            fmla.Status = EligibilityStatus.Eligible;

            // approve the leave then check the results
            cs.ApplyDetermination(firstCase, fmla.Policy.Code, firstStartDate, firstEndDate, AdjudicationStatus.Approved);

            // find the policy again
            fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
            Assert.IsNotNull(fmla, "FMLA Policy not found");

            // check that we've calculated usage for the leave for this policy
            Assert.IsTrue(fmla.Usage.Any());
            //AppliedPolicyUsage firstDay = fmla.Usage.FirstOrDefault(fd => fd.DateUsed == firstStartDate);
            //Assert.IsNotNull(firstDay);
            //Assert.AreEqual(AdjudicationStatus.Denied, firstDay.Determination);
            //Assert.AreEqual(AdjudicationDenialReason.EliminationPeriod, firstDay.DenialReason);

            // clean up for the next test
            deleteTestCases(empId);
        }

        [TestMethod]
        public void DissallowCertificationOverlap()
        {
            var @case = new Case
            {
                Certifications = new List<Certification>
                {
                    new Certification
                    {
                        StartDate = DateTime.Parse("2000-01-01"),
                        EndDate = DateTime.Parse("2000-01-02")
                    }
                }
            };

            using (var service = new CaseService())
            {
                var newCert = new Certification
                {
                    StartDate = DateTime.Parse("1999-12-31"),
                    EndDate = DateTime.Parse("2000-01-04")
                };

                Action action = () => @case = service.CreateOrModifyCertification(@case, newCert, true);
                action.ShouldThrow<AbsenceSoftException>();
            }

            using (var service = new CaseService())
            {
                var newCert = new Certification
                {
                    StartDate = DateTime.Parse("2000-02-01"),
                    EndDate = DateTime.Parse("2000-02-02")
                };

                Action action = () => @case = service.CreateOrModifyCertification(@case, newCert, true);
                action.ShouldNotThrow<AbsenceSoftException>();
            }
        }

        [TestMethod]
        public void TestDuplicatePrevention()
        {
            using (CaseService svc = new CaseService())
            {
                var emp = Employee.AsQueryable().Where(e => e.EmployeeNumber == "demo_001").FirstOrDefault();
                Assert.IsNotNull(emp);
                var reasons = svc.GetAbsenceReasons(emp.Id);
                Assert.IsNotNull(reasons);
                Assert.IsTrue(reasons.Any());
                var r = reasons.First();

                Case.AsQueryable().Where(c => c.Employee.Id == emp.Id && c.Reason.Code == r.Code).ForEach(a => a.Delete());

                var newCase = svc.CreateCase(CaseStatus.Open, emp.Id, DateTime.Today, DateTime.Today.AddDays(15), AbsenceSoft.Data.Enums.CaseType.Administrative, r.Code, description: "CaseServiceTest.CreateCase");
                var copyCat = newCase.Clone();
                newCase = svc.UpdateCase(newCase);
                copyCat = svc.UpdateCase(copyCat);
                copyCat.Id.Should().Be(newCase.Id);
            }
        }



        [TestMethod]
        public void TestCaseAssignedOfficeLocation()
        {
            string officeLocationRule_Val = "Test_rule";
            Case myCase = new Case()
            {
                EmployerId = "000000000000000000000002",
                CustomerId = "000000000000000000000002",
                Employee = new Employee()
                {
                    Info = new EmployeeInfo()
                    {
                        OfficeLocation = officeLocationRule_Val
                    }
                },
                CaseNumber = "ACCOMTEST4",
                AssignedToId = "000000000000000000000000"
            };
            CaseAssignmentRule myCaseAssignmentRule = new CaseAssignmentRule()

            {
                Name = "testName",
                Description = "testDescription",
                RuleType = "11",
                RuleValues = new string[] { officeLocationRule_Val },
                CaseAssignmentType = CaseAssigmentType.TeamMember,
                CaseAssignmentId = "00000000000000007"
            };
            List<User> lstuser = new List<User>();
            User user1 = new User()
            {
                Id = "00000000000000007",
                Email = "test@test.com",
                FirstName = "Test1",
                LastName = "User1",
                CustomerId = "000000000000000000000002"

            };
            lstuser.Add(user1);
            User user2 = new User()
            {
                Id = "00000000000000006",
                Email = "test@test2.com",
                FirstName = "Test2",
                LastName = "User2",
                CustomerId = "000000000000000000000002"
            };
            lstuser.Add(user2);
            User user3 = new User()
            {
                Id = "00000000000000005",
                Email = "test@test3.com",
                FirstName = "Test3",
                LastName = "User3",
                CustomerId = "000000000000000000000002"
            };
            lstuser.Add(user3);

            OfficeLocationRule officeLocationRule = new OfficeLocationRule();
            officeLocationRule.ApplyCaseAssignmentRules(ref lstuser, myCase, myCaseAssignmentRule);
            Assert.IsTrue(lstuser.Contains(user1));
        }
        [TestMethod]
        public void TestCaseAssignedOfficeLocationNotAssigned()
        {
            Case myCase = new Case()
            {
                EmployerId = "000000000000000000000002",
                CustomerId = "000000000000000000000002",
                Employee = new Employee()
                {
                    Info = new EmployeeInfo()
                    {
                        OfficeLocation = "Test_rule"
                    }
                },
                CaseNumber = "ACCOMTEST4",
                AssignedToId = "000000000000000000000000"
            };
            CaseAssignmentRule myCaseAssignmentRule = new CaseAssignmentRule()

            {
                Name = "testName",
                Description = "testDescription",
                RuleType = "11",
                RuleValues = new string[] { "Test_rule" },
                CaseAssignmentType = CaseAssigmentType.TeamMember,
                CaseAssignmentId = "00000000000000007"

            };

            List<User> lstuser = new List<User>();
            User user1 = new User()
            {
                Id = "00000000000000007",
                Email = "test@test.com",
                FirstName = "Test1",
                LastName = "User1",
                CustomerId = "000000000000000000000002"

            };
            lstuser.Add(user1);
            User user2 = new User()
            {
                Id = "00000000000000005",
                Email = "test@test2.com",
                FirstName = "Test2",
                LastName = "User2",
                CustomerId = "000000000000000000000002"
            };
            lstuser.Add(user2);

            OfficeLocationRule officeLocationRule = new OfficeLocationRule();
            officeLocationRule.ApplyCaseAssignmentRules(ref lstuser, myCase, myCaseAssignmentRule);
            Assert.IsFalse(lstuser.Contains(user2));

        }

        private Case CreateCaseForPriorUsage()
        {
            return new Case()
            {
                Employee = new Employee(),
                EmployerId = "000000000000000000000002",
            };
        }

        [TestMethod]
        public void PriorUsagePerOccurrenceReturnsFalseWithNotPerOccurrenceReasonAppliedPolicy()
        {
            string customerId = "000000000000000000000002";
            string employerId = "000000000000000000000002";

            Employer employer = Employer.GetById("000000000000000000000002");
           
            Employee testEmployee = new Employee()
            {
                EmployeeNumber = "20120202-1",
                CustomerId = customerId,
                EmployerId = employerId,

                FirstName = "Test",
                LastName = "Empp 111",
                IsExempt = false,
                IsKeyEmployee = false,
                WorkCountry = "US",
                WorkState = "CA",                
                Meets50In75MileRule = false,
                PriorHours = new List<PriorHours>()
                {
                    new PriorHours() {HoursWorked=2016,
                        AsOf = new DateTime(2012,11,21)}
                },
                ServiceDate = new DateTime(2009, 02, 02),

                WorkSchedules = new List<Schedule>() {
                    new Schedule() { StartDate = new DateTime(2010, 03, 02),
                        ScheduleType = ScheduleType.Weekly,
                        Times = TestWorkSchedules.MakeSchedule(new DateTime(2010, 03, 02),40)}
                }
            };
            testEmployee.Employer = employer;
            testEmployee.Save();
            var theCase = CreateCaseForPriorUsage();
            theCase.Employee = testEmployee;
            theCase.StartDate = DateTime.Now.AddDays(-10);
            theCase.EndDate = DateTime.Now;

            theCase.Segments.Add(new CaseSegment()
            {
                Status = CaseStatus.Open,
                AppliedPolicies = new List<AppliedPolicy>()
                {
                    new AppliedPolicy()
                    {
                        Status = EligibilityStatus.Eligible,
                        Policy = new Policy()
                        {
                            Code = "FMLA"
                        },
                        PolicyReason = new PolicyAbsenceReason()
                        {
                            PeriodType = PeriodType.CalendarYear
                        }
                    }
                }
            });
            var priorUsage = new CalculatePriorUsage(theCase, DateTime.Now, false, false, false, null);
            Assert.IsFalse(priorUsage.WorkingCaseHasAnyPerOccurrencePolicies());

            testEmployee.Delete();
        }

        [TestMethod]
        public void PriorUsagePerOccurrenceReturnsTrueWithPerOccurrenceAppliedPolicyPeriodType()
        {
            string customerId = "000000000000000000000002";
            string employerId = "000000000000000000000002";

            Employer employer = Employer.GetById("000000000000000000000002");
            
            Employee testEmployee = new Employee()
            {
                EmployeeNumber = "20120202-2",
                CustomerId = customerId,
                EmployerId = employerId,

                FirstName = "Test",
                LastName = "Empp 111",
                IsExempt = false,
                IsKeyEmployee = false,
                WorkCountry = "US",
                WorkState = "CA",
                Meets50In75MileRule = false,
                PriorHours = new List<PriorHours>()
                {
                    new PriorHours() {HoursWorked=2016,
                        AsOf = new DateTime(2012,11,21)}
                },
                ServiceDate = new DateTime(2009, 02, 02),

                WorkSchedules = new List<Schedule>() {
                    new Schedule() { StartDate = new DateTime(2010, 03, 02),
                        ScheduleType = ScheduleType.Weekly,
                        Times = TestWorkSchedules.MakeSchedule(new DateTime(2010, 03, 02),40)}
                }
            };
            testEmployee.Employer = employer;
            testEmployee.Save();

            var theCase = CreateCaseForPriorUsage();
            theCase.Employee = testEmployee;
            theCase.StartDate = DateTime.Now.AddDays(-10);
            theCase.EndDate = DateTime.Now;

            theCase.Segments.Add(new CaseSegment()
            {
                Status = CaseStatus.Open,
                AppliedPolicies = new List<AppliedPolicy>()
                {
                    new AppliedPolicy()
                    {
                        Status = EligibilityStatus.Eligible,
                        Policy = new Policy()
                        {
                            Code = "FMLA"
                        },
                        PolicyReason = new PolicyAbsenceReason()
                        {
                            PeriodType = PeriodType.PerOccurrence,                            
                        }
                    }
                }
            });
            var priorUsage = new CalculatePriorUsage(theCase, DateTime.Now, false, false, false, null);
            Assert.IsTrue(priorUsage.WorkingCaseHasAnyPerOccurrencePolicies());

            testEmployee.Delete();
        }

        /// <summary>
        /// Test Time Available in hours in 75 work week policy summary
        /// </summary>
        [TestMethod]
        public void TestTimeAvailableInHoursIn75WorkWeekPolicySummary()
        {

            string policyCode = "75WW";
            string employeeId = "000000000000000000000003";
            Employee employee = Employee.GetById(employeeId);
            var testPolicy = new Policy()
            {
                Name = "75 work week test",
                Code = policyCode,
                AbsenceReasons = new List<PolicyAbsenceReason>()
                {
                    new PolicyAbsenceReason()
                    {
                        Reason = Reasons.EmployeeHealthCondition,
                        CaseTypes = CaseType.Consecutive ,
                        EntitlementType = EntitlementType.WorkWeeks,
                        Entitlement = 75
                    }
                }
            };

            testPolicy.Save();

            using (CaseService svc = new CaseService())
            using (EligibilityService eligSvc = new EligibilityService())
            {

                DateTime startDate = new DateTime(2017, 2, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime endDate = new DateTime(2017, 2, 28, 0, 0, 0, DateTimeKind.Utc);

                string reasonCode = AbsenceReason.GetByCode("EHC").Code;

                var newCase = svc.CreateCase(CaseStatus.Open, employee.Id, startDate, endDate, CaseType.Consecutive, reasonCode);

                eligSvc.RunEligibility(newCase);
                // get the employee policy summary
                var policy75WWSummary = svc.GetEmployeePolicySummary(newCase.Employee, new DateTime(2017, 11, 1).ToMidnight(), newCase, true, new string[] { policyCode }).FirstOrDefault(summary => summary.PolicyCode == policyCode);

                Assert.AreEqual(policy75WWSummary.HoursRemaining, "3000 hours");
            }

        }

        [TestMethod]
        public void FMLAOverrideHolidayIgnoredNoLostTime()
        {
            var testPolicy = Policy.GetByCode("FMLA");
            testPolicy.PolicyBehaviorType = PolicyBehavior.IgnoredNoLostTime;
            testPolicy.Save();

            string empId = "000000000000000000000003";
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            CaseService cs = new CaseService();
            // make the case 13 weeks long
            Case newCase = cs.CreateCase(CaseStatus.Open, empId, new DateTime(2017, 1, 1, 0, 0, 0, DateTimeKind.Utc), new DateTime(2017, 1, 20, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(newCase);

            // make sure there is an fmla policy
            AppliedPolicy fmla = newCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");


            Assert.IsNotNull(fmla, "FMLA Policy not found");

            Assert.AreEqual(20, fmla.Usage.Count);

            double minsUsed = fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.MinutesUsed);
            double daysUsed = Math.Round(fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.PercentOfDay), 4);
            double weeksUsed = Math.Round(fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.DaysUsage(EntitlementType.WorkWeeks)), 4);

            Assert.IsTrue(minsUsed == 6240);
            Assert.IsTrue(daysUsed == 13);
            Assert.IsTrue(weeksUsed == 2.6);

            testPolicy = Policy.GetByCode("FMLA");
            testPolicy.PolicyBehaviorType = PolicyBehavior.ReducesWorkWeek;
            testPolicy.Save();

        }


        [TestMethod]
        public void FMLAOverrideHolidayIgnored()
        {
            var testPolicy = Policy.GetByCode("FMLA");
            testPolicy.PolicyBehaviorType = PolicyBehavior.Ignored;
            testPolicy.Save();

            string empId = "000000000000000000000003";
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            CaseService cs = new CaseService();
            // make the case 13 weeks long
            Case newCase = cs.CreateCase(CaseStatus.Open, empId, new DateTime(2017, 1, 1, 0, 0, 0, DateTimeKind.Utc), new DateTime(2017, 1, 20, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(newCase);

            // make sure there is an fmla policy
            AppliedPolicy fmla = newCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");


            Assert.IsNotNull(fmla, "FMLA Policy not found");

            Assert.AreEqual(20, fmla.Usage.Count);

            double minsUsed = fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.MinutesUsed);
            double daysUsed = Math.Round(fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.PercentOfDay));
            double weeksUsed = Math.Round(fmla.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.DaysUsage(EntitlementType.WorkWeeks)));

            Assert.IsTrue(minsUsed == 7200);
            Assert.IsTrue(daysUsed == 15);
            Assert.IsTrue(weeksUsed == 3);

            testPolicy = Policy.GetByCode("FMLA");
            testPolicy.PolicyBehaviorType = PolicyBehavior.ReducesWorkWeek;
            testPolicy.Save();

        }

        [TestMethod]
        public void FMLAOverrideHolidayReducesWorkWeek()
        {
            var testPolicy = Policy.GetByCode("FMLA");
            testPolicy.PolicyBehaviorType = PolicyBehavior.ReducesWorkWeek;
            testPolicy.Save();

            string empId = "000000000000000000000003";
            string absenceReasonCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == absenceReasonCode).First();

            deleteTestCases(empId);

            using (CaseService caseService = new CaseService())
            using (EligibilityService eligibilityService = new EligibilityService())
            {
                Case newCase = caseService.CreateCase(CaseStatus.Open, empId, new DateTime(2017, 05, 01, 0, 0, 0, DateTimeKind.Utc), new DateTime(2017, 09, 15, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                LeaveOfAbsence loa = eligibilityService.RunEligibility(newCase);

                AppliedPolicy fmla = newCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
                Assert.IsNotNull(fmla, "FMLA Policy not found");
                Assert.AreNotEqual(fmla.Usage.FirstOrDefault(usage => usage.DateUsed == new DateTime(2017, 05, 29, 0, 0, 0, DateTimeKind.Utc)).Determination, AdjudicationStatus.Denied);
                Assert.AreNotEqual(fmla.Usage.FirstOrDefault(usage => usage.DateUsed == new DateTime(2017, 07, 04, 0, 0, 0, DateTimeKind.Utc)).Determination, AdjudicationStatus.Denied);
            }
        }
        [TestMethod]
        public void TestKeyEmployeeFMLAMessage()
        {
            string employeeId = "000000000000000000000003";
            string absenceReasonCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == absenceReasonCode).First();

            deleteTestCases(employeeId);

            CaseService cs = new CaseService();
            Case newCase = cs.CreateCase(CaseStatus.Open, employeeId, new DateTime(2017, 1, 1, 0, 0, 0, DateTimeKind.Utc), new DateTime(2017, 1, 20, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

            EligibilityService esvc = new EligibilityService();
            newCase = esvc.RunEligibility(newCase).Case;
            CaseSummaryPolicy fmlaPolicySummary = newCase.Summary.Policies.FirstOrDefault(p => p.PolicyCode == "FMLA");
            Assert.IsTrue(fmlaPolicySummary.Messages.Count == 0);
            newCase.Employee.IsKeyEmployee = true;
            cs.RunCalcs(newCase);
            fmlaPolicySummary = newCase.Summary.Policies.FirstOrDefault(p => p.PolicyCode == "FMLA");
            Assert.IsTrue(fmlaPolicySummary.Messages.Count > 0);
            Assert.AreEqual("This employee is designated as a Key Employee. Under FMLA regulation, Key Employees are not required to be reinstated to a same or similar position", fmlaPolicySummary.Messages[0]);
        }

        [TestMethod]
        public void TestFMLACeremonialReason()
        {
            var policy = Policy.GetByCode("FMLA");
            Assert.IsNotNull(policy);

            CaseService caseService = new CaseService();
            var employee = Employee.GetById("000000000000000000000002");
            deleteTestCases("000000000000000000000002");
            DateTime caseStartDate = new DateTime(2017, 10, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime caseEndDate = new DateTime(2017, 12, 23, 0, 0, 0, DateTimeKind.Utc);
            var reasonCode = AbsenceReason.GetByCode("CEREMONY").Code;
            Case _case = caseService.CreateCase(CaseStatus.Open, employee.Id, caseStartDate, caseEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reasonCode);

            _case.Contact = new EmployeeContact()
            {
                ContactTypeCode = "CHILD",
                ContactTypeName = "Child",
                CustomerId = _case.CustomerId,
                EmployeeId = _case.Employee.Id,
                YearsOfAge = 5,
                MilitaryStatus = MilitaryStatus.ActiveDuty,
                Contact = new Contact()
                {
                    FirstName = "Thomas",
                    LastName = "Johny",
                    Title = "Bratt"
                }
            };

            EligibilityService eligibilityService = new EligibilityService();
            _case = eligibilityService.RunEligibility(_case).Case;

            // make sure there is an FMLA policy
            Assert.IsNotNull(_case.Segments);
            AppliedPolicy fmla = _case.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy.Code == "FMLA"));
            Assert.IsNotNull(fmla, "FMLA Policy not found");
        }

        [TestMethod]
        public void UpdateContactOnAssociatedCasesTest()
        {
            string empId = "000000000000000000000003";

            string caseCode = "FHC";
            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            CaseService cs = new CaseService();
            DateTime empStart = new DateTime(2014, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime empEnd = new DateTime(2014, 3, 14, 0, 0, 0, DateTimeKind.Utc);
            Case empCase = cs.CreateCase(CaseStatus.Open, empId, empStart, empEnd, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
            EmployeeContact initialContact = new EmployeeContact()
            {
                ContactTypeCode = "CHILD",
                ContactTypeName = "Child",
                CustomerId = empCase.CustomerId,
                EmployeeId = empCase.Employee.Id,
                MilitaryStatus = MilitaryStatus.Civilian,
                Contact = new Contact()
                {
                    FirstName = "Little",
                    LastName = "Johnny",
                    Title = "Bratt",
                    DateOfBirth = new DateTime(2014, 12, 31, 0, 0, 0, DateTimeKind.Utc)
                }
            };

            EmployeeService es = new EmployeeService();

            es.SaveContact(initialContact);

            empCase.Contact = initialContact;

            empCase.Save();

            DateTime updatedDOB = new DateTime(2012, 11, 1, 0, 0, 0, DateTimeKind.Utc);

            EmployeeContact updatedEmployeeContact = new EmployeeContact()
            {
                Id = empCase.Contact.Id,
                ContactTypeCode = "CHILD",
                ContactTypeName = "Child",
                CustomerId = empCase.CustomerId,
                EmployeeId = empCase.Employee.Id,
                MilitaryStatus = MilitaryStatus.Civilian,
                Contact = new Contact()
                {
                    FirstName = "Little",
                    LastName = "Johnny",
                    Title = "Bratt",
                    DateOfBirth = updatedDOB
                }
            };
            es.SaveContact(updatedEmployeeContact);
            cs.UpdateContactOnAssociatedCases(updatedEmployeeContact);
            Case updatedCase = cs.GetCaseById(empCase.Id);
            Assert.IsNotNull(updatedCase);
            Assert.AreEqual(updatedCase.Contact.Contact.DateOfBirth, updatedDOB);

            deleteTestCases(empId);
        }


        [TestMethod,Ignore]
        public void TimeRemainingShowsZeroHoursDisplayShowsTimeLeft()
        {

            string customerId = "000000000000000000000002";
            string employerId = "000000000000000000000002";

            Employee testEmployee = new Employee()
            {
                EmployeeNumber = "Emp-552",
                CustomerId = customerId,
                EmployerId = employerId,

                FirstName = "Test",
                LastName = "Emp 552",
                IsExempt = false,
                IsKeyEmployee = false,
                WorkCountry = "US",
                WorkState = "CA",
                PriorHours = new List<PriorHours>()
                {
                    new PriorHours() {HoursWorked=2016, AsOf = new DateTime(2012,11,21)}
                },
                ServiceDate = new DateTime(2009, 02, 02),

                WorkSchedules = new List<Schedule>() {
                    new Schedule() { StartDate = new DateTime(2010, 03, 02),
                        ScheduleType = ScheduleType.Weekly,
                        Times = TestWorkSchedules.MakeSchedule(new DateTime(2010, 03, 02),40)}
                }
            };
            testEmployee.Save();

            string empId = testEmployee.Id;
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().FirstOrDefault(r => r.Code == caseCode);

            deleteTestCases(empId);
            DateTime caseStartDate = new DateTime(2017, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime caseEndDate = new DateTime(2017, 12,31, 0, 0, 0, DateTimeKind.Utc);

            using (CaseService caseSvc = new CaseService())
            using (EligibilityService eligSvc = new EligibilityService())
            {

                Case caseOne = caseSvc.CreateCase(CaseStatus.Open, empId, caseStartDate, caseEndDate,
                    AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                caseOne = eligSvc.RunEligibility(caseOne).Case;
                caseOne = caseSvc.ApplyDetermination(caseOne, null, caseStartDate, caseEndDate, AdjudicationStatus.Approved);
                caseOne = caseSvc.UpdateCase(caseOne);

                AppliedPolicy std = caseOne.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "STD");
                Assert.IsNotNull(std, " Policy not found");

                string[] policyCode = { "STD" };
                List<PolicySummary> policySummary = caseSvc.GetEmployeePolicySummary(testEmployee, DateTime.UtcNow, policyCode);
               // Assert.IsTrue(policySummary[0].HoursRemaining.Equals("0 hours"), "Hours Remaining Should be 0 hours");
                Assert.IsTrue(policySummary[0].TimeRemaining.Equals(0), "Time Remaining Should be 0");

            }

            testEmployee.Delete();
        }



        [TestMethod,Ignore]
        public void TestTimeGainedBack()
        {

            string customerId = "000000000000000000000002";
            string employerId = "000000000000000000000002";


            Employee testEmployee = new Employee()
            {
                EmployeeNumber = "Emp-552",
                CustomerId = customerId,
                EmployerId = employerId,
                FirstName = "Test",
                LastName = "Emp 552",
                IsExempt = false,
                IsKeyEmployee = false,
                WorkCountry = "US",
                WorkState = "NY",
                PriorHours = new List<PriorHours>()
                {
                    new PriorHours()
                    {
                        HoursWorked = 2016, AsOf = new DateTime(2017,11,21)
                    }
                },

                ServiceDate = new DateTime(2009, 02, 02),
                WorkSchedules = new List<Schedule>()
                {
                    new Schedule() { StartDate = new DateTime(2009, 02, 02), ScheduleType = ScheduleType.Weekly, Times = TestWorkSchedules.MakeSchedule(new DateTime(2016,01,01),8)}
                }
            };
            testEmployee.Save();

            string empId = testEmployee.Id;
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().FirstOrDefault(r => r.Code == caseCode);

            deleteTestCases(empId);
            DateTime caseOneStartDate = new DateTime(2017, 1, 3, 0, 0, 0, DateTimeKind.Utc);
            DateTime caseOneEndDate = new DateTime(2017, 03, 17, 0, 0, 0, DateTimeKind.Utc);

            DateTime caseTwoStartDate = new DateTime(2017, 12, 26, 0, 0, 0, DateTimeKind.Utc);
            DateTime caseTwoEndDate = new DateTime(2018, 03, 25, 0, 0, 0, DateTimeKind.Utc);


            using (CaseService caseSvc = new CaseService())
            using (EligibilityService eligSvc = new EligibilityService())
            {
                // Create Case 1
                Case caseOne = caseSvc.CreateCase(CaseStatus.Open, empId, caseOneStartDate, caseOneEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                LeaveOfAbsence loa = eligSvc.RunEligibility(caseOne);
                caseOne = caseSvc.ApplyDetermination(caseOne, null, caseOneStartDate, caseOneEndDate, AdjudicationStatus.Approved);
                caseOne = caseSvc.UpdateCase(caseOne);
                caseOne.Status = CaseStatus.Closed;
                caseOne.ClosureReason = CaseClosureReason.LeaveCancelled;
                caseOne.Segments[0].Status = CaseStatus.Closed;
                caseOne.Save();
                caseOne.WfOnCaseClosed();

                AppliedPolicy fmla = caseOne.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
                Assert.IsNotNull(fmla, "FMLA");

                AppliedPolicyUsage apuApproved = fmla.Usage.FirstOrDefault(u => u.DateUsed == new DateTime(2017, 3, 17, 0, 0, 0, DateTimeKind.Utc));
                Assert.AreEqual(true, apuApproved.Determination == AdjudicationStatus.Approved);

                // Create Case 2
                Case caseTwo = caseSvc.CreateCase(CaseStatus.Open, empId, caseTwoStartDate, caseTwoEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                loa = eligSvc.RunEligibility(caseTwo);

                caseTwo = caseSvc.ApplyDetermination(caseTwo, null, caseTwoStartDate, caseTwoEndDate, AdjudicationStatus.Approved);
                caseTwo = caseSvc.UpdateCase(caseTwo);
                caseTwo.Save();

                fmla = caseTwo.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
                Assert.IsNotNull(fmla, "FMLA");

                // check first day of first week
                apuApproved = fmla.Usage.FirstOrDefault(u => u.DateUsed == new DateTime(2017, 12, 26, 0, 0, 0, DateTimeKind.Utc));
                Assert.AreEqual(true, apuApproved.Determination == AdjudicationStatus.Approved);

                // check last day of last(12) week
                AppliedPolicyUsage apuLastApproved = fmla.Usage.FirstOrDefault(u => u.DateUsed == new DateTime(2018, 3, 18, 0, 0, 0, DateTimeKind.Utc));
                Assert.AreEqual(true, apuLastApproved.Determination == AdjudicationStatus.Approved);

                AppliedPolicyUsage apuDenied = fmla.Usage.FirstOrDefault(u => u.DateUsed == new DateTime(2018, 3, 19, 0, 0, 0, DateTimeKind.Utc));
                Assert.AreEqual(true, apuDenied.Determination == AdjudicationStatus.Denied);

                AppliedPolicyUsage apuLastDenied = fmla.Usage.FirstOrDefault(u => u.DateUsed == new DateTime(2018, 3, 25, 0, 0, 0, DateTimeKind.Utc));
                Assert.AreEqual(true, apuLastDenied.Determination == AdjudicationStatus.Denied);
            }
            //do the Cleanup
            testEmployee.Delete();
        }

        [TestMethod]
        public void CalculateDaysOnJobTransferOrRestrictionTest()
        {
            using (CaseContext c = new CaseContext(CaseType.Consecutive))
            {
                using (CaseService caseSvc = new CaseService(false))
                {
                    using (EligibilityService eligSvc = new EligibilityService())
                    {
                        using (DemandService demSvc = new DemandService(null))
                        {
                            c.Case.Metadata.Set("IsWorkRelated", true);
                            c.Case.AccommodationRequest = new AccommodationRequest();
                            c.Case.AccommodationRequest.Accommodations = new List<Accommodation>();
                            c.Case.AccommodationRequest.Accommodations.Add(new Accommodation { StartDate = new DateTime(2018, 3, 1, 0, 0, 0), EndDate = new DateTime(2018, 03, 05, 0, 0, 0), Duration = AccommodationDuration.Temporary });
                            c.Case.AccommodationRequest.Accommodations.Add(new Accommodation { StartDate = new DateTime(2018, 1, 25, 0, 0, 0), EndDate = new DateTime(2018, 01, 31, 0, 0, 0), Duration = AccommodationDuration.Temporary });
                            c.Case.AccommodationRequest.Accommodations.Add(new Accommodation { StartDate = new DateTime(2018, 02, 19, 0, 0, 0), EndDate = new DateTime(2018, 02, 20, 0, 0, 0), Duration = AccommodationDuration.Temporary });
                            c.Case.AccommodationRequest.Accommodations.Add(new Accommodation { StartDate = new DateTime(2018, 02, 14, 0, 0, 0), EndDate = new DateTime(2018, 02, 16, 0, 0, 0), Duration = AccommodationDuration.Temporary });
                            c.Case.AccommodationRequest.Accommodations.Add(new Accommodation { StartDate = new DateTime(2018, 02, 15, 0, 0, 0), Duration = AccommodationDuration.Permanent });
                            c.Case.WorkRelated = new WorkRelatedInfo();
                            c.Case.WorkRelated.DaysOnJobTransferOrRestriction = 25;
                            eligSvc.RunEligibility(c.Case);
                            caseSvc.RunCalcs(c.Case);
                            c.Case.Save();

                            var employee = c.Case.Employee;
                            EmployeeRestriction employeeRestriction1 = new EmployeeRestriction()
                            {
                                Employee = employee,
                                Case = c.Case,
                                Restriction = new WorkRestriction()
                                {
                                    Values = new List<AppliedDemandValue<WorkRestrictionValue>> { new WorkRestrictionValue { Text = WorkRestrictionType.Fully.ToString() } },
                                    Type = WorkRestrictionType.Fully,
                                    Dates = { StartDate = new DateTime(2018, 02, 5, 0, 0, 0), EndDate = new DateTime(2018, 02, 15, 0, 0, 0) }
                                }
                            };
                            demSvc.SaveJobRestriction(employeeRestriction1);

                            EmployeeRestriction employeeRestriction2 = new EmployeeRestriction()
                            {
                                Employee = employee,
                                Case = c.Case,
                                Restriction = new WorkRestriction()
                                {
                                    Values = new List<AppliedDemandValue<WorkRestrictionValue>> { new AppliedDemandValue<WorkRestrictionValue> { Text = WorkRestrictionType.None.ToString() } },
                                    Type = WorkRestrictionType.None,
                                    Dates = { StartDate = new DateTime(2018, 02, 10, 0, 0, 0), EndDate = new DateTime(2018, 02, 25, 0, 0, 0) }
                                }
                            };
                            demSvc.SaveJobRestriction(employeeRestriction2);

                            EmployeeRestriction employeeRestriction3 = new EmployeeRestriction()
                            {
                                Employee = employee,
                                Case = c.Case,
                                Restriction = new WorkRestriction()
                                {
                                    Values = new List<AppliedDemandValue<WorkRestrictionValue>> { new WorkRestrictionValue { Text = WorkRestrictionType.Partially.ToString() } } ,
                                    Type = WorkRestrictionType.Partially,
                                    Dates = { StartDate = new DateTime(2018, 02, 1, 0, 0, 0), EndDate = new DateTime(2018, 02, 9, 0, 0, 0) }
                                }
                            };
                            demSvc.SaveJobRestriction(employeeRestriction3);

                            EmployeeRestriction employeeRestriction4 = new EmployeeRestriction()
                            {
                                Employee = employee,
                                Case = c.Case,
                                Restriction = new WorkRestriction()
                                {
                                    Values = new List<AppliedDemandValue<WorkRestrictionValue>> { new WorkRestrictionValue { Text = WorkRestrictionType.Partially.ToString() } },
                                    Type = WorkRestrictionType.Partially,
                                    Dates = { StartDate = new DateTime(2018, 02, 5, 0, 0, 0), EndDate = new DateTime(2018, 02, 10, 0, 0, 0) }
                                }
                            };
                            demSvc.SaveJobRestriction(employeeRestriction4);

                            Case myCase = caseSvc.GetCaseById(c.Case.Id);
                            myCase.WorkRelated.CalculatedDaysOnJobTransferOrRestriction = caseSvc.CalculateDaysOnJobTransferOrRestriction(myCase);
                            myCase.Save();

                            Case savedCase = caseSvc.GetCaseById(c.Case.Id);
                            Assert.IsNotNull(savedCase);
                            Assert.IsNotNull(savedCase.WorkRelated);
                            Assert.IsTrue(savedCase.WorkRelated.DaysOnJobTransferOrRestriction == 25);
                        }
                    }
                }

            }

        }

        public class CaseContext : IDisposable
        {
            private Case _case = null;
            private User _user = null;
            public Case Case { get { return _case; } }
            public User User { get { return _user; } }
            public CaseContext(CaseType caseType = CaseType.Consecutive, AppliedRuleEvalResult ruleResult = AppliedRuleEvalResult.Pass)
            {
                _user = User.GetById("000000000000000000000002");
                _case = CreateCase(caseType, ruleResult);
            }
            public void Dispose()
            {
                try
                {
                    string caseId = null;
                    if (_case != null)
                    {
                        caseId = _case.Id;
                        _case.Delete();
                    }
                    _case = null;
                    _user = null;
                    ToDoItem.Delete(ToDoItem.Query.EQ(t => t.CaseId, caseId));
                    ToDoItem.Delete(ToDoItem.Query.EQ(t => t.EmployeeId, EMPLOYEE_ID));
                    //ActivityHistory.Delete(Query.Or(Query.EQ("CaseId", new BsonObjectId(new ObjectId(caseId))), Query.EQ("EmployeeId", new BsonObjectId(new ObjectId(EMPLOYEE_ID)))));
                    //WorkflowInstance.Delete(WorkflowInstance.Query.EQ(w => w.CaseId, caseId));
                    Communication.Delete(Communication.Query.EQ(c => c.CaseId, caseId));
                    var docs = Attachment.AsQueryable().Where(a => a.CaseId == caseId).ToList().Select(a => a.Document).ToList();
                    Attachment.Delete(Attachment.Query.EQ(a => a.CaseId, caseId));
                    Document.Delete(Document.Query.In(d => d.Id, docs.Select(d => d.Id)));
                    if (docs.Any())
                        using (FileService fs = new FileService())
                            foreach (var d in docs)
                                try
                                {
                                    fs.DeleteS3Object(d.Locator, Settings.Default.S3BucketName_Files);
                                }
                                catch { }
                    CaseAssignee.Delete(CaseAssignee.Query.EQ(a => a.CaseId, caseId));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            public CaseContext Refresh()
            {
                _case = Case.GetById(_case.Id);
                return this;
            }
            public CaseContext Http()
            {
                if (HttpContext.Current != null)
                    return this;
                HttpContext context = HttpContext.Current = new HttpContext(new HttpRequest(null, "http://tempuri.org", null), new HttpResponse(null));
                context.Request.Browser = new HttpBrowserCapabilities() { Capabilities = new Dictionary<string, string> { { "supportsEmptyStringInCookieValue", "false" }, { "cookies", "false" } } };
                AbsenceSoft.Data.Customers.Customer.Current = _user.Customer;
                context.User = new GenericPrincipal(new GenericIdentity(_user.Id), new[] { ObjectId.Empty.ToString() });
                context.Items["__current_user__"] = _user;
                return this;
            }
            public CaseContext SelfService()
            {
                Http();
                HttpContext.Current.Items["__portal_context"] = "ESS";
                AbsenceSoft.Data.Customers.Employer.Current = Case.Employer;
                return this;
            }
            public CaseContext Portal()
            {
                Http();
                HttpContext.Current.Items.Remove("__portal_context");
                AbsenceSoft.Data.Customers.Employer.Current = null;
                return this;
            }
            public List<ToDoItem> ToDos()
            {
                // Avoid user query data access issues, especially in ESS contexts.
                var query = ToDoItem.Query.Find(
                    ToDoItem.Query.And(
                        ToDoItem.Query.EQ(t => t.CustomerId, CUSTOMER_ID),
                        ToDoItem.Query.EQ(t => t.EmployerId, EMPLOYER_ID),
                        ToDoItem.Query.EQ(t => t.EmployeeId, EMPLOYEE_ID),
                        ToDoItem.Query.In(t => t.CaseId, new BsonValue[] { BsonNull.Value, new BsonObjectId(new ObjectId(_case.Id)) })
                    //ToDoItem.Query.NE(t => t.Status, ToDoItemStatus.Complete),
                    //ToDoItem.Query.NE(t => t.Status, ToDoItemStatus.Cancelled)
                    )
                );
                return query.ToList();
            }
        }

        protected static Case CreateCase(CaseType caseType = CaseType.Consecutive, AppliedRuleEvalResult ruleResult = AppliedRuleEvalResult.Pass)
        {
            Case.Delete(Case.Query.EQ(c => c.Employee.Id, EMPLOYEE_ID));
            using (CaseService caseSvc = new CaseService(false))
            using (EligibilityService eligSvc = new EligibilityService())
            {
                Case c = caseSvc.CreateCase(CaseStatus.Open, EMPLOYEE_ID, START_DATE, END_DATE, caseType, REASON_CODE);
                c.AssignedToId = USER_ID;
                c.AssignedToName = USER_ID;
                eligSvc.RunEligibility(c);
                foreach (var policy in c.Segments[0].AppliedPolicies)
                {
                    policy.RuleGroups.ForEach(g => g.Rules.Where(r => r.Result != ruleResult).ForEach(r =>
                    {
                        r.Overridden = true;
                        r.OverrideFromResult = r.Result;
                        r.OverrideValue = r.Rule.LeftExpression;
                        r.OverrideNotes = "Manually Overridden by System for Test";
                        r.Result = ruleResult;
                    }));
                }
                eligSvc.RunEligibility(c);
                c.SetCaseEvent(CaseEventType.EstimatedReturnToWork, END_DATE);
                return c.Save();
            }
        }


        [TestMethod]
        public void ChangeCaseFromConsecutiveToReduce()
        {

            string empId = "000000000000000000000003";
            string reasonCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == reasonCode).First();

            // first bit stolen from create case
            using (CaseService svc = new CaseService())
            using (EligibilityService eligSvc = new EligibilityService())
            {

                Case.AsQueryable().Where(c => c.Employee.Id == empId && c.Reason.Code == reason.Code).ForEach(a => a.Delete());

                // start with a three week Consecutive case
                var newCase = svc.CreateCase(CaseStatus.Open, empId, new DateTime(2018, 03, 01, 0, 0, 0, DateTimeKind.Utc), new DateTime(2018, 03, 22, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code, description: "CaseServiceTest.CreateCase");
                Assert.IsNotNull(newCase);
                newCase = svc.UpdateCase(newCase);
                Assert.IsNotNull(newCase.Id);
                eligSvc.RunEligibility(newCase);

                // change the case to Reduced type with extend the end date and changeEndDate set to true 
                newCase = svc.ChangeCase(newCase, newCase.StartDate, new DateTime(2018, 04, 30, 0, 0, 0, DateTimeKind.Utc), false, true, CaseType.Reduced, false, false);
                Assert.AreEqual(1, newCase.Segments.Count);

                // again change the case to Consecutive type with change the end date and changeEndDate set to false 
                newCase = svc.ChangeCase(newCase, newCase.StartDate, new DateTime(2018, 03, 31, 0, 0, 0, DateTimeKind.Utc), false, false, CaseType.Consecutive, false, false);
                Assert.AreEqual(2, newCase.Segments.Count);

                newCase = svc.ApplyDetermination(newCase, null, new DateTime(2018, 03, 01, 0, 0, 0, DateTimeKind.Utc), new DateTime(2018, 04, 30, 0, 0, 0, DateTimeKind.Utc), AdjudicationStatus.Approved);
                newCase = svc.UpdateCase(newCase);

                var usageforReduced = newCase.Segments[1].AppliedPolicies[0].Usage.Where(u => u.DateUsed == Date.NewUtcDate(2018, 4, 1)).ToList();
                usageforReduced.First().Determination.Should().Be(AdjudicationStatus.Approved, "Time for 04/01 should be Approved");

                usageforReduced = newCase.Segments[1].AppliedPolicies[0].Usage.Where(u => u.DateUsed == Date.NewUtcDate(2018, 4, 30)).ToList();
                usageforReduced.First().Determination.Should().Be(AdjudicationStatus.Approved, "Time for 04/30 should be Approved");

                var orderedSegments = newCase.Segments.OrderBy(s => s.StartDate);
                var firstSegment = orderedSegments.First();
                Assert.AreEqual(newCase.StartDate, firstSegment.StartDate);

                var lastSegment = orderedSegments.Last();
                Assert.AreEqual(new DateTime(2018, 04, 01, 0, 0, 0, DateTimeKind.Utc), lastSegment.StartDate);
                Assert.AreEqual(new DateTime(2018, 04, 30, 0, 0, 0, DateTimeKind.Utc), lastSegment.EndDate);
            }
        }

   

        [TestMethod]
        public void ChangeCaseFromConsecutiveToReduceToCheckTimeOnEachSegments()
        {

            string empId = "000000000000000000000003";
            string reasonCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == reasonCode).First();

            // first bit stolen from create case
            using (CaseService svc = new CaseService())
            using (EligibilityService eligSvc = new EligibilityService())
            {

                Case.AsQueryable().Where(c => c.Employee.Id == empId && c.Reason.Code == reason.Code).ForEach(a => a.Delete());

                // start with a three week Consecutive case
                var newCase = svc.CreateCase(CaseStatus.Open, empId, new DateTime(2018, 07, 11, 0, 0, 0, DateTimeKind.Utc), new DateTime(2018, 08, 30, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code, description: "CaseServiceTest.CreateCase");
                Assert.IsNotNull(newCase);
                newCase = svc.UpdateCase(newCase);
                Assert.IsNotNull(newCase.Id);
                eligSvc.RunEligibility(newCase);

                // change the case to Reduced type with extend the end date and changeEndDate set to true
                AbsenceSoft.Data.Schedule reducedSeg0 = new AbsenceSoft.Data.Schedule()
                {
                    StartDate = newCase.StartDate,
                    EndDate = new DateTime(2018, 07, 31, 0, 0, 0, DateTimeKind.Utc),
                    ScheduleType = AbsenceSoft.Data.Enums.ScheduleType.Weekly
                };
                foreach (AbsenceSoft.Data.Time t in newCase.Employee.WorkSchedules[0].Times)
                {
                    reducedSeg0.Times.Add(new AbsenceSoft.Data.Time()
                    {
                        SampleDate = t.SampleDate,
                        TotalMinutes = 120
                    });
                }
                newCase = svc.ChangeCase(newCase, newCase.StartDate, new DateTime(2018, 07, 31, 0, 0, 0, DateTimeKind.Utc), false, true, CaseType.Reduced, false, false, reducedSeg0);

                AbsenceSoft.Data.Schedule reducedSeg1 = new AbsenceSoft.Data.Schedule()
                {
                    StartDate = new DateTime(2018, 08, 01, 0, 0, 0, DateTimeKind.Utc),
                    EndDate = new DateTime(2018, 08, 15, 0, 0, 0, DateTimeKind.Utc),
                    ScheduleType = AbsenceSoft.Data.Enums.ScheduleType.Weekly
                };
                foreach (AbsenceSoft.Data.Time t in newCase.Employee.WorkSchedules[0].Times)
                {
                    reducedSeg1.Times.Add(new AbsenceSoft.Data.Time()
                    {
                        SampleDate = t.SampleDate,
                        TotalMinutes = 240
                    });
                }
                newCase = svc.ChangeCase(newCase, new DateTime(2018, 08, 01, 0, 0, 0, DateTimeKind.Utc), new DateTime(2018, 08, 15, 0, 0, 0, DateTimeKind.Utc), false, true, CaseType.Reduced, false, false, reducedSeg1);

                Assert.AreEqual(2, newCase.Segments.Count);

                newCase.Segments[0].LeaveSchedule[0].Times.FirstOrDefault().TotalMinutes.Should().Be(120,"1st segment shuld have 120 form M-S");
                newCase.Segments[1].LeaveSchedule[0].Times.FirstOrDefault().TotalMinutes.Should().Be(240, "2nd segment shuld have 240 form M-S");

            }
        }

        [TestMethod]
        public void TestConsecutiveAfterApprovedIntermittentTOR()
        {
            // Remove any cases for this employee
            Case.Repository.Delete(c => c.Employee.Id == "000000000000000000000132");

            Employee emp = Employee.GetById("000000000000000000000132");
            emp.Should().NotBeNull("Employee should exist in the database");

            DateTime newConsecutiveStartDate = new DateTime(2018, 3, 31, 0, 0, 0, DateTimeKind.Utc);
            DateTime newConsecutiveEndDate = new DateTime(2018, 7, 31, 0, 0, 0, DateTimeKind.Utc);

            using (CaseService caseSvc = new CaseService(false))
            using (EligibilityService eligSvc = new EligibilityService())
            {
                AbsenceReason reason = AbsenceReason.GetByCode("EHC");
                // Create the intermittent leave, prior to the consecutive (historical)
                Case intermittent = caseSvc.CreateCase(CaseStatus.Open, emp.Id, Date.NewUtcDate(2018, 3, 09), Date.NewUtcDate(2018, 7, 31), CaseType.Intermittent, reason.Code);
                eligSvc.RunEligibility(intermittent);
                var policyCodes = intermittent.Segments.SelectMany(s => s.AppliedPolicies).Select(p => p.Policy.Code).Distinct().ToList();
                policyCodes.Should().Contain("FMLA");
                intermittent.Segments[0].AppliedPolicies.RemoveAll(p => p.Policy.Code != "FMLA"); // ensure we're only dealing with FMLA                
                intermittent = caseSvc.RunCalcs(intermittent);
                intermittent = caseSvc.UpdateCase(intermittent, CaseEventType.CaseCreated);

                // Approve the entire intermittent leave
                intermittent = caseSvc.ApplyDetermination(intermittent, null, intermittent.StartDate, intermittent.EndDate.Value, AdjudicationStatus.Approved);

                // Create two new User request
                intermittent = caseSvc.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>(2)
                {
                    new IntermittentTimeRequest()
                    {
                        RequestDate = Date.NewUtcDate(2018, 3, 12),
                        TotalMinutes = 480,
                        Detail = intermittent.Segments[0].AppliedPolicies.Select(p => new IntermittentTimeRequestDetail()
                        {
                            PolicyCode = p.Policy.Code,
                            Approved = 480,
                            Pending = 0,
                            Denied = 0
                        }).ToList()
                    },
                    new IntermittentTimeRequest()
                    {
                        RequestDate = Date.NewUtcDate(2018, 3, 13),
                        TotalMinutes = 480,
                        Detail = intermittent.Segments[0].AppliedPolicies.Select(p => new IntermittentTimeRequestDetail()
                        {
                            PolicyCode = p.Policy.Code,
                            Approved = 480,
                            Pending = 0,
                            Denied = 0
                        }).ToList()
                    }
                });
                intermittent = caseSvc.UpdateCase(intermittent);

                //Change case from intermittent to consecutive by change the start date only 
                Case consecutive = caseSvc.ChangeCase(intermittent, newConsecutiveStartDate, newConsecutiveEndDate, false, false, CaseType.Consecutive, false, false);
                Assert.AreEqual(2, consecutive.Segments.Count);
                // Here FMLA ExhaustionDate should be two days earlier because there is 2 approved user request already in place.
                Assert.AreEqual(Date.NewUtcDate(2018, 6, 21), consecutive.Segments[1].AppliedPolicies[0].FirstExhaustionDate);

            }
        }

        [TestMethod]
        public void AirlineFmlaEligibility()
        {
            string customerId = "000000000000000000000002";
            string employerId = "000000000000000000000002";

            // Check for Is Airline Flight Crew Employee
            Employee testEmployee = new Employee()
            {
                EmployeeNumber = "000000000000000000000009",
                CustomerId = customerId,
                EmployerId = employerId,

                FirstName = "Test",
                LastName = "Empp 111",
                IsExempt = false,
                IsKeyEmployee = false,
                WorkCountry = "US",
                WorkState = "CA",
                IsFlightCrew = true,
                Meets50In75MileRule = false,
                PriorHours = new List<PriorHours>()
                {
                    new PriorHours() {HoursWorked=2016,
                        AsOf = new DateTime(2012,11,21)}
                },
                ServiceDate = new DateTime(2009, 02, 02),

                WorkSchedules = new List<Schedule>() {
                    new Schedule() { StartDate = new DateTime(2010, 03, 02),
                        ScheduleType = ScheduleType.Weekly,
                        Times = TestWorkSchedules.MakeSchedule(new DateTime(2010, 03, 02),40)}
                }
            };
            testEmployee.Save();

            var emp = Employee.AsQueryable().Where(e => e.EmployeeNumber == testEmployee.EmployeeNumber).FirstOrDefault();
            // Check Employee not null
            Assert.IsNotNull(emp);
            // Check Employee is cabin crew
            Assert.IsTrue((bool)emp.IsFlightCrew);

            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.GetByCode(caseCode);

            deleteTestCases(testEmployee.EmployeeNumber);

            CaseService cs = new CaseService();
            // make the case 72 days long
            Case newCase = cs.CreateCase(CaseStatus.Open, emp.Id, new DateTime(2019, 7, 22, 0, 0, 0, DateTimeKind.Utc), new DateTime(2019, 10, 1, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(newCase);

            // now, they are eligible and they had policy count > 1
            Assert.AreEqual(1, newCase.Segments.Count);
            Assert.IsTrue(newCase.Segments[0].AppliedPolicies.Count >= 1);

            // make sure there is an FMLA-FC policy
            AppliedPolicy fmla = newCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA-FC");
            Assert.IsNotNull(fmla, "FMLA-FC Policy for Airline Flight Crew not found");

            // Check for Eligibility of FMLA-FC policy
            var eligiblePolicy = newCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => (a.Policy).Code == "FMLA-FC").Status == EligibilityStatus.Eligible;
            // Assert for policy eligiblilty
            Assert.IsTrue(eligiblePolicy, "Not Eligible for FMLA-FC Policy for Airline Flight");

            // check for applied policy rule FMLA-FC
            var isRulePassed = newCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => (a.Policy).Code == "FMLA-FC").RuleGroups.SelectMany(s => s.Rules).Where(a => a.Pass != true).ToList();
            Assert.AreEqual(0, isRulePassed.Count);

            // Now the usage for 72 days  
            Assert.AreEqual(72, fmla.Usage.Count);
        }

        [TestMethod]
        public void AirlineFmlaExhaustionForConsecutiveLeave()
        {
            string customerId = "000000000000000000000002";
            string employerId = "000000000000000000000002";
            string caseCode = "EHC";
            string policyCode = "FMLA-FC";

            // Check for Is Airline Flight Crew Employee
            Employee testEmployee = new Employee()
            {
                EmployeeNumber = "000000000000000000000009",
                CustomerId = customerId,
                EmployerId = employerId,

                FirstName = "Test",
                LastName = "Empp 111",
                IsExempt = false,
                IsKeyEmployee = false,
                WorkCountry = "US",
                WorkState = "CA",
                IsFlightCrew = true,
                Meets50In75MileRule = false,
                PriorHours = new List<PriorHours>()
                {
                    new PriorHours() {HoursWorked=2016,
                        AsOf = new DateTime(2012,11,21)}
                },
                ServiceDate = new DateTime(2009, 02, 02),

                WorkSchedules = new List<Schedule>() {
                    new Schedule() { StartDate = new DateTime(2010, 03, 02),
                        ScheduleType = ScheduleType.Weekly,
                        Times = TestWorkSchedules.MakeSchedule(new DateTime(2010, 03, 02),40)}
                }
            };
            testEmployee.Save();

            using (var caseService = new CaseService(false))
            using (var eligService = new EligibilityService())
            {
                AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

                deleteTestCases(testEmployee.Id);

                // make the case atleast 12 weeks long covering 72 days to make it exhausted
                Case consecutiveCase = caseService.CreateCase(CaseStatus.Open, testEmployee.Id, new DateTime(2018, 7, 05, 0, 0, 0, DateTimeKind.Utc), new DateTime(2018, 9, 26, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

                LeaveOfAbsence loa = eligService.RunEligibility(consecutiveCase);
                consecutiveCase = caseService.UpdateCase(consecutiveCase, CaseEventType.CaseCreated);
                // Approve the FMLA-FC
                consecutiveCase = caseService.ApplyDetermination(
                    consecutiveCase,
                    policyCode,
                    consecutiveCase.StartDate,
                    consecutiveCase.EndDate.Value,
                    AdjudicationStatus.Approved);
                // make sure there is an FMLA-FC policy
                AppliedPolicy fmlafc = consecutiveCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);
                Assert.IsNotNull(fmlafc, "FMLA-FC Policy for Airline Flight Crew not found");

                Assert.AreEqual(84, fmlafc.Usage.Count);     // 84 days consecutive leave is applied

                double daysUsed = Math.Round(fmlafc.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.PercentOfDay), 4);

                Assert.IsTrue(daysUsed == 72);

                // the first exhausted day is Wednesday(26-Sep-2018) as it started on Thursday
                //AppliedPolicyUsage apu = fmlafc.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                // 26-Sep-2018 is Wednesday and should be the first exhausted day
                //Assert.AreEqual(new DateTime(2018, 9, 27, 0, 0, 0, DateTimeKind.Utc), apu.DateUsed);
            }
        }

        [TestMethod]
        public void AirlineFmlaExhaustionForCurrentConsecutiveLeaveWhereThereIsPriorConsecutiveLeave()
        {
            string customerId = "000000000000000000000002";
            string employerId = "000000000000000000000002";
            string caseCode = "EHC";
            string policyCode = "FMLA-FC";

            // Check for Is Airline Flight Crew Employee
            Employee testEmployee = new Employee()
            {
                EmployeeNumber = "000000000000000000000009",
                CustomerId = customerId,
                EmployerId = employerId,

                FirstName = "Test",
                LastName = "Empp 111",
                IsExempt = false,
                IsKeyEmployee = false,
                WorkCountry = "US",
                WorkState = "CA",
                IsFlightCrew = true,
                Meets50In75MileRule = false,
                PriorHours = new List<PriorHours>()
                {
                    new PriorHours() {HoursWorked=2016,
                        AsOf = new DateTime(2012,11,21)}
                },
                ServiceDate = new DateTime(2009, 02, 02),

                WorkSchedules = new List<Schedule>() {
                    new Schedule() { StartDate = new DateTime(2010, 03, 02),
                        ScheduleType = ScheduleType.Weekly,
                        Times = TestWorkSchedules.MakeSchedule(new DateTime(2010, 03, 02),40)}
                }
            };
            testEmployee.Save();

            var emp = Employee.AsQueryable().Where(e => e.EmployeeNumber == testEmployee.EmployeeNumber).FirstOrDefault();
            Assert.IsNotNull(emp);
            Assert.IsTrue((bool)emp.IsFlightCrew);

            using (var caseService = new CaseService(false))
            using (var eligService = new EligibilityService())
            {
                AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

                deleteTestCases(testEmployee.Id);

                // make the case 12 weeks long covering 72 days
                Case priorConsecutiveCase = caseService.CreateCase(CaseStatus.Open, testEmployee.Id, new DateTime(2018, 7, 05, 0, 0, 0, DateTimeKind.Utc), new DateTime(2018, 8, 27, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

                LeaveOfAbsence priorloa = eligService.RunEligibility(priorConsecutiveCase);
                priorConsecutiveCase = caseService.UpdateCase(priorConsecutiveCase, CaseEventType.CaseCreated);
                // Approve the FMLA-FC
                priorConsecutiveCase = caseService.ApplyDetermination(
                    priorConsecutiveCase,
                    policyCode,
                    priorConsecutiveCase.StartDate,
                    priorConsecutiveCase.EndDate.Value,
                    AdjudicationStatus.Approved);
                // make sure there is an FMLA-FC policy
                AppliedPolicy priorFmlafc = priorConsecutiveCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);
                Assert.IsNotNull(priorFmlafc, "FMLA-FC Policy for Airline Flight Crew not found");
                Assert.AreEqual(54, priorFmlafc.Usage.Count);        // 47 days approved 

                double daysUsedPrior = Math.Round(priorFmlafc.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.PercentOfDay), 4);

                Assert.IsTrue(daysUsedPrior == 47); // 47 days approved

                // make the case 12 weeks long covering 72 days
                Case currentConsecutiveCase = caseService.CreateCase(CaseStatus.Open, testEmployee.Id, new DateTime(2018, 8, 31, 0, 0, 0, DateTimeKind.Utc), new DateTime(2018, 10, 30, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

                LeaveOfAbsence currentloa = eligService.RunEligibility(currentConsecutiveCase);
                currentConsecutiveCase = caseService.UpdateCase(currentConsecutiveCase, CaseEventType.CaseCreated);
                // Approve the FMLA-FC
                currentConsecutiveCase = caseService.ApplyDetermination(
                    currentConsecutiveCase,
                    policyCode,
                    currentConsecutiveCase.StartDate,
                    currentConsecutiveCase.EndDate.Value,
                    AdjudicationStatus.Approved);
                // make sure there is an FMLA-FC policy
                AppliedPolicy fmlafcCurrent = currentConsecutiveCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);
                Assert.IsNotNull(fmlafcCurrent, "FMLA-FC Policy for Airline Flight Crew not found");
                Assert.AreEqual(61, fmlafcCurrent.Usage.Count); // 61 days Usage

                double daysUsedCurrent = Math.Round(fmlafcCurrent.Usage.Where(u => u.Determination != AdjudicationStatus.Denied).Sum(u => u.PercentOfDay), 4);
                Assert.IsTrue(daysUsedCurrent == 25); // 25 days approved 

                double daysUsed = daysUsedPrior + daysUsedCurrent;
                Assert.IsTrue(daysUsed == 72); // total 72 days approved including both cases

                // the first exhausted day is Wednesday(28-Sep-2018) as it started on Thursday
                AppliedPolicyUsage apu = fmlafcCurrent.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                // 29-Sep-2018 is Wednesday and should be the first exhausted day
                Assert.AreEqual(new DateTime(2018, 9, 29, 0, 0, 0, DateTimeKind.Utc), apu.DateUsed);
            }
        }

        [TestMethod]
        public void AirlineFmlaFixedWorkDaysCalcFMLAFCIntermittentAndConsecutive()
        {
            // Remove any cases for this employee
            Case.Repository.Delete("000000000000000000000009");

            //Test exhaustion for a current intermittent leave where there is a prior consecutive leave that did not previously exhaust the full 72 days.
            string customerId = "000000000000000000000002";
            string employerId = "000000000000000000000002";
            string caseCode = "EHC";
            // Check for Is Airline Flight Crew Employee
            Employee testEmployee = new Employee()
            {
                EmployeeNumber = "000000000000000000000009",
                CustomerId = customerId,
                EmployerId = employerId,
                //StartDayOfWeek = DayOfWeek.Monday,
                FirstName = "Test",
                LastName = "Empp 552",
                IsExempt = false,
                IsKeyEmployee = false,
                WorkCountry = "US",
                WorkState = "CA",
                IsFlightCrew = true,
                PriorHours = new List<PriorHours>()
                {
                    new PriorHours() {HoursWorked=2016, AsOf = new DateTime(2012,11,21)}
                },
                ServiceDate = new DateTime(2009, 02, 02),
                WorkSchedules = new List<Schedule>() {
                    new Schedule() { StartDate = new DateTime(2010, 03, 02),
                        ScheduleType = ScheduleType.Weekly,
                        Times = TestWorkSchedules.MakeSchedule(new DateTime(2010, 03, 02),40)}
                }
            };
            testEmployee.Save();
            var emp = Employee.AsQueryable().Where(e => e.EmployeeNumber == testEmployee.EmployeeNumber).FirstOrDefault();
            Assert.IsNotNull(emp);
            Assert.IsTrue((bool)emp.IsFlightCrew);
            Case intermittent = null;
            //using (var employeeService = new EmployeeService())
            using (var caseService = new CaseService(false))
            using (var eligService = new EligibilityService())
            {
                AbsenceReason reason = AbsenceReason.GetByCode(caseCode);
                deleteTestCases(testEmployee.Id);
                // make the case 12 weeks long covering 72 days
                Case priorConsecutiveCase = caseService.CreateCase(CaseStatus.Open, testEmployee.Id, new DateTime(2018, 7, 05, 0, 0, 0, DateTimeKind.Utc), new DateTime(2018, 8, 27, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                LeaveOfAbsence loa = eligService.RunEligibility(priorConsecutiveCase);

                var policyCodesConsecutive = priorConsecutiveCase.Segments.SelectMany(s => s.AppliedPolicies).Select(p => p.Policy.Code).Distinct().ToList();
                policyCodesConsecutive.Should().Contain("FMLA-FC");
                priorConsecutiveCase.Segments[0].AppliedPolicies.RemoveAll(p => p.Policy.Code != "FMLA-FC"); // ensure we're only dealing with FMLA           
                //priorConsecutiveCase = caseService.RunCalcs(priorConsecutiveCase);
                priorConsecutiveCase = caseService.UpdateCase(priorConsecutiveCase, CaseEventType.CaseCreated);
                // Approve the FMLA-FC
                priorConsecutiveCase = caseService.ApplyDetermination(
                    priorConsecutiveCase,
                    "FMLA-FC",
                    priorConsecutiveCase.StartDate,
                    priorConsecutiveCase.EndDate.Value,
                    AdjudicationStatus.Approved);
                // make sure there is an FMLA-FC policy
                AppliedPolicy fmlafcPrior = priorConsecutiveCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA-FC");
                Assert.IsNotNull(fmlafcPrior, "FMLA-FC Policy not found");
                Assert.AreEqual(54, fmlafcPrior.Usage.Count);        // 54 days approved

                // Create the second intermittent leave
                intermittent = caseService.CreateCase(
                    CaseStatus.Open,
                    testEmployee.Id,
                    Date.NewUtcDate(2018, 09, 03),
                    Date.NewUtcDate(2018, 12, 30),
                    CaseType.Intermittent,
                    reason.Code
                    );
                intermittent.Segments.Should().HaveCount(1, "Should only be a single segment");
                LeaveOfAbsence loaIntermittent = eligService.RunEligibility(intermittent);
                var policyCodes = intermittent.Segments.SelectMany(s => s.AppliedPolicies).Select(p => p.Policy.Code).Distinct().ToList();
                policyCodes.Should().Contain("FMLA-FC");
                intermittent.Segments[0].AppliedPolicies.RemoveAll(p => p.Policy.Code != "FMLA-FC"); // ensure we're only dealing with FMLA           
                intermittent = caseService.RunCalcs(intermittent);
                intermittent = caseService.UpdateCase(intermittent, CaseEventType.CaseCreated);
                // Approve the entire intermittent leave
                intermittent = caseService.ApplyDetermination(intermittent, "FMLA-FC", intermittent.StartDate, intermittent.EndDate.Value, AdjudicationStatus.Approved);
                intermittent = caseService.UpdateCase(intermittent);
                // Create time off request
                IntermittentTimeRequest tor(DateTime date)
                {
                    return new IntermittentTimeRequest()
                    {
                        IntermittentType = IntermittentType.Incapacity,
                        PassedCertification = true,
                        RequestDate = date,
                        TotalMinutes = 40,
                        Detail = new List<IntermittentTimeRequestDetail>(1)
                        {
                            new IntermittentTimeRequestDetail()
                            {
                                Approved = 40,
                                PolicyCode = "FMLA-FC",
                                TotalMinutes = 40
                            }
                        },
                        EmployeeEntered = false,
                        IsIntermittentRestriction = false,
                        IsMaxOccurenceReached = false,
                        ManagerApproved = false
                    };
                };
                // Create the Time Off Requests
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 09, 04)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 09, 05)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 09, 06)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 09, 07)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 09, 10)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 09, 11)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 09, 12)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 09, 13)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 09, 14)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 09, 17)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 09, 18)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 09, 19)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 09, 20)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 09, 21)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 09, 24)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 09, 25)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 09, 26)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 09, 27)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 09, 28)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 10, 01)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 10, 02)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 10, 03)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 10, 04)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 10, 05)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 10, 08)) }));
                intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 10, 09)) }));
                //With PS-1464 in place, handling of TOR has Changed once a TOR Request is  Denied it will deny the rest of the segment
                //intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 10, 10)) }));
                //intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 10, 11)) }));
                //intermittent = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittent, new List<IntermittentTimeRequest>() { tor(Date.NewUtcDate(2018, 10, 12)) }));
            
                AppliedPolicy fmlafcCurrent = intermittent.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA-FC");
                Assert.IsNotNull(fmlafcCurrent, "FMLA-FC Policy not found");

                Assert.AreEqual(25, fmlafcCurrent.Usage.Where(u => u.Determination == AdjudicationStatus.Approved && u.MinutesUsed > 0).Count());

            }
        }

        [TestMethod]
        public void TestLeaveApprovalOnPartialScheduleHours()
        {
            string customerId = "000000000000000000000002";
            string employerId = "000000000000000000000002";
            EmployeeService es = new EmployeeService();
            Employee testEmployee = new Employee()
            {
                EmployeeNumber = "Emp-742",
                CustomerId = customerId,
                EmployerId = employerId,
                FirstName = "Test",
                LastName = "Emp 742",
                IsExempt = false,
                IsKeyEmployee = false,
                WorkState = "OH",
                WorkCountry = "US",
                PriorHours = new List<PriorHours>()
                {
                    new PriorHours() {HoursWorked=3000, AsOf = new DateTime(2018,04,01)}
                },
                ServiceDate = new DateTime(2009, 02, 02)
            };

            Schedule firstSchedule = new Schedule()
            {
                StartDate = new DateTime(2018, 05, 31),
                EndDate = new DateTime(2018, 06, 09),
                ScheduleType = ScheduleType.Weekly,
                Times = TestWorkSchedules.MakeSchedule(new DateTime(2018, 05, 31), 45)
            };
            es.SetWorkSchedule(testEmployee, firstSchedule, null);
            es.Update(testEmployee);

            Schedule secondSchedule = new Schedule()
            {
                StartDate = new DateTime(2018, 06, 10),
                ScheduleType = ScheduleType.Weekly,
                Times = TestWorkSchedules.MakeSchedule(new DateTime(2018, 06, 10), 40)
            };
            es.SetWorkSchedule(testEmployee, secondSchedule, null);
            es.Update(testEmployee);

            string empId = testEmployee.Id;
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().FirstOrDefault(r => r.Code == caseCode);

            deleteTestCases(empId);
            DateTime caseStartDate = new DateTime(2018, 05, 23, 0, 0, 0, DateTimeKind.Utc);
            DateTime caseEndDate = new DateTime(2018, 06, 16, 0, 0, 0, DateTimeKind.Utc);

            using (CaseService caseSvc = new CaseService())
            using (EligibilityService eligSvc = new EligibilityService())
            {

                Case caseOne = caseSvc.CreateCase(CaseStatus.Open, empId, caseStartDate, caseEndDate, AbsenceSoft.Data.Enums.CaseType.Intermittent, reason.Code);
                caseOne = eligSvc.RunEligibility(caseOne).Case;
                caseOne = caseSvc.ApplyDetermination(caseOne, "FMLA", caseStartDate, caseEndDate, AdjudicationStatus.Approved);
                caseOne = caseSvc.UpdateCase(caseOne);

                AppliedPolicy fmla = caseOne.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
                Assert.IsNotNull(fmla, " FMLA not found");
                Assert.IsTrue(fmla.Usage.Count() == 25, "Number of Usage are not equal to 25"); // ( days between 05/23 to 06/16 )

                var usages23 = fmla.Usage.Where(u => u.DateUsed == Date.NewUtcDate(2018, 05, 23)).ToList();
                usages23.First().IntermittentDetermination.Should().Be(IntermittentStatus.Allowed, "Time for 05/23/2018 should be Approved");

                var usages30 = fmla.Usage.Where(u => u.DateUsed == Date.NewUtcDate(2018, 05, 30)).ToList();
                usages30.First().IntermittentDetermination.Should().Be(IntermittentStatus.Allowed, "Time for 05/30/2018 should be Approved");

                var usages15 = fmla.Usage.Where(u => u.DateUsed == Date.NewUtcDate(2018, 06, 15)).ToList();
                usages15.First().IntermittentDetermination.Should().Be(IntermittentStatus.Allowed, "Time for 06/15/2018 should be Approved");

            }

            testEmployee.Delete();

        }

        [TestMethod]
        public void TestTimeTrackerwithVariableScheduleHours()
        {
            string customerId = "000000000000000000000002";
            string employerId = "000000000000000000000002";
            DateTime caseStartDate = new DateTime(2018, 08, 22, 0, 0, 0, DateTimeKind.Utc);
            DateTime caseEndDate = new DateTime(2018, 08, 31, 0, 0, 0, DateTimeKind.Utc);

            EmployeeService es = new EmployeeService();
            Employee testEmployee = new Employee()
            {
                EmployeeNumber = "Emp-758",
                CustomerId = customerId,
                EmployerId = employerId,
                FirstName = "Test",
                LastName = "Emp 758",
                IsExempt = false,
                IsKeyEmployee = false,
                WorkCountry = "US",
                WorkState = "CA",
                PriorHours = new List<PriorHours>()
                {
                    new PriorHours() { HoursWorked = 1716.75, AsOf = new DateTime(2012,11,21) }
                },
                ServiceDate = new DateTime(2009, 02, 02),

                WorkSchedules = new List<Schedule>() {
                    new Schedule() {
                        StartDate = new DateTime(2010, 03, 02),
                        EndDate = new DateTime(2018, 07,28),
                        ScheduleType = ScheduleType.Weekly,
                        Times = TestWorkSchedules.MakeSchedule(new DateTime(2010, 03, 02),40)
                    },
                    new Schedule() {
                        StartDate = new DateTime(2018,07,29),
                        ScheduleType = ScheduleType.Variable,
                        Times = new List<Time>()
                        {
                            new Time() { SampleDate = new DateTime(2018,07,29), TotalMinutes = 480 },
                            new Time() { SampleDate = new DateTime(2018,07,30), TotalMinutes = 480 },
                            new Time() { SampleDate = new DateTime(2018,07,31), TotalMinutes = 480 },
                            new Time() { SampleDate = new DateTime(2018,08,01), TotalMinutes = 480 },
                            new Time() { SampleDate = new DateTime(2018,08,02), TotalMinutes = 0 },
                            new Time() { SampleDate = new DateTime(2018,08,03), TotalMinutes = 0 },
                            new Time() { SampleDate = new DateTime(2018,08,04), TotalMinutes = 480 },
                        }
                    }
                }
            };
            testEmployee.Save();
            es.SetWorkSchedule(testEmployee, null, new List<VariableScheduleTime>(6)
                {
                    new VariableScheduleTime()
                    {
                        CustomerId = testEmployee.CustomerId,
                        EmployerId = testEmployee.EmployerId,
                        EmployeeId = testEmployee.Id,
                        Time = new Time()
                        {
                            SampleDate = caseStartDate.AddDays(0), //08/22
                            TotalMinutes = 480
                        }
                    },
                    new VariableScheduleTime()
                    {
                        CustomerId = testEmployee.CustomerId,
                        EmployerId = testEmployee.EmployerId,
                        EmployeeId = testEmployee.Id,
                        Time = new Time()
                        {
                            SampleDate = caseStartDate.AddDays(3), //08/25
                            TotalMinutes = 480
                        }
                    },
                    new VariableScheduleTime()
                    {
                        CustomerId = testEmployee.CustomerId,
                        EmployerId = testEmployee.EmployerId,
                        EmployeeId = testEmployee.Id,
                        Time = new Time()
                        {
                            SampleDate = caseStartDate.AddDays(4), //08/26
                            TotalMinutes = 480
                        }
                    },
                    new VariableScheduleTime()
                    {
                        CustomerId = testEmployee.CustomerId,
                        EmployerId = testEmployee.EmployerId,
                        EmployeeId = testEmployee.Id,
                        Time = new Time()
                        {
                            SampleDate = caseStartDate.AddDays(5), //08/27
                            TotalMinutes = 480
                        }
                    },
                    new VariableScheduleTime()
                    {
                        CustomerId = testEmployee.CustomerId,
                        EmployerId = testEmployee.EmployerId,
                        EmployeeId = testEmployee.Id,
                        Time = new Time()
                        {
                            SampleDate = caseStartDate.AddDays(6), //08/28
                            TotalMinutes = 480
                        }
                    },
                    new VariableScheduleTime()
                    {
                        CustomerId = testEmployee.CustomerId,
                        EmployerId = testEmployee.EmployerId,
                        EmployeeId = testEmployee.Id,
                        Time = new Time()
                        {
                            SampleDate = caseStartDate.AddDays(7), //08/29
                            TotalMinutes = 480
                        }
                    }
                });
            es.Update(testEmployee);
            string caseCode = "EHC";
            AbsenceReason reason = AbsenceReason.AsQueryable().FirstOrDefault(r => r.Code == caseCode);
            string empId = testEmployee.Id;
            string policyCode = "FMLA";
            deleteTestCases(empId);
            using (CaseService caseSvc = new CaseService())
            using (EligibilityService eligSvc = new EligibilityService())
            {
                Case caseOne = caseSvc.CreateCase(CaseStatus.Open, empId, caseStartDate, caseEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                caseOne = eligSvc.RunEligibility(caseOne).Case;
                caseOne = caseSvc.ApplyDetermination(caseOne, null, caseStartDate, caseEndDate, AdjudicationStatus.Approved);
                caseOne = caseSvc.UpdateCase(caseOne);

                AppliedPolicy fmla = caseOne.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);
                Assert.IsNotNull(fmla, " Policy not found");

                // From 08/22 to 08/31 = 8 * 6 = 48 hours / 1.2 weeks
                List<PolicySummary> policySummary = caseSvc.GetEmployeePolicySummary(testEmployee, new DateTime(2018, 09, 06), policyCode);
                policySummary[0].TimeUsed.Should().Be(2, "time used should be 2 where variable time skips over 2 calendar weeks");
                policySummary[0].HoursUsed.Should().Be("48h", "hours used should be 48 hours");
            }
            testEmployee.Delete();
        }

        [TestMethod]
        public void ChangeExhaustedLeaves()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            CaseService cs = new CaseService();

            DateTime startDate = new DateTime(2018, 12, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime endDate = new DateTime(2018, 12, 31, 0, 0, 0, DateTimeKind.Utc);
            DateTime testDate = new DateTime(2018, 12, 23, 0, 0, 0, DateTimeKind.Utc);

            Case testCase = cs.CreateCase(CaseStatus.Open, empId, startDate, endDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(testCase);


            testCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>())
                    .ForEach(ap => cs.ApplyDetermination(testCase, ap.Policy.Code, startDate, endDate, AdjudicationStatus.Approved));

            cs.GetProjectedUsage(testCase);

            Assert.IsTrue(testCase.Segments.Count > 0);
            Assert.IsNotNull(testCase.Segments[0].AppliedPolicies);
            Assert.IsTrue(testCase.Segments[0].AppliedPolicies.Count > 0);

            DateTime DeniedStartDate = new DateTime(2018, 12, 23, 0, 0, 0, DateTimeKind.Utc);
            DateTime DeniedEndDate = new DateTime(2018, 12, 31, 0, 0, 0, DateTimeKind.Utc);

            testCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>())
                    .ForEach(ap => cs.ApplyDetermination(testCase, ap.Policy.Code, DeniedStartDate, DeniedEndDate, AdjudicationStatus.Denied, AdjudicationDenialReason.Exhausted, "Test", AdjudicationDenialReason.ExhaustedDescription));

            cs.GetProjectedUsage(testCase);

            AppliedPolicy policy = testCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == "FMLA");
            AppliedPolicyUsage newUsage = policy.Usage.FirstOrDefault(fd => fd.DateUsed == testDate);
            Assert.AreEqual(AdjudicationStatus.Denied, newUsage.Determination);

            DateTime DeniedStartDate2 = new DateTime(2018, 12, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime DeniedEndDate2 = new DateTime(2018, 12, 9, 0, 0, 0, DateTimeKind.Utc);
            testCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>())
                    .ForEach(ap => cs.ApplyDetermination(testCase, ap.Policy.Code, DeniedStartDate2, DeniedEndDate2, AdjudicationStatus.Denied, AdjudicationDenialReason.PaperworkNotReceived, "Test", AdjudicationDenialReason.PaperworkNotReceivedDescription));

            cs.GetProjectedUsage(testCase);

            policy = testCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == "FMLA");

            newUsage = policy.Usage.FirstOrDefault(fd => fd.DateUsed == testDate);
            Assert.AreEqual(AdjudicationStatus.Pending, newUsage.Determination);

            // and then clean up from the tests
            deleteTestCases(empId);

        }

        #region Consecutive: Unit Tests for regaining policy usage exactly after policy period and not one day after.

        [TestMethod]
        public void CalcRollingBackPeriodConsecutiveCaseTest()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2017, 5, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2017, 9, 30, 0, 0, 0, DateTimeKind.Utc);

            DateTime secondCaseStartDate = new DateTime(2018, 4, 30, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondCaseEndDate = new DateTime(2018, 9, 30, 0, 0, 0, DateTimeKind.Utc);


            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2017, 7, 24, 0, 0, 0, DateTimeKind.Utc));

                // Create second case
                Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondCaseStartDate, secondCaseEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(secondCase);

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);
                Assert.IsNotNull(fmla, "FMLA Policy not found");

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(secondCase);
                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);

                apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 4, 30, 0, 0, 0, DateTimeKind.Utc));

                apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Pending);
                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 5, 1, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }//end: CalcRollingBackPeriodConsecutiveCaseTest()

        [TestMethod]
        public void CalcsRollingForwardPeriodConsecutiveTest()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2017, 5, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2017, 9, 30, 0, 0, 0, DateTimeKind.Utc);

            DateTime secondCaseStartDate = new DateTime(2018, 4, 30, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondCaseEndDate = new DateTime(2018, 9, 30, 0, 0, 0, DateTimeKind.Utc);


            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingForward;

                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2017, 7, 24, 0, 0, 0, DateTimeKind.Utc));

                // Create second case
                Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondCaseStartDate, secondCaseEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(secondCase);

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);
                Assert.IsNotNull(fmla, "FMLA Policy not found");

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingForward;

                // Get Usage
                cs.GetProjectedUsage(secondCase);
                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);

                apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 4, 30, 0, 0, 0, DateTimeKind.Utc));

                apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Pending);
                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 5, 1, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }//end: CalcsRollingForwardPeriodConsecutiveTest()

        [TestMethod]
        public void CalcsCalendarYearPeriodConsecutiveCaseTest()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2017, 1, 3, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2017, 3, 25, 0, 0, 0, DateTimeKind.Utc);

            DateTime secondCaseStartDate = new DateTime(2018, 1, 3, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondCaseEndDate = new DateTime(2018, 3, 27, 0, 0, 0, DateTimeKind.Utc);


            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                //Assert.AreEqual(12, Math.Round(daysUsed / 7));

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.Period = 1;
                fmla.PolicyReason.PeriodType = PeriodType.CalendarYear;

                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                //Assert.IsTrue(apu.DateUsed == new DateTime(2017, 3, 25, 0, 0, 0, DateTimeKind.Utc));

                // Create second case
                Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondCaseStartDate, secondCaseEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(secondCase);

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);
                Assert.IsNotNull(fmla, "FMLA Policy not found");

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.Period = 1;
                fmla.PolicyReason.PeriodType = PeriodType.CalendarYear;

                // Get Usage
                cs.GetProjectedUsage(secondCase);
                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);

                apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Pending);
                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 1, 3, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }//end: CalcsCalendarYearPeriodConsecutiveCaseTest()

        [TestMethod]
        public void CalcsFixedResetPeriodConsecutiveCaseTest()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2018, 4, 5, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2018, 7, 31, 0, 0, 0, DateTimeKind.Utc);

            DateTime secondCaseStartDate = new DateTime(2019, 2, 15, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondCaseEndDate = new DateTime(2019, 5, 15, 0, 0, 0, DateTimeKind.Utc);


            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;                
                fmla.PolicyReason.PeriodType = PeriodType.FixedResetPeriod;
                fmla.PolicyReason.ResetMonth = Month.March;
                fmla.PolicyReason.ResetDayOfMonth = 1;


                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 6, 28, 0, 0, 0, DateTimeKind.Utc));

                // Create second case
                Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondCaseStartDate, secondCaseEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(secondCase);

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);
                Assert.IsNotNull(fmla, "FMLA Policy not found");

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;                
                fmla.PolicyReason.PeriodType = PeriodType.FixedResetPeriod;
                fmla.PolicyReason.ResetMonth = Month.March;
                fmla.PolicyReason.ResetDayOfMonth = 1;

                // Get Usage
                cs.GetProjectedUsage(secondCase);
                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);

                apu = fmla.Usage.LastOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2019, 2, 28, 0, 0, 0, DateTimeKind.Utc));

                apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == new DateTime(2019, 3, 1, 0, 0, 0, DateTimeKind.Utc));
                Assert.IsTrue(apu.Determination == AdjudicationStatus.Pending);
            }

            // clean up
            deleteTestCases(empId);

        }//end: CalcsFixedResetPeriodConsecutiveCaseTest()

        [TestMethod]
        public void CalcsFixedYearFromServiceDatePeriodConsecutiveCaseTest()
        {
            // Employee Service date: 02/02/2009
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2017, 5, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2017, 9, 30, 0, 0, 0, DateTimeKind.Utc);

            DateTime secondCaseStartDate = new DateTime(2018, 2, 2, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondCaseEndDate = new DateTime(2018, 6, 30, 0, 0, 0, DateTimeKind.Utc);


            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                Employee emp = Employee.GetById(empId);

                if (emp != null)
                {
                    var servicedate = emp.ServiceDate;
                }

                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.FixedYearFromServiceDate;

                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2017, 7, 24, 0, 0, 0, DateTimeKind.Utc));

                // Create second case
                Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondCaseStartDate, secondCaseEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(secondCase);

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);
                Assert.IsNotNull(fmla, "FMLA Policy not found");

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.FixedYearFromServiceDate;

                // Get Usage
                cs.GetProjectedUsage(secondCase);
                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);

                apu = fmla.Usage.LastOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                //Assert.IsTrue(apu.DateUsed == new DateTime(2018, 2, 1, 0, 0, 0, DateTimeKind.Utc));

                apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Pending);
                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 2, 2, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }//end: CalcsFixedYearFromServiceDatePeriodConsecutiveCaseTest()

        [TestMethod]
        public void CalcRollingBackPeriodConsecutiveCaseLeapYearTest()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2016, 2, 23, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2016, 5, 20, 0, 0, 0, DateTimeKind.Utc);

            DateTime secondCaseStartDate = new DateTime(2017, 2, 22, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondCaseEndDate = new DateTime(2017, 5, 20, 0, 0, 0, DateTimeKind.Utc);


            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2016, 5, 17, 0, 0, 0, DateTimeKind.Utc));

                // Create second case
                Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondCaseStartDate, secondCaseEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(secondCase);

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);
                Assert.IsNotNull(fmla, "FMLA Policy not found");

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(secondCase);
                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);

                apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2017, 2, 22, 0, 0, 0, DateTimeKind.Utc));

                apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Pending);
                Assert.IsTrue(apu.DateUsed == new DateTime(2017, 2, 23, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }

        #endregion

        #region Intermittent: Unit Tests for regaining policy usage exactly after policy period and not one day after.

        [TestMethod]
        public void CalcRollingBackPeriodIntermittentCaseTest()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2017, 5, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2017, 9, 30, 0, 0, 0, DateTimeKind.Utc);

            DateTime secondCaseStartDate = new DateTime(2018, 4, 30, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondCaseEndDate = new DateTime(2018, 9, 30, 0, 0, 0, DateTimeKind.Utc);


            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2017, 7, 24, 0, 0, 0, DateTimeKind.Utc));

                // Create second case of Intermittent type
                Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondCaseStartDate, secondCaseEndDate, AbsenceSoft.Data.Enums.CaseType.Intermittent, reason.Code);
                esvc.RunEligibility(secondCase);

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);
                Assert.IsNotNull(fmla, "FMLA Policy not found");

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(secondCase);
                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);
                
                // Approve FMLA policy
                cs.ApplyDetermination(secondCase, policyCode, secondCaseStartDate, secondCaseEndDate, AdjudicationStatus.Approved);
                secondCase.Save();

                apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 4, 30, 0, 0, 0, DateTimeKind.Utc));

                apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Pending);
//                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 5, 1, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }//end: CalcRollingBackPeriodIntermittentCaseTest()

        [TestMethod]
        public void CalcsRollingForwardPeriodIntermittentCaseTest()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2017, 5, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2017, 9, 30, 0, 0, 0, DateTimeKind.Utc);

            DateTime secondCaseStartDate = new DateTime(2018, 4, 30, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondCaseEndDate = new DateTime(2018, 9, 30, 0, 0, 0, DateTimeKind.Utc);


            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingForward;

                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2017, 7, 24, 0, 0, 0, DateTimeKind.Utc));

                // Create second case of Intermittent type
                Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondCaseStartDate, secondCaseEndDate, AbsenceSoft.Data.Enums.CaseType.Intermittent, reason.Code);
                esvc.RunEligibility(secondCase);

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);
                Assert.IsNotNull(fmla, "FMLA Policy not found");

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingForward;

                // Get Usage
                cs.GetProjectedUsage(secondCase);
                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);
                
                // Approve FMLA policy
                cs.ApplyDetermination(secondCase, policyCode, secondCaseStartDate, secondCaseEndDate, AdjudicationStatus.Approved);
                secondCase.Save();

                apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 4, 30, 0, 0, 0, DateTimeKind.Utc));

                apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Pending);
                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 5, 1, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }//end: CalcsRollingForwardPeriodIntermittentCaseTest()

        [TestMethod]
        public void CalcsCalendarYearPeriodIntermittentCaseTest()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2017, 1, 3, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2017, 3, 25, 0, 0, 0, DateTimeKind.Utc);

            DateTime secondCaseStartDate = new DateTime(2018, 1, 3, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondCaseEndDate = new DateTime(2018, 3, 27, 0, 0, 0, DateTimeKind.Utc);


            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);                

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.Period = 1;
                fmla.PolicyReason.PeriodType = PeriodType.CalendarYear;

                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                //Assert.IsTrue(apu.DateUsed == new DateTime(2017, 3, 25, 0, 0, 0, DateTimeKind.Utc));

                // Create second case of Intermittent type
                Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondCaseStartDate, secondCaseEndDate, AbsenceSoft.Data.Enums.CaseType.Intermittent, reason.Code);
                esvc.RunEligibility(secondCase);

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);
                Assert.IsNotNull(fmla, "FMLA Policy not found");

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.Period = 1;
                fmla.PolicyReason.PeriodType = PeriodType.CalendarYear;

                // Get Usage
                cs.GetProjectedUsage(secondCase);
                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);

                // Approve FMLA policy
                cs.ApplyDetermination(secondCase, policyCode, secondCaseStartDate, secondCaseEndDate, AdjudicationStatus.Approved);
                secondCase.Save();

                apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Pending);
                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 1, 3, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }//end: CalcsCalendarYearPeriodIntermittentCaseTest()

        [TestMethod]
        public void CalcsFixedResetPeriodIntermittentCaseTest()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2018, 4, 5, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2018, 7, 31, 0, 0, 0, DateTimeKind.Utc);

            DateTime secondCaseStartDate = new DateTime(2019, 2, 15, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondCaseEndDate = new DateTime(2019, 5, 15, 0, 0, 0, DateTimeKind.Utc);


            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.PeriodType = PeriodType.FixedResetPeriod;
                fmla.PolicyReason.ResetMonth = Month.March;
                fmla.PolicyReason.ResetDayOfMonth = 1;


                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 6, 28, 0, 0, 0, DateTimeKind.Utc));

                // Create second case of Intermittent type
                Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondCaseStartDate, secondCaseEndDate, AbsenceSoft.Data.Enums.CaseType.Intermittent, reason.Code);
                esvc.RunEligibility(secondCase);

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);
                Assert.IsNotNull(fmla, "FMLA Policy not found");

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.PeriodType = PeriodType.FixedResetPeriod;
                fmla.PolicyReason.ResetMonth = Month.March;
                fmla.PolicyReason.ResetDayOfMonth = 1;

                // Get Usage
                cs.GetProjectedUsage(secondCase);
                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);

                // Approve FMLA policy
                cs.ApplyDetermination(secondCase, policyCode, secondCaseStartDate, secondCaseEndDate, AdjudicationStatus.Approved);
                secondCase.Save();

                //apu = fmla.Usage.LastOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                //Assert.IsTrue(apu.DateUsed == new DateTime(2019, 2, 28, 0, 0, 0, DateTimeKind.Utc));

                //apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Pending);
                //Assert.IsTrue(apu.DateUsed == new DateTime(2019, 3, 1, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }//end: CalcsFixedResetPeriodIntermittentCaseTest()

        [TestMethod]
        public void CalcsFixedResetPeriodIntermittentCaseTest1()
        {
            string empId = "000000000000000000000001";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2018, 4, 5, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2018, 7, 31, 0, 0, 0, DateTimeKind.Utc);

            DateTime secondCaseStartDate = new DateTime(2019, 2, 15, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondCaseEndDate = new DateTime(2019, 5, 15, 0, 0, 0, DateTimeKind.Utc);


            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.PeriodType = PeriodType.FixedResetPeriod;
                fmla.PolicyReason.ResetMonth = Month.March;
                fmla.PolicyReason.ResetDayOfMonth = 1;


                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 6, 28, 0, 0, 0, DateTimeKind.Utc));

                // Create second case of Intermittent type
                Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondCaseStartDate, secondCaseEndDate, AbsenceSoft.Data.Enums.CaseType.Intermittent, reason.Code);
                esvc.RunEligibility(secondCase);

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);
                Assert.IsNotNull(fmla, "FMLA Policy not found");

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.PeriodType = PeriodType.FixedResetPeriod;
                fmla.PolicyReason.ResetMonth = Month.March;
                fmla.PolicyReason.ResetDayOfMonth = 1;

                // Get Usage
                cs.GetProjectedUsage(secondCase);
                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);

                // Approve FMLA policy
                cs.ApplyDetermination(secondCase, policyCode, secondCaseStartDate, secondCaseEndDate, AdjudicationStatus.Approved);
                secondCase.Save();

                //apu = fmla.Usage.LastOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                //Assert.IsTrue(apu.DateUsed == new DateTime(2019, 2, 28, 0, 0, 0, DateTimeKind.Utc));

                //apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Pending);
                //Assert.IsTrue(apu.DateUsed == new DateTime(2019, 3, 1, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }//end: CalcsFixedResetPeriodIntermittentCaseTest()

        [TestMethod,Ignore]
        public void CalcsFixedResetPeriodIntermittentCaseTest2()
        {
            string empId = "000000000000000000000002";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2018, 4, 5, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2018, 7, 31, 0, 0, 0, DateTimeKind.Utc);

            DateTime secondCaseStartDate = new DateTime(2019, 2, 15, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondCaseEndDate = new DateTime(2019, 5, 15, 0, 0, 0, DateTimeKind.Utc);


            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.PeriodType = PeriodType.FixedResetPeriod;
                fmla.PolicyReason.ResetMonth = Month.March;
                fmla.PolicyReason.ResetDayOfMonth = 1;


                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 6, 28, 0, 0, 0, DateTimeKind.Utc));

                // Create second case of Intermittent type
                Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondCaseStartDate, secondCaseEndDate, AbsenceSoft.Data.Enums.CaseType.Intermittent, reason.Code);
                esvc.RunEligibility(secondCase);

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);
                Assert.IsNotNull(fmla, "FMLA Policy not found");

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.PeriodType = PeriodType.FixedResetPeriod;
                fmla.PolicyReason.ResetMonth = Month.March;
                fmla.PolicyReason.ResetDayOfMonth = 1;

                // Get Usage
                cs.GetProjectedUsage(secondCase);
                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);

                // Approve FMLA policy
                cs.ApplyDetermination(secondCase, policyCode, secondCaseStartDate, secondCaseEndDate, AdjudicationStatus.Approved);
                secondCase.Save();

                apu = fmla.Usage.LastOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2019, 5, 15, 0, 0, 0, DateTimeKind.Utc));

                //apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Pending);
                //Assert.IsTrue(apu.DateUsed == new DateTime(2019, 3, 1, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }//end: CalcsFixedResetPeriodIntermittentCaseTest()

       

        [TestMethod, Ignore]
        public void CalcsFixedYearFromServiceDatePeriodIntermittentCaseTest()
        {
            // Employee Service date: 02/02/2009
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2017, 5, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2017, 9, 30, 0, 0, 0, DateTimeKind.Utc);

            DateTime secondCaseStartDate = new DateTime(2018, 1, 15, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondCaseEndDate = new DateTime(2018, 4, 30, 0, 0, 0, DateTimeKind.Utc);


            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                Employee emp = Employee.GetById(empId);

                if (emp != null)
                {
                    var servicedate = emp.ServiceDate;
                }

                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.FixedYearFromServiceDate;

                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2017, 7, 22, 0, 0, 0, DateTimeKind.Utc));

                // Create second case of Intermittent type
                Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondCaseStartDate, secondCaseEndDate, AbsenceSoft.Data.Enums.CaseType.Intermittent, reason.Code);
                esvc.RunEligibility(secondCase);

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);
                Assert.IsNotNull(fmla, "FMLA Policy not found");

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.FixedYearFromServiceDate;

                // Get Usage
                cs.GetProjectedUsage(secondCase);
                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);

                // Approve FMLA policy
                cs.ApplyDetermination(secondCase, policyCode, secondCaseStartDate, secondCaseEndDate, AdjudicationStatus.Approved);
                secondCase.Save();

                apu = fmla.Usage.LastOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 2, 1, 0, 0, 0, DateTimeKind.Utc));

                apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Pending);
                if (apu != null)
                    Assert.IsTrue(apu.DateUsed == new DateTime(2018, 2, 2, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }//end: CalcsFixedYearFromServiceDatePeriodIntermittentCaseTest()

        [TestMethod]
        public void CalcRollingBackPeriodIntermittentCaseLeapYearTest()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2016, 2, 23, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2016, 5, 20, 0, 0, 0, DateTimeKind.Utc);

            DateTime secondCaseStartDate = new DateTime(2017, 2, 22, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondCaseEndDate = new DateTime(2017, 5, 20, 0, 0, 0, DateTimeKind.Utc);


            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7));

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2016, 5, 17, 0, 0, 0, DateTimeKind.Utc));

                // Create second case of Intermittent type
                Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondCaseStartDate, secondCaseEndDate, AbsenceSoft.Data.Enums.CaseType.Intermittent, reason.Code);
                esvc.RunEligibility(secondCase);

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);
                Assert.IsNotNull(fmla, "FMLA Policy not found");

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.WorkWeeks;
                fmla.PolicyReason.Entitlement = 12;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(secondCase);
                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);

                apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2017, 2, 22, 0, 0, 0, DateTimeKind.Utc));

                apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Pending);
//                Assert.IsTrue(apu.DateUsed == new DateTime(2017, 2, 23, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }

        #endregion
    }
}
   
