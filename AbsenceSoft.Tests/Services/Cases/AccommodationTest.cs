﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using System.Collections.Generic;
using AbsenceSoft.Data.Customers;

namespace AbsenceSoft.Tests.Services.Cases
{
    [TestClass]
    public class AccommodationTest
    {
        [TestMethod]
        public void AdjudicateAccommodationTest()
        {
            Case myCase = new Case()
            {
                EmployerId = "000000000000000000000002",
                CustomerId = "000000000000000000000002",
                Employee = Employee.AsQueryable().Where(e => e.EmployerId == "000000000000000000000001" && e.CustomerId == "000000000000000000000001").First(),
                CaseNumber = "ACCOMTEST1",
                Id = "123456789012345678901234",
                AssignedToId = "000000000000000000000000"
            };
            myCase.IsAccommodation = true;
            myCase.AccommodationRequest = new AccommodationRequest()
            {
                GeneralHealthCondition = "Test",
                Status = CaseStatus.Open,
                Accommodations = new List<Accommodation>()
                {
                    new Accommodation()
                    {
                        Description = "Test",
                        Duration = AccommodationDuration.Temporary,
                        Type = AccommodationType.GetByCode("EQUIPMENTORSOFTWARE"),
                        Usage = new List<AccommodationUsage>()
                        {
                            new AccommodationUsage()
                            {
                                StartDate = new DateTime(2014, 7, 31).ToMidnight(),
                                Determination = AdjudicationStatus.Pending
                            }
                        }
                    }
                }
            };
            var accom = myCase.AccommodationRequest.Accommodations.First();
            using (AccommodationService svc = new AccommodationService())
            {
                myCase = svc.ApplyDetermination(myCase,
                    new DateTime(2014, 7, 31).ToMidnight(),
                    new DateTime(2014, 8, 31).ToMidnight(),
                    AdjudicationStatus.Approved, null, null, null,
                    accom.Id);
            }
            Assert.AreEqual(2, accom.Usage.Count);
            Assert.AreEqual(AdjudicationStatus.Approved, accom.Determination);
            Assert.AreEqual(new DateTime(2014, 7, 31).ToMidnight(), accom.StartDate);
        }

        #region Temporary Duration

        [TestMethod]
        [TestCategory("Accommodations.Determination")]
        public void AdjudicateAccomm_Temporary_PendingToApproved_Test()
        {
            Case myCase = new Case()
            {
                EmployerId = "000000000000000000000002",
                CustomerId = "000000000000000000000002",
                Employee = Employee.AsQueryable().Where(e => e.EmployerId == "000000000000000000000001" && e.CustomerId == "000000000000000000000001").First(),
                CaseNumber = "ACCOMTEST2",
                Id = "123456789012345678901234",
                AssignedToId = "000000000000000000000000"
            };
            myCase.IsAccommodation = true;
            myCase.AccommodationRequest = new AccommodationRequest()
            {
                GeneralHealthCondition = "Test",
                Status = CaseStatus.Open,
                Accommodations = new List<Accommodation>()
                {
                    new Accommodation()
                    {
                        Description = "Test",
                        Duration = AccommodationDuration.Temporary,
                        Type = AccommodationType.GetByCode("EQUIPMENTORSOFTWARE"),
                        Usage = new List<AccommodationUsage>()
                        {
                            new AccommodationUsage()
                            {
                                StartDate = new DateTime(2014, 10, 1).ToMidnight(),
                                EndDate = new DateTime(2014, 10, 1).ToMidnight(),
                                Determination = AdjudicationStatus.Pending
                            }
                        }
                    }
                }
            };
            var accom = myCase.AccommodationRequest.Accommodations.First();
            using (AccommodationService svc = new AccommodationService())
            {
                myCase = svc.ApplyDetermination(myCase,
                    new DateTime(2014, 10, 1).ToMidnight(),
                    new DateTime(2014, 10, 15).ToMidnight(),
                    AdjudicationStatus.Approved, null, null, null,
                    accom.Id);
            }
            Assert.AreEqual(1, accom.Usage.Count);
            Assert.AreEqual(AdjudicationStatus.Approved, accom.Determination);
            Assert.AreEqual(new DateTime(2014, 10, 1).ToMidnight(), accom.StartDate);
            Assert.AreEqual(new DateTime(2014, 10, 1).ToMidnight(), accom.EndDate);
        }

        [TestMethod]
        [TestCategory("Accommodations.Determination")]
        public void AdjudicateAccomm_Temporary_FullyApprovedToPartiallyDenied_Test()
        {
            Case myCase = new Case()
            {
                EmployerId = "000000000000000000000002",
                CustomerId = "000000000000000000000002",
                Employee = Employee.AsQueryable().Where(e => e.EmployerId == "000000000000000000000001" && e.CustomerId == "000000000000000000000001").First(),
                CaseNumber = "ACCOMTEST3",
                Id = "123456789012345678901234",
                AssignedToId = "000000000000000000000000"
            };
            myCase.IsAccommodation = true;
            myCase.AccommodationRequest = new AccommodationRequest()
            {
                GeneralHealthCondition = "Test",
                Status = CaseStatus.Open,
                Accommodations = new List<Accommodation>()
                {
                    new Accommodation()
                    {
                        Description = "Test",
                        Duration = AccommodationDuration.Temporary,
                        Type = AccommodationType.GetByCode("EQUIPMENTORSOFTWARE"),
                        Usage = new List<AccommodationUsage>()
                        {
                            new AccommodationUsage()
                            {
                                StartDate = new DateTime(2014, 10, 1).ToMidnight(),
                                EndDate = new DateTime(2014, 10, 15).ToMidnight(),
                                Determination = AdjudicationStatus.Approved
                            }
                        }
                    }
                }
            };
            var accom = myCase.AccommodationRequest.Accommodations.First();
            using (AccommodationService svc = new AccommodationService())
            {
                myCase = svc.ApplyDetermination(myCase,
                    new DateTime(2014, 10, 1).ToMidnight(),
                    new DateTime(2014, 10, 7).ToMidnight(),
                    AdjudicationStatus.Denied, AccommodationAdjudicationDenialReason.EssentialJobFunctionCanNotAccommodate, null, AccommodationAdjudicationDenialReason.EssentialJobFunctionCanNotAccommodateDescription,
                    accom.Id);
            }
            Assert.AreEqual(2, accom.Usage.Count);
            //Assert.AreEqual(AdjudicationStatus.Approved, accom.Determination);
            Assert.AreEqual(new DateTime(2014, 10, 1).ToMidnight(), accom.StartDate);
            Assert.AreEqual(new DateTime(2014, 10, 15).ToMidnight(), accom.EndDate);

            AccommodationUsage approvedUsage = accom.Usage.Where(x => x.Determination == AdjudicationStatus.Approved).First();
            AccommodationUsage deniedUsage = accom.Usage.Where(x => x.Determination == AdjudicationStatus.Denied).First();

            //should be denied 10/1 - 10/7
            Assert.AreEqual(new DateTime(2014, 10, 1).ToMidnight(), deniedUsage.StartDate);
            Assert.AreEqual(new DateTime(2014, 10, 7).ToMidnight(), deniedUsage.EndDate);

            //approved 10/8 - 10/15
            Assert.AreEqual(new DateTime(2014, 10, 8).ToMidnight(), approvedUsage.StartDate);
            Assert.AreEqual(new DateTime(2014, 10, 15).ToMidnight(), approvedUsage.EndDate);
        }

        #endregion

        #region Permanent

        [TestMethod]
        [TestCategory("Accommodations.Determination")]
        public void AdjudicateAccomm_Permanent_PendingToApproved_ApprovalStartDateOnly_Test()
        {
            Case myCase = new Case()
            {
                EmployerId = "000000000000000000000002",
                CustomerId = "000000000000000000000002",
                Employee = Employee.AsQueryable().Where(e => e.EmployerId == "000000000000000000000001" && e.CustomerId == "000000000000000000000001").First(),
                CaseNumber = "ACCOMTEST4",
                Id = "123456789012345678901234",
                AssignedToId = "000000000000000000000000"
            };
            myCase.IsAccommodation = true;
            myCase.AccommodationRequest = new AccommodationRequest()
            {
                GeneralHealthCondition = "Test",
                Status = CaseStatus.Open,
                Accommodations = new List<Accommodation>()
                {
                    new Accommodation()
                    {
                        Description = "Test",
                        Duration = AccommodationDuration.Permanent,
                        Type = AccommodationType.GetByCode("EQUIPMENTORSOFTWARE"),
                        Usage = new List<AccommodationUsage>()
                        {
                            new AccommodationUsage()
                            {
                                StartDate = new DateTime(2014, 10, 1).ToMidnight(),
                                EndDate = new DateTime(2014, 10, 15).ToMidnight(),
                                Determination = AdjudicationStatus.Pending
                            }
                        }
                    }
                }
            };
            var accom = myCase.AccommodationRequest.Accommodations.First();
            using (AccommodationService svc = new AccommodationService())
            {
                myCase = svc.ApplyDetermination(myCase,
                    new DateTime(2014, 10, 1).ToMidnight(),
                    null,
                    AdjudicationStatus.Approved, null, null, null,
                    accom.Id);
            }
            Assert.AreEqual(1, accom.Usage.Count);
            Assert.AreEqual(AdjudicationStatus.Approved, accom.Determination);
            Assert.AreEqual(new DateTime(2014, 10, 1).ToMidnight(), accom.StartDate);
            Assert.AreEqual(new DateTime(2014, 10, 15).ToMidnight(), accom.EndDate);
        }

        #endregion

        [TestMethod]
        public void AccommodationInteractiveProcessTest()
        {
            string employerId = "000000000000000000000002";

            using (AccommodationService service = new AccommodationService())
            {
                AccommodationInteractiveProcess process = service.GetEmployerAccommodationInteractiveProcess(null, employerId);

                Assert.IsNotNull(process);
                Assert.IsNotNull(process.Steps.Any());
            }
        }

        [TestMethod]
        public void TestAccommodationTypeOrdering()
        {
            string EmployerId = "000000000000000000000002";
            string CustomerId = "000000000000000000000002";


            AbsenceSoft.Data.Customers.Customer currentCustomer = AbsenceSoft.Data.Customers.Customer.GetById("000000000000000000000002");

            if (!currentCustomer.HasFeature(Feature.AccommodationTypeCategories))
            {
                currentCustomer.AddFeature(Feature.AccommodationTypeCategories);
            }

            AbsenceSoft.Data.Customers.Employer currentEmployer = AbsenceSoft.Data.Customers.Employer.GetById("000000000000000000000002");


            using (var accommodationService = new AccommodationService(currentCustomer, currentEmployer, null))
            {

                AccommodationType accomodationType = new AccommodationType();
                accomodationType.Name = "Keyboard";
                accomodationType.EmployerId = EmployerId;
                accomodationType.CustomerId = CustomerId;
                accomodationType.Order = 1;
                accomodationType.Code = "KEY1";
                accomodationType.Categories = new SortedList<int, string>();
                accomodationType.Categories.Add(0, "Office");
                accomodationType.Categories.Add(1, "Equipment");
                accommodationService.SaveAccommodationType(accomodationType);

                accomodationType = new AccommodationType();
                accomodationType.Name = "Non Office Equipment";
                accomodationType.Order = 2;
                accomodationType.EmployerId = EmployerId;
                accomodationType.CustomerId = CustomerId;
                accomodationType.Code = "NOE";
                accommodationService.SaveAccommodationType(accomodationType);

                accomodationType = new AccommodationType();
                accomodationType.Name = "Chair";
                accomodationType.Order = 3;
                accomodationType.EmployerId = EmployerId;
                accomodationType.CustomerId = CustomerId;
                accomodationType.Code = "CHA1";
                accomodationType.Categories = new SortedList<int, string>();
                accomodationType.Categories.Add(0, "Office");
                accomodationType.Categories.Add(1, "Equipment");
                accommodationService.SaveAccommodationType(accomodationType);


                accomodationType = new AccommodationType();
                accomodationType.Name = "Parking ";
                accomodationType.EmployerId = EmployerId;
                accomodationType.CustomerId = CustomerId;
                accomodationType.Order = 4;
                accomodationType.Code = "PARKING";
                accomodationType.Categories = new SortedList<int, string>();
                accomodationType.Categories.Add(0, "Arrival");
                accommodationService.SaveAccommodationType(accomodationType);


                var rootCategories = accommodationService.GetAccommodationTypes();


                Assert.IsNotNull(rootCategories);
                Assert.IsNotNull(rootCategories.Any());
             

            }
        }

        [TestMethod]
        public void TestAccommodationCategoryFeature()
        {
            string EmployerId = "000000000000000000000002";
            string CustomerId = "000000000000000000000002";


            AbsenceSoft.Data.Customers.Customer currentCustomer = AbsenceSoft.Data.Customers.Customer.GetById(CustomerId);
            if (currentCustomer.HasFeature(Feature.AccommodationTypeCategories))
            {
                currentCustomer.RemoveFeature(Feature.AccommodationTypeCategories);
            }

            AbsenceSoft.Data.Customers.Employer currentEmployer = AbsenceSoft.Data.Customers.Employer.GetById(EmployerId);


            using (var accommodationService = new AccommodationService(currentCustomer, currentEmployer, null))
            {

                var rootCategories = accommodationService.GetAccommodationTypes();

                Assert.IsNotNull(rootCategories);
                Assert.IsNotNull(rootCategories.Any());
                var childCategory = rootCategories.Where(c => c.Children != null && c.Children.Any(m => !string.IsNullOrEmpty(m.Name))).ToList();
                Assert.AreEqual(childCategory.Count, 0);

            }
        }

        [TestMethod]
        public void TestAccommodationTypeReOrdering()
        {
            string EmployerId = "000000000000000000000002";
            string CustomerId = "000000000000000000000002";


            AbsenceSoft.Data.Customers.Customer currentCustomer = AbsenceSoft.Data.Customers.Customer.GetById("000000000000000000000002");

            if (!currentCustomer.HasFeature(Feature.AccommodationTypeCategories))
            {
                currentCustomer.AddFeature(Feature.AccommodationTypeCategories);
            }

            AbsenceSoft.Data.Customers.Employer currentEmployer = AbsenceSoft.Data.Customers.Employer.GetById("000000000000000000000002");


            using (var accommodationService = new AccommodationService(currentCustomer, currentEmployer, null))
            {
                

                AccommodationType accomodationType = new AccommodationType();
                accomodationType.Name = "Keyboard";
                accomodationType.EmployerId = EmployerId;
                accomodationType.CustomerId = CustomerId;
                accomodationType.Order = 1;
                accomodationType.Code = "KEY1";
                accomodationType.Categories = new SortedList<int, string>();
                accomodationType.Categories.Add(0, "Office");
                accomodationType.Categories.Add(1, "Equipment");
                accommodationService.SaveAccommodationType(accomodationType);

                accomodationType = new AccommodationType();
                accomodationType.Name = "Non Office Equipment";
                accomodationType.Order = 2;
                accomodationType.EmployerId = EmployerId;
                accomodationType.CustomerId = CustomerId;
                accomodationType.Code = "NOE";
                accommodationService.SaveAccommodationType(accomodationType);

                accomodationType = new AccommodationType();
                accomodationType.Name = "Chair";
                accomodationType.Order = 3;
                accomodationType.EmployerId = EmployerId;
                accomodationType.CustomerId = CustomerId;
                accomodationType.Code = "CHA1";
                accomodationType.Categories = new SortedList<int, string>();
                accomodationType.Categories.Add(0, "Office");
                accomodationType.Categories.Add(1, "Equipment");
                accommodationService.SaveAccommodationType(accomodationType);


                accomodationType = new AccommodationType();
                accomodationType.Name = "Parking ";
                accomodationType.EmployerId = EmployerId;
                accomodationType.CustomerId = CustomerId;
                accomodationType.Order = 4;
                accomodationType.Code = "PARKING";
                accomodationType.Categories = new SortedList<int, string>();
                accomodationType.Categories.Add(0, "Arrival");
                accommodationService.SaveAccommodationType(accomodationType);


                var rootCategories = accommodationService.GetAccommodationTypes();


                Assert.IsNotNull(rootCategories);
                Assert.IsNotNull(rootCategories.Any());
                Assert.AreEqual("Office", rootCategories[5].Name.Trim());
                Assert.AreEqual("Non Office Equipment", rootCategories[6].Name.Trim());
                Assert.AreEqual("Arrival", rootCategories[7].Name.Trim());
             

            }
        }
    }
}

