﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Logic.Cases;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Tests.Services.Cases
{
    [TestClass]
    public class ScheduleCalcTest
    {
        [TestMethod, Ignore]
      
        public void WorkWeeksUsedCalculatesWhenStartDayOfWeekIsChanged()
        {
            // Get an employee
            var absenceSoftTestEmployerId = "000000000000000000000002";

            var startDate = new DateTime(2017, 3, 1);
            Schedule elevenDaySchedule = new Schedule()
            {
                StartDate = startDate,
                ScheduleType = ScheduleType.Rotating
            };

            List<Time> sampleSchedule = new List<Time>(11);
            // make them work on an 11 day rotating schedule
            for (int i = 0; i < 11; i++)
            {
                int totalMinutes = 0;
                // 8 hours worked on day 2, 3, 4, 8
                if (i == 1 || i == 2 || i == 3 || i == 7)
                {
                    totalMinutes = 8 * 60;
                }
                // 4 hours worked on day 9
                else if (i == 8)
                {
                    totalMinutes = 4 * 60;
                }


                sampleSchedule.Add(new Time() { TotalMinutes = totalMinutes, SampleDate = elevenDaySchedule.StartDate.AddDays(i) });
            }
            elevenDaySchedule.Times = sampleSchedule;

            // create an employee so we aren't mucking with other employees being used
            var employee = new Employee()
            {
                EmployerId = absenceSoftTestEmployerId,
                CustomerId = absenceSoftTestEmployerId,
                EmployeeNumber = "ScheduleCalcTest",
                LastName = "CalcTest",
                FirstName = "Schedule",
                WorkState = "FL",
                ServiceDate = startDate,
                HireDate = startDate,
                RehireDate = null,
                PriorHours = new List<PriorHours>(1) { new PriorHours() { AsOf = DateTime.UtcNow.ToMidnight(), HoursWorked = 2040 } },
                Status = EmploymentStatus.Active,
                IsExempt = false,
                Gender = Gender.Male,
                Salary = 50000,
                PayType = PayType.Salary,
                IsKeyEmployee = false,
                Meets50In75MileRule = true,
                SpouseEmployeeNumber = null,
                StartDayOfWeek = DayOfWeek.Thursday,
                WorkSchedules = new List<Schedule>() { elevenDaySchedule },
                CreatedById = "000000000000000000000000",
                ModifiedById = "000000000000000000000000",
                Info = new EmployeeInfo { Email = "schedulecalctest@absencesoft.com" }
            };

            employee.Save();

            // Create an intermittent case from march 1st to march 11th, so it will encompass their whole schedule
            var endDate = new DateTime(2017, 3, 11);

            // run calcs
            var reason = AbsenceReason.GetByCode("EHC");
            using (CaseService caseSvc = new CaseService(false))
            using (EligibilityService eligSvc = new EligibilityService())
            {
                Case c = caseSvc.CreateCase(CaseStatus.Open, employee.Id, startDate, endDate, CaseType.Intermittent, reason.Code);
                c = eligSvc.RunEligibility(c).Case;
                foreach (var policy in c.Segments.SelectMany(s => s.AppliedPolicies).Where(ap => ap.Status == EligibilityStatus.Ineligible))
                {
                    policy.Status = EligibilityStatus.Eligible;
                    foreach (var ruleGroup in policy.RuleGroups)
                    {
                        foreach (var rule in ruleGroup.Rules)
                        {
                            rule.OverrideFromResult = rule.Result;
                            rule.Overridden = true;
                            rule.Result = AppliedRuleEvalResult.Pass;
                            rule.OverrideValue = rule.Rule.RightExpression;
                            rule.OverrideNotes = "Override For Schedule Calc Test";
                            rule.Pass = true;
                        }
                    }
                }
                c = eligSvc.RunEligibility(c).Case;
                c.Save();

                // approve the FMLA policy
                c = caseSvc.ApplyDetermination(c, "FMLA", startDate, endDate, AdjudicationStatus.Approved);

                // add and approve an intermittent time request on 3/8/2017 for 8 hours
                IntermittentTimeRequest marchEighth = new IntermittentTimeRequest()
                {
                    RequestDate = new DateTime(2017, 3, 8),
                    TotalMinutes = 8 * 60,
                    Detail = new List<IntermittentTimeRequestDetail>()
                    {
                        new IntermittentTimeRequestDetail() {PolicyCode="FMLA", Approved = 8 *60 }
                    }
                };

                // add and approve an intermittent time request on 3/9/2017 for 4 hours
                IntermittentTimeRequest marchNinth = new IntermittentTimeRequest()
                {
                    RequestDate = new DateTime(2017, 3, 9),
                    TotalMinutes = 4 * 60,
                    Detail = new List<IntermittentTimeRequestDetail>()
                    {
                        new IntermittentTimeRequestDetail() {PolicyCode="FMLA", Approved = 4 *60 }
                    }
                };



                c = caseSvc.CreateOrModifyTimeOffRequest(c, new List<IntermittentTimeRequest> { marchEighth, marchNinth });
                c.Save();
                // get the employee policy summary
                var policySummary = caseSvc.GetEmployeePolicySummaryByCaseId(c.Id, new EligibilityStatus[] { EligibilityStatus.Eligible });

                var fmlaTimeUsed = policySummary.FirstOrDefault(ps => ps.PolicyCode == "FMLA");
                Assert.IsNotNull(fmlaTimeUsed);

                // find the fmla time used info, should be .5 weeks
                // If the schedule starts on Thursday, they have taken two days off, across two weeks, each with 4 days that they work
                Assert.AreEqual(.5, fmlaTimeUsed.TimeUsed);

                c.Delete();
            }
        }
    }
}
