﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Cases;

namespace AbsenceSoft.Tests.Services.Cases
{
    [TestClass, Ignore]
    public class DiagnosisGuidelinesServiceTest
    {
        [TestMethod]
        public void GetValidDiagnosisGuidelines()
        {
            string icdCode = "354.0";

            using (DiagnosisGuidelinesService svc = new DiagnosisGuidelinesService())
            {
                var guidelines = svc.GetDiagnosisGuidelines(icdCode);

                Assert.IsNotNull(guidelines);
                Assert.IsTrue(guidelines.Code == "354.0");
                Assert.IsNotNull(guidelines.RtwSummary);
                Assert.IsNotNull(guidelines.RtwBestPractices.Any());
                Assert.IsNotNull(guidelines.PhisicalTherapy.Any());
                Assert.IsNotNull(guidelines.Chiropractical.Any());
                Assert.IsNotNull(guidelines.ActivityModifications.Any());

                guidelines.Delete();
            }
        }

        [TestMethod]
        public void GetInvalidDiagnosisGuidelines()
        {
            string icdCode = "1";

            using (DiagnosisGuidelinesService svc = new DiagnosisGuidelinesService())
            {
                var guidelines = svc.GetDiagnosisGuidelines(icdCode);

                Assert.IsNull(guidelines);
            }
        }

        [TestMethod]
        public void ExpiredDiagnosisGuidelines()
        {
            string code = "100";
            string description = "Expired";

            DiagnosisGuidelines guidelines = new DiagnosisGuidelines() 
            { 
                Code = code, 
                Description = description, 
                ExpirationDate = DateTime.Now.AddDays(-1) 
            };
            guidelines.Save();

            using (DiagnosisGuidelinesService svc = new DiagnosisGuidelinesService())
            {
                guidelines = svc.GetDiagnosisGuidelines(code);

                Assert.IsTrue(guidelines.Description != description);
            }
        }
    }
}
