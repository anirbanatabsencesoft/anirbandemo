﻿using System;
using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Policies;
using System.Collections.Generic;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic;
using AbsenceSoft.Common;
using AbsenceSoft;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Logic.Pay;
using System.IO;
using System.Reflection;
using System.Web;
using System.Security.Principal;
using MongoDB.Bson;

namespace AbsenceSoft.Tests.Services.Cases
{
    [TestClass]
    public class CommunicationServiceTest
    {
        private TestContext testContextInstance;
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        private Document customerLogo = null;

        [Ignore]
        [TestMethod]
        public void EmailCommunicationLogoWhackedTest()
        {
            //
            // Get the customer and designate a logo for that customer, 'cause yeah.
            AbsenceSoft.Data.Customers.Customer cust = AbsenceSoft.Data.Customers.Customer.GetById("000000000000000000000002");
            using (CustomerFileService fs = new CustomerFileService())
            {
                var dir = new DirectoryInfo(Environment.CurrentDirectory);
                string path = dir.Parent.Parent.FullName;
                path = Path.Combine(path, @"AbsenceSoft\Content\Images", "AbsenceSoft-logo-200.png");
                var file = File.ReadAllBytes(path);
                customerLogo = fs.UploadFile(cust.Id, "AbsenceSoft-logo-200.png", file, "image/png");
                cust.LogoId = customerLogo.Id;
                cust.Save();
            }

            //
            // Create a case to play with
            Case.Delete(Case.Query.EQ(c => c.Employee.Id, "000000000000000000000003"));
            Case myCase = null;
            using (CaseService caseSvc = new CaseService(false))
            using (EligibilityService eligSvc = new EligibilityService())
            {
                myCase = caseSvc.CreateCase(CaseStatus.Open, "000000000000000000000003", new DateTime(2015, 8, 31, 0, 0, 0, 0, DateTimeKind.Utc), new DateTime(2015, 9, 18, 0, 0, 0, 0, DateTimeKind.Utc), CaseType.Consecutive, "EHC");
                myCase.AssignedToId = "000000000000000000000002";
                myCase.AssignedToName = "000000000000000000000002";
                eligSvc.RunEligibility(myCase);
                foreach (var policy in myCase.Segments[0].AppliedPolicies)
                {
                    policy.RuleGroups.ForEach(g => g.Rules.Where(r => r.Result != AppliedRuleEvalResult.Pass).ForEach(r =>
                    {
                        r.Overridden = true;
                        r.OverrideFromResult = r.Result;
                        r.OverrideValue = r.Rule.LeftExpression;
                        r.OverrideNotes = "Manually Overridden by System for Test";
                        r.Result = AppliedRuleEvalResult.Pass;
                    }));
                }
                eligSvc.RunEligibility(myCase);
                myCase.SetCaseEvent(CaseEventType.EstimatedReturnToWork, new DateTime(2015, 9, 18, 0, 0, 0, 0, DateTimeKind.Utc));
                myCase.Save();
                myCase = caseSvc.CaseClosed(myCase, new DateTime(2015, 9, 18, 0, 0, 0, 0, DateTimeKind.Utc), CaseClosureReason.ReturnToWork);
            }

            //
            // Set up our hack for an HTTP Context that the Communication Service requires
            if (HttpContext.Current == null)
            {
                var user = User.GetById("000000000000000000000002");
                HttpContext context = HttpContext.Current = new HttpContext(new HttpRequest(null, "http://tempuri.org", null), new HttpResponse(null));
                context.Request.Browser = new HttpBrowserCapabilities() { Capabilities = new Dictionary<string, string> { { "supportsEmptyStringInCookieValue", "false" }, { "cookies", "false" } } };
                AbsenceSoft.Data.Customers.Customer.Current = cust;
                context.User = new GenericPrincipal(new GenericIdentity(user.Id), new[] { ObjectId.Empty.ToString() });
                context.Items["__current_user__"] = user;
            }

            //
            // Generate the communication and send it
            Communication comm = null;
            using (CommunicationService svc = new CommunicationService())
            {
                comm = svc.CreateCommunication(myCase.Id, "CASECLOSE", CommunicationType.Email);
                comm.Recipients = new List<Contact>(1)
                {
                    new Contact()
                    {
                        Email = "test.buddy@absencetracker.com",
                        FirstName = "Test",
                        LastName = "Buddy"
                    }
                };
                comm = svc.SendCommunication(comm, null, "CASECLOSE");
            }

            // Always happy, always.
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void SameSexSpouseIsSpouseInLettersTest()
        {
            //
            // Get the customer and designate a logo for that customer, 'cause yeah.
            AbsenceSoft.Data.Customers.Customer cust = AbsenceSoft.Data.Customers.Customer.GetById("000000000000000000000002");

            //
            // Create a case to play with
            Case.Delete(Case.Query.EQ(c => c.Employee.Id, "000000000000000000000003"));
            Case myCase = null;
            using (CaseService caseSvc = new CaseService(false))
            using (EligibilityService eligSvc = new EligibilityService())
            {
                myCase = caseSvc.CreateCase(CaseStatus.Open, "000000000000000000000003", new DateTime(2015, 8, 31, 0, 0, 0, 0, DateTimeKind.Utc), new DateTime(2015, 9, 18, 0, 0, 0, 0, DateTimeKind.Utc), CaseType.Consecutive, "FHC");
                myCase.AssignedToId = "000000000000000000000002";
                myCase.AssignedToName = "000000000000000000000002";
                myCase.Contact = new EmployeeContact()
                {
                    ContactTypeCode = "SAMESEXSPOUSE",
                    ContactTypeName = "Same Sex Spouse",
                    CustomerId = cust.Id,
                    EmployerId = myCase.EmployerId,
                    EmployeeId = myCase.Employee.Id,
                    MilitaryStatus = MilitaryStatus.Civilian,
                    Contact = new Contact()
                    {
                        FirstName = "Tonya",
                        LastName = "LaBonya"
                    }
                }.Save();
                eligSvc.RunEligibility(myCase);
                foreach (var policy in myCase.Segments[0].AppliedPolicies)
                {
                    policy.RuleGroups.ForEach(g => g.Rules.Where(r => r.Result != AppliedRuleEvalResult.Pass).ForEach(r =>
                    {
                        r.Overridden = true;
                        r.OverrideFromResult = r.Result;
                        r.OverrideValue = r.Rule.LeftExpression;
                        r.OverrideNotes = "Manually Overridden by System for Test";
                        r.Result = AppliedRuleEvalResult.Pass;
                    }));
                }
                eligSvc.RunEligibility(myCase);
                myCase.SetCaseEvent(CaseEventType.EstimatedReturnToWork, new DateTime(2015, 9, 18, 0, 0, 0, 0, DateTimeKind.Utc));
                myCase.Save();
            }

            //
            // Set up our hack for an HTTP Context that the Communication Service requires
            if (HttpContext.Current == null)
            {
                var user = User.GetById("000000000000000000000002");
                HttpContext context = HttpContext.Current = new HttpContext(new HttpRequest(null, "http://tempuri.org", null), new HttpResponse(null));
                context.Request.Browser = new HttpBrowserCapabilities() { Capabilities = new Dictionary<string, string> { { "supportsEmptyStringInCookieValue", "false" }, { "cookies", "false" } } };
                AbsenceSoft.Data.Customers.Customer.Current = cust;
                context.User = new GenericPrincipal(new GenericIdentity(user.Id), new[] { ObjectId.Empty.ToString() });
                context.Items["__current_user__"] = user;
            }

            //
            // Generate the communication and send it
            Communication comm = null;
            using (CommunicationService svc = new CommunicationService())
                comm = svc.CreateCommunication(myCase.Id, "ELGNOTICE", CommunicationType.Mail);

            Assert.IsTrue(comm.Body.Contains(">Spouse<"));
            Assert.IsFalse(comm.Body.Contains(" Sex "));
        }

        [TestMethod]
        public void TestPaperworkDueDateAdjustmentDays()
        {
            //
            // Get the customer and designate a logo for that customer, 'cause yeah.
            AbsenceSoft.Data.Customers.Customer cust = AbsenceSoft.Data.Customers.Customer.GetById("000000000000000000000002");

            //
            // Create a case to play with
            Case.Delete(Case.Query.EQ(c => c.Employee.Id, "000000000000000000000003"));
            Case myCase = null;
            using (CaseService caseSvc = new CaseService(false))
            {
                myCase = caseSvc.CreateCase(CaseStatus.Open, "000000000000000000000003", new DateTime(2015, 8, 31, 0, 0, 0, 0, DateTimeKind.Utc), new DateTime(2015, 9, 18, 0, 0, 0, 0, DateTimeKind.Utc), CaseType.Consecutive, "EHC");
                myCase.Save();
            }

            //
            // Set up our hack for an HTTP Context that the Communication Service requires
            if (HttpContext.Current == null)
            {
                var user = User.GetById("000000000000000000000002");
                HttpContext context = HttpContext.Current = new HttpContext(new HttpRequest(null, "http://tempuri.org", null), new HttpResponse(null));
                context.Request.Browser = new HttpBrowserCapabilities() { Capabilities = new Dictionary<string, string> { { "supportsEmptyStringInCookieValue", "false" }, { "cookies", "false" } } };
                AbsenceSoft.Data.Customers.Customer.Current = cust;
                context.User = new GenericPrincipal(new GenericIdentity(user.Id), new[] { ObjectId.Empty.ToString() });
                context.Items["__current_user__"] = user;
            }

            //
            // Generate the communication
            Communication comm = null;
            using (CommunicationService svc = new CommunicationService())
            {
                comm = svc.CreateCommunication(myCase.Id, "ELGNOTICE", CommunicationType.Email);
            }
            var commPaperwork = comm.Paperwork.FirstOrDefault();
            Assert.IsNotNull(commPaperwork);
            var returnDateAdjustmentDays = Paperwork.GetByCode(commPaperwork.Paperwork.Code, myCase.CustomerId, myCase.EmployerId).ReturnDateAdjustmentDays;
            Assert.AreEqual(returnDateAdjustmentDays, commPaperwork.Paperwork.ReturnDateAdjustmentDays);
        }

        [TestCleanup]
        public void Cleanup()
        {
            if (customerLogo != null)
                customerLogo.Remove();
        }
    }
}
