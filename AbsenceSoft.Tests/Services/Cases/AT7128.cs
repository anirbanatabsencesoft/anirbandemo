﻿using AbsenceSoft;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Security;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Tests.ProdSupport
{
    [TestClass] 
    public class AT7128
    {
        [TestMethod]
        public void TestAT7128FmlaShowingThirteenWeeksUsed()
        {
            #region Case JSON
            BsonDocument caseBson = MongoDB.Bson.BsonDocument.Parse(@"{
                    ""_id"" : ObjectId(""54ff1a68a32aa00b402175e2""), 
    ""SHOriginalStartDate"" : ISODate(""2017-10-19T00:00:00.000Z""), 
    ""CaseMigrationDate"" : ISODate(""2018-08-03T00:00:00.000Z""), 
    ""CaseMigrationFile"" : ""0802_merged.txt"", 
    ""cdt"" : ISODate(""2017-07-18T00:00:00.000Z""), 
    ""cby"" : ObjectId(""000000000000000000000000""), 
    ""mdt"" : ISODate(""2018-08-03T00:00:00.000Z""), 
    ""mby"" : ObjectId(""000000000000000000000000""), 
    ""IsDeleted"" : false, 
    ""CustomerId"" : ObjectId(""000000000000000000000001""), 
    ""EmployerId"" : ObjectId(""000000000000000000000001""), 
    ""CaseNumber"" : ""2017 - 0107015"", 
    ""EmployerCaseNumber"" : ""2017 - 0107015"", 
    ""StartDate"" : ISODate(""2017-10-19T00:00:00.000Z""), 
    ""EndDate"" : ISODate(""2018-01-10T00:00:00.000Z""), 
    ""Segments"" : [
        {
            ""_id"" : ""904ef3bc-5c4c-451c-af3d-2f424a6adbaa"",
            ""StartDate"" : ISODate(""2017-10-19T00:00:00.000Z""),
            ""EndDate"" : ISODate(""2018-01-10T00:00:00.000Z""),
            ""Type"" : NumberInt(1),
            ""AppliedPolicies"" : [
                {
                    ""_id"" : ""96554778-7fab-44b2-91d0-1a584cc286ee"",
                    ""Policy"" : {
                        ""_id"" : ObjectId(""591db793c5f3460eb83030b9""),
                        ""cdt"" : ISODate(""2017-05-18T00:00:00.000Z""),
                        ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""),
                        ""mdt"" : ISODate(""2018-07-12T00:00:00.000Z""),
                        ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""),
                        ""IsDeleted"" : false,
                        ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""),
                        ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""),
                        ""Code"" : ""FMLA"",
                        ""Suppressed"" : false,
                        ""Name"" : ""Family Medical Leave Act"",
                        ""Description"" : null,
                        ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000Z""),
                        ""PolicyType"" : NumberInt(0),
                        ""RuleGroups"" : [
                            {
                                ""_id"" : ""b6b398ba-b219-4ce6-9bff-139c203261c3"",
                                ""Name"" : ""Do not auto add"",
                                ""Description"" : ""Manual Selection"",
                                ""Rules"" : [
                                    {
                                        ""_id"" : ""f8b34c30-aa20-4517-8df0-4298f8e55126"",
                                        ""Name"" : ""Always True"",
                                        ""Description"" : ""Always True"",
                                        ""LeftExpression"" : ""1()"", 
                                        ""Operator"" : ""=="", 
                                        ""RightExpression"" : ""1"", 
                                        ""NotRunOrError"" : true, 
                                        ""ErrorMessages"" : [

                                        ]
    }
                                ], 
                                ""SuccessType"" : NumberInt(0), 
                                ""HasBeenEvaluated"" : false, 
                                ""Pass"" : false, 
                                ""RuleGroupType"" : NumberInt(0), 
                                ""Entitlement"" : null, 
                                ""PaymentEntitlement"" : [

                                ]
}, 
                            {
                                ""_id"" : ""df8522d8-0356-4d8e-92de-7abeb7a201e6"", 
                                ""Name"" : ""Pay Code Physician"", 
                                ""Description"" : ""Pay Code Physician"", 
                                ""Rules"" : [
                                    {
                                        ""_id"" : ""041fda91-fa65-4197-aff5-871a0adce677"",
                                        ""Name"" : ""Pay Code Physician"",
                                        ""Description"" : ""Pay Code Physician"",
                                        ""LeftExpression"" : ""CaseCustomField('==', 'Yes', 'Is Physician - Executive')"",
                                        ""Operator"" : ""=="",
                                        ""RightExpression"" : ""true"",
                                        ""Result"" : NumberInt(-1),
                                        ""ActualValue"" : false,
                                        ""Fail"" : true,
                                        ""ErrorMessages"" : [

                                        ]
                                    }
                                ], 
                                ""SuccessType"" : NumberInt(0), 
                                ""HasBeenEvaluated"" : true, 
                                ""Pass"" : false, 
                                ""CrosswalkCode"" : ""0320"", 
                                ""CrosswalkTypeCode"" : ""PAYCODE"", 
                                ""RuleGroupType"" : NumberInt(4), 
                                ""Entitlement"" : null, 
                                ""PaymentEntitlement"" : [

                                ]
                            }, 
                            {
                                ""_id"" : ""40aececf-9269-483b-867a-d5333558a189"", 
                                ""Name"" : ""FMLA Eligibility"", 
                                ""Description"" : ""All of the following statements about the Employee must be true:"", 
                                ""Rules"" : [
                                    {
                                        ""_id"" : ""6dd00d61-14d8-402e-9f89-cdea7f29cf41"",
                                        ""Name"" : ""Hours Worked"",
                                        ""Description"" : ""Hours worked"",
                                        ""LeftExpression"" : ""TotalHoursWorkedLast12Months()"",
                                        ""Operator"" : "">="",
                                        ""RightExpression"" : ""1250"",
                                        ""NotRunOrError"" : true,
                                        ""ErrorMessages"" : [

                                        ]
                                    }
                                ], 
                                ""SuccessType"" : NumberInt(0), 
                                ""HasBeenEvaluated"" : false, 
                                ""Pass"" : false, 
                                ""RuleGroupType"" : NumberInt(1), 
                                ""Entitlement"" : null
                            }
                        ], 
                        ""AbsenceReasons"" : [
                            {
                                ""CaseTypeConsecutive"" : true,
                                ""CaseTypeIntermittent"" : true,
                                ""CaseTypeReduced"" : true,
                                ""CaseTypeAdministrative"" : false,
                                ""EntitlementTypeText"" : ""Work Weeks"",
                                ""IntermittentRestrictionsMinimumTimeUnitTypeText"" : """",
                                ""IntermittentRestrictionsMaximumTimeUnitTypeText"" : """",
                                ""IntermittentRestrictionsCalcTypeTypeText"" : """",
                                ""AllowRelapse"" : true,
                                ""RelapseAllowedTime"" : 30.0,
                                ""RelapseAllowedTimeUnit"" : NumberInt(2),
                                ""RelapseAllowSelection"" : true,
                                ""RelapseReevaluateEligibility"" : false,
                                ""RelapseUseLatestPolicyDefinition"" : false,
                                ""_id"" : ""1d65d6b0-9d6a-4cae-9e05-9f9329e1105e"",
                                ""ReasonId"" : ""000000000000000001000000"",
                                ""EntitlementType"" : NumberInt(1),
                                ""Entitlement"" : 12.0,
                                ""EliminationType"" : NumberInt(8),
                                ""CombinedForSpouses"" : false,
                                ""PeriodType"" : NumberInt(5),
                                ""Period"" : 12.0,
                                ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000Z""),
                                ""CaseTypes"" : NumberInt(7),
                                ""Paid"" : false,
                                ""PaymentTiers"" : [

                                ], 
                                ""RuleGroups"" : [
                                    {
                                        ""_id"" : ""287f4624-400b-48dd-a172-ccc71d688376"",
                                        ""Name"" : ""Relationship to Employee"",
                                        ""Description"" : ""One of the following statements about \""Relationship to Employee\"" must be true:"",
                                        ""Rules"" : [

                                        ], 
                                        ""SuccessType"" : NumberInt(1), 
                                        ""HasBeenEvaluated"" : false, 
                                        ""Pass"" : false, 
                                        ""RuleGroupType"" : NumberInt(0), 
                                        ""Entitlement"" : null
                                    }
                                ], 
                                ""WorkCountry"" : null, 
                                ""ResidenceState"" : null, 
                                ""ResidenceCountry"" : null, 
                                ""ShowType"" : NumberInt(3), 
                                ""PolicyEvents"" : [

                                ], 
                                ""UseWholeDollarAmountsOnly"" : false
                            }, 
                            {
                                ""CaseTypeConsecutive"" : true, 
                                ""CaseTypeIntermittent"" : true, 
                                ""CaseTypeReduced"" : true, 
                                ""CaseTypeAdministrative"" : false, 
                                ""EntitlementTypeText"" : ""Work Weeks"", 
                                ""IntermittentRestrictionsMinimumTimeUnitTypeText"" : """", 
                                ""IntermittentRestrictionsMaximumTimeUnitTypeText"" : """", 
                                ""IntermittentRestrictionsCalcTypeTypeText"" : """", 
                                ""AllowRelapse"" : true, 
                                ""RelapseAllowedTime"" : 30.0, 
                                ""RelapseAllowedTimeUnit"" : NumberInt(2), 
                                ""RelapseAllowSelection"" : true, 
                                ""RelapseReevaluateEligibility"" : false, 
                                ""RelapseUseLatestPolicyDefinition"" : false, 
                                ""_id"" : ""efdafea4-6e2c-4989-83ae-7f7451beea6d"", 
                                ""ReasonId"" : ""000000000000000001000001"", 
                                ""EntitlementType"" : NumberInt(1), 
                                ""Entitlement"" : 12.0, 
                                ""EliminationType"" : NumberInt(8), 
                                ""CombinedForSpouses"" : true, 
                                ""PeriodType"" : NumberInt(5), 
                                ""Period"" : 12.0, 
                                ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000Z""), 
                                ""CaseTypes"" : NumberInt(7), 
                                ""Paid"" : false, 
                                ""PaymentTiers"" : [

                                ], 
                                ""RuleGroups"" : [
                                    {
                                        ""_id"" : ""01452e2c-8b00-47d9-8804-e571eed12d72"",
                                        ""Name"" : ""Do not auto add"",
                                        ""Description"" : ""Do not auto add"",
                                        ""Rules"" : [
                                            {
                                                ""_id"" : ""85527755-8680-4a2a-b36f-cd09b8a1f768"",
                                                ""Name"" : ""Do not auto add"",
                                                ""Description"" : ""Do not auto add"",
                                                ""LeftExpression"" : ""Employee.EmployeeNumber"",
                                                ""Operator"" : ""=="",
                                                ""RightExpression"" : ""'***'"",
                                                ""NotRunOrError"" : true,
                                                ""ErrorMessages"" : [

                                                ]
                                            }
                                        ], 
                                        ""SuccessType"" : NumberInt(0), 
                                        ""HasBeenEvaluated"" : false, 
                                        ""Pass"" : false, 
                                        ""RuleGroupType"" : NumberInt(0), 
                                        ""Entitlement"" : null, 
                                        ""PaymentEntitlement"" : [

                                        ]
                                    }, 
                                    {
                                        ""_id"" : ""492c1d17-7002-4f31-8267-bca0169f6618"", 
                                        ""Name"" : ""Relationship to Employee"", 
                                        ""Description"" : ""One of the following statements about \""Relationship to Employee\"" must be true:"", 
                                        ""Rules"" : [

                                        ], 
                                        ""SuccessType"" : NumberInt(1), 
                                        ""HasBeenEvaluated"" : false, 
                                        ""Pass"" : false, 
                                        ""RuleGroupType"" : NumberInt(0), 
                                        ""Entitlement"" : null
                                    }, 
                                    {
                                        ""_id"" : ""77da60b6-0f4a-4ed0-a676-7991c8b7d156"", 
                                        ""Name"" : ""Relationship to Employee Outside of 'TX'"", 
                                        ""Description"" : ""One of the following statements about \""Relationship to Employee\"" OR Work State must be false:"", 
                                        ""Rules"" : [

                                        ], 
                                        ""SuccessType"" : NumberInt(3), 
                                        ""HasBeenEvaluated"" : false, 
                                        ""Pass"" : false, 
                                        ""RuleGroupType"" : NumberInt(0), 
                                        ""Entitlement"" : null
                                    }, 
                                    {
                                        ""_id"" : ""2f9ffbb5-5d6e-4bf5-ad46-5dda9583d712"", 
                                        ""Name"" : ""Relationship to Employee Outside of 'LA'"", 
                                        ""Description"" : ""One of the following statements about \""Relationship to Employee\"" OR Work State must be false:"", 
                                        ""Rules"" : [

                                        ], 
                                        ""SuccessType"" : NumberInt(3), 
                                        ""HasBeenEvaluated"" : false, 
                                        ""Pass"" : false, 
                                        ""RuleGroupType"" : NumberInt(0), 
                                        ""Entitlement"" : null
                                    }, 
                                    {
                                        ""_id"" : ""8d5c9ade-6ae4-437c-9b2e-61f91f5999e9"", 
                                        ""Name"" : ""Relationship to Employee Outside of 'MS'"", 
                                        ""Description"" : ""One of the following statements about \""Relationship to Employee\"" OR Work State must be false:"", 
                                        ""Rules"" : [

                                        ], 
                                        ""SuccessType"" : NumberInt(3), 
                                        ""HasBeenEvaluated"" : false, 
                                        ""Pass"" : false, 
                                        ""RuleGroupType"" : NumberInt(0), 
                                        ""Entitlement"" : null
                                    }
                                ], 
                                ""WorkCountry"" : null, 
                                ""ResidenceState"" : null, 
                                ""ResidenceCountry"" : null, 
                                ""ShowType"" : NumberInt(3), 
                                ""PolicyEvents"" : [

                                ], 
                                ""UseWholeDollarAmountsOnly"" : false
                            }, 
                            {
                                ""CaseTypeConsecutive"" : true, 
                                ""CaseTypeIntermittent"" : true, 
                                ""CaseTypeReduced"" : true, 
                                ""CaseTypeAdministrative"" : false, 
                                ""EntitlementTypeText"" : ""Work Weeks"", 
                                ""IntermittentRestrictionsMinimumTimeUnitTypeText"" : """", 
                                ""IntermittentRestrictionsMaximumTimeUnitTypeText"" : """", 
                                ""IntermittentRestrictionsCalcTypeTypeText"" : """", 
                                ""AllowRelapse"" : true, 
                                ""RelapseAllowedTime"" : 30.0, 
                                ""RelapseAllowedTimeUnit"" : NumberInt(2), 
                                ""RelapseAllowSelection"" : true, 
                                ""RelapseReevaluateEligibility"" : false, 
                                ""RelapseUseLatestPolicyDefinition"" : false, 
                                ""_id"" : ""01f09508-3a3f-4797-b2a3-1ab443804650"", 
                                ""ReasonId"" : ""000000000000000001000002"", 
                                ""EntitlementType"" : NumberInt(1), 
                                ""Entitlement"" : 12.0, 
                                ""EliminationType"" : NumberInt(8), 
                                ""CombinedForSpouses"" : false, 
                                ""PeriodType"" : NumberInt(5), 
                                ""Period"" : 12.0, 
                                ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000Z""), 
                                ""CaseTypes"" : NumberInt(7), 
                                ""Paid"" : false, 
                                ""PaymentTiers"" : [

                                ], 
                                ""RuleGroups"" : [
                                    {
                                        ""_id"" : ""5594edfa-897f-4ce5-8fb4-ba9418f95d9d"",
                                        ""Name"" : ""Leave would end 12 months from the Delivery / Expected Delivery Date"",
                                        ""Description"" : ""Leave must be taken within 12 months from the Delivery / Expected Delivery Date"",
                                        ""Rules"" : [

                                        ], 
                                        ""SuccessType"" : NumberInt(0), 
                                        ""HasBeenEvaluated"" : false, 
                                        ""Pass"" : false, 
                                        ""RuleGroupType"" : NumberInt(1), 
                                        ""Entitlement"" : null
                                    }
                                ], 
                                ""WorkCountry"" : null, 
                                ""ResidenceState"" : null, 
                                ""ResidenceCountry"" : null, 
                                ""Gender"" : NumberInt(70), 
                                ""ShowType"" : NumberInt(3), 
                                ""PolicyEvents"" : [
                                    {
                                        ""EventType"" : NumberInt(2),
                                        ""DateType"" : NumberInt(4),
                                        ""EventPeriod"" : NumberInt(12)
                                    }
                                ], 
                                ""UseWholeDollarAmountsOnly"" : false
                            }, 
                            {
                                ""CaseTypeConsecutive"" : true, 
                                ""CaseTypeIntermittent"" : true, 
                                ""CaseTypeReduced"" : true, 
                                ""CaseTypeAdministrative"" : false, 
                                ""EntitlementTypeText"" : ""Work Weeks"", 
                                ""IntermittentRestrictionsMinimumTimeUnitTypeText"" : """", 
                                ""IntermittentRestrictionsMaximumTimeUnitTypeText"" : """", 
                                ""IntermittentRestrictionsCalcTypeTypeText"" : """", 
                                ""AllowRelapse"" : true, 
                                ""RelapseAllowedTime"" : 30.0, 
                                ""RelapseAllowedTimeUnit"" : NumberInt(2), 
                                ""RelapseAllowSelection"" : true, 
                                ""RelapseReevaluateEligibility"" : false, 
                                ""RelapseUseLatestPolicyDefinition"" : false, 
                                ""_id"" : ""5bca47ef-f3ba-45af-b47f-cae0b668c95e"", 
                                ""ReasonId"" : ""000000000000000001000022"", 
                                ""EntitlementType"" : NumberInt(1), 
                                ""Entitlement"" : 12.0, 
                                ""EliminationType"" : NumberInt(8), 
                                ""CombinedForSpouses"" : true, 
                                ""PeriodType"" : NumberInt(5), 
                                ""Period"" : 12.0, 
                                ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000Z""), 
                                ""CaseTypes"" : NumberInt(7), 
                                ""Paid"" : false, 
                                ""PaymentTiers"" : [

                                ], 
                                ""RuleGroups"" : [
                                    {
                                        ""_id"" : ""6152cd4b-a40e-4fd1-86ec-31b2d5d4646d"",
                                        ""Name"" : ""Leave would end 12 months from the Delivery / Expected Delivery Date"",
                                        ""Description"" : ""Leave must be taken within 12 months from the Delivery / Expected Delivery Date"",
                                        ""Rules"" : [

                                        ], 
                                        ""SuccessType"" : NumberInt(0), 
                                        ""HasBeenEvaluated"" : false, 
                                        ""Pass"" : false, 
                                        ""RuleGroupType"" : NumberInt(1), 
                                        ""Entitlement"" : null
                                    }
                                ], 
                                ""WorkCountry"" : null, 
                                ""ResidenceState"" : null, 
                                ""ResidenceCountry"" : null, 
                                ""ShowType"" : NumberInt(3), 
                                ""PolicyEvents"" : [
                                    {
                                        ""EventType"" : NumberInt(2),
                                        ""DateType"" : NumberInt(4),
                                        ""EventPeriod"" : NumberInt(12)
                                    }
                                ], 
                                ""UseWholeDollarAmountsOnly"" : false
                            }, 
                            {
                                ""CaseTypeConsecutive"" : true, 
                                ""CaseTypeIntermittent"" : true, 
                                ""CaseTypeReduced"" : true, 
                                ""CaseTypeAdministrative"" : false, 
                                ""EntitlementTypeText"" : ""Work Weeks"", 
                                ""IntermittentRestrictionsMinimumTimeUnitTypeText"" : """", 
                                ""IntermittentRestrictionsMaximumTimeUnitTypeText"" : """", 
                                ""IntermittentRestrictionsCalcTypeTypeText"" : """", 
                                ""AllowRelapse"" : true, 
                                ""RelapseAllowedTime"" : 30.0, 
                                ""RelapseAllowedTimeUnit"" : NumberInt(2), 
                                ""RelapseAllowSelection"" : true, 
                                ""RelapseReevaluateEligibility"" : false, 
                                ""RelapseUseLatestPolicyDefinition"" : false, 
                                ""_id"" : ""a8f3f165-321d-4d10-9f4f-0dc3afcd0283"", 
                                ""ReasonId"" : ""000000000000000001000004"", 
                                ""EntitlementType"" : NumberInt(1), 
                                ""Entitlement"" : 26.0, 
                                ""EliminationType"" : NumberInt(8), 
                                ""CombinedForSpouses"" : true, 
                                ""PeriodType"" : NumberInt(5), 
                                ""Period"" : 12.0, 
                                ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000Z""), 
                                ""CaseTypes"" : NumberInt(7), 
                                ""Paid"" : false, 
                                ""PaymentTiers"" : [

                                ], 
                                ""RuleGroups"" : [
                                    {
                                        ""_id"" : ""b4e9b188-70eb-472a-8315-238c4eb8324a"",
                                        ""Name"" : ""Service Member's Military Status"", 
                                        ""Description"" : ""One of the following statements about \""Service Member's Military Status\"" must be true:"", 
                                        ""Rules"" : [
                                            {
                                                ""_id"" : ""ddeb80ff-b9cc-4b3b-9241-7c0907c1fda1"",
                                                ""Name"" : ""Veteran"",
                                                ""Description"" : ""Service member's military status is Veteran"", 
                                                ""LeftExpression"" : ""Case.Contact.MilitaryStatus"",
                                                ""Operator"" : ""=="",
                                                ""RightExpression"" : ""MilitaryStatus.ActiveDuty"",
                                                ""NotRunOrError"" : true,
                                                ""ErrorMessages"" : [

                                                ]
                                            }, 
                                            {
                                                ""_id"" : ""f215076a-79b9-436f-a874-c7a2d2b6de0a"", 
                                                ""Name"" : ""Active Duty"", 
                                                ""Description"" : ""Service member's military status is Active Duty"", 
                                                ""LeftExpression"" : ""Case.Contact.MilitaryStatus"", 
                                                ""Operator"" : ""=="", 
                                                ""RightExpression"" : ""MilitaryStatus.ActiveDuty"", 
                                                ""NotRunOrError"" : true, 
                                                ""ErrorMessages"" : [

                                                ]
                                            }
                                        ], 
                                        ""SuccessType"" : NumberInt(1), 
                                        ""HasBeenEvaluated"" : false, 
                                        ""Pass"" : false, 
                                        ""RuleGroupType"" : NumberInt(1), 
                                        ""Entitlement"" : null, 
                                        ""PaymentEntitlement"" : [

                                        ]
                                    }, 
                                    {
                                        ""_id"" : ""aa616347-981d-4869-99ef-752713c01217"", 
                                        ""Name"" : ""Service Member's Relationship to Employee"", 
                                        ""Description"" : ""One of the following statements about \""Service Member's Relationship to Employee\"" must be true:"", 
                                        ""Rules"" : [
                                            {
                                                ""_id"" : ""8e81378e-225a-4f8a-8818-6bd14a10f619"",
                                                ""Name"" : ""Not SELF"",
                                                ""Description"" : ""is not for the employee's SELF"", 
                                                ""LeftExpression"" : ""CaseRelationshipType"",
                                                ""Operator"" : ""=="",
                                                ""RightExpression"" : ""'SELF'"",
                                                ""NotRunOrError"" : true,
                                                ""ErrorMessages"" : [

                                                ]
                                            }
                                        ], 
                                        ""SuccessType"" : NumberInt(2), 
                                        ""HasBeenEvaluated"" : false, 
                                        ""Pass"" : false, 
                                        ""RuleGroupType"" : NumberInt(0), 
                                        ""Entitlement"" : null, 
                                        ""PaymentEntitlement"" : [

                                        ]
                                    }, 
                                    {
                                        ""_id"" : ""19dcb92b-f159-4c39-9332-fce4f8672a28"", 
                                        ""Name"" : ""Pay Code Military"", 
                                        ""Description"" : ""Pay Code Military"", 
                                        ""Rules"" : [
                                            {
                                                ""_id"" : ""849d034c-3472-4bf4-8bb5-372567eb6fee"",
                                                ""Name"" : ""Pay Code Military"",
                                                ""Description"" : ""Pay Code Military"",
                                                ""LeftExpression"" : ""Employee.EmployeeNumber"",
                                                ""Operator"" : ""!="",
                                                ""RightExpression"" : ""'***'"",
                                                ""Result"" : NumberInt(1),
                                                ""ActualValue"" : ""154049"",
                                                ""Pass"" : true,
                                                ""ErrorMessages"" : [

                                                ]
                                            }
                                        ], 
                                        ""SuccessType"" : NumberInt(0), 
                                        ""HasBeenEvaluated"" : true, 
                                        ""Pass"" : true, 
                                        ""CrosswalkCode"" : ""0371"", 
                                        ""CrosswalkTypeCode"" : ""PAYCODE"", 
                                        ""RuleGroupType"" : NumberInt(4), 
                                        ""Entitlement"" : null, 
                                        ""PaymentEntitlement"" : [

                                        ]
                                    }
                                ], 
                                ""WorkCountry"" : null, 
                                ""ResidenceState"" : null, 
                                ""ResidenceCountry"" : null, 
                                ""ShowType"" : NumberInt(3), 
                                ""PolicyEvents"" : [

                                ], 
                                ""UseWholeDollarAmountsOnly"" : false
                            }, 
                            {
                                ""CaseTypeConsecutive"" : true, 
                                ""CaseTypeIntermittent"" : true, 
                                ""CaseTypeReduced"" : true, 
                                ""CaseTypeAdministrative"" : false, 
                                ""EntitlementTypeText"" : ""Work Weeks"", 
                                ""IntermittentRestrictionsMinimumTimeUnitTypeText"" : """", 
                                ""IntermittentRestrictionsMaximumTimeUnitTypeText"" : """", 
                                ""IntermittentRestrictionsCalcTypeTypeText"" : """", 
                                ""AllowRelapse"" : true, 
                                ""RelapseAllowedTime"" : 30.0, 
                                ""RelapseAllowedTimeUnit"" : NumberInt(2), 
                                ""RelapseAllowSelection"" : true, 
                                ""RelapseReevaluateEligibility"" : false, 
                                ""RelapseUseLatestPolicyDefinition"" : false, 
                                ""_id"" : ""bb5a250f-989c-409f-84e2-bcd5e8d7bc0b"", 
                                ""ReasonId"" : ""000000000000000001000005"", 
                                ""EntitlementType"" : NumberInt(1), 
                                ""Entitlement"" : 12.0, 
                                ""EliminationType"" : NumberInt(8), 
                                ""PerUseCap"" : 12.0, 
                                ""PerUseCapType"" : NumberInt(1), 
                                ""CombinedForSpouses"" : false, 
                                ""PeriodType"" : NumberInt(5), 
                                ""Period"" : 12.0, 
                                ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000Z""), 
                                ""CaseTypes"" : NumberInt(7), 
                                ""Paid"" : false, 
                                ""PaymentTiers"" : [

                                ], 
                                ""RuleGroups"" : [
                                    {
                                        ""_id"" : ""31903daf-fbc6-40af-97ab-8ad6bdfbbdcb"",
                                        ""Name"" : ""Pay Code"",
                                        ""Description"" : ""Pay Code Military"",
                                        ""Rules"" : [
                                            {
                                                ""_id"" : ""d6f70dd8-a4a4-4dd5-a0af-a0b7eb7f6ab7"",
                                                ""Name"" : ""Always True"",
                                                ""Description"" : ""Always True"",
                                                ""LeftExpression"" : ""Employee.EmployeeNumber"",
                                                ""Operator"" : ""!="",
                                                ""RightExpression"" : ""'***'"",
                                                ""Result"" : NumberInt(1),
                                                ""ActualValue"" : ""115022"",
                                                ""Pass"" : true,
                                                ""ErrorMessages"" : [

                                                ]
                                            }
                                        ], 
                                        ""SuccessType"" : NumberInt(0), 
                                        ""HasBeenEvaluated"" : true, 
                                        ""Pass"" : true, 
                                        ""CrosswalkCode"" : ""0372"", 
                                        ""CrosswalkTypeCode"" : ""PAYCODE"", 
                                        ""RuleGroupType"" : NumberInt(4), 
                                        ""Entitlement"" : null, 
                                        ""PaymentEntitlement"" : [

                                        ]
                                    }, 
                                    {
                                        ""_id"" : ""752943b9-e40f-4dc5-bb71-619d044ed055"", 
                                        ""Name"" : ""Relationship to Employee"", 
                                        ""Description"" : ""One of the following statements about \""Relationship to Employee\"" must be true:"", 
                                        ""Rules"" : [
                                            {
                                                ""_id"" : ""ab8098a9-2eab-4d8f-b6ba-55065cc51b9d"",
                                                ""Name"" : ""Relationship To Employee Rule"",
                                                ""Description"" : ""Relationship To Employee Rule"",
                                                ""LeftExpression"" : ""CaseRelationshipType"",
                                                ""Operator"" : ""::"",
                                                ""RightExpression"" : ""'SPOUSE,PARENT,CHILD'"",
                                                ""NotRunOrError"" : true,
                                                ""ErrorMessages"" : [

                                                ]
                                            }
                                        ], 
                                        ""SuccessType"" : NumberInt(0), 
                                        ""HasBeenEvaluated"" : false, 
                                        ""Pass"" : false, 
                                        ""RuleGroupType"" : NumberInt(1), 
                                        ""Entitlement"" : null, 
                                        ""PaymentEntitlement"" : [

                                        ]
                                    }, 
                                    {
                                        ""_id"" : ""6a805f81-f5e3-4ebb-a6b1-94f0c9f35a24"", 
                                        ""Name"" : ""Service Member's Military Status"", 
                                        ""Description"" : ""One of the following statements about \""Service Member's Military Status\"" must be true:"", 
                                        ""Rules"" : [
                                            {
                                                ""_id"" : ""3d8bc3b2-87bc-4cc5-af95-5cc696830936"",
                                                ""Name"" : ""Active Duty"",
                                                ""Description"" : ""Service member's military status is Active Duty"", 
                                                ""LeftExpression"" : ""Case.Contact.MilitaryStatus"",
                                                ""Operator"" : ""=="",
                                                ""RightExpression"" : ""MilitaryStatus.ActiveDuty"",
                                                ""NotRunOrError"" : true,
                                                ""ErrorMessages"" : [

                                                ]
                                            }
                                        ], 
                                        ""SuccessType"" : NumberInt(0), 
                                        ""HasBeenEvaluated"" : false, 
                                        ""Pass"" : false, 
                                        ""RuleGroupType"" : NumberInt(1), 
                                        ""Entitlement"" : null
                                    }
                                ], 
                                ""WorkCountry"" : null, 
                                ""ResidenceState"" : null, 
                                ""ResidenceCountry"" : null, 
                                ""ShowType"" : NumberInt(3), 
                                ""PolicyEvents"" : [

                                ], 
                                ""UseWholeDollarAmountsOnly"" : false
                            }, 
                            {
                                ""CaseTypeConsecutive"" : true, 
                                ""CaseTypeIntermittent"" : true, 
                                ""CaseTypeReduced"" : true, 
                                ""CaseTypeAdministrative"" : false, 
                                ""EntitlementTypeText"" : ""Work Weeks"", 
                                ""IntermittentRestrictionsMinimumTimeUnitTypeText"" : """", 
                                ""IntermittentRestrictionsMaximumTimeUnitTypeText"" : """", 
                                ""IntermittentRestrictionsCalcTypeTypeText"" : """", 
                                ""AllowRelapse"" : true, 
                                ""RelapseAllowedTime"" : 30.0, 
                                ""RelapseAllowedTimeUnit"" : NumberInt(2), 
                                ""RelapseAllowSelection"" : true, 
                                ""RelapseReevaluateEligibility"" : false, 
                                ""RelapseUseLatestPolicyDefinition"" : false, 
                                ""_id"" : ""1f8f77b5-5027-4ed4-b078-58bf703590ec"", 
                                ""ReasonId"" : ""598df693c5f3553dc8bafa4e"", 
                                ""EntitlementType"" : NumberInt(1), 
                                ""Entitlement"" : 12.0, 
                                ""CombinedForSpouses"" : true, 
                                ""PeriodType"" : NumberInt(5), 
                                ""EffectiveDate"" : ISODate(""2017-07-12T00:00:00.000Z""), 
                                ""CaseTypes"" : NumberInt(7), 
                                ""Paid"" : false, 
                                ""PaymentTiers"" : [

                                ], 
                                ""RuleGroups"" : [
                                    {
                                        ""_id"" : ""6c316478-e938-409a-b678-8c65c741a501"",
                                        ""Name"" : ""Do not auto add"",
                                        ""Description"" : ""Do not auto add"",
                                        ""Rules"" : [
                                            {
                                                ""_id"" : ""ff7c4ad1-2620-41ef-a3a0-8b895b1baf66"",
                                                ""Name"" : ""Never True"",
                                                ""Description"" : ""Never True"",
                                                ""LeftExpression"" : ""Employee.EmployeeNumber"",
                                                ""Operator"" : ""=="",
                                                ""RightExpression"" : ""'***'"",
                                                ""NotRunOrError"" : true,
                                                ""ErrorMessages"" : [

                                                ]
                                            }
                                        ], 
                                        ""SuccessType"" : NumberInt(0), 
                                        ""HasBeenEvaluated"" : false, 
                                        ""Pass"" : false, 
                                        ""RuleGroupType"" : NumberInt(0), 
                                        ""Entitlement"" : null, 
                                        ""PaymentEntitlement"" : [

                                        ]
                                    }
                                ], 
                                ""WorkCountry"" : null, 
                                ""ResidenceState"" : null, 
                                ""ResidenceCountry"" : null, 
                                ""ShowType"" : NumberInt(3), 
                                ""PolicyEvents"" : [

                                ], 
                                ""AllowOffset"" : false, 
                                ""UseWholeDollarAmountsOnly"" : false
                            }
                        ], 
                        ""WorkCountry"" : ""US"", 
                        ""WorkState"" : null, 
                        ""PolicyBehaviorType"" : NumberInt(1), 
                        ""ResidenceCountry"" : ""US"", 
                        ""ResidenceState"" : null, 
                        ""Gender"" : null, 
                        ""ConsecutiveTo"" : [

                        ]
                    }, 
                    ""PolicyCrosswalkCodes"" : [

                    ], 
                    ""PolicyReason"" : {
                        ""CaseTypeConsecutive"" : true, 
                        ""CaseTypeIntermittent"" : true, 
                        ""CaseTypeReduced"" : true, 
                        ""CaseTypeAdministrative"" : false, 
                        ""EntitlementTypeText"" : ""Work Weeks"", 
                        ""IntermittentRestrictionsMinimumTimeUnitTypeText"" : """", 
                        ""IntermittentRestrictionsMaximumTimeUnitTypeText"" : """", 
                        ""IntermittentRestrictionsCalcTypeTypeText"" : """", 
                        ""AllowRelapse"" : true, 
                        ""RelapseAllowedTime"" : 30.0, 
                        ""RelapseAllowedTimeUnit"" : NumberInt(2), 
                        ""RelapseAllowSelection"" : true, 
                        ""RelapseReevaluateEligibility"" : false, 
                        ""RelapseUseLatestPolicyDefinition"" : false, 
                        ""_id"" : ""1d65d6b0-9d6a-4cae-9e05-9f9329e1105e"", 
                        ""ReasonId"" : ""000000000000000001000000"", 
                        ""EntitlementType"" : NumberInt(1), 
                        ""Entitlement"" : 12.0, 
                        ""EliminationType"" : NumberInt(8), 
                        ""CombinedForSpouses"" : false, 
                        ""PeriodType"" : NumberInt(5), 
                        ""Period"" : 12.0, 
                        ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000Z""), 
                        ""CaseTypes"" : NumberInt(7), 
                        ""Paid"" : false, 
                        ""PaymentTiers"" : [

                        ], 
                        ""RuleGroups"" : [
                            {
                                ""_id"" : ""287f4624-400b-48dd-a172-ccc71d688376"",
                                ""Name"" : ""Relationship to Employee"",
                                ""Description"" : ""One of the following statements about \""Relationship to Employee\"" must be true:"",
                                ""Rules"" : [

                                ], 
                                ""SuccessType"" : NumberInt(1), 
                                ""HasBeenEvaluated"" : false, 
                                ""Pass"" : false, 
                                ""RuleGroupType"" : NumberInt(0), 
                                ""Entitlement"" : null
                            }
                        ], 
                        ""WorkCountry"" : null, 
                        ""ResidenceState"" : null, 
                        ""ResidenceCountry"" : null, 
                        ""ShowType"" : NumberInt(3), 
                        ""PolicyEvents"" : [

                        ], 
                        ""UseWholeDollarAmountsOnly"" : false
                    }, 
                    ""Status"" : NumberInt(1), 
                    ""StartDate"" : ISODate(""2017-10-19T00:00:00.000Z""), 
                    ""EndDate"" : ISODate(""2018-01-10T00:00:00.000Z""), 
                    ""Usage"" : [
                        {
                            ""_id"" : ""e5fa544b-2182-45a3-97dd-155e95aa24e5"",
                            ""DateUsed"" : ISODate(""2018-01-10T00:00:00.000Z""),
                            ""MinutesUsed"" : 720.0,
                            ""MinutesInDay"" : 720.0,
                            ""PolicyReasonId"" : ""000000000000000001000000"",
                            ""UserEntered"" : true,
                            ""Determination"" : NumberInt(1),
                            ""IsIntermittentRestriction"" : false,
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 2.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""50ba3c36-5b2f-49dc-ba76-4a4733b9fae1"", 
                            ""DateUsed"" : ISODate(""2018-01-09T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 2.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""d9f0c213-c3ee-4768-8589-715623ce80c4"", 
                            ""DateUsed"" : ISODate(""2018-01-08T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 2.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""da929ac7-517e-4449-8d42-f67901594baa"", 
                            ""DateUsed"" : ISODate(""2018-01-07T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 2.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""32bb4892-80a9-4dd6-abab-e800a3c2cccf"", 
                            ""DateUsed"" : ISODate(""2018-01-06T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""d36a6e8a-8a94-46a6-95b5-0e1e2682b7b4"", 
                            ""DateUsed"" : ISODate(""2018-01-05T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""2b990860-505d-4a59-8d4c-d7b83e67590a"", 
                            ""DateUsed"" : ISODate(""2018-01-04T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""c1eacdb2-d5ae-422c-847b-5211e96de729"", 
                            ""DateUsed"" : ISODate(""2018-01-03T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""e726dfb6-9172-4bdd-9554-deb6811befd9"", 
                            ""DateUsed"" : ISODate(""2018-01-02T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""8615876a-48ee-4600-baa6-d16a917647c2"", 
                            ""DateUsed"" : ISODate(""2018-01-01T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""a42f7000-03cc-48a4-a4a0-c61832af003c"", 
                            ""DateUsed"" : ISODate(""2017-12-31T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""62f86aa1-0ccc-42f0-a9b8-2486fd5c0961"", 
                            ""DateUsed"" : ISODate(""2017-12-30T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""e3ed50c6-53a5-4d1d-bd57-ef55ec0c9d72"", 
                            ""DateUsed"" : ISODate(""2017-12-29T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""2089a5c7-3d91-4d35-a495-612b74c80cff"", 
                            ""DateUsed"" : ISODate(""2017-12-28T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""415435e4-815f-4aee-bec4-a4dc8a26fdd6"", 
                            ""DateUsed"" : ISODate(""2017-12-27T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""ec020bf4-3b6b-462d-a69c-737417d4b65d"", 
                            ""DateUsed"" : ISODate(""2017-12-26T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""c6c89c62-4c30-4864-ab9a-dfae100d9d85"", 
                            ""DateUsed"" : ISODate(""2017-12-25T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""5f73e39b-0609-45bd-af4e-7003c7957f1c"", 
                            ""DateUsed"" : ISODate(""2017-12-24T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""2b93e8ca-da19-4ff9-b565-62f00d11fe62"", 
                            ""DateUsed"" : ISODate(""2017-12-23T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""a86563e7-4474-4d36-859b-4562b3eb4afc"", 
                            ""DateUsed"" : ISODate(""2017-12-22T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""cca13b50-afb5-413b-abaa-11b1b2a4b1d2"", 
                            ""DateUsed"" : ISODate(""2017-12-21T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""75bf3ea5-0c26-458d-85b8-a6571348a52b"", 
                            ""DateUsed"" : ISODate(""2017-12-20T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""59cc4c55-a1ad-425a-ad3b-d223df3e20bd"", 
                            ""DateUsed"" : ISODate(""2017-12-19T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""7fab100a-d7a2-49fa-a826-6267ad980921"", 
                            ""DateUsed"" : ISODate(""2017-12-18T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""0f024d16-69f6-46a9-a02b-9056e754a99c"", 
                            ""DateUsed"" : ISODate(""2017-12-17T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""cdd9cf37-12e3-4ac9-ad76-367523cf88f2"", 
                            ""DateUsed"" : ISODate(""2017-12-16T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""08e071c8-ecd6-4d84-aa17-9aac61240b7c"", 
                            ""DateUsed"" : ISODate(""2017-12-15T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""8abed707-3933-4199-8bf4-609ad539947f"", 
                            ""DateUsed"" : ISODate(""2017-12-14T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""bddcaf1e-7951-4ed6-a21a-10e093b766f9"", 
                            ""DateUsed"" : ISODate(""2017-12-13T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""73f6ae2c-0f0e-46ad-9dfb-2879237668ba"", 
                            ""DateUsed"" : ISODate(""2017-12-12T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""ebc21c46-30b2-46ae-96ba-e36ab2a4c061"", 
                            ""DateUsed"" : ISODate(""2017-12-11T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""e9a13bf3-753f-4a3d-85ce-f0fe803b9d78"", 
                            ""DateUsed"" : ISODate(""2017-12-10T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""083a7eff-f230-41ae-84c7-693d64e545db"", 
                            ""DateUsed"" : ISODate(""2017-12-09T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""109d06bb-a901-48c5-aefc-8335850ec56c"", 
                            ""DateUsed"" : ISODate(""2017-12-08T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""8b9544c5-73db-41d6-9238-381661ccc474"", 
                            ""DateUsed"" : ISODate(""2017-12-07T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""510fa810-786b-4184-9c53-ecc77e984fa1"", 
                            ""DateUsed"" : ISODate(""2017-12-06T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""717f6f28-fa41-4b19-8ecb-51e161c7a469"", 
                            ""DateUsed"" : ISODate(""2017-12-05T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""fa6351cb-24fc-48dd-9f40-d24b6b0b28c1"", 
                            ""DateUsed"" : ISODate(""2017-12-04T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""55c9d68b-0606-4b87-9ed4-24e64f169bee"", 
                            ""DateUsed"" : ISODate(""2017-12-03T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""659fd09c-c85c-4a8b-8e4b-720745bfba1d"", 
                            ""DateUsed"" : ISODate(""2017-12-02T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""ebad9724-48a9-4a54-adfb-ea3dc8e9e1d8"", 
                            ""DateUsed"" : ISODate(""2017-12-01T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 12.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""68d3ab51-5e12-4417-abd2-ddc5e72c0eb4"", 
                            ""DateUsed"" : ISODate(""2017-11-30T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""ee2451de-899a-4684-8b6a-9b76b57f9da7"", 
                            ""DateUsed"" : ISODate(""2017-11-29T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""404fd4d8-8afc-4979-b940-a293f5c3e53a"", 
                            ""DateUsed"" : ISODate(""2017-11-28T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""8541f101-bf06-4aef-b66f-96568f674e37"", 
                            ""DateUsed"" : ISODate(""2017-11-27T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""0b61802f-97cb-4192-9467-755875666ece"", 
                            ""DateUsed"" : ISODate(""2017-11-26T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""2c028f5c-e136-416a-90b6-e56052cd0481"", 
                            ""DateUsed"" : ISODate(""2017-11-25T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""2db51bec-8739-49a6-91db-9483366f3544"", 
                            ""DateUsed"" : ISODate(""2017-11-24T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""a70ad9cc-5d17-49af-a85b-a414b8b7fedc"", 
                            ""DateUsed"" : ISODate(""2017-11-23T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""cd194d47-0739-45db-87dc-c72c2e0e9f6c"", 
                            ""DateUsed"" : ISODate(""2017-11-22T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""4189ef5a-1f68-4cae-a7c6-ff19ac0e0462"", 
                            ""DateUsed"" : ISODate(""2017-11-21T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""07f59c32-4ef7-4be8-8a95-c82ce26f459f"", 
                            ""DateUsed"" : ISODate(""2017-11-20T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""4e44c9ed-bfcd-437d-901b-992bcfa6816e"", 
                            ""DateUsed"" : ISODate(""2017-11-19T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""74ab6484-5638-4e93-ab15-ae60ca6134a3"", 
                            ""DateUsed"" : ISODate(""2017-11-18T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""34e4b7d8-40d6-4bdc-a9a8-4b1801322c36"", 
                            ""DateUsed"" : ISODate(""2017-11-17T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""7af200de-8900-4678-b4ea-79c88ea86746"", 
                            ""DateUsed"" : ISODate(""2017-11-16T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""1570749d-2572-42ac-a97b-dabef3361677"", 
                            ""DateUsed"" : ISODate(""2017-11-15T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""79c04913-958c-4609-82b1-abfac938e263"", 
                            ""DateUsed"" : ISODate(""2017-11-14T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""05fd328e-5cd9-45b5-90bc-cb82978f8a71"", 
                            ""DateUsed"" : ISODate(""2017-11-13T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""c8927432-3c4d-4b5c-843f-9e97d5f6af6b"", 
                            ""DateUsed"" : ISODate(""2017-11-12T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""be597100-2801-4535-a000-479ccf380632"", 
                            ""DateUsed"" : ISODate(""2017-11-11T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""400d4c99-ff12-4666-b9b8-9c226f4f5638"", 
                            ""DateUsed"" : ISODate(""2017-11-10T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""e17e3b24-4bc8-4e5a-a0b2-03cc4b91b6cd"", 
                            ""DateUsed"" : ISODate(""2017-11-09T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""b50f31eb-7541-4f83-a035-94f26ea81f80"", 
                            ""DateUsed"" : ISODate(""2017-11-08T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""712c5094-b1ce-4f49-9679-692772bcad27"", 
                            ""DateUsed"" : ISODate(""2017-11-07T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""90867ad6-1807-489d-a36f-11707f549f94"", 
                            ""DateUsed"" : ISODate(""2017-11-06T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""63f01a45-1c8e-4e39-ae71-dc4467ada900"", 
                            ""DateUsed"" : ISODate(""2017-11-05T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""755542e0-a88b-459d-aa44-635eb67888f5"", 
                            ""DateUsed"" : ISODate(""2017-11-04T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""b5bf6c0a-852f-46fe-89ac-ffbb1910c927"", 
                            ""DateUsed"" : ISODate(""2017-11-03T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""71760d8b-1354-45f7-a78c-b89e6f217e49"", 
                            ""DateUsed"" : ISODate(""2017-11-02T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""fdfb8033-9e9c-4ae2-a246-d9f993aae7e7"", 
                            ""DateUsed"" : ISODate(""2017-11-01T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""41b0335a-16b8-42d3-bc01-a3b756ddaa29"", 
                            ""DateUsed"" : ISODate(""2017-10-31T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""150fdb07-37fb-477e-869e-3fd5c8e9ac2f"", 
                            ""DateUsed"" : ISODate(""2017-10-30T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""2e6576e7-7835-43c4-9c1a-802fb4c0d282"", 
                            ""DateUsed"" : ISODate(""2017-10-29T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""ed97adb1-08b9-432b-8708-9281d09d6573"", 
                            ""DateUsed"" : ISODate(""2017-10-28T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""8c7c11b3-af47-4ce5-9aa1-61b918858235"", 
                            ""DateUsed"" : ISODate(""2017-10-27T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""6563c1e5-92d5-4f6e-ac19-5870f61d6b74"", 
                            ""DateUsed"" : ISODate(""2017-10-26T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""2243ba8a-69be-423f-8681-05e87c599ba9"", 
                            ""DateUsed"" : ISODate(""2017-10-25T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""4e9c9200-040e-4327-ad59-6594d87929bc"", 
                            ""DateUsed"" : ISODate(""2017-10-24T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""5bcf304f-4435-4c75-9cdc-44134186fa99"", 
                            ""DateUsed"" : ISODate(""2017-10-23T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""30ec01cb-134e-47b6-993e-d83b6a22b925"", 
                            ""DateUsed"" : ISODate(""2017-10-22T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""b349c5b0-ed34-4c3f-a246-62dbac709f79"", 
                            ""DateUsed"" : ISODate(""2017-10-21T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""f31746d9-1c68-418e-8429-e3affc6ef315"", 
                            ""DateUsed"" : ISODate(""2017-10-20T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""bc1a0bd5-7f59-4cfa-9953-d84d7715469e"", 
                            ""DateUsed"" : ISODate(""2017-10-19T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }
                    ], 
                    ""RuleGroups"" : [

                    ], 
                    ""ManuallyAdded"" : true, 
                    ""PolicyDateOverridden"" : {
                        ""_id"" : ""68feaf72-aa4c-4bd0-804a-d50f48ce45ab""
                    }, 
                    ""ManuallyAddedNote"" : ""Case Conversion""
                }, 
                {
                    ""_id"" : ""6238f480-aeb8-407f-911d-6a52200b85de"", 
                    ""Policy"" : {
                        ""_id"" : ObjectId(""58eea1cbc5f3360818dc69f8""), 
                        ""cdt"" : ISODate(""2017-04-12T00:00:00.000Z""), 
                        ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                        ""mdt"" : ISODate(""2017-09-13T00:00:00.000Z""), 
                        ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                        ""IsDeleted"" : false, 
                        ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
                        ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
                        ""Code"" : ""0215"", 
                        ""Suppressed"" : false, 
                        ""Name"" : ""Short Term Disability"", 
                        ""Description"" : null, 
                        ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000Z""), 
                        ""PolicyType"" : NumberInt(2), 
                        ""RuleGroups"" : [
                            {
                                ""_id"" : ""39c796bd-3eea-49df-9729-0900f66cc17d"",
                                ""Name"" : ""Do not auto add"",
                                ""Description"" : ""This will ensure that this policy is not automatically added to any case and is left for manual selection."",
                                ""Rules"" : [
                                    {
                                        ""_id"" : ""82017c85-8f8a-417d-9bd3-130880456d7e"",
                                        ""Name"" : ""Always False"",
                                        ""Description"" : ""Always False"",
                                        ""LeftExpression"" : ""1()"",
                                        ""Operator"" : ""=="",
                                        ""RightExpression"" : ""1"",
                                        ""NotRunOrError"" : true,
                                        ""ErrorMessages"" : [

                                        ]
                                    }
                                ], 
                                ""SuccessType"" : NumberInt(0), 
                                ""HasBeenEvaluated"" : false, 
                                ""Pass"" : false, 
                                ""RuleGroupType"" : NumberInt(0), 
                                ""Entitlement"" : null, 
                                ""PaymentEntitlement"" : [

                                ]
                            }
                        ], 
                        ""AbsenceReasons"" : [
                            {
                                ""CaseTypeConsecutive"" : true,
                                ""CaseTypeIntermittent"" : false,
                                ""CaseTypeReduced"" : true,
                                ""CaseTypeAdministrative"" : false,
                                ""EntitlementTypeText"" : ""Work Weeks"",
                                ""_id"" : ""8f18a777-b2f6-4316-9ce2-25bda15ad96b"",
                                ""ReasonId"" : ""000000000000000001000000"",
                                ""EntitlementType"" : NumberInt(1),
                                ""Entitlement"" : 26.0,
                                ""EliminationType"" : NumberInt(3),
                                ""PolicyEliminationCalcType"" : NumberInt(1),
                                ""CombinedForSpouses"" : false,
                                ""PeriodType"" : NumberInt(0),
                                ""Period"" : 12.0,
                                ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000Z""),
                                ""CaseTypes"" : NumberInt(5),
                                ""Paid"" : true,
                                ""PaymentTiers"" : [

                                ], 
                                ""RuleGroups"" : [
                                    {
                                        ""_id"" : ""d97cc9a3-1cf5-4594-9821-0830e76a6a14"",
                                        ""Name"" : ""Length of Service"",
                                        ""Description"" : ""Length of Service"",
                                        ""Rules"" : [
                                            {
                                                ""_id"" : ""2f09c6ac-3519-4b57-bbff-f5b112e66665"",
                                                ""Name"" : ""FTE Status"",
                                                ""Description"" : ""FTE Status"",
                                                ""LeftExpression"" : ""EmployeeCustomField('>=', 0.5, 'Total FTE')"",
                                                ""Operator"" : ""=="",
                                                ""RightExpression"" : ""true"",
                                                ""NotRunOrError"" : true,
                                                ""ErrorMessages"" : [

                                                ]
                                            }, 
                                            {
                                                ""_id"" : ""876c0a87-aea3-4704-8892-a412b07bd4ca"", 
                                                ""Name"" : ""Length of Service"", 
                                                ""Description"" : ""Length of Service >= 6 months"", 
                                                ""LeftExpression"" : ""HasMinLengthOfService(6, Unit.Months)"", 
                                                ""Operator"" : ""=="", 
                                                ""RightExpression"" : ""true"", 
                                                ""NotRunOrError"" : true, 
                                                ""ErrorMessages"" : [

                                                ]
                                            }
                                        ], 
                                        ""SuccessType"" : NumberInt(0), 
                                        ""HasBeenEvaluated"" : false, 
                                        ""Pass"" : false, 
                                        ""RuleGroupType"" : NumberInt(1), 
                                        ""Entitlement"" : null, 
                                        ""PaymentEntitlement"" : [

                                        ]
                                    }
                                ], 
                                ""WorkCountry"" : null, 
                                ""ResidenceState"" : null, 
                                ""ResidenceCountry"" : null, 
                                ""ShowType"" : NumberInt(3), 
                                ""PolicyEvents"" : [

                                ], 
                                ""UseWholeDollarAmountsOnly"" : false
                            }, 
                            {
                                ""CaseTypeConsecutive"" : true, 
                                ""CaseTypeIntermittent"" : false, 
                                ""CaseTypeReduced"" : true, 
                                ""CaseTypeAdministrative"" : false, 
                                ""EntitlementTypeText"" : ""Work Weeks"", 
                                ""_id"" : ""6d447ad3-2b74-48d5-802c-9721a6998959"", 
                                ""ReasonId"" : ""000000000000000001000002"", 
                                ""EntitlementType"" : NumberInt(1), 
                                ""Entitlement"" : 26.0, 
                                ""EliminationType"" : NumberInt(5), 
                                ""PolicyEliminationCalcType"" : NumberInt(1), 
                                ""CombinedForSpouses"" : false, 
                                ""PeriodType"" : NumberInt(0), 
                                ""Period"" : 12.0, 
                                ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000Z""), 
                                ""CaseTypes"" : NumberInt(5), 
                                ""Paid"" : true, 
                                ""PaymentTiers"" : [

                                ], 
                                ""RuleGroups"" : [
                                    {
                                        ""_id"" : ""1d5b4644-e794-4cb5-b273-bbef84880366"",
                                        ""Name"" : ""Length of Service"",
                                        ""Description"" : ""Length of Service"",
                                        ""Rules"" : [
                                            {
                                                ""_id"" : ""ef9b37aa-52ce-412e-9796-ef8b9f9d9445"",
                                                ""Name"" : ""FTE Status"",
                                                ""Description"" : ""FTE Status"",
                                                ""LeftExpression"" : ""EmployeeCustomField('>=', 0.5, 'Total FTE')"",
                                                ""Operator"" : ""=="",
                                                ""RightExpression"" : ""true"",
                                                ""NotRunOrError"" : true,
                                                ""ErrorMessages"" : [

                                                ]
                                            }, 
                                            {
                                                ""_id"" : ""a42d0cfb-1e5c-4441-9854-9de7c624ab57"", 
                                                ""Name"" : ""Length of Service"", 
                                                ""Description"" : ""Length of Service"", 
                                                ""LeftExpression"" : ""HasMinLengthOfService(6, Unit.Months)"", 
                                                ""Operator"" : ""=="", 
                                                ""RightExpression"" : ""true"", 
                                                ""NotRunOrError"" : true, 
                                                ""ErrorMessages"" : [

                                                ]
                                            }
                                        ], 
                                        ""SuccessType"" : NumberInt(0), 
                                        ""HasBeenEvaluated"" : false, 
                                        ""Pass"" : false, 
                                        ""RuleGroupType"" : NumberInt(1), 
                                        ""Entitlement"" : null, 
                                        ""PaymentEntitlement"" : [

                                        ]
                                    }
                                ], 
                                ""WorkCountry"" : null, 
                                ""ResidenceState"" : null, 
                                ""ResidenceCountry"" : null, 
                                ""Gender"" : NumberInt(70), 
                                ""ShowType"" : NumberInt(3), 
                                ""PolicyEvents"" : [

                                ], 
                                ""AllowOffset"" : false, 
                                ""UseWholeDollarAmountsOnly"" : false
                            }
                        ], 
                        ""WorkCountry"" : ""US"", 
                        ""WorkState"" : null, 
                        ""ResidenceCountry"" : ""US"", 
                        ""ResidenceState"" : null, 
                        ""Gender"" : null, 
                        ""ConsecutiveTo"" : [

                        ]
                    }, 
                    ""PolicyCrosswalkCodes"" : [

                    ], 
                    ""PolicyReason"" : {
                        ""CaseTypeConsecutive"" : true, 
                        ""CaseTypeIntermittent"" : false, 
                        ""CaseTypeReduced"" : true, 
                        ""CaseTypeAdministrative"" : false, 
                        ""EntitlementTypeText"" : ""Work Weeks"", 
                        ""_id"" : ""8f18a777-b2f6-4316-9ce2-25bda15ad96b"", 
                        ""ReasonId"" : ""000000000000000001000000"", 
                        ""EntitlementType"" : NumberInt(1), 
                        ""Entitlement"" : 26.0, 
                        ""EliminationType"" : NumberInt(3), 
                        ""PolicyEliminationCalcType"" : NumberInt(1), 
                        ""CombinedForSpouses"" : false, 
                        ""PeriodType"" : NumberInt(0), 
                        ""Period"" : 12.0, 
                        ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000Z""), 
                        ""CaseTypes"" : NumberInt(5), 
                        ""Paid"" : true, 
                        ""PaymentTiers"" : [

                        ], 
                        ""RuleGroups"" : [
                            {
                                ""_id"" : ""d97cc9a3-1cf5-4594-9821-0830e76a6a14"",
                                ""Name"" : ""Length of Service"",
                                ""Description"" : ""Length of Service"",
                                ""Rules"" : [
                                    {
                                        ""_id"" : ""2f09c6ac-3519-4b57-bbff-f5b112e66665"",
                                        ""Name"" : ""FTE Status"",
                                        ""Description"" : ""FTE Status"",
                                        ""LeftExpression"" : ""EmployeeCustomField('>=', 0.5, 'Total FTE')"",
                                        ""Operator"" : ""=="",
                                        ""RightExpression"" : ""true"",
                                        ""NotRunOrError"" : true,
                                        ""ErrorMessages"" : [

                                        ]
                                    }, 
                                    {
                                        ""_id"" : ""876c0a87-aea3-4704-8892-a412b07bd4ca"", 
                                        ""Name"" : ""Length of Service"", 
                                        ""Description"" : ""Length of Service >= 6 months"", 
                                        ""LeftExpression"" : ""HasMinLengthOfService(6, Unit.Months)"", 
                                        ""Operator"" : ""=="", 
                                        ""RightExpression"" : ""true"", 
                                        ""NotRunOrError"" : true, 
                                        ""ErrorMessages"" : [

                                        ]
                                    }
                                ], 
                                ""SuccessType"" : NumberInt(0), 
                                ""HasBeenEvaluated"" : false, 
                                ""Pass"" : false, 
                                ""RuleGroupType"" : NumberInt(1), 
                                ""Entitlement"" : null, 
                                ""PaymentEntitlement"" : [

                                ]
                            }
                        ], 
                        ""WorkCountry"" : null, 
                        ""ResidenceState"" : null, 
                        ""ResidenceCountry"" : null, 
                        ""ShowType"" : NumberInt(3), 
                        ""PolicyEvents"" : [

                        ], 
                        ""UseWholeDollarAmountsOnly"" : false
                    }, 
                    ""Status"" : NumberInt(1), 
                    ""StartDate"" : ISODate(""2017-10-19T00:00:00.000Z""), 
                    ""EndDate"" : ISODate(""2017-11-29T00:00:00.000Z""), 
                    ""Usage"" : [
                        {
                            ""_id"" : ""ddc8bf52-5d1c-47ba-8b3f-8caa936a3018"",
                            ""DateUsed"" : ISODate(""2017-11-29T00:00:00.000Z""),
                            ""MinutesUsed"" : 720.0,
                            ""MinutesInDay"" : 720.0,
                            ""PolicyReasonId"" : ""000000000000000001000000"",
                            ""UserEntered"" : true,
                            ""Determination"" : NumberInt(1),
                            ""IsIntermittentRestriction"" : false,
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""d8692810-8469-4b07-9705-588beb530b79"", 
                            ""DateUsed"" : ISODate(""2017-11-28T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""d077c2ba-94e6-4885-a531-e82444e572f0"", 
                            ""DateUsed"" : ISODate(""2017-11-27T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""f8b6d8ff-c00e-41ec-b61b-9570a4e75f02"", 
                            ""DateUsed"" : ISODate(""2017-11-26T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""2066669c-38bb-453a-b10b-1d7d64f2fed6"", 
                            ""DateUsed"" : ISODate(""2017-11-25T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""1bab8867-906d-4b98-935d-90ca074d0aa8"", 
                            ""DateUsed"" : ISODate(""2017-11-24T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""4c014493-ddb7-48e4-abea-c6eb44afc8e5"", 
                            ""DateUsed"" : ISODate(""2017-11-23T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""f9569887-22bd-4c7a-b691-ef6b82de0169"", 
                            ""DateUsed"" : ISODate(""2017-11-22T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""b8272e9d-fb45-4191-9734-c7b1da84c817"", 
                            ""DateUsed"" : ISODate(""2017-11-21T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""120206ec-c4ba-4138-b52f-d1ec1ca3f01b"", 
                            ""DateUsed"" : ISODate(""2017-11-20T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""e36295ee-2bbc-4f91-889e-c1226717588d"", 
                            ""DateUsed"" : ISODate(""2017-11-19T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""ab71c362-abc8-4c69-8c27-6876beee1694"", 
                            ""DateUsed"" : ISODate(""2017-11-18T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""aa96ec5c-4fdc-4656-bd4b-0ce929b05510"", 
                            ""DateUsed"" : ISODate(""2017-11-17T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""66cf350d-690a-4cce-9156-7e22dfdafba5"", 
                            ""DateUsed"" : ISODate(""2017-11-16T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""26c779ed-d641-4a3c-8911-171dbc046cef"", 
                            ""DateUsed"" : ISODate(""2017-11-15T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""ae2a0a0e-46ea-44aa-aa12-a9bf26c7e31b"", 
                            ""DateUsed"" : ISODate(""2017-11-14T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""761c92ae-7a35-483d-a6d5-d2d3cf8491b7"", 
                            ""DateUsed"" : ISODate(""2017-11-13T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""47d376a1-f9ec-4c11-a60b-e50671202333"", 
                            ""DateUsed"" : ISODate(""2017-11-12T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""9755a3e5-9091-4941-b62e-8fefec85e3bf"", 
                            ""DateUsed"" : ISODate(""2017-11-11T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""c0563fb6-755c-4293-8a75-d22b43e1b00c"", 
                            ""DateUsed"" : ISODate(""2017-11-10T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""88097cbb-8724-4de7-bd49-2083c8509de3"", 
                            ""DateUsed"" : ISODate(""2017-11-09T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""21b119ff-9208-4a50-b74d-9a10ae370431"", 
                            ""DateUsed"" : ISODate(""2017-11-08T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""ab636437-5629-48da-a311-ff1a9447c0a0"", 
                            ""DateUsed"" : ISODate(""2017-11-07T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""52448dc9-de2a-43fd-b6a8-6654b24adeda"", 
                            ""DateUsed"" : ISODate(""2017-11-06T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""1b95b707-49cb-4642-afc3-3047a8d4a06c"", 
                            ""DateUsed"" : ISODate(""2017-11-05T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""ebc6d69d-693d-4b4d-99b8-5f0b38a1a113"", 
                            ""DateUsed"" : ISODate(""2017-11-04T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""9078043b-d0ca-40c0-996b-8880f994e1b0"", 
                            ""DateUsed"" : ISODate(""2017-11-03T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""3486089c-f38f-48f0-9d4f-54050b0d7306"", 
                            ""DateUsed"" : ISODate(""2017-11-02T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""d141ce8e-c913-49b0-92a0-58359ddfdc40"", 
                            ""DateUsed"" : ISODate(""2017-11-01T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 14.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 30.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""9acb5b9a-7202-4e90-b9f0-2cc1fde5ad29"", 
                            ""DateUsed"" : ISODate(""2017-10-31T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""82906394-71af-436f-9eed-ff57de1b6575"", 
                            ""DateUsed"" : ISODate(""2017-10-30T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""4d31bca0-a6d4-4dec-b020-7030b317c4e4"", 
                            ""DateUsed"" : ISODate(""2017-10-29T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""0e8f19ee-f01c-494b-8a6a-fc968421263b"", 
                            ""DateUsed"" : ISODate(""2017-10-28T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""5447311d-a609-466a-92f0-60c17f40d2cb"", 
                            ""DateUsed"" : ISODate(""2017-10-27T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""c867819a-8e87-4ab3-9003-302b3faefad8"", 
                            ""DateUsed"" : ISODate(""2017-10-26T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""2426a326-a0dd-42c1-b7ea-148d261f2a87"", 
                            ""DateUsed"" : ISODate(""2017-10-25T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""4d9fd1dc-bddb-4096-9fbb-136985ff7db5"", 
                            ""DateUsed"" : ISODate(""2017-10-24T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""c37d7798-1d58-4c70-a271-ef63a6e1dc46"", 
                            ""DateUsed"" : ISODate(""2017-10-23T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""1d83e60c-b698-474e-a2a7-bab4e8b0138c"", 
                            ""DateUsed"" : ISODate(""2017-10-22T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 3.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""fc1b7f1c-2647-4560-a9ff-8cf7820c33a9"", 
                            ""DateUsed"" : ISODate(""2017-10-21T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""543fe2b1-404c-4ba5-8048-bd18c77bb61b"", 
                            ""DateUsed"" : ISODate(""2017-10-20T00:00:00.000Z""), 
                            ""MinutesUsed"" : 0.0, 
                            ""MinutesInDay"" : 0.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : Infinity
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }, 
                        {
                            ""_id"" : ""1ea2ec09-3e12-4178-aa1c-2254c4be6303"", 
                            ""DateUsed"" : ISODate(""2017-10-19T00:00:00.000Z""), 
                            ""MinutesUsed"" : 720.0, 
                            ""MinutesInDay"" : 720.0, 
                            ""PolicyReasonId"" : ""000000000000000001000000"", 
                            ""UserEntered"" : true, 
                            ""Determination"" : NumberInt(1), 
                            ""IsIntermittentRestriction"" : false, 
                            ""AvailableToUse"" : [
                                {
                                    ""UnitType"" : NumberInt(2),
                                    ""BaseUnit"" : 0.08333333333333333
                                }, 
                                {
                                    ""UnitType"" : NumberInt(8), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(4), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(9), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(1), 
                                    ""BaseUnit"" : 1.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(3), 
                                    ""BaseUnit"" : 7.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(7), 
                                    ""BaseUnit"" : 5.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(5), 
                                    ""BaseUnit"" : 31.0
                                }, 
                                {
                                    ""UnitType"" : NumberInt(6), 
                                    ""BaseUnit"" : 365.0
                                }
                            ], 
                            ""IsLocked"" : true
                        }
                    ], 
                    ""RuleGroups"" : [

                    ], 
                    ""ManuallyAdded"" : true, 
                    ""PolicyDateOverridden"" : {
                        ""_id"" : ""d6053930-3d79-4b50-a356-bf116b2f1ca6""
                    }, 
                    ""ManuallyAddedNote"" : ""Case Conversion""
                }
            ], 
            ""Status"" : NumberInt(1), 
            ""LeaveSchedule"" : [

            ], 
            ""Absences"" : [
                {
                    ""_id"" : ""296914db-f6d1-43c8-b3c2-9451237e93bd"",
                    ""TotalMinutes"" : NumberInt(720),
                    ""SampleDate"" : ISODate(""2018-01-10T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""ec315b08-6bf7-40d3-96e4-c8bf9d64cd1f"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2018-01-01T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""ccbf9546-6dcf-4224-8890-75f00bbc2089"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-12-28T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""ee50579d-bcab-4ba2-b41c-b6a90f14c806"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-12-25T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""1b46f608-083f-4dbb-a20b-f3d7d3488873"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-12-21T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""e70f670a-71ed-42d9-ae9f-9e2e3b353974"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-12-20T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""af44d0c1-1e6d-4755-9144-cb30b115c43f"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-12-13T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""4a0a6663-ec7d-4398-917f-6d8c2204e4e2"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-12-04T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""a932b67f-8e15-4a1e-af41-53b497fd5f8c"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-11-30T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""d9e7304b-ed8c-48ee-b36b-5182e310239f"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-11-29T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""f9772032-daf3-486c-acb6-9899244f056b"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-11-22T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""3e6c7850-98a6-46b0-a8b8-3ce512a6952d"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-11-20T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""6cbf0b61-f6b3-4711-be53-3e92f760c2ea"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-11-16T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""33ed0e49-c36a-4fd8-91f7-55a91963b267"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-11-13T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""13428edb-7f35-4e73-b5fe-ffe555d56979"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-11-09T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""7fb94bfe-7139-4de0-9340-f8d4780692be"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-11-08T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""0bb495cd-69d3-44cb-981f-acade03e7f4d"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-11-06T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""63eb6cd8-afa9-4d81-9714-f37b33ab30be"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-11-02T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""26fa3a7a-26c2-449c-b3a8-411d45a889c7"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-11-01T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""8e61e45a-3f95-46bb-8d71-3c088131f139"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-10-25T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""8abecf00-ef4f-49e1-a3fd-147137670c57"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-10-23T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""4594075d-ca4f-4663-ac9f-853083a248c2"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-10-19T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""3dd93d12-ab23-4133-8959-ab6d12502426"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2018-01-08T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""515ee987-77ce-4a8b-af48-f145db5db45e"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2018-01-04T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""df1460e3-79ae-4080-a45a-2f144c96f33f"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2018-01-03T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""55d04c9e-600c-4038-b00e-b98557126cb9"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-12-27T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""0c96c375-acd5-4372-8a23-7ddbb00883b3"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-12-18T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""ce58fba2-eed3-4f70-87ed-3848decd8d9c"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-12-14T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""1b9ff159-0d0a-4d05-850d-302f08e240e4"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-12-11T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""cee21c83-7aa8-45ab-a645-cd3b1f72f73f"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-12-07T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""622add6e-e4f7-4146-b726-fb2cc553be68"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-12-06T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""112b6261-c73a-429d-92d1-39d6eee14bc3"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-11-27T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""88605408-90bd-4af2-aeed-ee246708cc82"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-11-23T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""bd9b53a8-d30a-4f89-bc42-c608a1438341"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-11-15T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""3bdc6c0e-6651-40ff-bc52-11c56b4e7828"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-10-30T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""f034a9c3-b9c9-4034-b4e5-da702e76618c"", 
                    ""TotalMinutes"" : NumberInt(720), 
                    ""SampleDate"" : ISODate(""2017-10-26T00:00:00.000Z"")
                }
            ], 
            ""UserRequests"" : [

            ], 
            ""CreatedDate"" : ISODate(""2018-08-03T00:00:00.000Z"")
        }
    ], 
    ""SpouseCaseId"" : null, 
    ""CaseEvents"" : [
        {
            ""_id"" : ""de8c0d00-d7e2-4db1-87d7-9054a02c7c75"",
            ""EventType"" : NumberInt(0),
            ""AllEventDates"" : [
                ISODate(""2017-07-18T00:00:00.000Z"")
            ]
        }, 
        {
            ""_id"" : ""fcb92345-8130-4255-8bd0-0c29696a9116"", 
            ""EventType"" : NumberInt(11), 
            ""AllEventDates"" : [
                ISODate(""2018-01-29T00:00:00.000Z"")
            ]
        }, 
        {
            ""_id"" : ""dcfe34ba-4976-4ce8-81cb-cb0d7613602a"", 
            ""EventType"" : NumberInt(1), 
            ""AllEventDates"" : [
                ISODate(""2018-01-11T00:00:00.000Z"")
            ]
        }, 
        {
            ""_id"" : ""413c51b3-9a65-4ab9-a364-27581a7ebe62"", 
            ""EventType"" : NumberInt(5), 
            ""AllEventDates"" : [
                ISODate(""2017-10-19T00:00:00.000Z"")
            ]
        }
    ], 
    ""Employee"" : {
        ""_id"" : ObjectId(""5a62522bc5f32e3528b2fb71""), 
        ""LastERow"" : ""ATXWrZJAtHnmfr1yhWnFaA=="", 
        ""LastCRow"" : ""FjD18PZL10Deh/s0DeYRkw=="", 
        ""EtlChangeTriggerScriptRun"" : ""Sun Jul 22 2018 20:36:51 GMT-0600 (MDT)"", 
        ""cdt"" : ISODate(""2018-01-19T00:00:00.000Z""), 
        ""cby"" : ObjectId(""000000000000000000000000""), 
        ""mdt"" : ISODate(""2018-08-03T00:00:00.000Z""), 
        ""mby"" : ObjectId(""000000000000000000000000""), 
        ""IsDeleted"" : false, 
        ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
        ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
        ""EmployeeNumber"" : ""135361"", 
        ""Info"" : {
            ""_id"" : ""f94124c4-4cc3-4563-98c5-66f2fa78734c"", 
            ""Email"" : null, 
            ""Address"" : {
                ""_id"" : ""c6cf2f58-1546-46f6-91e2-0b817134eb2f"", 
                ""Address1"" : ""853 SUNRISE LN NW"", 
                ""City"" : ""Grand Rapids"", 
                ""State"" : ""MI"", 
                ""PostalCode"" : ""49534"", 
                ""Country"" : ""USA""
            }, 
            ""AltAddress"" : {
                ""_id"" : ""41094bfe-d348-4be3-a0a7-0050bdb23737"", 
                ""Address1"" : null, 
                ""City"" : null, 
                ""State"" : null, 
                ""PostalCode"" : null, 
                ""Country"" : ""US""
            }, 
            ""HomePhone"" : ""6168022401"", 
            ""OfficeLocation"" : ""MICH 100-100 Michigan-Butterworth""
        }, 
        ""FirstName"" : ""DANIELLE"", 
        ""MiddleName"" : ""K"", 
        ""LastName"" : ""ZWIT"", 
        ""Gender"" : NumberInt(70), 
        ""DoB"" : ISODate(""1989-06-05T00:00:00.000Z""), 
        ""Ssn"" : {
            ""h"" : ""EDF81CFE57535FD3306BDABF99CFB31030C24FFC"", 
            ""e"" : ""a4TF+41+aPN9UWpYDDFhMSUzSNHzQsDzON1oO3/ozViwcJIL07yCgEaPHOGSaKHt5h3JnvPOgfmz8umiZ9L/QH1Gi+XOBUjLjxhGKN2Y9Seaw3wGBjfTNzl5I0gIX290KNXMdv0youxlvOtoYUZO1oaTlabriAwNTc28lomYuG8=""
        }, 
        ""Status"" : NumberInt(65), 
        ""Salary"" : 25.99, 
        ""PayType"" : NumberInt(2), 
        ""JobTitle"" : ""N049XN REGISTERED NURSE"", 
        ""PriorHours"" : [
            {
                ""_id"" : ""fb4c14c7-6132-454b-98b9-dfe625bc7568"",
                ""HoursWorked"" : 1351.73,
                ""AsOf"" : ISODate(""2018-03-19T00:00:00.000Z"")
            }, 
            {
                ""CaseNumber"" : ""2017-0107015"", 
                ""_id"" : ""8a9eeac7-2b3d-41df-81af-8d835543493a"", 
                ""HoursWorked"" : 1890.0, 
                ""AsOf"" : ISODate(""2017-07-18T00:00:00.000Z"")
            }
        ], 
        ""MinutesWorkedPerWeek"" : [

        ], 
        ""ServiceDate"" : ISODate(""2015-11-01T00:00:00.000Z""), 
        ""HireDate"" : ISODate(""2012-11-05T00:00:00.000Z""), 
        ""WorkState"" : ""MI"", 
        ""WorkCountry"" : ""US"", 
        ""WorkSchedules"" : [
            {
                ""_id"" : ""bba5f44d-2ba8-4bc0-91df-69f73967983b"",
                ""ScheduleType"" : NumberInt(0),
                ""StartDate"" : ISODate(""2017-10-19T00:00:00.000Z""),
                ""EndDate"" : null,
                ""Times"" : [
                    {
                        ""_id"" : ""72cf08f5-ffcc-40cd-9153-347596a28a0a"",
                        ""TotalMinutes"" : NumberInt(0),
                        ""SampleDate"" : ISODate(""2017-10-15T00:00:00.000Z"")
                    }, 
                    {
                        ""_id"" : ""464ea7b2-5f4a-482d-9578-dbfca3d3155d"", 
                        ""TotalMinutes"" : NumberInt(720), 
                        ""SampleDate"" : ISODate(""2017-10-16T00:00:00.000Z"")
                    }, 
                    {
                        ""_id"" : ""bab2c4c9-dc96-46d6-97ec-f1fcb552c09c"", 
                        ""TotalMinutes"" : NumberInt(0), 
                        ""SampleDate"" : ISODate(""2017-10-17T00:00:00.000Z"")
                    }, 
                    {
                        ""_id"" : ""34c1efe2-3e9b-4b8a-8e60-e3a4613ecf8f"", 
                        ""TotalMinutes"" : NumberInt(720), 
                        ""SampleDate"" : ISODate(""2017-10-18T00:00:00.000Z"")
                    }, 
                    {
                        ""_id"" : ""cc5fbf52-4992-4fd8-ab9d-c115209d9fc4"", 
                        ""TotalMinutes"" : NumberInt(720), 
                        ""SampleDate"" : ISODate(""2017-10-19T00:00:00.000Z"")
                    }, 
                    {
                        ""_id"" : ""72ecd9ac-bcf3-42a3-8dd0-8aedd7abec8c"", 
                        ""TotalMinutes"" : NumberInt(0), 
                        ""SampleDate"" : ISODate(""2017-10-20T00:00:00.000Z"")
                    }, 
                    {
                        ""_id"" : ""387453a0-596f-4843-a69d-a70956fcace0"", 
                        ""TotalMinutes"" : NumberInt(0), 
                        ""SampleDate"" : ISODate(""2017-10-21T00:00:00.000Z"")
                    }
                ]
            }
        ], 
        ""Meets50In75MileRule"" : false, 
        ""IsKeyEmployee"" : false, 
        ""IsExempt"" : false, 
        ""MilitaryStatus"" : NumberInt(0), 
        ""CustomFields"" : [
            {
                ""_id"" : ObjectId(""58f7d42dc5f3410818be918f""),
                ""cdt"" : ISODate(""2017-04-19T00:00:00.000Z""),
                ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""),
                ""mdt"" : ISODate(""2017-09-07T00:00:00.000Z""),
                ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""),
                ""IsDeleted"" : false,
                ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""),
                ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""),
                ""Code"" : ""TOTALFTE"",
                ""Suppressed"" : false,
                ""Target"" : NumberInt(1),
                ""Name"" : ""Total FTE"",
                ""Description"" : ""Total FTE"",
                ""Label"" : ""Total FTE"",
                ""HelpText"" : ""Total FTE"",
                ""DataType"" : NumberInt(2),
                ""ValueType"" : NumberInt(1),
                ""ListValues"" : [

                ], 
                ""SelectedValue"" : {
                    ""_t"" : ""System.Decimal"", 
                    ""_v"" : ""0.90""
                }, 
                ""FileOrder"" : NumberInt(1), 
                ""IsRequired"" : false, 
                ""IsCollectedAtIntake"" : false
            }, 
            {
                ""_id"" : ObjectId(""592f0d8ac5f3381368900be2""), 
                ""cdt"" : ISODate(""2017-05-31T00:00:00.000Z""), 
                ""cby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
                ""mdt"" : ISODate(""2017-09-06T00:00:00.000Z""), 
                ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""IsDeleted"" : false, 
                ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
                ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
                ""Code"" : ""PROCESSLEVEL"", 
                ""Suppressed"" : false, 
                ""Target"" : NumberInt(1), 
                ""Name"" : ""Process Level"", 
                ""Description"" : ""Process level associated with employee's primary position."", 
                ""Label"" : ""Process Level"", 
                ""HelpText"" : ""Process level associated with employee's primary position."", 
                ""DataType"" : NumberInt(1), 
                ""ValueType"" : NumberInt(1), 
                ""ListValues"" : [

                ], 
                ""SelectedValue"" : ""861N Neonatal Services"", 
                ""FileOrder"" : NumberInt(5), 
                ""IsRequired"" : false, 
                ""IsCollectedAtIntake"" : false
            }, 
            {
                ""_id"" : ObjectId(""58f7d400c5f3410818be9187""), 
                ""cdt"" : ISODate(""2017-04-19T00:00:00.000Z""), 
                ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""mdt"" : ISODate(""2017-09-06T00:00:00.000Z""), 
                ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""IsDeleted"" : false, 
                ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
                ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
                ""Code"" : ""EMPLOYEESTATUS"", 
                ""Suppressed"" : false, 
                ""Target"" : NumberInt(1), 
                ""Name"" : ""Employee Status "", 
                ""Description"" : ""Employee Status "", 
                ""Label"" : ""Employee Status "", 
                ""HelpText"" : ""Employee Status "", 
                ""DataType"" : NumberInt(1), 
                ""ValueType"" : NumberInt(1), 
                ""ListValues"" : [

                ], 
                ""SelectedValue"" : ""A1 Reg FT w/Bens"", 
                ""FileOrder"" : NumberInt(3), 
                ""IsRequired"" : false, 
                ""IsCollectedAtIntake"" : false
            }, 
            {
                ""_id"" : ObjectId(""58f7d4acc5f3410818be91bb""), 
                ""cdt"" : ISODate(""2017-04-19T00:00:00.000Z""), 
                ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""mdt"" : ISODate(""2017-09-06T00:00:00.000Z""), 
                ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""IsDeleted"" : false, 
                ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
                ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
                ""Code"" : ""CURRENTPTO"", 
                ""Suppressed"" : false, 
                ""Target"" : NumberInt(1), 
                ""Name"" : ""Current PTO Balance"", 
                ""Description"" : ""Current PTO Balance"", 
                ""Label"" : ""Current PTO Balance"", 
                ""HelpText"" : ""Current PTO Balance"", 
                ""DataType"" : NumberInt(2), 
                ""ValueType"" : NumberInt(1), 
                ""ListValues"" : [

                ], 
                ""SelectedValue"" : {
                    ""_t"" : ""System.Decimal"", 
                    ""_v"" : ""161.36""
                }, 
                ""FileOrder"" : NumberInt(4), 
                ""IsRequired"" : false, 
                ""IsCollectedAtIntake"" : false
            }, 
            {
                ""_id"" : ObjectId(""5953a3e1c5f32f1a88e7b10e""), 
                ""cdt"" : ISODate(""2017-06-28T00:00:00.000Z""), 
                ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""mdt"" : ISODate(""2017-09-21T00:00:00.000Z""), 
                ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""IsDeleted"" : false, 
                ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
                ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
                ""Code"" : ""COSTCENTER"", 
                ""Suppressed"" : false, 
                ""Target"" : NumberInt(1), 
                ""Name"" : ""Cost Center"", 
                ""Description"" : null, 
                ""Label"" : ""SubDepartment / Cost Center"", 
                ""HelpText"" : ""SubDepartment / Cost Center"", 
                ""DataType"" : NumberInt(1), 
                ""ValueType"" : NumberInt(1), 
                ""ListValues"" : [

                ], 
                ""SelectedValue"" : ""20000 HDVCH-NICU/NIM"", 
                ""FileOrder"" : NumberInt(34), 
                ""IsRequired"" : false, 
                ""IsCollectedAtIntake"" : false
            }, 
            {
                ""_id"" : ObjectId(""5924474ec5f3460eb8311884""), 
                ""cdt"" : ISODate(""2017-05-23T00:00:00.000Z""), 
                ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""mdt"" : ISODate(""2017-09-07T00:00:00.000Z""), 
                ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""IsDeleted"" : false, 
                ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
                ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
                ""Code"" : ""C_NAME"", 
                ""Suppressed"" : false, 
                ""Target"" : NumberInt(1), 
                ""Name"" : ""COMPANY NAME"", 
                ""Description"" : null, 
                ""Label"" : ""COMPANY NAME"", 
                ""HelpText"" : ""Company Name"", 
                ""DataType"" : NumberInt(1), 
                ""ValueType"" : NumberInt(1), 
                ""ListValues"" : [

                ], 
                ""SelectedValue"" : ""Spectrum Health"", 
                ""FileOrder"" : NumberInt(20), 
                ""IsRequired"" : false, 
                ""IsCollectedAtIntake"" : false
            }, 
            {
                ""_id"" : ObjectId(""58f7d15ac5f3410818be90fd""), 
                ""cdt"" : ISODate(""2017-04-19T00:00:00.000Z""), 
                ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""mdt"" : ISODate(""2017-09-06T00:00:00.000Z""), 
                ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
                ""IsDeleted"" : false, 
                ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
                ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
                ""Code"" : ""2PMFTE"", 
                ""Suppressed"" : false, 
                ""Target"" : NumberInt(1), 
                ""Name"" : ""2nd position FTE"", 
                ""Description"" : ""2nd position FTE"", 
                ""Label"" : ""2nd position FTE"", 
                ""HelpText"" : ""2nd position FTE"", 
                ""DataType"" : NumberInt(2), 
                ""ValueType"" : NumberInt(1), 
                ""ListValues"" : [

                ], 
                ""SelectedValue"" : null, 
                ""FileOrder"" : NumberInt(2), 
                ""IsRequired"" : false, 
                ""IsCollectedAtIntake"" : false
            }, 
            {
                ""_id"" : ObjectId(""58f7d3d7c5f3410818be9180""), 
                ""cdt"" : ISODate(""2017-04-19T00:00:00.000Z""), 
                ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""mdt"" : ISODate(""2017-09-06T00:00:00.000Z""), 
                ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
                ""IsDeleted"" : false, 
                ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
                ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
                ""Code"" : ""UNION"", 
                ""Suppressed"" : false, 
                ""Target"" : NumberInt(1), 
                ""Name"" : ""Union"", 
                ""Description"" : ""Union"", 
                ""Label"" : ""Union"", 
                ""HelpText"" : ""Union"", 
                ""DataType"" : NumberInt(1), 
                ""ValueType"" : NumberInt(1), 
                ""ListValues"" : [

                ], 
                ""SelectedValue"" : null, 
                ""FileOrder"" : NumberInt(6), 
                ""IsRequired"" : false, 
                ""IsCollectedAtIntake"" : false
            }, 
            {
                ""_id"" : ObjectId(""58f7d3b6c5f3410818be917b""), 
                ""cdt"" : ISODate(""2017-04-19T00:00:00.000Z""), 
                ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""mdt"" : ISODate(""2017-09-06T00:00:00.000Z""), 
                ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
                ""IsDeleted"" : false, 
                ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
                ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
                ""Code"" : ""INTEGRATIONDATE"", 
                ""Suppressed"" : false, 
                ""Target"" : NumberInt(1), 
                ""Name"" : ""Integration Date"", 
                ""Description"" : ""Integration Date"", 
                ""Label"" : ""Integration Date"", 
                ""HelpText"" : ""Integration Date"", 
                ""DataType"" : NumberInt(8), 
                ""ValueType"" : NumberInt(1), 
                ""ListValues"" : [

                ], 
                ""SelectedValue"" : null, 
                ""FileOrder"" : NumberInt(7), 
                ""IsRequired"" : false, 
                ""IsCollectedAtIntake"" : false
            }, 
            {
                ""_id"" : ObjectId(""58f7d04ac5f3410818be90d0""), 
                ""cdt"" : ISODate(""2017-04-19T00:00:00.000Z""), 
                ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""mdt"" : ISODate(""2017-05-11T00:00:00.000Z""), 
                ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
                ""IsDeleted"" : false, 
                ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
                ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
                ""Code"" : ""2PMJOBTITLE"", 
                ""Suppressed"" : false, 
                ""Target"" : NumberInt(1), 
                ""Name"" : ""2nd Position Job Title"", 
                ""Description"" : ""2nd Position Job Title"", 
                ""Label"" : ""2nd Position Job Title"", 
                ""HelpText"" : ""2nd Position Job Title"", 
                ""DataType"" : NumberInt(1), 
                ""ValueType"" : NumberInt(1), 
                ""ListValues"" : [

                ], 
                ""SelectedValue"" : null, 
                ""FileOrder"" : NumberInt(8), 
                ""IsRequired"" : false, 
                ""IsCollectedAtIntake"" : false
            }, 
            {
                ""_id"" : ObjectId(""58f7d358c5f3410818be916d""), 
                ""cdt"" : ISODate(""2017-04-19T00:00:00.000Z""), 
                ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""mdt"" : ISODate(""2017-09-06T00:00:00.000Z""), 
                ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
                ""IsDeleted"" : false, 
                ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
                ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
                ""Code"" : ""2PMDEPARTMENT"", 
                ""Suppressed"" : false, 
                ""Target"" : NumberInt(1), 
                ""Name"" : ""2nd Position Department"", 
                ""Description"" : ""2nd Position Department"", 
                ""Label"" : ""2nd Position Department"", 
                ""HelpText"" : ""2nd Position Department"", 
                ""DataType"" : NumberInt(2), 
                ""ValueType"" : NumberInt(1), 
                ""ListValues"" : [

                ], 
                ""SelectedValue"" : null, 
                ""FileOrder"" : NumberInt(9), 
                ""IsRequired"" : false, 
                ""IsCollectedAtIntake"" : false
            }, 
            {
                ""_id"" : ObjectId(""58f7d139c5f3410818be90f8""), 
                ""cdt"" : ISODate(""2017-04-19T00:00:00.000Z""), 
                ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""mdt"" : ISODate(""2017-09-06T00:00:00.000Z""), 
                ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
                ""IsDeleted"" : false, 
                ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
                ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
                ""Code"" : ""2PMJOBLOCATION"", 
                ""Suppressed"" : false, 
                ""Target"" : NumberInt(1), 
                ""Name"" : ""2nd Position Job Location"", 
                ""Description"" : ""2nd Position Job Location"", 
                ""Label"" : ""2nd Position Job Location"", 
                ""HelpText"" : ""2nd Position Job Location"", 
                ""DataType"" : NumberInt(1), 
                ""ValueType"" : NumberInt(1), 
                ""ListValues"" : [

                ], 
                ""SelectedValue"" : null, 
                ""FileOrder"" : NumberInt(10), 
                ""IsRequired"" : false, 
                ""IsCollectedAtIntake"" : false
            }, 
            {
                ""_id"" : ObjectId(""58f7d0f2c5f3410818be90e9""), 
                ""cdt"" : ISODate(""2017-04-19T00:00:00.000Z""), 
                ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""mdt"" : ISODate(""2017-09-06T00:00:00.000Z""), 
                ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
                ""IsDeleted"" : false, 
                ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
                ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
                ""Code"" : ""2PMPAYRATE"", 
                ""Suppressed"" : false, 
                ""Target"" : NumberInt(1), 
                ""Name"" : ""2nd Position Pay Rate"", 
                ""Description"" : ""2nd Position Pay Rate"", 
                ""Label"" : ""2nd Position Pay Rate"", 
                ""HelpText"" : ""2nd Position Pay Rate"", 
                ""DataType"" : NumberInt(2), 
                ""ValueType"" : NumberInt(1), 
                ""ListValues"" : [

                ], 
                ""SelectedValue"" : null, 
                ""FileOrder"" : NumberInt(11), 
                ""IsRequired"" : false, 
                ""IsCollectedAtIntake"" : false
            }, 
            {
                ""_id"" : ObjectId(""58f7cf8ec5f3410818be908c""), 
                ""cdt"" : ISODate(""2017-04-19T00:00:00.000Z""), 
                ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""mdt"" : ISODate(""2017-09-06T00:00:00.000Z""), 
                ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
                ""IsDeleted"" : false, 
                ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
                ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
                ""Code"" : ""2PMLASTNAME"", 
                ""Suppressed"" : false, 
                ""Target"" : NumberInt(1), 
                ""Name"" : ""2nd Position Manager Last Name"", 
                ""Description"" : ""2nd Position Manager Last Name"", 
                ""Label"" : ""2nd Position Manager Last Name"", 
                ""HelpText"" : ""2nd Position Manager Last Name"", 
                ""DataType"" : NumberInt(1), 
                ""ValueType"" : NumberInt(1), 
                ""ListValues"" : [

                ], 
                ""SelectedValue"" : null, 
                ""FileOrder"" : NumberInt(12), 
                ""IsRequired"" : false, 
                ""IsCollectedAtIntake"" : false
            }, 
            {
                ""_id"" : ObjectId(""58f7cffac5f3410818be90c0""), 
                ""cdt"" : ISODate(""2017-04-19T00:00:00.000Z""), 
                ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""mdt"" : ISODate(""2017-09-06T00:00:00.000Z""), 
                ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
                ""IsDeleted"" : false, 
                ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
                ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
                ""Code"" : ""2PMFIRSTNAME"", 
                ""Suppressed"" : false, 
                ""Target"" : NumberInt(1), 
                ""Name"" : ""2nd Position Manager First Name"", 
                ""Description"" : ""2nd Position Manager First Name"", 
                ""Label"" : ""2nd Position Manager First Name"", 
                ""HelpText"" : ""2nd Position Manager First Name"", 
                ""DataType"" : NumberInt(1), 
                ""ValueType"" : NumberInt(1), 
                ""ListValues"" : [

                ], 
                ""SelectedValue"" : null, 
                ""FileOrder"" : NumberInt(13), 
                ""IsRequired"" : false, 
                ""IsCollectedAtIntake"" : false
            }, 
            {
                ""_id"" : ObjectId(""58f7d022c5f3410818be90cb""), 
                ""cdt"" : ISODate(""2017-04-19T00:00:00.000Z""), 
                ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""mdt"" : ISODate(""2017-09-06T00:00:00.000Z""), 
                ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
                ""IsDeleted"" : false, 
                ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
                ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
                ""Code"" : ""2PMPHONE"", 
                ""Suppressed"" : false, 
                ""Target"" : NumberInt(1), 
                ""Name"" : ""2nd Position Manager Phone"", 
                ""Description"" : ""2nd Position Manager Phone"", 
                ""Label"" : ""2nd Position Manager Phone"", 
                ""HelpText"" : ""2nd Position Manager Phone"", 
                ""DataType"" : NumberInt(1), 
                ""ValueType"" : NumberInt(1), 
                ""ListValues"" : [

                ], 
                ""SelectedValue"" : ""A1-Reg FT w/Bens"", 
                ""FileOrder"" : NumberInt(14), 
                ""IsRequired"" : false, 
                ""IsCollectedAtIntake"" : false
            }, 
            {
                ""_id"" : ObjectId(""58f7d08dc5f3410818be90da""), 
                ""cdt"" : ISODate(""2017-04-19T00:00:00.000Z""), 
                ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""mdt"" : ISODate(""2017-09-06T00:00:00.000Z""), 
                ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
                ""IsDeleted"" : false, 
                ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
                ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
                ""Code"" : ""2PMEMAIL"", 
                ""Suppressed"" : false, 
                ""Target"" : NumberInt(1), 
                ""Name"" : ""2nd Position Manager Email"", 
                ""Description"" : ""2nd Position Manager Email"", 
                ""Label"" : ""2nd Position Manager Email"", 
                ""HelpText"" : ""2nd Position Manager Email"", 
                ""DataType"" : NumberInt(1), 
                ""ValueType"" : NumberInt(1), 
                ""ListValues"" : [

                ], 
                ""SelectedValue"" : ""0.900000"", 
                ""FileOrder"" : NumberInt(15), 
                ""IsRequired"" : false, 
                ""IsCollectedAtIntake"" : false
            }, 
            {
                ""_id"" : ObjectId(""58f7cf65c5f3410818be9081""), 
                ""cdt"" : ISODate(""2017-04-19T00:00:00.000Z""), 
                ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""mdt"" : ISODate(""2017-10-05T00:00:00.000Z""), 
                ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
                ""IsDeleted"" : false, 
                ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
                ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
                ""Code"" : ""2PMEMPLOYEENUMBER"", 
                ""Suppressed"" : false, 
                ""Target"" : NumberInt(1), 
                ""Name"" : ""2nd Position Manager Employee Number"", 
                ""Description"" : ""2nd Position Manager Employee Number"", 
                ""Label"" : ""2nd Position Manager Employee Number"", 
                ""HelpText"" : ""2nd Position Manager Employee Number"", 
                ""DataType"" : NumberInt(2), 
                ""ValueType"" : NumberInt(1), 
                ""ListValues"" : [

                ], 
                ""SelectedValue"" : {
                    ""_t"" : ""System.Decimal"", 
                    ""_v"" : ""12.788108""
                }, 
                ""FileOrder"" : NumberInt(18), 
                ""IsRequired"" : false, 
                ""IsCollectedAtIntake"" : false
            }, 
            {
                ""_id"" : ObjectId(""592f0dafc5f3381368900be8""), 
                ""cdt"" : ISODate(""2017-05-31T00:00:00.000Z""), 
                ""cby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
                ""mdt"" : ISODate(""2017-10-13T00:00:00.000Z""), 
                ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
                ""IsDeleted"" : false, 
                ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
                ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
                ""Code"" : ""2NDPROCESSLEVEL"", 
                ""Suppressed"" : false, 
                ""Target"" : NumberInt(1), 
                ""Name"" : ""Secondary Position Process Level"", 
                ""Description"" : ""Process level associated with employee's secondary position."", 
                ""Label"" : ""Secondary Position Process Level"", 
                ""HelpText"" : ""Process level associated with employee's secondary position."", 
                ""DataType"" : NumberInt(1), 
                ""ValueType"" : NumberInt(1), 
                ""ListValues"" : [

                ], 
                ""SelectedValue"" : null, 
                ""FileOrder"" : NumberInt(19), 
                ""IsRequired"" : false, 
                ""IsCollectedAtIntake"" : false
            }, 
            {
                ""_id"" : ObjectId(""58f7d391c5f3410818be9175""), 
                ""cdt"" : ISODate(""2017-04-19T00:00:00.000Z""), 
                ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""mdt"" : ISODate(""2017-10-13T00:00:00.000Z""), 
                ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
                ""IsDeleted"" : false, 
                ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
                ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
                ""Code"" : ""2PMCOMPANYNAME"", 
                ""Suppressed"" : false, 
                ""Target"" : NumberInt(1), 
                ""Name"" : ""2nd Position Company Name(Entity name)"", 
                ""Description"" : ""2nd Position Company Name(Entity name)"", 
                ""Label"" : ""2nd Position Company Name(Entity name)"", 
                ""HelpText"" : ""2nd Position Company Name(Entity name)"", 
                ""DataType"" : NumberInt(1), 
                ""ValueType"" : NumberInt(1), 
                ""ListValues"" : [

                ], 
                ""SelectedValue"" : null, 
                ""FileOrder"" : NumberInt(20), 
                ""IsRequired"" : false, 
                ""IsCollectedAtIntake"" : false
            }, 
            {
                ""_id"" : ObjectId(""59244b41c5f3460eb8311907""), 
                ""cdt"" : ISODate(""2017-05-23T00:00:00.000Z""), 
                ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""mdt"" : ISODate(""2017-11-17T00:00:00.000Z""), 
                ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
                ""IsDeleted"" : false, 
                ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
                ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
                ""Code"" : ""OCCUPATION"", 
                ""Suppressed"" : false, 
                ""Target"" : NumberInt(1), 
                ""Name"" : ""UNUSED_OCCUPATION"", 
                ""Description"" : null, 
                ""Label"" : ""OCCUPATION"", 
                ""HelpText"" : ""Occupation"", 
                ""DataType"" : NumberInt(1), 
                ""ValueType"" : NumberInt(1), 
                ""ListValues"" : [

                ], 
                ""SelectedValue"" : null, 
                ""FileOrder"" : NumberInt(28), 
                ""IsRequired"" : false, 
                ""IsCollectedAtIntake"" : false
            }, 
            {
                ""_id"" : ObjectId(""59244b75c5f3460eb831190f""), 
                ""cdt"" : ISODate(""2017-05-23T00:00:00.000Z""), 
                ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""mdt"" : ISODate(""2017-05-23T00:00:00.000Z""), 
                ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
                ""IsDeleted"" : false, 
                ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
                ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
                ""Code"" : ""CONTACT"", 
                ""Suppressed"" : false, 
                ""Target"" : NumberInt(1), 
                ""Name"" : ""CONTACT"", 
                ""Description"" : null, 
                ""Label"" : ""CONTACT"", 
                ""HelpText"" : ""Contact"", 
                ""DataType"" : NumberInt(1), 
                ""ValueType"" : NumberInt(1), 
                ""ListValues"" : [

                ], 
                ""SelectedValue"" : null, 
                ""FileOrder"" : NumberInt(29), 
                ""IsRequired"" : false, 
                ""IsCollectedAtIntake"" : false
            }, 
            {
                ""_id"" : ObjectId(""5953a3bac5f32f1a88e7b108""), 
                ""cdt"" : ISODate(""2017-06-28T00:00:00.000Z""), 
                ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
                ""mdt"" : ISODate(""2017-11-17T00:00:00.000Z""), 
                ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
                ""IsDeleted"" : false, 
                ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
                ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
                ""Code"" : ""DEPARTMENT"", 
                ""Suppressed"" : false, 
                ""Target"" : NumberInt(1), 
                ""Name"" : ""UNUSED_DEPARTMENT"", 
                ""Description"" : null, 
                ""Label"" : ""Department(Process Level)"", 
                ""HelpText"" : ""Department(Process Level)"", 
                ""DataType"" : NumberInt(1), 
                ""ValueType"" : NumberInt(1), 
                ""ListValues"" : [

                ], 
                ""SelectedValue"" : null, 
                ""FileOrder"" : NumberInt(33), 
                ""IsRequired"" : false, 
                ""IsCollectedAtIntake"" : false
            }
        ], 
        ""EmployerName"" : ""Spectrum Health ""
    }, 
    ""Reason"" : {
        ""_id"" : ObjectId(""000000000000000001000000""), 
        ""cdt"" : ISODate(""2014-12-27T00:00:00.000Z""), 
        ""cby"" : ObjectId(""000000000000000000000000""), 
        ""mdt"" : ISODate(""2018-08-03T00:00:00.000Z""), 
        ""mby"" : ObjectId(""000000000000000000000000""), 
        ""IsDeleted"" : false, 
        ""Code"" : ""EHC"", 
        ""Suppressed"" : false, 
        ""Name"" : ""Employee Health Condition"", 
        ""Flags"" : null, 
        ""CaseTypes"" : NumberInt(7)
    }, 
    ""Status"" : NumberInt(1), 
    ""AssignedToName"" : ""Luci Duggan"", 
    ""EmployerName"" : ""Spectrum Health "", 
    ""ClosureReason"" : NumberInt(0), 
    ""Description"" : ""Migrated to AbsenceTracker"", 
    ""Disability"" : {
        ""_id"" : ""0fe0b590-3060-49e7-a3ad-a58d7e908f0b"", 
        ""PrimaryDiagnosis"" : null, 
        ""PrimaryPathId"" : null, 
        ""PrimaryPathText"" : null, 
        ""PrimaryPathMinDays"" : null, 
        ""PrimaryPathMaxDays"" : null, 
        ""PrimaryPathDaysUIText"" : null, 
        ""SecondaryDiagnosis"" : [

        ], 
        ""MedicalComplications"" : false, 
        ""Hospitalization"" : false, 
        ""GeneralHealthCondition"" : null, 
        ""ConditionStartDate"" : null, 
        ""CoMorbidityGuidelineDetail"" : null
    }, 
    ""Certifications"" : [

    ], 
    ""Summary"" : {
        ""_id"" : ""05653a9a-910a-4837-b06d-dcb7cd14a353"", 
        ""g"" : NumberInt(1), 
        ""a"" : NumberInt(1), 
        ""t"" : NumberInt(1), 
        ""p"" : [
            {
                ""_id"" : ""f421be2c-f1db-49b1-8a41-d81adf82012f"",
                ""c"" : ""FMLA"",
                ""n"" : ""Family Medical Leave Act"",
                ""d"" : [
                    {
                        ""_id"" : ""25f4d4dd-9fbc-44ba-9e43-9f51f6acee9e"",
                        ""s"" : ISODate(""2017-10-19T00:00:00.000Z""),
                        ""e"" : ISODate(""2018-01-10T00:00:00.000Z""),
                        ""t"" : NumberInt(1),
                        ""g"" : NumberInt(0),
                        ""a"" : NumberInt(1)
                    }
                ], 
                ""m"" : [

                ]
            }, 
            {
                ""_id"" : ""334dc356-6405-4f52-91de-8af6998679ea"", 
                ""c"" : ""0215"", 
                ""n"" : ""Short Term Disability"", 
                ""d"" : [
                    {
                        ""_id"" : ""8cfe982f-0df4-4310-80fa-6bed82d7b30a"",
                        ""s"" : ISODate(""2017-10-19T00:00:00.000Z""),
                        ""e"" : ISODate(""2017-11-29T00:00:00.000Z""),
                        ""t"" : NumberInt(1),
                        ""g"" : NumberInt(0),
                        ""a"" : NumberInt(1)
                    }
                ], 
                ""m"" : [

                ]
            }
        ], 
        ""r"" : true, 
        ""e"" : false, 
        ""f"" : false, 
        ""l"" : false, 
        ""u"" : null, 
        ""x"" : null, 
        ""z"" : ISODate(""2018-01-10T00:00:00.000Z""), 
        ""m"" : ISODate(""2018-01-11T00:00:00.000Z""), 
        ""MinApprovedThruDate"" : ISODate(""2017-10-19T00:00:00.000Z""), 
        ""MinDuration"" : null, 
        ""MinDurationString"" : null, 
        ""ApprovalNotificationDateSent"" : null
    }, 
    ""Pay"" : {
        ""_id"" : ""f18c2f39-d417-4627-87f6-c5b909b5b767"", 
        ""PayPeriods"" : [

        ], 
        ""ApplyOffsetsByDefault"" : true, 
        ""WaiveWaitingPeriod"" : {
            ""_id"" : ""3052b024-e525-48ae-b63a-b05071be442c""
        }, 
        ""PayScheduleId"" : null
    }, 
    ""IsAccommodation"" : false, 
    ""IsInformationOnly"" : false, 
    ""WorkRelated"" : null, 
    ""CustomFields"" : [
        {
            ""_id"" : ObjectId(""58ee636dc5f3350818e97340""),
            ""cdt"" : ISODate(""2017-04-12T00:00:00.000Z""),
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""),
            ""mdt"" : ISODate(""2017-12-11T00:00:00.000Z""),
            ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""),
            ""IsDeleted"" : false,
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""),
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""),
            ""Code"" : ""PTO"",
            ""Suppressed"" : false,
            ""Target"" : NumberInt(2),
            ""Name"" : ""PTO Supplement"",
            ""Description"" : null,
            ""Label"" : ""PTO Supplement"",
            ""HelpText"" : ""PTO Supplement Used"",
            ""DataType"" : NumberInt(1),
            ""ValueType"" : NumberInt(2),
            ""ListValues"" : [
                {
                    ""Key"" : ""No"",
                    ""Value"" : ""No""
                }, 
                {
                    ""Key"" : ""Yes"", 
                    ""Value"" : ""Yes""
                }
            ], 
            ""SelectedValue"" : ""No"", 
            ""FileOrder"" : NumberInt(19), 
            ""IsRequired"" : true, 
            ""IsCollectedAtIntake"" : true
        }, 
        {
            ""_id"" : ObjectId(""592447f0c5f3460eb8311899""), 
            ""cdt"" : ISODate(""2017-05-23T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2017-12-11T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""NATURE"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""NATURE"", 
            ""Description"" : ""InjNature\r\n"", 
            ""Label"" : ""NATURE"", 
            ""HelpText"" : ""INJURY NATURE"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(2), 
            ""ListValues"" : [
                {
                    ""Key"" : ""Abrasion"",
                    ""Value"" : ""Abrasion""
                }, 
                {
                    ""Key"" : ""Abrasion/Scratch"", 
                    ""Value"" : ""Abrasion/Scratch""
                }, 
                {
                    ""Key"" : ""Accommodation"", 
                    ""Value"" : ""Accommodation""
                }, 
                {
                    ""Key"" : ""Accommodation unavailable"", 
                    ""Value"" : ""Accommodation unavailable""
                }, 
                {
                    ""Key"" : ""Accommodation- Parking"", 
                    ""Value"" : ""Accommodation- Parking""
                }, 
                {
                    ""Key"" : ""Accommodations- Unnecessary"", 
                    ""Value"" : ""Accommodations- Unnecessary""
                }, 
                {
                    ""Key"" : ""Accommodations-Permanent"", 
                    ""Value"" : ""Accommodations-Permanent""
                }, 
                {
                    ""Key"" : ""Adoption"", 
                    ""Value"" : ""Adoption""
                }, 
                {
                    ""Key"" : ""Aids"", 
                    ""Value"" : ""Aids""
                }, 
                {
                    ""Key"" : ""All Other"", 
                    ""Value"" : ""All Other""
                }, 
                {
                    ""Key"" : ""All Other Occupational Disease"", 
                    ""Value"" : ""All Other Occupational Disease""
                }, 
                {
                    ""Key"" : ""Allergic Reaction"", 
                    ""Value"" : ""Allergic Reaction""
                }, 
                {
                    ""Key"" : ""Amputation"", 
                    ""Value"" : ""Amputation""
                }, 
                {
                    ""Key"" : ""Asbestosis"", 
                    ""Value"" : ""Asbestosis""
                }, 
                {
                    ""Key"" : ""Asphyxiation"", 
                    ""Value"" : ""Asphyxiation""
                }, 
                {
                    ""Key"" : ""Bite"", 
                    ""Value"" : ""Bite""
                }, 
                {
                    ""Key"" : ""Bite Wound"", 
                    ""Value"" : ""Bite Wound""
                }, 
                {
                    ""Key"" : ""Blister"", 
                    ""Value"" : ""Blister""
                }, 
                {
                    ""Key"" : ""Burn"", 
                    ""Value"" : ""Burn""
                }, 
                {
                    ""Key"" : ""Cancer"", 
                    ""Value"" : ""Cancer""
                }, 
                {
                    ""Key"" : ""Cancers"", 
                    ""Value"" : ""Cancers""
                }, 
                {
                    ""Key"" : ""Cardiac Condition"", 
                    ""Value"" : ""Cardiac Condition""
                }, 
                {
                    ""Key"" : ""Carpal Tunnel Syndrome"", 
                    ""Value"" : ""Carpal Tunnel Syndrome""
                }, 
                {
                    ""Key"" : ""Carpal tunnel"", 
                    ""Value"" : ""Carpal tunnel""
                }, 
                {
                    ""Key"" : ""Chemical Burn"", 
                    ""Value"" : ""Chemical Burn""
                }, 
                {
                    ""Key"" : ""Chemical Dependency"", 
                    ""Value"" : ""Chemical Dependency""
                }, 
                {
                    ""Key"" : ""Circulatory"", 
                    ""Value"" : ""Circulatory""
                }, 
                {
                    ""Key"" : ""Circulatory system diseases"", 
                    ""Value"" : ""Circulatory system diseases""
                }, 
                {
                    ""Key"" : ""Claim Inquiry"", 
                    ""Value"" : ""Claim Inquiry""
                }, 
                {
                    ""Key"" : ""Closed Head Injury"", 
                    ""Value"" : ""Closed Head Injury""
                }, 
                {
                    ""Key"" : ""Comm Dis Exp"", 
                    ""Value"" : ""Comm Dis Exp""
                }, 
                {
                    ""Key"" : ""Concussion"", 
                    ""Value"" : ""Concussion""
                }, 
                {
                    ""Key"" : ""Contagious Disease"", 
                    ""Value"" : ""Contagious Disease""
                }, 
                {
                    ""Key"" : ""Contusion"", 
                    ""Value"" : ""Contusion""
                }, 
                {
                    ""Key"" : ""Crushing"", 
                    ""Value"" : ""Crushing""
                }, 
                {
                    ""Key"" : ""Cut/Laceration"", 
                    ""Value"" : ""Cut/Laceration""
                }, 
                {
                    ""Key"" : ""Cut/Laceration - BBF"", 
                    ""Value"" : ""Cut/Laceration - BBF""
                }, 
                {
                    ""Key"" : ""Dermatitis"", 
                    ""Value"" : ""Dermatitis""
                }, 
                {
                    ""Key"" : ""Disaster LOA"", 
                    ""Value"" : ""Disaster LOA""
                }, 
                {
                    ""Key"" : ""Disease"", 
                    ""Value"" : ""Disease""
                }, 
                {
                    ""Key"" : ""Dislocation"", 
                    ""Value"" : ""Dislocation""
                }, 
                {
                    ""Key"" : ""Dust Disease Noc(All Other)"", 
                    ""Value"" : ""Dust Disease Noc(All Other)""
                }, 
                {
                    ""Key"" : ""Electric Shock"", 
                    ""Value"" : ""Electric Shock""
                }, 
                {
                    ""Key"" : ""Electrocution/Shock"", 
                    ""Value"" : ""Electrocution/Shock""
                }, 
                {
                    ""Key"" : ""Environmental Effect"", 
                    ""Value"" : ""Environmental Effect""
                }, 
                {
                    ""Key"" : ""Exp To Bbf"", 
                    ""Value"" : ""Exp To Bbf""
                }, 
                {
                    ""Key"" : ""Exposure"", 
                    ""Value"" : ""Exposure""
                }, 
                {
                    ""Key"" : ""Exposure To Chemical"", 
                    ""Value"" : ""Exposure To Chemical""
                }, 
                {
                    ""Key"" : ""Exposure to Chemical w/o rxn"", 
                    ""Value"" : ""Exposure to Chemical w/o rxn""
                }, 
                {
                    ""Key"" : ""Exposure to Disease/BBF w/o rx"", 
                    ""Value"" : ""Exposure to Disease/BBF w/o rx""
                }, 
                {
                    ""Key"" : ""Exposure to Fumes"", 
                    ""Value"" : ""Exposure to Fumes""
                }, 
                {
                    ""Key"" : ""FMLA - Adoption"", 
                    ""Value"" : ""FMLA - Adoption""
                }, 
                {
                    ""Key"" : ""FMLA - Child"", 
                    ""Value"" : ""FMLA - Child""
                }, 
                {
                    ""Key"" : ""FMLA - Influenza"", 
                    ""Value"" : ""FMLA - Influenza""
                }, 
                {
                    ""Key"" : ""FMLA - Parent"", 
                    ""Value"" : ""FMLA - Parent""
                }, 
                {
                    ""Key"" : ""FMLA - Self"", 
                    ""Value"" : ""FMLA - Self""
                }, 
                {
                    ""Key"" : ""FMLA - Spouse"", 
                    ""Value"" : ""FMLA - Spouse""
                }, 
                {
                    ""Key"" : ""FMLA Military LOA"", 
                    ""Value"" : ""FMLA Military LOA""
                }, 
                {
                    ""Key"" : ""FMLA- Foster Care"", 
                    ""Value"" : ""FMLA- Foster Care""
                }, 
                {
                    ""Key"" : ""FMLA- Maternity"", 
                    ""Value"" : ""FMLA- Maternity""
                }, 
                {
                    ""Key"" : ""FMLA- Military Exigency"", 
                    ""Value"" : ""FMLA- Military Exigency""
                }, 
                {
                    ""Key"" : ""FMLA- Paternity"", 
                    ""Value"" : ""FMLA- Paternity""
                }, 
                {
                    ""Key"" : ""FMLA-Other"", 
                    ""Value"" : ""FMLA-Other""
                }, 
                {
                    ""Key"" : ""Fainting"", 
                    ""Value"" : ""Fainting""
                }, 
                {
                    ""Key"" : ""Family LOA"", 
                    ""Value"" : ""Family LOA""
                }, 
                {
                    ""Key"" : ""Foreign Body"", 
                    ""Value"" : ""Foreign Body""
                }, 
                {
                    ""Key"" : ""Fracture"", 
                    ""Value"" : ""Fracture""
                }, 
                {
                    ""Key"" : ""Fractures"", 
                    ""Value"" : ""Fractures""
                }, 
                {
                    ""Key"" : ""Gastrointestinal Disorder"", 
                    ""Value"" : ""Gastrointestinal Disorder""
                }, 
                {
                    ""Key"" : ""Gunshot Wound"", 
                    ""Value"" : ""Gunshot Wound""
                }, 
                {
                    ""Key"" : ""Gynecological Disorder"", 
                    ""Value"" : ""Gynecological Disorder""
                }, 
                {
                    ""Key"" : ""Hearing Loss(Traumatic Only)"", 
                    ""Value"" : ""Hearing Loss(Traumatic Only)""
                }, 
                {
                    ""Key"" : ""Hearing loss/problems"", 
                    ""Value"" : ""Hearing loss/problems""
                }, 
                {
                    ""Key"" : ""Heat Prostration"", 
                    ""Value"" : ""Heat Prostration""
                }, 
                {
                    ""Key"" : ""Hernia"", 
                    ""Value"" : ""Hernia""
                }, 
                {
                    ""Key"" : ""Herniated"", 
                    ""Value"" : ""Herniated""
                }, 
                {
                    ""Key"" : ""Illness-Routine"", 
                    ""Value"" : ""Illness-Routine""
                }, 
                {
                    ""Key"" : ""Immunization Reaction"", 
                    ""Value"" : ""Immunization Reaction""
                }, 
                {
                    ""Key"" : ""Immunization Reaction - Flu"", 
                    ""Value"" : ""Immunization Reaction - Flu""
                }, 
                {
                    ""Key"" : ""Infection"", 
                    ""Value"" : ""Infection""
                }, 
                {
                    ""Key"" : ""Infertility Treatments"", 
                    ""Value"" : ""Infertility Treatments""
                }, 
                {
                    ""Key"" : ""Inflamation -  Arthritis"", 
                    ""Value"" : ""Inflamation -  Arthritis""
                }, 
                {
                    ""Key"" : ""Inflammation"", 
                    ""Value"" : ""Inflammation""
                }, 
                {
                    ""Key"" : ""Influenza"", 
                    ""Value"" : ""Influenza""
                }, 
                {
                    ""Key"" : ""Injury"", 
                    ""Value"" : ""Injury""
                }, 
                {
                    ""Key"" : ""Intracranial Injury"", 
                    ""Value"" : ""Intracranial Injury""
                }, 
                {
                    ""Key"" : ""Laceration"", 
                    ""Value"" : ""Laceration""
                }, 
                {
                    ""Key"" : ""Long Term Disability"", 
                    ""Value"" : ""Long Term Disability""
                }, 
                {
                    ""Key"" : ""MVA"", 
                    ""Value"" : ""MVA""
                }, 
                {
                    ""Key"" : ""Medical Leave"", 
                    ""Value"" : ""Medical Leave""
                }, 
                {
                    ""Key"" : ""Medical Leave-Accommodation"", 
                    ""Value"" : ""Medical Leave-Accommodation""
                }, 
                {
                    ""Key"" : ""Mental Disorder"", 
                    ""Value"" : ""Mental Disorder""
                }, 
                {
                    ""Key"" : ""Mental Stress"", 
                    ""Value"" : ""Mental Stress""
                }, 
                {
                    ""Key"" : ""Military LOA"", 
                    ""Value"" : ""Military LOA""
                }, 
                {
                    ""Key"" : ""Multi Diseases/Condition/Disor"", 
                    ""Value"" : ""Multi Diseases/Condition/Disor""
                }, 
                {
                    ""Key"" : ""Multiple Injuries"", 
                    ""Value"" : ""Multiple Injuries""
                }, 
                {
                    ""Key"" : ""Multiple Sclerosis"", 
                    ""Value"" : ""Multiple Sclerosis""
                }, 
                {
                    ""Key"" : ""Multiple Traumatic Injuries"", 
                    ""Value"" : ""Multiple Traumatic Injuries""
                }, 
                {
                    ""Key"" : ""Muscle Spasms"", 
                    ""Value"" : ""Muscle Spasms""
                }, 
                {
                    ""Key"" : ""Musculoskeletal"", 
                    ""Value"" : ""Musculoskeletal""
                }, 
                {
                    ""Key"" : ""Myocardial Infarction"", 
                    ""Value"" : ""Myocardial Infarction""
                }, 
                {
                    ""Key"" : ""Neurological Disorders - seizu"", 
                    ""Value"" : ""Neurological Disorders - seizu""
                }, 
                {
                    ""Key"" : ""Numbness"", 
                    ""Value"" : ""Numbness""
                }, 
                {
                    ""Key"" : ""Other"", 
                    ""Value"" : ""Other""
                }, 
                {
                    ""Key"" : ""Other Infectious Disease"", 
                    ""Value"" : ""Other Infectious Disease""
                }, 
                {
                    ""Key"" : ""Other Respiratory diseases"", 
                    ""Value"" : ""Other Respiratory diseases""
                }, 
                {
                    ""Key"" : ""Other Traumatic Injury"", 
                    ""Value"" : ""Other Traumatic Injury""
                }, 
                {
                    ""Key"" : ""PLOA-Extended"", 
                    ""Value"" : ""PLOA-Extended""
                }, 
                {
                    ""Key"" : ""Pain"", 
                    ""Value"" : ""Pain""
                }, 
                {
                    ""Key"" : ""Permanent Restriction"", 
                    ""Value"" : ""Permanent Restriction""
                }, 
                {
                    ""Key"" : ""Personal LOA"", 
                    ""Value"" : ""Personal LOA""
                }, 
                {
                    ""Key"" : ""Pinch"", 
                    ""Value"" : ""Pinch""
                }, 
                {
                    ""Key"" : ""Pneumonia"", 
                    ""Value"" : ""Pneumonia""
                }, 
                {
                    ""Key"" : ""Poisoning: Chemical"", 
                    ""Value"" : ""Poisoning: Chemical""
                }, 
                {
                    ""Key"" : ""Preg-C-Section"", 
                    ""Value"" : ""Preg-C-Section""
                }, 
                {
                    ""Key"" : ""Pregnancy-Complicated"", 
                    ""Value"" : ""Pregnancy-Complicated""
                }, 
                {
                    ""Key"" : ""Pregnancy-Demise/Termination"", 
                    ""Value"" : ""Pregnancy-Demise/Termination""
                }, 
                {
                    ""Key"" : ""Pregnancy-Routine"", 
                    ""Value"" : ""Pregnancy-Routine""
                }, 
                {
                    ""Key"" : ""Pregnancy-Twins"", 
                    ""Value"" : ""Pregnancy-Twins""
                }, 
                {
                    ""Key"" : ""Puncture"", 
                    ""Value"" : ""Puncture""
                }, 
                {
                    ""Key"" : ""Puncture - BBF"", 
                    ""Value"" : ""Puncture - BBF""
                }, 
                {
                    ""Key"" : ""Radiation"", 
                    ""Value"" : ""Radiation""
                }, 
                {
                    ""Key"" : ""Radiation Sickness"", 
                    ""Value"" : ""Radiation Sickness""
                }, 
                {
                    ""Key"" : ""Respiratory Disorders"", 
                    ""Value"" : ""Respiratory Disorders""
                }, 
                {
                    ""Key"" : ""Rupture"", 
                    ""Value"" : ""Rupture""
                }, 
                {
                    ""Key"" : ""Seizure"", 
                    ""Value"" : ""Seizure""
                }, 
                {
                    ""Key"" : ""Severance"", 
                    ""Value"" : ""Severance""
                }, 
                {
                    ""Key"" : ""Signs or Symptoms"", 
                    ""Value"" : ""Signs or Symptoms""
                }, 
                {
                    ""Key"" : ""Skin Disorder"", 
                    ""Value"" : ""Skin Disorder""
                }, 
                {
                    ""Key"" : ""Skin Infection"", 
                    ""Value"" : ""Skin Infection""
                }, 
                {
                    ""Key"" : ""Skin Sensitivit"", 
                    ""Value"" : ""Skin Sensitivit""
                }, 
                {
                    ""Key"" : ""Sprain/Strain"", 
                    ""Value"" : ""Sprain/Strain""
                }, 
                {
                    ""Key"" : ""Strain/Sprain"", 
                    ""Value"" : ""Strain/Sprain""
                }, 
                {
                    ""Key"" : ""Stroke"", 
                    ""Value"" : ""Stroke""
                }, 
                {
                    ""Key"" : ""Surgery"", 
                    ""Value"" : ""Surgery""
                }, 
                {
                    ""Key"" : ""Surgical Procedure"", 
                    ""Value"" : ""Surgical Procedure""
                }, 
                {
                    ""Key"" : ""Surgical Proceedure"", 
                    ""Value"" : ""Surgical Proceedure""
                }, 
                {
                    ""Key"" : ""TB Conversion"", 
                    ""Value"" : ""TB Conversion""
                }, 
                {
                    ""Key"" : ""Tendonitis"", 
                    ""Value"" : ""Tendonitis""
                }, 
                {
                    ""Key"" : ""Tracking"", 
                    ""Value"" : ""Tracking""
                }, 
                {
                    ""Key"" : ""Trauma to nerve"", 
                    ""Value"" : ""Trauma to nerve""
                }, 
                {
                    ""Key"" : ""Tuberculosis(TB)"", 
                    ""Value"" : ""Tuberculosis(TB)""
                }, 
                {
                    ""Key"" : ""Tumor"", 
                    ""Value"" : ""Tumor""
                }, 
                {
                    ""Key"" : ""Unknown"", 
                    ""Value"" : ""Unknown""
                }, 
                {
                    ""Key"" : ""Vascular - Migraine"", 
                    ""Value"" : ""Vascular - Migraine""
                }, 
                {
                    ""Key"" : ""Vascular Loss"", 
                    ""Value"" : ""Vascular Loss""
                }, 
                {
                    ""Key"" : ""Vertigo"", 
                    ""Value"" : ""Vertigo""
                }, 
                {
                    ""Key"" : ""Vision Loss"", 
                    ""Value"" : ""Vision Loss""
                }, 
                {
                    ""Key"" : ""Vision loss/problems"", 
                    ""Value"" : ""Vision loss/problems""
                }
            ], 
            ""SelectedValue"" : ""Pregnancy-Routine"", 
            ""FileOrder"" : NumberInt(21), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""5924482fc5f3460eb83118a1""), 
            ""cdt"" : ISODate(""2017-05-23T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2017-05-23T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""PART"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""PART"", 
            ""Description"" : null, 
            ""Label"" : ""PART"", 
            ""HelpText"" : ""Injury Part"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""Reproductive System"", 
            ""FileOrder"" : NumberInt(22), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""59244852c5f3460eb83118a7""), 
            ""cdt"" : ISODate(""2017-05-23T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2017-12-11T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""DEGREE"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""DEGREE"", 
            ""Description"" : null, 
            ""Label"" : ""DEGREE"", 
            ""HelpText"" : ""Injury Degree"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(2), 
            ""ListValues"" : [
                {
                    ""Key"" : ""ACC"",
                    ""Value"" : ""ACC""
                }, 
                {
                    ""Key"" : ""Death Claim"", 
                    ""Value"" : ""Death Claim""
                }, 
                {
                    ""Key"" : ""Lost Time"", 
                    ""Value"" : ""Lost Time""
                }, 
                {
                    ""Key"" : ""Medical Only"", 
                    ""Value"" : ""Medical Only""
                }, 
                {
                    ""Key"" : ""NTS"", 
                    ""Value"" : ""NTS""
                }, 
                {
                    ""Key"" : ""No JRIR"", 
                    ""Value"" : ""No JRIR""
                }, 
                {
                    ""Key"" : ""STD"", 
                    ""Value"" : ""STD""
                }
            ], 
            ""SelectedValue"" : ""STD"", 
            ""FileOrder"" : NumberInt(23), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""59244940c5f3460eb83118c8""), 
            ""cdt"" : ISODate(""2017-05-23T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2017-08-26T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""WPROCESS"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""WProcess"", 
            ""Description"" : null, 
            ""Label"" : ""WProcess"", 
            ""HelpText"" : "" Work Process"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [
                {
                    ""Key"" : ""administrative/clerical"",
                    ""Value"" : ""administrative/clerical""
                }, 
                {
                    ""Key"" : ""cashier duties"", 
                    ""Value"" : ""cashier duties""
                }, 
                {
                    ""Key"" : ""PATIENT CARE"", 
                    ""Value"" : ""PATIENT CARE""
                }, 
                {
                    ""Key"" : ""maintenance equipment"", 
                    ""Value"" : ""maintenance equipment""
                }, 
                {
                    ""Key"" : ""Laundry"", 
                    ""Value"" : ""Laundry""
                }, 
                {
                    ""Key"" : ""Sterlizing Equipment"", 
                    ""Value"" : ""Sterlizing Equipment""
                }, 
                {
                    ""Key"" : ""cooking"", 
                    ""Value"" : ""cooking""
                }, 
                {
                    ""Key"" : ""shipping & receiving"", 
                    ""Value"" : ""shipping & receiving""
                }, 
                {
                    ""Key"" : ""Other Dietary Work"", 
                    ""Value"" : ""Other Dietary Work""
                }, 
                {
                    ""Key"" : ""janitorial duties"", 
                    ""Value"" : ""janitorial duties""
                }, 
                {
                    ""Key"" : ""maintenance facility"", 
                    ""Value"" : ""maintenance facility""
                }, 
                {
                    ""Key"" : ""Driving"", 
                    ""Value"" : ""Driving""
                }, 
                {
                    ""Key"" : ""security"", 
                    ""Value"" : ""security""
                }, 
                {
                    ""Key"" : ""transporting product"", 
                    ""Value"" : ""transporting product""
                }, 
                {
                    ""Key"" : ""maintenance grounds"", 
                    ""Value"" : ""maintenance grounds""
                }
            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(24), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""59244978c5f3460eb83118ce""), 
            ""cdt"" : ISODate(""2017-05-23T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2017-07-08T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""SAFETYEQUIP"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""Safetyequip"", 
            ""Description"" : null, 
            ""Label"" : ""Safetyequip"", 
            ""HelpText"" : ""Safety Equipment"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(25), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""592449c0c5f3460eb83118d8""), 
            ""cdt"" : ISODate(""2017-05-23T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2017-07-08T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""SAFETYUSED"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""Safetyused"", 
            ""Description"" : null, 
            ""Label"" : ""Safetyused"", 
            ""HelpText"" : ""Safety Used"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(26), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""5925700dc5f3490eb806e88e""), 
            ""cdt"" : ISODate(""2017-05-24T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""mdt"" : ISODate(""2017-05-24T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""RBS"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""Reserve Balance Sum"", 
            ""Description"" : ""Reserve Balance Sum from Coverys"", 
            ""Label"" : ""Reserve Balance Sum"", 
            ""DataType"" : NumberInt(2), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : null, 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""59257025c5f3490eb806e892""), 
            ""cdt"" : ISODate(""2017-05-24T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""mdt"" : ISODate(""2018-03-12T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""PS"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""TOTAL COST"", 
            ""Description"" : ""Paid Sum from Coverys"", 
            ""Label"" : ""TOTAL COST"", 
            ""DataType"" : NumberInt(2), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : {
                ""_t"" : ""System.Decimal"", 
                ""_v"" : ""0.00""
            }, 
            ""FileOrder"" : null, 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""59257040c5f3490eb806e897""), 
            ""cdt"" : ISODate(""2017-05-24T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""mdt"" : ISODate(""2017-05-24T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""IS"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""Incurred Sum"", 
            ""Description"" : ""Incurred Sum from Coverys"", 
            ""Label"" : ""Incurred Sum"", 
            ""DataType"" : NumberInt(2), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : null, 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""59257060c5f3490eb806e89b""), 
            ""cdt"" : ISODate(""2017-05-24T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""mdt"" : ISODate(""2017-05-24T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""IPC"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""Ind Pd Custom"", 
            ""Description"" : ""Ind Pd Custom from Coverys"", 
            ""Label"" : ""Ind Pd Custom"", 
            ""DataType"" : NumberInt(2), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : null, 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""59257074c5f3490eb806e89e""), 
            ""cdt"" : ISODate(""2017-05-24T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""mdt"" : ISODate(""2017-05-24T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""MPC"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""Med Pd Custom"", 
            ""Description"" : ""Med Pd Custom from Coverys"", 
            ""Label"" : ""Med Pd Custom"", 
            ""DataType"" : NumberInt(2), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : null, 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""5925708ec5f3490eb806e8a3""), 
            ""cdt"" : ISODate(""2017-05-24T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""mdt"" : ISODate(""2017-05-24T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""EPC"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""Exp Pd Custom"", 
            ""Description"" : ""Exp Pd Custom from Coverys"", 
            ""Label"" : ""Exp Pd Custom"", 
            ""DataType"" : NumberInt(2), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : null, 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""592570b1c5f3490eb806e8ac""), 
            ""cdt"" : ISODate(""2017-05-24T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""mdt"" : ISODate(""2017-05-24T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""IRBC"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""Ind Res Bal Custom"", 
            ""Description"" : ""Ind Res Bal Custom from Coverys"", 
            ""Label"" : ""Ind Res Bal Custom"", 
            ""DataType"" : NumberInt(2), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : null, 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""592570cbc5f3490eb806e8be""), 
            ""cdt"" : ISODate(""2017-05-24T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""mdt"" : ISODate(""2017-07-13T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""MRBC"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""Med Res Bal"", 
            ""Description"" : ""Med Res Bal Custom from Coverys"", 
            ""Label"" : ""Med Res Bal"", 
            ""DataType"" : NumberInt(2), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : null, 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""592570e5c5f3490eb806e8d5""), 
            ""cdt"" : ISODate(""2017-05-24T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""mdt"" : ISODate(""2017-05-24T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""ERBC"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""Exp Res Bal Custom"", 
            ""Description"" : ""Exp Res Bal Custom from Coverys"", 
            ""Label"" : ""Exp Res Bal Custom"", 
            ""DataType"" : NumberInt(2), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : null, 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""5952a21cc5f3302eac8736dd""), 
            ""cdt"" : ISODate(""2017-06-27T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2017-12-11T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""PARTTYPE"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""INJURY PART TYPE"", 
            ""Description"" : ""Injury Body Side"", 
            ""Label"" : ""INJURY PART TYPE"", 
            ""HelpText"" : ""InjBodySide"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(2), 
            ""ListValues"" : [
                {
                    ""Key"" : ""Body Systems"",
                    ""Value"" : ""Body Systems""
                }, 
                {
                    ""Key"" : ""Head"", 
                    ""Value"" : ""Head""
                }, 
                {
                    ""Key"" : ""Lower Extremities"", 
                    ""Value"" : ""Lower Extremities""
                }, 
                {
                    ""Key"" : ""Multiple Body Parts"", 
                    ""Value"" : ""Multiple Body Parts""
                }, 
                {
                    ""Key"" : ""Neck"", 
                    ""Value"" : ""Neck""
                }, 
                {
                    ""Key"" : ""Not Classified"", 
                    ""Value"" : ""Not Classified""
                }, 
                {
                    ""Key"" : ""Trunk"", 
                    ""Value"" : ""Trunk""
                }, 
                {
                    ""Key"" : ""Upper Extremities"", 
                    ""Value"" : ""Upper Extremities""
                }
            ], 
            ""SelectedValue"" : ""Body Systems"", 
            ""FileOrder"" : NumberInt(29), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""5952a39ac5f3302eac87370f""), 
            ""cdt"" : ISODate(""2017-06-27T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2017-12-11T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""ACTIVITY"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""INJURY ACTIVITY"", 
            ""Description"" : ""InjActivity"", 
            ""Label"" : ""INJURY ACTIVITY"", 
            ""HelpText"" : ""InjActivity"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(2), 
            ""ListValues"" : [
                {
                    ""Key"" : ""Cleaning"",
                    ""Value"" : ""Cleaning""
                }, 
                {
                    ""Key"" : ""Clerical duties"", 
                    ""Value"" : ""Clerical duties""
                }, 
                {
                    ""Key"" : ""Clinical- non patient care"", 
                    ""Value"" : ""Clinical- non patient care""
                }, 
                {
                    ""Key"" : ""Cooking"", 
                    ""Value"" : ""Cooking""
                }, 
                {
                    ""Key"" : ""Cutting/shearing"", 
                    ""Value"" : ""Cutting/shearing""
                }, 
                {
                    ""Key"" : ""Drilling"", 
                    ""Value"" : ""Drilling""
                }, 
                {
                    ""Key"" : ""Driving"", 
                    ""Value"" : ""Driving""
                }, 
                {
                    ""Key"" : ""Injection"", 
                    ""Value"" : ""Injection""
                }, 
                {
                    ""Key"" : ""Lifting"", 
                    ""Value"" : ""Lifting""
                }, 
                {
                    ""Key"" : ""Mopping"", 
                    ""Value"" : ""Mopping""
                }, 
                {
                    ""Key"" : ""Moving stock"", 
                    ""Value"" : ""Moving stock""
                }, 
                {
                    ""Key"" : ""Opening door/window/etc"", 
                    ""Value"" : ""Opening door/window/etc""
                }, 
                {
                    ""Key"" : ""Operating machine"", 
                    ""Value"" : ""Operating machine""
                }, 
                {
                    ""Key"" : ""Painting"", 
                    ""Value"" : ""Painting""
                }, 
                {
                    ""Key"" : ""Patient care"", 
                    ""Value"" : ""Patient care""
                }, 
                {
                    ""Key"" : ""Programming"", 
                    ""Value"" : ""Programming""
                }, 
                {
                    ""Key"" : ""Pushing/pulling"", 
                    ""Value"" : ""Pushing/pulling""
                }, 
                {
                    ""Key"" : ""Repairing equipment"", 
                    ""Value"" : ""Repairing equipment""
                }, 
                {
                    ""Key"" : ""Security duties"", 
                    ""Value"" : ""Security duties""
                }, 
                {
                    ""Key"" : ""Sheath pulls"", 
                    ""Value"" : ""Sheath pulls""
                }, 
                {
                    ""Key"" : ""Sitting down/standing up"", 
                    ""Value"" : ""Sitting down/standing up""
                }, 
                {
                    ""Key"" : ""Sterilization"", 
                    ""Value"" : ""Sterilization""
                }, 
                {
                    ""Key"" : ""Suturing"", 
                    ""Value"" : ""Suturing""
                }, 
                {
                    ""Key"" : ""Suturing/surgery"", 
                    ""Value"" : ""Suturing/surgery""
                }, 
                {
                    ""Key"" : ""Typing"", 
                    ""Value"" : ""Typing""
                }, 
                {
                    ""Key"" : ""Unknown"", 
                    ""Value"" : ""Unknown""
                }, 
                {
                    ""Key"" : ""Walking"", 
                    ""Value"" : ""Walking""
                }, 
                {
                    ""Key"" : ""Washing hands"", 
                    ""Value"" : ""Washing hands""
                }
            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(30), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""5952a63cc5f3302eac87392c""), 
            ""cdt"" : ISODate(""2017-06-27T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2017-12-11T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""TYPE"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""OSHA INJURY SUB TYPE"", 
            ""Description"" : ""OSHA_InjSubType"", 
            ""Label"" : ""OSHA INJURY SUB TYPE"", 
            ""HelpText"" : ""OSHA_InjSubType"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(2), 
            ""ListValues"" : [
                {
                    ""Key"" : ""Chemical(formaldehyde / other)"",
                    ""Value"" : ""Chemical(formaldehyde / other)""
                }, 
                {
                    ""Key"" : ""Contact dermatitis"", 
                    ""Value"" : ""Contact dermatitis""
                }, 
                {
                    ""Key"" : ""Dust diseases"", 
                    ""Value"" : ""Dust diseases""
                }, 
                {
                    ""Key"" : ""Heat stress"", 
                    ""Value"" : ""Heat stress""
                }, 
                {
                    ""Key"" : ""Heatstroke"", 
                    ""Value"" : ""Heatstroke""
                }, 
                {
                    ""Key"" : ""Infectious disease"", 
                    ""Value"" : ""Infectious disease""
                }, 
                {
                    ""Key"" : ""Infectious material cut"", 
                    ""Value"" : ""Infectious material cut""
                }, 
                {
                    ""Key"" : ""Inflammation of skin"", 
                    ""Value"" : ""Inflammation of skin""
                }, 
                {
                    ""Key"" : ""Loss of consciousness"", 
                    ""Value"" : ""Loss of consciousness""
                }, 
                {
                    ""Key"" : ""Low back pain"", 
                    ""Value"" : ""Low back pain""
                }, 
                {
                    ""Key"" : ""Minor medical"", 
                    ""Value"" : ""Minor medical""
                }, 
                {
                    ""Key"" : ""Needlestick"", 
                    ""Value"" : ""Needlestick""
                }, 
                {
                    ""Key"" : ""Occupational asthma"", 
                    ""Value"" : ""Occupational asthma""
                }, 
                {
                    ""Key"" : ""Other"", 
                    ""Value"" : ""Other""
                }, 
                {
                    ""Key"" : ""Radiation exposure"", 
                    ""Value"" : ""Radiation exposure""
                }, 
                {
                    ""Key"" : ""Rash"", 
                    ""Value"" : ""Rash""
                }, 
                {
                    ""Key"" : ""Repeatitive trauma"", 
                    ""Value"" : ""Repeatitive trauma""
                }, 
                {
                    ""Key"" : ""Toxic inhalation"", 
                    ""Value"" : ""Toxic inhalation""
                }, 
                {
                    ""Key"" : ""Tuberculosis"", 
                    ""Value"" : ""Tuberculosis""
                }
            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(31), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""5952a753c5f3302eac873958""), 
            ""cdt"" : ISODate(""2017-06-27T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2017-12-11T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""BODYPARTS"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""BODY PARTS"", 
            ""Description"" : null, 
            ""Label"" : ""BODY PARTS"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(2), 
            ""ListValues"" : [
                {
                    ""Key"" : ""Body Systems"",
                    ""Value"" : ""Body Systems""
                }, 
                {
                    ""Key"" : ""Head"", 
                    ""Value"" : ""Head""
                }, 
                {
                    ""Key"" : ""Lower Extremities"", 
                    ""Value"" : ""Lower Extremities""
                }, 
                {
                    ""Key"" : ""Multiple Body Parts"", 
                    ""Value"" : ""Multiple Body Parts""
                }, 
                {
                    ""Key"" : ""Neck"", 
                    ""Value"" : ""Neck""
                }, 
                {
                    ""Key"" : ""Not Classified"", 
                    ""Value"" : ""Not Classified""
                }, 
                {
                    ""Key"" : ""Trunk"", 
                    ""Value"" : ""Trunk""
                }, 
                {
                    ""Key"" : ""Upper Extremities"", 
                    ""Value"" : ""Upper Extremities""
                }
            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(32), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""5953a391c5f32f1a88e7b103""), 
            ""cdt"" : ISODate(""2017-06-28T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2017-06-28T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""DATETREATED"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""Date First Treated"", 
            ""Description"" : null, 
            ""Label"" : ""Date First Treated"", 
            ""HelpText"" : ""Date First Treated"", 
            ""DataType"" : NumberInt(8), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(32), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""5953a426c5f32f1a88e7b118""), 
            ""cdt"" : ISODate(""2017-06-28T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2017-06-28T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""CERTIFIEDDATE"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""Certification"", 
            ""Description"" : ""Certification"", 
            ""Label"" : ""Certification"", 
            ""HelpText"" : ""Certification"", 
            ""DataType"" : NumberInt(8), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(35), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""5953a44ec5f32f1a88e7b11e""), 
            ""cdt"" : ISODate(""2017-06-28T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2018-03-29T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""DESCRIPTION"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""E"", 
            ""Description"" : null, 
            ""Label"" : ""E"", 
            ""HelpText"" : ""InjDescriptionOfInjury"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(36), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""5953a474c5f32f1a88e7b124""), 
            ""cdt"" : ISODate(""2017-06-28T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2018-03-27T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""PREPAREDBY"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""A"", 
            ""Description"" : ""InjPreparedBy\r\n"", 
            ""Label"" : ""A"", 
            ""HelpText"" : ""InjPreparedBy"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(37), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""5953a4c0c5f32f1a88e7b12c""), 
            ""cdt"" : ISODate(""2017-06-28T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2018-03-27T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""POSITION"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""B"", 
            ""Description"" : ""InjPrepByTitle"", 
            ""Label"" : ""B"", 
            ""HelpText"" : ""InjPrepByTitle"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(38), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""5953a4e7c5f32f1a88e7b131""), 
            ""cdt"" : ISODate(""2017-06-28T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2018-03-27T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""PREPAREDATE"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""C"", 
            ""Description"" : ""InjPrepByDate\r\n"", 
            ""Label"" : ""C"", 
            ""HelpText"" : ""InjPrepByDate"", 
            ""DataType"" : NumberInt(8), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(39), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""5953a50fc5f32f1a88e7b137""), 
            ""cdt"" : ISODate(""2017-06-28T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2018-03-27T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""PREPAREPHONE"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""D"", 
            ""Description"" : ""InjPrepPhone\r\n"", 
            ""Label"" : ""D"", 
            ""HelpText"" : ""InjPrepPhone"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(40), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""5953a536c5f32f1a88e7b149""), 
            ""cdt"" : ISODate(""2017-06-28T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2017-06-28T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""DATEREPORTED"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""DateNotifiedEmployer"", 
            ""Description"" : ""DateNotifiedEmployer\r\n"", 
            ""Label"" : ""DateNotifiedEmployer"", 
            ""HelpText"" : ""DateNotifiedEmployer"", 
            ""DataType"" : NumberInt(8), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(41), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""5953a58fc5f32f1a88e7b173""), 
            ""cdt"" : ISODate(""2017-06-28T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2017-06-28T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""OPRIVACY"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""OSHA_PrivacyConcern"", 
            ""Description"" : ""OSHA_PrivacyConcern\r\n"", 
            ""Label"" : ""OSHA_PrivacyConcern"", 
            ""HelpText"" : ""OSHA_PrivacyConcern"", 
            ""DataType"" : NumberInt(4), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(42), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""595fef2bc5f3422b389762cb""), 
            ""cdt"" : ISODate(""2017-07-07T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2017-07-07T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""MEMO"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""Memo"", 
            ""Description"" : ""Memo"", 
            ""Label"" : ""Memo"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : ""LD - OFF 10/19/17 - 1/10/18"", 
            ""FileOrder"" : null, 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""59666c25c5f32f3394c887c4""), 
            ""cdt"" : ISODate(""2017-07-12T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2017-12-11T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""PHYEXE"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""Is Physician - Executive"", 
            ""Description"" : ""Determines if Physician / Executive policy will be selected pay code will be send"", 
            ""Label"" : ""Is Physician - Executive"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(2), 
            ""ListValues"" : [
                {
                    ""Key"" : ""No"",
                    ""Value"" : ""No""
                }, 
                {
                    ""Key"" : ""Yes"", 
                    ""Value"" : ""Yes""
                }
            ], 
            ""SelectedValue"" : ""No"", 
            ""FileOrder"" : null, 
            ""IsRequired"" : true, 
            ""IsCollectedAtIntake"" : true
        }, 
        {
            ""_id"" : ObjectId(""596cb935c5f3301eb0f7e80c""), 
            ""cdt"" : ISODate(""2017-07-17T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2017-07-17T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""OILLNESS"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""OSHA Illness Type"", 
            ""Description"" : null, 
            ""Label"" : ""OSHA Illness Type"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : null, 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""596cc514c5f3301eb0f7ea5a""), 
            ""cdt"" : ISODate(""2017-07-17T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2017-07-17T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58cfeccdc5f32f16b02c2ce5""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""INJBODYSIDE"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""INJURY BODY SIDE"", 
            ""Description"" : ""INJURY BODY SIDE"", 
            ""Label"" : ""INJURY BODY SIDE"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [

            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : null, 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""599ee862c5f3441298871d32""), 
            ""cdt"" : ISODate(""2017-08-24T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2018-03-20T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""CAUSE"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""Cause of Injury  "", 
            ""Description"" : ""Injury Cause"", 
            ""Label"" : ""CAUSE"", 
            ""HelpText"" : ""CAUSE"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(2), 
            ""ListValues"" : [
                {
                    ""Key"" : ""Abrasion"",
                    ""Value"" : ""Abrasion""
                }, 
                {
                    ""Key"" : ""Abusive Or Combative Patient"", 
                    ""Value"" : ""Abusive Or Combative Patient""
                }, 
                {
                    ""Key"" : ""Abusive/Combative Patient"", 
                    ""Value"" : ""Abusive/Combative Patient""
                }, 
                {
                    ""Key"" : ""After Disposal/Protruded Trash"", 
                    ""Value"" : ""After Disposal/Protruded Trash""
                }, 
                {
                    ""Key"" : ""After Use/Before Disposal"", 
                    ""Value"" : ""After Use/Before Disposal""
                }, 
                {
                    ""Key"" : ""Animal Or Insect"", 
                    ""Value"" : ""Animal Or Insect""
                }, 
                {
                    ""Key"" : ""Animal bite"", 
                    ""Value"" : ""Animal bite""
                }, 
                {
                    ""Key"" : ""Animal/Insect Injury - misc"", 
                    ""Value"" : ""Animal/Insect Injury - misc""
                }, 
                {
                    ""Key"" : ""Animal/Insect bite"", 
                    ""Value"" : ""Animal/Insect bite""
                }, 
                {
                    ""Key"" : ""Assisting With Pnt Procedure"", 
                    ""Value"" : ""Assisting With Pnt Procedure""
                }, 
                {
                    ""Key"" : ""Assisting With Pt Procedure"", 
                    ""Value"" : ""Assisting With Pt Procedure""
                }, 
                {
                    ""Key"" : ""Before Use Of Item"", 
                    ""Value"" : ""Before Use Of Item""
                }, 
                {
                    ""Key"" : ""Bend/reach/twist"", 
                    ""Value"" : ""Bend/reach/twist""
                }, 
                {
                    ""Key"" : ""Bite by Animal"", 
                    ""Value"" : ""Bite by Animal""
                }, 
                {
                    ""Key"" : ""Bite/sting by insect"", 
                    ""Value"" : ""Bite/sting by insect""
                }, 
                {
                    ""Key"" : ""Brief/Single Noise"", 
                    ""Value"" : ""Brief/Single Noise""
                }, 
                {
                    ""Key"" : ""Bump against object/equip - mi"", 
                    ""Value"" : ""Bump against object/equip - mi""
                }, 
                {
                    ""Key"" : ""Bump against object/equip-misc"", 
                    ""Value"" : ""Bump against object/equip-misc""
                }, 
                {
                    ""Key"" : ""Bump against tool/utensil"", 
                    ""Value"" : ""Bump against tool/utensil""
                }, 
                {
                    ""Key"" : ""Burn By Acid Chemicals"", 
                    ""Value"" : ""Burn By Acid Chemicals""
                }, 
                {
                    ""Key"" : ""Burn By Contact With Hot Objec"", 
                    ""Value"" : ""Burn By Contact With Hot Objec""
                }, 
                {
                    ""Key"" : ""Burn By Fire Or Flame"", 
                    ""Value"" : ""Burn By Fire Or Flame""
                }, 
                {
                    ""Key"" : ""Burn By Miscellaneous"", 
                    ""Value"" : ""Burn By Miscellaneous""
                }, 
                {
                    ""Key"" : ""Burn By Steam Or Hot Fluid"", 
                    ""Value"" : ""Burn By Steam Or Hot Fluid""
                }, 
                {
                    ""Key"" : ""Burn By Temperature Extremes"", 
                    ""Value"" : ""Burn By Temperature Extremes""
                }, 
                {
                    ""Key"" : ""Carrying"", 
                    ""Value"" : ""Carrying""
                }, 
                {
                    ""Key"" : ""Caught In Machine Or Machinery"", 
                    ""Value"" : ""Caught In Machine Or Machinery""
                }, 
                {
                    ""Key"" : ""Caught In/By Miscellaneous"", 
                    ""Value"" : ""Caught In/By Miscellaneous""
                }, 
                {
                    ""Key"" : ""Caught In/By Object Handled"", 
                    ""Value"" : ""Caught In/By Object Handled""
                }, 
                {
                    ""Key"" : ""Caught in collapsing strc/equi"", 
                    ""Value"" : ""Caught in collapsing strc/equi""
                }, 
                {
                    ""Key"" : ""Caught in/by equip/machine"", 
                    ""Value"" : ""Caught in/by equip/machine""
                }, 
                {
                    ""Key"" : ""Caught in/by object"", 
                    ""Value"" : ""Caught in/by object""
                }, 
                {
                    ""Key"" : ""Chemical/Soap"", 
                    ""Value"" : ""Chemical/Soap""
                }, 
                {
                    ""Key"" : ""Cleaning Floors - Mop/Wax/Swee"", 
                    ""Value"" : ""Cleaning Floors - Mop/Wax/Swee""
                }, 
                {
                    ""Key"" : ""Cleaning/Disassembling Device"", 
                    ""Value"" : ""Cleaning/Disassembling Device""
                }, 
                {
                    ""Key"" : ""Cleaning/Handling/Empty Trash"", 
                    ""Value"" : ""Cleaning/Handling/Empty Trash""
                }, 
                {
                    ""Key"" : ""Collision With A Fixed Object"", 
                    ""Value"" : ""Collision With A Fixed Object""
                }, 
                {
                    ""Key"" : ""Collison With Another Vehicle"", 
                    ""Value"" : ""Collison With Another Vehicle""
                }, 
                {
                    ""Key"" : ""Comm Disease Exposure"", 
                    ""Value"" : ""Comm Disease Exposure""
                }, 
                {
                    ""Key"" : ""Contact With Electric Current"", 
                    ""Value"" : ""Contact With Electric Current""
                }, 
                {
                    ""Key"" : ""Contact with Cold Object/Subst"", 
                    ""Value"" : ""Contact with Cold Object/Subst""
                }, 
                {
                    ""Key"" : ""Contact with Hot Object/Subst"", 
                    ""Value"" : ""Contact with Hot Object/Subst""
                }, 
                {
                    ""Key"" : ""Contact with hot object/substa"", 
                    ""Value"" : ""Contact with hot object/substa""
                }, 
                {
                    ""Key"" : ""Contributing Factor Unable to"", 
                    ""Value"" : ""Contributing Factor Unable to""
                }, 
                {
                    ""Key"" : ""Crash Of Aircraft"", 
                    ""Value"" : ""Crash Of Aircraft""
                }, 
                {
                    ""Key"" : ""Crawl/climb/step"", 
                    ""Value"" : ""Crawl/climb/step""
                }, 
                {
                    ""Key"" : ""Cut By Broken Glass"", 
                    ""Value"" : ""Cut By Broken Glass""
                }, 
                {
                    ""Key"" : ""Cut By Miscellaneous"", 
                    ""Value"" : ""Cut By Miscellaneous""
                }, 
                {
                    ""Key"" : ""Cut By Tool Or Utensil"", 
                    ""Value"" : ""Cut By Tool Or Utensil""
                }, 
                {
                    ""Key"" : ""Disassembling Device/Clng Eq/I"", 
                    ""Value"" : ""Disassembling Device/Clng Eq/I""
                }, 
                {
                    ""Key"" : ""Disposal Of Needle"", 
                    ""Value"" : ""Disposal Of Needle""
                }, 
                {
                    ""Key"" : ""Drawing Blood"", 
                    ""Value"" : ""Drawing Blood""
                }, 
                {
                    ""Key"" : ""Due To Ice"", 
                    ""Value"" : ""Due To Ice""
                }, 
                {
                    ""Key"" : ""During Use Of Item"", 
                    ""Value"" : ""During Use Of Item""
                }, 
                {
                    ""Key"" : ""Electrical Shock"", 
                    ""Value"" : ""Electrical Shock""
                }, 
                {
                    ""Key"" : ""Environmental heat"", 
                    ""Value"" : ""Environmental heat""
                }, 
                {
                    ""Key"" : ""Excessive Or Repetitive Use"", 
                    ""Value"" : ""Excessive Or Repetitive Use""
                }, 
                {
                    ""Key"" : ""Exp to Harmful Substance-misc"", 
                    ""Value"" : ""Exp to Harmful Substance-misc""
                }, 
                {
                    ""Key"" : ""Exp to Trauma/Stressful Event"", 
                    ""Value"" : ""Exp to Trauma/Stressful Event""
                }, 
                {
                    ""Key"" : ""Explosion"", 
                    ""Value"" : ""Explosion""
                }, 
                {
                    ""Key"" : ""Explosion Or Flare Back"", 
                    ""Value"" : ""Explosion Or Flare Back""
                }, 
                {
                    ""Key"" : ""Exposure - HIV/AIDS"", 
                    ""Value"" : ""Exposure - HIV/AIDS""
                }, 
                {
                    ""Key"" : ""Exposure - Influenza"", 
                    ""Value"" : ""Exposure - Influenza""
                }, 
                {
                    ""Key"" : ""Exposure - Medical Injection"", 
                    ""Value"" : ""Exposure - Medical Injection""
                }, 
                {
                    ""Key"" : ""Exposure - Meningitis"", 
                    ""Value"" : ""Exposure - Meningitis""
                }, 
                {
                    ""Key"" : ""Exposure - Pertussis"", 
                    ""Value"" : ""Exposure - Pertussis""
                }, 
                {
                    ""Key"" : ""Exposure - TB"", 
                    ""Value"" : ""Exposure - TB""
                }, 
                {
                    ""Key"" : ""Exposure to BBF"", 
                    ""Value"" : ""Exposure to BBF""
                }, 
                {
                    ""Key"" : ""Exposure to Chemicals"", 
                    ""Value"" : ""Exposure to Chemicals""
                }, 
                {
                    ""Key"" : ""Exposure to Disease/BBF - misc"", 
                    ""Value"" : ""Exposure to Disease/BBF - misc""
                }, 
                {
                    ""Key"" : ""Exposure to Disease/BBF-misc"", 
                    ""Value"" : ""Exposure to Disease/BBF-misc""
                }, 
                {
                    ""Key"" : ""Exposure to Fumes"", 
                    ""Value"" : ""Exposure to Fumes""
                }, 
                {
                    ""Key"" : ""Exposure to Radioactive Beam"", 
                    ""Value"" : ""Exposure to Radioactive Beam""
                }, 
                {
                    ""Key"" : ""Exposure to electricity"", 
                    ""Value"" : ""Exposure to electricity""
                }, 
                {
                    ""Key"" : ""Exposure to light"", 
                    ""Value"" : ""Exposure to light""
                }, 
                {
                    ""Key"" : ""Exposure to soap/sanitizer"", 
                    ""Value"" : ""Exposure to soap/sanitizer""
                }, 
                {
                    ""Key"" : ""Exposure- Meningitis"", 
                    ""Value"" : ""Exposure- Meningitis""
                }, 
                {
                    ""Key"" : ""Exposure- Other"", 
                    ""Value"" : ""Exposure- Other""
                }, 
                {
                    ""Key"" : ""Exposure- Pertussis"", 
                    ""Value"" : ""Exposure- Pertussis""
                }, 
                {
                    ""Key"" : ""Exposure-H1N1"", 
                    ""Value"" : ""Exposure-H1N1""
                }, 
                {
                    ""Key"" : ""Exposure-Radioactive Substance"", 
                    ""Value"" : ""Exposure-Radioactive Substance""
                }, 
                {
                    ""Key"" : ""Fainted"", 
                    ""Value"" : ""Fainted""
                }, 
                {
                    ""Key"" : ""Fainting"", 
                    ""Value"" : ""Fainting""
                }, 
                {
                    ""Key"" : ""Fall - misc."", 
                    ""Value"" : ""Fall - misc.""
                }, 
                {
                    ""Key"" : ""Fall Due to Slip on Water/Liqu"", 
                    ""Value"" : ""Fall Due to Slip on Water/Liqu""
                }, 
                {
                    ""Key"" : ""Fall From  Ladder Scaffolding"", 
                    ""Value"" : ""Fall From  Ladder Scaffolding""
                }, 
                {
                    ""Key"" : ""Fall From Collapsing Struc/Equ"", 
                    ""Value"" : ""Fall From Collapsing Struc/Equ""
                }, 
                {
                    ""Key"" : ""Fall From Different Level"", 
                    ""Value"" : ""Fall From Different Level""
                }, 
                {
                    ""Key"" : ""Fall From Slippery Conditions"", 
                    ""Value"" : ""Fall From Slippery Conditions""
                }, 
                {
                    ""Key"" : ""Fall From Slippery Conditions/"", 
                    ""Value"" : ""Fall From Slippery Conditions/""
                }, 
                {
                    ""Key"" : ""Fall Miscellaneous"", 
                    ""Value"" : ""Fall Miscellaneous""
                }, 
                {
                    ""Key"" : ""Fall On Same Level"", 
                    ""Value"" : ""Fall On Same Level""
                }, 
                {
                    ""Key"" : ""Fall To Lower Level"", 
                    ""Value"" : ""Fall To Lower Level""
                }, 
                {
                    ""Key"" : ""Fall due to Ice"", 
                    ""Value"" : ""Fall due to Ice""
                }, 
                {
                    ""Key"" : ""Fall due to fainting event"", 
                    ""Value"" : ""Fall due to fainting event""
                }, 
                {
                    ""Key"" : ""Fall due to slipping on ice"", 
                    ""Value"" : ""Fall due to slipping on ice""
                }, 
                {
                    ""Key"" : ""Fall due to slipping on object"", 
                    ""Value"" : ""Fall due to slipping on object""
                }, 
                {
                    ""Key"" : ""Fall due to trip"", 
                    ""Value"" : ""Fall due to trip""
                }, 
                {
                    ""Key"" : ""Fall due to trip over object"", 
                    ""Value"" : ""Fall due to trip over object""
                }, 
                {
                    ""Key"" : ""Fall due to trip over self"", 
                    ""Value"" : ""Fall due to trip over self""
                }, 
                {
                    ""Key"" : ""Fall due to water(inside)"", 
                    ""Value"" : ""Fall due to water(inside)""
                }, 
                {
                    ""Key"" : ""Fall due to water(outside)"", 
                    ""Value"" : ""Fall due to water(outside)""
                }, 
                {
                    ""Key"" : ""Fall from Slip on Water/Liquid"", 
                    ""Value"" : ""Fall from Slip on Water/Liquid""
                }, 
                {
                    ""Key"" : ""Fall from Trip Stair/Step/Curb"", 
                    ""Value"" : ""Fall from Trip Stair/Step/Curb""
                }, 
                {
                    ""Key"" : ""Fall from Trip/Uneven Surface"", 
                    ""Value"" : ""Fall from Trip/Uneven Surface""
                }, 
                {
                    ""Key"" : ""Faulty/Damaged Equipment"", 
                    ""Value"" : ""Faulty/Damaged Equipment""
                }, 
                {
                    ""Key"" : ""Fell/Trippd Due To Uneven Surf"", 
                    ""Value"" : ""Fell/Trippd Due To Uneven Surf""
                }, 
                {
                    ""Key"" : ""Fell/Tripped Due To Obstacle"", 
                    ""Value"" : ""Fell/Tripped Due To Obstacle""
                }, 
                {
                    ""Key"" : ""Fire - misc."", 
                    ""Value"" : ""Fire - misc.""
                }, 
                {
                    ""Key"" : ""Foreign Body In Eye"", 
                    ""Value"" : ""Foreign Body In Eye""
                }, 
                {
                    ""Key"" : ""Foreign matter in eye"", 
                    ""Value"" : ""Foreign matter in eye""
                }, 
                {
                    ""Key"" : ""Hit/Kick/Beat/Shove"", 
                    ""Value"" : ""Hit/Kick/Beat/Shove""
                }, 
                {
                    ""Key"" : ""Holding"", 
                    ""Value"" : ""Holding""
                }, 
                {
                    ""Key"" : ""Human Bite"", 
                    ""Value"" : ""Human Bite""
                }, 
                {
                    ""Key"" : ""Ingestion of Harmful Substance"", 
                    ""Value"" : ""Ingestion of Harmful Substance""
                }, 
                {
                    ""Key"" : ""Inhale Harmful Substance-Multi"", 
                    ""Value"" : ""Inhale Harmful Substance-Multi""
                }, 
                {
                    ""Key"" : ""Inhale Harmful Substance-Singl"", 
                    ""Value"" : ""Inhale Harmful Substance-Singl""
                }, 
                {
                    ""Key"" : ""Injection/Needlestick"", 
                    ""Value"" : ""Injection/Needlestick""
                }, 
                {
                    ""Key"" : ""Injured by handheld object/equ"", 
                    ""Value"" : ""Injured by handheld object/equ""
                }, 
                {
                    ""Key"" : ""Injured by tool/utensil"", 
                    ""Value"" : ""Injured by tool/utensil""
                }, 
                {
                    ""Key"" : ""Innoculation/Vaccination Adver"", 
                    ""Value"" : ""Innoculation/Vaccination Adver""
                }, 
                {
                    ""Key"" : ""Innoculation/Vaccination-Injur"", 
                    ""Value"" : ""Innoculation/Vaccination-Injur""
                }, 
                {
                    ""Key"" : ""Intentional Violence - misc"", 
                    ""Value"" : ""Intentional Violence - misc""
                }, 
                {
                    ""Key"" : ""Item Piercing Through Sharps C"", 
                    ""Value"" : ""Item Piercing Through Sharps C""
                }, 
                {
                    ""Key"" : ""Kneel/Squat"", 
                    ""Value"" : ""Kneel/Squat""
                }, 
                {
                    ""Key"" : ""Lift/Lower"", 
                    ""Value"" : ""Lift/Lower""
                }, 
                {
                    ""Key"" : ""Lifting/Transferring Patient"", 
                    ""Value"" : ""Lifting/Transferring Patient""
                }, 
                {
                    ""Key"" : ""Mult types-repetitive motions"", 
                    ""Value"" : ""Mult types-repetitive motions""
                }, 
                {
                    ""Key"" : ""Needlestick - BBF"", 
                    ""Value"" : ""Needlestick - BBF""
                }, 
                {
                    ""Key"" : ""Needlestick - clean"", 
                    ""Value"" : ""Needlestick - clean""
                }, 
                {
                    ""Key"" : ""Needlestick-BBF"", 
                    ""Value"" : ""Needlestick-BBF""
                }, 
                {
                    ""Key"" : ""No Cause"", 
                    ""Value"" : ""No Cause""
                }, 
                {
                    ""Key"" : ""Nonroadway - sudden start/stop"", 
                    ""Value"" : ""Nonroadway - sudden start/stop""
                }, 
                {
                    ""Key"" : ""Nonroadway collision - misc."", 
                    ""Value"" : ""Nonroadway collision - misc.""
                }, 
                {
                    ""Key"" : ""Other"", 
                    ""Value"" : ""Other""
                }, 
                {
                    ""Key"" : ""Other/Unknown"", 
                    ""Value"" : ""Other/Unknown""
                }, 
                {
                    ""Key"" : ""Overexertion - misc."", 
                    ""Value"" : ""Overexertion - misc.""
                }, 
                {
                    ""Key"" : ""Overexertion-misc"", 
                    ""Value"" : ""Overexertion-misc""
                }, 
                {
                    ""Key"" : ""Overexertion-obj-misc"", 
                    ""Value"" : ""Overexertion-obj-misc""
                }, 
                {
                    ""Key"" : ""Patient Care"", 
                    ""Value"" : ""Patient Care""
                }, 
                {
                    ""Key"" : ""Patient Moved"", 
                    ""Value"" : ""Patient Moved""
                }, 
                {
                    ""Key"" : ""Patient handling - misc"", 
                    ""Value"" : ""Patient handling - misc""
                }, 
                {
                    ""Key"" : ""Patient lateral transfer"", 
                    ""Value"" : ""Patient lateral transfer""
                }, 
                {
                    ""Key"" : ""Patient reposition"", 
                    ""Value"" : ""Patient reposition""
                }, 
                {
                    ""Key"" : ""Patient transfer"", 
                    ""Value"" : ""Patient transfer""
                }, 
                {
                    ""Key"" : ""Patient transport"", 
                    ""Value"" : ""Patient transport""
                }, 
                {
                    ""Key"" : ""Pedestrian-Vehicle Incident"", 
                    ""Value"" : ""Pedestrian-Vehicle Incident""
                }, 
                {
                    ""Key"" : ""Physical Contact w Other Perso"", 
                    ""Value"" : ""Physical Contact w Other Perso""
                }, 
                {
                    ""Key"" : ""Physical Contact with Other Pe"", 
                    ""Value"" : ""Physical Contact with Other Pe""
                }, 
                {
                    ""Key"" : ""Pulling Up Sheath"", 
                    ""Value"" : ""Pulling Up Sheath""
                }, 
                {
                    ""Key"" : ""Puncture"", 
                    ""Value"" : ""Puncture""
                }, 
                {
                    ""Key"" : ""Push/pull/turn"", 
                    ""Value"" : ""Push/pull/turn""
                }, 
                {
                    ""Key"" : ""Pushing/Pull/Lift Eq.Or Obj."",
                    ""Value"" : ""Pushing/Pull/Lift Eq. Or Obj.""
                }, 
                {
                    ""Key"" : ""Recapping Needle"", 
                    ""Value"" : ""Recapping Needle""
                }, 
                {
                    ""Key"" : ""Repeated Exposure to Noise"", 
                    ""Value"" : ""Repeated Exposure to Noise""
                }, 
                {
                    ""Key"" : ""Repetitive motion - misc"", 
                    ""Value"" : ""Repetitive motion - misc""
                }, 
                {
                    ""Key"" : ""Repetitive typing/mousing/text"", 
                    ""Value"" : ""Repetitive typing/mousing/text""
                }, 
                {
                    ""Key"" : ""Repetitive use of hand-no tool"", 
                    ""Value"" : ""Repetitive use of hand-no tool""
                }, 
                {
                    ""Key"" : ""Repetitive use of tools/equip"", 
                    ""Value"" : ""Repetitive use of tools/equip""
                }, 
                {
                    ""Key"" : ""Roadway - jackknife/overturned"", 
                    ""Value"" : ""Roadway - jackknife/overturned""
                }, 
                {
                    ""Key"" : ""Roadway - ran off surface"", 
                    ""Value"" : ""Roadway - ran off surface""
                }, 
                {
                    ""Key"" : ""Roadway - sudden start/stop"", 
                    ""Value"" : ""Roadway - sudden start/stop""
                }, 
                {
                    ""Key"" : ""Roadway collision - misc."", 
                    ""Value"" : ""Roadway collision - misc.""
                }, 
                {
                    ""Key"" : ""Roadway-vehicle hit obj/animal"", 
                    ""Value"" : ""Roadway-vehicle hit obj/animal""
                }, 
                {
                    ""Key"" : ""Robbery Or Criminal Assault"", 
                    ""Value"" : ""Robbery Or Criminal Assault""
                }, 
                {
                    ""Key"" : ""Rub/abraded - misc."", 
                    ""Value"" : ""Rub/abraded - misc.""
                }, 
                {
                    ""Key"" : ""Rub/abraded shoe/apprarel/acc."", 
                    ""Value"" : ""Rub/abraded shoe/apprarel/acc.""
                }, 
                {
                    ""Key"" : ""Rub/abraded-misc."", 
                    ""Value"" : ""Rub/abraded-misc.""
                }, 
                {
                    ""Key"" : ""Running"", 
                    ""Value"" : ""Running""
                }, 
                {
                    ""Key"" : ""Sharp - BBF"", 
                    ""Value"" : ""Sharp - BBF""
                }, 
                {
                    ""Key"" : ""Sharp - clean"", 
                    ""Value"" : ""Sharp - clean""
                }, 
                {
                    ""Key"" : ""Shooting"", 
                    ""Value"" : ""Shooting""
                }, 
                {
                    ""Key"" : ""Sitting"", 
                    ""Value"" : ""Sitting""
                }, 
                {
                    ""Key"" : ""Slip from slippery conditions"", 
                    ""Value"" : ""Slip from slippery conditions""
                }, 
                {
                    ""Key"" : ""Slip on ice"", 
                    ""Value"" : ""Slip on ice""
                }, 
                {
                    ""Key"" : ""Slip on object"", 
                    ""Value"" : ""Slip on object""
                }, 
                {
                    ""Key"" : ""Slip on water/liquid"", 
                    ""Value"" : ""Slip on water/liquid""
                }, 
                {
                    ""Key"" : ""Slip/Trip - misc."", 
                    ""Value"" : ""Slip/Trip - misc.""
                }, 
                {
                    ""Key"" : ""Slipped/Did Not Fall"", 
                    ""Value"" : ""Slipped/Did Not Fall""
                }, 
                {
                    ""Key"" : ""Standing"", 
                    ""Value"" : ""Standing""
                }, 
                {
                    ""Key"" : ""Stepped on/kicked object/equip"", 
                    ""Value"" : ""Stepped on/kicked object/equip""
                }, 
                {
                    ""Key"" : ""Strain Holding Or Carrying"", 
                    ""Value"" : ""Strain Holding Or Carrying""
                }, 
                {
                    ""Key"" : ""Strain Lifting"", 
                    ""Value"" : ""Strain Lifting""
                }, 
                {
                    ""Key"" : ""Strain Miscellaneous"", 
                    ""Value"" : ""Strain Miscellaneous""
                }, 
                {
                    ""Key"" : ""Strain Pushing Or Pulling"", 
                    ""Value"" : ""Strain Pushing Or Pulling""
                }, 
                {
                    ""Key"" : ""Strain Reaching"", 
                    ""Value"" : ""Strain Reaching""
                }, 
                {
                    ""Key"" : ""Strain Using Tool Or Machine"", 
                    ""Value"" : ""Strain Using Tool Or Machine""
                }, 
                {
                    ""Key"" : ""Strike Against Moving Parts"", 
                    ""Value"" : ""Strike Against Moving Parts""
                }, 
                {
                    ""Key"" : ""Strike Against Object Lifted"", 
                    ""Value"" : ""Strike Against Object Lifted""
                }, 
                {
                    ""Key"" : ""Strike Against Stationary Obj"", 
                    ""Value"" : ""Strike Against Stationary Obj""
                }, 
                {
                    ""Key"" : ""Strike Or Step Miscellaneous"", 
                    ""Value"" : ""Strike Or Step Miscellaneous""
                }, 
                {
                    ""Key"" : ""Struck By Falling Or Flying Ob"", 
                    ""Value"" : ""Struck By Falling Or Flying Ob""
                }, 
                {
                    ""Key"" : ""Struck By Miscellaneous"", 
                    ""Value"" : ""Struck By Miscellaneous""
                }, 
                {
                    ""Key"" : ""Struck By Motor Vehicle"", 
                    ""Value"" : ""Struck By Motor Vehicle""
                }, 
                {
                    ""Key"" : ""Struck By Moving Parts Of Mach"", 
                    ""Value"" : ""Struck By Moving Parts Of Mach""
                }, 
                {
                    ""Key"" : ""Struck By Object Handled By Ot"", 
                    ""Value"" : ""Struck By Object Handled By Ot""
                }, 
                {
                    ""Key"" : ""Struck By Object Lifted/Handle"", 
                    ""Value"" : ""Struck By Object Lifted/Handle""
                }, 
                {
                    ""Key"" : ""Struck by door"", 
                    ""Value"" : ""Struck by door""
                }, 
                {
                    ""Key"" : ""Struck by falling object"", 
                    ""Value"" : ""Struck by falling object""
                }, 
                {
                    ""Key"" : ""Struck by object/equip - misc"", 
                    ""Value"" : ""Struck by object/equip - misc""
                }, 
                {
                    ""Key"" : ""Struck by object/equip-misc"", 
                    ""Value"" : ""Struck by object/equip-misc""
                }, 
                {
                    ""Key"" : ""Struck by vehicle-nonroadway"", 
                    ""Value"" : ""Struck by vehicle-nonroadway""
                }, 
                {
                    ""Key"" : ""Struck/Kicked/Scratched-Animal"", 
                    ""Value"" : ""Struck/Kicked/Scratched-Animal""
                }, 
                {
                    ""Key"" : ""Surgery"", 
                    ""Value"" : ""Surgery""
                }, 
                {
                    ""Key"" : ""Transporting Patient"", 
                    ""Value"" : ""Transporting Patient""
                }, 
                {
                    ""Key"" : ""Trip on stair/step/curb"", 
                    ""Value"" : ""Trip on stair/step/curb""
                }, 
                {
                    ""Key"" : ""Trip over object"", 
                    ""Value"" : ""Trip over object""
                }, 
                {
                    ""Key"" : ""Trip over self"", 
                    ""Value"" : ""Trip over self""
                }, 
                {
                    ""Key"" : ""Trip over uneven surface"", 
                    ""Value"" : ""Trip over uneven surface""
                }, 
                {
                    ""Key"" : ""Unrelated to Inoculation"", 
                    ""Value"" : ""Unrelated to Inoculation""
                }, 
                {
                    ""Key"" : ""Vehicle Collision Miscellaneou"", 
                    ""Value"" : ""Vehicle Collision Miscellaneou""
                }, 
                {
                    ""Key"" : ""Vehicle Upset"", 
                    ""Value"" : ""Vehicle Upset""
                }, 
                {
                    ""Key"" : ""Walking"", 
                    ""Value"" : ""Walking""
                }, 
                {
                    ""Key"" : ""With Drawing/Inserting Needle"", 
                    ""Value"" : ""With Drawing/Inserting Needle""
                }
            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : null, 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""599ee887c5f3441298871d37""), 
            ""cdt"" : ISODate(""2017-08-24T00:00:00.000Z""), 
            ""cby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""mdt"" : ISODate(""2017-12-11T00:00:00.000Z""), 
            ""mby"" : ObjectId(""58b9d693c5f3332184eaf4cd""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""CAUSETYPE"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""Cause"", 
            ""Description"" : ""Cause Type"", 
            ""Label"" : ""CAUSE TYPE"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(2), 
            ""ListValues"" : [
                {
                    ""Key"" : ""Abraded by object"",
                    ""Value"" : ""Abraded by object""
                }, 
                {
                    ""Key"" : ""Abusive/Combative Patient"", 
                    ""Value"" : ""Abusive/Combative Patient""
                }, 
                {
                    ""Key"" : ""Animal/Insect Injury"", 
                    ""Value"" : ""Animal/Insect Injury""
                }, 
                {
                    ""Key"" : ""Caught in collapsing structur"", 
                    ""Value"" : ""Caught in collapsing structur""
                }, 
                {
                    ""Key"" : ""Caught in/by object/equip"", 
                    ""Value"" : ""Caught in/by object/equip""
                }, 
                {
                    ""Key"" : ""Exposure - Disease/BBF"", 
                    ""Value"" : ""Exposure - Disease/BBF""
                }, 
                {
                    ""Key"" : ""Exposure - Medical Injectio"", 
                    ""Value"" : ""Exposure - Medical Injectio""
                }, 
                {
                    ""Key"" : ""Exposure - Noise"", 
                    ""Value"" : ""Exposure - Noise""
                }, 
                {
                    ""Key"" : ""Exposure - electricity"", 
                    ""Value"" : ""Exposure - electricity""
                }, 
                {
                    ""Key"" : ""Exposure - hot/cold"", 
                    ""Value"" : ""Exposure - hot/cold""
                }, 
                {
                    ""Key"" : ""Exposure - radiation"", 
                    ""Value"" : ""Exposure - radiation""
                }, 
                {
                    ""Key"" : ""Exposure-Harmful Substance"", 
                    ""Value"" : ""Exposure-Harmful Substance""
                }, 
                {
                    ""Key"" : ""Fainting"", 
                    ""Value"" : ""Fainting""
                }, 
                {
                    ""Key"" : ""Fall"", 
                    ""Value"" : ""Fall""
                }, 
                {
                    ""Key"" : ""Fall to lower level"", 
                    ""Value"" : ""Fall to lower level""
                }, 
                {
                    ""Key"" : ""Fire"", 
                    ""Value"" : ""Fire""
                }, 
                {
                    ""Key"" : ""Fire/Explosion"", 
                    ""Value"" : ""Fire/Explosion""
                }, 
                {
                    ""Key"" : ""Needlestick/Sharp"", 
                    ""Value"" : ""Needlestick/Sharp""
                }, 
                {
                    ""Key"" : ""Nonroadway Motor Veh Incid"", 
                    ""Value"" : ""Nonroadway Motor Veh Incid""
                }, 
                {
                    ""Key"" : ""Other/Unknown"", 
                    ""Value"" : ""Other/Unknown""
                }, 
                {
                    ""Key"" : ""Overexertion  -patient"", 
                    ""Value"" : ""Overexertion  -patient""
                }, 
                {
                    ""Key"" : ""Overexertion - object/equip"", 
                    ""Value"" : ""Overexertion - object/equip""
                }, 
                {
                    ""Key"" : ""Overexertion - other"", 
                    ""Value"" : ""Overexertion - other""
                }, 
                {
                    ""Key"" : ""Overexertion - patient"", 
                    ""Value"" : ""Overexertion - patient""
                }, 
                {
                    ""Key"" : ""Overexertion-object/equip"", 
                    ""Value"" : ""Overexertion-object/equip""
                }, 
                {
                    ""Key"" : ""Overexertion-patient"", 
                    ""Value"" : ""Overexertion-patient""
                }, 
                {
                    ""Key"" : ""Pedestrian-Vehicle Incident"", 
                    ""Value"" : ""Pedestrian-Vehicle Incident""
                }, 
                {
                    ""Key"" : ""Physical contact"", 
                    ""Value"" : ""Physical contact""
                }, 
                {
                    ""Key"" : ""Repetitive motion"", 
                    ""Value"" : ""Repetitive motion""
                }, 
                {
                    ""Key"" : ""Roadway Motor Veh Incident"", 
                    ""Value"" : ""Roadway Motor Veh Incident""
                }, 
                {
                    ""Key"" : ""Slip/Trip"", 
                    ""Value"" : ""Slip/Trip""
                }, 
                {
                    ""Key"" : ""Struck against object/equip"", 
                    ""Value"" : ""Struck against object/equip""
                }, 
                {
                    ""Key"" : ""Struck by object/equip"", 
                    ""Value"" : ""Struck by object/equip""
                }, 
                {
                    ""Key"" : ""Struck by powered vehicle"", 
                    ""Value"" : ""Struck by powered vehicle""
                }, 
                {
                    ""Key"" : ""Traumatic/stressful event"", 
                    ""Value"" : ""Traumatic/stressful event""
                }, 
                {
                    ""Key"" : ""Violence by person"", 
                    ""Value"" : ""Violence by person""
                }
            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : null, 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""59a1e1e2f9eb5552285969d3""), 
            ""cdt"" : ISODate(""2017-05-23T00:00:00.000Z""), 
            ""cby"" : ObjectId(""000000000000000000000000""), 
            ""mdt"" : ISODate(""2017-08-26T00:00:00.000Z""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""UNSAFEACT"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""Unsafe Act"", 
            ""Description"" : null, 
            ""Label"" : ""Unsafe Act"", 
            ""HelpText"" : ""Unsafe Act"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [
                {
                    ""Key"" : ""did not follow procedure"",
                    ""Value"" : ""did not follow procedure""
                }, 
                {
                    ""Key"" : ""horseplay"", 
                    ""Value"" : ""horseplay""
                }, 
                {
                    ""Key"" : ""lapse of attention"", 
                    ""Value"" : ""lapse of attention""
                }, 
                {
                    ""Key"" : ""lack of training/experience"", 
                    ""Value"" : ""lack of training/experience""
                }
            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(25), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }, 
        {
            ""_id"" : ObjectId(""59a1e28cf9eb5552285969d5""), 
            ""cdt"" : ISODate(""2017-08-26T00:00:00.000Z""), 
            ""cby"" : ObjectId(""000000000000000000000000""), 
            ""mdt"" : ISODate(""2017-08-26T00:00:00.000Z""), 
            ""mby"" : ObjectId(""000000000000000000000000""), 
            ""IsDeleted"" : false, 
            ""CustomerId"" : ObjectId(""58b9d693c5f3332184eaf4ce""), 
            ""EmployerId"" : ObjectId(""58cb0b4bc5f33320084ff892""), 
            ""Code"" : ""INJOCCURED"", 
            ""Suppressed"" : false, 
            ""Target"" : NumberInt(2), 
            ""Name"" : ""Injury Occurred"", 
            ""Description"" : null, 
            ""Label"" : ""Injury Occurred"", 
            ""HelpText"" : ""Injury Occurred"", 
            ""DataType"" : NumberInt(1), 
            ""ValueType"" : NumberInt(1), 
            ""ListValues"" : [
                {
                    ""Key"" : ""environmental condition"",
                    ""Value"" : ""environmental condition""
                }, 
                {
                    ""Key"" : ""equipment failure"", 
                    ""Value"" : ""equipment failure""
                }, 
                {
                    ""Key"" : ""human error"", 
                    ""Value"" : ""human error""
                }, 
                {
                    ""Key"" : ""insufficient training"", 
                    ""Value"" : ""insufficient training""
                }, 
                {
                    ""Key"" : ""overexertion"", 
                    ""Value"" : ""overexertion""
                }, 
                {
                    ""Key"" : ""poor design"", 
                    ""Value"" : ""poor design""
                }, 
                {
                    ""Key"" : ""unguarded equipment"", 
                    ""Value"" : ""unguarded equipment""
                }, 
                {
                    ""Key"" : ""unsafe act/condition"", 
                    ""Value"" : ""unsafe act/condition""
                }
            ], 
            ""SelectedValue"" : null, 
            ""FileOrder"" : NumberInt(25), 
            ""IsRequired"" : false, 
            ""IsCollectedAtIntake"" : false
        }
    ], 
    ""RiskProfile"" : null, 
    ""CurrentOfficeLocation"" : ""MICH 100-100 MICHIGAN-BUTTERWORTH"", 
    ""AssignedToId"" : ObjectId(""58da8bdec5f330216031f956"")
}");

            #endregion
            using (var userService = new AuthenticationService())
            using (var customerService = new CustomerService())
            using (var employerService = new EmployerService())
            using (var employeeService = new EmployeeService())
            using (var eligService = new EligibilityService())
            using (var caseService = new CaseService(false))
            {
                #region Create user

                // Create user
                var user = userService.CreateUser(
                    Guid.NewGuid() + "@absencesoft.io",
                    "I'm not telling",
                    "Chad",
                    "Scharf",
                    Role.SystemAdministrator.Id);
                user.UserFlags = UserFlag.CreateCustomer | UserFlag.UpdateEmployer | UserFlag.ProductTour | UserFlag.FirstEmployee;
                userService.UpdateUser(user);

                userService.CurrentUser = user;
                customerService.CurrentUser = user;
                employerService.CurrentUser = user;
                employeeService.CurrentUser = user;
                caseService.CurrentUser = user;
                eligService.CurrentUser = user;

                #endregion Create user

                #region Create customer

                // Create customer
                var customer = customerService.CreateCustomer("AT-7128", "at7128" + Guid.NewGuid().ToString(), "111-222-3333", "111-222-3333", new Address()
                {
                    Address1 = "123 Any Street",
                    City = "Somewhere",
                    State = "MI",
                    Country = "US",
                    PostalCode = "42593"
                }, user);
                customer.EnableAllEmployersByDefault = true;
                customerService.Update(customer);

                userService.CurrentCustomer = customer;
                customerService.CurrentCustomer = customer;
                employerService.CurrentCustomer = customer;
                employeeService.CurrentCustomer = customer;
                caseService.CurrentCustomer = customer;
                eligService.CurrentCustomer = customer;

                // Update user
                user.Roles = new List<string>() { Role.SystemAdministrator.Id };
                user.CustomerId = customer.Id;
                user.Done(UserFlag.CreateCustomer);
                userService.UpdateUser(user);

                #endregion Create customer

                #region Create employer

                // Create employer
                var employer = employerService.CreateEmployer(customer);
                employer.Holidays = new List<Holiday>(0);
                employer.Name = "AT-7128";
                employer.Url = "ess-" + customer.Url;
                employer.MaxEmployees = 1;
                employer.ReferenceCode = null;
                employer.AllowIntermittentForBirthAdoptionOrFosterCare = true;
                employer.IgnoreScheduleForPriorHoursWorked = true;
                employer.IgnoreAverageMinutesWorkedPerWeek = true;
                employer.Enable50In75MileRule = false;
                employer.EnableSpouseAtSameEmployerRule = false;
                employer.FMLPeriodType = PeriodType.RollingBack;
                employer.FTWeeklyWorkHours = 40;
                employer.IsPubliclyTradedCompany = false;
                employer.ModifiedById = user.Id;
                employer.SuppressPolicies = Policy.AsQueryable().Where(p => p.CustomerId == null && p.Code != "FMLA").Select(p => p.Code).ToList();
                employer.SuppressWorkflows = Workflow.AsQueryable().Where(p => p.CustomerId == null).Select(w => w.Code).ToList();
                employerService.Update(employer);

                userService.CurrentEmployer = employer;
                customerService.CurrentEmployer = employer;
                employerService.CurrentEmployer = employer;
                employeeService.CurrentEmployer = employer;
                caseService.CurrentEmployer = employer;
                eligService.CurrentEmployer = employer;

                // Set the user's employer id
                user.Employers.Add(new EmployerAccess()
                {
                    Employer = employer,
                    AutoAssignCases = true
                });
                user.Done(UserFlag.UpdateEmployer);
                userService.UpdateUser(user);

                #endregion Create employer

                #region Override FMLA policy

                // Override FMLA policy
                var fmla = Policy.GetByCode("FMLA");
                fmla.Clean();
                fmla.CustomerId = customer.Id;
                fmla.EmployerId = employer.Id;
                fmla.RuleGroups.Clear();
                fmla.AbsenceReasons.ForEach(r => r.RuleGroups.Clear());
                fmla.Save();

                #endregion Override FMLA policy

                #region Employee JSON
                #endregion

                #region Create employee

                // Create employee
                var employee = employeeService.Update(new Employee()
                {
                    CustomerId = customer.Id,
                    EmployerId = employer.Id,
                    EmployerName = employer.Name,
                    EmployeeNumber = "AT-7128-TEST",
                    FirstName = "AT-7128",
                    LastName = "Person",
                    Gender = Gender.Female,
                    HireDate = "05/11/2012".ToDate(),
                    ServiceDate = "05/11/2012".ToDate(),
                    Meets50In75MileRule = false,
                    IsKeyEmployee = false,
                    IsExempt = false,
                    Status = EmploymentStatus.Active,
                    MilitaryStatus = MilitaryStatus.Civilian,
                    WorkCountry = "US",
                    WorkState = "MI",
                    EmployeeClassCode = WorkType.FullTime
                },
                new Schedule()
                {
                    ScheduleType = ScheduleType.Weekly,
                    StartDate = "10/19/2017".ToDate(),
                    Times = new List<Time>(7)
                    {
                        new Time() { SampleDate = "10/15/2017".ToDate(), TotalMinutes = 0 },
                        new Time() { SampleDate = "10/16/2017".ToDate(), TotalMinutes = 720 },
                        new Time() { SampleDate = "10/17/2017".ToDate(), TotalMinutes = 0 },
                        new Time() { SampleDate = "10/18/2017".ToDate(), TotalMinutes = 720 },
                        new Time() { SampleDate = "10/19/2017".ToDate(), TotalMinutes = 720 },
                        new Time() { SampleDate = "10/20/2017".ToDate(), TotalMinutes = 0 },
                        new Time() { SampleDate = "10/21/2017".ToDate(), TotalMinutes = 0 }
                    }
                });

                #endregion Create employe

                Case testCase = null;
                testCase = BsonSerializer.Deserialize<Case>(caseBson);

                testCase.Employee = employee;
                testCase.CustomerId = customer.Id;
                testCase.EmployerId = employer.Id;
                testCase.Save();

                var employeeTimeTracker = caseService.GetEmployeePolicySummary(employee, "08/16/2018".ToDate(), fmla.Code);
                employeeTimeTracker.Should().HaveCount(1);
                employeeTimeTracker[0].PolicyCode.Should().BeEquivalentTo(fmla.Code);
                //employeeTimeTracker[0].TimeUsed.Should().Be(12);
                //employeeTimeTracker[0].TimeRemaining.Should().Be(0);

            }
        }
    }
}
