﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;

namespace AbsenceSoft.Tests.Services.Cases
{
    [TestClass]
    public class CaseAssignmentServiceTest
    {
        [TestMethod]
        public void CustomerGetsDefaultRoles()
        {
            AbsenceSoft.Data.Customers.Customer c = AbsenceSoft.Data.Customers.Customer.AsQueryable().First();
            User u = GetCaseAssignmentTestUser(c.Id, null);
            CaseAssignmentService svc = new CaseAssignmentService(u);
            svc.CreateDefaultCaseAssigneeTypesForCustomer(c.Id);

            List<CaseAssignmentType> types =  svc.GetCaseAssignmentTypes(c.Id);
            Assert.IsTrue(types.Count == 6, string.Format("Expected 6 case assignment types, found {0}", types.Count));
        }

        private User GetCaseAssignmentTestUser(string customerId, string employerId)
        {
            User user = new User()
            {
                CustomerId = customerId
            };
            

            if (user.Employers == null)
                user.Employers = new List<EmployerAccess>();
            if (!string.IsNullOrEmpty(employerId) && !user.Employers.Any(e => e.EmployerId == employerId))
                user.Employers.Add(new EmployerAccess()
                {
                    EmployerId = employerId,
                    AutoAssignCases = true
                });
            if (user.Employers.Count(e => e.EmployerId == employerId) > 1)
            {
                var first = user.Employers.First(e => e.EmployerId == employerId);
                user.Employers.RemoveAll(e => e.EmployerId == employerId && e.Id != first.Id);
            }

            return user;
        }

    }
}
