﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using System.Linq;

namespace AbsenceSoft.Tests.Services.Cases
{
    [TestClass]
    public class LeapYearCalcsTest
    {
        [TestMethod]
        public void TestLeapYearEntitlementCalcsForFMLA()
        {
            #region BSON

            BsonDocument caseBson = BsonDocument.Parse(@"{
    ""_id"": ObjectId(""55b7f6b7c5f33611101be322""),
    ""cdt"": ISODate(""2015-07-28T21:40:07.080Z""),
    ""cby"": ObjectId(""000000000000000000000002""),
    ""mdt"": ISODate(""2015-07-28T21:40:07.532Z""),
    ""mby"": ObjectId(""000000000000000000000002""),
    ""CustomerId"": ObjectId(""000000000000000000000002""),
    ""EmployerId"": ObjectId(""000000000000000000000002""),
    ""CaseNumber"": ""747695411"",
    ""EmployerCaseNumber"": null,
    ""StartDate"": ISODate(""2015-07-01T00:00:00.000Z""),
    ""EndDate"": ISODate(""2016-07-01T00:00:00.000Z""),
    ""Segments"": [
        {
            ""_id"": ""ebc7f24c-469b-4319-a9db-d1d583022e65"",
            ""StartDate"": ISODate(""2015-07-01T00:00:00.000Z""),
            ""EndDate"": ISODate(""2016-07-01T00:00:00.000Z""),
            ""Type"": 1,
            ""AppliedPolicies"": [
                {
                    ""_id"": ""cf40eb05-0af6-4639-b8c6-e08901b0b057"",
                    ""Policy"": {
                        ""_id"": ObjectId(""552d8385315fd80f98341d70""),
                        ""cdt"": ISODate(""2015-04-14T21:15:49.988Z""),
                        ""cby"": ObjectId(""000000000000000000000000""),
                        ""mdt"": ISODate(""2015-07-28T20:11:15.836Z""),
                        ""mby"": ObjectId(""000000000000000000000000""),
                        ""CustomerId"": null,
                        ""EmployerId"": null,
                        ""Code"": ""FMLA"",
                        ""Name"": ""Family Medical Leave Act"",
                        ""Description"": null,
                        ""EffectiveDate"": ISODate(""1970-01-01T00:00:00.000Z""),
                        ""PolicyType"": 0,
                        ""RuleGroups"": [
                            {
                                ""_id"": ""d2258fd2-85cb-496a-ac43-07164c26db06"",
                                ""Name"": ""FMLA Eligibility"",
                                ""Description"": ""All of the following statements about the Employee must be true:"",
                                ""Rules"": [
                                    {
                                        ""_id"": ""09c865a6-652a-4ec6-8e70-cd0ebf47aba6"",
                                        ""Name"": ""1,250 Hours Worked"",
                                        ""Description"": ""The Employee has worked at least 1,250 hours in the last 12 months"",
                                        ""LeftExpression"": ""TotalHoursWorkedLast12Months()"",
                                        ""Operator"": "">="",
                                        ""RightExpression"": ""1250""
                                    },
                                    {
                                        ""_id"": ""70917e1d-0108-4d92-9ffd-8ced52042e88"",
                                        ""Name"": ""50 Employees in 75 Mile Radius Rule"",
                                        ""Description"": ""The Employee works in an office with at least 50 employees that all work within a 75 mile radius"",
                                        ""LeftExpression"": ""Meets50In75MileRule()"",
                                        ""Operator"": ""=="",
                                        ""RightExpression"": ""true""
                                    },
                                    {
                                        ""_id"": ""841892ba-da18-4409-a73b-96b1199462e6"",
                                        ""Name"": ""Worked 12 Months"",
                                        ""Description"": ""The Employee has worked at least 12 months"",
                                        ""LeftExpression"": ""Has1YearOfService()"",
                                        ""Operator"": ""=="",
                                        ""RightExpression"": ""true""
                                    },
                                    {
                                        ""_id"": ""22cf0342-690b-4936-9715-c58346fd3438"",
                                        ""Name"": ""Key Employees Ineligible"",
                                        ""Description"": ""Employee may not be a key employee"",
                                        ""LeftExpression"": ""Employee.IsKeyEmployee"",
                                        ""Operator"": ""=="",
                                        ""RightExpression"": ""false""
                                    }
                                ],
                                ""SuccessType"": 0,
                                ""RuleGroupType"": 1,
                                ""Entitlement"": null
                            }
                        ],
                        ""AbsenceReasons"": [
                            {
                                ""_id"": ""588bad52-f35b-497a-9951-1fa6355c77ca"",
                                ""ReasonCode"": ""EHC"",
                                ""EntitlementType"": 1,
                                ""Entitlement"": 12.0000000000000000,
                                ""EliminationType"": 8,
                                ""Elimination"": 3.0000000000000000,
                                ""CombinedForSpouses"": false,
                                ""PeriodType"": 5,
                                ""Period"": 12.0000000000000000,
                                ""EffectiveDate"": ISODate(""1970-01-01T00:00:00.000Z""),
                                ""CaseTypes"": 7,
                                ""Paid"": false,
                                ""PaymentTiers"": [ ],
                                ""RuleGroups"": [
                                    {
                                        ""_id"": ""af7d189e-5d29-448c-bef1-777839d4f9fe"",
                                        ""Name"": ""Relationship to Employee"",
                                        ""Description"": ""One of the following statements about \""Relationship to Employee\"" must be true:"",
                                        ""Rules"": [
                                            {
                                                ""_id"": ""1d471c27-264e-43f3-a96d-68013d870a92"",
                                                ""Name"": ""SELF"",
                                                ""Description"": ""is for the employee's SELF"",
                                                ""LeftExpression"": ""CaseRelationshipType"",
                                                ""Operator"": ""=="",
                                                ""RightExpression"": ""'SELF'""
                                            }
                                        ],
                                        ""SuccessType"": 1,
                                        ""RuleGroupType"": 0,
                                        ""Entitlement"": null
                                    }
                                ],
                                ""ResidenceState"": null,
                                ""ShowType"": 0,
                                ""PolicyEvents"": [ ],
                                ""UseWholeDollarAmountsOnly"": false
                            },
                            {
                                ""_id"": ""df664076-9143-4b1d-a3bd-1768ffbc410b"",
                                ""ReasonCode"": ""FHC"",
                                ""EntitlementType"": 1,
                                ""Entitlement"": 12.0000000000000000,
                                ""EliminationType"": 8,
                                ""Elimination"": 3.0000000000000000,
                                ""CombinedForSpouses"": true,
                                ""PeriodType"": 5,
                                ""Period"": 12.0000000000000000,
                                ""EffectiveDate"": ISODate(""1970-01-01T00:00:00.000Z""),
                                ""CaseTypes"": 7,
                                ""Paid"": false,
                                ""PaymentTiers"": [ ],
                                ""RuleGroups"": [
                                    {
                                        ""_id"": ""f607eafc-e8a8-484f-8a05-0daf925ec8ef"",
                                        ""Name"": ""Relationship to Employee"",
                                        ""Description"": ""One of the following statements about \""Relationship to Employee\"" must be true:"",
                                        ""Rules"": [
                                            {
                                                ""_id"": ""6e6c8b83-55a8-45e3-a44d-afdba3f47f02"",
                                                ""Name"": ""SPOUSE"",
                                                ""Description"": ""is for the employee's SPOUSE"",
                                                ""LeftExpression"": ""CaseRelationshipType"",
                                                ""Operator"": ""=="",
                                                ""RightExpression"": ""'SPOUSE'""
                                            },
                                            {
                                                ""_id"": ""e6ddfd56-a0f1-4d75-91a6-f4c45652f70d"",
                                                ""Name"": ""PARENT"",
                                                ""Description"": ""is for the employee's PARENT"",
                                                ""LeftExpression"": ""CaseRelationshipType"",
                                                ""Operator"": ""=="",
                                                ""RightExpression"": ""'PARENT'""
                                            },
                                            {
                                                ""_id"": ""2e20a577-d980-4d8b-8f2d-efd80234aef2"",
                                                ""Name"": ""STEPPARENT"",
                                                ""Description"": ""is for the employee's STEPPARENT"",
                                                ""LeftExpression"": ""CaseRelationshipType"",
                                                ""Operator"": ""=="",
                                                ""RightExpression"": ""'STEPPARENT'""
                                            },
                                            {
                                                ""_id"": ""0cbbefed-6bcc-4d06-b6a6-74ad51e6e40b"",
                                                ""Name"": ""FOSTERPARENT"",
                                                ""Description"": ""is for the employee's FOSTERPARENT"",
                                                ""LeftExpression"": ""CaseRelationshipType"",
                                                ""Operator"": ""=="",
                                                ""RightExpression"": ""'FOSTERPARENT'""
                                            },
                                            {
                                                ""_id"": ""14dbbaf0-312c-4420-81b1-789a729d8a91"",
                                                ""Name"": ""CHILD"",
                                                ""Description"": ""is for the employee's CHILD"",
                                                ""LeftExpression"": ""CaseRelationshipType"",
                                                ""Operator"": ""=="",
                                                ""RightExpression"": ""'CHILD'""
                                            },
                                            {
                                                ""_id"": ""77ab01ff-8375-4786-bd66-130250a37bc4"",
                                                ""Name"": ""STEPCHILD"",
                                                ""Description"": ""is for the employee's STEPCHILD"",
                                                ""LeftExpression"": ""CaseRelationshipType"",
                                                ""Operator"": ""=="",
                                                ""RightExpression"": ""'STEPCHILD'""
                                            },
                                            {
                                                ""_id"": ""8b678416-16a1-4e4f-91b3-c68c63cd090f"",
                                                ""Name"": ""FOSTERCHILD"",
                                                ""Description"": ""is for the employee's FOSTERCHILD"",
                                                ""LeftExpression"": ""CaseRelationshipType"",
                                                ""Operator"": ""=="",
                                                ""RightExpression"": ""'FOSTERCHILD'""
                                            },
                                            {
                                                ""_id"": ""427671b4-13e0-4efb-8051-023de5fcf9d4"",
                                                ""Name"": ""ADOPTEDCHILD"",
                                                ""Description"": ""is for the employee's ADOPTEDCHILD"",
                                                ""LeftExpression"": ""CaseRelationshipType"",
                                                ""Operator"": ""=="",
                                                ""RightExpression"": ""'ADOPTEDCHILD'""
                                            },
                                            {
                                                ""_id"": ""aee57be2-69c9-4012-ab94-43492c0aab59"",
                                                ""Name"": ""LEGALWARD"",
                                                ""Description"": ""is for the employee's LEGALWARD"",
                                                ""LeftExpression"": ""CaseRelationshipType"",
                                                ""Operator"": ""=="",
                                                ""RightExpression"": ""'LEGALWARD'""
                                            },
                                            {
                                                ""_id"": ""d108b079-999a-49e6-b525-198299cd288c"",
                                                ""Name"": ""SAMESEXSPOUSE"",
                                                ""Description"": ""is for the employee's SAMESEXSPOUSE"",
                                                ""LeftExpression"": ""CaseRelationshipType"",
                                                ""Operator"": ""=="",
                                                ""RightExpression"": ""'SAMESEXSPOUSE'""
                                            }
                                        ],
                                        ""SuccessType"": 1,
                                        ""RuleGroupType"": 0,
                                        ""Entitlement"": null
                                    },
                                    {
                                        ""_id"": ""42e3a29e-e859-411b-9b43-d019ac48d9f2"",
                                        ""Name"": ""Relationship to Employee Outside of 'TX'"",
                                        ""Description"": ""One of the following statements about \""Relationship to Employee\"" OR Work State must be false:"",
                                        ""Rules"": [
                                            {
                                                ""_id"": ""2eb6dcbb-95a4-477d-94e3-1f4328785119"",
                                                ""Name"": ""Work State is TX"",
                                                ""Description"": ""Where the employee's work state is TX"",
                                                ""LeftExpression"": ""Employee.WorkState"",
                                                ""Operator"": ""=="",
                                                ""RightExpression"": ""'TX'""
                                            },
                                            {
                                                ""_id"": ""b42490ef-5460-49c9-852f-a6d5313f3dff"",
                                                ""Name"": ""SAMESEXSPOUSE"",
                                                ""Description"": ""is for the employee's SAMESEXSPOUSE"",
                                                ""LeftExpression"": ""CaseRelationshipType"",
                                                ""Operator"": ""=="",
                                                ""RightExpression"": ""'SAMESEXSPOUSE'""
                                            }
                                        ],
                                        ""SuccessType"": 3,
                                        ""RuleGroupType"": 0,
                                        ""Entitlement"": null
                                    },
                                    {
                                        ""_id"": ""a73c1f13-826f-4154-adcf-df3aed57982b"",
                                        ""Name"": ""Relationship to Employee Outside of 'LA'"",
                                        ""Description"": ""One of the following statements about \""Relationship to Employee\"" OR Work State must be false:"",
                                        ""Rules"": [
                                            {
                                                ""_id"": ""6265924c-324c-4694-afcb-5028d0297a3b"",
                                                ""Name"": ""Work State is LA"",
                                                ""Description"": ""Where the employee's work state is LA"",
                                                ""LeftExpression"": ""Employee.WorkState"",
                                                ""Operator"": ""=="",
                                                ""RightExpression"": ""'LA'""
                                            },
                                            {
                                                ""_id"": ""e983093e-950f-4813-9d57-258ecc845cba"",
                                                ""Name"": ""SAMESEXSPOUSE"",
                                                ""Description"": ""is for the employee's SAMESEXSPOUSE"",
                                                ""LeftExpression"": ""CaseRelationshipType"",
                                                ""Operator"": ""=="",
                                                ""RightExpression"": ""'SAMESEXSPOUSE'""
                                            }
                                        ],
                                        ""SuccessType"": 3,
                                        ""RuleGroupType"": 0,
                                        ""Entitlement"": null
                                    },
                                    {
                                        ""_id"": ""f38b299d-0bb6-4878-856c-90cdf11c1587"",
                                        ""Name"": ""Relationship to Employee Outside of 'MS'"",
                                        ""Description"": ""One of the following statements about \""Relationship to Employee\"" OR Work State must be false:"",
                                        ""Rules"": [
                                            {
                                                ""_id"": ""2d5f6d75-017c-46ec-86c9-5db5a3418f8e"",
                                                ""Name"": ""Work State is MS"",
                                                ""Description"": ""Where the employee's work state is MS"",
                                                ""LeftExpression"": ""Employee.WorkState"",
                                                ""Operator"": ""=="",
                                                ""RightExpression"": ""'MS'""
                                            },
                                            {
                                                ""_id"": ""cd24a6b1-4aff-4842-adc4-c318ceb551c2"",
                                                ""Name"": ""SAMESEXSPOUSE"",
                                                ""Description"": ""is for the employee's SAMESEXSPOUSE"",
                                                ""LeftExpression"": ""CaseRelationshipType"",
                                                ""Operator"": ""=="",
                                                ""RightExpression"": ""'SAMESEXSPOUSE'""
                                            }
                                        ],
                                        ""SuccessType"": 3,
                                        ""RuleGroupType"": 0,
                                        ""Entitlement"": null
                                    }
                                ],
                                ""ResidenceState"": null,
                                ""ShowType"": 0,
                                ""PolicyEvents"": [ ],
                                ""UseWholeDollarAmountsOnly"": false
                            },
                            {
                                ""_id"": ""626beb80-8c73-4a43-8713-cf63b63da262"",
                                ""ReasonCode"": ""PREGMAT"",
                                ""EntitlementType"": 1,
                                ""Entitlement"": 12.0000000000000000,
                                ""EliminationType"": 8,
                                ""Elimination"": 3.0000000000000000,
                                ""CombinedForSpouses"": false,
                                ""PeriodType"": 5,
                                ""Period"": 12.0000000000000000,
                                ""EffectiveDate"": ISODate(""1970-01-01T00:00:00.000Z""),
                                ""CaseTypes"": 7,
                                ""Paid"": false,
                                ""PaymentTiers"": [ ],
                                ""RuleGroups"": [ ],
                                ""ResidenceState"": null,
                                ""ShowType"": 0,
                                ""PolicyEvents"": [ ],
                                ""UseWholeDollarAmountsOnly"": false
                            },
                            {
                                ""_id"": ""5ff8a2b8-2b2c-42b6-9700-65b344f17a38"",
                                ""ReasonCode"": ""ADOPT"",
                                ""EntitlementType"": 1,
                                ""Entitlement"": 12.0000000000000000,
                                ""EliminationType"": 8,
                                ""Elimination"": 3.0000000000000000,
                                ""CombinedForSpouses"": false,
                                ""PeriodType"": 5,
                                ""Period"": 12.0000000000000000,
                                ""EffectiveDate"": ISODate(""1970-01-01T00:00:00.000Z""),
                                ""CaseTypes"": 7,
                                ""Paid"": false,
                                ""PaymentTiers"": [ ],
                                ""RuleGroups"": [ ],
                                ""ResidenceState"": null,
                                ""ShowType"": 0,
                                ""PolicyEvents"": [ ],
                                ""UseWholeDollarAmountsOnly"": false
                            },
                            {
                                ""_id"": ""00cdaf8d-da91-496f-9997-1b2589d575c4"",
                                ""ReasonCode"": ""MILITARY"",
                                ""EntitlementType"": 1,
                                ""Entitlement"": 26.0000000000000000,
                                ""EliminationType"": 8,
                                ""Elimination"": 3.0000000000000000,
                                ""CombinedForSpouses"": true,
                                ""PeriodType"": 5,
                                ""Period"": 12.0000000000000000,
                                ""EffectiveDate"": ISODate(""1970-01-01T00:00:00.000Z""),
                                ""CaseTypes"": 7,
                                ""Paid"": false,
                                ""PaymentTiers"": [ ],
                                ""RuleGroups"": [
                                    {
                                        ""_id"": ""beb181ff-8fe8-405a-a6c5-ef223bff2c8e"",
                                        ""Name"": ""Service Member's Relationship to Employee"",
                                        ""Description"": ""One of the following statements about \""Service Member's Relationship to Employee\"" must be true:"",
                                        ""Rules"": [
                                            {
                                                ""_id"": ""7ef8428b-d355-4b17-9bcb-aa134f2c7767"",
                                                ""Name"": ""Not SELF"",
                                                ""Description"": ""is not for the employee's SELF"",
                                                ""LeftExpression"": ""CaseRelationshipType"",
                                                ""Operator"": ""=="",
                                                ""RightExpression"": ""'SELF'""
                                            }
                                        ],
                                        ""SuccessType"": 2,
                                        ""RuleGroupType"": 0,
                                        ""Entitlement"": null
                                    },
                                    {
                                        ""_id"": ""18e483a6-c2d4-4fc0-87f3-3cc9f0b96efb"",
                                        ""Name"": ""Service Member's Military Status"",
                                        ""Description"": ""One of the following statements about \""Service Member's Military Status\"" must be true:"",
                                        ""Rules"": [
                                            {
                                                ""_id"": ""1bff552f-eff3-44ee-b9a2-e5399675aae7"",
                                                ""Name"": ""Active Duty"",
                                                ""Description"": ""Service member's military status is Active Duty"",
                                                ""LeftExpression"": ""Case.Contact.MilitaryStatus"",
                                                ""Operator"": ""=="",
                                                ""RightExpression"": ""MilitaryStatus.ActiveDuty""
                                            },
                                            {
                                                ""_id"": ""c04d7ebd-e724-4164-8399-bc1c3439547e"",
                                                ""Name"": ""Veteran"",
                                                ""Description"": ""Service member's military status is Veteran"",
                                                ""LeftExpression"": ""Case.Contact.MilitaryStatus"",
                                                ""Operator"": ""=="",
                                                ""RightExpression"": ""MilitaryStatus.Veteran""
                                            }
                                        ],
                                        ""SuccessType"": 1,
                                        ""RuleGroupType"": 1,
                                        ""Entitlement"": null
                                    }
                                ],
                                ""ResidenceState"": null,
                                ""ShowType"": 0,
                                ""PolicyEvents"": [ ],
                                ""UseWholeDollarAmountsOnly"": false
                            },
                            {
                                ""_id"": ""627b6cdd-1413-4686-9377-6638125069d3"",
                                ""ReasonCode"": ""EXIGENCY"",
                                ""EntitlementType"": 1,
                                ""Entitlement"": 12.0000000000000000,
                                ""EliminationType"": 8,
                                ""Elimination"": 3.0000000000000000,
                                ""PerUseCap"": 15.0000000000000000,
                                ""PerUseCapType"": 8,
                                ""CombinedForSpouses"": false,
                                ""PeriodType"": 5,
                                ""Period"": 12.0000000000000000,
                                ""EffectiveDate"": ISODate(""1970-01-01T00:00:00.000Z""),
                                ""CaseTypes"": 7,
                                ""Paid"": false,
                                ""PaymentTiers"": [ ],
                                ""RuleGroups"": [
                                    {
                                        ""_id"": ""eea44290-b2ea-455b-a084-4692e5c8a8ec"",
                                        ""Name"": ""Service Member's Relationship to Employee"",
                                        ""Description"": ""One of the following statements about \""Service Member's Relationship to Employee\"" must be true:"",
                                        ""Rules"": [
                                            {
                                                ""_id"": ""eb81d789-5341-4c17-869b-7fa07dd6dbe9"",
                                                ""Name"": ""Not SELF"",
                                                ""Description"": ""is not for the employee's SELF"",
                                                ""LeftExpression"": ""CaseRelationshipType"",
                                                ""Operator"": ""=="",
                                                ""RightExpression"": ""'SELF'""
                                            },
                                            {
                                                ""_id"": ""1bd32c3e-4593-44ee-93de-5702f465310b"",
                                                ""Name"": ""Not NEXTOFKIN"",
                                                ""Description"": ""is not for the employee's NEXTOFKIN"",
                                                ""LeftExpression"": ""CaseRelationshipType"",
                                                ""Operator"": ""=="",
                                                ""RightExpression"": ""'NEXTOFKIN'""
                                            }
                                        ],
                                        ""SuccessType"": 2,
                                        ""RuleGroupType"": 0,
                                        ""Entitlement"": null
                                    },
                                    {
                                        ""_id"": ""daadab33-9345-44f2-bb09-d31b1ef55ae5"",
                                        ""Name"": ""Service Member's Military Status"",
                                        ""Description"": ""One of the following statements about \""Service Member's Military Status\"" must be true:"",
                                        ""Rules"": [
                                            {
                                                ""_id"": ""5f18502c-c289-4d32-9921-fb8b5b4c673f"",
                                                ""Name"": ""Active Duty"",
                                                ""Description"": ""Service member's military status is Active Duty"",
                                                ""LeftExpression"": ""Case.Contact.MilitaryStatus"",
                                                ""Operator"": ""=="",
                                                ""RightExpression"": ""MilitaryStatus.ActiveDuty""
                                            }
                                        ],
                                        ""SuccessType"": 1,
                                        ""RuleGroupType"": 1,
                                        ""Entitlement"": null
                                    }
                                ],
                                ""ResidenceState"": null,
                                ""ShowType"": 0,
                                ""PolicyEvents"": [ ],
                                ""UseWholeDollarAmountsOnly"": false
                            }
                        ],
                        ""WorkState"": null,
                        ""ResidenceState"": null,
                        ""Gender"": null,
                        ""ConsecutiveTo"": [ ]
                    },
                    ""PolicyReason"": {
                        ""_id"": ""588bad52-f35b-497a-9951-1fa6355c77ca"",
                        ""ReasonCode"": ""EHC"",
                        ""EntitlementType"": 1,
                        ""Entitlement"": 12.0000000000000000,
                        ""EliminationType"": 8,
                        ""Elimination"": 3.0000000000000000,
                        ""CombinedForSpouses"": false,
                        ""PeriodType"": 5,
                        ""Period"": 12.0000000000000000,
                        ""EffectiveDate"": ISODate(""1970-01-01T00:00:00.000Z""),
                        ""CaseTypes"": 7,
                        ""Paid"": false,
                        ""PaymentTiers"": [ ],
                        ""RuleGroups"": [
                            {
                                ""_id"": ""af7d189e-5d29-448c-bef1-777839d4f9fe"",
                                ""Name"": ""Relationship to Employee"",
                                ""Description"": ""One of the following statements about \""Relationship to Employee\"" must be true:"",
                                ""Rules"": [
                                    {
                                        ""_id"": ""1d471c27-264e-43f3-a96d-68013d870a92"",
                                        ""Name"": ""SELF"",
                                        ""Description"": ""is for the employee's SELF"",
                                        ""LeftExpression"": ""CaseRelationshipType"",
                                        ""Operator"": ""=="",
                                        ""RightExpression"": ""'SELF'""
                                    }
                                ],
                                ""SuccessType"": 1,
                                ""RuleGroupType"": 0,
                                ""Entitlement"": null
                            }
                        ],
                        ""ResidenceState"": null,
                        ""ShowType"": 0,
                        ""PolicyEvents"": [ ],
                        ""UseWholeDollarAmountsOnly"": false
                    },
                    ""Status"": 1,
                    ""StartDate"": ISODate(""2015-07-01T00:00:00.000Z""),
                    ""EndDate"": ISODate(""2016-07-01T00:00:00.000Z""),
                    ""Usage"": [ ],
                    ""RuleGroups"": [
                        {
                            ""_id"": ""8def1601-a8af-49e6-8337-c895de4f5c2f"",
                            ""RuleGroup"": {
                                ""_id"": ""d2258fd2-85cb-496a-ac43-07164c26db06"",
                                ""Name"": ""FMLA Eligibility"",
                                ""Description"": ""All of the following statements about the Employee must be true:"",
                                ""Rules"": [
                                    {
                                        ""_id"": ""09c865a6-652a-4ec6-8e70-cd0ebf47aba6"",
                                        ""Name"": ""1,250 Hours Worked"",
                                        ""Description"": ""The Employee has worked at least 1,250 hours in the last 12 months"",
                                        ""LeftExpression"": ""TotalHoursWorkedLast12Months()"",
                                        ""Operator"": "">="",
                                        ""RightExpression"": ""1250""
                                    },
                                    {
                                        ""_id"": ""70917e1d-0108-4d92-9ffd-8ced52042e88"",
                                        ""Name"": ""50 Employees in 75 Mile Radius Rule"",
                                        ""Description"": ""The Employee works in an office with at least 50 employees that all work within a 75 mile radius"",
                                        ""LeftExpression"": ""Meets50In75MileRule()"",
                                        ""Operator"": ""=="",
                                        ""RightExpression"": ""true""
                                    },
                                    {
                                        ""_id"": ""841892ba-da18-4409-a73b-96b1199462e6"",
                                        ""Name"": ""Worked 12 Months"",
                                        ""Description"": ""The Employee has worked at least 12 months"",
                                        ""LeftExpression"": ""Has1YearOfService()"",
                                        ""Operator"": ""=="",
                                        ""RightExpression"": ""true""
                                    },
                                    {
                                        ""_id"": ""22cf0342-690b-4936-9715-c58346fd3438"",
                                        ""Name"": ""Key Employees Ineligible"",
                                        ""Description"": ""Employee may not be a key employee"",
                                        ""LeftExpression"": ""Employee.IsKeyEmployee"",
                                        ""Operator"": ""=="",
                                        ""RightExpression"": ""false""
                                    }
                                ],
                                ""SuccessType"": 0,
                                ""RuleGroupType"": 1,
                                ""Entitlement"": null
                            },
                            ""Rules"": [
                                {
                                    ""_id"": ""f196a315-959e-4241-81b7-8dcd4eee8a3a"",
                                    ""Rule"": {
                                        ""_id"": ""09c865a6-652a-4ec6-8e70-cd0ebf47aba6"",
                                        ""Name"": ""1,250 Hours Worked"",
                                        ""Description"": ""The Employee has worked at least 1,250 hours in the last 12 months"",
                                        ""LeftExpression"": ""TotalHoursWorkedLast12Months()"",
                                        ""Operator"": "">="",
                                        ""RightExpression"": ""1250""
                                    },
                                    ""Pass"": true,
                                    ""Fail"": false,
                                    ""Result"": 1,
                                    ""ActualValue"": 1686.6666666666667000,
                                    ""ActualValueString"": ""1686.66666666667"",
                                    ""Overridden"": false,
                                    ""OverrideFromResult"": 0,
                                    ""Messages"": [ ]
                                },
                                {
                                    ""_id"": ""be32836e-0c19-49eb-82c4-bb5199815340"",
                                    ""Rule"": {
                                        ""_id"": ""70917e1d-0108-4d92-9ffd-8ced52042e88"",
                                        ""Name"": ""50 Employees in 75 Mile Radius Rule"",
                                        ""Description"": ""The Employee works in an office with at least 50 employees that all work within a 75 mile radius"",
                                        ""LeftExpression"": ""Meets50In75MileRule()"",
                                        ""Operator"": ""=="",
                                        ""RightExpression"": ""true""
                                    },
                                    ""Pass"": true,
                                    ""Fail"": false,
                                    ""Result"": 1,
                                    ""ActualValue"": true,
                                    ""ActualValueString"": ""True"",
                                    ""Overridden"": false,
                                    ""OverrideFromResult"": 0,
                                    ""Messages"": [ ]
                                },
                                {
                                    ""_id"": ""d76aad35-6f04-4b0b-92fa-edeb0351f2d5"",
                                    ""Rule"": {
                                        ""_id"": ""841892ba-da18-4409-a73b-96b1199462e6"",
                                        ""Name"": ""Worked 12 Months"",
                                        ""Description"": ""The Employee has worked at least 12 months"",
                                        ""LeftExpression"": ""Has1YearOfService()"",
                                        ""Operator"": ""=="",
                                        ""RightExpression"": ""true""
                                    },
                                    ""Pass"": true,
                                    ""Fail"": false,
                                    ""Result"": 1,
                                    ""ActualValue"": true,
                                    ""ActualValueString"": ""True"",
                                    ""Overridden"": false,
                                    ""OverrideFromResult"": 0,
                                    ""Messages"": [ ]
                                },
                                {
                                    ""_id"": ""e5807110-cfcc-454d-87b2-dc31ce2fcbdf"",
                                    ""Rule"": {
                                        ""_id"": ""22cf0342-690b-4936-9715-c58346fd3438"",
                                        ""Name"": ""Key Employees Ineligible"",
                                        ""Description"": ""Employee may not be a key employee"",
                                        ""LeftExpression"": ""Employee.IsKeyEmployee"",
                                        ""Operator"": ""=="",
                                        ""RightExpression"": ""false""
                                    },
                                    ""Pass"": true,
                                    ""Fail"": false,
                                    ""Result"": 1,
                                    ""ActualValue"": false,
                                    ""ActualValueString"": ""False"",
                                    ""Overridden"": false,
                                    ""OverrideFromResult"": 0,
                                    ""Messages"": [ ]
                                }
                            ],
                            ""Pass"": true,
                            ""EvalDate"": ISODate(""2015-07-28T21:40:06.549Z"")
                        }
                    ],
                    ""Selection"": [
                        {
                            ""_id"": ""2bc0790c-8432-4af9-824f-293771cbead2"",
                            ""RuleGroup"": {
                                ""_id"": ""af7d189e-5d29-448c-bef1-777839d4f9fe"",
                                ""Name"": ""Relationship to Employee"",
                                ""Description"": ""One of the following statements about \""Relationship to Employee\"" must be true:"",
                                ""Rules"": [
                                    {
                                        ""_id"": ""1d471c27-264e-43f3-a96d-68013d870a92"",
                                        ""Name"": ""SELF"",
                                        ""Description"": ""is for the employee's SELF"",
                                        ""LeftExpression"": ""CaseRelationshipType"",
                                        ""Operator"": ""=="",
                                        ""RightExpression"": ""'SELF'""
                                    }
                                ],
                                ""SuccessType"": 1,
                                ""RuleGroupType"": 0,
                                ""Entitlement"": null
                            },
                            ""Rules"": [
                                {
                                    ""_id"": ""d4c74ca3-1f7e-4eff-80e7-9fea3145f71e"",
                                    ""Rule"": {
                                        ""_id"": ""1d471c27-264e-43f3-a96d-68013d870a92"",
                                        ""Name"": ""SELF"",
                                        ""Description"": ""is for the employee's SELF"",
                                        ""LeftExpression"": ""CaseRelationshipType"",
                                        ""Operator"": ""=="",
                                        ""RightExpression"": ""'SELF'""
                                    },
                                    ""Pass"": true,
                                    ""Fail"": false,
                                    ""Result"": 1,
                                    ""ActualValue"": ""SELF"",
                                    ""ActualValueString"": ""SELF"",
                                    ""Overridden"": false,
                                    ""OverrideFromResult"": 0,
                                    ""Messages"": [ ]
                                }
                            ],
                            ""Pass"": true,
                            ""EvalDate"": ISODate(""2015-07-28T21:40:06.050Z"")
                        }
                    ],
                    ""ManuallyAdded"": false
                }
            ],
            ""Status"": 0,
            ""LeaveSchedule"": [ ],
            ""Absences"": [ ],
            ""UserRequests"": [ ],
            ""CreatedDate"": ISODate(""2015-07-28T21:40:05.988Z"")
        }
    ],
    ""SpouseCaseId"" : null,
    ""CaseEvents"" : [ 
        {
            ""_id"" : ""18c8ab67-3475-4148-be84-366ea398726e"",
            ""EventType"" : 14,
            ""AllEventDates"" : [ 
                ISODate(""2016-07-03T00:00:00.000Z"")
            ]
        }, 
        {
            ""_id"" : ""c9082518-5f4b-4cbc-8db6-653b2a22ec0a"",
            ""EventType"" : 0,
            ""AllEventDates"" : [ 
                ISODate(""2015-07-28T00:00:00.000Z"")
            ]
        }
    ],
    ""Employee"" : {
        ""_id"" : ObjectId(""55b7f2f7c5f32e072caaa37a""),
        ""LastERow"" : ""OpOdKfK9J572N0+mMU39rw=="",
        ""cdt"" : ISODate(""2015-07-28T21:24:07.310Z""),
        ""cby"" : ObjectId(""000000000000000000000002""),
        ""mdt"" : ISODate(""2015-07-28T21:39:46.875Z""),
        ""mby"" : ObjectId(""000000000000000000000002""),
        ""CustomerId"" : ObjectId(""000000000000000000000002""),
        ""EmployerId"" : ObjectId(""000000000000000000000002""),
        ""EmployeeNumber"" : ""S900000002"",
        ""Info"" : {
            ""_id"" : ""99e41e69-33bc-425f-89ad-1ca9487de9e9"",
            ""HomeEmail"" : null,
            ""Email"" : ""markbacom@absencesoft.com"",
            ""AltEmail"" : ""mark@absencesoft.com"",
            ""Address"" : {
                ""_id"" : ""06ce7428-eb13-4fab-a4d9-2602708ae986"",
                ""Address1"" : ""124 Second St"",
                ""Address2"" : ""#326"",
                ""City"" : ""Brooklyn"",
                ""State"" : ""NY"",
                ""PostalCode"" : ""10002"",
                ""Country"" : ""US""
            },
            ""AltAddress"" : {
                ""_id"" : ""b860b866-bc42-4d65-81b8-e07f17db7851"",
                ""Address1"" : null,
                ""City"" : null,
                ""State"" : null,
                ""PostalCode"" : null,
                ""Country"" : ""US""
            },
            ""WorkPhone"" : ""2122222229"",
            ""HomePhone"" : ""2121111119"",
            ""CellPhone"" : ""2122222299"",
            ""AltPhone"" : ""2122222299"",
            ""OfficeLocation"" : ""NY107""
        },
        ""Job"" : {
            ""_id"" : ""3a26639b-0808-47b4-abef-81b43062f862"",
            ""JobFunctions"" : [],
            ""JobDescription"" : ""Office Clerical, Data Entry, Answer Phones, Lifting under 20 lbs."",
            ""Classification"" : 1,
            ""CensusJobCategoryCode"" : ""Census101"",
            ""SOCCode"" : ""SOC212""
        },
        ""FirstName"" : ""Harry"",
        ""MiddleName"" : ""Q"",
        ""LastName"" : ""0 to 5 Year"",
        ""Gender"" : 77,
        ""DoB"" : ISODate(""1982-02-02T00:00:00.000Z""),
        ""Ssn"" : null,
        ""Status"" : 65,
        ""Salary"" : 90000.0000000000000000,
        ""PayType"" : 1,
        ""WorkType"" : 2,
        ""JobTitle"" : ""Service Rep"",
        ""PriorHours"" : [ 
            {
                ""_id"" : ""4a1827e5-c6b2-44b2-861d-57296b505921"",
                ""HoursWorked"" : 1512.0000000000000000,
                ""AsOf"" : ISODate(""2011-11-11T00:00:00.000Z"")
            }
        ],
        ""ServiceDate"" : ISODate(""2012-03-03T00:00:00.000Z""),
        ""HireDate"" : ISODate(""2012-03-03T00:00:00.000Z""),
        ""WorkState"" : ""NY"",
        ""WorkCountry"" : ""USA"",
        ""WorkSchedules"" : [ 
            {
                ""_id"" : ""e4e4a93d-151d-49b4-bb07-5f24b003fe71"",
                ""ScheduleType"" : 0,
                ""StartDate"" : ISODate(""2011-11-11T00:00:00.000Z""),
                ""EndDate"" : null,
                ""Times"" : [ 
                    {
                        ""_id"" : ""17aac2c3-d8f9-49b1-9dcf-40d7c6c46448"",
                        ""TotalMinutes"" : 400,
                        ""SampleDate"" : ISODate(""2011-11-06T00:00:00.000Z"")
                    }, 
                    {
                        ""_id"" : ""39addd96-587e-4477-8f99-8a2a66e958de"",
                        ""TotalMinutes"" : 400,
                        ""SampleDate"" : ISODate(""2011-11-07T00:00:00.000Z"")
                    }, 
                    {
                        ""_id"" : ""935410a2-64be-4cdf-91b2-9b7379f63906"",
                        ""TotalMinutes"" : 400,
                        ""SampleDate"" : ISODate(""2011-11-08T00:00:00.000Z"")
                    }, 
                    {
                        ""_id"" : ""4b76b335-2e05-45af-9733-2ff3fba5312f"",
                        ""TotalMinutes"" : 400,
                        ""SampleDate"" : ISODate(""2011-11-09T00:00:00.000Z"")
                    }, 
                    {
                        ""_id"" : ""6e0d70d2-f96f-4b38-9582-e5d7cd202570"",
                        ""TotalMinutes"" : 400,
                        ""SampleDate"" : ISODate(""2011-11-10T00:00:00.000Z"")
                    }, 
                    {
                        ""_id"" : ""0f24f463-600a-48a2-8897-e60a821a70bf"",
                        ""TotalMinutes"" : 0,
                        ""SampleDate"" : ISODate(""2011-11-11T00:00:00.000Z"")
                    }, 
                    {
                        ""_id"" : ""7367c95b-0e82-4457-8da6-3623d9165502"",
                        ""TotalMinutes"" : 0,
                        ""SampleDate"" : ISODate(""2011-11-12T00:00:00.000Z"")
                    }
                ]
            }
        ],
        ""Meets50In75MileRule"" : true,
        ""IsKeyEmployee"" : false,
        ""IsExempt"" : false,
        ""MilitaryStatus"" : 0,
        ""CustomFields"" : [],
        ""EmployerName"" : ""Signature One""
    },
    ""Reason"" : {
        ""_id"" : ObjectId(""000000000000000001000000""),
        ""cdt"" : ISODate(""2015-04-14T21:15:49.049Z""),
        ""cby"" : ObjectId(""000000000000000000000000""),
        ""mdt"" : ISODate(""2015-07-28T20:11:14.775Z""),
        ""mby"" : ObjectId(""000000000000000000000000""),
        ""Code"" : ""EHC"",
        ""Name"" : ""Employee Health Condition"",
        ""Flags"" : null
    },
    ""Status"" : 0,
    ""AssignedToId"" : ObjectId(""000000000000000000000002""),
    ""AssignedToName"" : ""QA"",
    ""EmployerName"" : ""Signature One"",
    ""ClosureReason"" : null,
    ""Disability"" : {
        ""_id"" : ""ccdb8e43-5246-497e-ae57-8547d37ab6b7"",
        ""PrimaryDiagnosis"" : null,
        ""PrimaryPathId"" : null,
        ""PrimaryPathText"" : null,
        ""PrimaryPathMinDays"" : null,
        ""PrimaryPathMaxDays"" : null,
        ""PrimaryPathDaysUIText"" : null,
        ""SecondaryDiagnosis"" : null,
        ""MedicalComplications"" : false,
        ""Hospitalization"" : false,
        ""GeneralHealthCondition"" : null,
        ""ConditionStartDate"" : null
    },
    ""Certifications"" : [],
    ""Summary"" : {
        ""_id"" : ""3abcdb50-567d-47a1-904b-3c3383c7a003"",
        ""g"" : 1,
        ""a"" : 0,
        ""t"" : 1,
        ""p"" : [ 
            {
                ""_id"" : ""7c7cce8f-1772-4335-8f44-bd4932335bdf"",
                ""p"" : ""552d8385315fd80f98341d70"",
                ""c"" : ""FMLA"",
                ""n"" : ""Family Medical Leave Act"",
                ""d"" : [ 
                    {
                        ""_id"" : ""6fc46ebf-ceb0-4fe3-a9f9-881a96924725"",
                        ""s"" : ISODate(""2015-07-01T00:00:00.000Z""),
                        ""e"" : ISODate(""2015-09-23T00:00:00.000Z""),
                        ""t"" : 1,
                        ""g"" : 0,
                        ""a"" : 0
                    }, 
                    {
                        ""_id"" : ""312a1120-5731-49a4-a7cc-c70ba57a08fd"",
                        ""s"" : ISODate(""2015-09-24T00:00:00.000Z""),
                        ""e"" : ISODate(""2016-06-29T00:00:00.000Z""),
                        ""t"" : 1,
                        ""g"" : 0,
                        ""a"" : 2
                    }, 
                    {
                        ""_id"" : ""654f65f3-1949-4305-804e-f406bc3a5c32"",
                        ""s"" : ISODate(""2016-06-30T00:00:00.000Z""),
                        ""e"" : ISODate(""2016-07-01T00:00:00.000Z""),
                        ""t"" : 1,
                        ""g"" : 0,
                        ""a"" : 0
                    }
                ],
                ""m"" : []
            }
        ],
        ""r"" : false,
        ""e"" : false,
        ""f"" : false,
        ""l"" : false,
        ""u"" : ""0 Weeks"",
        ""x"" : ISODate(""2015-09-24T00:00:00.000Z""),
        ""z"" : null,
        ""m"" : null,
        ""MinApprovedThruDate"" : null,
        ""MinDuration"" : null,
        ""MinDurationString"" : null
    },
    ""Pay"" : {
        ""_id"" : ""8315de04-58ba-4d9e-90a2-80ab7ae755dd"",
        ""PayPeriods"" : [],
        ""ApplyOffsetsByDefault"" : true,
        ""WaiveWaitingPeriod"" : {
            ""_id"" : ""b05fae28-6b69-432a-95ba-07ee89ef322f"",
            ""OverrideValue"" : false
        },
        ""PayScheduleId"" : null
    },
    ""IsAccommodation"" : false,
    ""IsInformationOnly"" : false,
    ""WorkRestrictions"" : [],
    ""CustomFields"" : []
}");


            BsonDocument employeeBson = BsonDocument.Parse(@"{
    ""_id"" : ObjectId(""55b7f2f7c5f32e072caaa37a""),
    ""LastERow"" : ""OpOdKfK9J572N0+mMU39rw=="",
    ""cdt"" : ISODate(""2015-07-28T21:24:07.310Z""),
    ""cby"" : ObjectId(""000000000000000000000002""),
    ""mdt"" : ISODate(""2015-07-28T21:40:06.003Z""),
    ""mby"" : ObjectId(""000000000000000000000002""),
    ""CustomerId"" : ObjectId(""000000000000000000000002""),
    ""EmployerId"" : ObjectId(""000000000000000000000002""),
    ""EmployeeNumber"" : ""S900000002"",
    ""Info"" : {
        ""_id"" : ""99e41e69-33bc-425f-89ad-1ca9487de9e9"",
        ""HomeEmail"" : null,
        ""Email"" : ""markbacom@absencesoft.com"",
        ""AltEmail"" : ""mark@absencesoft.com"",
        ""Address"" : {
            ""_id"" : ""06ce7428-eb13-4fab-a4d9-2602708ae986"",
            ""Address1"" : ""124 Second St"",
            ""Address2"" : ""#326"",
            ""City"" : ""Brooklyn"",
            ""State"" : ""NY"",
            ""PostalCode"" : ""10002"",
            ""Country"" : ""US""
        },
        ""AltAddress"" : {
            ""_id"" : ""b860b866-bc42-4d65-81b8-e07f17db7851"",
            ""Address1"" : null,
            ""City"" : null,
            ""State"" : null,
            ""PostalCode"" : null,
            ""Country"" : ""US""
        },
        ""WorkPhone"" : ""2122222229"",
        ""HomePhone"" : ""2121111119"",
        ""CellPhone"" : ""2122222299"",
        ""AltPhone"" : ""2123333339"",
        ""OfficeLocation"" : ""NY107""
    },
    ""Job"" : {
        ""_id"" : ""3a26639b-0808-47b4-abef-81b43062f862"",
        ""JobFunctions"" : [],
        ""JobDescription"" : ""Office Clerical, Data Entry, Answer Phones, Lifting under 20 lbs."",
        ""Classification"" : 1,
        ""CensusJobCategoryCode"" : ""Census101"",
        ""SOCCode"" : ""SOC212""
    },
    ""FirstName"" : ""Harry"",
    ""MiddleName"" : ""Q"",
    ""LastName"" : ""0 to 5 Year"",
    ""Gender"" : 77,
    ""DoB"" : ISODate(""1982-02-02T00:00:00.000Z""),
    ""Ssn"" : null,
    ""Status"" : 65,
    ""Salary"" : 90000.0000000000000000,
    ""PayType"" : 1,
    ""WorkType"" : 2,
    ""JobTitle"" : ""Service Rep"",
    ""PriorHours"" : [ 
        {
            ""_id"" : ""4a1827e5-c6b2-44b2-861d-57296b505921"",
            ""HoursWorked"" : 1512.0000000000000000,
            ""AsOf"" : ISODate(""2011-11-11T00:00:00.000Z"")
        }
    ],
    ""ServiceDate"" : ISODate(""2012-03-03T00:00:00.000Z""),
    ""HireDate"" : ISODate(""2012-03-03T00:00:00.000Z""),
    ""WorkState"" : ""NY"",
    ""WorkCountry"" : ""USA"",
    ""WorkSchedules"" : [ 
        {
            ""_id"" : ""e4e4a93d-151d-49b4-bb07-5f24b003fe71"",
            ""ScheduleType"" : 0,
            ""StartDate"" : ISODate(""2011-11-11T00:00:00.000Z""),
            ""EndDate"" : null,
            ""Times"" : [ 
                {
                    ""_id"" : ""17aac2c3-d8f9-49b1-9dcf-40d7c6c46448"",
                    ""TotalMinutes"" : 400,
                    ""SampleDate"" : ISODate(""2011-11-06T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""39addd96-587e-4477-8f99-8a2a66e958de"",
                    ""TotalMinutes"" : 400,
                    ""SampleDate"" : ISODate(""2011-11-07T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""935410a2-64be-4cdf-91b2-9b7379f63906"",
                    ""TotalMinutes"" : 400,
                    ""SampleDate"" : ISODate(""2011-11-08T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""4b76b335-2e05-45af-9733-2ff3fba5312f"",
                    ""TotalMinutes"" : 400,
                    ""SampleDate"" : ISODate(""2011-11-09T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""6e0d70d2-f96f-4b38-9582-e5d7cd202570"",
                    ""TotalMinutes"" : 400,
                    ""SampleDate"" : ISODate(""2011-11-10T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""0f24f463-600a-48a2-8897-e60a821a70bf"",
                    ""TotalMinutes"" : 0,
                    ""SampleDate"" : ISODate(""2011-11-11T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""7367c95b-0e82-4457-8da6-3623d9165502"",
                    ""TotalMinutes"" : 0,
                    ""SampleDate"" : ISODate(""2011-11-12T00:00:00.000Z"")
                }
            ]
        }
    ],
    ""Meets50In75MileRule"" : true,
    ""IsKeyEmployee"" : false,
    ""IsExempt"" : false,
    ""MilitaryStatus"" : 0,
    ""CustomFields"" : [],
    ""EmployerName"" : ""Signature One""
}");

            #endregion

            Case testCase = null;
            Employee emp = null;

            testCase = BsonSerializer.Deserialize<Case>(caseBson);
            emp = BsonSerializer.Deserialize<Employee>(employeeBson);

            testCase.Employee = emp;
            emp.Save();

            using (CaseService svc = new CaseService())
                testCase = svc.RunCalcs(testCase);

            Assert.AreEqual(2, testCase.Summary.Policies.First().Detail.Count(), "Should only be 6 periods of FMLA, 12 weeks pending and the rest exhausted");
            Assert.AreEqual(AdjudicationSummaryStatus.Denied, testCase.Summary.Policies.First().Detail.Last().Determination, "Last Period should be Denied");
            Assert.AreEqual(AdjudicationDenialReason.Exhausted, testCase.Summary.Policies.First().Detail.Last().DenialReasonCode, AdjudicationDenialReason.ExhaustedDescription);
        }
    }
}
