﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data;
using System.Linq;
using System.Collections.Generic;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Tests.Services.Cases
{
    [TestClass]
    public class LeaveOfAbsenceTest
    {
        [TestMethod]
        public void EmployeeLoadTest()
        {
            // load a demo employee
            var ee = AbsenceSoft.Data.Customers.Employee.AsQueryable().Where(e => e.EmployeeNumber == "demo_001").FirstOrDefault();
            Assert.IsNotNull(ee);

            // follow the link
            var emp = ee.Employer;
            Assert.IsNotNull(emp);
        }


        [TestMethod]
        public void TotalHoursWorked()
        {
            LeaveOfAbsence loa = new LeaveOfAbsence();

            // this test relies on the test data (sorry, there's too much data to copy
            // and paste to here) if you decide to be free of the demo set up copy the data here

            Employee tedJones = Employee.AsQueryable().Where(e => e.EmployeeNumber == "demo_001").FirstOrDefault();
            Assert.IsNotNull(tedJones);

            loa.Employee = tedJones;
            loa.Employer = tedJones.Employer;
            loa.WorkSchedule = tedJones.WorkSchedules;

            // his new start date
            //monday january 14th, 2013 - blow away his schedule and create a new one
            //sunday 20th

            tedJones.PriorHours = new List<PriorHours>();
            tedJones.WorkSchedules = new List<Schedule>();
            tedJones.HireDate = new DateTime(2013, 1, 20).ToMidnight();
            tedJones.ServiceDate = new DateTime(2013, 1, 20).ToMidnight();

            // create a full time pattern that starts on any given Sunday (just for grins it's this year)
            DateTime schedDate = new DateTime(2014, 1, 26);
            List<Time> fullTime = TestWorkSchedules.GetFullTimeList();
            
            tedJones.WorkSchedules.Add(new Schedule()
            {
                Times = fullTime,
                StartDate = new DateTime(2013, 1, 20).ToMidnight(),
                ScheduleType = ScheduleType.Weekly
            });
            loa.WorkSchedule = tedJones.WorkSchedules;

            // 24 hours
            loa.Case = new Case()
            {
                StartDate = new DateTime(2013, 1, 25)
            };

            double hw = 0d;

            hw = loa.TotalHoursWorkedLast12Months();
            Assert.AreEqual(24d, hw); // includes mlk day 01-21-2013

            tedJones.WorkSchedules[0].StartDate = new DateTime(2013, 1, 2); // 16 work days
            tedJones.HireDate = tedJones.WorkSchedules[0].StartDate;
            tedJones.ServiceDate = tedJones.WorkSchedules[0].StartDate;
            hw = loa.TotalHoursWorkedLast12Months();
            Assert.AreEqual(128d, hw);

            tedJones.WorkSchedules[0].StartDate = new DateTime(2013, 1, 1); // still 16 work days because of new years
            tedJones.HireDate = tedJones.WorkSchedules[0].StartDate;
            tedJones.ServiceDate = tedJones.WorkSchedules[0].StartDate;
            hw = loa.TotalHoursWorkedLast12Months();
            Assert.AreEqual(128d, hw);

            // let's test the future
            // change the case date to june 
            loa.Case.StartDate = new DateTime(2014, 6, 27);

            tedJones.WorkSchedules[0].StartDate = new DateTime(2014, 6, 9); // 14 work days
            tedJones.HireDate = tedJones.WorkSchedules[0].StartDate;
            tedJones.ServiceDate = tedJones.WorkSchedules[0].StartDate;
            hw = loa.TotalHoursWorkedLast12Months();
            Assert.AreEqual(112d, hw);

            tedJones.WorkSchedules[0].StartDate = new DateTime(2014, 5, 23); // 24 work days - 1 day for memorial day
            tedJones.HireDate = tedJones.WorkSchedules[0].StartDate;
            tedJones.ServiceDate = tedJones.WorkSchedules[0].StartDate;
            hw = loa.TotalHoursWorkedLast12Months();
            Assert.AreEqual(192d, hw);

            List<Time> weirdTime = TestWorkSchedules.RotatingSchedule();
            tedJones.WorkSchedules[0].Times = weirdTime;
            tedJones.WorkSchedules[0].ScheduleType = ScheduleType.Rotating;

            // start with testing the base pattern - no holiday
            tedJones.WorkSchedules[0].StartDate = new DateTime(2014, 3, 2);
            tedJones.HireDate = tedJones.WorkSchedules[0].StartDate;
            tedJones.ServiceDate = tedJones.WorkSchedules[0].StartDate;
            //loa.Case.CaseEventDate = new DateTime(2014, 3, 23);                 // one full pass
            //Assert.IsTrue(loa.TotalHoursWorked() == 144d);
            loa.Case.StartDate = new DateTime(2014, 4, 13);                 // two full passes
            hw = loa.TotalHoursWorkedLast12Months();
            Assert.AreEqual(288d, hw);

            loa.Case.StartDate = new DateTime(2014, 4, 20);                 // two full passes + the second week of the pattern
            hw = loa.TotalHoursWorkedLast12Months();
            Assert.AreEqual(324d, hw);

            loa.Case.StartDate = new DateTime(2014, 4, 27);                 // two full passes + the second week + 3rd week of the pattern
            hw = loa.TotalHoursWorkedLast12Months();
            Assert.AreEqual(360d, hw);

            // test matching pattern start dates (because we haven't done that yet) 
            tedJones.WorkSchedules[0].StartDate = new DateTime(2014, 2, 2);
            tedJones.HireDate = tedJones.WorkSchedules[0].StartDate;
            tedJones.ServiceDate = tedJones.WorkSchedules[0].StartDate;

            loa.Case.StartDate = new DateTime(2014, 2, 23);                 // 1 full pass and the first week - GW birthday (falls on the 12 hour day)
            hw = loa.TotalHoursWorkedLast12Months();
            Assert.AreEqual(132d, hw);

            loa.Case.StartDate = new DateTime(2014, 3, 2);                 // 1 full pass and the first week - GW birhtday 
            hw = loa.TotalHoursWorkedLast12Months();
            Assert.AreEqual(204d, hw);

        }                   // public void TotalHoursWorked()

        [Ignore]
        [TestMethod]
        public void TotalHoursWorkedWithPriorHours()
        {
            LeaveOfAbsence loa = new LeaveOfAbsence();
            Employee tedJones = Employee.AsQueryable().Where(e => e.EmployeeNumber == "demo_001").FirstOrDefault();
            Assert.IsNotNull(tedJones);
            loa.Employee = tedJones;
            loa.Employer = tedJones.Employer;
            tedJones.PriorHours = new List<PriorHours>(1)
            {
                new PriorHours() { AsOf = new DateTime(2013, 1, 20).ToMidnight(), HoursWorked = 2040 }
            };
            tedJones.WorkSchedules = new List<Schedule>();

            // create a full time pattern that starts on any given Sunday (just for grins it's this year)
            List<Time> fullTime = TestWorkSchedules.GetFullTimeList();
            tedJones.WorkSchedules.Add(new Schedule()
            {
                Times = fullTime,
                StartDate = new DateTime(2012, 1, 20),
                ScheduleType = AbsenceSoft.Data.Enums.ScheduleType.Weekly
            });
            loa.WorkSchedule = tedJones.WorkSchedules;

            // 24 hours
            loa.Case = new AbsenceSoft.Data.Cases.Case()
            {
                StartDate = new DateTime(2013, 1, 25)
            };

            double hw = 0d;

            hw = loa.TotalHoursWorkedLast12Months();
            // Should be 2040 as of 1/20/2013, but then adding 24 hours up to the case start date, and then Subtracting 32 hours 
            //  based on the offset from the total span of days between the As Of date and Case Start date (4 days)
            Assert.AreEqual(2040d, hw); // includes mlk day 01-21-2013

        } // TotalHoursWorkedWithPriorHours

        [TestMethod]
        public void TotalMonthsWorked()
        {
            LeaveOfAbsence loa = new LeaveOfAbsence();

            // this test relies on the test data (sorry, there's too much data to copy
            // and paste to here) if you decide to be free of the demo set up copy the data here
            Employee tedJones = AbsenceSoft.Data.Customers.Employee.AsQueryable().Where(e => e.EmployeeNumber == "demo_001").FirstOrDefault();
            Assert.IsNotNull(tedJones);

            loa.Employee = tedJones;
            loa.Employer = tedJones.Employer;
            loa.WorkSchedule = tedJones.WorkSchedules;

            tedJones.PriorHours = new List<PriorHours>();
            tedJones.WorkSchedules = new List<Schedule>();
            tedJones.ServiceDate = new DateTime(2014, 7, 29).ToMidnight();

            // create a full time pattern that starts on any given Sunday (just for grins it's this year)

            tedJones.WorkSchedules.Add(new Schedule()
            {
                Times = TestWorkSchedules.GetFullTimeList(),
                StartDate = new DateTime(2014, 7, 29).ToMidnight(),
                ScheduleType = AbsenceSoft.Data.Enums.ScheduleType.Weekly
            });
            loa.WorkSchedule = tedJones.WorkSchedules;

            // start in jan end in jan
            loa.Case = new AbsenceSoft.Data.Cases.Case()
            {
                StartDate = new DateTime(2014, 8, 29)
            };

            Assert.IsTrue(loa.TotalMonthsWorked() == 2);

            loa.Case.StartDate = new DateTime(2014, 6, 1);
            Assert.IsTrue(loa.TotalMonthsWorked() == 0); // Should not have worked any months

            loa.Case.StartDate = new DateTime(2014, 10, 15);
            Assert.IsTrue(loa.TotalMonthsWorked() == 4);

            loa.Case.StartDate = new DateTime(2014, 11, 30);
            Assert.IsTrue(loa.TotalMonthsWorked() == 5);
        }


        [TestMethod]
        public void MaterializeScheduleTest()
        {
            LeaveOfAbsence loa = new LeaveOfAbsence();

            // this test relies on the test data (sorry, there's too much data to copy
            // and paste to here) if you decide to be free of the demo set up copy the data here
            Employee tedJones = AbsenceSoft.Data.Customers.Employee.AsQueryable().Where(e => e.EmployeeNumber == "demo_001").FirstOrDefault();
            Assert.IsNotNull(tedJones);

            loa.Employee = tedJones;
            loa.Employer = tedJones.Employer;
            loa.WorkSchedule = tedJones.WorkSchedules;

            // loa's must have a case - this one is arbitrary
            loa.Case = new AbsenceSoft.Data.Cases.Case();

            tedJones.PriorHours = new List<PriorHours>();
            tedJones.WorkSchedules = new List<Schedule>();

            // create a full time pattern that starts on any given Sunday (just for grins it's this year)
            tedJones.WorkSchedules.Add(new Schedule()
            {
                Times = TestWorkSchedules.GetFullTimeList(),
                StartDate = new DateTime(2013, 1, 20),
                ScheduleType = AbsenceSoft.Data.Enums.ScheduleType.Weekly
            });
            loa.WorkSchedule = tedJones.WorkSchedules;

            // 1 week 40 hours
            int total = 0;
            List<Time> testDates = loa.MaterializeSchedule(new DateTime(2014, 2, 9), new DateTime(2014, 2, 15));
            total = testDates.Sum(t => t.TotalMinutes ?? 0);
            Assert.IsTrue(total == 2400);

            // 2 weeks 80 hours - 1 holiday 72 hours
            testDates = loa.MaterializeSchedule(new DateTime(2014, 2, 9), new DateTime(2014, 2, 22));
            total = testDates.Sum(t => t.TotalMinutes ?? 0);
            Assert.IsTrue(total == 4320);

            // 2 weeks + 3 week days - 1 holiday = 5760
            testDates = loa.MaterializeSchedule(new DateTime(2014, 2, 9), new DateTime(2014, 2, 26));
            total = testDates.Sum(t => t.TotalMinutes ?? 0);
            Assert.IsTrue(total == 5760);

        }

        [TestMethod]
        public void Has1YearOfServiceFail()
        {
            LeaveOfAbsence loa = new LeaveOfAbsence();
            loa.Case = new Case() { StartDate = DateTime.UtcNow.ToMidnight() };
            loa.Employee = new Employee()
            {
                ServiceDate = loa.Case.StartDate.SameDayLastYear().AddDays(1),
            };
            Assert.IsFalse(loa.Has1YearOfService());
        }

        [TestMethod]
        public void Has1YearOfServiceTrue()
        {
            LeaveOfAbsence loa = new LeaveOfAbsence();
            loa.Case = new Case() { StartDate = DateTime.UtcNow.ToMidnight() };
            loa.Employee = new Employee()
            {
                ServiceDate = loa.Case.StartDate.SameDayLastYear()
            };
            Assert.IsTrue(loa.Has1YearOfService());
        }

        [TestMethod]
        public void Meets50In75MileRuleTrue()
        {
            LeaveOfAbsence loa = new LeaveOfAbsence();
            loa.Employer = new Employer()
            {
                Enable50In75MileRule = false
            };
            loa.Employee = new Employee()
            {
                Meets50In75MileRule = true
            };
            Assert.IsTrue(loa.Meets50In75MileRule());
        }

        [TestMethod]
        public void Meets50In75MileRuleEmployerDisabledTrue()
        {
            LeaveOfAbsence loa = new LeaveOfAbsence();
            loa.Employer = new Employer()
            {
                Enable50In75MileRule = false
            };
            loa.Employee = new Employee()
            {
                Meets50In75MileRule = false
            };
            Assert.IsTrue(loa.Meets50In75MileRule());
        }

        [TestMethod]
        public void Meets50In75MileRuleEmployerEnabledTrue()
        {
            LeaveOfAbsence loa = new LeaveOfAbsence();
            loa.Employer = new Employer()
            {
                Enable50In75MileRule = true
            };
            loa.Employee = new Employee()
            {
                Meets50In75MileRule = true
            };
            Assert.IsTrue(loa.Meets50In75MileRule());
        }

        [TestMethod]
        public void Meets50In75MileRuleFail()
        {
            LeaveOfAbsence loa = new LeaveOfAbsence();
            loa.Employer = new Employer()
            {
                Enable50In75MileRule = true
            };
            loa.Employee = new Employee()
            {
                Meets50In75MileRule = false
            };
            Assert.IsFalse(loa.Meets50In75MileRule());
        }

        private LeaveOfAbsence CreateLeaveOfAbsenceForCustomFieldTests(string fieldName, CustomFieldType fieldType, object fieldValue)
        {
            LeaveOfAbsence loa = new LeaveOfAbsence()
            {
                Employee = new Employee()
            };
            if (!string.IsNullOrEmpty(fieldName))
            {
                loa.Employee.CustomFields.Add(CreateCustomField(fieldName, fieldType, fieldValue));
            }

            return loa;
        }

        private CustomField CreateCustomField(string fieldName, CustomFieldType fieldType, object fieldValue)
        {
            return new CustomField()
            {
                Name = fieldName,
                DataType = fieldType,
                SelectedValue = fieldValue
            };
        }

        #region Text Employee Custom Field Rules

        [TestMethod]
        public void EmployeeWithNoCustomFieldsFailsCustomFieldEvaluation()
        {
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(null, CustomFieldType.Text, null);
            Assert.IsFalse(loa.EmployeeCustomField("==", "blah", "fieldName"));
        }

        [TestMethod]
        public void EmployeeWithTextCustomFieldPassesEqualsTest()
        {
            string customFieldName = "Test Custom Field";
            string customFieldValue = "Blah";
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Text, customFieldValue);
            Assert.IsTrue(loa.EmployeeCustomField("==", customFieldValue, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithTextCustomFieldPassesNotEqualsTest()
        {
            string customFieldName = "Test Custom Field";
            string customFieldValue = "Blah";
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Text, customFieldValue);
            Assert.IsTrue(loa.EmployeeCustomField("!=", "NotBlah", customFieldName));
        }

        [TestMethod]
        public void EmployeeWithTextCustomFieldFailsEqualsTest()
        {
            string customFieldName = "Test Custom Field";
            string customFieldValue = "Blah";
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Text, customFieldValue);
            Assert.IsFalse(loa.EmployeeCustomField("==", "NotBlah", customFieldName));
        }

        [TestMethod]
        public void EmployeeWithTextCustomFieldFailsNotEqualsTest()
        {
            string customFieldName = "Test Custom Field";
            string customFieldValue = "Blah";
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Text, customFieldValue);
            Assert.IsFalse(loa.EmployeeCustomField("!=", "Blah", customFieldName));
        }

        #endregion

        #region Number Employee Custom Field Rules

        [TestMethod]
        public void EmployeeWithNumberCustomFieldPassesEqualsTest()
        {
            string customFieldName = "Test Custom Field";
            int customFieldValue = 1;
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Number, customFieldValue);
            Assert.IsTrue(loa.EmployeeCustomField("==", 1, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithNumberCustomFieldFailsEqualsTest()
        {
            string customFieldName = "Test Custom Field";
            int customFieldValue = 1;
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Number, customFieldValue);
            Assert.IsFalse(loa.EmployeeCustomField("==", 2, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithNumberCustomFieldPassesNotEqualsTest()
        {
            string customFieldName = "Test Custom Field";
            int customFieldValue = 1;
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Number, customFieldValue);
            Assert.IsTrue(loa.EmployeeCustomField("!=", 2, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithNumberCustomFieldFailsNotEqualsTest()
        {
            string customFieldName = "Test Custom Field";
            int customFieldValue = 1;
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Number, customFieldValue);
            Assert.IsFalse(loa.EmployeeCustomField("!=", 1, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithNumberCustomFieldPassesGreaterThanTest()
        {
            string customFieldName = "Test Custom Field";
            int customFieldValue = 1;
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Number, customFieldValue);
            Assert.IsTrue(loa.EmployeeCustomField(">", 0, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithNumberCustomFieldFailsGreaterThanTest()
        {
            string customFieldName = "Test Custom Field";
            int customFieldValue = 1;
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Number, customFieldValue);
            Assert.IsFalse(loa.EmployeeCustomField(">", 1, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithNumberCustomFieldPassesGreaterThanOrEqualToTest()
        {
            string customFieldName = "Test Custom Field";
            int customFieldValue = 1;
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Number, customFieldValue);
            Assert.IsTrue(loa.EmployeeCustomField(">=", 1, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithNumberCustomFieldFailsGreaterThanOrEqualToTest()
        {
            string customFieldName = "Test Custom Field";
            int customFieldValue = 1;
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Number, customFieldValue);
            Assert.IsFalse(loa.EmployeeCustomField(">=", 2, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithNumberCustomFieldPassesLessThanTest()
        {
            string customFieldName = "Test Custom Field";
            int customFieldValue = 1;
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Number, customFieldValue);
            Assert.IsTrue(loa.EmployeeCustomField("<", 2, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithNumberCustomFieldFailsLessThanTest()
        {
            string customFieldName = "Test Custom Field";
            int customFieldValue = 1;
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Number, customFieldValue);
            Assert.IsFalse(loa.EmployeeCustomField("<", 1, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithNumberCustomFieldPassesLessThanOrEqualToTest()
        {
            string customFieldName = "Test Custom Field";
            int customFieldValue = 1;
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Number, customFieldValue);
            Assert.IsTrue(loa.EmployeeCustomField("<=", 1, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithNumberCustomFieldFailsLessThanOrEqualToTest()
        {
            string customFieldName = "Test Custom Field";
            int customFieldValue = 1;
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Number, customFieldValue);
            Assert.IsFalse(loa.EmployeeCustomField("<=", 0, customFieldName));
        }

        #endregion

        #region Flag Employee Custom Field Rules

        [TestMethod]
        public void EmployeeWithBooleanCustomFieldPassesTrueTest()
        {
            string customFieldName = "Test Flag Field";
            bool customFieldValue = true;
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Flag, customFieldValue);
            Assert.IsTrue(loa.EmployeeCustomField("==", true, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithBooleanCustomFieldPassesFalseTest()
        {
            string customFieldName = "Test Flag Field";
            bool customFieldValue = false;
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Flag, customFieldValue);
            Assert.IsTrue(loa.EmployeeCustomField("==", false, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithBooleanCustomFieldFailsTrueTest()
        {
            string customFieldName = "Test Flag Field";
            bool customFieldValue = true;
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Flag, customFieldValue);
            Assert.IsFalse(loa.EmployeeCustomField("==", false, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithBooleanCustomFieldFailsFalseTest()
        {
            string customFieldName = "Test Flag Field";
            bool customFieldValue = false;
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Flag, customFieldValue);
            Assert.IsFalse(loa.EmployeeCustomField("==", true, customFieldName));
        }

        #endregion

        #region Date Employee Custom Field Rules

        [TestMethod]
        public void EmployeeWithDateCustomFieldPassesEqualsTest()
        {
            string customFieldName = "Test Custom Field";
            DateTime customFieldValue = new DateTime(2018, 1, 1).ToMidnight();
            DateTime ruleValue = new DateTime(2018, 1, 1).ToMidnight();
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Date, customFieldValue);
            Assert.IsTrue(loa.EmployeeCustomField("==", ruleValue, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithDateCustomFieldFailsEqualsTest()
        {
            string customFieldName = "Test Custom Field";
            DateTime customFieldValue = new DateTime(2018, 1, 1).ToMidnight();
            DateTime ruleValue = new DateTime(2018, 1, 2).ToMidnight();
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Date, customFieldValue);
            Assert.IsFalse(loa.EmployeeCustomField("==", ruleValue, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithDateCustomFieldPassesNotEqualsTest()
        {
            string customFieldName = "Test Custom Field";
            DateTime customFieldValue = new DateTime(2018, 1, 1).ToMidnight();
            DateTime ruleValue = new DateTime(2018, 1, 2).ToMidnight();
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Date, customFieldValue);
            Assert.IsTrue(loa.EmployeeCustomField("!=", ruleValue, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithDateCustomFieldFailsNotEqualsTest()
        {
            string customFieldName = "Test Custom Field";
            DateTime customFieldValue = new DateTime(2018, 1, 1).ToMidnight();
            DateTime ruleValue = new DateTime(2018, 1, 1).ToMidnight();
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Date, customFieldValue);
            Assert.IsFalse(loa.EmployeeCustomField("!=", ruleValue, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithDateCustomFieldPassesGreaterThanTest()
        {
            string customFieldName = "Test Custom Field";
            DateTime customFieldValue = new DateTime(2018, 1, 1).ToMidnight();
            DateTime ruleValue = new DateTime(2017, 12, 31).ToMidnight();
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Date, customFieldValue);
            Assert.IsTrue(loa.EmployeeCustomField(">", ruleValue, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithDateCustomFieldFailsGreaterThanTest()
        {
            string customFieldName = "Test Custom Field";
            DateTime customFieldValue = new DateTime(2018, 1, 1).ToMidnight();
            DateTime ruleValue = new DateTime(2018, 1, 1).ToMidnight();
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Date, customFieldValue);
            Assert.IsFalse(loa.EmployeeCustomField(">", ruleValue, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithDateCustomFieldPassesGreaterThanOrEqualToTest()
        {
            string customFieldName = "Test Custom Field";
            DateTime customFieldValue = new DateTime(2018, 1, 1).ToMidnight();
            DateTime ruleValue = new DateTime(2018, 1, 1).ToMidnight();
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Date, customFieldValue);
            Assert.IsTrue(loa.EmployeeCustomField(">=", ruleValue, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithDateCustomFieldFailsGreaterThanOrEqualToTest()
        {
            string customFieldName = "Test Custom Field";
            DateTime customFieldValue = new DateTime(2018, 1, 1).ToMidnight();
            DateTime ruleValue = new DateTime(2018, 1, 2).ToMidnight();
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Date, customFieldValue);
            Assert.IsFalse(loa.EmployeeCustomField(">=", ruleValue, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithDateCustomFieldPassesLessThanTest()
        {
            string customFieldName = "Test Custom Field";
            DateTime customFieldValue = new DateTime(2018, 1, 1).ToMidnight();
            DateTime ruleValue = new DateTime(2018, 1, 2).ToMidnight();
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Date, customFieldValue);
            Assert.IsTrue(loa.EmployeeCustomField("<", ruleValue, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithDateCustomFieldFailsLessThanTest()
        {
            string customFieldName = "Test Custom Field";
            DateTime customFieldValue = new DateTime(2018, 1, 1).ToMidnight();
            DateTime ruleValue = new DateTime(2018, 1, 1).ToMidnight();
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Date, customFieldValue);
            Assert.IsFalse(loa.EmployeeCustomField("<", ruleValue, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithDateCustomFieldPassesLessThanOrEqualToTest()
        {
            string customFieldName = "Test Custom Field";
            DateTime customFieldValue = new DateTime(2018, 1, 1).ToMidnight();
            DateTime ruleValue = new DateTime(2018, 1, 1).ToMidnight();
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Date, customFieldValue);
            Assert.IsTrue(loa.EmployeeCustomField("<=", ruleValue, customFieldName));
        }

        [TestMethod]
        public void EmployeeWithDateCustomFieldFailsLessThanOrEqualToTest()
        {
            string customFieldName = "Test Custom Field";
            DateTime customFieldValue = new DateTime(2018, 1, 1).ToMidnight();
            DateTime ruleValue = new DateTime(2017, 12, 31).ToMidnight();
            LeaveOfAbsence loa = CreateLeaveOfAbsenceForCustomFieldTests(customFieldName, CustomFieldType.Date, customFieldValue);
            Assert.IsFalse(loa.EmployeeCustomField("<=", ruleValue, customFieldName));
        }

        #endregion
    }   
}
