﻿using AbsenceSoft.Data.Pay;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Pay;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Tests.Services.Cases
{
    [TestClass]
    public class PayServiceTest
    {

        [TestMethod]
        public void GetPolicyPayOrder()
        {
            string employerIdForTest = "000000000000000000000002";
            PayService service = new PayService(GetPolicyPayTestUser(employerIdForTest));
            List<PolicyPayOrder> policies = service.GetEmployerPolicyPayOrder(employerIdForTest);
            Assert.IsTrue(policies.Count == 8, string.Format("Expected seven paid policies and {0} were returned", policies.Count));
        }

        [TestMethod]
        public void GetDefaultPolicyPayOrder()
        {
            string employerIdForTest = "000000000000000000000002";
            PayService service = new PayService(GetPolicyPayTestUser(employerIdForTest));
            List<PolicyPayOrder> policies = service.GenerateDefaultPolicyPayOrder(employerIdForTest);
            Assert.IsTrue(policies.Count == 7, string.Format("Expected seven paid policies and {0} were returned", policies.Count));
        }

        [TestMethod]
        public void SavePolicyPayOrder()
        {
            string employerIdForTest = "000000000000000000000002";
            PayService service = new PayService(GetPolicyPayTestUser(employerIdForTest));
            List<PolicyPayOrder> policies = service.GetEmployerPolicyPayOrder(employerIdForTest);
            policies = policies.OrderBy(p => p.Name).ToList();
            var i = 1;
            foreach (var payOrder in policies)
            {
                payOrder.Order = i;
                i++;
            }
            service.SaveEmployerPolicyPayOrder(policies);
        }

        private User GetPolicyPayTestUser(string employerId)
        {
            User user = new User();
            if (user.Employers == null)
                user.Employers = new List<EmployerAccess>();
            if (!user.Employers.Any(e => e.EmployerId == employerId))
                user.Employers.Add(new EmployerAccess()
                {
                    EmployerId = employerId,
                    AutoAssignCases = true
                });
            if (user.Employers.Count(e => e.EmployerId == employerId) > 1)
            {
                var first = user.Employers.First(e => e.EmployerId == employerId);
                user.Employers.RemoveAll(e => e.EmployerId == employerId && e.Id != first.Id);
            }

            return user;
        }
    }
}
