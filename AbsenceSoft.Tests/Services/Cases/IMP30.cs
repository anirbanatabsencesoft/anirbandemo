﻿using System;
using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Policies;
using System.Collections.Generic;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic;
using AbsenceSoft.Common;
using AbsenceSoft;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Logic.Pay;
using static AbsenceSoft.Setup.Policies.PolicyBase;
using AbsenceSoft.Logic.CaseAssignmentRules;
using static AbsenceSoft.Logic.Cases.CaseService;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Common.Properties;
using System.Web;
using System.Security.Principal;
using MongoDB.Bson;

namespace AbsenceSoft.Tests.Services.Cases
{
    [TestClass]
    public class IMP30
    {
        #region Positive Tests

        [TestMethod]
        public void CalcsCalendarDaysFirstUseTypeTest()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2018, 8, 27, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2018, 10, 25, 0, 0, 0, DateTimeKind.Utc);

            DateTime relapseStartDate = new DateTime(2018, 11, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime relapseEndDate = new DateTime(2018, 11, 30, 0, 0, 0, DateTimeKind.Utc);

            DateTime secondCaseStartDate = new DateTime(2019, 8, 17, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondCaseEndDate = new DateTime(2019, 9, 10, 0, 0, 0, DateTimeKind.Utc);


            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(60, daysUsed);

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.CalendarDaysFromFirstUse;
                fmla.PolicyReason.Entitlement = 90;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(60, daysUsed);

                string caseId = firstCase.Id;
                Case completedCase = Case.Repository.GetById(caseId);

                if (completedCase != null)
                {
                    //Close the Case 
                    completedCase.Status = AbsenceSoft.Data.Enums.CaseStatus.Closed;
                    completedCase.ModifiedById = "000000000000000000000002";
                    completedCase.SetModifiedDate(DateTime.UtcNow);
                    completedCase.Segments.ForEach(s => s.Status = CaseStatus.Closed);
                    completedCase.SaveStatus();
                }
                Assert.AreEqual(CaseStatus.Closed, completedCase.Status);

                // Reopen case with Relapse
                Relapse relapse = new Relapse
                {
                    CaseId = completedCase.Id,
                    StartDate = relapseStartDate,
                    EndDate = relapseEndDate,
                    CaseType = (CaseType)(Enum.Parse(typeof(CaseType), completedCase.CaseType.ToString())),
                    Employee = completedCase.Employee
                };

                LeaveOfAbsence loa = cs.ReopenCase(completedCase, relapse);
                Assert.AreEqual(CaseStatus.Open, loa.Case.Status);

                Case reOpenedCase = cs.UpdateRelapse(loa.Case, loa.Relapse);
                Assert.AreEqual(2, loa.Case.Segments.Count);

                // Approve FMLA policy
                cs.ApplyDetermination(reOpenedCase, policyCode, relapseStartDate, relapseEndDate, AdjudicationStatus.Approved);
                reOpenedCase.Save();

                fmla = reOpenedCase.Segments[1].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(24, daysUsed);

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 11, 25, 0, 0, 0, DateTimeKind.Utc));


                // Create second case of Intermittent type
                Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondCaseStartDate, secondCaseEndDate, AbsenceSoft.Data.Enums.CaseType.Intermittent, reason.Code);
                esvc.RunEligibility(secondCase);

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);
                Assert.IsNotNull(fmla, "FMLA Policy not found");

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.CalendarDaysFromFirstUse;
                fmla.PolicyReason.Entitlement = 90;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(secondCase);
                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);

                apu = fmla.Usage.Where(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted) .LastOrDefault();
                //Assert.IsTrue(apu.DateUsed == new DateTime(2019, 8, 27, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }//end: TestCalcsCalendarDaysFirstUseTypeTest()

        [TestMethod,Ignore]
        public void CalcsCalendarWeeksFirstUseTypeTest()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2018, 8, 27, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2018, 10, 28, 0, 0, 0, DateTimeKind.Utc);

            DateTime relapseStartDate = new DateTime(2018, 10, 29, 0, 0, 0, DateTimeKind.Utc);
            DateTime relapseEndDate = new DateTime(2018, 11, 25, 0, 0, 0, DateTimeKind.Utc);

            DateTime secondCaseStartDate = new DateTime(2018, 12, 17, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondCaseEndDate = new DateTime(2019, 12, 17, 0, 0, 0, DateTimeKind.Utc);


            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(9, Math.Round(daysUsed / 7, 0));

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.CalendarWeeksFromFirstUse;
                fmla.PolicyReason.Entitlement = 13;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(9, daysUsed / 7);

                string caseId = firstCase.Id;
                Case completedCase = Case.Repository.GetById(caseId);

                if (completedCase != null)
                {
                    //Close the Case 
                    completedCase.Status = AbsenceSoft.Data.Enums.CaseStatus.Closed;
                    completedCase.ModifiedById = "000000000000000000000002";
                    completedCase.SetModifiedDate(DateTime.UtcNow);
                    completedCase.Segments.ForEach(s => s.Status = CaseStatus.Closed);
                    completedCase.SaveStatus();
                }
                Assert.AreEqual(CaseStatus.Closed, completedCase.Status);

                // Reopen case with Relapse
                Relapse relapse = new Relapse
                {
                    CaseId = completedCase.Id,
                    StartDate = relapseStartDate,
                    EndDate = relapseEndDate,
                    CaseType = (CaseType)(Enum.Parse(typeof(CaseType), completedCase.CaseType.ToString())),
                    Employee = completedCase.Employee
                };

                LeaveOfAbsence loa = cs.ReopenCase(completedCase, relapse);
                Assert.AreEqual(CaseStatus.Open, loa.Case.Status);

                Case reOpenedCase = cs.UpdateRelapse(loa.Case, loa.Relapse);
                Assert.AreEqual(2, loa.Case.Segments.Count);

                // Approve FMLA policy
                cs.ApplyDetermination(reOpenedCase, policyCode, relapseStartDate, relapseEndDate, AdjudicationStatus.Approved);
                reOpenedCase.Save();

                fmla = reOpenedCase.Segments[1].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(4,Math.Round(daysUsed/7,0));

                AppliedPolicyUsage apu = fmla.Usage.OrderByDescending(c => c.DateUsed).FirstOrDefault(u => u.Determination == AdjudicationStatus.Approved);
                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 11, 25, 0, 0, 0, DateTimeKind.Utc));

                // Create second case of Intermittent type
                Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondCaseStartDate, secondCaseEndDate, AbsenceSoft.Data.Enums.CaseType.Intermittent, reason.Code);
                esvc.RunEligibility(secondCase);

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);
                Assert.IsNotNull(fmla, "FMLA Policy not found");

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.CalendarWeeksFromFirstUse;
                fmla.PolicyReason.Entitlement = 13;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(secondCase);
                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);

                apu = fmla.Usage.Where(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted).LastOrDefault();
                Assert.IsTrue(apu.DateUsed == new DateTime(2019, 8, 27, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }//end: TestCalcsCalendarWeeksFirstUseTypeTest()

        [TestMethod,Ignore]
        public void CalcsCalendarMonthsFirstUseTypeTest()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2018, 8, 27, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2018, 10, 25, 0, 0, 0, DateTimeKind.Utc);

            DateTime relapseStartDate = new DateTime(2018, 11, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime relapseEndDate = new DateTime(2018, 11, 30, 0, 0, 0, DateTimeKind.Utc);

            DateTime secondCaseStartDate = new DateTime(2018, 12, 17, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondCaseEndDate = new DateTime(2019, 12, 17, 0, 0, 0, DateTimeKind.Utc);


            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(2, Math.Round(daysUsed / 30, 0));

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.CalendarMonthsFromFirstUse;
                fmla.PolicyReason.Entitlement = 3;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(2, daysUsed / 30);

                string caseId = firstCase.Id;
                Case completedCase = Case.Repository.GetById(caseId);

                if (completedCase != null)
                {
                    //Close the Case 
                    completedCase.Status = AbsenceSoft.Data.Enums.CaseStatus.Closed;
                    completedCase.ModifiedById = "000000000000000000000002";
                    completedCase.SetModifiedDate(DateTime.UtcNow);
                    completedCase.Segments.ForEach(s => s.Status = CaseStatus.Closed);
                    completedCase.SaveStatus();
                }
                Assert.AreEqual(CaseStatus.Closed, completedCase.Status);

                // Reopen case with Relapse
                Relapse relapse = new Relapse
                {
                    CaseId = completedCase.Id,
                    StartDate = relapseStartDate,
                    EndDate = relapseEndDate,
                    CaseType = (CaseType)(Enum.Parse(typeof(CaseType), completedCase.CaseType.ToString())),
                    Employee = completedCase.Employee
                };

                LeaveOfAbsence loa = cs.ReopenCase(completedCase, relapse);
                Assert.AreEqual(CaseStatus.Open, loa.Case.Status);

                Case reOpenedCase = cs.UpdateRelapse(loa.Case, loa.Relapse);
                Assert.AreEqual(2, loa.Case.Segments.Count);

                // Approve FMLA policy
                cs.ApplyDetermination(reOpenedCase, policyCode, relapseStartDate, relapseEndDate, AdjudicationStatus.Approved);
                reOpenedCase.Save();

                fmla = reOpenedCase.Segments[1].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(1, Math.Round(daysUsed / 30));

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 11, 25, 0, 0, 0, DateTimeKind.Utc));


                // Create second case of Intermittent type
                Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondCaseStartDate, secondCaseEndDate, AbsenceSoft.Data.Enums.CaseType.Intermittent, reason.Code);
                esvc.RunEligibility(secondCase);

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);
                Assert.IsNotNull(fmla, "FMLA Policy not found");

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.CalendarMonthsFromFirstUse;
                fmla.PolicyReason.Entitlement = 3;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(secondCase);
                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);

                apu = fmla.Usage.Where(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted).LastOrDefault();
                //Assert.IsTrue(apu.DateUsed == new DateTime(2019, 8, 27, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }//end: TestCalcsCalendarMonthsFirstUseTypeTest()

        [TestMethod]
        public void CalcsCalendarYearsFirstUseTypeTest()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2018, 8, 27, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2019, 3, 10, 0, 0, 0, DateTimeKind.Utc);

            DateTime relapseStartDate = new DateTime(2019, 3, 28, 0, 0, 0, DateTimeKind.Utc);
            DateTime relapseEndDate = new DateTime(2019, 9, 23, 0, 0, 0, DateTimeKind.Utc);

            DateTime secondCaseStartDate = new DateTime(2019, 12, 17, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondCaseEndDate = new DateTime(2020, 12, 17, 0, 0, 0, DateTimeKind.Utc);


            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7, 0));

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.CalendarYearsFromFirstUse;
                fmla.PolicyReason.Entitlement = 1;
                fmla.PolicyReason.Period = 24;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(28, daysUsed / 7);

                string caseId = firstCase.Id;
                Case completedCase = Case.Repository.GetById(caseId);

                if (completedCase != null)
                {
                    //Close the Case 
                    completedCase.Status = AbsenceSoft.Data.Enums.CaseStatus.Closed;
                    completedCase.ModifiedById = "000000000000000000000002";
                    completedCase.SetModifiedDate(DateTime.UtcNow);
                    completedCase.Segments.ForEach(s => s.Status = CaseStatus.Closed);
                    completedCase.SaveStatus();
                }
                Assert.AreEqual(CaseStatus.Closed, completedCase.Status);

                // Reopen case with Relapse
                Relapse relapse = new Relapse
                {
                    CaseId = completedCase.Id,
                    StartDate = relapseStartDate,
                    EndDate = relapseEndDate,
                    CaseType = (CaseType)(Enum.Parse(typeof(CaseType), completedCase.CaseType.ToString())),
                    Employee = completedCase.Employee
                };

                LeaveOfAbsence loa = cs.ReopenCase(completedCase, relapse);
                Assert.AreEqual(CaseStatus.Open, loa.Case.Status);

                Case reOpenedCase = cs.UpdateRelapse(loa.Case, loa.Relapse);
                Assert.AreEqual(2, loa.Case.Segments.Count);

                // Approve FMLA policy
                cs.ApplyDetermination(reOpenedCase, policyCode, relapseStartDate, relapseEndDate, AdjudicationStatus.Approved);
                reOpenedCase.Save();

                fmla = reOpenedCase.Segments[1].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(22, Math.Round(daysUsed / 7));

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2019, 8, 27, 0, 0, 0, DateTimeKind.Utc));


                // Create second case of Intermittent type
                Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondCaseStartDate, secondCaseEndDate, AbsenceSoft.Data.Enums.CaseType.Intermittent, reason.Code);
                esvc.RunEligibility(secondCase);

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);
                Assert.IsNotNull(fmla, "FMLA Policy not found");

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.CalendarYearsFromFirstUse;
                fmla.PolicyReason.Entitlement = 1;
                fmla.PolicyReason.Period = 24;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(secondCase);
                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == policyCode);

                apu = fmla.Usage.Where(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted).LastOrDefault();
                //Assert.IsTrue(apu.DateUsed == new DateTime(2020, 8, 26, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }//end: TestCalcsCalendarYearsFirstUseTypeTest()

        #endregion

        #region Negative Tests        

        [TestMethod]
        public void CalcsCalendarDaysFirstUseTypeNegativeTest()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2018, 8, 27, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2018, 10, 25, 0, 0, 0, DateTimeKind.Utc);

            DateTime secondCaseStartDate = new DateTime(2018, 12, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondCaseEndDate = new DateTime(2019, 1, 30, 0, 0, 0, DateTimeKind.Utc);


            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                // There's only 60 days between 8/27/2018 - 10/22/2018
                //  https://www.timeanddate.com/date/durationresult.html?m1=08&d1=27&y1=2018&m2=10&d2=25&y2=2018&ti=on
                double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(62, Math.Round(daysUsed, 0));

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.CalendarDaysFromFirstUse;
                fmla.PolicyReason.Entitlement = 90;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(60, daysUsed);

                string caseId = firstCase.Id;
                Case completedCase = Case.Repository.GetById(caseId);

                if (completedCase != null)
                {
                    //Close the Case 
                    completedCase.Status = AbsenceSoft.Data.Enums.CaseStatus.Closed;
                    completedCase.ModifiedById = "000000000000000000000002";
                    completedCase.SetModifiedDate(DateTime.UtcNow);
                    completedCase.Segments.ForEach(s => s.Status = CaseStatus.Closed);
                    completedCase.SaveStatus();
                }
                Assert.AreEqual(CaseStatus.Closed, completedCase.Status);

                Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondCaseStartDate, secondCaseEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(secondCase);

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.CalendarDaysFromFirstUse;
                fmla.PolicyReason.Entitlement = 90;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(secondCase);

                // Approve FMLA policy
                cs.ApplyDetermination(secondCase, policyCode, secondCaseStartDate, secondCaseEndDate, AdjudicationStatus.Approved);
                secondCase.Save();

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 12, 31, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }//end: TestCalcsCalendarDaysFirstUseTypeNegativeTest()

        [TestMethod,Ignore]
        public void CalcsCalendarWeeksFirstUseTypeNegativeTest()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2018, 8, 27, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2018, 10, 28, 0, 0, 0, DateTimeKind.Utc);

            DateTime secondCaseStartDate = new DateTime(2018, 12, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondCaseEndDate = new DateTime(2019, 1, 30, 0, 0, 0, DateTimeKind.Utc);


            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(9, Math.Round(daysUsed / 7, 0));

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.CalendarWeeksFromFirstUse;
                fmla.PolicyReason.Entitlement = 13;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(9, daysUsed / 7);

                string caseId = firstCase.Id;
                Case completedCase = Case.Repository.GetById(caseId);

                if (completedCase != null)
                {
                    //Close the Case 
                    completedCase.Status = AbsenceSoft.Data.Enums.CaseStatus.Closed;
                    completedCase.ModifiedById = "000000000000000000000002";
                    completedCase.SetModifiedDate(DateTime.UtcNow);
                    completedCase.Segments.ForEach(s => s.Status = CaseStatus.Closed);
                    completedCase.SaveStatus();
                }
                Assert.AreEqual(CaseStatus.Closed, completedCase.Status);

                Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondCaseStartDate, secondCaseEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(secondCase);

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.CalendarWeeksFromFirstUse;
                fmla.PolicyReason.Entitlement = 13;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(secondCase);

                // Approve FMLA policy
                cs.ApplyDetermination(secondCase, policyCode, secondCaseStartDate, secondCaseEndDate, AdjudicationStatus.Approved);
                secondCase.Save();

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 12, 29, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }//end: TestCalcsCalendarWeeksFirstUseTypeNegativeTest()

        [TestMethod]
        public void CalcsCalendarMonthsFirstUseTypeNegativeTest()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2018, 8, 27, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2018, 10, 25, 0, 0, 0, DateTimeKind.Utc);

            DateTime secondCaseStartDate = new DateTime(2018, 12, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondCaseEndDate = new DateTime(2019, 1, 30, 0, 0, 0, DateTimeKind.Utc);

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(2, Math.Round(daysUsed / 30, 0));

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.CalendarMonthsFromFirstUse;
                fmla.PolicyReason.Entitlement = 3;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(2, daysUsed / 30);

                string caseId = firstCase.Id;
                Case completedCase = Case.Repository.GetById(caseId);

                if (completedCase != null)
                {
                    //Close the Case 
                    completedCase.Status = AbsenceSoft.Data.Enums.CaseStatus.Closed;
                    completedCase.ModifiedById = "000000000000000000000002";
                    completedCase.SetModifiedDate(DateTime.UtcNow);
                    completedCase.Segments.ForEach(s => s.Status = CaseStatus.Closed);
                    completedCase.SaveStatus();
                }
                Assert.AreEqual(CaseStatus.Closed, completedCase.Status);

                Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondCaseStartDate, secondCaseEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(secondCase);

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.CalendarMonthsFromFirstUse;
                fmla.PolicyReason.Entitlement = 3;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(secondCase);

                // Approve FMLA policy
                cs.ApplyDetermination(secondCase, policyCode, secondCaseStartDate, secondCaseEndDate, AdjudicationStatus.Approved);
                secondCase.Save();

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 12, 31, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }//end: TestCalcsCalendarMonthsFirstUseTypeNegativeTest()

        [TestMethod]
        public void CalcsCalendarYearsFirstUseTypeNegativeTest()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2018, 8, 27, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2019, 3, 10, 0, 0, 0, DateTimeKind.Utc);

            DateTime secondCaseStartDate = new DateTime(2019, 4, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime secondCaseEndDate = new DateTime(2019, 9, 30, 0, 0, 0, DateTimeKind.Utc);

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                double daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(12, Math.Round(daysUsed / 7, 0));

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.CalendarYearsFromFirstUse;
                fmla.PolicyReason.Entitlement = 1;
                fmla.PolicyReason.Period = 24;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                daysUsed = Math.Round(fmla.Usage.Where(u => u.UsesTime && u.Determination == AdjudicationStatus.Approved).Sum(u => u.DaysUsage(EntitlementType.CalendarDays)), 4);
                Assert.AreEqual(28, daysUsed / 7);

                string caseId = firstCase.Id;
                Case completedCase = Case.Repository.GetById(caseId);

                if (completedCase != null)
                {
                    //Close the Case 
                    completedCase.Status = AbsenceSoft.Data.Enums.CaseStatus.Closed;
                    completedCase.ModifiedById = "000000000000000000000002";
                    completedCase.SetModifiedDate(DateTime.UtcNow);
                    completedCase.Segments.ForEach(s => s.Status = CaseStatus.Closed);
                    completedCase.SaveStatus();
                }
                Assert.AreEqual(CaseStatus.Closed, completedCase.Status);

                Case secondCase = cs.CreateCase(CaseStatus.Open, empId, secondCaseStartDate, secondCaseEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(secondCase);

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.CalendarYearsFromFirstUse;
                fmla.PolicyReason.Entitlement = 1;
                fmla.PolicyReason.Period = 24;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(secondCase);

                // Approve FMLA policy
                cs.ApplyDetermination(secondCase, policyCode, secondCaseStartDate, secondCaseEndDate, AdjudicationStatus.Approved);
                secondCase.Save();

                fmla = secondCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2019, 9, 17, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }//end: TestCalcsCalendarYearsFirstUseTypeNegativeTest()

        #endregion

        #region Exhaustion Tests

        [TestMethod]
        public void CalcsCalendarDaysFirstUseTypeExhaustionTest()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2018, 7, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2018, 9, 30, 0, 0, 0, DateTimeKind.Utc);

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.CalendarDaysFromFirstUse;
                fmla.PolicyReason.Entitlement = 90;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 9, 29, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }//end: TestCalcsCalendarDaysFirstUseTypeExhaustionTest()

        [TestMethod,Ignore]
        public void CalcsCalendarWeeksFirstUseTypeExhaustionTest()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2018, 7, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2018, 9, 30, 0, 0, 0, DateTimeKind.Utc);

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.CalendarWeeksFromFirstUse;
                fmla.PolicyReason.Entitlement = 13;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 9, 30, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }//end: TestCalcsCalendarWeeksFirstUseTypeExhaustionTest()

        [TestMethod]
        public void CalcsCalendarMonthsFirstUseTypeExhaustionTest()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2018, 7, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2018, 9, 30, 0, 0, 0, DateTimeKind.Utc);

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.CalendarMonthsFromFirstUse;
                fmla.PolicyReason.Entitlement = 3;
                fmla.PolicyReason.Period = 12;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2018, 9, 29, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }//end: TestCalcsCalendarMonthsFirstUseTypeExhaustionTest()

        [TestMethod]
        public void CalcsCalendarYearsFirstUseTypeExhaustionTest()
        {
            string empId = "000000000000000000000003";
            string caseCode = "EHC";
            string policyCode = "FMLA";

            DateTime firstStartDate = new DateTime(2018, 7, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime firstEndDate = new DateTime(2019, 9, 30, 0, 0, 0, DateTimeKind.Utc);

            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(empId);

            using (CaseService cs = new CaseService())
            using (EligibilityService esvc = new EligibilityService())
            {
                // create first case
                Case firstCase = cs.CreateCase(CaseStatus.Open, empId, firstStartDate, firstEndDate, AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code);
                esvc.RunEligibility(firstCase);

                AppliedPolicy fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                // set Calendar Days From First Use
                fmla.PolicyReason.EntitlementType = EntitlementType.CalendarYearsFromFirstUse;
                fmla.PolicyReason.Entitlement = 1;
                fmla.PolicyReason.Period = 24;
                fmla.PolicyReason.PeriodType = PeriodType.RollingBack;

                // Get Usage
                cs.GetProjectedUsage(firstCase);

                // Approve FMLA policy
                cs.ApplyDetermination(firstCase, policyCode, firstStartDate, firstEndDate, AdjudicationStatus.Approved);
                firstCase.Save();

                fmla = firstCase.Segments.SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>()).FirstOrDefault(a => (a.Policy ?? new Policy()).Code == policyCode);
                Assert.IsNotNull(fmla);

                AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                Assert.IsTrue(apu.DateUsed == new DateTime(2019, 7, 1, 0, 0, 0, DateTimeKind.Utc));
            }

            // clean up
            deleteTestCases(empId);

        }//end: TestCalcsCalendarYearsFirstUseTypeExhaustionTest()

        #endregion

        #region Utility Functions

        private void deleteTestCases(string empId)
        {
            Case.Repository.Collection.Remove(Case.Query.EQ(c => c.Employee.Id, empId));
            ToDoItem.Repository.Collection.Remove(ToDoItem.Query.EQ(c => c.EmployeeId, empId));
            Communication.Repository.Collection.Remove(Communication.Query.EQ(c => c.EmployeeId, empId));
            Attachment.Repository.Collection.Remove(Attachment.Query.EQ(c => c.EmployeeId, empId));
        }

        #endregion
    }
}
