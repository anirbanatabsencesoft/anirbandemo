﻿using System;
using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Policies;
using System.Collections.Generic;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic;
using AbsenceSoft.Common;
using AbsenceSoft;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Logic.Pay;
using MongoDB.Bson;
using Newtonsoft.Json;
using MongoDB.Bson.Serialization;

namespace AbsenceSoft.Tests.Services.Cases
{
    [TestClass]
    public class CaseSummaryServiceTest
    {
        [TestMethod]
        public void TestCaseSummaryReverseDatesAT185Issue()
        {
            #region BSON

            BsonDocument caseBson = MongoDB.Bson.BsonDocument.Parse(@"{
    ""_id"" : ObjectId(""54ff1a68a32aa00b402175e2""),
    ""ContactPreference"" : ""Phone"",
    ""TimePreference"" : ""Morning"",
    ""SentToPayroll"" : [],
    ""cdt"" : ISODate(""2015-03-10T16:23:03.983Z""),
    ""cby"" : ObjectId(""54ac4f12a32aa006d47413d3""),
    ""mdt"" : ISODate(""2015-07-17T16:52:23.110Z""),
    ""mby"" : ObjectId(""547330ada32aa00f54e2ce01""),
    ""CustomerId"" : ObjectId(""000000000000000000000002""),
    ""EmployerId"" : ObjectId(""000000000000000000000002""),
    ""CaseNumber"" : ""1167901267"",
    ""EmployerCaseNumber"" : null,
    ""StartDate"" : ISODate(""2015-03-20T00:00:00.000Z""),
    ""EndDate"" : ISODate(""2015-05-25T00:00:00.000Z""),
    ""Segments"" : [ 
        {
            ""_id"" : ""6c42757e-f788-4342-9cab-5ccda42aa3fe"",
            ""StartDate"" : ISODate(""2015-03-20T00:00:00.000Z""),
            ""EndDate"" : ISODate(""2015-05-25T00:00:00.000Z""),
            ""Type"" : 2,
            ""AppliedPolicies"" : [ 
                {
                    ""_id"" : ""1d948ad7-b9be-41e5-bb9b-3fdcc0e0ba56"",
                    ""Policy"" : {
                        ""_id"" : ObjectId(""53bdb56da32a9e0f8076f392""),
                        ""SupplementalToMax"" : null,
                        ""SupplementalTo"" : [],
                        ""cdt"" : ISODate(""2014-07-09T21:34:37.356Z""),
                        ""cby"" : ObjectId(""000000000000000000000000""),
                        ""mdt"" : ISODate(""2015-03-10T02:59:39.016Z""),
                        ""mby"" : ObjectId(""000000000000000000000000""),
                        ""CustomerId"" : null,
                        ""EmployerId"" : null,
                        ""Code"" : ""FMLA"",
                        ""Name"" : ""Family Medical Leave Act"",
                        ""Description"" : null,
                        ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000Z""),
                        ""PolicyType"" : 0,
                        ""RuleGroups"" : [ 
                            {
                                ""_id"" : ""4e8a9dde-48da-4beb-9ebc-547586f223cf"",
                                ""Name"" : ""FMLA Eligibility"",
                                ""Description"" : ""All of the following statements about the Employee must be true:"",
                                ""Rules"" : [ 
                                    {
                                        ""_id"" : ""0e524282-b341-4a2b-b524-62f4c538739e"",
                                        ""Name"" : ""1,250 Hours Worked"",
                                        ""Description"" : ""The Employee has worked at least 1,250 hours in the last 12 months"",
                                        ""LeftExpression"" : ""TotalHoursWorkedLast12Months()"",
                                        ""Operator"" : "">="",
                                        ""RightExpression"" : ""1250""
                                    }, 
                                    {
                                        ""_id"" : ""29034cc4-5166-4c5a-8cad-35ee7cfd95e7"",
                                        ""Name"" : ""50 Employees in 75 Mile Radius Rule"",
                                        ""Description"" : ""The Employee works in an office with at least 50 employees that all work within a 75 mile radius"",
                                        ""LeftExpression"" : ""Meets50In75MileRule()"",
                                        ""Operator"" : ""=="",
                                        ""RightExpression"" : ""true""
                                    }, 
                                    {
                                        ""_id"" : ""6ab936f9-b7ad-4530-9fd9-ae27185997b1"",
                                        ""Name"" : ""Worked 12 Months"",
                                        ""Description"" : ""The Employee has worked at least 12 months"",
                                        ""LeftExpression"" : ""Has1YearOfService()"",
                                        ""Operator"" : ""=="",
                                        ""RightExpression"" : ""true""
                                    }, 
                                    {
                                        ""_id"" : ""73171423-8897-42da-8e80-1cbe9f6374b0"",
                                        ""Name"" : ""Key Employees Ineligible"",
                                        ""Description"" : ""Employee may not be a key employee"",
                                        ""LeftExpression"" : ""Employee.IsKeyEmployee"",
                                        ""Operator"" : ""=="",
                                        ""RightExpression"" : ""false""
                                    }
                                ],
                                ""SuccessType"" : 0,
                                ""RuleGroupType"" : 1,
                                ""Entitlement"" : null
                            }
                        ],
                        ""AbsenceReasons"" : [ 
                            {
                                ""_id"" : ""1519ee51-9288-4ba4-b7b7-7827aab34d60"",
                                ""ReasonCode"" : ""EHC"",
                                ""EntitlementType"" : 1,
                                ""Entitlement"" : 12.0000000000000000,
                                ""EliminationType"" : 8,
                                ""Elimination"" : 3.0000000000000000,
                                ""CombinedForSpouses"" : false,
                                ""PeriodType"" : 5,
                                ""Period"" : 12.0000000000000000,
                                ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000Z""),
                                ""CaseTypes"" : 7,
                                ""Paid"" : false,
                                ""PaymentTiers"" : [],
                                ""RuleGroups"" : [ 
                                    {
                                        ""_id"" : ""fef66178-3ed6-49de-b90c-cc142d9bf68e"",
                                        ""Name"" : ""Relationship to Employee"",
                                        ""Description"" : ""One of the following statements about \""Relationship to Employee\"" must be true:"",
                                        ""Rules"" : [ 
                                            {
                                                ""_id"" : ""424eedca-f4f0-4bec-ac08-5dd5e43ab0c5"",
                                                ""Name"" : ""SELF"",
                                                ""Description"" : ""is for the employee's SELF"",
                                                ""LeftExpression"" : ""CaseRelationshipType"",
                                                ""Operator"" : ""=="",
                                                ""RightExpression"" : ""'SELF'""
                                            }
                                        ],
                                        ""SuccessType"" : 1,
                                        ""RuleGroupType"" : 0,
                                        ""Entitlement"" : null
                                    }
                                ],
                                ""ResidenceState"" : null,
                                ""ShowType"" : 0,
                                ""PolicyEvents"" : [],
                                ""UseWholeDollarAmountsOnly"" : false
                            }, 
                            {
                                ""_id"" : ""d21bda10-b5ae-42a3-8eb2-640e9937a4c4"",
                                ""ReasonCode"" : ""FHC"",
                                ""EntitlementType"" : 1,
                                ""Entitlement"" : 12.0000000000000000,
                                ""EliminationType"" : 8,
                                ""Elimination"" : 3.0000000000000000,
                                ""CombinedForSpouses"" : true,
                                ""PeriodType"" : 5,
                                ""Period"" : 12.0000000000000000,
                                ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000Z""),
                                ""CaseTypes"" : 7,
                                ""Paid"" : false,
                                ""PaymentTiers"" : [],
                                ""RuleGroups"" : [ 
                                    {
                                        ""_id"" : ""cd12decb-7f09-4024-9c41-9c10bfa67d6d"",
                                        ""Name"" : ""Relationship to Employee"",
                                        ""Description"" : ""One of the following statements about \""Relationship to Employee\"" must be true:"",
                                        ""Rules"" : [ 
                                            {
                                                ""_id"" : ""72e54953-e3e7-452b-80c9-b98007c73e3d"",
                                                ""Name"" : ""SPOUSE"",
                                                ""Description"" : ""is for the employee's SPOUSE"",
                                                ""LeftExpression"" : ""CaseRelationshipType"",
                                                ""Operator"" : ""=="",
                                                ""RightExpression"" : ""'SPOUSE'""
                                            }, 
                                            {
                                                ""_id"" : ""17e55f80-7a9f-4258-a971-1513e5098a81"",
                                                ""Name"" : ""PARENT"",
                                                ""Description"" : ""is for the employee's PARENT"",
                                                ""LeftExpression"" : ""CaseRelationshipType"",
                                                ""Operator"" : ""=="",
                                                ""RightExpression"" : ""'PARENT'""
                                            }, 
                                            {
                                                ""_id"" : ""a4986827-4213-448a-a15b-afc6ddf09a9f"",
                                                ""Name"" : ""STEPPARENT"",
                                                ""Description"" : ""is for the employee's STEPPARENT"",
                                                ""LeftExpression"" : ""CaseRelationshipType"",
                                                ""Operator"" : ""=="",
                                                ""RightExpression"" : ""'STEPPARENT'""
                                            }, 
                                            {
                                                ""_id"" : ""bba62a79-6479-4815-856d-a87386adee78"",
                                                ""Name"" : ""FOSTERPARENT"",
                                                ""Description"" : ""is for the employee's FOSTERPARENT"",
                                                ""LeftExpression"" : ""CaseRelationshipType"",
                                                ""Operator"" : ""=="",
                                                ""RightExpression"" : ""'FOSTERPARENT'""
                                            }, 
                                            {
                                                ""_id"" : ""2f4d501b-c982-4f85-87cd-62a34af864fb"",
                                                ""Name"" : ""CHILD"",
                                                ""Description"" : ""is for the employee's CHILD"",
                                                ""LeftExpression"" : ""CaseRelationshipType"",
                                                ""Operator"" : ""=="",
                                               ""RightExpression"" : ""'CHILD'""
                                            }, 
                                            {
                                                ""_id"" : ""8488ef9b-1d77-4dd6-9e24-b4b3ff93bcd2"",
                                                ""Name"" : ""STEPCHILD"",
                                                ""Description"" : ""is for the employee's STEPCHILD"",
                                                ""LeftExpression"" : ""CaseRelationshipType"",
                                                ""Operator"" : ""=="",
                                                ""RightExpression"" : ""'STEPCHILD'""
                                            }, 
                                            {
                                                ""_id"" : ""eb12859e-9381-44a7-867c-a52617d61146"",
                                                ""Name"" : ""FOSTERCHILD"",
                                                ""Description"" : ""is for the employee's FOSTERCHILD"",
                                                ""LeftExpression"" : ""CaseRelationshipType"",
                                                ""Operator"" : ""=="",
                                                ""RightExpression"" : ""'FOSTERCHILD'""
                                            }, 
                                            {
                                                ""_id"" : ""836984d5-4b00-408b-8916-7d2a305a2a76"",
                                                ""Name"" : ""ADOPTEDCHILD"",
                                                ""Description"" : ""is for the employee's ADOPTEDCHILD"",
                                                ""LeftExpression"" : ""CaseRelationshipType"",
                                                ""Operator"" : ""=="",
                                                ""RightExpression"" : ""'ADOPTEDCHILD'""
                                            }, 
                                            {
                                                ""_id"" : ""7262a63f-70e2-4908-9944-95ed9c79d76b"",
                                                ""Name"" : ""LEGALWARD"",
                                                ""Description"" : ""is for the employee's LEGALWARD"",
                                                ""LeftExpression"" : ""CaseRelationshipType"",
                                                ""Operator"" : ""=="",
                                                ""RightExpression"" : ""'LEGALWARD'""
                                            }
                                        ],
                                        ""SuccessType"" : 1,
                                        ""RuleGroupType"" : 0,
                                        ""Entitlement"" : null
                                    }
                                ],
                                ""ResidenceState"" : null,
                                ""ShowType"" : 0,
                                ""PolicyEvents"" : [],
                                ""UseWholeDollarAmountsOnly"" : false
                            }, 
                            {
                                ""_id"" : ""bd6b8764-1b58-442a-af30-bc3a056e25d2"",
                                ""ReasonCode"" : ""PREGMAT"",
                                ""EntitlementType"" : 1,
                                ""Entitlement"" : 12.0000000000000000,
                                ""EliminationType"" : 8,
                                ""Elimination"" : 3.0000000000000000,
                                ""CombinedForSpouses"" : false,
                                ""PeriodType"" : 5,
                                ""Period"" : 12.0000000000000000,
                                ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000Z""),
                                ""CaseTypes"" : 7,
                                ""Paid"" : false,
                                ""PaymentTiers"" : [],
                                ""RuleGroups"" : [],
                                ""ResidenceState"" : null,
                                ""ShowType"" : 0,
                                ""PolicyEvents"" : [],
                                ""UseWholeDollarAmountsOnly"" : false
                            }, 
                            {
                                ""_id"" : ""36fb0b8e-1963-49d8-a411-4b3e15660f9d"",
                                ""ReasonCode"" : ""ADOPT"",
                                ""EntitlementType"" : 1,
                                ""Entitlement"" : 12.0000000000000000,
                                ""EliminationType"" : 8,
                                ""Elimination"" : 3.0000000000000000,
                                ""CombinedForSpouses"" : false,
                                ""PeriodType"" : 5,
                                ""Period"" : 12.0000000000000000,
                                ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000Z""),
                                ""CaseTypes"" : 7,
                                ""Paid"" : false,
                                ""PaymentTiers"" : [],
                                ""RuleGroups"" : [],
                                ""ResidenceState"" : null,
                                ""ShowType"" : 0,
                                ""PolicyEvents"" : [],
                                ""UseWholeDollarAmountsOnly"" : false
                            }, 
                            {
                                ""_id"" : ""327db47b-5710-4ac1-b1f9-52d2f8b3fec5"",
                                ""ReasonCode"" : ""MILITARY"",
                                ""EntitlementType"" : 1,
                                ""Entitlement"" : 26.0000000000000000,
                                ""EliminationType"" : 8,
                                ""Elimination"" : 3.0000000000000000,
                                ""CombinedForSpouses"" : true,
                                ""PeriodType"" : 5,
                                ""Period"" : 12.0000000000000000,
                                ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000Z""),
                                ""CaseTypes"" : 7,
                                ""Paid"" : false,
                                ""PaymentTiers"" : [],
                                ""RuleGroups"" : [ 
                                    {
                                        ""_id"" : ""0f286d46-288a-4fb0-a0f2-641e6ee7b443"",
                                        ""Name"" : ""Service Member's Relationship to Employee"",
                                        ""Description"" : ""One of the following statements about \""Service Member's Relationship to Employee\"" must be true:"",
                                        ""Rules"" : [ 
                                            {
                                                ""_id"" : ""a2e39e6d-9938-4b60-a900-ec11a47d374f"",
                                                ""Name"" : ""Not SELF"",
                                                ""Description"" : ""is not for the employee's SELF"",
                                                ""LeftExpression"" : ""CaseRelationshipType"",
                                                ""Operator"" : ""=="",
                                                ""RightExpression"" : ""'SELF'""
                                            }
                                        ],
                                        ""SuccessType"" : 2,
                                        ""RuleGroupType"" : 0,
                                        ""Entitlement"" : null
                                    }, 
                                    {
                                        ""_id"" : ""0e78518c-534b-4469-a09c-6007a3f155db"",
                                        ""Name"" : ""Service Member's Military Status"",
                                        ""Description"" : ""One of the following statements about \""Service Member's Military Status\"" must be true:"",
                                        ""Rules"" : [ 
                                            {
                                                ""_id"" : ""e90acb0f-f20a-4847-9af3-a2c33d19e5e6"",
                                                ""Name"" : ""Active Duty"",
                                                ""Description"" : ""Service member's military status is Active Duty"",
                                                ""LeftExpression"" : ""Case.Contact.MilitaryStatus"",
                                                ""Operator"" : ""=="",
                                                ""RightExpression"" : ""MilitaryStatus.ActiveDuty""
                                            }, 
                                            {
                                                ""_id"" : ""1d4d54e3-ec71-43b0-b9d1-42989b46d4c6"",
                                                ""Name"" : ""Veteran"",
                                                ""Description"" : ""Service member's military status is Veteran"",
                                                ""LeftExpression"" : ""Case.Contact.MilitaryStatus"",
                                                ""Operator"" : ""=="",
                                                ""RightExpression"" : ""MilitaryStatus.Veteran""
                                            }
                                        ],
                                        ""SuccessType"" : 1,
                                        ""RuleGroupType"" : 1,
                                        ""Entitlement"" : null
                                    }
                                ],
                                ""ResidenceState"" : null,
                                ""ShowType"" : 0,
                                ""PolicyEvents"" : [],
                                ""UseWholeDollarAmountsOnly"" : false
                            }, 
                            {
                                ""_id"" : ""1a6204dc-ff4f-45ee-bada-68341eb32bf6"",
                                ""ReasonCode"" : ""EXIGENCY"",
                                ""EntitlementType"" : 1,
                                ""Entitlement"" : 12.0000000000000000,
                                ""EliminationType"" : 8,
                                ""Elimination"" : 3.0000000000000000,
                                ""PerUseCap"" : 15.0000000000000000,
                               ""PerUseCapType"" : 8,
                                ""CombinedForSpouses"" : false,
                                ""PeriodType"" : 5,
                                ""Period"" : 12.0000000000000000,
                                ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000Z""),
                                ""CaseTypes"" : 7,
                                ""Paid"" : false,
                                ""PaymentTiers"" : [],
                                ""RuleGroups"" : [ 
                                    {
                                        ""_id"" : ""4345a935-fb8f-4bde-9898-272699089c2e"",
                                        ""Name"" : ""Service Member's Relationship to Employee"",
                                        ""Description"" : ""One of the following statements about \""Service Member's Relationship to Employee\"" must be true:"",
                                        ""Rules"" : [ 
                                            {
                                                ""_id"" : ""606ba696-947d-4764-8c16-4f295051684c"",
                                                ""Name"" : ""Not SELF"",
                                                ""Description"" : ""is not for the employee's SELF"",
                                                ""LeftExpression"" : ""CaseRelationshipType"",
                                                ""Operator"" : ""=="",
                                                ""RightExpression"" : ""'SELF'""
                                            }, 
                                            {
                                                ""_id"" : ""85e3acd0-727e-42e5-b8a9-4e491f82423b"",
                                                ""Name"" : ""Not NEXTOFKIN"",
                                                ""Description"" : ""is not for the employee's NEXTOFKIN"",
                                                ""LeftExpression"" : ""CaseRelationshipType"",
                                                ""Operator"" : ""=="",
                                                ""RightExpression"" : ""'NEXTOFKIN'""
                                            }
                                        ],
                                        ""SuccessType"" : 2,
                                        ""RuleGroupType"" : 0,
                                        ""Entitlement"" : null
                                    }, 
                                    {
                                        ""_id"" : ""4c30da05-f407-4a76-81e4-cb105e013aca"",
                                        ""Name"" : ""Service Member's Military Status"",
                                        ""Description"" : ""One of the following statements about \""Service Member's Military Status\"" must be true:"",
                                        ""Rules"" : [ 
                                            {
                                                ""_id"" : ""7bf42ae9-3e77-4527-aba4-71c323ba3aa6"",
                                                ""Name"" : ""Active Duty"",
                                                ""Description"" : ""Service member's military status is Active Duty"",
                                                ""LeftExpression"" : ""Case.Contact.MilitaryStatus"",
                                                ""Operator"" : ""=="",
                                                ""RightExpression"" : ""MilitaryStatus.ActiveDuty""
                                            }
                                        ],
                                        ""SuccessType"" : 1,
                                        ""RuleGroupType"" : 1,
                                        ""Entitlement"" : null
                                    }
                                ],
                                ""ResidenceState"" : null,
                                ""ShowType"" : 0,
                                ""PolicyEvents"" : [],
                                ""UseWholeDollarAmountsOnly"" : false
                            }
                        ],
                        ""WorkState"" : null,
                        ""ResidenceState"" : null,
                        ""Gender"" : null,
                        ""ConsecutiveTo"" : []
                    },
                    ""PolicyReason"" : {
                        ""_id"" : ""bd6b8764-1b58-442a-af30-bc3a056e25d2"",
                        ""ReasonCode"" : ""PREGMAT"",
                        ""EntitlementType"" : 1,
                        ""Entitlement"" : 12.0000000000000000,
                        ""EliminationType"" : 8,
                        ""Elimination"" : 3.0000000000000000,
                        ""CombinedForSpouses"" : false,
                        ""PeriodType"" : 5,
                        ""Period"" : 12.0000000000000000,
                        ""EffectiveDate"" : ISODate(""1970-01-01T00:00:00.000Z""),
                        ""CaseTypes"" : 7,
                        ""Paid"" : false,
                        ""PaymentTiers"" : [],
                        ""RuleGroups"" : [],
                        ""ResidenceState"" : null,
                        ""ShowType"" : 0,
                        ""PolicyEvents"" : [],
                        ""UseWholeDollarAmountsOnly"" : false
                    },
                    ""Status"" : 1,
                    ""StartDate"" : ISODate(""2015-03-20T00:00:00.000Z""),
                    ""EndDate"" : ISODate(""2015-05-25T00:00:00.000Z""),
                    ""Usage"" : [ 
                        {
                            ""_id"" : ""1b95ed10-a469-4e35-8977-adb2ae251435"",
                            ""DateUsed"" : ISODate(""2015-03-20T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""7886a621-7c81-4017-a5e4-241cf23d410c"",
                            ""DateUsed"" : ISODate(""2015-03-21T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""52f8fa81-36fc-448f-a2b3-48932de445ee"",
                            ""DateUsed"" : ISODate(""2015-03-22T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""5b996c27-e4fc-4c70-bdf8-8a54a844bf42"",
                            ""DateUsed"" : ISODate(""2015-03-23T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""a246b088-036b-4710-a6ce-4973128699f3"",
                            ""DateUsed"" : ISODate(""2015-03-24T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""2e6db12e-37c3-49c6-8212-9c097ecdc9f6"",
                            ""DateUsed"" : ISODate(""2015-03-25T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""48c57147-11fe-4192-bc52-6b718090b168"",
                            ""DateUsed"" : ISODate(""2015-03-26T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                   ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""dd4f4d7b-b7af-4941-90db-9a41d010313d"",
                            ""DateUsed"" : ISODate(""2015-03-27T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""94e7d89a-d5f6-41d5-ba98-17cc23ebc879"",
                            ""DateUsed"" : ISODate(""2015-03-28T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""81e8f16d-8f7f-4623-9520-e79ccc5a203f"",
                            ""DateUsed"" : ISODate(""2015-03-29T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""207bad06-6b51-4525-b7b2-e410d0b64561"",
                            ""DateUsed"" : ISODate(""2015-03-30T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""e376ce43-16c4-4296-829b-4f01297aac25"",
                            ""DateUsed"" : ISODate(""2015-03-31T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""b3f0d921-222a-4200-8e27-7fc0d9854511"",
                            ""DateUsed"" : ISODate(""2015-04-01T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""15bf67e5-c465-4fcf-879c-1283c4495d7e"",
                            ""DateUsed"" : ISODate(""2015-04-02T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""ee19659b-9072-4ec3-9dbf-4cd1eecc3309"",
                            ""DateUsed"" : ISODate(""2015-04-03T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""14d0a1cc-8c4a-438c-8c4b-ba50f01700fc"",
                            ""DateUsed"" : ISODate(""2015-04-04T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                   ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""2307206a-9f4b-496c-a4cf-700d61277c79"",
                            ""DateUsed"" : ISODate(""2015-04-05T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""f26ab166-02e5-422c-84de-3fafacf8752e"",
                            ""DateUsed"" : ISODate(""2015-04-06T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                   ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""65698769-9087-4f2f-8fd5-737adb21d591"",
                            ""DateUsed"" : ISODate(""2015-04-07T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""3d2b63ab-6cba-453d-9b85-07ae8a7a1b97"",
                            ""DateUsed"" : ISODate(""2015-04-08T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""484d6d99-58e6-4d7d-938c-cf329215fce4"",
                            ""DateUsed"" : ISODate(""2015-04-09T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""bccaf95a-c623-465f-939a-65901e3d0540"",
                            ""DateUsed"" : ISODate(""2015-04-10T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""ea3a7c8a-e90b-4a26-9587-ced01770e72b"",
                            ""DateUsed"" : ISODate(""2015-04-11T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""2d85a2f0-dd86-4c6b-b7fe-73a7e8e855fe"",
                            ""DateUsed"" : ISODate(""2015-04-12T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""bc92f69b-fcb0-4062-bd19-a1887b1d57a6"",
                            ""DateUsed"" : ISODate(""2015-04-13T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                   ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""0f9259d7-72aa-46dd-8066-d792bb58132b"",
                            ""DateUsed"" : ISODate(""2015-04-14T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""7704def4-112e-4151-a314-02d1984dc022"",
                            ""DateUsed"" : ISODate(""2015-04-15T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""dc2a6c93-7781-4bf9-a790-6d4da3cb58d7"",
                            ""DateUsed"" : ISODate(""2015-04-16T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""2a6e3330-74b1-4b8d-8679-656c22f357bd"",
                            ""DateUsed"" : ISODate(""2015-04-17T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""434c324e-c4c1-4da7-a58f-1be502c0d7d3"",
                            ""DateUsed"" : ISODate(""2015-04-18T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""c7a59e6d-e9d8-4006-aeb4-12dc4b88b419"",
                            ""DateUsed"" : ISODate(""2015-04-19T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""f98f63ba-935c-4e7e-8d3a-4d2cf7cf7624"",
                            ""DateUsed"" : ISODate(""2015-04-20T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""bbd38645-7dce-45f6-bd1f-dfab055ee8f4"",
                            ""DateUsed"" : ISODate(""2015-04-21T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                   ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""3e5043d1-9ee6-495a-ba2d-efa7acc4847c"",
                            ""DateUsed"" : ISODate(""2015-04-22T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""924949f5-9b37-44ce-8bcf-6a3a52c8be8c"",
                            ""DateUsed"" : ISODate(""2015-04-23T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""a39d2471-41be-4536-a325-7168082581b4"",
                            ""DateUsed"" : ISODate(""2015-04-24T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""fb8a18ad-9300-4c08-903e-ebaa9433b6ec"",
                            ""DateUsed"" : ISODate(""2015-04-25T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""5b81fefc-4e23-4b32-a12e-f9686b292219"",
                            ""DateUsed"" : ISODate(""2015-04-26T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""5090cf73-0c86-429e-b956-dc3adb045863"",
                            ""DateUsed"" : ISODate(""2015-04-27T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""8712824d-36da-4dd0-a495-b64f56899889"",
                            ""DateUsed"" : ISODate(""2015-04-28T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                   ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""1f013ed6-2f6c-4e87-af0d-e05926bac823"",
                            ""DateUsed"" : ISODate(""2015-04-29T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""87e60381-b9e9-4f47-be55-a4c45fbc7716"",
                            ""DateUsed"" : ISODate(""2015-04-30T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                   ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""3c953220-b04c-43f7-83e3-a73f22a8b5f5"",
                            ""DateUsed"" : ISODate(""2015-05-01T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""9614d788-febc-404b-b04b-4bc39c40d7a4"",
                            ""DateUsed"" : ISODate(""2015-05-02T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                   ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""1f683a47-f76f-4e69-b393-a6b177f8ed69"",
                            ""DateUsed"" : ISODate(""2015-05-03T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""abc16b35-6cb3-4e04-837d-705857697b72"",
                            ""DateUsed"" : ISODate(""2015-05-04T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""11bdfbcd-b292-4a8d-bbfc-d3479af6c326"",
                            ""DateUsed"" : ISODate(""2015-05-05T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""f89e5bd5-d747-4827-8ea1-6f74179f0b6b"",
                            ""DateUsed"" : ISODate(""2015-05-06T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""8159618f-73e2-42bd-861e-a69eae0686a3"",
                            ""DateUsed"" : ISODate(""2015-05-07T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""6812892b-5946-4e6a-ab90-65e9a93d7f21"",
                            ""DateUsed"" : ISODate(""2015-05-08T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""280d7105-753d-4dcd-ae9b-c890e583892e"",
                            ""DateUsed"" : ISODate(""2015-05-09T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                   ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""e44e1f82-f1cb-4fe1-a75c-d859848d1110"",
                            ""DateUsed"" : ISODate(""2015-05-10T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""b2d6a002-eb32-4c69-b7d5-536fbf529a86"",
                            ""DateUsed"" : ISODate(""2015-05-11T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""5ae0ead6-74d0-4746-9a38-1892356b87c8"",
                            ""DateUsed"" : ISODate(""2015-05-12T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""8293e4d8-7b10-4962-b543-41fd83647a75"",
                            ""DateUsed"" : ISODate(""2015-05-13T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""c52e6ff5-666e-4b21-ac62-0d4975e6541f"",
                            ""DateUsed"" : ISODate(""2015-05-14T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""3859fd27-afb4-4aed-a7fa-fc76ae1fc6b5"",
                            ""DateUsed"" : ISODate(""2015-05-15T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""de16f855-0579-4c8b-bd81-a233cc669555"",
                            ""DateUsed"" : ISODate(""2015-05-16T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""99d3d30b-47bf-4b32-b1d4-be1ce52bf283"",
                            ""DateUsed"" : ISODate(""2015-05-17T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                   ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""54fc1fbf-fe52-487b-a891-93ec7fd399dc"",
                            ""DateUsed"" : ISODate(""2015-05-18T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""6d9909c4-5ffe-450f-af59-ae210e599103"",
                            ""DateUsed"" : ISODate(""2015-05-19T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""e0361d80-77f0-4fdd-acae-45ae4968c85a"",
                            ""DateUsed"" : ISODate(""2015-05-20T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                   ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""cb7c6d18-4cb4-47b7-98fe-4ae268970138"",
                            ""DateUsed"" : ISODate(""2015-05-21T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""0a4eee5e-8fa4-46c2-9793-b41c1316894b"",
                            ""DateUsed"" : ISODate(""2015-05-22T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""a14cd943-bb32-4e9b-8bf5-a99d392b2fd2"",
                            ""DateUsed"" : ISODate(""2015-05-23T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""b5b65e31-5e62-4543-aa7c-8490bd5d086c"",
                            ""DateUsed"" : ISODate(""2015-05-24T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""6a0fc51e-17e7-4217-9728-13912e903cbf"",
                            ""DateUsed"" : ISODate(""2015-05-25T00:00:00.000Z""),
                            ""MinutesUsed"" : 0.0000000000000000,
                            ""MinutesInDay"" : 0.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""762430a7-bcbf-427a-acc5-79d24131abbd"",
                            ""DateUsed"" : ISODate(""2015-05-26T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""14c884da-6369-42f6-a24b-553590642d47"",
                            ""DateUsed"" : ISODate(""2015-05-27T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                   ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""100f6902-5889-425b-8ce6-7908d27d3e02"",
                            ""DateUsed"" : ISODate(""2015-05-28T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""de512e6d-a71e-4261-82ec-19ea1c683d81"",
                            ""DateUsed"" : ISODate(""2015-05-29T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                   ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""8f6af094-ade8-47af-9b31-6039b8b00020"",
                            ""DateUsed"" : ISODate(""2015-05-30T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""c4ecc57e-77db-47aa-85a7-04cbabc6dadb"",
                            ""DateUsed"" : ISODate(""2015-05-31T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                   ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 31.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""888181b3-8d60-4b39-ac45-7aba0c25cc5e"",
                            ""DateUsed"" : ISODate(""2015-06-01T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""8250df81-a361-4b31-820c-2c765313707b"",
                            ""DateUsed"" : ISODate(""2015-06-02T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""debc1db7-96d1-4dd1-9efa-314385d3544e"",
                            ""DateUsed"" : ISODate(""2015-06-03T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""46e34858-803d-4d32-a562-96b36332acfa"",
                            ""DateUsed"" : ISODate(""2015-06-04T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""a3412237-8e80-4eb6-9c2e-3a8203249daf"",
                            ""DateUsed"" : ISODate(""2015-06-05T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""2a38b8c0-e9c5-48dd-9d95-d2004727bc6a"",
                            ""DateUsed"" : ISODate(""2015-06-06T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""f07f9f85-97bc-4885-9abf-9e9684d6c92a"",
                            ""DateUsed"" : ISODate(""2015-06-07T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                   ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""086c5dbe-64be-4b85-b352-7b4a3bd003dd"",
                            ""DateUsed"" : ISODate(""2015-06-08T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""1905e99b-85b5-49d2-bd45-1a360516cc33"",
                            ""DateUsed"" : ISODate(""2015-06-09T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""5dc6d57e-7ff1-4be7-a914-a902dda2f6b5"",
                            ""DateUsed"" : ISODate(""2015-06-10T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""b863b324-012f-4acb-8782-c81ee37b9f75"",
                            ""DateUsed"" : ISODate(""2015-06-11T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""9b534921-9438-49a5-9af0-6ed7e17b4744"",
                            ""DateUsed"" : ISODate(""2015-06-12T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""b77b5543-205d-43aa-ab65-e6c82753a1cd"",
                            ""DateUsed"" : ISODate(""2015-06-13T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""b806731e-e0fd-4b24-b8e7-d0633e49913a"",
                            ""DateUsed"" : ISODate(""2015-06-14T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""e0713c4a-dc1d-4eb2-bb6f-0410aee80436"",
                            ""DateUsed"" : ISODate(""2015-06-15T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                   ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""51a2ac47-30c0-4f06-bf11-afee365e7fa1"",
                            ""DateUsed"" : ISODate(""2015-06-16T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""9ebec546-2a3b-485c-8403-646b91e17969"",
                            ""DateUsed"" : ISODate(""2015-06-17T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""fe5eb9ea-cc92-4ca2-ae00-a0754274db26"",
                            ""DateUsed"" : ISODate(""2015-06-18T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                   ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""2c241065-7265-4b19-a8ee-b58060074955"",
                            ""DateUsed"" : ISODate(""2015-06-19T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""9eaa9a9f-d913-4f30-b231-d00c43d556bf"",
                            ""DateUsed"" : ISODate(""2015-06-20T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""0f605e52-4ce3-4bbe-87b8-3ce8cd2de95e"",
                            ""DateUsed"" : ISODate(""2015-06-21T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""0934bcd1-ea4b-4512-aa67-857736d9aa50"",
                            ""DateUsed"" : ISODate(""2015-06-22T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""2ba96b85-666a-46db-af64-f35c05f6943b"",
                            ""DateUsed"" : ISODate(""2015-06-23T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""ac1bb216-45d2-4ad5-b525-7fbb7ff9676f"",
                            ""DateUsed"" : ISODate(""2015-06-24T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""02680824-5033-4b36-8928-98f0b3827e70"",
                            ""DateUsed"" : ISODate(""2015-06-25T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                   ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""100facd6-f82b-4e01-b240-fa9be36bd81a"",
                            ""DateUsed"" : ISODate(""2015-06-26T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""8c2eaeba-1a63-40c1-af18-573d3617a85e"",
                            ""DateUsed"" : ISODate(""2015-06-27T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                   ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""e0970018-0a75-424b-951e-23987b6c1d60"",
                            ""DateUsed"" : ISODate(""2015-06-28T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 6.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }, 
                        {
                            ""_id"" : ""8b6bd1bb-284c-49a6-8379-a177d3771af3"",
                            ""DateUsed"" : ISODate(""2015-06-29T00:00:00.000Z""),
                            ""MinutesUsed"" : 480.0000000000000000,
                            ""MinutesInDay"" : 480.0000000000000000,
                            ""PolicyReasonCode"" : ""PREGMAT"",
                            ""UserEntered"" : false,
                            ""Determination"" : 0,
                            ""IntermittentDetermination"" : 0,
                            ""AvailableToUse"" : [ 
                                {
                                    ""UnitType"" : 2,
                                    ""BaseUnit"" : 0.1250000000000000
                                }, 
                                {
                                    ""UnitType"" : 8,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 4,
                                   ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 9,
                                    ""BaseUnit"" : 1.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 1,
                                    ""BaseUnit"" : 6.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 3,
                                    ""BaseUnit"" : 7.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 7,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 5,
                                    ""BaseUnit"" : 30.0000000000000000
                                }, 
                                {
                                    ""UnitType"" : 6,
                                    ""BaseUnit"" : 365.0000000000000000
                                }
                            ]
                        }
                    ],
                    ""RuleGroups"" : [ 
                        {
                            ""_id"" : ""5c31cab5-f1aa-42d0-b47b-25dc1dba0a84"",
                            ""RuleGroup"" : {
                                ""_id"" : ""4e8a9dde-48da-4beb-9ebc-547586f223cf"",
                                ""Name"" : ""FMLA Eligibility"",
                                ""Description"" : ""All of the following statements about the Employee must be true:"",
                                ""Rules"" : [ 
                                    {
                                        ""_id"" : ""0e524282-b341-4a2b-b524-62f4c538739e"",
                                        ""Name"" : ""1,250 Hours Worked"",
                                        ""Description"" : ""The Employee has worked at least 1,250 hours in the last 12 months"",
                                        ""LeftExpression"" : ""TotalHoursWorkedLast12Months()"",
                                        ""Operator"" : "">="",
                                        ""RightExpression"" : ""1250""
                                    }, 
                                    {
                                        ""_id"" : ""29034cc4-5166-4c5a-8cad-35ee7cfd95e7"",
                                        ""Name"" : ""50 Employees in 75 Mile Radius Rule"",
                                        ""Description"" : ""The Employee works in an office with at least 50 employees that all work within a 75 mile radius"",
                                        ""LeftExpression"" : ""Meets50In75MileRule()"",
                                        ""Operator"" : ""=="",
                                        ""RightExpression"" : ""true""
                                    }, 
                                    {
                                        ""_id"" : ""6ab936f9-b7ad-4530-9fd9-ae27185997b1"",
                                        ""Name"" : ""Worked 12 Months"",
                                        ""Description"" : ""The Employee has worked at least 12 months"",
                                        ""LeftExpression"" : ""Has1YearOfService()"",
                                        ""Operator"" : ""=="",
                                        ""RightExpression"" : ""true""
                                    }, 
                                    {
                                        ""_id"" : ""73171423-8897-42da-8e80-1cbe9f6374b0"",
                                        ""Name"" : ""Key Employees Ineligible"",
                                        ""Description"" : ""Employee may not be a key employee"",
                                        ""LeftExpression"" : ""Employee.IsKeyEmployee"",
                                        ""Operator"" : ""=="",
                                        ""RightExpression"" : ""false""
                                    }
                                ],
                                ""SuccessType"" : 0,
                                ""RuleGroupType"" : 1,
                                ""Entitlement"" : null
                            },
                            ""Rules"" : [ 
                                {
                                    ""_id"" : ""c1d92560-2a9d-46ae-b4a1-f1d40fa7ef52"",
                                    ""Rule"" : {
                                        ""_id"" : ""0e524282-b341-4a2b-b524-62f4c538739e"",
                                        ""Name"" : ""1,250 Hours Worked"",
                                        ""Description"" : ""The Employee has worked at least 1,250 hours in the last 12 months"",
                                        ""LeftExpression"" : ""TotalHoursWorkedLast12Months()"",
                                        ""Operator"" : "">="",
                                        ""RightExpression"" : ""1250""
                                    },
                                    ""Pass"" : true,
                                    ""Fail"" : false,
                                    ""Result"" : 1,
                                    ""ActualValue"" : 1858.0000000000000000,
                                    ""Overridden"" : false,
                                    ""OverrideFromResult"" : 0,
                                    ""Messages"" : []
                                }, 
                                {
                                    ""_id"" : ""3b3b024e-5b76-4aa0-a85f-c0082ee40091"",
                                    ""Rule"" : {
                                        ""_id"" : ""29034cc4-5166-4c5a-8cad-35ee7cfd95e7"",
                                        ""Name"" : ""50 Employees in 75 Mile Radius Rule"",
                                        ""Description"" : ""The Employee works in an office with at least 50 employees that all work within a 75 mile radius"",
                                        ""LeftExpression"" : ""Meets50In75MileRule()"",
                                        ""Operator"" : ""=="",
                                        ""RightExpression"" : ""true""
                                    },
                                    ""Pass"" : true,
                                    ""Fail"" : false,
                                    ""Result"" : 1,
                                    ""ActualValue"" : true,
                                    ""Overridden"" : false,
                                    ""OverrideFromResult"" : 0,
                                    ""Messages"" : []
                                }, 
                                {
                                    ""_id"" : ""01251b2a-4be0-44e1-8c4a-8a90dba92c55"",
                                    ""Rule"" : {
                                        ""_id"" : ""6ab936f9-b7ad-4530-9fd9-ae27185997b1"",
                                        ""Name"" : ""Worked 12 Months"",
                                        ""Description"" : ""The Employee has worked at least 12 months"",
                                        ""LeftExpression"" : ""Has1YearOfService()"",
                                        ""Operator"" : ""=="",
                                        ""RightExpression"" : ""true""
                                    },
                                    ""Pass"" : true,
                                    ""Fail"" : false,
                                    ""Result"" : 1,
                                    ""ActualValue"" : true,
                                    ""Overridden"" : false,
                                    ""OverrideFromResult"" : 0,
                                    ""Messages"" : []
                                }, 
                                {
                                    ""_id"" : ""a8c45711-f1c9-430d-9647-07f684fdac9b"",
                                    ""Rule"" : {
                                        ""_id"" : ""73171423-8897-42da-8e80-1cbe9f6374b0"",
                                        ""Name"" : ""Key Employees Ineligible"",
                                        ""Description"" : ""Employee may not be a key employee"",
                                        ""LeftExpression"" : ""Employee.IsKeyEmployee"",
                                        ""Operator"" : ""=="",
                                        ""RightExpression"" : ""false""
                                    },
                                    ""Pass"" : true,
                                    ""Fail"" : false,
                                    ""Result"" : 1,
                                    ""ActualValue"" : false,
                                    ""Overridden"" : false,
                                    ""OverrideFromResult"" : 0,
                                    ""Messages"" : []
                                }
                            ],
                            ""Pass"" : true,
                            ""EvalDate"" : ISODate(""2015-03-10T16:23:03.936Z"")
                        }
                    ],
                    ""Selection"" : [],
                    ""ManuallyAdded"" : false
                }
            ],
            ""Status"" : 0,
            ""LeaveSchedule"" : [],
            ""Absences"" : [],
            ""UserRequests"" : [],
            ""CreatedDate"" : ISODate(""2015-03-10T16:23:03.843Z"")
        }
    ],
    ""SpouseCaseId"" : null,
    ""CaseEvents"" : [ 
        {
            ""_id"" : ""28484304-c1b0-400f-a495-5a99a778cb01"",
            ""EventType"" : 14,
            ""AllEventDates"" : [ 
                ISODate(""2015-06-10T00:00:00.000Z""), 
                ISODate(""2015-08-05T00:00:00.000Z""), 
                ISODate(""2015-05-27T00:00:00.000Z""), 
                ISODate(""2015-06-24T00:00:00.000Z""), 
                ISODate(""2015-09-14T00:00:00.000Z""), 
                ISODate(""2015-06-30T00:00:00.000Z""), 
                ISODate(""2015-05-26T00:00:00.000Z""), 
                ISODate(""2015-07-22T00:00:00.000Z"")
            ]
        }, 
        {
            ""_id"" : ""15e469ac-866d-4e1a-a3eb-b6d66d7becc7"",
            ""EventType"" : 0,
            ""AllEventDates"" : [ 
                ISODate(""2015-03-10T00:00:00.000Z"")
            ]
        }, 
        {
            ""_id"" : ""1bdbe667-ce92-4b4c-93ce-961f2068cb0a"",
            ""EventType"" : 9,
            ""AllEventDates"" : [ 
                ISODate(""2015-03-18T00:00:00.000Z""), 
                ISODate(""2015-03-25T00:00:00.000Z""), 
                ISODate(""2015-04-10T00:00:00.000Z""), 
                ISODate(""2015-04-10T00:00:00.000Z""), 
                ISODate(""2015-04-10T00:00:00.000Z""), 
                ISODate(""2015-04-14T00:00:00.000Z""), 
                ISODate(""2015-07-16T00:00:00.000Z""), 
                ISODate(""2015-07-16T00:00:00.000Z""), 
                ISODate(""2015-07-16T00:00:00.000Z""), 
                ISODate(""2015-07-16T00:00:00.000Z""), 
                ISODate(""2015-07-16T00:00:00.000Z""), 
                ISODate(""2015-07-16T00:00:00.000Z""), 
                ISODate(""2015-07-16T00:00:00.000Z""), 
                ISODate(""2015-07-16T00:00:00.000Z""), 
                ISODate(""2015-07-16T00:00:00.000Z""), 
                ISODate(""2015-07-16T00:00:00.000Z""), 
                ISODate(""2015-07-16T00:00:00.000Z""), 
                ISODate(""2015-07-16T00:00:00.000Z"")
            ]
        }
    ],
    ""Employee"" : {
        ""_id"" : ObjectId(""54734a23a32a9f0d00081eac""),
        ""cdt"" : ISODate(""2014-11-24T15:09:23.764Z""),
        ""cby"" : ObjectId(""000000000000000000000000""),
        ""mdt"" : ISODate(""2015-03-10T11:12:35.643Z""),
        ""mby"" : ObjectId(""000000000000000000000000""),
        ""CustomerId"" : ObjectId(""000000000000000000000002""),
        ""EmployerId"" : ObjectId(""000000000000000000000002""),
        ""EmployeeNumber"" : ""102133"",
        ""Info"" : {
            ""_id"" : ""42ba8537-da95-42a6-8573-412c1613ae22"",
            ""HomeEmail"" : null,
            ""Email"" : ""sadfasdfasdf@asdfasdfasdf.com"",
            ""AltEmail"" : ""asdfasdf.asdfasdfsdf@asdfasdfsadfasdf.com"",
            ""Address"" : {
                ""_id"" : ""5855d588-4dc6-4443-a7bb-ba9461ecb446"",
                ""Address1"" : ""2699 asdfsadfasdfsdaf rd."",
                ""City"" : ""asdfasdfasdffds"",
                ""State"" : ""MI"",
                ""PostalCode"" : ""65498"",
                ""Country"" : ""US""
            },
            ""AltAddress"" : {
                ""_id"" : ""4b6ab924-adf2-4a8a-b195-5b4f6c0bb03c"",
                ""Address1"" : null,
                ""City"" : null,
                ""State"" : null,
                ""PostalCode"" : null,
                ""Country"" : ""US""
            },
            ""WorkPhone"" : ""6166691641"",
            ""HomePhone"" : ""6165507943"",
            ""CellPhone"" : ""6165507943"",
            ""OfficeLocation"" : ""That one place""
        },
        ""Job"" : {
            ""_id"" : ""81fbe2c9-b5ef-428e-a413-e7670d54c732"",
            ""JobFunctions"" : []
       },
        ""FirstName"" : ""LastOf"",
        ""LastName"" : ""TheMohiccans"",
        ""Gender"" : 70,
        ""DoB"" : ISODate(""1982-06-05T00:00:00.000Z""),
        ""Ssn"" : null,
        ""Status"" : 65,
        ""Salary"" : 11.3500000000000000,
        ""PayType"" : 2,
        ""WorkType"" : 1,
        ""JobTitle"" : ""Office Coordinator"",
        ""PriorHours"" : [],
        ""ServiceDate"" : ISODate(""2012-11-14T00:00:00.000Z""),
        ""HireDate"" : ISODate(""2012-11-14T00:00:00.000Z""),
        ""RehireDate"" : ISODate(""2012-11-14T00:00:00.000Z""),
        ""WorkState"" : ""MI"",
        ""WorkCountry"" : ""US"",
        ""WorkSchedules"" : [ 
            {
                ""_id"" : ""72141cd6-81e2-4d67-958f-2a164be55ca8"",
                ""ScheduleType"" : 2,
                ""StartDate"" : ISODate(""2014-01-09T00:00:00.000Z""),
                ""EndDate"" : null,
                ""Times"" : []
            }
        ],
        ""Meets50In75MileRule"" : true,
        ""IsKeyEmployee"" : false,
        ""IsExempt"" : false,
        ""MilitaryStatus"" : 0,
        ""CustomFields"" : [ 
            {
                ""_id"" : ObjectId(""54aadefba32aa00688264757""),
                ""cdt"" : ISODate(""2015-01-05T18:59:07.592Z""),
                ""cby"" : ObjectId(""541c5801a32aa00e20b3252e""),
                ""mdt"" : ISODate(""2015-01-05T18:59:07.592Z""),
                ""mby"" : ObjectId(""541c5801a32aa00e20b3252e""),
                ""CustomerId"" : ObjectId(""000000000000000000000002""),
                ""EmployerId"" : ObjectId(""000000000000000000000002""),
                ""Target"" : 1,
                ""Name"" : ""Sick"",
                ""Code"" : ""SICK"",
                ""Description"" : ""Sick"",
                ""Label"" : ""Sick"",
                ""HelpText"" : ""Current Sick Time Balance"",
                ""DataType"" : 1,
                ""ValueType"" : 1,
                ""ListValues"" : [],
                ""SelectedValue"" : null,
                ""FileOrder"" : 4,
                ""IsRequired"" : false,
                ""IsCollectedAtIntake"" : true
            }, 
            {
                ""_id"" : ObjectId(""54a71ee1a32aa00e18206029""),
                ""cdt"" : ISODate(""2015-01-02T22:42:41.000Z""),
                ""cby"" : ObjectId(""547330ada32aa00f54e2ce01""),
                ""mdt"" : ISODate(""2015-01-05T18:59:16.859Z""),
                ""mby"" : ObjectId(""541c5801a32aa00e20b3252e""),
                ""CustomerId"" : ObjectId(""000000000000000000000002""),
                ""EmployerId"" : ObjectId(""000000000000000000000002""),
                ""Target"" : 1,
                ""Name"" : ""PTO"",
                ""Code"" : ""PTO"",
                ""Description"" : ""PTO"",
                ""Label"" : ""PTO"",
                ""HelpText"" : ""Current Paid Time Off Balance"",
                ""DataType"" : 1,
                ""ValueType"" : 1,
                ""ListValues"" : [],
                ""SelectedValue"" : ""24.00"",
                ""FileOrder"" : 3,
                ""IsRequired"" : false,
                ""IsCollectedAtIntake"" : true
            }
        ],
        ""EmployerName"" : ""Some Guy Hires People, Inc.""
    },
    ""Reason"" : {
        ""_id"" : ObjectId(""000000000000000001000002""),
        ""cdt"" : ISODate(""2014-07-09T21:34:36.592Z""),
        ""cby"" : ObjectId(""000000000000000000000000""),
        ""mdt"" : ISODate(""2015-03-10T02:59:37.783Z""),
        ""mby"" : ObjectId(""000000000000000000000000""),
        ""Code"" : ""PREGMAT"",
        ""Name"" : ""Pregnancy/Maternity"",
        ""Flags"" : 1
    },
    ""Status"" : 0,
    ""CancelReason"" : 0,
    ""AssignedToId"" : ObjectId(""54ac4f12a32aa006d47413d3""),
    ""AssignedToName"" : ""Some Person"",
    ""EmployerName"" : ""Blah Blah Blah"",
    ""ClosureReason"" : null,
    ""Description"" : ""Pregnancy"",
    ""Narrative"" : ""asdfasdfasfasf asdf asdfasd fsdf"",
    ""Disability"" : {
        ""_id"" : ""dcab0bbc-48da-4407-af1d-fd13e896e724"",
        ""PrimaryDiagnosis"" : null,
        ""PrimaryPathId"" : null,
        ""PrimaryPathText"" : null,
        ""PrimaryPathMinDays"" : null,
        ""PrimaryPathMaxDays"" : null,
        ""PrimaryPathDaysUIText"" : null,
        ""SecondaryDiagnosis"" : null,
        ""MedicalComplications"" : false,
        ""Hospitalization"" : false,
        ""GeneralHealthCondition"" : null,
        ""ConditionStartDate"" : null
    },
    ""Certifications"" : [],
    ""Summary"" : {
        ""_id"" : ""5c26b192-d394-4016-ab94-4ba2553df164"",
        ""g"" : 1,
        ""a"" : 0,
        ""t"" : 2,
        ""p"" : [ 
            {
                ""_id"" : ""7659c9cb-b67a-439f-aa75-5d4925accd59"",
                ""p"" : ""53bdb56da32a9e0f8076f392"",
                ""c"" : ""FMLA"",
                ""n"" : ""Family Medical Leave Act"",
                ""d"" : [ 
                    {
                        ""_id"" : ""4bde28a0-eee0-4cf7-ae18-f7a7c6bd1e68"",
                        ""s"" : ISODate(""2015-03-20T00:00:00.000Z""),
                        ""e"" : ISODate(""2015-06-29T00:00:00.000Z""),
                        ""t"" : 2,
                        ""g"" : 0,
                        ""a"" : 0
                    }, 
                    {
                        ""_id"" : ""5b7714d2-2256-4d24-a5b8-cb68a56a1116"",
                        ""s"" : ISODate(""2015-06-30T00:00:00.000Z""),
                        ""e"" : ISODate(""2015-05-25T00:00:00.000Z""),
                        ""t"" : 2,
                        ""g"" : 0,
                        ""a"" : 0
                    }
                ],
                ""m"" : []
            }
        ],
        ""r"" : false,
        ""e"" : false,
        ""f"" : false,
        ""l"" : false,
        ""u"" : ""0 Weeks"",
        ""x"" : null,
        ""z"" : null,
        ""m"" : null,
        ""MinApprovedThruDate"" : null,
        ""MinDuration"" : null,
        ""MinDurationString"" : null
    },
    ""Pay"" : {
        ""_id"" : ""e7e53cd1-95e6-41c6-bf4f-3628e999b01a"",
        ""PayPeriods"" : [],
        ""ApplyOffsetsByDefault"" : false,
        ""WaiveWaitingPeriod"" : {
            ""_id"" : ""f5dd253b-7de6-4d96-a59c-b67ad323b1f4""
        },
        ""PayScheduleId"" : null
    },
    ""IsAccommodation"" : false,
    ""IsInformationOnly"" : false,
    ""WorkRestrictions"" : [],
    ""CustomFields"" : []
}
");

            BsonDocument employeeBson = BsonDocument.Parse(@"{
    ""_id"" : ObjectId(""54734a23a32a9f0d00081eac""),
    ""LastERow"" : ""3ZHUxwGZ5BAz+9eBDdO6cA=="",
    ""LastCRow"" : ""QhuPhiVNOGhqL+X8ygNoeA=="",
    ""cdt"" : ISODate(""2014-11-24T15:09:23.764Z""),
    ""cby"" : ObjectId(""000000000000000000000000""),
    ""mdt"" : ISODate(""2015-07-22T12:10:36.205Z""),
    ""mby"" : ObjectId(""541c5801a32aa00e20b3252e""),
    ""CustomerId"" : ObjectId(""000000000000000000000002""),
    ""EmployerId"" : ObjectId(""000000000000000000000002""),
    ""EmployeeNumber"" : ""102133"",
    ""Info"" : {
        ""_id"" : ""42ba8537-da95-42a6-8573-412c1613ae22"",
        ""HomeEmail"" : null,
        ""Email"" : ""sdfgsdfgdfsg@sdfgsdfgsdfgsdfgsdfg.com"",
        ""AltEmail"" : ""sdfgdfgdsf.sdgsdfgdsfgsdfg@sdfgsdfgdsfgdfsg.com"",
        ""Address"" : {
            ""_id"" : ""5855d588-4dc6-4443-a7bb-ba9461ecb446"",
            ""Address1"" : ""1234 sdfgsdfgsdfgdsfg rd."",
            ""City"" : ""Somehweresville"",
            ""State"" : ""MI"",
            ""PostalCode"" : ""65498"",
            ""Country"" : ""US""
        },
        ""AltAddress"" : {
            ""_id"" : ""4b6ab924-adf2-4a8a-b195-5b4f6c0bb03c"",
            ""Address1"" : null,
            ""City"" : null,
            ""State"" : null,
            ""PostalCode"" : null,
            ""Country"" : ""US""
        },
        ""WorkPhone"" : ""5552225151"",
        ""HomePhone"" : ""5552225151"",
        ""CellPhone"" : ""5552225151"",
        ""OfficeLocation"" : ""Presidential Overtures""
    },
    ""Job"" : {
        ""_id"" : ""81fbe2c9-b5ef-428e-a413-e7670d54c732"",
        ""JobFunctions"" : []
    },
    ""FirstName"" : ""Donna"",
    ""LastName"" : ""Summers"",
    ""Gender"" : 70,
    ""DoB"" : ISODate(""1987-07-11T00:00:00.000Z""),
    ""Ssn"" : null,
    ""Status"" : 73,
    ""Salary"" : 11.35,
    ""PayType"" : 2,
    ""WorkType"" : 1,
    ""JobTitle"" : ""Office Coordinator"",
    ""PriorHours"" : [],
    ""ServiceDate"" : ISODate(""2012-11-14T00:00:00.000Z""),
    ""HireDate"" : ISODate(""2012-11-14T00:00:00.000Z""),
    ""RehireDate"" : ISODate(""2012-11-14T00:00:00.000Z""),
    ""WorkState"" : ""MI"",
    ""WorkCountry"" : ""US"",
    ""WorkSchedules"" : [ 
        {
            ""_id"" : ""04cd1ee6-056f-4826-b5b7-6083966e2772"",
            ""ScheduleType"" : 0,
            ""StartDate"" : ISODate(""2014-01-09T00:00:00.000Z""),
            ""EndDate"" : ISODate(""2015-06-29T00:00:00.000Z""),
            ""Times"" : [ 
                {
                    ""_id"" : ""15e68ee1-b379-4f6a-9873-6c38e788ff57"",
                    ""TotalMinutes"" : 480,
                    ""SampleDate"" : ISODate(""2014-01-05T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""12cdfe97-6142-4812-bee1-8dfed8ec96f5"",
                    ""TotalMinutes"" : 480,
                    ""SampleDate"" : ISODate(""2014-01-06T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""ca087a03-d396-4389-ad6c-c92a646e7e5c"",
                    ""TotalMinutes"" : 480,
                    ""SampleDate"" : ISODate(""2014-01-07T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""6696786e-49e8-4651-9308-0d285705fc96"",
                    ""TotalMinutes"" : 480,
                    ""SampleDate"" : ISODate(""2014-01-08T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""5ac9bab0-ba11-45c8-813d-4c92d86f8239"",
                    ""TotalMinutes"" : 480,
                    ""SampleDate"" : ISODate(""2014-01-09T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""4705d02d-3849-47c4-ba06-685e44eaa919"",
                    ""TotalMinutes"" : 480,
                    ""SampleDate"" : ISODate(""2014-01-10T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""947dad4f-4977-4aae-ad04-694ebc24e0de"",
                    ""TotalMinutes"" : 480,
                    ""SampleDate"" : ISODate(""2014-01-11T00:00:00.000Z"")
                }
            ]
        }, 
        {
            ""_id"" : ""c8c2fea9-856d-4593-8220-c193c4dc2025"",
            ""ScheduleType"" : 0,
            ""StartDate"" : ISODate(""2015-06-30T00:00:00.000Z""),
            ""EndDate"" : ISODate(""2015-07-21T00:00:00.000Z""),
            ""Times"" : [ 
                {
                    ""_id"" : ""25ff7c22-5747-436c-a5e4-33afdb50134e"",
                    ""TotalMinutes"" : 0,
                    ""SampleDate"" : ISODate(""2015-06-28T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""868c624d-347a-47ae-a6e9-ae0422a9cfbb"",
                    ""TotalMinutes"" : 480,
                    ""SampleDate"" : ISODate(""2015-06-29T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""b585f812-76d6-4012-a438-db668c250a1c"",
                    ""TotalMinutes"" : 480,
                    ""SampleDate"" : ISODate(""2015-06-30T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""1317e835-ff21-4967-b470-5568dd57f66e"",
                    ""TotalMinutes"" : 480,
                    ""SampleDate"" : ISODate(""2015-07-01T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""43db6b31-c54c-4ed8-841b-0183903caf28"",
                    ""TotalMinutes"" : 480,
                    ""SampleDate"" : ISODate(""2015-07-02T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""e9926e53-7ebf-49d3-9a55-aade6841b9f6"",
                    ""TotalMinutes"" : 480,
                    ""SampleDate"" : ISODate(""2015-07-03T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""4b99bd42-7539-4896-bb93-452f0ecf4503"",
                    ""TotalMinutes"" : 0,
                    ""SampleDate"" : ISODate(""2015-07-04T00:00:00.000Z"")
                }
            ]
        }, 
        {
            ""_id"" : ""3a0df95f-4c72-403e-a10b-2c73644f37e4"",
            ""ScheduleType"" : 2,
            ""StartDate"" : ISODate(""2015-07-22T00:00:00.000Z""),
            ""EndDate"" : null,
            ""Times"" : [ 
                {
                    ""_id"" : ""e1b27da8-1489-4d6a-adc4-4e7611fe8c3c"",
                    ""TotalMinutes"" : null,
                    ""SampleDate"" : ISODate(""2015-07-19T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""2c079f50-b23b-491e-b7d4-8c4a3544e515"",
                    ""TotalMinutes"" : 336,
                    ""SampleDate"" : ISODate(""2015-07-20T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""8bfa7a56-ad8f-450a-879a-5e440b0b8a4e"",
                    ""TotalMinutes"" : 336,
                    ""SampleDate"" : ISODate(""2015-07-21T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""ea0d350b-df8d-45b1-b592-4cf06d95db8e"",
                    ""TotalMinutes"" : 336,
                    ""SampleDate"" : ISODate(""2015-07-22T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""c5df48ad-1d00-45d7-ac88-bb6a80aa0e1a"",
                    ""TotalMinutes"" : 336,
                    ""SampleDate"" : ISODate(""2015-07-23T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""49f83497-c175-4ab3-b145-2396f35502d3"",
                    ""TotalMinutes"" : 336,
                    ""SampleDate"" : ISODate(""2015-07-24T00:00:00.000Z"")
                }, 
                {
                    ""_id"" : ""53249da5-24a7-42db-8e3c-bae9b5dc8d59"",
                    ""TotalMinutes"" : null,
                    ""SampleDate"" : ISODate(""2015-07-25T00:00:00.000Z"")
                }
            ]
        }
    ],
    ""Meets50In75MileRule"" : true,
    ""IsKeyEmployee"" : false,
    ""IsExempt"" : false,
    ""SpouseEmployeeNumber"" : """",
    ""MilitaryStatus"" : 0,
    ""CustomFields"" : [ 
        {
            ""_id"" : ObjectId(""54a71ee1a32aa00e18206029""),
            ""cdt"" : ISODate(""2015-01-02T22:42:41.000Z""),
            ""cby"" : ObjectId(""547330ada32aa00f54e2ce01""),
            ""mdt"" : ISODate(""2015-02-19T23:17:31.972Z""),
            ""mby"" : ObjectId(""541c5801a32aa00e20b3252e""),
            ""CustomerId"" : ObjectId(""000000000000000000000002""),
            ""EmployerId"" : ObjectId(""000000000000000000000002""),
            ""Target"" : 1,
            ""Code"" : ""PTO"",
            ""Name"" : ""PTO"",
            ""Description"" : ""PTO"",
            ""Label"" : ""PTO"",
            ""HelpText"" : ""Current Paid Time Off Balance"",
            ""DataType"" : 1,
            ""ValueType"" : 1,
            ""ListValues"" : [],
            ""SelectedValue"" : null,
            ""FileOrder"" : 3,
            ""IsRequired"" : false,
            ""IsCollectedAtIntake"" : true
        }, 
        {
            ""_id"" : ObjectId(""54aadefba32aa00688264757""),
            ""cdt"" : ISODate(""2015-01-05T18:59:07.592Z""),
            ""cby"" : ObjectId(""541c5801a32aa00e20b3252e""),
            ""mdt"" : ISODate(""2015-02-19T23:17:37.354Z""),
            ""mby"" : ObjectId(""541c5801a32aa00e20b3252e""),
            ""CustomerId"" : ObjectId(""000000000000000000000002""),
            ""EmployerId"" : ObjectId(""000000000000000000000002""),
            ""Target"" : 1,
            ""Code"" : ""SICK"",
            ""Name"" : ""Sick"",
            ""Description"" : ""Sick"",
            ""Label"" : ""Sick"",
            ""HelpText"" : ""Current Sick Time Balance"",
            ""DataType"" : 1,
            ""ValueType"" : 1,
            ""ListValues"" : [],
            ""SelectedValue"" : null,
            ""FileOrder"" : 4,
            ""IsRequired"" : false,
            ""IsCollectedAtIntake"" : true
        }
    ],
    ""EmployerName"" : ""Dude, where's my car?""
}");

            #endregion

            Case testCase = null;
            Employee emp = null;

            testCase = BsonSerializer.Deserialize<Case>(caseBson);
            emp = BsonSerializer.Deserialize<Employee>(employeeBson);

            testCase.Employee = emp;
            emp.Save();

            using (CaseService svc = new CaseService())
                testCase = svc.ChangeCase(testCase, testCase.StartDate, testCase.EndDate.Value, false, false, CaseType.Intermittent, false, false);

            foreach (var policy in testCase.Summary.Policies)
                foreach (var detail in policy.Detail)
                    Assert.IsTrue(detail.StartDate < detail.EndDate.Value,
                        string.Format("The policy '{0}' has a start date of '{1:MM/dd/yyyy}' which falls after the end date of '{2:MM/dd/yyyy}'.", 
                        policy.PolicyName, detail.StartDate, detail.EndDate.Value));

            Assert.IsTrue(true);
        }
    }
}
