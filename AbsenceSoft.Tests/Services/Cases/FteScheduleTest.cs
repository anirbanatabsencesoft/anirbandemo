﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Policies;
using System.Collections.Generic;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data;

namespace AbsenceSoft.Tests.Services.Cases
{
    [TestClass]
    public class FteScheduleTest
    {
        private Employee employeeWithFteHours;
        private Employee employeeWithFTEPercentage;
        private Case theHoursPerWeekCase;
        private Case theFTEPercentageCase;


        private Employee getTestEmployee(ScheduleType scheduleType, int duration, FteWeeklyDuration weeklyDuration)
        {
            string customerId = "000000000000000000000002";
            string employerId = "000000000000000000000002";
            Employee testEmployee = new Employee()
            {
                EmployeeNumber = "20190101-1",
                CustomerId = customerId,
                EmployerId = employerId,

                FirstName = "EmpWithFTESchedule",
                LastName = "LastName",
                IsExempt = false,
                IsKeyEmployee = false,
                WorkCountry = "US",
                WorkState = "CA",
                Meets50In75MileRule = false,
                PriorHours = new List<PriorHours>()
                {
                    new PriorHours() {HoursWorked=2016, AsOf = new DateTime(2012,11,21)}
                },
                ServiceDate = new DateTime(2009, 02, 02),

                WorkSchedules = new List<Schedule>() {
                    new Schedule() { StartDate = new DateTime(2017, 01, 01),
                        ScheduleType = scheduleType,
                        FteMinutesPerWeek = duration,
                        FteWeeklyDuration = weeklyDuration
                    }
                }
            };
            return testEmployee;
        }
        private void toggleHolidays(PolicyBehavior policyBehavior)
        {
            var testPolicy = Policy.GetByCode("FMLA");
            testPolicy.PolicyBehaviorType = policyBehavior;
            testPolicy.Save();
        }
        private Case createTestCase(Employee employee, DateTime startDate, DateTime endDate, CaseType caseType, bool deletePreviousCase = true)
        {
            Case newCase;
            if (deletePreviousCase)
            {
                Case.AsQueryable().Where(c => c.Employee.Id == employee.Id && c.Reason.Code == "EHC").ForEach(a => a.Delete());
            }

            using (CaseService svc = new CaseService())
            using (EligibilityService eligSvc = new EligibilityService())
            {
                string reasonCode = AbsenceReason.GetByCode("EHC").Code;

                var theCase = svc.CreateCase(CaseStatus.Open, employee.Id, startDate, endDate, caseType, reasonCode);
                eligSvc.RunEligibility(theCase);
                theCase.Segments[0].AppliedPolicies.RemoveAll(p => p.Policy.Code != "FMLA"); // ensure we're only dealing with FMLA  
                newCase = svc.RunCalcs(theCase);               
                newCase = svc.ApplyDetermination(newCase, null, newCase.StartDate, newCase.EndDate.Value, AdjudicationStatus.Approved);
                newCase = svc.UpdateCase(newCase, CaseEventType.CaseCreated);
                newCase.Save();
            }
            return newCase;
        }

        // Create time off request
        private IntermittentTimeRequest tor(DateTime date, int minutes)
        {
            return new IntermittentTimeRequest()
            {
                IntermittentType = IntermittentType.Incapacity,
                PassedCertification = true,
                RequestDate = date,
                TotalMinutes = minutes,
                Detail = new List<IntermittentTimeRequestDetail>(1)
                { 
                            new IntermittentTimeRequestDetail()
                            {
                                Approved = minutes,
                                PolicyCode = "FMLA",
                                TotalMinutes = minutes
                            }
                },
                EmployeeEntered = false,
                IsIntermittentRestriction = false,
                IsMaxOccurenceReached = false,
                ManagerApproved = false

            };
        }
        public void BuildTestEmployeeAndCases(DateTime startDate, DateTime endDate, CaseType caseType, int scheduleTime)
        {
            //toggleHolidays(PolicyBehavior.Ignored);

            employeeWithFteHours = getTestEmployee(ScheduleType.FteVariable, scheduleTime, FteWeeklyDuration.FteTimePerWeek);
            employeeWithFTEPercentage = getTestEmployee(ScheduleType.FteVariable, scheduleTime, FteWeeklyDuration.FtePercentage);

            employeeWithFteHours.Save();
            employeeWithFTEPercentage.Save();

            theHoursPerWeekCase = createTestCase(employeeWithFteHours, startDate, endDate, caseType);
            theFTEPercentageCase = createTestCase(employeeWithFTEPercentage, startDate, endDate, caseType);

            //toggleHolidays(PolicyBehavior.ReducesWorkWeek);
        }

        [TestMethod]
        public void ReducedCaseWithFTELeaveSchedule()
        {
            DateTime startDate = new DateTime(2018, 8, 5, 0, 0, 0, DateTimeKind.Utc);
            DateTime endDate = new DateTime(2018, 8, 11, 0, 0, 0, DateTimeKind.Utc);

            BuildTestEmployeeAndCases(startDate, endDate, CaseType.Reduced, 2400);

            toggleHolidays(PolicyBehavior.Ignored);

            theHoursPerWeekCase.Segments[0].LeaveSchedule[0].FteWeeklyDuration = FteWeeklyDuration.FteTimePerWeek;
            theHoursPerWeekCase.Segments[0].LeaveSchedule[0].FteMinutesPerWeek = 900;


            using (CaseService svc = new CaseService())
            using (EligibilityService eligSvc = new EligibilityService())
            {
                string reasonCode = AbsenceReason.GetByCode("EHC").Code;                
                eligSvc.RunEligibility(theHoursPerWeekCase);
                theHoursPerWeekCase.Segments[0].AppliedPolicies.RemoveAll(p => p.Policy.Code != "FMLA"); // ensure we're only dealing with FMLA  
                theHoursPerWeekCase = svc.RunCalcs(theHoursPerWeekCase);
                theHoursPerWeekCase = svc.ApplyDetermination(theHoursPerWeekCase, null, theHoursPerWeekCase.StartDate, theHoursPerWeekCase.EndDate.Value, AdjudicationStatus.Approved);
                theHoursPerWeekCase = svc.UpdateCase(theHoursPerWeekCase, CaseEventType.CaseCreated);
                theHoursPerWeekCase.Save();
            }
           
            toggleHolidays(PolicyBehavior.ReducesWorkWeek);

            AppliedPolicy fmla = theHoursPerWeekCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
            AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == startDate);
            Assert.AreEqual(0, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == startDate.AddDays(1));
            Assert.AreEqual(300, apu.MinutesUsed);

            employeeWithFteHours.Delete();
            employeeWithFTEPercentage.Delete();
        }

        [TestMethod, Ignore]
        public void CaseWithMultipleLeaveTypes()
        {
            DateTime startDate = new DateTime(2018, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime endDate = new DateTime(2018, 3, 17, 0, 0, 0, DateTimeKind.Utc);

            BuildTestEmployeeAndCases(startDate, endDate, CaseType.Consecutive, 2400);            

            startDate = new DateTime(2018, 3, 18, 0, 0, 0, DateTimeKind.Utc);
            endDate = new DateTime(2018, 4, 1, 0, 0, 0, DateTimeKind.Utc);

            toggleHolidays(PolicyBehavior.Ignored);

            Case theHoursPerWeekCase2 = createTestCase(employeeWithFteHours, startDate, endDate, CaseType.Intermittent, deletePreviousCase: false);
            Case theFTEPercentageCase2 = createTestCase(employeeWithFTEPercentage, startDate, endDate, CaseType.Intermittent, deletePreviousCase: false);


            using (CaseService caseSvc = new CaseService())
            using (EligibilityService eligSvc = new EligibilityService())
            {
                DateTime tempStartDate = startDate.Clone();
                while (tempStartDate < endDate)
                {
                    theHoursPerWeekCase2 = caseSvc.UpdateCase(caseSvc.CreateOrModifyTimeOffRequest(theHoursPerWeekCase2, new List<IntermittentTimeRequest>() { tor(tempStartDate, 600) }));
                    theFTEPercentageCase2 = caseSvc.UpdateCase(caseSvc.CreateOrModifyTimeOffRequest(theFTEPercentageCase2, new List<IntermittentTimeRequest>() { tor(tempStartDate, 600) }));

                    tempStartDate = tempStartDate.AddDays(1);
                }

            }

            toggleHolidays(PolicyBehavior.ReducesWorkWeek);

            AppliedPolicy fmla = theHoursPerWeekCase2.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
            AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u =>u.Determination ==AdjudicationStatus.Denied && u.DateUsed == new DateTime(2018, 3, 23, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(AdjudicationDenialReason.Exhausted, apu.DenialReasonCode);
            apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DateUsed == new DateTime(2018, 3, 25, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(AdjudicationDenialReason.Exhausted, apu.DenialReasonCode);
            apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DateUsed == new DateTime(2018, 3, 25, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(AdjudicationDenialReason.Exhausted, apu.DenialReasonCode);

            fmla = theFTEPercentageCase2.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
            apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DateUsed == new DateTime(2018, 3, 23, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(AdjudicationDenialReason.Exhausted, apu.DenialReasonCode);
            apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DateUsed == new DateTime(2018, 3, 24, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(AdjudicationDenialReason.Exhausted, apu.DenialReasonCode);
            apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied && u.DateUsed == new DateTime(2018, 3, 25, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(AdjudicationDenialReason.Exhausted, apu.DenialReasonCode);

            Case.AsQueryable().Where(c => c.Employee.Id == employeeWithFteHours.Id && c.Reason.Code == "EHC").ForEach(a => a.Delete());
            Case.AsQueryable().Where(c => c.Employee.Id == employeeWithFTEPercentage.Id && c.Reason.Code == "EHC").ForEach(a => a.Delete());

            employeeWithFteHours.Delete();
            employeeWithFTEPercentage.Delete();
        }

        [TestMethod]
        public void LeaveExhaustion()
        {
            DateTime startDate = new DateTime(2018, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime endDate = new DateTime(2018, 4, 1, 0, 0, 0, DateTimeKind.Utc);

            BuildTestEmployeeAndCases(startDate, endDate, CaseType.Consecutive, 2400);

            AppliedPolicy fmla = theHoursPerWeekCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
            AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied);
            Assert.AreEqual(new DateTime(2018, 3, 26, 0, 0, 0, DateTimeKind.Utc), apu.DateUsed);

            startDate = new DateTime(2019, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            endDate = new DateTime(2019, 4, 1, 0, 0, 0, DateTimeKind.Utc);

            Case theCase = createTestCase(employeeWithFteHours, startDate, endDate, CaseType.Consecutive, false);
            fmla = theCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
            apu = fmla.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied);
            Assert.AreEqual(new DateTime(2019, 3, 26, 0, 0, 0, DateTimeKind.Utc), apu.DateUsed);

            Case.AsQueryable().Where(c => c.Employee.Id == employeeWithFteHours.Id && c.Reason.Code == "EHC").ForEach(a => a.Delete());

            employeeWithFteHours.Delete();
            employeeWithFTEPercentage.Delete();
        }

        [TestMethod]
        public void CaseCreationWithFteSchedule()
        {
            DateTime startDate = new DateTime(2018, 7, 8, 0, 0, 0, DateTimeKind.Utc);
            DateTime endDate = new DateTime(2018, 7, 14, 0, 0, 0, DateTimeKind.Utc);

            BuildTestEmployeeAndCases(startDate, endDate, CaseType.Consecutive, 2400);            

            AppliedPolicy fmla = theHoursPerWeekCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
            AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == startDate);
            Assert.AreEqual(0, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == startDate.AddDays(1));
            Assert.AreEqual(480, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == startDate.AddDays(2));
            Assert.AreEqual(480, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == startDate.AddDays(3));
            Assert.AreEqual(480, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == startDate.AddDays(4));
            Assert.AreEqual(480, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == startDate.AddDays(5));
            Assert.AreEqual(480, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == endDate);
            Assert.AreEqual(0, apu.MinutesUsed);

            fmla = theFTEPercentageCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == startDate);
            Assert.AreEqual(0, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == startDate.AddDays(1));
            Assert.AreEqual(480, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == startDate.AddDays(2));
            Assert.AreEqual(480, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == startDate.AddDays(3));
            Assert.AreEqual(480, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == startDate.AddDays(4));
            Assert.AreEqual(480, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == startDate.AddDays(5));
            Assert.AreEqual(480, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == endDate);
            Assert.AreEqual(0, apu.MinutesUsed);

            employeeWithFteHours.Delete();
            employeeWithFTEPercentage.Delete();
        }
        

        [TestMethod, Ignore]
        public void CaseCreationWithHolidaysEnabled()
        {
            DateTime startDate = new DateTime(2018, 11, 4, 0, 0, 0, DateTimeKind.Utc);
            DateTime endDate = new DateTime(2018, 11, 13, 0, 0, 0, DateTimeKind.Utc);

            employeeWithFteHours = getTestEmployee(ScheduleType.FteVariable, 2400, FteWeeklyDuration.FteTimePerWeek);
            employeeWithFTEPercentage = getTestEmployee(ScheduleType.FteVariable, 2400, FteWeeklyDuration.FtePercentage);

            employeeWithFteHours.Save();
            employeeWithFTEPercentage.Save();

            theHoursPerWeekCase = createTestCase(employeeWithFteHours, startDate, endDate, CaseType.Consecutive);
            theFTEPercentageCase = createTestCase(employeeWithFTEPercentage, startDate, endDate, CaseType.Consecutive);

            AppliedPolicy fmla = theHoursPerWeekCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");

            //Total missed time (480 min) is allocated to 13th Nov 2018 because 12 Nov 2018 is Holiday
            AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == endDate);
            Assert.AreEqual(480, apu.MinutesUsed);

            //12 Nov 2018 is Holiday
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == endDate.AddDays(-1));
            Assert.AreEqual(0, apu.MinutesUsed);


            fmla = theFTEPercentageCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");
            //Total missed time (480 min) is allocated to 13th Nov 2018 because 12 Nov 2018 is Holiday
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == endDate);
            Assert.AreEqual(480, apu.MinutesUsed);

            //12 Nov 2018 is Holiday
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == endDate.AddDays(-1));
            Assert.AreEqual(0, apu.MinutesUsed);

            employeeWithFteHours.Delete();
            employeeWithFTEPercentage.Delete();
        }

       

        [TestMethod]
        public void ChangeCaseToHaveMultipleSegments()
        {
            DateTime startDate = new DateTime(2018, 7, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime endDate = new DateTime(2018, 7, 28, 0, 0, 0, DateTimeKind.Utc);

            toggleHolidays(PolicyBehavior.Ignored);

            BuildTestEmployeeAndCases(startDate, endDate, CaseType.Consecutive, 2400);

            
            using (CaseService svc = new CaseService())
            {
                theHoursPerWeekCase = svc.ChangeCase(theHoursPerWeekCase, startDate, new DateTime(2018, 7, 14, 0, 0, 0, DateTimeKind.Utc), false, false, CaseType.Reduced, false, false);
                theFTEPercentageCase = svc.ChangeCase(theFTEPercentageCase, startDate, new DateTime(2018, 7, 14, 0, 0, 0, DateTimeKind.Utc), false, false, CaseType.Reduced, false, false);
            }

            Assert.AreEqual(2, theHoursPerWeekCase.Segments.Count);
            Assert.AreEqual(2, theFTEPercentageCase.Segments.Count);

            startDate = startDate.AddDays(3);
            endDate = endDate.AddDays(-4);
            using (CaseService svc = new CaseService())
            {
                theHoursPerWeekCase = svc.ChangeCase(theHoursPerWeekCase, startDate, endDate, true, true, CaseType.Consecutive, true, true);
                theFTEPercentageCase = svc.ChangeCase(theFTEPercentageCase, startDate, endDate, true, true, CaseType.Consecutive, true, true);
            }
            Assert.AreEqual(1, theHoursPerWeekCase.Segments.Count);
            Assert.AreEqual(1, theFTEPercentageCase.Segments.Count);   

            using (CaseService svc = new CaseService())
            {
                theHoursPerWeekCase = svc.RunCalcs(theHoursPerWeekCase);
                theHoursPerWeekCase = svc.ApplyDetermination(theHoursPerWeekCase, null, theHoursPerWeekCase.StartDate, theHoursPerWeekCase.EndDate.Value, AdjudicationStatus.Approved);

                theFTEPercentageCase = svc.RunCalcs(theFTEPercentageCase);
                theFTEPercentageCase = svc.ApplyDetermination(theFTEPercentageCase, null, theFTEPercentageCase.StartDate, theFTEPercentageCase.EndDate.Value, AdjudicationStatus.Approved);
            }

            toggleHolidays(PolicyBehavior.ReducesWorkWeek);

            AppliedPolicy fmla = theHoursPerWeekCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");

            //Verify last week allocation for case with schedule as FTEHoursPerWeek
            AppliedPolicyUsage apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == endDate);
            Assert.AreEqual(480, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == endDate.AddDays(-1));
            Assert.AreEqual(480, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == endDate.AddDays(-2));
            Assert.AreEqual(0, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == endDate.AddDays(-3));
            Assert.AreEqual(0, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == endDate.AddDays(-4));
            Assert.AreEqual(480, apu.MinutesUsed);

            //Verify first week allocation for case with schedule as FTEHoursPerWeek
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == startDate);
            Assert.AreEqual(480, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == startDate.AddDays(1));
            Assert.AreEqual(480, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == startDate.AddDays(2));
            Assert.AreEqual(480, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == startDate.AddDays(3));
            Assert.AreEqual(0, apu.MinutesUsed);

            fmla = theFTEPercentageCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(a => a.Policy.Code == "FMLA");

            //Verify last week allocation for case with schedule as FTEPercentage
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == endDate);
            Assert.AreEqual(480, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == endDate.AddDays(-1));
            Assert.AreEqual(480, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == endDate.AddDays(-2));
            Assert.AreEqual(0, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == endDate.AddDays(-3));
            Assert.AreEqual(0, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == endDate.AddDays(-4));
            Assert.AreEqual(480, apu.MinutesUsed);

            //Verify first week allocation for case with schedule as FTEPercentage
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == startDate);
            Assert.AreEqual(480, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == startDate.AddDays(1));
            Assert.AreEqual(480, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == startDate.AddDays(2));
            Assert.AreEqual(480, apu.MinutesUsed);
            apu = fmla.Usage.FirstOrDefault(u => u.DateUsed == startDate.AddDays(3));
            Assert.AreEqual(0, apu.MinutesUsed);

            

            employeeWithFteHours.Delete();
            employeeWithFTEPercentage.Delete();
            toggleHolidays(PolicyBehavior.ReducesWorkWeek);

        }


    }
}
