﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Logic.Cases;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Tests.Services.ChangeCase
{
    [TestClass]
    public class ChangeCase
    {
        private Employee employeeForChangeCase;

        private Employee getTestEmployee()
        {
            string customerId = "000000000000000000000002";
            string employerId = "000000000000000000000002";
            Employer employer = Employer.GetById(employerId);

            Employee testEmployee = new Employee()
            {
                EmployeeNumber = "20190409-1",
                CustomerId = customerId,
                EmployerId = employerId,
                FirstName = "Consecutive",
                LastName = "Admin",
                IsExempt = false,
                IsKeyEmployee = false,
                WorkCountry = "US",
                WorkState = "CA",
                Meets50In75MileRule = false,
                ServiceDate = new DateTime(2009, 02, 02),
                PriorHours = new List<PriorHours>()
                {
                    new PriorHours() {HoursWorked=2016, AsOf = new DateTime(2012,11,21)}
                },
                WorkSchedules = new List<Schedule>() {
                    new Schedule() { StartDate = new DateTime(2017, 03, 02),
                        ScheduleType = ScheduleType.Weekly,
                        Times = TestWorkSchedules.MakeSchedule(new DateTime(2017, 03, 02),40)}
                }
            };
            testEmployee.Employer = employer;
            testEmployee.Save();
            return testEmployee;
        }

        private Policy createTestPolicy()
        {
            string policyCode = "IMP-982";
            var testPolicy = new Policy()
            {
                Name = "Change Case IMP-982",
                Code = policyCode,
                AbsenceReasons = new List<PolicyAbsenceReason>()
                {
                    new PolicyAbsenceReason()
                    {
                        Reason = Setup.Policies.PolicyBase.Reasons.EmployeeHealthCondition,
                        CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced | CaseType.Administrative ,
                        EntitlementType = EntitlementType.WorkWeeks,
                        Entitlement = 75
                    }
                }
            };

            testPolicy.Save();
            return testPolicy;
        }
        private void deleteTestCases(string empId)
        {
            Case.Repository.Collection.Remove(Case.Query.EQ(c => c.Employee.Id, empId));
            AbsenceSoft.Data.ToDo.ToDoItem.Repository.Collection.Remove(AbsenceSoft.Data.ToDo.ToDoItem.Query.EQ(c => c.EmployeeId, empId));
            Communication.Repository.Collection.Remove(Communication.Query.EQ(c => c.EmployeeId, empId));
            Attachment.Repository.Collection.Remove(Attachment.Query.EQ(c => c.EmployeeId, empId));
        }

        /// <summary>
        /// This is a test to check Usage Calculation based on policy absence reason flag "OnlyCalculateUsageForLostTime"
        /// which if FALSE, lumps Administrative time for a policy with Leave time; 
        /// if that flag on the Policy Absence Reason is TRUE, it does not count the time together 
        /// (Admin time is separate from other lost-time types)
        /// </summary>
        [TestMethod]
        public void TestUsageBasedOnPolicyFlag()
        {
            Policy testPolicy = createTestPolicy();
            
            testPolicy.AbsenceReasons.FirstOrDefault(a => a.ReasonCode == "EHC").OnlyCalculateUsageForLostTime = false;
            testPolicy.AbsenceReasons.FirstOrDefault(a => a.ReasonCode == "EHC").Entitlement = 4;
            testPolicy.AbsenceReasons.FirstOrDefault(a => a.ReasonCode == "EHC").EntitlementType = EntitlementType.WorkWeeks;
            testPolicy.PolicyBehaviorType = PolicyBehavior.Ignored;
            testPolicy.Save();

            if (employeeForChangeCase == null)
            {
                employeeForChangeCase = getTestEmployee();
            }
            string caseCode = "EHC";
            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(employeeForChangeCase.Id);
            CaseService cs = new CaseService();

            DateTime startDate = new DateTime(2019, 1, 6, 0, 0, 0, DateTimeKind.Utc);
            DateTime endDate = new DateTime(2019, 1, 19, 0, 0, 0, DateTimeKind.Utc);

            Case nonAdminCase = cs.CreateCase(CaseStatus.Open, employeeForChangeCase.Id, startDate, endDate, CaseType.Consecutive, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(nonAdminCase);
            nonAdminCase = cs.UpdateCase(nonAdminCase);

            startDate = new DateTime(2019, 2, 3, 0, 0, 0, DateTimeKind.Utc);
            endDate = new DateTime(2019, 2, 23, 0, 0, 0, DateTimeKind.Utc);

            Case adminCase = cs.CreateCase(CaseStatus.Open, employeeForChangeCase.Id, startDate, endDate, CaseType.Administrative, reason.Code);
            loa = esvc.RunEligibility(adminCase);
            adminCase = cs.UpdateCase(adminCase);

            var casePolicy = adminCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == testPolicy.Code);

            //Policy entitlement is 4 week, and flag "OnlyCalculateUsageForLostTime" is false. 
            //So Admin leave usage will be counted with non admin leave and policy should exhaust in 4 week.
            Assert.AreNotEqual(casePolicy.Usage.FirstOrDefault(usage => usage.DateUsed == new DateTime(2019, 02, 18, 0, 0, 0, DateTimeKind.Utc)).Determination, AdjudicationStatus.Pending);

            deleteTestCases(employeeForChangeCase.Id);

            testPolicy.AbsenceReasons.FirstOrDefault(a => a.ReasonCode == "EHC").OnlyCalculateUsageForLostTime = true;
            testPolicy.Save();

            startDate = new DateTime(2019, 1, 6, 0, 0, 0, DateTimeKind.Utc);
            endDate = new DateTime(2019, 1, 19, 0, 0, 0, DateTimeKind.Utc);

            nonAdminCase = cs.CreateCase(CaseStatus.Open, employeeForChangeCase.Id, startDate, endDate, CaseType.Consecutive, reason.Code);

            esvc = new EligibilityService();
            loa = esvc.RunEligibility(nonAdminCase);
            nonAdminCase = cs.UpdateCase(nonAdminCase);

            startDate = new DateTime(2019, 2, 3, 0, 0, 0, DateTimeKind.Utc);
            endDate = new DateTime(2019, 2, 23, 0, 0, 0, DateTimeKind.Utc);

            adminCase = cs.CreateCase(CaseStatus.Open, employeeForChangeCase.Id, startDate, endDate, CaseType.Administrative, reason.Code);
            loa = esvc.RunEligibility(adminCase);
            adminCase = cs.UpdateCase(adminCase);

            casePolicy = adminCase.Segments[0].AppliedPolicies.FirstOrDefault(p => p.Policy.Code == testPolicy.Code);

            //Policy entitlement is 4 week, and flag "OnlyCalculateUsageForLostTime" is true. 
            //So Admin leave usage will be counted saperatly from non admin leave. Admin leave can make use of the full entitlement of 4 weeks
            //independent of policy usage in non admin cases
            Assert.AreEqual(casePolicy.Usage.FirstOrDefault(usage => usage.DateUsed == new DateTime(2019, 02, 18, 0, 0, 0, DateTimeKind.Utc)).Determination, AdjudicationStatus.Pending);

            deleteTestCases(employeeForChangeCase.Id);
            testPolicy.Delete();
        }

        /// <summary>
        /// Create a Consecutive case and change it to different type in following sequence
        ///     1. Admin Type
        ///     2. Reduced Type
        ///     3. Admin Type
        ///     4. Intermittent Type
        ///     5. Admin Type
        ///     6. Consecutive Type
        /// </summary>
        [TestMethod]
        public void TestChangeCaseToAndFromAdminType()
        {
            Policy testPolicy = createTestPolicy();

            if (employeeForChangeCase == null)
            {
                employeeForChangeCase = getTestEmployee();
            }

            string caseCode = "EHC";
            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            deleteTestCases(employeeForChangeCase.Id);
            CaseService cs = new CaseService();

            DateTime startDate = new DateTime(2019, 1, 6, 0, 0, 0, DateTimeKind.Utc);
            DateTime endDate = new DateTime(2019, 1, 12, 0, 0, 0, DateTimeKind.Utc);

            Case nonAdminCase = cs.CreateCase(CaseStatus.Open, employeeForChangeCase.Id, startDate, endDate, CaseType.Consecutive, reason.Code);

            EligibilityService esvc = new EligibilityService();
            LeaveOfAbsence loa = esvc.RunEligibility(nonAdminCase);
            Assert.IsNotNull(nonAdminCase);
            nonAdminCase = cs.UpdateCase(nonAdminCase);
            Assert.IsNotNull(nonAdminCase.Id);

            cs.ChangeCase(nonAdminCase, startDate, endDate, false, false, CaseType.Administrative, false, false);

            Assert.AreEqual(1, nonAdminCase.Segments.Count);
            Assert.AreEqual(CaseType.Administrative, nonAdminCase.Segments[0].Type);
            Assert.AreEqual(1, nonAdminCase.Segments[0].AppliedPolicies.Where(p => p.Policy.Code == "IMP-982").Count());

            cs.ChangeCase(nonAdminCase, startDate, endDate, false, false, CaseType.Reduced, false, false);
            Assert.AreEqual(1, nonAdminCase.Segments.Count);
            Assert.AreEqual(CaseType.Reduced, nonAdminCase.Segments[0].Type);
            Assert.AreEqual(1, nonAdminCase.Segments[0].AppliedPolicies.Where(p => p.Policy.Code == "IMP-982").Count());

            cs.ChangeCase(nonAdminCase, startDate, endDate, false, false, CaseType.Administrative, false, false);
            Assert.AreEqual(1, nonAdminCase.Segments.Count);
            Assert.AreEqual(CaseType.Administrative, nonAdminCase.Segments[0].Type);
            Assert.AreEqual(1, nonAdminCase.Segments[0].AppliedPolicies.Where(p => p.Policy.Code == "IMP-982").Count());

            cs.ChangeCase(nonAdminCase, startDate, endDate, false, false, CaseType.Intermittent, false, false);
            Assert.AreEqual(1, nonAdminCase.Segments.Count);
            Assert.AreEqual(CaseType.Intermittent, nonAdminCase.Segments[0].Type);
            Assert.AreEqual(1, nonAdminCase.Segments[0].AppliedPolicies.Where(p => p.Policy.Code == "IMP-982").Count());

            cs.ChangeCase(nonAdminCase, startDate, endDate, false, false, CaseType.Administrative, false, false);
            Assert.AreEqual(1, nonAdminCase.Segments.Count);
            Assert.AreEqual(CaseType.Administrative, nonAdminCase.Segments[0].Type);
            Assert.AreEqual(1, nonAdminCase.Segments[0].AppliedPolicies.Where(p => p.Policy.Code == "IMP-982").Count());

            cs.ChangeCase(nonAdminCase, startDate, endDate, false, false, CaseType.Consecutive, false, false);
            Assert.AreEqual(1, nonAdminCase.Segments.Count);
            Assert.AreEqual(CaseType.Consecutive, nonAdminCase.Segments[0].Type);
            Assert.AreEqual(1, nonAdminCase.Segments[0].AppliedPolicies.Where(p => p.Policy.Code == "IMP-982").Count());

            deleteTestCases(employeeForChangeCase.Id);
            testPolicy.Delete();
        }        

        /// <summary>
        /// The unit tests for changing partially or extending as a different type(not just replacing the entire case with a new type)
        /// </summary>
        [TestMethod]
        public void ChangeCaseTestWithAdminType()
        {
            if (employeeForChangeCase == null)
            {
                employeeForChangeCase = getTestEmployee();
            }
            // first bit stolen from create case
            using (CaseService svc = new CaseService())
            {                
                Assert.IsNotNull(employeeForChangeCase);
                var reasons = svc.GetAbsenceReasons(employeeForChangeCase.Id);
                Assert.IsNotNull(reasons);
                Assert.IsTrue(reasons.Any());
                var r = reasons.First();

                Case.AsQueryable().Where(c => c.Employee.Id == employeeForChangeCase.Id && c.Reason.Code == r.Code).ForEach(a => a.Delete());

                // start with a three week case
                var newCase = svc.CreateCase(CaseStatus.Open, employeeForChangeCase.Id, new DateTime(2018, 2, 2, 0, 0, 0, DateTimeKind.Utc), new DateTime(2018, 2, 22, 0, 0, 0, DateTimeKind.Utc), CaseType.Administrative, r.Code, description: "Test.CreateCase");
                Assert.IsNotNull(newCase);
                newCase = svc.UpdateCase(newCase);
                Assert.IsNotNull(newCase.Id);

                // first add one that comes a week after this on
                svc.ChangeCase(newCase, new DateTime(2018, 3, 2, 0, 0, 0, DateTimeKind.Utc), new DateTime(2018, 3, 8, 0, 0, 0, DateTimeKind.Utc), true, true, CaseType.Administrative, false, false);

                // Now, since we said change the start and end date, we should only have a single segment
                Assert.AreEqual(1, newCase.Segments.Count);
                Assert.AreEqual(new DateTime(2018, 3, 2, 0, 0, 0, DateTimeKind.Utc), newCase.StartDate);
                Assert.AreEqual(new DateTime(2018, 3, 8, 0, 0, 0, DateTimeKind.Utc), newCase.EndDate);

                // now make it start a week earlier
                svc.ChangeCase(newCase, new DateTime(2018, 1, 26, 0, 0, 0, DateTimeKind.Utc), new DateTime(2018, 2, 22, 0, 0, 0, DateTimeKind.Utc), true, true, CaseType.Administrative, false, false);
                Assert.AreEqual(1, newCase.Segments.Count);
                Assert.AreEqual(new DateTime(2018, 1, 26, 0, 0, 0, DateTimeKind.Utc), newCase.StartDate);
                Assert.AreEqual(new DateTime(2018, 2, 22, 0, 0, 0, DateTimeKind.Utc), newCase.EndDate);

                // now make the second one end later
                svc.ChangeCase(newCase, new DateTime(2018, 3, 2, 0, 0, 0, DateTimeKind.Utc), new DateTime(2018, 3, 22, 0, 0, 0, DateTimeKind.Utc), true, true, CaseType.Administrative, false, false);
                Assert.AreEqual(1, newCase.Segments.Count);
                Assert.AreEqual(new DateTime(2018, 3, 2, 0, 0, 0, DateTimeKind.Utc), newCase.StartDate);
                Assert.AreEqual(new DateTime(2018, 3, 22, 0, 0, 0, DateTimeKind.Utc), newCase.EndDate);

                // now the icky test, drop an intermittent case that overlaps the end of the first and the
                // beginning of the second. It should insert it as the second segment and then adjust the
                // other two segments accordingly
                svc.ChangeCase(newCase, new DateTime(2018, 2, 16, 0, 0, 0, DateTimeKind.Utc), new DateTime(2018, 3, 5, 0, 0, 0, DateTimeKind.Utc), true, false, CaseType.Intermittent, false, false);
                Assert.AreEqual(2, newCase.Segments.Count);
                Assert.AreEqual(new DateTime(2018, 2, 16, 0, 0, 0, DateTimeKind.Utc), newCase.StartDate);
                Assert.AreEqual(new DateTime(2018, 3, 22, 0, 0, 0, DateTimeKind.Utc), newCase.EndDate);
            }
        }

        [TestMethod]
        public void ChangeCaseOneDayAdminTest()
        {
            if (employeeForChangeCase == null)
            {
                employeeForChangeCase = getTestEmployee();
            }
            using (CaseService svc = new CaseService())
            {
                var reasons = svc.GetAbsenceReasons(employeeForChangeCase.Id);
                Assert.IsNotNull(reasons);
                Assert.IsTrue(reasons.Any());
                var r = reasons.First();

                Case.AsQueryable().Where(c => c.Employee.Id == employeeForChangeCase.Id && c.Reason.Code == r.Code).ForEach(a => a.Delete());

                // start with a three week case
                var newCase = svc.CreateCase(CaseStatus.Open, employeeForChangeCase.Id, new DateTime(2014, 2, 2, 0, 0, 0, DateTimeKind.Utc), new DateTime(2014, 2, 22, 0, 0, 0, DateTimeKind.Utc), CaseType.Consecutive, r.Code, description: "Test.CreateCase");
                Assert.IsNotNull(newCase);
                newCase = svc.UpdateCase(newCase);
                Assert.IsNotNull(newCase.Id);

                newCase = svc.ChangeCase(newCase, newCase.StartDate, newCase.StartDate, false, false, CaseType.Administrative, false, false);
                Assert.AreEqual(2, newCase.Segments.Count);

                var orderedSegments = newCase.Segments.OrderBy(s => s.StartDate);
                var firstSegment = orderedSegments.First();
                Assert.AreEqual(newCase.StartDate, firstSegment.StartDate);
                Assert.AreEqual(newCase.StartDate, firstSegment.EndDate);

                var lastSegment = orderedSegments.Last();
                Assert.AreEqual(newCase.StartDate.AddDays(1), lastSegment.StartDate);
                Assert.AreEqual(newCase.EndDate, lastSegment.EndDate);
            }
        }

        [TestMethod]
        public void CaseSummaryInPolicyGapsWithAdminCase()
        {
            if (employeeForChangeCase == null)
            {
                employeeForChangeCase = getTestEmployee();
            }

            using (CaseService svc = new CaseService())
            using (EligibilityService eligSvc = new EligibilityService())
            {                
                DateTime intermittentStartDate = new DateTime(2018, 10, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime intermittentEndDate = new DateTime(2018, 11, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime newAdminStartDate = new DateTime(2018, 2, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime newAdminEndDate = new DateTime(2018, 3, 1, 0, 0, 0, DateTimeKind.Utc);
                string reasonCode = AbsenceReason.GetByCode("EHC").Code;

                var intermittentCase = svc.CreateCase(CaseStatus.Open, employeeForChangeCase.Id, intermittentStartDate, intermittentEndDate, CaseType.Intermittent, reasonCode);
                eligSvc.AddManualPolicy(intermittentCase, "FMLA", "testing adding a manual policy");
                eligSvc.RunEligibility(intermittentCase);
                svc.ChangeCase(intermittentCase, newAdminStartDate, newAdminEndDate, true, false, CaseType.Consecutive, false, false);
                eligSvc.RunEligibility(intermittentCase);

                var policy = intermittentCase.Summary.Policies.First();
                var calculatedPolicyEndDate = policy.Detail.Where(f => f.CaseType == CaseType.Consecutive).Max(f => f.EndDate);

                Assert.AreEqual(newAdminEndDate, calculatedPolicyEndDate,
                    string.Format("Case Summary for Policy {0} calculated incorrect end date", policy.PolicyName));
            }
        }

        /// <summary>
        /// Tests that when Cases overlap but their segments do not that no exception is thrown
        /// </summary>
        [TestMethod]
        public void CaseSegmentNoOverlapWithAdmin()
        {
            if (employeeForChangeCase == null)
            {
                employeeForChangeCase = getTestEmployee();
            }
            using (CaseService svc = new CaseService())
            {
                DateTime adminStartDate = new DateTime(2018, 4, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime adminEndDate = new DateTime(2018, 5, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime intermittentStartDate = new DateTime(2018, 10, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime intermittentEndDate = new DateTime(2018, 11, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime newConsecutiveStartDate = new DateTime(2018, 2, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime newConsecutiveEndDate = new DateTime(2018, 3, 1, 0, 0, 0, DateTimeKind.Utc);
                string reasonCode = AbsenceReason.GetByCode("EHC").Code;

                var originalCase = svc.CreateCase(CaseStatus.Open, employeeForChangeCase.Id, adminStartDate, adminEndDate, CaseType.Administrative, reasonCode).Save();
                var intermittentCase = svc.CreateCase(CaseStatus.Open, employeeForChangeCase.Id, intermittentStartDate, intermittentEndDate, CaseType.Intermittent, reasonCode).Save();
                svc.ChangeCase(intermittentCase, newConsecutiveStartDate, newConsecutiveEndDate, true, false, CaseType.Consecutive, false, false);
                svc.UpdateCase(intermittentCase);
                originalCase.Delete();
                intermittentCase.Delete();
            }
        }        

    }
}
