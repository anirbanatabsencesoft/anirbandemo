﻿using AbsenceSoft;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Security;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Tests.ProdSupport
{
    internal static class TestAT6587Extensions
    {
        public static DateTime ToDate(this string date)
        {
            return DateTime.Parse(date).ToMidnight();
        }
    }

    [TestClass]
    public class AT6587
    {

        [TestMethod]
        public void TestAT6587FmlaShowingTwelvePointSevenWeeksUsed()
        {
            using (var userService = new AuthenticationService())
            using (var customerService = new CustomerService())
            using (var employerService = new EmployerService())
            using (var employeeService = new EmployeeService())
            using (var eligService = new EligibilityService())
            using (var caseService = new CaseService(false))
            {
                #region Create user

                // Create user
                var user = userService.CreateUser(
                    Guid.NewGuid() + "@absencesoft.io",
                    "I'm not telling",
                    "Chad",
                    "Scharf",
                    Role.SystemAdministrator.Id);
                user.UserFlags = UserFlag.CreateCustomer | UserFlag.UpdateEmployer | UserFlag.ProductTour | UserFlag.FirstEmployee;
                userService.UpdateUser(user);

                userService.CurrentUser = user;
                customerService.CurrentUser = user;
                employerService.CurrentUser = user;
                employeeService.CurrentUser = user;
                caseService.CurrentUser = user;
                eligService.CurrentUser = user;

                #endregion Create user

                #region Create customer

                // Create customer
                var customer = customerService.CreateCustomer("AT-6587", "at6587" + Guid.NewGuid().ToString(), "111-222-3333", "111-222-3333", new Address()
                {
                    Address1 = "123 Any Street",
                    City = "Somewhere",
                    State = "MI",
                    Country = "US",
                    PostalCode = "42593"
                }, user);
                customer.EnableAllEmployersByDefault = true;
                customerService.Update(customer);

                userService.CurrentCustomer = customer;
                customerService.CurrentCustomer = customer;
                employerService.CurrentCustomer = customer;
                employeeService.CurrentCustomer = customer;
                caseService.CurrentCustomer = customer;
                eligService.CurrentCustomer = customer;

                // Update user
                user.Roles = new List<string>() { Role.SystemAdministrator.Id };
                user.CustomerId = customer.Id;
                user.Done(UserFlag.CreateCustomer);
                userService.UpdateUser(user);

                #endregion Create customer

                #region Create employer

                // Create employer
                var employer = employerService.CreateEmployer(customer);
                employer.Holidays = new List<Holiday>(0);
                employer.Name = "AT-6587";
                employer.Url = "ess-" + customer.Url;
                employer.MaxEmployees = 1;
                employer.ReferenceCode = null;
                employer.AllowIntermittentForBirthAdoptionOrFosterCare = true;
                employer.IgnoreScheduleForPriorHoursWorked = true;
                employer.IgnoreAverageMinutesWorkedPerWeek = true;
                employer.Enable50In75MileRule = false;
                employer.EnableSpouseAtSameEmployerRule = false;
                employer.FMLPeriodType = PeriodType.RollingBack;
                employer.FTWeeklyWorkHours = 40;
                employer.IsPubliclyTradedCompany = false;
                employer.ModifiedById = user.Id;
                employer.SuppressPolicies = Policy.AsQueryable().Where(p => p.CustomerId == null && p.Code != "FMLA").Select(p => p.Code).ToList();
                employer.SuppressWorkflows = Workflow.AsQueryable().Where(p => p.CustomerId == null).Select(w => w.Code).ToList();
                employerService.Update(employer);

                userService.CurrentEmployer = employer;
                customerService.CurrentEmployer = employer;
                employerService.CurrentEmployer = employer;
                employeeService.CurrentEmployer = employer;
                caseService.CurrentEmployer = employer;
                eligService.CurrentEmployer = employer;

                // Set the user's employer id
                user.Employers.Add(new EmployerAccess()
                {
                    Employer = employer,
                    AutoAssignCases = true
                });
                user.Done(UserFlag.UpdateEmployer);
                userService.UpdateUser(user);

                #endregion Create employer

                #region Override FMLA policy

                // Override FMLA policy
                var fmla = Policy.GetByCode("FMLA");
                fmla.Clean();
                fmla.CustomerId = customer.Id;
                fmla.EmployerId = employer.Id;
                fmla.RuleGroups.Clear();
                fmla.AbsenceReasons.ForEach(r => r.RuleGroups.Clear());
                fmla.Save();

                #endregion Override FMLA policy

                #region Create employee

                // Create employee
                var employee = employeeService.Update(new Employee()
                {
                    CustomerId = customer.Id,
                    EmployerId = employer.Id,
                    EmployerName = employer.Name,
                    EmployeeNumber = "AT-6587-TEST",
                    FirstName = "AT-6587",
                    LastName = "Person",
                    Gender = Gender.Female,
                    HireDate = "09/13/1999".ToDate(),
                    ServiceDate = "01/21/2018".ToDate(),
                    Meets50In75MileRule = false,
                    IsKeyEmployee = false,
                    IsExempt = false,
                    Status = EmploymentStatus.Active,
                    MilitaryStatus = MilitaryStatus.Civilian,
                    WorkCountry = "US",
                    WorkState = "MI",
                    EmployeeClassCode = WorkType.FullTime
                },
                new Schedule()
                {
                    ScheduleType = ScheduleType.Weekly,
                    StartDate = "11/10/2017".ToDate(),
                    Times = new List<Time>(7)
                    {
                        new Time() { SampleDate = "11/05/2017".ToDate(), TotalMinutes = 0 },
                        new Time() { SampleDate = "11/06/2017".ToDate(), TotalMinutes = 480 },
                        new Time() { SampleDate = "11/07/2017".ToDate(), TotalMinutes = 480 },
                        new Time() { SampleDate = "11/08/2017".ToDate(), TotalMinutes = 480 },
                        new Time() { SampleDate = "11/09/2017".ToDate(), TotalMinutes = 480 },
                        new Time() { SampleDate = "11/10/2017".ToDate(), TotalMinutes = 480 },
                        new Time() { SampleDate = "11/11/2017".ToDate(), TotalMinutes = 0 }
                    }
                });

                #endregion Create employee

                #region Create intermittent case

                // Create the first case, intermittent
                var intermittentCase = caseService.CreateCase(
                    CaseStatus.Open,
                    employee.Id,
                    "11/10/2017".ToDate(),
                    "03/16/2018".ToDate(),
                    CaseType.Intermittent,
                    AbsenceReason.GetByCode("EHC").Code,
                    null,
                    "2017-0110057",
                    null,
                    null);
                intermittentCase.AssignedTo = user;
                eligService.RunEligibility(intermittentCase);
                intermittentCase = caseService.UpdateCase(intermittentCase, CaseEventType.CaseCreated);

                intermittentCase.Segments.Should().HaveCount(1);
                intermittentCase.Segments[0].AppliedPolicies.Should().HaveCount(1);
                intermittentCase.Segments[0].AppliedPolicies[0].Policy.Should().NotBeNull();
                intermittentCase.Segments[0].AppliedPolicies[0].Policy.Code.Should().Be(fmla.Code);

                // Approve the FMLA
                intermittentCase = caseService.ApplyDetermination(
                    intermittentCase,
                    fmla.Code,
                    intermittentCase.StartDate,
                    intermittentCase.EndDate.Value,
                    AdjudicationStatus.Approved);

                // Quick method to build TORs
                IntermittentTimeRequest tor(string date)
                {
                    return new IntermittentTimeRequest()
                    {
                        IntermittentType = IntermittentType.Incapacity,
                        PassedCertification = true,
                        RequestDate = date.ToDate(),
                        TotalMinutes = 480,
                        Detail = new List<IntermittentTimeRequestDetail>(1)
                        {
                            new IntermittentTimeRequestDetail()
                            {
                                Approved = 480,
                                PolicyCode = fmla.Code,
                                TotalMinutes = 480
                            }
                        },
                        EmployeeEntered = false,
                        IsIntermittentRestriction = false,
                        IsMaxOccurenceReached = false,
                        ManagerApproved = false
                    };
                };

                // Create the Time Off Requests
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("11/10/2017") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("11/13/2017") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("11/22/2017") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("11/24/2017") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("01/24/2018") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("01/29/2018") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("01/30/2018") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("02/05/2018") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("02/07/2018") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("02/08/2018") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("02/09/2018") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("02/16/2018") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("02/19/2018") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("02/20/2018") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("02/21/2018") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("02/22/2018") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("02/26/2018") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("02/27/2018") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("03/02/2018") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("03/05/2018") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("03/06/2018") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("03/07/2018") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("03/08/2018") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("03/13/2018") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("03/15/2018") }));
                intermittentCase = caseService.UpdateCase(caseService.CreateOrModifyTimeOffRequest(intermittentCase, new List<IntermittentTimeRequest>() { tor("03/16/2018") }));

                #endregion Create intermittent case

                // Get the employee time tracker up to (as of) the date the ticket was opened, May 23, 2018
                var employeeTimeTracker = caseService.GetEmployeePolicySummary(employee, "05/23/2018".ToDate(), fmla.Code);
                employeeTimeTracker.Should().HaveCount(1);
                employeeTimeTracker[0].PolicyCode.Should().BeEquivalentTo(fmla.Code);
                employeeTimeTracker[0].TimeUsed.Should().Be(5.2);
                employeeTimeTracker[0].TimeRemaining.Should().Be(6.8);

                var usage13032018 = intermittentCase.Segments[0].AppliedPolicies[0].Usage.Where(u => u.DateUsed == Date.NewUtcDate(2018, 3, 13)).ToList();
                usage13032018.First().Determination.Should().Be(AdjudicationStatus.Approved, "Time for 13/03/2018 should be Approved");

                var usage15032018 = intermittentCase.Segments[0].AppliedPolicies[0].Usage.Where(u => u.DateUsed == Date.NewUtcDate(2018, 3, 15)).ToList();
                usage15032018.First().Determination.Should().Be(AdjudicationStatus.Approved, "Time for 15/03/2018 should be Approved");


                #region Create consecutive case

                // Create second case, consecutive
                var consecutiveCase = caseService.CreateCase(
                    CaseStatus.Open,
                    employee.Id,
                    "11/27/2017".ToDate(),
                    "01/16/2018".ToDate(),
                    CaseType.Consecutive,
                    AbsenceReason.GetByCode("EHC").Code,
                    null,
                    "2017-0110597",
                    null,
                    null);
                consecutiveCase.AssignedTo = user;
                eligService.RunEligibility(consecutiveCase);
                consecutiveCase = caseService.UpdateCase(consecutiveCase, CaseEventType.CaseCreated);

                consecutiveCase.Segments.Should().HaveCount(1);
                consecutiveCase.Segments[0].AppliedPolicies.Should().HaveCount(1);
                consecutiveCase.Segments[0].AppliedPolicies[0].Policy.Should().NotBeNull();
                consecutiveCase.Segments[0].AppliedPolicies[0].Policy.Code.Should().Be(fmla.Code);

                // Approve the FMLA
                consecutiveCase = caseService.ApplyDetermination(
                    consecutiveCase,
                    fmla.Code,
                    consecutiveCase.StartDate,
                    consecutiveCase.EndDate.Value,
                    AdjudicationStatus.Approved);

                consecutiveCase = caseService.CaseClosed(consecutiveCase, consecutiveCase.EndDate, CaseClosureReason.Other, otherReasonDetails: "AT-6587 Test Closed Case");

                #endregion Create consecutive case

                // Get the employee time tracker up to (as of) the date the ticket was opened, May 23, 2018
                employeeTimeTracker = caseService.GetEmployeePolicySummary(employee, "05/23/2018".ToDate(), fmla.Code);
                employeeTimeTracker.Should().HaveCount(1);
                employeeTimeTracker[0].PolicyCode.Should().BeEquivalentTo(fmla.Code);
                employeeTimeTracker[0].TimeUsed.Should().Be(12.4);
                employeeTimeTracker[0].TimeRemaining.Should().Be(-.4);


                var affectedCases = Case.AsQueryable().Where(c => c.Employee.Id == consecutiveCase.Employee.Id
                && c.StartDate < consecutiveCase.EndDate
                && c.Id != consecutiveCase.Id
                && c.Status == CaseStatus.Open).OrderBy(o => o.StartDate).ToList(); ;

                foreach (var @case in affectedCases)
                {
                    caseService.RunCalcs(@case);
                    caseService.UpdateCase(@case);
                }

                employeeTimeTracker = caseService.GetEmployeePolicySummary(employee, "05/23/2018".ToDate(), fmla.Code);
                employeeTimeTracker.Should().HaveCount(1);
                employeeTimeTracker[0].PolicyCode.Should().BeEquivalentTo(fmla.Code);
                employeeTimeTracker[0].TimeUsed.Should().Be(12);
                employeeTimeTracker[0].TimeRemaining.Should().Be(0);

                intermittentCase = Case.GetById(intermittentCase.Id);

                usage13032018 = intermittentCase.Segments[0].AppliedPolicies[0].Usage.Where(u => u.DateUsed == Date.NewUtcDate(2018, 3, 13)).ToList();
                usage13032018.First().Determination.Should().Be(AdjudicationStatus.Approved, "Time for 13/03/2018 should be Approved");

                usage15032018 = intermittentCase.Segments[0].AppliedPolicies[0].Usage.Where(u => u.DateUsed == Date.NewUtcDate(2018, 3, 15)).ToList();
                //usage15032018.First().Determination.Should().Be(AdjudicationStatus.Denied, "Time for 15/03/2018 should be Denied");

            }
        }



        [TestMethod, Ignore]
        public void TestBondingStartDate()
        {
            const string CUSTOMER_ID = "000000000000000000000002"; // Test
            const string EMPLOYER_ID = "000000000000000000000002"; // Test
            const string USER_ID = "000000000000000000000002"; // qa@absencesoft.com

            using (var employeeService = new EmployeeService())
            using (var eligService = new EligibilityService())
            using (var caseService = new CaseService(false))
            {


                #region Create employee

                // Create employee
                var employee = employeeService.Update(new Employee()
                {
                    CustomerId = CUSTOMER_ID,
                    EmployerId = EMPLOYER_ID,

                    EmployeeNumber = "PS-595-TEST",
                    FirstName = "PS-595",
                    LastName = "Person",
                    Gender = Gender.Female,
                    HireDate = "09/13/1999".ToDate(),
                    ServiceDate = "01/21/2018".ToDate(),
                    Meets50In75MileRule = false,
                    IsKeyEmployee = false,
                    IsExempt = false,
                    Status = EmploymentStatus.Active,
                    MilitaryStatus = MilitaryStatus.Civilian,
                    WorkCountry = "US",
                    WorkState = "NY",
                    EmployeeClassCode = WorkType.FullTime
                },
                new Schedule()
                {
                    ScheduleType = ScheduleType.Weekly,
                    StartDate = "11/10/2017".ToDate(),
                    Times = new List<Time>(7)
                    {
                        new Time() { SampleDate = "11/05/2017".ToDate(), TotalMinutes = 0 },
                        new Time() { SampleDate = "11/06/2017".ToDate(), TotalMinutes = 480 },
                        new Time() { SampleDate = "11/07/2017".ToDate(), TotalMinutes = 480 },
                        new Time() { SampleDate = "11/08/2017".ToDate(), TotalMinutes = 480 },
                        new Time() { SampleDate = "11/09/2017".ToDate(), TotalMinutes = 480 },
                        new Time() { SampleDate = "11/10/2017".ToDate(), TotalMinutes = 480 },
                        new Time() { SampleDate = "11/11/2017".ToDate(), TotalMinutes = 0 }
                    }
                });

                #endregion Create employee

                var nyPfl = Policy.GetByCode("NYPFL");
                var nyStd = Policy.GetByCode("NY-STD");


                #region Create consecutive case

                // Create consecutive case, 
                var consecutiveCase = caseService.CreateCase(
                    CaseStatus.Open,
                    employee.Id,
                    "01/05/2021".ToDate(),
                    "11/25/2021".ToDate(),
                    CaseType.Consecutive,
                    AbsenceReason.GetByCode("PREGMAT").Code,
                    null,
                    "2017-0110597",
                    null,
                    null);

                consecutiveCase.Metadata.SetRawValue("WillUseBonding", true);
                DateTime deliveryDate = new DateTime(2021, 7, 1);
                DateTime bondingStartDate = new DateTime(2021, 5, 16);
                DateTime bondingEndDate = new DateTime(2021, 6, 13);
                consecutiveCase.SetCaseEvent(CaseEventType.DeliveryDate, deliveryDate);
                consecutiveCase.SetCaseEvent(CaseEventType.BondingStartDate, bondingStartDate);
                consecutiveCase.SetCaseEvent(CaseEventType.BondingEndDate, bondingEndDate);

                consecutiveCase.AssignedToId = USER_ID;
                eligService.RunEligibility(consecutiveCase);

                consecutiveCase.Segments.Should().HaveCount(1);

                var usageNyStd = consecutiveCase.Segments[0].AppliedPolicies.Where(ap => ap.Policy.Code == nyStd.Code).FirstOrDefault();
                usageNyStd.Should().NotBeNull();
                var usage13032018 = usageNyStd.Usage.Where(u => u.DateUsed == Date.NewUtcDate(2021, 5, 16)).ToList();
                usage13032018.First().Determination.Should().Be(AdjudicationStatus.Pending, "Time for 16/05/2021 should be Pending");
                
                // Approve the FMLA
                consecutiveCase = caseService.ApplyDetermination(
                    consecutiveCase,
                    nyStd.Code,
                    consecutiveCase.StartDate,
                    consecutiveCase.EndDate.Value,
                    AdjudicationStatus.Denied,
                    AdjudicationDenialReason.Exhausted);

                usageNyStd = consecutiveCase.Segments[0].AppliedPolicies.Where(ap => ap.Policy.Code == nyStd.Code).FirstOrDefault();

                usage13032018 = usageNyStd.Usage.Where(u => u.DateUsed == Date.NewUtcDate(2021, 5, 16)).ToList();
                usage13032018.First().Determination.Should().Be(AdjudicationStatus.Denied, "Time for 16/05/2021 should be Denied");

                var usageNyPfl = consecutiveCase.Segments[0].AppliedPolicies.Where(ap => ap.Policy.Code == nyPfl.Code).FirstOrDefault();

                var usage16052021 = usageNyPfl.Usage.Where(u => u.DateUsed == Date.NewUtcDate(2021, 5, 16)).ToList();
                usage16052021.First().Determination.Should().Be(AdjudicationStatus.Pending, "Time for 16/05/2021 should be Pending");

                var usage13062021 = usageNyPfl.Usage.Where(u => u.DateUsed == Date.NewUtcDate(2021, 6, 13)).ToList();
                usage16052021.First().Determination.Should().Be(AdjudicationStatus.Pending, "Time for 13/06/2021 should be Pending");
                #endregion Create consecutive case


            }
        }
    }
}
