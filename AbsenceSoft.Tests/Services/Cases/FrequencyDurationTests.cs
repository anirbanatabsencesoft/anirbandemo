﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Cases;
using System.Collections.Generic;
using AbsenceSoft.Data.Enums;
using System.Linq;
using System.Linq.Expressions;
using FluentAssertions;

namespace AbsenceSoft.Tests.Services.Cases
{
    [TestClass]
    public class FrequencyDurationTests
    {
        private static List<IntermittentTimeRequest> requests;

        [TestInitialize]
        public void TestInitialize()
        {
            requests = new List<IntermittentTimeRequest>();
        }

        [TestMethod]
        public void DurationExceededTests()
        {
            requests.GetExceededDurations(Unit.Weeks, 1, Unit.Hours).Should().HaveCount(0);
            requests.GetExceededDurations(Unit.Months, 1, Unit.Hours).Should().HaveCount(0);
            requests.GetExceededDurations(Unit.Years, 1, Unit.Hours).Should().HaveCount(0);
            //
            // month 1
            //
            // week 1
            requests.Add(new IntermittentTimeRequest { RequestDate = new DateTime(2015, 11, 2), TotalMinutes = 60 });
            requests.Add(new IntermittentTimeRequest { RequestDate = new DateTime(2015, 11, 3), TotalMinutes = 60 });
            requests.Add(new IntermittentTimeRequest { RequestDate = new DateTime(2015, 11, 4), TotalMinutes = 60 });

            requests.GetExceededDurations(Unit.Weeks, 1, Unit.Hours).Should().HaveCount(0);
            requests.GetExceededDurations(Unit.Months, 1, Unit.Hours).Should().HaveCount(0);
            requests.GetExceededDurations(Unit.Years, 1, Unit.Hours).Should().HaveCount(0);

            requests.Add(new IntermittentTimeRequest { RequestDate = new DateTime(2015, 11, 5), TotalMinutes = 61 });
            requests.GetExceededDurations(Unit.Weeks, 1, Unit.Hours).Should().HaveCount(1);
            requests.GetExceededDurations(Unit.Months, 1, Unit.Hours).Should().HaveCount(1);
            requests.GetExceededDurations(Unit.Years, 1, Unit.Hours).Should().HaveCount(1);

            requests.Add(new IntermittentTimeRequest { RequestDate = new DateTime(2015, 11, 6), TotalMinutes = 61 });

            requests.GetExceededDurations(Unit.Weeks, 1, Unit.Hours).Should().HaveCount(2);
            requests.GetExceededDurations(Unit.Months, 1, Unit.Hours).Should().HaveCount(2);
            requests.GetExceededDurations(Unit.Years, 1, Unit.Hours).Should().HaveCount(2);

        }

        [TestMethod]
        public void FrequencyExceededTests()
        {
            requests.GetExceededFrequencies(1, Unit.Weeks).Should().HaveCount(0);
            requests.GetExceededFrequencies(1, Unit.Months).Should().HaveCount(0);
            requests.GetExceededFrequencies(1, Unit.Years).Should().HaveCount(0);
            //
            // month 1
            //
            // week 1
            requests.Add(new IntermittentTimeRequest { RequestDate = new DateTime(2015, 11, 2) });
            requests.Add(new IntermittentTimeRequest { RequestDate = new DateTime(2015, 11, 3) });
            requests.Add(new IntermittentTimeRequest { RequestDate = new DateTime(2015, 11, 4) });

            requests.GetExceededFrequencies(3, Unit.Weeks).Should().HaveCount(0);
            requests.GetExceededFrequencies(3, Unit.Months).Should().HaveCount(0);
            requests.GetExceededFrequencies(3, Unit.Years).Should().HaveCount(0);
            //
            // week 2
            requests.Add(new IntermittentTimeRequest { RequestDate = new DateTime(2015, 11, 17) });

            requests.GetExceededFrequencies(3, Unit.Weeks).Should().HaveCount(0);
            requests.GetExceededFrequencies(3, Unit.Months).Should().HaveCount(1);
            requests.GetExceededFrequencies(4, Unit.Months).Should().HaveCount(0);
            requests.GetExceededFrequencies(4, Unit.Years).Should().HaveCount(0);
            //
            // month 2
            //
            // week 1
            requests.Add(new IntermittentTimeRequest { RequestDate = new DateTime(2015, 12, 7) });
            requests.Add(new IntermittentTimeRequest { RequestDate = new DateTime(2015, 12, 8) });
            requests.Add(new IntermittentTimeRequest { RequestDate = new DateTime(2015, 12, 9) });

            requests.GetExceededFrequencies(3, Unit.Weeks).Should().HaveCount(0);
            requests.GetExceededFrequencies(2, Unit.Weeks).Should().HaveCount(2);
            requests.GetExceededFrequencies(3, Unit.Months).Should().HaveCount(1);
            requests.GetExceededFrequencies(2, Unit.Months).Should().HaveCount(2);
            requests.GetExceededFrequencies(10, Unit.Years).Should().HaveCount(0);

            var actual = requests.GetExceededFrequencies(1, Unit.Years);
            actual.Should().HaveCount(1);
            actual.First().Difference.Should().Be(requests.Count - 1);

        }
    }
}
