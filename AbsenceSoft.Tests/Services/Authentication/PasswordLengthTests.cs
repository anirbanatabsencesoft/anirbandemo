﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Data.Customers;
using System.Collections.Generic;
using FluentAssertions;

namespace AbsenceSoft.Tests.Services.Authentication
{
    [TestClass]
    public class PasswordLengthTests
    {
        AuthenticationService sut;
        PasswordPolicy policy;
        ICollection<string> errors;

        [TestInitialize]
        public void TestInitialize()
        {
            sut = new AuthenticationService();
            policy = new PasswordPolicy();
            errors = new List<string>();
        }


        [TestMethod]
        [TestCategory("Password Policy")]
        public void AppliesPolicyWhenSpecified()
        {
            policy.MinimumLength = 12;
            TestPassword("!Password111").Should().BeTrue();
            TestPassword("!Password11").Should().BeFalse();
        }

        [TestMethod]
        [TestCategory("Password Policy")]
        public void AppliesDefaultLengthWhenNotSpecified()
        {
            TestPassword("!Passwo1").Should().BeTrue();
            TestPassword("!Passw1").Should().BeFalse();
        }

        private bool TestPassword(string password)
        {
            return sut.ValidatePasswordAgainstPolicy(policy, new AbsenceSoft.Data.Security.User(), password, out errors);
        }
    }
}
