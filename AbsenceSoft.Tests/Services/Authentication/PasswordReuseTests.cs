﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Data.Customers;
using System.Collections.Generic;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data;

namespace AbsenceSoft.Tests.Services.Authentication
{
    [TestClass]
    public class PasswordReuseTests
    {
        AuthenticationService sut;
        PasswordPolicy policy;
        ICollection<string> errors;
        User user;
        DateTime now;

        const string pwd1 = "!Password11111111";
        const string pwd2 = "!Password22222222";

        [TestInitialize]
        public void TestInitialize()
        {
            sut = new AuthenticationService();
            policy = new PasswordPolicy();
            errors = new List<string>();
            user = new User();
            now = DateTime.UtcNow;
        }

        [TestMethod]
        [TestCategory("Password Policy")]
        public void EnforcesDaysReusePolicy()
        {
            TestPassword(pwd1).Should().BeTrue();

            policy.ReuseLimitDays = 1;
            AddRandomPasswordsToHistory();
            AddPasswordToHistory(pwd1);
            TestPassword(pwd1).Should().BeFalse();

            AddPasswordToHistory(pwd2, policy.ReuseLimitDays + 1);
            TestPassword(pwd2).Should().BeTrue();
        }

        [TestMethod]
        [TestCategory("Password Policy")]
        public void EnforcesCountsReusePolicy()
        {
            TestPassword(pwd1).Should().BeTrue();

            policy.ReuseLimitCount = 5;
            AddRandomPasswordsToHistory(policy.ReuseLimitCount - 1, 100);
            AddPasswordToHistory(pwd1, 50);
            TestPassword(pwd1).Should().BeFalse();

            AddRandomPasswordsToHistory(policy.ReuseLimitCount + 1, 25);
            TestPassword(pwd1).Should().BeTrue();
        }

        private void AddPasswordToHistory(string password, int daysOld = 0)
        {
            user.Passwords.Add(new UserPassword
            {
                Password = new CryptoString(password),
                CreatedDate = now.AddDays(-daysOld)
            });
        }

        private void AddRandomPasswordsToHistory(int count = 5, int daysOld = 0)
        {
            for (int i = 0; i < count; i++)
            {
                user.Passwords.Add(new UserPassword
                {
                    Password = new CryptoString(Guid.NewGuid().ToString()),
                    CreatedDate = now.AddDays(-daysOld)
                });
            }
        }

        private bool TestPassword(string password)
        {
            var result = sut.ValidatePasswordAgainstPolicy(policy, user, password, out errors);
            foreach (var error in errors)
            {
                Console.WriteLine("ERROR: {0}", error);
            }
            return result;
        }
    }
}
