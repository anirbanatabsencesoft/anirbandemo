﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Customers;
using System.Web;
using System.Collections.Generic;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Security;

namespace AbsenceSoft.Tests.Services.Authentication
{
    [TestClass]
    public class DisabledPermissionTest
    {
        private const string CustomerId = "000000000000000000000002";

        [TestMethod]
        public void PermissionIsNotDisabledWhenEmployerIsNull()
        {
            bool isDisabled = Permission.IsDisabled(Permission.CreateCase, null);
            Assert.IsFalse(isDisabled);
        }

        [TestMethod]
        public void PermissionIsNotDisabledWhenEmployerIsNotDisabled()
        {
            var notDisabledEmployer = new Employer();
            bool isDisabled = Permission.IsDisabled(Permission.CreateCase, notDisabledEmployer);
            Assert.IsFalse(isDisabled);
        }

        [TestMethod]
        public void PermissionIsDisabledWhenEmployerIsDisabled()
        {
            var disabledEmployer = new Employer()
            {
                StartDate = DateTime.Today.AddDays(1)
            };
            bool isDisabled = Permission.IsDisabled(Permission.CreateCase, disabledEmployer);
            Assert.IsTrue(isDisabled);
        }

        [TestMethod]
        public void EmployerIsNotReturnedForCreateCaseWhenEmployerIsDisabled()
        {
            using (var employerService = new EmployerService())
            using (var authenticationService = new AuthenticationService())
            {
                var absencesoftTest = AbsenceSoft.Data.Customers.Customer.GetById(CustomerId);
                var newTestEmployer = employerService.CreateEmployer(absencesoftTest);
                newTestEmployer.StartDate = DateTime.Today.AddDays(1);
                newTestEmployer.Save();
                var testUser = authenticationService.CreateUser("testuser.disabledemployer@test.com", "blahblah", "Test", "User", Role.SystemAdministrator.Id);
                testUser.CustomerId = CustomerId;
                authenticationService.UpdateUser(testUser);
                List<Employer> employers = employerService.GetEmployersForUser(testUser, Permission.CreateCase.Id);
                bool employersContainsDisabledTestEmployer = employers.Any(e => e.Id == newTestEmployer.Id);
                Assert.IsFalse(employersContainsDisabledTestEmployer);
            }
        }
    }
}
