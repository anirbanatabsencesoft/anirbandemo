﻿using System;
using System.Collections.Generic;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AbsenceSoft.Tests.Services.Authentication
{
    [TestClass]
    public class ResetPasswordTest
    {
        
        readonly string ResetKey = "";
        readonly string newPassword = "";
        List<User> userListPass = null;
        List<User> userListFail = null;

        public ResetPasswordTest()
        {
            userListPass = new List<User>()
            {
                new User(){ FirstName = "User1", LastName = "UL1", ResetKey = "Reset11", ResetKeyDate = DateTime.Now.AddHours(-8) },
                new User(){ FirstName = "User2", LastName = "UL2", ResetKey = "Reset22", ResetKeyDate = DateTime.Now.AddHours(-20) },
                new User(){ FirstName = "User4", LastName = "UL4", ResetKey = "Reset44", ResetKeyDate = DateTime.Now.AddHours(-23) }
            };

            userListFail = new List<User>()
            {
                new User(){ FirstName = "User1", LastName = "UL1", ResetKey = "Reset11", ResetKeyDate = DateTime.Now.AddHours(-48) },
                new User(){ FirstName = "User4", LastName = "UL4", ResetKey = "Reset44", ResetKeyDate = DateTime.Now.AddHours(-25) },
                new User(){ FirstName = "User5", LastName = "UL5", ResetKey = "Reset55", ResetKeyDate = DateTime.Now.AddHours(-60) },
            };
        }

        [TestMethod]
        public void ResetPasswordPositive()
        {
            Assert.IsNotNull(userListPass);
            foreach (var user in userListPass)
            {
                Assert.IsTrue(IsResetPasswordOk(user.ResetKey));
            }
        }

        [TestMethod]
        public void ResetPasswordNegative()
        {
            Assert.IsNotNull(userListFail);
            foreach (var user in userListFail)
            {
                Assert.IsFalse(IsResetPasswordFailure(user.ResetKey));
            }
        }

        private bool IsResetPasswordOk(string resetKey)
        {
            var selectedUser = userListPass.Find(p => p.ResetKey == resetKey);
            
            Assert.IsNotNull(selectedUser?.ResetKeyDate);

            var isDateInRange = (selectedUser?.ResetKeyDate <= DateTime.Now && selectedUser?.ResetKeyDate > DateTime.Now.AddHours(-24));
            Assert.IsTrue(isDateInRange);
            return isDateInRange;
        }

        private bool IsResetPasswordFailure(string resetKey)
        {
            var selectedUser = userListFail.Find(p => p.ResetKey == resetKey);
            
            Assert.IsNotNull(selectedUser?.ResetKeyDate);

            var isDateInRange = (selectedUser?.ResetKeyDate <= DateTime.Now && selectedUser?.ResetKeyDate > DateTime.Now.AddHours(-24));
            Assert.IsFalse(isDateInRange);
            return isDateInRange;
        }
    }
}
