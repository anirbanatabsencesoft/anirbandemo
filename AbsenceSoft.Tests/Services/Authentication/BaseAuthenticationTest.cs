﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Tests.Services.Authentication
{
    public abstract class BaseAuthenticationTest
    {
        protected const string TestUser1Email = "test.login@absencesoft.com";
        protected const string TestUser1Password = "T3stL0gin";

        protected const string ESSUserEmail = "ess.login@absencesoft.com";
        protected const string ESSUserPassword = "T3stL0gin";

        protected const string TestCustomer1Id = "000000000000000000000002";

        protected static AbsenceSoft.Data.Customers.Customer GetTestCustomer1(Action<AbsenceSoft.Data.Customers.Customer> modify = null)
        {
            var c = AbsenceSoft.Data.Customers.Customer.GetById(TestCustomer1Id);
            if (c != null && modify != null)
            {
                modify(c);
                c.Save();
            }
            return c;
        }

        protected static AbsenceSoft.Data.Customers.Customer CreateTestCustomer1(Action<AbsenceSoft.Data.Customers.Customer> mutator = null)
        {
            CleanUpTestCustomer1();
            var c = new AbsenceSoft.Data.Customers.Customer
            {
                Id = TestCustomer1Id
            };
            if (mutator != null)
            {
                mutator(c);
            }
            c.Save();
            return c;
        }

        protected static User GetTestUser1(Action<User> modify = null)
        {
            var user = User.AsQueryable().Where(u => u.Email == "test.login@absencesoft.com").FirstOrDefault();
            if(user != null)
            {
                if(modify != null)
                {
                    modify(user);
                    user.Save();
                }
            }
            return user;
        }

        protected static User GetESSUser(Action<User> modify = null)
        {
            var user = User.AsQueryable().Where(u => u.Email == "ess.login@absencesoft.com").FirstOrDefault();
            if (user != null)
            {
                if (modify != null)
                {
                    modify(user);
                    user.Save();
                }
            }
            return user;
        }

        protected static User CreateTestUser1(Action<User> mutator = null)
        {
            CleanUpTestUser1();

            User user = new User();
            user.Id = "123000000000000000000000";
            user.Email = "test.login@absencesoft.com";
            user.FirstName = "Developer";
            user.CreatedById = "000000000000000000000000";
            user.ModifiedById = "000000000000000000000000";
            user.Password = new CryptoString() { PlainText = "T3stL0gin" };
            user.Roles = new List<string>() { Role.SystemAdministrator.Id };
            user.CustomerId = "000000000000000000000002";

            if (mutator != null)
            {
                mutator(user);
            }
            user.Save();

            return user;
        }

        protected static User CreateTestESSUser(Action<User> mutator = null)
        {
            CleanUpEssUser();

            User user = new User();
            user.Id = "124000000000000000000000";
            user.Email = ESSUserEmail;
            user.FirstName = "Developer";
            user.UserType = UserType.SelfService;
            user.CreatedById = "000000000000000000000000";
            user.ModifiedById = "000000000000000000000000";
            user.Password = new CryptoString() { PlainText = ESSUserPassword };
            user.Roles = new List<string>() { Role.SystemAdministrator.Id };
            user.CustomerId = "000000000000000000000002";
            user.Employers.Add(new EmployerAccess()
            {
                EmployerId = "000000000000000000000002"
            });
            if (mutator != null)
            {
                mutator(user);
            }
            user.Save();

            return user;
        }

        protected static void CleanUpTestCustomer1()
        {
            var c = GetTestCustomer1();
            if (c != null)
            {
                c.Delete();
            }
        }

        protected static void CleanUpTestUser1()
        {
            var user = GetTestUser1();
            if (user != null)
            {
                user.Delete();
            }
        }

        protected static void CleanUpEssUser()
        {
            var user = GetESSUser();
            if (user != null)
                user.Delete();
        }
    }
}
