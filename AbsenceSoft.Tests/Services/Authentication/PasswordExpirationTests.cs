﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Data.Security;
using FluentAssertions;
using AbsenceSoft.Data.Enums;
using System.Linq;
using System.Linq.Expressions;
using AbsenceSoft.Logic.Administration;

namespace AbsenceSoft.Tests.Services.Authentication
{
    [TestClass]
    public class PasswordExpirationTests : BaseAuthenticationTest
    {
        const int PasswordExpirationDays = 90;
        AuthenticationService sut;
        static DateTime testDate = DateTime.UtcNow;

        [TestInitialize]
        public void TestInitialize()
        {
            CreateTestCustomer1(c =>
            {
                c.PasswordPolicy.ExpirationDays = PasswordExpirationDays;
            });
            CreateTestUser1();
            sut = new AuthenticationService();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            sut.Dispose();
        }

        [TestMethod]
        [TestCategory("Password Policy")]
        public void AuthShouldMigratePasswordAndStillAuthSubsequently()
        {
            TheLoginResult().Should().Be(LoginResult.Ok);
            var u = GetTestUser1();
            u.Passwords.Should().HaveCount(1);
            //
            // NOTE: Test could fail if ran at midnight.
            // need to be able to stub a DTProvider
            u.Passwords.First().CreatedDate.Value.Date.Should().Be(u.CreatedDate.Date);
            u.Passwords.First().Password.Hashed.Should().Be(u.Password.Hashed);
            TheLoginResult().Should().Be(LoginResult.Ok);
        }

        [TestMethod]
        [TestCategory("Password Policy")]
        public void AuthPassesWhenDisabled()
        {
            CreateTestCustomer1(c =>
            {
                c.PasswordPolicy.ExpirationDays = 0;
            });
            CreateTestUser1(u =>
            {
                u.Passwords.Add(new UserPassword
                {
                    Password = u.Password,
                    CreatedDate = DateTime.MinValue
                });
            });
            TheLoginResult().Should().Be(LoginResult.Ok);
        }

        [TestMethod]
        [TestCategory("Password Policy")]
        public void UserWithEndDateIsDisabled()
        {
            var user = CreateTestUser1();
            user.EndDate = DateTime.Now.AddDays(-1);
            user.IsDisabled.Should().BeTrue();
        }

        [TestMethod]
        [TestCategory("Password Policy")]
        public void UserWithEndDateInFutureIsNotDisabled()
        {
            var user = CreateTestUser1();
            user.EndDate = DateTime.Now.AddDays(1);
            user.IsDisabled.Should().BeFalse();
        }

        [TestMethod]
        [TestCategory("Password Policy")]
        public void NewUserIsNotDisabled()
        {
            var user = CreateTestUser1();
            user.IsDisabled.Should().BeFalse();
        }

        [TestMethod]
        [TestCategory("Password Policy")]
        public void AuthPassesWhenNotExpired()
        {
            TheLoginResult().Should().Be(LoginResult.Ok);
        }

        [TestMethod]
        [TestCategory("Password Policy")]
        public void AuthFailsWhenExpired()
        {
            var user = GetTestUser1(u =>
            {
                u.Passwords.FirstOrDefault().CreatedDate = testDate
                        .Subtract(TimeSpan.FromDays(PasswordExpirationDays))
                        .Subtract(TimeSpan.FromSeconds(1d));
            });
            TheLoginResult().Should().Be(LoginResult.Expired);
        }
                
        [TestMethod]
        [Ignore]
        [TestCategory("Password Policy")]
        public void ChangingPasswordAppendsToHistory()
        {
            const string NewPwd = "T3stL0gin2";
            var user = CreateTestUser1();
            string oldPwd = user.Password.Hashed;

            using (var svc = new AdministrationService())
            {
                svc.ChangePassword(user, NewPwd);
            }
            user = GetTestUser1();
            user.Passwords.Should().HaveCount(2);
            user.Passwords
                .OrderByDescending(o=>o.CreatedDate)
                .First().Password.Hashed.Should().Be(user.Password.Hashed);

        }

        private LoginResult TheLoginResult()
        {
            User actual;
            return sut.Login(TestUser1Email, TestUser1Password, out actual);
        }

    }
}
