﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace AbsenceSoft.Tests.Services.Authentication
{
    [TestClass]
    public class UserFlagsTests : BaseAuthenticationTest
    {
        [TestMethod]
        public void PersistsChangedPasswordFlag()
        {
            var u = CreateTestUser1();
            //
            // default
            u.MustChangePassword.Should().BeFalse();
            //
            // set
            u.MustChangePassword = true;
            u.Save();
            u = GetTestUser1();
            u.MustChangePassword.Should().BeTrue();
            //
            // unset
            u.MustChangePassword = false;
            u.Save();
            u = GetTestUser1();
            u.MustChangePassword.Should().BeFalse();
        }
    }
}
