﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;
using System.Collections.Generic;
using System.Linq;
using AbsenceSoft.Data;
using System.Web;
using AbsenceSoft.Data.Customers;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;
using AT.Logic.Authentication;
using Moq;
using Microsoft.Owin.Security;

namespace AbsenceSoft.Tests.Services.Authentication
{
    [TestClass]
    public class AuthenticationTest : BaseAuthenticationTest
    {

        [TestMethod]
        public void UserLogin()
        {
            var loginUser = CreateTestUser1();

            User user = null;
            var result = new AuthenticationService().Using(s => s.Login(TestUser1Email, TestUser1Password, out user));
            Assert.IsNotNull(user);
            Assert.AreEqual(user.Id, loginUser.Id);
            Assert.AreEqual(result, LoginResult.Ok);

            loginUser.Delete();
        }

        [TestMethod]
        public void ESSLogin()
        {
            User essUser = CreateTestESSUser();
            User user = null;
            using (var authService = new AuthenticationService())
            {
                LoginResult loginResult = authService.Login(ESSUserEmail, ESSUserPassword, out user, UserType.SelfService);
                Assert.IsNotNull(user);
                Assert.AreEqual(user.Id, essUser.Id);
                Assert.AreEqual(loginResult, LoginResult.Ok);
            }
        }

        [TestMethod]
        public void PasswordStrength()
        {
            AuthenticationService tas = new AuthenticationService();

            ICollection<string> errors;
            Func<string, bool> exp = (pwd) => tas.ValidatePasswordAgainstPolicy(PasswordPolicy.Default(), new User(), pwd, out errors);

            Assert.IsFalse(exp("123"));
            Assert.IsFalse(exp("123abc"));
            Assert.IsFalse(exp("13abcDE"));
            Assert.IsFalse(exp("ABCDEFGEH"));
            Assert.IsFalse(exp("abcdefgeewq"));
            Assert.IsFalse(exp("12# Abc"));

            Assert.IsTrue(exp("12345$abB"));
            Assert.IsTrue(exp("A bCdef89$"));
            Assert.IsTrue(exp("Imp0rt@nt"));
        }
        
        [TestMethod]
        [Ignore]
        public void WelcomeUserEmail()
        {
            User user = new User
            {
                FirstName = "demo first",
                LastName = "demo last",
                Email = "rshankar@absencesoft.com",
                CustomerId = "57196"
            };
            var authenticationService = new AuthenticationService();
            var result = authenticationService.WelcomeUserEmail(user);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public async Task SelfServiceUserRegistrationTest()
        {
            User existingUser = null;
            try
            {
                existingUser = User.AsQueryable().Where(u => u.Email == "000000002@absencesoft.com").FirstOrDefault();
                if (existingUser != null)
                    existingUser.Delete();
                existingUser = null;

                HttpContext context = HttpContext.Current = new HttpContext(new HttpRequest(null, "http://tempuri.org", null), new HttpResponse(null));
                context.Request.Browser = new HttpBrowserCapabilities() { Capabilities = new Dictionary<string, string> { { "supportsEmptyStringInCookieValue", "false" }, { "cookies", "false" } } };
                context.Items[User.CURRENT_USER_PORTAL_CONTEXT] = "ESS";
                AbsenceSoft.Data.Customers.Customer.Current = AbsenceSoft.Data.Customers.Customer.GetById("000000000000000000000002");
                AbsenceSoft.Data.Customers.Employer.Current = AbsenceSoft.Data.Customers.Employer.GetById("000000000000000000000002");

                string errorMessage = null;
                bool success = false;
                using (AuthenticationService service = new AuthenticationService())
                {
                    var user= service.AuthenticateSso(new HttpContextWrapper(context), "000000002", out errorMessage, null, null);
                    if (user != null)
                    {
                        var authenticationManager = new Mock<IAuthenticationManager>();                        
                        var SignInManager = new ApplicationSignInManager(authenticationManager.Object);
                        success = await SignInManager.SignInSSOAsync(user.Email, AT.Entities.Authentication.ApplicationType.SelfService);
                    }
                }
                Assert.IsTrue(success, "SSO Authentication was not successful");
                Assert.IsNull(errorMessage, errorMessage);

                existingUser = User.Current ?? User.AsQueryable().Where(u => u.Email == "000000002@absencesoft.com").FirstOrDefault();
                Assert.IsNotNull(existingUser, "User account doesn't seem to have been created, uh oh");

                Assert.AreEqual("000000000000000000000002", existingUser.CustomerId);

                EmployerAccess access = existingUser.Employers.FirstOrDefault(e => e.EmployerId == "000000000000000000000002");
                Assert.IsNotNull(access, "Employer access not found");
                Assert.IsFalse(access.AutoAssignCases, "Auto-Assign cases should be false");
                Assert.AreEqual(access.EmployeeId, "000000000000000000000002", "EmployeeId does not match");

                Assert.IsTrue(access.Roles.Count > 0, "Must have at least 1 role assigned to the employer access");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                HttpContext.Current = null;
                if (existingUser != null)
                    existingUser.Delete();
            }
        }

        [TestMethod]
        public async Task SelfServiceUserSSOTestUsingEmployerReferenceCode()
        {
            User existingUser = null;
            try
            {
                existingUser = User.AsQueryable().Where(u => u.Email == "000000002@absencesoft.com").FirstOrDefault();
                if (existingUser != null)
                    existingUser.Delete();
                existingUser = null;

                var employer = AbsenceSoft.Data.Customers.Employer.GetById("000000000000000000000002");
                employer.ReferenceCode = "SKIPPY";
                employer.Save();

                HttpContext context = HttpContext.Current = new HttpContext(new HttpRequest(null, "http://tempuri.org", null), new HttpResponse(null));
                context.Items[User.CURRENT_USER_PORTAL_CONTEXT] = "ESS";
                context.Request.Browser = new HttpBrowserCapabilities() { Capabilities = new Dictionary<string, string> { { "supportsEmptyStringInCookieValue", "false" }, { "cookies", "false" } } };
                AbsenceSoft.Data.Customers.Customer.Current = AbsenceSoft.Data.Customers.Customer.GetById("000000000000000000000002");
                AbsenceSoft.Data.Customers.Employer.Current = employer;
                string errorMessage = null;
                bool success = false;
                using (AuthenticationService service = new AuthenticationService())
                {
                    var user = service.AuthenticateSso(new HttpContextWrapper(context), "000000002", out errorMessage, null, "skippy");
                    if (user != null)
                    {
                        var authenticationManager = new Mock<IAuthenticationManager>();
                        var SignInManager = new ApplicationSignInManager(authenticationManager.Object);
                        success = await SignInManager.SignInSSOAsync(user.Email, AT.Entities.Authentication.ApplicationType.SelfService);
                    }
                }            
                Assert.IsTrue(success, errorMessage ?? "SSO Authentication was not successful");
                Assert.IsNull(errorMessage, errorMessage);

                existingUser = User.Current ?? User.AsQueryable().Where(u => u.Email == "000000002@absencesoft.com").FirstOrDefault();
                Assert.IsNotNull(existingUser, "User account doesn't seem to have been created, uh oh");

                Assert.AreEqual("000000000000000000000002", existingUser.CustomerId);

                EmployerAccess access = existingUser.Employers.FirstOrDefault(e => e.EmployerId == "000000000000000000000002");
                Assert.IsNotNull(access, "Employer access not found");
                Assert.IsFalse(access.AutoAssignCases, "Auto-Assign cases should be false");
                Assert.AreEqual(access.EmployeeId, "000000000000000000000002", "EmployeeId does not match");
            }
            finally
            {
                HttpContext.Current = null;
                if (existingUser != null)
                    existingUser.Delete();
            }
        }

        [TestMethod]
        public async Task SelfServiceUserSSOTestUsingEmployerReferenceCodeAndEmployerContactEmailAddress()
        {
            User existingUser = null;
            string userEmail = "employercontact.dude@absencesoft.com";
            try
            {
                existingUser = User.AsQueryable().Where(u => u.Email == userEmail).FirstOrDefault();
                if (existingUser != null)
                    existingUser.Delete();
                existingUser = null;

                var employer = AbsenceSoft.Data.Customers.Employer.GetById("000000000000000000000002");
                employer.ReferenceCode = "SKIPPY";
                employer.Save();

                employer.Customer.AddFeature(Feature.EmployerContact);
                employer.Customer.Save();

                EmployerContactType type = EmployerContactType.AsQueryable().FirstOrDefault(t => t.CustomerId == employer.CustomerId && t.Code == "PEANUT-BUTTER") ?? new EmployerContactType()
                {
                    Code = "PEANUT-BUTTER",
                    CustomerId = employer.CustomerId,
                    Name = "Peanut Butter"
                }.Save();

                EmployerContact contact = EmployerContact.AsQueryable().FirstOrDefault(t => t.CustomerId == employer.CustomerId && t.EmployerId == employer.Id && t.Contact.Email == userEmail) ?? new EmployerContact()
                {
                    Contact = new Contact()
                    {
                        FirstName = "Mister",
                        LastName = "Mann",
                        Email = userEmail
                    },
                    CustomerId = employer.CustomerId,
                    EmployerId = employer.Id
                };
                contact.ContactTypeCode = type.Code;
                contact.ContactTypeName = type.Name;
                contact.Save();

                Role chunky = Role.AsQueryable().Where(r => r.CustomerId == employer.CustomerId && r.Name == "Chunky Style" && r.Type == RoleType.SelfService).FirstOrDefault() ?? new Role()
                {
                    CustomerId = employer.CustomerId,
                    Name = "Chunky Style",
                    Type = RoleType.SelfService
                };
                chunky.ContactTypes = new List<string>(1) { "PEANUT-BUTTER" };
                chunky.Permissions = new List<string>(2)
                {
                    Permission.ViewAllEmployees.Id,
                    Permission.EmployeeDashboard.Id
                };
                chunky.Save();

                HttpContext context = HttpContext.Current = new HttpContext(new HttpRequest(null, "http://tempuri.org", null), new HttpResponse(null));
                context.Items[User.CURRENT_USER_PORTAL_CONTEXT] = "ESS";
                context.Request.Browser = new HttpBrowserCapabilities() { Capabilities = new Dictionary<string, string> { { "supportsEmptyStringInCookieValue", "false" }, { "cookies", "false" } } };
                AbsenceSoft.Data.Customers.Customer.Current = AbsenceSoft.Data.Customers.Customer.GetById("000000000000000000000002");
                AbsenceSoft.Data.Customers.Employer.Current = employer;
                string errorMessage = null;
                bool success = false;
                using (AuthenticationService service = new AuthenticationService())
                {
                    var user = service.AuthenticateSso(new HttpContextWrapper(context), userEmail, out errorMessage, null, "skippy");
                    if (user != null)
                    {
                        var authenticationManager = new Mock<IAuthenticationManager>();
                        var SignInManager = new ApplicationSignInManager(authenticationManager.Object);
                        success = await SignInManager.SignInSSOAsync(user.Email, AT.Entities.Authentication.ApplicationType.SelfService);
                    }
                }
                
                Assert.IsTrue(success, errorMessage ?? "SSO Authentication was not successful");
                Assert.IsNull(errorMessage, errorMessage);

                existingUser = User.Current ?? User.AsQueryable().Where(u => u.Email == userEmail).FirstOrDefault();
                Assert.IsNotNull(existingUser, "User account doesn't seem to have been created, uh oh");

                Assert.AreEqual("000000000000000000000002", existingUser.CustomerId);

                EmployerAccess access = existingUser.Employers.FirstOrDefault(e => e.EmployerId == "000000000000000000000002");
                Assert.IsNotNull(access, "Employer access not found");
                Assert.IsFalse(access.AutoAssignCases, "Auto-Assign cases should be false");
                Assert.IsNull(access.EmployeeId, "EmployeeId should be null");
                Assert.IsTrue(access.Roles.Contains(chunky.Id), "Should contain the auto-provisioned role based on employer contact type code for role");
            }
            finally
            {
                HttpContext.Current = null;
                if (existingUser != null)
                    existingUser.Delete();
            }
        }


        [TestMethod]
        public async Task SelfServiceUserSSOTestUsingEmployerReferenceCodeAndEmployerContactEmailAddressWhoIsAlsoAnEmployee()
        {
            User existingUser = null;
            string userEmail = "employercontact.person@absencesoft.com";
            string oldEmail = null;
            Employee peterPan = null;
            try
            {
                existingUser = User.AsQueryable().Where(u => u.Email == userEmail).FirstOrDefault();
                if (existingUser != null)
                    existingUser.Delete();
                existingUser = null;

                var employer = AbsenceSoft.Data.Customers.Employer.GetById("000000000000000000000002");
                employer.ReferenceCode = "SKIPPY";
                employer.Save();

                employer.Customer.AddFeature(Feature.EmployerContact);
                employer.Customer.Save();

                EmployerContactType type = EmployerContactType.AsQueryable().FirstOrDefault(t => t.CustomerId == employer.CustomerId && t.Code == "PEANUT-BUTTER") ?? new EmployerContactType()
                {
                    Code = "PEANUT-BUTTER",
                    CustomerId = employer.CustomerId,
                    Name = "Peanut Butter"
                }.Save();

                EmployerContact contact = EmployerContact.AsQueryable().FirstOrDefault(t => t.CustomerId == employer.CustomerId && t.EmployerId == employer.Id && t.Contact.Email == userEmail) ?? new EmployerContact()
                {
                    Contact = new Contact()
                    {
                        FirstName = "Mister",
                        LastName = "Mann",
                        Email = userEmail
                    },
                    CustomerId = employer.CustomerId,
                    EmployerId = employer.Id
                };
                contact.ContactTypeCode = type.Code;
                contact.ContactTypeName = type.Name;
                contact.Save();

                Role chunky = Role.AsQueryable().Where(r => r.CustomerId == employer.CustomerId && r.Name == "Chunky Style" && r.Type == RoleType.SelfService).FirstOrDefault() ?? new Role()
                {
                    CustomerId = employer.CustomerId,
                    Name = "Chunky Style",
                    Type = RoleType.SelfService
                };
                chunky.ContactTypes = new List<string>(1) { "PEANUT-BUTTER" };
                chunky.Permissions = new List<string>(2)
                {
                    Permission.ViewAllEmployees.Id,
                    Permission.EmployeeDashboard.Id
                };
                chunky.Save();

                peterPan = Employee.GetById("000000000000000000000013"); // This emp is an HR and Supervisor of other employees, a good mirror for Pru's use-case especially.
                oldEmail = peterPan.Info.Email;
                peterPan.Info.Email = userEmail;
                peterPan.Save();

                HttpContext context = HttpContext.Current = new HttpContext(new HttpRequest(null, "http://tempuri.org", null), new HttpResponse(null));
                context.Items[User.CURRENT_USER_PORTAL_CONTEXT] = "ESS";
                context.Request.Browser = new HttpBrowserCapabilities() { Capabilities = new Dictionary<string, string> { { "supportsEmptyStringInCookieValue", "false" }, { "cookies", "false" } } };
                AbsenceSoft.Data.Customers.Customer.Current = AbsenceSoft.Data.Customers.Customer.GetById("000000000000000000000002");
                AbsenceSoft.Data.Customers.Employer.Current = employer;
                string errorMessage = null;
                bool success = false;
                using (AuthenticationService service = new AuthenticationService())
                {
                    var user = service.AuthenticateSso(new HttpContextWrapper(context), userEmail, out errorMessage, null, "skippy");
                    if (user != null)
                    {
                        var authenticationManager = new Mock<IAuthenticationManager>();
                        var SignInManager = new ApplicationSignInManager(authenticationManager.Object);
                        success = await SignInManager.SignInSSOAsync(user.Email, AT.Entities.Authentication.ApplicationType.SelfService);
                    }
                }
                Assert.IsTrue(success, errorMessage ?? "SSO Authentication was not successful");
                Assert.IsNull(errorMessage, errorMessage);

                existingUser = User.Current ?? User.AsQueryable().Where(u => u.Email == userEmail).FirstOrDefault();
                Assert.IsNotNull(existingUser, "User account doesn't seem to have been created, uh oh");

                Assert.AreEqual("000000000000000000000002", existingUser.CustomerId);

                EmployerAccess access = existingUser.Employers.FirstOrDefault(e => e.EmployerId == "000000000000000000000002");
                Assert.IsNotNull(access, "Employer access not found");
                Assert.IsFalse(access.AutoAssignCases, "Auto-Assign cases should be false");
                Assert.IsNotNull(access.EmployeeId, "EmployeeId should not be null");
                Assert.AreNotEqual(access.EmployeeId, "", "EmployeeId should not be an empty string");
                Assert.IsTrue(access.Roles.Contains(chunky.Id), "Should contain the auto-provisioned role based on employer contact type code for role");
            }
            finally
            {
                HttpContext.Current = null;
                if (existingUser != null)
                    existingUser.Delete();
                if (peterPan != null)
                {
                    peterPan.Info.Email = userEmail;
                    peterPan.Save();
                }
            }
        }

    }
}
