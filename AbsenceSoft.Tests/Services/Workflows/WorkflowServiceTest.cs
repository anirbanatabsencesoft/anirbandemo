﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Workflows;
using FluentAssertions;
using AbsenceSoft.Tests.Workflows;
using System.Collections.Generic;

namespace AbsenceSoft.Tests.Services.Workflows 
{
    [TestClass]
    public class WorkflowServiceTest : WorkflowTestsBase
    {
        const string CUSTOMER_ID = "000000000000000000000002";  // Test
        const string EMPLOYER_ID = "000000000000000000000002";  // Test
        const string USER_ID = "000000000000000000000002";  // qa@absencesoft.com

        private WorkflowService workflowservice;
        private string workflowCode = "CaseCreated";  // Case Create workflow code
        private Guid activityId = new Guid("20117E61-860E-4A61-8EDA-555980E4AA48");  // activity 'Is Accommodation Only'
         
        [TestInitialize]
        public void InitTests()
        {
            workflowservice = new WorkflowService(AbsenceSoft.Data.Customers.Customer.GetById(CUSTOMER_ID), Employer.GetById(EMPLOYER_ID), User.GetById(USER_ID));
        }

        [TestMethod]
        public void RunwithWorkflowCodeAndActivityId()
        {
            using (CaseContext c = new CaseContext())
            {
                string wfCode = "QAWORKFLOWCODE";
                string firstToDoTitle = "First ToDo Should be Auto Completed";
                string secondTodoTitle = "Second ToDo Should Be Pending";

                WorkflowActivity firstActivity = new WorkflowActivity()
                {
                    Id = new Guid("0f8fad5b-d9cb-469f-a165-70867728950e"),
                    Name = "First Activity",
                    ActivityId = new CaseReviewActivity().Id
                };

                firstActivity.Metadata.Set("Autocomplete", true);
                firstActivity.Metadata.Set("Title", firstToDoTitle);

                WorkflowActivity secondActivity = new WorkflowActivity()
                {
                    Id = new Guid("7c9e6679-7425-40de-944b-e07fc1f90ae7"),
                    Name = "Second Activity",
                    ActivityId = new CaseReviewActivity().Id
                };

                secondActivity.Metadata.Set("Title", secondTodoTitle);

                Workflow wf = new Workflow()
                {
                    Code = wfCode,
                    Name = "QA Workflow",
                    Description = "Test workflow that should Generate todos",
                    TargetEventType = EventType.CaseCreated,
                    CriteriaSuccessType = RuleGroupSuccessType.And,
                    Activities = new List<WorkflowActivity>()
                    {
                        firstActivity,
                        secondActivity
                    },
                    Transitions = new List<Transition>()
                    {
                        new Transition()
                        {
                            Id = new Guid("81a130d2-502f-4cf1-a376-63edeb000e9f"),
                            SourceActivityId = firstActivity.Id,
                            TargetActivityId = secondActivity.Id,
                            ForOutcome = Activity.CompleteOutcomeValue
                        }
                    }
                };

                wf.Save();

                c.Case.Save();
                workflowservice.Run(wf.Code, c.Case.Id, firstActivity.Id);
                var todos = c.ToDos();
                todos.Count().Should().BeGreaterThan(0);

                var matchingTodo = todos.FirstOrDefault(t => t.Title == firstToDoTitle);
                matchingTodo.Should().NotBeNull();
                matchingTodo.Status.Should().Be(ToDoItemStatus.Complete);

                matchingTodo = todos.FirstOrDefault(t => t.Title == secondTodoTitle);
                matchingTodo.Should().NotBeNull();
                matchingTodo.Status.Should().Be(ToDoItemStatus.Pending);

                wf.Delete();
            }
        }
        
        [TestMethod, ExpectedException(typeof(AbsenceSoftException))]
        public void RunwithWrongWorkflowcode()
        {
            using (CaseContext c = new CaseContext())
            {
                c.Case.Save();
                workflowservice.Run("WRONGWORKFLOWCODE", c.Case.Id, activityId);
            }
        }

        [TestMethod, ExpectedException(typeof(AbsenceSoftException))]
        public void RunwithWrongActivityId()
        {
            using (CaseContext c = new CaseContext())
            {
                c.Case.Save();
                workflowservice.Run(workflowCode, c.Case.Id, new Guid("010398fe-1b1d-4607-bba6-5158f741ecf9"));
            }
        }

        [ TestMethod]
        public void RunwithNoActivityId()
        {
            using (CaseContext c = new CaseContext())
            {
                string wfCode = "QAWORKFLOWCODE";
                string firstToDoTitle = "First ToDo Should be Auto Completed";
                string secondTodoTitle = "Second ToDo Should Be Pending";

                WorkflowActivity firstActivity = new WorkflowActivity()
                {
                    Id = new Guid("0f8fad5b-d9cb-469f-a165-70867728950e"),
                    Name = "First Activity",
                    ActivityId = new CaseReviewActivity().Id
                };
                firstActivity.Metadata.Set("Autocomplete", true);
                firstActivity.Metadata.Set("Title", firstToDoTitle);

                WorkflowActivity secondActivity = new WorkflowActivity()
                {
                    Id = new Guid("7c9e6679-7425-40de-944b-e07fc1f90ae7"),
                    Name = "Second Activity",
                    ActivityId = new CaseReviewActivity().Id
                };

                secondActivity.Metadata.Set("Title", secondTodoTitle);
                Workflow wf = new Workflow()
                {
                    Code = wfCode,
                    Name = "QA Workflow",
                    Description = "Test workflow that should Generate todos",
                    TargetEventType = EventType.CaseCreated,
                    CriteriaSuccessType = RuleGroupSuccessType.And,
                    Activities = new List<WorkflowActivity>()
                    {
                        firstActivity,
                        secondActivity
                    },
                    Transitions = new List<Transition>()
                    {
                        new Transition()
                        {
                            Id = new Guid("81a130d2-502f-4cf1-a376-63edeb000e9f"),
                            SourceActivityId = firstActivity.Id,
                            TargetActivityId = secondActivity.Id,
                            ForOutcome = Activity.CompleteOutcomeValue
                        }
                    }
                };
                wf.Save();

                c.Case.Save();
                workflowservice.Run(wf.Code, c.Case.Id, null);
                var todos = c.ToDos();
                todos.Count().Should().BeGreaterThan(0);

                var matchingTodo = todos.FirstOrDefault(t => t.Title == firstToDoTitle);
                matchingTodo.Should().NotBeNull();
                matchingTodo.Status.Should().Be(ToDoItemStatus.Complete);

                matchingTodo = todos.FirstOrDefault(t => t.Title == secondTodoTitle);
                matchingTodo.Should().NotBeNull();
                matchingTodo.Status.Should().Be(ToDoItemStatus.Pending);

                wf.Delete();
            }
        } 
    }
}
