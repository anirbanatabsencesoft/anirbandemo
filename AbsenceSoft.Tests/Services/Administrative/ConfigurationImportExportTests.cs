﻿using System;
using System.Linq;
using FluentAssertions;
using FluentAssertions.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Policies;
using System.Collections.Generic;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data;
using AbsenceSoft.Logic.Administration.Configuration;
using System.IO;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Common.Properties;

namespace AbsenceSoft.Tests.Services.Administrative
{
    [TestClass]
    public class ConfigurationImportExportTests
    {
        private TestContext testContextInstance;
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get { return testContextInstance; } set { testContextInstance = value; } }

        private ImportExportFile DoExportForTest()
        {
            using (ImportExportService svc = new ImportExportService("000000000000000000000003", null))
            {
                ImportExportSettings settings = new ImportExportSettings()
                {
                    EmployerIds = new List<string>(1) { "000000000000000000000003" },
                    IncludeAbsenceReasons = true,
                    IncludeEmployers = true,
                    IncludeAccommodationQuestions = true,
                    IncludeAccommodationTypes = true,
                    IncludeContactTypes = true,
                    IncludeCustomer = true,
                    IncludeCustomFields = true,
                    IncludePaperwork = true,
                    IncludePaySchedules = true,
                    IncludePolicies = true,
                    IncludeRoles = true,
                    IncludeTemplates = true,
                    IncludeWorkflows = true
                };

                ImportExportFile file = svc.Export(settings);
                file.Should().NotBeNull();
                file.FileName.Should().NotBeNullOrWhiteSpace();
                file.FileLocator.Should().NotBeNullOrWhiteSpace();
                file.File.Should().NotBeNullOrEmpty();
                return file;
            }
        }

        /// <summary>
        /// Tests the configuration export.
        /// </summary>
        /// <param name="context">The context.</param>
        [TestMethod]
        public void TestConfigurationExport()
        {
            using (ImportExportService svc = new ImportExportService("000000000000000000000003", null))
            {
                ImportExportFile file = null;
                try
                {
                    file = DoExportForTest();
                    string savePath = Path.GetTempFileName();
                    try
                    {
                        File.WriteAllBytes(savePath, file.File);
                        File.Exists(savePath).Should().BeTrue();
                    }
                    finally
                    {
                        File.Delete(savePath);
                    }

                    try
                    {
                        savePath = new FileService().Using(fs => fs.DownloadFileToDisk(file.FileLocator, Settings.Default.S3BucketName_Exports));
                        File.Exists(savePath).Should().BeTrue();
                    }
                    finally
                    {
                        File.Delete(savePath);
                    }
                }
                finally
                {
                    if (file != null && !string.IsNullOrWhiteSpace(file.FileLocator))
                        new FileService().Using(fs => fs.DeleteS3Object(file.FileLocator, Settings.Default.S3BucketName_Exports));
                }
            }
        }

        /// <summary>
        /// Tests the configuration export.
        /// </summary>
        /// <param name="context">The context.</param>
        [TestMethod]
        public void TestConfigurationImport()
        {
            ImportExportFile file = null;
            try
            {
                using (ImportExportService svc = new ImportExportService("000000000000000000000003", null))
                {
                    file = DoExportForTest();
                    svc.Import(file);
                }
            }
            finally
            {
                if (file != null && !string.IsNullOrWhiteSpace(file.FileLocator))
                    new FileService().Using(fs => fs.DeleteS3Object(file.FileLocator, Settings.Default.S3BucketName_Exports));
            }
        }
    }
}
