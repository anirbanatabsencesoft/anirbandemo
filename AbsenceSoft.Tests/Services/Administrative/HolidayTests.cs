﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Customers;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Tests.Services.Administrative
{
    [TestClass]
    public class HolidayTests
    {
        [TestMethod]
        public void OrClosestWeekdayTest()
        {
            List<DateTime> holidayList = new HolidayService().GetHolidayList(
                new DateTime(2017, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc), 
                new DateTime(2017, 12, 31, 0, 0, 0, 0, DateTimeKind.Utc), 
                Employer.GetById("000000000000000000000002"));

            holidayList.FirstOrDefault(d => d.Date == makeDate(2017, 1, 2)).Should().Be(makeDate(2017, 1, 2), "New Year's Day 2017 should be observed on the 2nd");
            holidayList.FirstOrDefault(d => d.Date == makeDate(2017, 11, 10)).Should().Be(makeDate(2017, 11, 10), "Veterans Day 2017 should be observed on the 10th");
        }

        private DateTime makeDate(int year, int month, int day) { return new DateTime(year, month, day, 0, 0, 0, 0, DateTimeKind.Utc).Date; }
    }
}
