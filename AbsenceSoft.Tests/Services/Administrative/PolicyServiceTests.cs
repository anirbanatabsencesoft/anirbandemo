﻿using System;
using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Policies;
using System.Collections.Generic;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Data.Rules;

namespace AbsenceSoft.Tests.Services.Administrative
{
    [TestClass]
    public class PolicyServiceTests
    {
        [TestMethod]
        public void GetPolicyRuleTest()
        {
            using (PolicyService svc = new PolicyService("000000000000000000000002", "000000000000000000000002"))
            {
                Guid id = Guid.NewGuid();
                RuleExpression expression = new RuleExpression()
                {
                    ControlType = ControlType.CheckBox,
                    IsFunction = true,
                    Name = "HasMinLengthOfService",
                    Operators = new Dictionary<string, string>() { { "==", "Is" } },
                    Parameters = new List<RuleExpressionParameter>()
                    {
                        new RuleExpressionParameter()
                        {
                            Name = "minLengthOfService",
                            Prompt = "Minimum Length of Service",
                            ControlType = ControlType.Numeric,
                            HelpText = "Enter in the numeric portion of the minimum length of service (i.e. for 3 months, enter in '3' here).",
                            Value = 12
                        },
                        new RuleExpressionParameter()
                        {
                            Name = "unitType",
                            Prompt = "Unit Type",
                            ControlType = ControlType.SelectList,
                            HelpText = "Enter in the unit portion of the minimum length of service (i.e. for 3 months, select 'Months').",
                            Value = Unit.Months
                        }
                    },
                    Prompt = "Minumum length of service",
                    HelpText = "Minimum length of service has been met; provide the length of service and applicable unit type for that duration (e.g. days, months, years, etc.)",
                    RuleDescription = "Minimum length of service has been met",
                    RuleId = id,
                    RuleName = "Minimum hours per week",
                    RuleOperator = "==",
                    Value = true
                };
                Rule rule = svc.GetRule(expression);
                Assert.IsNotNull(rule);
                Assert.AreEqual(id, rule.Id);
                Assert.AreEqual("Minimum hours per week", rule.Name);
                Assert.AreEqual("Minimum length of service has been met", rule.Description);
                Assert.AreEqual("HasMinLengthOfService(12, Unit.Months)", rule.LeftExpression);
                Assert.AreEqual("==", rule.Operator);
                Assert.AreEqual("true", rule.RightExpression);
            }
        }

        [TestMethod]
        public void GetPolicyRuleExpressionTest()
        {
            using (PolicyService svc = new PolicyService("000000000000000000000002", "000000000000000000000002"))
            {
                Guid id = Guid.NewGuid();
                Rule rule = new Rule()
                {
                    Id = id,
                    Name = "Minimum hours per week",
                    Description = "Minimum length of service has been met",
                    LeftExpression = "HasMinLengthOfService(12, Unit.Months)",
                    Operator = "==",
                    RightExpression = "true"
                };
                var expression = svc.GetExpression(rule, svc.GetRuleExpressions());
                Assert.IsNotNull(expression);
                Assert.AreEqual("HasMinLengthOfService", expression.Name);
                Assert.AreEqual(id, expression.RuleId);
                Assert.AreEqual("==", expression.RuleOperator);
                Assert.AreEqual("Minimum hours per week", expression.RuleName);
                Assert.AreEqual("Minimum length of service has been met", expression.RuleDescription);
                Assert.AreEqual("true", expression.StringValue);
                Assert.AreEqual(2, expression.Parameters.Count);
                Assert.AreEqual(2, expression.Operators.Count);
                Assert.IsTrue(expression.IsFunction);
            }
        }
    }
}
