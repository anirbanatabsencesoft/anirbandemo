﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Customers;
using System.Collections.Generic;
using System.Linq;
using AbsenceSoft.Data.Rules;

namespace AbsenceSoft.Tests.Services.Pay
{
    [TestClass]
    public class PayServiceTest
    {
        [TestMethod]
        public void PayScheduleTest()
        {
            // test the various pay schedules

            PaySchedule weekly = new PaySchedule()
            {
                DayStart = 0,
                DayEnd = 6,
                PayPeriodType = PayPeriodType.Weekly
            };

            DateTime weekStart = new DateTime(2014, 09, 28, 0, 0, 0, DateTimeKind.Utc);

            PayScheduleService pss = new PayScheduleService();

            // start with an easy one, 9/28 is sunday, so the first date in the schedule should be Sunday and the last should be saturday
            List<DateTime> dates = pss.PayDatesForSchedule(weekly, new DateTime(2014, 09, 28, 0, 0, 0, DateTimeKind.Utc));
            testWeeklyResuts(dates, weekStart);

            // move to tuesday, should get the same results
            dates = pss.PayDatesForSchedule(weekly, new DateTime(2014, 09, 30, 0, 0, 0, DateTimeKind.Utc));
            testWeeklyResuts(dates, weekStart);

            // move to saturday, and should still be the same
            dates = pss.PayDatesForSchedule(weekly, new DateTime(2014, 10, 04, 0, 0, 0, DateTimeKind.Utc));
            testWeeklyResuts(dates, weekStart);

            // now start the week in the middle
            weekly.DayStart = 2;
            weekly.DayEnd = 1;

            // sunday should roll back to the tuesday start date
            weekStart = new DateTime(2014, 09, 23, 0, 0, 0, DateTimeKind.Utc);
            dates = pss.PayDatesForSchedule(weekly, new DateTime(2014, 09, 28, 0, 0, 0, DateTimeKind.Utc));
            testWeeklyResuts(dates, weekStart);

            PaySchedule biWeekly = new PaySchedule()
            {
                StartDate = new DateTime(2014, 09, 7, 0, 0, 0, DateTimeKind.Utc),
                PayPeriodType = PayPeriodType.BiWeekly
            };
            DateTime biWeeklyStart = new DateTime(2014, 09, 21, 0, 0, 0, DateTimeKind.Utc);
            dates = pss.PayDatesForSchedule(biWeekly, new DateTime(2014, 09, 28, 0, 0, 0, DateTimeKind.Utc));
            testBiWeeklyResuts(dates, biWeeklyStart);

            dates = pss.PayDatesForSchedule(biWeekly, new DateTime(2014, 09, 23, 0, 0, 0, DateTimeKind.Utc));
            testBiWeeklyResuts(dates, biWeeklyStart);

            dates = pss.PayDatesForSchedule(biWeekly, new DateTime(2014, 10, 01, 0, 0, 0, DateTimeKind.Utc));
            testBiWeeklyResuts(dates, biWeeklyStart);

            // now for a monthly test 30 days hath september, april, june and november
            PaySchedule monthly = new PaySchedule()
            {
                PayPeriodType = PayPeriodType.Monthly,
                StartDate = new DateTime(2014, 1, 1, 0, 0, 0, DateTimeKind.Utc)
            };

            dates = pss.PayDatesForSchedule(monthly, new DateTime(2014, 09, 28, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(dates.Count, 30);
            Assert.AreEqual(dates[0], new DateTime(2014, 9, 1, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(dates[29], new DateTime(2014, 9, 30, 0, 0, 0, DateTimeKind.Utc));

            dates = pss.PayDatesForSchedule(monthly, new DateTime(2014, 10, 01, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(dates.Count, 31);
            Assert.AreEqual(dates[0], new DateTime(2014, 10, 1, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(dates[30], new DateTime(2014, 10, 31, 0, 0, 0, DateTimeKind.Utc));

            PaySchedule semiMonthly = new PaySchedule()
            {
                PayPeriodType = PayPeriodType.SemiMonthly,
                DayStart = 1,
                DayEnd = 15
            };

            dates = pss.PayDatesForSchedule(semiMonthly, new DateTime(2014, 09, 28, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(dates.Count, 15);
            Assert.AreEqual(dates[0], new DateTime(2014, 9, 16, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(dates[14], new DateTime(2014, 9, 30, 0, 0, 0, DateTimeKind.Utc));

            dates = pss.PayDatesForSchedule(semiMonthly, new DateTime(2014, 10, 02, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(dates.Count, 15);
            Assert.AreEqual(dates[0], new DateTime(2014, 10, 01, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(dates[14], new DateTime(2014, 10, 15, 0, 0, 0, DateTimeKind.Utc));

            // put the weekly schedule back to normal
            weekly.DayStart = 0;
            weekly.DayEnd = 6;

            // generate 4 weeks of weekly schedule
            List<PaySchedulePayments> paymentDates = pss.PaymentDateRangesForSchedule(weekly, new DateTime(2014, 09, 28, 0, 0, 0, DateTimeKind.Utc), new DateTime(2014, 10, 23, 0, 0, 0, DateTimeKind.Utc));

            // should end on 10-25 and be 4 weeks long
            Assert.AreEqual(paymentDates.Count, 4);
            Assert.AreEqual(paymentDates[3].EndDate, new DateTime(2014, 10, 25, 0, 0, 0, DateTimeKind.Utc));
        }

        private void testWeeklyResuts(List<DateTime> dates, DateTime testStart)
        {
            Assert.AreEqual(dates.Count, 7);

            Assert.AreEqual(dates[0], testStart);
            Assert.AreEqual(dates[6], testStart.AddDays(6));

        }

        private void testBiWeeklyResuts(List<DateTime> dates, DateTime testStart)
        {
            Assert.AreEqual(dates.Count, 14);

            Assert.AreEqual(dates[0], testStart);
            Assert.AreEqual(dates[13], testStart.AddDays(13));

        }


        [TestMethod]
        public void PayScheduleSetTest()
        {
            // this test plays with the default, and rules to set an employee scehdule
            string empId = "000000000000000000000017";

            Employee checkIt = Employee.GetById(empId);

            // get a list of the schedules for this employer
            PayScheduleService pss = new PayScheduleService();

            List<PaySchedule> ps = pss.PayScheduleForEmployer(checkIt.EmployerId);

            PaySchedule theDefault = checkIt.Employer.DefaultPaySchedule;

            // the default should have been set in test data
            Assert.IsNotNull(theDefault);

            // right now there are no rules (or there better not be, if someone added them to test data then they broke the test)
            checkIt.PayScheduleId = null;
            pss.SetDefaultEmployeePaySchedule(checkIt);
            checkIt.Save();

            checkIt = Employee.GetById(empId);

            // the default should be set.... now set a rule on one of the schedules
            Assert.AreEqual(checkIt.PayScheduleId, theDefault.Id);

            // and now check the payment amounts, get the weekly schedule and test that
            PaySchedule biMonthly = PaySchedule.AsQueryable().Where(p => p.Name == "Twice a month" && p.EmployerId == checkIt.EmployerId).FirstOrDefault();
            Assert.IsNotNull(biMonthly);

            // the test data builds the emp with a start date of today - 365, so... set the start date back half a year and it should pick this up
            biMonthly.AssignmentRules = new RuleGroup()
            {
                Name = "Less than a year",
                Rules = new List<Rule>() {
                    new Rule() { LeftExpression = "Employee.IsExempt", RightExpression = "false" , Operator = "==" }
                }
            };

            // the method retrieves the rules so save it
            biMonthly.Save();

            checkIt.PayScheduleId = null;

            pss.SetDefaultEmployeePaySchedule(checkIt);

            Assert.AreEqual(checkIt.PayScheduleId, biMonthly.Id);

            // then put it back in case some other test wants to use it
            biMonthly.AssignmentRules = null;
            biMonthly.Save();

        }

    }
}
