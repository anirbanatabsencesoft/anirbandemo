﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Customers;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Tests.Services.Customer
{
    [TestClass]
    public class CustomerServiceTest
    {
        [TestMethod]
        public void CustomerAddUpdateTest()
        {
            removeCustomer("B-rock");
            removeCustomer("City of Bedrock");

            CustomerService cs = new CustomerService();

            AbsenceSoft.Data.Customers.Customer bedrock = new AbsenceSoft.Data.Customers.Customer()
            {
                Name = "City of Bedrock",
                Url = "bedrock.gov"
            };

            bool thisShouldWork = true;
            try
            {
                cs.Update(bedrock);
            }
            catch (Exception)
            {
                thisShouldWork = false;
            }

            Assert.IsTrue(thisShouldWork);

            AbsenceSoft.Data.Customers.Customer b2 = new AbsenceSoft.Data.Customers.Customer()
            {
                Name = "Brock",
                Url = "bedrock.gov"
            };

            bool thisShouldFail = false;
            try
            {
                cs.Update(b2);
            }
            catch(Exception)
            {
                thisShouldFail = true;
            }

            Assert.IsTrue(thisShouldFail);
            
        }           // public void CustomerAddUpdateTest()

        private void removeCustomer(string name)
        {
            AbsenceSoft.Data.Customers.Customer cust = AbsenceSoft.Data.Customers.Customer.AsQueryable().Where(c=> c.Name == name).FirstOrDefault();

            if(cust != null)
                cust.Delete();

        }
    }
}
