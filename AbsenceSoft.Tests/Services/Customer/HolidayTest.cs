﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Customers;
using System.Collections.Generic;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Tests.Services.Customer
{
    [TestClass]
    public class HolidayTest
    {
        [TestMethod]
        public void TestHolidays()
        {
            List<Holiday> testHolidays =new List<Holiday>()
            {
                new Holiday() { Name = "New Year’s Day",                Month = Month.January,      DayOfMonth = 1, StartDate = new DateTime(2010,1,1), OrClosestWeekday = true },
                new Holiday() { Name = "Martin Luther King’s Birthday", Month = Month.January,      WeekOfMonth = 3,    DayOfWeek = DayOfWeek.Monday, StartDate = new DateTime(2010,1,1) },
                new Holiday() { Name = "George Washington's Birthday",  Month = Month.February,     WeekOfMonth = 3,    DayOfWeek = DayOfWeek.Monday, StartDate = new DateTime(2010,1,1) },
                new Holiday() { Name = "Memorial Day",                  Month = Month.May,          WeekOfMonth = -1,   DayOfWeek = DayOfWeek.Monday, StartDate = new DateTime(2010,1,1) },
                new Holiday() { Name = "Independence Day",              Month = Month.July,         DayOfMonth = 4, StartDate = new DateTime(2010,1,1), OrClosestWeekday = true },
                new Holiday() { Name = "Labor Day",                     Month = Month.September,    WeekOfMonth = 1,    DayOfWeek = DayOfWeek.Monday, StartDate = new DateTime(2010,1,1) },
                new Holiday() { Name = "Columbus Day",                  Month = Month.October,      WeekOfMonth = 2,    DayOfWeek = DayOfWeek.Monday, StartDate = new DateTime(2010,1,1) },
                new Holiday() { Name = "Veterans Day",                  Month = Month.November,     DayOfMonth = 11, StartDate = new DateTime(2010,1,1), OrClosestWeekday = true },
                new Holiday() { Name = "Thanksgiving Day",              Month = Month.November,     WeekOfMonth = 4,    DayOfWeek = DayOfWeek.Thursday, StartDate = new DateTime(2010,1,1) },
                new Holiday() { Name = "Christmas Day",                 Month = Month.December,     DayOfMonth = 25, StartDate = new DateTime(2010,1,1), OrClosestWeekday = true }
            };
            
            HolidayService hs = new HolidayService();
            
            DateTime h;
            Assert.IsTrue(hs.MakeDate(testHolidays[1], 2014, out h));       // mlk day 
            Assert.IsTrue(h.Day == 20 && h.Month == 1 && h.Year == 2014);

            Assert.IsTrue(hs.MakeDate(testHolidays[4], 2014, out h));       // 4th of july
            Assert.IsTrue(h.Day == 4 && h.Month == 7 && h.Year == 2014);

            Assert.IsTrue(hs.MakeDate(testHolidays[3], 2014, out h));       // memorial day 5/26/2014
            Assert.IsTrue(h.Day == 26 && h.Month == 5 && h.Year == 2014);

            Assert.IsTrue(hs.MakeDate(testHolidays[3], 2013, out h));       // memorial day 5/27/2013
            Assert.IsTrue(h.Day == 27 && h.Month == 5 && h.Year == 2013);

            Assert.IsTrue(hs.MakeDate(testHolidays[5], 2014, out h));       // labor day 9/1/2014
            Assert.IsTrue(h.Day == 1 && h.Month == 9 && h.Year == 2014);

            Assert.IsTrue(hs.MakeDate(testHolidays[8], 2014, out h));       // thanksgiving 11/27/2014
            Assert.IsTrue(h.Day == 27 && h.Month == 11 && h.Year == 2014);

            Assert.IsTrue(hs.MakeDate(testHolidays[8], 2011, out h));       // thanksgiving 11/24/2011
            Assert.IsTrue(h.Day == 24 && h.Month == 11 && h.Year == 2011);

            // before start date, boom!
            Assert.IsFalse(hs.MakeDate(testHolidays[8], 2008, out h));       // thanksgiving 11/24/2008 - before start date
            Assert.IsFalse(h.Day == 27 && h.Month == 11 && h.Year == 2008);

            List<DateTime> hols = hs.HolidaysForYear(testHolidays, 2014);
            Assert.IsTrue(testHolidays.Count == hols.Count);

            // same dates as before, just check a couple
            h = hols[5]; // labor day
            Assert.IsTrue(h.Day == 1 && h.Month == 9 && h.Year == 2014);

            h = hols[3];        // memorial day
            Assert.IsTrue(h.Day == 26 && h.Month == 5 && h.Year == 2014);

            
        
        }
    }
}
