﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Tests;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Logic.Tests.Customers
{
    [TestClass]
    public class EmployeeServiceTests
    {
        private Employee jerryJones;

        [TestInitialize]
        public void Initialize()
        {
            // TODO: this code current counts on Setup to have already run. it should NOT be dependent upon that

            #region add data to test
            Role adminRole = Role.AsQueryable().Where(r => r.Name == "Administrator").FirstOrDefault();
            User devUser = User.AsQueryable().Where(u => u.Email == "dev@absencesoft.com").FirstOrDefault();

            Contact demoContact = new Contact()
            {
                Email = "demo@absencesoft.com",
                WorkPhone = "+1.303.332.6852",
                FirstName = "Demo",
                LastName = "Administrator",
                Address = new Address()
                {
                    Address1 = "10200 W 44th AVE",
                    Address2 = "SUITE 140",
                    City = "Wheat Ridge",
                    State = "CO",
                    PostalCode = "80033"
                },
            };

            Customer demoCustomer = Customer.AsQueryable().Where(c => c.Name == "AbsenceSoft Logic Tests").FirstOrDefault() ?? new Customer();
            demoCustomer.Name = "AbsenceSoft Logic Tests";
            demoCustomer.Url = "logictests";
            demoCustomer.Contact = demoContact;
            demoCustomer.CreatedById = devUser.Id;
            demoCustomer.ModifiedById = devUser.Id;
            demoCustomer.Save();

            Employer demoEmployer = Employer.AsQueryable().Where(e => e.ReferenceCode == "logictests").FirstOrDefault() ?? new Employer();
            demoEmployer.Name = "AbsenceSoft Logic Tests";
            demoEmployer.ReferenceCode = "logictests";
            demoEmployer.Contact = demoContact;
            demoEmployer.Customer = demoCustomer;
            demoEmployer.CustomerId = demoCustomer.Id;
            demoEmployer.Ein = new CryptoString("11-111-1113");
            demoEmployer.ResetMonth = Month.January;
            demoEmployer.ResetDayOfMonth = 1;
            demoEmployer.FMLPeriodType = PeriodType.RollingBack;
            demoEmployer.Holidays.Clear();
            demoEmployer.Holidays.AddRange(new List<Holiday>()
            {
                new Holiday() { Name = "New Year’s Day",                Month = Month.January,      DayOfMonth = 1, StartDate = new DateTime(2010,1,1), OrClosestWeekday = true },
                new Holiday() { Name = "Martin Luther King’s Birthday", Month = Month.January,      WeekOfMonth = 3,    DayOfWeek = DayOfWeek.Monday, StartDate = new DateTime(2010,1,1) },
                new Holiday() { Name = "George Washington's Birthday",  Month = Month.February,     WeekOfMonth = 3,    DayOfWeek = DayOfWeek.Monday, StartDate = new DateTime(2010,1,1) },
                new Holiday() { Name = "Memorial Day",                  Month = Month.May,          WeekOfMonth = -1,   DayOfWeek = DayOfWeek.Monday, StartDate = new DateTime(2010,1,1) },
                new Holiday() { Name = "Independence Day",              Month = Month.July,         DayOfMonth = 4, StartDate = new DateTime(2010,1,1), OrClosestWeekday = true },
                new Holiday() { Name = "Labor Day",                     Month = Month.September,    WeekOfMonth = 1,    DayOfWeek = DayOfWeek.Monday, StartDate = new DateTime(2010,1,1) },
                new Holiday() { Name = "Columbus Day",                  Month = Month.October,      WeekOfMonth = 2,    DayOfWeek = DayOfWeek.Monday, StartDate = new DateTime(2010,1,1) },
                new Holiday() { Name = "Veterans Day",                  Month = Month.November,     DayOfMonth = 11, StartDate = new DateTime(2010,1,1), OrClosestWeekday = true },
                new Holiday() { Name = "Thanksgiving Day",              Month = Month.November,     WeekOfMonth = 4,    DayOfWeek = DayOfWeek.Thursday, StartDate = new DateTime(2010,1,1) },
                new Holiday() { Name = "Christmas Day",                 Month = Month.December,     DayOfMonth = 25, StartDate = new DateTime(2010,1,1), OrClosestWeekday = true }
            });
            demoEmployer.CreatedById = devUser.Id;
            demoEmployer.ModifiedById = devUser.Id;
            demoEmployer.Save();

            DateTime jerrysJonesDate = DateTime.UtcNow.Date.AddYears(-1);
            jerryJones = Employee.AsQueryable().Where(e => e.EmployeeNumber == "test_001").FirstOrDefault() ?? new Employee();
            jerryJones.EmployeeNumber = "test_001";
            jerryJones.Customer = demoCustomer;
            jerryJones.Employer = demoEmployer;
            jerryJones.Info.Email = "test.jerry.jones@absencesoft.com";
            jerryJones.FirstName = "Jerrytest";
            jerryJones.LastName = "Jones";
            jerryJones.Gender = Gender.Male;
            jerryJones.Ssn = new CryptoString() { PlainText = "010-11-0101" };
            jerryJones.DoB = new DateTime(1980, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            jerryJones.Info.Address = new Address()
            {
                Address1 = "123 N. Speer Blvd.",
                Address2 = "Apt 8011",
                City = "Denver",
                State = "CO",
                PostalCode = "80000",
            };
            jerryJones.Info.HomePhone = "+1.303.111.1111";
            jerryJones.Info.CellPhone = "+1.720.222.2222";
            jerryJones.Info.WorkPhone = demoContact.WorkPhone;
            jerryJones.Info.OfficeLocation = "HQ - Denver, CO";
            jerryJones.HireDate = jerrysJonesDate;
            jerryJones.ServiceDate = jerrysJonesDate;
            jerryJones.WorkState = "CO";
            jerryJones.PriorHours = new List<PriorHours>() { new PriorHours() { AsOf = jerrysJonesDate, HoursWorked = ((DateTime.Today - jerrysJonesDate).TotalDays / 7) * 40 } };
            jerryJones.WorkSchedules = new List<Schedule>();
            jerryJones.WorkSchedules.Add(new Schedule()
            {
                StartDate = jerrysJonesDate,
                ScheduleType = ScheduleType.Weekly,
                Times = TestWorkSchedules.MakeSchedule(jerrysJonesDate, 40),
            });
            jerryJones.CreatedById = devUser.Id;
            jerryJones.ModifiedById = devUser.Id;
            jerryJones.Save();

            DateTime marysDate = DateTime.UtcNow.Date.AddYears(-2);
            Employee marySmith = Employee.AsQueryable().Where(e => e.EmployeeNumber == "test_002").FirstOrDefault() ?? new Employee();
            marySmith.EmployeeNumber = "test_002";
            marySmith.Customer = demoCustomer;
            marySmith.Employer = demoEmployer;
            marySmith.Info.Email = "test.mary.smith@absencesoft.com";
            marySmith.FirstName = "Marytest";
            marySmith.LastName = "Smith";
            marySmith.Gender = Gender.Male;
            marySmith.Ssn = new CryptoString() { PlainText = "010-11-0102" };
            marySmith.DoB = new DateTime(1980, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            marySmith.Info.Address = new Address()
            {
                Address1 = "123 N. Speer Blvd.",
                Address2 = "Apt 8011",
                City = "Denver",
                State = "CO",
                PostalCode = "80000",
            };
            marySmith.Info.HomePhone = "+1.303.111.1111";
            marySmith.Info.CellPhone = "+1.720.222.2222";
            marySmith.Info.WorkPhone = demoContact.WorkPhone;
            marySmith.Info.OfficeLocation = "HQ - Denver, CO";
            marySmith.HireDate = jerrysJonesDate;
            marySmith.ServiceDate = marysDate;
            marySmith.WorkState = "CO";
            marySmith.PriorHours = new List<PriorHours>() { new PriorHours() { AsOf = jerrysJonesDate, HoursWorked = ((DateTime.Today - jerrysJonesDate).TotalDays / 7) * 40 } };
            marySmith.WorkSchedules = new List<Schedule>();
            marySmith.WorkSchedules.Add(new Schedule()
            {
                StartDate = jerrysJonesDate,
                ScheduleType = ScheduleType.Weekly,
                Times = TestWorkSchedules.MakeSchedule(jerrysJonesDate, 40),
            });
            marySmith.CreatedById = devUser.Id;
            marySmith.ModifiedById = devUser.Id;
            marySmith.Save();

            DateTime jerrySmithsDate = DateTime.UtcNow.Date.AddYears(-3);
            Employee jerrySmith = Employee.AsQueryable().Where(e => e.EmployeeNumber == "test_003").FirstOrDefault() ?? new Employee();
            jerrySmith.EmployeeNumber = "test_003";
            jerrySmith.Customer = demoCustomer;
            jerrySmith.Employer = demoEmployer;
            jerrySmith.Info.Email = "test.jerry.smith@absencesoft.com";
            jerrySmith.FirstName = "jerrytest";
            jerrySmith.LastName = "Smith";
            jerrySmith.Gender = Gender.Male;
            jerrySmith.Ssn = new CryptoString() { PlainText = "010-11-0103" };
            jerrySmith.DoB = new DateTime(1980, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            jerrySmith.Info.Address = new Address()
            {
                Address1 = "123 N. Speer Blvd.",
                Address2 = "Apt 8011",
                City = "Denver",
                State = "CO",
                PostalCode = "80000",
            };
            jerrySmith.Info.HomePhone = "+1.303.111.1111";
            jerrySmith.Info.CellPhone = "+1.720.222.2222";
            jerrySmith.Info.WorkPhone = demoContact.WorkPhone;
            jerrySmith.Info.OfficeLocation = "HQ - Denver, CO";
            jerrySmith.HireDate = jerrysJonesDate;
            jerrySmith.ServiceDate = jerrySmithsDate;
            jerrySmith.WorkState = "CO";
            jerrySmith.PriorHours = new List<PriorHours>() { new PriorHours() { AsOf = jerrysJonesDate, HoursWorked = ((DateTime.Today - jerrysJonesDate).TotalDays / 7) * 40 } };
            jerrySmith.WorkSchedules = new List<Schedule>();
            jerrySmith.WorkSchedules.Add(new Schedule()
            {
                StartDate = jerrysJonesDate,
                ScheduleType = ScheduleType.Weekly,
                Times = TestWorkSchedules.MakeSchedule(jerrysJonesDate, 40),
            });
            jerrySmith.CreatedById = devUser.Id;
            jerrySmith.ModifiedById = devUser.Id;
            jerrySmith.Save();
            #endregion
        }

        [TestMethod]
        public void TestEmployeeList_CaseInsensitiveSearch()
        {
            var employeeService = new AbsenceSoft.Logic.Customers.EmployeeService();

            // case insensitive search
            employeeService.EmployeeList(new ListCriteria().Set("FirstName", "jerrytest")).Total.Should().Be(2);
        }

        [TestMethod]
        public void TestEmployeeList()
        {
            var employeeService = new AbsenceSoft.Logic.Customers.EmployeeService();

            // case insensitive search
            var results = employeeService.EmployeeList(new ListCriteria().Set("FirstName", "jerrytest").Set("LastName", "jones"));
            results.Total.Should().Be(1);
            var first = results.Results.First();
            first.Get<string>("FirstName").Should().Be("Jerrytest");
            first.Get<string>("LastName").Should().Be("Jones");
            first.Get<string>("EmployeeNumber").Should().Be("test_001");
            first.Get<string>("Id").Should().Be(jerryJones.Id);
            first.Get<DateTime?>("HireDate").Should().Be(jerryJones.HireDate);
        }


        // used to remove info we created in other tests
        private void removeTestEmp(string empNum)
        {
            // see if our test was here before and remove the data
            Employee d1 = Employee.AsQueryable().Where(e => e.EmployeeNumber == empNum).FirstOrDefault();

            if (d1 == null)
                return;

            List<EmployeeContact> ec = EmployeeContact.AsQueryable().Where(c => c.EmployeeId == d1.Id).ToList();
            foreach (EmployeeContact e in ec)
                e.Delete();

            d1.Delete();

        }

        // place holder, for all the ways to test an employee add and update
        [TestMethod]
        public void EmployeeSeviceAddUpdate()
        {
            // current test relies on demo data, needs to be updated (and it assumes it worked)
            Customer demoCustomer = Customer.AsQueryable().Where(c => c.Name == "AbsenceSoft Demo").FirstOrDefault();
            Employer demoEmployer = Employer.AsQueryable().Where(e => e.ReferenceCode == "demo").FirstOrDefault();

            // see if our test was here before and remove the data
            removeTestEmp("1");
            removeTestEmp("11");
            removeTestEmp("111");
            removeTestEmp("2");

            // add an employee
            Employee fred = new Employee()
            {
                EmployeeNumber = "1",
                CustomerId = demoCustomer.Id,
                EmployerId = demoEmployer.Id,
                DoB = new DateTime(1960, 02, 01),
                FirstName = "Fred",
                LastName = "Flintstone",
                Gender = Gender.Male,
                HireDate = new DateTime(2010, 01, 02),
                IsExempt = false,
                IsKeyEmployee = false,
                JobTitle = "Bronto Crane Operator",
                ServiceDate = new DateTime(2010, 01, 15),
                Ssn = new CryptoString("012214321"),
                WorkSchedules = new List<Schedule>() {
                    new Schedule() { StartDate = new DateTime(2010,01,15), ScheduleType = ScheduleType.Weekly, Times = TestWorkSchedules.MakeSchedule(new DateTime(2014,2,2),8)}
                },
                Info = new EmployeeInfo() { Email = "fred.flintstone@absencesoft.com" }
            };

            EmployeeService es = new EmployeeService();
            es.Update(fred);

            // try to add it again with same name
            Employee f1 = new Employee()
            {
                EmployeeNumber = "1",
                CustomerId = demoCustomer.Id,
                EmployerId = demoEmployer.Id,
                DoB = new DateTime(1960, 02, 01),
                FirstName = "Fred",
                LastName = "Flintstone",
                Gender = Gender.Male,
                HireDate = new DateTime(2010, 01, 02),
                IsExempt = false,
                IsKeyEmployee = false,
                JobTitle = "Heavy Lift Dino Operator",
                ServiceDate = new DateTime(2010, 01, 15),
                Ssn = new CryptoString("999999999"),
                WorkSchedules = new List<Schedule>() {
                    new Schedule() { StartDate = new DateTime(2010,01,15), ScheduleType = ScheduleType.Weekly, Times = TestWorkSchedules.MakeSchedule(new DateTime(2014,2,2),8)}
                }
            };

            bool thisShouldFail = false;
            try
            {
                es.Update(f1);
            }
            catch (Exception)
            {

                thisShouldFail = true;
            }

            Assert.IsTrue(thisShouldFail);

            // try and update fred's job description... this should work
            fred.JobTitle = "Gravel Slinger";
            bool thisShouldPass = true;
            try
            {
                es.Update(fred);
            }
            catch (Exception)
            {

                thisShouldPass = false;         // it's backwards from the other tests
            }

            Assert.IsTrue(thisShouldPass);

            // try to add it again with same ssn
            Employee barney = new Employee()
            {
                EmployeeNumber = "111",
                CustomerId = demoCustomer.Id,
                EmployerId = demoEmployer.Id,
                DoB = new DateTime(1964, 02, 01),
                FirstName = "Barney",
                LastName = "Rubble",
                Gender = Gender.Male,
                HireDate = new DateTime(2010, 01, 02),
                IsExempt = false,
                IsKeyEmployee = false,
                JobTitle = "Rock Inspector",
                ServiceDate = new DateTime(2010, 01, 15),
                Ssn = new CryptoString("012214321"),
                WorkSchedules = new List<Schedule>() {
                    new Schedule() { StartDate = new DateTime(2010,01,15), ScheduleType = ScheduleType.Weekly, Times = TestWorkSchedules.MakeSchedule(new DateTime(2014,2,2),8)}
                }
            };

            thisShouldFail = false;
            try
            {
                es.Update(barney);
            }
            catch (Exception)
            {

                thisShouldFail = true;
            }
            Assert.IsTrue(thisShouldFail);

            // now fix barney's ssn but give him a bad work state
            barney.WorkCountry = "US";
            barney.WorkState = "BR";            // Bedrock, USA
            barney.Ssn = new CryptoString("987767890");

            thisShouldFail = false;
            try
            {
                es.Update(barney);
            }
            catch (Exception)
            {

                thisShouldFail = true;
            }
            Assert.IsTrue(thisShouldFail);


            // fix the state and now he should save
            barney.WorkState = "FL";            // it's like bedrock
            thisShouldPass = true;
            try
            {
                es.Update(barney);
            }
            catch (Exception)
            {
                thisShouldPass = false;
            }

            Assert.IsTrue(thisShouldPass);

            // test hire date
            Employee f2 = new Employee()
            {
                EmployeeNumber = "11",
                CustomerId = demoCustomer.Id,
                EmployerId = demoEmployer.Id,
                DoB = new DateTime(1960, 02, 01),
                FirstName = "Fred",
                LastName = "Flintstone",
                Gender = Gender.Male,
                HireDate = new DateTime(2014, 01, 02),
                IsExempt = false,
                IsKeyEmployee = false,
                JobTitle = "Heavy Lift Dino Operator",
                ServiceDate = new DateTime(2010, 01, 15),
                Ssn = new CryptoString("012214321"),
                WorkSchedules = new List<Schedule>() {
                    new Schedule() { StartDate = new DateTime(2010,01,15), ScheduleType = ScheduleType.Weekly, Times = TestWorkSchedules.MakeSchedule(new DateTime(2014,2,2),8)}
                }
            };

            thisShouldFail = false;
            try
            {
                es.Update(f2);
            }
            catch (Exception)
            {

                thisShouldFail = true;
            }

            Assert.IsTrue(thisShouldFail);

            // service date

            // add another and make it the spouse of a previous
            // time for Wilma to mary fred
            Employee wilma = new Employee()
            {
                EmployeeNumber = "2",
                CustomerId = demoCustomer.Id,
                EmployerId = demoEmployer.Id,
                DoB = new DateTime(1961, 09, 01),
                FirstName = "Wilma",
                LastName = "Flintstone",
                Gender = Gender.Female,
                HireDate = new DateTime(2014, 02, 01),
                IsExempt = false,
                IsKeyEmployee = false,
                JobTitle = "Cave Maker",
                ServiceDate = new DateTime(2014, 02, 14),
                Ssn = new CryptoString("555552222"),
                WorkSchedules = new List<Schedule>() {
                    new Schedule() { StartDate = new DateTime(2010,01,15), ScheduleType = ScheduleType.Weekly, Times = TestWorkSchedules.MakeSchedule(new DateTime(2014,2,2),8)}
                },
                SpouseEmployeeNumber = fred.EmployeeNumber
            };


            // check the contacts of the first
            es.Update(wilma);

            Assert.IsTrue(!string.IsNullOrWhiteSpace(wilma.Id));

            List<EmployeeContact> fredsContacts = EmployeeContact.AsQueryable().Where(e => e.EmployeeId == fred.Id).ToList();
            List<EmployeeContact> wilmasContacts = EmployeeContact.AsQueryable().Where(e => e.EmployeeId == wilma.Id).ToList();

            // each should have 2 contacts and each should be in the others list
            bool thisShouldBeTrue = false;
            if (fredsContacts.Exists(fc => fc.EmployeeId == fred.Id && fc.RelatedEmployeeNumber == wilma.EmployeeNumber) &&
                    wilmasContacts.Exists(wc => wc.EmployeeId == wilma.Id && wc.RelatedEmployeeNumber == fred.EmployeeNumber))
                thisShouldBeTrue = true;

            Assert.IsTrue(thisShouldBeTrue);

            // now make wilma a key employee and make sure her status updates
            wilma.IsKeyEmployee = true;
            es.Update(wilma);

            Assert.IsTrue(wilma.IsExempt);

            // the same isn't true for fred, he's exempt but not key
            fred.IsExempt = true;
            es.Update(fred);
            Assert.IsFalse(fred.IsKeyEmployee);


        }                           // public void EmployeeSeviceAddUpdate()

        [TestMethod]
        public void WorkScheduleTest()
        {
            // create a work schedule 
            List<Schedule> ws = new List<Schedule>();

            // create a new schedule with a 40 hour week
            ws.Add(new Schedule()
            {
                StartDate = new DateTime(2012, 01, 02),
                ScheduleType = ScheduleType.Weekly,
                Times = TestWorkSchedules.GetFullTimeList()
            });

            EmployeeService es = new EmployeeService();

            // start date, no end date, dates in a row... true
            Assert.IsTrue(es.AreSchedulesValid(ws));

            ws.Add(new Schedule()
            {
                StartDate = new DateTime(2012, 04, 01),
                ScheduleType = ScheduleType.Rotating,
                Times = TestWorkSchedules.RotatingSchedule()
            });

            // set the end date on the first
            ws[0].EndDate = new DateTime(2012, 03, 31);

            // should still be good
            Assert.IsTrue(es.AreSchedulesValid(ws));

            // now break the end date
            // set the end date on the first
            ws[0].EndDate = new DateTime(2013, 03, 31);

            // now should be broken
            Assert.IsFalse(es.AreSchedulesValid(ws));

            // put the date back
            ws[0].EndDate = new DateTime(2012, 03, 31);
            Assert.IsTrue(es.AreSchedulesValid(ws));

            // now break one of the days in the sequence
            DateTime holdDate = ws[0].Times[3].SampleDate;
            ws[0].Times[3].SampleDate = holdDate.AddDays(44);
            Assert.IsFalse(es.AreSchedulesValid(ws));

            // now put the date back and do the next test
            ws[0].Times[3].SampleDate = holdDate;

            // take the weekly schedule and shift it off by two days
            foreach (Time t in ws[0].Times)
                t.SampleDate = t.SampleDate.AddDays(2);

            Assert.IsFalse(es.AreSchedulesValid(ws));

            // put it back
            foreach (Time t in ws[0].Times)
                t.SampleDate = t.SampleDate.AddDays(-2);

            Assert.IsTrue(es.AreSchedulesValid(ws));

            // add in a one time schedule
            ws[1].EndDate = new DateTime(2012, 8, 31);
            ws.Add(new Schedule()
            {
                StartDate = new DateTime(2012, 09, 01),
                ScheduleType = ScheduleType.Variable,
                Times = TestWorkSchedules.MakeSchedule(new DateTime(2012, 09, 01), 20)
            });

            Assert.IsTrue(es.AreSchedulesValid(ws));

            // set the end date and then 
            // break one of the dates
            ws[2].EndDate = new DateTime(2012, 9, 30);

            holdDate = ws[2].Times[4].SampleDate;
            ws[2].Times[4].SampleDate = new DateTime(2011, 1, 1);
            Assert.IsFalse(es.AreSchedulesValid(ws));
            ws[2].Times[4].SampleDate = new DateTime(2014, 1, 1);
            Assert.IsFalse(es.AreSchedulesValid(ws));

            ws[2].Times[4].SampleDate = holdDate;

            Assert.IsTrue(es.AreSchedulesValid(ws));

        }

        [TestMethod]
        public void WorkScheduleSetTest()
        {
            EmployeeService es = new EmployeeService();

            Employee emp = new Employee();
            var schedDate = new DateTime(2014, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var schedule1 = new Schedule()
            {
                StartDate = schedDate,
                ScheduleType = ScheduleType.Weekly,
                Times = new List<Time>()
                {
                    new Time() { SampleDate = schedDate.AddDays(-3), TotalMinutes = 0 },
                    new Time() { SampleDate = schedDate.AddDays(-2), TotalMinutes = 480 },
                    new Time() { SampleDate = schedDate.AddDays(-1), TotalMinutes = 480 },
                    new Time() { SampleDate = schedDate.AddDays(0), TotalMinutes = 480 },
                    new Time() { SampleDate = schedDate.AddDays(1), TotalMinutes = 480 },
                    new Time() { SampleDate = schedDate.AddDays(2), TotalMinutes = 480 },
                    new Time() { SampleDate = schedDate.AddDays(3), TotalMinutes = 0 },
                }
            };

            es.SetWorkSchedule(emp, schedule1, null);
            Assert.IsTrue(es.AreSchedulesValid(emp.WorkSchedules));

            schedDate = schedDate.AddDays(20);
            var schedule2 = new Schedule()
            {
                StartDate = schedDate,
                ScheduleType = ScheduleType.Weekly,
                Times = new List<Time>()
                {
                    new Time() { SampleDate = schedDate.AddDays(-2), TotalMinutes = 60 },
                    new Time() { SampleDate = schedDate.AddDays(-1), TotalMinutes = 120 },
                    new Time() { SampleDate = schedDate.AddDays(0), TotalMinutes = 0 },
                    new Time() { SampleDate = schedDate.AddDays(1), TotalMinutes = 120 },
                    new Time() { SampleDate = schedDate.AddDays(2), TotalMinutes = 320 },
                    new Time() { SampleDate = schedDate.AddDays(3), TotalMinutes = 120 },
                    new Time() { SampleDate = schedDate.AddDays(4), TotalMinutes = 90 },
                }
            };

            es.SetWorkSchedule(emp, schedule2, null);
            Assert.IsTrue(es.AreSchedulesValid(emp.WorkSchedules));

            schedDate = new DateTime(2014, 1, 12, 0, 0, 0, 0, DateTimeKind.Utc);
            var schedule3 = new Schedule()
            {
                StartDate = schedDate,
                EndDate = schedDate.AddDays(7),
                ScheduleType = ScheduleType.Weekly,
                Times = new List<Time>()
                {
                    new Time() { SampleDate = schedDate.AddDays(0), TotalMinutes = 0 },
                    new Time() { SampleDate = schedDate.AddDays(1), TotalMinutes = 600 },
                    new Time() { SampleDate = schedDate.AddDays(2), TotalMinutes = 600 },
                    new Time() { SampleDate = schedDate.AddDays(3), TotalMinutes = 600 },
                    new Time() { SampleDate = schedDate.AddDays(4), TotalMinutes = 600 },
                    new Time() { SampleDate = schedDate.AddDays(5), TotalMinutes = 0 },
                    new Time() { SampleDate = schedDate.AddDays(6), TotalMinutes = 0 },
                }
            };

            es.SetWorkSchedule(emp, schedule3, null);
            Assert.IsTrue(es.AreSchedulesValid(emp.WorkSchedules));

        }

        [TestMethod]
        public void VariableScheduleTest()
        {
            using (EmployeeService es = new EmployeeService())
            {
                Employee emp = es.GetEmployee("000000000000000000008043");
                emp.WorkSchedules[0].ScheduleType = ScheduleType.Variable;

                DateTime startDate = new DateTime(2015, 3, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime endDate = new DateTime(2015, 4, 4, 0, 0, 0, DateTimeKind.Utc); // 5 weeks

                LeaveOfAbsence loa = new LeaveOfAbsence()
                {
                    Customer = emp.Customer,
                    Employer = emp.Employer,
                    Employee = emp,
                    WorkSchedule = emp.WorkSchedules
                };
                var time = loa.MaterializeSchedule(startDate, endDate); // Get times inclusive of start/end date
                time.Count.Should().Be(35, "5 weeks (7 days a week)");
                var sum = time.Sum(t => t.TotalMinutes ?? 0); // Get sum of minutes scheduled for those 5 weeks
                sum.Should().Be(40 * 5 * 60, "200 hours, 40/hrs a week for 5 weeks");

                // Create a historic 25-hour week at the beginning of our start date
                es.SetWorkSchedule(emp, null, new List<VariableScheduleTime>(7)
                {
                    new VariableScheduleTime()
                    {
                        CustomerId = emp.CustomerId,
                        EmployerId = emp.EmployerId,
                        EmployeeId = emp.Id,
                        Time = new Time()
                        {
                            SampleDate = startDate.AddDays(0),
                            TotalMinutes = 0
                        }
                    },
                    new VariableScheduleTime()
                    {
                        CustomerId = emp.CustomerId,
                        EmployerId = emp.EmployerId,
                        EmployeeId = emp.Id,
                        Time = new Time()
                        {
                            SampleDate = startDate.AddDays(1),
                            TotalMinutes = 4 * 60 // 4 hours
                        }
                    },
                    new VariableScheduleTime()
                    {
                        CustomerId = emp.CustomerId,
                        EmployerId = emp.EmployerId,
                        EmployeeId = emp.Id,
                        Time = new Time()
                        {
                            SampleDate = startDate.AddDays(2),
                            TotalMinutes = 5 * 60 // 5 hours
                        }
                    },
                    new VariableScheduleTime()
                    {
                        CustomerId = emp.CustomerId,
                        EmployerId = emp.EmployerId,
                        EmployeeId = emp.Id,
                        Time = new Time()
                        {
                            SampleDate = startDate.AddDays(3),
                            TotalMinutes = 6 * 60 // 6 hours
                        }
                    },
                    new VariableScheduleTime()
                    {
                        CustomerId = emp.CustomerId,
                        EmployerId = emp.EmployerId,
                        EmployeeId = emp.Id,
                        Time = new Time()
                        {
                            SampleDate = startDate.AddDays(4),
                            TotalMinutes = 7 * 60 // 7 hours
                        }
                    },
                    new VariableScheduleTime()
                    {
                        CustomerId = emp.CustomerId,
                        EmployerId = emp.EmployerId,
                        EmployeeId = emp.Id,
                        Time = new Time()
                        {
                            SampleDate = startDate.AddDays(5),
                            TotalMinutes = 3 * 60 // 3 hours
                        }
                    },
                    new VariableScheduleTime()
                    {
                        CustomerId = emp.CustomerId,
                        EmployerId = emp.EmployerId,
                        EmployeeId = emp.Id,
                        Time = new Time()
                        {
                            SampleDate = startDate.AddDays(6),
                            TotalMinutes = 0
                        }
                    }
                });

                time = loa.MaterializeSchedule(startDate, endDate); // Get times inclusive of start/end date
                time.Count.Should().Be(35, "5 weeks (7 days a week)");
                sum = time.Sum(t => t.TotalMinutes ?? 0); // Get sum of minutes scheduled for those 5 weeks
                sum.Should().Be((25 + (40 * 4)) * 60, "185 hours, 25 hours first week + 40/hrs a week for 4 weeks");

                // Create a new "partial" week, where only a couple of days have entries
                es.SetWorkSchedule(emp, null, new List<VariableScheduleTime>(2)
                {
                    new VariableScheduleTime()
                    {
                        CustomerId = emp.CustomerId,
                        EmployerId = emp.EmployerId,
                        EmployeeId = emp.Id,
                        Time = new Time()
                        {
                            SampleDate = startDate.AddDays(9),
                            TotalMinutes = 2 * 60 // 2 hours
                        }
                    },
                    new VariableScheduleTime()
                    {
                        CustomerId = emp.CustomerId,
                        EmployerId = emp.EmployerId,
                        EmployeeId = emp.Id,
                        Time = new Time()
                        {
                            SampleDate = startDate.AddDays(11),
                            TotalMinutes = 4 * 60 // 4 hours
                        }
                    }
                });

                time = loa.MaterializeSchedule(startDate, endDate); // Get times inclusive of start/end date
                time.Count.Should().Be(35, "5 weeks (7 days a week)");
                sum = time.Sum(t => t.TotalMinutes ?? 0); // Get sum of minutes scheduled for those 5 weeks
                sum.Should().Be((25 + 6 + (40 * 3)) * 60, "151 hours, 25 hours first week + 6 hours second week + 40/hrs a week for 3 weeks");
                
                // Test latent entry nulling out prior weeks (assume no time scheduled at all)

                // Create a new "partial" week, where only a couple of days have entries
                es.SetWorkSchedule(emp, null, new List<VariableScheduleTime>(1)
                {
                    new VariableScheduleTime()
                    {
                        CustomerId = emp.CustomerId,
                        EmployerId = emp.EmployerId,
                        EmployeeId = emp.Id,
                        Time = new Time()
                        {
                            SampleDate = endDate.AddDays(-1), // That Friday
                            TotalMinutes = 60 // 1 hour
                        }
                    }
                });

                time = loa.MaterializeSchedule(startDate, endDate); // Get times inclusive of start/end date
                time.Count.Should().Be(35, "5 weeks (7 days a week)");
                sum = time.Sum(t => t.TotalMinutes ?? 0); // Get sum of minutes scheduled for those 5 weeks
                sum.Should().Be((25 + 6 + (40 * 2) + 1) * 60, 
                    "112 hours, 25 hours first week + 6 hours second week + 40/hrs a week for 2 weeks and 1 hour the last Friday");

                // Cleanup :-)
                VariableScheduleTime.Repository.Collection.Remove(VariableScheduleTime.Query.EQ(v => v.EmployeeId, emp.Id), MongoDB.Driver.RemoveFlags.None);
            }
        }

        [TestMethod]
        public void FindEmployeeByEmail()
        {
            var employeeService = new AbsenceSoft.Logic.Customers.EmployeeService();

            string customerId = "000000000000000000000001";
            string employerId = "000000000000000000000001";
            string email = "fred.flintstone@absencesoft.com";

            //add test employee

            // add an employee
            Employee fred = new Employee()
            {
                EmployeeNumber = "1",
                CustomerId = customerId,
                EmployerId = employerId,
                DoB = new DateTime(1960, 02, 01),
                FirstName = "Fred",
                LastName = "Flintstone",
                Gender = Gender.Male,
                HireDate = new DateTime(2010, 01, 02),
                IsExempt = false,
                IsKeyEmployee = false,
                JobTitle = "Bronto Crane Operator",
                ServiceDate = new DateTime(2010, 01, 15),
                Ssn = new CryptoString("012214321"),
                WorkSchedules = new List<Schedule>() {
                    new Schedule() { StartDate = new DateTime(2010,01,15), ScheduleType = ScheduleType.Weekly, Times = TestWorkSchedules.MakeSchedule(new DateTime(2014,2,2),8)}
                },
                Info = new EmployeeInfo() { Email = email }
            };
            fred.Save();


            Employee emp = employeeService.GetEmployeeByEmail(employerId, email);
            Assert.IsNotNull(emp);

            email = "ds@test.com";
            Employee emp2 = employeeService.GetEmployeeByEmail(employerId, email);
            Assert.IsNull(emp2);

        }

        private Employee CreateEmployeeForSearchUnitTests()
        {
            Employee employee = new Employee()
            {
                EmployerId = "000000000000000000000001",
                CustomerId = "000000000000000000000001",
                EmployeeNumber = "E001",
                FirstName = "John",
                LastName = "Smith",
                Gender = Gender.Male,
                DoB = new DateTime(1950, 10, 20),
                HireDate = new DateTime(2010, 01, 02),
                ServiceDate = new DateTime(2010, 01, 15),
                OfficeLocationDisplayName = "CA001",
                Info = new EmployeeInfo() { Email = "john.smith@absencesoft.com" }

            };
            return employee;
        }

        [TestMethod]
        public void SearchResultDescriptionTest()
        {
            Employee employee = CreateEmployeeForSearchUnitTests();

            string actualDescription = employee.GetDescriptionForSearchResult(includeTitle: true);
            string expectedDescription = "Employee: John Smith (#E001) | Male | DOB: 10/20/1950 | Hire Date: 01/02/2010 | Service Date: 01/15/2010 | john.smith@absencesoft.com | Location: CA001";

            Assert.AreEqual(expectedDescription, actualDescription);
        }
       

        [TestMethod]
        public void SearchResultDescriptionWithoutTitleTest()
        {
            Employee employee = CreateEmployeeForSearchUnitTests();

            string actualDescription = employee.GetDescriptionForSearchResult(includeTitle: false);
            string expectedDescription = "Male | DOB: 10/20/1950 | Hire Date: 01/02/2010 | Service Date: 01/15/2010 | john.smith@absencesoft.com | Location: CA001";

            Assert.AreEqual(expectedDescription, actualDescription);
        }

        [TestMethod]
        public void SearchResultDescriptionWithoutDoBTest()
        {
            Employee employee = CreateEmployeeForSearchUnitTests();
            employee.DoB = null;

            string actualDescription = employee.GetDescriptionForSearchResult(includeTitle: true);
            string expectedDescription = "Employee: John Smith (#E001) | Male | Hire Date: 01/02/2010 | Service Date: 01/15/2010 | john.smith@absencesoft.com | Location: CA001";

            Assert.AreEqual(expectedDescription, actualDescription);
        }

        [TestMethod]
        public void SearchResultDescriptionWithoutGenderAndHireDateTest()
        {
            Employee employee = CreateEmployeeForSearchUnitTests();
            employee.Gender = null;
            employee.HireDate = null;

            string actualDescription = employee.GetDescriptionForSearchResult(includeTitle: true);
            string expectedDescription = "Employee: John Smith (#E001) | DOB: 10/20/1950 | Service Date: 01/15/2010 | john.smith@absencesoft.com | Location: CA001";

            Assert.AreEqual(expectedDescription, actualDescription);
        }

        [TestMethod]
        public void SearchResultDescriptionWithoutTitleAndDOBTest()
        {
            Employee employee = CreateEmployeeForSearchUnitTests();
            employee.DoB = null;

            string actualDescription = employee.GetDescriptionForSearchResult();
            string expectedDescription = "Male | Hire Date: 01/02/2010 | Service Date: 01/15/2010 | john.smith@absencesoft.com | Location: CA001";

            Assert.AreEqual(expectedDescription, actualDescription);
        }

        [TestMethod]
        public void SetOfficeLocationDisplayNameTest()
        {
            Employee employee = CreateEmployeeForSearchUnitTests();

            EmployeeOrganization employeeOrganization = new EmployeeOrganization()
            {
                EmployerId = "000000000000000000000001",
                CustomerId = "000000000000000000000001",
                EmployeeNumber = "E001",
                TypeCode = "OFFICELOCATION",
                Dates = new DateRange(new DateTime(2017, 01, 01)),
                Name = "CA001",
                Code = "10001"
            };

            List<EmployeeOrganization> employeeOrganizations = new List<EmployeeOrganization>();
            employeeOrganizations.Add(employeeOrganization);

            employee.SetOfficeLocationDisplayName(employeeOrganizations);

            string actual = employee.OfficeLocationDisplayName;
            string expected = "CA001";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void SetMultipleOfficeLocationsDisplayNameTest()
        {
            Employee employee = CreateEmployeeForSearchUnitTests();

            List<EmployeeOrganization> employeeOrganizations = new List<EmployeeOrganization>()
            {
                new EmployeeOrganization(){
                     EmployerId = "000000000000000000000001",
                     CustomerId = "000000000000000000000001",
                     EmployeeNumber = "E001",
                     TypeCode = "OFFICELOCATION",
                     Dates = new DateRange(new DateTime(2017, 01, 01)),
                     Name = "CA001",
                     Code = "10001"
                },
                new EmployeeOrganization(){
                    EmployerId = "000000000000000000000001",
                    CustomerId = "000000000000000000000001",
                    EmployeeNumber = "E001",
                    TypeCode = "OFFICELOCATION",
                    Dates = new DateRange(new DateTime(2017, 01, 02)),
                    Name = "CA002",
                    Code = "10002"
                }
            };

            employee.SetOfficeLocationDisplayName(employeeOrganizations);

            string actual = employee.OfficeLocationDisplayName;
            string expected = "CA002";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void SetOfficeLocationWhenDisplayNameIsNullTest()
        {
            Employee employee = CreateEmployeeForSearchUnitTests();
            employee.OfficeLocationDisplayName = null;

            List<EmployeeOrganization> employeeOrganizations = new List<EmployeeOrganization>();

            employee.SetOfficeLocationDisplayName(employeeOrganizations);

            string actual = employee.OfficeLocationDisplayName;

            Assert.IsNull(actual);
        }

        [TestMethod]
        public void TestGetEmployeeContactsByAbsenceReasonCode()
        {
            var employeeService = new EmployeeService();
            var results = employeeService.GetEmployeeContactsByAbsenceReasonCode("000000000000000000000300", "FHC");
            Assert.IsNotNull(results);
        }

        [TestMethod]
        public void TestGetEmployeeContactTypesByAbsenceReasonCode()
        {
            var employeeService = new EmployeeService();
            var results = employeeService.GetEmployeeContactTypesByAbsenceReasonCode("FHC");
            Assert.IsNotNull(results);
        }
    }
}
