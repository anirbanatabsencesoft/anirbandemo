﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;
using System.Collections.Generic;
using System.Linq;
using AbsenceSoft.Logic.Customers;
using System.Web;
using AbsenceSoft.Web;
using System.Security.Claims;

namespace AbsenceSoft.Tests.Services.Customer
{
    [TestClass]
    public class EmployerServiceTests
    {
        [TestMethod]
        public void EmployerCantDuplicateReferenceCode()
        {
            // first get rid of our previous runs
            DeleteEmployer("Slate Rock and Gravel Company");
            DeleteEmployer("Rockhead and Quarry Cave Construction Company");

            // get the demo customer
            AbsenceSoft.Data.Customers.Customer demoCustomer = AbsenceSoft.Data.Customers.Customer.AsQueryable().Where(c => c.Name == "AbsenceSoft Demo").FirstOrDefault();

            EmployerService es = new EmployerService();
            Employer firstEmployer = new Employer()
            {
                CustomerId = demoCustomer.Id,
                Ein = new AbsenceSoft.Data.CryptoString("01-7398713"),
                FMLPeriodType = PeriodType.CalendarYear,
                Name = "Slate Rock and Gravel Company",
                ReferenceCode = "thePit",
                ResetDayOfMonth = 1,
                ResetMonth = Month.April
            };

            es.Update(firstEmployer);

            Employer employerMatchesReferenceCode = new Employer()
            {
                CustomerId = demoCustomer.Id,
                Ein = new AbsenceSoft.Data.CryptoString("03-7398713"),
                FMLPeriodType = PeriodType.CalendarYear,
                Name = "Rockhead and Quarry Cave Construction Company",
                ReferenceCode = "thePit",
                ResetDayOfMonth = 1,
                ResetMonth = Month.April
            };

            using (var employerService = new EmployerService(demoCustomer, employerMatchesReferenceCode, null))
            {
                Assert.IsTrue(employerService.ReferenceCodeIsInUse(employerMatchesReferenceCode));
            }
        }

        // delete our previous runs
        private void DeleteEmployer(string name)
        {
            Employer d = Employer.AsQueryable().Where(e => e.Name == name).FirstOrDefault();
            if (d != null)
                d.Delete();

        }

        [TestMethod]
        public void EmployerWithNoDatesIsNotDisabled()
        {
            Employer testEmployer = new Employer();
            Assert.IsFalse(testEmployer.IsDisabled);
        }

        [TestMethod]
        public void EmployerWithEffectiveDateInTheFutureIsDisabled()
        {
            Employer testEmployer = new Employer()
            {
                StartDate = DateTime.Now.AddDays(1)
            };

            Assert.IsTrue(testEmployer.IsDisabled);

        }

        [TestMethod]
        public void EmployerWithDisabledDateInThePastIsDisabled()
        {
            Employer testEmployer = new Employer()
            {
                EndDate = DateTime.Now.AddDays(-1)
            };
            Assert.IsTrue(testEmployer.IsDisabled);
        }

        [TestMethod]
        public void EmployerWithEffectiveDateInThePastIsNotDisabled()
        {
            Employer testEmployer = new Employer()
            {
                StartDate = DateTime.Now.AddDays(-1)
            };
            Assert.IsFalse(testEmployer.IsDisabled);
        }

        [TestMethod]
        public void EmployerWithDisabledDateInTheFutureIsNotDisabled()
        {
            Employer testEmployer = new Employer()
            {
                EndDate = DateTime.Now.AddDays(1)
            };
            Assert.IsFalse(testEmployer.IsDisabled);
        }

        [TestMethod]
        public void EmployerWithBothDisabledAndEffectiveDatesInTheFutureIsDisabled()
        {
            Employer testEmployer = new Employer()
            {
                StartDate = DateTime.Now.AddDays(1),
                EndDate = DateTime.Now.AddDays(7)
            };
            Assert.IsTrue(testEmployer.IsDisabled);

        }

        [TestMethod]
        public void EmployerWithBothDisabledAndEffectiveDatesInThePastIsDisabled()
        {
            Employer testEmployer = new Employer()
            {
                StartDate = DateTime.Now.AddDays(-7),
                EndDate = DateTime.Now.AddDays(-1)
            };
            Assert.IsTrue(testEmployer.IsDisabled);
        }

        [TestMethod]
        public void EmployerInBetweenDisabledAndEffectiveDatesIsNotDisabled()
        {
            Employer testEmployer = new Employer()
            {
                StartDate = DateTime.Now.AddDays(-7),
                EndDate = DateTime.Now.AddDays(7)
            };
            Assert.IsFalse(testEmployer.IsDisabled);
        }

        private void SetCurrentContext(string employerId)
        {
            User LoginUser = User.AsQueryable().Where(u => u.Id == "000000000000000000000002").FirstOrDefault() ?? new User()
            {
                Id = "000000000000000000000002",
                Email = "qa@absencesoft.com",
                FirstName = "QA",
                LastName = "User",
                CreatedById = "000000000000000000000000",
                ModifiedById = "000000000000000000000000",
                CustomerId = "000000000000000000000002",
                Password = new AbsenceSoft.Data.CryptoString() { PlainText = "Test1234" },
                Roles = new List<string>() { Role.SystemAdministrator.Id }

            };
            Assert.IsNotNull(LoginUser);
            LoginUser.Save();

            HttpContext context = HttpContext.Current = new HttpContext(new HttpRequest(null, "http://tempuri.org", null), new HttpResponse(null));
            context.Request.Browser = new HttpBrowserCapabilities() { Capabilities = new Dictionary<string, string> { { "supportsEmptyStringInCookieValue", "false" }, { "cookies", "false" } } };
            Current.Customer(AbsenceSoft.Data.Customers.Customer.GetById(employerId));

            HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                                            new ClaimsIdentity(new System.Security.Principal.GenericIdentity(LoginUser.Id)
                                            ,new List<Claim>()
                                            {
                                                new Claim("Key",LoginUser.Id)                                               
                                            }), LoginUser.Roles.ToArray());
            Current.User().Customer = Current.Customer();
        }

        [TestMethod]
        public void EmployerContactsWithEndDateNullTest()
        {
            string employerId = "000000000000000000000002";
            SetCurrentContext(employerId);

            EmployerContact contactData = new EmployerContact();
            contactData.Contact.FirstName = "Jhon";
            contactData.Contact.LastName = "Thomas";
            contactData.Contact.Email = "Jhon@gmail.com";
            contactData.Contact.WorkPhone = "4325679";
            contactData.ContactTypeCode = "123";
            contactData.EmployerId = employerId;
            contactData.CustomerId = "000000000000000000000002";
            contactData.Contact.EndDate = null;
            contactData.Save();
            List<EmployerContact> employerContacts;
            using (var employerService = new EmployerService())
            {
                employerContacts = employerService.GetEmployerContacts(employerId);
            }
            Assert.IsNotNull(employerContacts);
            Assert.IsTrue(employerContacts.Exists(e => e.Id == contactData.Id));


        }


        [TestMethod]
        public void EmployerContactsWithEndDateTest()
        {
            string employerId = "000000000000000000000002";
            SetCurrentContext(employerId);

            EmployerContact contactData = new EmployerContact();
            contactData.Contact.FirstName = "Peter";
            contactData.Contact.LastName = "Parker";
            contactData.Contact.Email = "Peter.Parker@gmail.com";
            contactData.Contact.WorkPhone = "563980";
            contactData.ContactTypeCode = "234";
            contactData.EmployerId = employerId;
            contactData.CustomerId = "000000000000000000000002";
            contactData.Contact.EndDate = DateTime.UtcNow.Date;
            contactData.Save();
            List<EmployerContact> employerContacts;
            using (var employerService = new EmployerService())
            {
                employerContacts = employerService.GetEmployerContacts(employerId);
            }
            Assert.IsNotNull(employerContacts);
            Assert.IsTrue(employerContacts.Exists(e => e.Id == contactData.Id));


            contactData.Contact.EndDate = DateTime.UtcNow.Date.AddDays(-1);
            contactData.Save();

            using (var employerService = new EmployerService())
            {
                employerContacts = employerService.GetEmployerContacts(employerId);
            }
            Assert.IsNotNull(employerContacts);
            Assert.IsTrue(employerContacts.Exists(e => e.Id == contactData.Id) == false);
        }

    }
}
