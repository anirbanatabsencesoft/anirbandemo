﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.RiskProfiles;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.RiskProfiles;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Tests.RiskProfiles
{
    [TestClass]
    public class RiskProfileTests
    {
        private string TestCustomerId = "000000000000000000000002";
        private string TestEmployerId = "000000000000000000000002";
        private string TestEmployeeId = "000000000000000100000002";
        private string TestAbsenceReasonId = "000000000000000001000000";
        private string TestAbsenceReasonCode = "EHC";
        private Customer TestCustomer
        {
            get
            {
                return Customer.GetById(TestCustomerId);
            }
        }

        private Employer TestEmployer
        {
            get
            {
                return Employer.GetById(TestEmployerId);
            }
        }

        private Employee TestEmployee
        {
            get
            {
                return Employee.GetById(TestEmployeeId);
            }
        }

        [TestMethod]
        public void EmployeeMatchesProfileWithNoRuleGroups()
        {
            ClearRiskProfiles();
            RiskProfile noRulesProfile = CreateTestProfile("No Rules", EntityTarget.Employee);
            using (var riskProfileService = new RiskProfileService(TestCustomer, TestEmployer, User.System))
            {
                var calculatedProfile = riskProfileService.CalculateRiskProfile(TestEmployee);
                Assert.AreEqual(noRulesProfile.Id, calculatedProfile.Id);
            }
        }


        [TestMethod]
        public void EmployeeMatchesProfileTest()
        {
            ClearRiskProfiles();
            /// Creates a basic risk profile that the employee must have a work state of California
            var ruleGroup = new RuleGroup()
            {
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>()
                {
                    new Rule()
                    {
                        LeftExpression = "Employee.WorkState",
                        Operator = "==",
                        RightExpression ="'CA'"
                    }
                }
            };

            RiskProfile worksInCalifornia = CreateTestProfile("California", EntityTarget.Employee, 1, ruleGroup);
            using (var riskProfileService = new RiskProfileService(TestCustomer, TestEmployer, User.System))
            {
                var calculatedProfile = riskProfileService.CalculateRiskProfile(TestEmployee);
                Assert.AreEqual(worksInCalifornia.Id, calculatedProfile.Id);
            }
        }


        [TestMethod]
        public void EmployeeDoesNotMatchProfileTest()
        {
            ClearRiskProfiles();
            /// Creates a basic risk profile that the employee must have a work state of California
            var ruleGroup = new RuleGroup()
            {
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>()
                {
                    new Rule()
                    {
                        LeftExpression = "Employee.WorkState",
                        Operator = "==",
                        RightExpression ="'WI'"
                    }
                }
            };

            RiskProfile worksInWisconsin = CreateTestProfile("Wisconsin", EntityTarget.Employee, 1, ruleGroup);
            using (var riskProfileService = new RiskProfileService(TestCustomer, TestEmployer, User.System))
            {
                var calculatedProfile = riskProfileService.CalculateRiskProfile(TestEmployee);
                Assert.IsNull(calculatedProfile);
            }
        }

        [TestMethod]
        public void EmployeeMatchesHighestRiskProfileTest()
        {
            ClearRiskProfiles();
            /// Creates a basic risk profile that the employee must have a work state of California
            var californiaRuleGroup = new RuleGroup()
            {
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>()
                {
                    new Rule()
                    {
                        LeftExpression = "Employee.WorkState",
                        Operator = "==",
                        RightExpression ="'CA'"
                    }
                }
            };

            var meets50In75MileRuleGroup = new RuleGroup()
            {
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>()
                {
                    new Rule()
                    {
                        LeftExpression = "Employee.Meets50In75MileRule",
                        Operator = "==",
                        RightExpression = "true",
                    }
                }
            };

            RiskProfile worksInCalifornia = CreateTestProfile("California", EntityTarget.Employee, 1, californiaRuleGroup);
            RiskProfile meets50in75 = CreateTestProfile("50 In 75", EntityTarget.Employee, 2, meets50In75MileRuleGroup);


            using (var riskProfileService = new RiskProfileService(TestCustomer, TestEmployer, User.System))
            {
                var calculatedProfile = riskProfileService.CalculateRiskProfile(TestEmployee);
                Assert.AreEqual(meets50in75.Id, calculatedProfile.Id);
            }
        }

        [TestMethod]
        public void CaseMatchesProfileTest()
        {
            ClearRiskProfiles();
            var ruleGroup = new RuleGroup()
            {
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>()
                {
                    new Rule()
                    {
                        LeftExpression = "Case.IsIntermittent",
                        Operator = "==",
                        RightExpression = "true"
                    }
                }
            };

            RiskProfile isIntermittent = CreateTestProfile("Intermittent", EntityTarget.Case, 1, ruleGroup);
            using (var caseService = new CaseService())
            using (var eligibilityService = new EligibilityService())
            using (var riskProfileService = new RiskProfileService(TestCustomer, TestEmployer, User.System))
            {
                var newCase = caseService.CreateCase(CaseStatus.Open, TestEmployeeId, DateTime.Now, DateTime.Now.AddMonths(1), CaseType.Intermittent, TestAbsenceReasonCode);
                newCase = eligibilityService.RunEligibility(newCase).Case;
                newCase = caseService.UpdateCase(newCase, CaseEventType.CaseCreated);

                var calculatedProfile = riskProfileService.CalculateRiskProfile(TestEmployee, newCase);
                Assert.AreEqual(isIntermittent.Id, calculatedProfile.Id);
            }
        }

        [TestMethod]
        public void CaseDoesNotMatchProfileTest()
        {
            ClearRiskProfiles();
            var ruleGroup = new RuleGroup()
            {
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>()
                {
                    new Rule()
                    {
                        LeftExpression = "Case.IsIntermittent",
                        Operator = "==",
                        RightExpression = "false"
                    }
                }
            };

            RiskProfile isIntermittent = CreateTestProfile("Intermittent", EntityTarget.Case, 1, ruleGroup);
            using (var caseService = new CaseService())
            using (var eligibilityService = new EligibilityService())
            using (var riskProfileService = new RiskProfileService(TestCustomer, TestEmployer, User.System))
            {
                var newCase = caseService.CreateCase(CaseStatus.Open, TestEmployeeId, DateTime.Now, DateTime.Now.AddMonths(1), CaseType.Intermittent, TestAbsenceReasonCode);
                newCase = eligibilityService.RunEligibility(newCase).Case;
                newCase = caseService.UpdateCase(newCase, CaseEventType.CaseCreated);

                var calculatedProfile = riskProfileService.CalculateRiskProfile(TestEmployee, newCase);
                Assert.IsNull(calculatedProfile);
            }
        }

        [TestMethod]
        public void CaseMatchesHighestRiskProfileTest()
        {
            ClearRiskProfiles();
            var intermittentRuleGroup = new RuleGroup()
            {
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>()
                {
                    new Rule()
                    {
                        LeftExpression = "Case.IsIntermittent",
                        Operator = "==",
                        RightExpression = "true"
                    }
                }
            };

            var accommodationRuleGroup = new RuleGroup()
            {
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>()
                {
                    new Rule()
                    {
                        LeftExpression = "Case.IsAccommodation",
                        Operator = "==",
                        RightExpression = "true"
                    }
                }
            };

            RiskProfile isAccommodation = CreateTestProfile("Accommodation", EntityTarget.Case, 1, accommodationRuleGroup);
            RiskProfile isIntermittent = CreateTestProfile("Intermittent", EntityTarget.Case, 2, intermittentRuleGroup);
            using (var caseService = new CaseService())
            using (var eligibilityService = new EligibilityService())
            using (var riskProfileService = new RiskProfileService(TestCustomer, TestEmployer, User.System))
            {
                var newCase = caseService.CreateCase(CaseStatus.Open, TestEmployeeId, DateTime.Now, DateTime.Now.AddMonths(1), CaseType.Intermittent, TestAbsenceReasonCode);
                newCase = eligibilityService.RunEligibility(newCase).Case;
                newCase = caseService.UpdateCase(newCase, CaseEventType.CaseCreated);

                var calculatedProfile = riskProfileService.CalculateRiskProfile(TestEmployee, newCase);
                Assert.AreEqual(isIntermittent.Id, calculatedProfile.Id);
            }
        }

        [TestMethod]
        public void TestFirstRiskProfileCalculation()
        {
            using (var riskProfileService = new RiskProfileService(TestCustomer, TestEmployer, User.System))
            {
                CalculatedRiskProfile crap = null;
                RiskProfile greenProfile = CreateTestProfile("Green", EntityTarget.Employee);

                /// We don't have any history, so it shouldn't match
                bool crapShouldBeFalse = riskProfileService.LatestCalculatedProfileMatchesCurrentCalculation(crap, greenProfile);
                Assert.IsFalse(crapShouldBeFalse);
            }
        }

        [TestMethod]
        public void TestRecalculatingNoRiskProfile()
        {
            using (var riskProfileService = new RiskProfileService(TestCustomer, TestEmployer, User.System))
            {
                CalculatedRiskProfile crap = new CalculatedRiskProfile()
                {
                    RiskProfile = null
                };
                RiskProfile profile = null;
                bool crapShouldBeTrue = riskProfileService.LatestCalculatedProfileMatchesCurrentCalculation(crap, profile);
                Assert.IsTrue(crapShouldBeTrue);
            }
        }

        [TestMethod]
        public void TestDifferentRiskProfileCalculation()
        {
            using (var riskProfileService = new RiskProfileService(TestCustomer, TestEmployer, User.System))
            {
                CalculatedRiskProfile crap = new CalculatedRiskProfile()
                {
                    RiskProfile = CreateTestProfile("Yellow", EntityTarget.Employee)
                };
                RiskProfile profile = CreateTestProfile("Green", EntityTarget.Case);

                /// The profiles are different, so it shouldn't match
                bool crapShouldBeFalse = riskProfileService.LatestCalculatedProfileMatchesCurrentCalculation(crap, profile);
                Assert.IsFalse(crapShouldBeFalse);
            }
        }

        [TestMethod]
        public void TestDuplicateProfileCalculation()
        {
            using (var riskProfileService = new RiskProfileService(TestCustomer, TestEmployer, User.System))
            {
                RiskProfile profile = CreateTestProfile("Green", EntityTarget.Case);
                CalculatedRiskProfile notCrap = new CalculatedRiskProfile()
                {
                    RiskProfile = profile
                };

                /// The profiles are the same, so this should match
                bool crapShouldBeTrue = riskProfileService.LatestCalculatedProfileMatchesCurrentCalculation(notCrap, profile);
                Assert.IsTrue(crapShouldBeTrue);
            };

        }

        [TestMethod]
        public void TestNewlyNullProfileCalculation()
        {
            using (var riskProfileService = new RiskProfileService(TestCustomer, TestEmployer, User.System))
            {
                RiskProfile profile = null;
                CalculatedRiskProfile notCrap = new CalculatedRiskProfile()
                {
                    RiskProfile = CreateTestProfile("Green", EntityTarget.Case)
                };
                bool crapShouldBeFalse = riskProfileService.LatestCalculatedProfileMatchesCurrentCalculation(notCrap, profile);
                Assert.IsFalse(crapShouldBeFalse);
            }
        }

        [TestMethod]
        public void TestCaseMatchesAbsenceReason()
        {
            ClearRiskProfiles();
            using (var caseService = new CaseService())
            using (var eligibilityService = new EligibilityService())
            using (var riskProfileService = new RiskProfileService(TestCustomer, TestEmployer, User.System))
            {
                var expressions = riskProfileService.GetRuleExpressions();
                var caseGroup = expressions.FirstOrDefault(e => e.Title == "Case Information");
                caseGroup.Should().NotBeNull();
                var absenceReasonRule = caseGroup.Expressions.FirstOrDefault(c => c.Prompt == "Absence Reason");
                absenceReasonRule.Should().NotBeNull();
                absenceReasonRule.Value = "EHC";
                var ehcRuleGroup = new RuleGroup()
                {
                    SuccessType = RuleGroupSuccessType.And,
                    Rules = new List<Rule>()
                {
                    (Rule)riskProfileService.GetRule(absenceReasonRule)
                }
                };

                RiskProfile isEHC = CreateTestProfile("EHC", EntityTarget.Case, 1, ehcRuleGroup);

                var newCase = caseService.CreateCase(CaseStatus.Open, TestEmployeeId, DateTime.Now, DateTime.Now.AddMonths(1), CaseType.Consecutive, TestAbsenceReasonCode);
                newCase = eligibilityService.RunEligibility(newCase).Case;
                newCase = caseService.UpdateCase(newCase, CaseEventType.CaseCreated);

                var calculatedProfile = riskProfileService.CalculateRiskProfile(TestEmployee, newCase);
                Assert.AreEqual(isEHC.Id, calculatedProfile.Id);
            }
        }

        [TestMethod, Ignore]
        public void TestEmployeesLeaveOfAbsenceComparedToODGGuidelinesCalculation()
        {
            Case.Delete(Case.Query.EQ(c => c.Employee.Id, TestEmployeeId));
            DateTime firstCaseStartDate = new DateTime(2016, 1, 1);
            DateTime firstCaseEndDate = new DateTime(2016, 1, 31);
            DateTime secondCaseStartDate = new DateTime(2016, 2, 1);
            DateTime secondCaseEndDate = new DateTime(2016, 2, 29);
            var firstCase = CreateTestCase(firstCaseStartDate, firstCaseEndDate);
            var secondCase = CreateTestCase(secondCaseStartDate, secondCaseEndDate);
            LeaveOfAbsence absence = new LeaveOfAbsence()
            {
                Employee = firstCase.Employee
            };

            /// employee has two cases, one with 31 approved days off and one with 29 approved days off
            /// first case = (31 - 14) / 14 = 121 % over
            /// Second case = (29 - 14) / 14 = 107 % over
            /// averare = 121 + 107 / 2 = 114% over
            var absenceComparedToODGGuidelines = absence.AverageAbsenceDurationComparedToODGGuidelines();
            Assert.AreEqual(114, absenceComparedToODGGuidelines);
        }

        [TestMethod, Ignore]
        public void TestCasesLeaveOfAbsenceComparedToODGGuidelinesCalculation()
        {
            Case.Delete(Case.Query.EQ(c => c.Employee.Id, TestEmployeeId));
            DateTime firstCaseStartDate = new DateTime(2016, 1, 1);
            DateTime firstCaseEndDate = new DateTime(2016, 1, 31);
            var firstCase = CreateTestCase(firstCaseStartDate, firstCaseEndDate);
            LeaveOfAbsence absence = new LeaveOfAbsence()
            {
                Employee = firstCase.Employee,
                Case = firstCase
            };

            /// employee has one case with 31 approved days off
            /// case = (31 - 14) / 14 = 121 % over
            var absenceComparedToODGGuidelines = absence.AverageAbsenceDurationComparedToODGGuidelines();
            Assert.AreEqual(121, absenceComparedToODGGuidelines);
        }

        private Case CreateTestCase(DateTime startDate, DateTime endDate)
        {
            string employeeHealthConditionId = "000000000000000001000000";
            using (var caseService = new CaseService())
            using (var eligibilityService = new EligibilityService())
            {
                Case theCase = caseService.CreateCase(CaseStatus.Open, TestEmployeeId, startDate, endDate, CaseType.Consecutive, employeeHealthConditionId);
                theCase.Disability = new DisabilityInfo()
                {
                    PrimaryDiagnosis = new DiagnosisCode()
                    {
                        Code = "010"
                    }
                };
                eligibilityService.RunEligibility(theCase);
                foreach (var policy in theCase.Summary.Policies)
                {
                    caseService.ApplyDetermination(theCase, policy.PolicyCode, startDate, endDate, AdjudicationStatus.Approved);
                }
                return caseService.UpdateCase(theCase);
            }
        }

        private RiskProfile CreateTestProfile(string name, EntityTarget target, int order = 1, params RuleGroup[] ruleGroups)
        {
            RiskProfile profile = new RiskProfile()
            {
                CustomerId = TestCustomerId,
                EmployerId = TestEmployerId,
                Name = name,
                Code = name.Replace(" ", "").ToUpper(),
                Target = target,
                Order = order,
                RuleGroups = ruleGroups.ToList()
            };


            return profile.Save();
        }

        private void ClearRiskProfiles()
        {
            RiskProfile.Delete(RiskProfile.Query.EQ(rp => rp.CustomerId, TestCustomerId));
        }

    }
}
