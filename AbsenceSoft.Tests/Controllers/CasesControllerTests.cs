﻿using AbsenceSoft.Controllers;
using AbsenceSoft.Logic;
using AbsenceSoft.Models;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Web.Mvc;
using System.Linq;
using AbsenceSoft.Models.Cases;
using System.Collections.Generic;
using System.Web;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.CaseAssignmentRules;

namespace AbsenceSoft.Tests.Controllers
{
    [TestClass]
    public class CasesControllerTests
    {
        [TestMethod]
        public void TestGetLeaveType()
        {
            var _case = new Case();
            _case.Segments.Add(new CaseSegment() { Type = CaseType.Consecutive, StartDate = new DateTime(2012, 02, 12), EndDate = new DateTime(2012, 03, 01) });
            new CasesController().GetLeaveType(_case).Should().Be("Consecutive");
            _case.Segments.First().Type = CaseType.Intermittent;
            new CasesController().GetLeaveType(_case).Should().Be("Intermittent");
            _case.Segments.First().Type = CaseType.Reduced;
            new CasesController().GetLeaveType(_case).Should().Be("Reduced");

            // return it to start position
            _case = new Case();
            _case.Segments.Add(new CaseSegment() { Type = CaseType.Intermittent, StartDate = new DateTime(2012, 02, 12), EndDate = new DateTime(2012, 03, 01) });
            _case.Segments.Add(new CaseSegment() { Type = CaseType.Consecutive, StartDate = new DateTime(2012, 01, 12), EndDate = new DateTime(2012, 02, 11) });
             new CasesController().GetLeaveType(_case).Should().Be("Consecutive: 01/12/2012-02/11/2012, Intermittent: 02/12/2012-03/01/2012");


            // return it to start position
            _case = new Case();
            _case.Segments.Add(new CaseSegment() { Type = CaseType.Intermittent, StartDate = new DateTime(2012, 02, 12), EndDate = new DateTime(2012, 03, 01) });
            _case.Segments.Add(new CaseSegment() { Type = CaseType.Consecutive, StartDate = new DateTime(2012, 01, 12), EndDate = new DateTime(2012, 02, 11) });
            _case.Segments.Add(new CaseSegment() { Type = CaseType.Consecutive, StartDate = new DateTime(2012, 03, 2), EndDate = new DateTime(2012, 03, 12) });
            new CasesController().GetLeaveType(_case).Should().Be("Consecutive: 01/12/2012-02/11/2012, 03/02/2012-03/12/2012, Intermittent: 02/12/2012-03/01/2012");
        }

        [TestMethod]
        public void TestIntermittentRequestSerialization()
        {
            IntermittentRequestViewModel model = new IntermittentRequestViewModel()
            {
                Details = new List<IntermittentRequestDetailViewModel>()
                {
                    new IntermittentRequestDetailViewModel()
                    {
                        ApprovedTime = 80,
                        PolicyCode = "FMLA"
                    }
                },
                CaseId = "000000000000000000000000",
                RequestDate = DateTime.Today
            };
            string json = JsonConvert.SerializeObject(model);

            Assert.IsTrue(json.Contains("1h 20m"));

            IntermittentRequestViewModel newModel = JsonConvert.DeserializeObject<IntermittentRequestViewModel>(json);
            Assert.AreEqual(newModel.Details.Count, 1);
            Assert.AreEqual(newModel.Details[0].ApprovedTime, 80);
        }

        [TestMethod]
        public void TestOfficeLocationRuleTypeExist()
        {
            HttpContext context = HttpContext.Current = new HttpContext(new HttpRequest(null, "http://tempuri.org", null), new HttpResponse(null));
            context.Request.Browser = new HttpBrowserCapabilities() { Capabilities = new Dictionary<string, string> { { "supportsEmptyStringInCookieValue", "false" }, { "cookies", "false" } } };
            AbsenceSoft.Data.Customers.Customer.Current = AbsenceSoft.Data.Customers.Customer.GetById("000000000000000000000002");

            if (!AbsenceSoft.Data.Customers.Customer.Current.HasFeature(Feature.AdminOrganizations))
            {
                AbsenceSoft.Data.Customers.Customer.Current.AddFeature(Feature.AdminOrganizations, true);
            }
            Dictionary<string, string> ruleTypes = new CaseAssignmentRuleSetController().GetRuleTypes();
            Assert.IsTrue(ruleTypes.Keys.Contains("11"));           
        }
        [TestMethod]
        public void TestOfficeLocationRuleTypeNotExist()
        {
            HttpContext context = HttpContext.Current = new HttpContext(new HttpRequest(null, "http://tempuri.org", null), new HttpResponse(null));
            context.Request.Browser = new HttpBrowserCapabilities() { Capabilities = new Dictionary<string, string> { { "supportsEmptyStringInCookieValue", "false" }, { "cookies", "false" } } };
            AbsenceSoft.Data.Customers.Customer.Current = AbsenceSoft.Data.Customers.Customer.GetById("000000000000000000000002");                         
            AbsenceSoft.Data.Customers.Customer.Current.AddFeature(Feature.AdminOrganizations, false);

            Dictionary<string, string> ruleTypes = new CaseAssignmentRuleSetController().GetRuleTypes();
            Assert.IsFalse(ruleTypes.Keys.Contains("11"));
        }
      
    }
}