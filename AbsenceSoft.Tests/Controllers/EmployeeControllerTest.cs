﻿using AbsenceSoft.Controllers;
using AbsenceSoft.Logic;
using AbsenceSoft.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System.Web.Mvc;
using AbsenceSoft.Web;

namespace AbsenceSoft.Tests.Controllers
{
    [TestClass]
    public class EmployeeControllerTest
    {
        [TestMethod, TestCategory("Controllers.Employee")]
        public void EmployeeList()
        {
            EmployeesController controller = new EmployeesController();
            ListCriteria crit = new ListCriteria();
            crit["Name"] = "j";
            crit["EmployeeNumber"] = "0";
            crit.SortBy = "LastName";
            crit.PageNumber = 1;
            crit.PageSize = 10;
            JsonResult result = controller.List(crit);
            Assert.IsNotNull(result);
            Assert.IsNull(result.Data as JsonErrorModel);

            string json = JsonConvert.SerializeObject(result.Data);
            Assert.IsNotNull(json);
        }
    }
}
