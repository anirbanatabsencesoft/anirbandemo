﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft;
using AbsenceSoft.Controllers;
using AbsenceSoft.Common.Security;
using AbsenceSoft.Models.Home;
using AbsenceSoft.Models.SelfServiceRegistration;
using AbsenceSoft.Web;
using AT.Logic.Authentication;
using Moq;
using Microsoft.Owin.Security;
using AT.Entities.Authentication;
using System.Threading.Tasks;

namespace AbsenceSoft.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        
        [TestMethod, TestCategory("Controllers.Home")]
        public void Index()
        {
            HomeController controller = new HomeController();
            ViewResult result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
        }

        [TestMethod, TestCategory("Controllers.Home")]
        public void Login()
        {
           
            HomeController controller = new HomeController();         
            ViewResult result = controller.Login() as ViewResult;
            Assert.IsNotNull(result);
        }

        [TestMethod, TestCategory("Controllers.Home")]
        public void LoginDevUser()
        {
            var authenticationManager = new Mock<IAuthenticationManager>();
            var MockSignInManager = new Mock<ApplicationSignInManager>(authenticationManager.Object);
            HomeController controller = new HomeController(MockSignInManager.Object);
            MockSignInManager.Setup(m => m.SignInAsync(It.IsAny<string>(), It.IsAny<string>(),ApplicationType.Portal))
                        .Returns(Task.FromResult<SignInStatus>(SignInStatus.Ok));       
            LoginModel model = new LoginModel()
            {
                Email = "dev@absencesoft.com",
                Password = "Test1234",
            };
            var result = controller.Login(model);
            Assert.IsNotNull(result);
        }

        [TestMethod, TestCategory("Controllers.Home")]
        public async Task Logout()
        {
            var authenticationManager = new Mock<IAuthenticationManager>();
            var MockSignInManager = new Mock<ApplicationSignInManager>(authenticationManager.Object);
            HomeController controller = new HomeController(MockSignInManager.Object);            
            RedirectResult result = await controller.Logout() as RedirectResult;
            Assert.IsNotNull(result);
        }
    }
}
