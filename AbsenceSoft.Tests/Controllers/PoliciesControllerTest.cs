﻿using AbsenceSoft.Controllers;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Logic;
using AbsenceSoft.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Web;

namespace AbsenceSoft.Tests.Controllers
{
    [TestClass]
    public class PoliciesControllerTest
    {
        

        [TestMethod, TestCategory("Controllers.Policy")]
        public void Index()
        {
            PoliciesController controller = new PoliciesController();
            ViewResult result = controller.Index("000000000000000000000002", null) as ViewResult;
            Assert.IsNotNull(result);
        }


        [TestMethod, TestCategory("Controllers.Policy")]
        public void GetPolicyByCode()
        {
            string code = "FMLA";
            string employerId = "000000000000000000000002";

            HttpContext context = HttpContext.Current = new HttpContext(new HttpRequest(null, "http://tempuri.org", null), new HttpResponse(null));
            context.Request.Browser = new HttpBrowserCapabilities() { Capabilities = new Dictionary<string, string> { { "supportsEmptyStringInCookieValue", "false" }, { "cookies", "false" } } };
            AbsenceSoft.Data.Customers.Customer.Current = AbsenceSoft.Data.Customers.Customer.GetById("000000000000000000000002");

            PoliciesController controller = new PoliciesController();
            controller.ControllerContext = new ControllerContext(new System.Web.HttpContextWrapper(context), new System.Web.Routing.RouteData(), controller);
            controller.ControllerContext.HttpContext.Items["__customer"] = Customer.GetById(employerId);
            JsonResult result = controller.GetPolicyByCode(code, employerId);
            Assert.IsNotNull(result);
            Assert.IsNull(result.Data as JsonErrorModel);

            string json = JsonConvert.SerializeObject(result.Data);
            Assert.IsNotNull(json);
        }
    }
}
