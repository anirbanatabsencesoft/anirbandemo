﻿using AbsenceSoft.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Tests
{
    /// <summary>
    /// Work Schedules are used through out the test project, use these methods 
    /// to get some common test ones both valid and invalid
    /// </summary>
    public static class TestWorkSchedules
    {

        public static Schedule GetFullTimeSchedule()
        {
            Schedule s = new Schedule()
            {
                StartDate = new DateTime(2014, 1, 26, 0, 0, 0, DateTimeKind.Utc),
                EndDate = new DateTime(2014, 2, 1, 0, 0, 0, DateTimeKind.Utc),
                ScheduleType = AbsenceSoft.Data.Enums.ScheduleType.Weekly,
                Times = GetFullTimeList()
            };

            return s;

        }

        public static List<Time> GetFullTimeList()
        {
            DateTime schedDate = new DateTime(2014, 1, 26,0,0,0, DateTimeKind.Utc);
            List<Time> fullTime = new List<Time>()
            {
                new Time() { SampleDate = schedDate.AddDays(0), TotalMinutes = 0 },
                new Time() { SampleDate = schedDate.AddDays(1), TotalMinutes = 480 },
                new Time() { SampleDate = schedDate.AddDays(2), TotalMinutes = 480 },
                new Time() { SampleDate = schedDate.AddDays(3), TotalMinutes = 480 },
                new Time() { SampleDate = schedDate.AddDays(4), TotalMinutes = 480 },
                new Time() { SampleDate = schedDate.AddDays(5), TotalMinutes = 480 },
                new Time() { SampleDate = schedDate.AddDays(6), TotalMinutes = 0 },
            };

            return fullTime;

        }

        /// <summary>
        /// This method returns a 21 days rotating schedule that has 144 total hours in the pattern
        /// </summary>
        /// <returns></returns>
        public static List<Time> RotatingSchedule(DateTime? patternStart = null)
        {
            // the hard one from the email
            /*
                Sun – 18 hrs
                Mon – 18 hrs
                Tues – 0 hrs
                Weds – 0 hrs
                Thurs – 0 hrs
                Fri – 18 hrs
                Sat – 18 hrs
                Sun – 0 hrs
                Mon – 0 hrs
                Tues – 0 hrs
                Weds – 12 hrs
                Thurs – 12 hrs
                Fri – 12 hrs
                Sat – 0 hrs
                Sun – 0 hrs
                Mon – 12 hrs
                Tues – 12 hrs
                Weds – 12 hrs
                Thurs – 0 hrs
                Fri – 0 hrs
                Sat – 0 hrs
            */
            DateTime schedDate = patternStart ?? new DateTime(2014, 2, 2, 0, 0, 0, DateTimeKind.Utc);       // a sunday
            List<Time> weirdTime = new List<Time>()
            {
                new Time() { SampleDate = schedDate.AddDays(0), TotalMinutes = 18 * 60 },
                new Time() { SampleDate = schedDate.AddDays(1), TotalMinutes = 18 * 60 },
                new Time() { SampleDate = schedDate.AddDays(2), TotalMinutes = 0  * 60 },
                new Time() { SampleDate = schedDate.AddDays(3), TotalMinutes = 0  * 60 },
                new Time() { SampleDate = schedDate.AddDays(4), TotalMinutes = 0  * 60 },
                new Time() { SampleDate = schedDate.AddDays(5), TotalMinutes = 18  * 60 },
                new Time() { SampleDate = schedDate.AddDays(6), TotalMinutes = 18  * 60 },
                new Time() { SampleDate = schedDate.AddDays(7), TotalMinutes = 0  * 60 },
                new Time() { SampleDate = schedDate.AddDays(8), TotalMinutes = 0  * 60 },
                new Time() { SampleDate = schedDate.AddDays(9), TotalMinutes = 0  * 60 },
                new Time() { SampleDate = schedDate.AddDays(10), TotalMinutes = 12  * 60 },
                new Time() { SampleDate = schedDate.AddDays(11), TotalMinutes = 12  * 60 },
                new Time() { SampleDate = schedDate.AddDays(12), TotalMinutes = 12  * 60 },
                new Time() { SampleDate = schedDate.AddDays(13), TotalMinutes = 0  * 60 },
                new Time() { SampleDate = schedDate.AddDays(14), TotalMinutes = 0  * 60 },
                new Time() { SampleDate = schedDate.AddDays(15), TotalMinutes = 12  * 60 },
                new Time() { SampleDate = schedDate.AddDays(16), TotalMinutes = 12  * 60 },
                new Time() { SampleDate = schedDate.AddDays(17), TotalMinutes = 12  * 60 },
                new Time() { SampleDate = schedDate.AddDays(18), TotalMinutes = 0  * 60 },
                new Time() { SampleDate = schedDate.AddDays(19), TotalMinutes = 0  * 60 },
                new Time() { SampleDate = schedDate.AddDays(20), TotalMinutes = 0  * 60 }
            };          // 144 total hours in pattern

            return weirdTime;
        }

        public static List<Time> MakeSchedule(DateTime startDate, int hoursPerWeek)
        {
            DateTime scheduleStart = startDate.GetFirstDayOfWeek();
            int remainderMinutes = (hoursPerWeek * 60) % 5;
            int minutesPerDay = (int)Math.Floor((double)((hoursPerWeek * 60) / 5));

            List<Time> times = new List<Time>()
            {
                new Time() { SampleDate = scheduleStart.AddDays(0), TotalMinutes = 0 },
                new Time() { SampleDate = scheduleStart.AddDays(1), TotalMinutes = minutesPerDay },
                new Time() { SampleDate = scheduleStart.AddDays(2), TotalMinutes = minutesPerDay },
                new Time() { SampleDate = scheduleStart.AddDays(3), TotalMinutes = minutesPerDay },
                new Time() { SampleDate = scheduleStart.AddDays(4), TotalMinutes = minutesPerDay },
                new Time() { SampleDate = scheduleStart.AddDays(5), TotalMinutes = minutesPerDay + remainderMinutes },
                new Time() { SampleDate = scheduleStart.AddDays(6), TotalMinutes = 0 },
            };

            return times;
        }
    }
}
