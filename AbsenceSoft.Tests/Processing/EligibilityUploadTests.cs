﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Processing;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Processing;
using AbsenceSoft.Logic.Processing.EL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using AbsenceSoft.Logic.Jobs;
using AbsenceSoft.Data.Jobs;
using System.IO;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Common.Properties;
using AbsenceSoft.Logic.Customers;

namespace AbsenceSoft.Tests.Processing
{
    [TestClass]
    public class EligibilityUploadTests
    {
        private static TestContext _context;

        [ClassInitialize]
        public static void EligibilityUploadTestsTestInit(TestContext context)
        {
            _context = context;
        }

        [TestMethod, TestCategory("Processing"), TestCategory("Eligibility Upload")]
        public void TestBasicScheduleUpdate()
        {

            string rowText = @"E,300000001,Andersons,Harrie,,Service Rep,,NY,,2121111111,2122222222,2122222222,,seth@absencesoft.com,,123 Main St,,Manhattan,NY,10001,US,,,Supervisor,Jane,12125552525,seth@absencesoft.com,,Doe,John,12125552525,seth@absencesoft.com,,1/1/1980,M,N,Y,N,0,A,,80000,1,2/2/2009,3/3/2009,2/2/2009,2400,2016,0,480,480,480,480,480,0,N,,2,,,,,999-65-5555,ABC,4,";
            // Get the row source from the item.
            EligibilityRow row = new EligibilityRow(rowText.Split(',')) { Delimiter = ',' };
            Assert.IsFalse(row.IsError);

            EligibilityUpload upload = new EligibilityUpload()
            {
                CustomerId = "000000000000000000000001",
                EmployerId = "000000000000000000000001",
                FileKey = "debug/000000000000000000000001/000000000000000000000001/NO-FILE.txt",
                FileName = "NO-FILE.txt",
                Records = 1,
                Status = AbsenceSoft.Data.Enums.ProcessingStatus.Processing,
                CreatedById = User.DefaultUserId,
                ModifiedById = User.DefaultUserId,
                MimeType = "text/plain",
                Processed = 0L,
                Errors = 0L
            }
            .SetCreatedDate(DateTime.UtcNow)
            .SetModifiedDate(DateTime.UtcNow)
            .Save();

            EligibilityUploadResult result = new EligibilityUploadResult().SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow);
            result.RowNumber = 1;
            result.RowSource = rowText;
            result.RowParts = rowText.Split(',');
            result.CreatedById = User.DefaultUserId;
            result.ModifiedById = User.DefaultUserId;
            result.EligibilityUploadId = upload.Id;

            using (var svc = new EligibilityUploadService())
            {
                svc.ProcessRecords(upload, new List<EligibilityUploadResult>(1) { result });
            }

            if (result.IsError)
            {
                if (_context == null)
                    Console.WriteLine(result.Error);
                else
                    _context.WriteLine(result.Error);
            }
            Assert.IsFalse(result.IsError);
        }

        [TestMethod, TestCategory("Processing"), TestCategory("Eligibility Upload")]
        public void TestEmployeeWorkEmailUpdate()
        {
            EligibilityUpload upload = new EligibilityUpload()
            {
                CustomerId = "000000000000000000000002",
                EmployerId = "000000000000000000000002",
                FileKey = "debug/000000000000000000000001/000000000000000000000001/NO-FILE.txt",
                FileName = "NO-FILE.txt",
                Records = 2,
                Status = AbsenceSoft.Data.Enums.ProcessingStatus.Processing,
                CreatedById = User.DefaultUserId,
                ModifiedById = User.DefaultUserId,
                MimeType = "text/plain",
                Processed = 0L,
                Errors = 0L
            }
            .SetCreatedDate(DateTime.UtcNow)
            .SetModifiedDate(DateTime.UtcNow)
            .Save();

            string[] rows = new string[]
            {
                @"E,300000001,Andersons,Harrie,,Service Rep,,NY,,2121111111,2122222222,2122222222,,,seth@absencesoft.com,123 Main St,,Manhattan,NY,10001,US,,,Supervisor,Jane,12125552525,seth@absencesoft.com,,Doe,John,12125552525,seth@absencesoft.com,,1/1/1980,M,N,Y,N,0,A,,80000,1,2/2/2009,3/3/2009,2/2/2009,2400,2016,0,480,480,480,480,480,0,N,,2,,,,,999-65-5555,ABC,4,",
                @"E,300000001,Andersons,Harrie,,Service Rep,,NY,,2121111111,2122222222,2122222222,,seth@absencesoft.com,,123 Main St,,Manhattan,NY,10001,US,,,Supervisor,Jane,12125552525,seth@absencesoft.com,,Doe,John,12125552525,seth@absencesoft.com,,1/1/1980,M,N,Y,N,0,A,,80000,1,2/2/2009,3/3/2009,2/2/2009,2400,2016,0,480,480,480,480,480,0,N,,2,,,,,999-65-5555,ABC,4,"
            };

            int i = 0;
            List<EligibilityUploadResult> records = rows.Select(r =>
            {
                i++;
                EligibilityUploadResult result = new EligibilityUploadResult().SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow);
                result.RowNumber = i;
                result.RowSource = r;
                result.RowParts = r.Split(',');
                result.CreatedById = User.DefaultUserId;
                result.ModifiedById = User.DefaultUserId;
                result.EligibilityUploadId = upload.Id;
                return result;
            }).ToList();

            using (var svc = new EligibilityUploadService())
                svc.ProcessRecords(upload, records);

            Assert.IsFalse(records.Any(r => r.IsError));

            //Check after second upload that the alternate email is be null and work email is seth @absencesoft.com
            var employeeService = new EmployeeService();
            var employee = employeeService.GetEmployeeByEmployeeNumber("300000001", upload.CustomerId, upload.EmployerId);
            
            Assert.IsNotNull(employee);
            Assert.IsNotNull(employee.Info);
            Assert.IsNull(employee.Info.AltEmail);
            Assert.AreEqual("seth@absencesoft.com", employee.Info.Email);

        }

        [TestMethod, TestCategory("Processing"), TestCategory("Eligibility Upload")]
        public void TestCompanyWiseEl()
        {
            long fileSize = 0L;
            string locator = null;

            using (MemoryStream fakeFile = new MemoryStream())
            using (StreamWriter writer = new StreamWriter(fakeFile))
            using (FileService fs = new FileService())
            {
                writer.WriteLine(@"E,ELWO011,EL00111,WO011,,Service Rep,,NY,,2121111111,2122222222,2122222222,,seth@absencesoft.com,,123 Main St,,Manhattan,NY,10001,US,,,Supervisor,Jane,12125552525,seth@absencesoft.com,,Doe,John,12125552525,seth@absencesoft.com,,1/1/1980,M,N,Y,N,0,A,,80000,1,2/2/2009,3/3/2009,2/2/2009,2400,2016,0,480,480,480,480,480,0,N,,2,,,,test,999-65-5555,ABC,4,");
                writer.WriteLine(@"E,ELWO0114,EL00112,WO021,,Service Rep,,NY,,2121111111,2122222222,2122222222,,seth@absencesoft.com,,123 Main St,,Manhattan,NY,10001,US,,,Supervisor,Jane,12125552525,seth@absencesoft.com,,Doe,John,12125552525,seth@absencesoft.com,,1/1/1980,M,N,Y,N,0,A,,80000,1,2/2/2009,3/3/2009,2/2/2009,2400,2016,0,480,480,480,480,480,0,N,,2,,,,test,999-65-5555,ABC,4,");
                writer.WriteLine(@"E,ELWO0115,EL00113,WO031,,Service Rep,,NY,,2121111111,2122222222,2122222222,,seth@absencesoft.com,,123 Main St,,Manhattan,NY,10001,US,,,Supervisor,Jane,12125552525,seth@absencesoft.com,,Doe,John,12125552525,seth@absencesoft.com,,1/1/1980,M,N,Y,N,0,A,,80000,1,2/2/2009,3/3/2009,2/2/2009,2400,2016,0,480,480,480,480,480,0,N,,2,,,,MONS,999-65-5555,ABC,4,");
                writer.WriteLine(@"E,ELWO0161,EL01104,WO041,,Service Rep,,NY,,2121111111,2122222222,2122222222,,seth@absencesoft.com,,123 Main St,,Manhattan,NY,10001,US,,,Supervisor,Jane,12125552525,seth@absencesoft.com,,Doe,John,12125552525,seth@absencesoft.com,,1/1/1980,M,N,Y,N,0,A,,80000,1,2/2/2009,3/3/2009,2/2/2009,2400,2016,0,480,480,480,480,480,0,N,,2,,,,MONS,999-65-5555,ABC,4,");

                writer.Flush();
                fakeFile.Position = 0;
                fileSize = fakeFile.Length;

                locator = fs.UploadFile(fakeFile, "NO-FILE.txt", Settings.Default.S3BucketName_Processing, "000000000000000000000002");
            }


            EligibilityUpload upload = new EligibilityUpload()
            {
                CustomerId = "000000000000000000000002",
                FileKey = locator,
                FileName = "NO-FILE.txt",
                Records = 4,
                Status = AbsenceSoft.Data.Enums.ProcessingStatus.Pending,
                CreatedById = User.DefaultUserId,
                ModifiedById = User.DefaultUserId,
                MimeType = "text/plain",
                Processed = 0L,
                Errors = 0L
            }
            .SetCreatedDate(DateTime.UtcNow)
            .SetModifiedDate(DateTime.UtcNow)
            .Save();

            string uploadId = upload.Id;

            using (var svc = new EligibilityUploadService())
            {
                svc.ProcessFile(new QueueItem()
                {
                    QueueName = MessageQueues.AtCommonQueue,
                    Body = uploadId,
                    Attributes = new Dictionary<string, string>(3)
                    {
                        { "EligibilityUploadId", uploadId },
                        { "CustomerId", "000000000000000000000002"}
                    }
                });

            }

            List<EligibilityUpload> lstEligibilityUpload = EligibilityUpload.AsQueryable().Where(s => s.LinkedEligibilityUploadId == uploadId).ToList();
            foreach (var eligibilityUpload in lstEligibilityUpload)
            {
                using (var svc = new EligibilityUploadService())
                {
                    svc.ProcessFile(new QueueItem()
                    {
                        QueueName = MessageQueues.AtCommonQueue,
                        Body = eligibilityUpload.Id,
                        Attributes = new Dictionary<string, string>(3)
                       {
                          { "EligibilityUploadId", eligibilityUpload.Id },
                          { "CustomerId", eligibilityUpload.CustomerId},
                          { "EmployerId", eligibilityUpload.EmployerId}
                        }
                    });
                }
            }
        }


        [TestMethod, TestCategory("Processing"), TestCategory("Eligibility Upload")]
        public void TestCustomFieldUpdate()
        {
            string rowText = @"F,000000002,121038,T1,,1,754091750";
            // Get the row source from the item.
            EligibilityRow row = new EligibilityRow(rowText.Split(',')) { Delimiter = ',' };
            Assert.IsFalse(row.IsError);

            EligibilityUpload upload = new EligibilityUpload()
            {
                CustomerId = "000000000000000000000002",
                EmployerId = "000000000000000000000002",
                FileKey = "debug/000000000000000000000002/000000000000000000000002/NO-FILE.txt",
                FileName = "NO-FILE.txt",
                Records = 1,
                Status = AbsenceSoft.Data.Enums.ProcessingStatus.Processing,
                CreatedById = User.DefaultUserId,
                ModifiedById = User.DefaultUserId,
                MimeType = "text/plain",
                Processed = 0L,
                Errors = 0L
            }
            .SetCreatedDate(DateTime.UtcNow)
            .SetModifiedDate(DateTime.UtcNow)
            .Save();

            EligibilityUploadResult result = new EligibilityUploadResult().SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow);
            result.RowNumber = 1;
            result.RowSource = rowText;
            result.RowParts = rowText.Split(',');
            result.CreatedById = User.DefaultUserId;
            result.ModifiedById = User.DefaultUserId;
            result.EligibilityUploadId = upload.Id;

            using (var svc = new EligibilityUploadService())
            {
                svc.ProcessRecords(upload, new List<EligibilityUploadResult>(1) { result });
            }

            if (result.IsError)
            {
                if (_context == null)
                    Console.WriteLine(result.Error);
                else
                    _context.WriteLine(result.Error);
            }
            Assert.IsFalse(result.IsError);
        }

        [TestMethod, TestCategory("Processing"), TestCategory("Eligibility Upload")]
        public void TestBasicScheduleUpdateConsecutive()
        {
            string rowText = @"E,300000001,Andersons,Harrie,,Service Rep,,NY,,2121111111,2122222222,2122222222,,seth@absencesoft.com,,123 Main St,,Manhattan,NY,10001,US,,,Supervisor,Jane,12125552525,seth@absencesoft.com,,Doe,John,12125552525,seth@absencesoft.com,,1/1/1980,M,N,Y,N,0,A,,80000,1,2/2/2009,3/3/2009,2/2/2009,2400,2016,0,480,120,480,120,480,0,N";
            // Get the row source from the item.
            EligibilityRow row = new EligibilityRow(rowText.Split(',')) { Delimiter = ',' };
            Assert.IsFalse(row.IsError);

            EligibilityUpload upload = new EligibilityUpload()
            {
                CustomerId = "000000000000000000000001",
                EmployerId = "000000000000000000000001",
                FileKey = "debug/000000000000000000000001/000000000000000000000001/NO-FILE2.txt",
                FileName = "NO-FILE2.txt",
                Records = 1,
                Status = AbsenceSoft.Data.Enums.ProcessingStatus.Processing,
                CreatedById = User.DefaultUserId,
                ModifiedById = User.DefaultUserId,
                MimeType = "text/plain",
                Processed = 0L,
                Errors = 0L
            }
            .SetCreatedDate(DateTime.UtcNow)
            .SetModifiedDate(DateTime.UtcNow)
            .Save();

            EligibilityUploadResult result = new EligibilityUploadResult().SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow);
            result.RowNumber = 1;
            result.RowSource = rowText;
            result.RowParts = rowText.Split(',');
            result.CreatedById = User.DefaultUserId;
            result.ModifiedById = User.DefaultUserId;
            result.EligibilityUploadId = upload.Id;

            using (var svc = new EligibilityUploadService())
            {
                svc.ProcessRecords(upload, new List<EligibilityUploadResult>(1) { result });
            }

            if (result.IsError)
            {
                if (_context == null)
                    Console.WriteLine(result.Error);
                else
                    _context.WriteLine(result.Error);
            }
            Assert.IsFalse(result.IsError);
        }



        [TestMethod]
        public void TestContactLoad()
        {
            EmployeeContact.Delete(EmployeeContact.Query.And(
                EmployeeContact.Query.EQ(c => c.CustomerId, "000000000000000000000002"),
                EmployeeContact.Query.EQ(c => c.EmployerId, "000000000000000000000002"),
                EmployeeContact.Query.EQ(c => c.EmployeeId, "000000000000000000000001")
            ));

            EligibilityUpload upload = new EligibilityUpload()
            {
                CustomerId = "000000000000000000000002",
                EmployerId = "000000000000000000000002",
                FileKey = "debug/000000000000000000000002/000000000000000000000002/NO-FILE3.txt",
                FileName = "NO-FILE3.txt",
                Records = 9,
                Status = AbsenceSoft.Data.Enums.ProcessingStatus.Processing,
                CreatedById = User.DefaultUserId,
                ModifiedById = User.DefaultUserId,
                MimeType = "text/plain",
                Processed = 0L,
                Errors = 0L
            }
            .SetCreatedDate(DateTime.UtcNow)
            .SetModifiedDate(DateTime.UtcNow)
            .Save();

            string[] rows = new string[]
            {
                @"O,000000001,HR,HRBP,0,1,000000002,Monkey,Chunky",
                @"O,000000001,HR,Super Dude,0,2,000000002,GaGa,Lady",
                @"O,000000001,HR,Awesome Guy,0,3,000000003,Jobs,Stevie",
                @"O,000000001,HR,Mega Girl,0,4,000000005,Woman,Wonder",
                @"O,000000001,PARENT,Mommy,0,,000000010,Grace,Nancy",
                @"O,000000001,SUPERVISOR,,1,,000000006,Lee,Stan",
                @"O,000000001,CHILD,,0,,,Price,Danny",
                @"O,000000001,CHILD,,0,,,Price,Jennifer",
                @"O,000000001,CHILD,,0,,,Price,Brandon"
            };

            int i = 0;
            List<EligibilityUploadResult> records = rows.Select(r =>
            {
                i++;
                EligibilityUploadResult result = new EligibilityUploadResult().SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow);
                result.RowNumber = i;
                result.RowSource = r;
                result.RowParts = r.Split(',');
                result.CreatedById = User.DefaultUserId;
                result.ModifiedById = User.DefaultUserId;
                result.EligibilityUploadId = upload.Id;
                return result;
            }).ToList();

            using (var svc = new EligibilityUploadService())
                svc.ProcessRecords(upload, records);

            Assert.IsFalse(records.Any(r => r.IsError));

            var contacts = EmployeeContact.AsQueryable().Where(c => c.EmployeeId == "000000000000000000000001").ToList();
            Assert.AreEqual(8, contacts.Count, "Expected 8 contacts total, 1 should have been negated out of 9 as a duplicate Contact Type + Employee ID");


            upload = new EligibilityUpload()
            {
                CustomerId = "000000000000000000000002",
                EmployerId = "000000000000000000000002",
                FileKey = "debug/000000000000000000000002/000000000000000000000002/NO-FILE4.txt",
                FileName = "NO-FILE4.txt",
                Records = 1,
                Status = AbsenceSoft.Data.Enums.ProcessingStatus.Processing,
                CreatedById = User.DefaultUserId,
                ModifiedById = User.DefaultUserId,
                MimeType = "text/plain",
                Processed = 0L,
                Errors = 0L
            }
            .SetCreatedDate(DateTime.UtcNow)
            .SetModifiedDate(DateTime.UtcNow)
            .Save();

            rows = new string[]
            {
                @"O,000000001,SUPERVISOR,,1,,000000009,Simpson,OJ"
            };

            i = 0;
            records = rows.Select(r =>
            {
                i++;
                EligibilityUploadResult result = new EligibilityUploadResult().SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow);
                result.RowNumber = i;
                result.RowSource = r;
                result.RowParts = r.Split(',');
                result.CreatedById = User.DefaultUserId;
                result.ModifiedById = User.DefaultUserId;
                result.EligibilityUploadId = upload.Id;
                return result;
            }).ToList();

            using (var svc = new EligibilityUploadService())
                svc.ProcessRecords(upload, records);

            Assert.IsFalse(records.Any(r => r.IsError));


            contacts = EmployeeContact.AsQueryable().Where(c => c.EmployeeId == "000000000000000000000001").ToList();
            Assert.AreEqual(8, contacts.Count, "Expected 8 contacts total, Supervisor should have been replaced");

            upload = new EligibilityUpload()
            {
                CustomerId = "000000000000000000000002",
                EmployerId = "000000000000000000000002",
                FileKey = "debug/000000000000000000000002/000000000000000000000002/NO-FILE5.txt",
                FileName = "NO-FILE5.txt",
                Records = 1,
                Status = AbsenceSoft.Data.Enums.ProcessingStatus.Processing,
                CreatedById = User.DefaultUserId,
                ModifiedById = User.DefaultUserId,
                MimeType = "text/plain",
                Processed = 0L,
                Errors = 0L
            }
            .SetCreatedDate(DateTime.UtcNow)
            .SetModifiedDate(DateTime.UtcNow)
            .Save();

            // Clear all other types
            rows = new string[]
            {
                @"O,000000001,SUPERVISOR,,1,,000000009,Sutherland,Keifer,,,,,,,,,,,,,Y"
            };

            i = 0;
            records = rows.Select(r =>
            {
                i++;
                EligibilityUploadResult result = new EligibilityUploadResult().SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow);
                result.RowNumber = i;
                result.RowSource = r;
                result.RowParts = r.Split(',');
                result.CreatedById = User.DefaultUserId;
                result.ModifiedById = User.DefaultUserId;
                result.EligibilityUploadId = upload.Id;
                return result;
            }).ToList();

            using (var svc = new EligibilityUploadService())
                svc.ProcessRecords(upload, records);

            Assert.IsFalse(records.Any(r => r.IsError));

            contacts = EmployeeContact.AsQueryable().Where(c => c.EmployeeId == "000000000000000000000001").ToList();
            //Assert.AreEqual(8, contacts.Count, "Expected 5 contacts total, 3 HR should have been removed out of the 8 contacts");
        }

        [TestMethod]
        public void TestIntermittentOnELFile()
        {
            Case.Delete(Case.Query.EQ(c => c.Employee.Id, "000000000000000000000002"));

            string rowText = @"C,000000002,TestFMLA,1Year,TEST-ABCDEFG04,2,FHC,7,1,3,4,4,1,2014-09-01,2014-03-01,2014-03-31,,,0,,,,,,,,N,,N";
            // Get the row source from the item.
            CaseRow row = new CaseRow(rowText);
            Assert.IsFalse(row.IsError);

            //
            // Create Case
            CaseRow.ApplicationResult result = row.Apply("000000000000000000000002", "000000000000000000000002", User.DefaultUserId);
            if (result == CaseRow.ApplicationResult.Fail)
            {
                if (_context == null)
                    Console.WriteLine(row.Error);
                else
                    _context.WriteLine(row.Error);
            }
            Assert.AreEqual(CaseRow.ApplicationResult.Success, result);

            //
            // Adjudicate
            rowText = @"A,000000002,TestFMLA,1Year,TEST-ABCDEFG04,2014-03-01,2014-03-31,1,,FMLA,Y";
            row = new CaseRow(rowText);
            result = row.Apply("000000000000000000000002", "000000000000000000000002", User.DefaultUserId);
            if (result == CaseRow.ApplicationResult.Fail)
            {
                if (_context == null)
                    Console.WriteLine(row.Error);
                else
                    _context.WriteLine(row.Error);
            }
            Assert.AreEqual(CaseRow.ApplicationResult.Success, result);

            EligibilityUpload upload = new EligibilityUpload()
            {
                CustomerId = "000000000000000000000002",
                EmployerId = "000000000000000000000002",
                FileKey = "debug/000000000000000000000002/000000000000000000000002/NO-FILE4.txt",
                FileName = "NO-FILE4.txt",
                Records = 6,
                Status = AbsenceSoft.Data.Enums.ProcessingStatus.Processing,
                CreatedById = User.DefaultUserId,
                ModifiedById = User.DefaultUserId,
                MimeType = "text/plain",
                Processed = 0L,
                Errors = 0L
            }
            .SetCreatedDate(DateTime.UtcNow)
            .SetModifiedDate(DateTime.UtcNow)
            .Save();

            string[] rows = new string[]
            {
                @"I,000000002,TestFMLA,1Year,TEST-ABCDEFG04,2014-03-04,240,0,60,FMLA,Y,,0",
                @"I,000000002,TestFMLA,1Year,TEST-ABCDEFG04,2014-03-05,480,0,0,,Y,,1",
                @"I,000000002,TestFMLA,1Year,TEST-ABCDEFG04,2014-03-07,240,0,0,,Y,,0",
                @"I,000000002,TestFMLA,1Year,TEST-ABCDEFG04,2014-03-11,480,0,60,,Y,,1", // More time than worked, should fail
                @"I,000000002,TestFMLA,1Year,TEST-ABCDEFG04,2014-03-13,480,0,0,,Y,,0",
                @"I,000000002,TestFMLA,1Year,TEST-ABCDEFG04,2014-03-09,480,0,0,,Y,,1" // Sunday, should fail
            };

            int i = 0;
            List<EligibilityUploadResult> records = rows.Select(r =>
            {
                i++;
                EligibilityUploadResult eResult = new EligibilityUploadResult().SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow);
                eResult.RowNumber = i;
                eResult.RowSource = r;
                eResult.RowParts = r.Split(',');
                eResult.CreatedById = User.DefaultUserId;
                eResult.ModifiedById = User.DefaultUserId;
                eResult.EligibilityUploadId = upload.Id;
                return eResult;
            }).ToList();

            using (var svc = new EligibilityUploadService())
                svc.ProcessRecords(upload, records);

            Assert.AreEqual(1, upload.Errors, string.Join("\n", records.Where(r => r.IsError).Select(r => r.Error)));

            Case myCase = Case.AsQueryable().Where(c => c.Employee.Id == "000000000000000000000002" && c.CaseNumber == "TEST-ABCDEFG04").FirstOrDefault();
            Assert.IsNotNull(myCase);
        }

        [TestMethod]
        public void TestOfficeLocationUpdate()
        {
            string rowText = @"E,300000001,Andersons,Harrie,,Service Rep,Home Office,NY,,2121111111,2122222222,2122222222,,seth@absencesoft.com,,123 Main St,,Manhattan,NY,10001,US,,,Supervisor,Jane,12125552525,seth@absencesoft.com,,Doe,John,12125552525,seth@absencesoft.com,,1/1/1980,M,N,Y,N,0,A,,80000,1,2/2/2009,3/3/2009,2/2/2009,2400,2016,0,480,480,480,480,480,0,N";
            string customerId = "000000000000000000000001";
            string employerId = "000000000000000000000001";
            // Get the row source from the item.
            EligibilityRow row = new EligibilityRow(rowText.Split(',')) { Delimiter = ',' };
            Assert.IsFalse(row.IsError);

            EligibilityUpload upload = new EligibilityUpload()
            {
                CustomerId = customerId,
                EmployerId = employerId,
                FileKey = "debug/000000000000000000000001/000000000000000000000001/NO-FILE.txt",
                FileName = "NO-FILE.txt",
                Records = 1,
                Status = AbsenceSoft.Data.Enums.ProcessingStatus.Processing,
                CreatedById = User.DefaultUserId,
                ModifiedById = User.DefaultUserId,
                MimeType = "text/plain",
                Processed = 0L,
                Errors = 0L
            }
            .SetCreatedDate(DateTime.UtcNow)
            .SetModifiedDate(DateTime.UtcNow)
            .Save();

            EligibilityUploadResult result = new EligibilityUploadResult().SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow);
            result.RowNumber = 1;
            result.RowSource = rowText;
            result.RowParts = rowText.Split(',');
            result.CreatedById = User.DefaultUserId;
            result.ModifiedById = User.DefaultUserId;
            result.EligibilityUploadId = upload.Id;

            using (var svc = new EligibilityUploadService())
            {
                svc.ProcessRecords(upload, new List<EligibilityUploadResult>(1) { result });
            }
            Employee emp = Employee.AsQueryable().FirstOrDefault(e => e.EmployeeNumber == "300000001" && e.CustomerId == customerId && e.EmployerId == employerId);
            Organization homeOffice = Organization.GetByCode("HOME OFFICE", emp.CustomerId, emp.EmployerId);
            Assert.IsNotNull(homeOffice);
            Assert.AreEqual(homeOffice.Name, "Home Office");

            EmployeeOrganization employeeLocation = emp.GetOfficeLocation();
            Assert.IsNotNull(employeeLocation);
            Assert.AreEqual(homeOffice.Name, employeeLocation.Name);

            if (result.IsError)
            {
                if (_context == null)
                    Console.WriteLine(result.Error);
                else
                    _context.WriteLine(result.Error);
            }
            Assert.IsFalse(result.IsError);

            // Now, we need to do it for another employee.... That way we can change 

            rowText = @"E,300000002,Fisher,Carrie,,Psyco,Home Office,NY,,2121111111,2122222222,2122222222,,carrie@absencesoft.com,,123 Main St,,Manhattan,NY,10001,US,,,Supervisor,Jane,12125552525,seth@absencesoft.com,,Doe,John,12125552525,seth@absencesoft.com,,1/1/1980,M,N,Y,N,0,A,,80000,1,2/2/2009,3/3/2009,2/2/2009,2400,2016,0,480,480,480,480,480,0,N";

            // Get the row source from the item.
            row = new EligibilityRow(rowText.Split(',')) { Delimiter = ',' };
            Assert.IsFalse(row.IsError);

            upload = new EligibilityUpload()
            {
                CustomerId = customerId,
                EmployerId = employerId,
                FileKey = "debug/000000000000000000000001/000000000000000000000001/NO-FILE2.txt",
                FileName = "NO-FILE2.txt",
                Records = 1,
                Status = AbsenceSoft.Data.Enums.ProcessingStatus.Processing,
                CreatedById = User.DefaultUserId,
                ModifiedById = User.DefaultUserId,
                MimeType = "text/plain",
                Processed = 0L,
                Errors = 0L
            }
            .SetCreatedDate(DateTime.UtcNow)
            .SetModifiedDate(DateTime.UtcNow)
            .Save();

            result = new EligibilityUploadResult().SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow);
            result.RowNumber = 1;
            result.RowSource = rowText;
            result.RowParts = rowText.Split(',');
            result.CreatedById = User.DefaultUserId;
            result.ModifiedById = User.DefaultUserId;
            result.EligibilityUploadId = upload.Id;

            using (var svc = new EligibilityUploadService())
            {
                svc.ProcessRecords(upload, new List<EligibilityUploadResult>(1) { result });
            }

            // now, ensure our first employee still belongs to this office.

            emp = Employee.AsQueryable().FirstOrDefault(e => e.EmployeeNumber == "300000001" && e.CustomerId == customerId && e.EmployerId == employerId);
            homeOffice = Organization.GetByCode("HOME OFFICE", emp.CustomerId, emp.EmployerId);
            Assert.IsNotNull(homeOffice);
            Assert.AreEqual(homeOffice.Name, "Home Office");

            employeeLocation = emp.GetOfficeLocation();
            Assert.IsNotNull(employeeLocation);
            Assert.AreEqual(homeOffice.Name, employeeLocation.Name);

            if (result.IsError)
            {
                if (_context == null)
                    Console.WriteLine(result.Error);
                else
                    _context.WriteLine(result.Error);
            }
            Assert.IsFalse(result.IsError);
        }


        [TestMethod]
        public void TestJobRowUpdate()
        {
            var rows = new string[] {
                "E,300000001,Andersons,Harrie,,Service Rep,Home Office,NY,,2121111111,2122222222,2122222222,,seth@absencesoft.com,,123 Main St,,Manhattan,NY,10001,US,,,Supervisor,Jane,12125552525,seth@absencesoft.com,,Doe,John,12125552525,seth@absencesoft.com,,1/1/1980,M,N,Y,N,0,A,,80000,1,2/2/2009,3/3/2009,2/2/2009,2400,2016,0,480,480,480,480,480,0,N",
                "J,300000001,BUTT,Button Pusher,Sr. Button Pusher,2016-01-01,,"
            };
            string customerId = "000000000000000000000001";
            string employerId = "000000000000000000000001";

            EligibilityUpload upload = new EligibilityUpload()
            {
                CustomerId = customerId,
                EmployerId = employerId,
                FileKey = "debug/000000000000000000000001/000000000000000000000001/NO-FILE.txt",
                FileName = "NO-FILE.txt",
                Records = 2,
                Status = AbsenceSoft.Data.Enums.ProcessingStatus.Processing,
                CreatedById = User.DefaultUserId,
                ModifiedById = User.DefaultUserId,
                MimeType = "text/plain",
                Processed = 0L,
                Errors = 0L
            }
            .SetCreatedDate(DateTime.UtcNow)
            .SetModifiedDate(DateTime.UtcNow)
            .Save();

            int i = 0;
            List<EligibilityUploadResult> records = rows.Select(r =>
            {
                i++;
                // Get the row source from the item.
                EligibilityRow row = new EligibilityRow(r.Split(',')) { Delimiter = ',' };
                row.IsError.Should().BeFalse();

                EligibilityUploadResult eResult = new EligibilityUploadResult().SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow);
                eResult.RowNumber = i;
                eResult.RowSource = r;
                eResult.RowParts = r.Split(',');
                eResult.CreatedById = User.DefaultUserId;
                eResult.ModifiedById = User.DefaultUserId;
                eResult.EligibilityUploadId = upload.Id;
                return eResult;
            }).ToList();

            using (var svc = new EligibilityUploadService())
                svc.ProcessRecords(upload, records);

            foreach (var result in records)
            {
                if (result.IsError)
                {
                    if (_context == null)
                        Console.WriteLine(result.Error);
                    else
                        _context.WriteLine(result.Error);
                }
                result.IsError.Should().BeFalse();
            }

            Employee emp = Employee.AsQueryable().FirstOrDefault(e => e.EmployeeNumber == "300000001" && e.CustomerId == customerId && e.EmployerId == employerId);

            var jobs = emp.GetJobs();
            jobs.Should().NotBeNull().And.HaveCount(1);

            var active = new JobService(emp.CustomerId, emp.EmployerId, null).Using(s => s.GetCurrentEmployeeJob(emp));
            active.Should().NotBeNull();

            Job.GetByCode(active.JobCode, active.CustomerId, active.EmployerId).Should().NotBeNull().And.Subject.As<Job>().Name.Should().Be("Button Pusher");
        }

        [TestMethod]
        public void TestEmployeeOrganizationUpdate()
        {
            TestBasicScheduleUpdate();
            string rowText = @"G,300000001,NORTHOFFICE,,OFFICELOCATION,2/2/2009,,1,HEADOFFICE,,OFFICELOCATION";
            string customerId = "000000000000000000000001";
            string employerId = "000000000000000000000001";
            // Get the row source from the item.
            EligibilityRow row = new EligibilityRow(rowText.Split(',')) { Delimiter = ',' };
            Assert.IsFalse(row.IsError);

            EligibilityUpload upload = new EligibilityUpload()
            {
                CustomerId = customerId,
                EmployerId = employerId,
                FileKey = "debug/000000000000000000000001/000000000000000000000001/NO-FILE6.txt",
                FileName = "NO-FILE6.txt",
                Records = 1,
                Status = AbsenceSoft.Data.Enums.ProcessingStatus.Processing,
                CreatedById = User.DefaultUserId,
                ModifiedById = User.DefaultUserId,
                MimeType = "text/plain",
                Processed = 0L,
                Errors = 0L
            }
            .SetCreatedDate(DateTime.UtcNow)
            .SetModifiedDate(DateTime.UtcNow)
            .Save();

            EligibilityUploadResult result = new EligibilityUploadResult().SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow);
            result.RowNumber = 1;
            result.RowSource = rowText;
            result.RowParts = rowText.Split(',');
            result.CreatedById = User.DefaultUserId;
            result.ModifiedById = User.DefaultUserId;
            result.EligibilityUploadId = upload.Id;

            using (var svc = new EligibilityUploadService())
            {
                svc.ProcessRecords(upload, new List<EligibilityUploadResult>(1) { result });
            }
            Employee emp = Employee.AsQueryable().FirstOrDefault(e => e.EmployeeNumber == "300000001" && e.CustomerId == customerId && e.EmployerId == employerId);


            Organization parentOffice = Organization.GetByCode("HEADOFFICE", emp.CustomerId, emp.EmployerId);
            Assert.IsNotNull(parentOffice);
            Assert.AreEqual(parentOffice.Path, "/HEADOFFICE/");

            Organization homeOffice = Organization.GetByCode("NORTHOFFICE", emp.CustomerId, emp.EmployerId);
            Assert.IsNotNull(homeOffice);
            Assert.AreEqual(homeOffice.Path, "/HEADOFFICE/NORTHOFFICE/");

            EmployeeOrganization employeeLocation = emp.GetOfficeLocation();
            Assert.IsNotNull(employeeLocation);
            Assert.AreEqual(homeOffice.Code, employeeLocation.Code);
            Assert.AreEqual(employeeLocation.Dates.StartDate.Date, new DateTime(2009, 2, 2).Date);
            Assert.AreEqual(employeeLocation.Path, "/HEADOFFICE/NORTHOFFICE/");

            if (result.IsError)
            {
                if (_context == null)
                    Console.WriteLine(result.Error);
                else
                    _context.WriteLine(result.Error);
            }
            Assert.IsFalse(result.IsError);
        }


        /// <summary>
        /// Tests the multiple employers in one file.
        /// </summary>
        [Ignore, TestMethod, TestCategory("Processing"), TestCategory("Eligibility Upload")]
        public void TestMultipleEmployersInOneFile()
        {
            Contact testContact = new Contact()
            {
                Email = "TestMultipleEmployersInOneFile@absencesoft.com",
                WorkPhone = "+1.303.332.6852",
                FirstName = "Unit",
                LastName = "Test",
                Address = new Address()
                {
                    Address1 = "10200 W 44th AVE",
                    Address2 = "SUITE 140",
                    City = "Wheat Ridge",
                    State = "CO",
                    PostalCode = "80033"
                },
            };
            Customer testCustomer = new Customer()
            {
                Name = "TestMultipleEmployersInOneFile",
                Url = "testmultipleemployersinonefile",
                Contact = testContact,
                CreatedById = "000000000000000000000000",
                ModifiedById = "000000000000000000000000",
                Features = new List<AppFeature>()
                {
                    new AppFeature(Feature.ADA),
                    new AppFeature(Feature.ShortTermDisability),
                    new AppFeature(Feature.LOA),
                    new AppFeature(Feature.MultiEmployerAccess)
                },
                EnableAllEmployersByDefault = true
            }.Save();

            var addEmployer = new Func<string, Employer>(refCode => Employer.GetByUrl(string.Concat(testCustomer.Name, "-", refCode)) ?? new Employer()
            {
                Name = string.Concat(testCustomer.Name, ".", refCode),
                Url = string.Concat(testCustomer.Name, "-", refCode).ToLowerInvariant(),
                ReferenceCode = refCode,
                Contact = testContact,
                Customer = testCustomer,
                CustomerId = testCustomer.Id,
                Ein = new CryptoString("11-111-1111"),
                ResetMonth = Month.January,
                ResetDayOfMonth = 1,
                FMLPeriodType = PeriodType.RollingBack,
                FTWeeklyWorkHours = 35,
                Holidays = new List<Holiday>()
                {
                    new Holiday() { Name = "New Year’s Day",                Month = Month.January,      DayOfMonth = 1, StartDate = new DateTime(2010,1,1, 0, 0, 0, DateTimeKind.Utc), OrClosestWeekday = true },
                    new Holiday() { Name = "Martin Luther King’s Birthday", Month = Month.January,      WeekOfMonth = 3,    DayOfWeek = DayOfWeek.Monday, StartDate = new DateTime(2010,1,1, 0, 0, 0, DateTimeKind.Utc) },
                    new Holiday() { Name = "George Washington's Birthday",  Month = Month.February,     WeekOfMonth = 3,    DayOfWeek = DayOfWeek.Monday, StartDate = new DateTime(2010,1,1, 0, 0, 0, DateTimeKind.Utc) },
                    new Holiday() { Name = "Memorial Day",                  Month = Month.May,          WeekOfMonth = -1,   DayOfWeek = DayOfWeek.Monday, StartDate = new DateTime(2010,1,1, 0, 0, 0, DateTimeKind.Utc) },
                    new Holiday() { Name = "Independence Day",              Month = Month.July,         DayOfMonth = 4, StartDate = new DateTime(2010,1,1, 0, 0, 0, DateTimeKind.Utc), OrClosestWeekday = true },
                    new Holiday() { Name = "Labor Day",                     Month = Month.September,    WeekOfMonth = 1,    DayOfWeek = DayOfWeek.Monday, StartDate = new DateTime(2010,1,1, 0, 0, 0, DateTimeKind.Utc) },
                    new Holiday() { Name = "Columbus Day",                  Month = Month.October,      WeekOfMonth = 2,    DayOfWeek = DayOfWeek.Monday, StartDate = new DateTime(2010,1,1, 0, 0, 0, DateTimeKind.Utc) },
                    new Holiday() { Name = "Veterans Day",                  Month = Month.November,     DayOfMonth = 11, StartDate = new DateTime(2010,1,1, 0, 0, 0, DateTimeKind.Utc), OrClosestWeekday = true },
                    new Holiday() { Name = "Thanksgiving Day",              Month = Month.November,     WeekOfMonth = 4,    DayOfWeek = DayOfWeek.Thursday, StartDate = new DateTime(2010,1,1, 0, 0, 0, DateTimeKind.Utc) },
                    new Holiday() { Name = "Christmas Day",                 Month = Month.December,     DayOfMonth = 25, StartDate = new DateTime(2010,1,1, 0, 0, 0, DateTimeKind.Utc), OrClosestWeekday = true }
                },
                CreatedById = "000000000000000000000000",
                ModifiedById = "000000000000000000000000"
            }.Save());

            var tjHooker = addEmployer("TJHooker");
            var milliVanilli = addEmployer("MilliVanilli");
            var happyDays = addEmployer("HappyDays");
            var jerrySpringer = addEmployer("JerrySpringer");

            using (EligibilityUploadService svc = new EligibilityUploadService())
            {
                long fileSize = 0L;
                string locator = null;
                using (MemoryStream fakeFile = new MemoryStream())
                using (StreamWriter writer = new StreamWriter(fakeFile))
                using (FileService fs = new FileService())
                {
                    // Create a file with 4 employers, mixed record types, not all in the same order.
                    writer.WriteLine(@"E,000000001,Hooker,T.J.,,Police Detective,,NY,,2121111111,2122222222,2122222222,,TestMultipleEmployersInOneFile@absencesoft.com,,123 Main St,,Manhattan,NY,10001,US,,,Supervisor,Jane,12125552525,seth@absencesoft.com,,Doe,John,12125552525,seth@absencesoft.com,,1/1/1980,M,N,Y,N,0,A,,80000,1,2/2/2009,3/3/2009,2/2/2009,2400,2016,0,480,480,480,480,480,0,N,,,,,,TJHooker,");
                    writer.WriteLine(@"E,000000001,Springer,Jerry,,Talk Show Host,,NY,,2121111111,2122222222,2122222222,,TestMultipleEmployersInOneFile@absencesoft.com,,123 Main St,,Manhattan,NY,10001,US,,,Supervisor,Jane,12125552525,seth@absencesoft.com,,Doe,John,12125552525,seth@absencesoft.com,,1/1/1980,M,N,Y,N,0,A,,80000,1,2/2/2009,3/3/2009,2/2/2009,2400,2016,0,480,480,480,480,480,0,N,,,,,,JerrySpringer,");
                    writer.WriteLine(@"O,000000001,CHILD,,0,,,Shatner,William,,,,,,,,,,,,,,TJHooker,");
                    writer.WriteLine(@"O,000000001,CHILD,,0,,,Richard,Herd,,,,,,,,,,,,,,TJHooker,");
                    writer.WriteLine(@"O,000000001,CHILD,,0,,,Heather,Locklear,,,,,,,,,,,,,,TJHooker,");
                    writer.WriteLine(@"O,000000001,CHILD,,0,,,Farian,Frank,,,,,,,,,,,,,,MilliVanilli,");
                    writer.WriteLine(@"O,000000001,CHILD,,0,,,Cunningham,Richie,,,,,,,,,,,,,,HappyDays,");
                    writer.WriteLine(@"G,000000001,NORTHOFFICE,,OFFICELOCATION,2/2/2009,,1,HEADOFFICE,,OFFICELOCATION,HappyDays,");
                    writer.WriteLine(@"O,000000001,CHILD,,0,,,Morvan,Fab,,,,,,,,,,,,,,MilliVanilli,");
                    writer.WriteLine(@"O,000000001,CHILD,,0,,,Pilatus,Rob,,,,,,,,,,,,,,MilliVanilli,");
                    writer.WriteLine(@"O,000000001,CHILD,,0,,,Fonzarelli,Arthur,,,,,,,,,,,,,,HappyDays,");
                    writer.WriteLine(@"E,000000001,Vanilli,Milli,,Musical Fraud and 1-Hit Wonder,,NY,,2121111111,2122222222,2122222222,,TestMultipleEmployersInOneFile@absencesoft.com,,123 Main St,,Manhattan,NY,10001,US,,,Supervisor,Jane,12125552525,seth@absencesoft.com,,Doe,John,12125552525,seth@absencesoft.com,,1/1/1980,M,N,Y,N,0,A,,80000,1,2/2/2009,3/3/2009,2/2/2009,2400,2016,0,480,480,480,480,480,0,N,,,,,,MilliVanilli,");
                    writer.WriteLine(@"E,000000001,Days,Happy,,Diner,,NY,,2121111111,2122222222,2122222222,,TestMultipleEmployersInOneFile@absencesoft.com,,123 Main St,,Manhattan,NY,10001,US,,,Supervisor,Jane,12125552525,seth@absencesoft.com,,Doe,John,12125552525,seth@absencesoft.com,,1/1/1980,M,N,Y,N,0,A,,80000,1,2/2/2009,3/3/2009,2/2/2009,2400,2016,0,480,480,480,480,480,0,N,,,,,,HappyDays,");
                    writer.WriteLine(@"O,000000001,CHILD,,0,,,Springer,Katie,,,,,,,,,,,,,,JerrySpringer,");
                    writer.WriteLine(@"O,000000001,CHILD,,0,,,Malph,Ralph,,,,,,,,,,,,,,HappyDays,");
                    writer.WriteLine(@"O,000000001,SPOUSE,,0,,,Velton,Micki,,,,,,,,,,,,,,JerrySpringer,");
                    writer.WriteLine(@"O,000000001,SIBLING,,0,,,Lady,Drunk,,,,,,,,,,,,,,JerrySpringer,");
                    writer.WriteLine(@"G,000000001,NORTHOFFICE,,OFFICELOCATION,2/2/2009,,1,HEADOFFICE,,OFFICELOCATION,TJHooker,");
                    writer.WriteLine(@"G,000000001,NORTHOFFICE,,OFFICELOCATION,2/2/2009,,1,HEADOFFICE,,OFFICELOCATION,MilliVanilli,");
                    writer.WriteLine(@"G,000000001,NORTHOFFICE,,OFFICELOCATION,2/2/2009,,1,HEADOFFICE,,OFFICELOCATION,JerrySpringer,");
                    writer.WriteLine(@"E,000000001,Tripper,Jack,,Culinary Student and Bachelor,,NY,,2121111111,2122222222,2122222222,,TestMultipleEmployersInOneFile@absencesoft.com,,123 Main St,,Manhattan,NY,10001,US,,,Supervisor,Jane,12125552525,seth@absencesoft.com,,Doe,John,12125552525,seth@absencesoft.com,,1/1/1980,M,N,Y,N,0,A,,80000,1,2/2/2009,3/3/2009,2/2/2009,2400,2016,0,480,480,480,480,480,0,N,,,,,,ThreesCompany,");

                    writer.Flush();
                    fakeFile.Position = 0;
                    fileSize = fakeFile.Length;

                    locator = fs.UploadFile(fakeFile, "NO-FILE.txt", Settings.Default.S3BucketName_Processing, testCustomer.Id, happyDays.Id);
                }

                EligibilityUpload upload = new EligibilityUpload()
                {
                    // Interesting stuff
                    FileKey = locator,
                    FileName = "NO-FILE.txt",
                    Size = fileSize,
                    Status = ProcessingStatus.Pending,
                    MimeType = "text/plain",
                    Records = 0L,
                    Processed = 0L,
                    Errors = 0L,
                    // Boring audit fields and identifiers
                    CreatedById = User.System.Id,
                    CreatedBy = User.System,
                    ModifiedById = User.System.Id,
                    ModifiedBy = User.System,
                    CustomerId = testCustomer.Id,
                    Customer = testCustomer,
                    EmployerId = happyDays.Id,
                    Employer = happyDays
                }.Save();

                svc.ProcessFile(new QueueItem()
                {
                    QueueName = MessageQueues.AtCommonQueue,
                    Body = upload.Id,
                    Attributes = new Dictionary<string, string>(3)
                    {
                        { "EligibilityUploadId", upload.Id },
                        { "CustomerId", testCustomer.Id },
                        { "EmployerId", happyDays.Id }
                    }
                });

                upload = EligibilityUpload.GetById(upload.Id);
                upload.Errors.Should().Be(1, "there should be 1 error on this file, otherwise it should be perfect and flawless like its creator", upload.Errors);
                upload.Records.Should().Be(6, "there were only 5 records for Happy Days and 1 invalid record");
                upload.Status.Should().Be(ProcessingStatus.Complete, "this file should have been processed already and complete");

                var egRecs = EligibilityUpload.AsQueryable().Where(r => r.CustomerId == testCustomer.Id).ToList();
                egRecs.Count().Should().Be(4, "there should be 4 employer files, 3 of which were split from the original");

                foreach (var otherUpload in egRecs.Where(r => r.Id != upload.Id)) // Get all other eligibility uploads
                {
                    otherUpload.Status.Should().Be(ProcessingStatus.Pending, "each new upload from a file split should be pending, this one is {1} for {0}", otherUpload.Employer.Name, otherUpload.Status);
                    svc.ProcessFile(new QueueItem()
                    {
                        QueueName = MessageQueues.AtCommonQueue,
                        Body = otherUpload.Id,
                        Attributes = new Dictionary<string, string>(3)
                        {
                            { "EligibilityUploadId", otherUpload.Id },
                            { "CustomerId", otherUpload.CustomerId },
                            { "EmployerId", otherUpload.EmployerId }
                        }
                    });
                    var latestVersion = EligibilityUpload.GetById(otherUpload.Id); // need to re-read this dude from the database 'n' stuff
                    latestVersion.Errors.Should().Be(0, "there should be no errors on the file for {0}, it should be perfect and flawless like its creator", latestVersion.Employer.Name);
                    latestVersion.Records.Should().Be(5, "there should be exactly 5 records for {0}", latestVersion.Employer.Name);
                    latestVersion.Processed.Should().Be(5, "should have processed 5 records for {0}", latestVersion.Employer.Name);
                    latestVersion.Status.Should().Be(ProcessingStatus.Complete, "the file for {0} should have been processed already and complete", latestVersion.Employer.Name);
                }

                Employee songFraud = Employee.AsQueryable().Where(e => e.CustomerId == testCustomer.Id && e.EmployerId == milliVanilli.Id && e.EmployeeNumber == "000000001").FirstOrDefault();
                songFraud.Should().NotBeNull();

                EmployeeContact.AsQueryable().Where(c => c.EmployeeId == songFraud.Id).Count().Should().Be(5, "there should be 5 contacts loaded for Milli Vanilli");
            }
        }


        [TestMethod, TestCategory("Processing"), TestCategory("Eligibility Upload")]
        public void TestScheduleNoOverwriteFlag()
        {
            Employee emp = Employee.AsQueryable().Where(e => e.EmployerId == "000000000000000000000001" && e.EmployeeNumber == "300000001").FirstOrDefault();
            if (emp == null)
            {
                TestBasicScheduleUpdate();
                emp = Employee.AsQueryable().Where(e => e.EmployerId == "000000000000000000000001" && e.EmployeeNumber == "300000001").FirstOrDefault();
            }
            emp.Should().NotBeNull();

            var sd = new DateTime(2016, 1, 3).ToMidnight();
            new EmployeeService().Using(s => s.SetWorkSchedule(emp, new Schedule()
            {
                StartDate = sd,
                ScheduleType = ScheduleType.Weekly,
                Times = new List<Time>(7)
                {
                    new Time() { SampleDate = sd.GetFirstDayOfWeek(), TotalMinutes = 0 },
                    new Time() { SampleDate = sd.GetFirstDayOfWeek().AddDays(1), TotalMinutes = 360 },
                    new Time() { SampleDate = sd.GetFirstDayOfWeek().AddDays(2), TotalMinutes = 360 },
                    new Time() { SampleDate = sd.GetFirstDayOfWeek().AddDays(3), TotalMinutes = 360 },
                    new Time() { SampleDate = sd.GetFirstDayOfWeek().AddDays(4), TotalMinutes = 360 },
                    new Time() { SampleDate = sd.GetFirstDayOfWeek().AddDays(5), TotalMinutes = 360 },
                    new Time() { SampleDate = sd.GetLastDayOfWeek(), TotalMinutes = 0 },
                }
            }, null));

            emp.Save();

            string rowText = @"W,300000001,2400,1250,,,,,,,,Y,2016-01-01,,Y";
            // Get the row source from the item.
            EligibilityRow row = new EligibilityRow(rowText.Split(',')) { Delimiter = ',' };
            Assert.IsFalse(row.IsError);

            EligibilityUpload upload = new EligibilityUpload()
            {
                CustomerId = "000000000000000000000001",
                EmployerId = "000000000000000000000001",
                FileKey = "debug/000000000000000000000001/000000000000000000000001/NO-FILE99.txt",
                FileName = "NO-FILE99.txt",
                Records = 1,
                Status = AbsenceSoft.Data.Enums.ProcessingStatus.Processing,
                CreatedById = User.DefaultUserId,
                ModifiedById = User.DefaultUserId,
                MimeType = "text/plain",
                Processed = 0L,
                Errors = 0L
            }
            .SetCreatedDate(DateTime.UtcNow)
            .SetModifiedDate(DateTime.UtcNow)
            .Save();

            EligibilityUploadResult result = new EligibilityUploadResult().SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow);
            result.RowNumber = 1;
            result.RowSource = rowText;
            result.RowParts = rowText.Split(',');
            result.CreatedById = User.DefaultUserId;
            result.ModifiedById = User.DefaultUserId;
            result.EligibilityUploadId = upload.Id;

            using (var svc = new EligibilityUploadService())
            {
                svc.ProcessRecords(upload, new List<EligibilityUploadResult>(1) { result });
            }

            if (result.IsError)
            {
                if (_context == null)
                    Console.WriteLine(result.Error);
                else
                    _context.WriteLine(result.Error);
            }
            Assert.IsFalse(result.IsError);

            emp = Employee.GetById(emp.Id);
            emp.WorkSchedules.Count.Should().Be(2);
            emp.WorkSchedules.All(w => w.ScheduleType != ScheduleType.Variable).Should().BeTrue();
            emp.WorkSchedules[1].Times[1].TotalMinutes.Should().Be(360);
        }
    }
}
