﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Processing;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Logic.Processing;
using AbsenceSoft.Logic.Processing.EL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AbsenceSoft.Tests.Processing
{
    [TestClass]
    public class CaseExportTests
    {
    
        private const string _allId = "000000000000000000000002";

        [TestMethod, TestCategory("Processing"), TestCategory("Case Export")]
        public void TestCaseExportOfDocuments()
        {
           //// //Create Case
           bool IsSuccess = false;
            string rowText = @"C,000000002,TestFMLA,1Year,TestCaseExport0011,2,FHC,7,1,3,4,4,1,2014-09-01,2014-03-01,2014-03-31,,,0,,,,,,,,N,,N";

            CaseRow row = new CaseRow(rowText);
            Assert.IsFalse(row.IsError, row.Error);


            // Create Case
            CaseRow.ApplicationResult result = row.Apply(_allId, _allId, User.DefaultUserId);
            if (result == CaseRow.ApplicationResult.Fail)
            {
                Assert.IsFalse(row.IsError, row.Error);
            }

            Assert.AreEqual(CaseRow.ApplicationResult.Success, result, row.Error);

            var testCase = Case.AsQueryable().Where(c => c.CustomerId == _allId && c.EmployerId == _allId && c.CaseNumber == "TestCaseExport0011").FirstOrDefault();

            Attachment attachment = new Attachment()
            {
                EmployeeId = testCase.Employee.Id,
                CaseId = testCase.Id,
                ContentLength = AbsenceSoft.Tests.Resources.AttachementResource.caseExportAttachement_1.Length,
                ContentType = "application/pdf",
                FileName = "caseExportAttachement_1.pdf",
                CustomerId = testCase.CustomerId,
                EmployerId = testCase.EmployerId,
                File = AbsenceSoft.Tests.Resources.AttachementResource.caseExportAttachement_1
            };

            new AttachmentService().Using(svc => svc.CreateAttachment(attachment));

            attachment = new Attachment()
            {
                EmployeeId = testCase.Employee.Id,
                CaseId = testCase.Id,
                ContentLength = AbsenceSoft.Tests.Resources.AttachementResource.caseExportAttachement_2.Length,
                ContentType = "application/pdf",
                FileName = "caseExportAttachement_2.pdf",
                CustomerId = testCase.CustomerId,
                EmployerId = testCase.EmployerId,
                File = AbsenceSoft.Tests.Resources.AttachementResource.caseExportAttachement_2
            };

            new AttachmentService().Using(svc => svc.CreateAttachment(attachment));

            attachment = new Attachment()
            {
                EmployeeId = testCase.Employee.Id,
                CaseId = testCase.Id,
                ContentLength = AbsenceSoft.Tests.Resources.AttachementResource.caseExportAttachement_3.Length,
                ContentType = "application/vnd.ms-word",
                FileName = "caseExportAttachement_3.docx",
                CustomerId = testCase.CustomerId,
                EmployerId = testCase.EmployerId,
                File = AbsenceSoft.Tests.Resources.AttachementResource.caseExportAttachement_3
            };

            new AttachmentService().Using(svc => svc.CreateAttachment(attachment));

            attachment = new Attachment()
            {
                EmployeeId = testCase.Employee.Id,
                CaseId = testCase.Id,
                ContentLength = AbsenceSoft.Tests.Resources.AttachementResource.caseattachement.Length,
                ContentType = "text/plain",
                FileName = "caseattachement.txt",
                CustomerId = testCase.CustomerId,
                EmployerId = testCase.EmployerId,
                File = AbsenceSoft.Tests.Resources.AttachementResource.caseattachement
            };

            new AttachmentService().Using(svc => svc.CreateAttachment(attachment));

            attachment = new Attachment()
            {
                EmployeeId = testCase.Employee.Id,
                CaseId = testCase.Id,
                ContentLength = AbsenceSoft.Tests.Resources.AttachementResource.caseExportAttachement_4.Length,
                ContentType = "application/vnd.ms-excel",
                FileName = "caseExportAttachement_4.xlsx",
                CustomerId = testCase.CustomerId,
                EmployerId = testCase.EmployerId,
                File = AbsenceSoft.Tests.Resources.AttachementResource.caseExportAttachement_4
            };

            new AttachmentService().Using(svc => svc.CreateAttachment(attachment));

            //Test Bulkdownload

            CaseExportRequest caseExportRequest = new CaseExportRequest()
            {
                CustomerId = _allId,
                CaseId = testCase.Id,
                EmployerId = testCase.EmployerId,
                EmployeeNumber = testCase.Employee.EmployeeNumber,
                Employee = testCase.Employee,
                Status = AbsenceSoft.Data.Enums.ProcessingStatus.Pending,
                CreatedById = User.DefaultUserId,
                ModifiedById = User.DefaultUserId,
                DownloadType = AbsenceSoft.Data.Enums.CaseExportFormat.Pdf,
                Errors = new List<string>(),
                ContentType = "application/pdf"

            }
            .SetCreatedDate(DateTime.UtcNow)
            .SetModifiedDate(DateTime.UtcNow)
            .Save();

            using (CaseExportService svc = new CaseExportService())
            {
                IsSuccess = svc.ProcessCaseExport(new QueueItem()
                {
                    QueueName = MessageQueues.AtCommonQueue,
                    Body = caseExportRequest.Id
                });
            }

            Assert.IsTrue(IsSuccess);

            caseExportRequest = new CaseExportRequest()
            {
                CustomerId = _allId,
                CaseId = testCase.Id,
                EmployerId = testCase.EmployerId,
                EmployeeNumber = testCase.Employee.EmployeeNumber,
                Employee = testCase.Employee,
                Status = AbsenceSoft.Data.Enums.ProcessingStatus.Pending,
                CreatedById = User.DefaultUserId,
                ModifiedById = User.DefaultUserId,
                DownloadType = AbsenceSoft.Data.Enums.CaseExportFormat.Zip,
                Errors = new List<string>(),
                ContentType = "application/zip"

            }
         .SetCreatedDate(DateTime.UtcNow)
         .SetModifiedDate(DateTime.UtcNow)
         .Save();


            using (CaseExportService svc = new CaseExportService())
            {
                IsSuccess = svc.ProcessCaseExport(new QueueItem()
                {
                    QueueName = MessageQueues.AtCommonQueue,
                    Body = caseExportRequest.Id
                });
            }
            Assert.IsTrue(IsSuccess);
        }

        [TestCleanup]
        public void TestCleanup()
        {
          
        }
    }
}
