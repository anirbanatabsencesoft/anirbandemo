﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Processing;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using static AbsenceSoft.Logic.Processing.CaseMigrationService;

namespace AbsenceSoft.Tests.Processing
{
    public partial class CaseMigrationTests
    {
        const string IdForEverything = "123456789012345678901234";
        const string StdPolicyId = "123456789012345678901234";
        const string FmlaPolicyId = "123456789012345678901235";

        [TestMethod, TestCategory("Processing"), TestCategory("Case Migration"), Ignore]
        public void TestCaseConversionWithOverlappingConsecutiveLeavesWithReducedTimeUsed()
        {
            var caseNumber1 = "2014-0082199";
            var caseNumber2 = "2014-0085099";

            var customer = GetCustomer();
            var employer = GetEmployer(customer);
            var std = GetStdPolicy(customer, employer);
            var fmla = GetFmlaPolicy(customer, employer);

            string[] fileRows = new[]
            {
                "C	119106	EINSTEIN	ALBERT	2014-0082199	1	EHC	0						2014-06-16	2014-06-19		2014-10-07	2014-10-23	1				2014-06-19						",
                "C	119106	EINSTEIN	ALBERT	2014-0085099	1	EHC	0						2014-06-15	2014-06-18		2014-10-07	2014-10-27	1				2014-06-19						",
                "W	119106	EINSTEIN	ALBERT	2014-0082199	N		The Think Tank - 111 Super Secret Pl.	0	0	110	0	PROVIDER																Medical Leave				0								0	SUPER SMART DUDE					",
                "W	119106	EINSTEIN	ALBERT	2014-0085099	N		The Think Tank - 111 Super Secret Pl.	0	0	110	0	PROVIDER																Medical Leave				0								0	SUPER SMART DUDE					",
                "E	119106	EINSTEIN	ALBERT	R	SUPER SMART DUDE	The Think Tank	MI	US	(222)255-5555	(222)255-1234					8080 TOPHAT CT		Blanketville	CA	90528	USA			SAM	UNCLE	(222)225-7878			Curl	Jerry				1980-12-20	F	N				A		16.49	2	2011-09-26		2014-08-17			0	480	480	480	480	480	0		SUPER SMART DUDE				2012-04-01		111-10-0001			2014-0082199",
                "E	119106	EINSTEIN	ALBERT	R	SUPER SMART DUDE	The Think Tank	MI	US	(222)255-5555	(222)255-1234					8080 TOPHAT CT		Blanketville	CA	90528	USA			SAM	UNCLE	(222)225-7878			Curl	Jerry				1980-12-20	F	N				A		23.87	2	2011-09-26		2014-08-17			0	480	480	480	480	480	0		SUPER SMART DUDE				2012-04-01		111-10-0001			2014-0085099",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-06-18	2014-10-07	1	0	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-06-19	2014-06-19	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-06-23	2014-06-23	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-06-26	2014-06-26	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-06-30	2014-06-30	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-07-03	2014-07-03	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-07-07	2014-07-07	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-07-10	2014-07-10	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-07-14	2014-07-14	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-07-17	2014-07-17	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-07-21	2014-07-21	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-07-24	2014-07-24	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-07-28	2014-07-28	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-07-31	2014-07-31	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-08-04	2014-08-04	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-08-07	2014-08-07	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-08-11	2014-08-11	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-08-14	2014-08-14	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-08-18	2014-08-18	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-08-21	2014-08-21	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-08-25	2014-08-25	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-08-28	2014-08-28	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-09-01	2014-09-01	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-09-04	2014-09-04	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-09-08	2014-09-08	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-09-11	2014-09-11	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-09-15	2014-09-15	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-09-18	2014-09-18	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-09-22	2014-09-22	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-09-25	2014-09-25	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-09-29	2014-09-29	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0085099	2014-10-02	2014-10-02	1	240	0215    ",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-06-19	2014-10-07	1	0	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-06-23	2014-06-23	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-06-26	2014-06-26	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-06-30	2014-06-30	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-07-03	2014-07-03	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-07-07	2014-07-07	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-07-10	2014-07-10	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-07-14	2014-07-14	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-07-17	2014-07-17	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-07-21	2014-07-21	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-07-24	2014-07-24	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-07-28	2014-07-28	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-07-31	2014-07-31	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-08-04	2014-08-04	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-08-07	2014-08-07	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-08-11	2014-08-11	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-08-14	2014-08-14	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-08-18	2014-08-18	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-08-21	2014-08-21	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-08-25	2014-08-25	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-08-28	2014-08-28	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-09-01	2014-09-01	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-09-04	2014-09-04	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-09-08	2014-09-08	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-09-11	2014-09-11	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-09-15	2014-09-15	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-09-18	2014-09-18	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-09-22	2014-09-22	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-09-25	2014-09-25	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-09-29	2014-09-29	1	240	0215	",
                "A	119106	EINSTEIN	ALBERT	2014-0082199	2014-10-02	2014-10-02	1	240	0215    ",
            };

            StringBuilder logOutput = new StringBuilder();
            using (MemoryStream stream = new MemoryStream())
            using (StringWriter writer = new StringWriter(logOutput))
            using (CaseMigrationService svc = new CaseMigrationService()
            {
                CurrentCustomer = customer,
                CurrentEmployer = employer,
                CurrentUser = User.System,
                CustomerId = customer.Id,
                EmployerId = employer.Id
            })
            {
                svc.SetReportOutput(writer);
                
                var customFieldToUpdate = CustomField.DistinctFind(null, customer.Id, employer.Id, true).ToList();
                List<Employee> lstEmployee = new List<Employee>();
                List<ConversionFileRow> records = new List<ConversionFileRow>(fileRows.Length);
                for (var i = 0; i < fileRows.Length; i++)
                {
                    records.Add(new ConversionFileRow()
                    {
                        rowNumber = i,
                        recordText = fileRows[i].Split('\t'),
                        delimiter = '\t',
                        lineText = fileRows[i]
                    });
                }
                records
                    .GroupBy(r => r.recordText[1])
                    .ForEach(g => g.OrderBy(o => o.recordText[0] == "E" ? 1 : o.recordText[0] == "C" ? 2 : 3)
                    .ForEach(r => svc.ProcessRecord(r.rowNumber, r.recordText, r.delimiter, r.lineText, customFieldToUpdate, lstEmployee)));
            }
            var log = logOutput.ToString();
            log.Should().NotContain("ERROR").And.NotContain("Error", log);

            Case case1 = Case.AsQueryable().Where(c => c.CustomerId == customer.Id && c.EmployerId == employer.Id && c.CaseNumber == caseNumber1).FirstOrDefault();
            case1.Should().NotBeNull();
            Case case2 = Case.AsQueryable().Where(c => c.CustomerId == customer.Id && c.EmployerId == employer.Id && c.CaseNumber == caseNumber2).FirstOrDefault();
            case2.Should().NotBeNull();

            using (var svc = new CaseService(customer.Id, employer.Id, User.System))
            {
                var summary1 = svc.GetEmployeePolicySummary(case1.Employee, new DateTime(2014, 10, 8).ToMidnight(), case1, true, "0215").FirstOrDefault(s => s.PolicyCode == "0215");
                summary1.TimeUsed.Should().Be(124D);
                summary1.TimeRemaining.Should().Be(916D);
                var summary2 = svc.GetEmployeePolicySummary(case1.Employee, new DateTime(2014, 10, 8).ToMidnight(), case2, true, "0215").FirstOrDefault(s => s.PolicyCode == "0215");
                summary2.TimeUsed.Should().Be(124D);
                summary2.TimeRemaining.Should().Be(916D);
            }

            //var usageStats 
        }

        #region Staging Data

        private Customer GetCustomer()
        {
            #region Customer
            //
            // Create Customer
            return Customer.GetById(IdForEverything) ?? new Customer()
            {
                Id = IdForEverything,
                Name = "TestCaseConversionWithOverlappingConsecutiveLeavesWithReducedTimeUsed",
                Url = "testcaseconversionwithoverlappingconsecutiveleaveswithreducedtimeused",
                Features = new List<AppFeature>()
                {
                    new AppFeature(Feature.LOA) { Enabled = true },
                    new AppFeature(Feature.ADA) { Enabled = true },
                    new AppFeature(Feature.ShortTermDisability) { Enabled = true },
                    new AppFeature(Feature.PolicyCrosswalk) { Enabled = true },
                    new AppFeature(Feature.WorkRelatedReporting) { Enabled = true },
                    new AppFeature(Feature.InquiryCases) { Enabled = true },
                    new AppFeature(Feature.WorkRestriction) { Enabled = true }
                },
                SuppressReasons = new List<string>()
                {
                    "ORGAN",
                    "BONE",
                    "BEREAVEMENT",
                    "CRIME",
                    "JURY",
                    "PARENTAL",
                    "UNION",
                    "LFTHRMEDCOND",
                    "CIVICSERVICE",
                    "BLOOD",
                    "DOM",
                    "EDUCATIONAL",
                    "PREGMAT"
                },
                SuppressAccommodationTypes = new List<string>() { "SC", "EA" },
                Contact = new Contact()
                {
                    FirstName = "Billy",
                    LastName = "Bob",
                    Email = "billy.bob@sommeemail.com",
                    WorkPhone = "987-654-3210",
                    Address = new Address()
                    {
                        Address1 = "123 Easy Street",
                        City = "Chicago",
                        State = "IL",
                        PostalCode = "56487-1234",
                        Country = "US"
                    }
                },
                BillingContact = new Contact()
                {
                    FirstName = "Billy",
                    LastName = "Bob",
                    Email = "billy.bob@sommeemail.com",
                    WorkPhone = "987-654-3210",
                    Address = new Address()
                    {
                        Address1 = "123 Easy Street",
                        City = "Chicago",
                        State = "IL",
                        PostalCode = "56487-1234",
                        Country = "US"
                    }
                },
                EnableIpFilter = false,
                EnableAllEmployersByDefault = false,
                HideContactPreferences = false,
                DefaultAttachmentToConfidential = false,
                DefaultCommunicationToConfidential = false,
                DefaultNoteToConfidential = false,
                PasswordPolicy = new PasswordPolicy()
                {
                    ExpirationDays = 0,
                    MinimumLength = 0,
                    ReuseLimitCount = 0,
                    ReuseLimitDays = 0
                }
            }.Save();
            #endregion
        }

        private Employer GetEmployer(Customer customer)
        {
            #region Employer
            //
            // Create Employer
            return Employer.GetById(IdForEverything) ?? new Employer()
            {
                Id = IdForEverything,
                CustomerId = IdForEverything,
                Customer = customer,
                Enable50In75MileRule = false,
                AllowIntermittentForBirthAdoptionOrFosterCare = true,
                EnableSpouseAtSameEmployerRule = false,
                FMLPeriodType = PeriodType.RollingBack,
                IgnoreScheduleForPriorHoursWorked = false,
                //IgnoreAverageMinutesWorkedPerWeek = false,
                IsPubliclyTradedCompany = false,
                FTWeeklyWorkHours = 40.0D,
                MaxEmployees = 5000,
                Name = customer.Name,
                Url = string.Concat(customer.Url, "-ess"),
                Holidays = new List<Holiday>()
                {
                    new Holiday()
                    {
                        Name = "New Year’s Day",
                        DayOfMonth = 1,
                        Month = Month.January,
                        OrClosestWeekday = true,
                        StartDate = Date.UnixEpoch
                    },
                    new Holiday()
                    {
                        Name = "Martin Luther King’s Birthday",
                        Month = Month.January,
                        WeekOfMonth = 3,
                        DayOfWeek = DayOfWeek.Monday,
                        StartDate = Date.UnixEpoch
                    },
                    new Holiday()
                    {
                        Name = "George Washington's Birthday",
                        Month = Month.February,
                        WeekOfMonth = 3,
                        DayOfWeek = DayOfWeek.Monday,
                        OrClosestWeekday = true,
                        StartDate = Date.UnixEpoch
                    },
                    new Holiday()
                    {
                        Name = "Memorial Day",
                        Month = Month.May,
                        WeekOfMonth = -1,
                        DayOfMonth = 1,
                        DayOfWeek = DayOfWeek.Monday,
                        StartDate = Date.UnixEpoch
                    },
                    new Holiday()
                    {
                        Name = "Independence Day",
                        Month = Month.July,
                        DayOfMonth = 4,
                        OrClosestWeekday = true,
                        StartDate = Date.UnixEpoch
                    },
                    new Holiday()
                    {
                        Name = "Labor Day",
                        Month = Month.September,
                        WeekOfMonth = 1,
                        DayOfWeek = DayOfWeek.Monday,
                        StartDate = Date.UnixEpoch
                    },
                    new Holiday()
                    {
                        Name = "Columbus Day",
                        Month = Month.October,
                        WeekOfMonth = 2,
                        DayOfWeek = DayOfWeek.Monday,
                        StartDate = Date.UnixEpoch
                    },
                    new Holiday()
                    {
                        Name = "Veterans Day",
                        Month = Month.November,
                        DayOfMonth = 11,
                        OrClosestWeekday = true,
                        StartDate = Date.UnixEpoch
                    },
                    new Holiday()
                    {
                        Name = "Thanksgiving Day",
                        Month = Month.November,
                        WeekOfMonth = 4,
                        DayOfWeek = DayOfWeek.Thursday,
                        StartDate = Date.UnixEpoch
                    },
                    new Holiday()
                    {
                        Name = "Christmas Day",
                        Month = Month.December,
                        DayOfMonth = 25,
                        OrClosestWeekday = true,
                        StartDate = Date.UnixEpoch
                    }
                },
                Contact = customer.Contact,
                ReferenceCode = "CASEMIGRATIONTEST-B",
                SuppressPolicies = new List<string>()
                {
                    "AR-ADOPT",
                    "AR-DONOR",
                    "BRVMT",
                    "ADMIN",
                    "CA-CAP",
                    "CA-DOM",
                    "CA-MIL",
                    "CA-CFRA",
                    "CA-FSPA",
                    "CA-DONOR",
                    "CA-PFL",
                    "CA-CPDL",
                    "CA-TMLA",
                    "CA-TMLAS",
                    "CA-VFLED",
                    "CA-VFTL",
                    "CO-FML",
                    "CO-SCHOOL",
                    "CO-DOM",
                    "CT-FML",
                    "CT-PDL",
                    "CT-DOM",
                    "DC-FML",
                    "DC-PARENTAL",
                    "EDU",
                    "FL-DOM",
                    "HI-FML",
                    "HI-DONOR",
                    "HI-PDL",
                    "HI-DOM",
                    "IL-DONOR",
                    "IL-CBLL",
                    "IL-MIL",
                    "IL-PARENT",
                    "IL-VESSA",
                    "IN-MIL",
                    "IA-PDL",
                    "JURY",
                    "KS-PDL",
                    "KS-DOM",
                    "KY-ADOPT",
                    "LA-DONOR",
                    "LA-PDL",
                    "LA-PARENT",
                    "ME-DOM",
                    "ME-FML",
                    "ME-MIL",
                    "MD-MIL",
                    "MD-PARENT",
                    "MD-PDL",
                    "MA-DOM",
                    "MA-MATERN",
                    "MA-NECESSITIES",
                    "MN-DONOR",
                    "MN-MIL",
                    "MN-FML",
                    "MN-DOM",
                    "MT-PDL",
                    "NE-MIL",
                    "NH-PDL",
                    "WI-FML",
                    "NJ-FML",
                    "NJ-STD",
                    "NJ-DOM",
                    "NM-DOM",
                    "WI-DONATE",
                    "NY-DONOR",
                    "NY-MIL",
                    "NY-STD",
                    "NC-PARENT",
                    "OH-MIL",
                    "OR-CRIME",
                    "RI-FML",
                    "RI-CAREGIVER",
                    "TN-FML",
                    "VT-FML",
                    "VT-FMLEXT",
                    "WA-PDL",
                    "RI-PARENT",
                    "NV-PARENT",
                    "PFLBL",
                    "OR-FML",
                    "OR-FMLSC",
                    "PRWMPA",
                    "OR-DOM",
                    "RI-MIL",
                    "OR-MIL",
                    "UN",
                    "WA-DOM",
                    "WA-MIL",
                    "PD-MIL",
                    "WA-FML",
                    "WA-FAM",
                    "SC-DONOR",
                    "COMP-MED",
                    "ADA",
                    "WC",
                    "WCWRCMP",
                    "WCWRCMPPHY",
                    "STD",
                    "LTD",
                    "PER",
                    "MED",
                    "TBD1",
                    "CA-STD",
                    "0307",
                    "0322",
                    "USERRA",
                    "FMLA"
                }
            }.Save();
            #endregion
        }

        private Policy GetFmlaPolicy(Customer customer, Employer employer)
        {
            #region FMLA Policy
            return Policy.GetById(FmlaPolicyId) ?? new Policy()
            {
                Id = FmlaPolicyId,
                Name = "Family Medical Leave Act",
                Code = "0076",
                CustomerId = IdForEverything,
                Customer = customer,
                EmployerId = IdForEverything,
                Employer = employer,
                WorkCountry = "US",
                ResidenceCountry = "US",
                PolicyType = PolicyType.FMLA,
                EffectiveDate = Date.UnixEpoch,
                AbsenceReasons = new List<PolicyAbsenceReason>(2)
                {
                    new PolicyAbsenceReason()
                    {
                        ReasonCode = "EHC",
                        Entitlement = 480,
                        EntitlementType = EntitlementType.WorkingHours,
                        Period = 12,
                        PeriodType = PeriodType.RollingBack,
                        CombinedForSpouses = false,
                        EffectiveDate = Date.UnixEpoch,
                        CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                        ShowType = PolicyShowType.OnlyTimeUsed
                    }
                }
            }.Save();
            #endregion
        }

        private Policy GetStdPolicy(Customer customer, Employer employer)
        {
            #region STD Policy
            return Policy.GetById(StdPolicyId) ?? new Policy()
            {
                Id = StdPolicyId,
                Name = "Short Term Disability",
                Code = "0215",
                CustomerId = IdForEverything,
                Customer = customer,
                EmployerId = IdForEverything,
                Employer = employer,
                WorkCountry = "US",
                ResidenceCountry = "US",
                PolicyType = PolicyType.STD,
                EffectiveDate = Date.UnixEpoch,
                RuleGroups = new List<PolicyRuleGroup>(1)
                {
                    new PolicyRuleGroup()
                    {
                        Name = "Do not auto add",
                        RuleGroupType = PolicyRuleGroupType.Selection,
                        SuccessType = RuleGroupSuccessType.And,
                        Description = "This will ensure that this policy is not automatically added to any case and is left for manual selection.",
                        Rules = new List<Rule>(1)
                        {
                            new Rule()
                            {
                                Name = "Not equal",
                                Description = "Never passes",
                                LeftExpression = "1",
                                Operator = "==",
                                RightExpression = "0"
                            }
                        }
                    }
                },
                AbsenceReasons = new List<PolicyAbsenceReason>(2)
                {
                    new PolicyAbsenceReason()
                    {
                        ReasonCode = "EHC",
                        Entitlement = 1040,
                        EntitlementType = EntitlementType.WorkingHours,
                        Period = 1,
                        PeriodType = PeriodType.PerCase,
                        CombinedForSpouses = false,
                        EffectiveDate = Date.UnixEpoch,
                        CaseTypes = CaseType.Consecutive | CaseType.Reduced,
                        ShowType = PolicyShowType.OnlyTimeUsed,
                        Paid = true,
                        RuleGroups = new List<PolicyRuleGroup>(1)
                        {
                            new PolicyRuleGroup()
                            {
                                Name = "Length of Service",
                                Description = "Length of Service",
                                RuleGroupType = PolicyRuleGroupType.Eligibility,
                                SuccessType = RuleGroupSuccessType.And,
                                Rules = new List<Rule>(2)
                                {
                                    new Rule()
                                    {
                                        Name = "FTE Status",
                                        Description = "FTE Status",
                                        LeftExpression = "EmployeeCustomField('>=', 0.5, 'Total FTE')",
                                        Operator = "==",
                                        RightExpression = "true"
                                    },
                                    new Rule()
                                    {
                                        Name = "Length of Service",
                                        Description = "Length of Service >= 6 months",
                                        LeftExpression = "HasMinLengthOfService(6, Unit.Months)",
                                        Operator = "==",
                                        RightExpression = "true"
                                    }
                                }
                            }
                        }
                    }
                }
            }.Save();
            #endregion
        }

        #endregion
    }
}
