﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Processing;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Processing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Tests.Processing
{
    [TestClass]
    public partial class CaseMigrationTests
    {
        private static TestContext _context;
        private const string _allId = "000000000000000000000002";

        [ClassInitialize]
        public static void EligibilityUploadTestsTestInit(TestContext context)
        {
            _context = context;
        }

        private static void CreateFileBytes(string path, byte[] value)
        {
            if (File.Exists(path))
                File.Delete(path);

            File.WriteAllBytes(path, value);

        }

        [TestInitialize]
        public void InitTests()
        {
            string outputDir = Path.Combine(_context.TestResultsDirectory, "attachements");
            if (Directory.Exists(outputDir))
            {
                Directory.Delete(outputDir, true);
            }
            Directory.CreateDirectory(outputDir);

            var byteCaseattachement = AbsenceSoft.Tests.Resources.AttachementResource.caseattachement;
            string caseattachementFilePath = Path.Combine(outputDir, "caseattachement.txt");
            CreateFileBytes(caseattachementFilePath, byteCaseattachement);

            var byteCaseAttachmentResource_1 = AbsenceSoft.Tests.Resources.AttachementResource.caseAttachmentResource_1;
            string caseAttachmentResource_1FilePath = Path.Combine(outputDir, "CaseAttachmentResource_1.docx");
            CreateFileBytes(caseAttachmentResource_1FilePath, byteCaseAttachmentResource_1);


            var byteCaseAttachmentResource_2 = AbsenceSoft.Tests.Resources.AttachementResource.caseAttachmentResource_2;
            string caseAttachmentResource_2FilePath = Path.Combine(outputDir, "CaseAttachmentResource_2.docx");
            CreateFileBytes(caseAttachmentResource_2FilePath, byteCaseAttachmentResource_2);


            var byteCaseAttachmentResource_3 = AbsenceSoft.Tests.Resources.AttachementResource.caseAttachmentResource_3;
            string caseAttachmentResource_3FilePath = Path.Combine(outputDir, "CaseAttachmentResource_3.docx");
            CreateFileBytes(caseAttachmentResource_3FilePath, byteCaseAttachmentResource_3);
        }


        [TestMethod, TestCategory("Processing"), TestCategory("Case Migration")]
        public void TestCaseConversionEligibility()
        {

            //Create Employee 
            string rowText = "E,900000008,FirstName_8,LastName_8,,Service Rep,, NY,,2121111111,2122222222,2122222222,,seth @absencesoft.com,,123 Main St,, Manhattan, NY,10001,US,,,Supervisor,Jane,12125552525,seth @absencesoft.com,,Doe,John,12125552525,seth @absencesoft.com,,1/1/1980,M,N,Y,N,0,A,,80000,1,2/2/2009,3/3/2009,2/2/2009,2400,2016,0,480,480,480,480,480,0,N,,2,,,,,999-65-5595,ABC,4,TEST-0007";


            CaseRow row = new CaseRow(rowText);
            Assert.IsFalse(row.IsError);
            List<Employee> lstEmployee = new List<Employee>();
            List<EmployeeClass> employeeClassTypes = new List<EmployeeClass>();
            CaseRow.ApplicationResult result = row.ApplyEmployee(_allId, _allId, User.DefaultUserId, lstEmployee, employeeClassTypes);
            if (result == CaseRow.ApplicationResult.Fail)
            {
                if (_context == null)
                    Console.WriteLine(row.Error);
                else
                    _context.WriteLine(row.Error);
            }
            Assert.AreEqual(CaseRow.ApplicationResult.Success, result);
            //Create Case 
            rowText = "C,900000008,FirstName_1,LastName_2,TEST-0007,1,EHC,,,,,,,2016-09-01,2016-01-01,2016-01-31,,,0,,,,,,,,Y,,Y";

            row = new CaseRow(rowText);
            Assert.IsFalse(row.IsError);

            result = row.Apply(_allId, _allId, User.DefaultUserId, null, null, lstEmployee);
            if (result == CaseRow.ApplicationResult.Fail)
            {
                if (_context == null)
                    Console.WriteLine(row.Error);
                else
                    _context.WriteLine(row.Error);
            }
            Assert.AreEqual(CaseRow.ApplicationResult.Success, result);

            rowText = "W,900000008,FirstName_1,LastName_2,TEST-0007,1,1,a,2,3,2,1,PROVIDER,bob,mills,er,123,,asdf,CA,US,90210,1,1,08:34 AM,09:15 AM,stuff,fell,ouch,sharp,,1,6,a,b,c,d,1,1,3,6,,0,a,7,,ouchy";

            row = new CaseRow(rowText);
            Assert.IsFalse(row.IsError);

            result = row.Apply(_allId, _allId, User.DefaultUserId, null, null, lstEmployee);
            if (result == CaseRow.ApplicationResult.Fail)
            {
                if (_context == null)
                    Console.WriteLine(row.Error);
                else
                    _context.WriteLine(row.Error);
            }
            Assert.AreEqual(CaseRow.ApplicationResult.Success, result);
        }

        [TestMethod, TestCategory("Processing"), TestCategory("Case Migration")]
        public void TestCaseCustomFields()

        {
            //Create Case 
            string rowText = "C,000000002,FirstName_1,LastName_2,TEST-0006,1,EHC,,,,,,,2016-09-01,2016-01-01,2016-01-31,,,0,,,,,,,,Y,,Y";

            CaseRow row = new CaseRow(rowText);
            Assert.IsFalse(row.IsError);

            CaseRow.ApplicationResult result = row.Apply(_allId, _allId, User.DefaultUserId);
            if (result == CaseRow.ApplicationResult.Fail)
            {
                if (_context == null)
                    Console.WriteLine(row.Error);
                else
                    _context.WriteLine(row.Error);
            }
            Assert.AreEqual(CaseRow.ApplicationResult.Success, result);

            //Case Custom Fields
            var customFieldToUpdate = CustomField.DistinctFind(null, _allId, _allId, true).ToList();

            rowText = @"F,000000002,TESTTEXT,WAIVER,,2,TEST-0006";

            row = new CaseRow(rowText);
            Assert.IsFalse(row.IsError);


            result = row.Apply(_allId, _allId, User.DefaultUserId, null, customFieldToUpdate);
            if (result == CaseRow.ApplicationResult.Fail)
            {
                if (_context == null)
                    Console.WriteLine(row.Error);
                else
                    _context.WriteLine(row.Error);
            }
            Assert.AreEqual(CaseRow.ApplicationResult.Success, result);



            //Update Same Case Custom Fields
            rowText = @"F,000000002,TESTTEXT,TCS,,2,TEST-0006";

            row = new CaseRow(rowText);
            Assert.IsFalse(row.IsError);

            result = row.Apply(_allId, _allId, User.DefaultUserId, null, customFieldToUpdate);
            if (result == CaseRow.ApplicationResult.Fail)
            {
                if (_context == null)
                    Console.WriteLine(row.Error);
                else
                    _context.WriteLine(row.Error);
            }
            Assert.AreEqual(CaseRow.ApplicationResult.Success, result);


            Case myCase = Case.AsQueryable().FirstOrDefault(c => c.CaseNumber == "TEST-0006");
            Assert.IsNotNull(myCase);
            myCase.Delete();

        }


        [TestMethod, TestCategory("Processing"), TestCategory("Case Migration")]
        public void TestCaseAttachmentUsingFile()
        {

            bool IsError = false;
            string attachmentfilePath = Path.Combine(_context.TestResultsDirectory, "attachements");

            string caseAttachmentfileNameWithPath = attachmentfilePath + @"\caseattachement.txt";

            using (var svc = new CaseMigrationService("000000000000000000000002", string.Empty, attachmentfilePath))
            {
                IsError = svc.ProcessFile(caseAttachmentfileNameWithPath);
            }
            Assert.IsTrue(IsError);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            string outputDir = Path.Combine(_context.TestResultsDirectory, "attachements");

            if (Directory.Exists(outputDir))
            {
                Directory.Delete(outputDir, true);
            }
        }
        [TestMethod, TestCategory("Processing"), TestCategory("Case Migration")]
        public void TestCaseAttachmentUploadProcess()
        {

            bool IsError = false;
            string rowText = @"T,000000002,Test AR,Adopt,754091750,2017-04-06,1,,CaseAttachmentResource_1,CaseAttachmentResource_1.docx,0,Admin";
            // Get the row source from the item.

            long rowNumber = 1;
            char Delimeter = ',';

            string attachmentfilePath = Path.Combine(_context.TestResultsDirectory, "attachements");
            string[] recordText = rowText.Split(Delimeter);

            using (var svc = new CaseMigrationService("000000000000000000000002", string.Empty, attachmentfilePath))
            {
                svc.ProcessRecord(rowNumber, recordText, Delimeter, rowText);
                IsError = true;
            }

            Assert.IsTrue(IsError);
        }



        [TestMethod, TestCategory("Processing"), TestCategory("Case Migration")]
        public void TestCreateCaseAutoPolicies()
        {
            string rowText = @"C,000000002,TestFMLA,1Year,TEST-ABCDEFG01,1,EHC,,,,,,,2014-09-01,2014-01-01,2014-01-31,,,0,,,,,,,,Y,,Y";
            // Get the row source from the item.
            CaseRow row = new CaseRow(rowText);
            Assert.IsFalse(row.IsError);

            CaseRow.ApplicationResult result = row.Apply(_allId, _allId, User.DefaultUserId);
            if (result == CaseRow.ApplicationResult.Fail)
            {
                if (_context == null)
                    Console.WriteLine(row.Error);
                else
                    _context.WriteLine(row.Error);
            }
            Assert.AreEqual(CaseRow.ApplicationResult.Success, result);
        }

        [TestMethod, TestCategory("Processing"), TestCategory("Case Migration")]
        public void TestCreateCaseFMLAApproved()
        {
            string rowText = @"C,000000002,TestFMLA,1Year,TEST-ABCDEFG02,1,EHC,,,,,,,2014-09-01,2014-02-01,2014-02-28,,,0,,,,,,,,N,,N";
            // Get the row source from the item.
            CaseRow row = new CaseRow(rowText);
            Assert.IsFalse(row.IsError);

            CaseRow.ApplicationResult result = row.Apply(_allId, _allId, User.DefaultUserId);
            if (result == CaseRow.ApplicationResult.Fail)
            {
                if (_context == null)
                    Console.WriteLine(row.Error);
                else
                    _context.WriteLine(row.Error);
            }
            Assert.AreEqual(CaseRow.ApplicationResult.Success, result);

            //
            // Adjudicate
            rowText = @"A,000000002,TestFMLA,1Year,TEST-ABCDEFG02,2014-02-01,2014-02-28,1,8400,FMLA,Y";
            row = new CaseRow(rowText);
            result = row.Apply(_allId, _allId, User.DefaultUserId);
            if (result == CaseRow.ApplicationResult.Fail)
            {
                if (_context == null)
                    Console.WriteLine(row.Error);
                else
                    _context.WriteLine(row.Error);
            }
            Assert.AreEqual(CaseRow.ApplicationResult.Success, result);

            // add a note
            rowText = "N,000000002,Wesson,Smith,TEST-ABCDEFG02,2014-09-02,0,This is a converted case note. Yeah for me.,\"Harris, Sam\"";
            row = new CaseRow(rowText);
            result = row.Apply(_allId, _allId, User.DefaultUserId);
            if (result == CaseRow.ApplicationResult.Fail)
            {
                if (_context == null)
                    Console.WriteLine(row.Error);
                else
                    _context.WriteLine(row.Error);
            }
            Assert.AreEqual(CaseRow.ApplicationResult.Success, result);
            Assert.AreEqual(row.NoteDate, new DateTime(2014, 9, 2, 0, 0, 0, DateTimeKind.Utc));
            Assert.AreEqual(row.NoteEnteredByName, "Harris, Sam");

        }

        [TestMethod,Ignore, TestCategory("Processing"), TestCategory("Case Migration")]
        public void TestCreateCaseFMLAApprovedIntermittentWithTimeOffRequests()
        {
            string rowText = @"C,000000002,TestFMLA,1Year,TEST-ABCDEFG03,2,FHC,7,1,3,4,4,1,2014-09-01,2014-03-01,2014-03-31,,,0,,,,,,,,N,,N";
            // Get the row source from the item.
            CaseRow row = new CaseRow(rowText);
            Assert.IsFalse(row.IsError, row.Error);

            //
            // Create Case
            CaseRow.ApplicationResult result = row.Apply(_allId, _allId, User.DefaultUserId);
            if (result == CaseRow.ApplicationResult.Fail)
            {
                if (_context == null)
                    Console.WriteLine(row.Error);
                else
                    _context.WriteLine(row.Error);
            }
            Assert.AreEqual(CaseRow.ApplicationResult.Success, result, row.Error);

            //
            // Adjudicate
            rowText = @"A,000000002,TestFMLA,1Year,TEST-ABCDEFG03,2014-03-01,2014-03-31,1,,FMLA,Y";
            row = new CaseRow(rowText);
            result = row.Apply(_allId, _allId, User.DefaultUserId);
            if (result == CaseRow.ApplicationResult.Fail)
            {
                if (_context == null)
                    Console.WriteLine(row.Error);
                else
                    _context.WriteLine(row.Error);
            }
            Assert.AreEqual(CaseRow.ApplicationResult.Success, result, row.Error);

            //
            // Time Off Request
            rowText = @"I,000000002,TestFMLA,1Year,TEST-ABCDEFG03,2014-03-04,240,0,60,FMLA,Y";
            row = new CaseRow(rowText);
            result = row.Apply(_allId, _allId, User.DefaultUserId);
            if (result == CaseRow.ApplicationResult.Fail)
            {
                if (_context == null)
                    Console.WriteLine(row.Error);
                else
                    _context.WriteLine(row.Error);
            }
            Assert.AreEqual(CaseRow.ApplicationResult.Success, result, row.Error);
        }

        [TestMethod, TestCategory("Processing"), TestCategory("Case Migration")]
        public void TestCreateCaseCFRAFMLAApprovedWithExhaustionAndMinutesUsed()
        {
            string rowText = @"C,000000031,DomViolFemale,TestCA,102,1,EHC,,,,,,,9/24/2014,10/7/2013,7/4/2014,,,0";
            // Get the row source from the item.
            CaseRow row = new CaseRow(rowText);
            Assert.IsFalse(row.IsError);

            //
            // Create Case
            CaseRow.ApplicationResult result = row.Apply(_allId, _allId, User.DefaultUserId);
            Assert.AreEqual(CaseRow.ApplicationResult.Success, result, row.Error);

            //
            // Adjudicate FMLA
            rowText = @"A|000000031|DomViolFemale|TestCA|102|10/7/2013|3/7/2014|1|132960|FMLA";
            row = new CaseRow(rowText);
            Assert.IsFalse(row.IsError);
            result = row.Apply(_allId, _allId, User.DefaultUserId);
            Assert.AreEqual(CaseRow.ApplicationResult.Success, result, row.Error);

            //
            // Adjudicate CA-CFRA
            rowText = @"A|000000031|DomViolFemale|TestCA|102|10/7/2013|3/7/2014|1|132960|CA-CFRA";
            row = new CaseRow(rowText);
            Assert.IsFalse(row.IsError);
            result = row.Apply(_allId, _allId, User.DefaultUserId);
            Assert.AreEqual(CaseRow.ApplicationResult.Success, result, row.Error);

            //
            // Get Case
            var testCase = Case.AsQueryable().Where(c => c.EmployerId == _allId && c.CaseNumber == "102").FirstOrDefault();
            Assert.IsNotNull(testCase, "Case was not found");

            var fmla = testCase.Segments[0].AppliedPolicies.First(s => s.Policy.Code == "FMLA");
            var cfra = testCase.Segments[0].AppliedPolicies.First(s => s.Policy.Code == "CA-CFRA");

            // These will bomb
            Assert.IsTrue(fmla.Usage.Any(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted), "FMLA Exhaustion not met... should have been!");
            Assert.IsTrue(cfra.Usage.Any(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted), "CFRA Exhaustion not met... should have been!");
        }


        [TestMethod, TestCategory("Processing"), TestCategory("Case Migration")]
        public void TestCreateCaseWorkRelated()
        {
            string rowText = @"C,000000002,TestFMLA,1Year,TEST-WCOSHA1,1,EHC,,,,,,,2016-09-01,2016-01-01,2016-01-31,,,0,,,,,,,,Y,,Y";
            // Get the row source from the item.
            CaseRow row = new CaseRow(rowText);
            Assert.IsFalse(row.IsError, row.Error);
            CaseRow.ApplicationResult result = row.Apply(_allId, _allId, User.DefaultUserId);
            Assert.AreNotEqual(CaseRow.ApplicationResult.Fail, result, row.Error);
            Assert.AreEqual(CaseRow.ApplicationResult.Success, result);


            rowText = @"W,000000002,TestFMLA,1Year,TEST-WCOSHA1,1,1,a,2,3,2,1,PROVIDER,bob,mills,er,123,,asdf,CA,US,90210,1,1,08:34 AM,09:15 AM,stuff,fell,ouch,sharp,,1,6,a,b,c,d,1,1,3,6,,0,a,7,,ouchy";
            // Get the row source from the item.
            row = new CaseRow(rowText);
            Assert.IsFalse(row.IsError, row.Error);
            result = row.Apply(_allId, _allId, User.DefaultUserId);
            Assert.AreNotEqual(CaseRow.ApplicationResult.Fail, result, row.Error);
            Assert.AreEqual(CaseRow.ApplicationResult.Success, result);

            Case myCase = Case.AsQueryable().Where(c => c.CustomerId == _allId && c.EmployerId == _allId && c.CaseNumber == "TEST-WCOSHA1").FirstOrDefault();
            Assert.IsNotNull(myCase, "Case was not found");
            Assert.IsNotNull(myCase.WorkRelated, "Case was missing work related non-entity");
            Assert.IsTrue(myCase.Metadata.GetRawValue<bool>("IsWorkRelated"), "Case was not set as Is Work Related = true");
        }
    }
}
