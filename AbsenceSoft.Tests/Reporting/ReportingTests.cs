﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Reporting;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;

namespace AbsenceSoft.Tests.Reporting
{
    [TestClass]
    public class ReportingTests
    {
        private ReportService svc;

        [TestInitialize]
        public void InitTests()
        {
            svc = new ReportService()
            {
                Customer = new AbsenceSoft.Data.Customers.Customer()
                    {
                        Features = new System.Collections.Generic.List<AbsenceSoft.Data.Customers.AppFeature>()
                        {
                            new AbsenceSoft.Data.Customers.AppFeature()
                            {
                                EffectiveDate = DateTime.MinValue,
                                Enabled = true,
                                Feature = Feature.LOA
                            },
                            new AbsenceSoft.Data.Customers.AppFeature()
                            {
                                EffectiveDate = DateTime.MinValue,
                                Enabled = true,
                                Feature = Feature.ADA
                            },
                            new AbsenceSoft.Data.Customers.AppFeature()
                            {
                                EffectiveDate = DateTime.MinValue,
                                Enabled = true,
                                Feature = Feature.EmployeeSelfService
                            },
                            new AbsenceSoft.Data.Customers.AppFeature()
                            {
                                EffectiveDate = DateTime.MinValue,
                                Enabled = true,
                                Feature = Feature.ShortTermDisability
                            }
                        }
                    }
            };
        }

        [TestMethod, TestCategory("Reporting")]
        public void GetReportsTest()
        {
            var reports = svc.GetReports();
            Assert.IsTrue(reports.Any());
        }

        [TestMethod, TestCategory("Reporting")]
        public void GetReportsByCategoryTest()
        {
            var reports = svc.GetReports("Absence Status Report");
            Assert.IsTrue(reports.Any());
        }

        [TestMethod, TestCategory("Reporting")]
        public void GetReportTest()
        {
            var report = svc.GetReport("117AF888-E65E-44da-A043-61A5896FC316");
            Assert.IsNotNull(report);
        }

        [TestMethod, TestCategory("Reporting")]
        public void GetAccommodationsOperationsDetailedReportTest()
        {
            var report = svc.GetReport("28b4c98f-c321-4a1f-b928-e19886a06a3c");
            Assert.IsNotNull(report);
        }

        [TestMethod, TestCategory("Reporting")]
        public void GetAccommodationsOperationsDetailedReportParametersTest()
        {
            User user = User.AsQueryable().Where(u => u.CustomerId == "000000000000000000000002").FirstOrDefault() ?? User.System;
            var pars = svc.GetCriteria("28b4c98f-c321-4a1f-b928-e19886a06a3c", user);
            Assert.IsNotNull(pars);
        }

        [TestMethod, TestCategory("Reporting")]
        public void GetReportParametersTest()
        {
            User user = User.AsQueryable().Where(u => u.CustomerId == "000000000000000000000002").FirstOrDefault() ?? User.System;
            var pars = svc.GetCriteria("117AF888-E65E-44da-A043-61A5896FC316", user);
            Assert.IsNotNull(pars);
        }

        [TestMethod, TestCategory("Reporting")]
        public void ReportDataSerializationTest()
        {
            dynamic data = new ResultData();
            data.CaseNumber = "1234567890";
            data.EmployeeNumber = "000000000017";
            data.HireDate = DateTime.UtcNow.AddYears(-3);
            data.Name = "Jermaine Bigglebottom";
            data.Reason = "Family Health Condition";
            data.Type = "Consecutive";
            data.StartDate = DateTime.UtcNow.Date.AddDays(-30);
            data.EndDate = DateTime.UtcNow.Date.AddDays(5);
            data.Status = "Eligible";
            data.Determination = "Approved";

            string json = JsonConvert.SerializeObject(data);
            Assert.IsNotNull(json);
            Assert.IsFalse(string.IsNullOrWhiteSpace(json));
            Assert.AreNotEqual("{}", json);
        }

        [TestMethod, TestCategory("Reporting")]
        public void ReportDataGroupSerializationTest()
        {
            dynamic data = new ResultData();
            data.CaseNumber = "1234567890";
            data.EmployeeNumber = "000000000017";
            data.HireDate = DateTime.UtcNow.AddYears(-3);
            data.Name = "Jermaine Bigglebottom";
            data.Reason = "Family Health Condition";
            data.Type = "Consecutive";
            data.StartDate = DateTime.UtcNow.Date.AddDays(-30);
            data.EndDate = DateTime.UtcNow.Date.AddDays(5);
            data.Status = "Eligible";
            data.Determination = "Approved";

            ResultDataGroup group = new ResultDataGroup();
            group.Key = 17;
            group.Label = "Office Location 1";
            group.Items.Add(data);

            string json = JsonConvert.SerializeObject(group);
            Assert.IsNotNull(group);
            Assert.IsFalse(string.IsNullOrWhiteSpace(json));
            Assert.AreNotEqual("{}", json);
        }

        [TestMethod, TestCategory("Reporting")]
        public void ReportCriteriaValueOfCaseTypeTest()
        {
            string filterName = "CaseType";

            ReportCriteria criteria = new ReportCriteria();
            criteria.CustomerId = "000000000000000000000002";
            criteria.EndDate = DateTime.UtcNow;
            criteria.StartDate = DateTime.UtcNow.AddDays(-1);
            criteria.Filters.AddFluid(new ReportCriteriaItem()
            {
                ControlType = ControlType.TextBox,
                Name = filterName,
                Prompt = "Case Type",
                Required = true,
                Value = 2
            }).Options.AddRange(Enum.GetValues(typeof(CaseType)).OfType<CaseType>().Select(n => new ReportCriteriaItemOption()
            {
                Value = (int)n,
                Text = n.ToString().SplitCamelCaseString()
            }));

            Assert.AreEqual(CaseType.Intermittent, criteria.ValueOf<CaseType>(filterName));
        }

        [TestMethod, TestCategory("Reporting")]
        public void ReportCriteriaPlainEnglishTest()
        {
            User user = User.AsQueryable().Where(u => u.CustomerId == "000000000000000000000002").FirstOrDefault() ?? User.System;
            ReportService svc = new ReportService();
            var rpt = svc.GetReport("955E6AC2-4C2C-4cb5-A6B7-DF4CCD3BC252"); // Absence Status Report (no group)
            var criteria = rpt.GetCriteria(user);
            foreach (var filter in criteria.Filters)
            {
                switch (filter.Name)
                {
                    case "CaseStatus":
                        filter.Value = (int)CaseStatus.Open;
                        break;
                    case "CaseType":
                        filter.Value = (int)CaseType.Intermittent;
                        break;
                    case "Determination":
                        filter.Value = (int)AdjudicationStatus.Approved;
                        break;
                    case "OFFICELOCATION":
                        filter.Value = "LOC_CODE_001";
                        break;
                    case "AbsenceReason":                    
                    default:
                        filter.Value = filter.Options.Any() ? filter.Options.First().Value : null;
                        break;
                }
            }

            string english = criteria.ToString();

            Assert.IsNotNull(english);
        }

        [TestCleanup]
        public void Cleanup()
        {
            svc = null;
        }
    }
}
