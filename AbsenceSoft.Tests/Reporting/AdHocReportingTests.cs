﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using AbsenceSoft.Common.Postgres;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Reporting;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Reporting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using FluentAssertions.Collections;
using FluentAssertions.Common;
using Npgsql;

namespace AbsenceSoft.Tests.Reporting
{
    [TestClass]
    public class AdHocReportingTests
    {
        private AdHocReportingService _adHocReportingService;
        private Role _canNotSeeSalaryRole;
        private const string DataWarehouse = "DataWarehouse";

        private string DataWarehouseConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings[DataWarehouse].ConnectionString; }
        }


        [TestInitialize]
        public void InitTests()
        {
            _adHocReportingService = new AdHocReportingService(
                User.AsQueryable().FirstOrDefault(u => u.Email == "dev@absencesoft.com"));
            Assert.IsNotNull(_adHocReportingService.CurrentUser);
            Assert.IsNotNull(_adHocReportingService.CurrentCustomer);
            Assert.IsNotNull(_adHocReportingService.CustomerId);
            _canNotSeeSalaryRole = new Role()
            {
                CustomerId = "000000000000000000000002",
                Name = "Can Not See Salary Sucka",
                Type = RoleType.Portal,
                Permissions = Permission.All().Select(p => p.Id).Where(p => p != "ViewSalary").ToList()
            }.Save();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            if (_adHocReportingService != null)
                _adHocReportingService.Dispose();
            _adHocReportingService = null;
            if (_canNotSeeSalaryRole != null && !_canNotSeeSalaryRole.IsNew)
                _canNotSeeSalaryRole.Delete();
            _canNotSeeSalaryRole = null;
        }
        
        [TestMethod, TestCategory("AdHocReporting")]
        public void GetFieldsTest()
        {
            var fields = _adHocReportingService.GetFields("000000000000000000000006");
            fields.Should().NotBeEmpty("The view_case view should return some fields but did not");
            var caseNumberField = fields.FirstOrDefault(f => f.Name == "case_case_number");
            caseNumberField.Should().NotBeNull("The view_case view should contain a field named 'case_case_number'; I promise it should");
        }

        [TestMethod, TestCategory("AdHocReporting")]
        public void GetFieldsSecurityTest()
        {
            var fields = _adHocReportingService.GetFields("000000000000000000000005"); // view_empl
            fields.Should().NotBeEmpty("The view_case view should return some fields but did not");
            var salaryField = fields.FirstOrDefault(f => f.Name == "empl_salary");
            salaryField.Should().NotBeNull("The view_case view should contain a field named 'empl_salary'");

            User myLameUser = new User()
            {
                Email = "i-haz-not-teh-permissions@absencesoft.com",
                FirstName = "Lame",
                LastName = "User",
                ContactInfo = new Contact() { FirstName = "Lame", LastName = "User", Email = "i-haz-not-teh-permissions@absencesoft.com" },
                CustomerId = "000000000000000000000002",
                Roles = new List<string>(1) { _canNotSeeSalaryRole.Id }
            };
            using (AdHocReportingService svc = new AdHocReportingService(myLameUser))
            {
                var lameFields = svc.GetFields("000000000000000000000005");
                lameFields.Count.Should().Be(fields.Count - 1, "Lame User's Fields should include all but 1 of an Admin's");
                salaryField = lameFields.FirstOrDefault(f => f.Name == "empl_salary");
                salaryField.Should().BeNull("The view_case view should NOT contain a field named 'empl_salary' because lame user should not have permissions to see it");
            }
        }

        [Ignore]
        [TestMethod, TestCategory("AdHocReporting")]
        public void GetFieldFilterTest()
        {
            var reportTypeId = "000000000000000000000001";
            var fields = _adHocReportingService.GetFields(reportTypeId);
            foreach (var reportField in fields)
            {
                var reportFilter = _adHocReportingService.GetFieldFilter(reportTypeId, reportField.Name);
                if (reportFilter != null)
                {
                    switch (reportFilter.ControlType)
                    {
                        case ControlType.SelectList:
                        case ControlType.RadioButtonList:
                        case ControlType.CheckBoxList:
                            if (reportFilter.Options.Any())
                            {
                                Assert.IsTrue(true); // Note placing a breakpoint for debugging.
                            }
                            break;
                    }                  
                }
            }
        }

        private static void CreateFileBytes(string path, byte[] value)
        {
            if (File.Exists(path))
                File.Delete(path);

            using (var fileStream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None, 4096, FileOptions.SequentialScan))
            {
                fileStream.Write(value, 0, value.Length);
                fileStream.Flush(true);
                fileStream.Close();
            }
        }

        private static string GetFileStamp()
        {
            var now = DateTime.Now;
            return string.Format(
                "{0}_{1}_{2}-{3}_{4}_{5}.{6}",
                now.Year.ToString("D4"),
                now.Month.ToString("D2"),
                now.Day.ToString("D2"),
                now.Hour.ToString("D2"),
                now.Minute.ToString("D2"),
                now.Second.ToString("D2"),
                now.Millisecond);
        }

        [TestMethod, TestCategory("AdHocReporting")]
        public void AdHocQueryBuilderTest()
        {
            var fields = new List<ReportField>
            {
                new ReportField { Name = "cust_id", Display = "Customer Id", DataType = DbDataType.VarChar, Order = 1 },
                new ReportField { Name = "cust_customer_name", Display = "Customer Name", DataType = DbDataType.VarChar, Order = 2 },
                new ReportField { Name = "cust_customer_url", Display = "Customer Url", DataType = DbDataType.VarChar, Order = 3 },
                new ReportField { Name = "cust_contact_first_name", Display = "Contact First Name", DataType = DbDataType.VarChar, Order = 4 },
                new ReportField { Name = "cust_contact_last_name", Display = "Contact Last Name", DataType = DbDataType.VarChar, Order = 5 },
                new ReportField { Name = "cust_contact_email", Display = "Contact Email", DataType = DbDataType.VarChar, Order = 6 },
                new ReportField { Name = "cust_contact_address1", Display = "Contact Address1", DataType = DbDataType.VarChar, Order = 7 },
                new ReportField { Name = "cust_contact_address2", Display = "Contact Address2", DataType = DbDataType.VarChar, Order = 8 },
                new ReportField { Name = "cust_contact_city", Display = "Contact City", DataType = DbDataType.VarChar, Order = 9 },
                new ReportField { Name = "cust_customer_employer_count", Display = "Customer Employer Count", DataType = DbDataType.BigInt, Order = 9 }
            };

            var reportDefinition = new ReportDefinition
            {
                Customer = _adHocReportingService.CurrentCustomer,
                User = _adHocReportingService.CurrentUser,
                Name = "View Case Customised Report",
                Description = "This is my modified version of a summary report for all cases for me as the client.",
                ReportType = new ReportType
                {
                    AdHocReportType = AdHocReportType.Case,
                    IsCustomer = true,
                    IsEmployer = true,
                    Key = "view_case",
                    Name = "View Case",
                    Description = "This report is a summary of all cases for this client.",
                    Fields = fields
                },
                Fields = fields.Select(f => new SavedReportField()
                {
                    Name = f.Name
                }).ToList(),
                CreatedBy = _adHocReportingService.CurrentUser,
                ModifiedBy = _adHocReportingService.CurrentUser,
                Filters = new List<ReportFilter>
                {
                    new ReportFilter
                    {
                        ControlType = ControlType.TextBox,
                        FieldName = "cust_customer_name",
                        Operator = "~~",
                        Value = "Absence"//"AbsenceSoft Test"
                    },
                    new ReportFilter
                    {
                        ControlType = ControlType.TextBox,
                        FieldName = "cust_customer_name",
                        Operator = "|~",
                        Value = "est"//"AbsenceSoft Test"
                    },
                    new ReportFilter
                    {
                        ControlType = ControlType.TextBox,
                        FieldName = "cust_customer_employer_count",
                        Operator = "=",
                        Value = "1"
                    }
                }
            };

            var queryResult = AdHocQueryBuilder.Build(reportDefinition, fields, _adHocReportingService.CurrentUser);
            Assert.IsNotNull(queryResult);
            Assert.IsNotNull(queryResult.Query);
            Assert.IsFalse(string.IsNullOrWhiteSpace(queryResult.Query));
            Assert.AreEqual(queryResult.Query.Length, queryResult.Query.Trim().Length);
            Assert.IsTrue(queryResult.Query.Trim().Length > 0);

            // Uncomment and test against ETL database as needed.
            // Hopefully data will not have changed.
            //var db = new CommandRunner(DataWarehouse);
            //var cases = db.ExecuteDynamic(query);
            //Assert.IsTrue(cases.Any());

            // New extension method is nice for Dynamic Awesome.
            //var db = new CommandRunner(DataWarehouse);
            //var cases = db.ExecuteDynamicAwesome(query);
            //Assert.IsTrue(cases.Any());

            //var results = _adHocReportingService.RunReport(reportDefinition);
            //Assert.IsTrue(results.Items.Count > 0);

            //var csvBytes = _adHocReportingService.ExportReportAsCsv(reportDefinition, fields);
            //Assert.IsTrue(csvBytes.Length > 0);
            //CreateFileBytes(string.Format(@"C:\{0}_{1}.csv", reportDefinition.Name, GetFileStamp()), csvBytes);
        }

        /// <summary>
        /// Tests the ability to pull back a typed datatype from ETL database.
        /// </summary>
        [Ignore]
        [TestMethod, TestCategory("AdHocReporting")]
        public void UseCommandRunnerTest()
        {
            var db = new CommandRunner(DataWarehouse);
            var addresses = db.Execute<EtlAddress>("select * from address limit 10");
            foreach (var address in addresses)
            {
                Console.WriteLine(address.address1);
            }

            Assert.IsTrue(true);
        }

        /// <summary>
        /// Tests the ability to pull back a dynamic datatype from ETL database.
        /// </summary>
        [Ignore]
        [TestMethod, TestCategory("AdHocReporting")]
        public void UseCommandRunnerDynamicTest()
        {
            var db = new CommandRunner(DataWarehouse);
            var addresses = db.ExecuteDynamic("select * from address limit 10");
            foreach (var address in addresses)
            {
                Console.WriteLine(address.address1);
            }

            Assert.IsTrue(true);
        }

        /// <summary>
        /// Tests entire ETL database against reserved keywords in Postgres.
        /// </summary>
        [Ignore]
        [TestMethod, TestCategory("AdHocReporting")]
        public void GetReservedKeywordConflictsTest()
        {
            var problemTableNames = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
            var problemTableColumnNames = new List<string>();

            using (var conn = new NpgsqlConnection(DataWarehouseConnectionString))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand("select table_name, column_name from information_schema.columns col where table_schema = 'public' order by table_name, column_name", conn))
                {
                    using (var rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            var tableName = Convert.ToString(rdr["table_name"]);
                            var columnName = Convert.ToString(rdr["column_name"]);
                            if (PostgresReservedWords.Set.Contains(tableName))
                                problemTableNames.Add(string.Format("{0}", tableName));

                            if (PostgresReservedWords.Set.Contains(columnName))
                                problemTableColumnNames.Add(string.Format("{0}.{1}", tableName, columnName));
                        }
                    }
                }

                conn.Close();
            }

            Console.WriteLine("Reserved Keyword Table Count ({0}):", problemTableNames.Count);
            foreach (var value in problemTableNames)
                Console.WriteLine(value);

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Reserved Keyword Column Count ({0}):", problemTableColumnNames.Count);
            foreach (var value in problemTableColumnNames)
                Console.WriteLine(value);

            Assert.IsTrue(true);
        }

        /// <summary>
        /// Tests the DbDataTypeConverter. This will be useful for automation of pulling in initial report definitions.
        /// </summary>
        [Ignore]
        [TestMethod, TestCategory("AdHocReporting")]
        public void GetEtlDbDataTypesTest()
        {
            using (var conn = new NpgsqlConnection(DataWarehouseConnectionString))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand("select distinct is_nullable, data_type, character_maximum_length, character_octet_length, numeric_precision, numeric_precision_radix, numeric_scale, datetime_precision, interval_type, interval_precision, udt_name from information_schema.columns col where table_schema = 'public'", conn))
                {
                    using (var rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            var dataType = DbDataTypeConverter.GetDbDataTypeFromPostgres(Convert.ToString(rdr["data_type"]));
                            var udtName = DbDataTypeConverter.GetDbDataTypeFromPostgres(Convert.ToString(rdr["udt_name"]));

                            Assert.AreEqual(dataType, udtName);
                        }
                    }
                }

                conn.Close();
            }
        }
    }
}
