﻿namespace AbsenceSoft.Tests.Reporting
{
    public class EtlAddress
    {
        public string id { get; set; }
        public string name { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postal_code { get; set; }
        public string country { get; set; }
    }
}
