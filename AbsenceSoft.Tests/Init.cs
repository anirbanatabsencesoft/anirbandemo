﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using Aspose.Words;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Configuration;
using System.IO;
using System.Net.Configuration;
using System.Net.Mail;
using System.Reflection;
using System.Web;

namespace AbsenceSoft.Tests
{
    [TestClass]
    public class Init
    {
        [AssemblyInitialize()]
        public static void AssemblyInit(TestContext context)
        {
            // Init our log4net thingy
            log4net.Config.XmlConfigurator.Configure();

            AbsenceSoft.Common.Serializers.DateTimeSerializer.Register();

            User.Repository.Collection.Database.Drop();
            Audit<User>.Repository.Collection.Database.Drop();

            AbsenceSoft.Setup.Accommodation.AccommodationQuestionsData.SetupData();
            AbsenceSoft.Setup.Accommodation.AccommodationTypeData.SetupData();
			AbsenceSoft.Setup.ContactTypes.ContactTypesData.SetupData();
            AbsenceSoft.Setup.Reporting.ReportData.SetupData();
            AbsenceSoft.Setup.CoreData.SetupData(true);
            // TODO: Remove Demo Data from this list, it really doesn't belong but will take some re-write of the app
            AbsenceSoft.Setup.DemoData.SetupData();
            AbsenceSoft.Setup.TestData.SetupData();

            new License().SetLicense("Aspose.Words.lic");

            var smtpConfig = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
            if (smtpConfig.DeliveryMethod == SmtpDeliveryMethod.SpecifiedPickupDirectory && !Path.IsPathRooted(smtpConfig.SpecifiedPickupDirectory.PickupDirectoryLocation))
            {
                BindingFlags instanceFlags = BindingFlags.Instance | BindingFlags.NonPublic;
                PropertyInfo prop;
                object mailConfiguration, smtp, specifiedPickupDirectory;
                string path = Path.Combine(Environment.CurrentDirectory, smtpConfig.SpecifiedPickupDirectory.PickupDirectoryLocation);

                // get static internal property: MailConfiguration
                prop = typeof(SmtpClient).GetProperty("MailConfiguration", BindingFlags.Static | BindingFlags.NonPublic);
                mailConfiguration = prop.GetValue(null, null);

                // get internal property: Smtp
                prop = mailConfiguration.GetType().GetProperty("Smtp", instanceFlags);
                smtp = prop.GetValue(mailConfiguration, null);

                // get internal property: SpecifiedPickupDirectory
                prop = smtp.GetType().GetProperty("SpecifiedPickupDirectory", instanceFlags);
                specifiedPickupDirectory = prop.GetValue(smtp, null);

                // get private field: pickupDirectoryLocation, then set it to the supplied path
                FieldInfo field = specifiedPickupDirectory.GetType().GetField("pickupDirectoryLocation", instanceFlags);
                field.SetValue(specifiedPickupDirectory, path);
            }
        }
    }
}
