﻿using System;
using System.Collections.Specialized;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Customers;

namespace AbsenceSoft.Tests.Data
{
    [TestClass]
    public class ContactTypeTest
    {
        [TestMethod]
        public void TestStateRestrictiveContactTypeList()
        {
			Employee emp1 = new Employee() { WorkState = "CA", CustomerId="000000000000000000000001",EmployerId="000000000000000000000001" };
            var personalCA = new ContactTypeService().Using(s=>s.GetContactTypesForEmployee(emp1, ContactTypeDesignationType.Personal));
            
			Employee emp2 = new Employee() { Info = new EmployeeInfo() { Address = new AbsenceSoft.Data.Address() { State = "HI" } } };
			var personalHI = new ContactTypeService().Using(s=>s.GetContactTypesForEmployee(emp2, ContactTypeDesignationType.Personal));

            Assert.AreEqual(personalCA.Count, 47);
            Assert.AreEqual(personalHI.Count, 54);
        }
    }
}
