﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using AbsenceSoft.Data;

namespace AbsenceSoft.Tests.Data
{
    [TestClass]
    public class ScheduleTest
    {
        [TestMethod]
        public void ScheduleFormat()
        {
            // not much of a test, create a schedule and view it in the debugger
            Schedule s = new Schedule();

            s.Times = TestWorkSchedules.GetFullTimeList();
            string x = s.ToString();

            s.Times = TestWorkSchedules.RotatingSchedule();
            x = s.ToString();

        }




    }
}
