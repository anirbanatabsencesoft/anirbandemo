﻿using System;
using System.Collections.Specialized;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using FluentAssertions.Common;
using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace AbsenceSoft.Tests.Data
{
    [TestClass]
    public class MetadataTests
    {
        [TestMethod]
        public void TestFixStupid()
        {
            var json = @"{
  ""Code"": ""TEST1"",
  ""Name"": ""TESTing"",
  ""Description"": ""Just Testing Fix Stupid"",
  ""EffectiveDate"": ""0001-01-01T00:00:00Z"",
  ""PolicyType"": 6,
  ""RuleGroups"": [],
  ""AbsenceReasons"": [
    {
      ""Elimination"": """",
      ""ReasonCode"": ""EDUCATIONAL"",
      ""EntitlementType"": 1,
      ""Entitlement"": 12,
      ""CombinedForSpouses"": false,
      ""PeriodType"": 0,
      ""Period"": 1,
      ""EffectiveDate"": ""1970-01-01T00:00:00Z"",
      ""CaseTypes"": 15,
      ""Paid"": false,
      ""PaymentTiers"": [],
      ""RuleGroups"": [],
      ""ShowType"": 3,
      ""PolicyEvents"": [],
      ""UseWholeDollarAmountsOnly"": false,
      ""Id"": ""ef699f51-6bc1-4cd9-ac85-aa005a2aa485""
    }
  ],
  ""ConsecutiveTo"": [],
  ""IsCustom"": false,
  ""IsDisabled"": false,
  ""cdt"": ""2015-10-16T21:50:46.6373776Z"",
  ""cby"": ""000000000000000000000000"",
  ""mdt"": ""2015-10-16T21:50:46.6373776Z"",
  ""mby"": ""000000000000000000000000"",
  ""Id"": ""562171369640a619d07b3530""
}";

            Policy policy = JsonConvert.DeserializeObject<Policy>(json);
            policy.AbsenceReasons.Should().HaveCount(1);
            policy.AbsenceReasons[0].Elimination.Should().NotHaveValue();
            policy.AbsenceReasons[0].Metadata.GetRawValue<object>("Elimination").Should().BeNull();
            policy.Save();

            var readME = Policy.GetById(policy.Id); // should haz no ka-booms here.
            readME.Should().NotBeNull();
            readME.Id.Should().Be(policy.Id);
        }
    }
}
