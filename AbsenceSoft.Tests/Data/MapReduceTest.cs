﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Reporting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;

namespace AbsenceSoft.Tests.Data
{
    [TestClass]
    public class MapReduceTest
    {
        [Ignore]
        public void MapReduceToDoItemTest()
        {
            ReportResult result = new ReportResult();
            result.Criteria = new ReportCriteria();
            result.Criteria.CustomerId = "000000000000000000000002";
            result.Criteria.StartDate = new DateTime(2000, 1, 1).ToMidnight();
            result.Criteria.EndDate = new DateTime(2015, 1, 1).ToMidnight();
            result.Category = "TEST";
            result.Name = "Map-Reduce Test";
            result.RequestDate = DateTime.UtcNow;
            result.RequestedBy = User.System;

            RunReportMapReduce(result);
        }

        private void RunReportMapReduce(ReportResult result)
        {
            // Set our series data for the chart
            result.SeriesData = new List<List<object>>();

            User user = User.AsQueryable().Where(u => u.CustomerId == "000000000000000000000002").FirstOrDefault() ?? User.System;

            // Define our first row, which contains series labels for the x (col 1) and y (col 2~n) axis.
            List<object> labels = new List<object>();
            // Our X-Axis will be Employee
            labels.Add("Employee");
            // Our Y-Axis series data will be defined as all available ToDo item types (could be a busy chart)
            labels.AddRange(Enum.GetValues(typeof(ToDoItemType)).OfType<ToDoItemType>().OrderBy(t => (int)t).Select(t => t.ToString().SplitCamelCaseString()));
            // Add our series definition/labels row to the result series data collection.
            result.SeriesData.Add(labels);

            // Define our query
            DateTime startDate = result.Criteria.StartDate;
            DateTime endDate = result.Criteria.EndDate;
            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(user.BuildDataAccessFilters());
            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(ToDoItem.Query.Or(
                ToDoItem.Query.And(ToDoItem.Query.GTE(e => e.CreatedDate, new BsonDateTime(startDate)), ToDoItem.Query.LTE(e => e.CreatedDate, new BsonDateTime(endDate)))
            ));
            IMongoQuery query = ands.Count > 0 ? ToDoItem.Query.And(ands) : null;

            // Define our Map function
            StringBuilder map = new StringBuilder();
            var todoTypes = Enum.GetValues(typeof(ToDoItemType)).OfType<ToDoItemType>().OrderBy(t => (int)t).ToList();
            map.AppendLine("function() {");
            map.AppendLine("    var todo = this;");
            map.AppendLine("    var ret = [todo.AssignedToName];");
            map.AppendLine("    for (var i=0; i<types.length; i++) {");
            map.AppendLine("        if (types[i] === todo.ItemType) {");
            map.AppendLine("            ret.push(1);");
            map.AppendLine("        } else {");
            map.AppendLine("            ret.push(0);");
            map.AppendLine("        }");
            map.AppendLine("    }");
            map.AppendLine("    emit(todo.AssignedToId, { val: ret });");
            map.AppendLine("}");

            // Define our reduce function
            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(key, values) {");
            reduce.AppendLine(" var result = { val: [values[0].val[0]] };");
            reduce.AppendLine(" values.forEach(function(value) {");
            reduce.AppendLine("     for (var i=1; i<value.val.length; i++) {");
            reduce.AppendLine("         result.val[i] = result.val[i] || 0;");
            reduce.AppendLine("         result.val[i] += (value.val[i] || 0);");
            reduce.AppendLine("     }");
            reduce.AppendLine(" });");
            reduce.AppendLine(" return result;");
            reduce.AppendLine("}");

            // Define our map reduce arguments
            MapReduceArgs mapReduce = new MapReduceArgs();
            mapReduce.OutputMode = MapReduceOutputMode.Inline;
            mapReduce.Query = query;
            mapReduce.Scope = new ScopeDocument(new Dictionary<string, object>() { { "types", todoTypes.Select(t => (int)t).OrderBy(t => t).ToArray() } });
            mapReduce.MapFunction = new BsonJavaScript(map.ToString());
            mapReduce.ReduceFunction = new BsonJavaScript(reduce.ToString());

            // Execute our map reduce query operation against the ToDoItem collection
            var response = ToDoItem.Repository.Collection.MapReduce(mapReduce);
            // If not OK or we have an error, throw an exception
            if (!response.Ok || !string.IsNullOrWhiteSpace(response.ErrorMessage))
                result.Success = false;
            else
            {
                // Loop through our inline results and pump out the series data results to the series data collection
                //  on the result object
                foreach (var r in response.InlineResults)
                {
                    object[] rData = r.GetValue("value").AsBsonDocument.GetValue("val").AsBsonArray.Select(b => b.GetRawValue<object>()).ToArray();
                    if (rData != null)
                        result.SeriesData.Add(rData.ToList());
                }
            }
        }
    }
}
