﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Data.Security;
using System.Collections.Generic;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Cases;

namespace AbsenceSoft.Tests.Data
{
    [TestClass]
    public class UserTest
    {
        [TestMethod]
        public void CreateDevUser()
        {
            User devUser = User.AsQueryable().Where(u => u.Email == "dev@absencesoft.com").FirstOrDefault() ?? new User()
            {
                Id = "000000000000000000000000",
                Email = "dev@absencesoft.com",
                FirstName = "Developer",
                CreatedById = "000000000000000000000000",
                ModifiedById = "000000000000000000000000",
                Password = new AbsenceSoft.Data.CryptoString() { PlainText = "Test1234" },
                Roles = new List<string>() { Role.SystemAdministrator.Id },
            };
            Assert.IsNotNull(devUser);
            devUser.Save();
            Assert.IsFalse(string.IsNullOrWhiteSpace(devUser.Id));
        }

        [TestMethod]
        public void TestAllPermissions()
        {
            var perms = Permission.All();
            Assert.IsTrue(perms.Contains(Permission.ADADetail));
            Assert.IsTrue(perms.Contains(Permission.AdjudicateCase));
            Assert.IsTrue(perms.Contains(Permission.AttachFile));
            Assert.IsTrue(perms.Contains(Permission.CancelCase));
            Assert.IsTrue(perms.Contains(Permission.CancelToDo));
            Assert.IsTrue(perms.Contains(Permission.CloseAccount));
            Assert.IsTrue(perms.Contains(Permission.CompleteToDo));
            Assert.IsTrue(perms.Contains(Permission.CreateCase));
            Assert.IsTrue(perms.Contains(Permission.CreateEmployee));
            Assert.IsTrue(perms.Contains(Permission.CreateToDo));
            Assert.IsTrue(perms.Contains(Permission.DeleteEmployee));
            Assert.IsTrue(perms.Contains(Permission.DeleteUser));
            Assert.IsTrue(perms.Contains(Permission.DisableUser));
            Assert.IsTrue(perms.Contains(Permission.EditCase));
            Assert.IsTrue(perms.Contains(Permission.EditCustomerInfo));
            Assert.IsTrue(perms.Contains(Permission.EditEmployee));
            Assert.IsTrue(perms.Contains(Permission.EditEmployerInfo));
            Assert.IsTrue(perms.Contains(Permission.EditLeavePolicy));
            Assert.IsTrue(perms.Contains(Permission.EditRoleDefinition));
            Assert.IsTrue(perms.Contains(Permission.EditSalary));
            Assert.IsTrue(perms.Contains(Permission.EditSSN));
            Assert.IsTrue(perms.Contains(Permission.EditUser));
            Assert.IsTrue(perms.Contains(Permission.EditUserRoles));
            Assert.IsTrue(perms.Contains(Permission.EditWorkflow));
            Assert.IsTrue(perms.Contains(Permission.ReAssignCase));
            Assert.IsTrue(perms.Contains(Permission.ReAssignToDo));
            Assert.IsTrue(perms.Contains(Permission.RunAdHocReport));
            Assert.IsTrue(perms.Contains(Permission.RunCaseManagerReport));
            Assert.IsTrue(perms.Contains(Permission.RunSelfServiceReport));
            Assert.IsTrue(perms.Contains(Permission.SendCommunication));
            Assert.IsTrue(perms.Contains(Permission.TeamDashboard));
            Assert.IsTrue(perms.Contains(Permission.UploadEligibilityFile));
            Assert.IsTrue(perms.Contains(Permission.ViewAttachment));
            Assert.IsTrue(perms.Contains(Permission.ViewSalary));
            Assert.IsTrue(perms.Contains(Permission.ViewSSN));
            Assert.IsTrue(perms.Contains(Permission.ViewTeam));
        }
    }
}
