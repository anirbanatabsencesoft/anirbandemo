﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Tests.Classes
{
    [TestClass]
    public class TestCases
    {

        [TestMethod]
        public void AbsenceReasonShouldNotBeNullOnNewCase()
        {
            Case testCase = new Case();
            Assert.IsNotNull(testCase.Reason);
        }

        [TestMethod]
        public void AbsenceReasonShouldNotBeNullOnSavedInquiry()
        {
            Case testCase = new Case();
            testCase.Reason = null;
            testCase.Status = CaseStatus.Inquiry;
            testCase.Save();
            Assert.IsNotNull(testCase.Reason);
        }
    }
}
