﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Processing;
using AbsenceSoft.Data.RiskProfiles;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Driver;
using System;
using System.Linq;

namespace AbsenceSoft.Tests.AT_Tools
{
    [TestClass]
    public class CustomerAdminServiceTests
    {


        //Soft delete the case
        [TestMethod]
        public void DeleteCase()
        {

            bool blnDelCase = false;
            string caseCode = "EHC";
            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            CaseService svc = new CaseService();
            var emp = "000000000000000000000002";
            Assert.IsNotNull(emp);
            // Remove any cases for this employee
            Case.Repository.Delete(c => c.Employee.Id == "000000000000000000000002");
            var newCase = svc.CreateCase(CaseStatus.Open, emp, new DateTime(2018, 1, 5, 0, 0, 0, DateTimeKind.Utc), new DateTime(2018, 1, 12, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code, description: "CaseServiceTest.CreateCase");
            newCase = svc.UpdateCase(newCase);
            Assert.IsNotNull(newCase);
            Assert.IsNotNull(newCase.Id);
            var tdi = new ToDoService().CreateCaseReviewTodo(newCase.Id, "TEST").Id;
            Assert.IsNotNull(tdi);
            string caseId = newCase.Id;            
            emp = "000000000000000000000002";
            string mbyId = "000000000000000000000002";
            Case caseToDelete = Case.Repository.Where(m => m.Id == caseId && m.EmployerId == emp).FirstOrDefault();
            Assert.IsNotNull(caseToDelete, "Case is not null");
            if (caseToDelete != null)
            {
                //soft delete ActivityHistory
                Case.Repository.Collection.Update(Case.Query.And(
                  Case.Query.EQ(c => c.EmployerId, emp),
                  Case.Query.EQ(c => c.Id, caseId)
              ), Case.Updates
                  .Set(e => e.IsDeleted, true)
                  .Set(e => e.ModifiedById, mbyId)
                  .CurrentDate(e => e.ModifiedDate), UpdateFlags.Multi);
            }
            
            //Soft Delete ToDoItem          
            ToDoItem ToDoItemToDelete = ToDoItem.Repository.Where(m => m.CaseId == caseId && m.EmployerId == emp).FirstOrDefault();
            if (ToDoItemToDelete != null)
            {
                ToDoItem.Repository.Collection.Update(ToDoItem.Query.And(
                ToDoItem.Query.EQ(c => c.EmployerId, emp),
                ToDoItem.Query.EQ(c => c.CaseId, caseId)
        ), ToDoItem.Updates
            .Set(e => e.IsDeleted, true)
            .Set(e => e.ModifiedById, mbyId)
            .CurrentDate(e => e.ModifiedDate), UpdateFlags.Multi);
            }
            blnDelCase = true;
            Assert.IsTrue(blnDelCase, "Case soft deleted successfully.");
            newCase.Delete();
        }
        //Cancel the case
        [TestMethod]
        public void CancelCase()
        {
            // Now create a case for an employer.
            string empId = "000000000000000000000002";
            string caseCode = "EHC";
            // Remove any cases for this employee
            Case.Repository.Delete(c => c.Employee.Id == empId);
            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            Employee testEmp = Employee.GetById(empId);

            // create a test case
            CaseService cs = new CaseService();

            Case newCase = cs.CreateCase(CaseStatus.Open, empId, new DateTime(2018, 1, 13, 0, 0, 0, DateTimeKind.Utc), new DateTime(2018, 1, 20, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Administrative, reason.Code);
            Assert.IsNotNull(newCase);
            newCase.Description = "Case cancelled test";
            newCase.Save();
            Assert.IsNotNull(newCase.Id);
            Assert.IsNotNull(newCase.Segments);
            string caseId = newCase.Id;
            Case caseToCancel = Case.Repository.GetById(caseId);
            bool blnCancel = false;
            if (caseToCancel != null)
            {
                //To Cancel the Case 
                caseToCancel.Status = CaseStatus.Cancelled;
                caseToCancel.ModifiedById = "000000000000000000000002";
                caseToCancel.SetModifiedDate(DateTime.UtcNow);
                caseToCancel.Segments.ForEach(s => s.Status = CaseStatus.Cancelled);
                caseToCancel.SaveStatus();
                blnCancel = true;

            }
            Assert.IsTrue(blnCancel, "Case Successfully cancelled.");
            newCase.Delete();
        }

        //Close the case
        [TestMethod]
        public void CloseCase()
        {
            bool blnCompleteCase = false;
            // now create a case for an employer .
            string empId = "000000000000000000000002";
            string caseCode = "EHC";
            // Remove any cases for this employee
            Case.Repository.Delete(c => c.Employee.Id == "000000000000000000000002");
            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            Employee testEmp = Employee.GetById(empId);

            // create a test case
            CaseService cs = new CaseService();

            Case newCase = cs.CreateCase(CaseStatus.Open, empId, new DateTime(2018, 1, 21, 0, 0, 0, DateTimeKind.Utc), new DateTime(2018, 2, 1, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Administrative, reason.Code);
            Assert.IsNotNull(newCase);
            newCase.Description = "case closed test";
            newCase.Save();
            Assert.IsNotNull(newCase.Id);
            Assert.IsNotNull(newCase.Segments);
            string caseId = newCase.Id;
            Case caseToComplete = Case.Repository.GetById(caseId);
            if (caseToComplete != null)
            {
                //To Complete Case 
                caseToComplete.Status = AbsenceSoft.Data.Enums.CaseStatus.Closed;
                caseToComplete.ModifiedById = "000000000000000000000002";
                caseToComplete.SetModifiedDate(DateTime.UtcNow);
                caseToComplete.Segments.ForEach(s => s.Status = CaseStatus.Closed);
                caseToComplete.SaveStatus();
                blnCompleteCase = true;
            }
            Assert.IsTrue(blnCompleteCase, "Case completed.");
            newCase.Delete();
        }

        //Soft delete the todos
        [TestMethod]
        public void ToDoDelete()
        {
            bool blnTodoDelete = false;
            string caseCode = "EHC";
            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();
            CaseService svc = new CaseService();
            var emp = "000000000000000000000002";
            Assert.IsNotNull(emp);
            // Remove any cases for this employee
            Case.Repository.Delete(c => c.Employee.Id == "000000000000000000000002");
            var newCase = svc.CreateCase(CaseStatus.Open, emp, new DateTime(2017, 2, 5, 0, 0, 0, DateTimeKind.Utc), new DateTime(2017, 2, 12, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Administrative, reason.Code, description: "CaseServiceTest.CreateCase");            
            Assert.IsNotNull(newCase);
            newCase.Save();
            Assert.IsNotNull(newCase.Id);
            var tdi = new ToDoService().CreateCaseReviewTodo(newCase.Id, "TEST").Id;
            Assert.IsNotNull(tdi);
            string Id = tdi;
            string caseId = newCase.Id;
            ToDoItem todoItemToDelete = ToDoItem.Repository.GetById(Id);
            if (todoItemToDelete != null)
            {
                //To Cancel the Todo Item 
                todoItemToDelete.IsDeleted = true;
                todoItemToDelete.ModifiedById = "000000000000000000000002";
                todoItemToDelete.SetModifiedDate(DateTime.UtcNow);
                todoItemToDelete.Save();
                blnTodoDelete = true;

            }
            Assert.IsTrue(blnTodoDelete, "Todo soft deleted.");
            newCase.Delete();
        }

        //Cancel the todos
        [TestMethod]
        public void TodoCancel()
        {
            bool blnTodoCancel = false;
            string caseCode = "EHC";
            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();
            CaseService svc = new CaseService();
            var emp = "000000000000000000000002";
            Assert.IsNotNull(emp);
            // Remove any cases for this employee
            Case.Repository.Delete(c => c.Employee.Id == "000000000000000000000002");
            var newCase = svc.CreateCase(CaseStatus.Open, emp, new DateTime(2017, 2, 14, 0, 0, 0, DateTimeKind.Utc), new DateTime(2017, 2, 21, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Administrative, reason.Code, description: "CaseServiceTest.CreateCase");            
            Assert.IsNotNull(newCase);
            newCase.Save();
            Assert.IsNotNull(newCase.Id);
            var tdi = new ToDoService().CreateCaseReviewTodo(newCase.Id, "TEST").Id;
            Assert.IsNotNull(tdi);
            string Id = tdi;
            string caseId = newCase.Id;
            ToDoItem todoItemToCancel = ToDoItem.Repository.GetById(Id);
            if (todoItemToCancel != null)
            {
                //To Cancel the Todo Item 
                todoItemToCancel.Status = AbsenceSoft.Data.Enums.ToDoItemStatus.Cancelled;
                todoItemToCancel.ModifiedById = "000000000000000000000002";
                todoItemToCancel.SetModifiedDate(DateTime.UtcNow);
                todoItemToCancel.Save();
                blnTodoCancel = true;
            }
            Assert.IsTrue(blnTodoCancel, "Todo cancelled.");
            newCase.Delete();
        }

        //Complete the case
        [TestMethod]
        public void TodoComplete()
        {
            bool blnTodoComplete = false;
            string caseCode = "EHC";
            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();
            CaseService svc = new CaseService();
            var emp = "000000000000000000000002";
            Assert.IsNotNull(emp);
            // Remove any cases for this employee
            Case.Repository.Delete(c => c.Employee.Id == "000000000000000000000002");
            var newCase = svc.CreateCase(CaseStatus.Open, emp, new DateTime(2017, 2, 22, 0, 0, 0, DateTimeKind.Utc), new DateTime(2017, 2, 27, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Administrative, reason.Code, description: "CaseServiceTest.CreateCase");           
            Assert.IsNotNull(newCase);
            newCase.Save();
            Assert.IsNotNull(newCase.Id);
            var tdi = new ToDoService().CreateCaseReviewTodo(newCase.Id, "TEST").Id;
            Assert.IsNotNull(tdi);
            string Id = tdi;
            string caseId = newCase.Id;
            ToDoItem todoItemToComplete = ToDoItem.Repository.GetById(Id);
            if (todoItemToComplete != null)
            {
                //To Complete the ToDo item 
                todoItemToComplete.Status = AbsenceSoft.Data.Enums.ToDoItemStatus.Complete;
                todoItemToComplete.ModifiedById = "000000000000000000000002";
                todoItemToComplete.SetModifiedDate(DateTime.UtcNow);
                todoItemToComplete.Save();
                blnTodoComplete = true;
            }
            Assert.IsTrue(blnTodoComplete, "Todo completed.");            
            newCase.Delete();
        }

        //Soft delete the employee and its related data.
        [TestMethod]
        public void DeleteEmployee()
        {
            bool blnDelEmp = false;
            string caseCode = "EHC";
            AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == caseCode).First();

            CaseService svc = new CaseService();
            var emp = "000000000000000000000002";
            var mById = "000000000000000000000002";
            Assert.IsNotNull(emp);
            // Remove any cases for this employee
            Case.Repository.Delete(c => c.Employee.Id == emp);
            var newCase = svc.CreateCase(CaseStatus.Open, emp, new DateTime(2018, 1, 5, 0, 0, 0, DateTimeKind.Utc), new DateTime(2018, 1, 12, 0, 0, 0, DateTimeKind.Utc), AbsenceSoft.Data.Enums.CaseType.Consecutive, reason.Code, description: "CaseServiceTest.CreateCase");
            newCase = svc.UpdateCase(newCase);
            Assert.IsNotNull(newCase);
            Assert.IsNotNull(newCase.Id);
            var tdi = new ToDoService().CreateCaseReviewTodo(newCase.Id, "TEST").Id;
            Assert.IsNotNull(tdi);           

            if (!string.IsNullOrWhiteSpace(emp))
            {
                var query = Case.Repository.Where(o => o.Employee.Id == emp && o.EmployerId == emp && o.CustomerId == emp).ToList();
                var lstCase = query.Select(c => c.Id).ToList();
                string employeeNumber = AbsenceSoft.Data.Customers.Employee.Repository.Where(m => m.Id == emp && m.EmployerId == emp && m.CustomerId == emp).FirstOrDefault().EmployeeNumber;
                foreach (var caseId in lstCase)
                {
                    //Soft delete case
                    Case.Repository.Collection.Update(Case.Query.And(
                       Case.Query.EQ(c => c.EmployerId, emp),
                       Case.Query.EQ(c => c.CustomerId, emp),
                        Case.Query.EQ(c => c.Employee.Id, emp),
                       Case.Query.EQ(c => c.Id, caseId)
                   ), Case.Updates
                       .Set(e => e.IsDeleted, true)
                       .Set(e => e.ModifiedById, mById)
                       .CurrentDate(e => e.ModifiedDate), UpdateFlags.Multi);
                    blnDelEmp = true;
                    
                    //soft delete ToDoItem
                    ToDoItem ToDoItem = ToDoItem.Repository.Where(m => m.CaseId == caseId && m.EmployeeId == emp && m.EmployerId == emp && m.CustomerId == emp).FirstOrDefault();
                    if (ToDoItem != null)
                    {
                        ToDoItem.Repository.Collection.Update(ToDoItem.Query.And(

                      ToDoItem.Query.EQ(c => c.EmployerId, emp),
                      ToDoItem.Query.EQ(c => c.CustomerId, emp),
                      ToDoItem.Query.EQ(c => c.EmployeeId, emp),
                      ToDoItem.Query.EQ(c => c.CaseId, caseId)
                  ), ToDoItem.Updates
                      .Set(e => e.IsDeleted, true)
                      .Set(e => e.ModifiedById, mById)
                      .CurrentDate(e => e.ModifiedDate), UpdateFlags.Multi);
                        blnDelEmp = true;
                    }

                    
                }
                //soft delete Employee 
                Employee Employee = Employee.Repository.Where(m => m.Id == emp && m.EmployerId == emp && m.CustomerId == emp).FirstOrDefault();
                if (Employee != null)
                {
                    Employee.Repository.Collection.Update(Employee.Query.And(
                      Employee.Query.EQ(c => c.EmployerId, emp),
                      Employee.Query.EQ(c => c.CustomerId, emp),
                      Employee.Query.EQ(c => c.Id, emp)
                  ), Employee.Updates
                      .Set(e => e.IsDeleted, true)
                      .Set(e => e.ModifiedById, mById)
                      .CurrentDate(e => e.ModifiedDate), UpdateFlags.Multi);
                    blnDelEmp = true;
                }
                
                //soft delete EmployeeContact 
                EmployeeContact EmployeeContact = EmployeeContact.Repository.Where(m => m.EmployeeId == emp && m.EmployerId == emp && m.CustomerId == emp).FirstOrDefault();
                if (EmployeeContact != null)
                {
                    EmployeeContact.Repository.Collection.Update(EmployeeContact.Query.And(

                      EmployeeContact.Query.EQ(c => c.EmployerId, emp),
                      EmployeeContact.Query.EQ(c => c.CustomerId, emp),
                      EmployeeContact.Query.EQ(c => c.EmployeeId, emp)
                  ), EmployeeContact.Updates
                      .Set(e => e.IsDeleted, true)
                      .Set(e => e.ModifiedById, mById)
                      .CurrentDate(e => e.ModifiedDate), UpdateFlags.Multi);
                    blnDelEmp = true;
                }
                //soft delete EmployeeContact with related employee number
                EmployeeContact EmployeeContactRel = EmployeeContact.Repository.Where(m => m.RelatedEmployeeNumber == (EmployeeContact != null ? EmployeeContact.RelatedEmployeeNumber : null) && m.EmployerId == emp).FirstOrDefault();
                if (EmployeeContactRel != null)
                {
                    EmployeeContact.Repository.Collection.Update(EmployeeContact.Query.And(

                      EmployeeContact.Query.EQ(c => c.EmployerId, emp),
                      EmployeeContact.Query.EQ(c => c.CustomerId, emp),
                      EmployeeContact.Query.EQ(c => c.RelatedEmployeeNumber, employeeNumber)
                  ), EmployeeContact.Updates
                      .Set(e => e.IsDeleted, true)
                      .Set(e => e.ModifiedById, mById)
                      .CurrentDate(e => e.ModifiedDate), UpdateFlags.Multi);
                    blnDelEmp = true;
                }
                
                Assert.IsTrue(blnDelEmp);
                newCase.Delete();
            }
        }
    }
}
