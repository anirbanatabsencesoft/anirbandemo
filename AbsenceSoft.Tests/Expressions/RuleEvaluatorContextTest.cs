﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Workflows;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Tests.Expressions
{
    [TestClass]
    public class RuleEvaluatorContextTest
    {

        public RuleEvaluatorContext<Case> CreateRuleEvaluatorContext(EligibilityStatus? status = null)
        {
            var theCase = new Case()
            {
                Segments = new List<CaseSegment>()
                {
                    new CaseSegment()
                    {
                        AppliedPolicies = new List<AppliedPolicy>()
                        {
                            new AppliedPolicy()
                            {
                                Status = status ?? EligibilityStatus.Eligible
                            },
                            new AppliedPolicy()
                            {
                                Status = status ?? EligibilityStatus.Ineligible
                            }
                        }
                    }
                }
            };

            return new RuleEvaluatorContext<Case>()
            {
                Case = theCase
            };
        }

        [TestMethod]
        public void IsAnyIneligibleTest()
        {
            bool isAnyIneligible = CreateRuleEvaluatorContext().IsAnyIneligible();
            Assert.IsTrue(isAnyIneligible);
        }

        [TestMethod]
        public void IsAllEligible()
        {
            bool isAnyIneligible = CreateRuleEvaluatorContext(EligibilityStatus.Eligible).IsAnyIneligible();
            Assert.IsFalse(isAnyIneligible);
        }

        [TestMethod]
        public void IsAnyEligibleTest()
        {
            bool isAnyEligible = CreateRuleEvaluatorContext().IsEligible();
            Assert.IsTrue(isAnyEligible);
        }

        [TestMethod]
        public void IsAllIneligible()
        {
            bool isAnyEligible = CreateRuleEvaluatorContext(EligibilityStatus.Ineligible).IsEligible();
            Assert.IsFalse(isAnyEligible);
        }
    }
}
