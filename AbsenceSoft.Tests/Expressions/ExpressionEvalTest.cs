﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Rules;
using FluentAssertions;
using System.Collections.Generic;
using AbsenceSoft.Data.Rules;

namespace AbsenceSoft.Tests.Expressions
{
    [TestClass]
    public class ExpressionEvalTest
    {
        [TestMethod]
        public void FullExpressionTest()
        {
            Case testCase;
            using (CaseService svc = new CaseService())
            {
                var emp = Employee.AsQueryable().Where(e => e.EmployeeNumber == "demo_001").FirstOrDefault();
                Assert.IsNotNull(emp);
                var reasons = svc.GetAbsenceReasons(emp.Id);
                Assert.IsNotNull(reasons);
                Assert.IsTrue(reasons.Any());
                var r = reasons.First();

                Case.AsQueryable().Where(c => c.Employee.Id == emp.Id && c.Reason.Code == r.Code).ForEach(a => a.Delete());

                testCase = svc.CreateCase(CaseStatus.Open, emp.Id, DateTime.Today, DateTime.Today.AddDays(15), AbsenceSoft.Data.Enums.CaseType.Consecutive, r.Code, description: "CaseServiceTest.CreateCase");
                Assert.IsNotNull(testCase);
                testCase = svc.UpdateCase(testCase);
                Assert.IsNotNull(testCase.Id);
            }

            using (EligibilityService svc = new EligibilityService())
            {
                LeaveOfAbsence loa = svc.GetLeaveOfAbsence(testCase.Id);
                Assert.IsNotNull(loa);

                object status = LeaveOfAbsenceEvaluator.Eval("Case.Status", loa);
                Assert.AreEqual(status, CaseStatus.Open);
                bool isOpen = LeaveOfAbsenceEvaluator.Eval<bool>("Case.Status == CaseStatus.Open", loa);
                Assert.IsTrue(isOpen);
                object hoursWorked = LeaveOfAbsenceEvaluator.Eval("TotalHoursWorkedLast12Months()", loa);
                Assert.IsTrue((double)hoursWorked > 0);
                object custName = LeaveOfAbsenceEvaluator.Eval("Employee.Employer.Customer.Name", loa);
                Assert.AreEqual(custName, "AbsenceSoft Demo");
                bool contactSelf = LeaveOfAbsenceEvaluator.Eval<bool>("CaseRelationshipType == 'SELF'", loa);
                Assert.IsTrue(contactSelf);

                // These have some issues.
                string reasonName = LeaveOfAbsenceEvaluator.Eval<string>("Case.Reason.Name", loa);
                Assert.IsNotNull(reasonName);
                bool isReason = LeaveOfAbsenceEvaluator.Eval<bool>("Case.Reason.Name == '" + reasonName + "'", loa);
                Assert.IsTrue(isReason);


                //Verify And Matching rule behavior  (Not Work related check)
                var ruleForTest1 = BuildIsNotWorkRelatedRuleForTest("Is Not Work Related", "Leave of absence is not work related", "IsMetadataValueTrue(\'IsWorkRelated\')", "!=", "true");
                var andMatchingResult = true && LeaveOfAbsenceEvaluator.Eval<bool>(ruleForTest1.ToString(), loa);


                //Verify Not Matching Rule behavior - Is  Work related check
                var ruleForTest2 = BuildIsNotWorkRelatedRuleForTest("Is Work Related", "Leave of absence is work related", "IsMetadataValueTrue(\'IsWorkRelated\')", "==", "true");
                var notMatchingResult = !( LeaveOfAbsenceEvaluator.Eval<bool>(ruleForTest2.ToString(), loa));

                //Verify existing business  behavior wont change even after changing the  operator 
                Assert.AreEqual(andMatchingResult, notMatchingResult);
            }
        }

        [Serializable]
        public class ExpressionEvalTestThingy
        {
            public DateTime? Value { get; set; }
        }

        [TestMethod]
        public void DateExpressionTest()
        {
            ExpressionEvalTestThingy thingy = new ExpressionEvalTestThingy() { Value = new DateTime(2016, 2, 14).ToMidnight() };
            List<Tuple<string, bool>> expressions = new List<Tuple<string, bool>>(8)
            {
                new Tuple<string, bool>("Value <= #02/15/2016#", true),
                new Tuple<string, bool>("Value <= #02/14/2016#", true),
                new Tuple<string, bool>("Value <= #02/13/2016#", false),
                new Tuple<string, bool>("Value < #02/15/2016#", true),
                new Tuple<string, bool>("Value < #02/14/2016#", false),
                new Tuple<string, bool>("Value >= #02/13/2016#", true),
                new Tuple<string, bool>("Value >= #02/14/2016#", true),
                new Tuple<string, bool>("Value >= #02/15/2016#", false),
                new Tuple<string, bool>("Value > #02/13/2016#", true),
                new Tuple<string, bool>("Value > #02/14/2016#", false),
                new Tuple<string, bool>("Value == #02/14/2016#", true),
                new Tuple<string, bool>("Value == #02/15/2016#", false),
                new Tuple<string, bool>("Value != #02/15/2016#", true),
                new Tuple<string, bool>("Value != #02/14/2016#", false)
            };
            var expressionDate = GenericEvaluator.Eval<DateTime?>("Value", thingy);
            expressionDate.Should().HaveValue().And.Be(thingy.Value, "expression evaluated date should be equal to the original");
            foreach (var expr in expressions)
            {
                var result = GenericEvaluator.Eval<bool>(expr.Item1, thingy);
                result.Should().Be(expr.Item2, string.Format("the expression \"{0}\" failed", expr.Item1));
            }
        }

        public static Rule BuildIsNotWorkRelatedRuleForTest(string name, string description, string leftExpression, string op, 
            string rightExpression) => new Rule()
        {
            Name = name,
            Description = description,
            LeftExpression = leftExpression,
            Operator = op,
            RightExpression = rightExpression
        };
    }
}
