﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace AbsenceSoft.Conversion.Absentys
{
    public abstract class BaseAbsentysRecord
    {
        #region Protected Members

        protected string[] _parts;
        protected List<string> _errors;
        protected const int MaxMinutesPerWeek = 10080;

        #endregion Protected Members

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseAbsentysRecord"/> class.
        /// </summary>
        public BaseAbsentysRecord()
        {
            _errors = new List<string>();
            Delimiter = ',';
            _parts = null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseAbsentysRecord"/> class.
        /// </summary>
        /// <param name="rowSource">The row source.</param>
        public BaseAbsentysRecord(string[] rowSource) : this()
        {
            Parse(rowSource);
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the delimiter.
        /// </summary>
        /// <value>
        /// The delimiter.
        /// </value>
        public char Delimiter { get; set; }
        /// <summary>
        /// Gets a value indicating whether this instance is error.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is error; otherwise, <c>false</c>.
        /// </value>
        public bool IsError { get { return _errors.Any(); } }
        /// <summary>
        /// Gets the error.
        /// </summary>
        /// <value>
        /// The error.
        /// </value>
        public string Error { get { return IsError ? string.Join(Environment.NewLine, _errors) : null; } }
        /// <summary>
        /// Gets the errors.
        /// </summary>
        /// <value>
        /// The errors.
        /// </value>
        public IReadOnlyCollection<string> Errors { get { return _errors.AsReadOnly(); } }

        #endregion Public Properties

        #region Parsing

        /// <summary>
        /// Parses the specified row source and returns whether or not the parsed result contained no errors.
        /// </summary>
        /// <param name="rowSource">The row source/record parts to parse.</param>
        /// <returns><c>true</c> if successfully parsed without error, otherwise <c>false</c>.</returns>
        public virtual bool Parse(string[] rowSource = null)
        {
            _parts = rowSource;
            _Parse();
            return !IsError;
        }

        /// <summary>
        /// Parse the stored row source.
        /// </summary>
        protected abstract void _Parse();

        /// <summary>
        /// Adds the error.
        /// </summary>
        /// <param name="error">The error.</param>
        /// <param name="args">The arguments.</param>
        /// <returns></returns>
        public virtual bool AddError(string error, params object[] args)
        {
            if (string.IsNullOrWhiteSpace(error))
                return false;
            if (args.Length > 0)
                _errors.Add(string.Format(error, args));
            else
                _errors.Add(error);
            return false;
        }

        /// <summary>
        /// Normalizes the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        protected virtual string Normalize(int index)
        {
            if (index >= _parts.Length)
                return null;
            string data = _parts[index];
            if (string.IsNullOrWhiteSpace(data))
                return null;
            data = Regex.Replace(data, "(^\")|(\"$)", "").Trim();
            data = data.Replace("\"\"", "\"");
            data = data.Replace("\\n", Environment.NewLine);
            if (string.IsNullOrWhiteSpace(data))
                return null;
            return data;
        }

        /// <summary>
        /// Parses the int.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        protected virtual int? ParseInt(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return null;

            int n;
            if (int.TryParse(data, out n))
                return n;

            AddError("'{0}' is not a valid integer. Please provide a valid integer value for '{1}'.", data, name);
            return null;
        }

        /// <summary>
        /// Parses the date.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        protected virtual DateTime? ParseDate(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return null;

            DateTime dt;
            if (DateTime.TryParseExact(data, new string[] { "yyyy-MM-dd HH:mm:ss.ttttttttt", "yyyy-MM-dd", "MM/dd/yyyy", "M/d/yyyy", "MM/dd/yy", "M/d/yy" }, DateTimeFormatInfo.CurrentInfo, DateTimeStyles.AssumeUniversal, out dt))
                return dt.Kind == DateTimeKind.Local ? dt.ToUniversalTime() : dt;

            AddError("'{0}' is not a valid date or in an incorrect format. Please provide a date for '{1}' in the format of 'YYYY-MM-DD' or 'MM/DD/YYYY'.", data, name);
            return null;
        }

        /// <summary>
        /// Parses the byte.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        protected virtual byte? ParseByte(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return null;

            byte b;
            if (byte.TryParse(data, out b))
                return b;

            AddError("'{0}' is not a valid byte. Please provide a valid byte value for '{1}'.", data, name);
            return null;
        }

        /// <summary>
        /// Parses the character.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        protected virtual char? ParseChar(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return null;

            char c;
            if (char.TryParse(data, out c))
                return c;

            AddError("'{0}' is not a valid char value (1 character alpha). Please provide a valid char value for '{1}'.", data, name);
            return null;
        }

        /// <summary>
        /// Parses the bool.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        protected virtual bool? ParseBool(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return null;

            if (Regex.IsMatch(data, @"^[1YT]$", RegexOptions.CultureInvariant | RegexOptions.IgnoreCase))
                return true;
            if (Regex.IsMatch(data, @"^[0NF]$", RegexOptions.CultureInvariant | RegexOptions.IgnoreCase))
                return false;

            AddError("'{0}' is not a valid Boolean value. Please provide a valid 1 character Boolean value for '{1}' of either True ('Y', '1', or 'T') or False ('N', '0', or 'F').", data, name);
            return null;
        }

        /// <summary>
        /// Parses the decimal.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        protected virtual decimal? ParseDecimal(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return null;

            decimal d;
            if (decimal.TryParse(data, out d))
                return d;

            AddError("'{0}' is not a valid decimal. Please provide a valid decimal value for '{1}'.", data, name);
            return null;
        }

        /// <summary>
        /// Parses the double.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        protected virtual double? ParseDouble(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return null;

            double d;
            if (double.TryParse(data, out d))
                return d;

            AddError("'{0}' is not a valid floating point number. Please provide a valid floating point value for '{1}'.", data, name);
            return null;
        }

        /// <summary>
        /// Requireds the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        protected virtual bool Required(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return AddError("'{0}' is a required field and was not provided, was missing, blank or did not contain any valid information.", name);
            return true;
        }

        /// <summary>
        /// Ins the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="name">The name.</param>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        protected virtual bool In(int index, string name, params string[] values)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return false;
            if (!values.Contains(data))
                return AddError("'{0}' is not a valid value for {1}. The value for {1} must be one of '{2}'.", data, name, string.Join("', '", values));
            return true;
        }

        /// <summary>
        /// Valids the state of the us.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        protected virtual bool ValidUSState(string state, string name)
        {
            if (string.IsNullOrWhiteSpace(state))
                return false;
            if (state.Length != 2 || !Regex.IsMatch(state, @"^(A[LKSZRAEP]|C[AOT]|D[EC]|F[LM]|G[AU]|HI|I[ADLN]|K[SY]|LA|M[ADEHINOPST]|N[CDEHJMVY]|O[HKR]|P[ARW]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY])$"))
                return AddError("'{0}' is not a valid or recognized 2 character US Postal Service State abbreviation for a 'US' state, region or territory. Please provide a valid state for '{1}'.", state, name);
            return true;
        }

        /// <summary>
        /// Valids the email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        protected virtual bool ValidEmail(string email, string name)
        {
            if (!string.IsNullOrWhiteSpace(email) && (!email.Contains('@') || !email.Contains('.')))
                return AddError("'{0}' does not appear to be a valid email address. If providing '{1}', please provide a valid email address.", email, name);
            return true;
        }

        /// <summary>
        /// Valids the minutes per week.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        protected virtual bool ValidMinutesPerWeek(int? value)
        {
            if (value.HasValue)
            {
                return ValidRange("Scheduled Minutes per Week", value, 0, MaxMinutesPerWeek, "minutes");
            }
            return true;
        }

        /// <summary>
        /// Valids the range.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <param name="start">The start.</param>
        /// <param name="end">The end.</param>
        /// <param name="unitType">Type of the unit.</param>
        /// <returns></returns>
        protected virtual bool ValidRange(string name, int? value, int start, int end, string unitType = null)
        {
            if (!value.IsInRange(start, end))
            {
                return AddError("'Invalid value '{0}' for '{1}'. Valid value range is {2} through {3} {4}",
                    value,
                    name,
                    start,
                    end,
                    unitType
                );
            }
            return true;
        }

        #endregion Parsing
    }
}
