﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Conversion.Absentys
{
    public class PolicyRecord : BaseAbsentysRecord
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PolicyRecord"/> class.
        /// </summary>
        public PolicyRecord() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="PolicyRecord"/> class.
        /// </summary>
        /// <param name="rowSource">The row source.</param>
        public PolicyRecord(string[] rowSource) : base(rowSource) { }

        /// <summary>
        /// Gets or sets the leave number.
        /// </summary>
        /// <value>
        /// The leave number.
        /// </value>
        public string LeaveNumber { get; set; }

        /// <summary>
        /// Gets or sets the policy nickname.
        /// </summary>
        /// <value>
        /// The policy nickname.
        /// </value>
        public string PolicyNickname { get; set; }

        /// <summary>
        /// Gets or sets the leave start (1).
        /// </summary>
        /// <value>
        /// The leave start (1).
        /// </value>
        public DateTime? LeaveStart1 { get; set; }

        /// <summary>
        /// Gets or sets the leave end (1).
        /// </summary>
        /// <value>
        /// The leave end (1).
        /// </value>
        public DateTime? LeaveEnd1 { get; set; }

        /// <summary>
        /// Gets or sets the denial reason (1).
        /// </summary>
        /// <value>
        /// The denial reason (1).
        /// </value>
        public string DenialReason1 { get; set; }

        /// <summary>
        /// Gets or sets the ignore holidays.
        /// </summary>
        /// <value>
        /// The ignore holidays.
        /// </value>
        public bool? IgnoreHolidays { get; set; }

        /// <summary>
        /// Gets or sets the calendar days.
        /// </summary>
        /// <value>
        /// The calendar days.
        /// </value>
        public int? CalendarDays { get; set; }

        /// <summary>
        /// Gets or sets the working days.
        /// </summary>
        /// <value>
        /// The working days.
        /// </value>
        public int? WorkingDays { get; set; }

        /// <summary>
        /// Gets or sets the policy status.
        /// </summary>
        /// <value>
        /// The policy status.
        /// </value>
        public string PolicyStatus { get; set; }

        /// <summary>
        /// Gets or sets the denial reason.
        /// </summary>
        /// <value>
        /// The denial reason.
        /// </value>
        public string DenialReason { get; set; }

        /// <summary>
        /// Gets or sets the leave start.
        /// </summary>
        /// <value>
        /// The leave start.
        /// </value>
        public DateTime? LeaveStart { get; set; }

        /// <summary>
        /// Gets or sets the leave end.
        /// </summary>
        /// <value>
        /// The leave end.
        /// </value>
        public DateTime? LeaveEnd { get; set; }

        /// <summary>
        /// Gets or sets the initial packet (1).
        /// </summary>
        /// <value>
        /// The initial packet (1).
        /// </value>
        public DateTime? InitialPacket1 { get; set; }

        /// <summary>
        /// Gets or sets the status by.
        /// </summary>
        /// <value>
        /// The status by.
        /// </value>
        public string StatusBy { get; set; }

        /// <summary>
        /// Gets or sets the audit date.
        /// </summary>
        /// <value>
        /// The audit date.
        /// </value>
        public DateTime? AuditDate { get; set; }

        /// <summary>
        /// Gets or sets the adjudication date.
        /// </summary>
        /// <value>
        /// The adjudication date.
        /// </value>
        public DateTime? AdjudicationDate { get; set; }

        /// <summary>
        /// Gets or sets the policy start.
        /// </summary>
        /// <value>
        /// The policy start.
        /// </value>
        public DateTime? PolicyStart { get; set; }

        /// <summary>
        /// Gets or sets the initial packet.
        /// </summary>
        /// <value>
        /// The initial packet.
        /// </value>
        public DateTime? InitialPacket { get; set; }

        /// <summary>
        /// Gets or sets the cert due.
        /// </summary>
        /// <value>
        /// The cert due.
        /// </value>
        public DateTime? CertDue { get; set; }

        /// <summary>
        /// Gets or sets the cert received.
        /// </summary>
        /// <value>
        /// The cert received.
        /// </value>
        public DateTime? CertReceived { get; set; }

        /// <summary>
        /// Gets or sets the approval letter.
        /// </summary>
        /// <value>
        /// The approval letter.
        /// </value>
        public DateTime? ApprovalLetter { get; set; }

        /// <summary>
        /// Gets or sets the ICD-9 code.
        /// </summary>
        /// <value>
        /// The ICD-9 code.
        /// </value>
        public string ICD9Code { get; set; }

        /// <summary>
        /// Gets or sets the policy nickname (1).
        /// </summary>
        /// <value>
        /// The policy nickname (1).
        /// </value>
        public string PolicyNickname1 { get; set; }

        /// <summary>
        /// Gets or sets the status date.
        /// </summary>
        /// <value>
        /// The status date.
        /// </value>
        public DateTime? StatusDate { get; set; }

        /// <summary>
        /// Encapsulates the const field indexes for parsing this record type from the Absentys export files.
        /// </summary>
        protected class FieldIndex
        {
            public const int leave_no = 0;
            public const int policy_nickname = 1;
            public const int leave_start1 = 2;
            public const int leave_end1 = 3;
            public const int denial_reason1 = 4;
            public const int ignore_holidays = 5;
            public const int calendar_days = 6;
            public const int working_days = 7;
            public const int policy_status = 8;
            public const int denial_reason = 9;
            public const int leave_start = 10;
            public const int leave_end = 11;
            public const int intial_packet1 = 12;
            public const int status_by = 13;
            public const int audit_date = 14;
            public const int adjudication_date = 15;
            public const int policy_start = 16;
            public const int intial_packet = 17;
            public const int cert_due = 18;
            public const int cert_received = 19;
            public const int approval_letter = 20;
            public const int icd9_code = 21;
            public const int policy_nickname1 = 22;
            public const int status_date = 23;
        }

        /// <summary>
        /// Parse the stored row source.
        /// </summary>
        protected override void _Parse()
        {
            LeaveNumber = Normalize(FieldIndex.leave_no);
            PolicyNickname = Normalize(FieldIndex.policy_nickname);
            LeaveStart1 = ParseDate(FieldIndex.leave_start1, "leave_start1");
            LeaveEnd1 = ParseDate(FieldIndex.leave_end1, "leave_end1");
            DenialReason1 = Normalize(FieldIndex.denial_reason1);
            IgnoreHolidays = ParseBool(FieldIndex.ignore_holidays, "ignore_holidays");
            CalendarDays = ParseInt(FieldIndex.calendar_days, "calendar_days");
            WorkingDays = ParseInt(FieldIndex.working_days, "working_days");
            PolicyStatus = Normalize(FieldIndex.policy_status);
            DenialReason = Normalize(FieldIndex.denial_reason);
            LeaveStart = ParseDate(FieldIndex.leave_start, "leave_start");
            LeaveEnd = ParseDate(FieldIndex.leave_end, "leave_end");
            InitialPacket1 = ParseDate(FieldIndex.intial_packet1, "intial_packet1");
            StatusBy = Normalize(FieldIndex.status_by);
            AuditDate = ParseDate(FieldIndex.audit_date, "audit_date");
            AdjudicationDate = ParseDate(FieldIndex.adjudication_date, "adjudication_date");
            PolicyStart = ParseDate(FieldIndex.policy_start, "policy_start");
            InitialPacket = ParseDate(FieldIndex.intial_packet, "intial_packet");
            CertDue = ParseDate(FieldIndex.cert_due, "cert_due");
            CertReceived = ParseDate(FieldIndex.cert_received, "cert_received");
            ApprovalLetter = ParseDate(FieldIndex.approval_letter, "approval_letter");
            ICD9Code = Normalize(FieldIndex.icd9_code);
            PolicyNickname1 = Normalize(FieldIndex.policy_nickname1);
            StatusDate = ParseDate(FieldIndex.status_date, "status_date");
        }
    }
}
