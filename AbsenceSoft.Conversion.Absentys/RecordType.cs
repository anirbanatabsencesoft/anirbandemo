﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Conversion.Absentys
{
    internal enum RecordType
    {
        Unknown = 'U',
        Leave = 'L',
        Policy = 'P',
        CancelledLeave = 'C',
        Reminder  = 'R'
    }
}
