﻿using AbsenceSoft.Common.Properties;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Processing;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Processing;
using AbsenceSoft.Logic.Processing.EL;
using AbsenceSoft.Logic.SSO;
using AbsenceSoft.Process.MessageProcessing;
using AbsenceSoft.Rendering;
using AbsenceSoft.Reporting;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace AbsenceSoft.Conversion.Absentys
{
    class Program
    {
        private static Customer Customer { get; set; }
        private static string CustomerId { get; set; }

        public static Dictionary<string, LeaveRecord> LeavesToProcess { get; set; }
        public static List<PolicyRecord> PoliciesToProcess { get; set; }
        public static List<ReminderRecord> RemindersToProcess { get; set; }

        public static Dictionary<string, string> ReasonMap = new Dictionary<string, string>()
        {
            {"ADOPT", "ADOPT"},
            {"AIRPATROL", "ACTIVEDUTY"},
            {"APPOINT", "ADMIN"},
            {"BEREAVE", "BEREAVEMENT"},
            {"BEREAVE_CHILD", "BEREAVEMENT"},
            {"CHILD", "FHC"},
            {"CIVILUNION", "FHC"},
            {"COASTGUARD", "ACTIVEDUTY"},
            {"DONOR", "BLOOD"},
            {"DONOR_STEMCELL", "ORGAN"},
            {"EMC_LEAVE", "RESERVETRAIN"},
            {"EMERG_RESPOND", "ACTIVEDUTY"},
            {"EMERG_TRAIN", "RESERVETRAIN"},
            {"FAMMIL_CHILD", "EXIGENCY"},
            {"FAMMIL_IS", "MILITARY"},
            {"FAMMIL_IS_VET", "MILITARY"},
            {"FAMMIL_OTHER", "EXIGENCY"},
            {"FAMMIL_PARENT", "EXIGENCY"},
            {"FAMMIL_SPOUSE", "EXIGENCY"},
            {"FIRE_EMC_LEAVE", "ACTIVEDUTY"},
            {"FIRE_TRAIN", "RESERVETRAIN"},
            {"FOSTER", "ADOPT"},
            {"GRANDCHILD", "FHC"},
            {"GRANDPARENT", "FHC"},
            {"ILLCHILD", "FHC"},
            {"MARROW", "BONE"},
            {"MIL_NG_ACTIVE", "MILITARY"},
            {"MIL_NG_TRAIN", "RESERVETRAIN"},
            {"MIL_SDF_ACTIVE", "MILITARY"},
            {"MIL_SDF_TRAIN", "RESERVETRAIN"},
            {"MIL_VETERAN", "MILITARY"},
            {"MILITARY", "MILITARY"},
            {"NEWBORN", "BONDING"},
            {"OWN", "EHC"},
            {"PARENT", "FHC"},
            {"PARENTINLAW", "FHC"},
            {"PARTNER", "FHC"},
            {"PERSONAL", "PERSONAL"},
            {"PPL_LEGAL_CHILD", "DOM"},
            {"PPL_LEGAL_FAM", "DOM"},
            {"PPL_LEGAL_OWN", "DOM"},
            {"PPL_OTHER", "DOM"},
            {"PPL_OWN", "DOM"},
            {"PPL_RECOVER_CHILD", "DOM"},
            {"PPL_RECOVER_FAM", "DOM"},
            {"PPL_RECOVER_OWN", "DOM"},
            {"PPL  WITNESS", "DOM"},
            {"PREGNANCY", "PREGMAT"},
            {"RELATIVE", "BLOOD"},
            {"SCHOOL", "PARENTAL"},
            {"SPOUSE", "FHC"},
            {"VOL_RESCUE", "ACTIVEDUTY"}
        };


        public static Dictionary<string, string> PolicyMap = new Dictionary<string, string>()
        {
            {"Personal","PER"},
            {"FMLA","FMLA"},
            {"ORFML","OR-FML"},
            {"PDL","CA-CPDL"},
            {"CFRA","CA-CFRA"},
            // {"Medical","???"},
            {"Bereavement","BRVMT"},
            // {"STD","???"},
            // {"MedicalLeave","???"},
            // {"HDS","???"},
            {"WAFML","WA-FML"},
            {"CTFML","CT-FML"},
            // {"MLOA","???"},
            {"Parental","PARENT"},
            {"Military-LongTerm","USERRA"},
            {"PFMLA","MA-MATERN"},
            // {"MA_PARENT","MA-MATERN???"},
            {"CTPDL","CT-PDL"},
            // {"CA_FEHA","???"},
            {"WC","WC"},
            {"WAPDL","WA-PDL"},
             {"Military","USERRA"},
            {"USERRA","USERRA"},
            {"MNPARENT","MN-FML"},
            {"ORPDL","OR-FML"},
            // {"PRPREG","???"},
            // {"PRMAT","???"},
            // {"Military-Training","???"},
            { "WIPREG", "WI-FML"},
            {"NJFML","NJ-FML"},
            {"CA_PPL","CA_DOM"},
            {"DCMED","DC-FML"},
            {"WIPERS","WI-FML"},
            {"IL_PPL","IL-VESSA"},
            {"DCFAM","DC-FML"},
            // {"CAFIRE","???"},
            // {"CA_NG_TRAIN","???"},
            {"VTFML","VT-FML"},
            {"MEFML","ME-FML"},
            {"WA_MILITARY","WA-MIL"},
            {"HIFML","HI-FML"},
            // {"MD_PDL","MD-PARENT???"},
            // {"ORILLCHILD","???"},
            {"KYADOPT","KY-ADOPT"},
            {"MN_MILITARY","MN-MIL"},
            // {"KSPDL","???"}
        };

        static void Main(string[] args)
        {
            AbsenceSoft.Common.Serializers.DateTimeSerializer.Register();
            log4net.Config.XmlConfigurator.Configure();

            string fileName = null, customerId = null, type = null;
            RecordType recordType = RecordType.Unknown;
            #region Parse Arguments

            for (var i = 0; i < args.Length; i++)
            {
                string a = args[i];
                if (a == "-f")
                    fileName = GetSwitchValue(args, i);
                else if (a == "-c")
                    customerId = GetSwitchValue(args, i);
                else if (a == "-t")
                    type = GetSwitchValue(args, i);
                else if (a.Contains("?") || a.Contains("help"))
                {
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine("Command Syntax:");
                    Console.WriteLine("    AbsenceSoft.Conversion.Absentys.exe -t <type> -c <customerId> -f <file path>");
                    Console.WriteLine();
                    Console.WriteLine("    -c <customerId>: Specifies the customerId");
                    Console.WriteLine("    -f <file path>: Specifies the file path for the requested operation (export to or import from)");
                    Console.WriteLine("    -t <type>: Specifies the file type to convert, one of the following:");
                    Console.WriteLine("       Leave - Leave records");
                    Console.WriteLine("       Policy - Policy records");
                    Console.WriteLine("       Reminder - Reminder records");
                    Console.WriteLine();
                    return;
                }
            }

            bool isValid = ValidateArguments(fileName, customerId, type, out recordType);
            if (!isValid)
                return;

            Customer = Customer.GetById(customerId);
            CustomerId = customerId;
            using (var fileService = new FileService(Customer, null, User.System))
            {
                switch (recordType)
                {
                    case RecordType.Leave:
                        LeavesToProcess = new Dictionary<string, LeaveRecord>();
                        fileService.ReadDelimitedFileByLine(fileName, ParseLeaveRecords);
                        ProcessLeaveRecords();
                        break;
                    case RecordType.Policy:
                        PoliciesToProcess = new List<PolicyRecord>();
                        fileService.ReadDelimitedFileByLine(fileName, ParsePolicyRecords);
                        ProcessPolicyRecords();
                        break;
                    case RecordType.Reminder:
                        RemindersToProcess = new List<ReminderRecord>();
                        fileService.ReadDelimitedFileByLine(fileName, ParseReminderRecords);
                        ProcessReminderRecords();
                        break;
                }

            }

            #endregion
        }

        /// <summary>
        /// Validates that an end user has provided all necessary arguments to load a file
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="customerId"></param>
        /// <param name="employerId"></param>
        /// <param name="type"></param>
        /// <param name="recordType"></param>
        /// <returns></returns>
        public static bool ValidateArguments(string fileName, string customerId, string type, out RecordType recordType)
        {
            bool isValid = true;
            if (string.IsNullOrEmpty(fileName))
            {
                Console.Error.WriteLine("Please provide a file name argument using the -f switch");
                isValid = false;
            }

            if (string.IsNullOrEmpty(customerId))
            {
                Console.Error.WriteLine("Please provide a customer id argument using the -c switch");
                isValid = false;
            }

            if (!Enum.TryParse(type, out recordType))
            {
                Console.Error.WriteLine("Please provide a valid file type argument using the -t switch");
                isValid = false;
            }

            return isValid;
        }

        #region Leave Records 
        /// <summary>
        /// Parses each leave record, validates it, and adds it to the dictionary of leaves to be processed if it is valid
        /// </summary>
        /// <param name="lineNumber"></param>
        /// <param name="parts"></param>
        /// <param name="delimiter"></param>
        /// <param name="lineText"></param>
        private static void ParseLeaveRecords(long lineNumber, string[] parts, char delimiter, string lineText)
        {
            LeaveRecord record = new LeaveRecord(parts);
            if (ValidateLeaveRecord(record, lineNumber))
            {
                LeavesToProcess.Add(record.LeaveNumber, record);
            }
        }

        /// <summary>
        /// Validates that a leave record should be processed
        /// </summary>
        /// <param name="record"></param>
        /// <param name="lineNumber"></param>
        /// <returns></returns>
        private static bool ValidateLeaveRecord(LeaveRecord record, long lineNumber)
        {
            bool isValid = ValidateLeaveNumber(record.LeaveNumber, lineNumber);
            isValid = isValid && ValidateBadgeNumber(record.BadgeNumber, record.LeaveNumber, lineNumber);
            isValid = isValid && ValidateStartDate(record.LeaveStart, record.LeaveNumber, lineNumber);
            isValid = isValid && ValidateEndDate(record.LeaveEnd, record.LeaveNumber, lineNumber);
            isValid = isValid && ValidateOpenClosed(record.OpenClosed, record.LeaveNumber, lineNumber);
            isValid = isValid && ValidateFullIntermittent(record.Intermittent, record.LeaveNumber, lineNumber);
            isValid = isValid && ValidateReasonCode(record.ReasonCode, record.LeaveNumber, lineNumber);

            return isValid;
        }

        public static bool ValidateLeaveNumber(string leaveNumber, long lineNumber)
        {
            if (LeavesToProcess.ContainsKey(leaveNumber))
            {
                Console.Error.WriteLine("ERROR: Case Number {0} on line {1} is a duplicate.  This record will not be processed.", leaveNumber, lineNumber);
                return false;
            }

            return true;
        }

        public static bool ValidateBadgeNumber(string badgeNumber, string leaveNumber, long lineNumber)
        {
            if (string.IsNullOrEmpty(badgeNumber))
            {
                Console.Error.WriteLine("ERROR: Case Number {0} on line {1} is missing the employee badge number.  This record will not be processed.", leaveNumber, lineNumber);
                return false;
            }

            return true;
        }

        public static bool ValidateStartDate(DateTime? startDate, string leaveNumber, long lineNumber)
        {
            if (!startDate.HasValue)
            {
                Console.Error.WriteLine("ERROR: Case Number {0} on line {1} does not have a start date.  This record will not be processed.", leaveNumber, lineNumber);
                return false;
            }

            return true;
        }

        public static bool ValidateEndDate(DateTime? endDate, string leaveNumber, long lineNumber)
        {
            if (!endDate.HasValue)
            {
                Console.Error.WriteLine("ERROR: Case Number {0} on line {1} does not have an end date.  This record will not be processed.", leaveNumber, lineNumber);
                return false;
            }

            return true;
        }

        public static bool ValidateOpenClosed(string openClosed, string leaveNumber, long lineNumber)
        {
            string caseStatus = openClosed.ToUpper();
            if (caseStatus != "C" && caseStatus != "O")
            {
                Console.Error.WriteLine("ERROR: Case Number {0} on line {1} does not have a known value for open or closed.  This record will not be processed.", leaveNumber, lineNumber);
                return false;
            }

            return true;
        }

        public static bool ValidateFullIntermittent(string intermittent, string leaveNumber, long lineNumber)
        {
            string caseType = intermittent.ToUpper();
            if (caseType != "FULL" && caseType != "INTERMITTENT")
            {
                Console.Error.WriteLine("ERROR: Case Number {0} on line {1} does not have a known value for intermittent or full.  This record will not be processed.", leaveNumber, lineNumber);
                return false;
            }

            return true;
        }

        public static bool ValidateReasonCode(string reasonCode, string leaveNumber, long lineNumber)
        {
            reasonCode = reasonCode.Trim();
            if (!ReasonMap.ContainsKey(reasonCode))
            {
                Console.Error.WriteLine("ERROR: Case Number {0} on line {1} does not have a known value for reason code.  This record will not be processed.", leaveNumber, lineNumber);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Processes all records that exist in memory
        /// </summary>
        private static void ProcessLeaveRecords()
        {
            Console.WriteLine("Beginning Leave Record Processing");
            List<string> existingCaseNumbers = CheckForExistingCases();
            Dictionary<string, Employee> employees = GetEmployeesToProcess(LeavesToProcess.Select(ltp => ltp.Value.BadgeNumber).ToList());
            Dictionary<string, AbsenceReason> reasons = GetReasonsToProcess();
            Dictionary<string, Employer> employers = GetEmployersToProcess();

            using (var caseService = new CaseService())
            {
                var validLeaves = LeavesToProcess.Where(ltp => LeaveRecordIsValid(ltp.Value, existingCaseNumbers, employees, reasons, employers)).Select(ltp => ltp.Value).ToList();
                Console.WriteLine("Found {0} valid leaves to process", validLeaves.Count());
                foreach (var leave in validLeaves)
                {
                    try
                    {
                        Console.WriteLine("Processing Leave Record for Case {0}", leave.LeaveNumber);
                        Employer employer = employers[leave.ReferenceId];
                        string employeeReferenceCode = BuildEmployeeDictionaryKey(employer.ReferenceCode, leave.BadgeNumber);
                        Case newCase = new Case()
                        {
                            CustomerId = CustomerId,
                            Employer = employer,
                            Employee = employees[employeeReferenceCode],
                            CreatedById = User.System.Id,
                            ModifiedById = User.System.Id,
                            CaseNumber = leave.LeaveNumber,
                            EmployerCaseNumber = leave.LeaveNumber,
                            Description = "Migrated to AbsenceTracker from Absentys",
                            StartDate = leave.LeaveStart.Value,
                            EndDate = leave.LeaveEnd,
                            Reason = reasons[MapReasonCode(leave.ReasonCode)]
                        };

                        SetCaseEvents(leave, newCase);
                        AddCaseSegment(newCase, leave);

                        newCase = caseService.UpdateCase(newCase, CaseEventType.CaseCreated);
                        caseService.EvaluateCaseAssignmentRules(newCase);
                    }
                    catch (Exception ex)
                    {
                        // If there are any errors, log it and move on.  Not much else we can do at this point.
                        Console.WriteLine("ERROR: Leave Record for Case {0} was unable to process. Error was {1}", leave.LeaveNumber, ex.Message.ToLower());
                        Console.WriteLine("STACKTRACE: {0}", ex.StackTrace);

                        continue;
                    }
                }
            }

            Console.WriteLine("Finished Leave Record Processing");
        }

        /// <summary>
        /// Creates the case events based on the leave record
        /// </summary>
        /// <param name="leaveRecord"></param>
        /// <param name="newCase"></param>
        private static void SetCaseEvents(LeaveRecord leaveRecord, Case newCase)
        {
            DateTime createdDate = leaveRecord.CreatedDate.HasValue ? leaveRecord.CreatedDate.Value : DateTime.UtcNow.ToMidnight();
            newCase.SetCaseEvent(CaseEventType.CaseCreated, createdDate);
            newCase.Status = GetCaseStatus(leaveRecord.OpenClosed);
            if (leaveRecord.FinalRtw.HasValue)
                newCase.SetCaseEvent(CaseEventType.ReturnToWork, leaveRecord.FinalRtw.Value);

            if (leaveRecord.ExpectedRtwDate.HasValue)
                newCase.SetCaseEvent(CaseEventType.EstimatedReturnToWork, leaveRecord.ExpectedRtwDate.Value);

            if (leaveRecord.WorkRelated.HasValue)
                newCase.Metadata.SetRawValue<bool>("IsWorkRelated", leaveRecord.WorkRelated.Value);
        }

        /// <summary>
        /// Checks if any of the cases we want to process already exist in the database
        /// </summary>
        /// <returns></returns>
        private static List<string> CheckForExistingCases()
        {
            List<string> caseNumbersToProcess = LeavesToProcess.Select(c => c.Key).ToList();

            List<Case> existingCases = Case.AsQueryable().Where(c => c.CustomerId == CustomerId
                && caseNumbersToProcess.Contains(c.CaseNumber)).ToList();

            List<string> duplicateCaseNumbers = existingCases.Select(c => c.CaseNumber).ToList();

            foreach (Case theCase in existingCases)
            {
                Console.Error.WriteLine("ERROR: Case Number(s) {0} already exist.  Please use AbsenceSoft to modify the existing case.", theCase.CaseNumber);
            }

            return duplicateCaseNumbers;
        }

        /// <summary>
        /// Gets the employees we want to process, and alerts if any of them do not exist in the system
        /// </summary>
        /// <returns></returns>
        private static Dictionary<string, Employee> GetEmployeesToProcess(List<string> employeeNumbers)
        {
            employeeNumbers = employeeNumbers.Where(e => !string.IsNullOrEmpty(e)).Distinct().ToList();

            List<Employee> allEmployees = Employee.AsQueryable().Where(e =>
                e.CustomerId == CustomerId
                && employeeNumbers.Contains(e.EmployeeNumber)).ToList();


            Dictionary<string, Employer> employers = Employer.AsQueryable().Where(e => e.CustomerId == CustomerId).ToList()
                .ToDictionary(e => e.Id);
            var employeeDictionary = allEmployees.ToDictionary(e => BuildEmployeeDictionaryKey(employers[e.EmployerId].ReferenceCode, e.EmployeeNumber));

            /// If we don't have a key for an employee number that we want to process, then we were provided an employee number that doesn't exist in the database
            List<string> missingEmployeeNumbers = employeeNumbers.Where(e => !allEmployees.Any(a => a.EmployeeNumber == e)).ToList();
            foreach (string employeeNumber in missingEmployeeNumbers)
            {
                Console.Error.WriteLine("ERROR: Employee Number(s) {0} were not found in the database. Records for this employee number will not be processed", employeeNumber);
            }

            return employeeDictionary;
        }

        /// <summary>
        /// Gets all the absence reasons that we're going to add to at least one case
        /// </summary>
        /// <returns></returns>
        private static Dictionary<string, AbsenceReason> GetReasonsToProcess()
        {
            List<string> reasonCodes = LeavesToProcess.Select(p => MapReasonCode(p.Value.ReasonCode)).Distinct().ToList();
            IMongoQuery reasonMatchesCode = AbsenceReason.Query.And(AbsenceReason.Query.In(p => p.Code, reasonCodes));
            return AbsenceReason.DistinctFind(reasonMatchesCode, CustomerId, null, true).ToList().ToDictionary(p => p.Code);
        }


        /// <summary>
        /// Gets all the employers that we're going to add at least one case to
        /// </summary>
        /// <returns></returns>
        private static Dictionary<string, Employer> GetEmployersToProcess()
        {
            List<string> employerReferenceCodes = LeavesToProcess.Select(p => p.Value.ReferenceId).Distinct().ToList();
            Dictionary<string, Employer> employers = Employer.AsQueryable()
                .Where(e => e.CustomerId == CustomerId && employerReferenceCodes.Contains(e.ReferenceCode))
                .ToDictionary(e => e.ReferenceCode);
            List<string> missingReferenceCodes = employerReferenceCodes.Where(e => !employers.ContainsKey(e)).ToList();
            foreach (string referenceCode in missingReferenceCodes)
            {
                Console.Error.WriteLine("ERROR: Reference Code {0} was not found in the database. Records for this employer will not be processed", referenceCode);
            }

            return employers;
        }

        /// <summary>
        /// Checks the validity of a leave record against the values to process
        /// </summary>
        /// <param name="record">The record to check validity on</param>
        /// <param name="existingCaseNumbers">List of case numbers that already exist in the database</param>
        /// <param name="employees">The employees that we want to create records for</param>
        /// <returns></returns>
        private static bool LeaveRecordIsValid(LeaveRecord record, List<string> existingCaseNumbers, Dictionary<string, Employee> employees, Dictionary<string, AbsenceReason> reasons, Dictionary<string, Employer> employers)
        {
            ///If it already exists in the database, the record isn't valid
            if (existingCaseNumbers.Contains(record.LeaveNumber))
                return false;


            ///If we don't have a record for this employee that we want to process, the record isn't valid
            if (!employees.ContainsKey(BuildEmployeeDictionaryKey(record.ReferenceId, record.BadgeNumber)))
                return false;


            ///If we don't have a matching key for our existing reason code, the record isn't valid
            if (!reasons.ContainsKey(MapReasonCode(record.ReasonCode)))
                return false;


            ///If we don't have a matching employer for this leave record, it's not valid
            if (!employers.ContainsKey(record.ReferenceId))
                return false;


            /// All check pass, record valid
            return true;
        }

        /// <summary>
        /// Adds a case segment to the case based off of the leave record
        /// </summary>
        /// <param name="newCase"></param>
        /// <param name="leaveRecord"></param>
        private static void AddCaseSegment(Case newCase, LeaveRecord leaveRecord)
        {
            CaseType caseType = GetCaseType(leaveRecord.Intermittent);
            CaseSegment newSegment = new CaseSegment()
            {
                Type = caseType,
                Status = newCase.Status,
                StartDate = newCase.StartDate,
                EndDate = newCase.EndDate
            };
            newCase.Segments.Add(newSegment);
        }

        /// <summary>
        /// Maps an Absentys Reason Code to AbsenceSoft Reason Code
        /// </summary>
        /// <param name="reasonCode"></param>
        /// <returns></returns>
        private static string MapReasonCode(string reasonCode)
        {
            reasonCode = reasonCode.Trim();
            if (ReasonMap.ContainsKey(reasonCode))
                return ReasonMap[reasonCode];

            throw new ArgumentOutOfRangeException("Policy Nickname is not mapped to an AbsenceSoft policy code");
        }

        /// <summary>
        /// Converts an Absentys Intermittent string to one of our CaseType enums
        /// </summary>
        /// <param name="caseType"></param>
        /// <returns></returns>
        private static CaseType GetCaseType(string caseType)
        {
            caseType = caseType.ToUpper();
            if (caseType == "FULL")
                return CaseType.Consecutive;

            if (caseType == "INTERMITTENT")
                return CaseType.Intermittent;

            throw new ArgumentOutOfRangeException("Leave Record does not have a known case type value");
        }

        /// <summary>
        /// Converts a Absentys OpenClosed string to one of our CaseStatus enums
        /// </summary>
        /// <param name="caseStatus"></param>
        /// <returns></returns>
        private static CaseStatus GetCaseStatus(string caseStatus)
        {
            caseStatus = caseStatus.ToUpper();
            if (caseStatus == "C")
                return CaseStatus.Closed;

            if (caseStatus == "O")
                return CaseStatus.Open;

            throw new ArgumentOutOfRangeException("Leave Record does not have a known open closed value");
        }

        #endregion


        #region Policy Records
        /// <summary>
        /// Validates policy record and adds it to dictionary for processing
        /// </summary>
        /// <param name="lineNumber"></param>
        /// <param name="parts"></param>
        /// <param name="delimiter"></param>
        /// <param name="lineText"></param>
        private static void ParsePolicyRecords(long lineNumber, string[] parts, char delimiter, string lineText)
        {
            PolicyRecord record = new PolicyRecord(parts);
            if (ValidatePolicyRecord(record, lineNumber))
            {
                PoliciesToProcess.Add(record);
            }
        }

        /// <summary>
        /// Checks if a specific policy record is valid
        /// </summary>
        /// <param name="record"></param>
        /// <param name="lineNumber"></param>
        /// <returns></returns>
        private static bool ValidatePolicyRecord(PolicyRecord record, long lineNumber)
        {
            bool isValid = true;
            if (string.IsNullOrEmpty(record.LeaveNumber))
            {
                Console.Error.WriteLine("ERROR: Line {0} does not have a case number.  This record will not be processed", lineNumber);
                isValid = false;
            }

            string policyNickname = record.PolicyNickname.Trim();
            if (!PolicyMap.ContainsKey(policyNickname))
            {
                Console.Error.WriteLine("ERROR: Case Number {0} on line {1} has a policy nickname {2} which is not mapped to an AbsenceSoft Policy.  This record will not be processed", record.LeaveNumber, lineNumber, policyNickname);
                isValid = false;
            }

            return isValid;
        }

        /// <summary>
        /// Process all the policy records
        /// </summary>
        private static void ProcessPolicyRecords()
        {
            Dictionary<string, Case> casesToAddPoliciesFor = GetCasesToProcess();
            List<Policy> policiesToAdd = GetPoliciesToAdd();
            Dictionary<string, DiagnosisCode> diagnosisToAdd = GetDiagnosisToAdd();
            using (var eligibilityService = new EligibilityService())
            {
                Console.WriteLine("Beginning Policy Record Processing");
                // Only add policies to cases that exist in the db
                foreach (var policy in PoliciesToProcess.Where(p => casesToAddPoliciesFor.ContainsKey(p.LeaveNumber)))
                {
                    try
                    {
                        var theCase = casesToAddPoliciesFor[policy.LeaveNumber];
                        var policyToAdd = GetPolicyToAdd(policy, policiesToAdd, theCase);
                        if (policyToAdd == null)
                            continue;

                        string policyCode = MapPolicyNickname(policy.PolicyNickname);
                        theCase = eligibilityService.AddManualPolicy(theCase, policyToAdd, "Manually added by migration process from Absentys.");
                        Console.WriteLine("INFO: Policy {0} manually added to Case {1}.", policy.PolicyNickname, policy.LeaveNumber);
                        theCase = ApplyPolicyDetermination(policy, theCase, policyCode);
                        Console.WriteLine("INFO: Policy {0} determination updated for Case {1}.", policy.PolicyNickname, policy.LeaveNumber);
                        theCase = ApplyMedicalDiagnosis(policy, theCase, diagnosisToAdd);
                        Console.WriteLine("INFO: Diagnosis added to Case {0}.", policy.LeaveNumber);
                        theCase = ApplyCommunicationMetadata(policy, theCase, policyCode);
                        Console.WriteLine("INFO: Comms metadata added to Case {0}.", policy.LeaveNumber);
                        casesToAddPoliciesFor[policy.LeaveNumber] = theCase;
                    }
                    catch (Exception ex)
                    {
                        // If there are any errors, log it and move on.  Not much else we can do at this point.
                        Console.WriteLine("ERROR: Policy Record {0} for Case {1} was unable to process. Error was {2}", policy.PolicyNickname, policy.LeaveNumber, ex.Message.ToLower());
                        Console.WriteLine("STACKTRACE: {0}", ex.StackTrace);
                        continue;
                    }

                }
            }


            // Now that we're done adding all policies, go ahead and run calcs on every case
            using (var caseService = new CaseService())
            {
                foreach (var updatedCase in casesToAddPoliciesFor)
                {
                    var theCase = updatedCase.Value;
                    try
                    {
                        theCase = caseService.RunCalcs(theCase);
                        theCase = caseService.UpdateCase(theCase);
                        Console.WriteLine("INFO: Case {0} calculations completed.", theCase.CaseNumber);
                    }
                    catch (Exception ex)
                    {
                        // If there are any errors, log it and move on.  Not much else we can do at this point.
                        Console.WriteLine("ERROR: Case {0} was unable to run calcs. Error was {1}", theCase.CaseNumber, ex.Message.ToLower());
                        Console.WriteLine("STACKTRACE: {0}", ex.StackTrace);
                        continue;
                    }
                }
            }


            Console.WriteLine("Finished Policy Record Processing");
        }

        private static Policy GetPolicyToAdd(PolicyRecord policy, List<Policy> policies, Case theCase)
        {
            List<string> currentPolicies = theCase.Summary.Policies.Select(c => c.PolicyCode).ToList();
            string policyCode = MapPolicyNickname(policy.PolicyNickname);
            if (currentPolicies.Contains(policyCode))
            {
                Console.Error.WriteLine("ERROR: Policy already exists for {0} on case {1}.  This policy record will not be processed", policy.PolicyNickname, policy.LeaveNumber);
                return null;
            }

            var matchingPolicy = policies
                .Where(p => p.Code == policyCode)
                .OrderBy(p => p.Code)
                .ThenBy(p =>
                {
                    if (!string.IsNullOrWhiteSpace(p.EmployerId))
                        return 1;
                    if (!string.IsNullOrWhiteSpace(p.CustomerId))
                        return 2;
                    return 3;
                }).FirstOrDefault();

            if (matchingPolicy == null)
            {
                Console.Error.WriteLine("ERROR: Unable to find matching policy for {0} on case {1}.  This policy record will not be processed", policy.PolicyNickname, policy.LeaveNumber);
                return null;
            }

            string reasonCode = theCase.Reason.Code;
            if(!matchingPolicy.AbsenceReasons.Any(ar => ar.Reason.Code == reasonCode))
            {
                Console.Error.WriteLine("ERROR: Case {0} has reason {1} and no reasons on the policy {2} match this reason.  This policy record will not be processed", policy.LeaveNumber, reasonCode, policy.PolicyNickname);
                return null;
            }

            return matchingPolicy;
        }

        /// <summary>
        /// Applies to policy determination to the case
        /// </summary>
        /// <param name="policyRecord"></param>
        /// <param name="theCase"></param>
        /// <param name="policyCode"></param>
        private static Case ApplyPolicyDetermination(PolicyRecord policyRecord, Case theCase, string policyCode)
        {
            using (var caseService = new CaseService())
            {
                DateTime policyStartDate = policyRecord.LeaveStart ?? theCase.StartDate;
                DateTime policyEndDate = policyRecord.LeaveEnd ?? theCase.EndDate.Value;
                AdjudicationStatus policyStatus = GetPolicyStatus(policyRecord.PolicyStatus);
                if (policyStatus == AdjudicationStatus.Denied)
                {
                    string denialReason = policyRecord.DenialReason;
                    string denialReasonOther = policyRecord.DenialReason1;
                    if (string.IsNullOrEmpty(denialReason))
                        denialReason = "No Denial reason was supplied";

                    theCase = caseService.ApplyDetermination(theCase, policyCode, policyStartDate, policyEndDate, policyStatus, AdjudicationDenialReason.Other, denialReasonOther, denialReason);
                }
                else
                {
                    theCase = caseService.ApplyDetermination(theCase, policyCode, policyStartDate, policyEndDate, policyStatus);
                }
            }

            return theCase;

        }

        /// <summary>
        /// Applies the medical diagnosis if they passed one in, and we know what it is from our database
        /// </summary>
        /// <param name="policyRecord"></param>
        /// <param name="theCase"></param>
        /// <param name="diagnosisToAdd"></param>
        private static Case ApplyMedicalDiagnosis(PolicyRecord policyRecord, Case theCase, Dictionary<string, DiagnosisCode> diagnosisToAdd)
        {
            if (!string.IsNullOrEmpty(policyRecord.ICD9Code) && diagnosisToAdd.ContainsKey(policyRecord.ICD9Code))
            {
                theCase.Disability = new DisabilityInfo()
                {
                    PrimaryDiagnosis = diagnosisToAdd[policyRecord.ICD9Code]
                };
            }

            return theCase;
        }

        /// <summary>
        /// Storing dates communication was sent in metadata value, since we don't have it mapped to our communications
        /// </summary>
        /// <param name="policyRecord"></param>
        /// <param name="theCase"></param>
        private static Case ApplyCommunicationMetadata(PolicyRecord policyRecord, Case theCase, string policyCode)
        {
            theCase = ApplyDateTimeCommunicationMetadata(policyRecord.InitialPacket, string.Format("InitialPacketSent-{0}", policyCode), theCase);
            theCase = ApplyDateTimeCommunicationMetadata(policyRecord.CertDue, string.Format("CertificationDue-{0}", policyCode), theCase);
            theCase = ApplyDateTimeCommunicationMetadata(policyRecord.CertReceived, string.Format("CertificationReceived-{0}", policyCode), theCase);
            theCase = ApplyDateTimeCommunicationMetadata(policyRecord.ApprovalLetter, string.Format("ApprovalLetterReceived-{0}", policyCode), theCase);

            return theCase;
        }

        /// <summary>
        /// Puts a date time value in the case metadata
        /// </summary>
        /// <param name="dateTimeCommunicationSent"></param>
        /// <param name="metadataName"></param>
        /// <param name="theCase"></param>
        private static Case ApplyDateTimeCommunicationMetadata(DateTime? dateTimeCommunicationSent, string metadataName, Case theCase)
        {
            if (dateTimeCommunicationSent.HasValue)
                theCase.Metadata.SetRawValue(metadataName, dateTimeCommunicationSent.Value);

            return theCase;
        }


        /// <summary>
        /// Gets all the cases to process for all the policy records
        /// </summary>
        /// <returns></returns>
        private static Dictionary<string, Case> GetCasesToProcess()
        {
            List<string> caseNumbersToProcess = PoliciesToProcess.Select(p => p.LeaveNumber).Distinct().ToList();
            Dictionary<string, Case> casesToProcess = Case.AsQueryable().Where(c => c.CustomerId == CustomerId
            && caseNumbersToProcess.Contains(c.CaseNumber)).ToList().ToDictionary(c => c.CaseNumber);
            List<string> missingCaseNumbers = caseNumbersToProcess.Where(c => !casesToProcess.ContainsKey(c)).ToList();
            foreach (string caseNumber in missingCaseNumbers)
            {
                Console.Error.WriteLine("ERROR: Case Number {0} does not exist in the database. Policy records for this case will not be processed.", caseNumber);
            }

            return casesToProcess;
        }

        /// <summary>
        /// Gets copies of all of our policies that we want to add to the cases
        /// </summary>
        /// <returns></returns>
        private static List<Policy> GetPoliciesToAdd()
        {
            List<string> policyCodes = PoliciesToProcess.Select(p => MapPolicyNickname(p.PolicyNickname)).Distinct().ToList();
            return Policy.AsQueryable().Where(p => (p.CustomerId == CustomerId || p.CustomerId == null) && policyCodes.Contains(p.Code)).ToList();
        }

        /// <summary>
        /// Gets copies of all the diagnosis that we want to add to the cases
        /// </summary>
        /// <returns></returns>
        private static Dictionary<string, DiagnosisCode> GetDiagnosisToAdd()
        {
            List<string> icd9Codes = PoliciesToProcess.Select(p => p.ICD9Code).Where(code => !string.IsNullOrEmpty(code)).Distinct().ToList();
            var diagnosis = DiagnosisCode.AsQueryable()
                .Where(dc => icd9Codes.Contains(dc.Code) && dc.CodeType == DiagnosisType.ICD9)
                .ToList().ToDictionary(dc => dc.Code);

            List<string> missingIcd9Codes = icd9Codes.Where(d => !diagnosis.ContainsKey(d)).ToList();
            foreach (var icd9Code in missingIcd9Codes)
            {
                Console.Error.WriteLine("ERROR: Diagnosis Code {0} does not exist in the database. This diagnosis will not be added onto the case.", icd9Code);
            }

            return diagnosis;
        }

        /// <summary>
        /// Converts an Absentys Policy Status to an AbsenceSoft Adjudication Status enum
        /// </summary>
        /// <param name="policyStatus"></param>
        /// <returns></returns>
        private static AdjudicationStatus GetPolicyStatus(string policyStatus)
        {
            policyStatus = policyStatus.ToUpper();
            if (policyStatus == "APPROVED")
                return AdjudicationStatus.Approved;

            if (policyStatus == "DENIED")
                return AdjudicationStatus.Denied;

            return AdjudicationStatus.Pending;
        }

        /// <summary>
        /// Maps an Absentys Policy Nickname to AbsenceSoft Policy Code
        /// </summary>
        /// <param name="policyNickName"></param>
        /// <returns></returns>
        private static string MapPolicyNickname(string policyNickName)
        {
            policyNickName = policyNickName.Trim();
            if (PolicyMap.ContainsKey(policyNickName))
                return PolicyMap[policyNickName];

            throw new ArgumentOutOfRangeException("Policy Nickname is not mapped to an AbsenceSoft policy code");
        }

        #endregion
        private static void ProcessCancelledRecords(long lineNumber, string[] parts, char delimiter, string lineText)
        {
            throw new NotImplementedException();
        }



        #region Reminder Records

        private static void ParseReminderRecords(long lineNumber, string[] parts, char delimiter, string lineText)
        {
            ReminderRecord record = new ReminderRecord(parts);
            if (ValidateReminderRecord(record, lineNumber))
            {
                RemindersToProcess.Add(record);
            }
        }

        private static void ProcessReminderRecords()
        {
            Console.WriteLine("Beginning Reminder Record Processing");
            var employees = GetEmployeesToProcess(RemindersToProcess.Select(rtp => rtp.BadgeNumber).ToList());
            var employeesCases = GetEmployeeCases();
            /// Only process reminders for employees that we know who they are
            foreach (var reminder in RemindersToProcess)
            {
                string employeeKey = BuildEmployeeDictionaryKey(reminder.ReferenceId, reminder.BadgeNumber);
                if (!employees.ContainsKey(employeeKey) || !employeesCases.ContainsKey(employeeKey))
                    continue;

                var employee = employees[employeeKey];
                var employeeCases = employeesCases[employeeKey];
                var matchingCase = GetMatchingCase(reminder, employeeCases);
                if (matchingCase != null)
                {
                    CreateToDo(reminder, matchingCase, employee);
                }
                else
                {
                    CreateEmployeeNote(reminder, employee);
                }
            }


            Console.WriteLine("Finished Reminder Record Processing");
        }

        private static void CreateToDo(ReminderRecord reminderRecord, Case matchingCase, Employee employee)
        {
            ToDoItem newToDo = new ToDoItem()
            {
                CustomerId = CustomerId,
                EmployerId = matchingCase.EmployerId,
                Title = string.Format("{0} - {1}", reminderRecord.Description, reminderRecord.Notes),
                ItemType = ToDoItemType.Manual,
                Priority = ToDoItemPriority.Normal,
                EmployeeId = employee.Id,
                EmployeeName = employee.FullName,
                EmployeeNumber = employee.EmployeeNumber,
                CreatedById = User.System.Id,
                AssignedToId = matchingCase.AssignedToId,
                AssignedToName = matchingCase.AssignedToName
            };

            newToDo.Save();
        }

        private static void CreateEmployeeNote(ReminderRecord reminderRecord, Employee employee)
        {
            EmployeeNote empNote = new EmployeeNote()
            {
                Public = true,
                Notes = string.Format("{0} - {1}", reminderRecord.Description, reminderRecord.Notes),
                EmployeeId = employee.Id,
                CustomerId = CustomerId,
                EmployerId = employee.EmployerId,
                Category = NoteCategoryEnum.Other
            };

            empNote.Save();
        }

        /// <summary>
        /// Validates a reminder record
        /// </summary>
        /// <param name="record"></param>
        /// <param name="lineNumber"></param>
        /// <returns></returns>
        private static bool ValidateReminderRecord(ReminderRecord record, long lineNumber)
        {
            bool isValid = true;
            if (string.IsNullOrEmpty(record.BadgeNumber))
            {
                Console.Error.WriteLine("ERROR: Line {0} does not have a badge number.  This record will not be processed", lineNumber);
                isValid = false;
            }

            if (string.IsNullOrEmpty(record.ReferenceId))
            {
                Console.Error.WriteLine("ERROR: Line {0} does not have an employer reference code.  This record will not be processed", lineNumber);
                isValid = false;
            }

            return isValid;
        }

        /// <summary>
        /// Gets all the cases for employees that we have reminders for
        /// </summary>
        /// <returns></returns>
        private static Dictionary<string, List<Case>> GetEmployeeCases()
        {
            var employers = Employer.AsQueryable().Where(e => e.CustomerId == CustomerId).ToList();

            List<string> employeeNumbersToGetCasesFor = RemindersToProcess.Select(rtp => rtp.BadgeNumber)
                .Where(en => !string.IsNullOrEmpty(en)).Distinct().ToList();

            return Case.AsQueryable().Where(c => employeeNumbersToGetCasesFor.Contains(c.Employee.EmployeeNumber)).ToList()
                .Where(c => employers.Any(e => e.Id == c.EmployerId))
                .GroupBy(c => BuildEmployeeDictionaryKey(employers.FirstOrDefault(e => e.Id == c.EmployerId).ReferenceCode, c.Employee.EmployeeNumber))
                .ToDictionary(c => c.Key, c => c.ToList());
        }

        /// <summary>
        /// Checks if any case in a list matches the reminder record
        /// </summary>
        /// <param name="reminder"></param>
        /// <param name="cases"></param>
        /// <returns></returns>
        private static Case GetMatchingCase(ReminderRecord reminder, List<Case> cases)
        {
            if (!reminder.ReminderDate.HasValue && reminder.CreatedDate.HasValue)
                return null;

            /// Look for cases that match
            Case matchingCase = MatchCaseAgainstDate(reminder.ReminderDate, cases);
            if (matchingCase == null)
                matchingCase = MatchCaseAgainstDate(reminder.CreatedDate, cases);

            return matchingCase;
        }

        /// <summary>
        /// Checks if any case in a list matches a specific date
        /// </summary>
        /// <param name="date"></param>
        /// <param name="cases"></param>
        /// <returns></returns>
        private static Case MatchCaseAgainstDate(DateTime? date, List<Case> cases)
        {
            /// If the date doesn't have a value, we can't find anything to match
            if (!date.HasValue)
                return null;

            DateTime dateToCompare = date.Value;
            /// Put most recent cases first
            cases = cases.OrderByDescending(c => c.CreatedDate).ToList();
            /// Check for a case that's in the date range and open
            Case matchingCase = cases.FirstOrDefault(c => dateToCompare.DateInRange(c.StartDate, c.EndDate) && c.Status == CaseStatus.Open);

            /// Didn't find one, check for a case that's closed
            if (matchingCase == null)
                matchingCase = cases.FirstOrDefault(c => dateToCompare.DateInRange(c.StartDate, c.EndDate) && c.Status == CaseStatus.Closed);

            /// Whether we found anything or not, return it
            return matchingCase;
        }


        #endregion

        /// <summary>
        /// Gets the switch value.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        static string GetSwitchValue(string[] args, int index)
        {
            return args.Length > index + 1 ? args[index + 1] : null;
        }

        private static string BuildEmployeeDictionaryKey(string employerReferenceCode, string employeeNumber)
        {
            return string.Format("{0}-{1}", employerReferenceCode, employeeNumber);
        }
    }
}
