﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Conversion.Absentys
{
    /// <summary>
    /// Represents a cancelled case or leave of absence to be converted.
    /// </summary>
    public class CancelledRecord : LeaveRecord
    {
        /// <summary>
        /// Gets or sets the cancelled date.
        /// </summary>
        /// <value>
        /// The cancelled date.
        /// </value>
        public DateTime? CancelledDate { get; set; }

        /// <summary>
        /// Parse the stored row source.
        /// </summary>
        protected override void _Parse()
        {
            base._Parse();
            CancelledDate = ParseDate(FieldIndex.status_date, "leave_cancel_date");
            ExpectedRtwDate = null;
            OverallStatus = null;
            StatusDate = null;
        }
    }
}
