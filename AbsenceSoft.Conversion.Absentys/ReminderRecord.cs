﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Conversion.Absentys
{
    /// <summary>
    /// Represents a reminder from Absentys, generally maps to a ToDoItem in AbsenceTracker. Should create or map to
    /// a manual ToDoItem type for the specified case and store the original reference id in meta-data to capture on re-runs.
    /// </summary>
    public class ReminderRecord : BaseAbsentysRecord
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReminderRecord"/> class.
        /// </summary>
        public ReminderRecord() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReminderRecord"/> class.
        /// </summary>
        /// <param name="rowSource">The row source.</param>
        public ReminderRecord(string[] rowSource) : base(rowSource) { }
        
        /// <summary>
        /// Gets or sets the reference identifier (reference_id).
        /// </summary>
        /// <value>
        /// The reference identifier.
        /// </value>
        public string ReferenceId { get; set; }

        /// <summary>
        /// Gets or sets the employee SSN (employee_ssn).
        /// </summary>
        /// <value>
        /// The employee SSN.
        /// </value>
        public string EmployeeSsn { get; set; }

        /// <summary>
        /// Gets or sets the badge number (badge_no).
        /// </summary>
        /// <value>
        /// The badge number.
        /// </value>
        public string BadgeNumber { get; set; }

        /// <summary>
        /// Gets or sets the staff from identifier (staff_from_id).
        /// </summary>
        /// <value>
        /// The staff from identifier.
        /// </value>
        public string StaffFromId { get; set; }

        /// <summary>
        /// Gets or sets the staff to identifier (staff_to_id).
        /// </summary>
        /// <value>
        /// The staff from identifier.
        /// </value>
        public string StaffToId { get; set; }

        /// <summary>
        /// Gets or sets the reminder created (reminder_created).
        /// </summary>
        /// <value>
        /// The reminder created.
        /// </value>
        public DateTime? ReminderCreated { get; set; }

        /// <summary>
        /// Gets or sets the reminder date (reminder_date).
        /// </summary>
        /// <value>
        /// The reminder date.
        /// </value>
        public DateTime? ReminderDate { get; set; }

        /// <summary>
        /// Gets or sets the completed date (reminder_completed).
        /// </summary>
        /// <value>
        /// The completed date.
        /// </value>
        public DateTime? CompletedDate { get; set; }

        /// <summary>
        /// Gets or sets the description (reminder_desc).
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the urgency (urgency).
        /// </summary>
        /// <value>
        /// The urgency.
        /// </value>
        public string Urgency { get; set; }

        /// <summary>
        /// Gets or sets the notes (reminder_notes).
        /// </summary>
        /// <value>
        /// The notes.
        /// </value>
        public string Notes { get; set; }

        /// <summary>
        /// Gets or sets the created by (created_by).
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the created date (created_date).
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the modified date (modified_date).
        /// </summary>
        /// <value>
        /// The modified date.
        /// </value>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Encapsulates the const field indexes for parsing this record type from the Absentys export files.
        /// </summary>
        protected class FieldIndex
        {
            public const int reference_id = 0;
            public const int employee_ssn = 1;
            public const int badge_no = 2;
            public const int staff_from_id = 3;
            public const int staff_to_id = 4;
            public const int reminder_created = 5;
            public const int reminder_date = 6;
            public const int reminder_completed = 7;
            public const int reminder_desc = 8;
            public const int urgency = 9;
            public const int reminder_notes = 10;
            public const int created_by = 11;
            public const int created_date = 12;
            public const int modified_date = 13;
        }

        /// <summary>
        /// Parse the stored row source.
        /// </summary>
        protected override void _Parse()
        {
            ReferenceId = Normalize(FieldIndex.reference_id);
            EmployeeSsn = Normalize(FieldIndex.employee_ssn);
            BadgeNumber = Normalize(FieldIndex.badge_no);
            StaffFromId = Normalize(FieldIndex.staff_from_id);
            StaffToId = Normalize(FieldIndex.staff_to_id);
            ReminderCreated = ParseDate(FieldIndex.reminder_created, "reminder_created");
            ReminderDate = ParseDate(FieldIndex.reminder_date, "reminder_date");
            CompletedDate = ParseDate(FieldIndex.reminder_completed, "reminder_completed");
            Description = Normalize(FieldIndex.reminder_desc);
            Urgency = Normalize(FieldIndex.urgency);
            Notes = Normalize(FieldIndex.reminder_notes);
            CreatedBy = Normalize(FieldIndex.created_by);
            CreatedDate = ParseDate(FieldIndex.created_date, "created_date");
            ModifiedDate = ParseDate(FieldIndex.modified_date, "modified_date");
        }
    }
}
