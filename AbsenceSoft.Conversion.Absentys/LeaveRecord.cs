﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Conversion.Absentys
{
    /// <summary>
    /// Represents a leave of absence or case to be converted.
    /// </summary>
    public class LeaveRecord : BaseAbsentysRecord
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeaveRecord"/> class.
        /// </summary>
        public LeaveRecord() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="LeaveRecord"/> class.
        /// </summary>
        /// <param name="rowSource">The row source.</param>
        public LeaveRecord(string[] rowSource) : base(rowSource) { }

        /// <summary>
        /// Gets or sets the leave number.
        /// </summary>
        /// <value>
        /// The leave number.
        /// </value>
        public string LeaveNumber { get; set; }

        /// <summary>
        /// Gets or sets the reference identifier.
        /// </summary>
        /// <value>
        /// The reference identifier.
        /// </value>
        public string ReferenceId { get; set; }

        /// <summary>
        /// Gets or sets the employee SSN.
        /// </summary>
        /// <value>
        /// The employee SSN.
        /// </value>
        public string EmployeeSsn { get; set; }

        /// <summary>
        /// Gets or sets the badge number.
        /// </summary>
        /// <value>
        /// The badge number.
        /// </value>
        public string BadgeNumber { get; set; }

        /// <summary>
        /// Gets or sets the staff identifier.
        /// </summary>
        /// <value>
        /// The staff identifier.
        /// </value>
        public string StaffId { get; set; }

        /// <summary>
        /// Gets or sets the date last worked.
        /// </summary>
        /// <value>
        /// The date last worked.
        /// </value>
        public DateTime? DateLastWorked { get; set; }

        /// <summary>
        /// Gets or sets the leave start.
        /// </summary>
        /// <value>
        /// The leave start.
        /// </value>
        public DateTime? LeaveStart { get; set; }

        /// <summary>
        /// Gets or sets the leave end.
        /// </summary>
        /// <value>
        /// The leave end.
        /// </value>
        public DateTime? LeaveEnd { get; set; }

        /// <summary>
        /// Gets or sets the days lost.
        /// </summary>
        /// <value>
        /// The days lost.
        /// </value>
        public int? DaysLost { get; set; }

        /// <summary>
        /// Gets or sets the request date.
        /// </summary>
        /// <value>
        /// The request date.
        /// </value>
        public DateTime? RequestDate { get; set; }

        /// <summary>
        /// Gets or sets the final RTW.
        /// </summary>
        /// <value>
        /// The final RTW.
        /// </value>
        public DateTime? FinalRtw { get; set; }

        /// <summary>
        /// Gets or sets the intermittent.
        /// </summary>
        /// <value>
        /// The intermittent.
        /// </value>
        public string Intermittent { get; set; }

        /// <summary>
        /// Gets or sets the work related.
        /// </summary>
        /// <value>
        /// The work related.
        /// </value>
        public bool? WorkRelated { get; set; }

        /// <summary>
        /// Gets or sets the fmla related.
        /// </summary>
        /// <value>
        /// The fmla related.
        /// </value>
        public bool? FmlaRelated { get; set; }

        /// <summary>
        /// Gets or sets the supervisor report.
        /// </summary>
        /// <value>
        /// The supervisor report.
        /// </value>
        public DateTime? SupervisorReport { get; set; }

        /// <summary>
        /// Gets or sets the report method.
        /// </summary>
        /// <value>
        /// The report method.
        /// </value>
        public string ReportMethod { get; set; }

        /// <summary>
        /// Gets or sets the RTW status.
        /// </summary>
        /// <value>
        /// The RTW status.
        /// </value>
        public string RtwStatus { get; set; }

        /// <summary>
        /// Gets or sets the RTW confirmed date.
        /// </summary>
        /// <value>
        /// The RTW confirmed date.
        /// </value>
        public DateTime? RtwConfirmedDate { get; set; }

        /// <summary>
        /// Gets or sets the suspicious absence.
        /// </summary>
        /// <value>
        /// The suspicious absence.
        /// </value>
        public string SuspiciousAbsence { get; set; }

        /// <summary>
        /// Gets or sets the memo.
        /// </summary>
        /// <value>
        /// The memo.
        /// </value>
        public string Memo { get; set; }

        /// <summary>
        /// Gets or sets the reason code.
        /// </summary>
        /// <value>
        /// The reason code.
        /// </value>
        public string ReasonCode { get; set; }

        /// <summary>
        /// Gets or sets the last work day missed.
        /// </summary>
        /// <value>
        /// The last work day missed.
        /// </value>
        public DateTime? LastWorkDayMissed { get; set; }

        /// <summary>
        /// Gets or sets the open closed.
        /// </summary>
        /// <value>
        /// The open closed.
        /// </value>
        public string OpenClosed { get; set; }

        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        /// <value>
        /// The modified date.
        /// </value>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the expected RTW date.
        /// </summary>
        /// <value>
        /// The expected RTW date.
        /// </value>
        public DateTime? ExpectedRtwDate { get; set; }

        /// <summary>
        /// Gets or sets the overall status.
        /// </summary>
        /// <value>
        /// The overall status.
        /// </value>
        public string OverallStatus { get; set; }

        /// <summary>
        /// Gets or sets the status date.
        /// </summary>
        /// <value>
        /// The status date.
        /// </value>
        public DateTime? StatusDate { get; set; }
        
        /// <summary>
        /// Encapsulates the const field indexes for parsing this record type from the Absentys export files.
        /// </summary>
        protected class FieldIndex
        {
            public const int leave_no = 0;
            public const int reference_id = 1;
            public const int employee_ssn = 2;
            public const int badge_no = 3;
            public const int staff_id = 4;
            public const int date_last_worked = 5;
            public const int leave_start = 6;
            public const int leave_end = 7;
            public const int days_lost = 8;
            public const int request_date = 9;
            public const int final_rtw = 10;
            public const int intermittent = 11;
            public const int work_related = 12;
            public const int fmla_related = 13;
            public const int supervisor_report = 14;
            public const int report_method = 15;
            public const int rtw_status = 16;
            public const int rtw_confirmed_date = 17;
            public const int suspicious_absence = 18;
            public const int memo = 19;
            public const int reason_code = 20;
            public const int last_work_day_missed = 21;
            public const int open_closed = 22;
            public const int created_by = 23;
            public const int created_date = 24;
            public const int modified_date = 25;
            public const int expected_rtw_date = 26;
            public const int overall_status = 27;
            public const int status_date = 28;
        }

        /// <summary>
        /// Parse the stored row source.
        /// </summary>
        protected override void _Parse()
        {
            LeaveNumber = Normalize(FieldIndex.leave_no);
            ReferenceId = Normalize(FieldIndex.reference_id);
            EmployeeSsn = Normalize(FieldIndex.employee_ssn);
            BadgeNumber = Normalize(FieldIndex.badge_no);
            StaffId = Normalize(FieldIndex.staff_id);
            DateLastWorked = ParseDate(FieldIndex.date_last_worked, "date_last_worked");
            LeaveStart = ParseDate(FieldIndex.leave_start, "leave_start");
            LeaveEnd = ParseDate(FieldIndex.leave_end, "leave_end");
            DaysLost = ParseInt(FieldIndex.days_lost, "days_lost");
            RequestDate = ParseDate(FieldIndex.request_date, "request_date");
            FinalRtw = ParseDate(FieldIndex.final_rtw, "final_rtw");
            Intermittent = Normalize(FieldIndex.intermittent);
            WorkRelated = ParseBool(FieldIndex.work_related, "work_related");
            FmlaRelated = ParseBool(FieldIndex.fmla_related, "fmla_related");
            SupervisorReport = ParseDate(FieldIndex.supervisor_report, "supervisor_report");
            ReportMethod = Normalize(FieldIndex.report_method);
            RtwStatus = Normalize(FieldIndex.rtw_status);
            RtwConfirmedDate = ParseDate(FieldIndex.rtw_confirmed_date, "rtw_confirmed_date");
            SuspiciousAbsence = Normalize(FieldIndex.suspicious_absence);
            Memo = Normalize(FieldIndex.memo);
            ReasonCode = Normalize(FieldIndex.reason_code);
            LastWorkDayMissed = ParseDate(FieldIndex.last_work_day_missed, "last_work_day_missed");
            OpenClosed = Normalize(FieldIndex.open_closed);
            CreatedBy = Normalize(FieldIndex.created_by);
            CreatedDate = ParseDate(FieldIndex.created_date, "created_date");
            ModifiedDate = ParseDate(FieldIndex.modified_date, "modified_date");
            ExpectedRtwDate = ParseDate(FieldIndex.expected_rtw_date, "expected_rtw_date");
            OverallStatus = Normalize(FieldIndex.overall_status);
            StatusDate = ParseDate(FieldIndex.status_date, "status_date");
        }
    }
}
