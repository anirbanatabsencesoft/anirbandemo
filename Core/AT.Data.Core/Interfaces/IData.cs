﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Data.Core.Interfaces
{
    /// <summary>
    /// Properties that most objects will have
    /// </summary>
    public interface IData
    {
        long? Id { get; set; }
        DateTime CreatedDate { get; set; }
        long CreatedById { get; set; }
        DateTime? ModifiedDate { get; set; }
        long? ModifiedById { get; set; }
    }
}
