﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Data.Core.Interfaces
{
    /// <summary>
    /// Data that is used for Configuration
    /// </summary>
    interface IConfigurationData
    {
        long CustomerId { get; set; }
        long EmployerId { get; set; }
        string Code { get; set; }
    }
}
