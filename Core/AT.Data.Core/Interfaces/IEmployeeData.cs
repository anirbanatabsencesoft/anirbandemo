﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Data.Core.Interfaces
{
    /// <summary>
    /// Data that can belong to a specific employee
    /// </summary>
    public interface IEmployeeData
    {
        long EmployeeId { get; set; }
    }
}
