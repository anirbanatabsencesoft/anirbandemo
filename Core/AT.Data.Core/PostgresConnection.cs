﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Data.Core
{
    public static class PostgresConnection
    {
        static PostgresConnection()
        {
            // Load the UserToCustomerMap and CustomerConnectionStrings
        }

        static Dictionary<string, string> CustomerConnectionStrings = new Dictionary<string, string>();
        static Dictionary<string, string> UserToCustomerMap = new Dictionary<string, string>();

        /// <summary>
        /// Creates a NpgsqlConnection that is specific to the customer who we want to access data for
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public static NpgsqlConnection CreateCustomerConnection(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
            {
                return CreateCoreConnection();
            }


            // For now, this method will just end up returning a core connection, 
            // but if we split things out into different databases between customers in the future
            // this method will enable us an easy way to get the correct connection string
            if (!UserToCustomerMap.ContainsKey(userId))
            {
                return CreateCoreConnection();
            }

            string customerId = UserToCustomerMap[userId];
            if (!CustomerConnectionStrings.ContainsKey(customerId))
            {
                return CreateCoreConnection();
            }

            return new NpgsqlConnection(CustomerConnectionStrings[customerId]);
        }

        /// <summary>
        /// Returns the connection for core data that would not be stored in a customer specific database
        /// </summary>
        /// <returns></returns>
        internal static NpgsqlConnection CreateCoreConnection()
        {
            return new NpgsqlConnection(ConfigurationManager.ConnectionStrings["AbsenceTracker"].ConnectionString);
        }
    }
}
