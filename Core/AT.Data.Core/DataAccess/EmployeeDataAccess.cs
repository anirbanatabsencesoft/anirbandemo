﻿using AT.Data.Core.Models;
using AT.Data.Core.Repositories;
using Insight.Database;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Data.Core.DataAccess
{
    internal class EmployeeDataAccess:BaseDataAccess
    {
        internal EmployeeDataAccess(string userId)
            :base(userId)
        {

        }

        internal async Task<Employee> UpdateEmployeeAsync(Employee employee)
        {
            NpgsqlConnection connection = PostgresConnection.CreateCustomerConnection(UserId);
            IEmployeeRepository repository = connection.AsParallel<IEmployeeRepository>();
            await repository.UpdateEmployeeAsync(employee, UserId);
            return employee;
        }
    }
}
