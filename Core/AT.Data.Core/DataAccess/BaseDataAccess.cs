﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Data.Core.DataAccess
{
    abstract internal class BaseDataAccess
    {
        internal string UserId { get; private set; }

        protected BaseDataAccess(string userId)
        {
            UserId = userId;
        }
    }
}
