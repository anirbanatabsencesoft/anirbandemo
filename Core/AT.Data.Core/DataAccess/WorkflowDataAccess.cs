﻿using AT.Data.Core.Models;
using AT.Data.Core.Repositories;
using Insight.Database;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Data.Core.DataAccess
{
    class WorkflowDataAccess:BaseDataAccess
    {
        public WorkflowDataAccess(string userId)
            :base(userId)
        {

        }

        public async Task<Workflow> UpdateWorkflowAsync(Workflow workflow)
        {
            NpgsqlConnection connection = PostgresConnection.CreateCoreConnection();
            IWorkflowRepository repository = connection.AsParallel<IWorkflowRepository>();
            await repository.UpdateWorkflowAsync(workflow, UserId);
            return workflow;
        }
    }
}
