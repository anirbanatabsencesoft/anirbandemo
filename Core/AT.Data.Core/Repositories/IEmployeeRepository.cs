﻿using AT.Data.Core.Models;
using Insight.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Data.Core.Repositories
{
    /// <summary>
    /// Employee repository to handle employee data access
    /// </summary>
    interface IEmployeeRepository
    {
        [Sql("update_employee")]
        Task UpdateEmployeeAsync(Employee employee, string userId);
    }
}
