﻿using AT.Data.Core.Models;
using Insight.Database;
using System.Threading.Tasks;

namespace AT.Data.Core.Repositories
{
    interface IWorkflowRepository
    {
        [Sql("update_workflow")]
        Task UpdateWorkflowAsync(Workflow workflow, string userId);
    }
}
