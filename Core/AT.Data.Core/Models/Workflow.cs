﻿using AT.Data.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Data.Core.Models
{
    class Workflow: IData, IConfigurationData
    {
        public long? Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedById { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedById { get; set; }

        public long CustomerId { get; set; }

        public long EmployerId { get; set; }

        public string Name { get; set; }
        public string Code { get; set; }
        
    }
}
