﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;

namespace AT.Core.PostgresSetup
{
    class Program
    {

        protected Program()
        {

        }

        static void Main(string[] args)
        {
            MergeSql();
        }

        /// <summary>
        /// This method will be called internally to merge all the SQLs developed so far.
        /// </summary>
        static void MergeSql()
        {
            //get sqldirs
            var sqlDir = Path.Combine(Directory.GetParent(Directory.GetParent(Directory.GetParent(Environment.CurrentDirectory).ToString()).ToString()).ToString(), ".sql");
            string[] devDirs = new string[] { "tables", "lookups", "views", "functions"};

            //declare location and name of output file
            string outputFile = Path.Combine(sqlDir, "deployment", "complete", "CompletePostgresScript.sql");

            //check if file exists or not. If then delete it.
            if (File.Exists(outputFile))
                File.Delete(outputFile);

            //loop
            foreach(var devDir in devDirs)
            {
                var files = Directory.GetFiles(Path.Combine(sqlDir, "development", devDir), "*.sql", SearchOption.AllDirectories);
                foreach(var file in files)
                {
                    File.AppendAllText(outputFile, string.Concat(Environment.NewLine, Environment.NewLine, "--------------", "File: ", Path.GetFileName(file), "--------------", Environment.NewLine));
                    File.AppendAllText(outputFile, File.ReadAllText(file));
                }
            }

            Console.WriteLine("Code is generated");
        }
    }
}
