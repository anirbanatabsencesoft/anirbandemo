﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AT.Common.Core
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// Extension for XML serialization for any object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="toSerialize"></param>
        /// <returns></returns>
        public static string SerializeToXML<T>(this T toSerialize)
        {
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                StringWriter textWriter = new StringWriter();
                xmlSerializer.Serialize(textWriter, toSerialize);
                return textWriter.ToString();
            }
            catch { return string.Empty; }
        }

        /// <summary>
        /// Extension method for string to de-serialize
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlString"></param>
        /// <returns></returns>
        public static T DeserializeXMLToObject<T>(this string xmlString)
        {
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                StringReader textReader = new StringReader(xmlString);
                return (T)xmlSerializer.Deserialize(textReader);
            }
            catch { return default(T); }
        }

        /// <summary>
        /// Extension method for JObject to return as generic type
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="strType"></param>
        /// <returns></returns>
        public static dynamic ToGenericType(this JObject obj, string strType)
        {
            try
            {
                JsonSerializer serializer = new JsonSerializer();
                var type = Type.GetType(strType);
                var deserializedObject = serializer.Deserialize(new JTokenReader(obj), type);
                return Convert.ChangeType(deserializedObject, type);
            }
            catch { return null; }
        }
    }
}
