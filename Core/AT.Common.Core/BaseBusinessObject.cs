using System;
using System.Collections.Generic;
using System.Text;
using System.IO;



namespace AT.Common.Core
{

    /// <summary>
    /// The base class for business objects
    /// This is an abstract class with the abstract method Invoke
    /// The Invoke is actually the factory of methods to invoke
    /// </summary>
	public abstract class BaseBusinessObject {

		/// <summary>
        /// This is the factory for the method to fire
        /// </summary>
        /// <typeparam name="T">The return type</typeparam>
        /// <param name="arg">The argument for the action. 
        /// This argument should be cast to concrete argument on concrete business object</param>
        /// <returns></returns>
		public abstract ResultSet<T> Invoke<T>(BaseArgument arg);

	}

}