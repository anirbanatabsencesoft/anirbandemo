using System.Net;

namespace AT.Common.Core
{
    /// <summary>
    /// The ResultSet instance is a type parameter for it's property Data 
    /// which is it's data type.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ResultSet<T> : ResultSetBase
    {
        public ResultSet()
        {
        }

        public ResultSet(T data, string message, bool success = true, HttpStatusCode statusCode = HttpStatusCode.OK)
            : base(message, success, statusCode)
        {
            this.Data = data;
        }

        /// <summary>
        /// The data contained in the result
        /// </summary>
        public T Data { get; set; }
    }

}