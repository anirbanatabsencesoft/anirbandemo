﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Common.Core
{
    /// <summary>
    /// This class provides all constants used throughout application
    /// </summary>
    public static partial class AppConstants
    {
        /// <summary>
        /// Only argument identifier constants to be defined here.
        /// </summary>
        public static class TokenIdentifier
        {
            //constants
            public const string HEADER_TOKEN_KEY = "X-AUTH-TOKEN";
            public const string URL_TOKEN_KEY = "token";
            public const string COOKIE_TOKEN_KEY = "token";
        }
    }
}
