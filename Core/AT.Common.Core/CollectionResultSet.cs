using System.Collections.Generic;
using System.Net;

namespace AT.Common.Core
{
    /// <summary>
    /// The CollectionResultSet instance is a type parameter for it's property Data collection 
    /// which is it's data type.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CollectionResultSet<T> : ResultSetBase
    {
        public CollectionResultSet(IEnumerable<T> data, string message = null, bool success = true, HttpStatusCode statusCode = HttpStatusCode.OK)
            : base(message, success, statusCode)
        {
            Data = data;
        }

        /// <summary>
        /// The collection of data of the action result
        /// </summary>
        public IEnumerable<T> Data { get; set; }
    }

}