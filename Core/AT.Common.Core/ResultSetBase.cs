using System.Net;

namespace AT.Common.Core
{
    /// <summary>
    /// The ResultSetBase instance is a type the represents result and related data
    /// </summary>
    public class ResultSetBase
    {
        public ResultSetBase()
        {
        }

        public ResultSetBase(string message, bool success = true, HttpStatusCode statusCode = HttpStatusCode.OK)
        {
            this.Success = success;
            this.StatusCode = statusCode;
            this.Message = message;
        }

        /// <summary>
        /// Shows whether the action/business logic succeeded 
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Message/Error to pass
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Total record count of data. This may be required for grid/dynamic loading
        /// </summary>
        public int? RecordCount { get; set; }

        /// <summary>
        /// Set the Http Status code
        /// </summary>
        public HttpStatusCode StatusCode { get; set; }
    }

}