﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Serialization;

namespace AT.Common.Core
{
    /// <summary>
    /// All utility methods to be stored here.
    /// </summary>
    public static class Utilities
    {
        /// <summary>
        /// Serialize an object to JSON
        /// </summary>
        /// <typeparam name="T">The type of the object</typeparam>
        /// <param name="obj">The object</param>
        /// <returns></returns>
        public static string SerializeToJson<T>(T obj)
        {
            return JsonConvert.SerializeObject(obj
                , Formatting.Indented
                , new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
        }

        /// <summary>
        /// De-serialize a JSON string to object.
        /// </summary>
        /// <typeparam name="T">The type of the object</typeparam>
        /// <param name="json">JSON string</param>
        /// <returns></returns>
        public static T DeserializeJson<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        /// <summary>
        /// Hashes the text using SHA1.
        /// </summary>
        /// <param name="text">The password.</param>
        /// <returns></returns>
        public static string ComputeSha1Hash(string text)
        {

            if (string.IsNullOrWhiteSpace(text))
                return text;

            byte[] hashValue;
            using (SHA1Managed hash = new SHA1Managed())
                hashValue = hash.ComputeHash(Encoding.UTF8.GetBytes(text));

            StringBuilder data = new StringBuilder();
            foreach (byte hexdigit in hashValue)
                data.Append(hexdigit.ToString("X2", CultureInfo.InvariantCulture.NumberFormat));
            return data.ToString();
        }

        /// <summary>
        /// Compute 256 hash
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string ComputeSha256Hash(string text)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(text));

                // Convert byte array to a string   
                StringBuilder data = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    data.Append(bytes[i].ToString("x2"));
                }
                return data.ToString();
            }
        }

        /// <summary>
        /// Reads the stream fully and returns byte array
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static byte[] ReadStreamFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        /// <summary>
        /// Get value from a key value pair
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T GetValue<T>(this IEnumerable<KeyValuePair<string, string>> collection, string key)
        {
            try
            {
                var value = collection.LastOrDefault(x => x.Key == key).Value;
                return (T)Convert.ChangeType(value, typeof(T));
            }
            catch
            {
                return default(T);
            }
        }

        /// <summary>
        /// Transforms a data from name value collection against a key
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T GetValue<T>(this NameValueCollection collection, string key)
        {
            try
            {
                var value = collection[key];
                return (T)ChangeType(value, typeof(T));
            }
            catch
            {
                return default(T);
            }
        }

        /// <summary>
        /// Change types handling nullable stucks
        /// </summary>
        /// <param name="value"></param>
        /// <param name="conversionType"></param>
        /// <returns></returns>
        public static object ChangeType(object value, Type conversionType)
        {
            if (conversionType == null)
            {
                throw new ArgumentNullException("conversionType");
            }
            
            if (conversionType.IsGenericType && conversionType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                if (value == null)
                {
                    return null;
                }

                NullableConverter nullableConverter = new NullableConverter(conversionType);
                conversionType = nullableConverter.UnderlyingType;
            }

            return Convert.ChangeType(value, conversionType);
        }

        /// <summary>
        /// Gets the api token from the ClaimsIdentity.
        /// </summary>
        /// <returns></returns>
        public static string GetTokenFromClaims()
        {
            var identity = HttpContext.Current.User.Identity as ClaimsIdentity;
            var claims = new List<Claim>(identity.Claims);
            var tokenClaim = claims.Find(p => p.Type.Equals("token"));
            if (tokenClaim != null)
            {
                return tokenClaim.Value;
            }
            return null;
        }
    }
}
