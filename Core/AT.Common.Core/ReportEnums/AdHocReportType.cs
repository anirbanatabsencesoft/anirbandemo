﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Common.Core.ReportEnums
{
    public enum AdHocReportType
    {
        Employee = 1,
        Case = 2,
        ToDo = 3,
        Customer = 4
    }
}
