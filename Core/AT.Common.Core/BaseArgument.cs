using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;

namespace AT.Common.Core
{
    /// <summary>
    /// The base argument for all command arguments
    /// </summary>
    public abstract class BaseArgument
    {
        protected BaseArgument()
        {
            Rows = 0;
            Offset = 100;
        }

        /// <summary>
        /// This is the registry identifier for all command arguments
        /// </summary>
        public abstract string Identifier { get; }

        /// <summary>
        /// Returns the message for validation
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// The number of rows to be retrieved from the offset
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// From which row the data to be retrieved.
        /// </summary>
        public int Rows { get; set; }

        /// <summary>
        /// checks whether the object is valid or not
        /// </summary>
        /// <returns></returns>
        public virtual bool IsValid
        {
            get
            {
                var context = new ValidationContext(this, serviceProvider: null, items: null);
                var results = new List<ValidationResult>();
                var isobjectvalid = Validator.TryValidateObject(this, context, results);

                //get error message
                StringBuilder sb = new StringBuilder();
                foreach (var validationResult in results)
                {
                    sb.Append(validationResult + Environment.NewLine);
                }
                this.Message = sb.ToString();
                //return result
                return isobjectvalid;
            }
        }

    }
}