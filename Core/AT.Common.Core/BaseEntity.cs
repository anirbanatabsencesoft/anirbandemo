using System;
using System.Collections.Generic;
using System.Text;
using System.IO;



namespace AT.Common.Core
{
    /// <summary>
    /// Any business entity should inherit from this class.
    /// </summary>
    public class BaseEntity
    {
        /// <summary>
        /// Returns the XML string of the entity
        /// </summary>
        /// <returns>XML String</returns>
        public string ToXML()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns the JSON string of the object
        /// </summary>
        /// <returns>JSON string</returns>
        public string ToJson()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Deep copy of the entity/object
        /// </summary>
        /// <returns></returns>
        public string DeepCopy()
        {
            throw new NotImplementedException();
        }
    }
}