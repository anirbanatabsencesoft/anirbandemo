﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Common.Core.NotificationEnums
{
    public enum RecipientType
    {
        To = 6202,
        CC = 6203
    }
}
