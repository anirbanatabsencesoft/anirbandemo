﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Common.Core.NotificationEnums
{
    public enum MessageFormat
    {
        Text = 5901,
        FormattedText = 5902,
        HTML = 5903
    }
}
