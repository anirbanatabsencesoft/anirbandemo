﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Common.Core.NotificationEnums
{
    public enum NotificationStatus
    {
        Sent = 6101,
        Discarded = 6102
    }
}
