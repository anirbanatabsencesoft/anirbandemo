﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Common.Core
{


    /// <summary>
    /// Group all enums for application type and features
    /// </summary>
    public class ApplicationEnums
    {
        public enum AppType
        {
            WebApp = 0,
            ESS = 1,
            Administration = 2,
            WebApi,
            CRON,
            Others
        }


        public enum AppFeature
        {
            None = 0,
            ADA = 1,
            ShortTermDisability = 2,
            MultiEmployerAccess,
            GuidelinesData,
            PolicyConfiguration,
            CommunicationConfiguration,
            EmployeeSelfService,
            LOA,
            ShortTermDisablityPay,
            ESSDataVisibilityInPortal,
            WorkflowConfiguration,
            CaseReporter,
            WorkRelatedReporting,
            IPRestrictions,
            EmployeeConsults,
            InquiryCases,
            OrgDataVisibility,
            JSA,
            CustomContent,
            AdHocReporting,
            EmployerContact,
            EmployerServiceOptions,
            OshaReporting,
            RiskProfile,
            JobConfiguration,
            WorkRestriction,
            BusinessIntelligenceReport,
            ContactPreferences,
            AdminPaySchedules,
            AdminPhysicalDemands,
            AdminCustomFields,
            AdminNecessities,
            AdminNoteCategories,
            AdminConsultations,
            AdminAssignTeams,
            AdminOrganizations,
            AdminDataUpload,
            AdminEmployers,
            AdminSecuritySettings,
            AccommodationTypeCategories,
            AdjustEmployeeStartDayOfWeek,
            SingleSignOn,
            PolicyCrosswalk,
            CaseBulkDownload,

        }


    }
    
}
