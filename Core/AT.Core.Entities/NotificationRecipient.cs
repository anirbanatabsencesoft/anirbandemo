using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using AT.Common.Core;

namespace AT.Core.Entities
{
	public class NotificationRecipient : BaseEntity {

        //The key
        public long? Id { get; set; }

        /// <summary>
        /// Email or Phone
        /// </summary>
        public string RecipientAddress{get;set;}
        		
		/// <summary>
		/// The foreign key for Notification
		/// </summary>
		public long NotificationId{get;set;}

        /// <summary>
        /// The type of the recipient i.e. SMS/CC/To
        /// </summary>
		public NotificationEnums.RecipientType RecipientType{get;set;}

        /// <summary>
        /// Identifies if recipient type would be Phone, To, CC, BCC which of these
        /// </summary>
        public int LookupRecipientTypeId { get; set; }

    }//end Recipient

}//end namespace Entities