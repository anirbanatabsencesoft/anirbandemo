using AT.Common.Core;

namespace AT.Core.Entities
{
    public class NotificationAttachment : BaseEntity {

		
		/// <summary>
		/// The Key
		/// </summary>
		public long? Id{get;set;}

		/// <summary>
		/// The notification id to relate
		/// </summary>
		public long NotificationId{get;set;}

		/// <summary>
		/// The file name of the attachment.
		/// </summary>
		public string FileName{get;set;}

		/// <summary>
		/// The content
		/// </summary>
		public byte[] FileContent{get;set;}

		/// <summary>
		/// The attachment header may be required for any attachment.
		/// </summary>
		public string AttachmentHeader{get;set;}

	}//end Attachment

}//end namespace Entities