﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Core.Entities
{
    /// <summary>
    /// The class for Language
    /// </summary>
    public class Language
    {
        /// <summary>
        /// The identifier of the Language
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// The code of the language
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// The short name of the language
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The full name of the language
        /// </summary>
        public string FullName { get; set; }
    }
}
