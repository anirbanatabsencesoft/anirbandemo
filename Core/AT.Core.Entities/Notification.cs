using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.ComponentModel;

using AT.Common.Core;


using AT.Data.Core.Interfaces;

namespace AT.Core.Entities
{
	public class Notification : BaseEntity, IData {
        
		/// <summary>
		/// The Key
		/// </summary>
		public long? Id{get;set;}

		/// <summary>
		/// The customer id for future references and DB operations.
        /// This is the Postgres ID
		/// </summary>
		public long? CustomerId{get;set;}

        /// <summary>
		/// The customer id for future references and DB operations.
        /// This is the Mongo ID
		/// </summary>
        public string CustomerObjectId { get; set; }
        
        /// <summary>
        /// The employer id
        /// </summary>
        public long? EmployerId { get; set; }

		/// <summary>
		/// Employer Id for future references and operation.
        /// Mongo Object Id
		/// </summary>
		public string EmployerObjectId{get;set;}

		/// <summary>
		/// The EmailId or Phone from where the notification is going.
		/// </summary>
		public string From{get;set;}

        public int LookupMessageFormatId { get; set; }

        /// <summary>
        /// The subject line of the notification.
        /// </summary>
        public string Subject{get;set;}

        public int LookupNotificationTypeId { get; set; }

        /// <summary>
        /// The message format of the body
        /// </summary>
		public NotificationEnums.MessageFormat MessageFormat{get;set;}

		/// <summary>
		/// The message content.
		/// </summary>
		public string Message{get;set;}

		/// <summary>
		/// The notification type. This can be Email, SMS or Fax.
		/// </summary>
		public NotificationEnums.NotificationType NotificationType{get;set;}

        /// <summary>
        /// The Status id of the current notification
        /// </summary>
        public int LookupNotificationStatusId { get; set; }


        /// <summary>
        /// The date & time the notification is created.
        /// </summary>
        public DateTime CreatedDate{get;set;}

        /// <summary>
        /// The user id that created
        /// </summary>
        public long CreatedById { get; set; }

        /// <summary>
        /// The Mongo Object Id
        /// </summary>
        public string CreatedByObjectId { get; set; }

        /// <summary>
        /// The date & time object modified
        /// </summary>
        public DateTime? ModifiedDate{get;set;}

        /// <summary>
        /// The user id that modified the object
        /// </summary>
        public long? ModifiedById { get; set; }
        
        /// <summary>
        /// The Mongo Id of the user modifeid the object
        /// </summary>
        public string ModifiedByObjectId { get; set; }
        
		/// <summary>
		/// The date and time when the notification is send.
		/// </summary>
		public DateTime? NotifiedOn{get;set;}

		/// <summary>
		/// The unique id if we want to maintain a trail (to be passed into the header).
		/// This should be a GUID.
		/// </summary>
		public string UniqueId{get;set;}

		/// <summary>
		/// The message id (if available from SMTP).
		/// </summary>
		public string MessageId{get;set;}

		/// <summary>
		/// The number of retries it followed or how many times it will process.
		/// </summary>
		public int? Retries{get;set;}

        /// <summary>
        /// Max number of retries.
        /// </summary>
        [DefaultValue(3)]
		public int MaxNumberOfRetries{get;set;}

		/// <summary>
		/// The date dtime when it's last retried.
		/// </summary>
		public DateTime? LastRetriedOn{get;set;}

		/// <summary>
		/// The list of recipients.
		/// </summary>
		public IList<NotificationRecipient> RecipientList{get;set;}

		/// <summary>
		/// The list of attachments.
		/// </summary>
		public IList<NotificationAttachment> AttachmentList{get;set;}

		/// <summary>
		/// Flag whether the notification should be archived or not.
		/// </summary>
		public bool IsArchived{get;set;}

        /// <summary>
        /// Maintains the Failure reason for a notification
        /// </summary>
        public string FailureReason { get; set; }

        public string Recipients { get; set; }

    }//end Notification

}//end namespace Entities