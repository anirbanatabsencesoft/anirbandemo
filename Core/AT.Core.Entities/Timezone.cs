﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Core.Entities
{
    /// <summary>
    /// The class for representing timezone information
    /// </summary>
    public class Timezone
    {
        /// <summary>
        /// The identifier of the timezone
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// The display name of the timezone
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// The standard name of the timezone
        /// </summary>
        public string StandardName { get; set; }
    }
}
