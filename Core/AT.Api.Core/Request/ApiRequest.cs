﻿using AT.Api.Core.Exceptions;

namespace AT.Api.Core.Request
{
    public abstract class ApiRequest<TParameters, TResponse>
    {
        protected abstract bool IsValid(TParameters parameters, out string message);
        protected abstract TResponse FulfillRequest(TParameters parameters);

        public TResponse Handle(TParameters parameters)
        {
            var message = string.Empty;

            if (!IsValid(parameters, out message))
            {
                throw new ApiException(System.Net.HttpStatusCode.BadRequest, message);
            }

            return FulfillRequest(parameters);
        }
    }
}
