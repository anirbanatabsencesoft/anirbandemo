﻿$(document).ready(function () {
    //add your code here. Make use of functions.
    hideUnwantedModules();
});


//hide all modules other than the current
function hideUnwantedModules() {
    var title = $(document).find("title").text();
    var module = title.split(" ")[0];
    $("ul#resources > li").not("#resource_" + module).hide();
}