﻿using Swashbuckle.Swagger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Description;

namespace AT.Api.Core
{
    /// <summary>
    /// This class is about to add header field for token
    /// </summary>
    public class SwaggerRequiredHeaderParameter: IOperationFilter
    {
        /// <summary>
        /// This method adds a filter for token
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="schemaRegistry"></param>
        /// <param name="apiDescription"></param>
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            if (operation.parameters == null)
            {
                operation.parameters = new List<Parameter>();
            }

            operation.parameters.Add(new Parameter
            {
                name = "X-AUTH-TOKEN",
                @in = "header",
                type = "string",
                description = "Authorization Token",
                required = false
            });
        }

        
    }
}
