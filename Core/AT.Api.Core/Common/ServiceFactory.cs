using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json.Linq;
using AT.Common.Core;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Web.Routing;
using System.Web.Http.Results;
using System.Linq.Expressions;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Net;
using System.Dynamic;
using AT.Common.Log;

namespace AT.Api.Core.Common {
    public class ServiceFactory
    {
        /// <summary>
        /// Private variable to be set on construction
        /// </summary>
        private readonly string _token;

        public ServiceFactory()
        {
        }

        /// <summary>
        /// Constructor. Set required variables.
        /// </summary>
        public ServiceFactory(string token)
        {
            this._token = token;
        }


#pragma warning disable S2436 // Classes and methods should not have too many generic parameters
                             /// <summary>
                             /// Get the business object using GetBusinessObjectFromFactory method and call
                             /// This call may be internal DLL or External Web Services
                             /// Invoke method and return the result.
                             /// </summary>
                             /// <param name="arg">The argument</param>
                             /// <param name="func">The function to be called</param>
        public async Task<ResultSet<TEntity>> ExecuteService<TController, TArgument, TEntity>(TArgument arg, Func<TController, Task<ResultSet<TEntity>>> func)
#pragma warning restore S2436 // Classes and methods should not have too many generic parameters
            where TController : BaseApiController, new()
        {
            //get the endpoint
            var endpoint = GetEndPointForController(func);
            if (string.IsNullOrWhiteSpace(endpoint))
            {
                TController controller = new TController() { Token = _token };                
                return await func(controller);
            }
            else
            {
                var uri = new Uri(endpoint);
                var baseAddress = uri.GetLeftPart(UriPartial.Authority);
                var apiPath = endpoint.Replace(baseAddress, "");
                return await CallHttp<TEntity>(baseAddress, apiPath, arg);
            }
        }
        
#pragma warning disable S2436 // Classes and methods should not have too many generic parameters
        /// <summary>
        /// Get the api for a controller and it's action
        /// </summary>
        /// <typeparam name="TController"></typeparam>
        /// <typeparam name="TArgument"></typeparam>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="func"></param>
        /// <returns></returns>
        private string GetEndPointForController<TController, TEntity>(Func<TController, Task<ResultSet<TEntity>>> func)
#pragma warning restore S2436 // Classes and methods should not have too many generic parameters
            where TController : BaseApiController, new()
        {
            var host = EndPointManager.Instance.GetHostForController(typeof(TController).FullName);
            if(!string.IsNullOrWhiteSpace(host))
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(string.Format("{0}/", host));

                //get all attributes
                var method = func.GetInvocationList()[0].Method;
                var routeAttrs = method.GetCustomAttributes(typeof(RouteAttribute), true);
                var routePrefixAttr = method.GetCustomAttributes(typeof(RoutePrefixAttribute), true);
                if (routeAttrs.Length == 0)
                    sb.Append(string.Format("{0}/{1}", typeof(TController).Name, method.Name));
                else
                {
                    if (routePrefixAttr.Length > 0)
                        sb.Append(string.Format("{0}/", routePrefixAttr[0]));

                    sb.Append(routeAttrs[0]);
                }

                return sb.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// Call the HTTP service
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="baseAddress">The base host address</param>
        /// <param name="apiPath">The api path</param>
        /// <param name="arg">The argument to be passed</param>
        /// <returns></returns>
        private async Task<ResultSet<T>> CallHttp<T>(string baseAddress, string apiPath, object arg = null)
        {
            //use the HTTP client
            using (HttpClient client = new HttpClient())
            {
                //set the required parameters
                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add(AppConstants.TokenIdentifier.HEADER_TOKEN_KEY, _token);


                try
                {
                    //define the response
                    HttpResponseMessage response = null;

                    //call HTTP service now
                    if (apiPath.ToUpper().Contains("/GET"))
                        response = await client.GetAsync(apiPath);
                    else if (apiPath.ToUpper().Contains("/PUT"))
                        response = await client.PutAsJsonAsync(apiPath, arg);
                    else
                        response = await client.PostAsJsonAsync(apiPath, arg);

                    //get contents
                    var contents = await response.Content.ReadAsStringAsync();

                    //the response must be type we defined.
                    return JsonConvert.DeserializeObject<ResultSet<T>>(contents);
                }
                catch (Exception ex)
                {
                    return new ResultSet<T> { Message = ex.Message };
                }
            }
        }

        /// <summary>
        /// Call a web api using HTTP
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="endPoint"></param>
        /// <param name="data"></param>
        /// <param name="method"></param>
        /// <returns></returns>
        public async Task<object> CallServiceAsync(string component, string endPoint, string data, string method)
        {
            var host = EndPointManager.Instance.GetHostForComponent(component);
            var version = "v1";
            var appender = endPoint.IndexOf("?") > -1 ? "&" : "?";
            var url = string.Format("{0}/{1}/{2}{3}token={4}", host, version, endPoint, appender, _token);

            //create request object
            HttpWebRequest request = WebRequest.CreateHttp(url);
            request.Method = method;
            request.ContentType = "application/json";
            request.ContentLength = data != null ? data.Length : 0;

            //create data object
            if (!string.IsNullOrWhiteSpace(data))
            {
                using (Stream webStream = request.GetRequestStream())
                using (StreamWriter requestWriter = new StreamWriter(webStream, System.Text.Encoding.ASCII))
                {
                    requestWriter.Write(data);
                }
            }

            //add header
            request.Headers["X-AUTH-TOKEN"] = _token;

            //result
            object result = null;

            //call
            try
            {
                using (WebResponse webResponse = await request.GetResponseAsync())
                using (Stream webStream = webResponse.GetResponseStream())
                {
                    if (webStream != null)
                    {
                        using (StreamReader responseReader = new StreamReader(webStream))
                        {
                            string response = await responseReader.ReadToEndAsync();
                            result = JsonConvert.DeserializeObject(response);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }

            return result;
        }


       /// <summary>
       /// Call the Web Api to get the file name for pdf or csv in S3
       /// </summary>
       /// <param name="component"></param>
       /// <param name="endPoint"></param>
       /// <param name="data"></param>
       /// <param name="method"></param>
       /// <returns></returns>
        public string CallFileService(string component, string endPoint, string data, string method)
        {
            var host = EndPointManager.Instance.GetHostForComponent(component);
            var version = "v1";
            var url = string.Format("{0}/{1}/{2}?token={3}", host, version, endPoint, _token);
            string output = string.Empty;
            //create request object
            HttpWebRequest request = WebRequest.CreateHttp(url);
            request.Method = method;
            request.ContentType = "application/json";
            //call
            try
            {
                using (WebResponse webResponse = request.GetResponse())
                using (Stream webStream = webResponse.GetResponseStream())
                {
                    if (webStream != null)
                    {
                        using (StreamReader responseReader = new StreamReader(webStream))
                        {
                            output = responseReader.ReadToEndAsync().GetAwaiter().GetResult();
                            output = output.Replace("\"", "");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return output;
        }
    }
}