﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using AT.Common.Core;
using Newtonsoft.Json;

namespace AT.Api.Core.Common
{
    /// <summary>
    /// This class will load the service registry from the configuration.
    /// Set the configuration only with private IPs or domain.
    /// </summary>
    public class EndPointManager
    {
        /// <summary>
        /// The registry info
        /// </summary>
        private List<ApiRegistry> RegistryList { get; set; }

        /// <summary>
        /// Static instance to load as singleton
        /// </summary>
        private static EndPointManager _instance;

        /// <summary>
        /// private constructor for restricting to create that object diretcly
        /// </summary>
        private EndPointManager()
        {
            LoadRegistry();
        }

        /// <summary>
        /// Returns the instance of the class
        /// </summary>
        public static EndPointManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new EndPointManager();

                return _instance;
            }
        }

        /// <summary>
        /// Load the registry from JSON
        /// </summary>
        private void LoadRegistry()
        {
            try
            {
                string baseDirectory;
                try
                {
                    baseDirectory = System.Web.HttpRuntime.AppDomainAppPath;
                }
                catch
                {
                    baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
                }
                string path = System.IO.Path.Combine(baseDirectory, "registry.json");
                this.RegistryList = JsonConvert.DeserializeObject<List<ApiRegistry>>(System.IO.File.ReadAllText(path));
            }
            catch
            {
                this.RegistryList = new List<ApiRegistry>();
            }
        }

        /// <summary>
        /// Returns the host entry for a particular controller
        /// </summary>
        /// <param name="controller"></param>
        /// <returns></returns>
        public string GetHostForController(string controller)
        {
            var registry = RegistryList.FirstOrDefault(p => p.Controllers.Contains(controller));
            if (registry != null)
                return registry.Host;

            return string.Empty;
        }

        /// <summary>
        /// Returns the host for a component
        /// </summary>
        /// <param name="component"></param>
        /// <returns></returns>
        public string GetHostForComponent(string component)
        {
            var registry = RegistryList.FirstOrDefault(p => p.Component == component.ToLower());
            if (registry != null)
                return registry.Host;

            return string.Empty;
        }

        /// <summary>
        /// Returns the registry entry for the host
        /// </summary>
        /// <param name="host"></param>
        /// <returns></returns>
        public ApiRegistry GetRegistryForHost(string host)
        {
            var registry = RegistryList.FirstOrDefault(p => new Uri(p.Host).Host == new Uri(host).Host);
            if (registry == null)
                registry = new ApiRegistry();

            return registry;
        }
    }
    
    /// <summary>
    /// Class for maintaining the registry
    /// </summary>
    public class ApiRegistry
    {
        /// <summary>
        /// Define the host for controller and component
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Define available controllers
        /// </summary>
        public string[] Controllers { get; set; }

        /// <summary>
        /// Define the component name
        /// </summary>
        public string Component { get; set; }

    }
}