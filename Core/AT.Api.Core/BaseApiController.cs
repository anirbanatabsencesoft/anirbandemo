﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web;
using System.Web.Routing;

using AT.Api.Core.Common;
using AT.Common.Core;
using AT.Logic.Authentication;
using Microsoft.Owin.Security;
using AT.Data.Authentication.Provider;
using AT.Entities.Authentication;
using AT.Provider.Cache.Base;
using System.Web.Http.Controllers;
using AT.Logic.Cache;
using System.Net.Http;
using System.Net;
using AT.Common.Log;
using AT.Api.Core.Contracts;
using AT.Api.Core.Exceptions;

namespace AT.Api.Core
{
    public class BaseApiController : ApiController, IAuthorizable
    {
        #region Initializers

        //The token
        private string _token;
        private string _employerId;

#pragma warning disable S2292 //for disabling warning to use backing fields - SonarLint
        /// <summary>
        /// Set the token to call internally
        /// </summary>
        public string Token { get { return _token; } set { _token = value; } }
#pragma warning restore S2292


        /// <summary>
        /// The service factory to be used by other APIs
        /// </summary>
        public ServiceFactory Service { get; private set; }

        /// <summary>
        /// Set the authenticated user if token is available
        /// </summary>
        public User AuthenticatedUser { get; set; }

        /// <summary>
        /// The context to be set either current or any custom
        /// </summary>
        public HttpContextBase Context { get; set; }

        /// <summary>
        /// Constructor to set the service factory and request validation
        /// </summary>
        public BaseApiController()
        {
            Initialize();
        }

        /// <summary>
        /// The constructor to set the context
        /// </summary>
        /// <param name="context"></param>
        public BaseApiController(HttpContextBase context)
        {
            this.Context = context;
            Initialize();
        }

        /// <summary>
        /// To be called from constructor
        /// </summary>
        private void Initialize()
        {
            //Set required variables first
            SetRequiredVariablesFromContext();

            //Check the context and throw error on failure
            if (!string.IsNullOrWhiteSpace(_token) && AuthenticatedUser == null)
            {
                GetUser();
            }
        }

        /// <summary>
        /// Get the context values and set required items.
        /// </summary>
        private void SetRequiredVariablesFromContext()
        {
            if (string.IsNullOrWhiteSpace(_token))
            {
                _token = Utilities.GetTokenFromClaims();

                if (string.IsNullOrWhiteSpace(_token))
                {
                    //set the context
                    if (this.Context == null)
                    {
                        this.Context = new HttpContextWrapper(HttpContext.Current);
                    }
                    //get the token
                    if (!string.IsNullOrWhiteSpace(this.Context.Request.Headers[AppConstants.TokenIdentifier.HEADER_TOKEN_KEY]))
                    {
                        _token = this.Context.Request.Headers[AppConstants.TokenIdentifier.HEADER_TOKEN_KEY];
                    }
                    else if (!string.IsNullOrWhiteSpace(this.Context.Request.QueryString[AppConstants.TokenIdentifier.URL_TOKEN_KEY]))
                    {
                        _token = this.Context.Request.QueryString[AppConstants.TokenIdentifier.URL_TOKEN_KEY];
                    }
                    else if (this.Context.Request.Cookies.Get(AppConstants.TokenIdentifier.COOKIE_TOKEN_KEY) != null && !string.IsNullOrWhiteSpace(this.Context.Request.Cookies.Get(AppConstants.TokenIdentifier.COOKIE_TOKEN_KEY).Value))
                    {
                        _token = this.Context.Request.Cookies.Get(AppConstants.TokenIdentifier.COOKIE_TOKEN_KEY).Value;
                    }
                }
            }
        }

        /// <summary>
        /// Get the user from token
        /// </summary>
        private void GetUser()
        {
            try
            {
                ApplicationUserManager userManager = new ApplicationUserManager(UserStoreProviderManager.Default.Repository);
                AuthenticationTicket authenticationTicket = userManager.ValidateTokenAsync(_token).GetAwaiter().GetResult();
                if (authenticationTicket != null)
                {
                    var userPrincipal = userManager.GetClaimsPrincipal(authenticationTicket).Result;

                    if (userPrincipal?.Identity != null)
                    {
                        //get the user data from cache
                        this.AuthenticatedUser = CacheHelper.Get<AT.Entities.Authentication.User>(userPrincipal.Identity.Name);
                        //if data is not available, get the data from database and cache for 30 mins
                        if (AuthenticatedUser == null)
                        {
                            this.AuthenticatedUser = userManager.FindByNameAsync(userPrincipal.Identity.Name).Result;
                            CacheHelper.Set(userPrincipal.Identity.Name, this.AuthenticatedUser, 7200);
                        }
                    }
                }
                else
                {
                    if (Context?.Response != null)
                    {
                        Context.Response.StatusCode = 403;
                    }
                    else
                    {
                        throw new UnauthorizedAccessException();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                if (Context != null && Context.Response != null)
                {
                    Context.Response.StatusCode = 403;
                }
            }
        }

        public string EmployerId
        {
            get
            {
                if (!string.IsNullOrEmpty(_employerId))
                {
                    return _employerId;
                }

                var employer = AuthenticatedUser.Employers.FirstOrDefault();

                if (employer == null)
                {
                    return string.Empty;
                }

                return employer.EmployerId;
            }
            set
            {
                _employerId = value;
            }
        }

        #endregion
    }
}
