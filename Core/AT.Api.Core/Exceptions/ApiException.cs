﻿using System;
using System.Net;

namespace AT.Api.Core.Exceptions
{
#pragma warning disable S3925 // "ISerializable" should be implemented correctly
    public class ApiException : Exception
#pragma warning restore S3925 // "ISerializable" should be implemented correctly    
    {
        public ApiException(HttpStatusCode statusCode)
        {
            StatusCode = statusCode;
        }

        public ApiException(HttpStatusCode statusCode, string message) : base(message)
        {
            StatusCode = statusCode;
        }

        public HttpStatusCode StatusCode { get; private set; }
    }
}
