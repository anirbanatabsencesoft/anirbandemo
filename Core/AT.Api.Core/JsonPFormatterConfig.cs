﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using WebApiContrib.Formatting.Jsonp;

namespace AT.Api.Core
{
    /// <summary>
    /// Config to add JSON P Formatter
    /// </summary>
    public static class JsonPFormatterConfig
    {
        /// <summary>
        /// Registers a JSON P formatter at level 0
        /// </summary>
        /// <param name="formatters"></param>
        public static void RegisterFormatters(MediaTypeFormatterCollection formatters)
        {
            formatters.Remove(formatters.JsonFormatter);
            formatters.Insert(0, new JsonpMediaTypeFormatter
            {
                SerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                }
            });
        }
    }
}
