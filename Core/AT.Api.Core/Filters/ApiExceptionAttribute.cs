﻿using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http.Filters;
using AT.Api.Core.Exceptions;
using AT.Common.Core;
using AT.Common.Log;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AT.Api.Core.Filters
{
    public class ApiExceptionAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            var exception = actionExecutedContext.Exception;
            HttpResponseMessage response = null;

            Logger.Error(exception);

            var serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            var failureResult = new ResultSetBase
            {
                Success = false,
                RecordCount = 0
            };

            if (exception.GetType() == typeof(ApiException))
            {
                var apiException = (ApiException)exception;
                
                failureResult.Message = apiException.Message;
                failureResult.StatusCode = apiException.StatusCode;                
            }
            else
            {
                failureResult.StatusCode = HttpStatusCode.InternalServerError;
                failureResult.Message = "There was an error processing your request";                
            }

            var jsonResponseContent = JsonConvert.SerializeObject(failureResult, serializerSettings);

            response = new HttpResponseMessage(failureResult.StatusCode)
            {
                Content = new StringContent(jsonResponseContent, Encoding.UTF8, "application/json")
            };

            actionExecutedContext.Response = response;
        }
    }
}
