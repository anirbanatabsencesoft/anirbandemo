﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

using AT.Logic.Cache;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;

namespace AT.Api.Core
{
    /// <summary>
    /// This class to be used for caching output at server as well as at client
    /// </summary>
    public class CacheAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// The timespan to cache the object at server
        /// </summary>
        public int ServerCacheDuration { get; private set; }

        /// <summary>
        /// The timespan to cache the object at client
        /// </summary>
        public int ClientCacheDuration { get; private set; }
    
        /// <summary>
        /// The cache key for the web api endpoints
        /// </summary>
        public string CacheKey { get; private set; }

        private const char cacheKeyDelimeter = '_';
        private const string clientCacheSuffix = "ct";


        /// <summary>
        /// Costructor to set the server and client timespan
        /// </summary>
        /// <param name="serverTimeSpanInSeconds">Set the time in seconds to cache the object in server</param>
        /// <param name="clientTimeSpanInSeconds">Set the time in seconds to cache the object in client</param>
        public CacheAttribute(int serverCacheDuration = 600, int clientCacheDuration = 600)
        {
            ServerCacheDuration = serverCacheDuration;
            ClientCacheDuration = clientCacheDuration;
        }

        /// <summary>
        /// Override the action execution method to do our task
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(HttpActionContext context)
        {
            if (context != null && IsCacheable(context))
            {
                //set the cache key here
                CacheKey = string.Join(Convert.ToString(cacheKeyDelimeter), new string[] { context.Request.RequestUri.AbsolutePath, context.Request.Headers.Accept.FirstOrDefault().ToString() });

                //if cache exists
                if (CacheHelper.Exists(CacheKey))
                {
                    //get the cache from server
                    var val = CacheHelper.Get<string>(CacheKey);
                    if (val != null)
                    {
                        //set the content to client cache
                        context.Response = context.Request.CreateResponse();
                        context.Response.Content = new StringContent(val);

                        //get the content type
                        var contenttype = CacheHelper.Get<MediaTypeHeaderValue>(string.Format("{0}_{1}", CacheKey, clientCacheSuffix));
                        if (contenttype == null)
                        {
                            contenttype = new MediaTypeHeaderValue(CacheKey.Split(cacheKeyDelimeter)[1]);
                        }

                        //add the content to header
                        context.Response.Content.Headers.ContentType = contenttype;
                        context.Response.Headers.CacheControl = SetClientCache();
                    }
                }
            }
        }


        /// <summary>
        /// Override action executed method
        /// </summary>
        /// <param name="actionExecutedContext"></param>
        public override void OnActionExecuted(System.Web.Http.Filters.HttpActionExecutedContext actionExecutedContext)
        {
            //Set the value to cache if not exists
            if (!(CacheHelper.Exists(CacheKey)))
            {
                var responseText = actionExecutedContext.Response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                CacheHelper.Set(CacheKey, responseText, ServerCacheDuration);
                CacheHelper.Set(string.Format("{0}_{1}", CacheKey, clientCacheSuffix), actionExecutedContext.Response.Content.Headers.ContentType, ServerCacheDuration);
            }

            //set client cache
            if (IsCacheable(actionExecutedContext.ActionContext))
            {
                actionExecutedContext.ActionContext.Response.Headers.CacheControl = SetClientCache();
            }
        }


        /// <summary>
        /// Check whether we can cache the output or not
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool IsCacheable(HttpActionContext context)
        {
            if (ServerCacheDuration > 0 && ClientCacheDuration > 0 && context.Request.Method == HttpMethod.Get)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Add client cache header info
        /// </summary>
        /// <returns></returns>
        private CacheControlHeaderValue SetClientCache()
        {
            var cachecontrol = new CacheControlHeaderValue();
            cachecontrol.MaxAge = TimeSpan.FromSeconds(ClientCacheDuration);
            cachecontrol.MustRevalidate = true;
            return cachecontrol;
        }


    }
}
