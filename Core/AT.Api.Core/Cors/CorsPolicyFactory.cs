﻿using System.Net.Http;
using System.Web.Http.Cors;

namespace AT.Api.Core.Cors
{
    public class CorsPolicyFactory : ICorsPolicyProviderFactory
    {
        public ICorsPolicyProvider GetCorsPolicyProvider(HttpRequestMessage request)
        {
            return new EnableAllCorsPolicyProvider();
        }
    }
}
