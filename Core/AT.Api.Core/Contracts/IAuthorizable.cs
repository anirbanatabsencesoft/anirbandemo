﻿using System.Web;
using AT.Entities.Authentication;

namespace AT.Api.Core.Contracts
{
    public interface IAuthorizable
    {
        User AuthenticatedUser { get; set; }
        HttpContextBase Context { get; set; }
        string EmployerId { get; set; }
    }
}
