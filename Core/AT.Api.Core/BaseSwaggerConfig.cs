﻿using Swashbuckle.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace AT.Api.Core
{
    /// <summary>
    /// Base Swagger Config holds all shared configuration methods across all apis
    /// </summary>
    public static class BaseSwaggerConfig
    {
        /// <summary>
        /// Registers the specified API for Swagger documentation.
        /// </summary>
        /// <param name="apiName">Name of the API.</param>
        /// <param name="assemblyToConfigure">The assembly to configure.</param>
        /// <param name="addTokenToHeader">If we need to add token to header</param>
        /// <param name="defaultVersion">Set the default version</param>
        /// <param name="configure">Optional additional configuration that should be done after the default configuration is done.</param>
        public static void Register(string apiName, Assembly assemblyToConfigure, bool addTokenToHeader = true, string defaultVersion = "v1", Action<SwaggerDocsConfig> configure = null)
        {
            GlobalConfiguration.Configuration
                .EnableSwagger("docs/" + apiName + "/{apiVersion}/swagger", c =>
                {
                    //the bversion and the name of documentation
                    c.SingleApiVersion(defaultVersion, string.Format("{0} Api Documentation", apiName));

                    //Add the XML configuration for controller and model
                    c.IncludeXmlComments(GetXmlCommentsPathForControllers(apiName));

                    //Ignore the obsolete
                    c.IgnoreObsoleteProperties();

                    //Apply vendor extension
                    c.DocumentFilter<ApplyDocumentVendorExtensions>();

                    //Use full name for types
                    c.UseFullTypeNameInSchemaIds();

                    //Add header token key
                    if (addTokenToHeader)
                    {
                        c.OperationFilter<SwaggerRequiredHeaderParameter>();
                    }

                    configure?.Invoke(c);
                })
                .EnableSwaggerUi(apiName + "/{*assetPath}", c =>
                {
                    c.DocumentTitle(string.Format("{0} API", apiName));

                    //Inject CSS
                    c.InjectStylesheet(assemblyToConfigure, string.Format("AT.Api.{0}.Content.css.bootstrap.min.css", apiName));
                    c.InjectStylesheet(assemblyToConfigure, string.Format("AT.Api.{0}.Content.css.bootstrap-theme.min.css", apiName));
                    c.InjectStylesheet(assemblyToConfigure, string.Format("AT.Api.{0}.Content.css.font-awesome.min.css", apiName));
                    c.InjectStylesheet(assemblyToConfigure, string.Format("AT.Api.{0}.Content.css.custom.css", apiName));

                    //Inject JS (if required for UI modification)
                    c.InjectJavaScript(assemblyToConfigure, string.Format("AT.Api.{0}.Content.js.custom.js", apiName));

                    //How to show the expansion
                    c.DocExpansion(DocExpansion.List);

                    c.SupportedSubmitMethods("GET", "POST", "DELETE", "PUT");
                    //Enabling discovery/UDDI
                    c.EnableDiscoveryUrlSelector();
                    //Enable OAuth. Change the values with what is required.
                    c.EnableOAuth2Support(
                        clientId: "test-client-id",
                        clientSecret: "123",
                        realm: "test-realm",
                        appName: "Swagger UI");
                });
        }

        private static string GetXmlCommentsPathForControllers(string apiName)
        {
            return System.String.Format(@"{0}\bin\AT.Api.{1}.xml", System.AppDomain.CurrentDomain.BaseDirectory, apiName);
        }

        public static void RegisterWithConfig(HttpConfiguration configuration, string apiName, Assembly assemblyToConfigure, bool addTokenToHeader = true, string defaultVersion = "v1", Action<SwaggerDocsConfig> configure = null)
        {
            configuration
                .EnableSwagger("docs/" + apiName + "/{apiVersion}/swagger", c =>
                {
                    //the bversion and the name of documentation
                    c.SingleApiVersion(defaultVersion, string.Format("{0} Api Documentation", apiName));

                    //Add the XML configuration for controller and model
                    c.IncludeXmlComments(GetXmlCommentsPathForControllers(apiName));

                    //Ignore the obsolete
                    c.IgnoreObsoleteProperties();

                    //Apply vendor extension
                    c.DocumentFilter<ApplyDocumentVendorExtensions>();

                    //Use full name for types
                    c.UseFullTypeNameInSchemaIds();

                    //Add header token key
                    if (addTokenToHeader)
                    {
                        c.OperationFilter<SwaggerRequiredHeaderParameter>();
                    }

                    configure?.Invoke(c);
                })
                .EnableSwaggerUi(c =>
                {
                    c.SupportedSubmitMethods("GET", "POST", "DELETE", "PUT", "PATCH");
                });
        }
    }
}
