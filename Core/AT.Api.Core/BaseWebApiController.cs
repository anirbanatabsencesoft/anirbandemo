﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

using AT.Common.Core;

namespace AT.Api.Core
{
#pragma warning disable S2436 // Classes and methods should not have too many generic parameters
                             /// <summary>
                             /// The abstract class to be implemented for all APIs
                             /// </summary>
                             /// <typeparam name="TOutput"></typeparam>
                             /// <typeparam name="TSearchArg"></typeparam>
                             /// <typeparam name="TSaveArg"></typeparam>
                             /// <typeparam name="TDeleteArg"></typeparam>
    public abstract class BaseWebApiController<TOutput, TSearchArg, TSaveArg, TDeleteArg> : BaseApiController
#pragma warning restore S2436 // Classes and methods should not have too many generic parameters
        where TSearchArg: BaseArgument, new()
        where TSaveArg : BaseArgument, new()
        where TDeleteArg : BaseArgument, new()
    {
        /// <summary>
        /// Get the object T by Id. This is going to be used by Postgres
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public abstract Task<ResultSet<TOutput>> GetById(long id);

        /// <summary>
        /// This is going to be used for Mongo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public abstract Task<ResultSet<TOutput>> GetByKey(string key);

        /// <summary>
        /// Search and return the list of object based on the search argument.
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public abstract Task<ResultSet<IEnumerable<TOutput>>> Search(TSearchArg arg);

        /// <summary>
        /// Search and return the list from json string passed as query string.
        /// </summary>
        /// <returns></returns>
        public abstract Task<ResultSet<IEnumerable<TOutput>>> SearchByQuery();

        /// <summary>
        /// Save the object against an argument and return the same output.
        /// The return output is required for inserted element with the ID.
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public abstract Task<ResultSet<TOutput>> Save(TSaveArg arg);
        
        /// <summary>
        /// Delete an object based on the argument and return boolean.
        /// </summary>
        /// <param name="arg"></param>
        /// <returns>The number of affected rows</returns>
        public abstract Task<ResultSet<int>> Delete(TDeleteArg arg);
    }
}