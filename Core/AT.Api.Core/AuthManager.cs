﻿using System;
using System.Configuration;
using AT.Data.Authentication.Provider;
using AT.Logic.Authentication.Server;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;

namespace AT.Api.Core
{
    /// <summary>
    /// Utility class to build/manage authentication operations
    /// </summary>
    public static class AuthManager
    {
        public static OAuthAuthorizationServerOptions BuildAuthOptions()
        {
            var tokenExpirationDuration = ConfigurationManager.AppSettings["TokenExpirationTimeInMinutes"];
            var tokenExpirationTime = 30; //default to 30 mins

            if (tokenExpirationDuration != null)
            {
                int.TryParse(tokenExpirationDuration, out tokenExpirationTime);
            }

            var allowInsecureHttpConfiguration = ConfigurationManager.AppSettings["AllowInsecureHttp"];
            var allowInsecureHttp = false;

            if (allowInsecureHttpConfiguration != null)
            {
                bool.TryParse(allowInsecureHttpConfiguration, out allowInsecureHttp);
            }

            var authServerOptions = new OAuthAuthorizationServerOptions()
            {
                //For Dev enviroment only (on production should be AllowInsecureHttp = false)
                AllowInsecureHttp = allowInsecureHttp,
                TokenEndpointPath = new PathString("/oauth2/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(tokenExpirationTime),
                Provider = new OAuthJwtServerProvider(UserStoreProviderManager.Default.Repository),
                AccessTokenFormat = new AuthenticationJwtTokenFormat("AbsenceSoft", UserStoreProviderManager.Default.Repository)
            };

            return authServerOptions;
        }
    }
}
