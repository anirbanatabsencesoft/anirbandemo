﻿using AT.Api.Core.Exceptions;
using AT.Api.Shared.Extensions;
using System.Net;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace AT.Api.Shared.Filters
{
    public class ModelStateEvaluatorAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var modelState = actionContext.ModelState;

            if (!modelState.IsValid)
            {
                var modelErrors = modelState.Errors();
                var errorMessage = string.Join(", ", modelErrors);
                var fullMessage = string.Format("There are issue(s) with the data provided. {0}", errorMessage);

                throw new ApiException(HttpStatusCode.BadRequest, fullMessage);
            }
        }
    }
}
