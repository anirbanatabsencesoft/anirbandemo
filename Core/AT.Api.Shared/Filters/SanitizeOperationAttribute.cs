﻿using AbsenceSoft.Logic.Customers;
using AT.Api.Core.Contracts;
using AT.Api.Core.Exceptions;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace AT.Api.Shared.Filters
{
    public class SanitizeOperationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var controller = (IAuthorizable)actionContext.ControllerContext.Controller;
            var user = controller.AuthenticatedUser;
            var httpContext = controller.Context;

            // Get the employer id using reference code from query string
            var employerReferenceCode = httpContext.Request.QueryString.Get("employerReferenceCode");

            if (!string.IsNullOrWhiteSpace(employerReferenceCode))
            {
                var employerService = new EmployerService();
                var employerId = employerService.GetEmployerIdByReferenceCode(employerReferenceCode);
                controller.EmployerId = employerId ?? throw new ApiException(HttpStatusCode.BadRequest, "Employer reference code is invalid");

            }

            if (user == null || httpContext?.Response?.StatusCode == (int)HttpStatusCode.Forbidden)
            {
                throw new ApiException(HttpStatusCode.Unauthorized, "User is not authenticated");
            }

            // Set the current user in HTTP context to be used in services for data access validations
            var identity = new ClaimsIdentity(
                new System.Security.Principal.GenericIdentity(user.Key.ToString()),
                new List<Claim>()
                {
                    new Claim("Key", user.Key.ToString())
                });

            var roles = user.Roles.Any() ? user.Roles.Select(role => role.Name).ToArray() : new string[0];

            var contextUser = new System.Security.Principal.GenericPrincipal(identity, roles);
            System.Web.HttpContext.Current.User = contextUser;
        }
    }
}
