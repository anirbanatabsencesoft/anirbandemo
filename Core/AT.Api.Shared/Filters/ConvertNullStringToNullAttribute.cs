﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace AT.Api.Shared.Filters
{
    public class ConvertNullStringToNullAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var actionArguments = actionContext.ActionArguments;

            if (actionArguments == null)
            {
                return;
            }

            var actionParams = new List<KeyValuePair<string, object>>();

            foreach (var argumentKeyValue in actionContext.ActionArguments)
            {
                var value = argumentKeyValue.Value;
                var valueJson = JsonConvert.SerializeObject(value);
                var jsonWithoutNullString = valueJson.Replace("\"null\"", "null");

                actionParams.Add(
                    new KeyValuePair<string, object>(
                        argumentKeyValue.Key,
                        JsonConvert.DeserializeObject(jsonWithoutNullString, value.GetType())));
            }

            foreach (var keyValuePair in actionParams)
            {
                actionArguments[keyValuePair.Key] = keyValuePair.Value;
            }
        }
    }
}
