﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AT.Api.Core.Exceptions;
using AT.Api.Shared.Model.CustomFields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AT.Api.Shared.Utitilies
{

    /// <summary>
    /// Custom field validator
    /// </summary>
    public class CustomFieldValidator
    {
        /// <summary>
        /// Validate custom field  value based on type
        /// </summary>
        /// <param name="configurationFields"></param>
        /// <param name="customFields"></param>
        /// <returns></returns>
        public static bool Validate(List<CustomField> configurationFields, SaveCustomFieldModel[] customFields)
        {
            foreach (var customField in customFields)
            {
                if (string.IsNullOrWhiteSpace(customField.Value))
                {
                    continue;
                }

                var configurationField = configurationFields.Where(cf => cf.Code == customField.Code).FirstOrDefault();

                var isValid = true;

                switch (configurationField.DataType)
                {
                    case CustomFieldType.Date:
                        DateTime date;
                        isValid = DateTime.TryParse(customField.Value, out date);
                        break;

                    case CustomFieldType.Number:
                        int number;
                        isValid = int.TryParse(customField.Value, out number);
                        break;
                    case CustomFieldType.Flag:
                        bool flag;
                        isValid = bool.TryParse(customField.Value, out flag);
                        break;
                }

                if (!isValid ||
                    (configurationField.ValueType == CustomFieldValueType.SelectList &&
                    !configurationField.ListValues.Any(l => l.Key == customField.Value || l.Value == customField.Value)))
                {
                    throw new ApiException(HttpStatusCode.BadRequest, "Invalid Configuration value");
                }
            }

            return true;
        }
    }
}
