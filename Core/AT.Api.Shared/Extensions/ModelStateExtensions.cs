﻿using System.Linq;
using System.Web.Http.ModelBinding;

namespace AT.Api.Shared.Extensions
{
    public static class ModelStateExtensions
    {
        public static string[] Errors(this ModelStateDictionary modelStateDictionary)
        {
            if (modelStateDictionary == null)
            {
                return new string[0];
            }

            var modelStateValues = modelStateDictionary.Values;

            if (modelStateValues == null || modelStateValues.Count == 0)
            {
                return new string[0];
            }

            return modelStateValues
                .SelectMany(
                    v => v.Errors
                        .Where(err => !string.IsNullOrWhiteSpace(err.ErrorMessage))
                        .Select(err => err.ErrorMessage))
                .ToArray();
        }
    }
}
