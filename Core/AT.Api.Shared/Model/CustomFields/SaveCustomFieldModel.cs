﻿namespace AT.Api.Shared.Model.CustomFields
{
    /// <summary>
    /// Save Custom Field Model
    /// </summary>
    public class SaveCustomFieldModel
    {
        /// <summary>
        /// Custom Field code
        /// </summary>
        public string Code { get; set; }


        /// <summary>
        /// Custom Field Value
        /// </summary>
        public string Value { get; set; }
    }
}