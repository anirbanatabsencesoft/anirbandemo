﻿using System.Collections.Generic;

namespace AT.Api.Shared.Model.CustomFields
{
    /// <summary>
    /// Custom Field Model
    /// </summary>
    public class CustomFieldModel
    {
        /// <summary>
        /// Custom Field code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Selected Value
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Data Type
        /// </summary>
        public int DataType { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// Value Type
        /// 1 = UserEntered, 2 = SelectList
        /// </summary>
        public int ValueType { get; set; }


        /// <summary>
        /// List values for selectlist value type
        /// </summary>
        public Dictionary<string, string> ListValues { get; set; }
    }
}