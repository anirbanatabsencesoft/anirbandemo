﻿using System;

namespace AT.Api.Shared.Model.Common
{
    public class ReadWorkScheduleModel : BaseWorkScheduleModel
    {
        public Guid Id { get; set; }
    }
}
