﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AT.Api.Shared.Model.Common
{
    public class BaseTimeModel
    {
        public int? TotalMinutes { get; set; }

        [DataType(DataType.Date), Required]
        public DateTime SampleDate { get; set; }

        public bool IsHoliday { get; set; }
    }
}
