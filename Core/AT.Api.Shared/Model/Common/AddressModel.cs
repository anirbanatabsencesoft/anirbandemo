﻿using System.ComponentModel.DataAnnotations;

namespace AT.Api.Shared.Model.Common
{
    /// <summary>
    /// Contains details about the address
    /// </summary>
    public class AddressModel
    {
        /// <summary>
        /// First address
        /// </summary>
        public string Address1 { get; set; }

        /// <summary>
        /// Second address
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// Name of the city
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Name of the state
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Country code
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Postal code
        /// </summary>
        [RegularExpression(@"^\d{5}(?:-\d{4})?$", ErrorMessage = "Zip code should be in the format XXXXX-XXXX")]
        public string PostalCode { get; set; }
    }
}