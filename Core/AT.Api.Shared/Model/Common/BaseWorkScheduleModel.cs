﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AT.Api.Shared.Model.Common
{
    public class BaseWorkScheduleModel
    {
        [Required]
        public int ScheduleType { get; set; }

        [DataType(DataType.Date), Required]
        public DateTime StartDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime? EndDate { get; set; }

        public List<BaseTimeModel> Times { get; set; }
    }
}
