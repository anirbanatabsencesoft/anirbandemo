﻿using AbsenceSoft.Process.MessageProcessing;
using System.ServiceProcess;

namespace AbsenceSoft.Process
{
    /// <summary>
    /// The Windows service that controlls the miner and/or worker which
    /// constantly connect data between DME's list engine SQL Server and
    /// the ElasticSearch cloud via AWS.
    /// </summary>
    public partial class ProcessService : ServiceBase
    {
        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="DataConnectorService"/> class.
        /// </summary>
        public ProcessService()
        {
            InitializeComponent();
        }//end: ctor

        #endregion

        /// <summary>
        /// When implemented in a derived class, executes when a Start command is sent to 
        /// the service by the Service Control Manager (SCM) or when the operating system 
        /// starts (for a service that starts automatically). Specifies actions to take 
        /// when the service starts.
        /// </summary>
        /// <param name="args">Data passed by the start command.</param>
        protected override void OnStart(string[] args)
        {
            // Kick off our async timer to startup our fun, but allow the service to
            //  get going first and immediately return, that way our startup fun doesn't
            //  time out the Windows service host controller (which is bad).
            startupTimer.Start();
        }//end: OnStart

        /// <summary>
        /// When implemented in a derived class, executes when a Stop command is sent to the 
        /// service by the Service Control Manager (SCM). Specifies actions to take when a 
        /// service stops running.
        /// </summary>
        protected override void OnStop()
        {
            ProcessTimer.StopProcessing();
        }//end: OnStop

        /// <summary>
        /// Handles the Elapsed event of the startupTimer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
        private void startupTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            ProcessTimer.StartProcessing();
        }//end: startupTimer_Elapsed

    }//end: class: DataConnectorService
}//end: namespace
