set env=%1
echo Checking for Service 'AbsenceSoftProcessService%env%'
sc query AbsenceSoftProcessService%env% | find "AbsenceSoftProcessService%env%" >nul
if %errorlevel%==0 goto ServiceFound
if %errorlevel%==1 goto ServiceMissing

:ServiceMissing
echo Service 'AbsenceSoftProcessService%env%' not found, skipping step
goto end

:ServiceFound
echo Service 'AbsenceSoftProcessService%env%' found, stopping service
net stop AbsenceSoftProcessService%env%
echo Service 'AbsenceSoftProcessService%env%' stopped
goto end

:end
echo All Done
exit /b 0