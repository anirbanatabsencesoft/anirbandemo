﻿set env=%1
echo Checking for Service 'AbsenceSoftProcessService%env%'
sc query AbsenceSoftProcessService%env%  |find "AbsenceSoftProcessService%env%" >nul
if %errorlevel%==0 goto ServiceFound
if %errorlevel%==1 goto ServiceMissing

:ServiceMissing
echo Service 'AbsenceSoftProcessService%env%' not found, installing service
sc create AbsenceSoftProcessService%env% binPath= "C:\AbsenceTrackerServices\ProcessService_%env%\AbsenceSoft.Process.exe" start= auto DisplayName= "AbsenceTracker - 
Process Service (%env%)"
if %errorlevel%==0 goto ServiceFound
goto end

:ServiceFound
echo Service 'AbsenceSoftProcessService%env%' found, starting service
net start AbsenceSoftProcessService%env%
echo Service 'AbsenceSoftProcessService%env%' started
goto end

:end
echo All Done
exit /b 0