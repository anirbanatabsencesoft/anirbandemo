﻿using AbsenceSoft.Common;
using AbsenceSoft.Common.Properties;
using AbsenceSoft.Data.Processing;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Logic.Processing;
using AbsenceSoft.Process.MessageProcessing;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AbsenceSoft.Process.MessageProcessing
{
    /// <summary>
    /// Entry point for message processing.
    /// Responsible for creating and managing threads to
    /// process message queue.
    /// </summary>
    public static class ProcessTimer
    {
        #region Static Fields
        /// <summary>
        /// The _sync object for thread-safe coordinated locking
        /// around _isProcessing.
        /// </summary>
        private static object _sync = new object();

        /// <summary>
        /// Tracks whether a processor is currently processing or is dormant
        /// </summary>
        private static ConcurrentDictionary<int, bool> _isProcessing;

        /// <summary>
        /// The timers used for work control.
        /// </summary>
        private static ConcurrentBag<Timer> _timers;

        /// <summary>
        /// The sync object for thread-safe handling of our stopped flag.
        /// </summary>
        private static ReaderWriterLockSlim _syncStopped = new ReaderWriterLockSlim();

        /// <summary>
        /// The stopped flag
        /// </summary>
        private static bool _stopped;

        /// <summary>
        /// The max thread capacity for concurrent processing of "stuff".
        /// </summary>
        private static int _threadCapacity;

        /// <summary>
        /// The list of variable thread functions referenced by index
        /// </summary>
        private static Dictionary<int, MessageQueueListener> _threadFunctions;

        #endregion

        #region ctor

        /// <summary>
        /// Initializes the <see cref="ProcessTimer"/> class.
        /// </summary>
        static ProcessTimer()
        {
            _stopped = true;
            _threadCapacity = Settings.Default.Process_EligibilityUploadFileThreads;
            _threadFunctions = new Dictionary<int, MessageQueueListener>(_threadCapacity);
            int x = 0;
            for (int i = 0; i < Settings.Default.Process_EligibilityUploadFileThreads; i++)
            {
                _threadFunctions.Add(x, new MessageQueueListener(MessageQueues.AtCommonQueue, () => IsStopped));
                x++;
            }
            _isProcessing = new ConcurrentDictionary<int, bool>(_threadCapacity, _threadCapacity);
        }//end: ctor

        #endregion

        #region Static Properties

        /// <summary>
        /// Gets or sets a value indicating whether the work is stopped.
        /// </summary>
        /// <value>
        ///   <c>true</c> if work is stopped; otherwise, <c>false</c>.
        /// </value>
        private static bool IsStopped
        {
            get
            {
                return _syncStopped.LockRead(() => _stopped);
            }
            set
            {
                _syncStopped.LockWrite(() => _stopped = value);
            }
        }//end: IsStopped

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handles the Elapsed event of the timer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ElapsedEventArgs"/> instance containing the event data.</param>
        static void timer_Elapsed(int timerId)
        {
            lock (_sync)
            {
                // check if already started, if so, then return, otherwise mark started
                if (_isProcessing[timerId])
                    return;
                _isProcessing[timerId] = true;
            }//end: lock

            // Read the batches in and then just keep doing it until no more batches to read
            while (!IsStopped && RunNext(timerId))
                Thread.SpinWait(0);

            lock (_sync)
            {
                // Reset back to not started, 'cause we're done and can go again, yay!
                _isProcessing[timerId] = false;
            }//end: lock
        }//end: timer_Elapsed

        #endregion

        #region Public Methods

        /// <summary>
        /// Starts the processing for all queue request sources.
        /// </summary>
        public static void StartProcessing()
        {
            Log.Info("StartProcessing requested");
            IsStopped = false;
            _timers = new ConcurrentBag<Timer>();
            // Call our work timer every 1 minute, starting after 10 seconds in order to ensure we're always doing something;
            //  but if this timer runs out of work, we're not going to be hammering SQS more than the number of SQS queues
            //  queries every minute (which shouldn't be too bad, of course the more parallel we get, the more the Network I/O gets hammered when there is
            //  no data to get).
            for (int i = 0; i < _threadCapacity; i++)
            {
                // Add the initial mining tracker
                _isProcessing.TryAdd(i, false);

                // Create our timer callback method
                TimerCallback cb = new TimerCallback(r =>
                {
                    int timerId = (int)(r ?? 0);
                    // Call our generic timer elapsed method.
                    //  We simply needed a wrapper here to encapsulate the source parameter
                    //  and pass that through as stateful info in the system marshaler.
                    timer_Elapsed(timerId);
                });
                // Initialize and add our timer that will elapse for working the queues
                //  We don't want to hammer the get next batch process all at once, so this
                //  staggers them out by 10 seconds for each concurrent thread so we're
                //  not kicking them all off at the same time.
                _timers.Add(new Timer(cb, i, 10000, 30000));
            }//end: foreach source

            Log.Info("Processing started");
        }//end: StartMining

        /// <summary>
        /// Stops the processing of all processesors.
        /// </summary>
        public static void StopProcessing()
        {
            Log.Info("StopProcessing requested");
            IsStopped = true;

            // Stop our timers from tickin' and dispose of them
            Parallel.ForEach(_timers, t =>
            {
                t.Change(Timeout.Infinite, Timeout.Infinite);
                t.Dispose();
            });
            // Set our collection to null, no need for it anymore
            _timers = null;

            var cleanup = Task.Factory.StartNew(() =>
            {
                bool isMining = _sync.Lock(() => _isProcessing.Values.Any(v => v));
                while (isMining)
                {
                    // It's still mining, so sleep on it for a split sec.
                    Thread.SpinWait(1);
                    // Now that we're up from our nap, check it out again for our loop.
                    isMining = _sync.Lock(() => _isProcessing.Values.Any(v => v));
                }
            });
            // Block call until we're actually done with the current mining call
            cleanup.Wait(25000); // Wait 25 seconds before throwing our hands up
            Log.Info("Processing stopped");
        }//end: StopMining

        #endregion

        #region Private Methods

        private static bool RunNext(int timerId)
        {
            try
            {
                Log.Info("Processing from timer {0}", timerId);

                ProcessResult result = ProcessResult.Retry;
                while (result == ProcessResult.Retry && !IsStopped)
                    result = _threadFunctions[timerId].Listen();

                Log.Info("Processing from timer {0} complete", timerId);
                return result == ProcessResult.Success;
            }
            catch (Exception ex)
            {
                // TODO: Proper error handling
                Log.Error(string.Format("Error on thread # {0}", timerId), ex);

                // We purposefully return false here because maybe we need to wait a minute rather than just hammering
                //  the same error over and over again.
                return false;
            }
        }//end: RunNextBatch


        #endregion
    }//end: class: Processor
}
