﻿using System.Collections.Generic;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Logic.Processing;

namespace AbsenceSoft.Process.MessageProcessing
{
    public interface IMessageProcessContainer
    {
        IEnumerable<IMessageProcessor> Get();
    }
}