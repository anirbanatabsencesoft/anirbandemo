﻿using AbsenceSoft.Logic.Processing;
using AbsenceSoft.Logic.Processing.CaseExport;
using AbsenceSoft.Logic.Processing.CaseRecalc;
using AbsenceSoft.Logic.Processing.EL;
using AbsenceSoft.Logic.Processing.RecalcFutureCases;
using AT.Logic.Reporting.Process;
using System;
using System.Collections.Generic;

namespace AbsenceSoft.Process.MessageProcessing
{
    internal class DefaultMessageProcessorContainer : IMessageProcessContainer
    {
        static List<IMessageProcessor> processors = new List<IMessageProcessor>
        {
            new ElMessageProcessor(),
            new CaseRecalcMessageProcessor(),
            new CaseExportMessageProcessor(),
            new ReportMessageProcessor(),
            new RecalcFutureCaseMessageProcessor()
        };

        public IEnumerable<IMessageProcessor> Get()
        {
            return processors;
        }
    }
}