﻿using AbsenceSoft.Common;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Logic.Processing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AbsenceSoft.Process.MessageProcessing
{
    /// <summary>
    /// Listens for and processes a single message
    /// from the specified QueueName.
    /// </summary>
    public class MessageQueueListener
    {
        private QueueService queueService = new QueueService();

        private readonly Func<bool> cancel;
        private readonly IMessageProcessContainer processors;
        private readonly string queueName;

        public MessageQueueListener(string queueName, Func<bool> cancel = null) : this(queueName, null, cancel) { }

        public MessageQueueListener(string queueName, IMessageProcessContainer messageProcessorContainer, Func<bool> cancel = null)
        {
            this.queueName = queueName;
            this.processors = messageProcessorContainer ?? new DefaultMessageProcessorContainer();
            this.cancel = cancel ?? new Func<bool>(() => true);
        }

        private TimerCallback GetTimerCallBack(QueueItem item)
        {
            return new TimerCallback(o =>
            {
                // We need to renew the queue item to ensure it doesn't get picked up again while we're successfully working with it.
                try
                {
                    Log.Info("Renewing queue item times for message: {0}", item);
                    using (QueueService qService = new QueueService())
                        qService.Renew(item, TimeSpan.FromSeconds(75));
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("Error renewing queue item: {0}:", item), ex);
                }
            });

        }

        internal ProcessResult Listen()
        {
            QueueItem item = null;
            IMessageProcessor currentProcessor = null;
            var renewCallback = GetTimerCallBack(item);
            try
            {
                item = WaitForQueueItem();
                if (item == null) return ProcessResult.Retry;

                Log.Debug("Processing message: {0}", item);

                using (Timer renewItem = new Timer(renewCallback, item, TimeSpan.FromSeconds(60), TimeSpan.FromSeconds(60)))
                {
                    return ProcessMessage(item, out currentProcessor);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error processing eligibility upload file or file part", ex);
                if (currentProcessor != null)
                {
                    currentProcessor.HandleException(item, ex);
                }
                //
                // Now we need to delete the item from the queue.
                if (item != null)
                {
                    queueService.Delete(item.QueueName, item.ReceiptHandle);
                }
                return ProcessResult.Failure;
            }
            finally
            {
                renewCallback = null;
            }

        }

        private ProcessResult ProcessMessage(QueueItem item, out IMessageProcessor currentProcessor)
        {
            currentProcessor = null;

            var cursor = processors.Get().GetEnumerator();
            var wasProcessed = false;
            while (cursor.MoveNext())
            {
                currentProcessor = cursor.Current;
                var context = new MessageProcessingContext();
                if (currentProcessor.IsProcessorFor(item))
                {
                    wasProcessed = true;
                    Log.Info("Processing message with IMessageProcessor: {0}", currentProcessor.GetType().Name);
                    currentProcessor.BeforeProcess(item, context);
                    if (context.EarlyCompletionResult.HasValue)
                    {
                        queueService.Delete(queueName, item.ReceiptHandle);
                        return context.EarlyCompletionResult.Value;
                    }

                    currentProcessor.Process(item, context);

                    currentProcessor.AfterProcess(item, context);
                }
            }
            if (!wasProcessed)
            {
                throw new AbsenceSoftException(string.Format("No processor handled message: {0}", item));
            }

            queueService.Delete(item.QueueName, item.ReceiptHandle);
            return ProcessResult.Success;
        }

        private QueueItem WaitForQueueItem()
        {
            QueueItem item = null;
            while (item == null)
            {
                if (cancel())
                {
                    break;
                }
                return queueService.Receive(queueName, 1).FirstOrDefault();
            }
            return item;
        }
    }
}
