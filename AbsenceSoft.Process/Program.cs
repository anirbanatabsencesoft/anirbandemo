﻿using System.ServiceProcess;

namespace AbsenceSoft.Process
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();
            new Aspose.Words.License().SetLicense("Aspose.Words.lic");
            Common.Serializers.DateTimeSerializer.Register();

            // Initialize Aspose PDF license object
            Aspose.Pdf.License license = new Aspose.Pdf.License();
            // Set license
            license.SetLicense("Aspose.Pdf.lic");
            // Set the value to indicate that license will be embedded in the application
            license.Embedded = true;

            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new ProcessService() 
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
