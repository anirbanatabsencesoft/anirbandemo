﻿namespace AbsenceSoft.Process
{
    partial class ProcessService
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.startupTimer = new System.Timers.Timer();
            ((System.ComponentModel.ISupportInitialize)(this.startupTimer)).BeginInit();
            // 
            // startupTimer
            // 
            this.startupTimer.AutoReset = false;
            this.startupTimer.Interval = 5000D;
            this.startupTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.startupTimer_Elapsed);
            // 
            // ProcessService
            // 
            this.ServiceName = "AbsenceSoftProcessService";
            ((System.ComponentModel.ISupportInitialize)(this.startupTimer)).EndInit();

        }

        #endregion

        private System.Timers.Timer startupTimer;
    }
}
