﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using AT.Provider.Cache.Base;
using System.Threading.Tasks;
using Moq;

namespace AT.Cacheic.Cache.Test
{
    [TestClass]
    public class CacheicCacheTest
    {
        public CacheicCacheTest()
        {
            Dictionary<string, string> Cache = new Dictionary<string, string>
            {
                { "Key1001", "Value1001" },
                { "Key1002", "Value1002" },
                { "Key1003", "Value1003" },
                { "Key1004", "Value1004" },
                { "Key1005", "Value1005" }
            };

            // Mock the Cache Repository using Moq
            Mock<BaseCacheProvider> mockCacheRepository = new Mock<BaseCacheProvider>();

            mockCacheRepository.Setup(mr => mr.Get<string>(
                                      It.IsAny<string>())).Returns(
                                            (string key) =>
                                            {
                                                if (string.IsNullOrEmpty(key))
                                                {
                                                    return "";
                                                }
                                                else
                                                {
                                                    string resultValue = string.Empty;
                                                    Cache.TryGetValue(key, out resultValue);
                                                    return resultValue;
                                                }
                                            }
                                        );

            mockCacheRepository.Setup(mr => mr.Delete(
                                     It.IsAny<string>())).Returns(
                                           (string key) =>
                                           {
                                               if (string.IsNullOrEmpty(key))
                                               {
                                                   return false;
                                               }
                                               else
                                               {
                                                   return Cache.Remove(key);
                                               }
                                           }
                                       );

            // Allows us to test saving a Cache
            mockCacheRepository.Setup(mr => mr.Set(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<TimeSpan>())).Returns(
                  (string key, dynamic value, TimeSpan expiry) =>
                  {
                      if (!string.IsNullOrEmpty(key) && value != null)
                      {
                          Cache.Add(key, value);
                          return true;
                      }
                      else
                      {
                          return false;
                      }
                  });

            // Complete the setup of our Mock Cache Repository
            this.MockCacheRepository = mockCacheRepository.Object;
        }


        /// <summary>
        /// Our Mock Cache Repository for use in testing
        /// </summary>
        public readonly BaseCacheProvider MockCacheRepository;

        [TestMethod]
        public void CanGetCacheByKey()
        {
            var dataResult = this.MockCacheRepository.Get<string>("Key1001");            
            Assert.IsInstanceOfType(dataResult, typeof(string));
            Assert.AreEqual("Value1001", dataResult);
        }

        [TestMethod]
        public void CanSetValueToCache()
        {
            // Create a new Cache item
            MockCacheRepository.Set("Key1006", "Value1006", TimeSpan.FromMilliseconds(15));

            // verify that our new Cache has been saved
            var CacheResult = this.MockCacheRepository.Get<string>("Key1006");
            Assert.AreEqual("Value1006", CacheResult);
        }


        [TestMethod]
        public void CanDeleteCacheValue()
        {
            // Create a new Cache item
            MockCacheRepository.Delete("Key1005");
            Assert.AreEqual(false, MockCacheRepository.Exists("Key1005"));
        }
    }
}
