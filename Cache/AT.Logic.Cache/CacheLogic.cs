﻿
using AbsenceSoft.Common.Properties;
using AT.Provider.Cache.Base;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Cache
{
    public class CacheLogic
    {
        /// <summary>
        /// The internal provider
        /// </summary>
        readonly BaseCacheProvider CacheProvider;
        private readonly string CacheEnvironment;

        public CacheLogic()
        {
            CacheProvider = CacheProviderManager.Default;
            CacheEnvironment = GetEnvironmentName();
        }

        private string GetEnvironmentName()
        {
            string name = Settings.Default.BaseUrl;
            if (!string.IsNullOrWhiteSpace(name))
            {
                try
                {
                    Uri baseUri = new Uri(name);
                    return baseUri.Host.Split('.').Length > 0 ? baseUri.Host.Split('.')[0] : string.Empty;
                }
                catch
                {
                    return string.Empty;
                }
            }
            return string.Empty;

        }

        /// <summary>
        /// Will check for Existence of a cache item with passed key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool Exists(string key)
        {
            return CacheProvider.Exists(CacheEnvironment + "_"+key);

        }

        /// <summary>
        /// Gets the cache item of type T from the Cache of given key
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T Get<T>(string key)
        {
            return CacheProvider.Get<T>(CacheEnvironment + "_" + key);
        }

        /// <summary>
        /// Adds the cache item of type T with given key, expiry is used to set for the Timespan the item has to be maintained in cache
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="expiry"></param>
        public bool Set<T>(string key, T value, TimeSpan? expiry = null)
        {
            return CacheProvider.Set<T>(CacheEnvironment + "_" + key, value, expiry);
        }

        /// <summary>
        /// Deletes the cache item of type T with given key
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        public bool Delete(string key)
        {
            return CacheProvider.Delete(CacheEnvironment + "_" + key);
        }
    }
}
