﻿using AT.Common.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Cache
{
    /// <summary>
    /// Helper class for cache get/set/clear
    /// </summary>
    public static class CacheHelper
    {
        //define the readonly logic object
        static readonly CacheLogic logicObject = new CacheLogic();

        /// <summary>
        /// Helper method to check whether a cache object exists or not
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool Exists(string key)
        {
            try
            {
                return logicObject.Exists(key);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        /// <summary>
        /// Returns the cache object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T Get<T>(string key)
        {
            try
            {
                return logicObject.Get<T>(key);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return default(T);
            }
        }

        /// <summary>
        /// Set a cache for a defined time in seconds
        /// </summary>
        /// <param name="key"></param>
        /// <param name="timeInSeconds">For how much second we want to cache the object</param>
        public static void Set<T>(string key, T value, int timeInSeconds = 600)
        {
            try
            {
                //remove if value is null
                if (value == null)
                {
                    Delete(key);
                }

                logicObject.Set<T>(key, value, new TimeSpan(0, 0, timeInSeconds));
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        /// <summary>
        /// Delete a cache object
        /// </summary>
        /// <param name="key"></param>
        public static void Delete(string key)
        {
            try
            {
                logicObject.Delete(key);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }
    }
}
