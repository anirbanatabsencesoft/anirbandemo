﻿using AT.Common.Log;
using AT.Provider.Cache.Base;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StackExchange.Redis;
using StackExchange.Redis.Extensions.Core;
using StackExchange.Redis.Extensions.Newtonsoft;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace AT.Provider.Cache
{
    /// <summary>
    /// Redis Cache provider used to save or fetch data from RedisCache
    /// </summary>
    public class RedisCacheProvider : BaseCacheProvider
    {
        #region Properties
        /// <summary>
        /// Holds the EndPoint IP Address of Cache Server
        /// </summary>
        protected static string EndPoint { get; set; }

        /// <summary>
        /// Password to connect the Cache Server Endpoint
        /// </summary>
        protected static string Password { get; set; }

        /// <summary>
        /// Used to check the no. of retries before exiting retry attempts 
        /// </summary>
        protected static int RetryCount { get; set; }

        /// <summary>
        /// Conn is ConnectionMultiplexer, returns lazy ConnectionMultiplexer
        /// </summary>
        public ConnectionMultiplexer Conn
        {
            get
            {
                return LazyConnection.Value;
            }
        }

        /// <summary>
        /// DBInstance is the database instance of the Cache Server
        /// </summary>
        private IDatabase DBInstance
        {
            get; set;
        }

        /// <summary>
        /// Client of Cache, used to connect and do operation with Cache Server
        /// </summary>
        private ICacheClient RedisCacheClient { get; set; }

        /// <summary>
        /// Is the Lazy ConnectionMultiplexer, reading the configuration settings and builds connection accordingly
        /// </summary>
        protected static readonly Lazy<ConnectionMultiplexer> LazyConnection = new Lazy<ConnectionMultiplexer>(() =>
        {
            if (String.IsNullOrWhiteSpace(EndPoint))
            {
                return null;
            }

            int defaultSyncTimeout;
            Int32.TryParse(ConfigurationManager.AppSettings["CacheSyncTimeout"], out defaultSyncTimeout);
            var CacheSyncTimeout = defaultSyncTimeout == 0 ? 20000 : defaultSyncTimeout;

            int defaultConnectionTimeOut;
            Int32.TryParse(ConfigurationManager.AppSettings["CacheConnectTimeout"], out defaultConnectionTimeOut);
            var CacheConnectTimeout = defaultConnectionTimeOut == 0 ? 20000 : defaultConnectionTimeOut;

            ConfigurationOptions config = new ConfigurationOptions();
            config.EndPoints.Add(EndPoint);
            if (!string.IsNullOrWhiteSpace(Password))
            {
                config.Password = Password;
            }
            config.ConnectRetry = RetryCount; // retry connection if broken
            config.KeepAlive = 30; // keep connection alive (ping every minute)
            config.Ssl = true;

            //Time (ms) to allow for synchronous operations
            config.SyncTimeout = CacheSyncTimeout;

            //Timeout (ms) for connect operations
            config.ConnectTimeout = CacheConnectTimeout;
            config.AbortOnConnectFail = false;
            config.AllowAdmin = true;

            return ConnectionMultiplexer.Connect(EndPoint);
        });

        #endregion

        /// <summary>
        /// Checks if the connection is still active.
        /// </summary>
        /// <returns>boolean</returns>
        public bool IsAlive()
        {
            try
            {
                return this.Conn != null && this.DBInstance != null && this.DBInstance.Ping() != null;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        /// <summary>
        /// Checks if the key exists in the Cache
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public override bool Exists(string key)
        {
            return DBInstance.KeyExists(key);
        }

        /// <summary>
        /// Returns Generic object from the cache for the provided key
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public override T Get<T>(string key)
        {
            try
            {
                return RedisCacheClient.Get<T>(key);
            }
            catch
            {
                return default(T);
            }
        }

        /// <summary>
        /// Sets the provided value to Cache with given key
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public override bool Set<T>(string key, T value, TimeSpan? expiry = null)
        {
            return SaveToCache(key, value, expiry);
        }

        /// <summary>
        /// Deletes the value of provided key from cache
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        public override bool Delete(string key)
        {
            try
            {
                if (IsAlive() && Exists(key))
                {
                    return DBInstance.KeyDelete(key);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        /// <summary>
        /// Set the parameter with corresponding config values
        /// </summary>
        /// <param name="config"></param>
        public override void SetParameters(System.Collections.Specialized.NameValueCollection config)
        {
            EndPoint = config["endPoint"];
            Password = config["password"];
                        
            RetryCount = Convert.ToInt32(config["retryCount"]);

            Init();
        }

        #region cache operations
        /// <summary>
        /// Retrieve value from cache based on provided key value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cachename"></param>
        /// <returns></returns>
        private bool SaveToCache<T>(string cachename, T t, TimeSpan? expiry = null)
        {
            return RedisCacheClient.Add(cachename, t, expiry ?? TimeSpan.FromHours(1));
        }

        #endregion

        #region Initially Cached
        private void Init()
        {
            if (Conn != null)
            {
                var serializer = new NewtonsoftSerializer();
                RedisCacheClient = new StackExchangeRedisCacheClient(Conn, serializer);

                DBInstance = RedisCacheClient.Database;
            }
        }
        #endregion
    }
}
