﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Provider.Cache.Base
{
    /// <summary>
    /// This configuration would provide all the configured Providers and Default Provider
    /// </summary>
    public class CacheProviderConfiguration : ConfigurationSection
    {
        /// <summary>
        /// Contains collection of Providers
        /// </summary>
        [ConfigurationProperty("providers")]
        public ProviderSettingsCollection Providers
        {
            get
            {
                return (ProviderSettingsCollection)base["providers"];
            }
        }

        /// <summary>
        /// Shows the Default Provider's Name
        /// </summary>
        [ConfigurationProperty("default", DefaultValue = "RedisCache")]
        public string DefaultProviderName
        {
            get
            {
                return base["default"] as string;
            }
        }
    }
}
