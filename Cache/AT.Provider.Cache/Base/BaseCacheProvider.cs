﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Provider.Cache.Base
{
    /// <summary>
    /// Base class to be inherited by all the Cache providers to save or fetch data from Provider.
    /// </summary>
    public abstract class BaseCacheProvider : ProviderBase
    {
        /// <summary>
        /// maintain the description locally 
        /// </summary>
        protected string ProviderDescription;

        /// <summary>
        /// Property description of the provider, for outside world
        /// </summary>
        public override string Description
        {
            get { return ProviderDescription; }
        }

        /// <summary>
        /// Will check for Existence of a cache item with passed key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public abstract bool Exists(string key);

        /// <summary>
        /// Gets the cache item of type T from the Cache of given key
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public abstract T Get<T>(string key);

        /// <summary>
        /// Adds the cache item of type T with given key, expiry is used to set for the Timespan the item has to be maintained in cache
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="expiry"></param>
        public abstract bool Set<T>(string key, T value, TimeSpan? expiry = null);

        /// <summary>
        /// Deletes the cache item of type T with given key
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        public abstract bool Delete(string key);

        /// <summary>
        /// Sets the Parameters of given NameValueCollection to respective properties for the CacheProvider to be configured reading config values
        /// </summary>
        /// <param name="config"></param>
        public abstract void SetParameters(NameValueCollection config);

    }
}
