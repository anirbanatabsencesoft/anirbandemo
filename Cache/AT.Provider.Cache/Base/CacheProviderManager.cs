﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace AT.Provider.Cache.Base
{
    public static class CacheProviderManager
    {
        #region private variables
        private static BaseCacheProvider DefaultProvider;
        private static ProviderSettingsCollection ProviderSettingsColl;
        private static CacheProviderCollection ProviderCollection;
        #endregion

        static CacheProviderManager()
        {
            Initialize();
        }

        /// <summary>
        /// Provider settings collection
        /// </summary>
        public static ProviderSettingsCollection ProviderSettings
        {
            get { return ProviderSettingsColl; }
        }

        /// <summary>
        /// Contains all the Providers configured
        /// </summary>
        public static CacheProviderCollection Providers
        {
            get
            {
                return ProviderCollection;
            }
        }

        /// <summary>
        /// Returns the default configured cache provider
        /// </summary>
        public static BaseCacheProvider Default
        {
            get { return DefaultProvider; }
        }

        /// <summary>
        /// Fetches the configuration information and populates cacheProviderCollection,
        /// </summary>
        private static void Initialize()
        {
            CacheProviderConfiguration configSection = (CacheProviderConfiguration)ConfigurationManager.GetSection("CacheProviders");
            if (configSection == null)
                throw new ConfigurationErrorsException("Cache provider section is not set.");

            ProviderCollection = new CacheProviderCollection();
            ProvidersHelper.InstantiateProviders(configSection.Providers, ProviderCollection, typeof(BaseCacheProvider));

            ProviderSettingsColl = configSection.Providers;

            if (ProviderCollection[configSection.DefaultProviderName] == null)
                throw new ConfigurationErrorsException("Default data provider is not set.");
            DefaultProvider = ProviderCollection[configSection.DefaultProviderName];
            var defaultSettings = ProviderSettingsColl[configSection.DefaultProviderName];

            DefaultProvider.SetParameters(defaultSettings.Parameters);
        }
    }
}
