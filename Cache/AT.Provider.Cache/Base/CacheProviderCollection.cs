﻿using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Provider.Cache.Base
{
    /// <summary>
    /// Colletion of available Providers configured
    /// </summary>
    public class CacheProviderCollection : ProviderCollection
    {
        /// <summary>
        /// Returns Provider object of given Provider name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        new public BaseCacheProvider this[string name]
        {
            get
            {
                return (BaseCacheProvider)base[name];
            }
        }
    }
}
