﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AT.Api.Cache.Controllers;
using System.Diagnostics;
using System.Threading;
using AT.Api.Core;
using System.Collections.Generic;
using Moq;
using System.Threading.Tasks;

namespace AT.Api.Cache.Test
{
    [TestClass]
    public class CacheApiTest
    {
        /// <summary>
        /// Our Mock Cache Repository for use in testing
        /// </summary>
        public readonly ICacheController MockCacheRepository;
        
        /// <summary>
        /// Constructor to set the controller
        /// </summary>
        public CacheApiTest()
        {
            
            Dictionary<string, string> Cache = new Dictionary<string, string>
            {
                { "Key1001", "Value1001" },
                { "Key1002", "Value1002" },
                { "Key1003", "Value1003" },
                { "Key1004", "Value1004" },
                { "Key1005", "Value1005" }
            };

            // Mock the Cache Repository using Moq
            Mock<ICacheController> mockCacheRepository = new Mock<ICacheController>();

            mockCacheRepository.Setup(mr => mr.Get(
                                      It.IsAny<string>())).Returns(
                                            async (string key) =>
                                            {
                                                if (string.IsNullOrEmpty(key))
                                                {
                                                    return await Task.FromResult(Cache);
                                                }
                                                else
                                                {
                                                    string resultValue = string.Empty;
                                                    Cache.TryGetValue(key, out resultValue);
                                                    return await Task.FromResult(resultValue);
                                                }
                                            }
                                        );

            mockCacheRepository.Setup(mr => mr.Delete(
                                     It.IsAny<string>())).Returns(
                                           (string key) =>
                                           {
                                               if (string.IsNullOrEmpty(key))
                                               {
                                                   return Task.FromResult(false);
                                               }
                                               else
                                               {
                                                   return Task.FromResult(Cache.Remove(key));
                                               }
                                           }
                                       );

            // Allows us to test saving a Cache
            mockCacheRepository.Setup(mr => mr.Set(It.IsAny<string>(), It.IsAny<string>())).Returns(
                  (string key, dynamic value) =>
                  {
                      if (!string.IsNullOrEmpty(key) && value != null)
                      {
                          Cache.Add(key, value);
                          return Task.FromResult(true);
                      }
                      else
                      {
                          return Task.FromResult(false);
                      }
                  });

            // Complete the setup of our Mock Cache Repository
            this.MockCacheRepository = mockCacheRepository.Object;
        }
        
        [TestMethod]
        public void TestSetCachePositive()
        {
            //   Check count before saving
            var dataResult = this.MockCacheRepository.Get(null);
            int CacheCount = dataResult.Result.Count;
            Assert.AreEqual(5, CacheCount);

            // Create a new Cache item
            var IsKeySet = MockCacheRepository.Set("Key1006", "Value1006").Result;
            Assert.IsTrue(IsKeySet);

            // demand a recount
            var getAllResult = this.MockCacheRepository.Get(null);
            CacheCount = getAllResult.Result.Count;
            Assert.AreEqual(6, CacheCount);

            // verify that our new Cache has been saved
            var CacheResult = this.MockCacheRepository.Get("Key1006").Result;
            Assert.AreEqual("Value1006", CacheResult);
        }

        [TestMethod]
        public void TestSetCacheNegative()
        {
            // Create a new Cache item as blank key and value
            var result = MockCacheRepository.Set("", "").Result;
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestGetCachePositive()
        {
            var dataResult = MockCacheRepository.Get("Key1001");
            Assert.IsInstanceOfType(dataResult.Result, typeof(string));
            Assert.AreEqual("Value1001", dataResult.Result);
        }

        [TestMethod]
        public void TestGetCacheNegative()
        {
            var dataResult = MockCacheRepository.Get("Key3001").Result;
            Assert.IsNull(dataResult);
        }
        
        [TestMethod]
        public void TestDeleteCachePositive()
        {
            //   Check count before saving
            var dataResult = this.MockCacheRepository.Get(null);
            int CacheCount = dataResult.Result.Count;
            Assert.AreEqual(5, CacheCount);

            // Create a new Cache item
            MockCacheRepository.Delete("Key1005");

            // demand a recount
            var getAllResult = this.MockCacheRepository.Get(null);
            CacheCount = getAllResult.Result.Count;
            Assert.AreEqual(4, CacheCount);
        }

        [TestMethod]
        public void TestDeleteCacheNegative()
        {
            //   Check count before saving
            var dataResult = this.MockCacheRepository.Get(null);
            int CacheCount = dataResult.Result.Count;
            Assert.AreEqual(5, CacheCount);

            // Create a new Cache item
            MockCacheRepository.Delete("Key1006");

            // demand a recount
            var getAllResult = this.MockCacheRepository.Get(null);
            CacheCount = getAllResult.Result.Count;
            Assert.AreNotEqual(4, CacheCount);
        }
        
    }
}
