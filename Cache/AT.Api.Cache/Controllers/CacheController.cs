﻿using AT.Api.Core;
using AT.Common.Log;
using AT.Common.Security;
using AT.Logic.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel.Channels;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace AT.Api.Cache.Controllers
{
    /// <summary>
    /// This is for cache usage from JS
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CacheController : BaseApiController
    {
        readonly CacheLogic _logic;

        /// <summary>
        /// The constructor to set logic object
        /// </summary>
        public CacheController()
        {
            _logic = new CacheLogic();
        }

        /// <summary>
        /// Gets the Value from cache for given key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpGet, Secure]
        [Route("cache/{key}")]
        public async Task<string> Get(string key)
        {
            if (AuthenticatedUser == null)
            {
                return await Task.FromResult("Unauthorized!");
            }

            if (!string.IsNullOrEmpty(key))
            {
                return await Task.FromResult(_logic.Get<string>(key));
            }
            else
            {
                var ErrorMessage = string.Format("Invalid key {0} passed", key ?? "");
                LogMessage(ErrorMessage);
                return "";
            }
        }

        /// <summary>
        /// Set the cache with passed values of type T and identified by key
        /// </summary>
        /// <param name="key"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        [HttpPost, Secure]
        [Route("cache/set")]
        public async Task<bool> Set(string key, string values)
        {
            if (AuthenticatedUser == null)
            {
                return await Task.FromResult(false);
            }

            if (!string.IsNullOrEmpty(key) && values != null)
            {
                return await Task.FromResult(_logic.Set<string>(key, values));
            }
            else
            {
                var ErrorMessage = string.Format("Invalid request with key {0} and values {1}.", key ?? "", values ?? "");
                LogMessage(ErrorMessage);
                return Task.FromResult(false).Result;
            }
        }

        /// <summary>
        /// Checks for existence of cache item with given key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpGet, Secure]
        [Route("cache/exists/{key}")]
        public async Task<bool> Exists(string key)
        {
            if (AuthenticatedUser == null)
            {
                return await Task.FromResult(false);
            }

            if (!string.IsNullOrEmpty(key))
            {
                return await Task.FromResult(_logic.Exists(key));
            }
            else
            {
                var ErrorMessage = string.Format("Invalid key {0} passed", key);
                LogMessage(ErrorMessage);
                return Task.FromResult(false).Result;
            }
        }

        /// <summary>
        /// Deletes the cache item with given key
        /// </summary>
        /// <param name="key"></param>
        [HttpDelete, Secure]
        [Route("cache/delete/{key}")]
        public async Task<bool> Delete(string key)
        {
            if (AuthenticatedUser == null)
            {
                return await Task.FromResult(false);
            }

            if (!string.IsNullOrEmpty(key))
            {
                return await Task.FromResult(_logic.Delete(key));
            }
            else
            {
                var ErrorMessage = string.Format("Invalid key {0} passed.", key);
                LogMessage(ErrorMessage);
                return Task.FromResult(false).Result;
            }
        }

        /// <summary>
        /// Log the error
        /// </summary>
        /// <param name="Message"></param>
        /// <returns></returns>
        private void LogMessage(string Message)
        {
            Logger.Error(Message);
        }

    }
}