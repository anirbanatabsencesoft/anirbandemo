using System.Web.Http;
using WebActivatorEx;
using AT.Api.Cache;
using Swashbuckle.Application;
using AT.Api.Core;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]
namespace AT.Api.Cache
{
    /// <summary>
    /// Register the swagger documentation
    /// </summary>
    public static class SwaggerConfig
    {
        /// <summary>
        /// Register the current assembly in to swagger
        /// </summary>
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;
            BaseSwaggerConfig.Register("Cache", thisAssembly);
        }
    }
}
