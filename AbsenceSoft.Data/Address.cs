﻿using AbsenceSoft.Common;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AbsenceSoft.Data
{
    /// <summary>
    /// The address class is NOT stored separetly in the DB, it is always a child document of another type and
    /// represents a physical address or location.
    /// </summary>
    [Serializable]
    public class Address : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Address"/> class.
        /// </summary>
        public Address()
        {
            Country = "US";
        }

        /// <summary>
        /// Gets or sets the name of the address (optional). This is for reference only
        /// and is not part of the actual address.
        /// </summary>
        [BsonIgnoreIfNull]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the street number and street name, house name, or other international
        /// address line 1 format required.
        /// </summary>
        [DisplayName("Address 1")]
        [AngularRequired]
        [AngularInputType(HTML5InputType.Text)]
        public string Address1 { get; set; }

        /// <summary>
        /// Gets or sets the 2nd address line such as a unit number, apartment or other information
        /// for international addresses.
        /// </summary>
        [BsonIgnoreIfNull]
        [DisplayName("Address 2")]
        [AngularInputType(HTML5InputType.Text)]
        public string Address2 { get; set; }

        /// <summary>
        /// Gets or sets the address' city.
        /// </summary>
        [DisplayName("City")]
        [AngularRequired]
        [AngularInputType(HTML5InputType.Text)]
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the address' state or province.
        /// </summary>
        [DisplayName("State")]
        public string State { get; set; }

        /// <summary>
        /// Gets or the employee's work statefor workflow rule expression.
        /// </summary>
        [BsonIgnore]
        public string StateUS { get { return State; } }

        /// <summary>
        /// Gets or sets the address' mailing postal code, in the US this is the ZIP Code.
        /// </summary>
        [DisplayName("Postal Code")]
        [AngularRequired]
        [AngularInputType(HTML5InputType.Text)]
        [RegularExpression(@"^\d{5}(?:-\d{4})?$", ErrorMessage = "Postal Code should be in the format XXXXX-XXXX.")]
        public string PostalCode { get; set; }

        /// <summary>
        /// Gets or sets the country's 2-digit ISO code.
        /// </summary>
        [BsonDefaultValue("US")]
        [DisplayName("Country")]
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the street address.
        /// </summary>
        /// <value>
        /// The street address.
        /// </value>
        [BsonIgnore]
        public string StreetAddress
        {
            get
            {
                return string.Concat(
                    Address1, 
                    string.IsNullOrWhiteSpace(Address2) || string.IsNullOrWhiteSpace(Address1) ? "" : ", ", 
                    Address2);
            }
            set { }
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            StringBuilder address = new StringBuilder();
            if (!string.IsNullOrWhiteSpace(Address1))
                address.AppendLine(Address1);
            if (!string.IsNullOrWhiteSpace(Address2))
                address.AppendLine(Address2);

            StringBuilder csz = new StringBuilder();
            if (!string.IsNullOrWhiteSpace(City))
                csz.Append(City);
            if (!string.IsNullOrWhiteSpace(State))
                csz.AppendFormat("{0}{1}", csz.Length > 0 ? ", " : "", State);
            if (!string.IsNullOrWhiteSpace(PostalCode))
                csz.AppendFormat("{0}{1}", csz.Length > 0 ? " " : "", PostalCode);
            if (csz.Length > 0)
                address.AppendLine(csz.ToString());

            if (!string.IsNullOrWhiteSpace(Country) && Country != "US")
                address.AppendLine(Country);

            return address.ToString();
        }
    }
}
