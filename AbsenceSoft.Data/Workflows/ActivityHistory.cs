﻿using AbsenceSoft.Data.Audit;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Workflows
{
    [Serializable, AuditClassNo]
    public class ActivityHistory : BaseEmployerEntity<ActivityHistory>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ActivityHistory"/> class.
        /// </summary>
        public ActivityHistory() { }

        /// <summary>
        /// Gets or sets the workflow instance identifier.
        /// </summary>
        /// <value>
        /// The workflow instance identifier.
        /// </value>
        [BsonRequired]
        [BsonRepresentation(BsonType.ObjectId)]
        public string WorkflowInstanceId { get; set; }

        /// <summary>
        /// Gets or sets the workflow identifier.
        /// </summary>
        /// <value>
        /// The workflow identifier.
        /// </value>
        [BsonRequired]
        [BsonRepresentation(BsonType.ObjectId)]
        public string WorkflowId { get; set; }

        /// <summary>
        /// Gets or sets the activity identifier.
        /// </summary>
        /// <value>
        /// The activity identifier.
        /// </value>
        public string ActivityId { get; set; }

        /// <summary>
        /// Gets or sets the name of the activity.
        /// </summary>
        /// <value>
        /// The name of the activity.
        /// </value>
        public string ActivityName { get; set; }

        /// <summary>
        /// Gets or sets the type of the activity.
        /// </summary>
        /// <value>
        /// The type of the activity.
        /// </value>
        public ActivityType ActivityType { get; set; }

        /// <summary>
        /// Gets or sets the workflow activity identifier.
        /// </summary>
        /// <value>
        /// The workflow activity identifier.
        /// </value>
        public Guid WorkflowActivityId { get; set; }

        /// <summary>
        /// Gets or sets the outcome.
        /// </summary>
        /// <value>
        /// The outcome.
        /// </value>
        public string Outcome { get; set; }

        /// <summary>
        /// Gets or sets the workflow running state as
        /// of the time the activity was completed.
        /// </summary>
        /// <value>
        /// The workflow running state at the time the
        /// activity was completed.
        /// </value>
        [AuditPropertyNo]
        public DynamicAwesome State { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is waiting on user input.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is waiting on user input; otherwise, <c>false</c>.
        /// </value>
        public bool IsWaitingOnUserInput { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance is in error.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is in error; otherwise, <c>false</c>.
        /// </value>
        [BsonIgnoreIfDefault]
        public bool IsError { get { return Outcome == Activity.ErrorOutcomeValue; } }

        /// <summary>
        /// Gets or sets the error associated with this instance.
        /// </summary>
        /// <value>
        /// The error associated with this instance.
        /// </value>
        [BsonIgnoreIfNull]
        public string Error { get; set; }

        /// <summary>
        /// Gets or sets the workflow activity history's case identifier, if any.
        /// </summary>
        /// <value>
        /// The case identifier.
        /// </value>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the workflow activity history's employee identifier, if any.
        /// </summary>
        /// <value>
        /// The employee identifier.
        /// </value>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the workflow.
        /// </summary>
        /// <value>
        /// The workflow.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public Workflow Workflow
        {
            get { return base.GetReferenceValue<Workflow>(this.WorkflowId); }
            set { this.WorkflowId = base.SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the workflow instance.
        /// </summary>
        /// <value>
        /// The workflow instance.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public WorkflowInstance WorkflowInstance
        {
            get { return base.GetReferenceValue<WorkflowInstance>(this.WorkflowInstanceId); }
            set { this.WorkflowInstanceId = base.SetReferenceValue(value); }
        }
    }
}
