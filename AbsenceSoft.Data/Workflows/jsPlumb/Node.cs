﻿using AbsenceSoft.Data.Rules;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Workflows.jsPlumb
{
    public class Node: BaseNonEntity
    {
        public Node(WorkflowActivity a, string type, int top, int left)
        {
            this.Text = a.Name;
            this.ActivityId = a.ActivityId;
            this.Type = type.ToLowerInvariant();
            this.Top = top;
            this.Left = left;
            this.Id = a.Id;
            this.Metadata = a.Metadata ?? new BsonDocument();
            this.Width = (int)((Math.Floor(this.Text.Length / 15D) + 1) * 120);
            this.Rules = a.Rules;
        }

        [JsonProperty("text")]
        public string Text { get; set; }
        
        [JsonProperty("activityId")]
        public string ActivityId { get; set; }
        
        [JsonProperty("w")]
        public int Width { get; private set; }

        [JsonProperty("h")]
        public int Height { get { return 80; } }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("top")]
        public int Top { get; set; }

        [JsonProperty("left")]
        public int Left { get; set; }

        [JsonProperty("id")]
        public override Guid Id { get; set; }

        [BsonIgnoreIfNull]
        public RuleGroup Rules { get; set; }
    }
}
