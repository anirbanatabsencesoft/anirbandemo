﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Workflows.jsPlumb
{
    public class jsPlumbContainer
    {
        public jsPlumbContainer()
        {
            this.Nodes = new List<Node>();
            this.Edges = new List<Edge>();
        }

        [JsonProperty("nodes")]
        public List<Node> Nodes { get; set; }

        [JsonProperty("edges")]
        public List<Edge> Edges { get; set; }
    }
}
