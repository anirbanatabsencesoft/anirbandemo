﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Workflows.jsPlumb
{
    public class EdgeData
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }
        
        [JsonProperty("type")]
        public string Type { get { return "connection"; } }

        [JsonProperty("outcome")]
        public string Outcome { get; set; }
    }
}
