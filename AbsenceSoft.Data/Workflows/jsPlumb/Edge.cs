﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Workflows.jsPlumb
{
    public class Edge
    {
        public Edge(Transition t)
        {
            this.Source = t.SourceActivityId;
            this.Target = t.TargetActivityId;
            this.Data = new EdgeData()
            {
                Id = t.Id,
                Outcome = t.ForOutcome
            };
        }

        [JsonProperty("source")]
        public Guid? Source { get; set; }

        [JsonProperty("target")]
        public Guid Target { get; set; }

        [JsonProperty("data")]
        public EdgeData Data { get; set; }

        
    }
}
