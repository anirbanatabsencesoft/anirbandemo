﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Workflows
{
    [Serializable]
    public abstract class Activity
    {
        /// <summary>
        /// The error outcome value
        /// </summary>
        public const string ErrorOutcomeValue = "ERROR";
        public const string CompleteOutcomeValue = "Complete";

        /// <summary>
        /// Gets the activity identifier.
        /// </summary>
        /// <value>
        /// The activity identifier.
        /// </value>
        public abstract string Id { get; }

        /// <summary>
        /// Gets the name of the activity.
        /// </summary>
        /// <value>
        /// The name of the activity.
        /// </value>
        public abstract string Name { get; }

        /// <summary>
        /// Gets the category of the activity.
        /// </summary>
        /// <value>
        /// The category of the activity.
        /// </value>
        public abstract string Category { get; }

        /// <summary>
        /// Gets the activity description.
        /// </summary>
        /// <value>
        /// The activity description.
        /// </value>
        public abstract string Description { get; }

        /// <summary>
        /// Gets the UI Template
        /// </summary>
        /// <value>
        /// The activities UI template
        /// </value>
        public virtual string Template { get { return "Action"; } }

        /// <summary>
        /// This will tell if particulat activity has to run first. 
        /// </summary>
        public virtual bool IsRunFirst { get; }

        /// <summary>
        /// Gets the type of the activity.
        /// </summary>
        /// <value>
        /// The type of the activity.
        /// </value>
        public abstract ActivityType ActivityType { get; }

        /// <summary>
        /// Gets the possible outcomes for the activity. If no outcomes are
        /// returned, then this is a terminating action. Default is only 2, Complete and ERROR.
        /// </summary>
        /// <returns>A collection of possible outcomes, codes or names, ids, etc. that
        /// uniquely identify each outcome an activity may have.</returns>
        public virtual IEnumerable<string> GetPossibleOutcomes()
        {
            yield return CompleteOutcomeValue;
            yield return ErrorOutcomeValue;
        }

        /// <summary>
        /// Gets the event types that this activity can be configured for
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<EventType> GetPossibleEventTypes()
        {
            List<EventType> possibleEventTypes = Enum.GetValues(typeof(EventType)).Cast<EventType>().ToList();
            return possibleEventTypes;
        }

        /// <summary>
        /// Returns the EventTypes that a case can respond to
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<EventType> GetCaseEventTypes()
        {
            yield return EventType.InquiryCreated;
            yield return EventType.InquiryAccepted;
            yield return EventType.InquiryClosed;
            yield return EventType.CaseCreated;
            yield return EventType.CaseClosed;
            yield return EventType.CaseCanceled;
            yield return EventType.CaseAdjudicated;
            yield return EventType.CaseExtended;
            yield return EventType.CaseReducedScheduleChanged;
            yield return EventType.CaseTypeChanged;
            yield return EventType.CaseReopened;
            yield return EventType.AccommodationCreated;
            yield return EventType.AccommodationChanged;
            yield return EventType.AccommodationDeleted;
            yield return EventType.AccommodationAdjudicated;
            yield return EventType.TimeOffRequested;
            yield return EventType.TimeOffAdjudicated;
            yield return EventType.PolicyExhausted;
            yield return EventType.PolicyAdjudicated;
            yield return EventType.PolicyManuallyAdded;
            yield return EventType.PolicyManuallyDeleted;
            yield return EventType.AttachmentCreated;
            yield return EventType.AttachmentDeleted;
            yield return EventType.CommunicationSent;
            yield return EventType.CommunicationDeleted;
            yield return EventType.CommunicationNoteCreated;
            yield return EventType.CommunicationNoteChanged;
            yield return EventType.PaperworkSent;
            yield return EventType.CaseNoteCreated;
            yield return EventType.CaseNoteChanged;
            yield return EventType.ToDoItemCreated;
            yield return EventType.ToDoItemCompleted;
            yield return EventType.ToDoItemChanged;
            yield return EventType.ToDoItemCanceled;
            yield return EventType.ToDoItemDeleted;
            yield return EventType.PayPeriodSubmitted;
            yield return EventType.CertificationCreated;
            yield return EventType.CertificationsChanged;
            yield return EventType.DiagnosisCreated;
            yield return EventType.DiagnosisChanged;
            yield return EventType.DiagnosisDeleted;
            yield return EventType.WorkRestrictionCreated;
            yield return EventType.WorkRestrictionChanged;
            yield return EventType.EmployeeJobChanged;
            yield return EventType.EmployeeJobApproved;
            yield return EventType.EmployeeJobDenied;
            yield return EventType.EmployeeNecessityChanged;
            yield return EventType.WorkRelatedReportingChanged;
            yield return EventType.EmployeeConsultCreated;
            yield return EventType.CaseCustomFieldChanged;
            yield return EventType.CaseRelapsed;
            yield return EventType.Manual;
        }

        /// <summary>
        /// Returns the possible outcomes.  The is solely for serialization purposes
        /// </summary>
        [BsonIgnore]
        public List<string> PossibleOutcomes
        {
            get { return GetPossibleOutcomes().ToList(); }
        }

        /// <summary>
        /// Determines whether the current activity is valid to run given the workflow instance, activity instance and user.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns><c>true</c> if valid to run, otherwise <c>false</c>.</returns>
        public abstract bool IsValidToRun(WorkflowInstance wf, WorkflowActivity activity, User user = null);

        /// <summary>
        /// Runs the specified activity on the provided workflow instance.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        public abstract ActivityHistory Run(WorkflowInstance wf, WorkflowActivity activity, User user = null);

        /// <summary>
        /// Completes the specified activity; generally called after Run if no outcome is specified; meaning
        /// there is another completion step necessary, often after user input (like completing a ToDo, clicking a button,
        /// confirming some shit, etc.).
        /// </summary>
        /// <param name="activity">The activity history record that is to be completed/finished.</param>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>An activity history result, <c>saved</c>, with the given outcome for the activity.</returns>
        public abstract ActivityHistory Complete(ActivityHistory activity, WorkflowInstance wf, User user = null);

        /// <summary>
        /// Runs the end.
        /// </summary>
        /// <param name="wf">The wf.</param>
        /// <param name="activity">The activity.</param>
        /// <param name="user">The user.</param>
        /// <param name="outcome">The outcome.</param>
        /// <param name="error">The error.</param>
        /// <returns></returns>
        protected virtual ActivityHistory RunEnd(WorkflowInstance wf, WorkflowActivity activity, User user = null, string outcome = null, string error = null)
        {
            ActivityHistory ah = new ActivityHistory()
            {
                WorkflowInstanceId = wf.Id,
                WorkflowId = wf.WorkflowId,
                ActivityId = activity.ActivityId,
                WorkflowActivityId = activity.Id,
                CustomerId = wf.CustomerId,
                EmployerId = wf.EmployerId,
                ActivityName = activity.Name,
                ActivityType = ActivityType,
                State = wf.State,
                Outcome = outcome,
                IsWaitingOnUserInput = outcome == null,
                Error = error,
                EmployeeId = wf.EmployeeId,
                CaseId = wf.CaseId
            };

            return ah;
        }

        /// <summary>
        /// Errors the specified workflow activity.
        /// </summary>
        /// <param name="wf">The wf.</param>
        /// <param name="activity">The activity.</param>
        /// <param name="message">The message.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public virtual ActivityHistory Error(WorkflowInstance wf, WorkflowActivity activity, string message, User user = null)
        {
            return RunEnd(wf, activity, user, ErrorOutcomeValue, message);
        }

        /// <summary>
        /// Gets the feature restriction.
        /// </summary>
        /// <returns></returns>
        public Feature GetFeatureRestriction()
        {
            var attr = this.GetType().GetCustomAttributes(typeof(FeatureRestrictionAttribute), true);
            if (attr == null || !attr.Any())
                return Feature.None;

            return (attr.First() as FeatureRestrictionAttribute).Feature;
        }
    }
}
