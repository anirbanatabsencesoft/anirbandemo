﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Workflows
{
    /// <summary>
    /// Lists the possible event types that can be triggered in the workflow
    /// sub-system.
    /// </summary>
    [Serializable]
    public enum EventType : int
    {
        /// <summary>
        /// The manual event type. Any and all workflows can be triggered manually.
        /// </summary>
        Manual = 0,
        /// <summary>
        /// The employee created event type
        /// </summary>
        EmployeeCreated = 1,
        /// <summary>
        /// The employee changed event type
        /// </summary>
        EmployeeChanged = 2,
        /// <summary>
        /// The work schedule changed event type
        /// </summary>
        WorkScheduleChanged = 3,
        /// <summary>
        /// The pay schedule changed event type
        /// </summary>
        PayScheduleChanged = 4,
        /// <summary>
        /// The employee terminated event type
        /// </summary>
        EmployeeTerminated = 5,
        /// <summary>
        /// The employee deleted event type
        /// </summary>
        EmployeeDeleted = 6,
        /// <summary>
        /// The case created event type
        /// </summary>
        CaseCreated = 7,
        /// <summary>
        /// The case closed event type
        /// </summary>
        CaseClosed = 9,
        /// <summary>
        /// The case canceled event type
        /// </summary>
        CaseCanceled = 10,
        /// <summary>
        /// The case adjudicated event type
        /// </summary>
        CaseAdjudicated = 11,
        /// <summary>
        /// The case extended event type
        /// </summary>
        CaseExtended = 12,
        /// <summary>
        /// The case reduced schedule changed event type
        /// </summary>
        CaseReducedScheduleChanged = 13,
        /// <summary>
        /// The case type changed event type
        /// </summary>
        CaseTypeChanged = 14,
        /// <summary>
        /// The case reopened event type
        /// </summary>
        CaseReopened = 15,
        /// <summary>
        /// The accommodation created event type
        /// </summary>
        AccommodationCreated = 16,
        /// <summary>
        /// The accommodation changed event type
        /// </summary>
        AccommodationChanged = 17,
        /// <summary>
        /// The accommodation deleted event type
        /// </summary>
        AccommodationDeleted = 18,
        /// <summary>
        /// The accommodation adjudicated event type
        /// </summary>
        AccommodationAdjudicated = 19,
        /// <summary>
        /// The time off requested event type
        /// </summary>
        TimeOffRequested = 20,
        /// <summary>
        /// The time off adjudicated event type
        /// </summary>
        TimeOffAdjudicated = 21,
        /// <summary>
        /// The policy exhausted event type
        /// </summary>
        PolicyExhausted = 22,
        /// <summary>
        /// The policy adjudicated event type
        /// </summary>
        PolicyAdjudicated = 23,
        /// <summary>
        /// The policy manually added event type
        /// </summary>
        PolicyManuallyAdded = 24,
        /// <summary>
        /// The policy manually deleted event type
        /// </summary>
        PolicyManuallyDeleted = 25,
        /// <summary>
        /// The attachment created event type
        /// </summary>
        AttachmentCreated = 26,
        /// <summary>
        /// The attachment deleted event type
        /// </summary>
        AttachmentDeleted = 27,
        /// <summary>
        /// The communication sent event type
        /// </summary>
        CommunicationSent = 28,
        /// <summary>
        /// The communication deleted event type
        /// </summary>
        CommunicationDeleted = 29,
        /// <summary>
        /// The communication note created event type
        /// </summary>
        CommunicationNoteCreated = 30,
        /// <summary>
        /// The communication note changed event type
        /// </summary>
        CommunicationNoteChanged = 31,
        /// <summary>
        /// The paperwork sent event type
        /// </summary>
        PaperworkSent = 32,
        /// <summary>
        /// The case note created event type
        /// </summary>
        CaseNoteCreated = 33,
        /// <summary>
        /// The case note changed event type
        /// </summary>
        CaseNoteChanged = 34,
        /// <summary>
        /// The employee note created event type
        /// </summary>
        EmployeeNoteCreated = 35,
        /// <summary>
        /// The employee note changed event type
        /// </summary>
        EmployeeNoteChanged = 36,
        /// <summary>
        /// To do item created event type
        /// </summary>
        ToDoItemCreated = 37,
        /// <summary>
        /// To do item completed event type
        /// </summary>
        ToDoItemCompleted = 38,
        /// <summary>
        /// To do item changed event type
        /// </summary>
        ToDoItemChanged = 39,
        /// <summary>
        /// To do item canceled event type
        /// </summary>
        ToDoItemCanceled = 40,
        /// <summary>
        /// To do item deleted event type
        /// </summary>
        ToDoItemDeleted = 41,
        /// <summary>
        /// The pay period submitted event type
        /// </summary>
        PayPeriodSubmitted = 42,
        /// <summary>
        /// The certification created event type
        /// </summary>
        CertificationCreated = 43,
        /// <summary>
        /// The certifications changed event type
        /// </summary>
        CertificationsChanged = 44,
        /// <summary>
        /// The diagnosis created event type
        /// </summary>
        DiagnosisCreated = 45,
        /// <summary>
        /// The diagnosis changed event type
        /// </summary>
        DiagnosisChanged = 46,
        /// <summary>
        /// The diagnosis deleted event type
        /// </summary>
        DiagnosisDeleted = 47,
        /// <summary>
        /// The work restriction created event type
        /// </summary>
        WorkRestrictionCreated = 48,
        /// <summary>
        /// The work restriction changed event type
        /// </summary>
        WorkRestrictionChanged = 49,
        /// <summary>
        /// The holiday changed event type
        /// </summary>
        HolidayChanged = 50,
        /// <summary>
        /// The inquiry created event type
        /// </summary>
        InquiryCreated = 51,
        /// <summary>
        /// The inquiry accepted event type
        /// </summary>
        InquiryAccepted = 52,
        /// <summary>
        /// The inquiry created event type
        /// </summary>
        InquiryClosed = 53,
        /// <summary>
        /// The employee job changed event type
        /// </summary>
        EmployeeJobChanged = 54,
        /// <summary>
        /// The employee job approved event type
        /// </summary>
        EmployeeJobApproved = 55,
        /// <summary>
        /// The employee job denied event type
        /// </summary>
        EmployeeJobDenied = 56,
        /// <summary>
        /// The employee office location changed event type
        /// </summary>
        EmployeeOfficeLocationChanged = 57,
        /// <summary>
        /// The employee necessity changed event type
        /// </summary>
        EmployeeNecessityChanged = 58,
        /// <summary>
        /// The employee consult created event type
        /// </summary>
        EmployeeConsultCreated = 59,
        /// <summary>
        /// The work related reporting for a case has changed event type
        /// </summary>
        WorkRelatedReportingChanged = 62,
        /// <summary>
        /// The custom field value for a case has changed event type
        /// </summary>
        CaseCustomFieldChanged = 63,
        /// <summary>
        /// The custom field value for a case contact info changed event type
        /// </summary>
        CaseContactInfoChanged = 64,
        /// <summary>
        /// The case events were updated
        /// </summary>
        CaseEventsUpdated = 65,
        /// <summary>
        /// The relapsed case
        /// </summary>
        CaseRelapsed = 66
    }
}
