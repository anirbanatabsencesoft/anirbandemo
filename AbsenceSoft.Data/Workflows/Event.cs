﻿using AbsenceSoft.Data.Audit;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Workflows
{
    /// <summary>
    /// Represents a single event that may trigger one or more workflows.
    /// The <c>Metadata</c> property of the Event stores additional information
    /// used for making decisions about the event and become part of any workflow's
    /// initial state.
    /// </summary>
    [Serializable, AuditClassNo]
    public class Event : BaseEmployerEntity<Event>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Event"/> class.
        /// </summary>
        public Event()
        {
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the type of the event.
        /// </summary>
        /// <value>
        /// The type of the event.
        /// </value>
        public EventType EventType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is a self service event.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is a self service event; otherwise, <c>false</c>.
        /// </value>
        public bool IsSelfService { get; set; }

        /// <summary>
        /// Gets or sets the event's case identifier, if any.
        /// </summary>
        /// <value>
        /// The case identifier.
        /// </value>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the event's employee identifier, if any.
        /// </summary>
        /// <value>
        /// The employee identifier.
        /// </value>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the event Case, if any
        /// </summary>
        /// <value>
        /// The case.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public Case Case
        {
            get { return base.GetReferenceValue<Case>(this.CaseId); }
            set { this.CaseId = base.SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the event Employee, if any
        /// </summary>
        /// <value>
        /// The employee.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public Employee Employee
        {
            get { return base.GetReferenceValue<Employee>(this.EmployeeId); }
            set { this.EmployeeId = base.SetReferenceValue(value); }
        }
    }
}
