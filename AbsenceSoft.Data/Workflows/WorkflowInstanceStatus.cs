﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Workflows
{
    /// <summary>
    /// Represents the status of the workflow instance.
    /// </summary>
    public enum WorkflowInstanceStatus : int
    {
        /// <summary>
        /// The pending workflow instance status.
        /// </summary>
        Pending = 0,
        /// <summary>
        /// The running workflow instance status.
        /// </summary>
        Running = 1,
        /// <summary>
        /// The paused workflow instance status.
        /// </summary>
        Paused = 2,
        /// <summary>
        /// The completed workflow instance status.
        /// </summary>
        Completed = 3,
        /// <summary>
        /// The canceled workflow instance status.
        /// </summary>
        Canceled = 4,
        /// <summary>
        /// The failed workflow instance status.
        /// </summary>
        Failed = 5
    }
}
