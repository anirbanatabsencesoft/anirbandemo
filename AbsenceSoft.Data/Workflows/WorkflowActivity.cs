﻿using AbsenceSoft.Data.Questions;
using AbsenceSoft.Data.Rules;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Workflows
{
    /// <summary>
    /// Maps an instance of an Activity for a workflow, including configuration
    /// information for that activity as it pertains to this workflow AND since there
    /// may be more than one copy of an activity on a workflow of the same type, etc.
    /// </summary>
    [Serializable]
    public class WorkflowActivity : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowActivity"/> class.
        /// </summary>
        public WorkflowActivity()
        {
        }

        /// <summary>
        /// Gets or sets the activity identifier.
        /// </summary>
        /// <value>
        /// The activity identifier.
        /// </value>
        [BsonRequired]
        public string ActivityId { get; set; }

        /// <summary>
        /// Gets or sets the name of this specific activity.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the rules for this specific activity.  Should be used only by Condition Activity
        /// </summary>
        /// <value>
        /// The rules
        /// </value>
        [BsonIgnoreIfNull]
        public RuleGroup Rules { get; set; }


        /// <summary>
        /// Gets or sets the QuestionConfiguration for this specific activity.  Should be used only by Question Activity
        /// </summary>
        /// <value>
        /// The rules
        /// </value>
        [BsonIgnoreIfNull]
        public Question Question { get; set; }


        /// <summary>
        /// Metadatas the has properties.
        /// </summary>
        /// <param name="properties">The properties.</param>
        /// <returns></returns>
        public bool MetadataHasProperties(params string[] properties)
        {
            foreach (string prop in properties)
            {
                if (!Metadata.Contains(prop))
                    return false;
            }

            return true;
        }
    }
}
