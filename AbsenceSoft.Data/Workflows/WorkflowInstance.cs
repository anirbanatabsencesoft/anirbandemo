﻿using AbsenceSoft.Data.Audit;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Data.Workflows
{
    [Serializable, AuditClassNo]
    public class WorkflowInstance : BaseEmployerEntity<WorkflowInstance>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowInstance"/> class.
        /// </summary>
        public WorkflowInstance() : base()
        {
            Messages = new List<string>();
        }

        /// <summary>
        /// Gets or sets the workflow identifier.
        /// </summary>
        /// <value>
        /// The workflow identifier.
        /// </value>
        [BsonRequired]
        [BsonRepresentation(BsonType.ObjectId)]
        public string WorkflowId { get; set; }

        /// <summary>
        /// Gets or sets the name of the workflow.
        /// </summary>
        /// <value>
        /// The name of the workflow.
        /// </value>
        public string WorkflowName { get; set; }

        /// <summary>
        /// Gets or sets the event identifier that triggered the workflow.
        /// </summary>
        /// <value>
        /// The event identifier of the event that triggered the workflow.
        /// </value>
        [BsonRequired]
        [BsonRepresentation(BsonType.ObjectId)]
        public string EventId { get; set; }

        /// <summary>
        /// Gets or sets the type of the event that triggered the workflow.
        /// </summary>
        /// <value>
        /// The type of the event that triggered the workflow.
        /// </value>
        public EventType EventType { get; set; }

        /// <summary>
        /// Gets or sets the status of this current workflow instance.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public WorkflowInstanceStatus Status { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance has exited
        /// (either completed or failed for some reason).
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has exited; otherwise, <c>false</c>.
        /// </value>
        public bool HasExited { get { return (int)Status > 1; } }

        /// <summary>
        /// Gets or sets the errors.
        /// </summary>
        /// <value>
        /// The errors.
        /// </value>
        public List<string> Messages { get; set; }

        /// <summary>
        /// Gets or sets the workflow running state.
        /// </summary>
        /// <value>
        /// The workflow running state.
        /// </value>
        [AuditPropertyNo]
        public DynamicAwesome State { get; set; }

        /// <summary>
        /// Gets the workflow running state as a dynamic type for expression evaluation
        /// w/o casting.
        /// </summary>
        /// <value>
        /// The workflow running state.
        /// </value>
        [AuditPropertyNo, BsonIgnore, JsonIgnore]
        public dynamic DynamicState { get { return (dynamic)State; } }

        /// <summary>
        /// Determines whether or not the State dynamic awesome property has any properties of its own.
        /// </summary>
        /// <param name="properties">The properties to check for, if any.</param>
        /// <returns></returns>
        public bool StateHasProperties(params string[] properties)
        {
            return !properties.ToList().Except(State.GetDynamicMemberNames()).Any();
        }

        /// <summary>
        /// Gets or sets the workflow's case identifier, if any.
        /// </summary>
        /// <value>
        /// The case identifier.
        /// </value>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the workflow's employee identifier, if any.
        /// </summary>
        /// <value>
        /// The employee identifier.
        /// </value>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the workflow Case, if any
        /// </summary>
        /// <value>
        /// The case.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public Case Case
        {
            get { return base.GetReferenceValue<Case>(CaseId); }
            set { CaseId = base.SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the workflow Employee, if any
        /// </summary>
        /// <value>
        /// The employee.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public Employee Employee
        {
            get { return base.GetReferenceValue<Employee>(EmployeeId); }
            set { EmployeeId = base.SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the workflow.
        /// </summary>
        /// <value>
        /// The workflow.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public Workflow Workflow
        {
            get { return base.GetReferenceValue(WorkflowId, w => w.Id, id => GetWorkflow(id)); }
            set { WorkflowId = base.SetReferenceValue(value, w => w.Id); }
        }

        /// <summary>
        /// Gets or sets the event.
        /// </summary>
        /// <value>
        /// The event.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public Event Event
        {
            get { return base.GetReferenceValue(EventId, e => e.Id, id => GetEvent(id)); }
            set { EventId = base.SetReferenceValue(value, e => e.Id); }
        }

        /// <summary>
        /// Adds the error.
        /// </summary>
        /// <param name="ex">The ex.</param>
        public void AddError(Exception ex)
        {
            AddMessage(ex.Message);
        }

        /// <summary>
        /// Adds the message.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="args">The arguments.</param>
        public void AddMessage(string format, params object[] args)
        {
            Messages = Messages ?? new List<string>();
            Messages.AddIfNotExists(string.Format(format, args));
        }

        /// <summary>
        /// Gets the workflow even if it has been deleted in the past.
        /// </summary>
        /// <param name="workflowId">The workflow identifier.</param>
        /// <returns></returns>
        private Workflow GetWorkflow(string workflowId)
        {
            var wf = Workflow.Repository.GetById(workflowId);
            if (wf == null
                || (!string.IsNullOrWhiteSpace(wf.CustomerId) && wf.CustomerId != CustomerId)
                || (!string.IsNullOrWhiteSpace(wf.EmployerId) && wf.EmployerId != EmployerId))
                return null;
            return wf;
        }

        /// <summary>
        /// Gets the event even if it has been deleted in the past.
        /// </summary>
        /// <param name="eventId">The event identifier.</param>
        /// <returns></returns>
        private Event GetEvent(string eventId)
        {
            var e = Event.Repository.GetById(eventId);
            if (e == null
                || (!string.IsNullOrWhiteSpace(e.CustomerId) && e.CustomerId != CustomerId)
                || (!string.IsNullOrWhiteSpace(e.EmployerId) && e.EmployerId != EmployerId))
                return null;
            return e;
        }
    }
}
