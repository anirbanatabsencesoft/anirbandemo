﻿using AbsenceSoft.Data.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Workflows
{
    /// <summary>
    /// Links 1 or 2 activities together within a workflow based on
    /// some condition, outcome. Also represents the START activity
    /// and TERMINATE workflow activity as transitions in and out
    /// of the workflow.
    /// </summary>
    [Serializable]
    public class Transition : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Transition"/> class.
        /// </summary>
        public Transition()
        {
        }

        /// <summary>
        /// Gets or sets the source activity identifier.
        /// </summary>
        /// <value>
        /// The source activity identifier.
        /// </value>
        public Guid? SourceActivityId { get; set; }

        /// <summary>
        /// Gets or sets the target activity identifier.
        /// </summary>
        /// <value>
        /// The target activity identifier.
        /// </value>
        public Guid TargetActivityId { get; set; }

        /// <summary>
        /// Gets or sets which source activity outcome this is targeted for.
        /// </summary>
        /// <value>
        /// For outcome.
        /// </value>
        public string ForOutcome { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance is the error path.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is the error path; otherwise, <c>false</c>.
        /// </value>
        public bool IsErrorPath { get { return ForOutcome == Activity.ErrorOutcomeValue; } }

        /// <summary>
        /// Gets a value indicating whether this instance is the starting transition of a workflow.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is the starting transition of a workflow; otherwise, <c>false</c>.
        /// </value>
        public bool IsStart { get { return !SourceActivityId.HasValue; } }
    }
}
