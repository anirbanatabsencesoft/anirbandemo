﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Workflows
{
    [Flags]
    public enum ActivityType : int
    {
        /// <summary>
        /// This activity does not have a type, meaning there is no action
        /// or anything to do, it just sits there and looks pretty. This is
        /// the default.
        /// </summary>
        None = 0,
        /// <summary>
        /// The action activity type represents an action that can have one or more outcomes.
        /// </summary>
        Action = 1,
        /// <summary>
        /// The user activity type represents an action that a user does and a result is expected back
        /// from the user to determine the outcome (such as a ToDo). These still have some system impact
        /// because they will create a ToDo item, but then the workflow activity will wait until that ToDo
        /// item is completed before determining the outcome.
        /// </summary>
        User = 2,
        /// <summary>
        /// The condition activity type provides some condition(s) with some related outcomes
        /// from each condition provided.
        /// </summary>
        Condition = 3
    }
}
