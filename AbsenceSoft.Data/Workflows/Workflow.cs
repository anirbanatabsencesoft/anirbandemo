﻿using AbsenceSoft.Data.Rules;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Data.Workflows
{
    [Serializable]
    public class Workflow : BaseEmployerOverrideableEntity<Workflow>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Workflow"/> class.
        /// </summary>
        public Workflow() : base()
        {
            Activities = new List<WorkflowActivity>();
            Transitions = new List<Transition>();
            CriteriaSuccessType = RuleGroupSuccessType.And;
            Criteria = new List<Rule>();
            TargetEventType = EventType.Manual;
        }

        /// <summary>
        /// Gets or sets the name of the workflow.
        /// </summary>
        [BsonRequired]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets an optional and helpful description of the workflow.
        /// </summary>
        [BsonIgnoreIfNull]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the TargetEventType
        /// </summary>
        [BsonRequired]
        public EventType TargetEventType { get; set; }

        /// <summary>
        /// Returns a string version of the target event type for display
        /// </summary>
        [BsonIgnore]
        public string EventTypeString { get { return TargetEventType.ToString().SplitCamelCaseString(); } set { return; } }

        /// <summary>
        /// Gets or sets the effective date of the workflow when applicable. Default is
        /// <c>null</c>. If <c>null</c> effective date is not applicable.
        /// </summary>
        [BsonDateTimeOptions(DateOnly = true), BsonIgnoreIfNull]
        public DateTime? EffectiveDate { get; set; }

        /// <summary>
        /// Gets whether or not this workflow is disabled for a specific customer/employer or not
        /// </summary>
        [BsonIgnore]
        public bool IsDisabled { get; set; }

        [BsonIgnoreIfNull]
        public int? Order { get; set; }

        /// <summary>
        /// Gets or sets the activities.
        /// </summary>
        /// <value>
        /// The activities.
        /// </value>
        public List<WorkflowActivity> Activities { get; set; }

        /// <summary>
        /// Gets or sets the transitions used to map between activites.
        /// </summary>
        /// <value>
        /// The transitions list for the workflow.
        /// </value>
        public List<Transition> Transitions { get; set; }

        /// <summary>
        /// Gets or sets the expression in plain English text for an end user's benefit or
        /// gives the criteria a plain-text name for display.
        /// </summary>
        [BsonIgnoreIfNull]
        public string CriteriaName { get; set; }

        /// <summary>
        /// Gets or sets the success evaluation type for the entire citeria itself.
        /// </summary>
        public RuleGroupSuccessType CriteriaSuccessType { get; set; }

        /// <summary>
        /// Gets or sets the rules and conditions for which this workflow would be considered.
        /// </summary>
        /// <value>
        /// The rules for running this workflow (initialize).
        /// </value>
        public List<Rule> Criteria { get; set; }

        /// <summary>
        /// Gets or sets the designer data.
        /// </summary>
        /// <value>
        /// The designer data.
        /// </value>
        [BsonIgnoreIfNull]
        public string DesignerData { get; set; }
    }
}
