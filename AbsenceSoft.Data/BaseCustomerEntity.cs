﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Collections.Generic;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Data
{
    [Serializable, JsonObject(MemberSerialization.OptOut)]
    public abstract class BaseCustomerEntity<TEntity> : BaseEntity<TEntity> where TEntity : BaseCustomerEntity<TEntity>, new()
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the customer id for the entity.
        /// </summary>
        [BsonRequired, BsonRepresentation(BsonType.ObjectId)]
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the entity's customer.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Customer Customer
        {
            get { return GetReferenceValue<Customer>(this.CustomerId); }
            set { CustomerId = SetReferenceValue(value); }
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching.
        /// </summary>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static new IQueryable<TEntity> AsQueryable()
        {
            if (User.Current != null)
                return AsQueryable(User.Current);
            return BaseEntity<TEntity>.AsQueryable();
        }

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching while automatically
        /// applying the appropriate data access filters onto it automagically.
        /// </summary>
        /// <param name="entity">The entity of type <c>TEntity</c> to which to build the filtered queryable off of</param>
        /// <param name="user">The user of which to build data visibility and data access from</param>
        /// <param name="permission">The permission that the user should have access to when filtering the data parameters</param>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static IQueryable<TEntity> AsQueryable(User user, Permission permission) { return AsQueryable(user, permission.Id); }
        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching while automatically
        /// applying the appropriate data access filters onto it automagically.
        /// </summary>
        /// <param name="entity">The entity of type <c>TEntity</c> to which to build the filtered queryable off of</param>
        /// <param name="user">The user of which to build data visibility and data access from</param>
        /// <param name="permission">The permission that the user should have access to when filtering the data parameters</param>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static IQueryable<TEntity> AsQueryable(User user, string permission = null)
        {
            var query = BaseEntity<TEntity>.AsQueryable();

            // If no user, then duh, no dice sucker, empty list it
            if (user == null)
                return new List<TEntity>(0).AsQueryable().Where(e => e.IsDeleted != true);

            // If they are an admin user, can get whatever they want
            if (user.UserType == UserType.Admin)
                return query;
            
            // User must belong to a customer, otherwise empty list it
            if (string.IsNullOrWhiteSpace(user.CustomerId)) return query.Where(e => e.CustomerId == null);

            query = query.Where(e => e.CustomerId == null || e.CustomerId == user.CustomerId);

            return query;
        }//end: AsQueryable

        /// <summary>
        /// Gets the entity by its Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>The entity found by the Id or <c>null</c> if no entity was found by that id.</returns>
        public static new TEntity GetById(string id)
        {
            using (new InstrumentationContext("{0}.GetById", ClassTypeName))
            {
                if (string.IsNullOrWhiteSpace(id))
                    return default(TEntity);

                var user = User.Current;

                TEntity val = Repository.GetById(id.ToLowerInvariant());
                if (val == null || val.IsDeleted)
                    return null;
                if (user == null)
                    return val;
                if (string.IsNullOrWhiteSpace(val.CustomerId))
                    return val;
                if (val.CustomerId != user.CustomerId && user.UserType != UserType.Admin)
                    return default(TEntity);
                return val;
            }
        }

        #endregion
    }//end: BaseCustomerEntity
}//end: namespace
