﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data
{
    [Serializable]
    public class Country
    {
        public string Name { get; set; } 
        public string Code { get; set; }
        public string RegionName { get; set; }
        public List<Region> Regions { get; set; }
    }
}
