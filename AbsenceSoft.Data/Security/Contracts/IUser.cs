﻿using System.Collections.Generic;

namespace AbsenceSoft.Data.Security.Contracts
{
    public interface IAppUser
    {
        string CustomerId { get; set; }
        List<EmployerAccess> Employers { get; set; }
    }
}
