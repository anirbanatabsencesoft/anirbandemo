﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Security
{
    /// <summary>
    /// Represents a self-signed Pkcs#12 PFX x509 certificate using RSA-SHA256 encryption and signing
    /// and also has an associated private key which is password protected.
    /// </summary>
    [Serializable]
    public sealed class PfxCertificate
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PfxCertificate"/> class.
        /// </summary>
        public PfxCertificate() { }

        /// <summary>
        /// Gets or sets the alias.
        /// </summary>
        /// <value>
        /// The alias.
        /// </value>
        [BsonIgnoreIfNull]
        public string Alias { get; set; }

        /// <summary>
        /// Gets or sets the subject.
        /// </summary>
        /// <value>
        /// The subject.
        /// </value>
        [BsonIgnoreIfNull]
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the thumbprint.
        /// </summary>
        /// <value>
        /// The thumbprint.
        /// </value>
        [BsonIgnoreIfNull]
        public string Thumbprint { get; set; }

        /// <summary>
        /// Gets or sets the serial number.
        /// </summary>
        /// <value>
        /// The serial number.
        /// </value>
        [BsonIgnoreIfNull]
        public string SerialNumber { get; set; }

        /// <summary>
        /// Gets or sets the issued date.
        /// </summary>
        /// <value>
        /// The issued date.
        /// </value>
        public DateTime IssuedDate { get; set; }

        /// <summary>
        /// Gets or sets the expires.
        /// </summary>
        /// <value>
        /// The expires.
        /// </value>
        public DateTime Expires { get; set; }

        /// <summary>
        /// Gets or sets the PFX certificate.
        /// </summary>
        /// <value>
        /// The PFX.
        /// </value>
        public byte[] Pfx { get; set; }

        /// <summary>
        /// Gets or sets the PFX certificate password.
        /// </summary>
        /// <value>
        /// The PFX password.
        /// </value>
        public CryptoString Password { get; set; }

        /// <summary>
        /// Gets or sets the public key.
        /// </summary>
        /// <value>
        /// The public key.
        /// </value>
        public string PublicKey { get; set; }
    }
}
