﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver.Builders;
using MongoRepository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Security
{
    [Serializable, JsonObject(MemberSerialization.OptOut)]
    public class SsoSession : Entity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SsoSession"/> class.
        /// </summary>
        public SsoSession()
        {
            SessionObjects = new Dictionary<string, object>();
        }

        /// <summary>
        /// Gets or sets the cache id.
        /// </summary>
        /// <value>
        /// The cache id.
        /// </value>
        [BsonRequired]
        [BsonDateTimeOptions(DateOnly = false, Kind = DateTimeKind.Utc, Representation = BsonType.DateTime)]
        public DateTime UpdateDateTime { get; set; }

        /// <summary>
        /// Gets or sets the session object.
        /// </summary>
        /// <value>
        /// The session object.
        /// </value>
        public Dictionary<string, object> SessionObjects { get; set; }

        /// <summary>
        /// Gets or sets the expires.
        /// </summary>
        /// <value>
        /// The expires.
        /// </value>
        [BsonRequired]
        [BsonDateTimeOptions(DateOnly = false, Kind = DateTimeKind.Utc, Representation = BsonType.DateTime)]
        public DateTime Expires { get { return UpdateDateTime.AddHours(2); } set { } }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            this.UpdateDateTime = DateTime.UtcNow;
            Repository.Update(this);
        }


        [BsonIgnore, JsonIgnore]
        private static object _repoSync = new object();
        [BsonIgnore, JsonIgnore]
        private static MongoRepository<SsoSession> _myRepository = null;
        /// <summary>
        /// Gets the repository used for manipulating this instance.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public static MongoRepository<SsoSession> Repository
        {
            get
            {
                if (_myRepository != null)
                    return _myRepository;

                lock (_repoSync)
                {
                    if (_myRepository != null)
                        return _myRepository;

                    var myRepo = typeof(SsoSession)
                        .GetCustomAttributes(typeof(RepositoryAttribute), true)
                        .OfType<RepositoryAttribute>()
                        .FirstOrDefault();

                    string cName = (myRepo == null || string.IsNullOrWhiteSpace(myRepo.ConnectionStringName)) ? "MongoServerSettings" : myRepo.ConnectionStringName;
                    var connString = ConfigurationManager.ConnectionStrings[cName];
                    if (connString == null)
                        throw new ApplicationException(string.Format("MongoDB connection string '{0}' is missing from configuration", cName));
                    _myRepository = new MongoRepository<SsoSession>(connString.ConnectionString, "SsoSession");
                }

                return _myRepository;
            }
        }//end: Repository
    }
}
