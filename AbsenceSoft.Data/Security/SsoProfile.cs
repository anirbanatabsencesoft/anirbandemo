﻿using AbsenceSoft.Data.Customers;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Audit;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Data.Security
{
    [AuditClassNo]
    [Serializable]
    public class SsoProfile : BaseCustomerEntity<SsoProfile>
    {
        public SsoProfile()
        {
            IsEss = true;
            IsPortal = true;
        }

        /// <summary>
        /// Gets or sets the optional employer id.
        /// </summary>
        /// <value>
        /// The employer id or <c>null</c>.
        /// </value>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the PFX certificate used for encrypting/decrypting metadata and encrypted assertions
        /// from the IdP.
        /// </summary>
        /// <value>
        /// The Local SP PFX certificate used for encryption and decryption.
        /// </value>
        public PfxCertificate LocalCertificate { get; set; }

        /// <summary>
        /// Gets or sets the x509 certificate provide by the partner used for signing or verifying signatures
        /// from partner assertions or AuthnResponses.
        /// </summary>
        /// <value>
        /// The partner's x509 certificate raw data.
        /// </value>
        public byte[] PartnerCertificate { get; set; }

        /// <summary>
        /// Gets the entity id for the Service Provider and identifies the local service provider profile.
        /// </summary>
        /// <value>
        /// The SP entity id.
        /// </value>
        public string EntityId
        {
            get 
            {
                return string.Format("urn:{0}{1}{2}", 
                    CustomerId,
                    string.IsNullOrWhiteSpace(EmployerId) ? "" : ":", 
                    EmployerId);
            }
            set { }
        }

        /// <summary>
        /// Gets or sets the entity id of the Identity Provider (IdP).
        /// </summary>
        /// <value>
        /// The IdP entity id.
        /// </value>
        public string IdpEntityId { get; set; }

        /// <summary>
        /// Gets or sets the idp metadata.
        /// </summary>
        /// <value>
        /// The idp metadata.
        /// </value>
        [BsonIgnoreIfNull]
        public string IdpMetadata { get; set; }

        /// <summary>
        /// Gets or sets the sp metadata.
        /// </summary>
        /// <value>
        /// The sp metadata.
        /// </value>
        [BsonIgnoreIfNull]
        public string SpMetadata { get; set; }

        /// <summary>
        /// Gets or sets the partner identity provider's single sign-on service URL.
        /// </summary>
        /// <value>
        /// The partner identity provider's single sign-on service URL.
        /// </value>
        public string SingleSignOnServiceUrl { get; set; }

        /// <summary>
        /// Gets or sets the partner identity provider's single sign-on service binding.
        /// <para>
        /// The default binding is urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect.
        /// </para>
        /// </summary>
        /// <value>
        /// The partner identity provider's single sign-on service binding.
        /// </value>
        public string SingleSignOnServiceBinding { get; set; }

        /// <summary>
        /// Gets or sets the flag indicating whether to sign authn requests.
        /// </summary>
        /// <value>
        /// The flag indicating whether to sign authn requests.
        /// </value>
        public bool SignAuthnRequest { get; set; }

        /// <summary>
        /// Gets or sets the flag indicating whether to set the force authentication attribute in authn requests.
        /// </summary>
        /// <value>
        /// The flag indicating whether to set the force authentication attribute in authn requests.
        /// </value>
        public bool ForceAuthn { get; set; }

        /// <summary>
        /// Gets or sets the flag indicating whether SAML responses should be signed.
        /// </summary>
        /// <value>
        /// The flag indicating whether SAML responses should be signed.
        /// </value>
        public bool WantSAMLResponseSigned { get; set; }

        /// <summary>
        /// Gets or sets the flag indicating whether SAML assertions should be signed.
        /// </summary>
        /// <value>
        /// The flag indicating whether SAML assertions should be signed.
        /// </value>
        public bool WantAssertionSigned { get; set; }

        /// <summary>
        /// Gets or sets the flag indicating whether SAML assertions should be encrypted.
        /// </summary>
        /// <value>
        /// The flag indicating whether SAML assertions should be encrypted.
        /// </value>
        public bool WantAssertionEncrypted { get; set; }

        /// <summary>
        /// Gets or sets the flag indicating whether the audience restriction condition is checked.
        /// </summary>
        /// <value>
        /// The flag indicating whether the audience restriction condition is checked.
        /// </value>
        public bool DisableAudienceRestrictionCheck { get; set; }

        /// <summary>
        /// Gets or sets the flag indicating whether a pending authentication request may be overridden and an IdP-initiated SAML response received.
        /// <para>
        /// If a service provider sends an authentication request then it expects the SAML response it receives to come from the identity provider
        /// it sent the authentication request to and that the SAML response is in response to this authentication request.
        /// If a different identity provider sends a SAML response or the expected identity provider sends a SAML response but it is not 
        /// in response to this authentication request, this is treated as an error if this flag is false. If this flag is true then these restrictions
        /// do not apply.
        /// </para>
        /// <para>
        /// Setting this flag to true supports an SP-initiated SSO flow being supplanted by an IdP-initiated SSO.
        /// </para>
        /// </summary>
        /// <value>
        /// The flag indicating whether a pending authentication request may be overridden.
        /// </value>
        public bool OverridePendingAuthnRequest { get; set; }

        /// <summary>
        /// Gets or sets the optional requested authentication context to include in the authn request.
        /// </summary>
        /// <value>
        /// The optional requested authentication context to include in the authn request.
        /// </value>
        public string RequestedAuthnContext { get; set; }

        /// <summary>
        /// Gets or sets the single logout service URL.
        /// </summary>
        /// <value>
        /// The single logout service URL.
        /// </value>
        public string SingleLogoutServiceUrl { get; set; }

        /// <summary>
        /// Gets or sets the partner provider's single sign-on service response URL.
        /// </summary>
        /// <value>
        /// The partner provider's single sign-on service response URL.
        /// </value>
        public string SingleLogoutServiceResponseUrl { get; set; }

        /// <summary>
        /// Gets or sets the partner provider's single sign-on service binding.
        /// <para>
        /// The default binding is urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect.
        /// </para>
        /// </summary>
        /// <value>
        /// The partner provider's single sign-on service binding.
        /// </value>
        public string SingleLogoutServiceBinding { get; set; }

        /// <summary>
        /// Gets or sets the logout request lifetime.
        /// <para>
        /// The default time span is 3 minutes.
        /// </para>
        /// </summary>
        /// <value>
        /// The logout request lifetime.
        /// </value>
        public TimeSpan LogoutRequestLifeTime { get; set; }

        /// <summary>
        /// Gets or sets the flag indicating whether to disable inbound logout.
        /// </summary>
        /// <value>
        /// The flag indicating whether to disable inbound logout.
        /// </value>
        public bool DisableInboundLogout { get; set; }

        /// <summary>
        /// Gets or sets the flag indicating whether to disable outbound logout.
        /// </summary>
        /// <value>
        /// The flag indicating whether to disable outbound logout.
        /// </value>
        public bool DisableOutboundLogout { get; set; }

        /// <summary>
        /// Gets or sets the flag indicating whether the InResponseTo is checked.
        /// </summary>
        /// <value>
        /// The flag indicating whether the InResponseTo is checked.
        /// </value>
        public bool DisableInResponseToCheck { get; set; }

        /// <summary>
        /// Gets or sets the flag indicating whether to sign sent logout responses.
        /// </summary>
        /// <value>
        /// The flag indicating whether to sign sent logout responses.
        /// </value>
        public bool SignLogoutRequest { get; set; }

        /// <summary>
        /// Gets or sets the flag indicating whether to sign sent logout responses.
        /// </summary>
        /// <value>
        /// The flag indicating whether to sign sent logout responses.
        /// </value>
        public bool SignLogoutResponse { get; set; }

        /// <summary>
        /// Gets or sets the flag indicating whether received logout requests should be signed.
        /// </summary>
        /// <value>
        /// The flag indicating whether received logout requests should be signed.
        /// </value>
        public bool WantLogoutRequestSigned { get; set; }

        /// <summary>
        /// Gets or sets the flag indicating whether received logout responses should be signed.
        /// </summary>
        /// <value>
        /// The flag indicating whether received logout responses should be signed.
        /// </value>
        public bool WantLogoutResponseSigned { get; set; }

        /// <summary>
        /// Gets or sets the flag to indicate whether to use embedded certificates.
        /// </summary>
        /// <value>
        /// The flag to indicate whether to use embedded certificates.
        /// </value>
        public bool UseEmbeddedCertificate { get; set; }

        /// <summary>
        /// Gets or sets the name identifier format.
        /// <para>
        /// The default name identifier format is urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified.
        /// </para>
        /// </summary>
        /// <value>
        /// The name identifier format.
        /// </value>
        public string NameIDFormat { get; set; }

        /// <summary>
        /// Gets or sets the XML signature digest algorithm.
        /// <para>
        /// The default algorithm is http://www.w3.org/2000/09/xmldsig#sha1.
        /// </para>
        /// </summary>
        /// <value>
        /// The XML signature digest algorithm.
        /// </value>
        public string DigestMethod { get; set; }

        /// <summary>
        /// Gets or sets the XML signature algorithm.
        /// <para>
        /// The default algorithm is http://www.w3.org/2000/09/xmldsig#rsa-sha1.
        /// </para>
        /// </summary>
        /// <value>
        /// The XML signature algorithm.
        /// </value>
        public string SignatureMethod { get; set; }

        /// <summary>
        /// Gets or sets the XML encryption key encryption algorithm.
        /// <para>
        /// The default algorithm is http://www.w3.org/2001/04/xmlenc#rsa-1_5.
        /// </para>
        /// </summary>
        /// <value>
        /// The XML encryption key encryption algorithm.
        /// </value>
        public string KeyEncryptionMethod { get; set; }

        /// <summary>
        /// Gets or sets the XML encryption data encryption algorithm.
        /// <para>
        /// The default algorithm is http://www.w3.org/2001/04/xmlenc#aes128-cbc.
        /// </para>
        /// </summary>
        /// <value>
        /// The XML encryption data encryption algorithm.
        /// </value>
        public string DataEncryptionMethod { get; set; }

        /// <summary>
        /// Gets or sets the clock skew.
        /// The clock skew allows for differences between local and partner computer clocks when checking time intervals.
        /// <para>
        /// The default time span is 3 minutes.
        /// </para>
        /// </summary>
        /// <value>
        /// The clock skew.
        /// </value>
        public TimeSpan ClockSkew { get; set; }

        /// <summary>
        /// Gets or sets the artifact resolution service URL.
        /// </summary>
        /// <value>
        /// The artifact resolution service URL.
        /// </value>
        public string ArtifactResolutionServiceUrl { get; set; }

        /// <summary>
        /// Self Service Login Source Name
        /// </summary>
        public string SelfServiceLoginSourceName { get; set; }

        /// <summary>
        /// Portal Login Source Name
        /// </summary>
        public string PortalLoginSourceName { get; set; }

        /// <summary>
        /// Login Source Show Logo
        /// </summary>
        public bool LoginSourceShowLogo { get; set; }

        /// <summary>
        /// Whether this profile applies to ESS
        /// </summary>
        public bool IsEss { get; set; }

        /// <summary>
        /// Whether this profile applies to the portal
        /// </summary>
        public bool IsPortal { get; set; }

        /// <summary>
        /// Gets or sets the entity's employer.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Employer Employer
        {
            get { return GetReferenceValue<Employer>(this.EmployerId); }
            set { EmployerId = SetReferenceValue(value); }
        }
    }
}
