﻿using AbsenceSoft.Common;
using AbsenceSoft.Common.Security;
using AbsenceSoft.Data.Audit;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AbsenceSoft;
using AbsenceSoft.Data.Dashboards;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using AbsenceSoft.Data.Security.Contracts;

namespace AbsenceSoft.Data.Security
{
    public static class UserStatus
    {
        public const string Active = "Active";
        public const string Disabled = "Disabled";
        public const string Deleted = "Deleted";
        public const string Locked = "Locked";
    }

    /// <summary>
    /// User Access/Roles information class 
    /// </summary>
    [Serializable]
    public class User : BaseCustomerEntity<User>, IAppUser
    {
        /// <summary>
        /// Represents the default user ID when the "system" creates or does stuff.
        /// </summary>
        public const string DefaultUserId = "000000000000000000000000";

        /// <summary>
        /// Initializes a new instance of <see cref="AbsenceSoft.Data.Security.User"/>.
        /// </summary>
        public User()
        {
            Passwords = new List<UserPassword>();
            Password = new CryptoString();
            Roles = new List<string>();
            Employers = new List<EmployerAccess>();
            ContactInfo = new Contact();
            UserType = UserType.Portal;
        }

        /// <summary>
        /// Gets or sets the user's job title
        /// </summary>
        [BsonIgnoreIfNull]
        public string JobTitle { get; set; }

        /// <summary>
        /// Gets or sets the user's title
        /// </summary>
        [BsonIgnoreIfNull]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the user's email address, which is also thier username.
        /// </summary>
        [BsonRequired]
        public string Email { get; set; }

        private CryptoString password;
        /// <summary>
        /// Gets or sets the user's password. 
        /// </summary>
        public CryptoString Password
        {
            get
            {
                return password;
            }
            set
            {
                // current password may not be archived
                EnsurePasswordArchival(password);
                // new password must be hashed and archived
                value = EnsurePasswordEncryption(value);
                EnsurePasswordArchival(value);
                password = value;
            }
        }

        /// <summary>
        /// Ensures the password archival.
        /// </summary>
        /// <param name="password">The password.</param>
        private void EnsurePasswordArchival(CryptoString password)
        {
            if (password != null && password.Hashed != null)
            {
                // at this point, there should only be one password in history
                // and we should be able to manipulated the CreatedDate of it
                // to simulated a password expiration
                var now = DateTime.UtcNow;
                if (!Passwords.Any() && CreatedDate != new DateTime())
                {
                    now = CreatedDate;
                }
                var lastPassword = Passwords
                    .OrderByDescending(o => o.CreatedDate)
                    .FirstOrDefault();
                if (lastPassword == null || lastPassword.Password.Hashed != password.Hashed)
                {
                    Passwords.Add(new UserPassword
                    {
                        Password = password,
                        CreatedDate = now
                    });
                }
            }
        }


        /// <summary>
        /// Gets or sets the user's password history
        /// </summary>
        /// <remarks>
        /// Password should be duplicated as the most recent entry
        /// here
        /// </remarks>
        [BsonIgnoreIfNull, JsonIgnore]
        public List<UserPassword> Passwords { get; set; }

        /// <summary>
        /// Gets a value indicating whether or not the user's account is locked.
        /// </summary>
        [BsonIgnore]
        public bool IsLocked { get { return LockedDate.HasValue; } }

        /// <summary>
        /// Gets or sets the date the user account was locked. Drives the IsLocked property.
        /// </summary>
        [BsonIgnoreIfNull]
        public DateTime? LockedDate { get; set; }

        /// <summary>
        /// Gets a value indicating whether or not the user's account is disabled.
        /// </summary>
        [BsonIgnore]
        public bool IsDisabled { get { return DisabledDate.HasValue || (EndDate.HasValue && EndDate < DateTime.Now); } }

        /// <summary>
        /// Gets or sets the date the user account was disabled. Drives the IsDisabled property.
        /// </summary>
        [BsonIgnoreIfNull]
        public DateTime? DisabledDate { get; set; }

        /// <summary>
        /// Gets or sets a Date at which the user will automatically be disabled
        /// </summary>
        [BsonIgnoreIfNull]
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the collection of roles the user has access to.
        /// </summary>
        [BsonRepresentation(BsonType.ObjectId)]
        public List<string> Roles { get; set; }

        /// <summary>
        /// Gets or sets the user account Last Activity Date.    
        /// </summary>
        [BsonIgnoreIfNull]
        public DateTime? LastActivityDate { get; set; }

        /// <summary>
        /// Gets or sets the user flags for interaction of the user with the system.
        /// </summary>
        public UserFlag UserFlags { get; set; }

        /// <summary>
        /// Gets or sets the First Name.    
        /// </summary>
        [BsonIgnoreIfNull]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the Last Name.    
        /// </summary>
        [BsonIgnoreIfNull]
        public string LastName { get; set; }

        /// <summary>
        /// Gets the display name of the user
        /// </summary>
        [BsonIgnore]
        public string DisplayName
        {
            get
            {
                StringBuilder me = new StringBuilder();
                if (!string.IsNullOrWhiteSpace(FirstName))
                    me.Append(FirstName);
                if (!string.IsNullOrWhiteSpace(LastName))
                    me.AppendFormat("{0}{1}", me.Length > 0 ? " " : "", LastName);
                return me.ToString();
            }
        }//DisplayName

        /// <summary>
        /// Gets or sets the user password reset key for easy indexing/searching later on.
        /// </summary>
        [BsonIgnoreIfNull]
        public string ResetKey { get; set; }

        /// <summary>
        /// Gets or sets the Must Change Password flag.    
        /// </summary>
        [BsonIgnore]
        public bool MustChangePassword
        {
            get
            {
                return (UserFlags & UserFlag.ChangePassword) == UserFlag.ChangePassword;
            }
            set
            {
                if (value)
                {
                    UserFlags |= UserFlag.ChangePassword;
                }
                else
                {
                    UserFlags &= ~UserFlag.ChangePassword;
                }
            }
        }

        /// <summary>
        /// Gets or sets the user's locked IP address
        /// </summary>
        [BsonIgnoreIfNull]
        public string LockedIpAddress { get; set; }

        /// <summary>
        /// Gets or sets the the number of failed login attempts
        /// </summary>
        public int FailedLoginAttempts { get; set; }

        /// <summary>
        /// Gets or sets the date/time of the last failed login attempt
        /// </summary>
        [BsonIgnoreIfNull]
        public DateTime? LastFailedLoginAttempt { get; set; }


        /// <summary>
        /// Gets the EmployeeId from the EmployerAccess record, if it exists
        /// </summary>
        public string EmployeeId
        {
            get
            {
                if (this.Employers == null || this.Employers.Count == 0)
                    return null;

                var employerAccess = this.Employers.FirstOrDefault(e => !string.IsNullOrEmpty(e.EmployeeId));
                if (employerAccess == null)
                    return null;

                return employerAccess.EmployeeId;
            }
        }

        /// <summary>
        /// Gets the EmployerId from the EmployerAccess record, if it exists
        /// </summary>
        public string EmployerId
        {
            get
            {
                if (this.Employers == null || this.Employers.Count == 0)
                    return null;

                var employerAccess = this.Employers.FirstOrDefault(e => !string.IsNullOrEmpty(e.EmployerId));
                if (employerAccess == null)
                    return null;

                return employerAccess.EmployerId;
            }
        }

        /// <summary>
        /// Gets or sets the collection of employers this user has access to along with
        /// any specific employer permissions, roles, assignments and other attributes
        /// of that User to Employer relationship, like how long have they been dating? lol
        /// </summary>
        public List<EmployerAccess> Employers { get; set; }

        /// <summary>
        /// Gets or sets the last dashboard identifier that the user should see on the landing page.
        /// </summary>
        /// <value>
        /// The last dashboard identifier that the user should see on the landing page.
        /// </value>
        [BsonRepresentation(BsonType.ObjectId)]
        public string DashboardId { get; set; }

        /// <summary>
        /// Gets or sets the contact information.
        /// </summary>
        /// <value>
        /// The contact information.
        /// </value>
        public Contact ContactInfo { get; set; }
        

        /// <summary>
        /// Computes a value that determines whether or not this user has access to the provided
        /// employer, and optionally ensures the user has the given permission for that employer
        /// as well.
        /// </summary>
        /// <param name="employerId">The employer Id for which to check access for.</param>
        /// <param name="permissions">The optional permissions to check for with that Employer access.</param>
        /// <returns><c>true</c> if the user has access, otherwise <c>false</c>.</returns>
        public bool HasEmployerAccess(string employerId, params string[] permissions)
        {
            if (Id == System.Id) return true;
            if (IsDeleted) return false;
            if (string.IsNullOrWhiteSpace(employerId)) return true;
            if (!string.IsNullOrWhiteSpace(employerId) && Employers == null) return false;
            if (!string.IsNullOrWhiteSpace(employerId) && !Employers.Any(e => e.IsActive && e.EmployerId == employerId && !e.Employer.IsDeleted)) return false;
            if (permissions.Any())
                return !permissions.Except(Permissions.GetPermissions(this, employerId)).Any();
            return true;
        }//end: HasEmployerAccess

        /// <summary>
        /// Computes a value that determines whether or not this user has access to the provided
        /// employer, and optionally ensures the user has the given permission for that employer
        /// as well.
        /// </summary>
        /// <param name="employerId">The employer Id for which to check access for.</param>
        /// <param name="permissions">The permissions to check for with that Employer access.</param>
        /// <returns><c>true</c> if the user has access, otherwise <c>false</c>.</returns>
        public bool HasEmployerAccess(string employerId, IEnumerable<Permission> permissions)
        {
            return HasEmployerAccess(employerId, permissions.Select(p => p.Id).ToArray());
        }//end: HasEmployerAccess

        /// <summary>
        /// Computes a value that determines whether or not this user has access to the provided
        /// employer, and optionally ensures the user has the given permission for that employer
        /// as well.
        /// </summary>
        /// <param name="employerId">The employer Id for which to check access for.</param>
        /// <param name="permission">The permission to check for with that Employer access.</param>
        /// <returns><c>true</c> if the user has access, otherwise <c>false</c>.</returns>
        public bool HasEmployerAccess(string employerId, Permission permission)
        {
            return HasEmployerAccess(employerId, new Permission[1] { permission });
        }//end: HasEmployerAccess
        

        /// <summary>
        /// This determines whether the user is a allowed access to a particular employee data depending upon employee
        /// organization affiliation rules.
        /// </summary>
        /// <param name="employeeOrganization"></param>
        /// <param name="userOrgList"></param>
        /// <returns></returns>
        public bool HasEmployeeOrganizationAccess(EmployeeOrganization employeeOrganization, List<EmployeeOrganization> userOrgList)
        {            
            if (userOrgList != null)
            {
                foreach (EmployeeOrganization userOrganization in userOrgList)
                {         
                    if (employeeOrganization.Affiliation == OrganizationAffiliation.Viewer && userOrganization.Affiliation == OrganizationAffiliation.Viewer
                        && employeeOrganization.EmployeeNumber == userOrganization.EmployeeNumber & employeeOrganization.EmployerId == userOrganization.EmployerId) return true;
                    if (employeeOrganization.Affiliation != OrganizationAffiliation.Viewer 
                        && employeeOrganization.Path.IsDescendantOf(userOrganization.Path, userOrganization.Affiliation != OrganizationAffiliation.Member)) 
                        return true;
                }
            }
            return false;
        }
        /// <summary>
        /// This method determines whether current user can access emp details passed as a parameter
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        public bool HasEmployeeOrganizationAccess(Employee emp)
        {
            if (Roles.Contains(Role.SystemAdministrator.Id)) return true;            
            if (Customer.HasFeature(Feature.OrgDataVisibility))
            {                
                bool viewAllEmployeePermission = User.Permissions.GetPermissions(this, emp.EmployerId).Contains(Permission.ViewAllEmployees.Id);
                if (viewAllEmployeePermission) return true;
                List<EmployeeOrganization> empOrgs = EmployeeOrganization.AsQueryable().Where(org => org.EmployerId == emp.EmployerId
                && org.CustomerId == emp.CustomerId && org.EmployeeNumber == emp.EmployeeNumber).ToList();
                if (empOrgs == null || empOrgs.Count == 0)
                    return false;                
                EmployerAccess empAccess = Employers.FirstOrDefault(empl => empl.EmployerId == emp.EmployerId);
                if (empAccess.EmployeeId == null) return false;
                List<EmployeeOrganization> userOrgs = EmployeeOrganization.AsQueryable().Where(org => org.EmployeeNumber == empAccess.Employee.EmployeeNumber).ToList();
                foreach (EmployeeOrganization org in empOrgs)
                {
                    return this.HasEmployeeOrganizationAccess(org, userOrgs);                    
                }
            }
            return true;
        }

        /// <summary>
        /// Determines if the user perform some flag
        /// </summary>
        /// <param name="flag">The flag to test</param>
        /// <returns></returns>
        public bool Must(UserFlag flag)
        {
            if (IsDeleted) return false;
            return UserFlags.HasFlag(flag);
        }

        /// <summary>
        /// Called when a task is completed against the user and a flag should be removed if still relevent.
        /// </summary>
        /// <param name="flag">The flag to remove</param>
        public bool Done(UserFlag flag)
        {
            if (UserFlags.HasFlag(flag))
            {
                UserFlags &= ~flag;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Gets or sets the user's customer.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Customer Customer
        {
            get { return GetReferenceValue<Customer>(this.CustomerId); }
            set { CustomerId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the dashboard.
        /// </summary>
        /// <value>
        /// The dashboard.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public Dashboard Dashboard
        {
            get { return GetReferenceValue<Dashboard>(this.DashboardId); }
            set { DashboardId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        [BsonIgnore]
        public string Status
        {
            get
            {
                if (IsDeleted)
                {
                    return UserStatus.Deleted;
                }
                else if (IsDisabled)
                {
                    return UserStatus.Disabled;
                }
                else if (IsLocked)
                {
                    return UserStatus.Locked;
                }
                else
                {
                    return UserStatus.Active;
                }
            }
            set { }
        }

        /// <summary>
        /// Called when saved
        /// </summary>
        protected override void OnSaved()
        {
            if (Current != null && IsUserCached() && Current.Id == Id)
                InvalidateUserCache();
            base.OnSaved();
        }

        [BsonDefaultValue(UserType.Portal)]
        public UserType UserType { get; set; }
        
        /// <summary>
        /// Called before saving
        /// </summary>
        /// <returns>
        /// A value indicating whether or not the save should proceed (not canceled).
        /// </returns>
        protected override bool OnSaving()
        {
            // Always ensure the email address is lower-cased every time, this is so we have consistency in matching.
            Email = string.IsNullOrWhiteSpace(Email) ? null : Email.ToLowerInvariant();
            ContactInfo = ContactInfo ?? new Contact();
            ContactInfo.FirstName = FirstName;
            ContactInfo.LastName = LastName;
            ContactInfo.Email = Email;

            // Call our base OnSaving event to go on with life, we're done here people, nothing to see here, move along.
            return base.OnSaving();
        }

        /// <summary>
        /// Ensures the password encryption.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        private CryptoString EnsurePasswordEncryption(CryptoString password)
        {
            // If we have a plain text password in there, ensure we hash it.
            if (password != null && !string.IsNullOrWhiteSpace(password.PlainText))
                password.Hashed = password.PlainText.Sha256Hash();

            // Ensure we never store an encrypted 2-way copy of the password, that's just asking for trouble.
            if (password != null)
                password.Encrypted = null;
            return password;
        }

        public const string CURRENT_USER_CONTEXT_KEY = "__current_user__";
        public const string CURRENT_USER_PORTAL_CONTEXT = "__portal_context";

        /// <summary>
        /// Invalidates the user cache.
        /// </summary>
        public static void InvalidateUserCache()
        {
            HttpContext context = HttpContext.Current;
            if (context != null && context.Items != null)
            {
                context.Items.Remove(CURRENT_USER_CONTEXT_KEY);
            }

        }

        /// <summary>
        /// Determines whether [is user cached].
        /// </summary>
        /// <returns></returns>
        public static bool IsUserCached()
        {
            HttpContext context = HttpContext.Current;
            return context != null && context.Items != null && context.Items.Contains(CURRENT_USER_CONTEXT_KEY);
        }

        /// <summary>
        /// Gets the current user from the context.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        [Obsolete("Use Current.User() in AbsenceSoft.Web instead.  If not in AbsenceSoft project or AbsenceSoft.SelfService, pass the context in, don't add a reference to those methods")]
        public static User Current
        {
            get
            {
                User cur = null;
                try
                {
                    HttpContext context = HttpContext.Current;
                    if (context != null && context.Items != null && context.User != null && context.User.Identity != null && !string.IsNullOrWhiteSpace(context.User.Identity.Name))
                    {
                        cur = context.Items[CURRENT_USER_CONTEXT_KEY] as User;
                        if (cur != null)
                            return cur;

                        if (!(context.User.Identity is ClaimsIdentity identity))
                        {
                            return null;
                        };
                        var key = identity.FindFirstValue("Key");
                        /// We call the GetById here for the repository because if User.Current calls GetById it will setup an infinite loop
                        /// where User.Current is called
                        if (key != null)
                        {
                            cur = User.Repository.GetById(key);
                            context.Items[CURRENT_USER_CONTEXT_KEY] = cur;
                        }
                    }
                }
                catch (NullReferenceException)
                {
                    cur = null;
                }
                catch (Exception ex)
                {
                    Log.Error("Error getting current user from context", ex);
                    cur = null;
                }
                return cur;
            }
        }
        private static User _system = new User() { Id = DefaultUserId, FirstName = "System", LastName = "User", Email = "admin@absencesoft.com", CustomerId = new ObjectId().ToString() };

        /// <summary>
        /// just um saying.... User has system and there it goes recursing to your death so don't take the
        /// ignore properties off of here
        /// </summary>
        /// <value>
        /// The system.
        /// </value>
        [
            BsonIgnore,
            JsonIgnore,
            AuditPropertyNo
        ]
        public static User System { get { return _system; } }

        [BsonIgnore, JsonIgnore, NonSerialized]
        private List<Role> __roleCache = new List<Role>();

        /// <summary>
        /// Permissions access for the user account which includes shortcuts for caching the current user permissions, etc.
        /// </summary>
        public static class Permissions
        {
            #region Permissions

            private const string PROJECTED_PERMISSIONS_CONTEXT_KEY = "__projected_permissions__";
            private const string EMPLOYER_PERMISSIONS_CONTEXT_KEY = "__employer_{0}_permissions__";

            /// <summary>
            /// Gets the current user's projected permissions. These are what's "possible" for a user
            /// to have, not what the user actually has.
            /// </summary>
            /// <remarks>
            /// So, for instance, the user has "Create Employee" access for Employer A, but not for Employer B, 
            /// it will show in this list, but Employer B will not when they actually go to do it.
            /// </remarks>
            [BsonIgnore, JsonIgnore]
            public static IReadOnlyList<string> ProjectedPermissions
            {
                get
                {
                    List<string> per = Permission.None;

                    try
                    {
                        HttpContext context = HttpContext.Current;
                        if (context != null && context.Items != null)
                        {
                            if (context.Items[PROJECTED_PERMISSIONS_CONTEXT_KEY] != null)
                                return (List<string>)context.Items[PROJECTED_PERMISSIONS_CONTEXT_KEY];

                            User cur = Current;

                            if (cur != null && cur.Roles != null && !cur.IsDeleted && !cur.IsDisabled && !cur.IsLocked)
                                per = GetProjectedPermissions(cur);

                            context.Items[PROJECTED_PERMISSIONS_CONTEXT_KEY] = per;
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Error getting current projected permissions from context", ex);
                        per = Permission.None;
                    }

                    return per.AsReadOnly();
                }
            }//end: ProjectedPermissions

            /// <summary>
            /// Gets the current user's employer permissions for the given employer Id.
            /// </summary>
            /// <param name="employerId">The employer Id of the employer effective permissions for the current user are required.</param>
            /// <returns>A read-only list of permissions effective for the user for the given Employer either from the employer
            /// override itself or globally if no employer override was found/provided.</returns>
            public static IReadOnlyList<string> EmployerPermissions(string employerId)
            {
                List<string> per = Permission.None;
                if (string.IsNullOrWhiteSpace(employerId))
                    return ProjectedPermissions;

                try
                {
                    string cacheKey = string.Format(EMPLOYER_PERMISSIONS_CONTEXT_KEY, employerId);

                    HttpContext context = HttpContext.Current;
                    if (context != null && context.Items != null)
                    {
                        if (context.Items[cacheKey] != null)
                            return (List<string>)context.Items[cacheKey];

                        User cur = Current;

                        if (cur != null && cur.Roles != null && !cur.IsDeleted && !cur.IsDisabled && !cur.IsLocked)
                            per = GetPermissions(cur, employerId);

                        context.Items[cacheKey] = per;
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("Error getting current employer permissions from context", ex);
                    per = Permission.None;
                }

                return per.AsReadOnly();
            }//end: EmployerPermissions

            /// <summary>
            /// Takes a user instance and returns back the effective permissions for that user
            /// based on the user roles and a distinct set of permissions for those combined roles either
            /// for the specific employer, if overridden, or globally for the Customer.
            /// </summary>
            /// <param name="u">The user instance to get effective permissions for</param>
            /// <param name="employerId">The Id of the employer to limit permissions for</param>
            /// <returns>A list of permissions</returns>
            public static List<string> GetPermissions(User u, string employerId = null)
            {
                // No user, guess what, no permissions, DUH; oh yeah, disabled and deleted ones have none too
                if (u == null || u.IsDisabled || u.IsDeleted)
                {
                    return Permission.None;
                }
                    

                // Build our list of applied roles that will actually create the final permission set
                List<string> appliedRoles = new List<string>();
                // If we have an Employer Id, let's check that first to see if we have roles set for that Employer override
                Employer employer = null;
                if (!string.IsNullOrWhiteSpace(employerId))
                {
                    // Get the Employer Access object associated with the employer Id for this user, if any
                    EmployerAccess access = null;
                    if (u.Employers != null && u.Employers.Any())
                    {
                        access = u.Employers.FirstOrDefault(e => e.IsActive && e.EmployerId == employerId);
                    }
                    
                    // If we have a match, then let's get all the roles from it.
                    if (access != null && access.Roles != null)
                    {
                        appliedRoles.AddRange(access.Roles);
                        employer = access.Employer;
                    }
                    else
                    {
                        // We may not have an employer override, but we need to keep the employer in mind 
                        // in case the employer is disabled
                        employer = Employer.GetById(employerId);
                    }
                        
                }
                // Oh, darn-it, we have no roles still, but wait, let's see if there are root/global roles
                if (u.Roles != null && u.Roles.Any())
                {
                    // Oh goodie, there are, add these instead
                    appliedRoles.AddRangeIfNotExists(u.Roles);
                }
                    

                // Foiled again, no roles at all for this user, either at the top level or for the employer, return None.
                if (!appliedRoles.Any())
                {
                    return Permission.None;
                }
                    

                // Check to see if the applied roles contain the System Administrator role.
                var sysAdmin = Role.SystemAdministrator;
                // If they do, then yeah, grant those permissions instead
                if (appliedRoles.Contains(sysAdmin.Id))
                {
                    return Permission.All(u).Where(p => !Permission.IsDisabled(p, employer)).Select(p => p.Id).ToList();
                }
                    

                // Check/Build our Cache
                if (u.__roleCache == null)
                {
                    u.__roleCache = new List<Role>();
                }
                    
                var toFill = appliedRoles.Where(a => !u.__roleCache.Any(c => c.Id == a)).ToArray();
                if (toFill.Length > 0)
                    // Get the collection of Role entities using a MongoDB $in: [] operator implied in our query here
                    foreach (var role in Role.AsQueryable().Where(r => toFill.Contains(r.Id)))
                    {
                        //Filter out roles based on Portal
                        if (IsEmployeeSelfServicePortal && role.Type == RoleType.SelfService)
                        {
                            u.__roleCache.Add(role);
                        }
                        else if (!IsEmployeeSelfServicePortal && role.Type == RoleType.Portal)
                        {
                            u.__roleCache.Add(role);
                        }
                    }

                //In ESS Portal filter ESS roles based if user works with own data or user's peer data
                u.__roleCache = ApplyRoleFilter(u, u.__roleCache);

                var roles = u.__roleCache.Where(r => appliedRoles.Any(a => a == r.Id)).ToList();
                // Then, get a distinct list of the permissions for all of these combined roles to get the effective permissions for the
                //  user for this employer (if applicable).
                var permissions = roles.SelectMany(r => r.Permissions ?? new List<string>(0)).Distinct().Where(p => !Permission.IsDisabled(p, employer)).ToList();

                return permissions;

            }//end: GetPermissions

            /// <summary>
            /// Takes a user instance and returns a list of all permissions that user *may* have access to. Once
            /// they are in the context of a specific Employer, all bets are off, but this will at least help drive
            /// what is visible to the user action-wise from a UI perspective when the Employer is not yet known or
            /// not yet selected.
            /// </summary>
            /// <param name="u"></param>
            /// <returns></returns>
            public static List<string> GetProjectedPermissions(User u)
            {
                // No user, guess what, no permissions, DUH
                if (u == null || u.IsDeleted || u.IsDisabled)
                    return Permission.None;

                // Build our list of applied roles that will actually create the final permission set
                List<string> appliedRoles = new List<string>();

                // If we have an Employer Id, let's check that first to see if we have roles set for that Employer override
                if (u.Employers != null && u.Employers.Any())
                    // Get the Employer Access objects associated with the user
                    foreach (var access in u.Employers)
                        // For each one, if we have roles, then let's add them to our applied roles if not already in there.
                        if (access.Roles != null && access.Roles.Any())
                            access.Roles.ForEach(r => appliedRoles.AddIfNotExists(r));

                // Let's see if there are root/global roles
                if (u.Roles != null && u.Roles.Any())
                    // Oh goodie, there are, add these as well if not already in there
                    u.Roles.ForEach(r => appliedRoles.AddIfNotExists(r));

                // No roles at all for this user at the top level or for the employer, return None.
                if (!appliedRoles.Any())
                    return Permission.None;

                // Check to see if the applied roles contain the System Administrator role.
                var sysAdmin = Role.SystemAdministrator;
                // If they do, then yeah, grant those permissions instead
                if (appliedRoles.Contains(sysAdmin.Id))
                {
                    if (sysAdmin.Permissions.Contains(Permission.CreateCaseWizardOnly.Id))
                    {
                        sysAdmin.Permissions.Remove(Permission.CreateCaseWizardOnly.Id);
                    }
                    return sysAdmin.Permissions;
                }

                // Check/Build our Cache
                if (u.__roleCache == null)
                    u.__roleCache = new List<Role>();
                var toFill = appliedRoles.Where(a => !u.__roleCache.Any(c => c.Id == a)).ToArray();
                if (toFill.Length > 0)
                    // Get the collection of Role entities using a MongoDB $in: [] operator implied in our query here
                    foreach (var role in Role.AsQueryable().Where(r => toFill.Contains(r.Id)))
                        u.__roleCache.Add(role);

                //In ESS Portal filter ESS roles based if user works with own data or user's peer data
                u.__roleCache = ApplyRoleFilter(u, u.__roleCache);
                var roles = u.__roleCache.Where(r => appliedRoles.Any(a => a == r.Id)).ToList();
                // Then, get a distinct list of the permissions for all of these combined roles to get the effective permissions
                return roles.SelectMany(r => r.Permissions ?? new List<string>(0)).Distinct().ToList();
            }//end: GetProjectedPermissions

            public static List<string> GetEmpIdsWithPermission(User u, List<string> lstEmployerId,string permisson)
            {
                // No user, guess what, no permissions, DUH; oh yeah, disabled and deleted ones have none too
                List<string> empId = new List<string>();
                if (u == null || u.IsDisabled || u.IsDeleted)
                {
                    return Permission.None;
                }

                var UserPermissionList = Permission.All(u).ToList();
                var sysAdmin = Role.SystemAdministrator;

                foreach (string employerId in lstEmployerId)
                {
                    // Build our list of applied roles that will actually create the final permission set
                    List<string> appliedRoles = new List<string>();
                    // If we have an Employer Id, let's check that first to see if we have roles set for that Employer override
                    Employer employer = null;
                    if (!string.IsNullOrWhiteSpace(employerId))
                    {
                        // Get the Employer Access object associated with the employer Id for this user, if any
                        EmployerAccess access = null;
                        if (u.Employers != null && u.Employers.Any())
                        {
                            access = u.Employers.FirstOrDefault(e => e.IsActive && e.EmployerId == employerId);
                        }

                        // If we have a match, then let's get all the roles from it.
                        if (access != null && access.Roles != null)
                        {
                            appliedRoles.AddRange(access.Roles);
                            employer = access.Employer;
                        }
                        else
                        {
                            // We may not have an employer override, but we need to keep the employer in mind 
                            // in case the employer is disabled
                            employer = Employer.GetById(employerId);
                        }

                    }
                    // Oh, darn-it, we have no roles still, but wait, let's see if there are root/global roles
                    if (u.Roles != null && u.Roles.Any())
                    {
                        // Oh goodie, there are, add these instead
                        appliedRoles.AddRangeIfNotExists(u.Roles);
                    }


                    // Foiled again, no roles at all for this user, either at the top level or for the employer, return None.
                    if (!appliedRoles.Any())
                    {
                        //return Permission.None;
                        continue;
                    }


                    // Check to see if the applied roles contain the System Administrator role.
                    // If they do, then yeah, grant those permissions instead
                    if (appliedRoles.Contains(sysAdmin.Id))
                    {
                        var lstPermission = UserPermissionList.Where(p => !Permission.IsDisabled(p, employer)).Select(p => p.Id).ToList();
                        if (employer != null && lstPermission.Contains(permisson))
                        {
                            empId.Add(employerId);
                        }
                        continue;
                    }


                    // Check/Build our Cache
                    if (u.__roleCache == null)
                    {
                        u.__roleCache = new List<Role>();
                    }

                    var toFill = appliedRoles.Where(a => !u.__roleCache.Any(c => c.Id == a)).ToArray();
                    if (toFill.Length > 0)
                        // Get the collection of Role entities using a MongoDB $in: [] operator implied in our query here
                        foreach (var role in Role.AsQueryable().Where(r => toFill.Contains(r.Id)))
                        {
                            //Filter out roles based on Portal
                            if (IsEmployeeSelfServicePortal && role.Type == RoleType.SelfService)
                            {
                                u.__roleCache.Add(role);
                            }
                            else if (!IsEmployeeSelfServicePortal && role.Type == RoleType.Portal)
                            {
                                u.__roleCache.Add(role);
                            }
                        }

                    //In ESS Portal filter ESS roles based if user works with own data or user's peer data
                    u.__roleCache = ApplyRoleFilter(u, u.__roleCache);

                    var roles = u.__roleCache.Where(r => appliedRoles.Any(a => a == r.Id)).ToList();
                    // Then, get a distinct list of the permissions for all of these combined roles to get the effective permissions for the
                    //  user for this employer (if applicable).
                    var permissions = roles.SelectMany(r => r.Permissions ?? new List<string>(0)).Distinct().Where(p => !Permission.IsDisabled(p, employer)).ToList();
                    if (employer != null && permissions.Contains(permisson))
                    {
                        empId.Add(employerId);
                    }
                }
                return empId;

            }

            private static List<Role> ApplyRoleFilter(User u, List<Role> roles)
            {
                if (roles == null || !roles.Any())
                    return roles;

                if (u.UserType == UserType.Admin)
                    return roles.Where(r => r.Type == RoleType.Administration).ToList();
                else if (User.IsEmployeeSelfServicePortal)
                    return roles.Where(r => r.Type == RoleType.SelfService).ToList();
                else
                    return roles.Where(r => r.Type == RoleType.Portal).ToList();
            }

            #endregion Permissions
        }//end: class: Permissions

        /// <summary>
        /// Return true if 
        /// </summary>
        public static bool IsEmployeeSelfServicePortal
        {
            get
            {
                HttpContext context = HttpContext.Current;
                if (context != null && context.Items != null)
                {
                    if (context.Items[CURRENT_USER_PORTAL_CONTEXT] != null && (string)context.Items[CURRENT_USER_PORTAL_CONTEXT] == "ESS")
                        return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Gets the has visibility to list of employee Ids this employee user can access/see.
        /// </summary>
        /// <value>
        /// The has visibility to.
        /// </value>
        public static List<string> HasVisibilityTo
        {
            get
            {
                var user = User.Current;
                if (user == null || user.IsDeleted || user.IsDisabled || user.Employers == null || !user.Employers.Any())
                    return new List<string>(0);
                var myEmployer = user.Employers.FirstOrDefault(e => e.IsActive && e.EmployeeId != null);
                if (myEmployer == null || (Employer.Current != null && Employer.CurrentId != myEmployer.EmployerId))
                    return new List<string>(0);

                return myEmployer.ContactOf.Concat(new List<string> { myEmployer.EmployeeId }).ToList();
            }
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            StringBuilder me = new StringBuilder();
            if (!string.IsNullOrWhiteSpace(FirstName))
                me.Append(FirstName);
            if (!string.IsNullOrWhiteSpace(LastName))
                me.AppendFormat("{0}{1}", me.Length > 0 ? " " : "", LastName);
            if (!string.IsNullOrWhiteSpace(Email))
                me.AppendFormat("{0}{1}", me.Length > 0 ? ", " : "", Email);

            return me.ToString();
        }

        /// <summary>
        /// This would be used to maintain the last Reset Password link was genearated to check the validity of the Reset Password url
        /// </summary>
        [BsonDateTimeOptions(DateOnly = false)]
        public DateTime? ResetKeyDate { get; set; }
    }
}
