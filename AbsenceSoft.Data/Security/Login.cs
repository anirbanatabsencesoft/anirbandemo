﻿using AbsenceSoft.Common;
using System;
using System.Web;
using AbsenceSoft;
using AbsenceSoft.Data.Audit;

namespace AbsenceSoft.Data.Security
{
    /// <summary>
    /// Login Class
    /// </summary>
    [AuditClassNo]
    [Serializable]
    public class Login : Audit<User>
    {
        /// <summary>
        /// Records the user's IP Address from where they logged in.
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// Records the user's browser string from when they logged in.
        /// </summary>
        public string Browser { get; set; }

        /// <summary>
        /// Audits a user's successful login with some additional details.
        /// </summary>
        /// <param name="user">The user to audit.</param>
        /// <param name="ipAddress">The IP Address of the user who logged in.</param>
        /// <param name="browser">The browser string of the user who logged in.</param>
        public static void AuditLogin(User user, string ipAddress = null, string browser = null)
        {
            using (new InstrumentationContext("Login.AuditLogin"))
            {
                string myIp = ipAddress, myBrowser = browser;
                if ((string.IsNullOrWhiteSpace(myIp) || string.IsNullOrWhiteSpace(myBrowser))
                    && HttpContext.Current != null && HttpContext.Current.Request != null)
                {
                    myIp = HttpContext.Current.Request.ClientIPAddress();
                    myBrowser = HttpContext.Current.Request.UserAgent;
                }

                new Login()
                {
                    AuditType = AuditType.Save,
                    CreatedBy = user.Id,
                    IpAddress = myIp,
                    Browser = myBrowser
                }.Save();
            }
        }
    }
}
