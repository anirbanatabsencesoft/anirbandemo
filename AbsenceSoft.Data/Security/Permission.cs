﻿using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Security
{

    /// <seealso cref="System.IEquatable{AbsenceSoft.Data.Security.Permission}" />
    /// <seealso cref="System.Collections.Generic.IEqualityComparer{AbsenceSoft.Data.Security.Permission}" />
    /// <seealso cref="System.IEquatable{System.String}" />
    /// <seealso cref="System.IComparable" />
    /// <seealso cref="System.IComparable{AbsenceSoft.Data.Security.Permission}" />
    /// <seealso cref="System.IComparable{System.String}" />
    /// <seealso cref="System.Collections.Generic.IComparer{AbsenceSoft.Data.Security.Permission}" />
    /// <seealso cref="System.Collections.Generic.IComparer{System.String}" />
    [Serializable]
    public struct Permission : IEquatable<Permission>, IEqualityComparer<Permission>, IEquatable<string>, IComparable, IComparable<Permission>, IComparable<string>, IComparer<Permission>, IComparer<string>
    {
        #region Const

        // Categories
        private const string CATEGORY_Configuration = "Configuration";
        private const string CATEGORY_Administration = "Administration";
        private const string CATEGORY_CaseManagement = "Case Management";
        private const string CATEGORY_EmployeeManagement = "Employee Management";
        private const string CATEGORY_TeamManagement = "Team Management";
        private const string CATEGORY_Workflow = "Workflow";
        private const string CATEGORY_Reporting = "Reporting";
        private const string CATEGORY_Communications = "Communications";
        private const string CATEGORY_ToDos = "ToDo Items";
        private const string CATEGORY_CustomCommunications = "Custom Communications";
        private const string CATEGORY_SelfService = "Self Service";
        private const string CATEGORY_SelfService_Attachments = "Self Service Attachments";
        private const string CATEGORY_SelfService_Communications = "Self Service Communications";
        private const string CATEGORY_Accommodations = "Accommodations";

        // Prefixes
        internal const string PREFIX_System = "";
        internal const string PREFIX_ToDo = "ToDo_";
        internal const string PREFIX_Communication = "Comm_";
        internal const string PREFIX_ContactType = "ContactType_";

        #endregion Private Const

        #region Private Properties

        private readonly string _id;
        private readonly string _name;
        private readonly string _category;
        private readonly string _helpText;
        private readonly RoleType[] _roleTypes;

        #endregion Private Properties

        #region Private .ctor

        /// <summary>
        /// Creates a new instance of <see cref="T:AbsenceSoft.Data.Security.Permission" />.
        /// </summary>
        /// <param name="id">The id of the role, or unique code/name used to identify the role.</param>
        /// <param name="name">The name of the role for display purposes.</param>
        /// <param name="category">The category of the role for display grouping purposes.</param>
        /// <param name="helpText">The help text of the role for display purposes.</param>
        /// <param name="roleTypes">The role types to specify for the permission; the default is ALL of Them!.</param>
        private Permission(string id, string name, string category = null, string helpText = null, params RoleType[] roleTypes)
        {
            _id = id;
            _name = name;
            _category = category;
            _helpText = helpText;
            _roleTypes = roleTypes;
            if (_roleTypes.Length == 0)
                _roleTypes = new RoleType[3] { RoleType.Portal, RoleType.SelfService, RoleType.Administration };
        }//end: ctor

        #endregion Private .ctor

        #region Public Properties

        /// <summary>
        /// Gets the id of the role, or unique code/name used to identify the role
        /// when checking, assigning or otherwise working with permissions
        /// </summary>
        public string Id { get { return _id; } }
        /// <summary>
        /// Gets the name of the role for display purposes.
        /// </summary>
        public string Name { get { return _name; } }
        /// <summary>
        /// Gets the category of the role for display grouping purposes.
        /// </summary>
        public string Category { get { return _category; } }
        /// <summary>
        /// Gets the help text of the role for display purposes.
        /// </summary>
        public string HelpText { get { return _helpText; } }
        /// <summary>
        /// Gets the role types for filtering display purposes.
        /// </summary>
        /// <value>
        /// The role types for filtering display purposes.
        /// </value>
        public RoleType[] RoleTypes { get { return _roleTypes; } }



        #endregion Public Properties


        #region Public Static Properties

        public static List<string> None { get { return new List<string>(); } }

        private static Permission _ViewCustomerAndEmployerInfo = new Permission("ViewCustomerAndEmployerInfo", "View Customer and Employer Info", CATEGORY_Administration, null, RoleType.Portal, RoleType.SelfService);
        public static Permission ViewCustomerAndEmployerInfo { get { return _ViewCustomerAndEmployerInfo; } }

        private static Permission _EditCustomerInfo = new Permission("EditCustomerInfo", "Edit Customer Info", CATEGORY_Administration);
        public static Permission EditCustomerInfo { get { return _EditCustomerInfo; } }

        public const string EditEmployerInfoPermission = "EditEmployerInfo";
        private static Permission _EditEmployerInfo = new Permission(EditEmployerInfoPermission, "Edit Employer Info", CATEGORY_Administration);
        public static Permission EditEmployerInfo { get { return _EditEmployerInfo; } }

        private static Permission _EditSSOSettings = new Permission("EditSSOSettings", "Edit Single Sign On Settings", CATEGORY_Administration, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditSSOSettings { get { return _EditSSOSettings; } }

        private static Permission _EditEmployerContacts = new Permission("EditEmployerContacts", "Edit Employer Contacts", CATEGORY_Administration, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditEmployerContacts { get { return _EditEmployerContacts; } }

        private static Permission _EditCustomerServiceOptions = new Permission("EditCustomerServiceOptions", "Edit Customer Service Options", CATEGORY_Administration, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditCustomerServiceOptions { get { return _EditCustomerServiceOptions; } }

        private static Permission _EditRoleDefinition = new Permission("EditRoleDefinition", "Edit Role Definition", CATEGORY_Administration);
        public static Permission EditRoleDefinition { get { return _EditRoleDefinition; } }
        private static Permission _CloseAccount = new Permission("CloseAccount", "Close Account", CATEGORY_Administration, null, RoleType.Portal, RoleType.SelfService);
        public static Permission CloseAccount { get { return _CloseAccount; } }

        private static Permission _EditCustomerUsers = new Permission("EditCustomerUsers", "Edit Customer's Users", CATEGORY_Administration, null, RoleType.Administration);
        public static Permission EditCustomerUsers { get { return _EditCustomerUsers; } }
        private static Permission _DeleteCustomer = new Permission("DeleteCustomer", "Delete Customer", CATEGORY_Administration, null, RoleType.Administration);
        public static Permission DeleteCustomer { get { return _DeleteCustomer; } }
        private static Permission _ClearEmployerData = new Permission("ClearEmployerData", "Clear Employer Data", CATEGORY_Administration, null, RoleType.Administration);
        public static Permission ClearEmployerData { get { return _ClearEmployerData; } }

        private static Permission _EditIPRanges = new Permission("EditIPRanges", "Edit IP Ranges", CATEGORY_Configuration, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditIPRanges { get { return _EditIPRanges; } }

        private static Permission _EditAbsenceReason = new Permission("EditAbsenceReasons", "Edit Absence Reasons", CATEGORY_Configuration, null, RoleType.Portal);
        public static Permission EditAbsenceReason { get { return _EditAbsenceReason; } }
        private static Permission _EditAccommodationType = new Permission("EditAccommodationTypes", "Edit Accommodation Types", CATEGORY_Configuration, null, RoleType.Portal);
        public static Permission EditAccommodationType { get { return _EditAccommodationType; } }
        private static Permission _EditPolicyCrosswalkType = new Permission("EditPolicyCrosswalkTypes", "Edit Policy Crosswalk Types", CATEGORY_Configuration, null, RoleType.Portal);
        public static Permission EditPolicyCrosswalkType { get { return _EditPolicyCrosswalkType; } }
        private static Permission _EditLeavePolicy = new Permission("EditLeavePolicy", "Edit Leave Policy", CATEGORY_Configuration, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditLeavePolicy { get { return _EditLeavePolicy; } }
        private static Permission _EditWorkflow = new Permission("EditWorkflow", "Edit Workflow Settings", CATEGORY_Configuration);
        public static Permission EditWorkflow { get { return _EditWorkflow; } }
        private static Permission _EditNoteCategory = new Permission("EditNoteCategories", "Edit Note Categories", CATEGORY_Configuration, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditNoteCategory { get { return _EditNoteCategory; } }

        private static Permission _EditCaseCloseReason = new Permission("EditCaseCloseReason", "Edit Case Case Close Reason", CATEGORY_Configuration, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditCaseCloseReason { get { return _EditCaseCloseReason; } }

        private static Permission _DeleteCaseCloseReason = new Permission("DeleteCaseCloseReason", "Delete Case Close Reason", CATEGORY_Configuration, null, RoleType.SelfService, RoleType.Portal);
        public static Permission DeleteCaseCloseReason { get { return _DeleteCaseCloseReason; } }
        private static Permission _CreateCaseCloseReason = new Permission("CreateCaseCloseReason", "Create Case Close Reason", CATEGORY_Configuration, null, RoleType.SelfService, RoleType.Portal);
        public static Permission CreateCaseCloseReason { get { return _CreateCaseCloseReason; } }

        private static Permission _EditCustomContent = new Permission("EditCustomContent", "Edit Custom Content", CATEGORY_Configuration, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditCustomContent { get { return _EditCustomContent; } }

        private static Permission _EditCommunicationTemplate = new Permission("EditCommunicationTemplate", "Edit Communication Template", CATEGORY_Configuration);
        public static Permission EditCommunicationTemplate { get { return _EditCommunicationTemplate; } }
        private static Permission _DeleteCommunicationTemplate = new Permission("DeleteCommunicationTemplate", "Delete Communication Template", CATEGORY_Configuration, null, RoleType.Administration);
        public static Permission DeleteCommunicationTemplate { get { return _DeleteCommunicationTemplate; } }
        private static Permission _EditPolicies = new Permission("EditPolicies", "Edit Policies", CATEGORY_Configuration);
        public static Permission EditPolicies { get { return _EditPolicies; } }
        private static Permission _EditPaySchedules = new Permission("EditPaySchedules", "Edit Pay Schedules", CATEGORY_Configuration, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditPaySchedules { get { return _EditPaySchedules; } }
        private static Permission _EditCustomFields = new Permission("EditCustomFields", "Edit Custom Fields", CATEGORY_Configuration, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditCustomFields { get { return _EditCustomFields; } }

        private static Permission _EditOrganizations = new Permission("EditOrganizations", "Edit Organizations", CATEGORY_Configuration, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditOrganizations { get { return _EditOrganizations; } }
        private static Permission _EditOshaApiToken = new Permission("EditOshaApiToken", "Edit Osha Api Token", CATEGORY_Configuration, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditOshaApiToken { get { return _EditOshaApiToken; } }
        private static Permission _EditPhysicalDemands = new Permission("EditPhysicalDemands", "Edit Physical Demands", CATEGORY_Configuration, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditPhysicalDemands { get { return _EditPhysicalDemands; } }
        private static Permission _EditNecessities = new Permission("EditNecessities", "Edit Necessities", CATEGORY_Configuration, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditNecessities { get { return _EditNecessities; } }
        private static Permission _EditRiskProfiles = new Permission("EditRiskProfiles", "Edit Risk Profiles", CATEGORY_Configuration, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditRiskProfiles { get { return _EditRiskProfiles; } }

        private static Permission _EditConsultations = new Permission("EditConsultations", "Edit Consultations", CATEGORY_Configuration, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditConsultations { get { return _EditConsultations; } }

        private static Permission _EditJobs = new Permission("EditJobs", "Edit Jobs", CATEGORY_Configuration, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditJobs { get { return _EditJobs; } }

        private static Permission _RunAdHocReport = new Permission("RunAdHocReport", "Run AdHoc Report", CATEGORY_Reporting, null, RoleType.SelfService, RoleType.Portal);
        public static Permission RunAdHocReport { get { return _RunAdHocReport; } }

        private static Permission _ExportReportDefinition = new Permission("ExportReportDefinition", "Export Report Definition", CATEGORY_Reporting, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ExportReportDefinition { get { return _ExportReportDefinition; } }

        private static Permission _ImportReportDefinition = new Permission("ImportReportDefinition", "Import Report Definition", CATEGORY_Reporting, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ImportReportDefinition { get { return _ImportReportDefinition; } }

        private static Permission _RunOshaReport = new Permission("RunOshaReport", "Run OSHA Report", CATEGORY_Reporting, null, RoleType.SelfService, RoleType.Portal);
        public static Permission RunOshaReport { get { return _RunOshaReport; } }

        private static Permission _ViewAdminReports = new Permission("ViewAdminReport", "View Report", CATEGORY_Reporting, null, RoleType.Administration);
        public static Permission ViewAdminReports { get { return _ViewAdminReports; } }

        public const string RunCommunicationReportPermission = "RunCommunicationReport";
        private static Permission _RunCommunicationReport = new Permission("RunCommunicationReport", "Run Communication Report", CATEGORY_Reporting, null, RoleType.SelfService, RoleType.Portal);
        public static Permission RunCommunicationReport { get { return _RunCommunicationReport; } }

        private static Permission _RunCaseManagerReport = new Permission("RunCaseManagerReport", "Run Case Manager Report", CATEGORY_Reporting, null, RoleType.SelfService, RoleType.Portal);
        public static Permission RunCaseManagerReport { get { return _RunCaseManagerReport; } }
        private static Permission _RunSelfServiceReport = new Permission("RunSelfServiceReport", "Run Self-Service Report", CATEGORY_Reporting, null, RoleType.SelfService, RoleType.Portal);
        public static Permission RunSelfServiceReport { get { return _RunSelfServiceReport; } }
        private static Permission _RunBIReport = new Permission("RunBIReport", "Run BI Report", CATEGORY_Reporting, null, RoleType.SelfService, RoleType.Portal);
        public static Permission RunBIReport { get { return _RunBIReport; } }

        private static Permission _ViewTeam = new Permission("ViewTeam", "View Team", CATEGORY_TeamManagement, null, RoleType.Portal);
        public static Permission ViewTeam { get { return _ViewTeam; } }
        private static Permission _AssignTeams = new Permission("AssignTeams", "Assign Teams", CATEGORY_TeamManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission AssignTeams { get { return _AssignTeams; } }
        private static Permission _TeamDashboard = new Permission("TeamDashboard", "Team Dashboard", CATEGORY_TeamManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission TeamDashboard { get { return _TeamDashboard; } }
        private static Permission _AllItems = new Permission("AllItems", "View All Items and Users", CATEGORY_TeamManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission AllItems { get { return _AllItems; } }       
        private static Permission _SearchCases = new Permission("SearchCases", "Search Cases", CATEGORY_TeamManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission SearchCases { get { return _SearchCases; } }
        private static Permission _ReAssignToDo = new Permission("ReAssignToDo", "Re-Assign ToDo", CATEGORY_TeamManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ReAssignToDo { get { return _ReAssignToDo; } }
        private static Permission _ReAssignCase = new Permission("ReAssignCase", "Re-Assign Case", CATEGORY_TeamManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ReAssignCase { get { return _ReAssignCase; } }
        private static Permission _CaseAssignee = new Permission("CaseAssignee", "Case Assignee", CATEGORY_TeamManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission CaseAssignee { get { return _CaseAssignee; } }
        private static Permission _EditUser = new Permission("EditUser", "Edit User", CATEGORY_TeamManagement);
        public static Permission EditUser { get { return _EditUser; } }
        private static Permission _EditUserRoles = new Permission("EditUserRoles", "Edit User Roles", CATEGORY_TeamManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditUserRoles { get { return _EditUserRoles; } }
        private static Permission _DeleteUser = new Permission("DeleteUser", "Delete User", CATEGORY_TeamManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission DeleteUser { get { return _DeleteUser; } }
        private static Permission _DisableUser = new Permission("DisableUser", "Disable User", CATEGORY_TeamManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission DisableUser { get { return _DisableUser; } }

        public const string EnableUserPermission = "EnableUser";
        private static Permission _EnableUser = new Permission(EnableUserPermission, "Enable User", CATEGORY_TeamManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EnableUser { get { return _EnableUser; } }
        private static Permission _ManageUserAccount = new Permission("ManageUserAccount", "Manage User Account", CATEGORY_TeamManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ManageUserAccount { get { return _ManageUserAccount; } }

        //CATEGORY_EmployeeManagement
        private static Permission _ViewEmployee = new Permission("ViewEmployee", "View Employee", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewEmployee { get { return _ViewEmployee; } }
        private static Permission _CreateEmployee = new Permission("CreateEmployee", "Create Employee", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission CreateEmployee { get { return _CreateEmployee; } }
        private static Permission _EditEmployee = new Permission("EditEmployee", "Edit Employee", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditEmployee { get { return _EditEmployee; } }
        private static Permission _DeleteEmployee = new Permission("DeleteEmployee", "Delete Employee", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission DeleteEmployee { get { return _DeleteEmployee; } }

        private static Permission _ViewSSN = new Permission("ViewSSN", "View SSN", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewSSN { get { return _ViewSSN; } }
        private static Permission _EditSSN = new Permission("EditSSN", "Edit SSN", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditSSN { get { return _EditSSN; } }
        private static Permission _ViewSalary = new Permission("ViewSalary", "View Salary", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewSalary { get { return _ViewSalary; } }
        private static Permission _EditSalary = new Permission("EditSalary", "Edit Salary", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditSalary { get { return _EditSalary; } }

        private static Permission _ViewEmployeeInfo = new Permission("ViewEmployeeInfo", "View Employee Info", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewEmployeeInfo { get { return _ViewEmployeeInfo; } }
        private static Permission _EditEmployeeInfo = new Permission("EditEmployeeInfo", "Edit Employee Info", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditEmployeeInfo { get { return _EditEmployeeInfo; } }

        private static Permission _ViewEmployeeContactInfo = new Permission("ViewEmployeeContactInfo", "View Employee Contact Info", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewEmployeeContactInfo { get { return _ViewEmployeeContactInfo; } }
        private static Permission _EditEmployeeContactInfo = new Permission("EditEmployeeContactInfo", "Edit Employee Contact Info", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditEmployeeContactInfo { get { return _EditEmployeeContactInfo; } }

        private static Permission _ViewEmployeeJobInfo = new Permission("ViewEmployeeJobInfo", "View Employee Job Info", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewEmployeeJobInfo { get { return _ViewEmployeeJobInfo; } }
        private static Permission _EditEmployeeJobInfo = new Permission("EditEmployeeJobInfo", "Edit Employee Job Info", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditEmployeeJobInfo { get { return _EditEmployeeJobInfo; } }
        private static Permission _ViewJobs = new Permission("ViewJobs", "View Jobs", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewJobs { get { return _ViewJobs; } }
        private static Permission _JobAssignment = new Permission("JobAssignment", "Job Assignment", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission JobAssignment { get { return _JobAssignment; } }

        private static Permission _ViewEmployeeJobDetails = new Permission("ViewEmployeeJobDetails", "View Employee Job Details", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewEmployeeJobDetails { get { return _ViewEmployeeJobDetails; } }
        private static Permission _EditEmployeeJobDetails = new Permission("EditEmployeeJobDetails", "Edit Employee Job Details", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditEmployeeJobDetails { get { return _EditEmployeeJobDetails; } }

        private static Permission _ViewEmployeeWorkSchedule = new Permission("ViewEmployeeWorkSchedule", "View Employee Work Schedule", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewEmployeeWorkSchedule { get { return _ViewEmployeeWorkSchedule; } }
        private static Permission _EditEmployeeWorkSchedule = new Permission("EditEmployeeWorkSchedule", "Edit Employee Work Schedule", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditEmployeeWorkSchedule { get { return _EditEmployeeWorkSchedule; } }

        private static Permission _ViewEmployeeTimeTracker = new Permission("ViewEmployeeTimeTracker", "View Employee Time Tracker", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewEmployeeTimeTracker { get { return _ViewEmployeeTimeTracker; } }
        private static Permission _ViewEmployeeAbsenceHistory = new Permission("ViewEmployeeAbsenceHistory", "View Employee Absence History", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewEmployeeAbsenceHistory { get { return _ViewEmployeeAbsenceHistory; } }

        private static Permission _ViewCustomFields = new Permission("ViewCustomFields", "View Custom Fields", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewCustomFields { get { return _ViewCustomFields; } }

        private static Permission _ViewContact = new Permission("ViewContact", "View Contact", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewContact { get { return _ViewContact; } }

        private static Permission _CreateContact = new Permission("CreateContact", "Create Contact", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission CreateContact { get { return _CreateContact; } }
        private static Permission _EditContact = new Permission("EditContact", "Edit Contact", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditContact { get { return _EditContact; } }
        private static Permission _DeleteContact = new Permission("DeleteContact", "Delete Contact", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission DeleteContact { get { return _DeleteContact; } }

        private static Permission _ViewPersonalContact = new Permission("ViewPersonalContact", "View Personal Contacts", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewPersonalContact { get { return _ViewPersonalContact; } }
        private static Permission _ViewMedicalContact = new Permission("ViewMedicalContact", "View Medical Contact", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewMedicalContact { get { return _ViewMedicalContact; } }
        private static Permission _ViewAdministrativeContact = new Permission("ViewAdministrativeContact", "View Administrative Contact", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewAdministrativeContact { get { return _ViewAdministrativeContact; } }

        private static Permission _UploadEligibilityFile = new Permission("UploadEligibilityFile", "Upload Eligibility File", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission UploadEligibilityFile { get { return _UploadEligibilityFile; } }

        private static Permission _ViewAllEmployees = new Permission("ViewAllEmployees", "View All Employees", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewAllEmployees { get { return _ViewAllEmployees; } }

        //CATEGORY_CaseManagement
        private static Permission _ViewCase = new Permission("ViewCase", "View Case", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewCase { get { return _ViewCase; } }
        public const string CreateCasePermissionId = "CreateCase";
        private static Permission _CreateCase = new Permission(CreateCasePermissionId, "Create Case", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission CreateCase { get { return _CreateCase; } }
        private static Permission _EditCase = new Permission("EditCase", "Edit Case", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditCase { get { return _EditCase; } }
        private static Permission _EditCaseEvents = new Permission("EditCaseEvents", "Edit Case Events", CATEGORY_CaseManagement, null, RoleType.Portal);
        public static Permission EditCaseEvents { get { return _EditCaseEvents; } }
        private static Permission _EditCaseSummaryDescription = new Permission("EditCaseSummaryDescription", "Edit Case Summary & Description", CATEGORY_CaseManagement, null, RoleType.Portal);
        public static Permission EditCaseSummaryDescription { get { return _EditCaseSummaryDescription; } }
        private static Permission _ViewCaseDescription = new Permission("ViewCaseDescription", "View Case Description", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewCaseDescription { get { return _ViewCaseDescription; } }
        private static Permission _AdjudicateCase = new Permission("AdjudicateCase", "Adjudicate Case", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission AdjudicateCase { get { return _AdjudicateCase; } }
        private static Permission _CancelCase = new Permission("CancelCase", "Cancel Case", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission CancelCase { get { return _CancelCase; } }
        private static Permission _AttachFile = new Permission("AttachFile", "Attach File", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission AttachFile { get { return _AttachFile; } }
        private static Permission _ViewAttachment = new Permission("ViewAttachment", "View Attached File", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewAttachment { get { return _ViewAttachment; } }
        private static Permission _EditAttachment = new Permission("EditAttachment", "Edit Attached File", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditAttachment { get { return _EditAttachment; } }
        private static Permission _DeleteAttachment = new Permission("DeleteAttachment", "Delete Attached File", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission DeleteAttachment { get { return _DeleteAttachment; } }

        private static Permission _ViewConfidentialAttachments = new Permission("ViewConfidentialAttachments", "View Confidential Attachments", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewConfidentialAttachments { get { return _ViewConfidentialAttachments; } }

        private static Permission _ViewSTDDetail = new Permission("ViewSTDDetail", "View STD Detail", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewSTDDetail { get { return _ViewSTDDetail; } }

        private static Permission _EditSTDDetail = new Permission("EditSTDDetail", "Edit STD Detail", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditSTDDetail { get { return _EditSTDDetail; } }

        private static Permission _ViewCertificationDetail = new Permission("ViewCertificationDetail", "View Certification Detail", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewCertificationDetail { get { return _ViewCertificationDetail; } }
        private static Permission _AddCertification = new Permission("AddCertification", "Add Certification", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission AddCertification { get { return _AddCertification; } }
        private static Permission _EditCertification = new Permission("EditCertification", "Edit Certification", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditCertification { get { return _EditCertification; } }
        private static Permission _DeleteCertification = new Permission("DeleteCertification", "Delete Certification", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission DeleteCertification { get { return _DeleteCertification; } }

        private static Permission _ViewITOR = new Permission("ViewITOR", "View Intermittent Timeoff Request", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewITOR { get { return _ViewITOR; } }
        private static Permission _AddITOR = new Permission("AddITOR", "Add Intermittent Timeoff Request", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission AddITOR { get { return _AddITOR; } }
        private static Permission _EditITOR = new Permission("EditITOR", "Edit Intermittent Timeoff Request", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditITOR { get { return _EditITOR; } }
        private static Permission _ApproveITOR = new Permission("ApproveITOR", "Approve Intermittent Timeoff Request", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ApproveITOR { get { return _ApproveITOR; } }

        public const string CreateCaseWizardOnlyPermissionId = "CreateCaseWizardOnly";
        private static Permission _CreateCaseWizardOnly = new Permission(CreateCaseWizardOnlyPermissionId, "Use Create Case Wizard", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission CreateCaseWizardOnly { get { return _CreateCaseWizardOnly; } }

        public const string EmployeeConsultsPermissionId = "EmployeeConsults";
        private static Permission _EmployeeConsults = new Permission(EmployeeConsultsPermissionId, "Employee Consults", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EmployeeConsults { get { return _EmployeeConsults; } }

        private static Permission _CaseBulkDownload = new Permission("CaseBulkDownload", "Case Bulk Download", CATEGORY_CaseManagement, null, RoleType.Portal);
        public static Permission CaseBulkDownload { get { return _CaseBulkDownload; } }

        public const string ConvertInquiryToCasePermissionId = "ConvertInquiryToCase";
        private static Permission _ConvertInquiryToCase = new Permission(ConvertInquiryToCasePermissionId, "Convert an Inquiry to Case", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ConvertInquiryToCase { get { return _ConvertInquiryToCase; } }

        // Accommodations
        private static Permission _ADADetail = new Permission("ADADetail", "View Accommodation Detail", CATEGORY_Accommodations, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ADADetail { get { return _ADADetail; } }

        private static Permission _AddAccommodation = new Permission("AddAccommodation", "Add Accommodation Detail", CATEGORY_Accommodations, null, RoleType.SelfService, RoleType.Portal);
        public static Permission AddAccommodation { get { return _AddAccommodation; } }

        private static Permission _ADAInteractiveProcess = new Permission("ADAInteractiveProcess", "View Accommodation Interactive Process", CATEGORY_Accommodations, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ADAInteractiveProcess { get { return _ADAInteractiveProcess; } }
        private static Permission _ADAEditInteractiveProcess = new Permission("ADAEditInteractiveProcess", "Edit Accommodation Interactive Process", CATEGORY_Accommodations, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ADAEditInteractiveProcess { get { return _ADAEditInteractiveProcess; } }
        private static Permission _EditDecisionDate = new Permission("EditDecisionDate", "Edit Decision Date", CATEGORY_Accommodations, null, RoleType.Portal);
        public static Permission EditDecisionDate { get { return _EditDecisionDate; } }
        private static Permission _ViewWorkRestrictionDetail = new Permission("ViewWorkRestrictionDetail", "View Work Restriction Detail", CATEGORY_Accommodations, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewWorkRestrictionDetail { get { return _ViewWorkRestrictionDetail; } }
        private static Permission _AddWorkRestrictionDetail = new Permission("AddWorkRestrictionDetail", "Add Work Restriction Detail", CATEGORY_Accommodations, null, RoleType.SelfService, RoleType.Portal);
        public static Permission AddWorkRestrictionDetail { get { return _AddWorkRestrictionDetail; } }
        private static Permission _EditWorkRestrictionDetail = new Permission("EditWorkRestrictionDetail", "Edit Work Restriction Detail", CATEGORY_Accommodations, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditWorkRestrictionDetail { get { return _EditWorkRestrictionDetail; } }
        private static Permission _DeleteWorkRestrictionDetail = new Permission("DeleteWorkRestrictionDetail", "Delete Work Restriction Detail", CATEGORY_Accommodations, null, RoleType.SelfService, RoleType.Portal);
        public static Permission DeleteWorkRestrictionDetail { get { return _DeleteWorkRestrictionDetail; } }

        // Necessities
        private static Permission _ViewEmployeeNecessity = new Permission("ViewEmployeeNecessity", "View Employee Necessities", CATEGORY_Accommodations, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewEmployeeNecessity { get { return _ViewEmployeeNecessity; } }
        private static Permission _AddEmployeeNecessity = new Permission("AddEmployeeNecessity", "Add Employee Necessity", CATEGORY_Accommodations, null, RoleType.SelfService, RoleType.Portal);
        public static Permission AddEmployeeNecessity { get { return _AddEmployeeNecessity; } }
        private static Permission _EditEmployeeNecessity = new Permission("EditEmployeeNecessity", "Edit Employee Necessity", CATEGORY_Accommodations, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditEmployeeNecessity { get { return _EditEmployeeNecessity; } }
        private static Permission _DeleteEmployeeNecessity = new Permission("DeleteEmployeeNecessity", "Delete Employee Necessity", CATEGORY_Accommodations, null, RoleType.SelfService, RoleType.Portal);
        public static Permission DeleteEmployeeNecessity { get { return _DeleteEmployeeNecessity; } }



        //ToDo
        private static Permission _ViewTodo = new Permission("ViewTodo", "View Todo", CATEGORY_Workflow, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewTodo { get { return _ViewTodo; } }
        private static Permission _CreateToDo = new Permission("CreateToDo", "Create ToDo", CATEGORY_Workflow, null, RoleType.SelfService, RoleType.Portal);
        public static Permission CreateToDo { get { return _CreateToDo; } }
        private static Permission _CancelToDo = new Permission("CancelToDo", "Cancel ToDo", CATEGORY_Workflow, null, RoleType.SelfService, RoleType.Portal);
        public static Permission CancelToDo { get { return _CancelToDo; } }
        private static Permission _CompleteToDo = new Permission("CompleteToDo", "Complete ToDo", CATEGORY_Workflow, null, RoleType.SelfService, RoleType.Portal);
        public static Permission CompleteToDo { get { return _CompleteToDo; } }
        private static Permission _ChangeToDoDueDate = new Permission("ChangeToDoDueDate", "Change ToDo Due Date", CATEGORY_Workflow, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ChangeToDoDueDate { get { return _ChangeToDoDueDate; } }
        private static Permission _ViewCaseWorkflows = new Permission("ViewCaseWorkflows", "View Case Workflows", CATEGORY_Workflow, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewCaseWorkflows { get { return _ViewCaseWorkflows; } }
        private static Permission _EditCaseWorkflows = new Permission("EditCaseWorkflows", "Edit Case Workflows", CATEGORY_Workflow, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditCaseWorkflows { get { return _EditCaseWorkflows; } }
        private static Permission _StartCaseWorkflows = new Permission("StartCaseWorkflows", "Start Case Workflows", CATEGORY_Workflow, null, RoleType.SelfService, RoleType.Portal);
        public static Permission StartCaseWorkflows { get { return _StartCaseWorkflows; } }

        public const string ReAssignTodoPermissionId = "ReAssignTodo";
        private static Permission _ReAssignTodo = new Permission(ReAssignTodoPermissionId, "Reassign Todo", CATEGORY_Workflow, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ReAssignTodo { get { return _ReAssignTodo; } }

        private static Permission _SendCommunication = new Permission("SendCommunication", "Send Communication", CATEGORY_Workflow, null, RoleType.SelfService, RoleType.Portal);
        public static Permission SendCommunication { get { return _SendCommunication; } }
        private static Permission _ViewConfidentialCommunications = new Permission("ViewConfidentialCommunications", "View Confidential Communications", CATEGORY_Workflow, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewConfidentialCommunications { get { return _ViewConfidentialCommunications; } }
        private static Permission _ExportData = new Permission("ExportData", "Export Data", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ExportData { get { return _ExportData; } }
        //Notes
        private static Permission _ViewNotes = new Permission("ViewNotes", "View Notes", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewNotes { get { return _ViewNotes; } }
        private static Permission _CreateNotes = new Permission("CreateNotes", "Create Notes", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission CreateNotes { get { return _CreateNotes; } }
        private static Permission _EditNotes = new Permission("EditNotes", "Edit Notes", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditNotes { get { return _EditNotes; } }
        private static Permission _DeleteNotes = new Permission("DeleteNotes", "Delete Notes", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission DeleteNotes { get { return _DeleteNotes; } }
        private static Permission _ViewConfidentialNotes = new Permission("ViewConfidentialNotes", "View Confidential Notes", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission ViewConfidentialNotes { get { return _ViewConfidentialNotes; } }
        private static Permission _EditOshaFormField = new Permission("EditOshaFormFields", "Edit Osha Form Field", CATEGORY_Configuration, null, RoleType.Portal);
        public static Permission EditOshaFormField { get { return _EditOshaFormField; } }

        //Self Service Permissions
        private static Permission _EmployeeDashboard = new Permission("EmployeeDashboard", "Employee Dashboard", CATEGORY_SelfService, null, RoleType.SelfService);
        public static Permission EmployeeDashboard { get { return _EmployeeDashboard; } }
        private static Permission _MyEmployeesDashboard = new Permission("MyEmployeesDashboard", "My Employees Dashboard", CATEGORY_SelfService, null, RoleType.SelfService);
        public static Permission MyEmployeesDashboard { get { return _MyEmployeesDashboard; } }
        public static Permission ESSMyReports { get { return new Permission("RunCaseManagerReport", "My Reports", CATEGORY_SelfService, null, RoleType.SelfService); } }

        //Edit E-Communication Request Flag
        private static Permission _EditECommunicationRequest = new Permission("EditECommunicationRequest", "Edit E-Communication Request", CATEGORY_CaseManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditECommunicationRequest { get { return _EditECommunicationRequest; } }

        //Edit Denial Reasons
        private static Permission _EditDenialReason = new Permission("EditDenialReason", "Edit Denial Reason", CATEGORY_Configuration, null, RoleType.Portal);
        public static Permission EditDenialReason { get { return _EditDenialReason; } }

        //Edit Employee Class
        private static Permission _EditEmployeeClass = new Permission("EditEmployeeClass", "Edit Employee Class", CATEGORY_Configuration, null, RoleType.Portal);
        public static Permission EditEmployeeClass { get { return _EditEmployeeClass; } }

        //Edit Case Reporter/Phone
        private static Permission _EditCaseReporterAndPhone = new Permission("EditCaseReporterAndPhone", "Edit Case Reporter", CATEGORY_EmployeeManagement, null, RoleType.SelfService, RoleType.Portal);
        public static Permission EditCaseReporterAndPhone { get { return _EditCaseReporterAndPhone; } }


        public static string[] AnyReports
        {
            get
            {
                return _AnyReports;
            }
        }
        /// <summary>
        /// Any reports permissions
        /// </summary>
        private static string[] _AnyReports = new string[]
        {
            _RunCaseManagerReport.Id,
            _RunBIReport.Id,
            _RunAdHocReport.Id
        };

        public static string[] AnyAdmin
        {
            get
            {
                return _AnyAdmin;
            }
        }

        /// <summary>
        /// Any admin permissions
        /// </summary>
        private static string[] _AnyAdmin = new string[]
        {
            _ViewCustomerAndEmployerInfo.Id,
            _EditCustomerInfo.Id,
            _EditEmployerInfo.Id,
            _EditRoleDefinition.Id,
            _CloseAccount.Id,
            _EditIPRanges.Id,
            _EditLeavePolicy.Id,
            _EditWorkflow.Id,
            _EditCommunicationTemplate.Id,
            _EditUser.Id,
            _EditUserRoles.Id,
            _DeleteUser.Id,
            _DisableUser.Id,
            _EnableUser.Id,
            _UploadEligibilityFile.Id,
            _ExportData.Id,
            _EditCustomerServiceOptions.Id
        };

        /// <summary>
        /// A hardcoded list of permissions that are disabled when the employer is disabled
        /// </summary>
        private static string[] _DisabledWhenEmployerIsDisabled = new string[]
        {
            CreateCase.Id,
            CreateEmployee.Id,

            AddAccommodation.Id,

            AdjudicateCase.Id,

            EditEmployee.Id,
            EditNotes.Id,

            DeleteAttachment.Id,
            DeleteCertification.Id,

            DeleteContact.Id,
            DeleteEmployee.Id,
            DeleteEmployeeNecessity.Id,
            DeleteNotes.Id,
            DeleteWorkRestrictionDetail.Id,

            AddWorkRestrictionDetail.Id,
            EditWorkRestrictionDetail.Id,
            DeleteWorkRestrictionDetail.Id,

            AddITOR.Id,
            ApproveITOR.Id,
            EditITOR.Id,

            AddCertification.Id,
            EditCertification.Id,
            DeleteCertification.Id,

            AddEmployeeNecessity.Id,
            EditEmployeeNecessity.Id,
            DeleteEmployeeNecessity.Id,

            EditSTDDetail.Id
        };



        #endregion Public Static Properties

        #region Static Methods



        private static List<Permission> _memberPermissions = null;
        private static object _sync = new object();

        /// <summary>
        /// Determines whether the specified permission is disabled if the employer is disabled.
        /// </summary>
        /// <param name="employer">The employer.</param>
        /// <returns>
        ///   <c>true</c> if the specified employer is disabled; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsDisabled(Permission permission, Employer employer)
        {
            return IsDisabled(permission.Id, employer);
        }

        /// <summary>
        /// Determines whether the specified permission identifier is disabled.
        /// </summary>
        /// <param name="permissionId">The permission identifier.</param>
        /// <param name="employer">The employer.</param>
        /// <returns>
        ///   <c>true</c> if the specified permission identifier is disabled; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsDisabled(string permissionId, Employer employer)
        {
            // This could be modified to accept a list of disabled permissions if we need to make that configurable in the future

            if (employer == null)
            {
                return false;
            }

            if (!employer.IsDisabled)
            {
                return false;
            }


            return _DisabledWhenEmployerIsDisabled.Contains(permissionId);
        }

        private static bool _loadingPermissions = false;
        /// <summary>
        /// Gets all of the permissions available in the system in no particular order (sorting is up to the UI or caller).
        /// </summary>
        /// <returns>A list of permissions.</returns>
        public static IEnumerable<Permission> All(User currentUser = null)
        {
            if (_memberPermissions == null)
            {
                lock (_sync)
                {
                    if (_memberPermissions == null)
                    {
                        _loadingPermissions = true;
                        _memberPermissions = new List<Permission>();
                        Type t = typeof(Permission);
                        var props = t.GetProperties(BindingFlags.Public | BindingFlags.Static | BindingFlags.GetProperty | BindingFlags.DeclaredOnly);
                        foreach (var p in props.Where(r => r.PropertyType == typeof(Permission)))
                            _memberPermissions.Add((Permission)p.GetValue(null));
                        _loadingPermissions = false;
                    }
                }
            }

            while (_loadingPermissions)
            {
                // Lock up the current thread until some other thread finishes filling out the _memberPermissions
                System.Threading.Thread.Sleep(0);
            }

            foreach (var p in _memberPermissions)
                yield return p;

            foreach (var p in AllToDos())
                yield return p;

            List<Permission> comms = new List<Permission>();
            if (currentUser == null)
                currentUser = User.Current;


            // Get all templates from DB, ONCE, then pass List into various occassions below. This prevents hell on DB earth hitting Template collection later on.
            var allTemplates = Template.AsQueryable().ToList();  

            if (currentUser != null)
            {
                /// get the custom communications for just the customer
                foreach (var communication in AllCommunicationsUsingList(allTemplates, currentUser.CustomerId)) // TODO Change to use Customer part of allCommunications list !!!!!!!!!!!!!!!!!!!!!!!!!!!
                {
                    comms.AddIfNotExists(communication);
                }

                if (currentUser.Employers != null)
                {
                    /// Get all the custom communications for each employer the current user has access to
                    foreach (var emp in currentUser.Employers)
                    {
                        foreach (var communication in AllCommunicationsUsingList(allTemplates, currentUser.CustomerId, emp.EmployerId)) // TODO Change to use Employer part of allCommunications list !!!!!!!!!!!!!!!!!!!!!!!!!!!
                        {
                            comms.AddIfNotExists(communication);
                        }
                    }
                }
            }

            /// Get all the communications that are default (IE, no customer, no employer)
            foreach (var p in AllCommunicationsUsingList(allTemplates)) // TODO Change to use All part of allCommunications list  !!!!!!!!!!!!!!!!!!!!!!!!!!!
                comms.AddIfNotExists(p);
            foreach (var p in comms)
                yield return p;
        }//end: All

        /// <summary>
        /// Gets all of the permissions available in the system in no particular order (sorting is up to the UI or caller)
        /// for the given list of ids passed in, if any.
        /// </summary>
        /// <param name="ids">The Id values of the permissions to return</param>
        /// <returns>A list of permissions matching the Ids passed in.</returns>
        public static IEnumerable<Permission> All(params string[] ids)
        {
            if (ids == null || !ids.Any())
                return new List<Permission>(0);
            return All().ToList().Where(p => ids.Contains(p.Id)).ToList();
        }//end: All ids

        public static IEnumerable<Permission> AllToDos()
        {
            foreach (string name in Enum.GetNames(typeof(ToDoItemType)))
            {
                if ((int)Enum.Parse(typeof(ToDoItemType), name) >= 0)
                {
                    yield return new Permission(string.Concat(PREFIX_ToDo, name), name.SplitCamelCaseString(), CATEGORY_ToDos);
                }
            }
        }//end: AllToDos

        public static IEnumerable<Permission> ToDos(params ToDoItemType[] types)
        {
            foreach (string name in types.Select(t => Enum.GetName(typeof(ToDoItemType), t)))
                yield return new Permission(string.Concat(PREFIX_ToDo, name), name.SplitCamelCaseString(), CATEGORY_ToDos);
        }//end: ToDos

        /// <summary>
        /// Returns Communication Permissions, like AllCommunications(), except we feed in all communications list THEN filter the data we need, 
        /// instead of hitting the database twice each time like in AllCommunications(). This performs better especially when this method is 
        /// called from a loop is just used multiple times in same context. 
        /// </summary>
        public static IEnumerable<Permission> AllCommunicationsUsingList(List<Template> allTemplates, string customerId = null, string employerId = null)
        {
            
            var templates = allTemplates.Where(t => t.EmployerId == null && t.CustomerId == null).ToList();
            foreach (var template in templates)
                yield return new Permission(string.Concat(PREFIX_Communication, template.Code), template.Name, CATEGORY_Communications);

            if (!string.IsNullOrWhiteSpace(customerId) || !string.IsNullOrWhiteSpace(employerId))
            {
                var tempIds = templates.Select(t => t.Id).ToList();
                IEnumerable<Template> customTemplates = allTemplates
                    .Where(t => t.CustomerId == customerId
                            && t.EmployerId == employerId
                            && !tempIds.Contains(t.Id));
                foreach (var template in customTemplates.ToList())
                    yield return new Permission(string.Concat(PREFIX_Communication, template.Code), template.Name, CATEGORY_CustomCommunications);
            }
        }//end: AllCommunications

        public static IEnumerable<Permission> AllCommunications(string customerId = null, string employerId = null)
        {
            var templates = Template.AsQueryable().Where(t => t.EmployerId == null && t.CustomerId == null).ToList();
            foreach (var template in templates)
                yield return new Permission(string.Concat(PREFIX_Communication, template.Code), template.Name, CATEGORY_Communications);

            if (!string.IsNullOrWhiteSpace(customerId) || !string.IsNullOrWhiteSpace(employerId))
            {
                var tempIds = templates.Select(t => t.Id).ToList();
                IEnumerable<Template> customTemplates = Template.AsQueryable()
                    .Where(t => t.CustomerId == customerId
                            && t.EmployerId == employerId
                            && !tempIds.Contains(t.Id));

                foreach (var template in customTemplates.ToList())
                    yield return new Permission(string.Concat(PREFIX_Communication, template.Code), template.Name, CATEGORY_CustomCommunications);
            }
        }//end: AllCommunications

        public static IEnumerable<Permission> AllCustomCommunications(string customerId = null, string employerId = null)
        {
            List<Permission> commsCustom = new List<Permission>();
            User currentUser = User.Current;

            var templates = Template.AsQueryable().Where(t => t.EmployerId == null && t.CustomerId == null).ToList();
            var tempIds = templates.Select(t => t.Id).ToList();

            IEnumerable<Template> customTemplates = customTemplates = Template.AsQueryable()
                          .Where(t => t.CustomerId == customerId
                                  && t.EmployerId == employerId
                                  && !tempIds.Contains(t.Id));

            foreach (var template in customTemplates.ToList())
                commsCustom.AddIfNotExists(new Permission(string.Concat(Permission.PREFIX_Communication, template.Code), template.Name, Permission.CATEGORY_CustomCommunications));

            if (currentUser.Employers != null)
            {
                customTemplates = null;
                foreach (var employer in currentUser.Employers)
                {
                    if (!string.IsNullOrWhiteSpace(customerId) || !string.IsNullOrWhiteSpace(employer.EmployerId))
                    {
                        //var tempIds = templates.Select(t => t.Id).ToList();
                        customTemplates = Template.AsQueryable()
                            .Where(t => t.CustomerId == customerId
                                    && t.EmployerId == employer.EmployerId
                                    && !tempIds.Contains(t.Id));

                        foreach (var template in customTemplates.ToList())
                            commsCustom.AddIfNotExists(new Permission(string.Concat(Permission.PREFIX_Communication, template.Code), template.Name, Permission.CATEGORY_CustomCommunications));

                    }
                }
            }
            return commsCustom;
        }

        public static void AddCommunicationToAllRoles(string customerId, string templateCode)
        {
            var roles = Role.AsQueryable().Where(r => r.CustomerId == customerId).ToList();
            templateCode = Permission.PREFIX_Communication + templateCode;
            foreach (Role role in roles)
            {
                if (!role.Permissions.Exists(p => p.Equals(templateCode)))
                {
                    role.Permissions.Add(templateCode);
                    role.Save();
                }
            }
        }

        public static IEnumerable<Permission> Communications(params string[] templates)
        {
            foreach (var template in templates)
                yield return new Permission(string.Concat(PREFIX_Communication, template), template, CATEGORY_CustomCommunications);
        }//end: Communications

        #endregion Static Methods

        #region IEqualityComparer, IEquatable, IComparable Implementation

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj is Permission) return this.Id == ((Permission)obj).Id;
            if (obj is string) return this.Id == (string)obj;
            return base.Equals(obj);
        }
        public bool Equals(Permission other) { return this.Id == other.Id; }
        public bool Equals(string other) { return this.Id == other; }
        public bool Equals(Permission x, Permission y) { return x.Id == y.Id; }

        public int GetHashCode(Permission obj) { return obj.Id.GetHashCode(); }
        public override int GetHashCode() { return this.Id.GetHashCode(); }

        public int CompareTo(object obj) { return this.Id.CompareTo(obj); }
        public int CompareTo(Permission other) { return this.Id.CompareTo(other.Id); }
        public int CompareTo(string other) { return this.Id.CompareTo(other); }

        public int Compare(Permission x, Permission y) { return x.Id.CompareTo(y.Id); }
        public int Compare(string x, string y) { return x.CompareTo(y); }

        public override string ToString() { return this.Id; }

        #endregion IEqualityComparer, IEquatable, IComparable Implementation

        #region Operators

        public static bool operator ==(Permission p1, string p2) { return p1.Equals(p2); }
        public static bool operator !=(Permission p1, string p2) { return !p1.Equals(p2); }
        public static bool operator ==(string p1, Permission p2) { return p2.Equals(p1); }
        public static bool operator !=(string p1, Permission p2) { return !p2.Equals(p1); }

        #endregion Operators
    }

    public static class PermissionExtensions
    {
        public static bool Has(this IEnumerable<Permission> permissions, ToDoItemType type)
        {
            return permissions.Any(p => p == string.Concat(Permission.PREFIX_ToDo, Enum.GetName(typeof(ToDoItemType), type)));
        }

        public static bool Has(this IEnumerable<string> permissions, ToDoItemType type)
        {
            return permissions.Any(p => p == string.Concat(Permission.PREFIX_ToDo, Enum.GetName(typeof(ToDoItemType), type)));
        }

        public static bool Has(this IEnumerable<Permission> permissions, string communication)
        {
            return permissions.Any(p => p == string.Concat(Permission.PREFIX_Communication, communication));
        }

        public static bool Has(this IEnumerable<string> permissions, string communication)
        {
            return permissions.Any(p => p == string.Concat(Permission.PREFIX_Communication, communication));
        }


        public static List<ToDoItemType> GetsToDos(this IEnumerable<Permission> permissions)
        {
            return permissions.Select(p => p.Id).GetsToDos();
        }

        public static List<ToDoItemType> GetsToDos(this IEnumerable<string> permissions)
        {
            return permissions
                .Where(p => p.StartsWith(Permission.PREFIX_ToDo))
                .Select(p => p.Substring(Permission.PREFIX_ToDo.Length))
                .Select(p =>
                {
                    ToDoItemType t;
                    if (Enum.TryParse<ToDoItemType>(p, true, out t))
                        return t;
                    return new ToDoItemType?();
                })
                .Where(t => t.HasValue)
                .Select(t => t.Value)
                .ToList();
        }

        public static List<string> GetsCommunications(this IEnumerable<Permission> permissions)
        {
            return permissions.Select(p => p.Id).GetsCommunications();
        }

        public static List<string> GetsCommunications(this IEnumerable<string> permissions)
        {
            return permissions
                .Where(p => p.StartsWith(Permission.PREFIX_Communication))
                .Select(p => p.Substring(Permission.PREFIX_Communication.Length))
                .ToList();
        }


    }
}
