﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Security
{
    [Serializable]
    public class SelfRegistrationRequest : BaseCustomerEntity<SelfRegistrationRequest>
    {
        public SelfRegistrationRequest()
        {

        }

        public SelfRegistrationRequest(string email, string ipAddress)
            :this()
        {
            Email = email;
            IpAddress = ipAddress;
        }

        [BsonRequired]
        public string Email { get; set; }

        [BsonIgnoreIfNull]
        public string FirstName { get; set; }

        [BsonRequired]
        public RegistrationStatus Status {get; set;}

        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string EmployerId { get; set; }

        [BsonRequired]
        public string VerificationKey { get; set; }

        [BsonIgnoreIfNull]
        public string AlternateEmail { get; set; }

        [BsonIgnoreIfNull]
        public string IpAddress { get; set; }

        [BsonIgnoreIfNull]
        public string UserId { get; set; }

        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string EmployeeId { get; set; }

    }
}
