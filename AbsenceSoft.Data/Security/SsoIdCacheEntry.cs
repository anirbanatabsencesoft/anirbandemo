﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver.Builders;
using MongoRepository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Security
{
    [Serializable, JsonObject(MemberSerialization.OptOut)]
    public class SsoIdCacheEntry : Entity
    {
        /// <summary>
        /// Gets or sets the cache id.
        /// </summary>
        /// <value>
        /// The cache id.
        /// </value>
        [BsonRequired]
        public string CacheId { get; set; }

        /// <summary>
        /// Gets or sets the expires.
        /// </summary>
        /// <value>
        /// The expires.
        /// </value>
        [BsonRequired]
        [BsonDateTimeOptions(DateOnly = false, Kind = DateTimeKind.Utc, Representation = BsonType.DateTime)]
        public DateTime Expires { get; set; }

        /// <summary>
        /// Saves this instance and returns <c>true</c> if the entry was new (didn't already exist).
        /// </summary>
        /// <returns>
        /// <c>true</c> if the ID doesn't already exist in the cache; otherwise <c>false</c>.
        /// </returns>
        public bool Save()
        {
            var entry = Repository.Collection.FindOne(Query.EQ("CacheId", new BsonString(CacheId)));
            if (entry == null)
                Repository.Update(this);
            return entry == null;
        }


        [BsonIgnore, JsonIgnore]
        private static object _repoSync = new object();
        [BsonIgnore, JsonIgnore]
        private static MongoRepository<SsoIdCacheEntry> _myRepository = null;
        /// <summary>
        /// Gets the repository used for manipulating this instance.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public static MongoRepository<SsoIdCacheEntry> Repository
        {
            get
            {
                if (_myRepository != null)
                    return _myRepository;

                lock (_repoSync)
                {
                    if (_myRepository != null)
                        return _myRepository;

                    var myRepo = typeof(SsoIdCacheEntry)
                        .GetCustomAttributes(typeof(RepositoryAttribute), true)
                        .OfType<RepositoryAttribute>()
                        .FirstOrDefault();

                    string cName = (myRepo == null || string.IsNullOrWhiteSpace(myRepo.ConnectionStringName)) ? "MongoServerSettings" : myRepo.ConnectionStringName;
                    var connString = ConfigurationManager.ConnectionStrings[cName];
                    if (connString == null)
                        throw new ApplicationException(string.Format("MongoDB connection string '{0}' is missing from configuration", cName));
                    _myRepository = new MongoRepository<SsoIdCacheEntry>(connString.ConnectionString, "SsoIdCacheEntry");
                }

                return _myRepository;
            }
        }//end: Repository
    }
}
