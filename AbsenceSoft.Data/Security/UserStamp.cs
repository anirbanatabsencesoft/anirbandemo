﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Security
{
    [Serializable]
    public class UserStamp<TValue, TReason> : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserStamp{TValue, TReason}"/> class.
        /// </summary>
        public UserStamp() : base()
        {
            Date = DateTime.UtcNow;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserStamp{TValue, TReason}" /> class.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="reason">The reason.</param>
        /// <param name="user">The user.</param>
        /// <param name="comments">The comments.</param>
        public UserStamp(TValue value, TReason reason, User user, string comments = null) : this()
        {
            Value = value;
            Reason = reason;
            User = user;
            if (user != null)
                Username = user.DisplayName;
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public virtual TValue Value { get; set; }

        /// <summary>
        /// Gets or sets the reason.
        /// </summary>
        /// <value>
        /// The reason.
        /// </value>
        public virtual TReason Reason { get; set; }

        /// <summary>
        /// Gets or sets the comments related to the user stamp (like a denial reason, etc.).
        /// </summary>
        /// <value>
        /// The comments.
        /// </value>
        public string Comments { get; set; }

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [BsonRepresentation(BsonType.ObjectId)]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        /// <value>
        /// The date.
        /// </value>
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        /// <value>
        /// The user.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public User User
        {
            get { return GetReferenceValue<User>(UserId); }
            set { UserId = SetReferenceValue(value); if (value != null) Username = value.DisplayName; }
        }
    }
}
