﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Audit;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AbsenceSoft.Data.Security
{
	/// <summary>
	/// Role Class
	/// </summary>
	[Serializable]
	public class Role : BaseEntity<Role>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="Role"/> class.
		/// </summary>
		[BsonConstructor]
		public Role()
		{
			Permissions = new List<string>();
			Type = RoleType.Portal;
		}

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		[BsonRequired]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the permissions.
		/// </summary>
		/// <value>
		/// The permissions.
		/// </value>
		public List<string> Permissions { get; set; }

		/// <summary>
		/// Gets or sets the customer identifier.
		/// </summary>
		/// <value>
		/// The customer identifier.
		/// </value>
		[BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
		public string CustomerId { get; set; }

		/// <summary>
		/// Gets or sets the default role.
		/// </summary>
		/// <value>
		/// The DefaultRole.
		/// </value>
		public bool DefaultRole { get; set; }

		/// <summary>
		/// Gets or sets the always assign.
		/// </summary>
		/// <value>
		/// The AlwaysAssign.
		/// </value>
		public bool AlwaysAssign { get; set; }

		/// <summary>
		/// Gets the permissions.
		/// </summary>
		/// <returns></returns>
		public List<Permission> GetPermissions()
		{
			return Permission.All(Permissions.ToArray()).ToList();
		}

		/// <summary>
		/// Gets and Sets Type of this role
		/// </summary>
		public RoleType Type { get; set; }

		/// <summary>
		/// Gets or sets the list of contact type codes that this role (for self service)
		/// where if exists would automatically get assigned at login.
		/// </summary>
		/// <value>
		/// The contact types which trigger automatic assignment.
		/// </value>
		public List<string> ContactTypes { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to revoke access if no contacts of types listed
		/// under ContactTypes exist for this user.
		/// </summary>
		/// <value>
		/// <c>true</c> if revoke access if no contacts of types; otherwise, <c>false</c>.
		/// </value>
		public bool RevokeAccessIfNoContactsOfTypes { get; set; }

		/// <summary>
		/// Gets the built-in system administrator role which has a hard-coded id of all zeros.
		/// </summary>
		[AuditPropertyNo]           // it's turtles all the way down
        public static Role SystemAdministrator
        {
            get
            {
                //we used to do both of the below lines in a chained sequence but for some reason, doing separately gains us half a second. Which adds up when it's used multiple times
                var allPermissions = Permission.All().ToList();
                var allPermissionsIds = allPermissions.Select(p => p.Id).ToList();


                return new Role()
                {
                    Id = "000000000000000000000000",
                    Name = "System Administrator",
                    Permissions = allPermissionsIds 
                };
            }
        }
    }
}
