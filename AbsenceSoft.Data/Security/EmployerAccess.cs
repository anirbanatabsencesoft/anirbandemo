﻿using AbsenceSoft.Common;
using AbsenceSoft.Common.Security;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AbsenceSoft.Data.Security
{
    [Serializable]
    public class EmployerAccess : BaseNonEntity
    {
        public EmployerAccess()
        {
            Roles = new List<string>();
            ContactOf = new List<string>();
            OrgCodes = new List<string>();
        }

        /// <summary>
        /// Gets or sets the user's employer access, which employer they have visibility to, this will rarely be more than one; so for now
        /// it's exactly one. :-)
        /// </summary>
        [BsonRequired, BsonRepresentation(BsonType.ObjectId)]
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the employee ID that ties the user directly to an employee account (for self service, HR functions, etc.)
        /// </summary>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string EmployeeId { get; set; }

        /// <summary>
        /// Whether or not to automatically assign cases to this user in the round-robin/rotary assignment.
        /// </summary>
        [BsonDefaultValue(false)]
        public bool AutoAssignCases { get; set; }

        /// <summary>
        /// Gets or sets the collection of roles the user has access to.
        /// </summary>
        [BsonRepresentation(BsonType.ObjectId)]
        public List<string> Roles { get; set; }

        /// <summary>
        /// Gets or sets the collection of organization code the user has access to.
        /// </summary>
        public List<string> OrgCodes { get; set; }

        /// <summary>
        /// Gets or sets the effective date of when this user actually gets the employer access granted.
        /// This is primarily for temporary assignment where the user will simply cover another
        /// user for some period of time and their access should not start until this date.
        /// </summary>
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime? EffectiveDate { get; set; }

        /// <summary>
        /// Gets or sets the expiration date of employer access for this user when they will no longer
        /// be able to access this Employer's data. This is primarily for temporary assignment where the
        /// user will no longer need or no longer should have acccess to the employer because they have been
        /// covering another user or will be re-assigned to another department/division, etc.
        /// </summary>
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime? ExpireDate { get; set; }

        /// <summary>
        /// Gets or sets the list of employee IDs that this user for this employer is a contact of.
        /// </summary>
        /// <value>
        /// The contact of list of employee IDs.
        /// </value>
        [BsonRepresentation(BsonType.ObjectId)]
        public List<string> ContactOf { get; set; }

        /// <summary>
        /// Gets a value indicating whether or not this employer access is currently active, meaning that
        /// if an effective and/or expire date is set, it is currently active within that range. If no
        /// extremeties for access are set, this always returns <c>true</c>.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public bool IsActive
        {
            get
            {
                return DateTime.UtcNow.ToMidnight()
                    .DateInRange(EffectiveDate ?? DateTime.MinValue, ExpireDate);
            }
        }//end: IsActive

        /// <summary>
        /// Gets or sets the user's employer.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Employer Employer
        {
            get { return GetReferenceValue<Employer>(this.EmployerId); }
            set { EmployerId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the user's employee record if applicable otherwise <c>null</c>.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Employee Employee
        {
            get { return GetReferenceValue<Employee>(this.EmployeeId); }
            set { EmployeeId = SetReferenceValue(value); }
        }
    }
}
