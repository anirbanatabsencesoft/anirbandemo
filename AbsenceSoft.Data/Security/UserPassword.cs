﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Security
{
    [Serializable]
    public class UserPassword
    {

        public UserPassword()
        {
            Password = new CryptoString();
            CreatedDate = DateTime.UtcNow;
        }

        [BsonRequired]
        public CryptoString Password { get; set; }

        [BsonRequired]
        public DateTime? CreatedDate { get; set; }
    }
}
