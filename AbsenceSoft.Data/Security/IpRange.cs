﻿using AbsenceSoft.Data.Customers;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Security
{
    [Serializable]
    public class IpRange : BaseCustomerEntity<IpRange>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IpRange"/> class.
        /// </summary>
        public IpRange() : base()
        {
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        [BsonIgnoreIfNull]
        public short? Ip1 { get; set; }

        [BsonIgnoreIfNull]
        public short? Ip2 { get; set; }

        [BsonIgnoreIfNull]
        public short? Ip3 { get; set; }

        [BsonIgnoreIfNull]
        public short? Ip41 { get; set; }

        [BsonIgnoreIfNull]
        public short? Ip42 { get; set; }

        [BsonIgnoreIfNull]
        public bool? IsEnabled { get; set; }

  
        ///// <summary>
        ///// Gets or sets the employerid this necessity belongs to
        ///// </summary>
        //[BsonRepresentation(BsonType.ObjectId), BsonIgnoreIfNull]
        //public string CustomerId { get; set; }

        ///// <summary>
        ///// A copy of the employer for this necessity
        ///// </summary>
        //[BsonIgnore, JsonIgnore]
        //public Customer Customer
        //{
        //    get { return GetReferenceValue<Customer>(this.CustomerId); }
        //    set { this.CustomerId = SetReferenceValue<Customer>(value); }
        //}
    }
}
