﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Collections.Generic;
using AbsenceSoft.Data.Cases;

namespace AbsenceSoft.Data
{
    [Serializable, JsonObject(MemberSerialization.OptOut)]
    public class BaseEmployerEntity<TEntity> : BaseCustomerEntity<TEntity> where TEntity : BaseEmployerEntity<TEntity>, new()
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the employer id for the entity.
        /// </summary>
        [BsonRequired, BsonRepresentation(BsonType.ObjectId)]
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the entity's employer.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Employer Employer
        {
            get { return GetReferenceValue<Employer>(this.EmployerId); }
            set { EmployerId = SetReferenceValue(value); }
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching.
        /// </summary>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static new IQueryable<TEntity> AsQueryable()
        {
            if (User.Current != null)
                return AsQueryable(User.Current);
            return BaseCustomerEntity<TEntity>.AsQueryable();
        }

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching while automatically
        /// applying the appropriate data access filters onto it automagically.
        /// </summary>
        /// <param name="entity">The entity of type <c>TEntity</c> to which to build the filtered queryable off of</param>
        /// <param name="user">The user of which to build data visibility and data access from</param>
        /// <param name="permission">The permission that the user should have access to when filtering the data parameters</param>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static new IQueryable<TEntity> AsQueryable(User user, Permission permission) { return AsQueryable(user, permission.Id); }
        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching while automatically
        /// applying the appropriate data access filters onto it automagically.
        /// </summary>
        /// <param name="entity">The entity of type <c>TEntity</c> to which to build the filtered queryable off of</param>
        /// <param name="user">The user of which to build data visibility and data access from</param>
        /// <param name="permission">The permission that the user should have access to when filtering the data parameters</param>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static new IQueryable<TEntity> AsQueryable(User user, string permission = null)
        {
            var query = BaseCustomerEntity<TEntity>.AsQueryable();

            // If no user, then duh, no dice sucker, empty list it
            if (user == null) return new List<TEntity>(0).AsQueryable();
            // User must belong to a customer, otherwise empty list it
            if (string.IsNullOrWhiteSpace(user.CustomerId)) return query.Where(e => e.CustomerId == null && e.EmployerId == null);
            // User must have access to at least 1 employer, otherwise empty list it
            var employerAccessIds = user.Employers == null ? new string[0] : user.Employers.Select(e => e.EmployerId).ToArray();
            var employerIdList = Employer.AsQueryable().Where(e => e.CustomerId == user.CustomerId && !e.IsDeleted && employerAccessIds.Contains(e.Id)).Select(e => e.Id).ToList();
            if (user.Employers == null || !user.Employers.Any(e => e.IsActive && employerIdList.Contains(e.EmployerId))) return query.Where(e => e.CustomerId == user.CustomerId && e.EmployerId == null);

            // Filter out any that are not active
            var employerIds = user.Employers.Where(e => e.IsActive && employerIdList.Contains(e.EmployerId))
            // Ensure the permissions passed in are empty (meaning just all active) or that 
            //  if we have explicit intent, that the user has the appropriate permissions for that function for that employer
                .Where(e => string.IsNullOrWhiteSpace(permission) || User.Permissions.GetPermissions(user, e.EmployerId).Contains(permission))
            // Project our Employer entity
                .Select(e => e.EmployerId)
                .Distinct()
            // Return the final list
                .ToArray();

            query = query.Where(e => employerIds.Contains(e.EmployerId))
                .Where(e => e.CustomerId == user.CustomerId);

            return query;
        }//end: AsQueryable

        /// <summary>
        /// Gets the entity by its Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>The entity found by the Id or <c>null</c> if no entity was found by that id.</returns>
        public static new TEntity GetById(string id)
        {
            using (new InstrumentationContext("{0}.GetById", ClassTypeName))
            {
                if (string.IsNullOrWhiteSpace(id))
                    return default(TEntity);

                TEntity val = Repository.GetById(id.ToLowerInvariant());
                if (val == null || val.IsDeleted)
                    return null;
                if (User.Current == null)
                    return val;
                if (string.IsNullOrWhiteSpace(val.CustomerId))
                    return val;
                if (val.CustomerId != User.Current.CustomerId)
                    return default(TEntity);
                if (string.IsNullOrWhiteSpace(val.EmployerId))
                    return val;
                if (User.Current.Roles.Contains(Role.SystemAdministrator.Id))
                    return val;
                if (!User.Current.HasEmployerAccess(val.EmployerId))
                    return default(TEntity);

                return val;
            }
        }

        #endregion
    }//end: BaseEmployerEntity
}//end: namespace
