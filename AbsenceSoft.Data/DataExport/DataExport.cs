﻿using AbsenceSoft.Common;
using AbsenceSoft.Common.Security;
using AbsenceSoft.Data.Audit;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AbsenceSoft.Data.DataExport
{
    [Serializable, AuditClassNo]
    public class DataExport : BaseEmployerEntity<DataExport>
    {
        public DataExport()
        {
            ZipFilePassword = new CryptoString();
        }

        public ExportStatus? Status { get; set; }

        public string Message { get; set; }

        public Double? CompletedIn { get; set; }

        public string FileReference { get; set; }

        public CryptoString ZipFilePassword { get; set; }

        protected override bool OnSaving()
        {
            // If we have a plain text password in there, ensure we hash it.
            if (ZipFilePassword != null && !string.IsNullOrWhiteSpace(ZipFilePassword.PlainText))
            {
                ZipFilePassword.Hashed = ZipFilePassword.PlainText.Sha256Hash();
                ZipFilePassword.PlainText = ZipFilePassword.PlainText;
            }

            // Ensure we never store an encrypted 2-way copy of the password, that's just asking for trouble.
            if (ZipFilePassword != null)
                ZipFilePassword.Encrypted = Crypto.Encrypt(ZipFilePassword.PlainText);

            // Call our base OnSaving event to go on with life, we're done here people, nothing to see here, move along.
            return base.OnSaving();
        }
    }
}
