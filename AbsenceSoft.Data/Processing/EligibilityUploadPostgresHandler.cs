﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using Insight.Database;
using PostgreSQLCopyHelper;

namespace AbsenceSoft.Data.Processing
{
    /// <summary>
    /// Handles and inserts EL Parsing data to Postgres
    /// </summary>
    public class EligibilityUploadPostgresHandler : IDisposable
    {
        #region Private Variables
        string _connectionStringName = "";
        string _connectionString = "";
        string _fileName = "";
        string _uploadId = "";
        string _customerId = "";
        string _employerId = "";
        long _processingId;
        readonly string _schemaName = "el_temp";
        List<EligibilityUploadResult> _rows = new List<EligibilityUploadResult>();
        #endregion


        #region Properties
        /// <summary>
        /// returns the temp table name
        /// </summary>
        public string TempTableName { get; private set; }

        /// <summary>
        /// Returns the total no of rows inserted
        /// </summary>
        public long TotalRowsInserted { get; private set; } = 0;
        #endregion

        /// <summary>
        /// Constructor to inject default properties
        /// </summary>
        /// <param name="fileName">The filename which we are parsing</param>
        /// <param name="uploadId">The Upload Id</param>
        /// <param name="customerId">The customer object id</param>
        /// <param name="employerId">The employer Id for which the EL is processed</param>
        /// <param name="processId">The process id for next phase processing</param>
        /// <param name="connectionStringName">Optional. Will use default datawarehouse connection string</param>
        public EligibilityUploadPostgresHandler(string fileName, string uploadId, string customerId, string employerId = "", long? processId = null, string connectionStringName = "DataWarehouse")
        {
            _connectionStringName = connectionStringName;
            _fileName = fileName;
            _uploadId = uploadId;
            _customerId = customerId;
            _employerId = employerId;

            if (processId.HasValue)
                _processingId = processId.Value;

            //set connection string
            _connectionString = System.Configuration.ConfigurationManager.ConnectionStrings[_connectionStringName].ConnectionString;

            //set temp table name
            TempTableName = GetTempTableName();

            //insert the initial master data
            CreateRecordBeforeProcessing();
        }


        #region EL Processing Entries

        /// <summary>
        /// Create a record for EL processing
        /// </summary>
        public void CreateRecordBeforeProcessing()
        {
            using (var conn = new NpgsqlConnection(_connectionString))
            {
                var sql = @"INSERT INTO public.el_file_processing(file_name, el_upload_id, customer_id, employer_id, parsed_row_temp_table_name) VALUES(@fileName, @uploadId, @customerId, @employerId, @tempTableName) RETURNING id;";
                _processingId = conn.ExecuteScalarSql<long>(sql, new { fileName = _fileName, uploadId = _uploadId, customerId = _customerId, employerId = _employerId, tempTableName = $"{_schemaName}.{TempTableName}" });
            }

            CreateSchema();
        }

        /// <summary>
        /// Finalyze parse snapshot
        /// </summary>
        /// <param name="invalidRows"></param>
        /// <param name="validRows"></param>
        public void FinalizeParsingSnapshot(long invalidRows)
        {
            //whatever is pending insert it
            BulkInsert();
            TotalRowsInserted += _rows.Count;
            _rows.Clear();

            using (var conn = new NpgsqlConnection(_connectionString))
            {
                var sql = @"UPDATE public.el_file_processing SET parsed_completed_on = public.utc(), parse_invalid_rows = @error_rows, parse_valid_rows = @passed, is_parse_complete = True WHERE id = @id;";
                conn.ExecuteSql(sql, new { id = _processingId, error_rows = invalidRows, passed = TotalRowsInserted });
            }
        }

        /// <summary>
        /// Create Temporary schema
        /// </summary>
        private void CreateSchema()
        {
            using (var conn = new NpgsqlConnection(_connectionString))
            {
                var updateSql = $" CREATE SCHEMA IF NOT EXISTS {_schemaName};";
                conn.ExecuteSql(updateSql);

                //create temp table
                conn.ExecuteSql(GetTempTableSQL().ToString());

                //create index
                conn.ExecuteSql($"CREATE INDEX {TempTableName}_status_type_err_ret ON {_schemaName}.{TempTableName}(processing_status, record_type_no, is_error, is_retry);");
            }
        }

        /// <summary>
        /// Returns the temp table SQL
        /// </summary>
        /// <returns></returns>
        public StringBuilder GetTempTableSQL()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"CREATE TABLE IF NOT EXISTS {_schemaName}.{TempTableName}");
            sb.AppendLine("(");
            sb.AppendLine("row_id bigint NOT NULL PRIMARY KEY");
            sb.AppendLine(", record_type_str varchar(256)");
            sb.AppendLine(", record_type_no double precision");
            sb.AppendLine(", employee_number varchar(256)");
            sb.AppendLine(", source_csv_string text");
            sb.AppendLine(", values_arr text[]");
            sb.AppendLine(", is_error boolean NOT NULL DEFAULT False");
            sb.AppendLine(", is_retry boolean NOT NULL DEFAULT False");
            sb.AppendLine(", error_message text");
            sb.AppendLine(", is_split boolean NOT NULL DEFAULT False");
            sb.AppendLine(", ref_code varchar(256)");
            sb.AppendLine(", processing_status integer NOT NULL DEFAULT 0"); //0 for Not processed, 1 for processing, 2 for complete
            sb.AppendLine(");");
            return sb;
        }

        /// <summary>
        /// Drop the temp table
        /// </summary>
        /// <param name="tempTableName"></param>
        public void DropTempTable(string tempTableName)
        {
            using (var conn = new NpgsqlConnection(_connectionString))
            {
                var updateSql = $" DROP TABLE {tempTableName};";
                conn.ExecuteSql(updateSql);
            }
        }

        #endregion


        #region Data Entries
        /// <summary>
        /// Returns the map for bulk insert
        /// </summary>
        /// <returns></returns>
        private PostgreSQLCopyHelper<EligibilityUploadResult> GetMap()
        {
            return new PostgreSQLCopyHelper<EligibilityUploadResult>(_schemaName, TempTableName)
                .MapBigInt("row_id", x => x.RowNumber)
                .MapText("record_type_str", x => x.RecordType)
                .MapDouble("record_type_no", x => x.RecordTypeNo)
                .MapText("employee_number", x => x.EmployeeNumber)
                .MapText("source_csv_string", x => x.RowSource)
                .MapArray("values_arr", x => x.RowParts)
                .MapBoolean("is_error", x => x.IsError)
                .MapBoolean("is_retry", x => x.IsRetry)
                .MapText("error_message", x => x.Error)
                .MapBoolean("is_split", x => x.IsSplit)
                .MapText("ref_code", x => x.RefCode)
            ;
        }

        /// <summary>
        /// Adds and bulk update on threshold
        /// </summary>
        /// <param name="result"></param>
        /// <param name="bulkSize"></param>
        public void Add(EligibilityUploadResult result, int bulkSize = 1000)
        {
            _rows.Add(result);
            if(_rows.Count >= bulkSize)
            {
                lock (_rows)
                {
                    BulkInsert();
                    TotalRowsInserted += _rows.Count;
                    _rows.Clear();
                }
            }
        }

        /// <summary>
        /// Insert data
        /// </summary>
        private void BulkInsert()
        {
            using (var conn = new NpgsqlConnection(_connectionString))
            {
                var map = GetMap();
                conn.Open();
                map.SaveAll(conn, _rows);
                conn.Close();
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Returns the temp table name.
        /// </summary>
        /// <returns></returns>
        private string GetTempTableName()
        {
            return String.Concat(Guid.NewGuid().ToString("N").Select(c => (char)(c + 17)));
        }
        #endregion

        /// <summary>
        /// Dispose off the object
        /// </summary>
        public void Dispose()
        {
            _connectionStringName = null;
            _connectionString = null;
            _fileName = null;
            _customerId = null;
            _employerId = null;
            _rows = null;
        }
    }
}
