﻿using AbsenceSoft.Data.Audit;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Processing
{
    [AuditClassNo]
    [Serializable]
    public class EligibilityUploadResult : BaseEntity<EligibilityUploadResult>
    {
        /// <summary>
        /// Gets or sets the eligibility upload identifier.
        /// </summary>
        /// <value>
        /// The eligibility upload identifier.
        /// </value>
        [BsonRepresentation(BsonType.ObjectId)]
        public string EligibilityUploadId { get; set; }

        long row_id;
        /// <summary>
        /// Gets or sets the row number.
        /// </summary>
        /// <value>
        /// The row number.
        /// </value>
        public long RowNumber { get { return row_id; } set { row_id = value; } }

        string error_message;
        /// <summary>
        /// Gets or sets the error.
        /// </summary>
        /// <value>
        /// The error.
        /// </value>
        [BsonIgnoreIfNull]
        public string Error { get { return error_message; } set { error_message = value; } }

        bool is_error;
        /// <summary>
        /// Gets or sets a value indicating whether this instance is error.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is error; otherwise, <c>false</c>.
        /// </value>
        public bool IsError { get { return !string.IsNullOrWhiteSpace(Error); } set { is_error = value; } }

        bool is_retry;
        /// <summary>
        /// Gets or sets a value indicating whether this instance is retry.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is retry; otherwise, <c>false</c>.
        /// </value>
        public bool IsRetry { get { return is_retry; } set { is_retry = value; } }

        string source_csv_string;
        /// <summary>
        /// Gets or sets the row source.
        /// </summary>
        /// <value>
        /// The row source.
        /// </value>
        [BsonIgnoreIfNull]
        public string RowSource { get { return source_csv_string; } set { source_csv_string = value; } }

        Array values_arr;
        /// <summary>
        /// Gets or sets the row parts.
        /// </summary>
        /// <value>
        /// The row parts.
        /// </value>
        [BsonIgnore]
        public string[] RowParts { get { return values_arr == null ? new string[0] : values_arr.OfType<object>().Select(o => o.ToString()).ToArray(); } set { values_arr = value; } }

        private string _tempRowSource = null;

        /// <summary>
        /// Called before saving
        /// </summary>
        /// <returns>
        /// A value indicating whether or not the save should proceed (not canceled).
        /// </returns>
        protected override bool OnSaving()
        {
            if (!IsError)
            {
                _tempRowSource = RowSource;
                RowSource = null;
            }
            return base.OnSaving();
        }

        /// <summary>
        /// Employee Number as Reference
        /// </summary>
        string employee_number;
        public string EmployeeNumber { get { return employee_number; } set { employee_number = value; } }


        /// <summary>
        /// Record type string/char
        /// </summary>
        string record_type_str;
        [BsonIgnore]
        public string RecordType { get { return record_type_str; } set { record_type_str = value.Trim(); } }


        double? record_type_no;
        [BsonIgnore]
        public double? RecordTypeNo {
            get
            {
                if (!record_type_no.HasValue)
                    record_type_no = GetRecordType();
                return record_type_no;
            }
            set
            {
                record_type_no = value;
            }
        }

        /// <summary>
        /// Stores the split info into temp table
        /// </summary>
        bool is_split;
        [BsonIgnore]
        public bool IsSplit { get { return is_split; } set { is_split = value; } }

        /// <summary>
        /// Stores the ref code info to temp table
        /// </summary>
        string ref_code;
        [BsonIgnore]
        public string RefCode { get { return ref_code; } set { ref_code = value; } }

        /// <summary>
        /// Return the number for sorting
        /// </summary>
        /// <returns></returns>
        private double GetRecordType()
        {
            switch (record_type_str)
            {
                case "E": return 1d;
                case "B": return 2d;
                case "S": return 3d;
                case "W": return 3.5d;
                case "M": return 4d;
                case "U": return 5d;
                case "H": return 6d;
                case "C": return 7d;
                case "F": return 7.5d;
                case "I": return 8d;
                case "G": return 9d;
                default: return 999d;
            }
        }

        /// <summary>
        /// Called when saved
        /// </summary>
        protected override void OnSaved()
        {
            if (!IsError && RowSource == null)
            {
                RowSource = _tempRowSource;
                _tempRowSource = null;
            }
            base.OnSaved();
        }
    }
}
