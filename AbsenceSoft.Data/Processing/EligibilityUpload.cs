﻿using AbsenceSoft.Data.Audit;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Processing
{
    [AuditClassNo]
    [Serializable]
    public class EligibilityUpload : BaseCustomerEntity<EligibilityUpload>
    {

     
        [BsonRepresentation(BsonType.ObjectId), BsonIgnoreIfNull]
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the entity's employer.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Employer Employer
        {
            get { return GetReferenceValue<Employer>(this.EmployerId); }
            set { EmployerId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the name of the file.
        /// </summary>
        /// <value>
        /// The name of the file.
        /// </value>
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the file key.
        /// </summary>
        /// <value>
        /// The file key.
        /// </value>
        public string FileKey { get; set; }


        /// <summary>
        /// Gets or sets the type of the MIME.
        /// </summary>
        /// <value>
        /// The type of the MIME.
        /// </value>
        public string MimeType { get; set; }

        /// <summary>
        /// Gets or sets the size.
        /// </summary>
        /// <value>
        /// The size.
        /// </value>
        public long Size { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public ProcessingStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the records.
        /// </summary>
        /// <value>
        /// The records.
        /// </value>
        public long Records { get; set; }

        /// <summary>
        /// Get/Set parsed records. Processing will be done after this.
        /// </summary>
        public long ParsedRecords { get; set; }

        /// <summary>
        /// List of records that fails parsing
        /// </summary>
        public long ParsingErrors { get; set; }

        /// <summary>
        /// Gets or sets the processed.
        /// </summary>
        /// <value>
        /// The processed.
        /// </value>
        public long Processed { get; set; }

        /// <summary>
        /// Gets or sets the errors.
        /// </summary>
        /// <value>
        /// The errors.
        /// </value>
        public long Errors { get; set; }

        [BsonRepresentation(BsonType.ObjectId),BsonIgnoreIfNull]
        public string LinkedEligibilityUploadId { get; set; }
        /// <summary>
        /// Gets the progress.
        /// </summary>
        /// <value>
        /// The progress.
        /// </value>
        [BsonIgnore]
        public string Progress { get {
            return (this.Records <= 0 ? 0.0d : ((double)this.Processed / this.Records)).ToString("P"); 
        } }

        /// <summary>
        /// Gets the elapsed.
        /// </summary>
        /// <value>
        /// The elapsed.
        /// </value>
        [BsonIgnore]
        public string Elapsed
        {
            get
            {
                return Status == ProcessingStatus.Processing || Status == ProcessingStatus.Pending
                    ? (DateTime.UtcNow - CreatedDate).ToReallyFriendlyTime()
                    : (ModifiedDate - CreatedDate).ToReallyFriendlyTime();
            }
        }

        /// <summary>
        /// Gets the name of the created by user.
        /// </summary>
        /// <value>
        /// The name of the created by user.
        /// </value>
        [BsonIgnore]
        public string CreatedByUserName
        {
            get
            {
                var u = CreatedById == User.DefaultUserId ? User.System : GetReferenceValue<User>(CreatedById);
                return u == null ? "" : u.DisplayName; 
            }
        }

    } // class: EligibilityUpload
} // namespace
