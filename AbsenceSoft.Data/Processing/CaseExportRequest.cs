﻿using AbsenceSoft.Data.Audit;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace AbsenceSoft.Data.Processing
{
    [AuditClassNo]
    [Serializable]
    public class CaseExportRequest : BaseEmployeeNumberEntity<CaseExportRequest>
    {
        /// <summary>
        /// Gets or sets the name of the file.
        /// </summary>
        /// <value>
        /// The name of the file.
        /// </value>
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the Content Type
        /// </summary>
        public string ContentType { get; set; }
   
        /// <summary>
        /// Gets or sets the Download Type
        /// </summary>
        public CaseExportFormat DownloadType { get; set; }
        
        /// <summary>
        /// Gets or sets the File Key
        /// </summary>
        public string FileKey { get; set; }

        /// <summary>
        /// Gets or sets the Document Id
        /// </summary>
        public string DocumentId { get; set; }

        /// <summary>
        /// Gets or sets the Document
        /// </summary>
        public Document Document {
            get { return GetReferenceValue<Document>(DocumentId); }
            set { DocumentId = SetReferenceValue<Document>(value); }
        }

        /// <summary>
        /// Gets or sets the Case Id
        /// </summary>
        [BsonRequired, BsonRepresentation(BsonType.ObjectId)]
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the comma separated Case Ids
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public string CaseIds { get; set; }

        /// <summary>
        /// Gets or sets the Case
        /// </summary>
        public Case Case
        {
            get { return GetReferenceValue<Case>(CaseId); }
            set { CaseId = SetReferenceValue<Case>(value); }
        }
        /// <summary>
        /// Gets or sets the type of the MIME.
        /// </summary>
        /// <value>
        /// The type of the MIME.
        /// </value>
        public string MimeType { get; set; }

        /// <summary>
        /// Gets or sets the size.
        /// </summary>
        /// <value>
        /// The size.
        /// </value>
        public long Size { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public ProcessingStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the errors.
        /// </summary>
        /// <value>
        /// The errors.
        /// </value>
        public List<string> Errors { get; set; }

        /// <summary>
        /// Gets the elapsed.
        /// </summary>
        /// <value>
        /// The elapsed.
        /// </value>
        [BsonIgnore]
        public string Elapsed
        {
            get
            {
                return Status == ProcessingStatus.Processing || Status == ProcessingStatus.Pending
                    ? (DateTime.UtcNow - CreatedDate).ToReallyFriendlyTime()
                    : (ModifiedDate - CreatedDate).ToReallyFriendlyTime();
            }
        }

        /// <summary>
        /// Gets the name of the created by user.
        /// </summary>
        /// <value>
        /// The name of the created by user.
        /// </value>
        [BsonIgnore]
        public string CreatedByUserName
        {
            get
            {
                var u = CreatedById == User.DefaultUserId ? User.System : GetReferenceValue<User>(CreatedById);
                return u == null ? "" : u.DisplayName; 
            }
        }

    } // class: CaseExportRequest
} // namespace
