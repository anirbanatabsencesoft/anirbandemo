﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data
{
    [Serializable]
    public class Region
    {
        public string Name { get; set; } 
        public string Code { get; set; }
    }
}
