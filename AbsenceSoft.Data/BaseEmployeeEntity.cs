﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Collections.Generic;
using AbsenceSoft.Data.Cases;


namespace AbsenceSoft.Data
{
    [Obsolete("Employee _id is nice, but please consider using BaseEmployeeNumberEntity instead for 'soft' employee references by employee number instead of internal _id", false)]
    [Serializable, JsonObject(MemberSerialization.OptOut)]
    public class BaseEmployeeEntity<TEntity> : BaseEmployerEntity<TEntity> where TEntity : BaseEmployeeEntity<TEntity>, new()
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the employee id for the entity.
        /// </summary>
        [BsonRequired, BsonRepresentation(BsonType.ObjectId)]
        public virtual string EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the employee number.
        /// </summary>
        /// <value>
        /// The employee number.
        /// </value>
        public virtual string EmployeeNumber { get; set; }

        /// <summary>
        /// Gets or sets the entity's employee.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public virtual Employee Employee
        {
            get { return GetReferenceValue<Employee>(this.EmployeeId); }
            set { EmployeeId = SetReferenceValue(value); }
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching.
        /// </summary>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static new IQueryable<TEntity> AsQueryable()
        {
            if (User.Current != null)
                return AsQueryable(User.Current);

            return BaseEmployerEntity<TEntity>.AsQueryable();
        }

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching while automatically
        /// applying the appropriate data access filters onto it automagically.
        /// </summary>
        /// <param name="entity">The entity of type <c>TEntity</c> to which to build the filtered queryable off of</param>
        /// <param name="user">The user of which to build data visibility and data access from</param>
        /// <param name="permission">The permission that the user should have access to when filtering the data parameters</param>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static new IQueryable<TEntity> AsQueryable(User user, Permission permission) { return AsQueryable(user, permission.Id); }
        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching while automatically
        /// applying the appropriate data access filters onto it automagically.
        /// </summary>
        /// <param name="entity">The entity of type <c>TEntity</c> to which to build the filtered queryable off of</param>
        /// <param name="user">The user of which to build data visibility and data access from</param>
        /// <param name="permission">The permission that the user should have access to when filtering the data parameters</param>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static new IQueryable<TEntity> AsQueryable(User user, string permission = null)
        {
            var query = BaseEmployerEntity<TEntity>.AsQueryable(user, permission);
            
            // Determine if the user is a SS User for any employer queried
            if ((User.IsEmployeeSelfServicePortal || (user.Customer != null && user.Customer.HasFeature(Enums.Feature.ESSDataVisibilityInPortal))) && !User.Permissions.GetProjectedPermissions(user).Contains(Permission.ViewAllEmployees.Id))
                query = query.Where(q => User.HasVisibilityTo.Contains(q.EmployeeId));

            return query;
        }//end: AsQueryable

        /// <summary>
        /// Updates the Employee Number.
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="originalEmployeeNumber"></param>
        /// <param name="newEmployeeNumber"></param>
        /// <param name="modifiedById"></param>
        /// <param name="customerId"></param>
        /// <param name="employerId"></param>
        public static void UpdateEmployeeNumber(string employeeId, string originalEmployeeNumber, string newEmployeeNumber, string modifiedById, string customerId, string employerId)
        {
            Update(Query.EQ(e => e.EmployeeId, employeeId), Updates.Set(e => e.EmployeeNumber, newEmployeeNumber).Set(e=>e.ModifiedDate,DateTime.UtcNow).Set(e => e.ModifiedById, modifiedById), MongoDB.Driver.UpdateFlags.Multi);
        }
        #endregion

        /// <summary>
        /// Called before saving
        /// </summary>
        /// <returns>
        /// A value indicating whether or not the save should proceed (not canceled).
        /// </returns>
        protected override bool OnSaving()
        {
            if (string.IsNullOrWhiteSpace(EmployeeNumber) && !string.IsNullOrWhiteSpace(EmployeeId) && Employee != null)
                EmployeeNumber = Employee.EmployeeNumber;
            return base.OnSaving();
        }
    }//end: BaseEmployeeEntity
}//end: namespace
