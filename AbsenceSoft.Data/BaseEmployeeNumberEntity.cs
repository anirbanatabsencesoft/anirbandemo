﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Collections.Generic;
using AbsenceSoft.Data.Cases;


namespace AbsenceSoft.Data
{
    [Serializable, JsonObject(MemberSerialization.OptOut)]
    public class BaseEmployeeNumberEntity<TEntity> : BaseEmployerEntity<TEntity> where TEntity : BaseEmployeeNumberEntity<TEntity>, new()
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the employee number.
        /// </summary>
        /// <value>
        /// The employee number.
        /// </value>
        [BsonRequired]
        public virtual string EmployeeNumber { get; set; }

        /// <summary>
        /// Gets or sets the entity's employee.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public virtual Employee Employee
        {
            get { return GetReferenceValue<Employee>(this.EmployeeNumber, k => k.EmployeeNumber, n => Employee.AsQueryable().Where(e => e.CustomerId == CustomerId && e.EmployerId == EmployerId && EmployeeNumber == n).FirstOrDefault()); }
            set { EmployeeNumber = SetReferenceValue(value, e => e.EmployeeNumber); }
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching.
        /// </summary>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static new IQueryable<TEntity> AsQueryable()
        {
            if (User.Current != null)
                return AsQueryable(User.Current);
            return BaseEmployerEntity<TEntity>.AsQueryable();
        }

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching while automatically
        /// applying the appropriate data access filters onto it automagically.
        /// </summary>
        /// <param name="entity">The entity of type <c>TEntity</c> to which to build the filtered queryable off of</param>
        /// <param name="user">The user of which to build data visibility and data access from</param>
        /// <param name="permission">The permission that the user should have access to when filtering the data parameters</param>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static new IQueryable<TEntity> AsQueryable(User user, Permission permission) { return AsQueryable(user, permission.Id); }
        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching while automatically
        /// applying the appropriate data access filters onto it automagically.
        /// </summary>
        /// <param name="entity">The entity of type <c>TEntity</c> to which to build the filtered queryable off of</param>
        /// <param name="user">The user of which to build data visibility and data access from</param>
        /// <param name="permission">The permission that the user should have access to when filtering the data parameters</param>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static new IQueryable<TEntity> AsQueryable(User user, string permission = null)
        {
            var query = BaseEmployerEntity<TEntity>.AsQueryable(user, permission).Where(e => e.IsDeleted != true);

            // Determine if the user is a SS User for any employer queried
#warning SECURITY: Need employee number mapping for ESS style data visibility permissions for EmployeeNumber related entities
            //if (User.IsEmployeeSelfServicePortal || (user.Customer.HasFeature(Enums.Feature.ESSDataVisibilityInPortal) && !User.Permissions.GetProjectedPermissions(user).Contains(Permission.ViewAllEmployees.Id)))
            //    query = query.Where(q => User.HasVisibilityTo.Contains(q.EmployeeId));

            return query;
        }//end: AsQueryable

        /// <summary>
        /// Updates the Employee Number.
        /// </summary>
        /// <param name="origEmployeeNumber"></param>
        /// <param name="newEmployeeNumber"></param>
        /// <param name="modifiedById"></param>
        /// <param name="customerId"></param>
        /// <param name="employerId"></param>
        public static void UpdateEmployeeNumber(string origEmployeeNumber, string newEmployeeNumber, string modifiedById, string customerId, string employerId)
        {
            Update(Query.And(Query.EQ(e => e.EmployeeNumber, origEmployeeNumber), Query.EQ(e => e.CustomerId, customerId), Query.EQ(e => e.EmployerId, employerId)), Updates.Set(e => e.EmployeeNumber, newEmployeeNumber).Set(e => e.ModifiedDate, DateTime.UtcNow).Set(e => e.ModifiedById, modifiedById), MongoDB.Driver.UpdateFlags.Multi);
        }
        #endregion
    }//end: BaseEmployeeNumberEntity
}//end: namespace
