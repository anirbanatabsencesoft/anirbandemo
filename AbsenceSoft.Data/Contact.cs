﻿using System;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using System.Text;

namespace AbsenceSoft.Data
{
    /// <summary>
    /// The contact class is NOT stored separetly in the DB, it is always a child document of another type and
    /// represents a contact or person.
    /// </summary>
    [Serializable]
    public class Contact : BaseNonEntity
    {
        [NonSerialized, BsonIgnore, JsonIgnore]
        private string workPhone;
        [NonSerialized, BsonIgnore, JsonIgnore]
        private string homePhone;
        [NonSerialized, BsonIgnore, JsonIgnore]
        private string cellPhone;
        [NonSerialized, BsonIgnore, JsonIgnore]
        private string faxPhone;

        public Contact()
        {
            Address = new Address();
        }

        /// <summary>
        /// Gets or sets the contact's company name.
        /// </summary>
        [BsonIgnoreIfNull]
        public string CompanyName { get; set; }

        /// <summary>
        /// Gets or sets the contact's Title address.
        /// </summary>
        [BsonIgnoreIfNull]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the contact's First Name.
        /// </summary>
        [BsonIgnoreIfNull]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the contact's Middle Name.
        /// </summary>
        [BsonIgnoreIfNull]
        public string MiddleName { get; set; }

        /// <summary>
        /// Gets or sets the contact's Last Name.
        /// </summary>
        [BsonIgnoreIfNull]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the contact's Date OF Birth.
        /// </summary>
        [BsonIgnoreIfNull]
        public DateTime? DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets the contact's Email address.
        /// </summary>
        [BsonIgnoreIfNull]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the contact's Alternate Email Address.
        /// </summary>
        [BsonIgnoreIfNull]
        public string AltEmail { get; set; }

        /// <summary>
        /// Gets or sets the contact's Work Phone.
        /// </summary>
        [BsonIgnoreIfNull]
        public string WorkPhone
        {
            get { return workPhone; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    workPhone = value;
                    return;
                }
                workPhone = value.StripPhone();
            }
        }

        /// <summary>
        /// Gets or sets the contact's Home Phone.
        /// </summary>
        [BsonIgnoreIfNull]
        public string HomePhone
        {
            get { return homePhone; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    homePhone = value;
                    return;
                }
                homePhone = value.StripPhone();
            }
        }

        /// <summary>
        /// Gets or sets the contact's Cell Phone.
        /// </summary>
        [BsonIgnoreIfNull]
        public string CellPhone
        {
            get { return cellPhone; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    cellPhone = value;
                    return;
                }
                cellPhone = value.StripPhone();
            }
        }

        /// <summary>
        /// Gets or sets the contact's fax phone number.
        /// </summary>
        [BsonIgnoreIfNull]
        public string Fax
        {
            get { return faxPhone; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    faxPhone = value;
                    return;
                }
                faxPhone = value.StripPhone();
            }
        }

        /// <summary>
        /// Gets or sets the contact's address.
        /// </summary>
        [BsonRequired]
        public Address Address { get; set; }

        /// <summary>
        /// Gets the full name.
        /// </summary>
        /// <value>
        /// The full name.
        /// </value>
        [BsonIgnore]
        public string FullName
        {
            get
            {
                StringBuilder name = new StringBuilder();
                if (!string.IsNullOrWhiteSpace(Title))
                    name.AppendFormat("{0}{1}", name.Length > 0 ? " " : "", Title);
                if (!string.IsNullOrWhiteSpace(FirstName))
                    name.AppendFormat("{0}{1}", name.Length > 0 ? " " : "", FirstName);
                if (!string.IsNullOrWhiteSpace(MiddleName))
                    name.AppendFormat("{0}{1}", name.Length > 0 ? " " : "", MiddleName);
                if (!string.IsNullOrWhiteSpace(LastName))
                    name.AppendFormat("{0}{1}", name.Length > 0 ? " " : "", LastName);
                return name.ToString();
            }
            set { }
        }


        [BsonIgnoreIfNull]
        public bool? IsPrimary { get; set; }

        /// <summary>
        /// Gets or sets the if a HCP contact person (not to be confused with the HCP provider's name).
        /// </summary>
        [BsonIgnoreIfNull]
        public string ContactPersonName { get; set; }

        /// <summary>
        /// Gets or sets the Contact End date
        /// </summary>
        [BsonIgnoreIfNull]
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime? EndDate { get; set; }

    }
}
