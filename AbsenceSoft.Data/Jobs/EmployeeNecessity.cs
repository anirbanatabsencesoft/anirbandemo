﻿using AbsenceSoft.Data.Cases;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Jobs
{
    [Serializable]
    public class EmployeeNecessity : BaseEmployeeEntity<EmployeeNecessity>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeNecessity"/> class.
        /// </summary>
        public EmployeeNecessity() : base()
        {
            Dates = DateRange.Null;
        }

        /// <summary>
        /// Gets or sets the necessity id.
        /// </summary>
        /// <value>
        /// The equipment necessity id.
        /// </value>
        [BsonRepresentation(BsonType.ObjectId)]
        public string NecessityId { get; set; }

        /// <summary>
        /// Gets or sets the equipment name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [BsonIgnoreIfNull]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the equipment type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        [BsonIgnoreIfNull]
        public NecessityType? Type { get; set; }

        /// <summary>
        /// Gets or sets the equipment description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [BsonIgnoreIfNull]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the optional case identifier if this job was assigned in relation to an active case.
        /// </summary>
        /// <value>
        /// The optional case identifier.
        /// </value>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the optional case number.
        /// </summary>
        /// <value>
        /// The case number.
        /// </value>
        [BsonIgnoreIfNull]
        public string CaseNumber { get; set; }

        /// <summary>
        /// Gets or sets the dates this job is applicable.
        /// </summary>
        /// <value>
        /// The dates.
        /// </value>
        [BsonIgnoreIfDefault]
        public DateRange Dates { get; set; }

        /// <summary>
        /// Gets or sets the necessity.
        /// </summary>
        /// <value>
        /// The equipment.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public Necessity Necessity
        {
            get { return GetReferenceValue<Necessity>(NecessityId); }
            set { NecessityId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the case.
        /// </summary>
        /// <value>
        /// The case.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public Case Case
        {
            get { return GetReferenceValue<Case>(CaseId); }
            set { CaseId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Called before saving
        /// </summary>
        /// <returns>
        /// A value indicating whether or not the save should proceed (not canceled).
        /// </returns>
        protected override bool OnSaving()
        {
            if (!string.IsNullOrWhiteSpace(NecessityId) && (
                string.IsNullOrWhiteSpace(Name)
                || Type == null))
            {
                var e = Necessity;
                if (e != null)
                {
                    Name = e.Name;
                    Type = e.Type;
                    Description = e.Description;
                }
            }
            if (!string.IsNullOrWhiteSpace(CaseId) && string.IsNullOrWhiteSpace(CaseNumber))
            {
                var c = Case;
                if (c != null)
                    CaseNumber = c.CaseNumber;
            }
            return base.OnSaving();
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return ToString(true);
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <param name="showDate">if set to <c>true</c> [show date].</param>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public virtual string ToString(bool showDate)
        {
            StringBuilder me = new StringBuilder();
            me.Append(Name);
            if (Type.HasValue)
                me.AppendFormat("{0}{1}", me.Length > 0 ? ", " : "",
                    Type.ToString().SplitCamelCaseString());
            if (showDate && Dates != null && !Dates.IsNull)
                me.AppendFormat("{0}{1}", me.Length > 0 ? ", " : "",
                    Dates.ToString("(permanent)"));

            return me.ToString();
        }
    }
}
