﻿using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Jobs
{
    [Serializable]
    public abstract class AppliedDemand<TApplied> : BaseNonEntity where TApplied : AppliedDemandValue<TApplied>, new()
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppliedDemand{TApplied}"/> class.
        /// </summary>
        public AppliedDemand()
        {
            Dates = DateRange.Null;
        }

        /// <summary>
        /// Gets or sets the demand code.
        /// </summary>
        /// <value>
        /// The demand code.
        /// </value>
        [BsonRequired]
        public string DemandId { get; set; }

        /// <summary>
        /// Gets or sets the date range for the applied demand to be applicable.
        /// </summary>
        /// <value>
        /// The dates the job location is applicable.
        /// </value>
        [BsonIgnoreIfNull]
        public DateRange Dates { get; set; }

        /// <summary>
        /// Gets or sets the values.
        /// </summary>
        /// <value>
        /// The values.
        /// </value>
        [BsonIgnoreIfNull]
        public List<AppliedDemandValue<TApplied>> Values { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [BsonIgnoreIfNull]
        public string Description { get; set; }

        /// <summary>
        /// When overridden in a derived class,
        /// Evaluates the specified applied demand against this applied demand instance to determine what the demand evaluation result is.
        /// </summary>
        /// <typeparam name="TOther">The type of the other applied demand result.</typeparam>
        /// <param name="demand">The applied demand to evaluate against, the DemandCode should generally match.</param>
        /// <returns>A demand evaluation result containing the result and reasons for the result in plain text.</returns>
        public abstract DemandEvaluationResult Evaluate<TOther>(AppliedDemand<TOther> demand) where TOther : AppliedDemandValue<TOther>, new();

        /// <summary>
        /// Gets or sets the type of the demand.
        /// </summary>
        /// <value>
        /// The type of the demand.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public Demand Demand
        {
            get { return GetReferenceValue<Demand>(DemandId); }
            set { DemandId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            if (this.Values == null || !this.Values.Any(v => v.Value != null))
                return string.Empty;

            return string.Join(", ", this.Values
                .Where(v => v.Value != null)
                .Select(v => string.Format("{0}", v)));
        }
    }
}
