﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Jobs
{
    [Serializable]
    public class JobIndustryCode : BaseNonEntity
    {
        /// <summary>
        /// Gets or sets the type of the code.
        /// </summary>
        /// <value>
        /// The type of the code.
        /// </value>
        public string CodeType { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
        public string Code { get; set; }
    }
}
