﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Jobs
{
    [Serializable]
    public class JobRequirement : AppliedDemand<JobRequirementValue>
    {
        /// <summary>
        /// Evaluates the specified applied demand against this applied demand instance to determine what the demand evaluation result is.
        /// </summary>
        /// <typeparam name="TOther">The type of the other applied demand result.</typeparam>
        /// <param name="demand">The applied demand to evaluate against, the DemandCode should generally match.</param>
        /// <returns>
        /// A demand evaluation result containing the result and reasons for the result in plain text.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override DemandEvaluationResult Evaluate<TOther>(AppliedDemand<TOther> demand)
        {
            // Need to compare applied demand being passed in with this applied demand, marry up requirements
            //  and determine what the result should be, either true or false, and ensure proper formatted reasons
            //  using the item values or other plain English explanation is set properly (language ability to come later).
            //
            // The reason this is an override and not just a base implementation is because of the meaning of TRUE/FALSE in the result
            //  as well as the way the English explanations/reasons may be structures in relation to how they are being evaluated.
            //....
            // For Job Requirements, remember the Numerical value (for value types) for the requirement needs to be HIGHER than the Restriction's
            //  so this should return a FALSE result if this aggregate VALUE is Lower than the passed in applied demand's aggregate value
            //  otherwise, TRUE
            // For Text, always return TRUE
            //  Then for schedule, need to determine if the ScheduleDemand is greater than the passed in demand's schedule demand (need to probably
            //      create some comparison logic there native to the ScheduleDemand, like IComparable<T> or operator implementations for that)
            //  Then for Boolean types, obviously return == each other, otherwise false.

            // Really? Who passes in null, seriously? Get out of here and just be successful with your null self.
            if (demand == null)
                return DemandEvaluationResult.Pass();

            // If dates don't overlap, then it's not applicable (out of range), so no success reason, it just passes
            if (demand.Dates != null && !demand.Dates.DateRangesOverLap(Dates))
                return DemandEvaluationResult.Pass();

            // If we don't have any values to compare, um, then duh, pass
            if (Values == null || !Values.Any())
                return DemandEvaluationResult.Pass();

            // If we aren't comparing any values, um, c'mon people, it passes, duh.
            if (demand.Values == null || !demand.Values.Any())
                return DemandEvaluationResult.Pass();

            // Now, for each restriction value get the corresponding value(s) from this Job based on value type
            var matches = new List<Tuple<AppliedDemandValue<JobRequirementValue>, AppliedDemandValue<TOther>>>();
            foreach (var value in demand.Values)
            {
                var myValues = Values.Where(v => v.DemandTypeId == value.DemandTypeId && v.Type == value.Type);
                foreach (var myVal in myValues)
                    matches.Add(new Tuple<AppliedDemandValue<JobRequirementValue>, AppliedDemandValue<TOther>>(myVal, value));
            }

            // Collectively run a single query to pull all Demand Types from the DB that we need. Super fast since it's by _id in MongoDB.
            var demandTypeIds = matches.Select(m => m.Item1.DemandTypeId).Union(matches.Select(m => m.Item2.DemandTypeId)).Distinct().ToList();
            List<DemandType> types = DemandType.AsQueryable().Where(t => demandTypeIds.Contains(t.Id)).ToList();

            // This will store all of our results which are aggregated later on, yay!
            List<DemandEvaluationResult> results = new List<DemandEvaluationResult>();

            // Loop through each of the match tuples to run comparisons, this is the super magical part that 
            foreach (var m in matches)
            {
                // Compare JOB REQUIREMENT value to the WORK RESTRICTION value
                int? compareResult = m.Item1.CompareTo(m.Item2);
                // If they are the same, that probably means it's of a type that doesn't require evaluation, OR, if equal then we're good
                if (compareResult == null)
                    results.Add(DemandEvaluationResult.Pass()); // Passes for no reason, no reason at all
                else
                {
                    // Ok, now we won't be wasting CPU cycles or potential queries to pull the demand types for these items.
                    //  These are needed to build the explanation thingies
                    m.Item1.DemandType = types.FirstOrDefault(t => t.Id == m.Item1.DemandTypeId) ?? m.Item1.DemandType;
                    m.Item2.DemandType = types.FirstOrDefault(t => t.Id == m.Item2.DemandTypeId) ?? m.Item2.DemandType;

                    // This is supposed to read something like this in plain English (for example):
                    //  "{Carrying w/out handles} has {Weight Requirements (lbs)} of {11 - 20 lbs} {but the employee has} {Weight Restrictions (lbs)} of {5 - 10 lbs}"
                    // The variator (parameter) is intended to act as a tweener that variates based on pass/fail/pass as match, etc.
                    var message = new Func<string, string>(variator => string.Format(
                            "{0} has {1} of {2} {5} {3} of {4}",
                            Demand.Name,
                            m.Item1.DemandType.RequirementLabel ?? m.Item1.DemandType.Name,
                            m.Item1,
                            m.Item2.DemandType.RestrictionLabel ?? m.Item2.DemandType.Name,
                            m.Item2,
                            variator));

                    if (compareResult > 0 || (m.Item1.Type == DemandValueType.Boolean && compareResult != 0))
                        // Our job requirement exceeds the restriction, that's bad, we can't do this job :-(
                        //  Also, because boolean values are special, if it's not zero then FAIL!
                        results.Add(DemandEvaluationResult.Fail(message("but the employee has")));
                    else
                    {
                        // YAY! :-) We can do this job because we rock and that's awesome!!!
                        // Our variator basically 
                        string variator = "and the employee only has";
                        // If the result is zero, it's an exact match, so make our variator make more sense
                        if (compareResult == 0)
                            variator = "which the employee matches with";
                        // Add the passing result with the passing message
                        results.Add(DemandEvaluationResult.Pass(message(variator)));
                    }
                }
            }

            // Return the aggregated results
            return DemandEvaluationResult.Aggregate(results);
        }
    }
}
