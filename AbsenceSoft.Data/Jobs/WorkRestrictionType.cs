﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Jobs
{
    /// <summary>
    /// Enumerates the possible work restriction types.
    /// </summary>
    [Serializable]
    public enum WorkRestrictionType : int
    {
        /// <summary>
        /// The employee is not restricted
        /// </summary>
        None = 0,
        /// <summary>
        /// The employee is fully restricted (i.e. vision = blind, can't lift anything, etc.)
        /// </summary>
        Fully = 1,
        /// <summary>
        /// The employee is partially restricted (can only do 'x' but not 'y' or only so much of 'z', etc.)
        /// </summary>
        Partially = 2
    }
}
