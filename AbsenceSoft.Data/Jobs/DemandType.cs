﻿using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace AbsenceSoft.Data.Jobs
{
    [Serializable]
    public class DemandType : BaseEmployerOverrideableEntity<DemandType>
    {
        /// <summary>
        /// Gets or sets the name of the demand type.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [BsonRequired]
        public string Name { get; set; }

        /// <summary>
        /// The requirement label
        /// </summary>
        [BsonElement("RequirementLabel"), JsonProperty("RequirementLabel")]
        private string _requirementLabel;

        /// <summary>
        /// Gets or sets the requirement label.
        /// </summary>
        /// <value>
        /// The requirement label.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public string RequirementLabel
        {
            get { return _requirementLabel ?? Name; }
            set { _requirementLabel = value == Name ? null : value; }
        }

        /// <summary>
        /// The restriction label
        /// </summary>
        [BsonElement("RestrictionLabel"), JsonProperty("RestrictionLabel")]
        private string _restrictionLabel;

        /// <summary>
        /// Gets or sets the restriction label.
        /// </summary>
        /// <value>
        /// The restriction label.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public string RestrictionLabel
        {
            get { return _restrictionLabel ?? Name; }
            set { _restrictionLabel = value == Name ? null : value; }
        }

        /// <summary>
        /// Gets or sets an optional and helpful description of the demand type.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [BsonIgnoreIfNull]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the type. The default value is <c>DemandValueType.Text</c> (0).
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        [BsonRequired, BsonDefaultValue(DemandValueType.Text), DefaultValue(DemandValueType.Text)]
        public DemandValueType Type { get; set; }

        /// <summary>
        /// Gets or sets the items. By default this value is <c>null</c> and may be empty
        /// based on the demand type.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        [BsonIgnoreIfNull]
        public List<DemandItem> Items { get; set; }

        /// <summary>
        /// Gets or sets the order this demand type is displayed in on the UI or in reports when showing
        /// demands in a matrix or other fashion.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        [BsonIgnoreIfNull]
        public int? Order { get; set; }

        /// <summary>
        /// Called when [saving].
        /// </summary>
        /// <returns></returns>
        protected override bool OnSaving()
        {
            if (Items != null && !Items.Any())
                Items = null;
            return base.OnSaving();
        }
    }
}
