﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Jobs
{
    [Serializable]
    public class DemandEvaluationResult : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DemandEvaluationResult"/> class.
        /// </summary>
        public DemandEvaluationResult()
        {
            Reasons = new List<string>();
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="DemandEvaluationResult"/> is pass/success/met/etc.
        /// </summary>
        /// <value>
        ///   <c>true</c> if pass/success/met/etc.; otherwise, <c>false</c>.
        /// </value>
        public bool Result { get; set; }

        /// <summary>
        /// Gets or sets the reasons the result is either <c>true</c> or <c>false</c>. A generated explanation
        /// based on configured demands against either requirements or restrictions.
        /// </summary>
        /// <value>
        /// The reasons the result is either <c>true</c> or <c>false</c>.
        /// </value>
        public List<string> Reasons { get; set; }

        /// <summary>
        /// Created a failed demand evaluation result for the specified reasons.
        /// </summary>
        /// <param name="forReasons">For reasons it failed.</param>
        /// <returns></returns>
        public static DemandEvaluationResult Fail(params string[] forReasons)
        {
            return new DemandEvaluationResult()
            {
                Result = false,
                Reasons = forReasons.ToList()
            };
        }

        /// <summary>
        /// Created a successful demand evaluation result for the specified reasons.
        /// </summary>
        /// <param name="forReasons">For reasons it succeeded.</param>
        /// <returns></returns>
        public static DemandEvaluationResult Pass(params string[] forReasons)
        {
            return new DemandEvaluationResult()
            {
                Result = true,
                Reasons = forReasons.ToList()
            };
        }

        /// <summary>
        /// Aggregates the specified results.
        /// </summary>
        /// <param name="results">The results.</param>
        /// <returns></returns>
        public static DemandEvaluationResult Aggregate(IEnumerable<DemandEvaluationResult> results)
        {
            if (results == null || !results.Any())
                return Pass();

            bool aggResult = results.All(r => r.Result);
            var reasons = results.Where(r => r.Result == aggResult).SelectMany(r => r.Reasons).ToList();

            return new DemandEvaluationResult()
            {
                Result = aggResult,
                Reasons = reasons
            };
        }
    }
}
