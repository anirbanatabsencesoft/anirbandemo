﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Jobs
{
    [Serializable]
    public class EmployeeRestriction : BaseEmployeeEntity<EmployeeRestriction>
    {
        /// <summary>
        /// Gets or sets the optional case identifier (the case in which this is associated). This still belongs
        /// primarily to the employee however.
        /// </summary>
        /// <value>
        /// The case identifier.
        /// </value>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the case number.
        /// </summary>
        /// <value>
        /// The case number.
        /// </value>
        [BsonIgnoreIfNull]
        public string CaseNumber { get; set; }

        /// <summary>
        /// Gets or sets the restriction.
        /// </summary>
        /// <value>
        /// The restriction.
        /// </value>
        public WorkRestriction Restriction { get; set; }

        /// <summary>
        /// Gets or sets the case.
        /// </summary>
        /// <value>
        /// The case.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public Case Case
        {
            get { return GetReferenceValue<Case>(CaseId); }
            set { CaseId = SetReferenceValue(value); }
        }
    }
}
