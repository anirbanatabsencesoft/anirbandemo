﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Jobs
{
    [Serializable]
    public class JobEvaluationResult
    {
        /// <summary>
        /// Gets or sets the job.
        /// </summary>
        /// <value>
        /// The job.
        /// </value>
        public Job Job { get; set; }

        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <value>
        /// The result.
        /// </value>
        public DemandEvaluationResult Result { get; set; }

        /// <summary>
        /// Gets a value indicating whether [meets restrictions].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [meets restrictions]; otherwise, <c>false</c>.
        /// </value>
        public bool MeetsRestrictions { get { return Result == null || Result.Result; } }

        /// <summary>
        /// Gets the reasons.
        /// </summary>
        /// <value>
        /// The reasons.
        /// </value>
        public List<string> Reasons
        {
            get
            {
                return Result == null 
                    ? new List<string>(0) 
                    : Result.Reasons ?? new List<string>(0);
            }
        }
    }
}
