﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Jobs
{
    /// <summary>
    /// Defines a schedule demand (similar to an intermittent certification).
    /// <para>
    /// Duration per occurance per frequency: 3 hours per 4 times per 10 work days
    /// <list type="bullet">
    /// <item>3 hours = Duration and Duration type</item>
    /// <item>4 times = occurance</item>
    /// <item>10 work days = frequency and frequency calendar type</item>
    /// </list>
    /// </para>
    /// </summary>
    [Serializable]
    public class ScheduleDemand : BaseNonEntity, IComparable<ScheduleDemand>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ScheduleDemand"/> class.
        /// </summary>
        public ScheduleDemand() : base()
        {
            // Defaults
            FrequencyType = Unit.Weeks;
            DurationType = Unit.Hours;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ScheduleDemand"/> class.
        /// </summary>
        /// <param name="cert">The cert.</param>
        public ScheduleDemand(Certification cert) : this()
        {
            if (cert == null)
                return;

            this.Duration = cert.Duration;
            this.DurationType = cert.DurationType;
            this.Frequency = cert.Frequency;
            this.FrequencyType = cert.FrequencyType;
            this.FrequencyUnitType = cert.FrequencyUnitType;
            this.Occurances = cert.Occurances;
        }

        /// <summary>
        /// Gets or sets the number of occurrances that are expected to occur through the specified frequency.
        /// </summary>
        public int Occurances { get; set; }

        /// <summary>
        /// Gets or sets the frequency of the occurances as they are certified/expected to occur.
        /// </summary>
        public int Frequency { get; set; }

        /// <summary>
        /// Gets or sets the units used to measure the frequency of the occurrances.
        /// </summary>
        public Unit FrequencyType { get; set; }

        /// <summary>
        /// set the Frequency Type to work days or calendar days (used when setting the period window test) 
        /// default is work days.
        /// </summary>
        public DayUnitType FrequencyUnitType { get; set; }

        /// <summary>
        /// Gets or sets the certified duration for each occurance to occur within the bounds of.
        /// </summary>
        public double Duration { get; set; }

        /// <summary>
        /// Gets or sets the units used to measure duration of each occurrance.
        /// </summary>
        public Unit DurationType { get; set; }

        /// <summary>
        /// Gets a pretty plain-english representation of this certification.
        /// </summary>
        /// <returns>A pretty plain-english representation of this certification.</returns>
        public override string ToString()
        {
            return string.Format("{0} occurance{5} every {1} {2} at {3} {4} per occurance",
                Occurances > 0 ? Occurances.ToString() : "No",
                Frequency,
                FrequencyType,
                Duration,
                DurationType,
                Occurances != 1 ? "s" : "");
        }

        /// <summary>
        /// Coverts this instance of a schedule demand to an intermittent certification.
        /// </summary>
        /// <param name="dates">The date range to apply to the intermittent certification (required).</param>
        /// <returns>A fully populated intermittent certification instance.</returns>
        public Certification ToCertification(DateRange dates)
        {
            return new Certification()
            {
                Duration = Duration,
                DurationType = DurationType,
                EndDate = dates.EndDate ?? dates.StartDate,
                Frequency = Frequency,
                FrequencyType = FrequencyType,
                FrequencyUnitType = FrequencyUnitType,
                Occurances = Occurances,
                StartDate = dates.StartDate
            };
        }

        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared. The return value has the following meanings: Value Meaning Less than zero This object is less than the <paramref name="other" /> parameter.Zero This object is equal to <paramref name="other" />. Greater than zero This object is greater than <paramref name="other" />.
        /// </returns>
        public int CompareTo(ScheduleDemand other)
        {
            // If other is null, this comes after it
            if (other == null)
                return 1;

            // All other things equal, they're equal
            if (Duration == other.Duration
                && DurationType == other.DurationType
                && Frequency == other.Frequency
                && FrequencyType == other.FrequencyType
                && FrequencyUnitType == other.FrequencyUnitType
                && Occurances == other.Occurances)
                return 0;

            // Run comparison algorithm using 1 year timeframe
            return CalculateMaxMinutesIn(1, Unit.Years).CompareTo(other.CalculateMaxMinutesIn(1, Unit.Years));
        }

        /// <summary>
        /// Calculates the maximum minutes in the number of units specified. Eeww, math, yuck.
        /// </summary>
        /// <param name="numberOf">The number of.</param>
        /// <param name="inUnit">The in unit.</param>
        /// <returns>A total number of minutes that could be scheduled in the timeframe passed in.</returns>
        public int CalculateMaxMinutesIn(double numberOf, Unit inUnit)
        {
            // What is the passed in timeframe in minutes?
            double timeframe = inUnit.ConvertUnits(Unit.Minutes, numberOf);
            // What is our current instance's duration in minutes?
            double duration = DurationType.ConvertUnits(Unit.Minutes, Duration);
            // What is our current instance's frequency in minutes (window)?
            double frequency = FrequencyType.ConvertUnits(Unit.Minutes, Frequency);
            // What is the maximum number of occurrance that can happen within our timeframe given
            //  the number of windows (by frequency) we can squeeze into our passed in timeframe
            //  and then multiplying that by the number of occurrances that can happen within
            //  each timeframe (so if 2 hours 2 times per week, that max of 2 * 2 * 60 = 240 minutes per week
            double maxOccurrences = (timeframe / frequency) * Occurances;
            // What are the maximum number of minutes that can be used in the given timeframe which is
            //  the product of duration (per occurrance) and max occurrances
            double maxMinutesInTimeframe = duration * maxOccurrences;
            // Round up using Ceiling to the next whole number, then convert to a 32-bit integer and return
            return Convert.ToInt32(Math.Ceiling(maxMinutesInTimeframe));
        }
    }
}
