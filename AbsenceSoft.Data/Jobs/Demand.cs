﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Data.Jobs
{
    [Serializable]
    public class Demand : BaseEmployerOverrideableEntity<Demand>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Demand"/> class.
        /// </summary>
        public Demand() : base()
        {
            Types = new List<string>();
        }

        /// <summary>
        /// Gets or sets the name of the demand.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [BsonRequired]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets an optional and helpful description of the demand.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the demand type ids that are associated with this demand
        /// which may be selected for a job requirement or work restriction.
        /// </summary>
        /// <value>
        /// The type ids.
        /// </value>
        public List<string> Types { get; set; }

        /// <summary>
        /// Gets or sets the order this demand is displayed in on the UI or in reports when showing
        /// demands in a matrix or other fashion.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        [BsonIgnoreIfNull]
        public int? Order { get; set; }

        /// <summary>
        /// Gets or sets the category this Demand belongs too
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// Gets the types.
        /// </summary>
        /// <returns>A list of demand type entities based on the ids provided.</returns>
        public List<DemandType> GetTypes()
        {
            return DemandType.AsQueryable().Where(dt => Types.Contains(dt.Id)).ToList();
        }
    }
}
