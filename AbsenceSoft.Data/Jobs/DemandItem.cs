﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Jobs
{
    [Serializable]
    public class DemandItem : BaseNonEntity
    {
        /// <summary>
        /// Gets or sets the name of the demand item.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [BsonRequired]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the comparison value of this demand item for quantification. Default is <c>0</c> (zero).
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        /// <remarks>
        /// <para>If a demand item has a value of 1 and another a value of 2, and a restriction is entered that states the Min Value = 2
        /// and a restriction puts the employee at a value of 1, we know the demand item is not met and therefore
        /// the demand type fails and the overall grouping of demands is not met.</para>
        /// </remarks>
        [BsonIgnoreIfDefault]
        public int Value { get; set; }

        /// <summary>
        /// Gets or sets the score, associated with a risk factor or scoring mechanism used
        /// within other or future functions of the site or for reporting purposes.
        /// </summary>
        /// <value>
        /// The score.
        /// </value>
        [BsonIgnoreIfNull]
        public decimal? Score { get; set; }

        /// <summary>
        /// Gets or sets the classification which represents the Social Security Administration's defintions of the 5 job classifications and related restrictions
        /// when relating to social security claims, but are also used for worker's comp and other benefits and legal definitions
        /// around disability.
        /// </summary>
        /// <value>
        /// The classification.
        /// </value>
        [BsonIgnoreIfNull]
        public JobClassification? Classification { get; set; }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
