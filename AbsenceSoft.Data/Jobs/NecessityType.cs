﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Jobs
{
    /// <summary>
    /// Enumerates the types of equipment represented by the Equipment entity.
    /// </summary>
    [Serializable]
    public enum NecessityType : int
    {
        /// <summary>
        /// The medical device equipment type, includes all external and internal medically necessary devices, implants, attachments,
        /// assistance devices (wheelchairs, crutches, etc.)
        /// </summary>
        MedicalDevice = 0,
        /// <summary>
        /// The medication equipment type isn't really equipment, but it is something that gets applied like medical equipment for a short term, long term or
        /// permanent period that may or may not impair an employee's ability to perform certain job duties. Medication equipment should be limited (for now)
        /// to general classifications of medication, such as narcotics, antibiotics, anti-depression, etc.
        /// </summary>
        Medication = 1
    }
}
