﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Serializers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Jobs
{
    [Serializable]
    public class Job : BaseEmployerOverrideableEntity<Job>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Job"/> class.
        /// </summary>
        public Job() : base()
        {
            Documents = new List<string>();
            Links = new List<Hyperlink>();
            Requirements = new List<JobRequirement>();
            IndustryCodes = new List<JobIndustryCode>();
            RestrictedNecessities = new List<JobRestrictedNecessity>();
        }

        /// <summary>
        /// Gets or sets the name of the job.
        /// </summary>
        /// <value>
        /// The name of the job.
        /// </value>
        [BsonRequired]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the job description. May be (basic) HTML or plain text. This is a description
        /// to be displayed under the job name in the UI to offer additional information.
        /// </summary>
        /// <value>
        /// The job description.
        /// </value>
        [BsonIgnoreIfNull]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the office locations that this job applies to.
        /// </summary>
        /// <value>
        /// The office locations.
        /// </value>
        [BsonIgnoreIfNull]
        public string OrganizationCode { get; set; }

        [BsonIgnoreIfNull]
        public JobClassification? Activity { get; set; }

        [BsonIgnore, JsonIgnore]
        public Organization Organization
        {
            get
            {
                return GetReferenceValue<Organization>(OrganizationCode, s => s.Code, s => Organization.GetByCode(s, CustomerId, EmployerId));
            }
            set
            {
                OrganizationCode = SetReferenceValue<Organization>(value, e => e.Code);
            }
        }

        /// <summary>
        /// Gets or sets the list of document ids. This may be <c>null</c> or contain a list of
        /// ids that point to Document entity records for attachments to the job (like a full description document, pictures, videos, etc.)
        /// </summary>
        /// <value>
        /// The documents for this job.
        /// </value>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public List<string> Documents { get; set; }

        /// <summary>
        /// Gets or sets the collection of links.
        /// </summary>
        /// <value>
        /// The links.
        /// </value>
        [BsonIgnoreIfNull]
        public List<Hyperlink> Links { get; set; }

        /// <summary>
        /// Gets or sets the industry codes.
        /// </summary>
        /// <value>
        /// The industry codes.
        /// </value>
        [BsonIgnoreIfNull]
        public List<JobIndustryCode> IndustryCodes { get; set; }

        /// <summary>
        /// Gets or sets the requirements.
        /// </summary>
        /// <value>
        /// The requirements.
        /// </value>
        [BsonIgnoreIfNull]
        public List<JobRequirement> Requirements { get; set; }

        /// <summary>
        /// Gets or sets the restricted necessities.
        /// </summary>
        /// <value>
        /// The restricted equipment.
        /// </value>
        [BsonIgnoreIfNull]
        public List<JobRestrictedNecessity> RestrictedNecessities { get; set; }

        /// <summary>
        /// Gets the documents based on the stored document ids.
        /// </summary>
        /// <returns>A list of associated document records.</returns>
        public List<Document> GetDocuments()
        {
            if (Documents == null || !Documents.Any())
                return new List<Document>(0);
            return Document.AsQueryable().Where(d => Documents.Contains(d.Id)).ToList();
        }

        /// <summary>
        /// Gets the restricted necessities for this job.
        /// </summary>
        /// <returns>A list of restricted necessities.</returns>
        public List<Necessity> GetRestrictedNecessities()
        {
            if (RestrictedNecessities == null || !RestrictedNecessities.Any())
                return new List<Necessity>(0);
            
            return Necessity.Query.Find(Necessity.Query.In(c => c.Id, RestrictedNecessities.Select(n => n.NecessityId))).ToList();
        }

        /// <summary>
        /// Called before saving
        /// </summary>
        /// <returns>
        /// A value indicating whether or not the save should proceed (not canceled).
        /// </returns>
        protected override bool OnSaving()
        {
            #region OnSaving

            if (!string.IsNullOrWhiteSpace(Code))
                Code = Code.ToUpperInvariant();

            if (Documents != null && !Documents.Any())
                Documents = null;
            if (Links != null && !Links.Any())
                Links = null;
            if (Requirements != null && !Requirements.Any())
                Requirements = null;
            if (IndustryCodes != null && !IndustryCodes.Any())
                IndustryCodes = null;
            if (RestrictedNecessities != null && !RestrictedNecessities.Any())
                RestrictedNecessities = null;

            bool saved = base.OnSaving();

            Documents = Documents ?? new List<string>();
            Links = Links ?? new List<Hyperlink>();
            Requirements = Requirements ?? new List<JobRequirement>();
            IndustryCodes = IndustryCodes ?? new List<JobIndustryCode>();
            RestrictedNecessities = RestrictedNecessities ?? new List<JobRestrictedNecessity>();

            return saved;

            #endregion
        }

        /// <summary>
        /// Evaluates the restrictions against this job to determine whether the result is a smiley face or
        /// sad face.
        /// </summary>
        /// <param name="restrictions">The restrictions to evaluate against this job's requirements.</param>
        /// <param name="necessities">The employee necessities.</param>
        /// <returns>
        /// An aggregate demand evaluation result for all restrictions passed against this job's requirements.
        /// </returns>
        public JobEvaluationResult EvaluateRestrictions(List<WorkRestriction> restrictions, List<EmployeeNecessity> necessities)
        {
            // Prepare our job evaluation result
            JobEvaluationResult result = new JobEvaluationResult() { Job = this };

            // Create a place to store any aggreate results
            List<DemandEvaluationResult> aggregate = new List<DemandEvaluationResult>();

            // Now let's check to see if there is any intersection of employee and restricted necessities
            if (RestrictedNecessities != null && RestrictedNecessities.Any() && necessities != null && necessities.Any())
            {
                // Get a collection of all bad necessity ids
                var badIds = RestrictedNecessities.Select(n => n.NecessityId).Intersect(necessities.Select(n => n.NecessityId)).ToList();
                foreach (var id in badIds)
                {
                    var thingy = necessities.FirstOrDefault(n => n.NecessityId == id);
                    if (thingy != null)
                    {
                        string conjunction = "is";
                        if (thingy.Name.EndsWith("s", StringComparison.InvariantCultureIgnoreCase))
                            conjunction = "are";
                        aggregate.Add(DemandEvaluationResult.Fail(string.Format("\"{0}\" {1} a necessity but restricted for this job", thingy.Name, conjunction)));
                    }
                }
            }
            else if (restrictions == null || !restrictions.Any() || Requirements == null || !Requirements.Any())
                // If we have no necessities and no restrictions, then it just passes, 'cause why else wouldn't it?
                aggregate.Add(DemandEvaluationResult.Pass());

            // Need to loop through each restriction, pull this job's matching requirement, if any
            //  for each match (based on demand id), then need to evaluate them and create an aggregate
            //  demand evaluation result and return it. (don't forget dates)
            if (!aggregate.Any())
                foreach (var cant in restrictions)
                    foreach (var can in Requirements.Where(r => r.DemandId == cant.DemandId))
                        aggregate.Add(can.Evaluate(cant));

            // Now aggregate the results together using this nifty handy super awesome method 'n' stuff.
            result.Result = DemandEvaluationResult.Aggregate(aggregate);
            return result;
        }
    }
}
