﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Jobs
{
    [Serializable]
    public class AppliedDemandValue<TApplied> : BaseNonEntity where TApplied : AppliedDemandValue<TApplied>, new()
    {
        /// <summary>
        /// Gets or sets the demand type code.
        /// </summary>
        /// <value>
        /// The demand type code.
        /// </value>
        [BsonRequired]
        public string DemandTypeId { get; set; }

        /// <summary>
        /// Gets or sets the type. The default value is <c>DemandValueType.Text</c> (0).
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        [BsonRequired, BsonDefaultValue(DemandValueType.Text), DefaultValue(DemandValueType.Text)]
        public DemandValueType Type { get; set; }

        /// <summary>
        /// Gets or sets the demand item.
        /// </summary>
        /// <value>
        /// The demand item.
        /// </value>
        [BsonIgnoreIfNull]
        public DemandItem DemandItem { get; set; }

        /// <summary>
        /// Gets or sets the response text. For <c>DemandValueType.Text</c>.
        /// </summary>
        /// <value>
        /// The response text.
        /// </value>
        [BsonIgnoreIfNull]
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="AppliedDemandValue{TApplied}"/> is applicable if the user
        /// responded Yes or No.
        /// </summary>
        /// <value>
        ///   <c>true</c> if applicable; otherwise, <c>false</c>.
        /// </value>
        [BsonIgnoreIfNull]
        public bool? Applicable { get; set; }

        /// <summary>
        /// Gets or sets the schedule.
        /// </summary>
        /// <value>
        /// The schedule.
        /// </value>
        [BsonIgnoreIfNull]
        public ScheduleDemand Schedule { get; set; }

        /// <summary>
        /// Gets the raw value based on the demand value type.
        /// </summary>
        /// <value>
        /// The value based on type.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public object Value
        {
            get
            {
                switch (Type)
                {
                    case DemandValueType.Text:
                        return Text;
                    case DemandValueType.Boolean:
                        if (Applicable == true)
                            Text = "Yes";
                        else
                            Text="No";
                        return Text;
                    case DemandValueType.Value:
                        return DemandItem;
                    case DemandValueType.Schedule:
                        return Schedule;
                    default:
                        return null;
                }
            }
            set
            {
                if (value == null)
                {
                    switch (Type)
                    {
                        case DemandValueType.Text:
                            Text = null;
                            break;
                        case DemandValueType.Boolean:
                            Applicable = null;
                            break;
                        case DemandValueType.Value:
                            DemandItem = null;
                            break;
                        case DemandValueType.Schedule:
                            Schedule = null;
                            break;
                    }
                    return;
                }
                if (value is string)
                    Text = value as string;
                else if (value is bool)
                    Applicable = (bool)value;
                else if (value is DemandItem)
                    DemandItem = value as DemandItem;
                else if (value is ScheduleDemand)
                    Schedule = value as ScheduleDemand;
            }
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            switch (Type)
            {
                case DemandValueType.Text:
                    return this.Text;
                case DemandValueType.Boolean:
                    return this.Value.ToString();
                case DemandValueType.Value:
                    if (this.DemandItem == null)
                        return "";
                    return this.DemandItem.Name;
                case DemandValueType.Schedule:
                    if (this.Schedule == null)
                        return "";
                    return string.Format("Frequency: {0} episode(s) every {1} {2}, Duration: {3} {4} per episode",
                        this.Schedule.Occurances, this.Schedule.Frequency, this.Schedule.FrequencyType, this.Schedule.Duration, this.Schedule.DurationType);
                default:
                    break;
            }
            return base.ToString();
        }
        
        /// <summary>
        /// Gets or sets the type of the demand.
        /// </summary>
        /// <value>
        /// The type of the demand.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public DemandType DemandType
        {
            get { return GetReferenceValue<DemandType>(DemandTypeId); }
            set { DemandTypeId = SetReferenceValue(value); }
        }

        /// <summary>
        /// ompares the current instance with another object of the same type and returns an integer that 
        /// indicates whether the current instance precedes, follows, or occurs in the same position in the 
        /// sort order as the other object.
        /// </summary>
        /// <typeparam name="TOther">The type of the other applied demand value to compare.</typeparam>
        /// <param name="other">The other applied demand value to compare.</param>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared. The return value has these meanings: 
        /// <list type="bullet">
        /// <item>Value Meaning Less than zero This instance precedes <paramref name="other" /> in the sort order. </item>
        /// <item>Zero This instance occurs in the same position in the sort order as <paramref name="other" />. </item>
        /// <item>Greater than zero This instance follows <paramref name="other" /> in the sort order.</item>
        /// </list>
        /// </returns>
        public int? CompareTo<TOther>(AppliedDemandValue<TOther> other) where TOther : AppliedDemandValue<TOther>, new()
        {
            // If types are different, have no idea
            if (Type != other.Type)
                return null;

            // If the values are both null, they're equal, sure, but c'mon, not
            //  sure what that should mean, NULL that sucker.
            if (Value == null && other.Value == null)
                return null;
            
            switch (Type)
            {
                case DemandValueType.Text:
                    // No way to compare text in this capacity, always return 0
                    return null;
                case DemandValueType.Boolean:
                    // If they're the same value, then 
                    if (Applicable == other.Applicable)
                        return 0;
                    if (Applicable == true)
                        return 1;
                    return -1;
                case DemandValueType.Value:
                    if (DemandItem == null || other.DemandItem == null)
                        return null;
                    return DemandItem.Value.CompareTo(other.DemandItem.Value);
                case DemandValueType.Schedule:
                    if (Schedule == null || other.Schedule == null)
                        return null;
                    return Schedule.CompareTo(other.Schedule);
                default:
                    return null;
            }
        }
    }
}
