﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Jobs
{
    [Serializable]
    public class EmployeeJob : BaseEmployeeNumberEntity<EmployeeJob>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeJob"/> class.
        /// </summary>
        public EmployeeJob() : base()
        {
            Dates = DateRange.Null;
            Status = new UserStamp<AdjudicationStatus, JobDenialReason?>(AdjudicationStatus.Pending, null, null, null);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeJob"/> class.
        /// </summary>
        /// <param name="job">The job.</param>
        public EmployeeJob(Job job) : this()
        {
            Job = job;
            if (job != null)
            {
                CustomerId = job.CustomerId;
                EmployerId = job.EmployerId;
                JobCode = job.Code;
                JobName = job.Name;
                JobTitle = job.Name;
            }
        }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public virtual UserStamp<AdjudicationStatus, JobDenialReason?> Status { get; set; }

        /// <summary>
        /// Gets or sets the job code.
        /// </summary>
        /// <value>
        /// The job code.
        /// </value>
        public virtual string JobCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the job.
        /// </summary>
        /// <value>
        /// The name of the job.
        /// </value>
        public string JobName { get; set; }

        /// <summary>
        /// Gets or sets the optional case identifier if this job was assigned in relation to an active case.
        /// </summary>
        /// <value>
        /// The optional case identifier.
        /// </value>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the optional case number.
        /// </summary>
        /// <value>
        /// The case number.
        /// </value>
        [BsonIgnoreIfNull]
        public string CaseNumber { get; set; }

        /// <summary>
        /// Gets or sets the dates this job is applicable.
        /// </summary>
        /// <value>
        /// The dates.
        /// </value>
        [BsonIgnoreIfDefault]
        [BsonIgnoreIfNull]
        public virtual DateRange Dates { get; set; }

        [BsonIgnore]
        public bool IsTemporary
        {
            get
            {
                if (Dates != null && Dates.EndDate.HasValue)
                    return true;

                return false;
            }
        }

        /// <summary>
        /// Gets or sets the job title. May be <c>null</c> or match the name of the job itself.
        /// </summary>
        /// <value>
        /// The job title.
        /// </value>
        public string JobTitle { get; set; }

        /// <summary>
        /// Gets or sets the office location organization code.
        /// </summary>
        /// <value>
        /// The office location code.
        /// </value>
        public virtual string OfficeLocationCode { get; set; }

        /// <summary>
        /// Gets or sets the job position.
        /// </summary>
        /// <value>
        /// Job position.
        /// </value>
        public virtual int? JobPosition { get; set; }

        /// <summary>
        /// Gets or sets the organization by reference using the Organization Code.
        /// </summary>
        /// <value>
        /// The organization.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public Organization OfficeLocation
        {
            get
            {
                return GetReferenceValue<Organization>(OfficeLocationCode, s => s.Code, s => Organization.GetByCode(s, CustomerId, EmployerId));
            }
            set
            {
                OfficeLocationCode = SetReferenceValue<Organization>(value, e => e.Code);
            }
        }

        /// <summary>
        /// Gets or sets the job.
        /// </summary>
        /// <value>
        /// The job.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public Job Job
        {
            get { return GetReferenceValue<Job>(JobCode, j => j.Code, j => Job.GetByCode(j, CustomerId, EmployerId)); }
            set { JobCode = SetReferenceValue(value, j => j.Code); }
        }

        /// <summary>
        /// Gets or sets the case.
        /// </summary>
        /// <value>
        /// The case.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public Case Case
        {
            get { return GetReferenceValue<Case>(CaseId); }
            set { CaseId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Called before saving
        /// </summary>
        /// <returns>
        /// A value indicating whether or not the save should proceed (not canceled).
        /// </returns>
        protected override bool OnSaving()
        {
            if (string.IsNullOrWhiteSpace(CaseNumber) && !string.IsNullOrWhiteSpace(CaseId) && Case != null)
                CaseNumber = Case.CaseNumber;

            return base.OnSaving();
        }
    }
}
