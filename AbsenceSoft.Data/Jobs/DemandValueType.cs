﻿using System;

namespace AbsenceSoft.Data.Jobs
{
    /// <summary>
    /// Enumerates the possible physical demand value types.
    /// </summary>
    [Serializable]
    public enum DemandValueType : int
    {
        /// <summary>
        /// Represents a non-quantifiable demand type that simply collects a free-form plain text response and has no intrinsic value
        /// other than "has" or "does not have"; so it's like <c>DemandClassType.Boolean</c> however the yes/no is implicit and instead
        /// a plain text answer is collected, and the fact that it's filled out means Yes/True.
        /// </summary>
        Text = 0,
        /// <summary>
        /// Represents a demand type that collects either a Yes or No response (as in, is this required or restricted, yes/no).
        /// </summary>
        Boolean = 1,
        /// <summary>
        /// Represents a demand class that the value of selection would indicate the minimum requirement. Demand types
        /// with this value type are displayed, collected and aggregated from lowest to highest in value.
        /// </summary>
        Value = 2,
        /// <summary>
        /// Represents a non-quantifiable demand type that needs to collect a work schedule requirements. For a job requirement, this is a minimum
        /// requirement, for a work restriction this is the maximum allowed. Using the Intermittent Certification logic, we can compare the
        /// employee's current work schedule against this restriction or job requirement and determine eligibility for this demand type.
        /// </summary>
        /// <remarks><para>This is basically the same as collecting an Intermittent Certification and could even be used to store the
        /// intermittent certification.</para></remarks>
        Schedule = 3
    }
}
