﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Jobs
{
    [Serializable]
    public class WorkRestrictionValue : AppliedDemandValue<WorkRestrictionValue>
    {
        // Work Restriction Specific Stuff goes here. Trust me, there will be stuff here.
    }
}
