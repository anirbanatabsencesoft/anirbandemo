﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Jobs
{
    [Serializable]
    public class JobRestrictedNecessity : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="JobRestrictedNecessity"/> class.
        /// </summary>
        public JobRestrictedNecessity() : base() { }

        /// <summary>
        /// Gets or sets the necessity id.
        /// </summary>
        /// <value>
        /// The necessity id.
        /// </value>
        [BsonRepresentation(BsonType.ObjectId)]
        public string NecessityId { get; set; }

        /// <summary>
        /// Gets or sets the reason why this is restricted for the job it is applied to.
        /// </summary>
        /// <value>
        /// The reason.
        /// </value>
        public string Reason { get; set; }

        /// <summary>
        /// Gets or sets the necessity.
        /// </summary>
        /// <value>
        /// The necessity.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public Necessity Necessity
        {
            get { return GetReferenceValue<Necessity>(NecessityId); }
            set { NecessityId = SetReferenceValue(value); }
        }
    }
}
