﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Jobs
{
    [Serializable]
    public enum JobDenialReason : int
    {
        Other = 0,
        RestrictionsConflictWithTasks = 1,
        BodyMechanics = 2,
        NoWork = 3,
        NotTrained = 4
    }
}
