﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Reporting
{
    [Serializable]
    public class SavedReportField : BaseNonEntity
    {

        public string Name { get; set; }

        [BsonIgnoreIfNull]
        public int? Order { get; set; }
    }
}
