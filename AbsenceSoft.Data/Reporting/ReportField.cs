﻿using AbsenceSoft.Data.Audit;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Data.Reporting
{
    /// <summary>
    /// A polymorphic representation of a report field both used for field metadata for building reports AND for
    /// storing field selections on an actual report definition.
    /// </summary>
    [Serializable, AuditClassNo]
    public class ReportField : BaseEntity<ReportField>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReportField"/> class.
        /// </summary>
        public ReportField()
        {
        }

        /// <summary>
        /// Gets or sets the customer id for the entity.
        /// </summary>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string CustomerId { get; set; }

        /// <summary>
        /// The column name OR value expression that wraps a column.
        /// May include the expression needed to extract a field from a conmplex type column
        /// such as JSON, Array, etc. These types of fields, like Custom Fields, are
        /// dynamic and we can't have dynamic columns in a view, SO, we have to inject
        /// special expressions to extract the information in our select statement AND
        /// in our filters.
        /// </summary>
        /// <example>
        /// cust_id
        /// case_assigned_to_name
        /// json_extract_path_text('{"f2":{"f3":1},"f4":{"f5":99,"f6":"foo"}}', 'f4', 'f6')
        /// '{"f2":{"f3":1},"f4":{"f5":99,"f6":"foo"}}'::json#>>'{f4,f6}'
        /// </example>
        /// <value>
        /// The name of the column OR column level expression.
        /// </value>
        [BsonRequired]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the name as displayed in the UI and what is built with the "as" keyword in the query output.
        /// </summary>
        /// <value>
        /// The display.
        /// </value>
        [BsonIgnoreIfNull]
        public string Display { get; set; }

        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        /// <value>
        /// The category.
        /// </value>
        [BsonIgnoreIfNull]
        public string Category { get; set; }

        /// <summary>
        /// Gets or sets the order the field appears in either in the report type or can be switched by the report designer.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        public int Order { get; set; }

        /// <summary>
        /// Gets or sets the type of the data.
        /// </summary>
        /// <value>
        /// The type of the data.
        /// </value>
        public DbDataType DataType { get; set; }

        /// <summary>
        /// Gets or sets the help text to be shown on hover for the prompt of this field
        /// in proper usage. This may not be an easy concept, so this is important.
        /// </summary>
        [BsonIgnoreIfNull]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the format; generally selectable based on the data type.
        /// </summary>
        /// <value>
        /// The format.
        /// </value>
        [BsonIgnoreIfNull]
        public string Format { get; set; }

        /// <summary>
        /// Gets or sets the translate boolean flag as to whether to remove spaces from the resulting format result.
        /// Often used for number formats where there may be pattern white-space after formatting to_char.
        /// </summary>
        /// <value>
        /// The translate flag.
        /// </value>
        /// <remarks>
        /// see: translate() function in PostgreSQL docs. This will use translate(..., ' ', '').
        /// </remarks>
        [BsonIgnoreIfNull]
        public bool? Translate { get; set; }

        /// <summary>
        /// Gets or sets the filter template for setting a filter for this field.
        /// </summary>
        /// <value>
        /// The filter template.
        /// </value>
        [BsonIgnoreIfNull]
        public ReportFilter Filter { get; set; }

        /// <summary>
        /// Gets or sets the required permission.
        /// </summary>
        /// <value>
        /// The required permission.
        /// </value>
        [BsonIgnoreIfNull]
        public string RequiredPermission { get; set; }

        /// <summary>
        /// Gets or sets the entity's customer.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Customer Customer
        {
            get { return GetReferenceValue<Customer>(CustomerId); }
            set { CustomerId = SetReferenceValue(value); }
        }

        #region Static Methods

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching.
        /// </summary>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public new static IQueryable<ReportField> AsQueryable()
        {
            return User.Current == null
                ? Repository.AsQueryable()
                : AsQueryable(User.Current);
        }

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching while automatically
        /// applying the appropriate data access filters onto it automagically.
        /// </summary>
        /// <param name="user">The user of which to build data visibility and data access from</param>
        /// <param name="permission">The permission that the user should have access to when filtering the data parameters</param>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static IQueryable<ReportField> AsQueryable(User user, Permission permission)
        {
            return AsQueryable(user, permission.Id);
        }

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching while automatically
        /// applying the appropriate data access filters onto it automagically.
        /// </summary>
        /// <param name="user">The user of which to build data visibility and data access from</param>
        /// <param name="permission">The permission that the user should have access to when filtering the data parameters</param>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static IQueryable<ReportField> AsQueryable(User user, string permission = null)
        {
            var query = Repository.AsQueryable();

            // If no user, then duh, no dice sucker, empty list it
            if (user == null) return new List<ReportField>(0).AsQueryable();

            // User must belong to a customer, otherwise empty list it
            if (string.IsNullOrWhiteSpace(user.CustomerId))
                return query.Where(e => e.CustomerId == null);

            // Ensure the customer matches
            query = query.Where(e => e.CustomerId == user.CustomerId || e.CustomerId == null);

            // Ensure the user has permissions for this field
            if (!user.Roles.Contains(Role.SystemAdministrator.Id))
            {
                var permissions = User.Permissions.GetProjectedPermissions(user);
                permissions.Insert(0, null);
                query = query.Where(e => permissions.Contains(e.RequiredPermission));
            }

            return query;
        }//end: AsQueryable

        /// <summary>
        /// Gets the entity by its Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>The entity found by the Id or <c>null</c> if no entity was found by that id.</returns>
        public new static ReportField GetById(string id)
        {
            using (new InstrumentationContext("{0}.GetById", ClassTypeName))
            {
                if (string.IsNullOrWhiteSpace(id))
                    return null;

                var val = Repository.GetById(id.ToLowerInvariant());
                if (val == null)
                    return null;
                // If no user to check against, just return the value
                if (User.Current == null)
                    return val;
                // If the field requires permissions, ensure user has those permissions
                if (!string.IsNullOrWhiteSpace(val.RequiredPermission))
                    if (!User.Current.Roles.Contains(Role.SystemAdministrator.Id))
                        if (!User.Permissions.ProjectedPermissions.Contains(val.RequiredPermission))
                            return null;
                // If no customer to measure against, just return the value
                if (string.IsNullOrWhiteSpace(val.CustomerId))
                    return val;
                // Ensure the customer matches that of the current user's
                if (val.CustomerId != User.Current.CustomerId)
                    return default(ReportField);
                // Finally, return the value
                return val;
            }
        }

        #endregion
    }
}
