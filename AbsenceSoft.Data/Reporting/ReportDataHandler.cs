﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using Insight.Database;
using AbsenceSoft.Common;
using AbsenceSoft.Common.Config;
using System.Data;
using NpgsqlTypes;

namespace AbsenceSoft.Data.Reporting
{
    [Serializable]
    /// <summary>
    /// Retrieves data from query for report operations
    /// </summary>
    public class ReportDataHandler
    {


        /// <summary>
        /// Returns the data from query
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public IList<dynamic> GetReportData(string sql, object parameters = null)
        {
            using(var conn = new NpgsqlConnection(ConnectionStrings.DataWarehouse))
            {
                return conn.QuerySql<dynamic>(sql, parameters, commandBehavior: System.Data.CommandBehavior.SequentialAccess);
            }
        }

        /// <summary>
        /// Report data with strongly types class instead of expando
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public IList<T> GetReportData<T>(string sql, object parameters = null)
        {
            using (var conn = new NpgsqlConnection(ConnectionStrings.DataWarehouse))
            {
                return conn.QuerySql<T>(sql, parameters);
            }
        }

        /// <summary>
        /// Inject the processing method directly to access data reader
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="action"></param>
        public void GetAndProcessData(string sql, object parameters = null, Action<NpgsqlDataReader> action = null)
        {
            Dictionary<string, object> dict = null;
            if(parameters != null)
                dict = ((object)parameters)
                               .GetType()
                               .GetProperties()
                               .ToDictionary(p => p.Name, p => p.GetValue(parameters));

            GetAndProcessData(sql, dict, action);
        }

        /// <summary>
        ///  Inject the processing method directly to access data reader
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="dict"></param>
        /// <param name="action"></param>
        public void GetAndProcessData(string sql, Dictionary<string, object> dict = null, Action<NpgsqlDataReader> action = null)
        {
            IList<NpgsqlParameter> parameters = null;
            if(dict != null)
            {
                parameters = new List<NpgsqlParameter>();
                foreach (var i in dict)
                    parameters.Add(new NpgsqlParameter(i.Key, i.Value));
            }

            GetAndProcessData(sql, parameters, action);
        }

        /// <summary>
        /// Inject the processing method directly to access data reader
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="action"></param>
        public void GetAndProcessData(string sql, IList<NpgsqlParameter> parameters = null, Action<NpgsqlDataReader> action = null)
        {
            if (action != null)
            {
                using (var conn = new NpgsqlConnection(ConnectionStrings.DataWarehouse))
                {
                    conn.Open();
                    using (var cmd = new NpgsqlCommand(sql, conn))
                    {
                        if (parameters != null)
                        {
                            foreach (var p in parameters)
                                cmd.Parameters.Add(p);
                        }

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                                action(reader);
                        }
                    }
                    conn.Close();
                }
            }
        }
    }
}
