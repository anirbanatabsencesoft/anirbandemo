﻿using System;

namespace AbsenceSoft.Data.Reporting
{
    /// <summary>
    /// Represents a date dimension for reporting purposes and is used to group all date fields together into a "smart" class.
    /// </summary>
    [Serializable]
    public class ReportDate
    {
        public ReportDate() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportDate"/> class.
        /// </summary>
        /// <param name="date">The date.</param>
        public ReportDate(DateTime? date)
        {
            Date = date;
            if (date.HasValue)
            {
                Year = date.Value.Year;
                Quarter = string.Format("Q{0}, {1}", date.GetFiscalQuarter(), Year);
                Month = date.Value.ToString("MMMM, yyyy");
                Week = date.GetIso8601WeekOfYear();
                WeekDay = date.Value.DayOfWeek.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        /// <value>
        /// The date.
        /// </value>
        public DateTime? Date { get; set; }

        /// <summary>
        /// Gets or sets the year dimension of the current date.
        /// </summary>
        /// <value>
        /// The year dimension of the current date.
        /// </value>
        public int? Year { get; set; }

        /// <summary>
        /// Gets or sets the quarter dimension of the current date.
        /// </summary>
        /// <value>
        /// The quarter dimension of the current date.
        /// </value>
        public string Quarter { get; set; }

        /// <summary>
        /// Gets or sets the month dimension of the current date.
        /// </summary>
        /// <value>
        /// The month dimension of the current date.
        /// </value>
        public string Month { get; set; }

        /// <summary>
        /// Gets or sets the week.
        /// </summary>
        /// <value>
        /// The week.
        /// </value>
        public int? Week { get; set; }

        /// <summary>
        /// Gets or sets the week day.
        /// </summary>
        /// <value>
        /// The week day.
        /// </value>
        public string WeekDay { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return Date.ToUIString();
        }
    }
}
