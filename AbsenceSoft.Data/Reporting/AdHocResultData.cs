﻿using System;
using MongoDB.Bson;

namespace AbsenceSoft.Data.Reporting
{
    [Serializable]
    public class AdHocResultData : DynamicAwesome
    {
        public AdHocResultData()
        {
            
        }

        public AdHocResultData(BsonDocument seed) : base(seed)
        {
            
        }

        public AdHocResultData(DynamicAwesome dynamicAwesome) : base(dynamicAwesome.AsBsonDocument)
        {
            
        }
    }
}
