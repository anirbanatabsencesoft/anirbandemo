﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace AbsenceSoft.Data.Reporting
{
    [Serializable, BsonIgnoreExtraElements(true, Inherited = true)]
    public class ReportFilter
    {
        #region Privates

        /// <summary>
        /// Stores a static list of enumeration types within the AppDomain that may be used for evaluating LeaveOfAbsence expresions.
        /// </summary>
        private static List<Type> enumTypes = Assembly.GetAssembly(typeof(AbsenceSoft.Data.BaseEntity<>)).GetTypes().Where(t => t.IsEnum).ToList();

        /// <summary>
        /// Gets a read-only enumerable collection of enum types that this evaluator knows about.
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<Type> GetEnumTypes() { return enumTypes.AsReadOnly(); }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportFilter"/> class.
        /// </summary>
        public ReportFilter()
        {
            ControlType = Enums.ControlType.ReadOnlyText;
            Operators = new Dictionary<string, string>();
        }

        /// <summary>
        /// The column name OR value expression that wraps a column.
        /// May include the expression needed to extract a field from a conmplex type column
        /// such as JSON, Array, etc. These types of fields, like Custom Fields, are
        /// dynamic and we can't have dynamic columns in a view, SO, we have to inject
        /// special expressions to extract the information in our select statement AND
        /// in our filters.
        /// </summary>
        /// <example>
        /// cust_id
        /// case_assigned_to_name
        /// json_extract_path_text('{"f2":{"f3":1},"f4":{"f5":99,"f6":"foo"}}', 'f4', 'f6')
        /// '{"f2":{"f3":1},"f4":{"f5":99,"f6":"foo"}}'::json#>>'{f4,f6}'
        /// </example>
        /// <value>
        /// The name of the column OR column level expression.
        /// </value>
        [BsonRequired]
        public string FieldName { get; set; }

        /// <summary>
        /// Gets or sets the textual label or prompt used to gather the criteria, e.g. "First Name".
        /// </summary>
        /// <value>
        /// The prompt.
        /// </value>
        public string Prompt { get; set; }

        /// <summary>
        /// Gets or sets the type of control for displaying the expression prompt in the UI.
        /// </summary>
        /// <value>
        /// The type of the control.
        /// </value>
        public ControlType ControlType { get; set; }

        /// <summary>
        /// Gets or set a hashtable of available expression operators that are allowed for this field.
        /// </summary>
        /// <value>
        /// The operators.
        /// </value>
        public Dictionary<string, string> Operators { get; set; }

        /// <summary>
        /// Gets or sets the operator.
        /// </summary>
        /// <value>
        /// The operator.
        /// </value>
        public string Operator { get; set; }

        /// <summary>
        /// Gets or sets the list of options, if any, for the expression prompt in the UI to choose from
        /// for select types, checkbox lists, etc.
        /// </summary>
        /// <value>
        /// The options.
        /// </value>
        public List<ReportFilterOption> Options { get; set; }

        /// <summary>
        /// Gets or sets the name of the type.
        /// </summary>
        /// <value>
        /// The name of the type.
        /// </value>
        public string TypeName { get; set; }

        /// <summary>
        /// Gets or sets the value (either default value or populated by the user, etc.) for this criteria item.
        /// This value may be a Date, a string, a number, a Boolean, etc.
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// Gets or sets the values when there is a multi-select for this criteria item. This should be
        /// an array of objects such as Dates, strings, numbers, Booleans, etc.
        /// </summary>
        /// <value>
        /// The values.
        /// </value>
        public List<object> Values { get; set; }

        /// <summary>
        /// Gets the criteria value cast as type T.
        /// </summary>
        /// <typeparam name="T">The type of criteria value expected to be cast and return as</typeparam>
        /// <returns>The current value cast as type T or default(T).</returns>
        public T ValueAs<T>()
        {
            return Convert<T>(Value);
        } // ValueAs<T>

        /// <summary>
        /// Gets the criteria values cast as a list of type T.
        /// </summary>
        /// <typeparam name="T">The type of criteria value expected to be cast and return as</typeparam>
        /// <returns>The current value cast as type T or an empty list.</returns>
        public List<T> ValuesAs<T>()
        {
            return Values == null ? new List<T>(0) : Values.Select(v => Convert<T>(v)).ToList();
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            if (Value == null && (Values == null || !Values.Any()))
                return string.Empty;

            string niceValue = null;
            if (Options != null && Options.Any())
            {
                Func<object, string> getNiceValue = GetNiceValueFunc();
                if (Values != null && Values.Any())
                    niceValue = Values.Count == 1 ? getNiceValue(Values[0]) : string.Join("' or '", Values.Select(v => getNiceValue(v) ?? v));
                else if (Value != null)
                    niceValue = getNiceValue(Value);
            }

            var res=ToStringControlTypeNiceValue(ref niceValue);
            if (res!=null && !res.Equals(niceValue))
            {
                return res;
            }

            if (string.IsNullOrWhiteSpace(niceValue) && Value is string && string.IsNullOrWhiteSpace(Value.ToString()))
                return string.Empty;

            return string.Format("{0} {1} '{2}'", Prompt, ReportOperator.GetName(Operator) ?? "is", niceValue ?? Value);
        }

        private Func<object, string> GetNiceValueFunc()
        {
           return new Func<object, string>(v =>
            {
                if (v == null)
                {
                    return string.Empty;
                }
                var opt = Options.FirstOrDefault(o => v.Equals(o.Value));
                if (opt == null)
                {
                    opt = Options.FirstOrDefault(o => v.ToString() == o.Value.ToString());
                }
                if (opt != null)
                {
                    return opt.Text;
                }
                return string.Empty;
            });
        }
        private string ToStringControlTypeNiceValue(ref string niceValue)
        {
            switch (ControlType)
            {
                case ControlType.Date:
                    niceValue = System.Convert.ToDateTime(Value).Date.ToString("MM/dd/yyyy");
                    break;
                case ControlType.Minutes:
                    int min = System.Convert.ToInt32(Value);
                    niceValue = min.ToFriendlyTime();
                    break;
                case ControlType.Numeric:
                    niceValue = Value.ToString();
                    break;
                case ControlType.CheckBox:
                    if (Value.Equals(true))
                    {
                        return string.Concat("is ", Prompt);
                    }
                    else
                    {
                        return string.Concat("is not ", Prompt);
                    }                    
                case ControlType.CheckBoxList:
                    var res= ToStringControlTypeCHBL(ref niceValue);
                    if (!res.Equals(niceValue))
                    {
                        return res;
                    }
                    break;
            }
            return niceValue;
        }
        private string ToStringControlTypeCHBL(ref string niceValue)
        {
            if (Values != null && Values.Any())
            {
                return string.Concat(
                    Prompt, " ",
                    Values.Count == 1 ? "is '" : (ReportOperator.GetName(Operator) ?? "is ('"),
                    niceValue ?? (Values.Count == 1 ? Values[0].ToString() : string.Join("' or '", Values)),
                    Values.Count == 1 ? "'" : "')");
            }
            else
            {
                niceValue= niceValue ?? (Value == null ? "" : Value.ToString());
                return niceValue;
            }
        }

        /// <summary>
        /// Converts the specified value to type of T.
        /// </summary>
        /// <typeparam name="T">The type to convert the value to</typeparam>
        /// <param name="v">The value to convert to type of T.</param>
        /// <returns>A converted version of v or default of T.</returns>

        private T Convert<T>(object v)
        {
            // If the value is null, return the default of type T
            if (v == null)
                return default(T);

            // If the type matches, just type-cast it and return the result
            if (v is T)
                return (T)v;

            // Get our type of T so we can use it for other tests and ensure .NET reflection caches the result
            //  as an instance on our memory heap for performance reasons
            Type typeOfT = typeof(T);

            // If it is a nullable type, we have to get the base type
            if (typeOfT.IsGenericType && typeOfT.GetGenericTypeDefinition() == typeof(Nullable<>))
                typeOfT = typeOfT.GetGenericArguments().First();

            // If the type matches, just type-cast it and return the result
            if (v.GetType() == typeOfT) return (T)v;

            // If it is an enum, we can't explicitly use Convert.ChangeType, so we have to use the Enum helpers
            //  for this instead.

            if (typeOfT.IsEnum)
            {
                return ConvertIsEnum<T>(typeOfT, v);
            }
            // If it is an assignable type, then use the ChangeType method.
            if (typeOfT.IsInstanceOfType(v))
                return (T)System.Convert.ChangeType(v, typeOfT);

            if (typeOfT == typeof(int))
            {
                int intValue;
                if (int.TryParse(System.Convert.ToString(v), out intValue))
                {
                    return (T)(object)intValue;
                }
            }

            // Can't figure anything else out, so just return the default of T again. Maybe this
            //  should attempt to throw an exception instead, will need to look into that.
            return default(T);
        }

        private T ConvertIsEnum<T>(Type typeOfT, object v)
        {
            if (v is string && int.TryParse(v.ToString(), out int iVal))
            {
                if (!Enum.IsDefined(typeOfT, iVal))
                    return default(T);
                return (T)Enum.ToObject(typeOfT, iVal);
            }
            else if (v is long)
                v = System.Convert.ToInt32(v);

            if (!Enum.IsDefined(typeOfT, v))
                return default(T);
            return (T)Enum.ToObject(typeOfT, v);
        }

        /// <summary>
        /// Gets the enum options.
        /// </summary>
        /// <typeparam name="TEnum">The type of the enum.</typeparam>
        /// <returns>A list of report filter options based on an enumeration.</returns>
        /// <exception cref="System.ArgumentException">TEnum must be an enumerated type</exception>
        public static List<ReportFilterOption> GetEnumOptions<TEnum>() where TEnum : struct, IConvertible
        {
            if (!typeof(TEnum).IsEnum)
                throw new ArgumentException("TEnum must be an enumerated type");

            return Enum
                .GetValues(typeof(TEnum))
                .OfType<TEnum>()
                .Select(m => new ReportFilterOption()
                {
                    Value = m,
                    Text = m.ToString().SplitCamelCaseString()
                })
                .ToList();
        }
        
        public bool KeepOptionAll { get; set; }

        /// <summary>
        /// Gets or sets the tweener text that is used to separate the name and value as a representative
        /// plain English phrase.
        /// </summary>
        public string Tweener { get; set; }

        /// <summary>
        /// Gets or sets whether or not this field is required by the user to be populated in order
        /// to query/run the report.
        /// </summary>
        public bool Required { get; set; }
    }
}
