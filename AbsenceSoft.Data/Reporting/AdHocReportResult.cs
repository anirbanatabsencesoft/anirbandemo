﻿using System;
using System.Collections.Generic;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace AbsenceSoft.Data.Reporting
{
    [Serializable]
    public class AdHocReportResult
    {
        /// <summary>
        /// Gets or sets the report name for display.
        /// </summary>
        [BsonIgnoreIfNull]
        public string Name { get; set; } 

        /// <summary>
        /// Gets or sets the type of the ad hoc report.
        /// </summary>
        /// <value>
        /// The type of the ad hoc report.
        /// </value>
        [BsonIgnoreIfNull]
        public string AdHocReportType { get; set; }

        /// <summary>
        /// Gets or sets the icon name for display.
        /// </summary>
        [BsonIgnoreIfNull]
        public string IconImage { get; set; }

        /// <summary>
        /// Gets or sets the date and time the report was originally requested to be run.
        /// </summary>
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc, Representation = BsonType.DateTime), BsonIgnoreIfDefault]
        public DateTime RequestDate { get; set; }

        /// <summary>
        /// Gets or sets the user that requested the report, also used for data visibility
        /// functions.
        /// </summary>
        [BsonIgnoreIfNull]
        public User RequestedBy { get; set; }

        /// <summary>
        /// The sql query used to create the report.
        /// </summary>
        [BsonIgnoreIfNull]
        public string Query { get; set; }

        /// <summary>
        /// Gets or sets the total time it took to complete in miliseconds.
        /// </summary>
        [BsonIgnoreIfNull]
        public double? CompletedIn { get; set; }

        /// <summary>
        /// Gets a value whether or not this report request was successfully run.
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Gets or sets an error message for the report run request.
        /// </summary>
        [BsonIgnoreIfNull]
        public string Error { get; set; }

        /// <summary>
        /// Gets or sets the list of result data items in the report results. If this is a grouped
        /// report then this collection will be <c>null</c>.
        /// </summary>
        [BsonIgnoreIfNull]
        public List<AdHocResultData> Items { get; set; }

        /// <summary>
        /// The number of records in the report.
        /// </summary>
        [BsonIgnoreIfNull]
        public long? RecordCount { get; set; }    
    }
}
