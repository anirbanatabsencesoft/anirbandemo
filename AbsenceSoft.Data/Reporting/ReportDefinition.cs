﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Data.Reporting
{
    [Serializable]
    public class ReportDefinition : BaseEntity<ReportDefinition>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReportDefinition"/> class.
        /// </summary>
        public ReportDefinition()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportDefinition"/> class.
        /// </summary>
        /// <param name="type">The type.</param>
        public ReportDefinition(ReportType type) : this()
        {
            ReportType = type;
            if (type != null)
                Name = type.Name;
        }

        /// <summary>
        /// Gets or sets the customer id for the entity.
        /// </summary>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the user identifier for the owner of the report (if private for the current user).
        /// This is optional, if the user is sharing the report definition, then this will be null.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [BsonIgnoreIfNull]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the type of the report.
        /// </summary>
        /// <value>
        /// The type of the report.
        /// </value>
        [BsonIgnoreIfNull]
        public ReportType ReportType { get; set; }

        /// <summary>
        /// Gets or sets the fields.
        /// </summary>
        /// <value>
        /// The fields.
        /// </value>
        [BsonIgnoreIfNull]
        public List<SavedReportField> Fields { get; set; }

        /// <summary>
        /// Gets or sets the filters.
        /// </summary>
        /// <value>
        /// The filters.
        /// </value>
        [BsonIgnoreIfNull]
        public List<ReportFilter> Filters { get; set; }

        /// <summary>
        /// Gets or sets the entity's customer.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Customer Customer
        {
            get { return GetReferenceValue<Customer>(CustomerId); }
            set { CustomerId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the user who owns this report.
        /// </summary>
        /// <value>
        /// The user.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public User User
        {
            get { return GetReferenceValue<User>(UserId); }
            set { UserId = SetReferenceValue(value); }
        }

        #region Static Methods

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching.
        /// </summary>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public new static IQueryable<ReportDefinition> AsQueryable()
        {
            return User.Current == null
                ? Repository.AsQueryable()
                : AsQueryable(User.Current);
        }

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching while automatically
        /// applying the appropriate data access filters onto it automagically.
        /// </summary>
        /// <param name="user">The user of which to build data visibility and data access from</param>
        /// <param name="permission">The permission that the user should have access to when filtering the data parameters</param>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static IQueryable<ReportDefinition> AsQueryable(User user, Permission permission)
        {
            return AsQueryable(user, permission.Id);
        }

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching while automatically
        /// applying the appropriate data access filters onto it automagically.
        /// </summary>
        /// <param name="user">The user of which to build data visibility and data access from</param>
        /// <param name="permission">The permission that the user should have access to when filtering the data parameters</param>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static IQueryable<ReportDefinition> AsQueryable(User user, string permission = null)
        {
            var query = Repository.AsQueryable();

            // If no user, then duh, no dice sucker, empty list it
            if (user == null) return new List<ReportDefinition>(0).AsQueryable();

            // Ensure the user only gets their own or global customer ones
            query = query.Where(e => e.UserId == null || e.UserId == user.Id);
            
            // User must belong to a customer, otherwise empty list it
            if (string.IsNullOrWhiteSpace(user.CustomerId))
                return query.Where(e => e.CustomerId == null);

            // Ensure the customer matches
            query = query.Where(e => e.CustomerId == user.CustomerId || e.CustomerId == null);

            return query;
        }//end: AsQueryable

        /// <summary>
        /// Gets the entity by its Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>The entity found by the Id or <c>null</c> if no entity was found by that id.</returns>
        public new static ReportDefinition GetById(string id)
        {
            using (new InstrumentationContext("{0}.GetById", ClassTypeName))
            {
                if (string.IsNullOrWhiteSpace(id))
                    return default(ReportDefinition);

                var val = Repository.GetById(id.ToLowerInvariant());
                if (val == null)
                    return null;
                if (User.Current == null)
                    return val;
                if (!string.IsNullOrWhiteSpace(val.UserId) && val.UserId != User.Current.Id)
                    return default(ReportDefinition);
                if (string.IsNullOrWhiteSpace(val.CustomerId))
                    return val;
                if (val.CustomerId != User.Current.CustomerId)
                    return default(ReportDefinition);
                return val;
            }
        }

        #endregion
    }
}
