﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace AbsenceSoft.Data.Reporting
{
    [Serializable, BsonIgnoreExtraElements(true, Inherited = true)]
    public class ReportFilterOption
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReportFilterOption"/> class.
        /// </summary>
        public ReportFilterOption() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportFilterOption"/> class.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="text">The text.</param>
        public ReportFilterOption(string value, string text) : this()
        {
            Text = text;
            Value = value;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportFilterOption"/> class.
        /// </summary>
        /// <param name="textAndValue">The text which represents the text and value (they are the same).</param>
        public ReportFilterOption(string textAndValue) : this(textAndValue, textAndValue) { }

        /// <summary>
        /// Gets or sets the name of the option (display text)
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the value of the option (value text)
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// Gets or sets the optional group text (for grouped checkbox lists or grouped select lists, etc.)
        /// </summary>
        public string GroupText { get; set; }
    }
}
