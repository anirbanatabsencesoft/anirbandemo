﻿using System.Collections.Generic;

namespace AbsenceSoft.Data.Reporting
{
    /// <summary>
    /// Represents Report Operator const values and associative stuff.
    /// </summary>
    public static class ReportOperator
    {
        /// <summary>
        /// The equals
        /// </summary>
        public const string Equals = "=";

        /// <summary>
        /// The not equals
        /// </summary>
        public const string NotEquals = "<>";

        /// <summary>
        /// The less than
        /// </summary>
        public const string LessThan = "<";

        /// <summary>
        /// The less than equal to
        /// </summary>
        public const string LessThanEquals = "<=";

        /// <summary>
        /// The greater than
        /// </summary>
        public const string GreaterThan = ">";

        /// <summary>
        /// The greater than equal to
        /// </summary>
        public const string GreaterThanEquals = ">=";

        /// <summary>
        /// The begins with
        /// </summary>
        public const string BeginsWith = "~|";

        /// <summary>
        /// The ends with
        /// </summary>
        public const string EndsWith = "|~";

        /// <summary>
        /// The contains
        /// </summary>
        public const string Contains = "~~";

        /// <summary>
        /// Gets the name of the passed in operator.
        /// </summary>
        /// <param name="op">The operator to get the name for. Should match
        /// one of the well-known defined operators in this class.</param>
        /// <returns>A friendly name or tweener text for an operator.</returns>
        public static string GetName(string op)
        {
            switch (op)
            {
                case Equals:
                    return "equals";
                case NotEquals:
                    return "does not equal";
                case LessThan:
                    return "is less than";
                case LessThanEquals:
                    return "is less than or equals";
                case GreaterThan:
                    return "is greater than";
                case GreaterThanEquals:
                    return "is greater than or equals";
                case BeginsWith:
                    return "begins with";
                case EndsWith:
                    return "ends with";
                case Contains:
                    return "contains";
            }
            return null;
        }

        /// <summary>
        /// Defines the "Equals" operator.
        /// </summary>
        /// <param name="operators">The operators.</param>
        /// <param name="add">if set to <c>true</c> [add].</param>
        /// <returns>The extended instance of Dictionary for a fluid interface.</returns>
        public static Dictionary<string, string> OpEquals(this Dictionary<string, string> operators)
        {
            return operators.AddFluid(Equals, GetName(Equals));
        }

        /// <summary>
        /// Defines the "Does Not Equal" operator.
        /// </summary>
        /// <param name="operators">The operators.</param>
        /// <param name="add">if set to <c>true</c> [add].</param>
        /// <returns>The extended instance of Dictionary for a fluid interface.</returns>
        public static Dictionary<string, string> OpNotEqual(this Dictionary<string, string> operators)
        {
            return operators.AddFluid(NotEquals, GetName(NotEquals));
        }

        /// <summary>
        /// Defines the "Less Than" operator.
        /// </summary>
        /// <param name="operators">The operators.</param>
        /// <param name="add">if set to <c>true</c> [add].</param>
        /// <returns>The extended instance of Dictionary for a fluid interface.</returns>
        public static Dictionary<string, string> OpLessThan(this Dictionary<string, string> operators)
        {
            return operators.AddFluid(LessThan, GetName(LessThan));
        }

        /// <summary>
        /// Defines the "Less Than Or Equal To" operator.
        /// </summary>
        /// <param name="operators">The operators.</param>
        /// <param name="add">if set to <c>true</c> [add].</param>
        /// <returns>The extended instance of Dictionary for a fluid interface.</returns>
        public static Dictionary<string, string> OpLessThanEqualTo(this Dictionary<string, string> operators)
        {
            return operators.AddFluid(LessThanEquals, GetName(LessThanEquals));
        }

        /// <summary>
        /// Defines the "Greater Than" operator.
        /// </summary>
        /// <param name="operators">The operators.</param>
        /// <param name="add">if set to <c>true</c> [add].</param>
        /// <returns>The extended instance of Dictionary for a fluid interface.</returns>
        public static Dictionary<string, string> OpGreaterThan(this Dictionary<string, string> operators)
        {
            return operators.AddFluid(GreaterThan, GetName(GreaterThan));
        }

        /// <summary>
        /// Defines the "Greater Than Or Equal To" operator.
        /// </summary>
        /// <param name="operators">The operators.</param>
        /// <param name="add">if set to <c>true</c> [add].</param>
        /// <returns>The extended instance of Dictionary for a fluid interface.</returns>
        public static Dictionary<string, string> OpGgreaterThanEqualTo(this Dictionary<string, string> operators)
        {
            return operators.AddFluid(GreaterThanEquals, GetName(GreaterThanEquals));
        }

        /// <summary>
        /// Defines the "Begins With" operator.
        /// </summary>
        /// <param name="operators">The operators.</param>
        /// <returns>The extended instance of Dictionary for a fluid interface.</returns>
        public static Dictionary<string, string> OpBeginsWith(this Dictionary<string, string> operators)
        {
            return operators.AddFluid(BeginsWith, GetName(BeginsWith));
        }

        /// <summary>
        /// Defines the "Ends With" operator.
        /// </summary>
        /// <param name="operators">The operators.</param>
        /// <returns>The extended instance of Dictionary for a fluid interface.</returns>
        public static Dictionary<string, string> OpEndsWith(this Dictionary<string, string> operators)
        {
            return operators.AddFluid(EndsWith, GetName(EndsWith));
        }

        /// <summary>
        /// Defines the "Contains" operator.
        /// </summary>
        /// <param name="operators">The operators.</param>
        /// <returns>The extended instance of Dictionary for a fluid interface.</returns>
        public static Dictionary<string, string> OpContains(this Dictionary<string, string> operators)
        {
            return operators.AddFluid(Contains, GetName(Contains));
        }
    }
}
