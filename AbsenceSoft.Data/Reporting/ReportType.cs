﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace AbsenceSoft.Data.Reporting
{
    [Serializable]
    public class ReportType : BaseEntity<ReportType>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReportType"/> class.
        /// </summary>
        public ReportType()
        {
            Fields = new List<ReportField>();
        }

        /// <summary>
        /// Gets or sets the type of the ad hoc report.
        /// </summary>
        /// <value>
        /// The type of the ad hoc report.
        /// </value>
        [BsonIgnoreIfNull]
        public AdHocReportType? AdHocReportType { get; set; }

        /// <summary>
        /// Gets or sets the report type key.
        /// </summary>
        /// <value>
        /// The report type key.
        /// </value>
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [BsonIgnoreIfNull]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the fields.
        /// </summary>
        /// <value>
        /// The fields.
        /// </value>
        [BsonIgnore]
        public List<ReportField> Fields { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is customer specific and has a customer_id.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is customer specific and has a customer_id; otherwise, <c>false</c>.
        /// </value>
        public bool IsCustomer { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is employer specific and has a employer_id.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is employer specific and has a employer_id; otherwise, <c>false</c>.
        /// </value>
        public bool IsEmployer { get; set; }
    }
}
