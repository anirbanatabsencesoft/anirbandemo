﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Reporting
{
    [Serializable]
    public class ReportCustomerDetails
    {
        public long CustomerId { get; set; }
        public string Customer { get; set; }
        public string Employer { get; set; }
        public int EmployeeCount { get; set; }
        public int EmployeeCount6month { get; set; }
        public int EmployeeCount3month { get; set; }
        public int EmployeeCount1month { get; set; }
        public int CaseCount { get; set; }
        public int TodoCount { get; set; }
        public int AttachmentCount { get; set; }
        public int NotesCount { get; set; }
        public int ActiveUsers { get; set; }
    }
}
