﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Reporting
{
    [Serializable]
    /// <summary>
    /// Model for Summary result for group by queries
    /// </summary>
    public class SummaryResult
    {
        /// <summary>
        /// The function used for summary/aggregate
        /// </summary>
        public string FunctionName { get; set; } 

        /// <summary>
        /// The column name on which the aggregate function calculated
        /// </summary>
        public string ColumnName { get; set; }

        /// <summary>
        /// The result of the aggregate 
        /// </summary>
        public long Result { get; set; }

        /// <summary>
        /// Set the column display name
        /// </summary>
        public string DisplayName { get; set; }

    }
}
