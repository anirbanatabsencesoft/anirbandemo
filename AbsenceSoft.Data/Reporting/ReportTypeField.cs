﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Reporting
{
    [Serializable]
    public class ReportTypeField : BaseEntity<ReportTypeField>
    {
        /// <summary>
        /// Gets or sets the report type identifier.
        /// </summary>
        /// <value>
        /// The report type identifier.
        /// </value>
        [BsonRequired]
        [BsonRepresentation(BsonType.ObjectId)]
        public string ReportTypeId { get; set; }

        /// <summary>
        /// The column name OR value expression that wraps a column.
        /// May include the expression needed to extract a field from a conmplex type column
        /// such as JSON, Array, etc. These types of fields, like Custom Fields, are
        /// dynamic and we can't have dynamic columns in a view, SO, we have to inject
        /// special expressions to extract the information in our select statement AND
        /// in our filters.
        /// </summary>
        /// <example>
        /// cust_id
        /// case_assigned_to_name
        /// json_extract_path_text('{"f2":{"f3":1},"f4":{"f5":99,"f6":"foo"}}', 'f4', 'f6')
        /// '{"f2":{"f3":1},"f4":{"f5":99,"f6":"foo"}}'::json#>>'{f4,f6}'
        /// </example>
        /// <value>
        /// The name of the column OR column level expression.
        /// </value>
        [BsonRequired]
        public string FieldName { get; set; }
    }
}
