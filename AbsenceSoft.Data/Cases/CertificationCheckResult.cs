﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Cases
{
    /// <summary>
    /// The results from checking a date against a policies certifications
    /// </summary>
    [Serializable]
    public class CertificationCheckResult
    {
        /// <summary>
        /// True if it is OK to add the abscense
        /// </summary>
        public bool Pass { get; set; }

        /// <summary>
        /// The error message
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// the certification that caused the check to fail
        /// </summary>
        public Certification FailedCertification { get; set; }

        /// <summary>
        /// The time object that was checked
        /// </summary>
        public Time CheckedTime { get; set; }

        /// <summary>
        /// Fails this instance.
        /// </summary>
        /// <returns></returns>
        public CertificationCheckResult SetFail() { this.Pass = false; return this; }

        /// <summary>
        /// Sets the error message.
        /// </summary>
        /// <param name="error">The error.</param>
        /// <returns></returns>
        public CertificationCheckResult SetErrorMessage(string error, params object[] formatArgs) { this.ErrorMessage = string.Format(error, formatArgs); return this; }

        /// <summary>
        /// Sets the failed certification.
        /// </summary>
        /// <param name="cert">The cert.</param>
        /// <returns></returns>
        public CertificationCheckResult SetFailedCertification(Certification cert) { this.FailedCertification = cert; return this; }
    }
}
