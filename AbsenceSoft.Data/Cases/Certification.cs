﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Cases
{
    /// <summary>
    /// Defines a leave certification:
    /// 
    /// A leave is specified as 
    /// 
    /// Duration per occurance per frequency
    /// 3 hours per 4 times per 10 work days
    /// 
    /// 3 hours = Duration and Duration type
    /// 4 times = occurance
    /// 10 work days = frequency and frequency calendar type
    /// </summary>
    [Serializable]
    public class Certification : BaseNonEntity
    {
        public Certification()
        {
            // Defaults
            FrequencyType = Unit.Weeks;
            DurationType = Unit.Hours;
            CertificationCreateDate = DateTime.UtcNow;
        }

        /// <summary>
        /// Gets the start date of the certification.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date of the certification.
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Gets or sets the number of occurrances that are expected to occur through the specified frequency.
        /// </summary>
        public int Occurances { get; set; }

        /// <summary>
        /// Gets or sets the frequency of the occurances as they are certified/expected to occur.
        /// </summary>
        public int Frequency { get; set; }

        /// <summary>
        /// Gets or sets the units used to measure the frequency of the occurrances.
        /// </summary>
        public Unit FrequencyType { get; set; }

        /// <summary>
        /// set the Frequency Type to work days or calendar days (used when setting the period window test) 
        /// default is work days.
        /// </summary>
        public DayUnitType FrequencyUnitType { get; set; }

        /// <summary>
        /// Gets or sets the certified duration for each occurance to occur within the bounds of.
        /// </summary>
        public double Duration { get; set; }

        /// <summary>
        /// Gets or sets the units used to measure duration of each occurrance.
        /// </summary>
        public Unit DurationType { get; set; }

        /// <summary>
        /// Gets or sets the IntermittentType of the certificate
        /// </summary>
        public IntermittentType? IntermittentType { get; set; }

        /// <summary>
        /// Gets or sets the Status of Incomplete certificate
        /// </summary>
        public bool IsCertificationIncomplete { get; set; }

        /// <summary>
        /// Gets or sets certificate creation Date
        /// </summary>
        public DateTime CertificationCreateDate { get; set; }
        

        /// <summary>
        /// Gets a pretty plain-english representation of this certification.
        /// </summary>
        /// <returns>A pretty plain-english representation of this certification.</returns>
        public override string ToString()
        {
            return string.Format("{0} occurance{5} every {1} {2} at {3} {4} per occurance",
                Occurances > 0 ? Occurances.ToString() : "No",
                Frequency,
                FrequencyType,
                Duration,
                DurationType,
                Occurances != 1 ? "s" : "");
        }
    }
}
