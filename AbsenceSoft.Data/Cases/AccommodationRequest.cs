﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Cases
{
    [Serializable]
    public class AccommodationRequest : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AccommodationRequest"/> class.
        /// </summary>
        public AccommodationRequest()
        {
            Status = CaseStatus.Open;
            Accommodations = new List<Accommodation>();
        }

        /// <summary>
        /// Gets or sets the case identifier.
        /// </summary>
        /// <value>
        /// The case identifier.
        /// </value>
        [BsonIgnore]
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the case number.
        /// </summary>
        /// <value>
        /// The case number.
        /// </value>
        [BsonIgnore]
        public string CaseNumber { get; set; }

        /// <summary>
        /// Gets or sets the accommodation request status; which is a case status, since
        /// this is like a little cute mini case inside of a case and is typically
        /// tracked separately from the case thingy itself but not separate but it's
        /// the same thing but not, right, make sense, good :-).
        /// </summary>
        public CaseStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the general health conditions description/text
        /// used to explain what accommodations are necessary for the employee.
        /// </summary>
        [BsonIgnoreIfNull]
        public string GeneralHealthCondition { get; set; }

        /// <summary>
        /// Gets or sets the description
        /// used to explain what accommodations are necessary for the employee.
        /// </summary>
        [BsonIgnoreIfNull]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the list of accommodations for this specific request.
        /// </summary>
        public List<Accommodation> Accommodations { get; set; }

        #region WorkflowExpressionHelpers

        public bool HasAccommodations
        {
            get
            {
                return Accommodations.Any();
            }
        }

        public AccommodationDuration FirstCreatedAccommodationDuration
        {
            get
            {
                var firstAccommodation = Accommodations.FirstOrDefault();
                return firstAccommodation == null
                    ? 0
                    : firstAccommodation.Duration;
            }
        }

        public AccommodationDuration LastCreatedAccommodationDuration
        {
            get
            {
                var lastAccommodation = Accommodations.LastOrDefault();
                return lastAccommodation == null
                    ? 0
                    : lastAccommodation.Duration;
            }
        }

        public bool IsAllAccommodationDurationsTemporary
        {
            get
            {
                return Accommodations.Any()
                    && Accommodations.All(p => p.Duration == AccommodationDuration.Temporary);
            }
        }

        public bool IsAllAccommodationDurationsPermanent
        {
            get
            {
                return Accommodations.Any()
                    && Accommodations.All(p => p.Duration == AccommodationDuration.Permanent);
            }
        }

        #endregion

        /// <summary>
        /// Gets the summary start date, if any, for the accommodation request, which is the earliest
        /// of any usage start date for all accommodations or <c>null</c>.
        /// </summary>
        [BsonIgnoreIfNull]
        public DateTime? StartDate { get { return Accommodations.Any() ? Accommodations.Min(u => u.StartDate) : (DateTime?)null; } set { } }

        /// <summary>
        /// Gets the suummary end date, if any, for the accommodation request, which is the latest
        /// of any usage end date for all accommodations  or <c>null</c>.
        /// </summary>
        [BsonIgnoreIfNull]
        public DateTime? EndDate
        {
            get
            {
                if (!Accommodations.Any()) return null;
                if (Accommodations.Any(u => !u.EndDate.HasValue)) return null;
                return Accommodations.Max(u => u.EndDate);
            }
            set { }
        }

        /// <summary>
        /// Gets the absolute earliest approved from date, if any, otherwise <c>null</c>.
        /// </summary>
        [BsonIgnoreIfNull]
        public DateTime? MinApprovedDate
        {
            get
            {
                if (Accommodations == null || !Accommodations.Any())
                    return null;
                var usage = Accommodations.SelectMany(a => a.Usage.Where(u => u.Determination == AdjudicationStatus.Approved)).ToList();
                if (!usage.Any())
                    return null;
                return usage.Min(u => u.StartDate);
            }
            set { }
        }

        /// <summary>
        /// Gets the absolute latest approved through date, if any, otherwise <c>null</c>.
        /// </summary>
        [BsonIgnoreIfNull]
        public DateTime? MaxApprovedDate
        {
            get
            {
                if (Accommodations == null || !Accommodations.Any())
                    return null;
                var usage = Accommodations.SelectMany(a => a.Usage.Where(u => u.Determination == AdjudicationStatus.Approved)).ToList();
                if (usage.Any() && usage.Any(u => u.EndDate.HasValue))
                    return usage.Max(u => u.EndDate);    
                    
                return null;
                

                //if (!usage.Any() || usage.Any(u => !u.EndDate.HasValue))
                //    return null;
                //return usage.Max(u => u.EndDate);
            }
            set { }
        }

        /// <summary>
        /// Gets the absolute latest approved through date + 1, if any, otherwise <c>null</c>.
        /// </summary>
        [BsonIgnore]
        public DateTime? MaxApprovedDatePlus1
        {
            get
            {
				if (MaxApprovedDate != null)
					return MaxApprovedDate.Value.AddDays(1);
				else
					return null;
            }
            set { }
        }


        /// <summary>
        /// Gets the summary adjudication status for all usage for this specific accommodation.
        /// </summary>
        public AdjudicationStatus Determination
        {
            get
            {
                var usage = Accommodations.SelectMany(a => a.Usage).ToList();
                if (!usage.Any())
                    return AdjudicationStatus.Pending;
                if (usage.Any(u => u.Determination == AdjudicationStatus.Approved))
                    return AdjudicationStatus.Approved;
                if (usage.All(u => u.Determination == AdjudicationStatus.Denied))
                    return AdjudicationStatus.Denied;

                return AdjudicationStatus.Pending;
            }
            set { }
        }

        /// <summary>
        /// Gets the summary/calculated duration of the accommodation.
        /// </summary>
        public string SummaryDuration
        {
            get
            {
                if (Accommodations == null || !Accommodations.Any())
                    return "Pending";
                AccommodationDuration duration = Accommodations.Any(a => a.Determination == AdjudicationStatus.Approved && a.Duration == AccommodationDuration.Permanent) ? AccommodationDuration.Permanent : AccommodationDuration.Temporary;
                if (duration == AccommodationDuration.Permanent)
                    return "Permanent";
                if (MinApprovedDate.HasValue && MaxApprovedDate.HasValue)
                {
                    TimeSpan dur = MaxApprovedDate.Value.ToMidnight() - MinApprovedDate.Value.ToMidnight();
                    int d = Convert.ToInt32(Math.Ceiling(dur.TotalDays));
                    if (d > 0)
                        return string.Format("{0:#,#} day{1}", d, d == 0 || d > 1 ? "s" : "");
                }
                return "Pending";
            }
            set { }
        }

		/// <summary>
		/// Gets the summary Duration Type of the Accommodation
		/// </summary>
		public string SummaryDurationType
		{
			get
			{
				if (Accommodations == null || !Accommodations.Any())
					return "Pending";
				AccommodationDuration duration = Accommodations.Any(a => a.Duration == AccommodationDuration.Permanent) ? AccommodationDuration.Permanent : AccommodationDuration.Temporary;

				if (duration == AccommodationDuration.Permanent)
					return "Permanent";
				else
					return "Temporary";
			}
			set { }
		}

        /// <summary>
        ///  Accommodation cancel reason
        /// </summary>
        public AccommodationCancelReason? CancelReason { get; set; }

        /// <summary>
        /// Accommodation cancel reason "Other" desc
        /// </summary>
        public string OtherReasonDesc { get; set; }

        /// <summary>
        /// Gets or sets the accommodation additional questions for the case
        /// </summary>
        [BsonIgnoreIfNull]
        public AccommodationInteractiveProcess AdditionalInfo { get; set; }
    }
}
