﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Audit;

namespace AbsenceSoft.Data.Cases
{
    [Serializable]
    [AuditClassNo]
    public class AppliedRuleGroup : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppliedRuleGroup"/> class.
        /// </summary>
        public AppliedRuleGroup()
        {
            Rules = new List<AppliedRule>();
        }

        /// <summary>
        /// Gets or sets a copy of the original rule group that was used for this application of it.
        /// </summary>
        public PolicyRuleGroup RuleGroup { get; set; }

        /// <summary>
        /// Gets or sets a list of applied rules that were evaluated for this applied rule group.
        /// </summary>
        public List<AppliedRule> Rules { get; set; }

        /// <summary>
        /// Gets a value indicating whether or not this applied rule group has passed based on it's applied rules
        /// and success type of the underlying rule group.
        /// </summary>
        public bool Pass
        {
            get
            {
                switch (RuleGroup.SuccessType)
                {
                    default:
                    case RuleGroupSuccessType.And:
                        return Rules.All(r => r.Pass);
                    case RuleGroupSuccessType.Or:
                        return Rules.Any(r => r.Pass);
                    case RuleGroupSuccessType.Not:
                        return Rules.All(r => r.Fail);
                    case RuleGroupSuccessType.Nor:
                        return Rules.Any(r => r.Fail);
                }
            }
            set { /*ignored, only provided for serialization*/ }
        }

        /// <summary>
        /// Gets or sets the date the rule group was last evaluated.
        /// </summary>
        [BsonIgnoreIfNull]
        public DateTime? EvalDate { get; set; }
    }
}
