﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using Newtonsoft.Json;
using AbsenceSoft.Data.Audit;

namespace AbsenceSoft.Data.Cases
{
    /// <summary>
    /// Date and info about that date, approved, denied
    /// flag and undeleted (etc)
    /// </summary>
    [Serializable]
    [AuditClassIgnoreProperty("Id")]
    public class AppliedPolicyUsage : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppliedPolicyUsage"/> class.
        /// </summary>
        public AppliedPolicyUsage()
        {
            Determination = AdjudicationStatus.Pending;
            AvailableToUse = new List<AppliedPolicyUsageType>();
        }

        /// <summary>
        /// The start date of the usage
        /// </summary>
        [BsonDateTimeOptions(DateOnly = true)]
        [AuditClassPrimaryKey(0)]
        public DateTime DateUsed { get; set; }

        /// <summary>
        /// The total amount of time used (obviously in minutes).
        /// </summary>
        //[AuditClassPrimaryKey(1)]
        public double MinutesUsed { get; set; }

        /// <summary>
        /// Used to determine what percentage of the day was used. Need to know how
        /// many minutes makes a full day
        /// </summary>
        public double MinutesInDay { get; set; }

        /// <summary>
        /// The policy reason id is assign on a per-use in case this day
        /// needs to be included in a spousal calculation
        /// </summary>
        public string PolicyReasonId { get; set; }

        /// <summary>
        /// The policy reason code
        /// </summary>
        public string PolicyReasonCode { get; set; }

        /// <summary>
        /// Gets or sets whether or not this usage is "fixed" meaning it prevents changing absence dates where
        /// the dates of this usage fall outside of the new absence dates, and that it is time that was already
        /// taken in the past and cannot be changed. The dates of the usage itself may be corrected if a correction
        /// is made to this explicit usage, however it may not 
        /// </summary>
        public bool UserEntered { get; set; }

        /// <summary>
        /// Gets or sets the optional determination status of the applied policy
        /// for the case.
        /// </summary>
        public AdjudicationStatus Determination { get; set; }

        /// <summary>
        /// Gets or sets the determination denial reason code. Only required if the determination
        /// status is Denied.
        /// </summary>
        [BsonIgnoreIfNull]
        public string DenialReasonCode { get; set; }

        /// <summary>
        /// Gets or sets the determination denial reason description. Only required if the determination
        /// status is Denied.
        /// </summary>
        [BsonIgnoreIfNull]
        public string DenialReasonName { get; set; }

        /// <summary>
        /// Gets or sets a denial explanation for setting a denial reason of Other.
        /// </summary>
        [BsonIgnoreIfNull]
        public string DenialExplanation { get; set; }

        /// <summary>
        /// Gets or sets the user id of the person who performed the determination on the applied
        /// policy range. If <c>null</c> then it was determined by the system automatically.
        /// </summary>
        [BsonIgnoreIfNull]
        public string DeterminedById { get; set; }

        /// <summary>
        /// When this usage is associated with an intermittent type
        /// this is the current status
        /// </summary>
        [BsonIgnoreIfNull]
        public IntermittentStatus? IntermittentDetermination { get; set; }

        /// <summary>
        /// When this usage is associated with an IsIntermittent Restriction
        /// this is the current status
        [BsonIgnoreIfNull]
        public bool IsIntermittentRestriction { get; set; }
        /// <summary>
        /// list of the entitlement types and the total available to use over that
        /// time period
        /// </summary>
        public List<AppliedPolicyUsageType> AvailableToUse { get; set; }

        /// <summary>
        /// Gets or sets the is locked.
        /// </summary>
        /// <value>
        /// The is locked.
        /// </value>
        [BsonIgnoreIfNull]
        public bool? IsLocked { get; set; }

        /// <summary>
        /// Shortcut to find the amount available to use for a given type
        /// </summary>
        /// <param name="et"></param>
        /// <returns></returns>
        [BsonIgnore, JsonIgnore]
        public AppliedPolicyUsageType this[EntitlementType et]
        {
            get
            {
                return AvailableToUse.FirstOrDefault(f => f.UnitType == et) ?? AvailableToUse.AddFluid(new AppliedPolicyUsageType()
                {
                    UnitType = et,
                    BaseUnit = 0d
                });
            }
            set
            {
                this[et].BaseUnit = value.BaseUnit;
            }
        }

        /// <summary>
        /// Return the usage for the day.
        /// What you get back is determined by the base type
        /// from the Entitlement type
        /// It can either be a unique count of the day or
        /// a percent used for a given type
        /// </summary>
        /// <param name="et"></param>
        /// <returns></returns>
        public double DaysUsage(EntitlementType et)
        {
            double denominator = this[et].BaseUnit;
            if (denominator == 0)
                return 0;

            // percentages are just what their name implies
            // what percentage of the day did they use
            if (et.PercentageType())
            {
                // shortcut and get it once
                return PercentOfDay * (1 / denominator);
            }

            // count types always count as a full unit whatever that may be
            // so, it is always 100 percent of a day so there
            // is no use multiplying it by 1
            if (et.CalendarType())
            {
                return (1 / denominator);
            }

            if (et == EntitlementType.ReasonablePeriod)
            {
                denominator = this[EntitlementType.WorkWeeks].BaseUnit;
                // We are facing many cases of denominator = 0 (for EntitlementType = ReasonablePeriod),so adding additioal check for 0 
                return denominator == 0 ? 0 : PercentOfDay * (1 / denominator);
            }

            // if it's not an implemented type then we will end up
            // giving it unlimited usage and the UI will have to deal with it
            return 0;

        }


        /// <summary>
        /// Gets a value indicating whether or not this policy should have usage applied. It
        /// is either a pending or approved date of usage and therefore should "burn" time from
        /// the entitled bucket.
        /// </summary>
        [BsonIgnore]
        public bool UsesTime
        {
            get
            {
                return Determination != AdjudicationStatus.Denied;
            }
        }

        /// <summary>
        /// What percentage of the day was used (Calculated field)
        /// </summary>
        [BsonIgnore]
        public double PercentOfDay
        {
            get
            {
                if (MinutesInDay == 0)
                    return 0;

                return Math.Round(MinutesUsed / MinutesInDay, 8);
            }
        }

        /// <summary>
        /// Store the Casenumber against which the usage is used 
        /// </summary>
        [BsonIgnore]
        public string CaseNumber { get; set; }
        /// <summary>
        /// Special property.... looks at the 
        /// Determination and the intermittent determination
        /// and returns a status that can be used for display purposes
        /// </summary>
        [BsonIgnore]
        public AdjudicationSummaryStatus SummaryStatus
        {
            get
            {
                // I would hope there was a better way to do this
                if (IntermittentDetermination == null)
                {
                    switch (Determination)
                    {
                        case AdjudicationStatus.Pending:
                            return AdjudicationSummaryStatus.Pending;
                        case AdjudicationStatus.Approved:
                            return AdjudicationSummaryStatus.Approved;
                        case AdjudicationStatus.Denied:
                            return AdjudicationSummaryStatus.Denied;
                    }
                }

                // Here, regardless of the actual Determination, we're going to summarize our status
                //  based off the intermitent status, 'cause that's what gets displayed and has meaning
                //  as far as this summary is concerned. The Determination is used for calculating "actual" usage
                //  but the summary is all about display and reporting, so THERE!
                switch (IntermittentDetermination)
                {
                    case IntermittentStatus.Allowed:
                        return AdjudicationSummaryStatus.Approved;
                    case IntermittentStatus.Denied:
                        return AdjudicationSummaryStatus.Denied;
                    case IntermittentStatus.Pending:
                        return AdjudicationSummaryStatus.Pending;
                }

                return AdjudicationSummaryStatus.Pending;
            }
        }

        /// <summary>
        /// This will keep track when a usage is added so we can get the last one if required
        /// </summary>
        /// 
        [BsonIgnore]
        public DateTime DateTimeAdded { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return $"{DateUsed.DayOfWeek}, {DateUsed:yyy-MM-dd} ({Determination}): {MinutesUsed.ToFriendlyTime()} used / {MinutesInDay.ToFriendlyTime()} in day";
        }
    }
}
