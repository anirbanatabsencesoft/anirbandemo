﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Cases
{
    /// <summary>
    /// Interactive process info for the case.
    /// </summary>
    [Serializable]
    public class AccommodationInteractiveProcess : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AccommodationInteractiveProcess"/> class.
        /// </summary>
        public AccommodationInteractiveProcess()
        {
            Steps = new List<Step>(); 
        }

        /// <summary>
        /// Gets or sets the steps.
        /// </summary>
        /// <value>
        /// The steps.
        /// </value>
        public List<Step> Steps { get; set; }

        [Serializable]
        public class Step : BaseNonEntity
        {
            /// <summary>
            /// The question enum for this step
            /// </summary>  
            [BsonRequired, BsonRepresentation(BsonType.ObjectId)]
            public string QuestionId { get; set; }

            /// <summary>
            /// The text displayed for the step
            /// </summary>
            [BsonIgnore, JsonIgnore]
            public AccommodationQuestion Question
            {
                get { return GetReferenceValue<AccommodationQuestion>(QuestionId); }
                set { QuestionId = SetReferenceValue(value); }
            }

            /// <summary>
            /// Gets a value indicating whether this instance is yes or no.
            /// </summary>
            /// <value>
            ///   <c>true</c> if this instance is yes or no; otherwise, <c>false</c>.
            /// </value>
            [BsonIgnore, JsonIgnore]
            public bool IsYesNo { get { return string.IsNullOrWhiteSpace(SAnswer) && (Choices == null || !Choices.Any()); } }

            /// <summary>
            /// Gets the choices available to the UI (shorthand will poulate any missing 
            /// options based on prior selection as well).
            /// </summary>
            /// <value>
            /// The choices available for selection of an answer.
            /// </value>
            [BsonIgnore, JsonIgnore]
            public List<string> Choices
            {
                get
                {
                    List<string> choices = new List<string>();
                    if (Question != null && Question.Choices != null && Question.Choices.Any())
                        choices.AddRange(Question.Choices);
                    if (!string.IsNullOrWhiteSpace(SAnswer))
                        choices.AddIfNotExists(SAnswer);
                    return choices;
                }
            }

            /// <summary>
            /// The answer provided by the user
            /// </summary>
            [BsonIgnoreIfNull]
            public bool? Answer { get; set; }

            /// <summary>
            /// The answer provided by the user
            /// </summary>
            [BsonIgnoreIfNull]
            public string SAnswer { get; set; }

            /// <summary>
            /// Step completed date
            /// </summary>
            [BsonIgnoreIfNull]
            [BsonDateTimeOptions(DateOnly = true)]
            public DateTime? CompletedDate { get; set; }

            /// <summary>
            /// Gets or sets the order.
            /// NEVER TO BE USED to key logic in UI ***OTHER THAN SORTING***
            /// </summary>
            /// <value>
            /// The order to be displayed in, PERIOD and only that.
            /// </value>
            /// <remarks>NEVER TO BE USED to key logic in UI ***OTHER THAN SORTING***</remarks>
            public int Order { get; set; }

            /// <summary>
            /// Gets or sets the date.
            /// </summary>
            /// <value>
            /// The date.
            /// </value>
            [BsonIgnoreIfNull]
            [BsonDateTimeOptions(DateOnly = true)]
            public DateTime? Date { get; set; }

            /// <summary>
            /// Gets or sets the decision overturned.
            /// </summary>
            /// <value>
            /// The decision overturned.
            /// </value>
            [BsonIgnoreIfNull]
            [BsonDateTimeOptions(DateOnly = true)]
            public DateTime? DecisionOverturned { get; set; }

            /// <summary>
            /// Gets or sets the date of initial decision.
            /// </summary>
            /// <value>
            /// The date of initial decision.
            /// </value>
            [BsonIgnoreIfNull]
            [BsonDateTimeOptions(DateOnly = true)]
            public DateTime? DateOfInitialDecision { get; set; }

            /// <summary>
            /// Gets or sets the time.
            /// </summary>
            /// <value>
            /// The time.
            /// </value>
            [BsonIgnoreIfNull]
            public string Time { get; set; }

            /// <summary>
            /// Gets or sets the provider.
            /// </summary>
            /// <value>
            /// The provider.
            /// </value>
            public string Provider { get; set; }

            /// <summary>
            /// Display label for reporting
            /// </summary>
            [BsonIgnoreIfNull]
            public string ReportLabel { get; set; }
        }
    }    
}
