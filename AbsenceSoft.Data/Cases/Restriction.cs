﻿using AbsenceSoft.Data.Customers;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Cases
{
    [Serializable, Obsolete("Restriction is Obsolete, use AbsenceSoft.Data.Jobs.Demand instead", false)]
	public class Restriction : BaseEntity<Restriction>
	{
        [Serializable, Obsolete("Limitation is Obsolete, use AbsenceSoft.Data.Jobs.DemandItem and DemandType instead", false)]
		public class Limitation: BaseNonEntity
		{
			public string Name { get; set; }
			public string Description { get; set; }
			public bool IsCustom { get; set; }
		}

		/// <summary>
		/// Gets and sets Restriction's Code
		/// </summary>
		public string Code { get; set; }

		/// <summary>
		/// Get and sets Restriction's Name
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Gets and Sets list of available limitations for this Restriction
		/// </summary>
		public List<Limitation> Limitations { get; set; }

        /// <summary>
        /// Gets and Sets list of available additional limitations for this Restriction
        /// </summary>
        public List<Limitation> AdditionalLimitations { get; set; }

        /// <summary>
        /// Gets and Sets list of available additional limitations for this Restriction
        /// </summary>
        public List<Limitation> SecondaryLimitations { get; set; }

        /// <summary>
        /// [OPTIONAL] Gets or sets the customer id that owns this absence reason.
        /// </summary>
        [BsonIgnoreIfNull, BsonIgnoreIfDefault, BsonRepresentation(BsonType.ObjectId)]
        public string CustomerId { get; set; }

        /// <summary>
        /// [OPTIONAL] Gets or sets the employer id that this employee works for.
        /// </summary>
        [BsonIgnoreIfNull, BsonIgnoreIfDefault, BsonRepresentation(BsonType.ObjectId)]
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the employee's customer.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Customer Customer
        {
            get { return GetReferenceValue<Customer>(this.CustomerId); }
            set { CustomerId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the employee's employer.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Employer Employer
        {
            get { return GetReferenceValue<Employer>(this.EmployerId); }
            set { EmployerId = SetReferenceValue(value); }
        }

		public Restriction()
		{
			Limitations = new List<Limitation>();
            AdditionalLimitations = new List<Limitation>();
            SecondaryLimitations = new List<Limitation>();
		}
	}
}
