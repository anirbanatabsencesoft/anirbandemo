﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Cases
{
    [Serializable]
    public class AccommodationQuestion : BaseEntity<AccommodationQuestion>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AccommodationQuestion"/> class.
        /// </summary>
        public AccommodationQuestion()
        {
            IsInteractiveProcessQuestion = true;
        }

        /// <summary>
        /// Gets or sets the optional customer id for which this Accommodation Question is configured.
        /// If <c>null</c> (default) then it's a global policy.
        /// </summary>
		[BsonRepresentation(BsonType.ObjectId)]
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the optional employer id for which this Accommodation Question is configured.
        /// If <c>null</c> (default) then it's a global policy. If this value is not <c>null</c>
        /// then CustomerId must also be populated with this employer's customer id.
        /// </summary>
		[BsonRepresentation(BsonType.ObjectId)]
        public string EmployerId { get; set; }

        [BsonIgnoreIfNull]
        public string Group { get; set; }

        /// <summary>
        /// Gets or sets Accommodation question
        /// </summary>
        [BsonRequired]
        public string Question { get; set; }

        /// <summary>
        /// Gets or sets the order of this Question 
        /// </summary>
        [BsonRequired]
        public int Order { get; set; }

        /// <summary>
        /// Gets or sets the accommodation type code.
        /// </summary>
        /// <value>
        /// The accommodation type code.
        /// </value>
        [BsonIgnoreIfNull]
        public string AccommodationTypeCode { get; set; }

        /// <summary>
        /// Gets or sets the type of the accommodation.
        /// </summary>
        /// <value>
        /// The type of the accommodation.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public AccommodationType AccommodationType
        {
            get { return GetReferenceValue<AccommodationType>(AccommodationTypeCode, t => t.Code, c => AccommodationType.GetByCode(c, CustomerId, EmployerId)); }
            set { AccommodationTypeCode = SetReferenceValue(value, t => t.Code); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is interactive process question.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is interactive process question; otherwise, <c>false</c>.
        /// </value>
        public bool IsInteractiveProcessQuestion { get; set; }

        [BsonIgnore, JsonIgnore]
        private List<string> _choices = new List<string>();

        /// <summary>
        /// Gets or sets the choices.
        /// </summary>
        /// <value>
        /// The choices.
        /// </value>
        public List<string> Choices { get { return _choices; } set { _choices = value; } }

        /// <summary>
        /// Prompt for the Step date
        /// </summary>
        public bool PromptDate { get; set; }

        /// <summary>
        /// Prompt for the decision overturned value
        /// </summary>
        public bool PromptDateDecisionOverturned { get; set; }

        /// <summary>
        /// Prompt for the initial decision date
        /// </summary>
        public bool PromptDateOfInitialDecision { get; set; }

        /// <summary>
        /// Prompt for the time
        /// </summary>
        public bool PromptTime { get; set; }

        /// <summary>
        /// Prompt for the provider
        /// </summary>
        public bool PromptProvider { get; set; }
    }
}
