﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace AbsenceSoft.Data.Cases
{
    [Serializable]
    public class CaseAssignmentRule : BaseNonEntity
    {
        /// <summary>
        /// Gets or sets the expression in plain English text for an end user's benefit or
        /// gives the rule a plain-text name for display.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description of the rule in plain English text that will explain to
        /// the end user the details of how the rule works
        /// </summary>
        public string Description { get; set; }        
        public string RuleType { get; set; }
        [BsonIgnoreIfNull]
        public string[] RuleValues { get; set; }
        [BsonIgnoreIfNull]
        public string Value { get; set; }
        [BsonIgnoreIfNull]
        public string Value2 { get; set; }
        [BsonIgnoreIfNull]
        public CaseAssigmentType? CaseAssignmentType { get; set; }
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string CaseAssignmentId { get; set; }

        public int Order { get; set; }
        [BsonIgnoreIfNull]
        public string CustomFieldId { get; set; }
        [BsonIgnoreIfNull]
        public string CustomFieldType { get; set; }

        /// <summary>
        /// Gets or sets the behavior.
        /// </summary>
        /// <value>
        /// The behavior.
        /// </value>
        [BsonIgnoreIfNull]
        public CaseAssignmentRuleBehavior? Behavior { get; set; }
    }    
}
