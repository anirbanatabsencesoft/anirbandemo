﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Cases
{
    /// <summary>
    /// disability info associated to a case.
    /// </summary>
    [Serializable]
    public class DisabilityInfo : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DisabilityInfo"/> class.
        /// </summary>
        public DisabilityInfo()
        {
            SecondaryDiagnosis = new List<DiagnosisCode>();
        }

        /// <summary>
        /// The primary diagnosis
        /// </summary>
        public DiagnosisCode PrimaryDiagnosis { get; set; }

        /// <summary>
        /// The primary diagnosis selected path Id from guidelines
        /// </summary>
        public string PrimaryPathId { get; set; }

        /// <summary>
        /// The primary diagnosis selected path from guidelines
        /// </summary>
        public string PrimaryPathText { get; set; }

        /// <summary>
        /// The primary diagnosis selected path from guidelines - min days
        /// </summary>
        public int? PrimaryPathMinDays { get; set; }

        /// <summary>
        /// The primary diagnosis selected path from guidelines - max days
        /// </summary>
        public int? PrimaryPathMaxDays { get; set; }

        /// <summary>
        /// The primary diagnosis selected path from guidelines -
        /// friendly display for the UI
        /// </summary>
        public string PrimaryPathDaysUIText { get; set; }

        /// <summary>
        /// Gets or sets the Job Activity at case level.
        /// </summary>
        [BsonIgnoreIfNull]
        public JobClassification? EmployeeJobActivity { get; set; }

        /// <summary>
        /// n number of secondary diagnoisis
        /// </summary>
        public List<DiagnosisCode> SecondaryDiagnosis { get; set; }

        /// <summary>
        /// Medical Complication
        /// </summary>
        public bool MedicalComplications { get; set; }

        /// <summary>
        /// Hospitialization
        /// </summary>
        public bool Hospitalization { get; set; }

        /// <summary>
        /// free form text field
        /// </summary>
        public string GeneralHealthCondition { get; set; }

        /// <summary>
        /// Condition Start Date
        /// </summary>
        public DateTime? ConditionStartDate { get; set; }

        /// <summary>
        /// Comorbity details based onprimary and secondar medical codes.
        /// </summary>
        public CoMorbidityGuideline CoMorbidityGuidelineDetail { get; set; }
    }
}
