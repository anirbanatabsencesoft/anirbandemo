﻿using AbsenceSoft.Data.Audit;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;

namespace AbsenceSoft.Data.Cases
{
    [Serializable]
    public class AccommodationUsage : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AccommodationUsage"/> class.
        /// </summary>
        public AccommodationUsage() : base() { }

        /// <summary>
        /// Gets or sets the start date, if any, that the accommodation usage/determination is applied
        /// from.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date, if any, that the accommodation usage/determination is applied
        /// through.
        /// </summary>
        [BsonIgnoreIfNull]
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the adjudication status for this specific usage/range of the accommodation.
        /// </summary>
        public AdjudicationStatus Determination { get; set; }

        /// <summary>
        /// Gets or sets the adjudication denial reason code for this specific usage/range of the accommodation
        /// when the determination is denied.
        /// </summary>
        [BsonIgnoreIfNull]
        public string DenialReasonCode { get; set; }

        /// <summary>
        /// Gets or sets the adjudication denial reason description for this specific usage/range of the accommodation
        /// when the determination is denied.
        /// </summary>
        [BsonIgnoreIfNull]
        public string DenialReasonName { get; set; }

        /// <summary>
        /// Gets or sets the denial reason description if denial reason is other and the determination is denied.
        /// </summary>
        [BsonIgnoreIfNull]
        public string DenialReasonOther { get; set; }

        /// <summary>
        /// Gets or sets determination date/time
        /// </summary>
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc, Representation = BsonType.DateTime), BsonElement("ddt"), JsonProperty("ddt")]
        public DateTime? DeterminationDate { get; set; }

        /// <summary>
        /// Gets or sets the user's email address whom determined the accommodation usage.
        /// </summary>
        [BsonElement("dby"), BsonRepresentation(BsonType.ObjectId), JsonProperty("dby")]
        public string DeterminationById { get; set; }

        /// <summary>
        /// Gets or sets the user account that adjudicated this entity.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public User DeterminationBy
        {
            get { return DeterminationById == User.DefaultUserId ? User.System : GetReferenceValue<User>(DeterminationById); }
            set { DeterminationById = SetReferenceValue(value); }
        }
    }
}
