﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Audit;

namespace AbsenceSoft.Data.Cases
{
    [Serializable]
    [AuditClassNo]
    public class CaseSummary : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CaseSummary"/> class.
        /// </summary>
        public CaseSummary()
        {
            Eligibility = EligibilityStatus.Pending;
            Determination = AdjudicationStatus.Pending;
            CaseType = (CaseType)0;
            Policies = new List<CaseSummaryPolicy>();
        }

        /// <summary>
        /// Gets or sets the eligibility.
        /// </summary>
        /// <value>
        /// The eligibility.
        /// </value>
        [BsonElement("g")]
        public EligibilityStatus Eligibility { get; set; }

        /// <summary>
        /// Gets or sets the determination.
        /// </summary>
        /// <value>
        /// The determination.
        /// </value>
        [BsonElement("a")]
        public AdjudicationStatus Determination { get; set; }

        /// <summary>
        /// Gets or sets the type of the case.
        /// </summary>
        /// <value>
        /// The type of the case.
        /// </value>
        [BsonElement("t")]
        public CaseType CaseType { get; set; }

        /// <summary>
        /// Gets or sets the policies.
        /// </summary>
        /// <value>
        /// The policies.
        /// </value>
        [BsonElement("p")]
        public List<CaseSummaryPolicy> Policies { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [case approved].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [case approved]; otherwise, <c>false</c>.
        /// </value>
        [BsonElement("r")]
        public bool CaseApproved { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [case denied].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [case denied]; otherwise, <c>false</c>.
        /// </value>
        [BsonElement("e")]
        public bool CaseDenied { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [fmla approved].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [fmla approved]; otherwise, <c>false</c>.
        /// </value>
        [BsonElement("f")]
        public bool FmlaApproved { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [fmla denied].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [fmla denied]; otherwise, <c>false</c>.
        /// </value>
        [BsonElement("l")]
        public bool FmlaDenied { get; set; }

        /// <summary>
        /// Gets or sets the fmla projected usage text.
        /// </summary>
        /// <value>
        /// The fmla projected usage text.
        /// </value>
        [BsonElement("u")]
        public string FmlaProjectedUsageText { get; set; }

        /// <summary>
        /// Gets or sets the case policy projected usage.
        /// </summary>
        /// <value>
        /// The case policy projected usage.
        /// </value>
        [BsonIgnore]
        public string PolicyProjectedUsageText { get; set; }

        /// <summary>
        /// Gets or sets the fmla exhaust date.
        /// </summary>
        /// <value>
        /// The fmla exhaust date.
        /// </value>
        [BsonElement("x")]
        public DateTime? FmlaExhaustDate { get; set; }

        /// <summary>
        /// Gets or sets the maximum approved thru date.
        /// </summary>
        /// <value>
        /// The maximum approved thru date.
        /// </value>
        [BsonElement("z")]
        public DateTime? MaxApprovedThruDate { get; set; }

        /// <summary>
        /// Gets or sets the maximum approved thru date plus1.
        /// </summary>
        /// <value>
        /// The maximum approved thru date plus1.
        /// </value>
        [BsonElement("m")]
        public DateTime? MaxApprovedThruDatePlus1 { get; set; }

        /// <summary>
        /// Gets or sets the minimum approved thru date.
        /// </summary>
        /// <value>
        /// The minimum approved thru date.
        /// </value>
        public DateTime? MinApprovedThruDate { get; set; }

        /// <summary>
        /// Gets or sets the minimum duration.
        /// </summary>
        /// <value>
        /// The minimum duration.
        /// </value>
        public TimeSpan? MinDuration { get; set; }

        /// <summary>
        /// Gets or sets the minimum duration string.
        /// </summary>
        /// <value>
        /// The minimum duration string.
        /// </value>
        public String MinDurationString { get; set; }

        /// <summary>
        /// Gets or sets the approval notification sent date.
        /// </summary>
        /// <value>
        /// The max sent date.
        /// </value>
        public DateTime? ApprovalNotificationDateSent { get; set; }
    }

    [Serializable]
    public class CaseSummaryPolicy : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CaseSummaryPolicy"/> class.
        /// </summary>
        public CaseSummaryPolicy()
        {
            Detail = new List<CaseSummaryPolicyDetail>();
            Messages = new List<string>();
        }

        /// <summary>
        /// Gets or sets the policy code.
        /// </summary>
        /// <value>
        /// The policy code.
        /// </value>
        [BsonElement("c")]
        public string PolicyCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the policy.
        /// </summary>
        /// <value>
        /// The name of the policy.
        /// </value>
        [BsonElement("n")]
        public string PolicyName { get; set; }

        /// <summary>
        /// Gets or sets the detail.
        /// </summary>
        /// <value>
        /// The detail.
        /// </value>
        [BsonElement("d")]
        public List<CaseSummaryPolicyDetail> Detail { get; set; }

        /// <summary>
        /// Gets or sets the messages.
        /// </summary>
        /// <value>
        /// The messages.
        /// </value>
        [BsonElement("m")]
        public List<string> Messages { get; set; }

        /// <summary>
        /// Gets or sets the ProjectedUsageText.
        /// </summary>
        /// <value>
        /// The ProjectedUsageText.
        /// </value>
        [BsonElement("u")]
        public string ProjectedUsageText { get; set; }
    }

    [Serializable]
    public class CaseSummaryPolicyDetail : BaseNonEntity
    {
        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>
        /// The start date.
        /// </value>
        [BsonElement("s")]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>
        /// The end date.
        /// </value>
        [BsonElement("e")]
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the type of the case.
        /// </summary>
        /// <value>
        /// The type of the case.
        /// </value>
        [BsonElement("t")]
        public CaseType CaseType { get; set; }

        /// <summary>
        /// Gets or sets the eligibility.
        /// </summary>
        /// <value>
        /// The eligibility.
        /// </value>
        [BsonElement("g")]
        public EligibilityStatus Eligibility { get; set; }

        /// <summary>
        /// Gets or sets the determination.
        /// </summary>
        /// <value>
        /// The determination.
        /// </value>
        [BsonElement("a")]
        public AdjudicationSummaryStatus Determination { get; set; }

        /// <summary>
        /// Gets or sets the determination denial reason code. Only required if the determination
        /// status is Denied.
        /// </summary>
        [BsonElement("c"), BsonIgnoreIfNull]
        public string DenialReasonCode { get; set; }

        /// <summary>
        /// Gets or sets the determination denial reason description. Only required if the determination
        /// status is Denied.
        /// </summary>
        [BsonElement("n"), BsonIgnoreIfNull]
        public string DenialReasonName { get; set; }

        /// <summary>
        /// Gets or sets the denial reason other.
        /// </summary>
        /// <value>
        /// The denial reason other.
        /// </value>
        [BsonElement("o"), BsonIgnoreIfNull]
        public string DenialReasonOther { get; set; }

        /// <summary>
        /// Gets or sets the payout percentage.
        /// </summary>
        /// <value>
        /// The payout percentage.
        /// </value>
        [BsonElement("p"), BsonIgnoreIfNull]
        public decimal? PayoutPercentage { get; set; }
    }
}
