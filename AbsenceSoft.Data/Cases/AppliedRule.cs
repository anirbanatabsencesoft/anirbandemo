﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Rules;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Data.Cases
{
    [Serializable]
    public class AppliedRule : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppliedRule"/> class.
        /// </summary>
        public AppliedRule()
        {
            Result = AppliedRuleEvalResult.Unknown;
            Messages = new List<string>();
        }

        /// <summary>
        /// Gets or sets a copy of the original policy rule that was evaluated.
        /// </summary>
        public Rule Rule { get; set; }

        /// <summary>
        /// Gets a value indicating whether or not the rule evaluation failed. This is not always the converse of Fail.
        /// The set operator is ignored.
        /// </summary>
        public bool Pass { get { return Result == AppliedRuleEvalResult.Pass; } set {/*Empty for serialization*/} }

        /// <summary>
        /// Gets a value indicating whether or not the rule evaluation failed. This is not always the converse of Pass.
        /// The set operator is ignored.
        /// </summary>
        public bool Fail { get { return Result == AppliedRuleEvalResult.Fail; } set {/*Empty for serialization*/} }

        /// <summary>
        /// Gets or sets the applied policy rule evaluation result enum value.
        /// </summary>
        public AppliedRuleEvalResult Result { get; set; }

        /// <summary>
        /// Gets or sets the actual or evaluated value discovered by the rules engine when evaluating
        /// the rule. This is gathered from evaluating the rule's left expression.
        /// </summary>
        public object ActualValue { get; set; }

        /// <summary>
        /// because object types can be lost while they are being serialized
        /// (like enums don't know they are enums when you deserialize them into
        /// an object) 
        /// </summary>
        [BsonIgnoreIfNull]
        public string ActualValueString { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not this applied rule result has been overridden by the user.
        /// </summary>
        public bool Overridden { get; set; }

        /// <summary>
        /// Gets or sets an override value provided by the user through the UI or API.
        /// </summary>
        [BsonIgnoreIfNull, BsonIgnoreIfDefault]
        public string OverrideValue { get; set; }

        /// <summary>
        /// Gets or sets the notes regarding an override value provided by the user through the UI or API
        /// as to why the override value was supplied/performed.
        /// </summary>
        [BsonIgnoreIfNull, BsonIgnoreIfDefault]
        public string OverrideNotes { get; set; }

        /// <summary>
        /// Gets or sets what the applied policy rule evaluation result enum value would be if the override had not applied.
        /// </summary>
        public AppliedRuleEvalResult OverrideFromResult { get; set; }

        /// <summary>
        /// Gets or sets any rule evaluation messages.
        /// </summary>
        public List<string> Messages { get; set; }
    }
}
