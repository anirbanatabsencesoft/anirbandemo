﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Pay;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.RiskProfiles;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Data.Cases
{
    [Serializable]
    public class Case : BaseEmployerEntity<Case>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Case"/> class.
        /// </summary>
        public Case()
        {
            Segments = new List<CaseSegment>();
            Certifications = new List<Certification>();
            CaseEvents = new List<CaseEvent>();
            Summary = new CaseSummary();
            Disability = new DisabilityInfo();
            CustomFields = new List<CustomField>();
            Pay = new PayInfo();
            Reason = new AbsenceReason() { Name = "None", Code = "NONE" };
        }

        /// <summary>
        /// The unique case number for this case.
        /// </summary>
        [BsonRequired]
        public string CaseNumber { get; set; }

        /// <summary>
        /// Gets or sets the employer's case number for external tracking or use.
        /// </summary>
        public string EmployerCaseNumber { get; set; }

        /// <summary>
        /// Gets or sets the date the leave of absence is supposed to start.
        /// </summary>
        [BsonRequired]
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date of the case/LOA.
        /// </summary>
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Set the Case Extended Modification Date
        /// </summary>
        [BsonIgnoreIfNull]
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime? ExtendedDate { get; set; }

        /// <summary>
        /// The total days in the case.  Does not care about holidays or work schedule
        /// </summary>
        [BsonIgnore]
        public int? DaysInCase
        {
            get
            {
                if (!EndDate.HasValue)
                {
                    return null;
                }

                // Add + 1 because otherwise we don't actually count the end date because it starts on midnight
                return (int)Math.Ceiling((EndDate.Value - StartDate).TotalDays) + 1;
            }
        }

        /// <summary>
        /// Gets or sets the list of case segments for the case.
        /// </summary>
        public List<CaseSegment> Segments { get; set; }

        /// <summary>
        /// Gets or sets the case Id for the exact same reason + person of a spousal leave of absence for the same employer.
        /// This is for interpreting entitlement based on rules that limiting spousal leaves of absence for the same reason
        /// and same family member.
        /// </summary>
        public string SpouseCaseId { get; set; }

        /// <summary>
        /// A list of the events that have occured and their dates
        /// </summary>
        public List<CaseEvent> CaseEvents { get; set; }

        /// <summary>
        /// Gets or sets a copy of the Employee referenced by this case.
        /// </summary>
        [BsonRequired]
        public Employee Employee { get; set; }

        [BsonIgnoreIfNull]
        public EmployerContact AuthorizedSubmitter { get; set; }

        [BsonIgnoreIfNull]
        public EmployeeContact CaseReporter { get; set; }

        [BsonIgnoreIfNull]
        public EmployeeContact CasePrimaryProvider { get; set; }
        /// <summary>
        /// Gets or sets a copy of the absence reason referenced by this case.
        /// </summary>
        public AbsenceReason Reason { get; set; }

        /// <summary>
        /// Gets or sets the compounded overall case status
        /// </summary>
        public CaseStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the case cancellation reason, if applicable.
        /// </summary>
        [BsonIgnoreIfNull]
        public CaseCancelReason? CancelReason { get; set; }

        /// <summary>
        /// Gets or sets the user id of the assigned user for this case.
        /// </summary>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string AssignedToId { get; set; }

        /// <summary>
        /// Need to differentiate between a Case Creation and Calculate eligibility
        /// </summary>
        [BsonIgnore]
        public bool IsInquiry { get; set; }
        /// <summary>
        /// Gets or sets the assigned to name for the case.
        /// </summary>
        public string AssignedToName { get; set; }

        /// <summary>
        /// Gets or sets a copy of the employer name that the case belongs to.
        /// </summary>
        [BsonIgnoreIfNull]
        public string EmployerName { get; set; }

        /// <summary>
        /// Gets or sets the Case Closure Reason referenced by this case.
        /// </summary>
        public CaseClosureReason? ClosureReason { get; set; }

        /// <summary>
        /// Gets or sets the Case Closure Reason detail referenced by this case.
        /// </summary>
        [BsonIgnoreIfNull]
        public string ClosureReasonDetails { get; set; }

        /// <summary>
        /// Gets or sets the Case Closure Other Reason detail referenced by this case.
        /// </summary>
        [BsonIgnoreIfNull]
        public string ClosureOtherReasonDetails { get; set; }
        /// <summary>
        /// Gets or sets the case display description
        /// </summary>
        [BsonIgnoreIfNull]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the case narrative
        /// </summary>
        [BsonIgnoreIfNull]
        public string Narrative { get; set; }

        /// <summary>
        /// The didsability info associated with a case
        /// </summary>
        [BsonIgnoreIfNull]
        public DisabilityInfo Disability { get; set; }

        /// <summary>
        /// If there is a case related to this one, then, this is the case
        /// </summary>
        [BsonIgnoreIfNull]
        public string ReleatedCaseId { get; set; }

        /// <summary>
        /// Gets or sets the related case number.
        /// </summary>
        /// <value>
        /// The related case number.
        /// </value>
        [BsonIgnoreIfNull]
        public string RelatedCaseNumber { get; set; }

        /// <summary>
        /// if there is a related case, then what type is that case
        /// </summary>
        [BsonIgnoreIfNull]
        public RelatedCaseType? RelatedCaseType { get; set; }

        /// <summary>
        /// Gets or sets the employee's contact that this leave was taken in relation to (parent, child, aunt, brother, etc.).
        /// </summary>
        [BsonIgnoreIfNull]
        public EmployeeContact Contact { get; set; }

        /// <summary>
        /// Gets or sets a collection of certifications if applicable for the case for intermittent requests for time off.
        /// </summary>
        public List<Certification> Certifications { get; set; }

        /// <summary>
        /// Gets or sets the case summary
        /// </summary>
        [BsonIgnoreIfNull]
        public CaseSummary Summary { get; set; }

        /// <summary>
        /// Gets or sets the pay for the case.
        /// </summary>
        /// <value>
        /// The pay.
        /// </value>
        public PayInfo Pay { get; set; }

        /// <summary>
        /// Gets or sets the accomodations for the case, this is for accomodation
        /// cases only and is not used and should be left <c>null</c> otherwise.
        /// </summary>
        [BsonIgnoreIfNull, BsonElement("Accom")]
        public AccommodationRequest AccommodationRequest { get; set; }

        /// <summary>
        /// Gets whether or not this case is an accommodation case.
        /// </summary>
        public bool IsAccommodation { get; set; }

        /// <summary>
        /// Gets whether or not this case is an information only case.
        /// </summary>
        public bool IsInformationOnly
        {
            get
            {
                return Segments != null && Segments.Any() && Segments.All(s => s.Type == Enums.CaseType.Administrative);
            }
            set { }
        }

        public bool IsReportedByAuthorizedSubmitter
        {
            get
            {
                return AuthorizedSubmitter != null
                    && !string.IsNullOrWhiteSpace(AuthorizedSubmitter.Id);
            }
        }

        /// <summary>
        /// Gets or sets the employee's spouse's exact same case for the purposes of limiting entitlement.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Case SpouseCase
        {
            get { return GetReferenceValue<Case>(SpouseCaseId); }
            set { SpouseCaseId = SetReferenceValue(value); }
        }

        /// <summary>
        /// shortcut to retrieve the related case
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Case ReleatedCase
        {
            get { return GetReferenceValue<Case>(ReleatedCaseId); }
            set { ReleatedCaseId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the work related information.
        /// </summary>
        /// <value>
        /// The work related information.
        /// </value>
        public WorkRelatedInfo WorkRelated { get; set; }

        /// <summary>
        /// Gets or sets the case's custom fields
        /// </summary>
        [BsonIgnoreIfNull]
        public List<CustomField> CustomFields { get; set; }

        /// <summary>
        /// Value is set depending on whether employee requested for Email Communication or Mail Communication
        /// </summary>
        [BsonIgnoreIfNull]
        public bool? SendECommunication { get; set; }

        /// <summary>
        /// Value is set to date depending on the day employee opted for Email or Mail mode of communication
        /// </summary>
        [BsonIgnoreIfNull]
        public DateTime? ECommunicationRequestDate { get; set; }

        /// <summary>
        /// Gets or sets the most recently calculated Risk Profile
        /// </summary>
        public RiskProfile RiskProfile { get; set; }

        /// <summary>
        /// Gets the case display title
        /// </summary>
        [BsonIgnore]
        public string Title
        {
            get
            {
                return string.Format("{0}, {3} {1:MM/dd/yyyy} to {2:MM/dd/yyyy}",
                    CaseNumber, StartDate, EndDate, Reason == null ? "" : Reason.Name);
            }
        }

        /// <summary>
        /// Gets a value indicating whether or not the case has any valid intermittent segment.
        /// </summary>
        [BsonIgnore]
        public bool IsIntermittent
        {
            get
            {
                return Segments != null &&
                    Segments.Any(s => s.Type == Enums.CaseType.Intermittent && s.Status != CaseStatus.Cancelled && s.IsEligibleOrPending);
            }
        }

        /// <summary>
        /// Gets a value indicating whether or not the case has the STD policy and that policy is eligible AND NOT denied
        /// </summary>
        [BsonIgnore]
        public bool IsSTD
        {
            get
            {
                foreach (CaseSegment segment in Segments)
                {
                    foreach (AppliedPolicy ap in segment.AppliedPolicies)
                    {
                        if (ap.Policy.PolicyType == PolicyType.STD && ap.Status == EligibilityStatus.Eligible && ap.Usage.Any(u => u.Determination != AdjudicationStatus.Denied))
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating wheather or not the case has the work restriction
        /// </summary>
        [BsonIgnore]
        public bool HasWorkRestrictions
        {
            get;
            set;
        }


        /// <summary>
        /// Gets a value indicating whether or not the case has the FMLA policy
        /// </summary>
        [BsonIgnore]
        public bool IsFMLA
        {
            get
            {
                foreach (CaseSegment segment in Segments)
                {
                    foreach (AppliedPolicy ap in segment.AppliedPolicies)
                    {
                        if (ap.Policy.PolicyType == PolicyType.FMLA && ap.Status == EligibilityStatus.Eligible)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether or not the case is relapsed
        /// </summary>
        [BsonIgnore]
        public bool IsRelapse
        {
            get
            {
                if (!string.IsNullOrEmpty(CurrentRelapseId) || Metadata.GetRawValue<bool>("IsRelapse"))
                {
                    return true;
                }
                return false;
            }         
        }

        /// <summary>
        /// Gets or sets the case's assigned user.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public User AssignedTo
        {
            get { return AssignedToId == User.DefaultUserId ? User.System : GetReferenceValue<User>(AssignedToId); }
            set { AssignedToId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets the calculated case status based on all  of the case segments.
        /// </summary>
        /// <returns>The current calculated case status.</returns>
        public CaseStatus GetCaseStatus()
        {
            if (Segments == null || !Segments.Any())
            {
                return Status;
            }

            if (Segments.Any(s => s.Status == CaseStatus.Inquiry))
            {
                return CaseStatus.Inquiry;
            }

            if (Segments.Any(s => s.Status == CaseStatus.Open))
            {
                return CaseStatus.Open;
            }

            if (Segments.Any(s => s.Status == CaseStatus.Requested))
            {
                return CaseStatus.Open;
            }

            return Segments.Any(s => s.Status == CaseStatus.Closed) ?
               CaseStatus.Closed : CaseStatus.Cancelled;
        }

        /// <summary>
        /// Gets the type of the case.
        /// </summary>
        /// <value>
        /// The type of the case.
        /// </value>
        [BsonIgnore]
        public string CaseType
        {
            get
            {
                var sb = new System.Text.StringBuilder();
                // get leave type
                var segmentDatesByType = new Dictionary<CaseType, List<CaseSegment>>();
                foreach (var segment in Segments)
                {
                    if (!segmentDatesByType.ContainsKey(segment.Type))
                    {
                        segmentDatesByType.Add(segment.Type, new List<CaseSegment>());
                    }
                    segmentDatesByType[segment.Type].Add(segment);
                }

                if (segmentDatesByType.Keys.Count() == 1)
                {
                    sb.Append(segmentDatesByType.Keys.First().ToString());
                }
                else
                {
                    foreach (var caseType in segmentDatesByType.Keys.OrderBy(o => o))
                    {
                        var format = sb.Length > 0 ? ", {0}: " : "{0}: ";
                        sb.AppendFormat(format, caseType.ToString());
                        int i = 0;
                        foreach (var segment in segmentDatesByType[caseType])
                        {
                            format = i == 0 ? "{0}-{1}" : ", {0}-{1}";
                            sb.AppendFormat(format, segment.StartDate.ToUIString(), segment.EndDate.ToUIString());
                            i++;
                        }
                    }
                }
                return sb.ToString();
            }
        }

        [BsonIgnoreIfNull]
        public string CurrentOfficeLocation { get; set; }

        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string CurrentRelapseId { get; set; }

        /// <summary>
        /// The list of user entered time, that will override existing usage
        /// </summary>
        public List<Time> MissedTimeOverride { get; set; }

        /// <summary>
        /// Overrides the default save method to ensure case status is set properly, then calls the
        /// underlying Save method for BaseEntity.
        /// </summary>
        /// <returns>The underlying Save method's return value.</returns>
        public override Case Save()
        {
            if (string.IsNullOrWhiteSpace(CaseNumber))
            {
                CaseNumber = GenerateNewCaseNumber();
                
            }
            // Set the case status.
            Status = GetCaseStatus();

            if (Reason == null && Status == CaseStatus.Inquiry)
            {
                Reason = new AbsenceReason() { Name = "None", Code = "NONE" };
            }

            return base.Save();
        }

        /// <summary>
        /// We are using RandomNumber as CaseNumber , there is a probability same CaseNumber apperars , but we need to prevent it 
        /// for same Customer and Employer
        /// </summary>
        /// <returns></returns>
        private string GenerateNewCaseNumber()
        {
            string caseNumber = RandomString.GenerateUniqueId();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(Case.Query.EQ(e => e.CustomerId, CustomerId));
            ands.Add(Case.Query.EQ(e => e.EmployerId, EmployerId));
            ands.Add(Case.Query.NE(e => e.IsDeleted, true));
            ands.Add(Case.Query.EQ(e => e.CaseNumber, caseNumber));
            

            if (Case.Query.Find(Case.Query.And(ands)).Count() > 0)
            {
                return GenerateNewCaseNumber(); 
            }
            return caseNumber;
        }
        /// <summary>
        /// This method can be used to update case status without any restirctions  , then calls the
        /// underlying Save method for BaseEntity.
        /// </summary>
        /// <returns>The underlying Save method's return value.</returns>
        public Case SaveStatus()
        {
            if (string.IsNullOrWhiteSpace(CaseNumber))
            {
                CaseNumber = GenerateNewCaseNumber();
            }

            if (Reason == null && Status == CaseStatus.Inquiry)
            {
                Reason = new AbsenceReason() { Name = "None", Code = "NONE" };
            }
            return base.Save();
        }

        /// <summary>
        /// Finds the first occurence of the requested event type,
        /// if not found returns null
        /// </summary>
        /// <param name="findThisEventType">The CaseEventType to Find</param>
        /// <returns>First occurence of that CaseEventType</returns>
        public CaseEvent FindCaseEvent(CaseEventType findThisEventType)
        {
            if (CaseEvents == null)
            {
                CaseEvents = new List<CaseEvent>();
            }

            var evt = CaseEvents.FirstOrDefault(ce => ce.EventType == findThisEventType);
            if (evt == null && findThisEventType == CaseEventType.CaseStartDate)
            {
                return SetCaseEvent(findThisEventType, StartDate);
            }

            if (evt == null && findThisEventType == CaseEventType.CaseEndDate && EndDate.HasValue)
            {
                return SetCaseEvent(findThisEventType, EndDate.Value);
            }

            return evt;
        }

        /// <summary>
        ///  Finds the last occurence of the requested event type,
        /// </summary>
        /// <param name="findThisEventType"></param>
        /// <returns></returns>

        public CaseEvent FindLastCaseEvent(CaseEventType findThisEventType)
        {
            if (CaseEvents == null)
            {
                CaseEvents = new List<CaseEvent>();
            }

            var evt = CaseEvents.OrderBy(o => o.EventDate).LastOrDefault(ce => ce.EventType == findThisEventType);

            return evt;
        }
        private CaseEvent __FindCaseEvent(CaseEventType findThisEventType)
        {
            return CaseEvents.FirstOrDefault(ce => ce.EventType == findThisEventType);
        }

        /// <summary>
        /// Gets the case event date.
        /// </summary>
        /// <param name="findThisEventType">Type of the find this event.</param>
        /// <returns></returns>
        public DateTime? GetCaseEventDate(CaseEventType findThisEventType)
        {
            if (CaseEvents == null)
            {
                return null;
            }

            var evt = __FindCaseEvent(findThisEventType);
            if (evt == null)
            {
                return null;
            }

            if (evt.AllEventDates == null || !evt.AllEventDates.Any())
            {
                return null;
            }

            return evt.EventDate;
        }

        /// <summary>
        /// Adds or updates a case event of the provided type with the specified event date.
        /// </summary>
        /// <param name="eventToAdd"></param>
        /// <param name="eventDate"></param>
        /// <returns></returns>
        public CaseEvent SetCaseEvent(CaseEventType eventToAdd, DateTime eventDate)
        {
            bool allowMultiple = false, unlimited = false;
            int max = 0;
            var att = eventToAdd.GetAttribute<MaxOccurrencesAttribute>();
            if (att != null)
            {
                allowMultiple = att.Unlimited || att.MaxOccurrences > 1;
                unlimited = att.Unlimited;
                max = att.MaxOccurrences;
            }
            CaseEvent curEvent = null;
            if (allowMultiple)
            {
                if (unlimited || CaseEvents.Count(e => e.EventType == eventToAdd) < max)
                {
                    curEvent = CaseEvents.AddFluid(new CaseEvent() { EventType = eventToAdd });
                }
                else
                {
                    curEvent = __FindCaseEvent(eventToAdd) ?? CaseEvents.AddFluid(new CaseEvent() { EventType = eventToAdd });
                }
            }
            else
            {
                curEvent = __FindCaseEvent(eventToAdd) ?? CaseEvents.AddFluid(new CaseEvent() { EventType = eventToAdd });
            }

            curEvent.EventDate = eventDate;
            return curEvent;
        }

        /// <summary>
        /// Removes all occurrences of a specified event type on the case
        /// </summary>
        /// <param name="eventToClear"></param>
        public void ClearCaseEvent(CaseEventType eventToClear)
        {
            CaseEvents.RemoveAll(ce => ce.EventType == eventToClear);
        }

        /// <summary>
        /// Gets a value indicating whether or not the all segemnts on the case have been denied.
        /// </summary>
        [BsonIgnore]
        public bool IsFullyDenied
        {
            get
            {
                return !Segments.SelectMany(x => x.AppliedPolicies).Where(y => y.Status == EligibilityStatus.Eligible)
                    .SelectMany(z => z.Usage).Any(a => a.Determination != AdjudicationStatus.Denied);
            }
        }

        /// <summary>
        /// Gets a value indicating whether or not the case has relapse.
        /// </summary> 
        public bool HasRelapse()
        {
            return CaseEvents.Any(ce => ce.EventType == CaseEventType.Relapse);
        }

        /// <summary>
        /// Called before saving
        /// </summary>
        /// <returns>
        /// A value indicating whether or not the save should proceed (not canceled).
        /// </returns>
        protected override bool OnSaving()
        {
            if (AssignedTo != null)
            {
                AssignedToName = AssignedTo.DisplayName;
            }

            if (string.IsNullOrWhiteSpace(EmployerName) && Employer != null)
            {
                EmployerName = Employer.Name;
            }

            if (string.IsNullOrWhiteSpace(CurrentOfficeLocation) && Employee != null && Employee.Info != null)
            {
                CurrentOfficeLocation = Employee.Info.OfficeLocation;
            }

            return base.OnSaving();
        }

        /// <summary>
        /// This methods returns true or false depending on whether case segments contains a case type passed as parameter.
        /// </summary>
        /// <param name="caseType"></param>
        /// <returns></returns>
        public bool ContainsCaseType(CaseType caseType)
        {
            return Segments != null &&
                Segments.Any(s => s.Type == caseType && s.Status != CaseStatus.Cancelled && (s.Type == Data.Enums.CaseType.Administrative || s.IsEligibleOrPending));
        }

        /// <summary>
        /// Gets the summary determination.
        /// </summary>
        /// <returns></returns>
        public AdjudicationSummaryStatus GetSummaryDetermination()
        {
            if (Segments == null || !Segments.Any(s => s.Status != CaseStatus.Cancelled))
            {
                return AdjudicationSummaryStatus.Pending;
            }

            // Eligibility
            var apList = Segments.Where(s => s.Status != CaseStatus.Cancelled && s.AppliedPolicies != null).SelectMany(s => s.AppliedPolicies).ToList();

            // Check if any applied policies
            if (!apList.Any())
            {
                return AdjudicationSummaryStatus.Pending;
            }

            if (apList.All(p => p.Status == EligibilityStatus.Ineligible))
            {
                return AdjudicationSummaryStatus.NotEligible;
            }

            // Determination
            // Get usage collection by itself
            var usage = apList.Where(p => p.Status == EligibilityStatus.Eligible && p.Usage != null && p.Usage.Any()).SelectMany(p => p.Usage).ToList();

            // If any are approved, then the case is approved
            if (usage.Any(u => u.Determination == AdjudicationStatus.Approved))
            {
                return AdjudicationSummaryStatus.Approved;
            }

            // If any are allowed for intermittent, then leave is allowed (sibling to approved)
            if (usage.Any(u => u.IntermittentDetermination == IntermittentStatus.Allowed))
            {
                return AdjudicationSummaryStatus.Allowed;
            }

            // If the entire thing is denied, then it's denied; otherwise should be pending
            if (usage.All(u => u.Determination == AdjudicationStatus.Denied || u.IntermittentDetermination == IntermittentStatus.Denied))
            {
                return AdjudicationSummaryStatus.Denied;
            }

            // If all else then it's pending
            return AdjudicationSummaryStatus.Pending;
        }

        #region Static Methods

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching.
        /// </summary>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static new IQueryable<Case> AsQueryable()
        {
            if (User.Current != null)
            {
                return AsQueryable(User.Current);
            }

            return BaseEntity<Case>.AsQueryable();
        }

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching while automatically
        /// applying the appropriate data access filters onto it automagically.
        /// </summary>
        /// <param name="entity">The entity of type <c>TEntity</c> to which to build the filtered queryable off of</param>
        /// <param name="user">The user of which to build data visibility and data access from</param>
        /// <param name="permission">The permission that the user should have access to when filtering the data parameters</param>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static new IQueryable<Case> AsQueryable(User user, Permission permission) { return AsQueryable(user, permission.Id); }
        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching while automatically
        /// applying the appropriate data access filters onto it automagically.
        /// </summary>
        /// <param name="entity">The entity of type <c>TEntity</c> to which to build the filtered queryable off of</param>
        /// <param name="user">The user of which to build data visibility and data access from</param>
        /// <param name="permission">The permission that the user should have access to when filtering the data parameters</param>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static new IQueryable<Case> AsQueryable(User user, string permission = null)
        {
            var query = BaseEmployerEntity<Case>.AsQueryable(user, permission);

            // Determine if the user is a SS User for any employer queried
            if (User.IsEmployeeSelfServicePortal || (user.Customer.HasFeature(Enums.Feature.ESSDataVisibilityInPortal) && !User.Permissions.GetProjectedPermissions(user).Contains(Permission.ViewAllEmployees.Id)))
            {
                query = query.Where(q => User.HasVisibilityTo.Contains(q.Employee.Id));
            }

            return query;
        }//end: AsQueryable

        /// <summary>
        /// Updates the cases employee current office location.
        /// </summary>
        /// <param name="employeeId">The employee identifier.</param>
        /// <param name="officeLocation">The office location.</param>
        public static void UpdateCasesEmployeeCurrentOfficeLocation(string employeeId, string officeLocation)
        {
            Update(Case.Query.EQ(c => c.Employee.Id, employeeId), Updates.Set(c => c.CurrentOfficeLocation, officeLocation), MongoDB.Driver.UpdateFlags.Multi);
        }

        /// <summary>
        /// Updates the Employee Number
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="originalEmployeeNumber"></param>
        /// <param name="newEmployeeNumber"></param>
        /// <param name="modifiedById"></param>
        /// <param name="customerId"></param>
        /// <param name="employerId"></param>
        public static void UpdateEmployeeNumber(string employeeId, string originalEmployeeNumber, string newEmployeeNumber, string modifiedById, string customerId, string employerId)
        {
            Update(Query.EQ(c => c.Employee.Id, employeeId), Updates.Set(c => c.Employee.EmployeeNumber, newEmployeeNumber).Set(c => c.ModifiedDate, DateTime.UtcNow).Set(c => c.ModifiedById, modifiedById), MongoDB.Driver.UpdateFlags.Multi);
            Update(Query.And(Query.EQ(c => c.Employee.SpouseEmployeeNumber, originalEmployeeNumber), Query.EQ(e => e.CustomerId, customerId), Query.EQ(e => e.EmployerId, employerId)), Updates.Set(c => c.Employee.SpouseEmployeeNumber, newEmployeeNumber).Set(c => c.ModifiedDate, DateTime.UtcNow).Set(c => c.ModifiedById, modifiedById), MongoDB.Driver.UpdateFlags.Multi);
        }

        #endregion
    }

}
