﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace AbsenceSoft.Data.Cases
{
    /// <summary>
    /// A diagnostic code of a given type, such as an ICD9 or ICD10 code, or some other code or whatever.
    /// </summary>
    [Serializable]
    public class DiagnosisCode : BaseEntity<DiagnosisCode>
    {
        /// <summary>
        /// 9 or 10
        /// </summary>
        public DiagnosisType CodeType { get; set; }

        /// <summary>
        /// The name. Example: A05.1
        /// </summary>
        public string Code { get; set; }
        
        /// <summary>
        /// The name. short description: Bot food poison
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The description of the code.
        /// Example: Botulism food poisoning
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Allowed duration's unit of measure. e.g. 2 weeks
        /// </summary>
        [BsonIgnoreIfNull]
        public Unit? DurationType { get; set; }

        /// <summary>
        /// Max allowed time, in the given unit of measure
        /// </summary>
        [BsonIgnoreIfNull]
        public int? MaxDuration { get; set; }

        /// <summary>
        /// A friendly 'code - description' for
        /// use in the UI.
        /// Example: A05.1 - Botulism food poisoning
        /// </summary>
        public string UIDescription 
        { 
            get{ return string.Format("{0} - {1}", Code, Name); }
            set{ }
        }
    }
}
