﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Cases
{
    /// <summary>
    /// Class representing a structured case segment which contains the applied policies for
    /// a given date range for a case that spans multiple types but for the same reason
    /// and is the same leave of absence.
    /// </summary>
    [Serializable]
    public class CaseSegment : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CaseSegment"/> class.
        /// </summary>
        public CaseSegment()
        {
            Absences = new List<Time>();
            LeaveSchedule = new List<Schedule>();
            AppliedPolicies = new List<AppliedPolicy>();
            UserRequests = new List<IntermittentTimeRequest>();
            CreatedDate = DateTime.UtcNow;
        }
        
        /// <summary>
        /// Gets or sets the date the leave of absence is supposed to start.
        /// </summary>
        [BsonRequired]
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date of the case/LOA.
        /// </summary>
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or set the case type.
        /// </summary>
        public CaseType Type { get; set; }

        /// <summary>
        /// Gets or sets the applied policies.
        /// </summary>
        public List<AppliedPolicy> AppliedPolicies { get; set; }

        /// <summary>
        /// Gets or sets the status for the case.
        /// </summary>
        public CaseStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the list of reduced schedules for reduced schedule cases.
        /// </summary>
        public List<Schedule> LeaveSchedule { get; set; }

        /// <summary>
        /// The list of supplied (intermittent) or calculated days (everything else)
        /// that will / have been applied to the Usage
        /// </summary>
        public List<Time> Absences { get; set; }

        /// <summary>
        /// All intermittent requests go into this collection. This allows
        /// use to use these in calculations (which will potentially give a different result)
        /// and still have what the user entered.
        /// </summary>
        [BsonIgnoreIfNull]
        public List<IntermittentTimeRequest> UserRequests { get; set; }

        /// <summary>
        /// Gets or sets the date the segment was created.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets a value indicating whether or not this segment has any eligible
        /// or pending policies.
        /// </summary>
        [BsonIgnore]
        public bool IsEligibleOrPending
        {
            get
            {
                if (AppliedPolicies != null && AppliedPolicies.Any(a => a.Status == EligibilityStatus.Eligible || a.Status == EligibilityStatus.Pending))
                    return true;
                return false;
            }
        }

        /// <summary>
        /// Gets a string representation of this segment.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{Type} {StartDate:MM/dd/yyyy} - {EndDate:MM/dd/yyyy}";
        }
    }
}
