﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Data.Cases
{
    [Serializable]
    public class WorkRelatedSharpsInfo : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkRelatedSharpsInfo"/> class.
        /// </summary>
        public WorkRelatedSharpsInfo() : base() { }

        /// <summary>
        /// Gets or sets the injury location.
        /// </summary>
        /// <value>
        /// The injury location.
        /// </value>
        [BsonIgnoreIfNull]
        public List<WorkRelatedSharpsInjuryLocation> InjuryLocation { get; set; }

        /// <summary>
        /// Gets or sets the type of sharp.
        /// </summary>
        /// <value>
        /// The type of sharp.
        /// </value>
        [BsonIgnoreIfNull]
        public string TypeOfSharp { get; set; }

        /// <summary>
        /// Gets or sets the brand of sharp.
        /// </summary>
        /// <value>
        /// The brand of sharp.
        /// </value>
        [BsonIgnoreIfNull]
        public string BrandOfSharp { get; set; }

        /// <summary>
        /// Gets or sets the model of sharp.
        /// </summary>
        /// <value>
        /// The model of sharp.
        /// </value>
        [BsonIgnoreIfNull]
        public string ModelOfSharp { get; set; }

        /// <summary>
        /// Gets or sets the body fluid involved.
        /// </summary>
        /// <value>
        /// The body fluid involved.
        /// </value>
        [BsonIgnoreIfNull]
        public string BodyFluidInvolved { get; set; }

        /// <summary>
        /// Gets or sets the have engineered injury protection.
        /// </summary>
        /// <value>
        /// The have engineered injury protection.
        /// </value>
        [BsonIgnoreIfNull]
        public bool? HaveEngineeredInjuryProtection { get; set; }

        /// <summary>
        /// Gets or sets the was protective mechanism activated.
        /// </summary>
        /// <value>
        /// The was protective mechanism activated.
        /// </value>
        [BsonIgnoreIfNull]
        public bool? WasProtectiveMechanismActivated { get; set; }

        /// <summary>
        /// Gets or sets the when did the injury occur.
        /// </summary>
        /// <value>
        /// The when did the injury occur.
        /// </value>
        [BsonIgnoreIfNull]
        public WorkRelatedSharpsWhen? WhenDidTheInjuryOccur { get; set; }

        /// <summary>
        /// Gets or sets the job classification.
        /// </summary>
        /// <value>
        /// The job classification.
        /// </value>
        [BsonIgnoreIfNull]
        public WorkRelatedSharpsJobClassification? JobClassification { get; set; }

        /// <summary>
        /// Gets or sets the job classification other.
        /// </summary>
        /// <value>
        /// The job classification other.
        /// </value>
        [BsonIgnoreIfNull]
        public string JobClassificationOther { get; set; }

        /// <summary>
        /// Gets or sets the location and department.
        /// </summary>
        /// <value>
        /// The location and department.
        /// </value>
        [BsonIgnoreIfNull]
        public WorkRelatedSharpsLocation? LocationAndDepartment { get; set; }

        /// <summary>
        /// Gets or sets the location and department other.
        /// </summary>
        /// <value>
        /// The location and department other.
        /// </value>
        [BsonIgnoreIfNull]
        public string LocationAndDepartmentOther { get; set; }

        /// <summary>
        /// Gets or sets the procedure.
        /// </summary>
        /// <value>
        /// The procedure.
        /// </value>
        [BsonIgnoreIfNull]
        public WorkRelatedSharpsProcedure? Procedure { get; set; }

        /// <summary>
        /// Gets or sets the procedure other.
        /// </summary>
        /// <value>
        /// The procedure other.
        /// </value>
        [BsonIgnoreIfNull]
        public string ProcedureOther { get; set; }

        /// <summary>
        /// Gets or sets the exposure detail.
        /// </summary>
        /// <value>
        /// The exposure detail.
        /// </value>
        [BsonIgnoreIfNull]
        public string ExposureDetail { get; set; }
    }
}
