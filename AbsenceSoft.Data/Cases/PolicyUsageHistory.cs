﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Cases
{
    /// <summary>
    /// Encapsulate lists of PolicyUsageTotals, to give it a "dictionary" type interface and
    /// make its use consistent through the case logic
    /// </summary>
    [Serializable]
    public class PolicyUsageHistory : IEnumerable
    {
        /// <summary>
        /// Gets or sets the usage totals.
        /// </summary>
        /// <value>
        /// The usage totals.
        /// </value>
        public List<PolicyUsageTotals> UsageTotals { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PolicyUsageHistory"/> class.
        /// </summary>
        public PolicyUsageHistory()
        {
            UsageTotals = new List<PolicyUsageTotals>();
        }

        /// <summary>
        /// Gets the <see cref="PolicyUsageTotals"/> with the specified policy code.
        /// </summary>
        /// <value>
        /// The <see cref="PolicyUsageTotals"/>.
        /// </value>
        /// <param name="policyCode">The policy code.</param>
        /// <returns></returns>
        public PolicyUsageTotals this[string policyCode]
        {
            get
            {
                foreach (PolicyUsageTotals put in UsageTotals)
                    if (put.PolicyCode == policyCode)
                        return put;

                PolicyUsageTotals ut = new PolicyUsageTotals() { PolicyCode = policyCode };
                UsageTotals.Add(ut);
                return ut;
            }
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator GetEnumerator()
        {
            return UsageTotals.GetEnumerator();
        }
    }
}
