﻿using AbsenceSoft.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using Newtonsoft.Json;
using AbsenceSoft.Common;

namespace AbsenceSoft.Data.Cases
{
    /// <summary>
    /// Use this class to create or modify an intermittent time off 
    /// request
    /// </summary>
    [Serializable]
    public class IntermittentTimeRequest : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IntermittentTimeRequest"/> class.
        /// </summary>
        public IntermittentTimeRequest()
        {
            Detail = new List<IntermittentTimeRequestDetail>();
        }

        /// <summary>
        /// The date to change
        /// </summary>
        public DateTime RequestDate { get; set; }

        /// <summary>
        /// Flag to set Max Occurence Reached
        /// </summary>
        public bool  IsMaxOccurenceReached { get; set; }
        /// <summary>
        /// Free form notes field; user supplied
        /// </summary>
        public string Notes { get; set; }

        /// <summary>
        /// Gets or sets the detail.
        /// </summary>
        /// <value>
        /// The detail.
        /// </value>
        public List<IntermittentTimeRequestDetail> Detail { get; set; }

        /// <summary>
        /// Total minutes of request (each detail object should sum to this amount)
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// Used by Workflow to check whether this specific request passed certification
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public bool PassedCertification { get; set; }

        [BsonIgnore]
        public string TotalTime
        {
            get
            {
                int hours = this.TotalMinutes / 60;
                int minutes = this.TotalMinutes % 60;
                if (EmployeeEntered)
                {
                    return string.Format("{0:0}h {1:0}m", hours, minutes);
                }
                if (minutes == 0)
                    return string.Format("{0:0} hours", hours);

                return string.Format("{0:0} hours and {1:0} minutes", hours, minutes);
            }
        }

        // these fields are for self service (when we get it)
        [BsonIgnore]
        public string TotalTimeForDay
        {
            get
            {
                int hours = this.TotalMinutes / 60;
                int minutes = this.TotalMinutes % 60;
                return string.Format("{0:0}h {1:0}m", hours, minutes);
            }
        }



        /// <summary>
        /// Did the employee enter the time? 
        /// </summary>
        public bool EmployeeEntered { get; set; }

        /// <summary>
        /// has a manager approved the time off request
        /// </summary>
        public bool ManagerApproved { get; set; }

        /// <summary>
        /// what date did the manager approve it
        /// </summary>
        [BsonIgnoreIfNull]
        public DateTime? ManagerApprovedDate { get; set; }

        /// <summary>
        /// what manager approved it.
        /// </summary>
        [BsonIgnoreIfNull]
        public string ManagerId { get; set; }

        /// <summary>
        /// IntermittentType
        /// </summary>
        public IntermittentType? IntermittentType { get; set; }

        /// <summary>
        /// Is Intermittent Restriction
        /// </summary>
        public bool IsIntermittentRestriction { get; set; }


        /// <summary>
        /// Start Time for Leave  
        /// </summary>
        [BsonIgnoreIfNull]
        public TimeOfDay? StartTimeForLeave { get; set; }

        /// <summary>
        /// End Time for Leave
        /// </summary>
        [BsonIgnoreIfNull]
        public TimeOfDay? EndTimeForLeave { get; set; }

        public DateTime CreatedDate { get; set; }


        /// <summary>
        /// Gets or sets the edit mode of operation
        /// </summary>
        [BsonIgnore]
        public bool IsEditMode { get; set; }

    }

    [Serializable]
    public class IntermittentTimeRequestDetail : BaseNonEntity
    {
        /// <summary>
        /// Gets or sets the policy code.
        /// </summary>
        /// <value>
        /// The policy code.
        /// </value>
        public string PolicyCode { get; set; }

        /// <summary>
        /// Minutes to set to pending
        /// </summary>
        public int Pending { get; set; }

        /// <summary>
        /// Minutes to set to approved
        /// </summary>
        public int Approved { get; set; }

        /// <summary>
        /// Minutes to deny
        /// </summary>
        public int Denied { get; set; }

        /// <summary>
        /// Total minutes
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// Gets or sets the determination denial reason code. Only required if the determination
        /// status is Denied.
        /// </summary>
        [BsonIgnoreIfNull]
        public string DenialReasonCode { get; set; }

        /// <summary>
        /// Gets or sets the determination denial reason description. Only required if the determination
        /// status is Denied.
        /// </summary>
        [BsonIgnoreIfNull]
        public string DenialReasonName { get; set; }

        /// <summary>
        /// Gets or sets the other denied reason for the denial
        /// </summary>
        public string DeniedReasonOther { get; set; }


    

    }

}
