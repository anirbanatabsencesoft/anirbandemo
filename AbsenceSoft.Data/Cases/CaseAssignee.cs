﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Data.Cases
{
    [Serializable]
    public class CaseAssignee : BaseCustomerEntity<CaseAssignee>
    {
        /// <summary>
        /// Gets or sets the case Id for the assignment.
        /// </summary>
        /// <value>
        /// The case identifier.
        /// </value>
        [BsonRequired, BsonRepresentation(BsonType.ObjectId)]
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the case assignee type code. This is optional and if specified defines the 
        /// case assignee type code for this assignment, otherwise if null
        /// represents the primary case assignment.
        /// </summary>
        /// <value>
        /// The case assignee type code.
        /// </value>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the compounded overall case status
        /// </summary>
        public CaseStatus Status { get; set; }

        /// <summary>
        /// Gets the by case identifier.
        /// </summary>
        /// <param name="caseId">The case identifier.</param>
        /// <returns></returns>
        public static List<CaseAssignee> GetByCaseId(string caseId)
        {
            return AsQueryable().Where(ca => ca.CaseId == caseId).ToList();                        
        }
    }
}
