﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Data.Cases
{
    [Serializable]
    public class ValidationMessage
    {
        /// <summary>
        /// Validation message type (info, warning, error)
        /// </summary>
        public ValidationType ValidationType { get; set; }

        /// <summary>
        /// Validation message
        /// </summary>
        public string Message { get; set; }
    }
}
