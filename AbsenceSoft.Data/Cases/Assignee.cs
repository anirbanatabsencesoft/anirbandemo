﻿using AbsenceSoft.Data.Security;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Cases
{
    /// <summary>
    /// A user assigned to a case
    /// </summary>
    public class Assignee: BaseNonEntity
    {
        /// <summary>
        /// The id of the user assigned to the case
        /// </summary>
        public string AssignedToId { get; set; }

        /// <summary>
        /// The display name of the user assigned to the case
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// The user assigned to the case
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public User AssignedTo
        {
            get{ return GetReferenceValue<User>(this.AssignedToId);}
            set{ this.AssignedToId = SetReferenceValue(value);}
        }

        /// <summary>
        /// The type of user assigned to the case
        /// </summary>
        public CaseAssignmentType AssignmentType { get; set; }

        /// <summary>
        /// The id of the user who assigned this person to the case
        /// </summary>
        public string AssignedById { get; set; }

        /// <summary>
        /// The user who assigned this person to the case
        /// </summary>
        [BsonIgnore]
        public User AssignedBy
        {
            get { return GetReferenceValue<User>(this.AssignedById); }
            set { this.AssignedById = SetReferenceValue<User>(value);}
        }

        /// <summary>
        /// The time the user was assigned to this role on this case
        /// </summary>
        public DateTime AssignedOn { get; set; }

    }
}
