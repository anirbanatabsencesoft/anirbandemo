﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Cases
{
    /// <summary>
    /// A basic class that holds the date and time that was used.
    /// 
    /// This class is used in case date calculations when all we care
    /// about is the date and how much time was used on that date
    /// 
    /// This class is note stored in the DB, it is used only for internal 
    /// calcs
    /// </summary>
    [Serializable]
    public class CaseDateUsage
    {
        /// <summary>
        /// the date the time was used
        /// </summary>
        [Obsolete("Delete me!",true)]
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime DateUsed { get; set; }

        /// <summary>
        /// how many minutes used that day
        /// </summary>
        public double MinutesUsed { get; set; }

        /// <summary>
        /// How long is this work day. This can be used to calculate
        /// what percentage of the full day was taken off
        /// </summary>
        public double MinutesInDay { get; set; }

        /// <summary>
        /// For this case date how many days are in this day's work week
        /// </summary>
        public double WorkDaysInWeek { get; set; }

        /// <summary>
        /// For this case date, how many work days (without holidays)
        /// are in the work month
        /// </summary>
        public double WorkDaysInMonth { get; set; }

        /// <summary>
        /// The policy reason id is assign on a per-use in case this day
        /// needs to be included in a spousal calculation
        /// </summary>
        public string PolicyReasonId { get; set; }

        /// <summary>
        /// what was the determination
        /// </summary>
        public AdjudicationStatus Determination { get; set; }

        /// <summary>
        /// Gets or sets the determination denial reason code. Only required if the determination
        /// status is Denied.
        /// </summary>
        public string DenialReasonCode { get; set; }

        /// <summary>
        /// Gets or sets the determination denial reason description. Only required if the determination
        /// status is Denied.
        /// </summary>
        public string DenialReasonName { get; set; }


        /// <summary>
        /// what percentage of the day was actually taken
        /// </summary>
        public double PercentOfDay
        {
            get
            {
                double result = Math.Round(MinutesUsed / MinutesInDay, 8);
                return double.IsNaN(result) ? 0 : result;
            }
        }

        /// <summary>
        /// What percentage of a week was this?
        /// </summary>
        public double PercentOfWeek
        {
            get
            {
                if (WorkDaysInWeek == 0)
                    return 0;

                double result = Math.Round(PercentOfDay * (1 / WorkDaysInWeek),6);
                return double.IsNaN(result) ? 0 : result;
            }
        }

        /// <summary>
        /// What percentage of the month does this time represent (Calculated field)
        /// </summary>
        public double PercentOfMonth
        {
            get
            {
                if (WorkDaysInMonth == 0)
                    return 0;

                double result = Math.Round(PercentOfDay * (1 / WorkDaysInMonth), 8);
                return double.IsNaN(result) ? 0 : result;
            }
        }

    }
}
