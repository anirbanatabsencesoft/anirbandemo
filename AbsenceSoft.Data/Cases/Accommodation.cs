﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Data.Cases
{
    [Serializable]
    public class Accommodation : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Accommodation"/> class.
        /// </summary>
        public Accommodation()
        {
			CreatedDate = DateTime.Now;
            Usage = new List<AccommodationUsage>();
        }

        /// <summary>
        /// Gets or sets the accommodation type for the request.
        /// </summary>
        public AccommodationType Type { get; set; }

        /// <summary>
        /// Gets or sets the duration of the accommodation, if any.
        /// </summary>
        public AccommodationDuration Duration { get; set; }

        /// <summary>
        /// Returns the total number of days in the accommodation, or null if the duration is permanent
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public int? DaysInAccommodation {
            get
            {
                if (!StartDate.HasValue || !EndDate.HasValue)
                    return null;

                return (int)(EndDate.Value - StartDate.Value).TotalDays;
            }
        }

        /// <summary>
        /// Gets or sets the accommodation description.
        /// </summary>
        [BsonIgnoreIfNull]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the collection of usage for the accommodation, similar to case applied policy
        /// usage, this tracks each date range and a status for that range (dates are optional of course)
        /// for split adjudications, etc.
        /// </summary>
        public List<AccommodationUsage> Usage { get; set; }

        /// <summary>
        /// Gets or sets the accommodation resolution.
        /// </summary>
        [BsonIgnoreIfNull]
        public string Resolution { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the accommodation was resolved.
        /// </summary>
        public bool Resolved { get; set; }

        /// <summary>
        /// Gets or sets the accommodation resolved date.
        /// </summary>
        /// <value>
        /// The accommodation resolved date.
        /// </value>
        [BsonIgnoreIfNull]
        public DateTime? ResolvedDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the accommodation was resolved.
        /// </summary>
        public bool Granted { get; set; }

        /// <summary>
        /// Gets or sets the accommodation granted date.
        /// </summary>
        /// <value>
        /// The accommodation granted date.
        /// </value>
        [BsonIgnoreIfNull]
        public DateTime? GrantedDate { get; set; }

        /// <summary>
        /// Gets or sets the cost of the accommodation, if any, stored in the customer's native currency.
        /// </summary>
        [BsonIgnoreIfNull]
        public decimal? Cost { get; set; }

        /// <summary>
        /// Gets the summary start date, if any, for the accommodation, which is the earliest
        /// of any usage start date or <c>null</c>.
        /// </summary>
        [BsonIgnoreIfNull]
        public DateTime? StartDate { get { return Usage.Any() ? Usage.Min(u => u.StartDate) : (DateTime?)null; } set { } }

        /// <summary>
        /// Gets the suummary end date, if any, for the accommodation, which is the latest
        /// of any usage end date or <c>null</c>.
        /// </summary>
        [BsonIgnoreIfNull]
        public DateTime? EndDate
        {
            get
            {
                if (!Usage.Any()) return null;
                if (Usage.Any(u => !u.EndDate.HasValue)) return null;
                return Usage.Max(u => u.EndDate);
            }
            set { }
        }

        /// <summary>
        /// Gets the summary adjudication status for all usage for this specific accommodation.
        /// </summary>
        public AdjudicationStatus Determination
        {
            get
            {
                if (!Usage.Any())
                    return AdjudicationStatus.Pending;
                if (Usage.Any(u => u.Determination == AdjudicationStatus.Approved))
                    return AdjudicationStatus.Approved;
                if (Usage.All(u => u.Determination == AdjudicationStatus.Denied))
                    return AdjudicationStatus.Denied;

                return AdjudicationStatus.Pending;
            }
            set { }
        }

        /// <summary>
        /// Gets the minimum start date of approved time for this specific accommodation.
        /// </summary>
        public DateTime? MinApprovedFromDate
        {
            get
            {
                if (Determination != AdjudicationStatus.Approved)
                    return null;

                DateTime? returnValue = null;
                if (Usage.Any(y => y.Determination == AdjudicationStatus.Approved))
                    returnValue = Usage.Where(y => y.Determination == AdjudicationStatus.Approved).ToList().Min(z => z.StartDate);

                return returnValue;
            }
            set { }
        }

        /// <summary>
        /// Gets the maximum approved through date for this specific accommodation.
        /// </summary>
        public DateTime? MaxApprovedThruDate
        {
            get
            {
                if (Determination != AdjudicationStatus.Approved)
                    return null;

                DateTime? returnValue = null;
                if (Usage.Where(y => y.Determination == AdjudicationStatus.Approved).ToList().Any(z => z.EndDate.HasValue))
                    returnValue = Usage.Where(y => y.Determination == AdjudicationStatus.Approved).ToList().Max(z => z.EndDate);

                return returnValue;
            }
            set { }
        }

        /// <summary>
        /// Gets or sets the accommodation interactive process for the case
        /// </summary>
        [BsonIgnoreIfNull]
        public AccommodationInteractiveProcess InteractiveProcess { get; set; }

		/// <summary>
		/// Gets or sets creation date/time
		/// </summary>
		[BsonDateTimeOptions(Kind = DateTimeKind.Utc, Representation = BsonType.DateTime), BsonElement("cdt"), JsonProperty("cdt")]
		public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        [BsonDefaultValue(CaseStatus.Open)]
        public CaseStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the cancel reason.
        /// </summary>
        /// <value>
        /// The cancel reason.
        /// </value>
        public AccommodationCancelReason? CancelReason { get; set; }

        /// <summary>
        /// Gets or sets the other reason desc.
        /// </summary>
        /// <value>
        /// The other reason desc.
        /// </value>
        public string OtherReasonDesc { get; set; }

        /// <summary>
        /// Gets or sets the is work related.
        /// </summary>
        /// <value>
        /// The is work related.
        /// </value>
        public bool? IsWorkRelated { get; set; }

    }
}
