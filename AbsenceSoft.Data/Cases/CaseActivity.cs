﻿using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.ToDo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Cases
{
    [Serializable]

    public class CaseActivity
    {
        public CaseActivity()
        {
            
        }

        public CaseActivity(CaseNote note)
            :this()
        {
            CreatedDate = note.CreatedDate;
            SetNoteCreatedBy(note);

            Type = "Note";
            Category = note.CategoryString;
            Description = note.Notes;
        }        

        public CaseActivity(ToDoItem todo)
            :this()
        {
            CreatedDate = todo.CreatedDate;
            CreatedBy = todo.CreatedBy?.DisplayName ?? string.Empty;
            Type = "ToDo";
            Category = "N/A";
            Description = todo.Title;
        }

        public CaseActivity(Communication communication)
            :this()
        {
            CreatedDate = communication.CreatedDate;
            CreatedBy = communication.CreatedBy?.DisplayName ?? string.Empty;
            Type = "Communication";
            Category = "N/A";
            Description = communication.Subject;
        }

        public CaseActivity(Attachment attachment)
            :this()
        {
            CreatedDate = attachment.CreatedDate;
            CreatedBy = attachment.CreatedBy?.DisplayName ?? string.Empty;
            Type = "Attachment";
            Category = attachment.AttachmentType.ToString();
            Description = attachment.Description;
        }


        public DateTime CreatedDate { get; set; }
        public string Category { get; set; }
        public string CreatedBy { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }

        /// <summary>
        /// Set note created by
        /// </summary>
        /// <param name="note"></param>
        private void SetNoteCreatedBy(CaseNote note)
        {
            if (!string.IsNullOrWhiteSpace(note.EnteredByName))
            {
                CreatedBy = note.EnteredByName;
            }
            else if (!string.IsNullOrWhiteSpace(note.EnteredByEmployeeNumber) && !string.IsNullOrEmpty(note.EmployerId))
            {
                CreatedBy = Employee.AsQueryable().Where(e => e.EmployerId == note.EmployerId && e.EmployeeNumber == note.EnteredByEmployeeNumber).FirstOrDefault()?.FullName ?? string.Empty;
            }
            else
            {
                CreatedBy = note.CreatedBy?.DisplayName ?? string.Empty;
            }
        }
    }
}
