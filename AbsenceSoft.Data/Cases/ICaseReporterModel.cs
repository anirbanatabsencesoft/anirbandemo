﻿namespace AbsenceSoft.Data.Cases
{
    public interface ICaseReporterModel
    {
        string Id { get; set; }
        string CustomerId { get; set; }
        string EmployerId { get; set; }
        Contact Contact { get; set; }
        string ContactTypeCode { get; set; }
        string ContactTypeName { get; set; }
    }
}