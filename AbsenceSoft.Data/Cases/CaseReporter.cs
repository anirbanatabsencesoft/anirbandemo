﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;

namespace AbsenceSoft.Data.Cases
{
    [Serializable]
    public class CaseReporter : BaseEmployerEntity<CaseReporter>
    {
        /// <summary>
        /// Gets or sets the case Id for the Case Reporter.
        /// </summary>
        /// <value>
        /// The case identifier.
        /// </value>
        [BsonRequired, BsonRepresentation(BsonType.ObjectId)]
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the Case Reporter.
        /// </summary>
        /// <value>
        /// The Case Reporter.
        /// </value>
        [BsonRequired]
        public EmployeeContact Reporter { get; set; }

        /// <summary>
        /// Gets the by case identifier.
        /// </summary>
        /// <param name="caseId">The case identifier.</param>
        /// <returns></returns>
        public static List<CaseReporter> GetByCaseId(string caseId)
        {
            return AsQueryable().Where(ca => ca.CaseId == caseId).ToList();
        }
    }

}
