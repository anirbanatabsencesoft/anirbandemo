﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Cases
{
    [Serializable]
    public class AccommodationType : BaseEmployerOverrideableEntity<AccommodationType>
	{
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
		[BsonRequired]
		public string Name { get; set; }

        /// <summary>
        /// Gets or sets the order
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        public int? Order { get; set; }

        [BsonIgnoreIfNull]
        [BsonDictionaryOptions(DictionaryRepresentation.ArrayOfArrays)]
        public SortedList<int, string> Categories { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to prevent new Accommodations
        /// from being created with this type (gets filtered from the UI on create)
        /// for things such as Case Conversion, etc.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [prevent new]; otherwise, <c>false</c>.
        /// </value>
        public bool PreventNew { get; set; }

        /// <summary>
        /// Gets or sets the Accomodation duration.
        /// </summary>
        /// <value>
        /// Temporary, Permanent or Both.
        /// </value>
        public AccommodationDuration Duration { get; set; }

        /// <summary>
        /// Gets or sets the case type flag.
        /// </summary>
        /// <value>
        /// The case type flag.
        /// </value>
        [BsonIgnoreIfNull]
        public CaseType CaseTypeFlag { get; set; }

        [BsonIgnore]
        public bool IsDisabled { get; set; }

        /// <summary>
        /// Gets or sets the name of the parent.
        /// </summary>
        /// <value>
        /// The name of the parent.
        /// </value>
        [BsonIgnore]
        public string ParentName { get; set; }


        /// <summary>
        /// Gets or sets the categories.
        /// </summary>
        /// <value>
        /// The categories.
        /// </value>
        [BsonIgnore]
        public List<AccommodationType> Children { get; set; }

        /// <summary>
        /// Called before saving
        /// </summary>
        /// <returns>
        /// A value indicating whether or not the save should proceed (not canceled).
        /// </returns>
		protected override bool OnSaving()
		{
			if (!string.IsNullOrWhiteSpace(Code))
				Code = Code.ToUpperInvariant();
            if (string.IsNullOrWhiteSpace(CustomerId) && Employer != null)
                CustomerId = Employer.CustomerId;
			return base.OnSaving();
		}
	}
}
