﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Cases
{
    /// <summary>
    /// Allows options for OSHA Reporting to be end user configurable
    /// </summary>
    [Serializable]
    public class OshaFormField : BaseEmployerOverrideableEntity<OshaFormField>
	{
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
		[BsonRequired]
		public string Name { get; set; }

        /// <summary>
        /// Gets or sets the OSHA form Category.
        /// </summary>
        /// <value>
        /// The OshaCategory.
        /// </value>
        [BsonIgnoreIfNull]
        public OshaFormType Category { get; set; }

        /// <summary>
        /// Gets or sets the Options for specific OSHA field Name.
        /// </summary>
        /// <value>
        /// Options to be confugred.
        /// </value>
        [BsonIgnoreIfNull]
        public List<string> Options { get; set; }


        /// <summary>
        /// Returns whether this Osha Form Field is disabled
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is disabled; otherwise, <c>false</c>.
        /// </value>
        [BsonIgnore]
        public bool IsDisabled { get; set; }

        /// <summary>
        /// Called before saving
        /// </summary>
        /// <returns>
        /// A value indicating whether or not the save should proceed (not canceled).
        /// </returns>
		protected override bool OnSaving()
		{
            if (!string.IsNullOrWhiteSpace(Code))
            {
                Code = Code.ToUpperInvariant();
            }
            if (string.IsNullOrWhiteSpace(CustomerId) && Employer != null)
            {
                CustomerId = Employer.CustomerId;
            }
			return base.OnSaving();
		}
	}
}
