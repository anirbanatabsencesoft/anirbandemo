﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AbsenceSoft.Data.Cases
{
    /// <summary>
    /// This class is used to track a generic set of events and
    /// the date that those events occured.
    /// </summary>
    [Serializable]
    public class CaseEvent : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CaseEvent"/> class.
        /// </summary>
        public CaseEvent() : base()
        {
            AllEventDates = new List<DateTime>(1);
        }

        /// <summary>
        /// Gets or sets the event type.
        /// </summary>
        public CaseEventType EventType { get; set; }

        /// <summary>
        /// Gets or sets the last date of occurance of the event.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public DateTime EventDate
        {
            get
            {
                return AllEventDates.LastOrDefault();
            }
            set
            {
                MaxOccurrencesAttribute att = EventType.GetType().GetCustomAttributes(typeof(MaxOccurrencesAttribute), false).OfType<MaxOccurrencesAttribute>().FirstOrDefault();
                if (att != null && !att.Unlimited && !AllEventDates.Contains(value))
                {
                    while (AllEventDates.Count >= att.MaxOccurrences)
                        AllEventDates.RemoveAt(AllEventDates.Count - 1);
                    AllEventDates.Add(value);
                }
                else if(EventType.Equals(CaseEventType.DeliveryDate) || EventType.Equals(CaseEventType.Relapse) || EventType.Equals(CaseEventType.BondingStartDate) || EventType.Equals(CaseEventType.BondingEndDate))
                {
                    AllEventDates.Remove(value);
                    AllEventDates.Add(value);
                }
                else
                    AllEventDates.AddIfNotExists(value);
            }
        }

        /// <summary>
        /// Gets or sets a collection of all event dates, inclusive of the "EventDate".
        /// Since an event may happen more than once; this ensures we capture all of them.
        /// Do not directly add dates to this collection, Set EventDate instead.
        /// </summary>
        public List<DateTime> AllEventDates { get; set; }
    }
}
