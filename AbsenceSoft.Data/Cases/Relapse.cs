﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Cases
{
    [Serializable]
    public class Relapse : BaseEmployerEntity<Relapse>
    {
        /// <summary>
        /// Gets or sets the case Id for the relapse.
        /// </summary>
        [BsonRequired, BsonRepresentation(BsonType.ObjectId)]
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the date the leave of absence is supposed to start for relapse.
        /// </summary>
        [BsonRequired]
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date of the relapse Case.
        /// </summary>
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets a copy of the Employee referenced by this relapse case from DB.
        /// </summary>
        [BsonRequired]
        public Employee Employee { get; set; }

        /// <summary>
        /// Gets or sets a copy of the Employee referenced by this relapse case from DB.
        /// </summary>
        [BsonRequired]
        public CaseType CaseType { get; set; }

        /// <summary>
        /// Wrok schedule if case type is reduced
        /// </summary>
        [BsonIgnoreIfNull]
        public Schedule LeaveSchedule { get; set; }

        /// <summary>
        /// Gets or sets the Case.
        /// </summary>
        /// <value>
        /// The document.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public Case Case
        {
            get { return GetReferenceValue<Case>(this.CaseId); }
            set { CaseId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching.
        /// </summary>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static new IQueryable<Relapse> AsQueryable()
        {
            if (User.Current != null)
                return AsQueryable(User.Current);
            return BaseEntity<Relapse>.AsQueryable();
        }

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching while automatically
        /// applying the appropriate data access filters onto it automagically.
        /// </summary>
        /// <param name="entity">The entity of type <c>TEntity</c> to which to build the filtered queryable off of</param>
        /// <param name="user">The user of which to build data visibility and data access from</param>
        /// <param name="permission">The permission that the user should have access to when filtering the data parameters</param>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static new IQueryable<Relapse> AsQueryable(User user, Permission permission)
        {
            return AsQueryable(user, permission.Id);
        }
        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching while automatically
        /// applying the appropriate data access filters onto it automagically.
        /// </summary>
        /// <param name="entity">The entity of type <c>TEntity</c> to which to build the filtered queryable off of</param>
        /// <param name="user">The user of which to build data visibility and data access from</param>
        /// <param name="permission">The permission that the user should have access to when filtering the data parameters</param>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static new IQueryable<Relapse> AsQueryable(User user, string permission = null)
        {
            var query = BaseEmployerEntity<Relapse>.AsQueryable(user, permission);

            // Determine if the user is a SS User for any employer queried
            if (User.IsEmployeeSelfServicePortal || (user.Customer.HasFeature(Enums.Feature.ESSDataVisibilityInPortal) && !User.Permissions.GetProjectedPermissions(user).Contains(Permission.ViewAllEmployees.Id)))
                query = query.Where(q => User.HasVisibilityTo.Contains(q.Employee.Id));

            return query;
        }//end: AsQueryable
    }
}
