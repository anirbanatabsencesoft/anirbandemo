﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Cases
{
    /// <summary>
    /// Allows categories for case to be end user configurable
    /// </summary>
   [Serializable]

    public class CaseCategory : BaseEmployerOverrideableEntity<CaseCategory>
    {
        /// <summary>
        /// The name of the category 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The description of this categories purpose
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the list of absence reason for note category.
        /// </summary>
        [BsonRepresentation(BsonType.ObjectId), BsonIgnoreIfNull]
        public List<string> ReasonIds { get; set; }
    }
}
