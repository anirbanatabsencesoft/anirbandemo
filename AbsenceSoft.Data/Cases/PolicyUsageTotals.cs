﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Data.Cases
{
    /// <summary>
    /// Class used to hold the values used per policy
    /// 
    /// This class also stores the days that were used so that they can be extracted
    /// by policy or totaled by date
    /// </summary>
    [Serializable]
    public class PolicyUsageTotals
    {
        public PolicyUsageTotals()
        {
            Allocated = new List<AppliedPolicyUsage>();
            NextPeriodStartDate = DateTime.MaxValue;              // default it to never ending
        }

        /// <summary>
        /// Gets or sets the policy code.
        /// </summary>
        /// <value>
        /// The policy code.
        /// </value>
        public string PolicyCode { get; set; }

        /// <summary>
        /// how was the time used on this policy
        /// </summary>
        public List<AppliedPolicyUsage> Allocated { get; set; }

        /// <summary>
        /// this is the end date of the calc window
        /// </summary>
        public DateTime NextPeriodStartDate { get; set; }

        /// <summary>
        /// The begining of the date range we are working with (aka window)
        /// </summary>
        public DateTime MeasureStartDate { get; set; }

        /// <summary>
        /// End date of the range we are working with (aka window)
        /// </summary>
        public DateTime MeasureEndDate { get; set; }

        /// <summary>
        /// how long is the measurement period
        /// </summary>
        public int DaysInPeriod { get; set; }

        public PeriodType MeasurePeriodType { get; set; }

        /// <summary>
        /// number of days to add to the current date to get the
        /// next window reset
        /// </summary>
        public int ResetDays { get; set; }

        public double PercentUsed(DateTime dt, EntitlementType et)
        {
            return Allocated.Where(d => d.DateUsed == dt).Sum(a => a.DaysUsage(et));
        }

        public double PercentUsed(DateTime dt, AdjudicationStatus adj, EntitlementType et)
        {
            return Allocated.Where(d => d.DateUsed == dt && d.Determination == adj).Sum(a => a.DaysUsage(et));
        }

        /// <summary>
        /// find the percentage used, all rows regardless of status
        /// </summary>
        /// <param name="et"></param>
        /// <returns></returns>
        public double PercentUsed(EntitlementType et)
        {
            return Allocated.Sum(a => a.DaysUsage(et));
        }

        /// <summary>
        /// Find denied percentages by denial reason
        /// </summary>
        /// <param name="et"></param>
        /// <param name="deniedCode"></param>
        /// <returns></returns>
        public decimal PercentUsed(EntitlementType et, string deniedCode)
        {
            deniedCode = string.IsNullOrWhiteSpace(deniedCode) ? deniedCode : deniedCode.ToUpperInvariant();
            return Allocated.Where(a => a.Determination == AdjudicationStatus.Denied && a.DenialReasonCode == deniedCode).Sum(a => Convert.ToDecimal(a.DaysUsage(et)));
        }

        /// <summary>
        /// find the percentage used for a given status
        /// </summary>
        /// <param name="et"></param>
        /// <param name="adj"></param>
        /// <returns></returns>
        public decimal PercentUsed(EntitlementType et, AdjudicationStatus adj)
        {
                return Allocated.Where(a => a.Determination == adj).Sum(a => Convert.ToDecimal(a.DaysUsage(et)));
        }

        public decimal PercentUsed(EntitlementType et, AdjudicationStatus adj, AppliedPolicy ap)
        {
            //Plese don't delete very helpful for calc debug
            //foreach (var all in Allocated.Where(a => a.Determination == adj))
            //{
            //    System.Diagnostics.Debug.WriteLine(all.CaseNumber + "," + all.DateUsed.ToShortDateString() + "," + all.MinutesInDay
            //        + "," + all.MinutesUsed + "," + all.DaysUsage(et));
            //}
            if (ap.PolicyReason.PerUseCap.HasValue)
            {
                if (ap.EndDate.HasValue) //  PS-1865 is the cause of it
                {
                    //We can't check usage beyond Segment Enddate. which could be leser to case EndDate
                    return Allocated.Where(a => a.Determination == adj && a.DateUsed <= ap.EndDate.Value
                            && a.PolicyReasonCode == (!string.IsNullOrEmpty(ap.PolicyReason.ReasonCode) ?
                            ap.PolicyReason.ReasonCode : ap.PolicyReason.Reason.Code))
                            .Sum(a => Convert.ToDecimal(a.DaysUsage(et)));
                }
                else
                {
                    return Allocated.Where(a => a.Determination == adj &&
                           a.PolicyReasonCode == (!string.IsNullOrEmpty(ap.PolicyReason.ReasonCode) ? 
                           ap.PolicyReason.ReasonCode : ap.PolicyReason.Reason.Code))
                           .Sum(a => Convert.ToDecimal(a.DaysUsage(et)));
                }
            }
            else
            {
                if (ap.EndDate.HasValue)
                {
                    //We can't check usage beyond Segment Enddate. which could be leser to case EndDate
                    return Allocated.Where(a => a.Determination == adj && a.DateUsed <= ap.EndDate.Value)
                                                       .Sum(a => Convert.ToDecimal(a.DaysUsage(et)));
                }
                else
                {
                    return Allocated.Where(a => a.Determination == adj).Sum(a => Convert.ToDecimal(a.DaysUsage(et)));
                }
            }
        }
        /// <summary>
        /// Percents the used.
        /// </summary>
        /// <param name="et">The et.</param>
        /// <param name="adj">The adj.</param>
        /// <param name="excludeIntermittent">if set to <c>true</c> [exclude intermittent].</param>
        /// <returns></returns>
        public decimal PercentUsed(EntitlementType et, AdjudicationStatus adj, bool excludeIntermittent)
        {
            return Allocated.Where(a => a.Determination == adj && (excludeIntermittent ? !a.IntermittentDetermination.HasValue : true)).Sum(a => Convert.ToDecimal(a.DaysUsage(et)));
        }

        public void AddAllocated(DateTime workDate, double minutes, double dayLength, string policyReasonCode, AdjudicationStatus adjStatus,
                                    string denialReasonCode, IntermittentStatus? intermittentDetermination,
                                    List<AppliedPolicyUsageType> aput, string denialExplanation = null, 
                                    string denialReasonName = null,bool userEntered = false,string caseNumber = null)
        {
            denialReasonCode = string.IsNullOrWhiteSpace(denialReasonCode) ? denialReasonCode : denialReasonCode.ToUpperInvariant();
            Allocated.Add(new AppliedPolicyUsage()
            {
                DateUsed = workDate,
                MinutesInDay = dayLength,
                MinutesUsed = minutes,
                PolicyReasonCode = policyReasonCode,
                Determination = adjStatus,
                DenialReasonCode = denialReasonCode,
                DenialReasonName = denialReasonName,
                DenialExplanation = denialExplanation,
                IntermittentDetermination = intermittentDetermination,
                AvailableToUse = aput,
                UserEntered =userEntered,
                DateTimeAdded = DateTime.Now,
                CaseNumber = caseNumber
            });
        }


        /// <summary>
        /// find the percentage used, all rows regardless of status
        /// </summary>
        /// <param name="et"></param>
        /// <returns></returns>
        public double MinutesUsed()
        {
            if (Allocated.Any())
                return Allocated.Sum(a => a.MinutesUsed);
            return 0d;
        }
        public double MinutesUsed(AdjudicationStatus adjStatus)
        {
            if (Allocated.Any(a => a.Determination == adjStatus))
                return Allocated.Where(a => a.Determination == adjStatus).Sum(a => a.MinutesUsed);
            return 0d;
        }

        /// <summary>
        /// set the dates of the next time we should do something with the
        /// usage totals to figure out our next reset date
        /// (Though this logic, might not really belong here, I'm not sure
        /// where else to put it at the moment)
        /// </summary>
        /// <param name="resetPoint">The reset point.</param>
        /// <param name="ap">The applied policy.</param>
        public void RollToNextPeriod(DateTime resetPoint, AppliedPolicy ap)
        {
            // rolling back is special, we want to keep the look back period to
            // whatever it's time frame is without reloading the data, so, we trim
            // off x days each time and in that way keep the time window sliding
            if (this.MeasurePeriodType == PeriodType.RollingBack)
            {

                //MeasureStartDate = resetPoint.AddMonths(-Convert.ToInt32(ap.PolicyReason.Period ?? 12d));
                //MeasureEndDate = resetPoint.AddDays(-1);
                //NextPeriodStartDate = resetPoint.AddDays(ResetDays);
                //Allocated = Allocated.Where(a => a.DateUsed > MeasureStartDate).ToList();

                MeasureStartDate = resetPoint.AddDays(-(DaysInPeriod - 1));
                MeasureEndDate = resetPoint.AddDays(-1);

                //If it's a new year of entitlement the employee accrues FMLA time back each day to use , so we don't want 
                //earlier year allocated value for calculation . 
                //If we don't clear up the earlier year value , this will be taken in account while doing calculation 
                //and the current year value will be exhausted earlier than expected
                //This issue will occur in only those cases where we created a case in one year and continues to next year 
                //and the first year has very few hours left.



                // Leap year hack for rolling back calcs when we cross the boundary of a leap day
                // or the original calcs were based on a leap year/day and now we're in a non-leap-year.
                if (DaysInPeriod == 365 || DaysInPeriod == 366)
                {
                    var leapDays = Date.NumberOfLeapDays(MeasureStartDate, MeasureEndDate);
                    if (DaysInPeriod == 365 && leapDays > 0)
                        // Adjust one more day back if we've crossed a leap day AND we only have 365 days in the period
                        //  otherwise we don't care, if no leap days are crossed then we don't need to adjust anything, even
                        //  if we're in a leap year itself.
                        MeasureStartDate = MeasureStartDate.AddDays(-leapDays);
                    else if (DaysInPeriod == 366 && leapDays == 0)
                        // Adjust one day forward if we've NOT crossed a leap day AND we have 366 days in the period
                        //  otherwise we don't care. In this scenario, the original usage measure crossed a leap day
                        //  and now in our measurement window we should no longer account for it.
                        MeasureStartDate = MeasureStartDate.AddDays(leapDays);
                }

                Allocated = Allocated.Where(a => a.DateUsed > MeasureStartDate).ToList();
                NextPeriodStartDate = resetPoint.AddDays(ResetDays);
            }
            else
            {
                Allocated.Clear();
                NextPeriodStartDate = NextPeriodStartDate.AddDays(ResetDays);
            }
        }

    } // public class PolicyUsageTotals
}
