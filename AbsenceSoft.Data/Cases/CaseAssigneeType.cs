﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace AbsenceSoft.Data.Cases
{
    [Serializable]
    public class CaseAssigneeType : BaseCustomerEntity<CaseAssigneeType>
    {
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [BsonIgnoreIfNull]
        public string Description { get; set; }

        [BsonIgnoreIfNull]        
        public int? ThresholdCaseCount { get; set; }

        /// <summary>
        /// Gets the case assignee type by code.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <param name="customerId">The customer identifier.</param>        
        /// <returns></returns>
        public static CaseAssigneeType GetByCode(string code, string customerId)
        {
            var types = CaseAssigneeType.AsQueryable().Where(cat => cat.Code == code && new[] { customerId, null }.Contains(cat.CustomerId)).ToList();
            return types.FirstOrDefault(t => t.CustomerId == customerId)
                ?? types.FirstOrDefault();
        }
    }
}
