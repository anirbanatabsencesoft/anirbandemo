﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace AbsenceSoft.Data.Cases
{
    [Serializable]
    public class CaseAssignmentRuleSet : BaseEmployerOverrideableEntity<CaseAssignmentRuleSet>
    {
        public CaseAssignmentRuleSet() 
        {
            CaseAssignmentRules = new List<CaseAssignmentRule>();
        }
        [BsonRequired]
        public string Name { get; set; }
        [BsonRequired]
        public string Description { get; set; }
        
        public string CaseAssigneeTypeCode { get; set; }

        public int CaseAssignmentRuleOrder { get; set; }


        [BsonIgnore, JsonIgnore]
        public CaseAssigneeType CaseAssigneeType
        {
            get { return GetReferenceValue<CaseAssigneeType>(CaseAssigneeTypeCode, cat => cat.Code, cat => CaseAssigneeType.GetByCode(cat, CustomerId)); }
            set { CaseAssigneeTypeCode = SetReferenceValue(value, cat => cat.Code); }
        }
        [BsonIgnoreIfNull]
        public List<CaseAssignmentRule> CaseAssignmentRules { get; set; }

        /// <summary>
        /// Gets or sets the behavior.
        /// </summary>
        /// <value>
        /// The behavior.
        /// </value>
        [BsonIgnoreIfNull]
        public CaseAssignmentRuleBehavior? Behavior { get; set; }

    }
}
