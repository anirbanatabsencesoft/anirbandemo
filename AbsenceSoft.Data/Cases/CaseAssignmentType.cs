﻿using AbsenceSoft.Data.Customers;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Cases
{
    /// <summary>
    /// Customer level entity for storing a particular type/role for case assignment
    /// </summary>
    public class CaseAssignmentType: BaseCustomerEntity<CaseAssignmentType>
    {
        /// <summary>
        /// The EmployerId - if null applies to all employers for a customer
        /// </summary>
        [BsonIgnoreIfNull]
        public string EmployerId { get; set; }

        [BsonIgnore]
        public Employer Employer
        {
            get { return GetReferenceValue<Employer>(this.EmployerId); }
            set { this.EmployerId = SetReferenceValue<Employer>(value); }
        }

        /// <summary>
        /// The name of this case assignee type
        /// </summary>
        [BsonRequired]
        public string Name { get; set; }

        /// <summary>
        /// The number of this type that can be assigned to one case - if null there is no limit
        /// </summary>
        public int? MaxAllowedPerCase { get; set; }

        
    }
}
