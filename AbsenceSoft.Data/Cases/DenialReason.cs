﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;
using System;

namespace AbsenceSoft.Data.Cases
{
    /// <summary>
    /// Allows denial reason for case to be end user configurable
    /// </summary>
    [Serializable]
    public class DenialReason : BaseEmployerOverrideableEntity<DenialReason>
    {

        public DenialReason()
        {
            AbsenceReasons = new List<string>();
            PolicyTypes = new List<string>();
            PolicyCodes = new List<string>();
            AccommodationTypes = new List<string>();
        }

        /// <summary>
        /// The description of this denial reason
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The order of this denial reason in UI dropdown
        /// </summary>
        public int? Order { get; set; }

        /// <summary>
        /// The denial reason is not allowed to be overridden as this is being used in conditional statement
        /// </summary>
        public bool Locked { get; set; }

        /// <summary>
        /// Gets or sets the list of absence reason for case
        /// </summary>
        [BsonIgnoreIfNull]
        public List<string> AbsenceReasons { get; set; }

        /// <summary>
        /// Gets or sets the target.
        /// </summary>
        /// <value>
        /// The target.
        /// </value>
        public DenialReasonTarget Target { get; set; }

        /// <summary>
        /// Gets or sets the list of policy type for case
        /// </summary>
        [BsonIgnoreIfNull]
        public List<string> PolicyTypes { get; set; }

        /// <summary>
        /// Gets or sets the list of policy code for case
        /// </summary>
        [BsonIgnoreIfNull]
        public List<string> PolicyCodes { get; set; }

        /// <summary>
        /// Gets or sets the list of accommodation type for case
        /// </summary>
        [BsonIgnoreIfNull]
        public List<string> AccommodationTypes { get; set; }
    }
}
