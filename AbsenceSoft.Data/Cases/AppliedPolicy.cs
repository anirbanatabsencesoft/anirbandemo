﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Data.Cases
{
    /// <summary>
    /// Represents a policy that has been selected and applied to a case for a specified
    /// duration. One or more policies may be applied to a case and one or more applied
    /// policies of the exact same type may be applied to a case, even if thier dates overlap,
    /// they will simply only be counted once for each minute of applicable usage.
    /// </summary>
    [Serializable]
    public class AppliedPolicy : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppliedPolicy"/> class.
        /// </summary>
        public AppliedPolicy()
        {
            Status = EligibilityStatus.Pending;
            Usage = new List<AppliedPolicyUsage>();
            //Usage = new List<PolicyUsage>();              should be on AppliedPolicyRange
            RuleGroups = new List<AppliedRuleGroup>();
            PolicyDateOverridden = new UserOverrideableValue<bool>();
            Selection = null;
        }

        /// <summary>
        /// Gets or sets a copy of the policy that was applied.
        /// </summary>
        public Policy Policy { get; set; }

        /// <summary>
        /// Gets or sets a copy of the passed policy crosswalk codes
        /// </summary>
        public List<PolicyCrosswalkCode> PolicyCrosswalkCodes { get; set; }

        /// <summary>
        /// Gets or sets a copy of the policy absence reason that was applied as a shortcut.
        /// </summary>
        public PolicyAbsenceReason PolicyReason { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the policy uses will be share with spouse
        /// spouses that work for the same employer taking leave for this same policy absence reason
        /// for the same person (must be selected in the UI, not automatic).
        /// </summary>
        [BsonIgnoreIfNull]
        public bool? IsCombinedForSpouses { get; set; }

        /// <summary>
        /// Gets or sets the eligibility status for the applied policy.
        /// </summary>
        public EligibilityStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the date that the policy starts to apply to the case.
        /// </summary>
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the optional end date that the policy applies to the case.
        /// </summary>
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime? EndDate { get; set; }


        /// <summary>
        /// Gets or sets the type of the policy behavior.
        /// </summary>
        /// <value>
        /// The type of the policy behavior.
        /// </value>
        [BsonIgnore]
        public PolicyBehavior PolicyBehaviorType
        {
            get
            {
                if (PolicyReason != null && PolicyReason.PolicyBehaviorType.HasValue)
                {
                    return PolicyReason.PolicyBehaviorType.Value;
                }
                else if (Policy != null && Policy.PolicyBehaviorType.HasValue)
                {
                    return Policy.PolicyBehaviorType.Value;
                }
                else
                {
                    return PolicyBehavior.ReducesWorkWeek;
                }
            }
        }

        /// <summary>
        /// Gets the calculated Exhaustion date based on the usage and gets the earliest
        /// date the policy is at a status of Exhausted.
        /// </summary>
        [BsonIgnore]
        public DateTime? FirstExhaustionDate
        {
            get
            {
                if (Usage != null)
                {
                    var usage = Usage
                        .Where(u => u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted)
                        .OrderBy(u => u.DateUsed)
                        .FirstOrDefault();
                    if (usage != null)
                        return usage.DateUsed;
                }

                return null;
            }
        }//FirstExhaustionDate

        /// <summary>
        /// Gets or sets the time used for the policy.
        /// </summary>
        public List<AppliedPolicyUsage> Usage { get; set; }

        /// <summary>
        /// Gets or sets the applied rule groups that were used for eligibility 
        /// along with the rule group status, rule statuses, etc.
        /// </summary>
        public List<AppliedRuleGroup> RuleGroups { get; set; }

        /// <summary>
        /// Gets or sets the applied rule groups that were used for selection
        /// along with the rule group status, rule statuses, etc.
        /// </summary>
        [BsonIgnoreIfNull]
        public List<AppliedRuleGroup> Selection { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not this policy was manually
        /// added to the absence and therefore should not have eligibility rules run against it.
        /// </summary>
        public bool ManuallyAdded { get; set; }

        public UserOverrideableValue<bool> PolicyDateOverridden { get; set; }

        /// <summary>
        /// Gets or sets a note about why the policy was manually added to the employe's case.
        /// </summary>
        [BsonIgnoreIfNull]
        public string ManuallyAddedNote { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is off set.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is off set; otherwise, <c>false</c>.
        /// </value>
        [BsonIgnoreIfDefault, BsonDefaultValue(false)]
        public bool IsOffSet { get; set; }

        /// <summary>
        /// this will tell if Eligibility is required to re-evaluated.
        /// </summary>
        [BsonIgnore]
        public bool? ReevaluateEligibility { get; set; }

        /// <summary>
        /// This will tell is if Elimination is already applied 
        /// </summary>
        [BsonIgnore]
        public bool IsEliminationApplied { get; set; }
    }
}
