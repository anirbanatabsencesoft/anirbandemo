﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace AbsenceSoft.Data.Cases
{
    [Serializable]
    public class WorkRelatedInfo : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkRelatedInfo"/> class.
        /// </summary>
        public WorkRelatedInfo() : base()
        {
            Reportable = true;
        }

        /// <summary>
        /// Gets or sets the illness or injury date.
        /// </summary>
        /// <value>
        /// The illness or injury date.
        /// </value>
        [BsonIgnoreIfNull]
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime? IllnessOrInjuryDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="WorkRelatedInfo"/> is reportable.
        /// Defaults to <c>true</c>.
        /// </summary>
        /// <value>
        ///   <c>true</c> if reportable; otherwise, <c>false</c>. Defaults to <c>true</c>.
        /// </value>
        [BsonDefaultValue(true), DefaultValue(true)]
        public bool Reportable { get; set; }

        /// <summary>
        /// Gets or sets the health care private.
        /// </summary>
        /// <value>
        /// The health care private.
        /// </value>
        [BsonIgnoreIfNull]
        public bool? HealthCarePrivate { get; set; }

        /// <summary>
        /// Gets or sets the where occurred.
        /// </summary>
        /// <value>
        /// The where occurred.
        /// </value>
        [BsonIgnoreIfNull]
        public string WhereOccurred { get; set; }
        
        /// <summary>
        /// Gets or sets the injury office location org code.
        /// </summary>
        /// <value>
        /// The injury location org code.
        /// </value>
        [BsonIgnoreIfNull]
        public string InjuryLocation { get; set; }
        /// <summary>
        /// Gets or sets the Injured Body Part. This is driven/populated by a user configured select list.
        /// </summary>
        /// <value>
        /// The Injured Body Part.
        /// </value>
        [BsonIgnoreIfNull]
        public string InjuredBodyPart { get; set; }

        /// <summary>
        /// Gets or sets the classification.
        /// </summary>
        /// <value>
        /// The classification.
        /// </value>
        [BsonIgnoreIfNull]
        public WorkRelatedCaseClassification? Classification { get; set; }

        /// <summary>
        /// Gets or sets the type of injury.
        /// </summary>
        /// <value>
        /// The type of injury.
        /// </value>
        [BsonIgnoreIfNull]
        public WorkRelatedTypeOfInjury? TypeOfInjury { get; set; }

        /// <summary>
        /// Gets or sets the days away from work.
        /// </summary>
        /// <value>
        /// The days away from work.
        /// </value>
        [BsonIgnoreIfNull]
        public int? DaysAwayFromWork { get; set; }

        /// <summary>
        /// Gets or sets the override days away from work.
        /// </summary>
        /// <value>
        /// The override days away from work.
        /// </value>
        [BsonIgnoreIfNull]
        public int? OverrideDaysAwayFromWork { get; set; }

        /// <summary>
        /// Gets or sets the days on job transfer or restriction.
        /// </summary>
        /// <value>
        /// The days on job transfer or restriction.
        /// </value>
        [BsonIgnoreIfNull]
        public int? DaysOnJobTransferOrRestriction { get; set; }

        /// <summary>
        /// Gets or sets the days on job transfer or restriction.
        /// </summary>
        /// <value>
        /// The days on job transfer or restriction.
        /// </value>
        [BsonIgnoreIfNull]
        public int? CalculatedDaysOnJobTransferOrRestriction { get; set; }

        /// <summary>
        /// Gets or sets the provider employee contact.
        /// </summary>
        /// <value>
        /// The provider employee contact.
        /// </value>
        [BsonIgnoreIfNull]
        public EmployeeContact Provider { get; set; }

        /// <summary>
        /// Gets or sets the emergency room.
        /// </summary>
        /// <value>
        /// The emergency room.
        /// </value>
        [BsonIgnoreIfNull]
        public bool? EmergencyRoom { get; set; }

        /// <summary>
        /// Gets or sets the hospitalized overnight.
        /// </summary>
        /// <value>
        /// The hospitalized overnight.
        /// </value>
        [BsonIgnoreIfNull]
        public bool? HospitalizedOvernight { get; set; }

        /// <summary>
        /// Gets or sets the time employee began work.
        /// </summary>
        /// <value>
        /// The time employee began work.
        /// </value>
        [BsonIgnoreIfNull]
        public TimeOfDay? TimeEmployeeBeganWork { get; set; }

        /// <summary>
        /// Gets or sets the time of event.
        /// </summary>
        /// <value>
        /// The time of event.
        /// </value>
        [BsonIgnoreIfNull]
        public TimeOfDay? TimeOfEvent { get; set; }

        /// <summary>
        /// Gets or sets the activity before incident.
        /// </summary>
        /// <value>
        /// The activity before incident.
        /// </value>
        [BsonIgnoreIfNull]
        public string ActivityBeforeIncident { get; set; }

        /// <summary>
        /// Gets or sets the what happened.
        /// </summary>
        /// <value>
        /// The what happened.
        /// </value>
        [BsonIgnoreIfNull]
        public string WhatHappened { get; set; }

        /// <summary>
        /// Gets or sets the injury or illness.
        /// </summary>
        /// <value>
        /// The injury or illness.
        /// </value>
        [BsonIgnoreIfNull]
        public string InjuryOrIllness { get; set; }

        /// <summary>
        /// Gets or sets the what harmed the employee.
        /// </summary>
        /// <value>
        /// The what harmed the employee.
        /// </value>
        [BsonIgnoreIfNull]
        public string WhatHarmedTheEmployee { get; set; }

        /// <summary>
        /// Gets or sets the date of death.
        /// </summary>
        /// <value>
        /// The date of death.
        /// </value>
        [BsonIgnoreIfNull]
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime? DateOfDeath { get; set; }

        /// <summary>
        /// Gets or sets the sharps.
        /// </summary>
        /// <value>
        /// The sharps.
        /// </value>
        [BsonIgnoreIfNull]
        public bool? Sharps { get; set; }

        /// <summary>
        /// Gets or sets the sharps information.
        /// </summary>
        /// <value>
        /// The sharps information.
        /// </value>
        [BsonIgnoreIfNull]
        public WorkRelatedSharpsInfo SharpsInfo { get; set; }

        /// <summary>
        /// Gets or sets the Is work related info status
        /// </summary>
        /// <value>
        /// Is work related or not
        /// </value>
        [BsonIgnoreIfNull]
        public bool IsWorkRelated { get; set; }
    }
}
