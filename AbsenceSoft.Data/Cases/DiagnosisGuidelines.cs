﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using AbsenceSoft.Common.Properties;

namespace AbsenceSoft.Data.Cases
{
    /// <summary>
    /// A guidelines from ODG for specifc diagnosis code
    /// </summary>
    [Serializable]
    public class DiagnosisGuidelines : BaseEntity<DiagnosisGuidelines>
    {
        /// <summary>
        /// Diagnosis Code (ICD-9 or ICD-10)
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// The description of code
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Return-To-Work Summary Guidelines
        /// </summary>
        [BsonElement("summary")]
        public RtwSummaryGuidelines RtwSummary { get; set; }

        /// <summary>
        /// Return-To-Work Best Practices
        /// </summary>
        [BsonElement("bp")]
        public List<RtwBestPractice> RtwBestPractices { get; set; }

        /// <summary>
        /// Chiropractical Guidelines
        /// </summary>
        [BsonElement("chiro")]
        public List<ChiropracticalGuideline> Chiropractical { get; set; }

        /// <summary>
        /// Phisical Therapy Guidelines
        /// </summary>
        [BsonElement("pt")]
        public List<PhisicalTherapyGuideline> PhisicalTherapy { get; set; }

        /// <summary>
        /// Activity Modifications/Job Restrictions Guidelines
        /// </summary>
        [BsonElement("activitymod")]
        public List<ActivityModification> ActivityModifications { get; set; }

        /// <summary>
        /// Gets or sets the record expiration date
        /// </summary>
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime ExpirationDate { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="DiagnosisGuidelines"/> class.
        /// </summary>
        public DiagnosisGuidelines()
        {
            RtwBestPractices = new List<RtwBestPractice>();
            Chiropractical = new List<ChiropracticalGuideline>();
            PhisicalTherapy = new List<PhisicalTherapyGuideline>();
            ActivityModifications = new List<ActivityModification>();

            //Set record expiration date
            ExpirationDate = DateTime.UtcNow.AddDays(Settings.Default.DiagnosisGuidelines_CacheDuration).ToMidnight();
        }
    }

    /// <summary>
    /// Return-To-Work Sumary Guidelines
    /// </summary>
    [Serializable]
    public class RtwSummaryGuidelines : BaseNonEntity
    {
        /// <summary>
        /// Mid Range number of claims
        /// </summary>
        public int ClaimsMidRange { get; set; }

        /// <summary>
        /// At Risk number of claims
        /// </summary>
        public int ClaimsAtRisk { get; set; }

        /// <summary>
        /// Mid Range number of absences
        /// </summary>
        public int AbsencesMidRange { get; set; }

        /// <summary>
        /// At Risk Number of absences
        /// </summary>
        public int AbsencesAtRisk { get; set; }
    }

    /// <summary>
    /// Return-To-Work Best Practice Guidelines
    /// </summary>
    [Serializable]
    public class RtwBestPractice : BaseNonEntity
    {
        /// <summary>
        /// Primary RTW path
        /// </summary>
        public string RtwPath { get; set; }

        /// <summary>
        /// Minimum number of days
        /// </summary>
        [BsonIgnoreIfNull]
        public int? MinDays { get; set; }

        /// <summary>
        /// Max number of days
        /// </summary>
        [BsonIgnoreIfNull]
        public int? MaxDays { get; set; }

        /// <summary>
        /// Returns String represantation of days
        /// </summary>
        [BsonIgnore]
        public string Days
        {
            get
            {
                string days = String.Empty;

                if (MinDays == null && MaxDays == null)
                    return days;

                if (MinDays != null && MinDays.HasValue)
                    days = MinDays.Value.ToString();

                if (MaxDays != null && MaxDays.HasValue)
                {
                    if (MaxDays.Value < 999)
                    {
                        if (!String.IsNullOrWhiteSpace(days))
                            days = days + " - ";

                        days = days + MaxDays.Value.ToString() + " days";
                    }
                    else
                    {
                        if (!String.IsNullOrWhiteSpace(days))
                            days = days + " to ";

                        days = days + "indefinite";
                    }
                }

                return days;
            }

        }
    }


    /// <summary>
    /// Activity Modification/Job Restriction Guideline
    /// </summary>
    [Serializable]
    public class ActivityModification : BaseNonEntity
    {
        /// <summary>
        /// Type of Job (Example: Modified work, Regular work)
        /// </summary>
        public string JobType { get; set; }

        /// <summary>
        /// Guidelines for Job modifications
        /// </summary>
        public string JobModifications { get; set; }

    }

    /// <summary>
    /// Phisical Therapy Guidelines
    /// </summary>
    [Serializable]
    public class PhisicalTherapyGuideline : BaseNonEntity
    {
        /// <summary>
        /// Primary path for guideline
        /// </summary>
        public string PrimaryPath { get; set; }

        /// <summary>
        /// Number of visits
        /// </summary>
        [BsonIgnoreIfNull]
        public int? Visits { get; set; }

        /// <summary>
        /// Number of Weeks
        /// </summary>
        [BsonIgnoreIfNull]
        public int? Weeks { get; set; }
    }

    /// <summary>
    /// Chiropractical Guidelines
    /// </summary>
    [Serializable]
    public class ChiropracticalGuideline : BaseNonEntity
    {
        /// <summary>
        /// Primary path for guideline
        /// </summary>
        public string PrimaryPath { get; set; }

        /// <summary>
        /// Number of visits
        /// </summary>
        [BsonIgnoreIfNull]
        public int? Visits { get; set; }

        /// <summary>
        /// Number of weeks
        /// </summary>
        [BsonIgnoreIfNull]
        public int? Weeks { get; set; }
    }

    /// <summary>
    /// Comorbity arguments and results.
    /// </summary>
    [Serializable]
    public class CoMorbidityGuideline
    {
        public CoMorbidityArgs Args { get; set; }

        #region Risk Assessment
        public decimal RiskAssessmentScore { get; set; }
        public string RiskAssessmentStatus { get; set; }

        #endregion


        #region Adjusted Summary Guidelines
        public decimal? MidrangeAllAbsence { get; set; }
        public decimal? AtRiskAllAbsence { get; set; }

        #endregion

        #region Adjusted Duration
        public decimal BestPracticesDays { get; set; }

        #endregion

        #region Others
        public decimal? ActualDaysLost { get; set; }

        #endregion

        public CoMorbidityGuideline()
        {
            this.Args = new CoMorbidityArgs();
        }


        /// <summary>
        /// Subclass to hold all the arguments for client query
        /// </summary>
        [Serializable]
        [XmlRoot(ElementName = "cbcalc", IsNullable = false)]
        public class CoMorbidityArgs
        {
            [XmlIgnore]
            public string ICDDiagnosisCodeString { get; set; }
            [XmlElement("country")]
            public string CountryCode { get; set; }
            [XmlElement("state")]
            public string StateCode { get; set; }
            [XmlElement("age")]
            public int? Age { get; set; }
            [XmlElement("bpcode")]
            public string BodyPartCode { get; set; }
            [XmlElement("noicode")]
            public string NatureOfInjuryCode { get; set; }

            [XmlElement("injurydate")]
            public String StartDate { get; set; }
            [XmlElement("rtwdate")]
            public String EndDate { get; set; }
            [XmlIgnore]
            public bool ReCalculate { get; set; }
            [XmlElement("fulloutput")]
            public string FullOutput { get; set; }
            [XmlElement("jobclass")]
            public int? JobActivityLevel { get; set; }

            [XmlIgnore]
            public string ICDProcedureCodeString { get; set; }
            [XmlIgnore]
            public string HCPCSCodeString { get; set; }
            [XmlIgnore]
            public string CPTProcedureCodeString { get; set; }
            [XmlIgnore]
            public string NDCProcedureCodeString { get; set; }

            #region Confounding Factors
            [XmlElement("cfactors")]
            public ConfoundingFactors CF { get; set; }

            public class ConfoundingFactors
            {
                /// <summary>
                /// Valid values are 1 or 0
                /// </summary>
                [XmlElement("depression")]
                public int Depression { get; set; }

                /// <summary>
                /// Valid values are 1 or 0
                /// </summary>
                [XmlElement("diabetes")]
                public int Diabetes { get; set; }

                /// <summary>
                /// Valid values are 1 or 0
                /// </summary>
                [XmlElement("hypertension")]
                public int Hypertension { get; set; }

                /// <summary>
                /// Valid values are 1 or 0
                /// </summary>
                [XmlElement("legalrep")]
                public int LegalRepresentation { get; set; }

                /// <summary>
                /// Valid values are 1 or 0
                /// </summary>
                [XmlElement("obesity")]
                public int Obesity { get; set; }

                /// <summary>
                /// Valid values are 1 or 0
                /// </summary>
                [XmlElement("smoker")]
                public int Smoker { get; set; }

                /// <summary>
                /// Valid values (0 – Not Applied, 1 - Less than 15 Days, 2 – 15 to 30 Days , 3 - > 30 Days)
                /// </summary>
                [XmlElement("opioids")]
                public int OPIOId { get; set; }

                /// <summary>
                /// Valid values are 1 or 0
                /// </summary>
                [XmlElement("subabuse")]
                public int SubstanceAbuse { get; set; }

                /// <summary>
                /// Valid values are 1 or 0
                /// </summary>
                [XmlElement("surgery")]
                public int SurgeryOrHospitalStay { get; set; }
            }
            #endregion

            #region Proc Summary
            [XmlElement("procsummary")]
            public ProcSummary PS { get; set; }

            public class ProcSummary
            {
                [XmlIgnore]
                public string ICDProcedureCodeString { get; set; }
                [XmlIgnore]
                public string HCPCSCodeString { get; set; }
                [XmlIgnore]
                public string CPTProcedureCodeString { get; set; }
                [XmlIgnore]
                public string NDCProcedureCodeString { get; set; }

                [XmlArray("icd")]
                [XmlArrayItem("code")]
                [BsonIgnore]
                public List<string> ICDProcedureCodes
                {
                    get
                    {
                        try
                        {
                            return ICDProcedureCodeString.Split(new[] { ',', ';', '|' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        }
                        catch
                        {
                            return new List<string>();
                        }
                    }
                }

                [XmlArray("hcpcs")]
                [XmlArrayItem("code")]
                [BsonIgnore]
                public List<string> HCPCSCodes
                {
                    get
                    {
                        try
                        {
                            return HCPCSCodeString.Split(new[] { ',', ';', '|' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        }
                        catch
                        {
                            return new List<string>();
                        }
                    }
                }

                [XmlArray("cpt")]
                [XmlArrayItem("code")]
                [BsonIgnore]
                public List<string> CPTProcedureCodes
                {
                    get
                    {
                        try
                        {
                            return CPTProcedureCodeString.Split(new[] { ',', ';', '|' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        }
                        catch
                        {
                            return new List<string>();
                        }
                    }
                }

                [XmlArray("ndc")]
                [XmlArrayItem("code")]
                [BsonIgnore]
                public List<string> NDCProcedureCodes
                {
                    get
                    {
                        try
                        {
                            return NDCProcedureCodeString.Split(new[] { ',', ';', '|' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        }
                        catch
                        {
                            return new List<string>();
                        }
                    }
                }
            }
            #endregion


            #region Derived Properties. Not be saved in database but to be set as output for argument.
            [XmlArray("icdcodes")]
            [XmlArrayItem("code")]
            [BsonIgnore]
            public List<string> ICDDiagnosisCodes
            {
                get
                {
                    try
                    {
                        return this.ICDDiagnosisCodeString.Split(new[] { ',', ';', '|' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    }
                    catch
                    {
                        return new List<string>();
                    }
                }
            }
            #endregion

            #region Overridden method
            public override string ToString()
            {
                XmlSerializer xmlSerializer = new XmlSerializer(this.GetType());
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");

                using (System.IO.StringWriter textWriter = new System.IO.StringWriter())
                {
                    xmlSerializer.Serialize(textWriter, this, ns);
                    var xml = textWriter.ToString();
                    System.Xml.Linq.XElement doc = System.Xml.Linq.XElement.Parse(xml);
                    doc.Descendants().Where(e => string.IsNullOrEmpty(e.Value)).Remove();
                    return doc.ToString();
                }
            }

            #endregion

            public CoMorbidityArgs()
            {
                this.CF = new ConfoundingFactors();
                this.PS = new ProcSummary();
            }
        }
    }

}
