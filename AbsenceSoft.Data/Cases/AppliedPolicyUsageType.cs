﻿using AbsenceSoft.Data.Enums;
using System;

namespace AbsenceSoft.Data.Cases
{
    [Serializable]
    public class AppliedPolicyUsageType
    {
        /// <summary>
        /// What is the unit type measurement
        /// </summary>
        public EntitlementType UnitType { get; set; }

        /// <summary>
        /// How many days are in that unit type
        /// </summary>
        public double BaseUnit { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return $"{UnitType} = n / {BaseUnit}";
        }
    }
}
