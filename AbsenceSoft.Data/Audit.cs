﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Audit;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoRepository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace AbsenceSoft.Data
{
    /// <summary>
    /// The type of audit record enum
    /// </summary>
    public enum AuditType : int
    {
        /// <summary>
        /// Indicates a delete operation, 0x0
        /// </summary>
        Delete = 0x0,
        /// <summary>
        /// Indicates a save operation, 0x1
        /// </summary>
        Save = 0x1,
    }

    /// <summary>
    /// The Audit entity is used for auditing other entities.
    /// </summary>
    /// <typeparam name="TEntity">The entity type that this audit is for.</typeparam>
    [Serializable, AuditClassNo]
    public class Audit<TEntity> : Entity where TEntity : BaseEntity<TEntity>, new()
    {
        [BsonIgnore]
        public static readonly string TypeName = typeof(TEntity).Name;

        /// <summary>
        /// Gets the audit type, e.g. Delete or Save, etc.
        /// </summary>
        public AuditType AuditType { get; internal set; }

        /// <summary>
        /// Gets the updated entity value that was updated (this is the updated value, not the prior value).
        /// </summary>
        public TEntity Value { get; set; }

        /// <summary>
        /// Gets or sets the date that this entity was created.
        /// </summary>
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc, Representation = BsonType.DateTime), BsonRequired, BsonElement("cdt")]
        public DateTime CreatedDate { get; private set; }

        /// <summary>
        /// Gets or sets the user's email address whom created the entity.
        /// </summary>
        [BsonElement("cby"), BsonRequired, BsonRepresentation(BsonType.ObjectId)]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Upserts the audit record to the database and if it does not already exist, sets its new Id property as
        /// well.
        /// </summary>
        public virtual Audit<TEntity> Save()
        {
            CreatedDate = DateTime.UtcNow;
            if (string.IsNullOrWhiteSpace(CreatedBy) && User.Current != null)
                CreatedBy = User.Current.Id;

            if (!AbsenceSoft.Common.Properties.Settings.Default.Audit)
                return this;

            using (new InstrumentationContext("Audit.{0}.Save", TypeName))
            {
                return Repository.Update(this);
            }
        }


        [BsonIgnore]
        private static object _repoSync = new object();
        [BsonIgnore]
        private static MongoRepository<Audit<TEntity>> _myRepository = null;
        /// <summary>
        /// Gets the repository used for manipulating this instance.
        /// </summary>
        [BsonIgnore]
        public static MongoRepository<Audit<TEntity>> Repository
        {
            get
            {
                if (_myRepository != null)
                    return _myRepository;

                lock (_repoSync)
                {
                    if (_myRepository != null)
                        return _myRepository;

                    var connString = ConfigurationManager.ConnectionStrings["MongoServerSettings_Audit"] ?? ConfigurationManager.ConnectionStrings["MongoServerSettings"];
                    if (connString == null)
                        throw new ApplicationException("MongoDB connection string not found.");

                    _myRepository = new MongoRepository<Audit<TEntity>>(connString.ConnectionString, TypeName);
                }

                return _myRepository;
            }
        }//end: Repository


        /// <summary>
        /// Gets the audit entries for the given entity by the original entity's ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static IEnumerable<Audit<TEntity>> GetById(string id)
        {
            using (new InstrumentationContext("Audit.{0}.GetById", TypeName))
            {
                if (string.IsNullOrWhiteSpace(id))
                    return new List<Audit<TEntity>>(0);

                return Repository.Where(a => a.Value.Id == id);
            }
        }

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching.
        /// </summary>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static IQueryable<Audit<TEntity>> AsQueryable()
        {
            return Repository.AsQueryable();
        }
    }
}
