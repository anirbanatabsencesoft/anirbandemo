﻿using System;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Data
{
    /// <summary>
    /// This class is a helper class to convert strings of data types from a database into a common DbDataType format.
    /// </summary>
    public static class DbDataTypeConverter
    {
        /// <summary>
        /// This method converts a string version of a Postgres datatype and returns a DbDataType version.
        /// </summary>
        /// <param name="dataType">The datatype to convert to DbDataType.</param>
        /// <returns>DbDataType from a Postgres string datatype.</returns>
        public static DbDataType GetDbDataTypeFromPostgres(string dataType)
        {
            if (string.IsNullOrWhiteSpace(dataType))
                throw new ArgumentException("Argument not allowed. Please choose a valid datatype to convert.", dataType);

            // Please refer to: https://www.postgresql.org/docs/9.5/static/datatype.html
            switch (dataType.ToLowerInvariant())
            {
                case "bit":
                case "bytea":
                    return DbDataType.Binary;
                case "bit varying":
                case "varbit":
                case "jsonb":
                    return DbDataType.VarBinary;
                case "int":
                case "int4":
                case "integer":
                case "serial":
                case "serial4":
                    return DbDataType.Int;
                case "bigint":
                case "int8":
                case "bigserial":
                case "serial8":
                    return DbDataType.BigInt;
                case "smallint":
                case "int2":
                case "smallserial":
                case "serial2":
                    return DbDataType.SmallInt;
                case "boolean":
                case "bool":
                    return DbDataType.Binary;
                case "double precision":
                case "float8":
                    return DbDataType.Double;
                case "numeric":
                    return DbDataType.Numeric;
                case "decimal":
                case "money":
                    return DbDataType.Decimal;
                case "text":
                case "json":
                    return DbDataType.Text;
                case "character":
                case "char":
                    return DbDataType.Char;
                case "character varying":
                case "varchar":
                    return DbDataType.VarChar;
                case "date":
                    return DbDataType.Date;
                case "real":
                case "float4":
                    return DbDataType.Real;
                case "time":
                case "timetz":
                case "time without time zone":
                case "time with time zone":
                    return DbDataType.Time;
                case "timestamp":
                case "timestamptz":
                case "timestamp without time zone":
                case "timestamp with time zone":
                    return DbDataType.Timestamp;
                case "uuid":
                    return DbDataType.UniqueIdentifier;
                default:
                    throw new ArgumentException("Argument not allowed. Please choose a valid datatype to convert.", dataType);
            }
        }
    }
}
