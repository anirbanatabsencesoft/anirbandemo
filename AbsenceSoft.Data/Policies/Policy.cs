﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbsenceSoft.Data.Policies
{
    /// <summary>
    /// Core Policy Entity
    /// </summary>
    [Serializable]
    public class Policy : BaseEmployerOverrideableEntity<Policy>
    {
        public Policy()
        {
            Code = Guid.NewGuid().ToString();
            EffectiveDate = DateTime.MinValue;
            RuleGroups = new List<PolicyRuleGroup>();
            AbsenceReasons = new List<PolicyAbsenceReason>();
            ConsecutiveTo = new List<string>();
        }

        /// <summary>
        /// Gets or sets the name of the policy.
        /// </summary>
        [BsonRequired]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets an optional and helpful description of the policy.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the effective date of the policy when applicable. Default is
        /// <c>DateTime.MinValue</c>.
        /// </summary>
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime EffectiveDate { get; set; }

        /// <summary>
        /// Gets or sets the type of policy (for descriptive/grouping and reporting purposes).
        /// </summary>
        public PolicyType PolicyType { get; set; }

        /// <summary>
        /// Gets or sets the global rule groups that are applied to all absence reasons for the policy,
        /// typically selection and eligibility rules that blanket the policy, e.g. FMLA 50 in 75 rule
        /// or FMLA's 1250 hours min rule.
        /// </summary>
        public List<PolicyRuleGroup> RuleGroups { get; set; }

        /// <summary>
        /// Gets or sets the list of absence reasons and each one's own specific rules, etc.
        /// There must be at least 1 reason in this collection in order for a policy to be selected.
        /// </summary>
        public List<PolicyAbsenceReason> AbsenceReasons { get; set; }

        /// <summary>
        /// Gets or set's the policy's valid work country, if any, otherwise this is <c>null</c>.
        /// This is a short-cut policy selection rule.
        /// </summary>
        [BsonDefaultValue("US")]
        public string WorkCountry { get; set; }

        /// <summary>
        /// Gets or sets the policy's valid work state, if any, otherwise this is <c>null</c>.
        /// This is a short-cut policy selection rule.
        /// </summary>
        public string WorkState { get; set; }

        /// <summary>
        /// Gets or sets the type of the policy behavior.
        /// </summary>
        /// <value>
        /// The type of the policy behavior.
        /// </value>
        [BsonIgnoreIfNull]
        public PolicyBehavior? PolicyBehaviorType { get; set; }

        /// <summary>
        /// Gets or set's the policy's valid residence country, if any, otherwise this is <c>null</c>.
        /// This is a short-cut policy selection rule.
        /// </summary>
        [BsonDefaultValue("US")]
        public string ResidenceCountry { get; set; }

        /// <summary>
        /// Gets or sets the policy valid residence/home state, if any, otherwise this is <c>null</c>.
        /// This is a short-cut policy selection rule.
        /// </summary>
        public string ResidenceState { get; set; }

        /// <summary>
        /// Gets or sets the policy absence reason's valid gender, if any, otherwise this is <c>null</c> for
        /// any gender. This is a short-cut policy selection rule.
        /// </summary>
        public Gender? Gender { get; set; }

        /// <summary>
        /// The name's of the policies that must be exhausted before this policy
        /// can be applied (codes and not IDs to make end user friendly)
        /// </summary>
        public List<string> ConsecutiveTo { get; set; }

        /// <summary>
        /// As an AT client, I need to designate a substitute policy on a case when a specific primary policy is ineligible. 
        /// This substitute policy should be applied automatically and consistently to provide coverage to an employee when the primary policy is not running
        /// The property value will be name's of the policies that must be ineligible before this policy
        /// can be applied (codes and not IDs to make end user friendly)
        /// </summary>
        public List<string> SubstituteFor { get; set; }


        /// <summary>
        /// Gets whether or not this policy is disabled for a specific customer/employer or not
        /// </summary>
        [BsonIgnore]
        public bool IsDisabled { get; set; }
        
        /// <summary>
        /// Is this policy is specific to Airline flight crew, If 'true' then this may be mutually exclusive to other similer policies
        /// </summary>
        public bool? AirlineFlightCrew { get; set; }

        /// <summary>
        /// Cleans this instance.
        /// </summary>
        public override void Clean()
        {
            base.Clean();
            if (RuleGroups != null)
                RuleGroups.ForEach(g => g.Clean());
            if (AbsenceReasons != null)
                AbsenceReasons.ForEach(r => r.Clean());
        }
    }
}
