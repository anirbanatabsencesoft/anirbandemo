﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Policies
{
    [Serializable]
    public class PolicyCrosswalkCode: BaseNonEntity
    {
        public PolicyCrosswalkCode()
        {

        }


        public PolicyCrosswalkCode(string typeCode, string typeName, string code)
            :this()
        {
            TypeCode = typeCode;
            TypeName = typeName;
            Code = code;
        }

        public string TypeCode { get; set; }

        public string TypeName { get; set; }

        public string Code { get; set; }
    }
}
