﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace AbsenceSoft.Data.Policies
{
    [Serializable]
    public class PolicyEvent
    {
        /// <summary>
        /// What case event to listen for
        /// </summary>
        public CaseEventType EventType {get; set;}
        
        /// <summary>
        /// What date field to work off of
        /// </summary>
        public EventDateType DateType { get; set; }

        /// <summary>
        /// Leave Period Permitted
        /// </summary>
        [BsonIgnoreIfNull]
        public int? EventPeriod { get; set; }
    }
}
