﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbsenceSoft.Data.Policies
{
    /// <summary>
    /// Represents a policy's absence reason. A policy may have one or more of these
    /// to comprise a complete set of policy variants that can be applied to a case.
    /// </summary>
    [Serializable]
    public class PolicyAbsenceReason : BaseNonEntity
    {
        public PolicyAbsenceReason()
        {
            ShowType = PolicyShowType.Always;
            EffectiveDate = DateTime.MinValue;
            RuleGroups = new List<PolicyRuleGroup>();
            PolicyEvents = new List<PolicyEvent>();
            PaymentTiers = new List<PaymentInfo>();
        }

        /// <summary>
        /// Gets or sets the reason id for the absence as it maps to a policy.
        /// </summary>
        //[BsonRepresentation(BsonType.ObjectId)]
        public string ReasonId { get; set; }

        /// <summary>
        /// Gets or sets the reason code for the absence
        /// </summary>
        public string ReasonCode { get; set; }       

        /// <summary>
        /// Gets or set the type of entitlement (time) for a policy that this rule grants. The
        /// Entitlement property represents the actual number or value based on this type.
        /// The default is 
        /// </summary>
        [BsonIgnoreIfNull]
        public EntitlementType? EntitlementType { get; set; }

        /// <summary>
        /// Gets or sets the entitlement (time) for the policy that this rule grants, the final
        /// entitlement is the greatest of the policy absence reason and all passing rules in all 
        /// passing groups for a given policy.
        /// </summary>
        [BsonIgnoreIfNull]
        public double? Entitlement { get; set; }

        /// <summary>
        /// Is there an elimination period before the entitlement can be used.
        /// If so, how is it calculated (WorkWeek, calendar months, etc)
        /// </summary>
        [BsonIgnoreIfNull]
        public EntitlementType? EliminationType { get; set; }

        /// <summary>
        /// How long is the elimination in units of Elimination type
        /// </summary>
        [BsonIgnoreIfNull]
        public double? Elimination { get; set; }

        /// <summary>
        /// is the elimination period included or exclude when the time frame is met. default is included
        /// </summary>
        [BsonIgnoreIfNull]
        public PolicyEliminationType? PolicyEliminationCalcType { get; set; }

        /// <summary>
        /// How much time is allowed per case. 
        /// The entitilement maybe for 12 weeks but you can only 
        /// take it 1 week per case
        /// </summary>
        [BsonIgnoreIfNull]
        public double? PerUseCap { get; set; }

        /// <summary>
        /// How is the cap measured
        /// It must be the same base type (calendar or percentage) as the
        /// entitilement type (please enforce this rule somewhere)
        /// </summary>
        [BsonIgnoreIfNull]
        public EntitlementType? PerUseCapType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the entitlement is a combined total max for
        /// spouses that work for the same employer taking leave for this same policy absence reason
        /// for the same person (must be selected in the UI, not automatic).
        /// </summary>
        [BsonDefaultValue(false)]
        public bool CombinedForSpouses { get; set; }

        /// <summary>
        /// Gets or sets the entitlement period type for the policy's absence reason. If null uses the employer's
        /// FMLCalc period
        /// </summary>
        [BsonIgnoreIfNull]
        public Enums.PeriodType? PeriodType { get; set; }

        /// <summary>
        /// Gets or sets the type of the policy behavior.
        /// </summary>
        /// <value>
        /// The type of the policy behavior.
        /// </value>
        [BsonIgnoreIfNull]
        public PolicyBehavior? PolicyBehaviorType { get; set; }

        /// <summary>
        /// Intermittent Restrictions Minimum Value
        /// </summary>
        [BsonIgnoreIfNull]
        public int? IntermittentRestrictionsMinimum { get; set; }

        /// <summary>
        /// Unit of Intermittent Restrictions Minimum 
        /// </summary>
        [BsonIgnoreIfNull]
        public Enums.EntitlementType? IntermittentRestrictionsMinimumTimeUnit { get; set; }

        /// <summary>
        /// Calc Type  of Intermittent Restrictions  
        /// </summary>
        [BsonIgnoreIfNull]
        public IntermittentRestrictions? IntermittentRestrictionsCalcType { get; set; }

        /// <summary>
        ///   Intermittent Restrictions Maximum Value
        /// </summary>
        [BsonIgnoreIfNull]
        public int? IntermittentRestrictionsMaximum { get; set; }

        /// <summary>
        ///  Unit of Intermittent Restrictions Maximum 
        /// </summary>
        [BsonIgnoreIfNull]
        public Enums.EntitlementType? IntermittentRestrictionsMaximumTimeUnit { get; set; }

        /// <summary>
        /// Gets or sets the period window for the policy that is used to measure prior/future usage, the
        /// final entitlement may be impacted by this window and the type of calculation that is applied.
        /// </summary>
        [BsonIgnoreIfNull]
        public double? Period { get; set; }

        /// <summary>
        /// Gets or sets the policy's absence reason effective date, if applicable.
        /// </summary>
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime EffectiveDate { get; set; }

        /// <summary>
        /// Gets or sets the allowed case types for this policy absence reason. Some reasons do not allow
        /// certain case types, e.g. Pregnancy doesn't allow intermittent time, duh, you can't be off and on
        /// pregnant unless you're an alien species, and then you wouldn't be covered under most employer, 
        /// federal or state policies anyway, 'cause you're from another planet, although certain immigration
        /// laws may preclude you to coverage, this tool will not allow it; except for members of the planet
        /// Krelug IV, which by nature mimics the representative physical laws of Earth to the extent of
        /// pregancy trimesters in equlivlent gender associative huminoids local to the the underground tunnels
        /// of the Algion Cluster, Sector 6.
        /// </summary>
        /// <remarks>Just kidding about the planet Krelug IV, it doesn't really count, they're only partially huminoid
        /// and not covered under FMLA because it's not part of the United States of America, instead it is a British colony.</remarks>
        public CaseType CaseTypes { get; set; }

        /// <summary>
        /// Gets or sets a value indicating as to whether or not the employer or law dictates that the employee be paid
        /// for time absent under this policy.
        /// </summary>
        public bool Paid { get; set; }

        /// <summary>
        /// If a policy is paid then at what what percentage is it paid at.
        /// 
        /// The value can change over time
        /// </summary>
        [BsonIgnoreIfNull]
        public List<PaymentInfo> PaymentTiers { get; set; }

        /// <summary>
        /// Gets or sets the policy reason's rule groups that are specific to this absence reason.
        /// </summary>
        [BsonIgnoreIfNull]
        public List<PolicyRuleGroup> RuleGroups { get; set; }

        /// <summary>
        /// Gets or sets the policy absence reason's valid work state, if any, otherwise this is <c>null</c>.
        /// This is a short-cut policy selection rule.
        /// </summary>
        [BsonIgnoreIfNull]
        public string WorkState { get; set; }

        /// <summary>
        /// Is this policy absence reason is specific to Airline flight crew, If 'true' then this may be mutually exclusive to other similer absence reasons
        /// </summary>
        [BsonIgnoreIfNull]
        public bool? AirlineFlightCrew { get; set; }

        /// <summary>
        /// Gets or sets the policy absence reason's valid work country, if any, otherwise this is <c>null</c>.
        /// This is a short-cut policy selection rule.
        /// </summary>
        [BsonDefaultValue("US")]
        public string WorkCountry { get; set; }

        /// <summary>
        /// Gets or sets the policy absence reason's valid residence/home state, if any, otherwise this is <c>null</c>.
        /// This is a short-cut policy selection rule.
        /// </summary>
        public string ResidenceState { get; set; }

        /// <summary>
        /// Gets or sets the policy absence reason's valid residence/home country, if any, otherwise this is <c>null</c>.
        /// This is a short-cut policy selection rule.
        /// </summary>
        [BsonDefaultValue("US")]
        public string ResidenceCountry { get; set; }

        /// <summary>
        /// Gets or sets the policy absence reason's valid gender, if any, otherwise this is <c>null</c> for
        /// any gender. This is a short-cut policy selection rule.
        /// </summary>
        [BsonIgnoreIfNull]
        public Gender? Gender { get; set; }

        /// <summary>
        /// Gets or sets the policy's general display type as to when and why it would be displayed to a user
        /// in a list (e.g. viewing all possibly eligible policies, etc.).
        /// </summary>
        public PolicyShowType ShowType { get; set; }

        /// <summary>
        /// Gets or sets the Absence Reason for this policy absence reason.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public AbsenceReason Reason
        {
            get { return GetReferenceValue<AbsenceReason>(this.ReasonId); }
            set
            {                
                this.ReasonId = SetReferenceValue(value);
                this.ReasonCode = AbsenceReason.GetById(this.ReasonId).Code;
            }
        }

        /// <summary>
        /// A list of events that can cause this policy for this reason to end.
        /// Effectively will set a list of policy events, that if met on the case will
        /// cause the end date of the applied policy to end.
        /// </summary>
        [BsonIgnoreIfNull]
        public List<PolicyEvent> PolicyEvents { get; set; }

        /// <summary>
        /// if this is a paid policy then this specifies if it is an offset payment
        /// </summary>
        [BsonIgnoreIfNull]
        public bool? AllowOffset { get; set; }

        /// <summary>
        /// On a paid policy the max payout amount allowed
        /// per week
        /// </summary>
        [BsonIgnoreIfNull]
        public decimal? MaxAllowedAmount { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to use whole dollar amounts only.
        /// </summary>
        /// <value>
        /// <c>true</c> if using whole dollar amounts only; otherwise, <c>false</c>.
        /// </value>
        [BsonDefaultValue(false)]
        public bool UseWholeDollarAmountsOnly { get; set; }

        /// <summary>
        /// Gets or sets the Absence Reason start month or arbitrary month for use for given usage calculations or other
        /// things that use fiscal or arbitrary year start dates, etc.
        /// </summary>
        [BsonIgnoreIfNull]
        public Month? ResetMonth { get; set; }

        /// <summary>
        /// Gets or sets the Absence Reason start day or arbitrary day for use for given usage calculations or other
        /// things that use fiscal or arbitrary year start dates, etc.
        /// </summary>
        [BsonIgnoreIfNull]
        public int? ResetDayOfMonth { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Relapse is allow.
        /// </summary>
        [BsonIgnoreIfNull]
        public bool? AllowRelapse { get; set; }

        /// <summary>
        /// Gets or sets the relapsed allowed time
        /// </summary>
        [BsonIgnoreIfNull]
        public double? RelapseAllowedTime { get; set; }

        /// <summary>
        /// Gets or sets the relapsed allowed time unit
        /// </summary>
        [BsonIgnoreIfNull]
        public Unit? RelapseAllowedTimeUnit { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Relapse is allow selection.
        /// </summary>
        [BsonIgnoreIfNull]
        public bool? RelapseAllowSelection { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Relapse will reevaluate the eligibility.
        /// </summary>
        [BsonIgnoreIfNull]
        public bool? RelapseReevaluateEligibility { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Relapse to use latest policy definition.
        /// </summary>
        [BsonIgnoreIfNull]
        public bool? RelapseUseLatestPolicyDefinition { get; set; }

        /// <summary>
        /// which if FALSE, lumps Administrative time for a policy with Leave time; 
        /// if that flag on the Policy Absence Reason is TRUE, it does not count the time together 
        /// (Admin time is separate from other lost-time types)
        /// </summary>
        [BsonIgnoreIfNull]
        public bool? OnlyCalculateUsageForLostTime { get; set; } = true;

        /// <summary>
        /// Gets or sets a value indicating fixed work days per week.
        /// </summary>
        [BsonIgnoreIfNull]
        public int? FixedWorkDaysPerWeek { get; set; }

        /// <summary>
        /// The name's of the policies that must be ineligible before this policy
        /// can be applied (codes and not IDs to make end user friendly)
        /// </summary>
        public List<string> SubstituteFor { get; set; }

        /// <summary>
        ///
        /// </summary>
        [BsonIgnoreIfNull]
        public Unit? PeriodUnit { get; set; }

        /// <summary>
        /// Cleans this instance.
        /// </summary>
        public override void Clean()
        {
            base.Clean();
            if (RuleGroups != null)
                RuleGroups.ForEach(g => g.Clean());
        }

        /// <summary>
        /// Adds the payment information.
        /// </summary>
        /// <param name="lpi">The lpi.</param>
        public void AddPaymentInfo(List<PaymentInfo> lpi)
        {
            PaymentTiers = lpi.Select(pi => new PaymentInfo()
            {
                Duration = pi.Duration,
                Order = pi.Order,
                PaymentPercentage = pi.PaymentPercentage
            }).ToList();
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            string reasonName = Reason == null ? null : Reason.Name;
            string duration = Period.HasValue ? (Period.Value == 1D ? "per" : "every " + Period.Value.ToString(Period.Value % 1 == 0 ? "N0" : "N2")) : "";
            string nativeUnit = string.Format(" month{0} per configured FMLA employer default period type", Period != 1D ? "s" : "");

            if (PeriodType.HasValue)
            {
                var period = Period ?? 1D;
                switch (PeriodType.Value)
                {
                    case Enums.PeriodType.PerCase:
                        duration = "per";
                        nativeUnit = " case";
                        break;
                    case Enums.PeriodType.CalendarYear:
                        duration = Period == 1 ? "per" : period.ToString(period % 1 == 0 ? "N0" : "N2");
                        nativeUnit = Period > 1 ? " calendar years" : " calendar year";
                        break;
                    case Enums.PeriodType.FixedYearFromServiceDate:
                        duration = "per";
                        nativeUnit = " year, fixed from the anniversary of the employee's service date";
                        break;
                    case Enums.PeriodType.FixedResetPeriod:
                        duration = "per";
                        nativeUnit = " year, fixed from the anniversary of a reset period date";
                        break;
                    case Enums.PeriodType.RollingForward:
                        duration = Period == 1 ? "per" : period.ToString(period % 1 == 0 ? "N0" : "N2");
                        if (PeriodUnit.HasValue)
                        {
                            nativeUnit = String.Concat(PeriodUnit," rolling forward");
                        }
                        else
                        {
                            nativeUnit = Period > 1 ? " months rolling forward" : " month rolling forward";
                        }
                        break;
                    case Enums.PeriodType.RollingBack:
                        duration = Period == 1 ? "per" : period.ToString(period % 1 == 0 ? "N0" : "N2");
                        if (PeriodUnit.HasValue)
                        {
                            nativeUnit = String.Concat(PeriodUnit," rolling back");
                        }
                        else
                        {
                            nativeUnit = Period > 1 ? " months rolling back" : " month rolling back";
                        }
                        break;
                    case Enums.PeriodType.Lifetime:
                        duration = "the employee's";
                        nativeUnit = " lifetime";
                        break;
                    case Enums.PeriodType.PerOccurrence:
                        duration = "per " + (Period > 1 ? period.ToString(period % 1 == 0 ? "N0" : "N2") : "");
                        nativeUnit = Period > 1 ? " occurrences" : "occurrence";                        
                        break;
                    default:
                        break;
                }
            }



            if (EntitlementType == Enums.EntitlementType.ReasonablePeriod)
            {
                duration = "of";
                nativeUnit = " time";
            }

            StringBuilder me = new StringBuilder();
            me.AppendFormat("Employee is entitled to {0} {1} {2}{3} for the reason of {4} when a case is {5}; effective as of {6:MM/dd/yyyy}.",
                EntitlementType == Enums.EntitlementType.ReasonablePeriod ? "a" : Entitlement.HasValue ? Entitlement.Value.ToString(Entitlement.Value % 1 == 0 ? "N0" : "N2") : "1",
                EntitlementType.ToString().SplitCamelCaseString(),
                duration,
                nativeUnit,
                reasonName,
                string.Join(" or ", Enum.GetValues(typeof(CaseType)).OfType<CaseType>().Where(c => CaseTypes.HasFlag(c)).Select(c => c.ToString().SplitCamelCaseString())),
                EffectiveDate);
            if (!string.IsNullOrWhiteSpace(WorkState) || !string.IsNullOrWhiteSpace(ResidenceState) || Gender.HasValue)
                me.AppendFormat(" This policy, for the reason of {8}, is constrained to only apply to those employees that{0}{1}{2}{3}{4}{5}{6}{7}.",
                    string.IsNullOrWhiteSpace(WorkState) ? "" : " work in the state of ",
                    WorkState,
                    !string.IsNullOrWhiteSpace(WorkState) && !string.IsNullOrWhiteSpace(ResidenceState) ? " or" : "",
                    string.IsNullOrWhiteSpace(ResidenceState) ? "" : " reside in the state of ",
                    ResidenceState,
                    (!string.IsNullOrWhiteSpace(WorkState) || !string.IsNullOrWhiteSpace(ResidenceState)) && Gender.HasValue ? " and that" : "",
                    Gender.HasValue ? " are " : "",
                    Gender.ToString().SplitCamelCaseString(),
                    reasonName);
            else
                me.AppendFormat(" This policy, for the reason of {0}, inherits all constraints on work state, state of residence or gender from the policy.", reasonName);
            if (CombinedForSpouses)
                me.Append(" Entitlement for this policy is combined/shared for spouses working at the same employer if allowed per employer preference.");
            if (PerUseCap.HasValue)
                me.AppendFormat(" A maximum amount of {0} {1} may be used per case for this policy.",
                    PerUseCap.HasValue ? PerUseCap.Value.ToString(PerUseCap.Value % 1 == 0 ? "N0" : "N2") : "",
                    PerUseCapType.ToString().SplitCamelCaseString());
            if (Elimination.HasValue)
                me.AppendFormat(" An elimination period will be applied for a duration of {0} {1} that is {2} of the approval period, meaning if the case is >= {0:N2} {1} {3}, otherwise the policy will be denied.",
                    Elimination.HasValue ? Elimination.Value.ToString(Elimination.Value % 1 == 0 ? "N0" : "N2") : "",
                    EliminationType.ToString().SplitCamelCaseString(),
                    PolicyEliminationCalcType.ToString().SplitCamelCaseString(),
                    PolicyEliminationCalcType == PolicyEliminationType.Exclusive ?
                    "the approval period for this policy will begin after the elimination period" :
                    "the approval period for this policy will be begin at the beginning of the case");
            if (PolicyEvents != null)
                foreach (var evt in PolicyEvents)
                {
                    string t = evt.EventType.ToString().SplitCamelCaseString();
                    if (!t.EndsWith("Date"))
                        t += " Date";
                    switch (evt.DateType)
                    {
                        case EventDateType.EndPolicyBasedOnEventDate:
                            me.AppendFormat(" The policy will end on the {0}.", t);
                            break;
                        case EventDateType.EndPolicyBasedOnCaseStartDate:
                            me.AppendFormat(" The policy will end on the Case Start Date if the {0} falls within the dates of the case.", t);
                            break;
                        case EventDateType.EndPolicyBasedOnCaseEndDate:
                            me.AppendFormat(" The policy will end on the Case End Date if the {0} falls within the dates of the case.", t);
                            break;
                        case EventDateType.StartPolicyBasedOnEventDate:
                            me.AppendFormat(" The policy will begin after the {0}.", t);
                            break;
                    }
                }
            if (Paid)
            {
                me.AppendFormat(" This policy is a paid policy during the configured payment period{0}{1:C2}{2}.",
                    MaxAllowedAmount.HasValue ? " with a weekly allowed max payment of " : "",
                    MaxAllowedAmount,
                    AllowOffset == true ? " which may be used to offset payment on another policy" : "");
                if (PaymentTiers != null && PaymentTiers.Any())
                    foreach (var p in PaymentTiers.OrderBy(o => o.Order))
                        me.AppendFormat(" {0}", p);
            }

            if (PolicyBehaviorType.HasValue)
            {

                switch (PolicyBehaviorType.Value)
                {
                    case Enums.PolicyBehavior.ReducesWorkWeek:
                        me.AppendFormat(" Holiday will reduce the amount of time used by removing the scheduled work day from the work schedule");
                        break;
                    case Enums.PolicyBehavior.Ignored:
                        me.AppendFormat(" Holiday will be completely ignored");
                        break;
                    case Enums.PolicyBehavior.IgnoredNoLostTime:
                        me.AppendFormat(" Holiday will not decrease the employee's scheduled time, however it does decrease the total time lost on that given holiday");
                        break;
                    default:
                        break;
                }
            }

            if (!(AllowRelapse ?? true))
            {
                me.Append(" This policy for this absence reason will not be carried forward upon relapse if already being tracked for the case; it will also exclude the policy from being automatically selected during relapse intake.");
            }

            if (RelapseAllowedTime.HasValue)
            {
                me.AppendFormat(" This policy will be carried forward on a relapse period only if the relapse occurs within {0} {1} of the prior Return to Work Date (RTW) or Case End Date if the RTW date was not set or provided. If the new relapse Start Date is outside of the allowed range, this policy will not be carried forward for that relapse period.", RelapseAllowedTime, RelapseAllowedTimeUnit);
            }

            if (RelapseAllowSelection == true)
            {
                me.Append(" This policy will be re-considered to be added to the case during a relapse event if not already being tracked on that case. Existing policy selection criteria and applicability are still considered as well.");
            }

            if (RelapseReevaluateEligibility == true)
            {
                me.Append(" Upon relapse this policy's eligibility will be re-evaluated as of the start date of relapse and where a policy may have been ineligible before may become eligible and vice versa. The user will still have the ability to override individual eligibility criteria and results before completing the relapse intake.");
            }

            return me.ToString();
        }
    }
}
