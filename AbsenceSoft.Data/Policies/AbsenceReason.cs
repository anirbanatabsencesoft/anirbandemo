﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Policies
{
    public static class AbsenceReasonCategory
    {
        public const string Administrative = "Administrative";
    }


    /// <summary>
    /// Absence Reason Entity
    /// </summary>
    [Serializable]
    public class AbsenceReason : BaseEmployerOverrideableEntity<AbsenceReason>
    {
        /// <summary>
        /// Gets or sets the absence reason name.
        /// </summary>
        [BsonRequired]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets an optional group name for the reason so it can be
        /// categorized (e.g. Health, Military, etc.)
        /// </summary>
        [BsonIgnoreIfDefault]
        public string Category { get; set; }

        /// <summary>
        /// Gets or sets some help text which explains this absence reason, legal definitions and assistance
        /// for selection, etc. This may or may not be HTML markup but will be used in the UI or other
        /// auto-generated UI documentation.
        /// </summary>
        [BsonIgnoreIfDefault]
        public string HelpText { get; set; }
		
		/// <summary>
		/// Gets or sets flags for this absence reason [OPTIONAL]
		/// </summary>
		public AbsenceReasonFlag? Flags { get; set; }

        /// <summary>
        /// Gets or sets case types for this absence reason.  Null means this reason shows up for All Case Types
        /// </summary>
        public CaseType? CaseTypes { get; set; }

        /// <summary>
        /// Gets or sets the country for this absence reason.  Null means this reason shows up for all countries
        /// </summary>
        [BsonIgnoreIfNull]
        public string Country { get; set; }

        [BsonIgnore]
        public bool IsDisabled { get; set; }

        protected override bool OnSaving()
        {
            if (string.IsNullOrWhiteSpace(CustomerId) && Employer != null)
                CustomerId = Employer.CustomerId;
            return base.OnSaving();
        }
    }
}
