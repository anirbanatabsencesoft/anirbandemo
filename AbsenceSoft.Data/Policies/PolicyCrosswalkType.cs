﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Policies
{
    [Serializable]
    public class PolicyCrosswalkType: BaseEmployerOverrideableEntity<PolicyCrosswalkType>
    {
        public string Name { get; set; } 
    }
}
