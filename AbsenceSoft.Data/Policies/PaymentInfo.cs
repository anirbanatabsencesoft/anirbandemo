﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Policies
{
    /// <summary>
    /// information about a payment band (used when there is more than one level of payment)
    /// for example: 4 weeks at 80%, then 4 weeks at 60%
    /// </summary>
    [Serializable, BsonIgnoreExtraElements]
    public class PaymentInfo
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance is a waiting period.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is a waiting period; otherwise, <c>false</c>.
        /// </value>
        [BsonDefaultValue(false)]
        public bool IsWaitingPeriod { get; set; }

        /// <summary>
        /// the payment has a decimal .65 = 65%
        /// </summary>
        public decimal PaymentPercentage { get; set; }

        /// <summary>
        /// on this band what is the max that can be paid if it is a supplemental too
        /// </summary>
        public decimal MaxPaymentPercentage { get; set; }
        
        /// <summary>
        /// how long do they receive the amount for
        /// </summary>
        public int Duration { get; set; }

        /// <summary>
        /// sort the payments on this field
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Gets or sets the max pay amount. This is the maximum dollar amount 
        /// that will get paid for the optionally set max pay duration.
        /// </summary>
        /// <value>
        /// The max pay amount.
        /// </value>
        public decimal? MaxPayAmount { get; set; }

        /// <summary>
        /// Gets or sets the minimum pay amount.
        /// </summary>
        /// <value>
        /// The minimum pay amount.
        /// </value>
        public decimal? MinPayAmount { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            StringBuilder me = new StringBuilder();
            if (IsWaitingPeriod)
                me.AppendFormat("Has a waiting period of {0} week{1}", Duration, Duration != 1 ? "s" : "");
            else
            {
                me.AppendFormat("{4}Pays no more than {0:p2} of base pay with total overall compensation of {1:p2} when combined/supplemental to other paid policies for up to {2} week{3}",
                    PaymentPercentage,
                    MaxPaymentPercentage,
                    Duration,
                    Duration != 1 ? "s" : "",
                    Order > 1 ? "And then " : "");
                if (MaxPayAmount.HasValue)
                    me.AppendFormat(" but only up to {0:c2}",MaxPayAmount);
            }
            me.Append(".");
            return me.ToString();
        }
    }
}
