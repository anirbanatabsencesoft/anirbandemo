﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbsenceSoft.Data.Policies
{
    /// <summary>
    /// Represents a group of policy rules applied using the specified grouping rule.
    /// </summary>
    [Serializable]
    public class PolicyRuleGroup : RuleGroup
    {
        public PolicyRuleGroup()
        {
            SuccessType = RuleGroupSuccessType.And;
            RuleGroupType = PolicyRuleGroupType.Selection;
            Rules = new List<Rule>();
        }

        /// <summary>
        /// Gets or sets the crosswalk code for this rule group
        /// </summary>
        [BsonIgnoreIfNull]
        public string CrosswalkCode { get; set; }

        /// <summary>
        /// Gets or sets the crosswalk type code for this rule group
        /// </summary>
        [BsonIgnoreIfNull]
        public string CrosswalkTypeCode { get; set; }

        /// <summary>
        /// Gets or sets the type of policy rule group this is, e.g. selection, eligibility, time, etc.
        /// </summary>
        public PolicyRuleGroupType RuleGroupType { get; set; }

        /// <summary>
        /// Gets or sets any additional or reduced entitlement (time) for the policy that this rule grants, the final
        /// entitlement is the greatest of the policy absence reason and all passing rules in all passing groups for 
        /// a given policy.
        /// </summary>
        public double? Entitlement { get; set; }
        
        /// <summary>
        /// For a payment rule group the entitlement is a list of payment info
        /// </summary>
        [BsonIgnoreIfNull]
        public List<PaymentInfo> PaymentEntitlement { get; set; }

        /// <summary>
        /// Cleans this instance.
        /// </summary>
        public override void Clean()
        {
            base.Clean();
            if (Rules != null)
                Rules.ForEach(r => r.Clean());
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            StringBuilder me = new StringBuilder();
            string t = SuccessType.ToString().SplitCamelCaseString();
            switch (SuccessType)
            {
                case RuleGroupSuccessType.And:
                    t = "when all of the following statements are true";
                    break;
                case RuleGroupSuccessType.Or:
                    t = "when at least one of the following statements are true";
                    break;
                case RuleGroupSuccessType.Not:
                    t = "when none of the following statements are true";
                    break;
                case RuleGroupSuccessType.Nor:
                    t = "when at least one of the following statements is not true";
                    break;
            }
            switch (RuleGroupType)
            {
                case PolicyRuleGroupType.Selection:
                    me.AppendFormat("Will select the policy for consideration {0}.", t);
                    break;
                case PolicyRuleGroupType.Eligibility:
                    me.AppendFormat("Will pass eligibility for this policy {0}.", t);
                    break;
                case PolicyRuleGroupType.Time:
                    me.AppendFormat("Will grant either the policy configured entitlement or an entitlement of {0} of the same type, whichever is greater, {1}.",
                        Entitlement.HasValue ? Entitlement.Value.ToString(Entitlement.Value % 1 == 0 ? "N0" : "N2") : "", 
                        t);
                    break;
                case PolicyRuleGroupType.Payment:
                    me.AppendFormat("With the following payment information: {0} Will override the payment tiers configured for the policy {1}.",
                        PaymentEntitlement == null || !PaymentEntitlement.Any() ? "n/a." : string.Join(" ", PaymentEntitlement), t);
                    break;
            }

            return me.ToString();
        }

        
    }
}
