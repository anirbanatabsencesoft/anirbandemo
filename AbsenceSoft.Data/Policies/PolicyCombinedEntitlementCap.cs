﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Policies
{
    [Serializable]
    public class PolicyCombinedEntitlementCap
    {
        /// <summary>
        /// Gets or sets the EntitlementType.
        /// </summary>
        [BsonIgnoreIfNull]
        public EntitlementType? CombinedEntitlementType { get; set; }

        /// <summary>
        /// Gets or sets the entitlement (time) for the policy that this rule grants, the final
        /// entitlement is the greatest of the policy absence reason and all passing rules in all 
        /// passing groups for a given policy.
        /// </summary>

        [BsonIgnoreIfNull]
        public double? CombinedEntitlementAmount { get; set; }

        /// <summary>
        /// Gets or sets the name of the policy.
        /// </summary>
        [BsonIgnoreIfNull]
        public List<string> CombinedEntitlementCapPolicy { get; set; }
    }
}
