﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Text;

namespace AbsenceSoft.Data
{
    [Serializable, JsonObject(MemberSerialization.OptOut)]
    public abstract class BaseEmployerOverrideableEntity<TEntity> : BaseEntity<TEntity> where TEntity : BaseEmployerOverrideableEntity<TEntity>, new()
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the customer id for the entity.
        /// </summary>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the entity's customer.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Customer Customer
        {
            get { return GetReferenceValue<Customer>(this.CustomerId); }
            set { CustomerId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the employer id for the entity.
        /// </summary>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the entity's employer.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Employer Employer
        {
            get { return GetReferenceValue<Employer>(this.EmployerId); }
            set { EmployerId = SetReferenceValue(value); }
        }

        /// <summary>
        /// The unique identifying code for this entity instance.
        /// This code is unique only within the confines of the Customer + Employer paradigm, OR global.
        /// If global, it can still be duplicated/overridden by a customer or employer specific version.
        /// </summary>
        [BsonRequired]
        public string Code { get; set; }

        /// <summary>
        /// Gets whether or not this policy is a custom employer policy or is overridden
        /// by the employer.
        /// </summary>
        [BsonIgnore]
        public bool IsCustom
        {
            get { return !string.IsNullOrWhiteSpace(CustomerId); }
            set { }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="BaseEmployerOverrideableEntity{TEntity}"/> is suppressed.
        /// </summary>
        /// <value>
        ///   <c>true</c> if suppressed; otherwise, <c>false</c>.
        /// </value>
        public bool Suppressed { get; set; }

        #endregion

        /// <summary>
        /// Called when [saving].
        /// </summary>
        /// <returns></returns>
        protected override bool OnSaving()
        {
            Code = string.IsNullOrWhiteSpace(Code)
                ? null
                : Code.Trim().ToUpperInvariant();
            return base.OnSaving();
        }

        #region Public Methods

        /// <summary>
        /// Called when this instance of this class has been deserialized. 
        /// This sets up the BsonExtraElements property, Metadata, with any 
        /// key value pairs from the JsonExtensionData _UIMetadata property.
        /// </summary>
        /// <param name="context">The context.</param>
        [OnDeserialized]
        internal void RemoveIsCustom(StreamingContext context)
        {
            if (Metadata.Contains("IsCustom"))
                Metadata.Remove("IsCustom");
        }

        public override void Clean()
        {
            base.Clean();
            CustomerId = null;
            EmployerId = null;
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching.
        /// </summary>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static new IQueryable<TEntity> AsQueryable()
        {
            if (User.Current != null)
                return AsQueryable(User.Current);
            return BaseEntity<TEntity>.AsQueryable();
        }

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching while automatically
        /// applying the appropriate data access filters onto it automagically.
        /// </summary>
        /// <param name="entity">The entity of type <c>TEntity</c> to which to build the filtered queryable off of</param>
        /// <param name="user">The user of which to build data visibility and data access from</param>
        /// <param name="permission">The permission that the user should have access to when filtering the data parameters</param>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static IQueryable<TEntity> AsQueryable(User user, Permission permission) { return AsQueryable(user, permission.Id); }
        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching while automatically
        /// applying the appropriate data access filters onto it automagically.
        /// </summary>
        /// <param name="entity">The entity of type <c>TEntity</c> to which to build the filtered queryable off of</param>
        /// <param name="user">The user of which to build data visibility and data access from</param>
        /// <param name="permission">The permission that the user should have access to when filtering the data parameters</param>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static IQueryable<TEntity> AsQueryable(User user, string permission = null)
        {
            var query = BaseEntity<TEntity>.AsQueryable();

            // If no user, then duh, no dice sucker, empty list it
            if (user == null) return new List<TEntity>(0).AsQueryable();
            // User must belong to a customer, otherwise empty list it
            if (string.IsNullOrWhiteSpace(user.CustomerId)) return query.Where(e => e.CustomerId == null && e.EmployerId == null);
            // User must have access to at least 1 employer, otherwise empty list it
            if (user.Employers == null || !user.Employers.Any(e => e.IsActive && e.Employer != null && !e.Employer.IsDeleted)) return query.Where(e =>
                (e.CustomerId == null && e.EmployerId == null) || (e.CustomerId == user.CustomerId && e.EmployerId == null));

            // Filter out any that are not active
            var employerIds = user.Employers.Where(e => e.IsActive && e.Employer != null && !e.Employer.IsDeleted)
                // Ensure the permissions passed in are empty (meaning just all active) or that 
                //  if we have explicit intent, that the user has the appropriate permissions for that function for that employer
                .Where(e => string.IsNullOrWhiteSpace(permission) || User.Permissions.GetPermissions(user, e.EmployerId).Contains(permission))
                // Project our Employer entity
                .Select(e => e.EmployerId)
                .Distinct()
                // Return the final list
                .ToArray();

            query = query.Where(e => employerIds.Contains(e.EmployerId) || e.EmployerId == null)
                .Where(e => e.CustomerId == user.CustomerId || e.CustomerId == null);

            return query;
        }//end: AsQueryable

        /// <summary>
        /// Gets the entity by its Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>The entity found by the Id or <c>null</c> if no entity was found by that id.</returns>
        public static new TEntity GetById(string id)
        {
            using (new InstrumentationContext("{0}.GetById", ClassTypeName))
            {
                if (string.IsNullOrWhiteSpace(id))
                    return default(TEntity);

                TEntity val = Repository.GetById(id.ToLowerInvariant());
                if (val == null || val.IsDeleted)
                    return null;
                if (User.Current == null)
                    return val;
                if (string.IsNullOrWhiteSpace(val.CustomerId))
                    return val;
                if (val.CustomerId != User.Current.CustomerId)
                    return default(TEntity);
                if (string.IsNullOrWhiteSpace(val.EmployerId))
                    return val;
                if (!User.Current.HasEmployerAccess(val.EmployerId))
                    return default(TEntity);

                return val;
            }
        }

        /// <summary>
        /// Gets the entity by its code rather than Id based on a customer, employer or core identifiers.
        /// </summary>
        /// <param name="code">The code for the entity</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <param name="includeSuppressed">if set to <c>true</c> [include suppressed].</param>
        /// <returns>
        /// The entity if found, otherwise <c>null</c> (default of TEntity).
        /// </returns>
        public static TEntity GetByCode(string code, string customerId = null, string employerId = null, bool includeSuppressed = false)
        {
            if (string.IsNullOrWhiteSpace(code))
                return default(TEntity);

            string c = code.ToUpperInvariant();

            TEntity val = Query.Find(
                Query.And(
                    Query.EQIgnoreCase(r => r.Code, c),
                    Query.Or(
                        Query.EQ(r => r.CustomerId, customerId),
                        Query.EQ(r => r.CustomerId, null),
                        Query.NotExists(r => r.CustomerId)
                    ),
                    Query.Or(
                        Query.EQ(r => r.EmployerId, employerId),
                        Query.EQ(r => r.EmployerId, null),
                        Query.NotExists(r => r.EmployerId)
                    )
                )).ToList().OrderBy(r =>
                {
                    if (!string.IsNullOrWhiteSpace(r.EmployerId))
                        return 1;
                    if (!string.IsNullOrWhiteSpace(r.CustomerId))
                        return 2;
                    return 3;
                }).FirstOrDefault();

            if (val == null)
                return val;
            if (val.IsCustom && val.Suppressed && !includeSuppressed)
                return default(TEntity);
            if (User.Current == null)
                return val;
            if (string.IsNullOrWhiteSpace(val.CustomerId))
                return val;
            if (User.Current == null || User.Current.Id == User.DefaultUserId)
                return val;
            if (val.CustomerId != User.Current.CustomerId)
                return default(TEntity);
            if (string.IsNullOrWhiteSpace(val.EmployerId))
                return val;
            if (!User.Current.HasEmployerAccess(val.EmployerId))
                return default(TEntity);

            return val;
        }

        /// <summary>
        /// Runs the query but only returns a distinct list based on code and the override heiararchy (Employer &gt; Customer &gt; Core).
        /// </summary>
        /// <param name="query">The query to run.</param>
        /// <param name="customerId">The customer identifier, if any (optional).</param>
        /// <param name="employerId">The employer identifier, if any (optional).</param>
        /// <param name="includeSuppressed">if set to <c>true</c> [include suppressed].</param>
        /// <param name="includeAllEmployers">if set to <c>true</c> then include all employers' data for the customer IF employerId it not provided.</param>
        /// <returns></returns>
        public static IEnumerable<TEntity> DistinctFind(IMongoQuery query, string customerId = null, string employerId = null, bool includeSuppressed = false, bool includeAllEmployers = false)
        {
            List<IMongoQuery> ands = new List<IMongoQuery>(query == null ? 2 : 3)
            {
                Query.Or(
                    Query.EQ(r => r.CustomerId, customerId),
                    Query.EQ(r => r.CustomerId, null),
                    Query.NotExists(r => r.CustomerId)
                )
            };
            if (!string.IsNullOrWhiteSpace(employerId) || !includeAllEmployers)
            {
                ands.Add(Query.Or(
                    Query.EQ(r => r.EmployerId, employerId),
                    Query.EQ(r => r.EmployerId, null),
                    Query.NotExists(r => r.EmployerId)
                ));
            }
            if (query != null)
                ands.Add(query);
            var results = Query.Find(Query.And(ands)).ToList().OrderBy(r => r.Code).ThenBy(r =>
            {
                if (!string.IsNullOrWhiteSpace(r.EmployerId))
                    return 1;
                if (!string.IsNullOrWhiteSpace(r.CustomerId))
                    return 2;
                return 3;
            });
            string lastCode = null;
            foreach (var item in results)
            {
                if (item == null)
                    continue;
                if (item.Code == lastCode)
                    continue;
                lastCode = item.Code;
                if (item.IsCustom && item.Suppressed && !includeSuppressed)
                    continue;
                yield return item;
            }
        }

        /// <summary>
        /// Creates a list of queries to collectively filter a collection query as part of a distinct aggregation
        /// search.
        /// </summary>
        /// <param name="currentUser">The current user.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">currentUser</exception>
        public static List<IMongoQuery> DistinctAnds(User currentUser, string customerId = null, string employerId = null)
        {
            if (currentUser == null)
                throw new ArgumentNullException("currentUser");
            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(currentUser.BuildDataAccessFilters());
            ands.Add(Query.IsNotDeleted());
            ands.Add(CreateIdQuery(q => q.CustomerId, customerId));
            ands.Add(CreateIdQuery(q => q.EmployerId, employerId));

            return ands;
        }

        /// <summary>
        /// Creates the identifier query.
        /// </summary>
        /// <param name="expression">The expression.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public static IMongoQuery CreateIdQuery(Expression<Func<TEntity, string>> expression, string id)
        {
            List<IMongoQuery> query = new List<IMongoQuery>()
            {
                Query.EQ(expression, null),
                Query.NotExists(expression)
            };

            if (!string.IsNullOrWhiteSpace(id))
                query.Add(Query.EQ(expression, id));

            return Query.Or(query);
        }

        /// <summary>
        /// Gets a list of distinct ids using the aggregation pipeline based on the queri filters for the $match phase.
        /// </summary>
        /// <param name="queries">The queries.</param>
        /// <returns></returns>
        public static List<string> DistinctAggregationIds(List<IMongoQuery> queries)
        {
            var find = Query.And(queries);
            var args = new AggregateArgs()
            {
                OutputMode = AggregateOutputMode.Cursor,
                Pipeline = new List<BsonDocument>()
                {
                    BsonDocument.Parse("{ $match: " + find.ToString() + " }"),
                    BsonDocument.Parse(PopulateIds()),
                    BsonDocument.Parse(CreateIdValues()),
                    BsonDocument.Parse(SortProjection()),
                    BsonDocument.Parse(GroupByCode())
                },
                BatchSize = 1000
            };


            return Repository.Collection.Aggregate(args)
                .Select(d => d["Id"].ToString()).ToList();
        }

        /// <summary>
        /// Populates the ids.
        /// </summary>
        /// <returns></returns>
        private static string PopulateIds()
        {
            StringBuilder projection = new StringBuilder();
            projection.Append("{ $project: {");
            projection.Append("EmployerId : { $ifNull :[\"$EmployerId\", null] }, ");
            projection.Append("CustomerId : { $ifNull :[\"$CustomerId\", null] }, ");
            projection.Append("Code: true } }");
            return projection.ToString();
        }

        /// <summary>
        /// Creates a projection for the aggregation pipeline that builds a type with the employer id, customer id and code.
        /// </summary>
        /// <returns></returns>
        private static string CreateIdValues()
        {
            StringBuilder projection = new StringBuilder();
            projection.Append("{ $project: {");
            projection.Append("EmployerId : { $cond: { if : { $eq:[\"$EmployerId\", null] }, then: 100, else: 0}}, ");
            projection.Append("CustomerId : { $cond: { if : { $eq:[\"$CustomerId\", null] }, then: 50, else: 1}}, ");
            projection.Append("Code: true } }");
            return projection.ToString();
        }

        /// <summary>
        /// Creates an aggregation pipeline step that sorts the results by code, employer and customer.
        /// </summary>
        /// <returns></returns>
        private static string SortProjection()
        {
            StringBuilder sort = new StringBuilder();
            sort.Append("{ $sort: {");
            sort.Append("Code: 1, ");
            sort.Append("EmployerId : 1, ");
            sort.Append("CustomerId : 1 } }");
            return sort.ToString();
        }

        /// <summary>
        /// Creates an aggregation pipeline step that groups by code.
        /// </summary>
        /// <returns></returns>
        private static string GroupByCode()
        {
            StringBuilder group = new StringBuilder();
            group.Append("{ $group: { ");
            group.Append("_id: \"$Code\", ");
            group.Append("Id: { $first: \"$_id\" }");
            group.Append("} }");
            return group.ToString();
        }

        /// <summary>
        /// Gets a distinct aggregation list of entities found using the list of queries.
        /// </summary>
        /// <param name="queries">The queries.</param>
        /// <returns></returns>
        public static List<TEntity> DistinctAggregation(List<IMongoQuery> queries)
        {
            List<string> idsToGet = DistinctAggregationIds(queries);
            return Query.Find(Query.In(q => q.Id, idsToGet)).ToList();
        }

        /// <summary>
        /// Tells if the current overrideable entity is suppressed at the customer level
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public bool IsSuppressed(Customer customer, Employer employer)
        {
            if (IsCustom || (employer == null && customer == null))
                return false;
            
            // If we have an employer AND it's in the suppressed list, then it is suppressed
            if (employer != null && EntityIsInSuppressedList(employer.SuppressedEntities))
                return true;

            // If it's not, we still need to check the customer
            return EntityIsInSuppressedList(customer.SuppressedEntities);
        }

        /// <summary>
        /// Checks if the entity exists in the suppressed list
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        private bool EntityIsInSuppressedList(List<SuppressedEntity> entities)
        {
            if (entities == null || entities.Count == 0)
                return false;

            return entities.FirstOrDefault(se => se.EntityCode == Code && se.EntityName == ClassTypeName) != null;
        }

        /// <summary>
        /// Toggles whether this entity is suppressed for the customer
        /// </summary>
        /// <param name="customer"></param>
        public void ToggleSuppression(Customer customer, Employer employer)
        {
            if (IsCustom || (customer == null && employer == null))
                return;

            if(employer != null)
            {
                employer.SuppressedEntities = AddOrRemoveSuppressedEntity(employer.SuppressedEntities);
                employer.Save();
            }
            else
            {
                customer.SuppressedEntities = AddOrRemoveSuppressedEntity(customer.SuppressedEntities);
                customer.Save();
            }
        }

        /// <summary>
        /// Handles the underlying operation that toggles whether an entity is suppressed or not
        /// </summary>
        /// <param name="suppressedEntities"></param>
        /// <returns></returns>
        private List<SuppressedEntity> AddOrRemoveSuppressedEntity(List<SuppressedEntity> suppressedEntities)
        {
            SuppressedEntity entity = suppressedEntities
                .FirstOrDefault(se => se.EntityCode == Code && se.EntityName == ClassTypeName);
            if (entity == null)
            {
                entity = new SuppressedEntity()
                {
                    EntityCode = Code,
                    EntityName = ClassTypeName
                };
                suppressedEntities.Add(entity);
                Suppressed = true;
            }
            else
            {
                suppressedEntities.Remove(entity);
                Suppressed = false;
            }

            return suppressedEntities;
        }

        #endregion
    }
}
