﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Dashboards
{
    [Serializable]
    public class WidgetContainer : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WidgetContainer"/> class.
        /// </summary>
        public WidgetContainer() : base()
        {
            Order = 0;
            Columns = 6;
            Hidden = false;
            Settings = new List<WidgetSetting>();
        }

        /// <summary>
        /// Gets or sets the title of the widget as displayed on the dashboard and is optional.
        /// </summary>
        /// <value>
        /// The title of the widget displayed on the dashboard.
        /// </value>
        [BsonIgnoreIfNull]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the widget identifier. This is to uniquely identify the specific widget
        /// implementation, typically a GUID or Unique code, that is specified by the
        /// widget class itself.
        /// </summary>
        /// <value>
        /// The widget identifier.
        /// </value>
        public string WidgetId { get; set; }

        /// <summary>
        /// Gets or sets the number of columns this widget takes up on a dashboard row.
        /// </summary>
        /// <value>
        /// The number of columns this widget takes up on a dashboard row.
        /// </value>
        public int Columns { get; set; }

        /// <summary>
        /// Gets or sets the order in which this widget is displayed on the dashboard.
        /// </summary>
        /// <value>
        /// The order in which this widget is displayed on the dashboard.
        /// </value>
        public int Order { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="WidgetContainer"/> is hidden.
        /// </summary>
        /// <value>
        ///   <c>true</c> if hidden; otherwise, <c>false</c>.
        /// </value>
        public bool Hidden { get; set; }

        /// <summary>
        /// Gets or sets the settings.
        /// </summary>
        /// <value>
        /// The settings.
        /// </value>
        public List<WidgetSetting> Settings { get; set; }
    }
}
