﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Dashboards
{
    [Serializable]
    public class WidgetSetting : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WidgetSetting"/> class.
        /// </summary>
        public WidgetSetting() : base()
        {
            Values = null;
        }

        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        [BsonIgnore]
        public object Value
        {
            get
            {
                return Values == null ? null : Values.First();
            }
            set
            {
                if (value == null)
                {
                    Values = null;
                    return;
                }
                if (Values == null)
                    Values = new List<object>(1);
                else
                    Values.Clear();
                Values.Add(value);
            }
        }

        /// <summary>
        /// Gets or sets the values.
        /// </summary>
        /// <value>
        /// The values.
        /// </value>
        public List<object> Values { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Concat(Key, ": ", Values == null || !Values.Any(v => v != null) ? "" : Values.Count > 1 ? "one of '" + string.Join("', '", Values) + "'" : Value.ToString());
        }
    }
}
