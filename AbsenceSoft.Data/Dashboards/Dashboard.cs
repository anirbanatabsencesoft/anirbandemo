﻿using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Dashboards
{
    [Serializable]
    public class Dashboard : BaseCustomerEntity<Dashboard>
    {
        public Dashboard()
        {
            Widgets = new List<WidgetContainer>();
        }

        /// <summary>
        /// Gets or sets the user identifier that owns this dashboard.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [BsonRepresentation(BsonType.ObjectId), BsonRequired]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the name of the dashboard.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [BsonIgnoreIfNull]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the widgets contained on the dashboard.
        /// </summary>
        /// <value>
        /// The widgets.
        /// </value>
        public List<WidgetContainer> Widgets { get; set; }

        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        /// <value>
        /// The user.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public User User
        {
            get { return GetReferenceValue<User>(this.UserId); }
            set { this.UserId = SetReferenceValue(value); }
        }
    }
}
