﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Data.Questions
{
    /// <summary>
    /// Represents a grouping of rules and how those rules must suceed or fail
    /// </summary>
    [Serializable]
    public class Question : BaseNonEntity
    {
        public Question()
        {
            // Defaults
            AnswerOptions = new List<string>();
        }

        /// <summary>
        /// Gets or sets the expression in plain English text for an end user's benefit or
        /// gives the rule group a plain-text name for display.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the list of Rules that apply to this group.
        /// </summary>
        public List<string> AnswerOptions { get; set; }


        



    }
}
