﻿using AbsenceSoft.Common.Security;
using AbsenceSoft.Data.Audit;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace AbsenceSoft.Data
{
    [
        Serializable,
        AuditClassNo
    ]
    public class CryptoString
    {
        public CryptoString() { }
        public CryptoString(string plainText) : this()
        {
            PlainText = plainText;
            Hashed = plainText.Sha256Hash();
            Encrypted = plainText.Encrypt();
        }//ctor

        [BsonIgnore]
        public string PlainText { get; set; }

        [BsonIgnoreIfNull, BsonElement("h")]
        public string Hashed { get; set; }

        [BsonIgnoreIfNull, BsonElement("e")]
        public string Encrypted { get; set; }

        public CryptoString Encrypt()
        {
            if (string.IsNullOrWhiteSpace(PlainText))
                return this;
            Encrypted = PlainText.Encrypt();
            return this;
        }
        public CryptoString Hash()
        {
            if (string.IsNullOrWhiteSpace(PlainText))
                return this;
            Hashed = PlainText.Sha256Hash();
            return this;
        }

        public static CryptoString GetHash(string plainText)
        {
            return new CryptoString()
            {
                PlainText = plainText,
                Hashed = plainText.Sha256Hash()
            };
        }

        public static CryptoString GetEncrypted(string plainText)
        {
            return new CryptoString()
            {
                PlainText = plainText,
                Encrypted = plainText.Encrypt()
            };
        }

        public static CryptoString GetPlainText(string plainText)
        {
            return new CryptoString() { PlainText = plainText };
        }
    }
}
