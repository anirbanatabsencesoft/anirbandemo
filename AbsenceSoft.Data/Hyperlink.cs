﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace AbsenceSoft.Data
{
    /// <summary>
    /// Represents a hyperlink for use in collecting web addresses in association to other entities/non-entities, websites, etc.
    /// </summary>
    [Serializable]
    public class Hyperlink : BaseNonEntity
    {
        /// <summary>
        /// Gets or sets the text of the link.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the URL.
        /// </summary>
        /// <value>
        /// The URL.
        /// </value>
        [BsonRequired]
        public string Url { get; set; }

        /// <summary>
        /// Gets or sets the title (optional).
        /// </summary>
        /// <value>
        /// The optional title.
        /// </value>
        [BsonIgnoreIfNull]
        public string Title { get; set; }
    }
}
