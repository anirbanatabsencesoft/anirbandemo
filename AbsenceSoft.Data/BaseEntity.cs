﻿using AbsenceSoft.Data.Audit;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.GeoJsonObjectModel;
using MongoRepository;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace AbsenceSoft.Data
{
    /// <summary>
    /// Represents the base entity for all persisted entity types.
    /// </summary>
    /// <typeparam name="TEntity">The type of entity this base entity is wrapped by</typeparam>
    [Serializable, JsonObject(MemberSerialization.OptOut)]
    [BsonIgnoreExtraElements(false, Inherited = true)]
    public abstract class BaseEntity<TEntity> : Entity where TEntity : BaseEntity<TEntity>, new()
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the date that this entity was created.
        /// </summary>
        [
            BsonDateTimeOptions(Kind = DateTimeKind.Utc, Representation = BsonType.DateTime),
            BsonRequired,
            BsonElement("cdt"),
            JsonProperty("cdt"),
            AuditPropertyNo
        ]
        public DateTime CreatedDate { get; private set; }
        /// <summary>
        /// Sets the created date. DO NOT USE THIS UNLESS YOU'RE THE EL LOAD PROCESS OR DOING BULK OPS
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        public TEntity SetCreatedDate(DateTime date)
        {
            this.CreatedDate = date;
            return (TEntity)this;
        }

        /// <summary>
        /// Gets or sets the user's email address whom created the entity.
        /// </summary>
        [
            BsonElement("cby"),
            BsonRequired,
            BsonRepresentation(BsonType.ObjectId),
            JsonProperty("cby"),
            AuditPropertyNo
        ]
        public string CreatedById { get; set; }

        /// <summary>
        /// Gets or sets the user account that created this entity.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public User CreatedBy
        {
            get { return CreatedById == User.DefaultUserId ? User.System : GetReferenceValue<User>(CreatedById); }
            set { CreatedById = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the date that this entity was modified.
        /// </summary>
        [
            BsonDateTimeOptions(Kind = DateTimeKind.Utc, Representation = BsonType.DateTime),
            BsonRequired,
            BsonElement("mdt"),
            JsonProperty("mdt"),
            AuditPropertyNo
        ]
        public DateTime ModifiedDate { get; private set; }
        /// <summary>
        /// Sets the modified date. DO NOT USE THIS UNLESS YOU'RE THE EL LOAD PROCESS OR DOING BULK OPS
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        public TEntity SetModifiedDate(DateTime date)
        {
            this.ModifiedDate = date;
            return (TEntity)this;
        }

        /// <summary>
        /// Gets or sets the user's email address whom last modified the entity.
        /// </summary>
        [
            BsonElement("mby"),
            BsonRequired,
            BsonRepresentation(BsonType.ObjectId),
            JsonProperty("mby"),
            AuditPropertyNo
        ]
        public string ModifiedById { get; set; }

        /// <summary>
        /// Gets or sets the user account that last modified this entity.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public User ModifiedBy
        {
            get { return ModifiedById == User.DefaultUserId ? User.System : GetReferenceValue<User>(ModifiedById); }
            set { ModifiedById = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets whether the entity is soft deleted
        /// </summary>
        [BsonDefaultValue(false)]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets the extra elements/properties for this document so we can handle any unknown or extra elements
        /// that are added before a typed property to an entity and we still persist those properties.
        /// </summary>
        [BsonExtraElements, JsonIgnore]
        public BsonDocument Metadata = new BsonDocument();

        /// <summary>
        /// Gets the dynamic metadata.
        /// </summary>
        /// <value>
        /// The dynamic metadata.
        /// </value>
        [AuditPropertyNo, BsonIgnore, JsonIgnore]
        public dynamic DynamicMetadata { get { return (dynamic)new DynamicAwesome(Metadata); } }

        /// <summary>
        /// Gets or sets the extra elements/properties for this document so we can handle any unknown or extra elements
        /// that are added before a typed property to an entity and we still project those to the UI via JSON and back.
        /// </summary>
        [JsonExtensionData, BsonIgnore]
        private Dictionary<string, object> _UIMetadata = new Dictionary<string, object>();

        /// <summary>
        /// Gets a value indicating whether or not this entity is a new entity or not.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public bool IsNew { get { return (string.IsNullOrWhiteSpace(this.Id) || this.CreatedDate == default(DateTime)); } }

        #endregion

        #region Public Methods

        /// <summary>
        /// Upserts (Inserts or Updates) this entity to the database and if it does not already exist,
        /// sets its new Id property as well.
        /// </summary>
        /// <exception cref="System.OperationCanceledException">If the cancelable event args <c>Canceled</c>
        /// property is set to true.</exception>
        public virtual TEntity Save()
        {
            using (new InstrumentationContext("{0}.Save", ClassTypeName))
            {
                if (OnSaving())
                {
                    this.Metadata.FixStupid(this);
                    if (string.IsNullOrWhiteSpace(this.CreatedById))
                        this.CreatedById = User.Current == null ? User.DefaultUserId : User.Current.Id;

                    this.ModifiedById = User.Current == null ? User.DefaultUserId : User.Current.Id;                    
                    this.ModifiedDate = DateTime.UtcNow;
                    if (IsNew && this.CreatedDate == default(DateTime))
                        this.CreatedDate = DateTime.UtcNow;

                    TEntity updated = Repository.Update((TEntity)this);
                    OnSaved();
                    return updated;
                }
                throw new OperationCanceledException();
            }
        }//Save

        /// <summary>
        /// Deletes this entity instance from the database.
        /// </summary>
        /// <exception cref="System.OperationCanceledException">If the cancelable event args <c>Canceled</c>
        /// property is set to true.</exception>
        public virtual void Delete()
        {
            using (new InstrumentationContext("{0}.Delete", ClassTypeName))
            {
                if (OnDeleting())
                {
                    if (User.Current != null)
                        this.ModifiedById = User.Current.Id;
                    this.ModifiedDate = DateTime.UtcNow;
                    this.IsDeleted = true;

                    Repository.Update((TEntity)this);
                    OnDeleted();
                }
                else
                    throw new OperationCanceledException();
            }
        }//Delete

        /// <summary>
        /// Cleans this instance.
        /// </summary>
        public virtual void Clean()
        {
            this.Id = null;
            this.ModifiedBy = null;
            this.CreatedBy = null;
            this.CreatedDate = DateTime.UtcNow;
            this.ModifiedDate = DateTime.UtcNow;
            this.IsDeleted = false;
        }

        #endregion

        #region Public Events

        /// <summary>
        /// Event is called before this entity of type TEntity is saved. This is useful
        /// for additional validation, etc. and may be canceled.
        /// </summary>
        public event Action<object, CancelableEntityEventArgs<TEntity>> Saving;

        /// <summary>
        /// Event is called after an entity has been saved, passing the newly saved entity.
        /// </summary>
        public event Action<object, EntityEventArgs<TEntity>> Saved;

        /// <summary>
        /// Event is called before this entity of type TEntity is deleted. This is useful
        /// for additional validation, etc. and may be canceled.
        /// </summary>
        public event Action<object, CancelableEntityEventArgs<TEntity>> Deleting;

        /// <summary>
        /// Event is called after an entity has been saved, passing the recently deleted entity.
        /// </summary>
        public event Action<object, EntityEventArgs<TEntity>> Deleted;

        #endregion

        #region Event Helpers

        /// <summary>
        /// Event is called before this entity of type TEntity is saved. This is useful
        /// for additional validation, etc. and may be canceled. This invokes the action
        /// specified exactly once and no more.
        /// </summary>
        /// <param name="action">The action delegate to invoke on Saving.</param>
        public void OnSavingNOnce(Action<object, CancelableEntityEventArgs<TEntity>> action)
        {
            if (action == null)
                return;

            Action<object, CancelableEntityEventArgs<TEntity>> wrapper = null;
            wrapper = new Action<object, CancelableEntityEventArgs<TEntity>>((o, e) =>
            {
                action.Invoke(o, e);
                this.Saving -= wrapper;
            });
            this.Saving += wrapper;
        }

        /// <summary>
        /// Event is called after an entity has been saved, passing the newly saved entity. 
        /// This invokes the action specified exactly once and no more.
        /// </summary>
        /// <param name="action">The action delegate to invoke on Saved.</param>
        public void OnSavedNOnce(Action<object, EntityEventArgs<TEntity>> action)
        {
            if (action == null)
                return;

            Action<object, EntityEventArgs<TEntity>> wrapper = null;
            wrapper = new Action<object, EntityEventArgs<TEntity>>((o, e) =>
            {
                action.Invoke(o, e);
                this.Saved -= wrapper;
            });
            this.Saved += wrapper;
        }

        /// <summary>
        /// Event is called before this entity of type TEntity is deleted. This is useful
        /// for additional validation, etc. and may be canceled. This invokes the action
        /// specified exactly once and no more.
        /// </summary>
        /// <param name="action">The action delegate to invoke on Deleting.</param>
        public void OnDeletingNOnce(Action<object, CancelableEntityEventArgs<TEntity>> action)
        {
            if (action == null)
                return;

            Action<object, CancelableEntityEventArgs<TEntity>> wrapper = null;
            wrapper = new Action<object, CancelableEntityEventArgs<TEntity>>((o, e) =>
            {
                action.Invoke(o, e);
                this.Deleting -= wrapper;
            });
            this.Deleting += wrapper;
        }

        /// <summary>
        /// Event is called after an entity has been saved, passing the recently deleted entity. 
        /// This invokes the action specified exactly once and no more.
        /// </summary>
        /// <param name="action">The action delegate to invoke on Deleted.</param>
        public void OnDeletedNOnce(Action<object, EntityEventArgs<TEntity>> action)
        {
            if (action == null)
                return;

            Action<object, EntityEventArgs<TEntity>> wrapper = null;
            wrapper = new Action<object, EntityEventArgs<TEntity>>((o, e) =>
            {
                action.Invoke(o, e);
                this.Deleted -= wrapper;
            });
            this.Deleted += wrapper;
        }

        #endregion Event Helpers

        #region Protected Handlers

        /// <summary>
        /// Called before saving
        /// </summary>
        /// <returns>A value indicating whether or not the save should proceed (not canceled).</returns>
        protected virtual bool OnSaving()
        {
            CancelableEntityEventArgs<TEntity> args = new CancelableEntityEventArgs<TEntity>((TEntity)this);
            if (Saving != null)
                Saving(this, args);
            return !args.Canceled;
        }//OnSaving

        /// <summary>
        /// Called when saved
        /// </summary>
        protected virtual void OnSaved()
        {
            if (Saved != null) Saved(this, new EntityEventArgs<TEntity>((TEntity)this));
        }//OnSaved

        /// <summary>
        /// Called before deleting
        /// </summary>
        /// <returns>A value indicating whether or not the delete should proceed (not canceled).</returns>
        protected virtual bool OnDeleting()
        {
            CancelableEntityEventArgs<TEntity> args = new CancelableEntityEventArgs<TEntity>((TEntity)this);
            if (Deleting != null)
                Deleting(this, args);
            return !args.Canceled;
        }//OnDeleting

        /// <summary>
        /// Called when deleted
        /// </summary>
        protected virtual void OnDeleted()
        {
            if (Deleted != null) Deleted(this, new EntityEventArgs<TEntity>((TEntity)this));
        }//OnDeleted

        #endregion

        #region Protected Methods

        /// <summary>
        /// The protected reference instance dictionary used for caching entity references.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        protected Dictionary<string, object> _refs = new Dictionary<string, object>();
        private static object _refsSync = new object();

        /// <summary>
        /// Gets the reference value for an Id based on the reference type passed into TRef.
        /// </summary>
        /// <typeparam name="TRef">The entity type the reference Id is referencing</typeparam>
        /// <param name="refId">The Id of the reference type to get by</param>
        /// <returns>An instance of the reference by Id or <c>null</c> if the reference is not found or Id is null.</returns>
        protected virtual TRef GetReferenceValue<TRef>(string refId) where TRef : BaseEntity<TRef>, new()
        {
            string key = string.Concat(typeof(TRef).Name, "-", refId ?? "");

            TRef existingVal = _refs?.ContainsKey(key) == true ? _refs[key] as TRef : null;
            if (!string.IsNullOrWhiteSpace(refId) && (existingVal == null || existingVal.Id != refId))
                existingVal = typeof(TRef).GetMethod("GetById", BindingFlags.Static | BindingFlags.FlattenHierarchy | BindingFlags.Public).Invoke(null, new object[] { refId }) as TRef;
            else if (string.IsNullOrWhiteSpace(refId))
                existingVal = null;

            lock (_refsSync)
            {
                if (_refs == null)
                    _refs = new Dictionary<string, object>();
                _refs[key] = existingVal;
            }

            return existingVal;
        }//GetReferenceValue

        /// <summary>
        /// Gets the reference value for a key value passed in using the current cache or fetch routine.
        /// </summary>
        /// <typeparam name="TRef">The entity type the reference Id is referencing</typeparam>
        /// <param name="keyValue">The key value for the reference type and keySelector to match cache hits on.</param>
        /// <param name="keySelector">The key selector.</param>
        /// <param name="fetch">The fetch.</param>
        /// <returns>
        /// An instance of the reference by keyValue or <c>null</c> if the reference is not found or Id is null.
        /// </returns>
        protected virtual TRef GetReferenceValue<TRef>(string keyValue, Func<TRef, string> keySelector, Func<string, TRef> fetch) where TRef : BaseEntity<TRef>, new()
        {
            string key = string.Concat(typeof(TRef).Name, "-", keyValue ?? "");

            TRef existingVal = _refs.ContainsKey(key) ? _refs[key] as TRef : null;
            if (!string.IsNullOrWhiteSpace(keyValue) && (existingVal == null || keySelector(existingVal) != keyValue))
                existingVal = fetch(keyValue);
            else if (string.IsNullOrWhiteSpace(keyValue))
                existingVal = null;

            lock (_refsSync)
            {
                _refs[key] = existingVal;
            }

            return existingVal;
        }//GetReferenceValue

        /// <summary>
        /// Sets the reference value for an Id based on the reference type passed into TRef, but also returns
        /// the Id of the set entity for setting the actual Id property value back in the parent entity.
        /// </summary>
        /// <typeparam name="TRef">The type value to be passed in to set</typeparam>
        /// <param name="value">The instance of the type value or <c>null</c> if being cleared out</param>
        /// <returns>The Id of the passed in value or <c>null</c> if no entity passed in</returns>
        protected virtual string SetReferenceValue<TRef>(TRef value) where TRef : BaseEntity<TRef>, new()
        {
            if (value == null)
                return null;

            string key = string.Concat(typeof(TRef).Name, "-", value.Id ?? "");
            lock (_refsSync)
            {
                _refs[key] = value;
            }

            return value.Id;
        }//SetReferenceValue

        /// <summary>
        /// Sets the reference value for an keySelector value based on the reference type passed into TRef, but also returns
        /// the keySelector key value of the set entity for setting the actual related property value back in the parent entity.
        /// </summary>
        /// <typeparam name="TRef">The type value to be passed in to set</typeparam>
        /// <param name="value">The instance of the type value or <c>null</c> if being cleared out</param>
        /// <param name="keySelector">The key selector being used to retrieve the appropriate key value from the entity.</param>
        /// <returns>The key of the passed in value or <c>null</c> if no entity passed in</returns>
        protected virtual string SetReferenceValue<TRef>(TRef value, Func<TRef, string> keySelector) where TRef : BaseEntity<TRef>, new()
        {
            if (value == null)
                return null;

            string keyVal = keySelector(value);
            if (string.IsNullOrWhiteSpace(keyVal))
                return null;

            string key = string.Concat(typeof(TRef).Name, "-", keyVal ?? "");
            lock (_refsSync)
            {
                _refs[key] = value;
            }

            return keyVal;
        }//SetReferenceValue

        #endregion

        #region Statics

        [BsonIgnore, JsonIgnore]
        public static readonly string ClassTypeName = typeof(TEntity).Name;

        [BsonIgnore, JsonIgnore]
        private static object _repoSync = new object();
        [BsonIgnore, JsonIgnore]
        private static MongoRepository<TEntity> _myRepository = null;
        /// <summary>
        /// Gets the repository used for manipulating this instance.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public static MongoRepository<TEntity> Repository
        {
            get
            {
                if (_myRepository != null)
                    return _myRepository;

                lock (_repoSync)
                {
                    if (_myRepository != null)
                        return _myRepository;

                    var myRepo = typeof(TEntity)
                        .GetCustomAttributes(typeof(RepositoryAttribute), true)
                        .OfType<RepositoryAttribute>()
                        .FirstOrDefault();

                    string cName = (myRepo == null || string.IsNullOrWhiteSpace(myRepo.ConnectionStringName)) ? "MongoServerSettings" : myRepo.ConnectionStringName;
                    var connString = ConfigurationManager.ConnectionStrings[cName];
                    if (connString == null)
                        throw new ApplicationException(string.Format("MongoDB connection string '{0}' is missing from configuration", cName));
                    _myRepository = new MongoRepository<TEntity>(connString.ConnectionString, ClassTypeName);
                }

                return _myRepository;
            }
        }//end: Repository

        /// <summary>
        /// Gets the entity by its Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>The entity found by the Id or <c>null</c> if no entity was found by that id.</returns>
        public static TEntity GetById(string id)
        {
            using (new InstrumentationContext("{0}.GetById", ClassTypeName))
            {
                if (string.IsNullOrWhiteSpace(id))
                    return default(TEntity);

                var me = Repository.GetById(id.ToLowerInvariant());
                if (me == null || me.IsDeleted)
                    return null;
                return me;
            }
        }

        /// <summary>
        /// Retrieve entities in batches
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="batchSize"></param>
        /// <returns></returns>
        public static IEnumerable<TEntity> GetByIds(IEnumerable<string> ids, int batchSize = 250)
        {
            using (new InstrumentationContext("{0}.GetByIds", ClassTypeName))
            {
                if (ids == null || !ids.Any()) return new List<TEntity>();

                return ids
                    .Distinct()
                    .Select(id => id.ToLowerInvariant())
                    .Batch(batchSize)
                    .Select(m =>
                        Repository.AsQueryable()
                        .Where(r => m.Contains(r.Id) && r.IsDeleted != true)
                        .ToList()
                    )
                    .SelectMany(m => m);
            }
        }

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching.
        /// </summary>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static IQueryable<TEntity> AsQueryable()
        {
            // We use e.IsDeleted != true so that the Mongo interpreter will still include entities that don't have the IsDeleted property
            return Repository.AsQueryable().Where(e => e.IsDeleted != true);
        }

        /// <summary>
        /// Updates one matching document in this collection.
        /// </summary>
        /// <param name="query">The query (usually a QueryDocument or constructed using the Query builder).</param>
        /// <param name="update">The update to perform on the matching document.</param>
        /// <param name="flags">The optional flags to set on the update operation. e.g. multi, none, etc.</param>
        /// <returns> A WriteConcernResult (or null if WriteConcern is disabled).</returns>
        public static WriteConcernResult Update(IMongoQuery query, IMongoUpdate update, UpdateFlags flags = UpdateFlags.None)
        {
            return Repository.Collection.Update(query, update, flags);
        }

        /// <summary>
        /// Finds one matching document using the query and sortBy parameters and applies the specified update to it.
        /// </summary>
        /// <param name="query">The query (usually a QueryDocument or constructed using the Query builder).</param>
        /// <param name="update">The update to apply to the matching document.</param>
        /// <param name="upsert">Whether to do an upsert if no matching document is found.</param>
        /// <returns>The returned <typeparamref name="TEntity"/> either modified or original</returns>
        public static TEntity FindAndModify(IMongoQuery query, IMongoUpdate update, bool returnNew = true, bool upsert = false)
        {
            FindAndModifyArgs args = new FindAndModifyArgs()
            {
                Query = Query.And(Query.IsNotDeleted(), query),
                SortBy = SortBy.Ascending("_id"),
                Update = update,
                Upsert = upsert,
                VersionReturned = returnNew ? FindAndModifyDocumentVersion.Modified : FindAndModifyDocumentVersion.Original
            };
            var result = Repository.Collection.FindAndModify(args);
            if (result.Ok && result.ModifiedDocument != null)
                return result.GetModifiedDocumentAs<TEntity>();

            return default(TEntity);
        }

        /// <summary>
        /// Counts the total entities in the repository.
        /// </summary>
        /// <returns>Count of entities in the collection.</returns>
        public static long Count()
        {
            using (new InstrumentationContext("{0}.Count", ClassTypeName))
            {
                return Repository.Count();
            }
        }

        /// <summary>
        /// Deletes an entity from the repository by its id.
        /// </summary>
        /// <param name="id">The entity's id.</param>
        /// <exception cref="201"></exception>
        public static void Delete(string id)
        {
            using (new InstrumentationContext("{0}.Delete", ClassTypeName))
            {
                if (string.IsNullOrWhiteSpace(id))
                    throw new ArgumentNullException("id");

                var entity = GetById(id);
                if (entity != null)
                    entity.Delete();
            }
        }

        /// <summary>
        /// Deletes a set of documents that match a given query.
        /// </summary>
        /// <param name="query">The query to use that finds the documents to remove.</param>
        /// <returns>A write concern result or null if not supported by the collection.</returns>
        public static WriteConcernResult Delete(IMongoQuery query)
        {
            using (new InstrumentationContext("{0}.Delete", ClassTypeName))
            {
                if (query == null)
                    throw new ArgumentNullException("query");

                var update = Updates.Set(e => e.IsDeleted, true)
                    .Set(e => e.ModifiedDate, DateTime.UtcNow);

                return Repository.Collection.Update(query, update, UpdateFlags.Multi);
            }
        }

        /// <summary>
        /// Deletes all entities in the repository.
        /// </summary>
        public static void DeleteAll()
        {
            using (new InstrumentationContext("{0}.DeleteAll", ClassTypeName))
            {
                Repository.DeleteAll();
            }
        }

        /// <summary>
        /// Checks if the entity exists for given predicate.
        /// </summary>
        /// <param name="predicate">The expression.</param>
        /// <returns>True when an entity matching the predicate exists, false otherwise.</returns>
        public static bool Exists(Expression<Func<TEntity, bool>> predicate)
        {
            using (new InstrumentationContext("{0}.Exists", ClassTypeName))
            {
                return Repository.Exists(predicate);
            }
        }

        #endregion

        #region Static Query Helpers

        /// <summary>
        /// Helper methods for querying the base entity's repository.
        /// </summary>
        /// <remarks>Yeah, that's right, some wrapper classes, oh yeah!</remarks>
        public class Query
        {
            /// <summary>
            /// Gets a mongo cursor for enumerating, filtering and projecting a raw mongoDB query against
            /// this entitie's collection directly. This IS performance, so use it b!@$#.
            /// </summary>
            /// <param name="query">The optional query to use for the find operation, if <c>null</c> or default uses FindAll instead.</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static MongoCursor<TEntity> Find(IMongoQuery query = null)
            {
                return query == null ? Repository.Collection.FindAll() : Repository.Collection.Find(And(query, IsNotDeleted()));
            }

            /// <summary>
            /// <para>Builds a query that checks if the entity is deleted</para>
            /// <para>Called when writing a query that bypasses .Find and/or .AsQueryable</para>
            /// </summary>
            /// <returns></returns>
            public static IMongoQuery IsNotDeleted()
            {
                return NE(e => e.IsDeleted, true);
            }

            /// <summary>
            /// Build mongo query using provided queryJson string and return
            /// a mongo cursor.
            /// </summary>
            /// <param name="queryJson"></param>
            /// <returns></returns>
            public static MongoCursor<TEntity> Find(string queryJson)
            {
                return Find(ParseQuery(queryJson));
            }

            /// <summary>
            /// Build a mongo query using provied queryJson
            /// </summary>
            /// <param name="queryJson"></param>
            /// <returns></returns>
            public static IMongoQuery ParseQuery(string queryJson)
            {
                var query = BsonDocument.Parse(queryJson);
                return new QueryDocument(query);
            }

            /// <summary>
            /// Adds one or more field names to be included in the results.
            /// Id, CreatedDate and ModifiedDate are ALWAYS included because they are required base properties.
            /// Also, all other fields marked with [BsonRequired] are also automatically included, sorry, they just have to be.
            /// </summary>
            /// <param name="memberExpressions">The member expressions</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static IMongoFields IncludeFields(params Expression<Func<TEntity, object>>[] memberExpressions)
            {
                // Get a list of the required fields so those are always included, 'cause they have to be for deserialization
                var required = RequiredFields();
                required.AddRange(memberExpressions.Select(e =>
                    {
                        // This is where we reverse engineer our lambda expressions passed in because
                        //  we actually have to append it to the plain fields builder (can't use the typed one inferred here).
                        var exp = e.Body as MemberExpression;
                        if (exp == null)
                        {
                            UnaryExpression ubody = (UnaryExpression)e.Body;
                            exp = ubody.Operand as MemberExpression;
                        }
                        if (exp != null)
                        {
                            string name = exp.Member.Name;
                            var elem = exp.Member.GetCustomAttribute(typeof(BsonElementAttribute), true) as BsonElementAttribute;
                            if (elem != null)
                                name = elem.ElementName;

                            return name;
                        }
                        return e.Body.ToString();
                    }));
                return Fields.Include(required.Distinct().ToArray());
            }

            /// <summary>
            /// Gets the entity's required field expressions.
            /// </summary>
            /// <returns></returns>
            public static List<string> RequiredFields()
            {
                return typeof(TEntity)
                    .GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public)
                    .Where(p => p.GetCustomAttributes(typeof(BsonRequiredAttribute), true).Any())
                    .Select(p =>
                    {
                        // here's where the fun comes in, the property may have a BsonElement attribute changing the name, so we need
                        //  to get that of course if that's the case.
                        string name = p.Name;
                        var elem = p.GetCustomAttributes(typeof(BsonElementAttribute), true).FirstOrDefault() as BsonElementAttribute;
                        if (elem != null)
                            name = elem.ElementName;
                        return name;
                    }).ToList();
            }

            /// <summary>
            /// Adds one or more field names to be excluded from the results.
            /// </summary>
            /// <param name="memberExpressions">The member expressions</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static FieldsBuilder<TEntity> ExcludeFields(params Expression<Func<TEntity, object>>[] memberExpressions)
            {
                return Fields<TEntity>.Exclude(memberExpressions);
            }

            /// <summary>
            /// Tests that the named array element contains all of the values (see $all).
            /// </summary>
            /// <typeparam name="TValue">The type of the enumerable member values.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="values">The values to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery All<TValue>(Expression<Func<TEntity, IEnumerable<TValue>>> memberExpression, IEnumerable<TValue> values)
            {
                return Query<TEntity>.All(memberExpression, values);
            }

            /// <summary>
            /// Tests that all the queries are true (see $and in newer versions of the server).
            /// </summary>
            /// <param name="queries">A list of subqueries.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery And(IEnumerable<IMongoQuery> queries)
            {
                return MongoDB.Driver.Builders.Query.And(queries);
            }

            /// <summary>
            /// Tests that all the queries are true (see $and in newer versions of the server).
            /// </summary>
            /// <param name="queries">A list of subqueries.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery And(params IMongoQuery[] queries)
            {
                return MongoDB.Driver.Builders.Query.And(queries);
            }

            /// <summary>
            /// Tests that at least one of the subqueries are true (see $or).
            /// </summary>
            /// <param name="queries">A list of subqueries.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery Or(IEnumerable<IMongoQuery> queries)
            {
                return MongoDB.Driver.Builders.Query.Or(queries);
            }

            /// <summary>
            /// Tests that at least one of the subqueries are true (see $or).
            /// </summary>
            /// <param name="queries">A list of subqueries.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery Or(params IMongoQuery[] queries)
            {
                return MongoDB.Driver.Builders.Query.Or(queries);
            }

            /// <summary>
            /// Tests that at least one item of the named array element matches a query (see $elemMatch).
            /// </summary>
            /// <typeparam name="TValue">The type of the enumerable member values.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="elementQueryBuilderFunction">A function that builds a query using the supplied query builder.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery ElemMatch<TValue>(Expression<Func<TEntity, IEnumerable<TValue>>> memberExpression, Func<QueryBuilder<TValue>, IMongoQuery> elementQueryBuilderFunction)
            {
                return Query<TEntity>.ElemMatch(memberExpression, elementQueryBuilderFunction);
            }


            /// <summary>
            /// Tests that any of the values in the named array element is equal to some value.
            /// </summary>
            /// <typeparam name="TValue">The type of the value.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="value">The value to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery EQ<TValue>(Expression<Func<TEntity, IEnumerable<TValue>>> memberExpression, TValue value)
            {
                return Query<TEntity>.EQ(memberExpression, value);
            }

            /// <summary>
            /// Tests that the value of the named element is equal to some value.
            /// </summary>
            /// <typeparam name="TMember">The member type.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="value">The value to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery EQ<TMember>(Expression<Func<TEntity, TMember>> memberExpression, TMember value)
            {
                return Query<TEntity>.EQ(memberExpression, value);
            }


            /// <summary>
            /// Tests that any of the values in the named array element is equal to some value using a case insensitive search.
            /// </summary>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="value">The value to compare to.</param>
            /// <returns>
            /// An IMongoQuery.
            /// </returns>
            public static IMongoQuery EQIgnoreCase(Expression<Func<TEntity, IEnumerable<string>>> memberExpression, string value)
            {
                return Query<TEntity>.Matches(
                    memberExpression, 
                    new BsonRegularExpression(new Regex(
                        string.Format("^{0}$", Regex.Escape(value)), RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Singleline)));
            }

            /// <summary>
            /// Tests that the value of the named element is equal to some value using a case insensitive search.
            /// </summary>
            /// <typeparam name="TMember">The member type.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="value">The value to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery EQIgnoreCase(Expression<Func<TEntity, string>> memberExpression, string value)
            {
                return Query<TEntity>.Matches(
                    memberExpression,
                    new BsonRegularExpression(new Regex(
                        string.Format("^{0}$", Regex.Escape(value)), RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Singleline)));
            }

            /// <summary>
            /// Tests that an element of that name does or does not exist (see $exists).
            /// </summary>
            /// <typeparam name="TMember">The member type.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery Exists<TMember>(Expression<Func<TEntity, TMember>> memberExpression)
            {
                return Query<TEntity>.Exists(memberExpression);
            }

            /// <summary>
            /// Tests that a location element specified by name intersects with the geometry (see $geoIntersects).
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <typeparam name="TCoordinates">The type of the coordinates.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="geometry">The geometry.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery GeoIntersects<TMember, TCoordinates>(Expression<Func<TEntity, TMember>> memberExpression, GeoJsonGeometry<TCoordinates> geometry) where TCoordinates : GeoJsonCoordinates
            {
                return Query<TEntity>.GeoIntersects(memberExpression, geometry);
            }

            /// <summary>
            /// Tests that any of the values in the named array element is greater than some value (see $lt).
            /// </summary>
            /// <typeparam name="TValue">The type of the value.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="value">The value to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery GT<TValue>(Expression<Func<TEntity, IEnumerable<TValue>>> memberExpression, TValue value)
            {
                return Query<TEntity>.GT(memberExpression, value);
            }

            /// <summary>
            /// Tests that the value of the named element is greater than some value (see $gt).
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="value">The value to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery GT<TMember>(Expression<Func<TEntity, TMember>> memberExpression, TMember value)
            {
                return Query<TEntity>.GT(memberExpression, value);
            }

            /// <summary>
            /// Tests that any of the values in the named array element is greater than or equal to some value (see $gte).
            /// </summary>
            /// <typeparam name="TValue">The type of the value.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="value">The value to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery GTE<TValue>(Expression<Func<TEntity, IEnumerable<TValue>>> memberExpression, TValue value)
            {
                return Query<TEntity>.GTE(memberExpression, value);
            }

            /// <summary>
            /// Tests that the value of the named element is greater than or equal to some value (see $gte).
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="value">The value to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery GTE<TMember>(Expression<Func<TEntity, TMember>> memberExpression, TMember value)
            {
                return Query<TEntity>.GTE(memberExpression, value);
            }

            /// <summary>
            /// Tests that any of the values in the named array element are equal to one of a list of values (see $in).
            /// </summary>
            /// <typeparam name="TValue">The type of the enumerable member values.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="values">The values to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery In<TValue>(Expression<Func<TEntity, IEnumerable<TValue>>> memberExpression, IEnumerable<TValue> values)
            {
                return Query<TEntity>.In(memberExpression, values);
            }

            /// <summary>
            /// Tests that the value of the named element is equal to one of a list of values (see $in).
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="values">The values to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery In<TMember>(Expression<Func<TEntity, TMember>> memberExpression, IEnumerable<TMember> values)
            {
                return Query<TEntity>.In(memberExpression, values);
            }

            /// <summary>
            /// Tests that any of the values in the named array element is less than some value (see $lt).
            /// </summary>
            /// <typeparam name="TValue">The type of the value.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="value">The value to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery LT<TValue>(Expression<Func<TEntity, IEnumerable<TValue>>> memberExpression, TValue value)
            {
                return Query<TEntity>.LT(memberExpression, value);
            }

            /// <summary>
            /// Tests that the value of the named element is less than some value (see $lt).
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="value">The value to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery LT<TMember>(Expression<Func<TEntity, TMember>> memberExpression, TMember value)
            {
                return Query<TEntity>.LT(memberExpression, value);
            }

            /// <summary>
            /// Tests that any of the values in the named array element is less than or equal to some value (see $lte).
            /// </summary>
            /// <typeparam name="TValue">The type of the value.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="value">The value to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery LTE<TValue>(Expression<Func<TEntity, IEnumerable<TValue>>> memberExpression, TValue value)
            {
                return Query<TEntity>.LTE(memberExpression, value);
            }

            /// <summary>
            /// Tests that the value of the named element is less than or equal to some value (see $lte).
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="value">The value to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery LTE<TMember>(Expression<Func<TEntity, TMember>> memberExpression, TMember value)
            {
                return Query<TEntity>.LTE(memberExpression, value);
            }

            /// <summary>
            /// Tests that any of the values in the named array element matches a regular expression (see $regex).
            /// </summary>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="regex">The regex.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery Matches(Expression<Func<TEntity, IEnumerable<string>>> memberExpression, BsonRegularExpression regex)
            {
                return Query<TEntity>.Matches(memberExpression, regex);
            }

            /// <summary>
            /// Tests that the value of the named element matches a regular expression (see $regex).
            /// </summary>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="regex">The regex.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery Matches(Expression<Func<TEntity, string>> memberExpression, BsonRegularExpression regex)
            {
                return Query<TEntity>.Matches(memberExpression, regex);
            }

            /// <summary>
            /// Matcheses the string.
            /// </summary>
            /// <param name="memberExpression">The member expression.</param>
            /// <param name="match">The match.</param>
            /// <param name="ands">The ands.</param>
            public static void MatchesString(Expression<Func<TEntity, string>> memberExpression, string match, List<IMongoQuery> ands)
            {
                if(!string.IsNullOrEmpty(match))
                {
                    ands.Add(Query<TEntity>.Matches(memberExpression, new BsonRegularExpression(match, "i")));
                }
            }

            /// <summary>
            /// Matcheses the string.
            /// </summary>
            /// <param name="memberExpression">The member expression.</param>
            /// <param name="match">The match.</param>
            /// <param name="ands">The ands.</param>
            public static IMongoQuery MatchesString(Expression<Func<TEntity, string>> memberExpression, string match)
            {
                if (!string.IsNullOrEmpty(match))
                {
                    return Query<TEntity>.Matches(memberExpression, new BsonRegularExpression(match, "i"));
                }

                return null;
            }

            /// <summary>
            /// Equals the string
            /// </summary>
            /// <param name="memberExpression">The member expression.</param>
            /// <param name="match">The match.</param>
            /// <param name="ands">The ands.</param>
            public static void EqualsString(Expression<Func<TEntity, string>> memberExpression, string match, List<IMongoQuery> ands)
            {
                if (!string.IsNullOrEmpty(match))
                {
                    ands.Add(Query<TEntity>.EQ(memberExpression, match));
                }
            }

            /// <summary>
            /// Equals the string
            /// </summary>
            /// <param name="memberExpression">The member expression.</param>
            /// <param name="match">The match.</param>
            /// <param name="ands">The ands.</param>
            public static IMongoQuery EqualsString(Expression<Func<TEntity, string>> memberExpression, string match)
            {
                if (!string.IsNullOrEmpty(match))
                    return Query<TEntity>.EQ(memberExpression, match);

                return null;
            }

            /// <summary>
            /// Matcheses the enum.
            /// </summary>
            /// <typeparam name="TEnum">The type of the enum.</typeparam>
            /// <param name="memberExpression">The member expression.</param>
            /// <param name="match">The match.</param>
            /// <param name="ands">The ands.</param>
            public static void MatchesEnum<TEnum>(Expression<Func<TEntity, TEnum>> memberExpression, string match, List<IMongoQuery> ands) where TEnum : struct
            {
                TEnum matchedEnum;

                if (Enum.TryParse(match, out matchedEnum))
                {
                    ands.Add(Query.EQ(memberExpression, matchedEnum));
                }
            }

            /// <summary>
            /// Strings the matches multiple fields.
            /// </summary>
            /// <param name="match">The match.</param>
            /// <param name="ands">The ands.</param>
            /// <param name="memberExpressions">The member expressions.</param>
            public static void StringMatchesMultipleFields(string match, List<IMongoQuery> ands, params Expression<Func<TEntity, string>>[] memberExpressions)
            {
                if (!string.IsNullOrEmpty(match))
                {
                    var regex = new BsonRegularExpression(match, "i");
                    List<IMongoQuery> queries = new List<IMongoQuery>();
                    foreach (var query in memberExpressions)
                    {
                        queries.Add(Query<TEntity>.Matches(query, regex));
                    }
                    ands.Add(Or(queries));
                }
            }

            /// <summary>
            /// Tests that any of the values in the named array element matches a regular expression (see $regex).
            /// </summary>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="text">The text.</param>
            /// <returns>
            /// An IMongoQuery.
            /// </returns>
            public static IMongoQuery Matches(Expression<Func<TEntity, IEnumerable<string>>> memberExpression, string text)
            {
                return Query<TEntity>.Matches(memberExpression, new BsonRegularExpression(new Regex(Regex.Escape(text), RegexOptions.CultureInvariant | RegexOptions.IgnoreCase)));
            }

            /// <summary>
            /// Tests that the value of the named element matches a regular expression (see $regex).
            /// </summary>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="text">The text.</param>
            /// <returns>
            /// An IMongoQuery.
            /// </returns>
            public static IMongoQuery Matches(Expression<Func<TEntity, string>> memberExpression, string text)
            {
                return Query<TEntity>.Matches(memberExpression, new BsonRegularExpression(new Regex(Regex.Escape(text), RegexOptions.CultureInvariant | RegexOptions.IgnoreCase)));
            }

            /// <summary>
            /// Tests that the any of the values in the named array element match some value (see $mod).
            /// </summary>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="modulus">The modulus.</param>
            /// <param name="value">The value to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery Mod(Expression<Func<TEntity, IEnumerable<int>>> memberExpression, long modulus, long value)
            {
                return Query<TEntity>.Mod(memberExpression, modulus, value);
            }

            /// <summary>
            /// Tests that the modulus of the value of the named element matches some value (see $mod).
            /// </summary>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="modulus">The modulus.</param>
            /// <param name="value">The value to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery Mod(Expression<Func<TEntity, int>> memberExpression, long modulus, long value)
            {
                return Query<TEntity>.Mod(memberExpression, modulus, value);
            }

            /// <summary>
            /// Tests that none of the values in the named array element is equal to some value (see $ne).
            /// </summary>
            /// <typeparam name="TValue">The type of the value.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="value">The value to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery NE<TValue>(Expression<Func<TEntity, IEnumerable<TValue>>> memberExpression, TValue value)
            {
                return Query<TEntity>.NE(memberExpression, value);
            }

            /// <summary>
            /// Tests that an element does not equal the value (see $ne).
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="value">The value to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery NE<TMember>(Expression<Func<TEntity, TMember>> memberExpression, TMember value)
            {
                return Query<TEntity>.NE(memberExpression, value);
            }

            /// <summary>
            /// Tests that the value of the named element is near a point (see $near).
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <typeparam name="TCoordinates">The type of the coordinates.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="point"The point.></param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery Near<TMember, TCoordinates>(Expression<Func<TEntity, TMember>> memberExpression, GeoJsonPoint<TCoordinates> point) where TCoordinates : GeoJsonCoordinates
            {
                return Query<TEntity>.Near(memberExpression, point);
            }

            /// <summary>
            /// Tests that the value of the named element is near some location (see $near).
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="x">The x value of the origin.</param>
            /// <param name="y">The y value of the origin.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery Near<TMember>(Expression<Func<TEntity, TMember>> memberExpression, double x, double y)
            {
                return Query<TEntity>.Near(memberExpression, x, y);
            }

            /// <summary>
            /// Tests that the value of the named element is near some location (see $near).
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <typeparam name="TCoordinates">The type of the coordinates.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="point">The point.</param>
            /// <param name="maxDistance">The max distance.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery Near<TMember, TCoordinates>(Expression<Func<TEntity, TMember>> memberExpression, GeoJsonPoint<TCoordinates> point, double maxDistance) where TCoordinates : GeoJsonCoordinates
            {
                return Query<TEntity>.Near(memberExpression, point, maxDistance);
            }

            /// <summary>
            /// Tests that the value of the named element is near some location (see $near).
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="x">The x value of the origin.</param>
            /// <param name="y">The y value of the origin.</param>
            /// <param name="maxDistance">The max distance.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery Near<TMember>(Expression<Func<TEntity, TMember>> memberExpression, double x, double y, double maxDistance)
            {
                return Query<TEntity>.Near(memberExpression, x, y, maxDistance);
            }

            /// <summary>
            /// Tests that the value of the named element is near some location (see $near).
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <typeparam name="TCoordinates">The type of the coordinates.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="point">The point.</param>
            /// <param name="maxDistance">The max distance.</param>
            /// <param name="spherical">if set to true [spherical].</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery Near<TMember, TCoordinates>(Expression<Func<TEntity, TMember>> memberExpression, GeoJsonPoint<TCoordinates> point, double maxDistance, bool spherical) where TCoordinates : GeoJsonCoordinates
            {
                return Query<TEntity>.Near(memberExpression, point, maxDistance, spherical);
            }

            /// <summary>
            /// Tests that the value of the named element is near some location (see $near).
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="x">The x value of the origin.</param>
            /// <param name="y">The y value of the origin.</param>
            /// <param name="maxDistance">The max distance.</param>
            /// <param name="spherical">if set to true [spherical].</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery Near<TMember>(Expression<Func<TEntity, TMember>> memberExpression, double x, double y, double maxDistance, bool spherical)
            {
                return Query<TEntity>.Near(memberExpression, x, y, maxDistance, spherical);
            }

            /// <summary>
            /// Tests that an element of that name does not exist (see $exists).
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery NotExists<TMember>(Expression<Func<TEntity, TMember>> memberExpression)
            {
                return Query<TEntity>.NotExists(memberExpression);
            }

            /// <summary>
            /// Tests that the none of the values of the named array element is equal to any item in a list of values (see $nin).
            /// </summary>
            /// <typeparam name="TValue">The type of the enumerable member values.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="values">The values to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery NotIn<TValue>(Expression<Func<TEntity, IEnumerable<TValue>>> memberExpression, IEnumerable<TValue> values)
            {
                return Query<TEntity>.NotIn(memberExpression, values);
            }

            /// <summary>
            /// Tests that the value of the named element is not equal to any item in a list of values (see $nin).
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="values">The values to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery NotIn<TMember>(Expression<Func<TEntity, TMember>> memberExpression, IEnumerable<TMember> values)
            {
                return Query<TEntity>.NotIn(memberExpression, values);
            }

            /// <summary>
            /// Tests that the none of the values of the named array element is equal to any item in a list of values (see $nin) 
            /// that are case insensitive using Regex.
            /// </summary>
            /// <typeparam name="TValue">The type of the enumerable member values.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="values">The values to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery NotInIgnoreCase(Expression<Func<TEntity, IEnumerable<string>>> memberExpression, IEnumerable<string> values)
            {
                var propName = GetPropertyName(memberExpression);
                return MongoDB.Driver.Builders.Query.NotIn(propName, values.Select(v => new BsonRegularExpression(new Regex(
                        string.Format("^{0}$", Regex.Escape(v)), RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Singleline))));
            }

            /// <summary>
            /// Tests that the value of the named element is not equal to any item in a list of values (see $nin) 
            /// that are case insensitive using Regex.
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="values">The values to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery NotInIgnoreCase(Expression<Func<TEntity, string>> memberExpression, IEnumerable<string> values)
            {
                var propName = GetPropertyName(memberExpression);
                return MongoDB.Driver.Builders.Query.NotIn(propName, values.Select(v => new BsonRegularExpression(new Regex(
                        string.Format("^{0}$", Regex.Escape(v)), RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Singleline))));
            }

            /// <summary>
            /// Tests that the size of the named array is equal to some value (see $size).
            /// </summary>
            /// <typeparam name="TValue">The type of the enumerable member values.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="size">The size to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery Size<TValue>(Expression<Func<TEntity, IEnumerable<TValue>>> memberExpression, int size)
            {
                return Query<TEntity>.Size(memberExpression, size);
            }

            /// <summary>
            /// Tests that any of the values in the named array element is equal to some type (see $type).
            /// </summary>
            /// <typeparam name="TValue">The type of the value.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="type">The type to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery Type<TValue>(Expression<Func<TEntity, IEnumerable<TValue>>> memberExpression, BsonType type)
            {
                return Query<TEntity>.Type(memberExpression, type);
            }

            /// <summary>
            /// Tests that the type of the named element is equal to some type (see $type).
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="type">The type to compare to.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery Type<TMember>(Expression<Func<TEntity, TMember>> memberExpression, BsonType type)
            {
                return Query<TEntity>.Type(memberExpression, type);
            }

            /// <summary>
            /// Builds a query from an expression. THIS IS EVIL, SHOULD ONLY BE USED UNDER DIRE CIRCUMSTANCES!!! You've Been Warned!
            /// </summary>
            /// <param name="expression">The expression.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery Where(Expression<Func<TEntity, bool>> expression)
            {
                return Query<TEntity>.Where(expression);
            }

            /// <summary>
            /// Tests that the value of the named element is within the specified geometry (see $within).
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <typeparam name="TCoordinates">The type of the coordinates.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="polygon">The polygon.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery Within<TMember, TCoordinates>(Expression<Func<TEntity, TMember>> memberExpression, GeoJsonPolygon<TCoordinates> polygon) where TCoordinates : GeoJsonCoordinates
            {
                return Query<TEntity>.Within(memberExpression, polygon);
            }

            /// <summary>
            /// Tests that the value of the named element is within a circle (see $within and $center).
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="centerX">The x coordinate of the origin.</param>
            /// <param name="centerY">The y coordinate of the origin.</param>
            /// <param name="radius">The radius of the circle.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery WithinCircle<TMember>(Expression<Func<TEntity, TMember>> memberExpression, double centerX, double centerY, double radius)
            {
                return Query<TEntity>.WithinCircle(memberExpression, centerX, centerY, radius);
            }

            /// <summary>
            /// Tests that the value of the named element is within a circle (see $within and $center).
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="centerX">The x coordinate of the origin.</param>
            /// <param name="centerY">The y coordinate of the origin.</param>
            /// <param name="radius">The radius of the circle.</param>
            /// <param name="spherical">if set to true [spherical].</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery WithinCircle<TMember>(Expression<Func<TEntity, TMember>> memberExpression, double centerX, double centerY, double radius, bool spherical)
            {
                return Query<TEntity>.WithinCircle(memberExpression, centerX, centerY, radius, spherical);
            }

            /// <summary>
            /// Tests that the value of the named element is within a polygon (see $within and $polygon).
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="points">An array of points that defines the polygon (the second dimension must be of length 2).</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery WithinPolygon<TMember>(Expression<Func<TEntity, TMember>> memberExpression, double[,] points)
            {
                return Query<TEntity>.WithinPolygon(memberExpression, points);
            }

            /// <summary>
            /// Tests that the value of the named element is within a rectangle (see $within and $box).
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <param name="memberExpression">The member expression representing the element to test.</param>
            /// <param name="lowerLeftX">The x coordinate of the lower left corner.</param>
            /// <param name="lowerLeftY">The y coordinate of the lower left corner.</param>
            /// <param name="upperRightX">The x coordinate of the upper right corner.</param>
            /// <param name="upperRightY">The y coordinate of the upper right corner.</param>
            /// <returns>An IMongoQuery.</returns>
            public static IMongoQuery WithinRectangle<TMember>(Expression<Func<TEntity, TMember>> memberExpression, double lowerLeftX, double lowerLeftY, double upperRightX, double upperRightY)
            {
                return Query<TEntity>.WithinRectangle(memberExpression, lowerLeftX, lowerLeftY, upperRightX, upperRightY);
            }
        }

        #endregion

        #region Static Update Helpers

        /// <summary>
        /// Helper methods for updates against the base entity's repository.
        /// </summary>
        /// <remarks>Yeah, that's right, some wrapper classes, oh yeah!</remarks>
        public class Updates
        {
            /// <summary>
            /// Adds a value to a named array element if the value is not already in the array (see $addToSet).
            /// </summary>
            /// <typeparam name="TValue">The type of the enumerable member values.</typeparam>
            /// <param name="memberExpression">The member expression.</param>
            /// <param name="value">The value to add to the set.</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static UpdateBuilder<TEntity> AddToSet<TValue>(Expression<Func<TEntity, IEnumerable<TValue>>> memberExpression, TValue value)
            {
                return Update<TEntity>.AddToSet<TValue>(memberExpression, value);
            }

            /// <summary>
            /// Adds a list of values to a named array element adding each value only if
            /// it not already in the array (see $addToSet and $each).
            /// </summary>
            /// <typeparam name="TValue">The type of the enumerable member values.</typeparam>
            /// <param name="memberExpression">The member expression.</param>
            /// <param name="values">The values to add to the set.</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static UpdateBuilder<TEntity> AddToSetEach<TValue>(Expression<Func<TEntity, IEnumerable<TValue>>> memberExpression, IEnumerable<TValue> values)
            {
                return Update<TEntity>.AddToSetEach<TValue>(memberExpression, values);
            }

            /// <summary>
            /// Sets the named element to the bitwise and of its value with another value (see $bit with "and").
            /// </summary>
            /// <param name="memberExpression">The member expression.</param>
            /// <param name="value">The value to be and-ed with the current value.</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static UpdateBuilder<TEntity> BitwiseAnd(Expression<Func<TEntity, int>> memberExpression, int value)
            {
                return Update<TEntity>.BitwiseAnd(memberExpression, value);
            }

            /// <summary>
            /// Sets the named element to the bitwise and of its value with another value (see $bit with "and").
            /// </summary>
            /// <param name="memberExpression">The member expression.</param>
            /// <param name="value">The value to be and-ed with the current value.</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static UpdateBuilder<TEntity> BitwiseAnd(Expression<Func<TEntity, long>> memberExpression, long value)
            {
                return Update<TEntity>.BitwiseAnd(memberExpression, value);
            }

            /// <summary>
            /// Sets the named element to the bitwise or of its value with another value (see $bit with "or").
            /// </summary>
            /// <param name="memberExpression">The member expression.</param>
            /// <param name="value">The value to be or-ed with the current value.</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static UpdateBuilder<TEntity> BitwiseOr(Expression<Func<TEntity, int>> memberExpression, int value)
            {
                return Update<TEntity>.BitwiseOr(memberExpression, value);
            }

            /// <summary>
            /// Sets the named element to the bitwise or of its value with another value (see $bit with "or").
            /// </summary>
            /// <param name="memberExpression">The member expression.</param>
            /// <param name="value">The value to be or-ed with the current value.</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static UpdateBuilder<TEntity> BitwiseOr(Expression<Func<TEntity, long>> memberExpression, long value)
            {
                return Update<TEntity>.BitwiseOr(memberExpression, value);
            }

            /// <summary>
            /// Combines several UpdateBuilders into a single UpdateBuilder.
            /// </summary>
            /// <param name="updates">The UpdateBuilders to combine.</param>
            /// <returns>A combined UpdateBuilder. The builder (so method calls can be chained).</returns>
            public static UpdateBuilder<TEntity> Combine(IEnumerable<IMongoUpdate> updates)
            {
                return Update<TEntity>.Combine(updates);
            }

            /// <summary>
            /// Combines several UpdateBuilders into a single UpdateBuilder.
            /// </summary>
            /// <param name="updates">The updates to combine.</param>
            /// <returns>A combined UpdateBuilder. The builder (so method calls can be chained).</returns>
            public static UpdateBuilder<TEntity> Combine(params IMongoUpdate[] updates)
            {
                return Update<TEntity>.Combine(updates);
            }

            /// <summary>
            /// Increments the named element by a value (see $inc).
            /// </summary>
            /// <param name="memberExpression">The member expression.</param>
            /// <param name="value">The value to increment by.</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static UpdateBuilder<TEntity> Inc(Expression<Func<TEntity, double>> memberExpression, double value)
            {
                return Update<TEntity>.Inc(memberExpression, value);
            }

            /// <summary>
            /// Increments the named element by a value (see $inc).
            /// </summary>
            /// <param name="memberExpression">The member expression.</param>
            /// <param name="value">The value to increment by.</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static UpdateBuilder<TEntity> Inc(Expression<Func<TEntity, int>> memberExpression, int value)
            {
                return Update<TEntity>.Inc(memberExpression, value);
            }

            /// <summary>
            /// Increments the named element by a value (see $inc).
            /// </summary>
            /// <param name="memberExpression">The member expression.</param>
            /// <param name="value">The value to increment by.</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static UpdateBuilder<TEntity> Inc(Expression<Func<TEntity, long>> memberExpression, long value)
            {
                return Update<TEntity>.Inc(memberExpression, value);
            }

            /// <summary>
            /// Removes the first value from the named array element (see $pop).
            /// </summary>
            /// <typeparam name="TValue">The type of the enumerable member values.</typeparam>
            /// <param name="memberExpression">The member expression.</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static UpdateBuilder<TEntity> PopFirst<TValue>(Expression<Func<TEntity, IEnumerable<TValue>>> memberExpression)
            {
                return Update<TEntity>.PopFirst(memberExpression);
            }

            /// <summary>
            /// Removes the last value from the named array element (see $pop).
            /// </summary>
            /// <typeparam name="TValue">The type of the enumerable member values.</typeparam>
            /// <param name="memberExpression">The member expression.</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static UpdateBuilder<TEntity> PopLast<TValue>(Expression<Func<TEntity, IEnumerable<TValue>>> memberExpression)
            {
                return Update<TEntity>.PopLast(memberExpression);
            }

            /// <summary>
            /// Removes all values from the named array element that match some query (see $pull).
            /// </summary>
            /// <typeparam name="TValue">The type of the enumerable member values.</typeparam>
            /// <param name="memberExpression">The member expression.</param>
            /// <param name="elementQueryBuilderFunction">A function that builds a query using the supplied query builder.</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static UpdateBuilder<TEntity> Pull<TValue>(Expression<Func<TEntity, IEnumerable<TValue>>> memberExpression, Func<QueryBuilder<TValue>, IMongoQuery> elementQueryBuilderFunction)
            {
                return Update<TEntity>.Pull(memberExpression, elementQueryBuilderFunction);
            }

            /// <summary>
            /// Removes all values from the named array element that are equal to some value (see $pull).
            /// </summary>
            /// <typeparam name="TValue">The type of the enumerable member values.</typeparam>
            /// <param name="memberExpression">The member expression.</param>
            /// <param name="value">The value to remove.</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static UpdateBuilder<TEntity> Pull<TValue>(Expression<Func<TEntity, IEnumerable<TValue>>> memberExpression, TValue value)
            {
                return Update<TEntity>.Pull(memberExpression, value);
            }

            /// <summary>
            /// Removes all values from the named array element that are equal to any of
            /// a list of values (see $pullAll).
            /// </summary>
            /// <typeparam name="TValue">The type of the enumerable member values.</typeparam>
            /// <param name="memberExpression">The member expression.</param>
            /// <param name="values">The values to remove.</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static UpdateBuilder<TEntity> PullAll<TValue>(Expression<Func<TEntity, IEnumerable<TValue>>> memberExpression, IEnumerable<TValue> values)
            {
                return Update<TEntity>.PullAll(memberExpression, values);
            }

            /// <summary>
            /// Adds a value to the end of the named array element (see $push).
            /// </summary>
            /// <typeparam name="TValue">The type of the enumerable member values.</typeparam>
            /// <param name="memberExpression">The member expression.</param>
            /// <param name="value">The value to add to the end of the array.</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static UpdateBuilder<TEntity> Push<TValue>(Expression<Func<TEntity, IEnumerable<TValue>>> memberExpression, TValue value)
            {
                return Update<TEntity>.Push(memberExpression, value);
            }

            /// <summary>
            /// Adds a list of values to the end of the named array element (see $pushAll).
            /// </summary>
            /// <typeparam name="TValue">The type of the enumerable member values.</typeparam>
            /// <param name="memberExpression">The member expression.</param>
            /// <param name="values">The values to add to the end of the array.</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static UpdateBuilder<TEntity> PushAll<TValue>(Expression<Func<TEntity, IEnumerable<TValue>>> memberExpression, IEnumerable<TValue> values)
            {
                return Update<TEntity>.PushAll(memberExpression, values);
            }

            /// <summary>
            /// Adds a list of values to the end of the named array element (see $push and $each).
            /// </summary>
            /// <typeparam name="TValue">The type of the value.</typeparam>
            /// <param name="memberExpression">The member expression.</param>
            /// <param name="values">The values to add to the end of the array.</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static UpdateBuilder<TEntity> PushEach<TValue>(Expression<Func<TEntity, IEnumerable<TValue>>> memberExpression, IEnumerable<TValue> values)
            {
                return Update<TEntity>.PushEach(memberExpression, values);
            }

            /// <summary>
            /// Adds a list of values to the end of the named array element (see $push and $each).
            /// </summary>
            /// <typeparam name="TValue">The type of the value.</typeparam>
            /// <param name="memberExpression">The member expression.</param>
            /// <param name="options">The options.</param>
            /// <param name="values">The values to add to the end of the array.</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static UpdateBuilder<TEntity> PushEach<TValue>(Expression<Func<TEntity, IEnumerable<TValue>>> memberExpression, Action<PushEachOptionsBuilder<TValue>> options, IEnumerable<TValue> values)
            {
                return Update<TEntity>.PushEach(memberExpression, options, values);
            }

            /// <summary>
            /// Adds a list of values to the end of the named array element (see $push and $each).
            /// </summary>
            /// <typeparam name="TValue">The type of the value.</typeparam>
            /// <param name="memberExpression">The member expression.</param>
            /// <param name="options">The options.</param>
            /// <param name="values">The values to add to the end of the array.</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static UpdateBuilder<TEntity> PushEach<TValue>(Expression<Func<TEntity, IEnumerable<TValue>>> memberExpression, PushEachOptions options, IEnumerable<TValue> values)
            {
                return Update<TEntity>.PushEach(memberExpression, options, values);
            }

            /// <summary>
            /// Replaces the entire document with a new document (the _id must remain the same).
            /// </summary>
            /// <param name="document">The replacement document.</param>
            /// <returns>An UpdateWrapper.</returns>
            public static IMongoUpdate Replace(TEntity document)
            {
                return Update<TEntity>.Replace(document);
            }

            /// <summary>
            /// Sets the value of the named element to a new value (see $set).
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <param name="memberExpression">The member expression.</param>
            /// <param name="value">The new value.</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static UpdateBuilder<TEntity> Set<TMember>(Expression<Func<TEntity, TMember>> memberExpression, TMember value)
            {
                return Update<TEntity>.Set(memberExpression, value);
            }

            /// <summary>
            /// Sets the value of the named element to the specified value only when an insert
            /// occurs as part of an upsert operation (see $setOnInsert).
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <param name="memberExpression">The member expression.</param>
            /// <param name="value"> The new value.</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static UpdateBuilder<TEntity> SetOnInsert<TMember>(Expression<Func<TEntity, TMember>> memberExpression, TMember value)
            {
                return Update<TEntity>.SetOnInsert(memberExpression, value);
            }

            /// <summary>
            /// Removes the named element from the document (see $unset).
            /// </summary>
            /// <typeparam name="TMember">The type of the member.</typeparam>
            /// <param name="memberExpression">The member expression.</param>
            /// <returns>The builder (so method calls can be chained).</returns>
            public static UpdateBuilder<TEntity> Unset<TMember>(Expression<Func<TEntity, TMember>> memberExpression)
            {
                return Update<TEntity>.Unset(memberExpression);
            }
        }

        #endregion

        #region Serialization Callbacks

        /// <summary>
        /// Called when this instance of this class is serializing (but before
        /// actually being serialized). This sets up the JsonExtensionData
        /// property, _UIMetadata, with any key value pairs from the BsonDocument
        /// Metadata property.
        /// </summary>
        /// <param name="context">The context.</param>
        [OnSerializing]
        internal void OnSerializing(StreamingContext context)
        {
            // Set _UIMetadata to prepare for serialization to JSON
            _UIMetadata.Clear();
            foreach (var key in Metadata.Names)
            {
                BsonValue v = Metadata.GetValue(key);
                object val = BsonTypeMapper.MapToDotNetValue(v);
                _UIMetadata.Add(key, val);
            }
        }

        /// <summary>
        /// Called when this instance of this class has been deserialized. 
        /// This sets up the BsonExtraElements property, Metadata, with any 
        /// key value pairs from the JsonExtensionData _UIMetadata property.
        /// </summary>
        /// <param name="context">The context.</param>
        [OnDeserialized]
        internal void OnDeserialized(StreamingContext context)
        {
            // Set Metadata (BSON) from the extra elements out of our JSON deserialization
            foreach (var item in _UIMetadata)
            {
                /// Special handling for JArrays.  We're going to assume they're all strings for now, since that's all we need support for
                if (item.Value is JArray)
                {
                    var arrayVal = ((JArray)item.Value).Select(jt => jt.ToObject<object>()).ToArray();
                    Metadata.SetRawValue(item.Key, arrayVal);
                }
                else
                    Metadata.SetRawValue(item.Key, item.Value);
            }
            Metadata.FixStupid(this);
        }

        #endregion Serialization Callbacks

        /// <summary>
        /// Given an expression, extract the listed property name; similar to reflection but with familiar LINQ+lambdas.  Technique @via http://stackoverflow.com/a/16647343/1037948
        /// </summary>
        /// <remarks>Cheats and uses the tostring output -- Should consult performance differences</remarks>
        /// <typeparam name="TModel">the model type to extract property names</typeparam>
        /// <typeparam name="TValue">the value type of the expected property</typeparam>
        /// <param name="propertySelector">expression that just selects a model property to be turned into a string</param>
        /// <param name="delimiter">Expression toString delimiter to split from lambda param</param>
        /// <param name="endTrim">Sometimes the Expression toString contains a method call, something like "Convert(x)", so we need to strip the closing part from the end</param>
        /// <returns>indicated property name</returns>
        private static string GetPropertyName<TModel, TValue>(Expression<Func<TModel, TValue>> propertySelector, char delimiter = '.', char endTrim = ')')
        {
            var asString = propertySelector.ToString(); // gives you: "o => o.Whatever"
            var firstDelim = asString.IndexOf(delimiter); // make sure there is a beginning property indicator; the "." in "o.Whatever" -- this may not be necessary?

            return firstDelim < 0
                ? asString
                : asString.Substring(firstDelim + 1).TrimEnd(endTrim);
        }
    }

    #region EntityEventArgs

    /// <summary>
    /// Represents an entity event argument as passed to entity events AFTER they occur.
    /// </summary>
    /// <typeparam name="TEntity">The type of entity that this entity event argument represents or is acted upon.</typeparam>
    public class EntityEventArgs<TEntity> : EventArgs where TEntity : BaseEntity<TEntity>, new()
    {
        /// <summary>
        /// Initializes a new instance of the CancelableEntityEventArgs class passing the entity value.
        /// </summary>
        /// <param name="entity">The entity value as it exists after the event occurred</param>
        public EntityEventArgs(TEntity entity)
        {
            this.Entity = entity;
        }//ctor

        /// <summary>
        /// Gets the Entity that this event was triggered from or the entity that was impacted by the event.
        /// </summary>
        public TEntity Entity { get; private set; }
    }//EntityEventArgs<TEntity>

    /// <summary>
    /// Represents a cancelable entity event argument as passed to entity events BEFORE they occur.
    /// </summary>
    /// <typeparam name="TEntity">The type of entity that this entity event argument represents or will be acted upon.</typeparam>
    public class CancelableEntityEventArgs<TEntity> : EntityEventArgs<TEntity> where TEntity : BaseEntity<TEntity>, new()
    {
        /// <summary>
        /// Initializes a new instance of the CancelableEntityEventArgs class passing the entity value.
        /// </summary>
        /// <param name="entity">The entity value as it exists before the event occurrs</param>
        public CancelableEntityEventArgs(TEntity entity) : base(entity)
        {
            this.Canceled = false;
        }//ctor

        /// <summary>
        /// Gets or sets a value indicating whether the event should be canceled.
        /// </summary>
        public bool Canceled { get; set; }
    }//CancelableEntityEventArgs<TEntity>

    #endregion
}
