﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// This is status of the current cases.
    /// </summary>
    public enum CaseStatus : int
    {
        Open = 0,
        Closed = 1,
        Cancelled = 2,
        Requested = 3,
        Inquiry = 4
    }
}
