﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum EventDateType : int
    {
        EndPolicyBasedOnEventDate = 0,
        EndPolicyBasedOnCaseStartDate = 1,
        EndPolicyBasedOnCaseEndDate = 2,
        StartPolicyBasedOnEventDate = 3,
        EndPolicyBasedOnPeriod = 4
    }
}
