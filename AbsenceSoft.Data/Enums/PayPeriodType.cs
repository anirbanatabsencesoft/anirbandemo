﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum PayPeriodType : int
    {
        Weekly = 1,
        BiWeekly = 2,
        SemiMonthly = 3,
        Monthly = 4
    }
}
