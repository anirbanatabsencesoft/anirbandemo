﻿namespace AbsenceSoft.Data.Enums
{
    public enum CaseExportFormat : int
    {
        Pdf = 1,
        Zip = 2
    }
}
