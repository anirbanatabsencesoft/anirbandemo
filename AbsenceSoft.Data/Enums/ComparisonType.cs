﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum ComparisonType
    {
        /// <summary>
        /// Value to be compared is equal to the specified rule value.
        /// </summary>
        Equal,
        /// <summary>
        /// Value to be compared is not equal to the specified rule value.
        /// </summary>
        NotEqual,
        /// <summary>
        /// Value to be compared is less than the specified rule value.
        /// </summary>
        Lt,
        /// <summary>
        /// Value to be compared is less than or equal to the specified rule value.
        /// </summary>
        Lte,
        /// <summary>
        /// Value to be compared is greater than the specified rule value.
        /// </summary>
        Gt,
        /// <summary>
        /// Value to be compared is greater than or equal to the specified rule value.
        /// </summary>
        Gte,
        /// <summary>
        /// Value to be compared should evaluate to itself in a bitwise OR operation.
        /// Ensures that at least all value flags are set, if additional are set still evals to <c>true</c>.
        /// Value MUST be numeric and MUST be a valid bit-mask value.
        /// </summary>
        BitOr,
        /// <summary>
        /// Value to be compared should evaluate to itself in a bitwise AND operation.
        /// Ensures all and only the exact flags are set.
        /// Value MUST be numeric and MUST be a valid bit-mask value.
        /// </summary>
        BitAnd,
    }
}
