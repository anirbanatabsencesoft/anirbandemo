﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum VisibilityListType
    {
        Individual = 1,
        Team = 2,
        All = 3
    }
}
