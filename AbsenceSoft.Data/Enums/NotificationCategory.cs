﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
	public enum NotificationCategory : int
	{
		System = 0,
		ToDo = 1,
		Communication = 2

	}
}
