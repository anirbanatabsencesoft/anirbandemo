﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    [Flags]
    public enum NoteCategoryTarget
    {
        Case = 1,
        Employee = 2,
        Communication = 4
    }
}
