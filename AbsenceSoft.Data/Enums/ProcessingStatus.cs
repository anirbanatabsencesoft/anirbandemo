﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum ProcessingStatus : int
    {
        Pending = 0,
        Processing = 1,
        Complete = 2,
        Canceled = 3,
        Failed = -1
    }
}
