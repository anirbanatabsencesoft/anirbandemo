﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum PayrollStatus : int
    {
        Pending = 0,
        SentToException = 1,
        SentToPayroll = 2
    }
}
