﻿namespace AbsenceSoft.Data.Enums
{
    public enum CaseAssignmentRuleBehavior : int
    {
        /// <summary>
        /// The passive behavior is the default behavior for a
        /// case assignment rule and directs the rule to apply the assignment
        /// if the rule passes, but if the rule doesn't pass, don't create the
        /// assignment and pass the full list to the next rule. This is useful
        /// for things like alpha-split, etc.
        /// </summary>
        Passive = 0,
        /// <summary>
        /// The filter behavior specifies that the rule filters against
        /// the users based on the match, if it passes, but if it doesn't
        /// pass then it will clear the list of users alltogether (same
        /// as unassigned).
        /// </summary>
        Filter = 1
    }
}
