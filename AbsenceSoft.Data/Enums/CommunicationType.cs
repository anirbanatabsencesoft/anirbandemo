﻿namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// These are the communication types used in the application"
    /// </summary>
    /// 
    public enum CommunicationType : int
    {
        Mail = 0,
        Email = 1,
        //Fax = 2,
        //Phone = 3,
        //Online = 4,
        //TextMsg = 5
    }
}
