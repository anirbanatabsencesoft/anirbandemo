﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
	public enum NotificationDeliveryMethod: int
	{
		Email = 0,
		Text = 1
	}
}
