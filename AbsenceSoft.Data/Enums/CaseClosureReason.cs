﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// These are the available case closure Reasons.
    /// </summary>
    public enum CaseClosureReason : int
    {
        ReturnToWork = 0,
        Terminated = 1,
        LeaveCancelled = 2,
        Other = 3
    }
}
