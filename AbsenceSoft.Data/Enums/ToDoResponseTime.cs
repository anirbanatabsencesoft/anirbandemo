﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum ToDoResponseTime:int
    {
        Hours,
        WorkingDays,
        CalendarDays,
        Months
    }
}
