﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// Type of todo items that may be created by todo.
    /// </summary>
    public enum ToDoItemType : int
    {
        /*
         * *********************************************************************
         * 
         * Next Sequence #: 30
         * 
         * *********************************************************************
         * Update the above any time you add new ToDoItemType values, so we just
         * know and it's easier to resolve conflicts in this area and avoid
         * numbering collisions.
         * *********************************************************************
         */

        NotSpecified = -1,
        Communication = 0,
        PaperworkDue = 1,
        PaperworkReview = 2,
        CaseReview = 3,
        ReturnToWork = 4,
        Manual = 5,
        CloseCase = 6,
        VerifyReturnToWork = 7,
        Question = 29,

        //Accommodations
        [FeatureRestriction(Feature.ADA)]
        ScheduleErgonomicAssessment = 8,
		[FeatureRestriction(Feature.ADA)]
        CompleteErgonomicAssessment = 9,
		[FeatureRestriction(Feature.ADA)]
        EquipmentSoftwareOrdered = 10,
		[FeatureRestriction(Feature.ADA)]
        EquipmentSoftwareInstalled = 11,
		[FeatureRestriction(Feature.ADA)]
        HRISScheduleChange = 12,
		[FeatureRestriction(Feature.ADA)]
        OtherAccommodation = 13,
		[FeatureRestriction(Feature.ADA)]
        NotifyManager = 25,
		[FeatureRestriction(Feature.ADA)]
        InitiateNewLeaveCase = 26,

        //STD
		[FeatureRestriction(Feature.ShortTermDisability)]
        STDCaseManagerContactsEE = 14,
		[FeatureRestriction(Feature.ShortTermDisability)]
        STDContactHCP = 15,
		[FeatureRestriction(Feature.ShortTermDisability)]
        STDDiagnosis = 16,
		[FeatureRestriction(Feature.ShortTermDisability)]
        STDDesignatePrimaryHCP = 17,
		[FeatureRestriction(Feature.ShortTermDisability)]
        STDGenHealthCond = 18,
		[FeatureRestriction(Feature.ShortTermDisability)]
        STDFollowUp = 19,

        //Self Service
		[FeatureRestriction(Feature.EmployeeSelfService)]
        ReviewCaseAttachment = 20,
		[FeatureRestriction(Feature.EmployeeSelfService)]
        ReviewCaseNote = 21,
		[FeatureRestriction(Feature.EmployeeSelfService)]
        ReviewEmployeeInformation = 22,
		[FeatureRestriction(Feature.EmployeeSelfService)]
        ReviewCaseCreated = 23,
		[FeatureRestriction(Feature.EmployeeSelfService)]
        ReviewWorkSchedule = 24,

        //Work Restrictions / JSA
        EnterWorkRestrictions = 27,

        DetermineRequest = 28
    }

	public static class ToDoItemTypeExtensions
	{
		public static Feature GetFeatureRestriction(this ToDoItemType p)
		{
			var attr = p.GetAttribute<FeatureRestrictionAttribute>();

			if (attr != null)
				return attr.Feature;
			else
				return Feature.None;
		}

		public static IDictionary<ToDoItemType, Feature> GetToDoItemListWithFeatureRestriction()
		{
			IDictionary<ToDoItemType, Feature> list = new Dictionary<ToDoItemType, Feature>();

			var enumType = typeof(ToDoItemType);
			var values = Enum.GetValues(enumType).Cast<ToDoItemType>();
            var memberInfos = enumType.GetMembers();
			foreach (var v in values)
			{
				list.Add(v, v.GetFeatureRestriction());
			}

			return list;
		}
	}

	public static class EnumExtensions
	{
		public static TAttribute GetAttribute<TAttribute>(this Enum value)
			where TAttribute : Attribute
		{
			var type = value.GetType();
			var name = Enum.GetName(type, value);
			return type.GetField(name) // I prefer to get attributes this way
				.GetCustomAttributes(false)
				.OfType<TAttribute>()
				.SingleOrDefault();
		}
	}
}
