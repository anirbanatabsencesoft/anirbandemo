﻿namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// Represents the Social Security Administration's defintions of the 5 job classifications and related restrictions
    /// when relating to social security claims, but are also used for worker's comp and other benefits and legal definitions
    /// around disability.
    /// </summary>
    public enum JobClassification : int
    {
        /// <summary>
        /// Sedentary work is limited to:
        ///  - lifting no more that 10 pounds at one time, or
        ///  - occasionally carrying small items such as files or small tools, for no longer than a total of 2 hours in any workday, and
        ///  - sitting for approximately 6 hours in an 8-hour workday, and
        ///  - standing for a total of 2 hours in an 8-hour work day is often necessary
        ///  - some walking, and
        ///  - some standing may be required.
        /// </summary>
        Sedentary = 1,
        /// <summary>
        /// Light work involves:
        ///  - lifting no more than 20 pounds at a time, and  
        ///  - frequent lifting or carrying of objects weighing up to 10 pounds, and
        ///  - may require a good deal of walking or standing, and/or
        ///  - may require sitting most of the time with some pushing and pulling of arm or leg controls.
        /// </summary>
        Light = 2,
        /// <summary>
        /// Medium work involves:
        ///  - lifting no more than 50 pounds at a time, and
        ///  - frequent lifting or carrying of objects weighing up to 25 pounds.
        /// </summary>
        Medium = 3,
        /// <summary>
        /// Heavy work involves:
        ///  - lifting no more than 100 pounds at a time, and
        ///  - frequent lifting or carrying of objects weighing up to 50 pounds. 
        /// </summary>
        Heavy = 4,
        /// <summary>
        /// Definition for very heavy work:
        ///  - lifting objects weighing more than 100 pounds at a time, and
        ///  - frequent lifting or carrying of objects weighing 50 pounds or more.
        /// </summary>
        VeryHeavy = 5
    }
}
