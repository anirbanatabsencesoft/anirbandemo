﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum CaseEventDateType : int
    {
        StartDate = 0,
        EndDate = 1,
        Period = 4
    }
}
