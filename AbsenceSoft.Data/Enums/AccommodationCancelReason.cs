﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// This is status of the current cases.
    /// </summary>
    public enum AccommodationCancelReason : int
    {
        Other = 0,
        AccommodationNoLongerNeeded = 1,
        LeaveNoLongerNeeded  = 2,
        RequestWithdrawn = 3,
        EnteredForWrongEmployee = 4,
        EnteredInError = 5
    }
}
