﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum CustomFieldUpdateBehavior : int
    {
        Passive = 0,
        Force = 1
    }
}
