﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Enum, AllowMultiple = false, Inherited = false)]
    public class MaxOccurrencesAttribute : Attribute
    {
        public MaxOccurrencesAttribute()
        {
            MaxOccurrences = -1;
        }
        public MaxOccurrencesAttribute(int max)
        {
            MaxOccurrences = max;
        }

        public int MaxOccurrences { get; private set; }

        public bool Unlimited { get { return MaxOccurrences < 0; } }
    }
}
