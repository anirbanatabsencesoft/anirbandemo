﻿using System;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// Represents a sort direction for use by anything that sorts
    /// </summary>
    [Serializable]
    public enum SortDirection : int
    {
        /// <summary>
        /// No sort is applied or no sort direction is specified (natural order).
        /// </summary>
        None = 0,
        /// <summary>
        /// The ascending sort direction.
        /// </summary>
        Ascending = 1,
        /// <summary>
        /// The descending sort direction.
        /// </summary>
        Descending = -1
    }
}
