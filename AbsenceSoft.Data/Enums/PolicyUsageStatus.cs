﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// This will show the status of any policy usage (typically follows adjudication status of a case). If the segment type
    /// is intermittent, this represents the status of any intermittent requests for time off.
    /// </summary>
    public enum PolicyUsageStatus : int
    {
        /// <summary>
        /// The status of this policy usage is the same as the applied policy's status. This is typical for all absence
        /// types other than intermittent.
        /// </summary>
        SameAsAppliedPolicy = 0,
        /// <summary>
        /// Shows the intermittent time off request as pending [counts towards usage].
        /// </summary>
        Pending = 1,
        /// <summary>
        /// Shows the intermittent time off request as approved [counts towards usage].
        /// </summary>
        Approved = 2,
        /// <summary>
        /// Shows the intermittent time off request as denied [does NOT count towards usage].
        /// </summary>
        Denied = 3
    }
}
