﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    [Serializable]
    public enum WorkRelatedSharpsWhen : int
    {
        /// <summary>
        /// The dont know when the injury occurred in relation to the protective mechanism activation.
        /// </summary>
        DontKnow = 0,
        /// <summary>
        /// The before activation when the injury occurred in relation to the protective mechanism activation.
        /// </summary>
        BeforeActivation = 1,
        /// <summary>
        /// The during activation when the injury occurred in relation to the protective mechanism activation.
        /// </summary>
        DuringActivation = 2,
        /// <summary>
        /// The after activation when the injury occurred in relation to the protective mechanism activation.
        /// </summary>
        AfterActivation = 3
    }
}
