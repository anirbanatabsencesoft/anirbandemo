﻿using System;

namespace AbsenceSoft.Data.Enums
{
    [Serializable]
    public enum WorkRelatedSharpsProcedure : int
    {
        /// <summary>
        /// The other work related sharps procedure being performed.
        /// </summary>
        Other = 0,
        /// <summary>
        /// The draw venous blood work related sharps procedure being performed.
        /// </summary>
        DrawVenousBlood = 1,
        /// <summary>
        /// The draw arterial blood work related sharps procedure being performed.
        /// </summary>
        DrawArterialBlood = 2,
        /// <summary>
        /// The injection work related sharps procedure being performed.
        /// </summary>
        Injection = 3,
        /// <summary>
        /// The start iv or central line work related sharps procedure being performed.
        /// </summary>
        StartIVOrCentralLine = 4,
        /// <summary>
        /// The heparin or saline flush work related sharps procedure being performed.
        /// </summary>
        HeparinOrSalineFlush = 5,
        /// <summary>
        /// The obtain body fluid or tissue sample work related sharps procedure being performed.
        /// </summary>
        ObtainBodyFluidOrTissueSample = 6,
        /// <summary>
        /// The cutting work related sharps procedure being performed.
        /// </summary>
        Cutting = 7,
        /// <summary>
        /// The suturing work related sharps procedure being performed.
        /// </summary>
        Suturing = 8
    }
}
