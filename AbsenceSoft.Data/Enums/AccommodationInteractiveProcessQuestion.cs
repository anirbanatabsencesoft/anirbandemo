﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum AccommodationInteractiveProcessQuestion : int
    {
        Question1 = 0,
        Question2 = 1,
        Question3 = 2,
        Question4 = 3,
        Question5 = 4,
        Question6 = 5,
        Question7 = 6        
   }
}
