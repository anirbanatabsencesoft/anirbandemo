﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum DueDateType : int
    {
        FromNow = 0,
        CaseStart = 1,
        CaseEnd = 2,
        ActiveSegmentStart = 3,
        ActiveSegmentEnd = 4,
        CaseRTW = 5
    }
}