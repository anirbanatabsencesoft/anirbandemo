﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// These are the available cases Types per compliance.
    /// </summary>
    [Flags]
    public enum CaseType : int
    {
        None = 0x0,
        Consecutive = 0x1,
        Intermittent = 0x2,
        Reduced = 0x4,
        Administrative = 0x8
    }
}
