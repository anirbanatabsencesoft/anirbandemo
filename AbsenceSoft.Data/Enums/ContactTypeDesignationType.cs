﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    [Flags]
    public enum ContactTypeDesignationType : int
    {
        None = 0x0,
        Self = 0x1,
        Administrative = 0x2,
        Personal = 0x4,
		Medical = 0x8
    }
}
