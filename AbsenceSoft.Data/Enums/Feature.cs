﻿namespace AbsenceSoft.Data.Enums
{
    public enum Feature : int
    {
        None = 0,
        ADA = 1,
        ShortTermDisability = 2,
        MultiEmployerAccess = 3,
        GuidelinesData = 4,
        PolicyConfiguration = 5,
        CommunicationConfiguration = 6,
        EmployeeSelfService = 7,
        LOA = 8,
        ShortTermDisablityPay = 9,
        ESSDataVisibilityInPortal = 10,
        WorkflowConfiguration = 11,
        CaseReporter = 12,
        WorkRelatedReporting = 13,
        IPRestrictions = 14,
        EmployeeConsults = 15,
        InquiryCases = 16,
        OrgDataVisibility = 17,
        JSA = 18,
        CustomContent = 19,
        AdHocReporting = 20,
        EmployerContact = 21,
        EmployerServiceOptions = 22,
        OshaReporting = 23,
        RiskProfile = 24,
        JobConfiguration = 25,
        WorkRestriction = 26,
        BusinessIntelligenceReport = 27,
        ContactPreferences = 28,
        AdminPaySchedules = 29,
        AdminPhysicalDemands = 30,
        AdminCustomFields = 31,
        AdminNecessities = 32,
        AdminNoteCategories = 33,
        AdminConsultations = 34,
        AdminAssignTeams = 35,
        AdminOrganizations = 36,
        AdminDataUpload = 37,
        AdminEmployers = 38,
        AdminSecuritySettings = 39,
        AccommodationTypeCategories = 40,
        AdjustEmployeeStartDayOfWeek = 41,
        SingleSignOn = 42,
        PolicyCrosswalk = 43,
        CaseBulkDownload = 44,
        DashboardChart = 45,
        ReportingTwoBeta = 46,
        CaseClosureCategories = 47,
        ESSWorkSchedule = 48,
        FlightCrew = 49,
        AdminDenialReason = 50,
        ESSWorkRelatedReporting = 53
        // Hi there! Are you adding more featuers here? If so, you had better
        // also go to AbsenceSoft.Logic.Customers.CustomerService.CreateCustomer(string,string,string,string,Address,User)
        // and in that method add the default for this there, otherwise you'll be in big trouble and will
        // be sent to bed without any desert.
        // Don't forget to add the default to AbsenceSoft.Setup.Features.FeaturesData.SetupData() as well
        // This will add the default to any existing customers
    }
}
