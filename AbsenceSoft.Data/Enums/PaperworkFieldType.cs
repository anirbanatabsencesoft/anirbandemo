﻿using System;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// Represents the different types of paperwork fields that can be configured for a paperwork.
    /// </summary>
    /// <remarks>
    /// The commented out values here, 1, 2, 3, 5, 6 and 7 represent FUTURE status values reserved here
    /// for future use simply because we don't have time/capacity to get them done this go-round.
    /// </remarks>
    [Serializable]
    public enum PaperworkFieldType : int
    {
        Text = 0,
#warning TODO: Need to complete out the remaining Paperwork Field Types once each respective section of UI has been refactored
        //HealthCareProvider = 1,
        //IntermittentCertification = 2,
        //AccommodationRequest = 3,
        WorkRestrictions = 4,
        //DiagnosisCodes = 5,
        //Note = 6,
        //ExpectedRtwDate = 7
    }
}
