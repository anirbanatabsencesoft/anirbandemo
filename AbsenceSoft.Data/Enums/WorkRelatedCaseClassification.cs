﻿using System;

namespace AbsenceSoft.Data.Enums
{
    [Serializable]
    public enum WorkRelatedCaseClassification : int
    {
        /// <summary>
        /// The other recordable cases classiciation for OSHA/WC work related cases.
        /// </summary>
        Other = 0,
        /// <summary>
        /// The death case classiciation for OSHA/WC work related cases.
        /// </summary>
        Death = 1,
        /// <summary>
        /// The days away from work case classiciation for OSHA/WC work related cases.
        /// </summary>
        DaysAwayFromWork = 2,
        /// <summary>
        /// The job transfer or restriction case classiciation for OSHA/WC work related cases.
        /// </summary>
        JobTransferOrRestriction = 3
    }
}
