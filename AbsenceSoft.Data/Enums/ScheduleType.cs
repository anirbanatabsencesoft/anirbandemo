﻿
using System.ComponentModel;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// Employees Schedule Types
    /// </summary>
    public enum ScheduleType : int
    {
        /// <summary>
        /// Represents a weekly schedule with a steady total number of hours per week spread across
        /// regular work days, M-F, S-S, etc.
        /// </summary>
        Weekly = 0,
        /// <summary>
        /// Represents a rotating schedule where a certain number of days are worked following another number
        /// of days not worked in a repeatable pattern between 2 dates. Usual for dock workers, oil rig
        /// workers, shift workers, resteraunt staff, nurses, etc.
        /// </summary>
        Rotating = 1,
        /// <summary>
        /// Represents a variable schedule where specific dates are used to represent an
        /// actual or real work schedule where no hours are known up-front and time is entered in as or after
        /// it has been worked or scheduled.
        /// </summary>
        Variable = 2,

        /// <summary>
        /// Represents a variable schedule where average hours per week is used to represent the work schedule
        /// there is no need to specify actual days of the week the employee is scheduled but rather the total sum of time.  
        /// </summary>
        [Description("FTE Variable")]
        FteVariable = 3
    }    
}
