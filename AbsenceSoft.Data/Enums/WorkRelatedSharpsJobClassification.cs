﻿using System;

namespace AbsenceSoft.Data.Enums
{
    [Serializable]
    public enum WorkRelatedSharpsJobClassification : int
    {
        /// <summary>
        /// The other work related sharps job classification.
        /// </summary>
        Other = 0,
        /// <summary>
        /// The doctor work related sharps job classification.
        /// </summary>
        Doctor = 1,
        /// <summary>
        /// The nurse work related sharps job classification.
        /// </summary>
        Nurse = 2,
        /// <summary>
        /// The intern or resident work related sharps job classification.
        /// </summary>
        InternOrResident = 3,
        /// <summary>
        /// The patient care support staff work related sharps job classification.
        /// </summary>
        PatientCareSupportStaff = 4,
        /// <summary>
        /// The technologist OR work related sharps job classification.
        /// </summary>
        TechnologistOR = 5,
        /// <summary>
        /// The technologist RT work related sharps job classification.
        /// </summary>
        TechnologistRT = 6,
        /// <summary>
        /// The technologist RAD work related sharps job classification.
        /// </summary>
        TechnologistRAD = 7,
        /// <summary>
        /// The phlebotomist or lab tech work related sharps job classification.
        /// </summary>
        PhlebotomistOrLabTech = 8,
        /// <summary>
        /// The housekeeper or laundry worker work related sharps job classification.
        /// </summary>
        HousekeeperOrLaundryWorker = 9,
        /// <summary>
        /// The trainee work related sharps job classification.
        /// </summary>
        Trainee = 10
    }
}
