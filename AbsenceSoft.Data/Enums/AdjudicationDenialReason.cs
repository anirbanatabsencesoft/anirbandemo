﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public static class AdjudicationDenialReason
    {
        public const string Other = "OTHER";
        public const string OtherDescription = "Other";
        public const string Exhausted = "EXHAUSTED";
        public const string ExhaustedDescription = "Exhausted";
        public const string NotAValidHealthcareProvider = "NOTAVALIDHEALTHCAREPROVIDER";
        public const string NotAValidHealthcareProviderDescription = "Not A Valid Healthcare Provider";
        public const string NotASeriousHealthCondition = "NOTASERIOUSHEALTHCONDITION";
        public const string NotASeriousHealthConditionDescription = "Not A Serious Health Condition";
        public const string EliminationPeriod = "ELIMINATIONPERIOD";
        public const string EliminationPeriodDescription = "Elimination Period";
        public const string PerUseCap = "PERUSECAP";
        public const string PerUseCapDescription = "Per Use Cap";
        public const string IntermittentNonAllowed = "INTERMITTENTNONALLOWED";
        public const string IntermittentNonAllowedDescription = "Intermittent Non Allowed";
        public const string PaperworkNotReceived = "PAPERWORKNOTRECEIVED";
        public const string PaperworkNotReceivedDescription = "Paperwork Not Received";
        public const string NotAnEligibleFamilyMember = "NOTANELIGIBLEFAMILYMEMBER";
        public const string NotAnEligibleFamilyMemberDescription = "Not An Eligible Family Member";
        public const string MaxOccurrenceReached = "MAXOCCURRENCEREACHED";
        public const string MaxOccurrenceReachedDescription = "Max Occurrence Reached";
        public const string NotApplicable = "NOTAPPLICABLE";
        public const string NotApplicableDescription = "Not Applicable";
    }
}
