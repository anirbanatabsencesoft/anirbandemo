﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// Type of Policy rule group
    /// </summary>
    public enum PolicyRuleGroupType : int
    {
        Selection = 0,
        Eligibility = 1,
        Time = 2,
        Payment = 3,
        Crosswalk = 4
    }
}
