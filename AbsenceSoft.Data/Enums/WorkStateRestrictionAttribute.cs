﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// Used for restricting enum values based on certain employee work states that they are applicable too.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public sealed class WorkStateRestrictionAttribute : Attribute
    {
        private string[] _states = null;

        public WorkStateRestrictionAttribute() { }
        public WorkStateRestrictionAttribute(params string[] states)
        {
            _states = states;
        }

        /// <summary>
        /// Gets the list of states that this enumerated value is restricted to.
        /// </summary>
        public string[] States { get { return _states; } }
    }
}
