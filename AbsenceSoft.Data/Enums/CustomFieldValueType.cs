﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// These are the available custom field types.
    /// </summary>
    public enum CustomFieldValueType : int
    {
        UserEntered = 0x1,
        SelectList = 0x2,
    }
}
