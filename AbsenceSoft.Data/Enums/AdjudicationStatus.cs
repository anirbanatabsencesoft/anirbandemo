﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    [Serializable]
    /// <summary>
    /// This will show the status of the cases which are under review
    /// </summary>
    public enum AdjudicationStatus : int
    {
        Pending = 0,
        Approved = 1,
        Denied = 2
    }
}
