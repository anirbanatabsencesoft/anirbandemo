﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum DayUnitType : int
    {
        Workday = 0,
        CalendarDay = 1
    }
}
