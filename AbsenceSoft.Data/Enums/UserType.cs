﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum UserType
    {
        Portal = 1,
        SelfService = 2,
        Admin = 3
    }
}
