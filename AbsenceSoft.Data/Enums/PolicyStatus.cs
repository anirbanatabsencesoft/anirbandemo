﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// This will show the status of the policy which are combination of Eligibility and Adjudication statuses
    /// This required for Workflow Rule and conditional rule expression
    /// </summary>
    public enum PolicyStatus : int
    {
        EligibilityIneligible = -1,
        EligibilityEligible = 1,
        AdjudicationPending = 3,
        AdjudicationApproved = 4,
        AdjudicationDenied = 5,
        IsExhausted = 6
    }
}