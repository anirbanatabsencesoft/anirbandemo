﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum NotificationFrequencyType
    {
        Immediately,
        Daily,
        Weekly
    }
}
