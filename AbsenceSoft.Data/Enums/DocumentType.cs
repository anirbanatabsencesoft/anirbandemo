﻿namespace AbsenceSoft.Data.Enums
{
    public enum DocumentType
    {
        None = 0,
        Template = 1,
        Html = 2,
        MSWordDocument = 3,
        Pdf = 4
    }
}
