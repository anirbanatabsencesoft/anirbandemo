﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum PaperworkFieldStatus : int
    {
        Incomplete = 0,
        Complete = 1
    }
}
