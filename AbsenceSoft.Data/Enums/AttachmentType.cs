﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum AttachmentType : int
    {
        Communication = 1,
        Paperwork = 2,
        Documentation = 3,
        Other = 0
    }
}
