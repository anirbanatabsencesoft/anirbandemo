﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum AdjudicationSummaryStatus : int
    {
        Pending = 0,
        Approved = 1,
        Denied = 2,
        Allowed = 3,
        NotEligible = 4
    }
}
