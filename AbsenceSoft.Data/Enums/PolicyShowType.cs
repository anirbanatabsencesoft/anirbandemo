﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum PolicyShowType : int
    {
        Always = 0,
        //TimeUsed = 1,
        //OnDemand = 2,
        OnlyTimeUsed = 3
    }
}
