﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public static class AccommodationAdjudicationDenialReason
    {
        public const string Other = "OTHER";
        public const string OtherDescription = "Other";
        public const string NotAReasonableRequest = "NOTAREASONABLEREQUEST";
        public const string NotAReasonableRequestDescription = "Not A Reasonable Request";
        public const string IneligibleHealthCondition = "INELIGIBLEHEALTHCONDITION";
        public const string IneligibleHealthConditionDescription = "Ineligible Health Condition";
        public const string FalsifiedDocumentation = "FALSIFIEDDOCUMENTATION";
        public const string FalsifiedDocumentationDescription = "Falsified Documentation";
        public const string RegulatoryProhibitionAgainstRequest = "REGULATORYPROHIBITIONAGAINSTREQUEST";
        public const string RegulatoryProhibitionAgainstRequestDescription = "Regulatory Prohibition Against Request";
        public const string EssentialJobFunctionCanNotAccommodate = "ESSENTIALJOBFUNCTIONCANNOTACCOMMODATE";
        public const string EssentialJobFunctionCanNotAccommodateDescription = "Essential Job Function Can Not Accommodate";
        public const string SiteCannotAccommodate = "SITECANNOTACCOMMODATE";
        public const string SiteCannotAccommodateDescription = "Site Cannot Accommodate";
        public const string NoPositionFitRestrictions = "NOPOSITIONFITRESTRICTIONS";
        public const string NoPositionFitRestrictionsDescription = "No Position Fit Restrictions";
        public const string CannotAccommodate = "CANNOTACCOMMODATE";
        public const string CannotAccommodateDescription = "Cannot Accommodate";
        public const string NoPaperworkProvided = "NOPAPERWORKPROVIDED";
        public const string NoPaperworkProvidedDescription = "No Paperwork Provided";
        public const string EmployeeDidNotParticipate = "EMPLOYEEDIDNOTPARTICIPATE";
        public const string EmployeeDidNotParticipateDescription = "Employee Did Not Participate";
        public const string DocumentationNotProvided = "DOCUMENTATIONNOTPROVIDED";
        public const string DocumentationNotProvidedDescription = "Documentation Not Provided";
        public const string IncompleteDocumentation = "INCOMPLETEDOCUMENTATION";
        public const string IncompleteDocumentationDescription = "Incomplete Documentation";
        public const string NotAReasonableAccommodation = "NOTAREASONABLEACCOMMODATION";
        public const string NotAReasonableAccommodationDescription = "Not A Reasonable Accommodation";
        public const string EmployeeDidNotAcceptAccommodationSuggestion = "EMPLOYEEDIDNOTACCEPTACCOMMODATIONSUGGESTION";
        public const string EmployeeDidNotAcceptAccommodationSuggestionDescription = "Employee Did Not Accept Accommodation Suggestion";
    }
}
