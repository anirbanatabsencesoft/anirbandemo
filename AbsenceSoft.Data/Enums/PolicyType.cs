﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// Types of availabe policies for the company to choose from
    /// </summary>
    public enum PolicyType : int
    {
        /// <summary>
        /// The FMLA policy type, (0)
        /// </summary>
        FMLA = 0,
        /// <summary>
        /// The state FML policy type, (1)
        /// </summary>
        StateFML = 1,
        /// <summary>
        /// The standard policy type, (2)
        /// </summary>
        STD = 2,
        /// <summary>
        /// The LTD policy type, (3)
        /// </summary>
        LTD = 3,
        /// <summary>
        /// The workers comp policy type, (4)
        /// </summary>
        WorkersComp = 4,
        /// <summary>
        /// The company policy type, (5)
        /// </summary>
        Company = 5,
        /// <summary>
        /// The other policy type, (6)
        /// </summary>
        Other = 6,
        /// <summary>
        /// The state disability policy type, (7)
        /// </summary>
        StateDisability = 7
    }
}
