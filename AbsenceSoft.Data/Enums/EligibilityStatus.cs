﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// These are the eligibility statuses for an employee
    /// </summary>
    public enum EligibilityStatus
    {
        Pending = 0,
        Ineligible = -1,
        Eligible = 1,
        Approved = 2
    }
}
