﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// how is a policies elimination period used
    /// </summary>
    public enum PolicyEliminationType
    {
        /// <summary>
        /// if the policy elimination period is met, the policy pays out the time during the period: example: FMLA
        /// </summary>
        Inclusive,

        /// <summary>
        /// the policy does not begin to pay out until the elimnation period is met: example: STD
        /// </summary>
        Exclusive
    }
}
