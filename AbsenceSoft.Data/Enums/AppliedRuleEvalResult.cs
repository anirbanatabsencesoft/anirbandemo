﻿namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// Represents the evaluation result of an applied policy rule.
    /// </summary>
    public enum AppliedRuleEvalResult : int
    {
        Unknown = 0,
        Pass = 1,
        Fail = -1,
    }
}
