﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// Case Assignment Types
    /// </summary>
    [Serializable]
    public enum CaseAssigmentType
    {
        Team = 1,
        TeamMember = 2
    }
}
