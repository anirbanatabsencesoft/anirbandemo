﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// In what order the tasks need to be handled by super users (users managing employees/cases)
    /// </summary>
    public enum ToDoItemPriority : int
    {
        Low = -1,
        Normal = 0,
        High = 1,
        Urgent = 2,
        Critical = 3
    }
}
