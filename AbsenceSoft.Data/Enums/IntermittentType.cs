﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.Data.Enums
{
    [Serializable]
    public enum IntermittentType
    {
        [Display(Name="Office Visit")]
        OfficeVisit = 0,
        [Display(Name="Incapacity")]
        Incapacity = 1
    }

}
