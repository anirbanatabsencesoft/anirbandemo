﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum DbDataType : int
    {
        /// <summary>
        /// Exact numeric value with precision 19 (if signed) or 20 (if unsigned) and
        /// scale 0 (signed: –2[63] &lt;= n &lt;= 2[63] – 1, unsigned:0 &lt;= n <= 2[64] – 1)
        /// (SQL_BIGINT). This maps to System.Int64.
        /// </summary>
        BigInt = 1,
        /// <summary>
        /// A stream of binary data (SQL_BINARY). This maps to an System.Array of type
        /// System.Byte.
        /// </summary>
        Binary = 2,
        /// <summary>
        /// Single bit binary data (SQL_BIT). This maps to System.Boolean.
        /// </summary>
        Bit = 3,
        /// <summary>
        /// A fixed-length character string (SQL_CHAR). This maps to System.String.
        /// </summary>
        Char = 4,
        /// <summary>
        /// Date data in the format yyyymmddhhmmss (SQL_TYPE_TIMESTAMP). This maps to
        /// System.DateTime.
        /// </summary>
        DateTime = 5,
        /// <summary>
        /// Signed, exact, numeric value with a precision of at least p and scale s,
        /// where 1 &lt;= p &lt;= 15 and s &lt;= p. The maximum precision is driver-specific (SQL_DECIMAL).
        /// This maps to System.Decimal.
        /// </summary>
        Decimal = 6,
        /// <summary>
        /// Signed, exact, numeric value with a precision p and scale s, where 1 &lt;= p
        /// &lt;= 15, and s &lt;= p (SQL_NUMERIC). This maps to System.Decimal.
        /// </summary>
        Numeric = 7,
        /// <summary>
        /// Signed, approximate, numeric value with a binary precision 53 (zero or absolute
        /// value 10[–308] to 10[308]) (SQL_DOUBLE). This maps to System.Double.
        /// </summary>
        Double = 8,
        /// <summary>
        /// Variable length binary data. Maximum length is data source–dependent (SQL_LONGVARBINARY).
        /// This maps to an System.Array of type System.Byte.
        /// </summary>
        Image = 9,
        /// <summary>
        /// Exact numeric value with precision 10 and scale 0 (signed: –2[31] &lt;= n &lt;=
        /// 2[31] – 1, unsigned:0 &lt;= n &lt;= 2[32] – 1) (SQL_INTEGER). This maps to System.Int32.
        /// </summary>
        Int = 10,
        /// <summary>
        /// Unicode character string of fixed string length (SQL_WCHAR). This maps to
        /// System.String.
        /// </summary>
        NChar = 11,
        /// <summary>
        /// Unicode variable-length character data. Maximum length is data source–dependent.
        /// (SQL_WLONGVARCHAR). This maps to System.String.
        /// </summary>
        NText = 12,
        /// <summary>
        /// A variable-length stream of Unicode characters (SQL_WVARCHAR). This maps
        /// to System.String.
        /// </summary>
        NVarChar = 13,
        /// <summary>
        /// Signed, approximate, numeric value with a binary precision 24 (zero or absolute
        /// value 10[–38] to 10[38]).(SQL_REAL). This maps to System.Single.
        /// </summary>
        Real = 14,
        /// <summary>
        /// A fixed-length GUID (SQL_GUID). This maps to System.Guid.
        /// </summary>
        UniqueIdentifier = 15,
        /// <summary>
        /// Data and time data in the format yyyymmddhhmmss (SQL_TYPE_TIMESTAMP). This
        /// maps to System.DateTime.
        /// </summary>
        SmallDateTime = 16,
        /// <summary>
        /// Exact numeric value with precision 5 and scale 0 (signed: –32,768 &lt;= n &lt;=
        /// 32,767, unsigned: 0 &lt;= n &lt;= 65,535) (SQL_SMALLINT). This maps to System.Int16.
        /// </summary>
        SmallInt = 17,
        /// <summary>
        /// Variable length character data. Maximum length is data source–dependent (SQL_LONGVARCHAR).
        /// This maps to System.String.
        /// </summary>
        Text = 18,
        /// <summary>
        /// A stream of binary data (SQL_BINARY). This maps to an System.Array of type
        /// System.Byte.
        /// </summary>
        Timestamp = 19,
        /// <summary>
        /// Exact numeric value with precision 3 and scale 0 (signed: –128 &lt;= n &gt;= 127,
        /// unsigned:0 &lt;= n &gt;= 255)(SQL_TINYINT). This maps to System.Byte.
        /// </summary>
        TinyInt = 20,
        /// <summary>
        /// Variable length binary. The maximum is set by the user (SQL_VARBINARY). This
        /// maps to an System.Array of type System.Byte.
        /// </summary>
        VarBinary = 21,
        /// <summary>
        /// A variable-length stream character string (SQL_CHAR). This maps to System.String.
        /// </summary>
        VarChar = 22,
        /// <summary>
        /// Date data in the format yyyymmdd (SQL_TYPE_DATE). This maps to System.DateTime.
        /// </summary>
        Date = 23,
        /// <summary>
        /// Date data in the format hhmmss (SQL_TYPE_TIMES). This maps to System.DateTime.
        /// </summary>
        Time = 24,
    }
}
