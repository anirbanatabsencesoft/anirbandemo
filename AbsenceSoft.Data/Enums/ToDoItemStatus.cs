﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// Case status for superusers (users managing employees/cases)
    /// </summary>
    public enum ToDoItemStatus : int
    {
        Pending = -1,
        Complete = 1,
        Overdue = 2,
        Cancelled = 3,
        Open = 4    // Combines Pending + Overdue
    }
}
