﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
	[Flags]
	public enum AbsenceReasonFlag : int
	{
        None = 0x0,
		RequiresRelationship = 0x1,
		HideInEmployeeSelfServiceSelection = 0x2,
        ShowAccommodation = 0x4,
        RequiresPregnancyDetails = 0x8
    }
}
