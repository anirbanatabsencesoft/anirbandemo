﻿using System;
namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// The type of period for entitlement windows used for calculations, usage and eligibility.
    /// </summary>
    public enum PeriodType
    {
        /// <summary>
        /// This is used when prior usage doesn't matter, the leave is calculated based on full entitlement without
        /// considering prior usage from other cases; it's on a case by case basis so entitlement resets for each case.
        /// </summary>
        PerCase = 0,
        /// <summary>
        /// Employers may choose to base calculation of FMLA hours used on the calendar year.
        /// Effectively, any employee who has an approved FMLA leave will have a running total
        /// going toward his maximum entitlement through the end of the calendar year in which
        /// his leave began. At the beginning of the new year, all FMLA time used in the previous
        /// calendar year will drop out of the employee's total and become available for use
        /// toward the maximum FMLA leave entitlement.
        /// </summary>
        /// <example>
        /// Sara is approved for a 12-week FMLA medical leave in July 2010.
        /// As of October 2010, Sara has used her full 12-week entitlement under the FMLA.
        /// Under the calendar-year tracking method, she would not be eligible for another FMLA
        /// leave until her entitlement reset on January 1, 2011
        /// </example>
        CalendarYear = 1,
        /// <summary>
        /// This option allows the employer to appoint a fixed 12-month period in which an
        /// employee's FMLA time used goes toward her maximum entitlement. Similar to the
        /// calendar-year option, the employee's FMLA entitlement will "reset" after the specified
        /// period using this method. Uses the employee's anniversary date as their set period based
        /// on thier service date.
        /// </summary>
        /// <example>
        /// Doug is approved for a 12- week FMLA for adoption in March 2010. His employer uses a fixed
        /// 12-month period based on each employee's anniversary date. Doug was hired in April of 2008;
        /// when he starts his leave of absence in March, he uses eight weeks of FMLA entitlement through
        /// his anniversary date in April, at which time the fixed period ends. This resets his FMLA maximum
        /// entitlement to the full 12 weeks in the new fixed period of April 2010 to April 2011.
        /// </example>
        FixedYearFromServiceDate = 2,
        /// <summary>
        /// This option allows the employer to appoint a fixed 12-month period in which an
        /// employee's FMLA time used goes toward her maximum entitlement. Similar to the
        /// calendar-year option, the employee's FMLA entitlement will "reset" after the specified
        /// period using this method. Uses the employer's fiscal year as their set period.
        /// </summary>
        FixedResetPeriod = 3,
        /// <summary>
        /// This method of tracking starts the tally as of the first date that FMLA leave is taken and 
        /// stretches forward 12 months from that date. Therefore, starting on the first day an employee 
        /// takes FMLA leave, she will have all FMLA hours used added to the same total until the same day 
        /// in the following calendar year. When that period expires, the 12-month period would reset as of 
        /// the first day an employee took FMLA leave outside of the previously designated 12-month period.
        /// </summary>
        /// <example>
        /// Anna is approved for FMLA to care for her elderly mother. She takes her first day of leave on 
        /// October 1, 2010. All FMLA time she uses from this date through September 30, 2011 will count toward 
        /// the same maximum total. If Anna reaches her maximum at any time before September 30, 2011, no 
        /// additional FMLA time can become available until on or after October 1, 2011.
        /// </example>
        //[Obsolete("Are you crazy?!? This is the absolutely last calc type to get implemented", false)] -- guess I am crazy!
        RollingForward = 4,
        /// <summary>
        /// This method uses a "rolling" calendar that measures backward 12 months from any given date for which 
        /// an employee uses FMLA leave time. Effectively, an employee's entitlement is not defined by any fixed 
        /// 12-month period, but rather a varying window of time based on the 12 months immediately preceding the 
        /// current date on which FMLA leave is taken. Using this method, FMLA time used can continuously 
        /// "roll off" an employee's total as hours fall outside of the current 12-month period.
        /// </summary>
        /// <example>
        /// Roger is approved for a maximum 12 months of Intermittent FMLA leave for a medical condition. He takes 
        /// one day a week to receive treatments for his condition, or a total of eight hours. Each time Roger 
        /// schedules a day off for FMLA leave time, his leave administrator determines whether or not he has 
        /// available FMLA time by calculating the time he used in the 12 months before the specific date of his 
        /// request. If the maximum is reached at any time, Roger will have to wait for hours to "roll" back into 
        /// his balance by falling outside of the 12-month period immediately preceding his next FMLA request.
        /// </example>
        RollingBack = 5,
        /// <summary>
        /// Period timeframe applies in a look-back scenario from all time (window timeframe is ignored).
        /// </summary>
        Lifetime = 6,
        /// <summary>
        /// Used in STD policies. Per occurance includes all the linked cases in the "chain" when calculating usage
        /// </summary>
        PerOccurrence = 7,
        /// <summary>
        /// This option specifies a fixed 12-month period that resets on July 1 of each year and is used
        /// as a general fixed reset period method for tracking the "school year", generally used by
        /// school activity and involvement leaves, like Nevada's.
        /// </summary>
        /// <example>
        /// Susan's child is 12 years old and attends a year-round school in Las Vegas, NV.
        /// Susan requests and is approved for 2 hour school activities leave on May 4, 2015. 
        /// Susan requests another 4 hour leave for the same reason on June 30, but only gets 2 hours approved, 
        /// the other 2 are denied due to exhaustion. 
        /// Susan then requests a leave on July 2, 2015 foor 4 hours which is approved
        /// because it is after the reset period for the school year of July 1.
        /// </example>
        SchoolYear = 8,
        /// <summary>
        /// Same as the School Year type, only prior usage within the period is only counted for the
        /// same contact type.
        /// </summary>
        SchoolYearPerContact = 9
    }
}
