﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    [Flags]
    public enum EntityTarget : int
    {
        None = 0,
        Employee = 1,
        Case = 2
    }
}
