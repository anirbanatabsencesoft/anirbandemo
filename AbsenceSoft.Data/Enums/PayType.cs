﻿
namespace AbsenceSoft.Data.Enums
{
    public enum PayType : int
    {
        Salary = 1,
        Hourly = 2
    }
}
