﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// This is ESS Report type to be categorized
    /// </summary>
    public enum ESSReportType : int
    {
        TeamReport,
        OrganizationReport
    }
}
