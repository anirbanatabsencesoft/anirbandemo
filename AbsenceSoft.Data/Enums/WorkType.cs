﻿
namespace AbsenceSoft.Data.Enums
{
    public static class WorkType
    {
        public const string FullTime = "FULLTIME";
        public const string PartTime = "PARTTIME";
        public const string PerDiem = "PERDIEM";
    }
}
