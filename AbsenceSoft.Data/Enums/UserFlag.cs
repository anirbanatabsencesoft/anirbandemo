﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    [Flags]
    public enum UserFlag : int
    {
        None = 0,
        ChangePassword = 1,
        CreateCustomer = 2,
        UpdateEmployer = 4,
        ProductTour = 8,
        FirstEmployee = 16,
        CompleteESSRegistration = 32,
        CompleteESSAssociation = 64
    }
}
