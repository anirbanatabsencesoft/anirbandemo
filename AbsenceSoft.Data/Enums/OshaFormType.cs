﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum OshaFormType : int
    {
        [Description("OSHA 301")]
        Osha301 = 1
    }
}
