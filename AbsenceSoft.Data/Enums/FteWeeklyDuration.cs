﻿using System.ComponentModel;

namespace AbsenceSoft.Data.Enums
{
    public enum FteWeeklyDuration : int
    {
        [Description("FTE Time Per Week")]
        FteTimePerWeek = 0,
        [Description("FTE Percentage")]
        FtePercentage = 1
    }
}
