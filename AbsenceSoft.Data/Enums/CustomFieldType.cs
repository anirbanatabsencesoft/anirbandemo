﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// These are the available custom field types.
    /// </summary>
    public enum CustomFieldType : int
    {
        Text = 0x1,
        Number = 0x2,       
        Flag = 0x4,
        Date = 0x8
    }
}
