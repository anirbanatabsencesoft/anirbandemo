﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
	public enum RoleType: int
	{
		Portal = 0,
		SelfService = 1,
        Administration = 2
	}
}
