﻿using System;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// Case Assignment Rule Logic Class Name
    /// </summary>
    [Serializable]
    public enum CaseAssignmentRuleType
    {
        EmployerRule = 1,
        WorkStateRule = 2,
        CaseTypeRule = 3,
        PolicyRule = 4,
        PolicyTypeRule = 5,
        AlphaSplitRule = 6,
        RotaryRule = 7,
        SelfRule = 8,
        UnassignedRule = 9,
        PolicyCrosswalkRule = 10,
        OfficeLocationRule = 11,
        AccommodationTypeRule = 12
    }
}
