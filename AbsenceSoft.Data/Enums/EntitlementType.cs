﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum EntitlementType : int
    {
        /// <summary>
        /// Entitlement and usage is measured in work weeks. When hours are taken it is taken as a % of a total work week.
        /// </summary>
        WorkWeeks = 1,
        /// <summary>
        /// Entitlement and usage is measured using working hours. These are the easiest and simply measure total hours entitled
        /// that the employee may take against thier normal working hours defined by their work schedule.
        /// </summary>
        WorkingHours = 2,
        /// <summary>
        /// Entitlement and usage are measured using calendar weeks, Sun-Sat. Partial weeks always being on Sunday.
        /// </summary>
        CalendarWeeks = 3,
        /// <summary>
        /// Entitlement and usage are measured using calendar days and average to 365.255 days per year. If time is taken on any
        /// actual date, that entire day counts against the usage and entitlement for that leave of absence, or in some cases
        /// usage is done whether or not the employee works or not (for special or informational coverages).
        /// </summary>
        CalendarDays = 4,
        /// <summary>
        /// Entitlement and usage are measured using calendar months, which limits usage to actual months, e.g. Feb on a leap year
        /// grants more entitled time than Feb on a non-leap-year and Feb in general grants always less time than October/Dec, etc.
        /// </summary>
        CalendarMonths = 5,
        /// <summary>
        /// Entitlement and usage are measured using caledar years, which limits usage to actual years as represented on the Gregorian
        /// calendar from Jan 1 - Dec 31. On leap years this method grants 1 additional day than on other years.
        /// </summary>
        CalendarYears = 6,

        /// <summary>
        /// Time measured in employee working months (not necessary based on the calendar)
        /// </summary>
        WorkingMonths = 7,
        /// <summary>
        /// Represents a single work day of entitlement typically used for parental, school activities or other small necessity leaves.
        /// </summary>
        WorkDays = 8,
        /// <summary>
        /// Basically means unlimited, it's up to the employer on a case-by-case basis as to how much is reasonable. This lets
        /// us know that we should probably do some fancy todo, a TODO item, put a note on the case, etc. but all it's really doing
        /// right now is telling us to ignore entitlement all-together.
        /// </summary>
        ReasonablePeriod = 9,
        /// <summary>
        /// Time measured as fixed work days per work week. Generally used for airline flight crew, 
        /// this entitlement type specifies the max number of fixed work days that can be used per 
        /// week (or lost per week) for a given leave period where work schedules are not used.
        /// </summary>
        FixedWorkDays = 10,

        /// <summary>
        /// Entitlement and usage are measured using calendar days from the very first date of usage within the case period
        /// </summary>
        CalendarDaysFromFirstUse = 11,
        /// <summary>
        /// Entitlement and usage are measured using calendar weeks from the very first date of usage within the case period
        /// </summary>
        CalendarWeeksFromFirstUse = 12,
        /// <summary>
        /// Entitlement and usage are measured using calendar months from the very first date of usage within the case period
        /// </summary>
        CalendarMonthsFromFirstUse = 13,
        /// <summary>
        /// Entitlement and usage are measured using calendar years from the very first date of usage within the case period
        /// </summary>
        CalendarYearsFromFirstUse = 14
    }

    /// <summary>
    /// We need some extra special info around entitlement types.
    /// This will tell us how to count them. Any time represents a full day
    /// or it's a percentage of a day
    /// </summary>
    public static class EntitlementTypeExtension
    {
        /// <summary>
        /// Is this a count type of enumerator 
        /// In this case only one per day need exist for the type to count as used
        /// </summary>
        /// <param name="et"></param>
        /// <returns></returns>
        public static bool CalendarType(this EntitlementType et)
        {
            switch (et)
            {
                case EntitlementType.CalendarWeeks:
                case EntitlementType.CalendarDays:
                case EntitlementType.CalendarMonths:
                case EntitlementType.CalendarYears:
                case EntitlementType.CalendarDaysFromFirstUse:
                case EntitlementType.CalendarWeeksFromFirstUse:
                case EntitlementType.CalendarMonthsFromFirstUse:
                case EntitlementType.CalendarYearsFromFirstUse:
                    return true;
                default:
                    return false;
            }

        }
        
        /// <summary>
        /// Is this a percentage type. 
        /// For this the percentages are summed to find out if an
        /// entire base unit has been used.
        /// </summary>
        /// <param name="et"></param>
        /// <returns></returns>
        public static bool PercentageType(this EntitlementType et)
        {
            switch (et)
            {
                case EntitlementType.WorkWeeks:
                case EntitlementType.WorkingHours:
                case EntitlementType.WorkingMonths:
                case EntitlementType.WorkDays:
                case EntitlementType.FixedWorkDays:
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Is this an unlimited type.
        /// For this typically means the user is entitled to an unlimited amount
        /// of time and therefore will never exceed entitlement because of usage;
        /// however usage is still calculated.
        /// </summary>
        /// <param name="et"></param>
        /// <returns></returns>
        public static bool UnlimitedType(this EntitlementType et)
        {
            return et == EntitlementType.ReasonablePeriod;
        }

        /// <summary>
        /// If a new type is added and it's not categorized then this
        /// can be used to check that as well
        /// </summary>
        /// <param name="et"></param>
        /// <returns></returns>
        public static bool IsTypeImplemented(this EntitlementType et)
        {
            return et.CalendarType() || et.PercentageType() || et.UnlimitedType();
        }


        /// <summary>
        /// Compare one Entitilement type to another, returns true
        /// if they are both the same base type: Calendar or Working Hour
        /// </summary>
        /// <param name="et">Type to compare to</param>
        /// <returns>true if they are the same, false if not or not implemented</returns>
        public static bool Compare(this EntitlementType et, EntitlementType compareTo)
        {
            if (!IsTypeImplemented(et) || !IsTypeImplemented(compareTo))
                return false;

            if (et.CalendarType())
                return compareTo.CalendarType();

            if (et.UnlimitedType())
                return compareTo.UnlimitedType();

            return compareTo.PercentageType();

        }

        public static Unit ToUnit(this EntitlementType? et)
        {
            return (et ?? EntitlementType.WorkingHours).ToUnit();
        }
        public static Unit ToUnit(this EntitlementType et)
        {
            switch (et)
            {
                case EntitlementType.WorkWeeks:
                case EntitlementType.CalendarWeeks:
                case EntitlementType.ReasonablePeriod:
                case EntitlementType.CalendarWeeksFromFirstUse:
                    return Unit.Weeks;
                case EntitlementType.CalendarMonths:
                case EntitlementType.WorkingMonths:
                case EntitlementType.CalendarMonthsFromFirstUse:
                    return Unit.Months;
                case EntitlementType.CalendarYears:
                case EntitlementType.CalendarYearsFromFirstUse:
                    return Unit.Years;
                case EntitlementType.CalendarDays:
                case EntitlementType.WorkDays:
                case EntitlementType.FixedWorkDays:
                case EntitlementType.CalendarDaysFromFirstUse:
                    return Unit.Days;
                case EntitlementType.WorkingHours:
                default:
                    return Unit.Hours;
            }
        }

        /// <summary>
        /// Check for Entitlement Type is of Calendar Type From First Use
        /// </summary>
        /// <param name="et">EntitlementType</param>
        /// <returns>bool</returns>
        public static bool CalendarTypeFromFirstUse(this EntitlementType et)
        {
            switch (et)
            {
                case EntitlementType.CalendarDaysFromFirstUse:
                case EntitlementType.CalendarWeeksFromFirstUse:
                case EntitlementType.CalendarMonthsFromFirstUse:
                case EntitlementType.CalendarYearsFromFirstUse:
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Calculate entitlement in days based on entitlement type
        /// </summary>
        /// <param name="et">EntitlementType</param>
        /// <param name="entitlement">entitlement</param>
        /// <returns>entitlement days</returns>
        public static double? ToUnitDays(this EntitlementType et, double? entitlement)
        {
            double? daysInPeriod = 0;
            switch (et)
            {
                case EntitlementType.CalendarDays:
                case EntitlementType.WorkDays:
                case EntitlementType.FixedWorkDays:
                case EntitlementType.CalendarDaysFromFirstUse:
                    daysInPeriod = entitlement.Value;
                    break;
                case EntitlementType.CalendarWeeks:
                case EntitlementType.WorkWeeks:
                case EntitlementType.CalendarWeeksFromFirstUse:
                    daysInPeriod = entitlement.Value * 7;
                    break;
                case EntitlementType.CalendarMonths:
                case EntitlementType.WorkingMonths:
                case EntitlementType.CalendarMonthsFromFirstUse:
                    daysInPeriod = entitlement.Value * 30;
                    break;
                case EntitlementType.CalendarYears:
                case EntitlementType.CalendarYearsFromFirstUse:
                    daysInPeriod = entitlement.Value * 365.25;
                    break;
                default:
                    daysInPeriod = 1;
                    break;
            }

            return daysInPeriod;
        }
    }
}
