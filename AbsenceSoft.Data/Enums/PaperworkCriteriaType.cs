﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
	public enum PaperworkCriteriaType : int
	{
		None = 0,
		Policy = 1,
		AbsenceReason = 2,
		CaseType = 3,
		LeaveOfAbsenceExpression = 4,
		ContactType = 5,
		MilitaryStatus = 6,
		Accommodation = 7,
        WorkState = 8
	}
}
