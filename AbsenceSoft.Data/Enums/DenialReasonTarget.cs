﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// Denial Reason Target
    /// </summary>
    [Flags]
    public enum DenialReasonTarget : int
    {
        None = 0,
        Policy = 1,
        Accommodation = 2
    }
}
