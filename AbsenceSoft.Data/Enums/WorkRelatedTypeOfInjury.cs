﻿using System;

namespace AbsenceSoft.Data.Enums
{
    [Serializable]
    public enum WorkRelatedTypeOfInjury : int
    {
        /// <summary>
        /// (0) The injury type of injury for OSHA/WC work related cases.
        /// </summary>
        Injury = 0,
        /// <summary>
        /// (1) The skin disorder type of injury for OSHA/WC work related cases.
        /// </summary>
        SkinDisorder = 1,
        /// <summary>
        /// (2) The respiratory condition type of injury for OSHA/WC work related cases.
        /// </summary>
        RespiratoryCondition = 2,
        /// <summary>
        /// (3) The poisoning type of injury for OSHA/WC work related cases.
        /// </summary>
        Poisoning = 3,
        /// <summary>
        /// (4) The hearing loss type of injury for OSHA/WC work related cases.
        /// </summary>
        HearingLoss = 4,
        /// <summary>
        /// (6) The patient handling type of injury for OSHA/WC work related cases.
        /// </summary>
        PatientHandling = 6,
        /// <summary>
        /// (5) All other illnesses type of injury for OSHA/WC work related cases.
        /// </summary>
        AllOtherIllnesses = 5
    }
}
