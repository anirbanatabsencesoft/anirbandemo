﻿using System;

namespace AbsenceSoft.Data.Enums
{
    [Serializable]
    public enum WorkRelatedSharpsInjuryLocation : int
    {
        /// <summary>
        /// The finger work related sharps injury location.
        /// </summary>
        Finger = 1,
        /// <summary>
        /// The hand left work related sharps injury location.
        /// </summary>
        HandLeft = 2,
        /// <summary>
        /// The hand right work related sharps injury location.
        /// </summary>
        HandRight = 3,
        /// <summary>
        /// The arm left work related sharps injury location.
        /// </summary>
        ArmLeft = 4,
        /// <summary>
        /// The arm right work related sharps injury location.
        /// </summary>
        ArmRight = 5,
        /// <summary>
        /// The face or head work related sharps injury location.
        /// </summary>
        FaceOrHead = 6,
        /// <summary>
        /// The torso work related sharps injury location.
        /// </summary>
        Torso = 7,
        /// <summary>
        /// The leg left work related sharps injury location.
        /// </summary>
        LegLeft = 8,
        /// <summary>
        /// The leg right work related sharps injury location.
        /// </summary>
        LegRight = 9,
        /// <summary>
        /// The other work related sharps injury location.
        /// </summary>
        Other = 0
    }
}
