﻿namespace AbsenceSoft.Data.Enums
{
    public enum AccommodationDuration : int
    {
        /// <summary>
        /// This is a temporary accommodation.
        /// </summary>
        Temporary = 1,
        /// <summary>
        /// This is a permanent accommodation.
        /// </summary>
        Permanent = 2,
        /// <summary>
        /// This represents permanent and temporary accommodation.
        /// </summary>
        Both = 3
    }
}
