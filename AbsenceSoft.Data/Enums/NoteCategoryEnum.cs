﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// Enumerates note categories.
    /// 
    /// **************************************************************
    /// WARNING: Any time this is updated, for now, 
    /// also need to update the SELECT list in the UI located here: 
    /// $/Absence Tracker/src/dev/AbsenceSoft/Scripts/app-angular/Directives/templates/view-todo-modal.html; Line ~1563
    /// **************************************************************
    /// </summary>
    [Obsolete("Use the note category class (AbsenceSoft.Data.Notes.NoteCategory) instead of this enum")]
    public enum NoteCategoryEnum : int
    {
        CaseSummary = 0,
        EmployeeConversation = 1,
        ManagerConversation = 2,
        HRConversation = 3,
        Medical = 4,
        TimeOffRequest = 5,
        Certification = 6,
        InteractiveProcess = 7,
		WorkRestriction = 8,
        OperationsConversation = 9,
        HealthCareProviderConversation = 10,
        WorkerCompensationConversation = 11,
        SafetyConversation = 12,
        LegalConversation = 13,
        ERCConversation = 14,
        EmployeeRelationsConversation = 15,
        BenefitsConversation = 16,
        LOAConversation = 17,
        FacilitiesConversation = 18,
        PayrollConversation = 19,
        STDLOAVendorConversation = 20,
        LTDVendorConversation = 21,
        Referral = 23,
        Other = 22,
        DisabilityNote = 24,
        Diagnosis = 25
    }
}
