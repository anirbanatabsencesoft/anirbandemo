﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// Represents a contact type designation.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class ContactTypeDesignationAttribute : Attribute
    {
        private ContactTypeDesignationType _designation;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContactTypeDesignationAttribute"/> class.
        /// </summary>
        public ContactTypeDesignationAttribute() : this(ContactTypeDesignationType.None) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContactTypeDesignationAttribute"/> class specifying
        /// the default designation for this attribute.
        /// </summary>
        /// <param name="designation">The designation for which the contact type is bucketed.</param>
        public ContactTypeDesignationAttribute(ContactTypeDesignationType designation) { _designation = designation; }

        /// <summary>
        /// Gets the designation for which the contact type is bucketed.
        /// </summary>
        public ContactTypeDesignationType Designation { get { return _designation; } }
    }
}
