﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum PolicyBehavior
    {
        ReducesWorkWeek = 1,
        Ignored = 2,
        IgnoredNoLostTime = 3,
    }
}
