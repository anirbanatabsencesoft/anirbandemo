﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
	/// <summary>
	/// Used for restricting enum values based on Application Feature
	/// </summary>
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Class, AllowMultiple = false)]
	public sealed class FeatureRestrictionAttribute : Attribute
	{
        /// <summary>
        /// The feature private storage thingy.
        /// </summary>
		private Feature _feature = Feature.None;

        /// <summary>
        /// Initializes a new instance of the <see cref="FeatureRestrictionAttribute"/> class.
        /// </summary>
		public FeatureRestrictionAttribute() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="FeatureRestrictionAttribute"/> class.
        /// </summary>
        /// <param name="feature">The feature.</param>
		public FeatureRestrictionAttribute(Feature feature)
		{
			_feature = feature;
		}

		/// <summary>
		/// Gets the list of states that this enumerated value is restricted to.
		/// </summary>
		public Feature Feature { get { return _feature; } }
	}
}
