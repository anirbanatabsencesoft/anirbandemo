﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum DiagnosisType : int
    {
        ICD9 = 9,
        ICD10 = 10
    }
}
