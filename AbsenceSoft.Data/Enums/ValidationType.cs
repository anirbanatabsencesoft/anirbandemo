﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// These are the types of validation messages that can be 
    /// returned to the UI from the server
    /// </summary>
    [Flags]
    public enum ValidationType : int
    {
        Info = 1,
        Warning = 2,
        Error = 3
    }
}
