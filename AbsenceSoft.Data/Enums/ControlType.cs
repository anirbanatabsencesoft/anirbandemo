﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum ControlType : int
    {
        ReadOnlyText = 0,
        TextBox = 1,
        SelectList = 2,
        TextArea = 3,
        Date = 4,
        Minutes = 5,
        Numeric = 6,
        RadioButtonList = 7,
        CheckBox = 8,
        CheckBoxList = 9,
        Hidden = 10,
        Duration = 11,
        MultiSelectList = 12
    }
}
