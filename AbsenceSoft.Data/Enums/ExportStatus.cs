﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum ExportStatus : int
    {
        Pending = 0,
        Processing = 1,
        Failed = 2,
        Complete = 3,
        Expired = 4,
    }
}
