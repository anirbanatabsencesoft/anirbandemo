﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    [Serializable]
    public enum IntermittentStatus : int
    {
        Pending = 0,
        Allowed = 1,
        Denied = 2
    }

    public static class IntermittentStatusExtensions
    {
        public static IntermittentStatus ToIntermittentStatus(this AdjudicationStatus status)
        {
            switch(status)
            {
                case AdjudicationStatus.Pending:
                    return IntermittentStatus.Pending;
                case AdjudicationStatus.Approved:
                    return IntermittentStatus.Allowed;
                case AdjudicationStatus.Denied:
                    return IntermittentStatus.Denied;
            }

            throw new AbsenceSoftException("IntermittentStatusExtensions: ToAdjudicationStatus - Unknown adjudication status");
        }
    }
}
