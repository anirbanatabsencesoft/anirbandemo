﻿using System;
namespace AbsenceSoft.Data.Enums
{
    [Serializable]
    /// <summary>
    /// Enumerates case event types representing different events that may occur on a case, sometimes more
    /// than once.
    /// </summary>
    public enum CaseEventType : int
    {
        /// <summary>
        /// Case is created.
        /// </summary>
        [MaxOccurrences(1)]
        CaseCreated = 0,
        /// <summary>
        /// Employee has returned to work.
        /// </summary>
        [MaxOccurrences(1)]
        ReturnToWork = 1,
        /// <summary>
        /// Employee is anticipating delivery of a baby on this date, or has delivered
        /// a baby on this date.
        /// </summary>
        [MaxOccurrences(1)]
        DeliveryDate = 2,
        /// <summary>
        /// Employee will start bonding on this date.
        /// </summary>
        [MaxOccurrences(1)]
        BondingStartDate = 3,
        /// <summary>
        /// Employee will end bonding on this date.
        /// </summary>
        [MaxOccurrences(1)]
        BondingEndDate = 4,
        /// <summary>
        /// Employee was injured or got sick/illness, or was diagnosed on this date.
        /// </summary>
        [MaxOccurrences(1)]
        IllnessOrInjuryDate = 5,
        /// <summary>
        /// Employee was admitted to the hospital on this date.
        /// </summary>
        HospitalAdmissionDate = 6,
        /// <summary>
        /// Employee was released from the hospital on this date.
        /// </summary>
        HospitalReleaseDate = 7,
        /// <summary>
        /// The employee signed their return to work release form on this date
        /// </summary>
        ReleaseReceived = 8,
        /// <summary>
        /// The dates paperwork was received from the employee.
        /// </summary>
        PaperworkReceived = 9,
        /// <summary>
        /// An accommodation request was added or an accommodation itself was added.
        /// </summary>
        AccommodationAdded = 10,
        /// <summary>
        /// The date the case was closed
        /// </summary>
        [MaxOccurrences(1)]
        CaseClosed = 11,
        /// <summary>
        /// The date the case was cancelled
        /// </summary>
        [MaxOccurrences(1)]
        CaseCancelled = 12,
        /// <summary>
        /// The date of adoption of a child
        /// </summary>
        [MaxOccurrences(1)]
        AdoptionDate = 13,
        /// <summary>
        /// Employee is estimated to return to work date. Can be multiple
        /// estimated return to work dates as things change.
        /// </summary>
        EstimatedReturnToWork = 14,
        /// <summary>
        /// Holidays were modified by employer. As a result,
        /// case must be recalculated.
        /// </summary>
        EmployerHolidayScheduleChanged = 15,
        /// <summary>
        /// The case start date; this is a special event type that always returns
        /// the case value rather than being stored in the case event collection.
        /// </summary>
        [MaxOccurrences(1)]
        CaseStartDate = 16,
        /// <summary>
        /// The case end date; this is a special event type that always returns
        /// the case value rather than being stored in the case event collection.
        /// </summary>
        [MaxOccurrences(1)]
        CaseEndDate = 17,
        /// <summary>
        /// The relapse of any Case 
        /// </summary>
        Relapse = 18
    }
}
