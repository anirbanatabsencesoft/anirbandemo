﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum RegistrationStatus
    {
        DisabledUser = 0,
        UserExists = 1,
        EmailSent = 2,
        EmployeeNotFound = 3,
        ErrorSendingEmail = 4,
        EmailResent = 5,
        EmailResentToAlt = 6,
        SelfRegistrationDisabled = 7,
        EmployeeNotUnique = 8
    }
}
