﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// Describes an employee's affiliation with an organization.
    /// </summary>
    public enum OrganizationAffiliation : int
    {
        /// <summary>
        /// The viewer is a person who can see the organization, but is not counted as a member of it.
        /// </summary>
        Viewer = 0,
        /// <summary>
        /// The employee is a member of this organization in some capacity, either as an employee, supervisor, manager, etc. An
        /// employee is also a Viewer of the organization.
        /// </summary>
        Member = 1,
        /// <summary>
        /// The leader is a person who is the leader of the organization and that makes them super awesome. A leader is also
        /// a Viewer and a Member of the organization.
        /// </summary>
        Leader = 2
    }
}
