﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum PaperworkReviewStatus : int
    {
        NotApplicable = 0,
        Pending = 1,
        Received = 2,
        Complete = 3,
        Incomplete = 4,
        Denied = 5,
		NotReceived = 6
    }
}
