﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// how is a Intermittent Restrictions used
    /// </summary>
    public enum IntermittentRestrictions
    {
        /// <summary>
        /// Intermittent restrictions will start from Beginning of Week
        /// </summary>
        BeginningOfPeriod,

        /// <summary>
        /// Intermittent restrictions will start From Date Of Absence
        /// </summary>
        FromDateOfAbsence

    }
}
