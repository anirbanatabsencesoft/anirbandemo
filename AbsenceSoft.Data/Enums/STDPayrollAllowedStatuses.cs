﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    [Flags]
    public enum STDPayrollAllowedStatuses
    {
        None = 0,
        Submit = 1,
        Exclude = 2
    }
}
