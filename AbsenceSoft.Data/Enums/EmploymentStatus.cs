﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public static class EmploymentStatusValue
    {
        public const char Active = 'A';
        public const char Inactive = 'I';
        public const char Terminated = 'T';
        public const char Leave = 'L';

        public static IEnumerable<char> AllValues = new[] { Active, Inactive, Terminated, Leave };

        public static bool IsValidValue(char value)
        {
            return AllValues.Contains(value);
        }

        private static Dictionary<char, string> codeLookup = new Dictionary<char, string>
        {
            {Active, "Active"},
            {Inactive, "Inactive"},
            {Terminated, "Terminated, Retired or Layoff"},
            {Leave, "Leave"}
        };
        public static IDictionary<char, string> GetCodeLookup()
        {
            return codeLookup;
        }

        public static string GetCodeLookupAsString()
        {
            return String.Join(", ", codeLookup.Select(s => String.Format("'{0}' ({1})", s.Key, s.Value)));
        }

    }

    /// <summary>
    /// Employment status for a given employee. i.e. active, terminated
    /// </summary>
    public enum EmploymentStatus
    {
        [Description("Active")]
        Active = EmploymentStatusValue.Active,

        [Description("Inactive")]
        Inactive = EmploymentStatusValue.Inactive,

        [Description("Terminated, Retired or Layoff")]
        Terminated = EmploymentStatusValue.Terminated,

        [Description("Leave")]
        Leave = EmploymentStatusValue.Leave
    }
}
