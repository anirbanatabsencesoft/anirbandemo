﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Enums
{
    public enum CaseCancelReason : int
    {
        Other = 0,
        LeaveNotNeededOrCancelled = 1,
        EnteredInError = 2,
        InquiryClosed = 3,
    }
}
