﻿namespace AbsenceSoft.Data.Enums
{
    /// <summary>
    /// Represents the rules for success for all rules in a rule group. Determines
    /// how the rule group itself marks a pass or fail.
    /// </summary>
    public enum RuleGroupSuccessType
    {
        /// <summary>
        /// All rules in the rule group must pass.
        /// </summary>
        And,
        /// <summary>
        /// At least one rule in the rule group must pass.
        /// </summary>
        Or,
        /// <summary>
        /// No rules in the rule group may pass (negative <c>And</c>).
        /// </summary>
        Not,
        /// <summary>
        /// At least one rule in the rule group must fail (negative <c>Or</c>).
        /// </summary>
        Nor,
    }
}
