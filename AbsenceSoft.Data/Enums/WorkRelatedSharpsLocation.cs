﻿using System;

namespace AbsenceSoft.Data.Enums
{
    [Serializable]
    public enum WorkRelatedSharpsLocation : int
    {
        /// <summary>
        /// The other work related sharps location and department.
        /// </summary>
        Other = 0,
        /// <summary>
        /// The patient room work related sharps location and department.
        /// </summary>
        PatientRoom = 1,
        /// <summary>
        /// The ICU work related sharps location and department.
        /// </summary>
        ICU = 2,
        /// <summary>
        /// The outside patient room work related sharps location and department.
        /// </summary>
        OutsidePatientRoom = 3,
        /// <summary>
        /// The emergency department work related sharps location and department.
        /// </summary>
        EmergencyDepartment = 4,
        /// <summary>
        /// The operating room or PACU work related sharps location and department.
        /// </summary>
        OperatingRoomOrPACU = 5,
        /// <summary>
        /// The clinical laboratory work related sharps location and department.
        /// </summary>
        ClinicalLaboratory = 6,
        /// <summary>
        /// The outpatient clinic or office work related sharps location and department.
        /// </summary>
        OutpatientClinicOrOffice = 7,
        /// <summary>
        /// The utility area work related sharps location and department.
        /// </summary>
        UtilityArea = 8
    }
}
