﻿using AbsenceSoft.Common;
using AbsenceSoft.Common.Properties;
using AbsenceSoft.Common.Security;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace AbsenceSoft.Data
{
    public static class Extensions
    {
        /// <summary>
        /// Upserts all of the entities in this IEnumerable collection against the database.
        /// </summary>
        /// <typeparam name="TEntity">The entity type to be saved to the database.</typeparam>
        /// <param name="items">The instance of IEnumerable of TEntity that this method extends.</param>
        public static void UpdateAll<TEntity>(this IEnumerable<TEntity> items) where TEntity : BaseEntity<TEntity>, new()
        {
            using (new InstrumentationContext("{0}.UpdateAll", BaseEntity<TEntity>.ClassTypeName))
            {
                new MongoRepository<TEntity>().Update(items);
            }
        }

        /// <summary>
        /// Audits an entity on update or delete so we can store the changes in an audit record if needed later on.
        /// </summary>
        /// <typeparam name="TEntity">The entity type we are auditing.</typeparam>
        /// <param name="entity">The updated or deleted entity.</param>
        /// <param name="type">The audit type to be performed.</param>
        /// <returns>The newly created audit record.</returns>
        public static Audit<TEntity> Audit<TEntity>(this TEntity entity, AuditType type) where TEntity : BaseEntity<TEntity>, new()
        {
            Audit<TEntity> audit = new Audit<TEntity>()
            {
                Value = entity,
                AuditType = type,
                CreatedBy = entity.ModifiedById,
            };
            audit.Save();
            return audit;
        }

        public static AdjudicationStatus Determination(this List<AppliedPolicyUsage> usage)
        {
            if (usage.Any(m => m.Determination == AdjudicationStatus.Approved))
                return AdjudicationStatus.Approved;
            else if (usage.All(m => m.Determination == AdjudicationStatus.Denied))
                return AdjudicationStatus.Denied;
            else
                return AdjudicationStatus.Pending;
        }

        /// <summary>
        /// Breaks up a collection or enumerable into a specified max batch size, and for
        /// query backed enumerables breaks up the result set into paged results obfuscated
        /// through a batch operation.
        /// </summary>
        /// <typeparam name="T">The type of T</typeparam>
        /// <param name="collection">The collection to batch or enumerate and batch</param>
        /// <param name="batchSize">The total maximum size of each batch</param>
        /// <returns>An enumerable collection of lists (batches).</returns>
        public static IEnumerable<List<T>> Batch<T>(this IEnumerable<T> collection, int batchSize)
        {
            int count = collection.Count();
            for (int index = 0; index < count; index += batchSize)
                yield return collection.Skip(index).Take(batchSize).ToList();
        }

        /// <summary>
        /// Fixes the stupid. Inspects the provided BsonDocument this method extends against the instance properties
        /// of the provided entity object to determine if the entity already has properties named the same, and if so, then
        /// attempts to filter out nonsense, merge the values or simply removes the element(s) from the BsonDocument
        /// since the property is not well formatted and already exists on the entity definition.
        /// </summary>
        /// <param name="meta">The metadata BsonDocument to which to compare elements from.</param>
        /// <param name="entity">The entity for which to map the BsonDocument element names against its properties.</param>
        /// <returns>The modified original BsonDocument for a fluid API</returns>
        public static BsonDocument FixStupid(this BsonDocument meta, object entity)
        {
            if (meta == null || entity == null || !meta.Any())
                return meta;

            var properties = entity.GetType().GetProperties();
            if (properties.Length == 0)
                return meta;

            var bad = new List<string>();
            foreach (var element in meta.Elements)
            {
                var prop = properties.FirstOrDefault(p => p.Name == element.Name);
                if (prop == null)
                    continue;

                bad.Add(element.Name);
            }

            bad.ForEach(meta.Remove);

            return meta;
        }

        /// <summary>
        /// Converts the units of time from one value to another.
        /// </summary>
        /// <param name="convertFromUnit">The convert from unit.</param>
        /// <param name="convertToUnit">The convert to unit.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static double ConvertUnits(this Unit convertFromUnit, Unit convertToUnit, double value)
        {
            if (convertFromUnit == convertToUnit)
                return value;

            double minutes = 0D;
            switch (convertFromUnit)
            {
                case Unit.Minutes:
                    minutes = value;
                    break;
                case Unit.Hours:
                    minutes = value * 60D;
                    break;
                case Unit.Days:
                    minutes = value * 1440D;
                    break;
                case Unit.Weeks:
                    minutes = value * 10080D;
                    break;
                case Unit.Months:
                    minutes = value * 43830D;
                    break;
                case Unit.Years:
                    minutes = value * 525960D;
                    break;
            }
            switch (convertToUnit)
            {
                case Unit.Minutes:
                    return minutes;
                case Unit.Hours:
                    return minutes / 60D;
                case Unit.Days:
                    return minutes / 1440D;
                case Unit.Weeks:
                    return minutes / 10080D;
                case Unit.Months:
                    return minutes / 43830D;
                case Unit.Years:
                    return minutes / 525960D;
            }
            return 0D;
        }

        /// <summary>
        /// Runs the query but only returns a distinct list based on code and the override heiararchy (Employer &gt; Customer &gt; Core).
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="includeSuppressed">if set to <c>true</c> [include suppressed].</param>
        /// <returns></returns>
        public static IEnumerable<TEntity> DistinctFind<TEntity>(this IQueryable<TEntity> query, bool includeSuppressed = false) where TEntity : BaseEmployerOverrideableEntity<TEntity>, new()
        {
            var results = query.ToList().OrderBy(r => r.Code).ThenBy(r =>
            {
                if (!string.IsNullOrWhiteSpace(r.EmployerId))
                    return 1;
                if (!string.IsNullOrWhiteSpace(r.CustomerId))
                    return 2;
                return 3;
            });
            string lastCode = null;
            foreach (var item in results)
            {
                if (item == null)
                    continue;
                if (item.Code == lastCode)
                    continue;
                lastCode = item.Code;
                if (item.IsCustom && item.Suppressed && !includeSuppressed)
                    continue;
                yield return item;
            }
        }


        /// <summary>
        /// get the value for a particular entity name
        /// </summary>
        /// <param name="meta"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static object ValueByName(this BsonDocument meta, string name)
        {
            if (meta == null || name == null || !meta.Any())
            {
                return null;
            }

            foreach (var element in meta.Elements)
            {            
                if (element.Name == name)
                { 
                    return element.Value;
                }               
            }
            return null;
        }
    }
}
