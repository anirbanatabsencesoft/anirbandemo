﻿using AbsenceSoft.Common.Serializers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Common;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Audit;

namespace AbsenceSoft.Data
{
    /// <summary>
    /// The schedule class is NOT stored separetly in the DB, it is always a child document of another type and
    /// represents a work schdule.
    /// </summary>
    [Serializable]
    public class Schedule : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of a <see cref="Schedule"/> class.
        /// </summary>
        public Schedule()
        {
            Times = new List<Time>();
        }

        /// <summary>
        /// The type of schedule this represents, either a Full Time, Part Time, One Time or Rotating.
        /// </summary>
        public ScheduleType ScheduleType { get; set; }

        /// <summary>
        /// Gets or sets the date that the representative schedule starts, whether or not that is the first
        /// date actual time is represented by the schedule; its effective date basically.
        /// </summary>
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the date that the representative schedule ends, or <c>null</c> if the schedule
        /// is perpetual, whether or not the end date represents actual time.
        /// </summary>
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// A list of times that comprise the base repeating segment of the schedule, e.g. for a full-time,
        /// weekday M-F 40hrs a week schedule, would begin on the first Monday of or from the StartDate
        /// </summary>
        public List<Time> Times { get; set; }

        /// <summary>
        /// Total minutes the emplyee is scheduled to work in a week
        /// </summary>
        public int FteMinutesPerWeek { get; set; }

        /// <summary>
        /// Total percentage the emplyee is scheduled to work in a week
        /// </summary>
        public decimal FteTimePercentage { get; set; }

        /// <summary>
        /// Average total minutes the emplyee is scheduled to work in a week
        /// </summary>
        public FteWeeklyDuration FteWeeklyDuration { get; set; }
        

        /// <summary>
        /// Returns a display formatted string representing this schedule
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            switch (ScheduleType)
            {
                case ScheduleType.Weekly:
                    return "Weekly: " + formattedTimes();
                case ScheduleType.Rotating:
                    return "Rotating: " + formattedTimes();
                case ScheduleType.Variable:
                    return "Variable: Average of " + formattedTimes();
                case ScheduleType.FteVariable:
                    return String.Concat("FTE Variable: Average Work Time in Week: ", FteMinutesPerWeek.ToFriendlyTime() , ", FTE Type: " , FteWeeklyDuration.GetAttribute<DescriptionAttribute>().Description);
            }

            return base.ToString();
        }

        /// <summary>
        /// Formats the times and tries to make something pretty 
        /// out of the day ranges in the schedule.
        /// </summary>
        /// <returns></returns>
        private string formattedTimes()
        {
            if (Times.Count == 0)
                return "";

            // do a basic level break, find sets in a row with the same hours and build a list

            // start day name, end day name, minutes
            List<ToFormat> dts = new List<ToFormat>(){
                new ToFormat(string.Format("{0:ddd}",Times[0].SampleDate),"",Times[0].TotalMinutes ?? 0)
            };

            // how many we have found (could use the count, so this is a just a shortcut)
            int fnd = 0;
            // how many in a row we have matched
            int matchCount = 0;

            // the level break 
            int mins = Times[0].TotalMinutes ?? 0;
            for (int x = 1; x < Times.Count; x++)
            {
                // if the minutes have changed, then format something
                if((Times[x].TotalMinutes ?? 0) != mins)
                {
                    // if two in a row do not match then it is just a one off time
                    // format accordingly
                    if(matchCount > 0)
                        dts[fnd].EndDay = string.Format("{0:ddd}", Times[x-1].SampleDate);

                    matchCount = 0;     // start counting them again
                    mins = Times[x].TotalMinutes ?? 0;
                    fnd++;

                    dts.Add(new ToFormat(string.Format("{0:ddd}",Times[x].SampleDate),"",mins));

                }

                matchCount++;

            }

            // and check for the last break
            dts[fnd].TotalMinutes = Times[Times.Count - 1].TotalMinutes ?? 0;
            if (matchCount > 1)
                dts[fnd].EndDay = string.Format("{0:ddd}", Times[Times.Count - 1].SampleDate);


            // now add up all these strings and return them
            string rtVal = "";
            fnd = 0;
            foreach (ToFormat f in dts.Where(d => d.TotalMinutes > 0))
            {
                rtVal += (fnd == 0 ? "" : ", ") + f.Format();
                fnd++;
            }

            return rtVal;
        }

        private class ToFormat
        {
            /// <summary>
            /// Gets or sets the start day.
            /// </summary>
            /// <value>
            /// The start day.
            /// </value>
            public string StartDay { get; set; }

            /// <summary>
            /// Gets or sets the end day.
            /// </summary>
            /// <value>
            /// The end day.
            /// </value>
            public string EndDay { get; set; }

            /// <summary>
            /// Gets or sets the total minutes.
            /// </summary>
            /// <value>
            /// The total minutes.
            /// </value>
            public int TotalMinutes { get; set; }

            /// <summary>
            /// Initializes a new instance of the <see cref="ToFormat"/> class.
            /// </summary>
            /// <param name="s">The s.</param>
            /// <param name="e">The e.</param>
            /// <param name="m">The m.</param>
            public ToFormat(string s, string e, int m)
            {
                StartDay = s;
                EndDay = e;
                TotalMinutes = m;
            }

            /// <summary>
            /// Formats this instance.
            /// </summary>
            /// <returns></returns>
            public String Format()
            {
                if (string.IsNullOrWhiteSpace(EndDay) || StartDay == EndDay)
                    return string.Format("{0}: {1}", StartDay, TotalMinutes.ToFriendlyTime());
                else
                    return string.Format("{0} - {1}: {2}", StartDay, EndDay, TotalMinutes.ToFriendlyTime());
            }
        }
    }
}
