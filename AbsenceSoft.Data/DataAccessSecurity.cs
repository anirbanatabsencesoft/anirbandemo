﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AbsenceSoft.Data
{
    public static class DataAccessSecurity
    {
        /// <summary>
        /// Creates a Mongo Query that will filter on a user's customer (if appropriate) as well as based on Employer access
        /// (where applicable) for a user's Employer access + affected permissions.
        /// </summary>
        /// <param name="user">The user account to build the data access filters for.</param>
        /// <param name="permission">The permission this user should have effective for each Employer to be filtered on, if appropriate; otherwise <c>null</c>.</param>
        /// <param name="employeeIdFieldName">Name of the employee id field.</param>
        /// <returns>
        /// A structured <see cref="T:MongoDB.Driver.IMongoQuery" /> that has an implemented $and: [{ $or: [{ CustomerId...]}, { $or: [EmployerId...]}]
        /// </returns>
        public static IMongoQuery BuildDataAccessFilters(this User user, Permission permission, string employeeIdFieldName = null) { return user.BuildDataAccessFilters(permission.Id, employeeIdFieldName); }
        /// <summary>
        /// Creates a Mongo Query that will filter on a user's customer (if appropriate) as well as based on Employer access
        /// (where applicable) for a user's Employer access + affected permissions.
        /// </summary>
        /// <param name="user">The user account to build the data access filters for.</param>
        /// <param name="permission">The permission this user should have effective for each Employer to be filtered on, if appropriate; otherwise <c>null</c>.</param>
        /// <param name="employeeIdFieldName">Name of the employee id field.</param>
        /// <returns>
        /// A structured <see cref="T:MongoDB.Driver.IMongoQuery" /> that has an implemented $and: [{ $or: [{ CustomerId...]}, { $or: [EmployerId...]}]
        /// </returns>
        public static IMongoQuery BuildDataAccessFilters(this User user, string permission = null, string employeeIdFieldName = null)
        {
            var noCustomerQuery = Query.EQ("CustomerId", BsonNull.Value);
            var noEmployerQuery = Query.EQ("EmployerId", BsonNull.Value);

            // If no user, then return a query that ensures we're only querying data without a CustomerId and EmployerId
            if (user == null) return Query.And(noCustomerQuery, noEmployerQuery);
            // User must belong to a customer, otherwise ensure we're only querying data without a CustomerId and EmployerId
            if (string.IsNullOrWhiteSpace(user.CustomerId)) return Query.And(noCustomerQuery, noEmployerQuery);
            // User must have access to at least 1 employer, otherwise, ensure we're only querying records that have a CustomerId, but NOT an
            //  EmployerId, 'cause they have no employer access, so duh.
            if (user.Employers == null || !user.Employers.Any(e => e.IsActive))
                return Query.And(
                    Query.In("CustomerId", new BsonValue[] { BsonNull.Value, new BsonObjectId(new ObjectId(user.CustomerId)) }),
                    Query.NotExists("EmployerId")
                );

            // Filter out any that are not active
            var employerIds = user.Employers.Where(e => e.IsActive).Select(e => e.EmployerId).Distinct();
          
            // Ensure the permissions passed in are empty (meaning just all active) or that 
            //  if we have explicit intent, that the user has the appropriate permissions for that function for that employer
            if (!string.IsNullOrWhiteSpace(permission) && employerIds != null)
                employerIds = User.Permissions.GetEmpIdsWithPermission(user, employerIds.ToList(), permission);



            // Build the final array
            var ids = employerIds
                // Convert to BsonValue collection
                .Select(e => (BsonValue)new BsonObjectId(new ObjectId(e)))
                // Return the final list
                .ToList();
            // Insert our null value
            ids.Insert(0, BsonNull.Value);

            List<IMongoQuery> ands = new List<IMongoQuery>()
            {
                Query.In("CustomerId", new BsonValue[] { BsonNull.Value, new BsonObjectId(new ObjectId(user.CustomerId)) }),
                Query.In("EmployerId", ids)
            };
            if (user.Customer.HasFeature(Feature.OrgDataVisibility))
            {
                ands.Add(BuildOrganizationDataAccessFilters(user, employeeIdFieldName));
            }
            else if (user.ApplyEmployeeSpecificFilters())
            {
                var empId = employeeIdFieldName ?? "EmployeeId";
                ands.Add(Query.Or(Query.NotExists(empId), Query.In(empId,
                    User.HasVisibilityTo.Select(u => new BsonObjectId(new ObjectId(u)))
                    .Concat(user.Employers.Where(e => !string.IsNullOrWhiteSpace(e.EmployeeId)).Select(e => new BsonObjectId(new ObjectId(e.EmployeeId)))))
                ));
            }
            if (user.Employers.Where(e => e.EmployeeId != null).Select(a => a.OrgCodes).Any())
            {
                ands.Add(BuildOrganizationDataAccessFilters(user));
            }

            // Return an And query with the Customer and Employer specifications, where each is OR'd based on existence or match.
            return Query.And(ands);
        }//end: BuildDataAccessFilters

        private static bool ApplyEmployeeSpecificFilters(this User user)
        {
            if (User.Permissions.GetProjectedPermissions(user).Contains(Permission.ViewAllEmployees.Id))
                return false;

            if (User.IsEmployeeSelfServicePortal || user.Customer.HasFeature(Feature.ESSDataVisibilityInPortal))
                return true;

            return false;
        }
        
        /// <summary>
        /// Build a query to determine whether current user can access employee details
        /// If Current user is an admin or has View All Permissions Organization filer is bypassed
        /// </summary>
        /// <param name="user"></param>
        /// <param name="employeeFieldName"></param>
        /// <returns></returns>
        public static IMongoQuery BuildOrganizationDataAccessFilters(this User user, string employeeFieldName = "EmployeeId")
           {
            Dictionary<string, List<EmployeeOrganization>> userOrganizations = null;
            Dictionary<string, List<EmployeeOrganization>> userEmployeeOrganizations = null;
            List<EmployeeOrganization> userEmployeeOrganizationsAssigned = null;
            Dictionary<string, bool> viewAllEmployeesPermissioned = null;
            List<IMongoQuery> query = new List<IMongoQuery>();

            var orgAssignedToUser = user.Employers.Where(e => e.EmployeeId != null)?.SelectMany(a => a.OrgCodes).Distinct().ToList();
            if (orgAssignedToUser != null && orgAssignedToUser.Any())
            {
                List<Employee> listEmps = new List<Employee>();
                foreach (var orgCode in orgAssignedToUser)
                {
                    List<string> employeesOfOrg = new List<string>();
                    userEmployeeOrganizationsAssigned = EmployeeOrganization.AsQueryable().Where(org => org.Code == orgCode).ToList();
                    if (userEmployeeOrganizationsAssigned != null && userEmployeeOrganizationsAssigned.Any())
                    {
                        employeesOfOrg = userEmployeeOrganizationsAssigned.Select(p => p.EmployeeNumber).ToList();
                    }
                    if (employeesOfOrg != null && employeesOfOrg.Any())
                    {
                        listEmps.AddRange(Employee.AsQueryable(User.Current).Where(empl => employeesOfOrg.Contains(empl.EmployeeNumber) ));
                    }
                }

                List<BsonObjectId> listEmpList = listEmps.Select(empl => new BsonObjectId(new ObjectId(empl.Id))).ToList();
                query.Add(Query.Or(Query.NotExists("EmployeeId"),
                                               Query.Or(Query.NotExists("EmployerId"), Query.EQ("EmployerId", BsonNull.Value)),
                                               Query.In("EmployeeId",
                                                        listEmpList
                                )));
            }
            if (user.Customer.HasFeature(Feature.OrgDataVisibility))
            {
                List<string> empAccessibleList = new List<string>();                
                viewAllEmployeesPermissioned = user.Employers.ToDictionary(e => e.EmployerId, e => User.Permissions.GetPermissions(user, e.EmployerId).Contains(Permission.ViewAllEmployees.Id));
                //Hard coding view all employee permissions.Handy while debugging
                //string[] keys = viewAllEmployeesPermissioned.Keys.ToArray<string>();
                //keys.ForEach(key => viewAllEmployeesPermissioned[key] = false);
                userEmployeeOrganizations = GetOrganizationFilterCachedItem<Dictionary<string, List<EmployeeOrganization>>>("EmpOrgs", () => User.Current.Employers.Where(e => e.EmployeeId != null && viewAllEmployeesPermissioned[e.EmployerId]==false).ToDictionary(e => e.EmployerId, e => EmployeeOrganization.AsQueryable().Where(org => org.EmployerId == e.EmployerId).ToList()), 1);
                userOrganizations = GetOrganizationFilterCachedItem<Dictionary<string, List<EmployeeOrganization>>>("UserOrgs", () => User.Current.Employers.Where(e => e.EmployeeId != null && viewAllEmployeesPermissioned[e.EmployerId] == false).ToDictionary(e => e.EmployerId, e => (e.EmployeeId != null ? EmployeeOrganization.AsQueryable().Where(org => org.EmployerId == e.EmployerId && org.EmployeeNumber == Employee.GetById(e.EmployeeId).EmployeeNumber).ToList() : null)), 1);                
                List<EmployeeOrganization> userOrgList = new List<EmployeeOrganization>();
                foreach (List<EmployeeOrganization> orgList in userOrganizations.Values)
                {
                    if (orgList != null && orgList.Count > 0 && (!viewAllEmployeesPermissioned[orgList[0].EmployerId]))

                    {
                        userOrgList.AddRange(orgList);
                    }
                }
                foreach (List<EmployeeOrganization> empOrgs in userEmployeeOrganizations.Values)
                {
                    foreach (EmployeeOrganization empOrg in empOrgs)
                    {
                        if (empOrg != null && userOrgList.Count > 0 && user.HasEmployeeOrganizationAccess(empOrg, userOrgList))
                        {
                            empAccessibleList.Add(empOrg.EmployeeNumber);
                        }                        
                    }
                }
                foreach (EmployerAccess emp in user.Employers)
                {
                    //always give access to employee's own records within the employer they belong too.
                    employeeFieldName = employeeFieldName ?? "EmployeeId";
                    query.Add(Query.In(employeeFieldName, User.Current.Employers.Where(e => e.EmployeeId != null).Select(e => new BsonObjectId(new ObjectId(e.EmployeeId)))));

                    if (!viewAllEmployeesPermissioned[emp.EmployerId])
                    {
                        
                        List<BsonObjectId> list = Employee.AsQueryable().Where(empl => empl.CustomerId == user.CustomerId && empl.EmployerId == emp.EmployerId && empAccessibleList.Contains(empl.EmployeeNumber)).Select(empl => new BsonObjectId(new ObjectId(empl.Id))).ToList();
                        query.Add(Query.Or(Query.NotExists(employeeFieldName),
                                           Query.Or(Query.NotExists("EmployerId"), Query.EQ("EmployerId", BsonNull.Value)),
                                           Query.In(employeeFieldName,
                                                    GetOrganizationFilterCachedItem<IQueryable<BsonObjectId>>("EmpAccessOrgs " + emp.EmployerId, () => Employee.AsQueryable()
                                                    .Where(empl => empl.CustomerId == user.CustomerId && empl.EmployerId == emp.EmployerId && empAccessibleList.Contains(empl.EmployeeNumber))
                                                    .Select(empl => new BsonObjectId(new ObjectId(empl.Id))), 1)) 
                                           ));

                        query.Add(Query.Or(Query.NotExists(employeeFieldName),
                                           Query.Or(Query.NotExists("EmployerId"), Query.EQ("EmployerId", BsonNull.Value)),
                                           Query.In(employeeFieldName,
                                                    list
                            )));
                    }
                    else
                    {
                        query.Add(Query.Or(Query.NotExists("EmployerId"),                                            
                                           Query.In("EmployerId", new BsonValue[] { BsonNull.Value, new BsonObjectId(new ObjectId(emp.EmployerId))})
                                           )); 
                    }
                }
            }
            IMongoQuery custQuery = Query.Or(Query.NotExists("CustomerId"), Query.In("CustomerId", new BsonValue[] { BsonNull.Value, new BsonObjectId(new ObjectId(user.CustomerId))}));
            return query != null && query.Count > 0 ? Query.And(custQuery, Query.Or(query)) : custQuery;            
        }

        private static object syncOrgLock = new object();
        /// <summary>
        /// Cache the reference values which is looked up when filtering employee details according to organization hierarchy
        /// Cached since these reference values are repeatedly needed by Todo's service, Case service & Employee Service.
        /// All execute in paralle, hence important to cache for performance benefit and sync'd to avoid concurrency issues
        /// Can be dilgently used to cache other objects.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheKey"></param>
        /// <param name="query"></param>
        /// <param name="durationInMinutes"></param>
        /// <returns></returns>
        private static T GetOrganizationFilterCachedItem<T>(string cacheKey, Func<T> query, int durationInMinutes)
        {
            lock (syncOrgLock)
            {
                object obj = HttpContext.Current.Cache[User.Current.Email + cacheKey];
                if (obj == null)
                {
                    obj = query();
                    HttpContext.Current.Cache.Insert(User.Current.Email + cacheKey, obj,
                            null, DateTime.Now.AddMinutes(durationInMinutes), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Default, null);
                }
                return (T)obj;
            }
        }
    }//end: DataAccessSecurity
}//end: namespace
