﻿using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;

namespace AbsenceSoft.Data
{
    /// <summary>
    /// Represents a core value that can be overridden by a user, used for easier
    /// tracking of audit information, who-dunnit and what the system value
    /// should be before and after the override, as well as a calculated
    /// Value property which returns the override value, if set, otherwise the
    /// underlying system value.
    /// </summary>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    [Serializable]
    public class UserOverrideableValue<TValue> : BaseNonEntity, IEquatable<TValue>, IComparable<TValue> where TValue : struct, IComparable<TValue>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserOverrideableValue{TValue}"/> class.
        /// </summary>
        public UserOverrideableValue() : base()
        {
            SystemValue = default(TValue);
            OverrideValue = default(TValue);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserOverrideableValue{TValue}"/> class with
        /// an an initial system value.
        /// </summary>
        /// <param name="systemValue">The system value.</param>
        public UserOverrideableValue(TValue systemValue) : base()
        {
            SystemValue = systemValue;
            OverrideValue = default(TValue);
        }

        /// <summary>
        /// Gets or sets the system value.
        /// </summary>
        /// <value>
        /// The system value.
        /// </value>
        [BsonIgnoreIfNull, BsonIgnoreIfDefault]
        public TValue SystemValue { get; set; }

        /// <summary>
        /// Gets or sets the override value.
        /// </summary>
        /// <value>
        /// The override value.
        /// </value>
        [BsonIgnoreIfNull, BsonIgnoreIfDefault]
        public TValue OverrideValue { get; set; }

        /// <summary>
        /// Gets the user override value if provided, otherwise returns the system value.
        /// </summary>
        /// <value>
        /// The real/actual value to be used by anything.
        /// </value>
        public TValue Value
        {
            get
            {
                if (object.Equals(OverrideValue, default(TValue)))
                    return SystemValue;
                return OverrideValue;
            }
        }

        [BsonIgnore]
        public bool HasOverride
        {
            get
            {
                return !object.Equals(OverrideValue, default(TValue));
            }
        }

        /// <summary>
        /// Gets or sets the user identifier that overrode the value.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [BsonIgnoreIfNull]
        [BsonRepresentation(BsonType.ObjectId)]
        public string OverrideById { get; set; }

        /// <summary>
        /// Gets or sets the override by.
        /// </summary>
        /// <value>
        /// The override by.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public User OverrideBy
        {
            get { return base.GetReferenceValue<User>(this.OverrideById); }
            set { this.OverrideById = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the override date.
        /// </summary>
        /// <value>
        /// The override date.
        /// </value>
        [BsonIgnoreIfNull]
        public DateTime? OverrideDate { get; set; }

        /// <summary>
        /// Sets the user override value and associated information.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public UserOverrideableValue<TValue> SetOverride(TValue value, User user)
        {
            if (Object.Equals(this.OverrideValue, value))
                return this;

            this.OverrideBy = user ?? User.System;
            this.OverrideDate = DateTime.UtcNow;
            this.OverrideValue = value;

            return this;
        }

        /// <summary>
        /// Clears the override.
        /// </summary>
        /// <returns></returns>
        public UserOverrideableValue<TValue> ClearOverride()
        {
            this.OverrideBy = null;
            this.OverrideById = null;
            this.OverrideValue = default(TValue);
            this.OverrideDate = null;

            return this;
        }

        #region IEquatable<TValue>, IComparable<TValue>, Operators

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            if (this.Value as object == null)
                return 0;
            return this.Value.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return object.Equals(this.Value, obj);
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.
        /// </returns>
        /// <exception cref="NotImplementedException"></exception>
        public bool Equals(TValue other)
        {
            return object.Equals(this.Value, other);
        }

        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared. The return value has the following meanings: Value Meaning Less than zero This object is less than the <paramref name="other" /> parameter.Zero This object is equal to <paramref name="other" />. Greater than zero This object is greater than <paramref name="other" />.
        /// </returns>
        /// <exception cref="NotImplementedException"></exception>
        public int CompareTo(TValue other)
        {
            if (object.Equals(this.Value, other))
                return 0;
            if (this.Value as object == null)
                return -1;
            return this.Value.CompareTo(other);
        }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="obj1">The obj1.</param>
        /// <param name="obj2">The obj2.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator ==(TValue obj1, UserOverrideableValue<TValue> obj2)
        {
            return object.Equals(obj1, obj2 == null ? null : (object)obj2.Value);
        }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="obj1">The obj1.</param>
        /// <param name="obj2">The obj2.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator !=(TValue obj1, UserOverrideableValue<TValue> obj2)
        {
            return !object.Equals(obj1, obj2 == null ? null : (object)obj2.Value);
        }

        /// <summary>
        /// Implements the operator &gt;.
        /// </summary>
        /// <param name="obj1">The obj1.</param>
        /// <param name="obj2">The obj2.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator >(TValue obj1, UserOverrideableValue<TValue> obj2)
        {
            if (object.Equals(obj1, obj2 == null ? null : (object)obj2.Value))
                return false;
            if (obj2 == null)
                return true;
            return obj1.CompareTo(obj2.Value) > 0;
        }

        /// <summary>
        /// Implements the operator &gt;=.
        /// </summary>
        /// <param name="obj1">The obj1.</param>
        /// <param name="obj2">The obj2.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator >=(TValue obj1, UserOverrideableValue<TValue> obj2)
        {
            if (object.Equals(obj1, obj2 == null ? null : (object)obj2.Value))
                return true;
            if (obj2 == null)
                return true;
            return obj1.CompareTo(obj2.Value) > 0;
        }

        /// <summary>
        /// Implements the operator &lt;.
        /// </summary>
        /// <param name="obj1">The obj1.</param>
        /// <param name="obj2">The obj2.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator <(TValue obj1, UserOverrideableValue<TValue> obj2)
        {
            if (object.Equals(obj1, obj2 == null ? null : (object)obj2.Value))
                return false;
            if (obj2 == null)
                return false;
            return obj1.CompareTo(obj2.Value) < 0;
        }

        /// <summary>
        /// Implements the operator &lt;=.
        /// </summary>
        /// <param name="obj1">The obj1.</param>
        /// <param name="obj2">The obj2.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator <=(TValue obj1, UserOverrideableValue<TValue> obj2)
        {
            if (object.Equals(obj1, obj2 == null ? null : (object)obj2.Value))
                return true;
            if (obj2 == null)
                return false;
            return obj1.CompareTo(obj2.Value) < 0;
        }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="obj1">The obj1.</param>
        /// <param name="obj2">The obj2.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator ==(UserOverrideableValue<TValue> obj1, TValue obj2)
        {
            return object.Equals(obj2, obj1 == null ? null : (object)obj1.Value);
        }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="obj1">The obj1.</param>
        /// <param name="obj2">The obj2.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator !=(UserOverrideableValue<TValue> obj1, TValue obj2)
        {
            return !object.Equals(obj2, obj1 == null ? null : (object)obj1.Value);
        }

        /// <summary>
        /// Implements the operator &gt;.
        /// </summary>
        /// <param name="obj1">The obj1.</param>
        /// <param name="obj2">The obj2.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator >(UserOverrideableValue<TValue> obj2, TValue obj1)
        {
            if (object.Equals(obj1, obj2 == null ? null : (object)obj2.Value))
                return false;
            if (obj2 == null)
                return true;
            return obj1.CompareTo(obj2.Value) > 0;
        }

        /// <summary>
        /// Implements the operator &gt;=.
        /// </summary>
        /// <param name="obj1">The obj1.</param>
        /// <param name="obj2">The obj2.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator >=(UserOverrideableValue<TValue> obj2, TValue obj1)
        {
            if (object.Equals(obj1, obj2 == null ? null : (object)obj2.Value))
                return true;
            if (obj2 == null)
                return true;
            return obj1.CompareTo(obj2.Value) > 0;
        }

        /// <summary>
        /// Implements the operator &lt;.
        /// </summary>
        /// <param name="obj1">The obj1.</param>
        /// <param name="obj2">The obj2.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator <(UserOverrideableValue<TValue> obj2, TValue obj1)
        {
            if (object.Equals(obj1, obj2 == null ? null : (object)obj2.Value))
                return false;
            if (obj2 == null)
                return false;
            return obj1.CompareTo(obj2.Value) < 0;
        }

        /// <summary>
        /// Implements the operator &lt;=.
        /// </summary>
        /// <param name="obj1">The obj1.</param>
        /// <param name="obj2">The obj2.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator <=(UserOverrideableValue<TValue> obj2, TValue obj1)
        {
            if (object.Equals(obj1, obj2 == null ? null : (object)obj2.Value))
                return true;
            if (obj2 == null)
                return false;
            return obj1.CompareTo(obj2.Value) < 0;
        }

        #endregion
    }
}
