﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Questions;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Workflows;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbsenceSoft.Data.ToDo
{
    /// <summary>
    /// ToDo Item Entity
    /// </summary>
    /// <seealso cref="AbsenceSoft.Data.BaseEmployeeEntity{AbsenceSoft.Data.ToDo.ToDoItem}" />
    [Serializable]
    public class ToDoItem : BaseEmployeeEntity<ToDoItem>
    {
        /// <summary>
        /// Constant representing the value used for all expression trees that access the ToDoItem object
        /// when evaluating rules, tokenizing communications, etc.
        /// </summary>
        public const string EXPRESSION_INSTANCE_NAME = "wfi";

        /// <summary>
        /// Initializes a new instance of the <see cref="ToDoItem"/> class.
        /// </summary>
        public ToDoItem()
        {
            ItemType = ToDoItemType.Manual;
            Status = ToDoItemStatus.Pending;
            Priority = ToDoItemPriority.Normal;
        }

        /// <summary>
        /// Gets or sets the related case Id for the todo item/task.
        /// </summary>
        /// <value>
        /// The case identifier.
        /// </value>
        [BsonRepresentation(BsonType.ObjectId)]
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the related workflow instance id for the todo item/task.
        /// </summary>
        /// <value>
        /// The workflow instance identifier.
        /// </value>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string WorkflowInstanceId { get; set; }

        /// <summary>
        /// Gets or sets the case number for the todo item/task.
        /// </summary>
        /// <value>
        /// The case number.
        /// </value>
        public string CaseNumber { get; set; }

        /// <summary>
        /// Gets or sets a copy of the employee name that the todo item/task relates to.
        /// </summary>
        /// <value>
        /// The name of the employee.
        /// </value>
        [BsonIgnoreIfNull]
        public string EmployeeName { get; set; }

        /// <summary>
        /// Gets or sets a copy of the employer name that the todo item/task belongs to.
        /// </summary>
        /// <value>
        /// The name of the employer.
        /// </value>
        [BsonIgnoreIfNull]
        public string EmployerName { get; set; }

        /// <summary>
        /// Gets or sets the todo item's type.
        /// </summary>
        /// <value>
        /// The type of the item.
        /// </value>
        [BsonRequired]
        public ToDoItemType ItemType { get; set; }

        /// <summary>
        /// Gets or sets the task title
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the user Id of the person it's assigned to.
        /// </summary>
        /// <value>
        /// The assigned to identifier.
        /// </value>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string AssignedToId { get; set; }

        /// <summary>
        /// Gets or sets a copy of the assigned to user name that the todo item/task relates to.
        /// </summary>
        /// <value>
        /// The name of the assigned to.
        /// </value>
        [BsonIgnoreIfNull]
        public string AssignedToName { get; set; }


        /// <summary>
        /// Gets or sets TO-DO cancel reason.
        /// </summary>
        /// <value>
        /// The Cancel Reason.
        /// </value>
        [BsonIgnoreIfNull]
        public string CancelReason { get; set; }

        /// <summary>
        /// Gets or sets the todo item status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        [BsonRequired]
        public ToDoItemStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the todo item priority.
        /// </summary>
        /// <value>
        /// The priority.
        /// </value>
        [BsonRequired]
        public ToDoItemPriority Priority { get; set; }

        /// <summary>
        /// Gets or sets the todo item's due date.
        /// </summary>
        /// <value>
        /// The due date.
        /// </value>
        [BsonRequired]
        public DateTime DueDate { get; set; }

        /// <summary>
        /// Gets or sets the original due date.
        /// </summary>
        /// <value>
        /// The original due date.
        /// </value>
        [BsonIgnoreIfNull]
        public DateTime? OriginalDueDate { get; set; }
        
        /// <summary>
        /// Gets or sets a plain text reason for a due date change.
        /// </summary>
        /// <value>
        /// The due date change reason.
        /// </value>
        [BsonIgnoreIfNull]
        public string DueDateChangeReason { get; set; }

        /// <summary>
        /// A weight assigned to the task from 0 to 1 that represents the relative weight/important
        /// or order of completion this task carries to other tasks of the same priority.
        /// </summary>
        /// <value>
        /// The weight.
        /// </value>
        public decimal Weight { get; set; }

        /// <summary>
        /// A text field that can be used in the expression evaluator for determining
        /// the next step
        /// </summary>
        /// <value>
        /// The result text.
        /// </value>
        [BsonIgnoreIfNull]
        public string ResultText { get; set; }

        /// <summary>
        /// Gets or sets a value that determines whether or not the todo item can
        /// be canceled by the user.
        /// </summary>
        /// <value>
        ///   <c>true</c> if optional; otherwise, <c>false</c>.
        /// </value>
        [BsonDefaultValue(false)]
        public bool Optional { get; set; }

        /// <summary>
        /// Gets or sets a value that if set will hide this ToDo item from the UI
        /// until a specified date, or the Due Date, whichever is earlier. If <c>null</c> then
        /// the ToDo item will be visible and workable the moment it is created.
        /// </summary>
        /// <value>
        /// The hide until.
        /// </value>
        [BsonIgnoreIfNull]
        public DateTime? HideUntil { get; set; }

        /// <summary>
        /// Gets or sets the todo item's assigned user.
        /// </summary>
        /// <value>
        /// The assigned to.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public User AssignedTo
        {
            get { return AssignedToId == User.DefaultUserId ? User.System : GetReferenceValue<User>(AssignedToId); }
            set { AssignedToId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the todo item's case. Assignes the case id and case number.
        /// </summary>
        /// <value>
        /// The case.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public Case Case
        {
            get { return GetReferenceValue<Case>(CaseId); }
            set { CaseId = SetReferenceValue(value); CaseNumber = value != null ? value.CaseNumber : CaseNumber; }
        }

        /// <summary>
        /// Gets or sets the workflow instance this ToDoItem is tied to.
        /// </summary>
        /// <value>
        /// The workflow instance.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public WorkflowInstance WorkflowInstance
        {
            get { return GetReferenceValue<WorkflowInstance>(WorkflowInstanceId); }
            set { WorkflowInstanceId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets a value for PaperworkDueDate
        /// </summary>
        [BsonIgnoreIfNull]
        public DateTime? PaperworkDueDate { get; set; }


        /// <summary>
        /// Gets or sets a value for Question Configuration 
        /// </summary>
        [BsonIgnoreIfNull]
        public Question Question { get; set; }

        /// <summary>
        /// Called before saving
        /// </summary>
        /// <returns>
        /// A value indicating whether or not the save should proceed (not canceled).
        /// </returns>
        protected override bool OnSaving()
        {
            if (AssignedTo != null)
            {
                AssignedToName = AssignedTo.DisplayName;
            }
                
            if ((Status == ToDoItemStatus.Pending || Status == ToDoItemStatus.Open) && DueDate.Date.AddDays(1) < DateTime.UtcNow.Date)
            {
                Status = ToDoItemStatus.Overdue;
            }
                
            if (string.IsNullOrWhiteSpace(EmployerName) && Employer != null)
            {
                EmployerName = Employer.Name;
            }
                
            return base.OnSaving();
        }
    }
}
