﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Customers
{
    [Serializable]
    public class EmployeeModel
    {
        public EmployeeModel()
        {
            //Status = EmploymentStatus.Active;
            //Info = new EmployeeInfo();
            //Job = new JobInfo();
            //WorkSchedules = new List<Schedule>();
            //MilitaryStatus = Enums.MilitaryStatus.Civilian;
            //PriorHours = new List<PriorHours>();
        }

        [BsonIgnoreIfNull]
        public string Id { get; set; }
        [BsonIgnoreIfNull]
        public string Email { get; set; }
        [BsonIgnoreIfNull]
        public string Address1 { get; set; }
        [BsonIgnoreIfNull]
        public string Address2 { get; set; }
        [BsonIgnoreIfNull]
        public string City { get; set; }
        [BsonIgnoreIfNull]
        public string State { get; set; }
        [BsonIgnoreIfNull]
        public string PostalCode { get; set; }
        [BsonIgnoreIfNull]
        public string Country { get; set; }
        [BsonIgnoreIfNull]
        public string WorkPhone { get; set; }
        [BsonIgnoreIfNull]
        public string HomePhone { get; set; }
        [BsonIgnoreIfNull]
        public string CellPhone { get; set; }
        [BsonIgnoreIfNull]
        public string OfficeLocation { get; set; }

        //[BsonIgnoreIfNull]
        //public string EmployeeNumber { get; set; }
        //[BsonIgnoreIfNull]
        //public EmployeeInfo Info { get; set; }
        //[BsonIgnoreIfNull]
        //public JobInfo Job { get; set; }
        //[BsonIgnoreIfNull]
        //public string FirstName { get; set; }
        //[BsonIgnoreIfNull]
        //public string MiddleName { get; set; }
        //[BsonIgnoreIfNull]
        //public string LastName { get; set; }
        //[BsonIgnoreIfNull]
        //public Gender? Gender { get; set; }
        //[BsonIgnoreIfNull]
        //public DateTime? DoB { get; set; }
        //[BsonIgnoreIfNull]
        //public CryptoString Ssn { get; set; }
        //[BsonIgnoreIfNull]
        //public EmploymentStatus? Status { get; set; }
        //[BsonIgnoreIfNull]
        //public double? Salary { get; set; }
        //[BsonIgnoreIfNull]
        //public PayType? PayType { get; set; }
        //[BsonIgnoreIfNull]
        //public WorkType? WorkType { get; set; }
        //[BsonIgnoreIfNull]
        //public string JobTitle { get; set; }
        //[BsonIgnoreIfNull]
        //public List<PriorHours> PriorHours { get; set; }
        //[BsonIgnoreIfNull]
        //[BsonDateTimeOptions(DateOnly = true)]
        //[BsonIgnoreIfNull]
        //public DateTime? ServiceDate { get; set; }
        //[BsonIgnoreIfNull]
        //[BsonDateTimeOptions(DateOnly = true)]
        //public DateTime? HireDate { get; set; }
        //[BsonIgnoreIfNull]
        //[BsonDateTimeOptions(DateOnly = true)]
        //public DateTime? RehireDate { get; set; }
        //[BsonIgnoreIfNull]
        //[BsonDateTimeOptions(DateOnly = true)]
        //public DateTime? TerminationDate { get; set; }
        //[BsonIgnoreIfNull]
        //public string WorkState { get; set; }
        //[BsonIgnoreIfNull]
        //public string WorkCountry { get; set; }
        //[BsonIgnoreIfNull]
        //public List<Schedule> WorkSchedules { get; set; }
        //[BsonIgnoreIfNull]
        //public string PayscheduleId { get; set; }
        //[BsonIgnoreIfNull]
        //public bool? Meets50In75MileRule { get; set; }
        //[BsonIgnoreIfNull]
        //public bool? IsKeyEmployee { get; set; }
        //[BsonIgnoreIfNull]
        //public bool? IsExempt { get; set; }
        //[BsonIgnoreIfNull]
        //public string SpouseEmployeeNumber { get; set; }
        //[BsonIgnoreIfNull]
        //public MilitaryStatus? MilitaryStatus { get; set; }
        //[BsonIgnoreIfNull]
        //public List<EmployeeCustomField> CustomFields { get; set; }
        //[BsonIgnoreIfNull]
        //public string EmployerName { get; set; }
        //[BsonIgnoreIfNull]
        //public Employee SpouseEmployee { get; set; }
        //[BsonIgnoreIfNull]
        //public PaySchedule Payschedule { get; set; }        
    }
}
