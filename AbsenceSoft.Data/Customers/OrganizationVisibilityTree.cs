﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Customers
{
    [Serializable]
    public class OrganizationVisibilityTree
    {
        public OrganizationVisibilityTree()
        {
            ChildOrganizationVisibilityTree = new List<OrganizationVisibilityTree>();
        }
        public OrganizationVisibilityTree(Organization organization, string affiliation, int level)
            : this()
        {
            Organization = organization;
            Affiliation = affiliation;
            Level = level;
            OrganizationName = organization.Name;
            OrganizationCode = organization.Code;
            OrganizationDescription = organization.Description;
            OrganizationPath = organization.Path.Path;
            OrganizationTypeCode = organization.TypeCode;
            EmployerId = organization.EmployerId;
            SicCode = organization.SicCode;
            NaicsCode = organization.NaicsCode;
            OrganizationAddress1 = organization.Address.Address1;
            OrganizationAddress2 = organization.Address.Address2;
            OrganizationState = organization.Address.State;
            OrganizationCity = organization.Address.City;
            OrganizationPostalCode = organization.Address.PostalCode;
            OrganizationCountry = organization.Address.Country;
            Id = organization.Id;
            Expanded = false;
        }
        private Organization Organization { get; set; }
        public int Level { get; set; }
        public List<OrganizationVisibilityTree> ChildOrganizationVisibilityTree { get; set; }
        
        public string OrganizationCode { get; set; }
        
        public string OrganizationName { get; set; }

        public string OrganizationDescription { get; set; }
        
        public string OrganizationPath { get; set; }
        
        public string OrganizationTypeCode { get; set; }
        
        public string EmployerId { get; set; }

        public string Id { get; set; }

        public string Affiliation { get; set; }

        public bool Expanded { get; set; }

        /// <summary>
        /// Gets or sets the sic code.
        /// </summary>
        /// <value>
        /// The sic code.
        /// </value>
        public string SicCode { get; set; }

        /// <summary>
        /// Gets or sets the naics code.
        /// </summary>
        /// <value>
        /// The naics code.
        /// </value>
        public string NaicsCode { get; set; }

        /// <summary>
        /// Gets or sets the organization address1.
        /// </summary>
        /// <value>
        /// The organization address1.
        /// </value>
        public string OrganizationAddress1 { get; set; }

        /// <summary>
        /// Gets or sets the organization address2.
        /// </summary>
        /// <value>
        /// The organization address2.
        /// </value>
        public string OrganizationAddress2 { get; set; }

        /// <summary>
        /// Gets or sets the state of the organization.
        /// </summary>
        /// <value>
        /// The state of the organization.
        /// </value>
        public string OrganizationState { get; set; }

        /// <summary>
        /// Gets or sets the organization city.
        /// </summary>
        /// <value>
        /// The organization city.
        /// </value>
        public string OrganizationCity { get; set; }

        /// <summary>
        /// Gets or sets the organization postal code.
        /// </summary>
        /// <value>
        /// The organization postal code.
        /// </value>
        public string OrganizationPostalCode { get; set; }

        /// <summary>
        /// Gets or sets the organization country.
        /// </summary>
        /// <value>
        /// The organization country.
        /// </value>
        public string OrganizationCountry { get; set; }
    
        public string AffiliationInitials 
        {
            get
            {
                if (Affiliation.Trim() != string.Empty && Affiliation.Trim() != "-1")
                {
                    return "(" + Enum.GetName(typeof(OrganizationAffiliation), int.Parse(Affiliation))[0].ToString().ToUpper() + ")";
                }
                else
                    return string.Empty;
            }       
        }

        public void CreateChildOrganizationTree(List<Organization> organizations)
        {
            foreach (Organization org in organizations)
            {
                if (org.Path.GetParentPath().Path == Organization.Path.Path)
                {
                    ChildOrganizationVisibilityTree.Add(new OrganizationVisibilityTree(org, string.Empty, Level + 1));
                }
            }
            ChildOrganizationVisibilityTree = ChildOrganizationVisibilityTree.OrderBy(tree => tree.Organization.Name).ToList();
            ChildOrganizationVisibilityTree.ForEach(orgTree => orgTree.CreateChildOrganizationTree(organizations));       
        }
        public void AssignVisibility(List<EmployeeOrganization> empOrgs)
        {
            EmployeeOrganization empOrg = empOrgs.FirstOrDefault(org => org.Path == Organization.Path 
                && org.EmployerId == Organization.EmployerId && org.CustomerId == Organization.CustomerId
                && org.Code == Organization.Code && org.TypeCode == Organization.TypeCode);
            if (empOrg != null)            
                this.Affiliation = Convert.ToString((int)empOrg.Affiliation);
            else
                this.Affiliation = string.Empty;
            ChildOrganizationVisibilityTree.ForEach(orgTree => orgTree.AssignVisibility(empOrgs));       
        }

        public bool Expand(string searchTerm)
        {
            foreach (OrganizationVisibilityTree tree in ChildOrganizationVisibilityTree)
            {
                if (tree.OrganizationCode.ToLower().Contains(searchTerm.ToLower()))
                    Expanded = true;
                if (tree.Expand(searchTerm))
                    Expanded = true;
            }
            return Expanded;
        }

        public void UpdateUserOrganization(Employee emp, MongoDB.Driver.BulkWriteOperation<EmployeeOrganization> bwo, AbsenceSoft.Data.Security.User currentUser)
        {

            if (Affiliation == "-1")
            {
                bwo.Find(EmployeeOrganization.Query.And(
                    EmployeeOrganization.Query.EQ(org => org.CustomerId, currentUser.CustomerId),
                    EmployeeOrganization.Query.EQ(org => org.EmployerId, EmployerId),
                    EmployeeOrganization.Query.EQ(org => org.EmployeeNumber, emp.EmployeeNumber),
                    EmployeeOrganization.Query.EQ(org => org.Code, OrganizationCode),
                    EmployeeOrganization.Query.EQ(org => org.TypeCode, OrganizationTypeCode)
                )).Update(EmployeeOrganization.Updates
                    .Set(e => e.IsDeleted, true)
                    .Set(e => e.ModifiedById, currentUser.Id)
                    .CurrentDate(e => e.ModifiedDate));
            }
            else if (Affiliation.Trim() != string.Empty)
            {
                OrganizationAffiliation affiliation = (OrganizationAffiliation)(int.Parse(Affiliation));
                var update = EmployeeOrganization.Updates
                            .SetOnInsert(org => org.EmployerId, EmployerId)
                            .SetOnInsert(org => org.CustomerId, currentUser.CustomerId)
                            .SetOnInsert(org => org.CreatedById, currentUser.Id)
                            .SetOnInsert(org => org.CreatedDate, DateTime.UtcNow)
                            .SetOnInsert(org => org.EmployeeNumber, emp.EmployeeNumber)
                            .SetOnInsert(org => org.Code, OrganizationCode)
                            .SetOnInsert(org => org.Name, OrganizationName)
                            .SetOnInsert(org => org.Path, AbsenceSoft.Common.HierarchyPath.Parse(OrganizationPath))
                            .SetOnInsert(org => org.TypeCode, OrganizationTypeCode)
                            .Set(org => org.Affiliation, affiliation)
                            .Set(org => org.ModifiedById, currentUser.Id)
                            .Set(org => org.IsDeleted, false)
                            .CurrentDate(r => r.ModifiedDate);
                          bwo.Find(EmployeeOrganization.Query.And(
                            EmployeeOrganization.Query.EQ(org => org.CustomerId, currentUser.CustomerId),
                            EmployeeOrganization.Query.EQ(org => org.EmployerId, EmployerId),
                            EmployeeOrganization.Query.EQ(org => org.EmployeeNumber, emp.EmployeeNumber),
                            EmployeeOrganization.Query.EQ(org => org.Code, OrganizationCode),
                            EmployeeOrganization.Query.EQ(org => org.TypeCode, OrganizationTypeCode)
                        ))
                        .Upsert()
                        .UpdateOne(update);
            }

            if (ChildOrganizationVisibilityTree != null && ChildOrganizationVisibilityTree.Count > 0)
            {
                foreach (OrganizationVisibilityTree orgTree in ChildOrganizationVisibilityTree)
                {
                    orgTree.UpdateUserOrganization(emp, bwo, currentUser);
                }
            }
        }
    }
}
