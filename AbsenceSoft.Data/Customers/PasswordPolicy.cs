﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AbsenceSoft;

namespace AbsenceSoft.Data.Customers
{
    [Serializable]
    public class PasswordPolicy
    {

        public const int MinimumLengthMin = 8;
        public const int MinimumLengthMax = 255;

        public static PasswordPolicy Default()
        {
            return new PasswordPolicy
            {
                ExpirationDays = 90,
                MinimumLength = 8
            };
        }

        [Range(0, int.MaxValue)]
        public int ExpirationDays { get; set; }

        [Range(MinimumLengthMin, MinimumLengthMax)]
        public int MinimumLength { get; set; }

        [Range(0, int.MaxValue)]
        public int ReuseLimitDays { get; set; }

        [Range(0, int.MaxValue)]
        public int ReuseLimitCount { get; set; }

        public PasswordPolicy Clamp()
        {
            ExpirationDays = ExpirationDays.Clamp(0, int.MaxValue);
            MinimumLength = MinimumLength.Clamp(MinimumLengthMin, MinimumLengthMax);
            return this;
        }
    }
}