﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Linq;

namespace AbsenceSoft.Data.Customers
{
    /// <summary>
    /// Represents an employee organization membership to which an employee may belong. Membership
    /// is optionally controlled/boxed by dates.
    /// </summary>
    [Serializable]
    public class EmployeeOrganization : BaseEmployerEntity<EmployeeOrganization>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeOrganization"/> class.
        /// </summary>
        public EmployeeOrganization() : base()
        {
            Affiliation = OrganizationAffiliation.Member;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeOrganization"/> class.
        /// </summary>
        /// <param name="org">The organization to initialize this employee organization from.</param>
        /// <param name="affiliation">The affiliation the employee has to this organization.</param>
        public EmployeeOrganization(Organization org, OrganizationAffiliation? affiliation = null) : this()
        {
            if (org != null)
            {
                Code = org.Code;
                Name = org.Name;
                Path = org.Path;
                TypeCode = org.TypeCode;
                CustomerId = org.CustomerId;
                EmployerId = org.EmployerId;
            }
            if (affiliation.HasValue)
                Affiliation = affiliation.Value;
        }

        /// <summary>
        /// Gets or sets the employee number.
        /// </summary>
        /// <value>
        /// The employee number.
        /// </value>
        [BsonRequired]
        public string EmployeeNumber { get; set; }

        /// <summary>
        /// Gets or sets the unique organization code.
        /// </summary>
        /// <value>
        /// The org code.
        /// </value>
        [BsonRequired]
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the full organization path (including any parent organizations).
        /// This is a long-term cached field but also will be used for heirarchical queries.
        /// </summary>
        /// <value>
        /// The org path.
        /// </value>
        [BsonRequired]
        public HierarchyPath Path { get; set; }

        /// <summary>
        /// Gets or sets the affiliation.
        /// </summary>
        /// <value>
        /// The affiliation.
        /// </value>
        [DefaultValue(OrganizationAffiliation.Member), BsonDefaultValue(OrganizationAffiliation.Member)]
        public OrganizationAffiliation Affiliation { get; set; }

        /// <summary>
        /// Gets or sets the name of the organization. This is a long-term cached field.
        /// </summary>
        /// <value>
        /// The name of the org.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the dates the employee belongs to the organization.
        /// </summary>
        /// <value>
        /// The dates.
        /// </value>
        /// 
        [BsonIgnoreIfNull]
        public DateRange Dates { get; set; }

        /// <summary>
        /// Gets or sets the type code for the organization.
        /// </summary>
        /// <value>
        /// The type code.
        /// </value>
        public string TypeCode { get; set; }

        /// <summary>
        /// Gets or sets the employee by reference using the Employee Number.
        /// </summary>
        /// <value>
        /// The employee.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public Employee Employee
        {
            get
            {
                return GetReferenceValue<Employee>(EmployeeNumber, 
                    s => s.EmployeeNumber, 
                    s => Employee.AsQueryable().Where(e => e.CustomerId == CustomerId && e.EmployerId == EmployerId && e.EmployeeNumber == s).FirstOrDefault());
            }
            set
            {
                EmployeeNumber = SetReferenceValue<Employee>(value, e => e.EmployeeNumber);
            }
        }

        /// <summary>
        /// Gets or sets the organization by reference using the Organization Code.
        /// </summary>
        /// <value>
        /// The organization.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public Organization Organization
        {
            get
            {
                return GetReferenceValue<Organization>(Code, s => s.Code, s => Organization.GetByCode(s, CustomerId, EmployerId));
            }
            set
            {
                Code = SetReferenceValue<Organization>(value, e => e.Code);
                // Lazy fun stuff
                if (value != null)
                {
                    Path = value.Path;
                    Name = value.Name;
                    TypeCode = value.TypeCode;
                }
                else
                {
                    Path = HierarchyPath.Null;
                    Name = null;
                    TypeCode = null;
                }
            }
        }

        /// <summary>
        /// Gets or sets the type of the organization.
        /// </summary>
        /// <value>
        /// The type of the organization.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public OrganizationType OrganizationType
        {
            get { return GetReferenceValue<OrganizationType>(TypeCode, k => k.Code, k => OrganizationType.GetByCode(k, CustomerId, EmployerId)); }
            set { TypeCode = SetReferenceValue(value, k => k.Code); }
        }

        /// <summary>
        /// Updates the Employee Number.
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="newEmployeeNumber"></param>
        public static void UpdateEmployeeNumber(string origEmployeeNumber, string newEmployeeNumber, string modifiedById, string customerId, string employerId)
        {
            Update(EmployeeOrganization.Query.And(Query.EQ(e => e.EmployeeNumber, origEmployeeNumber), Query.EQ(e => e.CustomerId, customerId), Query.EQ(e => e.EmployerId, employerId)), Updates.Set(e => e.EmployeeNumber, newEmployeeNumber).Set(vst => vst.ModifiedDate, DateTime.UtcNow).Set(vst => vst.ModifiedById, modifiedById), MongoDB.Driver.UpdateFlags.Multi);
        }
    }
}
