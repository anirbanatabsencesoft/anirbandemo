﻿using AbsenceSoft.Data.Security;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Customers
{
    [Serializable]
    public class Team: BaseCustomerEntity<Team>
    {

        public string Name { get; set;}
        public string Description { get; set; }

        [BsonIgnore]
        public bool CurrentUserIsTeamLead { get; set; }
    }
}
