﻿using System;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace AbsenceSoft.Data.Customers
{
    /// <summary>
    /// This entity tracks different employer service options.
    /// </summary>
    [Serializable, JsonObject(MemberSerialization.OptOut)]
    [BsonIgnoreExtraElements(false, Inherited = true)]
    public class EmployerServiceOption : BaseEmployerEntity<EmployerServiceOption>
    {
        /// <summary>
        /// The key of the employer service option, which is part of a unique index with CustomerId and EmployerId.
        /// </summary>
        [BsonRequired]
        public string Key { get; set; }

        /// <summary>
        /// The id of the customer service option.
        /// </summary>
        [BsonRequired]
        public string CusomerServiceOptionId { get; set; }

        /// <summary>
        /// Shortcut to retrieve the customer service option.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public CustomerServiceOption CusomerServiceOption
        {
            get { return GetReferenceValue<CustomerServiceOption>(this.CusomerServiceOptionId); }
            set { this.CusomerServiceOptionId = SetReferenceValue(value); }
        }
    }
}
