﻿using AbsenceSoft;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Customers
{
    [Serializable]
    public class EmployeeInfo : BaseNonEntity
    {
        [NonSerialized, BsonIgnore, JsonIgnore]
        private string workPhone;
        [NonSerialized, BsonIgnore, JsonIgnore]
        private string homePhone;
        [NonSerialized, BsonIgnore, JsonIgnore]
        private string cellPhone;
        [NonSerialized, BsonIgnore, JsonIgnore]
        private string altPhone;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeInfo"/> class.
        /// </summary>
        public EmployeeInfo()
        {
            Address = new Address();
            AltAddress = new Address();
        }

        /// <summary>
        /// Gets or sets the employee's email address.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the alternate email address for the employee.
        /// </summary>
        [BsonIgnoreIfNull]
        public string AltEmail { get; set; }

        /// <summary>
        /// Gets or sets the secondary email address for the employee.
        /// </summary>
        [BsonIgnoreIfNull]
        public string SecondaryEmail { get; set; }

        /// <summary>
        /// Gets or sets the employee's address.
        /// </summary>
        public Address Address { get; set; }

        /// <summary>
        /// Gets or sets the employee's address.
        /// </summary>
        public Address AltAddress { get; set; }

        /// <summary>
        /// Gets or sets the employee's work phone.
        /// </summary>
        [BsonIgnoreIfNull]
        public string WorkPhone
        {
            get { return workPhone; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    workPhone = value;
                    return;
                }
                workPhone = Regex.Replace(value, @"[^0-9]", "");
            }
        }

        /// <summary>
        /// Gets or sets the employee's home phone.
        /// </summary>
        [BsonIgnoreIfNull]
        public string HomePhone
        {
            get { return homePhone; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    homePhone = value;
                    return;
                }
                homePhone = Regex.Replace(value, @"[^0-9]", "");
            }
        }

        /// <summary>
        /// Gets or sets the employee's cell phone.
        /// </summary>
        [BsonIgnoreIfNull]
        public string CellPhone
        {
            get { return cellPhone; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    cellPhone = value;
                    return;
                }
                cellPhone = Regex.Replace(value, @"[^0-9]", "");
            }
        }

        /// <summary>
        /// Gets or sets the employee's alternate phone.
        /// </summary>
        [BsonIgnoreIfNull]
        public string AltPhone
        {
            get { return altPhone; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    altPhone = value;
                    return;
                }
                altPhone = Regex.Replace(value, @"[^0-9]", "");
            }
        }

        /// <summary>
        /// Gets or sets the name of, address of, tag for or other indicator for an employee's office location.
        /// </summary>
        /// <remarks>This gets synced on save and should be synced by any other process that creates an EmployeeOrg of OFFICELOCATION.</remarks>
        [BsonIgnoreIfNull]
        public string OfficeLocation { get; set; }
    }
}
