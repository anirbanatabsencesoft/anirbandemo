﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Customers
{
	[Serializable]
    public class ContactType : BaseEmployerOverrideableEntity<ContactType>
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="ContactType"/> class.
        /// </summary>
		public ContactType()
		{
			ContactCategory = ContactTypeDesignationType.None;			
		}

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
		[BsonRequired]
		public string Name { get; set; }

        /// <summary>
        /// Gets or sets the display on communications as.
        /// </summary>
        /// <value>
        /// The display on communications as.
        /// </value>
        [BsonIgnoreIfNull]
        public string DisplayOnCommunicationsAs { get; set; }

        /// <summary>
        /// Gets or sets the contact category.
        /// </summary>
        /// <value>
        /// The contact category.
        /// </value>
		[BsonRequired]
		public ContactTypeDesignationType ContactCategory { get; set; }

        /// <summary>
        /// Gets or sets the work state restrictions.
        /// </summary>
        /// <value>
        /// The work state restrictions.
        /// </value>
		[BsonIgnoreIfNull]
		public List<String> WorkStateRestrictions { get; set; }
	}
}
