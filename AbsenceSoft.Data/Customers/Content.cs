﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Customers
{
    [Serializable]
    [DebuggerDisplay("{Key}, {Language}, {Value}")]
    public class Content : BaseCustomerEntity<Content>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="Value"/> class.
        /// </summary>
        public Content() : base() { }

        /// <summary>
        /// Gets or sets the employer identifier.
        /// </summary>
        /// <value>
        /// The employer identifier.
        /// </value>
        [BsonIgnoreIfNull]
        [BsonRepresentation(BsonType.ObjectId)]
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        [BsonRequired]
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the content value; already HTML encoded.
        /// </summary>
        /// <value>
        /// The content.
        /// </value>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets the culture name in the format "languagecode2-country/regioncode2".
        /// </summary>
        /// <value>
        /// The culture name in the format "languagecode2-country/regioncode2", where languagecode2 is a lowercase two-letter code derived from ISO 639-1 and country/regioncode2 is an uppercase two-letter code derived from ISO 3166.
        /// </value>
        [BsonIgnoreIfNull]
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the employer.
        /// </summary>
        /// <value>
        /// The employer.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public Employer Employer
        {
            get { return GetReferenceValue<Employer>(this.EmployerId); }
            set { this.EmployerId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the culture.
        /// </summary>
        /// <value>
        /// The culture.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public CultureInfo Culture
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Language))
                    return CultureInfo.CurrentUICulture;

                var c = CultureInfo.GetCultureInfo(Language);
                if (c == null)
                    return CultureInfo.CurrentUICulture;

                return c;
            }
            set
            {
                if (value == null)
                    Language = null;
                else
                    Language = value.Name;
            }
        }

        public static IEnumerable<string> GetAllPossibleKeys()
        {
            yield return "Web.SignUp.Account.MatchesDomain";
            yield return "Web.Cases.Create.DescriptionSummaryNote";
            yield return "Web.Cases.Create.CreateCaseButtonNote";
            yield return "Web.Cases.Create.WorkRelated";
            yield return "Web.Cases.Create.ReviewPolicies";
            yield return "Web.Cases.Create.NoPolicies";
            yield return "Web.Cases.Create.ShortTermDisability";
            yield return "Web.Cases.Create.AccommodationRequest";
            yield return "Web.Cases.Create.ContactPreferences";
            yield return "Web.Cases.Create.ContactInformation";
            yield return "Web.Cases.Create.CaseReporter";
            yield return "Web.Cases.Create.Confirmation";
            yield return "Web.Cases.Create.Attachment";
            yield return "Web.Cases.Create.Note";
            yield return "Error";
            yield return "Errors.Users.Unauthorized";
            yield return "Errors.Users.NoAccess";
            yield return "Errors.SSO.NotImplemented";
            yield return "Errors.SSO.NoArtifactResolutionService";
            yield return "Errors.SSO.UnsupportedSAMLBinding";
            yield return "Errors.SSO.NoSigningKey";
            yield return "Errors.SSO.SignatureValidation";
            yield return "Errors.SSO.MissingAssertion";
            yield return "Errors.SSO.MissingNameID";
            yield return "Errors.SSO.AuthFailed";
            yield return "Errors.SSO.SAMLFailed";
            yield return "Errors.SSO.UnknownEmployer";
            yield return "ESS.SelfRegister.Messages.CheckEmail";
            yield return "ESS.SelfRegister.Messages.ResendEmail";
            yield return "ESS.SelfRegister.Messages.ResendToAltEmail";
            yield return "ESS.SelfRegister.Messages.DisabledUser";
            yield return "ESS.SelfRegister.Messages.UserExists";
            yield return "ESS.SelfRegister.Messages.ErrorSendingEmail";
            yield return "ESS.SelfRegister.Messages.EmployeeNotFound";
            yield return "ESS.SelfRegister.Messages.SelfRegistrationDisabled";
            yield return "ESS.SelfRegister.Email.CreateAccount";
            yield return "ESS.SelfRegister.Email.CreateAccount.LinkText";
            yield return "ESS.SelfRegister.Email.UserExistsToAdmin";
            yield return "ESS.SelfRegister.Email.UserExistsToUser";
            yield return "ESS.SelfRegister.Email.NoMatchEmployeeToAdmin";
            yield return "ESS.SelfRegister.Email.EmployeeNotUnique";
        }

        protected override bool OnSaving()
        {
            if(!Content.GetAllPossibleKeys().Contains(this.Key))
                return false;

            return base.OnSaving();
        }
    }
}
