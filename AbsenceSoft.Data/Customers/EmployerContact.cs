﻿using MongoDB.Bson.Serialization.Attributes;
using System.Linq;
using AbsenceSoft.Data.Cases;
using System;

namespace AbsenceSoft.Data.Customers
{
    [Serializable]
    /// <summary>
    /// Represents a contact for that employer
    /// Typically the first point of contact at a company for someone who may be eligible for a leave of absence
    /// </summary>
    public class EmployerContact: BaseEmployerEntity<EmployerContact>, ICaseReporterModel
    {

        public EmployerContact()
        {
            this.Contact = new Contact();
        }

        /// <summary>
        /// Gets or sets the contact details for this person, where applicable.
        /// </summary>
        [BsonRequired]
        public Contact Contact { get; set; }

        /// <summary>
        /// Gets or sets the type of this contact is to the employee.
        /// </summary>
        [BsonRequired]
        public string ContactTypeCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the contact type.
        /// </summary>
        /// <value>
        /// The name of the contact type.
        /// </value>
        [BsonIgnoreIfNull]
        public string ContactTypeName { get; set; }

        /// <summary>
        /// Called before saving
        /// </summary>
        /// <returns>
        /// A value indicating whether or not the save should proceed (not canceled).
        /// </returns>
        protected override bool OnSaving()
        {
            // Ensure contact type code is uppercase only.
            if (!string.IsNullOrWhiteSpace(ContactTypeCode))
                ContactTypeCode = ContactTypeCode.ToUpperInvariant();

            // If there is a contact type code value, but no contact type name value,
            // populate the contact type name property.
            if (!string.IsNullOrWhiteSpace(ContactTypeCode)
                && string.IsNullOrWhiteSpace(ContactTypeName))
            {
                // Grab employer contact type for customer by customer id and contact type code value.
                var employerContactType = EmployerContactType.AsQueryable()
                    .FirstOrDefault(p => p.CustomerId == CustomerId
                        && p.Code == ContactTypeCode);

                // If an instance was found populate the contact type name property.
                if (employerContactType != null)
                    ContactTypeName = employerContactType.Name;
            }

            // If no contact object is present continue with base behavior.
            if (this.Contact == null)
                return base.OnSaving();

            // Update contact specific information.
            this.Contact.Email = string.IsNullOrWhiteSpace(this.Contact.Email)
                ? null
                : this.Contact.Email.ToLowerInvariant();
            this.Contact.AltEmail = string.IsNullOrWhiteSpace(this.Contact.AltEmail)
                ? null
                : this.Contact.AltEmail.ToLowerInvariant();

            // Call base behavior.
            return base.OnSaving();
        }
    }
}
