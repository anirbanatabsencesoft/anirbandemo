﻿using AbsenceSoft.Common;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver.Linq;
using AbsenceSoft.Data.Security;
using System.Text.RegularExpressions;

namespace AbsenceSoft.Data.Customers
{
    /// <summary>
    /// An organization entity that supports hierarchy and general awesomeness. Future home
    /// of other stuff that describes organizations.
    /// </summary>
    [Serializable]
    public class Organization : BaseEmployerEntity<Organization>
    {

        public Organization()
        {
            Address = new Address();
        }
        /// <summary>
        /// Gets or sets the unique identification code for this organization (OU).
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
        [BsonRequired]
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the name of the organization.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [BsonIgnoreIfNull]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [BsonIgnoreIfNull]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the code path to include the parent organizations to this one.
        /// </summary>
        /// <value>
        /// The code path.
        /// </value>
        [BsonRequired]
        public HierarchyPath Path { get; set; }

        /// <summary>
        /// Gets or sets the type code.
        /// </summary>
        /// <value>
        /// The type code.
        /// </value>
        public string TypeCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the type.
        /// </summary>
        /// <value>
        /// The name of the type.
        /// </value>
        [BsonIgnoreIfNull]
        public string TypeName { get; set; }

        /// <summary>
        /// Gets or sets the sic code.
        /// </summary>
        /// <value>
        /// The sic code.
        /// </value>
        [BsonIgnoreIfNull]
        public string SicCode { get; set; }

        /// <summary>
        /// Gets or sets the naics code.
        /// </summary>
        /// <value>
        /// The naics code.
        /// </value>
        [BsonIgnoreIfNull]
        public string NaicsCode { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        /// <value>
        /// The address.
        /// </value>
        [BsonIgnoreIfNull]
        public Address Address { get; set; }

        /// <summary>
        /// Gets or sets the Osha establishment identifier.
        /// </summary>
        /// <value>
        /// The Osha establishment identifier.
        /// </value>
        [BsonIgnoreIfNull]
        public int? OshaEstablishmentId { get; set; }

        /// <summary>
        /// Gets or sets the type of the organization.
        /// </summary>
        /// <value>
        /// The type of the organization.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public OrganizationType OrganizationType
        {
            get { return GetReferenceValue<OrganizationType>(TypeCode, k => k.Code, k => OrganizationType.GetByCode(k, CustomerId, EmployerId)); }
            set
            {
                TypeCode = SetReferenceValue(value, k => k.Code);
                if (value != null)
                    TypeName = value.Name;
                else
                    TypeName = null;
            }
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return (string.IsNullOrWhiteSpace(Name) ? Code : string.Format("{0} ({1})", Name, Code)) ?? base.ToString();
        }

        /// <summary>
        /// Gets all of the descendants of this organization.
        /// </summary>
        /// <returns></returns>
        public List<Organization> GetAllDescendants()
        {
            return GetDescendants(null);
        }

        /// <summary>
        /// Gets all descendant organizations for a certain number of levels deep from this organization. If
        /// level is <c>null</c> (default) then returns all.
        /// </summary>
        /// <returns></returns>
        public List<Organization> GetDescendants(int? level = null)
        {
            var query = from o in Organization.AsQueryable()
                        where o.CustomerId == CustomerId
                        where o.EmployerId == EmployerId
                        where Path.GetDescendants(() => o.Path, level).Inject()
                        select o;
            return query.ToList();
        }

        /// <summary>
        /// Gets the ancestors of this organization.
        /// </summary>
        /// <returns></returns>
        public List<Organization> GetAncestors(int? minLevel = null)
        {
            List<string> ancestors = Path.GetAncestors(false, minLevel).Select(a => a.ToString()).ToList();
            return Organization.AsQueryable()
                .Where(o => o.CustomerId == CustomerId)
                .Where(o => o.EmployerId == EmployerId)
                .Where(o => ancestors.Contains(o.Path))
                .ToList();
        }

        /// <summary>
        /// Gets the organization by its code.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <returns></returns>
        public static Organization GetByCode(string code, string customerId, string employerId)
        {
            if (string.IsNullOrWhiteSpace(code))
                return null;

            string c = Regex.Escape(code.ToUpperInvariant());

            Organization val = Query.Find(
                Query.And(
                    Query.Matches(r => r.Code, new BsonRegularExpression(c, "i")),
                    Query.Or(
                        Query.EQ(r => r.CustomerId, customerId),
                        Query.EQ(r => r.CustomerId, null),
                        Query.NotExists(r => r.CustomerId)
                        ),
                    Query.Or(
                        Query.EQ(r => r.EmployerId, employerId),
                        Query.EQ(r => r.EmployerId, null),
                        Query.NotExists(r => r.EmployerId)
                        )
                )).ToList().OrderBy(r =>
                {
                    if (!string.IsNullOrWhiteSpace(r.EmployerId))
                        return 1;
                    if (!string.IsNullOrWhiteSpace(r.CustomerId))
                        return 2;
                    return 3;
                }).FirstOrDefault();

            if (val == null)
                return val;
            if (User.Current == null)
                return val;
            if (string.IsNullOrWhiteSpace(val.CustomerId))
                return val;
            if (val.CustomerId != User.Current.CustomerId)
                return null;
            if (string.IsNullOrWhiteSpace(val.EmployerId))
                return val;
            if (!User.Current.HasEmployerAccess(val.EmployerId))
                return null;

            return val;
        }

        /// <summary>
        /// Called when [saving].
        /// </summary>
        /// <returns></returns>
        protected override bool OnSaving()
        {
            if (!string.IsNullOrWhiteSpace(Code))
                Code = Code.ToUpperInvariant();
            return base.OnSaving();
        }
    }
}
