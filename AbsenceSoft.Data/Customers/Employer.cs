﻿using AbsenceSoft.Common;
using AbsenceSoft.Common.Properties;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Processing;
using AbsenceSoft.Data.ToDo;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Data.Customers
{
    /// <summary>
    /// The employer entity.
    /// </summary>
    [Serializable]
    public class Employer : BaseCustomerEntity<Employer>
    {
        public Employer()
        {
            Holidays = new List<Holiday>();
            Features = new List<AppFeature>();
            SuppressReasons = new List<string>();
            SuppressPolicies = new List<string>();
            SuppressWorkflows = new List<string>();
            SuppressAccommodationTypes = new List<string>();
            SuppressedEntities = new List<SuppressedEntity>();
        }

        /// <summary>
        /// Gets or sets the name of the employer.
        /// </summary>
        [BsonRequired]
        public string Name { get; set; }

        /// <summary>
        /// Customer's own personal code for this employer. This is set externally when setting up an
        /// employer account and used for matching things like employee file updates, exports, etc.
        /// </summary>
        [BsonIgnoreIfNull]
        public string ReferenceCode { get; set; }

        /// <summary>
        /// Gets or sets the employer's custom Url for accessing the site.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Gets or sets the optional logo id for this employer.
        /// </summary>
        [BsonRepresentation(BsonType.ObjectId), BsonIgnoreIfNull]
        public string LogoId { get; set; }

        /// <summary>
        /// Gets or sets the type of the email replies.
        /// </summary>
        /// <value>
        /// The type of the email replies.
        /// </value>
        [BsonIgnoreIfNull]
        public EmailReplies? EmailRepliesType { get; set; }

        /// <summary>
        /// Gets or sets the EIN for the employer; this property is NOT stored, it is only used for hashing.
        /// </summary>
        [BsonIgnoreIfNull]
        public CryptoString Ein { get; set; }

        /// <summary>
        /// Gets or sets the contact information for this employer.
        /// </summary>
        [BsonIgnoreIfNull]
        public Contact Contact { get; set; }

        /// <summary>
        /// Gets or sets the employer's fiscal start month or arbitrary month for use for given usage calculations or other
        /// things that use fiscal or arbitrary year start dates, etc.
        /// </summary>
        [BsonIgnoreIfNull]
        public Month? ResetMonth { get; set; }

        /// <summary>
        /// Gets or sets the employer's fiscal start day or arbitrary day for use for given usage calculations or other
        /// things that use fiscal or arbitrary year start dates, etc.
        /// </summary>
        [BsonIgnoreIfNull]
        public int? ResetDayOfMonth { get; set; }

        /// <summary>
        /// Gets or sets the calculation type for FML/FMLA that is used in calculating usage; this is important because
        /// of the terminology and has to be selected for ALL FML type leaves of absence/cases because of
        /// equal treatment laws, etc.
        /// </summary>
        public PeriodType FMLPeriodType { get; set; }

        /// <summary>
        /// Gets or sets the list of holidays observed for the purposes of leave of absence policies, e.g. FMLA exempted or
        /// non-counted holidays under the holiday rules.
        /// </summary>
        public List<Holiday> Holidays { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not this employer allows intermittent leaves of absence to
        /// be taken against birth, adoption or foster care leaves of absence reasons.
        /// </summary>
        public bool AllowIntermittentForBirthAdoptionOrFosterCare { get; set; }

        /// <summary>
        /// Some Employers want to upload their own Prior Hours Worked and Ignore our calculations. If this flag is ON, the normal Prior Hours calculations
        /// for the last 12 months will not be performed.
        /// </summary>
        public bool IgnoreScheduleForPriorHoursWorked { get; set; }

        /// <summary>
        /// Some Employers want to upload their own Minutes Worked Per Week and Ignore our calculations. If this flag is ON, the normal Minutes Worked per week calculations
        /// will not be performed.
        /// </summary>
        public bool IgnoreAverageMinutesWorkedPerWeek { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not this employer evaluates the 50 in 75 rule. If so, each employee's
        /// flag must be set individually.
        /// </summary>
        public bool Enable50In75MileRule { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not this employer evaluates the spouse at same employer rule. If so, each employee's
        /// spouse's usage will be mesaured where applicable against total entitlement.
        /// </summary>
        public bool EnableSpouseAtSameEmployerRule { get; set; }

        /// <summary>
        /// Gets or sets the weekly work hours an employee must be scheduled to work to be considered full-time.
        /// </summary>
        public double? FTWeeklyWorkHours { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the company is a publicly traded company or privately held.
        /// This is important for states such as Connecticut that have varying legislation which changes entitlement
        /// depending on what type of company you are.
        /// </summary>
        public bool? IsPubliclyTradedCompany { get; set; }

        /// <summary>
        /// Gets or sets the features this employer has.
        /// </summary>
        /// <value>
        /// The features.
        /// </value>
        public List<AppFeature> Features { get; set; }

        /// <summary>
        /// Gets or sets the max number of employees for the current plan/projected usage for billing purposes.
        /// </summary>
        [BsonIgnoreIfNull]
        public int? MaxEmployees { get; set; }

        /// <summary>
        /// Gets or sets the collection absence reason codes that should be suppressed.
        /// </summary>
        public List<string> SuppressReasons { get; set; }

        /// <summary>
        /// Gets or sets the collection accommodation type codes that should be suppressed.
        /// </summary>
        public List<string> SuppressAccommodationTypes { get; set; }

        /// <summary>
        /// Gets or sets the policies that should be suppressed for only manual selection (not auto-selection).
        /// </summary>
        public List<string> SuppressPolicies { get; set; }

        /// <summary>
        /// Gets or sets the workflows that should be suppressed for this employer.
        /// </summary>
        /// <value>
        /// The suppress workflow codes.
        /// </value>
        public List<string> SuppressWorkflows { get; set; }

        /// <summary>
        /// Gets or sets the entities that should be suppressed for this employer
        /// </summary>
        public List<SuppressedEntity> SuppressedEntities { get; set; }

        /// <summary>
        /// Gets or Sets the employer's default From field when sending communications via email
        /// </summary>
        [BsonIgnoreIfNull]
        public string DefaultEmailFromAddress { get; set; }

        [BsonIgnoreIfNull]
        public string ToDoTemplateCode { get; set; }
        /// <summary>
        /// Gets or sets the osha API token.
        /// </summary>
        /// <value>
        /// The osha API token.
        /// </value>
        [BsonIgnoreIfNull]
        public string OshaApiToken { get; set; }
        /// <summary>
        /// Gets or sets the employer's default pay schedule id for when an employee doesn't have a specific one set
        /// </summary>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string DefaultPayScheduleId { get; set; }

        [BsonIgnore, JsonIgnore]
        public PaySchedule DefaultPaySchedule
        {
            get { return GetReferenceValue<PaySchedule>(this.DefaultPayScheduleId); }
            set { DefaultPayScheduleId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>
        /// The start date.
        /// </value>
        [BsonDateTimeOptions(DateOnly = true)]
        [BsonIgnoreIfNull]
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>
        /// The end date.
        /// </value>
        [BsonDateTimeOptions(DateOnly = true)]
        [BsonIgnoreIfNull]
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Returns whether this employer is disabled based on effective date and disabled date
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is disabled; otherwise, <c>false</c>.
        /// </value>
        public bool IsDisabled
        {
            get
            {
                return !DateTime.Today.DateInRange(StartDate ?? DateTime.Today, EndDate);
            }
        }

        /// <summary>
        /// Determines whether or not this employer has a given feature enabled.
        /// </summary>
        /// <param name="feature">The feature to check.</param>
        /// <returns><c>true</c> if the feature is present, otherwise <c>false</c>.</returns>
        public bool HasFeature(Feature feature)
        {
            if (Features != null && Features.Count > 0)
            {

                AppFeature employerFeature = Features.Find(f => f.Feature == feature);

                if (employerFeature != null)
                {
                    if (employerFeature.Enabled && (!employerFeature.EffectiveDate.HasValue || (employerFeature.EffectiveDate.HasValue && employerFeature.EffectiveDate.Value <= DateTime.Now)) && (!employerFeature.IneffectiveDate.HasValue || (employerFeature.IneffectiveDate.HasValue && employerFeature.IneffectiveDate.Value > DateTime.Now)))
                        return true;
                    else
                        return false;
                }
            }

            //Employer does not has feature configured. Check if Customer has it
            return Customer.HasFeature(feature);
        }

        /// <summary>
        /// Adds the feature.
        /// </summary>
        /// <param name="feature">The feature.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <param name="effectiveDate">The effective date.</param>
        /// <param name="ineffectiveDate">The ineffective date.</param>
        public void AddFeature(Feature feature, bool enabled = true, DateTime? effectiveDate = null, DateTime? ineffectiveDate = null)
        {
            if (Features == null)
                Features = new List<AppFeature>();

            AppFeature ef = Features.FirstOrDefault(f => f.Feature == feature);
            if (ef == null)
                ef = Features.AddFluid(new AppFeature() { Feature = feature });
            ef.Enabled = enabled;
            ef.EffectiveDate = effectiveDate;
            ef.IneffectiveDate = ineffectiveDate;
        }

        /// <summary>
        /// Removes the feature.
        /// </summary>
        /// <param name="feature">The feature.</param>
        /// <param name="ineffectiveDate">The ineffective date.</param>
        public void RemoveFeature(Feature feature, DateTime? ineffectiveDate = null)
        {
            AddFeature(feature, false, null, ineffectiveDate);
        }

        /// <summary>
		/// Get List of Active Features
		/// </summary>
		/// <returns></returns>
		public IDictionary<Feature, bool> GetFeatureListWithStatus()
        {
            IDictionary<Feature, bool> list = Customer.GetFeatureListWithStatus();

            if (Features != null)
            {
                foreach (AppFeature employerFeature in Features)
                {
                    if (employerFeature.Enabled && (!employerFeature.EffectiveDate.HasValue || (employerFeature.EffectiveDate.HasValue && employerFeature.EffectiveDate.Value <= DateTime.Now)) && (!employerFeature.IneffectiveDate.HasValue || (employerFeature.IneffectiveDate.HasValue && employerFeature.IneffectiveDate.Value > DateTime.Now)))
                    {
                        list[employerFeature.Feature] = true;
                    }
                    else
                    {
                        list[employerFeature.Feature] = false;
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Gets the employer's formatted base Url for accessing the site, the employer's site root basically.
        /// </summary>
        /// <returns>A URL representing this customer</returns>
        public string GetBaseUrl()
        {
            string subDomain = string.IsNullOrWhiteSpace(Url) ? Url : string.Concat(Url, ".");
            return string.Format(Settings.Default.BaseUrlEss, subDomain);
        }

        /// <summary>
        /// Overrides the base OnSaving event.
        /// </summary>
        /// <returns></returns>
        protected override bool OnSaving()
        {
            if (!string.IsNullOrWhiteSpace(this.Url))
                this.Url = this.Url.ToLowerInvariant();

            return base.OnSaving();
        }

        /// <summary>
        /// Gets a employer by its URL. Useful for DNS reverse lookups for an employer (w/o an override).
        /// </summary>
        /// <param name="url">The URL (host) name for the employer.</param>
        /// <returns></returns>
        public static Employer GetByUrl(string url)
        {
            using (new InstrumentationContext("Employer.GetByUrl"))
            {
                string myUrl = url.ToLowerInvariant();
                var employer = Employer.AsQueryable().FirstOrDefault(c => c.Url == myUrl && !c.IsDeleted);
                if (employer == null) return null;
                return employer;
            }
        }

        [BsonIgnore, JsonIgnore]
        public string SignatureName
        {
            get
            {
                return Name ?? Customer.Contact.FirstName + " " + Customer.Contact.LastName;
            }
        }

        /// <summary>
        /// Gets the current Employer id from the current employer, if any, otherwise <c>null</c>.
        /// </summary>
        public static string CurrentId { get { var cur = Current; return cur == null ? null : cur.Id; } }

        public const string CURRENT_EMPLOYER_CONTEXT_KEY = "__current_employer__";
        /// <summary>
        /// Gets the current Employer from the context, if it is set or valid.
        /// </summary>
        [Obsolete("Use Current.Employer() in AbsenceSoft.Web instead.  If not in AbsenceSoft project or AbsenceSoft.SelfService, pass the context in, don't add a reference to those methods")]
        public static Employer Current
        {
            get
            {
                Employer cur = null;
                try
                {
                    HttpContext context = HttpContext.Current;
                    if (context != null && context.Items != null && context.Request != null && context.Request.RequestContext != null && context.Request.RequestContext.RouteData != null)
                    {
                        cur = context.Items[CURRENT_EMPLOYER_CONTEXT_KEY] as Employer;
                        if (cur != null)
                            return cur;

                        object id;
                        if (context.Request.RequestContext.RouteData.Values.TryGetValue("employerId", out id))
                            cur = Employer.GetById(id.ToString());
                        else if (context.Request.RequestContext.RouteData.Values.TryGetValue("employeeId", out id))
                        {
                            var temp = Employee.GetById(id.ToString());
                            if (temp != null)
                                cur = temp.Employer;
                        }
                        else if (context.Request.RequestContext.RouteData.Values.TryGetValue("caseId", out id))
                        {
                            var temp = Case.GetById(id.ToString());
                            if (temp != null)
                                cur = temp.Employer;
                        }
                        else if (context.Request.RequestContext.RouteData.Values.TryGetValue("communicationId", out id))
                        {
                            var temp = Communication.GetById(id.ToString());
                            if (temp != null)
                                cur = temp.Employer;
                        }
                        else if (context.Request.RequestContext.RouteData.Values.TryGetValue("todoItemId", out id))
                        {
                            var temp = ToDoItem.GetById(id.ToString());
                            if (temp != null)
                                cur = temp.Employer;
                        }
                        else if (context.Request.RequestContext.RouteData.Values.TryGetValue("employeeNoteId", out id))
                        {
                            var temp = EmployeeNote.GetById(id.ToString());
                            if (temp != null)
                                cur = temp.Employer;
                        }
                        else if (context.Request.RequestContext.RouteData.Values.TryGetValue("caseNoteId", out id))
                        {
                            var temp = CaseNote.GetById(id.ToString());
                            if (temp != null)
                                cur = temp.Employer;
                        }
                        else if (context.Request.RequestContext.RouteData.Values.TryGetValue("fileId", out id) || context.Request.RequestContext.RouteData.Values.TryGetValue("eligibilityUploadId", out id))
                        {
                            var temp = EligibilityUpload.GetById(id.ToString());
                            if (temp != null)
                                cur = temp.Employer;
                        }
                        else if (context.Request.RequestContext.RouteData.Values.TryGetValue("contactId", out id))
                        {
                            var temp = EmployeeContact.GetById(id.ToString());
                            if (temp != null)
                                cur = temp.Employer;
                        }
                        else if (context.Request.RequestContext.RouteData.Values.TryGetValue("jobId", out id))
                        {
                            var temp = Job.GetById(id.ToString());
                            if (temp != null)
                                cur = temp.Employer;
                        }

                        context.Items[CURRENT_EMPLOYER_CONTEXT_KEY] = cur;
                    }
                }
                catch (NullReferenceException)
                {
                    cur = null;
                }
                catch (Exception ex)
                {
                    Log.Error("Error getting current employer from context", ex);
                    cur = null;
                }
                return cur;
            }
            set
            {
                // This a short-cut just in case we need to set the employer through some other means outside of the Employer context.
                try
                {
                    HttpContext context = HttpContext.Current;
                    if (context != null && context.Items != null)
                        context.Items[CURRENT_EMPLOYER_CONTEXT_KEY] = value;
                }
                catch (NullReferenceException)
                {
                    Log.Error("Null reference when retrieving current employer");
                }
                catch (Exception ex)
                {
                    Log.Error("Error setting current employer from context", ex);
                }
            }
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Concat(Name, " (", Url, ")");
        }
    }
}
