﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Cases;

namespace AbsenceSoft.Data.Customers
{
    /// <summary>
    /// Represents an employee contact which may be shared by one or more employees (spouses)
    /// </summary>
    [Serializable]
    public class EmployeeContact : BaseEmployeeEntity<EmployeeContact>, ICaseReporterModel
    {
        public EmployeeContact()
        {
            ContactTypeCode = "SELF";
            MilitaryStatus = Enums.MilitaryStatus.Civilian;
            Contact = new Contact();
        }

        /// <summary>
        /// Gets or sets the related employee number, for example, if the employee is married to another employee
        /// of the same employer, then we would track this Spouse type contact with the actual Spouse's
        /// employee number, which in turn would render it read-only in the UI and would mean this contact was auto-created.
        /// </summary>
        /// <value>
        /// The related employee number.
        /// </value>
        [BsonIgnoreIfNull]
        public string RelatedEmployeeNumber { get; set; }

        /// <summary>
        /// Gets or sets the contact details for this person, where applicable.
        /// </summary>
        [BsonRequired]
        public Contact Contact { get; set; }

		/// <summary>
        /// Gets or sets the type of this contact is to the employee.
        /// </summary>
		[BsonRequired]
		public string ContactTypeCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the contact type.
        /// </summary>
        /// <value>
        /// The name of the contact type.
        /// </value>
		[BsonIgnoreIfNull]
		public string ContactTypeName { get; set; }

        /// <summary>
        /// Gets or sets the years of age (how old) the contact is.
        /// </summary>
        public double? YearsOfAge { 
            get {
                return Contact == null ? new double?() : !Contact.DateOfBirth.HasValue ? new double?() : Contact.DateOfBirth.Value.AgeInYears();
            } 
            set { } 
        }

        /// <summary>
        /// Gets or sets the contact's military status.
        /// </summary>
        [BsonRequired, BsonDefaultValue(MilitaryStatus.Civilian)]
        public MilitaryStatus MilitaryStatus { get; set; }

        /// <summary>
        /// Gets or sets the related employee this contact represents.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Employee RelatedEmployee
        {
            get
            { 
                return GetReferenceValue<Employee>(this.RelatedEmployeeNumber, emp => emp.EmployeeNumber, 
                    empNum => Employee.AsQueryable().Where(e => e.CustomerId == this.CustomerId && e.EmployerId == this.EmployerId && e.EmployeeNumber == empNum).FirstOrDefault()); 
            }
            set { this.RelatedEmployeeNumber = SetReferenceValue(value, emp => emp.EmployeeNumber); }
        }

        /// <summary>
        /// Called before saving
        /// </summary>
        /// <returns>
        /// A value indicating whether or not the save should proceed (not canceled).
        /// </returns>
		protected override bool OnSaving()
		{
			if (!string.IsNullOrWhiteSpace(ContactTypeCode))
				ContactTypeCode = ContactTypeCode.ToUpperInvariant();

			if (!string.IsNullOrWhiteSpace(ContactTypeCode) && string.IsNullOrWhiteSpace(ContactTypeName))
			{
				ContactType type = ContactType.GetByCode(ContactTypeCode, CustomerId, EmployerId);
				if (type != null)
					ContactTypeName = type.Name;
            }
            if (this.Contact != null)
            {
                this.Contact.Email = string.IsNullOrWhiteSpace(this.Contact.Email) ? null : this.Contact.Email.ToLowerInvariant();
                this.Contact.AltEmail = string.IsNullOrWhiteSpace(this.Contact.AltEmail) ? null : this.Contact.AltEmail.ToLowerInvariant();
            }

			return base.OnSaving();
		}

        /// <summary>
        /// Gets or sets the contact position.
        /// </summary>
        /// <value>
        /// The contact position.
        /// </value>
        public int? ContactPosition { get; set; }

        /// <summary>
        /// Updates the Employee Number.
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="originalEmployeeNumber"></param>
        /// <param name="newEmployeeNumber"></param>
        /// <param name="modifiedById"></param>
        /// <param name="customerId"></param>
        /// <param name="employerId"></param>
        public static new void UpdateEmployeeNumber(string employeeId, string originalEmployeeNumber, string newEmployeeNumber, string modifiedById, string customerId, string employerId)
        {
            Update(Query.EQ(vst => vst.EmployeeId, employeeId), Updates.Set(vst => vst.EmployeeNumber, newEmployeeNumber).Set(e => e.ModifiedDate, DateTime.UtcNow).Set(e => e.ModifiedById, modifiedById), MongoDB.Driver.UpdateFlags.Multi);
            Update(Query.And(Query.EQ(e => e.RelatedEmployeeNumber, originalEmployeeNumber), Query.EQ(e => e.CustomerId, customerId), Query.EQ(e => e.EmployerId, employerId)), Updates.Set(e => e.RelatedEmployeeNumber, newEmployeeNumber).Set(e => e.ModifiedDate, DateTime.UtcNow).Set(e => e.ModifiedById, modifiedById), MongoDB.Driver.UpdateFlags.Multi);
        }
    }
}
