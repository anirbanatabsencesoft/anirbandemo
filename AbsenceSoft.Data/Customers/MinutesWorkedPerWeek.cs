﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Customers
{
    [Serializable]
    public class MinutesWorkedPerWeek : BaseNonEntity
    {
        /// <summary>
        /// Gets or sets the Minutes worked .
        /// </summary>
        ///   
        public int MinutesWorked { get; set; }

        /// <summary>
        /// Gets or sets the As Of date for those  minutes worked.
        /// </summary>
        ///    
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime AsOf { get; set; }
    }
}
