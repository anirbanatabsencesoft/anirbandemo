﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Customers
{
    [Serializable]
    /// <summary>
    /// Categorize organizations by it's type
    /// </summary>
    public class OrganizationByType
    {

        /// <summary>
        /// The organization type category
        /// </summary>
        public OrganizationType OrganizationType { get; set; }

        /// <summary>
        /// The list of organizations belongs to this type
        /// </summary>
        public List<Organization> Organizations { get; set; }
    }
}
