﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AbsenceSoft.Data.Audit;

namespace AbsenceSoft.Data.Customers
{
    [Serializable]
    public class CustomField : BaseEmployerOverrideableEntity<CustomField>, IAuditInclude
    {
        private AuditChangeHelper _auditHelper;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomField"/> class.
        /// </summary>
        public CustomField()
        {
            Target = EntityTarget.Employee;
            ListValues = new List<ListItem>();
            IsCollectedAtIntake = true;

            _auditHelper = new AuditChangeHelper("CustomField");
        }

        /// <summary>
        /// Gets or sets the target.
        /// </summary>
        /// <value>
        /// The target.
        /// </value>
        public EntityTarget Target { get; set; }

        private string _name;
        /// <summary>
        /// Gets or sets the name for the custom field
        /// </summary>
        public string Name { get => this._name;
            set =>  
                 
                this._name = (value??"").Trim();
              }

        /// <summary>
        /// Gets or sets the description for the custom field
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the label for the custom field
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Gets or sets the help text for the custom field
        /// </summary>
        [BsonIgnoreIfNull]
        public string HelpText { get; set; }

        /// <summary>
        /// Gets or sets the data type for the custom field
        /// </summary>
        public CustomFieldType DataType { get; set; }

        /// <summary>
        /// Gets or sets the value type for the custom field
        /// </summary>
        public CustomFieldValueType ValueType { get; set; }

        /// <summary>
        /// Gets or sets the list values for the custom field
        /// </summary>
        public List<ListItem> ListValues { get; set; }

        /// <summary>
        /// The unlderying value
        /// </summary>
        [BsonIgnore]
        private object _value = null;

        /// <summary>
        /// Gets or sets the selected value for this custom field
        /// </summary>
        [AuditPropertyNo]
        public object SelectedValue
        {
            get { return _value; }
            set
            {
                object oldValue = _value;
                if (value == null || string.IsNullOrWhiteSpace(value.ToString()))
                    _value = null;
                else if (ValueType == CustomFieldValueType.SelectList)
                    _value = value;
                else
                {
                    switch (DataType)
                    {
                        case CustomFieldType.Text:
                            if (value is DateTime)
                                _value = ((DateTime)value).ToString("MM/dd/yyyy");
                            else
                                _value = value.ToString();
                            break;
                        case CustomFieldType.Number:
                            if (value is string)
                            {
                                bool negative = value.ToString().StartsWith("-");
                                string v = Regex.Replace(value.ToString(), @"[^0-9\.]", "");
                                if (v.Contains("."))
                                {
                                    decimal d;
                                    decimal.TryParse(v, out d);
                                    if (negative)
                                        d = -d;
                                    _value = d;
                                }
                                else
                                {
                                    int n;
                                    int.TryParse(v, out n);
                                    if (negative)
                                        n = -n;
                                    _value = n;
                                }
                            }
                            else
                                _value = value;
                            break;
                        case CustomFieldType.Flag:
                            _value = new List<string>() { "1", "y", "yes", "t", "true", "on", "checked", "ok" }.Contains(value.ToString().ToLowerInvariant());
                            break;
                        case CustomFieldType.Date:
                            if (value is DateTime)
                                _value = value;
                            else
                            {
                                DateTime dt;
                                if (DateTime.TryParse(value.ToString(), out dt))
                                    _value = dt;
                                else
                                    _value = value;
                            }
                            break;
                        default:
                            _value = value;
                            break;
                    }                                   // switch (DataType)
                }

                if(oldValue == null)
                    oldValue = "";

                _auditHelper.RemoveChange("SelectedValue");
                if (_value == null)
                    _auditHelper.Change("SelectedValue", oldValue.ToString(), "", AuditAction.Save);
                else
                    _auditHelper.Change("SelectedValue", oldValue.ToString(), _value.ToString(), AuditAction.Save);
            }                                           // set
        }

        /// <summary>
        /// Gets the selected value text.
        /// </summary>
        /// <value>
        /// The selected value text.
        /// </value>
        [AuditPropertyNo, BsonIgnore]
        public string SelectedValueText
        {
            get
            {
                if (SelectedValue == null)
                    return string.Empty;
                string sv = SelectedValue.ToString();
                var lv = (ListValues ?? new List<ListItem>(0)).FirstOrDefault(i => i.Key.ToLowerInvariant() == sv.ToLowerInvariant());
                if (lv == null)
                {
                    if (SelectedValue is DateTime)
                        return ((DateTime)SelectedValue).ToUIString();
                    return sv;
                }
                return lv.Value;
            }
        }

        /// <summary>
        /// Gets or sets the order in which the field appears on the eligibility file
        /// </summary>
        public int? FileOrder { get; set; }

        /// <summary>
        /// Gets or sets an indiciator as to whether or not this field [is required].
        /// </summary>
        public bool IsRequired { get; set; }

        /// <summary>
        /// Gets or sets an indiciator as field is of intake or just view
        /// </summary>
        public bool IsCollectedAtIntake { get; set; }

        /// <summary>
        /// Gets or sets an indicator as field that IsESS is true or false
        /// </summary>
        public bool IsESS { get; set; }
        /// <summary>
        /// Gets the audit change items.
        /// </summary>
        /// <param name="before">The before.</param>
        /// <param name="after">The after.</param>
        /// <returns></returns>
        public List<ChangeItem> GetAuditChangeItems(object before, object after)
        {
            _auditHelper.SetId(this.Id);
            return _auditHelper.GetChanges();
        }

        /// <summary>
        /// Changes the history reset.
        /// </summary>
        public void ChangeHistoryReset()
        {
            _auditHelper.Reset();
        }
    }

    [AuditClassNo]
    [Serializable]
    public class ListItem
    {
        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public string Value { get; set; }
    }
}
