﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Customers
{
    [Serializable]
    public class OrganizationType : BaseEmployerEntity<OrganizationType>
    {
        /// <summary>
        /// The office location type code
        /// </summary>
        public const string OfficeLocationTypeCode = "OFFICELOCATION";

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [BsonIgnoreIfNull]
        public string Description { get; set; }

        /// <summary>
        /// Gets the organization type by code.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <returns></returns>
        public static OrganizationType GetByCode(string code, string customerId, string employerId)
        {
            var types = OrganizationType.AsQueryable().Where(o => o.Code == code && new[] { customerId, null }.Contains(o.CustomerId) && new[] { employerId, null }.Contains(o.EmployerId)).ToList();
            return types.FirstOrDefault(t => t.EmployerId == employerId)
                ?? types.FirstOrDefault(t => t.CustomerId == customerId)
                ?? types.FirstOrDefault();
        }
    }
}
