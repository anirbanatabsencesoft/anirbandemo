﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Customers
{
    [Serializable]
    public class Holiday : IValidatableObject
    {
        /// <summary>
        /// The private accessor for the unique id, this is used so that we don't have
        /// to initialize a base constructor on parent classes, but serialization
        /// will automatically set this value through the setter on <c>Id</c>.
        /// </summary>
        [BsonIgnore]
        private Guid? _id;

        /// <summary>
        /// Initializes a new instance of the <see cref="Holiday"/> class.
        /// </summary>
        public Holiday()
        {
        }

        /// <summary>
        /// Gets or sets a unique identifier (GUID) for the instance so it can be easily identified
        /// in a list by external operations or called by external/internal reference.
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public virtual Guid Id
        {
            get
            {
                if (_id == null)
                    _id = Guid.NewGuid();
                return _id.Value;
            }
            set { _id = value; }
        }

        /// <summary>
        /// Gets or sets the representative name of the holiday.
        /// </summary>
        [BsonRequired]
        public string Name { get; set; }

        /// <summary>
        /// the start date of the holiday (e.g. the company started giving MLK day in 2012 so 1/1/2012
        /// </summary>
        [BsonRequired]
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// The date the company stopped giving the holiday. You got MLK so we took away the day after Thanksgiving, e.g. End date 12/30/2011
        /// </summary>
        [BsonIgnoreIfNull]
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the explicit date of the holiday, especially if it only occurs once or
        /// does not occur on a pre-defined schedule.
        /// </summary>
        [BsonIgnoreIfNull]
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime? Date { get; set; }

        /// <summary>
        /// Gets or sets the month number that a holiday occurs in 
        /// </summary>
        [BsonIgnoreIfNull]
        public Month? Month { get; set; }

        /// <summary>
        /// Gets or sets a static day of the month that is the same each year
        /// for a given holiday.
        /// </summary>
        [BsonIgnoreIfNull]
        public int? DayOfMonth { get; set; }

        /// <summary>
        /// Gets or sets the number occurance for that day of the week. 
        /// <c>null</c> means that it is ignored.
        /// A positive number starts at the beginning of the month and counts that many occurrances as first, second, third, etc. day of the week of that month.
        /// A negative number starts at the last occurrance and works backwards, e.g. last monday of the month would be -1.
        /// </summary>
        [BsonIgnoreIfNull]
        public int? WeekOfMonth { get; set; }

        /// <summary>
        /// Gets or sets the day of the week that corresponds to the DayOfWeekNumber property, e.g. Monday is the most common, although
        /// Thursday is used for Thankgiving, Monday is typically used for things like "First Monday of Feb", etc. this value indicates that day of the week.
        /// </summary>
        [BsonIgnoreIfNull]
        public DayOfWeek? DayOfWeek { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the holiday should be observed on the nearest weekday.
        /// </summary>
        /// <value>
        /// <c>true</c> if is to be observed on the nearest weekday; otherwise, <c>false</c>.
        /// </value>
        public bool OrClosestWeekday { get; set; }

        /// <summary>
        /// Returns a formatted representation of this holiday
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder me = new StringBuilder();
            me.Append(Name);
            me.Append(", occurs ");
            if (Date.HasValue)
            {
                me.AppendFormat("on {0:MM/dd/yyyy}", Date);
            }
            else if (Month.HasValue)
            {
                if (DayOfMonth.HasValue)
                    me.AppendFormat("on {0} {1}{2}", Month, DayOfMonth, GetDayOfMonthExtension());
                else if (WeekOfMonth.HasValue)
                {
                    me.AppendFormat(" {0}", GetDayOfWeekNumberText());
                    if (DayOfWeek.HasValue)
                        me.AppendFormat(" {0}", DayOfWeek);
                    me.AppendFormat(" of {0}", Month);
                }
            }
            if (OrClosestWeekday)
                me.Append(" or closest weekday");
            return me.ToString();
        }

        /// <summary>
        /// Gets the day of month extension.
        /// </summary>
        /// <returns></returns>
        private string GetDayOfMonthExtension()
        {
            char last = DayOfMonth.ToString().Last();
            switch (last)
            {
                case '1':
                    return "st";
                case '2':
                    return "nd";
                case '3':
                    return "rd";
                default:
                    return "th";
            }
        }
        /// <summary>
        /// Gets the day of week number text.
        /// </summary>
        /// <returns></returns>
        private string GetDayOfWeekNumberText()
        {
            int dow = WeekOfMonth ?? 0;
            switch (dow)
            {
                case 1:
                    return "First";
                case 2:
                    return "Second";
                case 3:
                    return "Thrid";
                case 4:
                    return "Fourth";
                case -1:
                    return "Last";
                case -2:
                    return "Second to Last";
                case -3:
                    return "Thrid to Last";
                case -4:
                    return "Fourth to Last";
                default:
                    return "";
            }
        }

        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            var holiday = validationContext.ObjectInstance as Holiday;
            if (holiday == null)
            {
                yield break;
            }

            if(holiday.Month.HasValue && holiday.DayOfMonth.HasValue)
            {
                if(holiday.Month.Value == Enums.Month.February && holiday.DayOfMonth.Value == 29)
                {
                    yield return new ValidationResult("Holidays cannot occur on a leap year day", new[] { "Month", "DayOfMonth" });
                }
            }
        }
    }
}
