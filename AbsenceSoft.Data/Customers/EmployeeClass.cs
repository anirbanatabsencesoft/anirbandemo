﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Customers
{
    public class EmployeeClass : BaseEmployerOverrideableEntity<EmployeeClass>
    {
        public EmployeeClass()
        {

        }

        /// <summary>
        /// The Name of this Employee Class
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The Description of this Employee Class
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The Employee code is not allowed to be overridden as this is being used in conditional statement
        /// </summary>
        public bool IsLocked { get; set; }

        /// <summary>
        /// The order of this Employee code in UI dropdown
        /// </summary>
        public int? Order { get; set; }

    }
}
