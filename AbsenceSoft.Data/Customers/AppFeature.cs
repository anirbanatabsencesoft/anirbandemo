﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace AbsenceSoft.Data.Customers
{
    [Serializable]
	public class AppFeature : BaseNonEntity
	{
		/// <summary>
		/// Gets or Sets Feature
		/// </summary>
		[BsonRequired]
		public Feature Feature { get; set; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
		[BsonIgnoreIfNull]
		public string Name 
		{
			get 
			{
				return Feature.ToString().SplitCamelCaseString();
			}
            set { }
		}
		
		/// <summary>
		/// Gets or Sets enabling of feature
		/// </summary>
		[BsonRequired]
		public bool Enabled { get; set; }

		/// <summary>
		/// Gets or Sets Date when feature will be effective
		/// </summary>
		[BsonDateTimeOptions(DateOnly = true)]
		public DateTime? EffectiveDate { get; set; }

		/// <summary>
		/// Gets or Sets Date when feature is ineffective
		/// </summary>
		[BsonDateTimeOptions(DateOnly = true)]
		public DateTime? IneffectiveDate { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppFeature"/> class.
        /// </summary>
		public AppFeature()
		{
			Feature = Enums.Feature.None;
			Enabled = false;
		}

        public AppFeature(Feature feature)
        {
            Feature = feature;
            Enabled = true;
        }
	}
}
