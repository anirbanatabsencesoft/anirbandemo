﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Customers
{
    [Serializable]
    public class EmployeeConsultation : BaseEmployeeNumberEntity<EmployeeConsultation>
    {
        /// <summary>
        /// Gets or sets the title from the original consult type.
        /// </summary>
        [BsonRequired, BsonRepresentation(BsonType.ObjectId)]
        public string ConsultationTypeId { get; set; }

        [BsonIgnore, JsonIgnore]
        public ConsultationType ConsultationType
        {
            get
            {
                return this.GetReferenceValue<ConsultationType>(this.ConsultationTypeId);
            }
            set
            {
                this.ConsultationTypeId = this.SetReferenceValue<ConsultationType>(value);
            }
        }

        /// <summary>
        /// Gets or sets the comments.
        /// </summary>
        [BsonRequired]
        public string Comments { get; set; }

        /// <summary>
        /// Gets or sets the consult from the original consult type.
        /// </summary>
        [BsonRequired]
        public string Consult { get; set; }

    }
}
