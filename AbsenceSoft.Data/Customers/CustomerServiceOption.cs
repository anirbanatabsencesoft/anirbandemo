﻿using System;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace AbsenceSoft.Data.Customers
{
    /// <summary>
    /// This entity tracks various customer related service option values which get referenced
    /// by entities like EmployerServiceOption.
    /// </summary>
    [Serializable, JsonObject(MemberSerialization.OptOut)]
    [BsonIgnoreExtraElements(false, Inherited = true)]
    public class CustomerServiceOption : BaseCustomerEntity<CustomerServiceOption>
    {
        /// <summary>
        /// The key of the customer service option, which is part of a unique index with CustomerId.
        /// </summary>
        [BsonRequired]
        public string Key { get; set; }

        /// <summary>
        /// The value of the customer service option.
        /// </summary>
        [BsonRequired]
        public string Value { get; set; }

        /// <summary>
        /// The order the value should be displayed in a user interface.
        /// </summary>
        [BsonRequired]
        public int Order { get; set; }
    }
}
