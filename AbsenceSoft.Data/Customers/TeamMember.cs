﻿using AbsenceSoft.Data.Security;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Customers
{
    [Serializable]
    public class TeamMember: BaseCustomerEntity<TeamMember>
    {

        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string UserId { get; set; }

        [BsonIgnore, JsonIgnore]
        public User User
        {
            get { return GetReferenceValue<User>(UserId); }
            set { UserId = SetReferenceValue(value); }
        }


        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string TeamId { get; set; }


        [BsonIgnore, JsonIgnore]
        public Team Team
        {
            get { return GetReferenceValue<Team>(TeamId); }
            set { TeamId = SetReferenceValue(value); }
        }

        public bool IsTeamLead { get; set; }

        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string AssignedById { get; set; }

        [BsonIgnore, JsonIgnore]
        public User AssignedBy
        {
            get { return GetReferenceValue<User>(AssignedById); }
            set { AssignedById = SetReferenceValue(value); }
        }

        public DateTime AssignedOn { get; set; }

        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string ModifiedById { get; set; }

        [BsonIgnore, JsonIgnore]
        public User ModifiedBy
        {
            get { return GetReferenceValue<User>(ModifiedById); }
            set { ModifiedById = SetReferenceValue(value); }
        }

        public DateTime ModifiedOn { get; set; }
    }
}
