﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Customers
{
    /// <summary>
    /// This class is not stored in the database. It is populated by the CaseService to
    /// provide the UI with payment date data
    /// </summary>
    [Serializable]
    public class PaySchedulePayments
    {
        /// <summary>
        /// Gets or sets the type of the pay period.
        /// </summary>
        /// <value>
        /// The type of the pay period.
        /// </value>
        public PayPeriodType PayPeriodType { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>
        /// The start date.
        /// </value>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>
        /// The end date.
        /// </value>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Gets or sets the processing days.
        /// </summary>
        /// <value>
        /// The processing days.
        /// </value>
        public int ProcessingDays { get; set; }
    }
}
