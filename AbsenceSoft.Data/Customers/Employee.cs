﻿using AbsenceSoft.Common.Security;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.RiskProfiles;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbsenceSoft.Data.Customers
{
    /// <summary>
    /// The employee entity.
    /// </summary>
    [Serializable]
    public class Employee : BaseEmployerEntity<Employee>
    {
        public Employee()
        {
            Status = EmploymentStatus.Active;
            Info = new EmployeeInfo();
            WorkSchedules = new List<Schedule>();
            MilitaryStatus = Enums.MilitaryStatus.Civilian;
            PriorHours = new List<PriorHours>();
            MinutesWorkedPerWeek = new List<MinutesWorkedPerWeek>();
            CustomFields = new List<CustomField>();
        }

        /// <summary>
        /// Gets or sets the employee number used by the employer to uniquely identify this employee.
        /// </summary>
        public string EmployeeNumber { get; set; }

        /// <summary>
        /// Gets or sets the employee's information that can change without impact to an active case
        /// such as contact information, supervisor/HR contacts, etc.
        /// </summary>
        public EmployeeInfo Info { get; set; }

        /// <summary>
        /// Gets or sets the employee's first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the employee's middle name or initial.
        /// </summary>
        [BsonIgnoreIfNull]
        public string MiddleName { get; set; }

        /// <summary>
        /// Gets or sets the employee's last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets the formatted full name for display
        /// </summary>
        [BsonIgnore]
        public string FullName
        {
            get
            {
                StringBuilder name = new StringBuilder();
                if (!string.IsNullOrWhiteSpace(LastName))
                    name.Append(LastName);
                if (!string.IsNullOrWhiteSpace(FirstName))
                    name.AppendFormat("{0}{1}", name.Length > 0 ? ", " : "", FirstName);
                if (!string.IsNullOrWhiteSpace(MiddleName))
                    name.AppendFormat("{0}{1}", name.Length > 0 ? "  " : "", MiddleName);
                return name.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the employee's gender. If not set, is <c>null</c>.
        /// </summary>
        [BsonIgnoreIfNull]
        public Gender? Gender { get; set; }

        /// <summary>
        /// Gets or sets the employee's date of birth.
        /// </summary>
        [BsonIgnoreIfNull]
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime? DoB { get; set; }

        /// <summary>
        /// Gets or sets the employee's social security number.
        /// </summary>
        public CryptoString Ssn { get; set; }

        /// <summary>
        /// Gets or sets the employee's status.
        /// </summary>
        public EmploymentStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the employee's salary information.
        /// </summary>
        [BsonIgnoreIfNull]
        public double? Salary { get; set; }

        /// <summary>
        /// Gets or sets the employee's pay type.
        /// </summary>
        [BsonIgnoreIfNull]
        public PayType? PayType { get; set; }

        /// <summary>
        /// Gets or sets the code of employee class.
        /// </summary>
        [BsonIgnoreIfNull]
        public string EmployeeClassCode { get; set; }

        /// <summary>
        /// Gets or sets the code of employee class.
        /// </summary>
        [BsonIgnoreIfNull]
        public string EmployeeClassName { get; set; }

        /// <summary>
        /// Gets or sets the cost center code for the employee
        /// </summary>
        [BsonIgnoreIfNull]
        public string CostCenterCode { get; set; }

        /// <summary>
        /// Gets or sets the employee's job title.
        /// </summary>
        [BsonIgnoreIfNull]
        public string JobTitle { get; set; }

        /// <summary>
        /// Gets or sets the job activity.
        /// </summary>
        /// <value>
        /// The job activity.
        /// </value>
        [BsonIgnoreIfNull]
        public JobClassification? JobActivity { get; set; }

        /// <summary>
        /// Gets or sets the department.
        /// </summary>
        /// <value>
        /// The department.
        /// </value>
        [BsonIgnoreIfNull]
        public string Department { get; set; }

        /// <summary>
        /// Gets or sets a collection of prior hours worked for the employee as of certain dates so this value
        /// can be collected in absence of a work schedule for calculating eligibility up to some date.
        /// </summary>
        [BsonIgnoreIfNull]
        public List<PriorHours> PriorHours { get; set; }

        /// <summary>
        /// Gets or sets a collection of Average Weekly Minutes for the employee as of certain dates so this value
        /// can be collected in absence of a work schedule for calculating eligibility up to some date.
        /// </summary>
        [BsonIgnoreIfNull]
        public List<MinutesWorkedPerWeek> MinutesWorkedPerWeek { get; set; }

        /// <summary>
        /// Gets or sets the employee's service date.
        /// </summary>
        [BsonIgnoreIfNull]
        [BsonDateTimeOptions(DateOnly = true)]
        public virtual DateTime? ServiceDate { get; set; }

        /// <summary>
        /// Gets or sets the employee's date of hire.
        /// </summary>
        [BsonIgnoreIfNull]
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime? HireDate { get; set; }

        /// <summary>
        /// Gets or sets the employee's re-hire date, if applicable.
        /// </summary>
        [BsonIgnoreIfNull]
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime? RehireDate { get; set; }

        /// <summary>
        /// Gets or sets the employee's date of termination.
        /// </summary>
        [BsonIgnoreIfNull]
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime? TerminationDate { get; set; }

        /// <summary>
        /// Gets or sets the employee's work state.
        /// </summary>
        public string WorkState { get; set; }

        /// <summary>
        /// Gets or the employee's work statefor workflow rule expression.
        /// </summary>
        [BsonIgnore]
        public string WorkStateUS { get { return WorkState; } }

        /// <summary>
        /// Gets or sets the employee's country of origin/employment.
        /// </summary>
        [BsonDefaultValue("US")]
        [BsonIgnoreIfNull]
        public string WorkCountry { get; set; }

        /// <summary>
        /// Gets or sets the work county.
        /// </summary>
        /// <value>
        /// The work county.
        /// </value>
        [BsonIgnoreIfNull]
        public string WorkCounty { get; set; }

        /// <summary>
        /// Gets or sets the work city.
        /// </summary>
        /// <value>
        /// The work city.
        /// </value>
        [BsonIgnoreIfNull]
        public string WorkCity { get; set; }

        /// <summary>
        /// Gets or sets the residence county.
        /// </summary>
        /// <value>
        /// The residence county.
        /// </value>
        [BsonIgnoreIfNull]
        public string ResidenceCounty { get; set; }
       
        /// <summary>
        /// Gets or sets the employee's work schdules
        /// </summary>
        public List<Schedule> WorkSchedules { get; set; }

        /// <summary>
        /// if not the default payment schedule, then it is this schedule
        /// </summary>
        [BsonIgnoreIfNull]
        public string PayScheduleId { get; set; }

        /// <summary>
        /// Returns either the employer default or the specific employee id
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public PaySchedule PaySchedule
        {
            get
            {
                if (!string.IsNullOrEmpty(PayScheduleId))
                    return GetReferenceValue<PaySchedule>(PayScheduleId);

                return Employer.DefaultPaySchedule;
            }
            set
            {
                PayScheduleId = SetReferenceValue<PaySchedule>(value);
            }
        }

        /// <summary>
        /// Gets or sets the indicator as to whether or not the employee's location meets the minimum 50 employees 
        /// in 75 mile radius rule as specified in the FMLA laws for the US.
        /// </summary>
        public bool Meets50In75MileRule { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not this is a key employee for the FMLA Key Employee Exemption.
        /// Key Employee Exception Exempts salaried employees from reinstatement if among highest paid 10 percent and 
        /// if restoration would lead to grievous economic harm to employer.
        /// </summary>
        public bool IsKeyEmployee { get; set; }

        /// <summary>
        /// Gets or set a value indicating whether or not this employee is exempt from minimum wage and overtime
        /// under regulations, 29 C.F.R. Part 541 of the Fair Labor Standards Act (FSLA).
        /// </summary>
        public bool IsExempt { get; set; }

        /// <summary>
        /// Gets or sets the employee's spouse's employee number if that employee's spouse is employed by the same employer.
        /// Otherwise this should be <c>null</c>. This is used for calculating max entitlement for FML provisions that
        /// change allowances for covered service member and other qualifying reasons where there is a max per each
        /// employee combined for a given reason.
        /// </summary>
        [BsonIgnoreIfNull]
        public string SpouseEmployeeNumber { get; set; }

        /// <summary>
        /// Gets or sets the contact's military status.
        /// </summary>
        [BsonRequired, BsonDefaultValue(MilitaryStatus.Civilian)]
        public MilitaryStatus MilitaryStatus { get; set; }

        /// <summary>
        /// Gets or sets the employee's custom fields
        /// </summary>
        [BsonIgnoreIfNull]
        public List<CustomField> CustomFields { get; set; }

        /// <summary>
        /// Gets or sets a copy of the employer name that the employee belongs to.
        /// </summary>
        [BsonIgnoreIfNull]
        public string EmployerName { get; set; }

        /// <summary>
        /// Gets or sets a copy of the most recently calculated risk profile
        /// </summary>
        [BsonIgnoreIfNull]
        public RiskProfile RiskProfile { get; set; }

        /// <summary>
        /// Gets or sets the start day of week for the employee's schedule, policy usage, etc..
        /// </summary>
        /// <value>
        /// The start day of week.
        /// </value>
        [BsonIgnoreIfNull]
        public DayOfWeek? StartDayOfWeek { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not this is a Flight Crew employee.
        /// </summary>
        [BsonIgnoreIfNull]
        public bool? IsFlightCrew { get; set; }

        /// <summary>
        /// Gets or sets the employee's spouse employee record.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Employee SpouseEmployee
        {
            get
            {
                return GetReferenceValue<Employee>(this.SpouseEmployeeNumber, emp => emp.EmployeeNumber,
                    empNum => Employee.AsQueryable().Where(e => e.CustomerId == this.CustomerId && e.EmployerId == this.EmployerId && e.EmployeeNumber == empNum).FirstOrDefault());
            }
            set { this.SpouseEmployeeNumber = SetReferenceValue(value, emp => emp.EmployeeNumber); }
        }

        /// <summary>
        /// Gets the employee display title
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public string Title
        {
            get
            {
                return string.Format("{0} {1} (#{2})", FirstName, LastName, EmployeeNumber);
            }
        }


        /// <summary>
        /// Gets the employee display title in Search
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public string SearchTitle
        {
            get
            {
                StringBuilder name = new StringBuilder();
                if (!string.IsNullOrWhiteSpace(FirstName))
                {
                    name.Append(FirstName);
                }
                if (!string.IsNullOrWhiteSpace(MiddleName))
                {
                    name.AppendFormat("{0}{1}", name.Length > 0 ? " " : "", MiddleName);
                }
                if (!string.IsNullOrWhiteSpace(LastName))
                {
                    name.AppendFormat("{0}{1}", name.Length > 0 ? " " : "", LastName);
                }
                if (!string.IsNullOrWhiteSpace(EmployeeNumber))
                {
                    name.AppendFormat(" (#{0})", EmployeeNumber);
                }
                return name.ToString();
            }
        }


        /// <summary>
        /// Gets the employee display description
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public string Description
        {
            get
            {
                return string.Format("Gender: {0}, Hire Date: {1:MM/dd/yyyy}, Service Date: {2:MM/dd/yyyy}, Email: {3}, Location: {4}", Gender, HireDate, ServiceDate, !String.IsNullOrEmpty(Info.Email) ? Info.Email : "none", Info == null || string.IsNullOrEmpty(GetOfficeLocationDisplay()) ? "unknown" : GetOfficeLocationDisplay());
            }
        }

        /// <summary>
        /// Gets and sets office locations instead of hitting the DB each time - GetOfficeLocationDisplay() hits database everytime we use Description property 
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public string OfficeLocationDisplayName
        {
            get;
            set;
        }

        /// <summary>
        /// Description of an employee
        /// </summary>
        /// <param name="includeTitle"></param>
        /// <returns></returns>
        public string GetDescriptionForSearchResult(bool includeTitle = false)
        {
            StringBuilder descriptionBuilder = new StringBuilder();
            if (!string.IsNullOrEmpty(SearchTitle) && includeTitle)
            {
                descriptionBuilder.AppendFormat(" | Employee: {0}", SearchTitle);
            }
            if (Gender.HasValue)
            {
                descriptionBuilder.AppendFormat(" | {0}", Gender);
            }
            if (DoB.HasValue)
            {
                descriptionBuilder.AppendFormat(" | DOB: {0:MM/dd/yyyy}", DoB.Value);
            }
            if (HireDate.HasValue)
            {
                descriptionBuilder.AppendFormat(" | Hire Date: {0:MM/dd/yyyy}", HireDate.Value);
            }
            if (ServiceDate.HasValue)
            {
                descriptionBuilder.AppendFormat(" | Service Date: {0:MM/dd/yyyy}", ServiceDate.Value);
            }
            if (!string.IsNullOrEmpty(Info.Email))
            {
                descriptionBuilder.AppendFormat(" | {0}", Info.Email);
            }
            if (!string.IsNullOrEmpty(OfficeLocationDisplayName))
            {
                descriptionBuilder.AppendFormat(" | Location: {0}", OfficeLocationDisplayName);
            }

            if (descriptionBuilder.ToString().Length > 3)
            {
                // remove first 3 characters to get rid of initial " | "
                return descriptionBuilder.Remove(0, 3).ToString();
            }
            else
            {
                return descriptionBuilder.ToString();
            }
        }

        /// <summary>
        /// Gets the current office location for the employee.
        /// </summary>
        /// <returns></returns>
        public EmployeeOrganization GetOfficeLocation(string code)
        {
            return EmployeeOrganization.AsQueryable().Where(e => e.EmployerId == EmployerId).ToList().Where(p => p.Code.Equals(code, StringComparison.InvariantCultureIgnoreCase))
                .FirstOrDefault();
        }

        /// <summary>
        /// Gets the current office location for the employee.
        /// </summary>
        /// <returns></returns>
        public EmployeeOrganization GetOfficeLocation()
        {
            return GetOfficeLocations()
                .Where(e => e.Dates == null || e.Dates.IncludesToday(true))
                .OrderByDescending(e => e.ModifiedDate)
                .FirstOrDefault();
        }

        /// <summary>
        /// Gets the office location display.
        /// </summary>
        /// <returns></returns>
        public string GetOfficeLocationDisplay()
        {
            var officeLocation = this.GetOfficeLocation();
            if (officeLocation != null)
                return officeLocation.Name;

            return null;
        }

        /// <summary>
        /// Gets the office location list for the employee.
        /// </summary>
        /// <returns></returns>
        public List<EmployeeOrganization> GetOfficeLocations()
        {
            var orgs = EmployeeOrganization.AsQueryable().Where(e => e.CustomerId == CustomerId
                && e.EmployerId == EmployerId
                && e.EmployeeNumber == EmployeeNumber
                && e.TypeCode == OrganizationType.OfficeLocationTypeCode)
                .ToList()
                .OrderByDescending(o => o.Dates)
                .ToList();

            return orgs;
        }

        /// <summary>
        /// Gets the jobs for this employee (all of 'em).
        /// </summary>
        /// <returns></returns>
        public virtual List<EmployeeJob> GetJobs()
        {
            return EmployeeJob.AsQueryable().Where(e => e.CustomerId == CustomerId && e.EmployerId == EmployerId && e.EmployeeNumber == EmployeeNumber).ToList();
        }

        /// <summary>
        /// Gets the current job.
        /// </summary>
        /// <returns></returns>
        public virtual EmployeeJob GetCurrentJob()
        {
            IEnumerable<EmployeeJob> currentJobs = GetJobs()
               .Where(e => e.Dates == null || e.Dates.IsNull || e.Dates.IncludesToday(true));

            if (currentJobs.Count() == 0)
                return null;

            if (currentJobs.Count() == 1)
                return currentJobs.First();

            return currentJobs.OrderByDescending(ej => ej.ModifiedDate).First();
        }

        /// <summary>
        /// Gets the job by id.
        /// </summary>
        /// <param name="id">The job id</param>
        /// <returns>Employee Job</returns>
        public EmployeeJob GetJobById(string id)
        {
            return EmployeeJob.GetById(id);
        }

        /// <summary>
        /// Gets the job on a particular date.
        /// </summary>
        /// <returns></returns>
        public EmployeeJob GetPeriodJob(DateTime date, IEnumerable<EmployeeJob> _currentJobs = null)
        {
            IEnumerable<EmployeeJob> currentJobs = (_currentJobs != null ? _currentJobs : GetJobs())
               .Where(e => e.Dates == null || e.Dates.IsNull || e.Dates.IncludesDate(date));

            if (currentJobs.Count() == 0)
                return null;

            if (currentJobs.Count() == 1)
                return currentJobs.First();

            return currentJobs.OrderByDescending(ej => ej.ModifiedDate).First();
        }

        /// <summary>
        /// Gets the necessities.
        /// </summary>
        /// <returns></returns>
        public List<EmployeeNecessity> GetNecessities()
        {
            return EmployeeNecessity.AsQueryable().Where(e => e.CustomerId == CustomerId
                && e.EmployerId == EmployerId
                && e.EmployeeNumber == EmployeeNumber)
                .ToList()
                .OrderByDescending(o => o.Dates)
                .ToList();
        }

        /// <summary>
        /// Set Office Location To Display
        /// Retrieving locations and setting them instead of hitting the DB each time
        /// </summary>
        /// <param name="empOrgs"></param>
        public void SetOfficeLocationDisplayName(List<EmployeeOrganization> employeeOrganizations)
        {
            if (employeeOrganizations != null && employeeOrganizations.Any())
            {
                var organization = employeeOrganizations.Where(e => e.CustomerId == CustomerId
                    && e.EmployerId == EmployerId
                    && e.EmployeeNumber == EmployeeNumber
                    && e.TypeCode == OrganizationType.OfficeLocationTypeCode
                    && e.Dates == null || e.Dates.IncludesToday(true))
                    .OrderByDescending(o => o.Dates)
                    .ThenByDescending(o => o.ModifiedDate)
                    .FirstOrDefault();
                if (organization != null)
                {
                    OfficeLocationDisplayName = organization.Name;
                }
            }
        }

        #region Static Methods

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching.
        /// </summary>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static new IQueryable<Employee> AsQueryable()
        {
            if (User.Current != null)
                return AsQueryable(User.Current);
            return Repository.AsQueryable();
        }

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching while automatically
        /// applying the appropriate data access filters onto it automagically.
        /// </summary>
        /// <param name="entity">The entity of type <c>TEntity</c> to which to build the filtered queryable off of</param>
        /// <param name="user">The user of which to build data visibility and data access from</param>
        /// <param name="permission">The permission that the user should have access to when filtering the data parameters</param>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static new IQueryable<Employee> AsQueryable(User user, Permission permission) { return AsQueryable(user, permission.Id); }
        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching while automatically
        /// applying the appropriate data access filters onto it automagically.
        /// </summary>
        /// <param name="entity">The entity of type <c>TEntity</c> to which to build the filtered queryable off of</param>
        /// <param name="user">The user of which to build data visibility and data access from</param>
        /// <param name="permission">The permission that the user should have access to when filtering the data parameters</param>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static new IQueryable<Employee> AsQueryable(User user, string permission = null)
        {
            var query = BaseEmployerEntity<Employee>.AsQueryable(user, permission);

            // Determine if the user is a SS User for any employer queried
            if (User.IsEmployeeSelfServicePortal || (user.Customer.HasFeature(Enums.Feature.ESSDataVisibilityInPortal) && !User.Permissions.GetProjectedPermissions(user).Contains(Permission.ViewAllEmployees.Id)))
                query = query.Where(q => User.HasVisibilityTo.Contains(q.Id));

            return query;
        }//end: AsQueryable

        #endregion

        /// <summary>
        /// Ensures all PII data is encrypted and then passes the save logic to the underlying base class' save method.
        /// </summary>
        /// <returns></returns>
        public override Employee Save()
        {
            if (Ssn != null && !string.IsNullOrWhiteSpace(Ssn.PlainText))
            {
                Ssn.Hashed = Ssn.PlainText.Sha256Hash();
                Ssn.Encrypted = Ssn.PlainText.Encrypt();
            }
            var me = base.Save();
            if (me.Info != null)
                Cases.Case.UpdateCasesEmployeeCurrentOfficeLocation(me.Id, me.Info.OfficeLocation);
            return me;
        }//Save

        /// <summary>
        /// Called when [saving].
        /// </summary>
        /// <returns></returns>
        protected override bool OnSaving()
        {
            if (string.IsNullOrWhiteSpace(EmployerName) && this.Employer != null)
                this.EmployerName = Employer.Name;
            if (this.Info != null)
            {
                this.Info.Email = string.IsNullOrWhiteSpace(this.Info.Email) ? null : this.Info.Email.ToLowerInvariant();
                this.Info.AltEmail = string.IsNullOrWhiteSpace(this.Info.AltEmail) ? null : this.Info.AltEmail.ToLowerInvariant();
            }
            return base.OnSaving();
        }//end: OnSaving

        /// <summary>
        /// Updates the Employee Number
        /// </summary>        
        /// <param name="originalEmployeeNumber"></param>
        /// <param name="newEmployeeNumber"></param>
        /// <param name="modifiedById"></param>
        /// <param name="customerId"></param>
        /// <param name="employerId"></param>
        public static void UpdateEmployeeNumber(string originalEmployeeNumber, string newEmployeeNumber, string modifiedById, string customerId, string employerId)
        {
            Update(Query.And(Query.EQ(e => e.SpouseEmployeeNumber, originalEmployeeNumber), Query.EQ(e => e.CustomerId, customerId), Query.EQ(e => e.EmployerId, employerId)), Updates.Set(e => e.SpouseEmployeeNumber, newEmployeeNumber).Set(e => e.ModifiedDate, DateTime.UtcNow).Set(e => e.ModifiedById, modifiedById), MongoDB.Driver.UpdateFlags.Multi);
        }
    }
}
