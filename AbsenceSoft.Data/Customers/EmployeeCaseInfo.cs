﻿using System;

namespace AbsenceSoft.Data.Customers
{
    [Serializable]
    public class EmployeeCaseInfo
    {

        public string CaseId { get; set; }
        public string CaseNumber { get; set; }
        public string Reason { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Status { get; set; }
        public string CaseType { get; set; }
        public int CaseStatus { get; set; }
    }
}
