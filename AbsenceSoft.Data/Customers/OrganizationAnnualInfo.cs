﻿using AbsenceSoft.Common;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver.Linq;
using AbsenceSoft.Data.Security;

namespace AbsenceSoft.Data.Customers
{
    /// <summary>
    /// An organization entity that supports hierarchy and general awesomeness. Future home
    /// of other stuff that describes OrganizationAnnualInfo.
    /// </summary>
    [Serializable]
    public class OrganizationAnnualInfo : BaseEmployerEntity<OrganizationAnnualInfo>
    {

        public OrganizationAnnualInfo()
        {
        }
       
        [BsonRequired]
        public string OrganizationCode { get; set; }

      
        [BsonRequired]
        public int Year { get; set; }

      
        [BsonIgnoreIfNull]
        public int? AverageEmployeeCount { get; set; }

        [BsonIgnoreIfNull]
        public decimal? TotalHoursWorked { get; set; }

        /// <summary>
        /// Called when [saving].
        /// </summary>
        /// <returns></returns>
        protected override bool OnSaving()
        {
            if (!string.IsNullOrWhiteSpace(OrganizationCode))
                OrganizationCode = OrganizationCode.ToUpperInvariant();
            return base.OnSaving();
        }
    }
}
