﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace AbsenceSoft.Data.Customers
{
    /// <summary>
    /// Represents a single day of variable scheduled time
    /// </summary>
    [Serializable]
    public class VariableScheduleTime : BaseEmployeeEntity<VariableScheduleTime>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VariableScheduleTime"/> class.
        /// </summary>
        public VariableScheduleTime() : base()
        {
            Time = new Time();
        }

        /// <summary>
        /// Gets or sets the employee case identifier.
        /// </summary>
        /// <value>
        /// The employee case identifier.
        /// </value>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string EmployeeCaseId { get; set; }

        /// <summary>
        /// Gets or sets the time.
        /// </summary>
        /// <value>
        /// The time.
        /// </value>
        public Time Time { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            if (Time != null)
            {
                return Time.ToString();
            }
            return base.ToString();
        }
    }
}
