﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Customers
{
    [Serializable]
    public class EmployerContactType: BaseCustomerEntity<EmployerContactType>
    {

        [BsonRequired]
        public string Name { get; set; }

        [BsonRequired]
        public string Code { get; set; }
    }
}
