﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Web;
using System.Web.Security;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Security.Claims;
using Microsoft.AspNet.Identity;

namespace AbsenceSoft.Data.Customers
{
    /// <summary>
    /// This class contains the customer security settings.
    /// A default instance should be used in the event a customer has used all defaults.
    /// </summary>
    [Serializable]
    public class SecuritySettings
    {
        /// <summary>
        /// The context key used through the site for current user.
        /// </summary>
        public const string CurrentUserContextKey = "__current_user__";

        /// <summary>
        /// Whether or not to show the inactive logout pompt to users for the website.
        /// </summary>
        [BsonIgnoreIfNull]
        public bool? ShowInactiveLogoutPrompt { get; set; }

        /// <summary>
        /// This is the amount of inactive time in minutes allowed on the website, before a user is logged out.
        /// </summary>
        [BsonIgnoreIfNull]
        [Range(0, int.MaxValue)]
        public int? InactiveLogoutPeriod { get; set; }

        /// <summary>
        /// Whether or not to show the inactive logout pompt to users for the self-service section of the webiste.
        /// </summary>
        [BsonIgnoreIfNull]
        public bool? ShowSelfServiceInactiveLogoutPrompt { get; set; }

        /// <summary>
        /// This is the amount of inactive time in minutes allowed on the self-service portion of the website, before a user is logged out.
        /// </summary>
        [BsonIgnoreIfNull]
        [Range(0, int.MaxValue)]
        public int? SelfServiceInactiveLogoutPeriod { get; set; }

        public bool EnableSelfServiceRegistration { get; set; }

        /// <summary>
        /// Whether the login page is disabled. <para />
        /// Only for SSO Customers
        /// </summary>
        public bool DisableLoginPage { get; set; }

        /// <summary>
        /// Whether the ESS login page is disabled. <para/>
        /// Only for SSO Customers also paying for ESS
        /// </summary>
        public bool DisableESSLoginPage { get; set; }

        /// <summary>
        /// The text to display when the login page is disabled.
        /// </summary>
        public string DisabledLoginPageText { get; set; }

        /// <summary>
        /// The default inactive logout period from the configuration manager.
        /// </summary>
        public static int DefaultInactiveLogoutPeriod
        {
            get
            {
                var inactiveLogoutPeriod = Convert.ToInt32(ConfigurationManager.AppSettings["InactiveLogoutPeriod"]);
                return inactiveLogoutPeriod > 0
                    ? inactiveLogoutPeriod
                    : Convert.ToInt32(FormsAuthentication.Timeout.TotalMinutes);
            }
        }

        /// <summary>
        /// The default self-service inactive logout period from the configuration manager.
        /// </summary>
        public static int DefaultSelfServiceInactiveLogoutPeriod
        {
            get
            {
                var selfServiceInactiveLogoutPeriod = Convert.ToInt32(ConfigurationManager.AppSettings["SelfServiceInactiveLogoutPeriod"]);
                return selfServiceInactiveLogoutPeriod > 0
                    ? selfServiceInactiveLogoutPeriod
                    : Convert.ToInt32(FormsAuthentication.Timeout.TotalMinutes);
            }
        }

        /// <summary>
        /// Returns the default value to use for security settings on the website in the event the customer has not set their own values.
        /// </summary>
        /// <returns>A SecuritySettings object with default configuration values.</returns>
        public static SecuritySettings Default()
        {
            return new SecuritySettings
            {
                ShowInactiveLogoutPrompt = true,
                InactiveLogoutPeriod = DefaultInactiveLogoutPeriod,
                ShowSelfServiceInactiveLogoutPrompt = true,
                SelfServiceInactiveLogoutPeriod = DefaultSelfServiceInactiveLogoutPeriod
            };
        }

        /// <summary>
        /// Gets the current user from a given context.
        /// If a user is provided, it is placed in the context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="user">The user.</param>
        /// <returns>A User object.</returns>
        public static User GetCurrentUser(HttpContextBase context, User user = null)
        {
            // If the user was not passed in and needs to be figured out.
            if (user != null)
            {
                context.Items[CurrentUserContextKey] = user;
                return user;
            }

            if (context.Items == null
                || string.IsNullOrWhiteSpace(context.User.Identity.Name))
                return null;

            // Look in the context items first.
            user = context.Items[CurrentUserContextKey] as User;
            if (user != null)
                return user;

            if (!(context.User.Identity is ClaimsIdentity identity))
            {
                return null;
            };
            var key = identity.FindFirstValue("Key");
            // Otherwise get the user from the database and set the context.
            if (key != null)
            {
                user = User.Repository.GetById(key);
                context.Items[CurrentUserContextKey] = user;
                return user;
            }
            return null;
        }

        /// <summary>
        /// Gets the security settings for the current logged in user or default settings.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="user">The user.</param>
        /// <returns>A SecuritySettings object.</returns>
        public static SecuritySettings GetSecuritySettings(HttpContextBase context, User user = null)
        {
            var securitySettings = Default();

            // If the user is not authenticated and in the context return default settings.
            if (context == null
                || context.User == null
                || context.User.Identity == null
                || !context.User.Identity.IsAuthenticated
                || !context.Request.IsAuthenticated)
                return securitySettings;

            // Get the current user.
            user = GetCurrentUser(context, user);

            // If the user still cannot be found or wasn't set, return default settings.
            if (user == null)
                return securitySettings;

            // Grab the customer.
            var customer = user.Customer;
            if (customer == null)
                return securitySettings;

            // Grab the customer's security settings or return default settings if there are none set.
            var customerSecuritySettings = customer.SecuritySettings;
            if (customerSecuritySettings == null)
                return securitySettings;

            // Where there are values, override the defaults.
            if (customerSecuritySettings.ShowInactiveLogoutPrompt != null)
                securitySettings.ShowInactiveLogoutPrompt = customerSecuritySettings.ShowInactiveLogoutPrompt;

            if (customerSecuritySettings.InactiveLogoutPeriod != null)
                securitySettings.InactiveLogoutPeriod = customerSecuritySettings.InactiveLogoutPeriod;

            if (customerSecuritySettings.ShowSelfServiceInactiveLogoutPrompt != null)
                securitySettings.ShowSelfServiceInactiveLogoutPrompt =
                    customerSecuritySettings.ShowSelfServiceInactiveLogoutPrompt;

            if (customerSecuritySettings.SelfServiceInactiveLogoutPeriod != null)
                securitySettings.SelfServiceInactiveLogoutPeriod =
                    customerSecuritySettings.SelfServiceInactiveLogoutPeriod;

            return securitySettings;
        }

        /// <summary>
        /// Gets the current timeout period as an integer.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="user">The user.</param>
        /// <returns>An integer value.</returns>
        public static int GetCurrentTimeoutPeriod(HttpContextBase context, User user = null)
        {
            var securitySettings = GetSecuritySettings(context);

            if (securitySettings == null)
                return DefaultInactiveLogoutPeriod;

            // If the user is not authenticated and in the context return default inactive logout period.
            if (context == null
                || context.User == null
                || context.User.Identity == null
                || !context.User.Identity.IsAuthenticated
                || !context.Request.IsAuthenticated)
                return Convert.ToInt32(securitySettings.InactiveLogoutPeriod);

            // Get the current user.
            user = GetCurrentUser(context, user);

            if (user == null)
                return DefaultInactiveLogoutPeriod;

            // If the user is in the self-service portal return the self-service logout period.
            if (User.IsEmployeeSelfServicePortal && user.Customer.HasFeature(Feature.ESSDataVisibilityInPortal))
                return Convert.ToInt32(securitySettings.SelfServiceInactiveLogoutPeriod);

            // Return the user's inactive logout period (or default value).
            return Convert.ToInt32(securitySettings.InactiveLogoutPeriod);
        }

        /// <summary>
        /// Gets the current timeout period as a TimeSpan.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="user">The user.</param>
        /// <returns>A TimeSpan value.</returns>
        public static TimeSpan GetCurrentTimeoutPeriodAsTimeSpan(HttpContextBase context, User user = null)
        {
            var currentTimeoutPeriod = GetCurrentTimeoutPeriod(context, user);
            return new TimeSpan(0, 0, currentTimeoutPeriod, 1); // Add an extra second for wiggle room.
        }

        /// <summary>
        /// Create and set a new authentication ticket.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="user">The user.</param>
        /// <returns>The HttpCookie object if it was created, otherwise null.</returns>
        public static HttpCookie CreateAuthenticationTicket(HttpContextBase context, User user)
        {
            if (context == null || user == null || user.IsDeleted)
                return null;

            // Set authentication cookie with default values.
            // They will be overriden on next page request with custom values on the redirect.
            FormsAuthentication.SetAuthCookie(user.Id, false);
            return context.Request.Cookies[FormsAuthentication.FormsCookieName];
        }

        /// <summary>
        /// Renews an existing authentication ticket.
        /// Note: If the Web page is accessed before half of the expiration time passes,
        /// the ticket expiration time will not be reset.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="forceRenew">Force renewal even if breaking the half time rule.</param>
        /// <returns>The HttpCookie object if it was obtainable or renewed, otherwise null.</returns>
        public static HttpCookie RenewAuthenticationTicket(HttpContextBase context, bool forceRenew = false)
        {
            // Grab the current authentication cookie.
            var authenticationTicketCookie = context.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (!context.Request.IsAuthenticated || authenticationTicketCookie == null)
                return null;

            var authenticationTicket = FormsAuthentication.Decrypt(authenticationTicketCookie.Value);

            // This triggers the regular sliding expiration functionality.
            // var newAuthTicket = FormsAuthentication.RenewTicketIfOld(oldAuthTicket);
            // Hover FormsAuthentication.RenewTicketIfOld(oldAuthTicket) is based on Timeout value in web.config
            // and we are dealing with custom sliding timeout values in either the web.config and the database.
            // Since we have to do custom updating, we will at least abide by the half time rule for
            // timeout periods over 5 minutes:
            // https://support.microsoft.com/en-us/kb/910443
            var now = DateTime.Now; // ticket expirations are declared in local datetime...
            if (authenticationTicket == null || authenticationTicket.Expired || authenticationTicket.Expiration <= now)
                return null; // Ticket doesn't exist or is expired.

            var ticketTimeoutPeriod = authenticationTicket.Expiration - authenticationTicket.IssueDate;
            var currentTimeoutPeriod = GetCurrentTimeoutPeriodAsTimeSpan(context);
            if (ticketTimeoutPeriod != currentTimeoutPeriod)
                forceRenew = true; // Even if the time period is more gracious.

            if (currentTimeoutPeriod.TotalMinutes < 5)
                forceRenew = true; // May effect performance but skip the half time rule.

            // Do renewal math calculations.
            var originationGap = now - authenticationTicket.IssueDate;
            var halfWayPeriod = Math.Floor(currentTimeoutPeriod.TotalMinutes / 2);
            var halfWayGap = now.Subtract(now.AddMinutes(-halfWayPeriod));
            var performSlide = originationGap < halfWayGap;

            if (!forceRenew && !performSlide)
                return null;

            // Create new authentication ticket
            var newAuthenticationTicket = new FormsAuthenticationTicket(
                authenticationTicket.Version,
                authenticationTicket.Name,
                now, // IssueDate
                now.Add(currentTimeoutPeriod), // Expiration
                authenticationTicket.IsPersistent,
                authenticationTicket.UserData,
                authenticationTicket.CookiePath);

            // Set authentication cookie values.
            authenticationTicketCookie.Value = FormsAuthentication.Encrypt(newAuthenticationTicket);
            authenticationTicketCookie.Expires = newAuthenticationTicket.Expiration;

            // Add the renewed authentication ticket cookie to the response.
            context.Response.Cookies.Set(authenticationTicketCookie);
            return authenticationTicketCookie;
        }

        /// <summary>
        /// Determines wether or not the Timeout dialog should be shown to the user for a given session.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>True if the Timeout dialog should be displayed, otherwise false.</returns>
        public static bool ShowTimeoutDialog(HttpContextBase context, DateTime expiry)
        {
            var now = DateTime.UtcNow;
            if (!context.Request.IsAuthenticated || expiry <= now)
            {
                return false;
            }
            // Get the current user.
            var user = GetCurrentUser(context);
            if (user == null)
            {
                return false;
            }
            var securitySettings = GetSecuritySettings(context, user);

            // If the user is in the self-service portal return make sure they want to show a logout dialog.
            if (User.IsEmployeeSelfServicePortal
                && user.Customer.HasFeature(Feature.ESSDataVisibilityInPortal)
                && (securitySettings.ShowSelfServiceInactiveLogoutPrompt == null
                    || securitySettings.ShowSelfServiceInactiveLogoutPrompt == false))
                return false;

            if (securitySettings.ShowInactiveLogoutPrompt == null
                || securitySettings.ShowInactiveLogoutPrompt == false)
                return false;

            // Do math to show dialog for timeouts under 3 minutes.
            var currentTimeoutPeriod = GetCurrentTimeoutPeriod(context);
            if (currentTimeoutPeriod < 3)
                return false;

            // Scale timeout settings to be longer as more time is given to a session up to 10 minutes.
            var timeRemaining = Convert.ToInt32(Math.Ceiling((expiry - now).TotalMinutes));
            if (timeRemaining <= 2)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Determines wether or not the Timeout dialog should display the time only or the date and time.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>True if the Timeout dialog should display time only, otherwise false and display date and time.</returns>
        public static bool WillExpireSameDayOrWithinNextMeridiem(HttpContextBase context, DateTime expiry)
        {
            var now = DateTime.UtcNow;
            if (!context.Request.IsAuthenticated || expiry <= now)
            {
                return false;
            }

            // Get the current user.
            var user = GetCurrentUser(context);
            if (user == null)
                return true;

            var securitySettings = GetSecuritySettings(context, user);

            // If the user is in the self-service portal return make sure they want to show a logout dialog.
            if (User.IsEmployeeSelfServicePortal
                && user.Customer.HasFeature(Feature.ESSDataVisibilityInPortal)
                && (securitySettings.ShowSelfServiceInactiveLogoutPrompt == false
                    || securitySettings.ShowSelfServiceInactiveLogoutPrompt == null))
                return true;

            if (securitySettings.ShowInactiveLogoutPrompt == null
                || securitySettings.ShowInactiveLogoutPrompt == false)
                return true;

            var expirationDate = expiry;

            // If the expiration date is within the same day return true.
            if (now.Year == expirationDate.Year
                && now.Month == expirationDate.Month
                && now.Day == expirationDate.Day)
                return true;

            // Will the cookie expire within a 6 hour window, or will it flop to the next meridiem?
            var timeRemaining = Math.Ceiling((expirationDate - now).TotalMinutes);
            return timeRemaining < new TimeSpan(6, 0, 0).TotalMinutes;
        }
    }
}
