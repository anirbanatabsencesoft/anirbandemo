﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Customers
{
    [Serializable]
    public class ConsultationType : BaseCustomerEntity<ConsultationType>
    {
        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        [BsonRequired]
        public string Consult { get; set; }

        [BsonRepresentation(BsonType.ObjectId), BsonIgnoreIfNull]
        public string EmployerId { get; set; }

        [BsonIgnore, JsonIgnore]
        public Employer Employer
        {
            get
            {
                return this.GetReferenceValue<Employer>(this.EmployerId);
            }
            set
            {
                this.EmployerId = SetReferenceValue<Employer>(value);
            }
        }
    }
}
