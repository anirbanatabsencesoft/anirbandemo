﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoRepository;
using AbsenceSoft.Data.Enums;
using Newtonsoft.Json;
using AbsenceSoft.Data.Rules;

namespace AbsenceSoft.Data.Customers
{
    [Serializable]
    public class PaySchedule : BaseEmployerEntity<PaySchedule>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PaySchedule"/> class.
        /// </summary>
        public PaySchedule()
        {
        }

        /// <summary>
        /// Timespan of the pay period
        /// </summary>
        [BsonRequired]
        public PayPeriodType PayPeriodType { get; set; }

        /// <summary>
        /// day of the start and end. If weekly or bi-weekly
        /// then it is the day of the week
        /// 
        /// if it is semi monthly then it is 1 for start of month or a number for the date (15th or 16th)
        /// </summary>
        [BsonRequired]
        public int DayStart { get; set; }
        
        /// <summary>
        /// day of the end of the week. If weekly or bi-weekly it is
        /// the day of the week
        /// 
        /// semi-monthly then end date, if the end of the month use 31
        /// when this is semi monthly the second payroll date is implied as
        /// the end day + 1 thru to the end of the month
        /// </summary>
        [BsonRequired]
        public int DayEnd { get; set; }

        /// <summary>
        /// The number of days after the close of the cycle to process payroll
        /// </summary>
        [BsonRequired]
        public int ProcessingDays { get; set; }

        /// <summary>
        /// Sets whether this PaySchedule is the employers default pay schedules or not
        /// </summary>
        [BsonIgnore]
        public bool Default { get; set; }

        /// <summary>
        /// Start date for the payperiod, must be a cycle start date
        /// </summary>
        [BsonRequired]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// unique name to identify the schedule
        /// 
        /// e.g. standard bi-weekly, Union123 base 
        /// </summary>
        [BsonRequired]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the assignment rules.
        /// </summary>
        /// <value>
        /// The assignment rules.
        /// </value>
        [BsonIgnoreIfNull]
        public RuleGroup AssignmentRules { get; set; }
    }
}
