﻿using AbsenceSoft.Common;
using AbsenceSoft.Common.Properties;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoRepository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Data.Customers
{
 /// <summary>
 /// Customer Entity
    /// </summary>
    [Serializable]
    public class Customer : BaseEntity<Customer>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Customer"/> class.
        /// </summary>
        public Customer()
        {
            SuppressReasons = new List<string>();
            SuppressPolicies = new List<string>();
            SuppressWorkflows = new List<string>();
            SuppressedEntities = new List<SuppressedEntity>();
            SuppressAccommodationTypes = new List<string>();
            PasswordPolicy = new PasswordPolicy();
        }

        /// <summary>
        /// Gets or sets the customer name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the customer's custom Url for accessing the site.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Gets or sets the customer's contact information.
        /// </summary>
        public Contact Contact { get; set; }

        /// <summary>
        /// Gets or sets the logo id for this customer.
        /// </summary>
        [BsonRepresentation(BsonType.ObjectId)]
        public string LogoId { get; set; }

        /// <summary>
        /// Gets or sets the customer's API Key that grants access to the REST API.
        /// </summary>
        [BsonIgnoreIfNull]
        public CryptoString ApiKey { get; set; }

        /// <summary>
        /// Gets or sets the consumer's API Secret that is basically their password for the REST API.
        /// </summary>
        [BsonIgnoreIfNull]
        public CryptoString ApiSecret { get; set; }

        /// <summary>
        /// Gets or sets the customers license expiration date
        /// </summary>
        [BsonIgnoreIfNull]
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime? ExpirationDate { get; set; }

        /// <summary>
        /// Gets or sets the customer's billing contact information if different than the primary contact information.
        /// </summary>
        [BsonIgnoreIfNull]
        public Contact BillingContact { get; set; }

        /// <summary>
        /// Gets or sets the customer's tax id.
        /// </summary>
        [BsonIgnoreIfNull]
        public CryptoString TaxId { get; set; }

		/// <summary>
        /// Get or set customer's password policy
        /// </summary>
        [BsonIgnoreIfNull]
        public PasswordPolicy PasswordPolicy { get; set; }
        
        /// <summary>
        /// Get or set customer's security settings
        /// </summary>
        [BsonIgnoreIfNull]
        public SecuritySettings SecuritySettings { get; set; }

        /// <summary>
		/// Gets or sets the features that the customer is subscribed to which help determine billing, and UI interactivity, etc.
		/// </summary>
		public List<AppFeature> Features { get; set; }

		/// <summary>
		/// Gets or Sets the customer's default From field when sending communications via email
		/// </summary>
		[BsonIgnoreIfNull]
        public string DefaultEmailFromAddress { get; set; }

        /// <summary>
        /// Gets or sets the type of the email replies.
        /// </summary>
        /// <value>
        /// The type of the email replies.
        /// </value>
        [BsonIgnoreIfNull]
        public EmailReplies? EmailRepliesType { get; set; }

        [BsonIgnoreIfNull]
        public string EmailDomain { get; set; }

        /// <summary>
        /// Gets or sets the collection absence reason codes that should be suppressed.
        /// </summary>
        public List<string> SuppressReasons { get; set; }

        /// <summary>
        /// Gets or sets the policies that should be suppressed for only manual selection (not auto-selection).
        /// </summary>
        public List<string> SuppressPolicies { get; set; }

        /// <summary>
        /// Gets or sets the workflows that should be suppressed for this employer.
        /// </summary>
        /// <value>
        /// The suppress workflow codes.
        /// </value>
        public List<string> SuppressWorkflows { get; set; }

        /// <summary>
        /// Gets or sets the accommodation types that should be suppressed for this customer
        /// </summary>
        public List<string> SuppressAccommodationTypes { get; set; }

        public List<SuppressedEntity> SuppressedEntities { get; set; }

        /// <summary>
        /// Gets or sets to do new user email template identifier.
        /// </summary>
        /// <value>
        /// To do new user email template identifier.
        /// </value>
        [BsonIgnoreIfNull]
        public string NewUserEmailTemplateCode { get; set; }
        

        /// <summary>
        /// Gets or sets to do template identifier.
        /// </summary>
        /// <value>
        /// To do template identifier.
        /// </value>
        [BsonIgnoreIfNull]
        public string ToDoTemplateCode { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether [enable ip filter].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [enable ip filter]; otherwise, <c>false</c>.
        /// </value>
        public bool EnableIpFilter { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether customer wants to skip displaying 'Contact Preferences' section on new Case.
        /// </summary>
        /// <value>
        /// <c>true</c> if specific customer wants to skip this section; normally, <c>false</c>.
        /// </value>
        public bool HideContactPreferences { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a customers wants to default to giving access to all employers for all users
        /// </summary>
        /// <value>
        /// <c>true</c> if specific customer wants users to have access to all employers by default; nomrall, <c>false</c>
        /// </value>
        public bool EnableAllEmployersByDefault { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a customer wants to default new note confidential selection to true
        /// </summary>
        /// <value>
        /// <c>true</c> if specific customer wants users to default new note confidential selection to true; normally, <c>false</c>
        /// </value>
        public bool DefaultNoteToConfidential { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a customer wants to default new communication confidential selection to true
        /// </summary>
        /// <value>
        /// <c>true</c> if specific customer wants users to default new communication confidential selection to true; normally, <c>false</c>
        /// </value>
        public bool DefaultCommunicationToConfidential { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a customer wants to default new attachment confidential selection to true
        /// </summary>
        /// <value>
        /// <c>true</c> if specific customer wants users to default new attachment confidential selection to true; normally, <c>false</c>
        /// </value>
        public bool DefaultAttachmentToConfidential { get; set; }

        /// <summary>
        /// Gets the customer's formatted base Url for accessing the site, the customer's site root basically.
        /// </summary>
        /// <param name="ess">if set to <c>true</c> [ess].</param>
        /// <returns>
        /// A URL representing this customer
        /// </returns>
        public string GetBaseUrl(bool ess = false)
        {
            string subDomain = string.IsNullOrWhiteSpace(Url) ? Url : string.Concat(Url, ".");
            return string.Format(ess ? Settings.Default.BaseUrlEss : Settings.Default.BaseUrl, subDomain);
        }

        /// <summary>
        /// Overrides the base OnSaving event to first pre-populate the ApiKey and ApiSecret if these are not already populated.
        /// </summary>
        /// <returns></returns>
        protected override bool OnSaving()
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(this.Url))
                    this.Url = this.Url.ToLowerInvariant();

                if (this.ApiKey == null)
                    this.ApiKey = new CryptoString(RandomString.GenerateKey(24));
                else
                    this.ApiKey.Encrypt().Hash();

                if (this.ApiSecret == null)
                    this.ApiSecret = new CryptoString(RandomString.GenerateSecret());
                else
                    this.ApiSecret.Encrypt().Hash();

                if (this.TaxId != null)
                    this.TaxId.Encrypt().Hash();
            }
            catch (Exception ex)
            {
                Log.Error("Error creating ApiKey and ApiSecret for Customer", ex);
                return false;
            }

            return base.OnSaving();
        }

        /// <summary>
        /// Gets a customer by its URL. Useful for DNS reverse lookups for a customer (w/o an override).
        /// </summary>
        /// <param name="url">The URL (host) name for the customer.</param>
        /// <returns></returns>
        public static Customer GetByUrl(string url)
        {
            using (new InstrumentationContext("Customer.GetByUrl"))
            {
                string myUrl = url.ToLowerInvariant();
                var cust = Customer.AsQueryable().FirstOrDefault(c => c.Url == myUrl && !c.IsDeleted);
                if (cust == null)
                {
                    var emp = Employer.GetByUrl(url);
                    if (emp != null)
                        return emp.Customer;
            }
                return cust;
        }
        }

        /// <summary>
        /// Gets or sets the current customer in the operating context.
        /// </summary>
        [Obsolete("Use Current.Customer() in AbsenceSoft.Web instead.  If not in AbsenceSoft project or AbsenceSoft.SelfService, pass the context in, don't add a reference to those methods")]
        public static Customer Current
        {
            get
            {
                if (User.Current != null && User.Current.Customer != null)
                    return User.Current.Customer;

                if (HttpContext.Current != null && HttpContext.Current.Items != null)
                    return HttpContext.Current.Items["__customer"] as Customer;

                return null;
            }
            set
            {
                if (User.Current != null && value != null && User.Current.CustomerId != value.Id)
                    return;
                if (HttpContext.Current != null && HttpContext.Current.Items != null)
                    HttpContext.Current.Items["__customer"] = value;
            }
        }

		/// <summary>
		/// Determines whether or not this customer has a given feature enabled.
		/// </summary>
		/// <param name="feature">The feature to check.</param>
		/// <returns><c>true</c> if the feature is present, otherwise <c>false</c>.</returns>
		public bool HasFeature(Feature feature)
		{
			if (Features != null && Features.Count > 0)
			{

                AppFeature ef = Features.FirstOrDefault(f => f.Feature == feature);

				if (ef != null)
				{
					if (ef.Enabled && (!ef.EffectiveDate.HasValue || (ef.EffectiveDate.HasValue && ef.EffectiveDate.Value <= DateTime.Now)) && (!ef.IneffectiveDate.HasValue || (ef.IneffectiveDate.HasValue && ef.IneffectiveDate.Value > DateTime.Now)))
						return true;
					else
						return false;
				}
			}

			return false;
		}

        /// <summary>
        /// Adds the feature.
        /// </summary>
        /// <param name="feature">The feature.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <param name="effectiveDate">The effective date.</param>
        /// <param name="ineffectiveDate">The ineffective date.</param>
        public void AddFeature(Feature feature, bool enabled = true, DateTime? effectiveDate = null, DateTime? ineffectiveDate = null)
        {
            if (Features == null)
                Features = new List<AppFeature>();

            AppFeature ef = Features.FirstOrDefault(f => f.Feature == feature);
            if (ef == null)
                ef = Features.AddFluid(new AppFeature() { Feature = feature });
            ef.Enabled = enabled;
            ef.EffectiveDate = effectiveDate;
            ef.IneffectiveDate = ineffectiveDate;
        }

        /// <summary>
        /// Removes the feature.
        /// </summary>
        /// <param name="feature">The feature.</param>
        /// <param name="ineffectiveDate">The ineffective date.</param>
        public void RemoveFeature(Feature feature, DateTime? ineffectiveDate = null)
        {
            AddFeature(feature, false, null, ineffectiveDate);
        }

		/// <summary>
		/// Get List of Features with their status
		/// </summary>
		/// <returns></returns>
		public IDictionary<Feature, bool> GetFeatureListWithStatus()
		{
			IDictionary<Feature, bool> list = new Dictionary<Feature, bool>();

			if (Features != null)
			{
				foreach (AppFeature feature in Features)
				{
					if (feature.Enabled && (!feature.EffectiveDate.HasValue || (feature.EffectiveDate.HasValue && feature.EffectiveDate.Value <= DateTime.Now)) && (!feature.IneffectiveDate.HasValue || (feature.IneffectiveDate.HasValue && feature.IneffectiveDate.Value > DateTime.Now)))
						list.Add(feature.Feature, true);
					else
						list.Add(feature.Feature, false);
				}
			}

			var m = Enum.GetValues(typeof(Feature)).Cast<Feature>().Except(list.Select(o => o.Key)).Where(f => f != Feature.None).Select(o => new KeyValuePair<Feature, bool>(o, false)).ToList();
			m.ForEach(i => list.Add(i));


			return list;
		}

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Concat(Name, " (", Url, ")");
        }

        public bool HasMoreThanOneEmployer()
        {
            return Employer.AsQueryable().Count(e => e.CustomerId == this.Id) > 1;
        }

        public bool HasSSO()
        {
            return SsoProfile.AsQueryable().Count(e => e.CustomerId == Id) > 0;
        }

    }
}
