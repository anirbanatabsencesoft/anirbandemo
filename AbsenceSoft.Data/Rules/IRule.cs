﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Rules
{
    // yes I do
    public interface IRule
    {
        /// <summary>
        /// Gets or sets the expression in plain English text for an end user's benefit or
        /// gives the rule a plain-text name for display.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets or sets the description of the rule in plain English text that will explain to
        /// the end user the details of how the rule works
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// Gets or sets the left-hand side of the expression to evaluate the rule with
        /// (see AbsenceSoft.Logic.Expressions).
        /// </summary>
        /// <remarks>
        /// The left-hand side of the expression should ALWAYS base property evaluations from the LeaveOfAbsence
        /// class as an instance and use dot-notation to access properties or methods thereto.
        /// </remarks>
        string LeftExpression { get; set; }

        /// <summary>
        /// Gets or sets the operator for the expression which sits in the middle between the right and left
        /// hand side expressions. Must be a C# operator, e.g. ==, !=, &gt;, &gt;=, &lt;, &lt;=, |=, &amp;=, etc.
        /// If using a Boolean method or property value to evaluate in the left side expression then 
        /// the operator should be a boolean == or != operator and the right-hand expression should be either
        /// <c>true</c> or <c>false</c>.
        /// </summary>
        string Operator { get; set; }

        /// <summary>
        /// Gets or sets the right-hand side of an expression for the actual value to report back to the UI/User and store
        /// as the reason for pass/fail. This is stored separately because it would be hard to always get
        /// the proper right or left side of the expression for measuring this
        /// (see AbsenceSoft.Logic.Expressions).
        /// </summary>
        /// <remarks>
        /// The value expression should ALWAYS base property evaluations from the LeaveOfAbsence class as an instance
        /// and use dot-notation to access properties or methods thereto.
        /// </remarks>
        string RightExpression { get; set; }

        /// <summary>
        /// The result from evaluating the rule
        /// </summary>
        AppliedRuleEvalResult Result { get; set; }

        /// <summary>
        /// Gets a value indicating whether or not the rule evaluation failed. This is not always the converse of Fail.
        /// </summary>
        bool Pass { get; }

        /// <summary>
        /// Gets a value indicating whether or not the rule evaluation failed. This is not always the converse of Pass.
        /// </summary>
        bool Fail { get; }

        /// <summary>
        /// If the rule has not been run or there was an error the result will be unknown
        /// </summary>
        bool NotRunOrError { get; }

        /// <summary>
        /// The value from the left hand side of the expression
        /// </summary>
        object ActualValue { get; set; }

        /// <summary>
        /// Error messages from execution of the rule
        /// </summary>
        List<string> ErrorMessages { get; set; }


    }
}
