﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Data.Rules
{
    /// <summary>
    /// Represents a grouping of rules and how those rules must suceed or fail
    /// </summary>
    [Serializable]
    public class RuleGroup : BaseNonEntity
    {
        public RuleGroup()
        {
            // Defaults
            SuccessType = RuleGroupSuccessType.And;
            Rules = new List<Rule>();
        }

        /// <summary>
        /// Gets or sets the expression in plain English text for an end user's benefit or
        /// gives the rule group a plain-text name for display.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description of the rule group in plain English text that will explain to
        /// the end user the details of how the rule group works
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the list of Rules that apply to this group.
        /// </summary>
        public virtual List<Rule> Rules { get; set; }

        /// <summary>
        /// Gets or sets the success evaluation type for the entire rule group itself.
        /// </summary>
        public RuleGroupSuccessType SuccessType { get; set; }

        /// <summary>
        /// Has this expression group been evaluated yet? (If you are going to include Pass then you need to know
        /// if it has been evaluated)
        /// </summary>
        public virtual bool HasBeenEvaluated { get; set; }

        /// <summary>
        /// Gets a value indicating whether this rule group has passed or not.
        /// </summary>
        public virtual bool Pass
        {
            get
            {
                if (!HasBeenEvaluated)
                    return false;

                switch (SuccessType)
                {
                    default:
                    case RuleGroupSuccessType.And:
                        return Rules.All(r => r.Pass);
                    case RuleGroupSuccessType.Or:
                        return Rules.Any(r => r.Pass);
                    case RuleGroupSuccessType.Not:
                        return Rules.All(r => r.Fail);
                    case RuleGroupSuccessType.Nor:
                        return Rules.Any(r => r.Fail);
                }
            }
            set { /*Empty for serialization purposes. But it would be nice having this in MongoDB and ElasticSearch directly :-)*/ }
        }

    }
}
