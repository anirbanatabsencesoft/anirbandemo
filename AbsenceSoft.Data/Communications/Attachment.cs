﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Communications
{
    [Serializable]
    public class Attachment : BaseEmployeeEntity<Attachment>
    {
        /// <summary>
        /// Gets or sets the case id.
        /// </summary>
        /// <value>
        /// The case id.
        /// </value>
        [BsonRequired, BsonRepresentation(BsonType.ObjectId)]
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the file id.
        /// </summary>
        /// <value>
        /// The file id.
        /// </value>
        [BsonRequired, BsonRepresentation(BsonType.ObjectId)]
        public string FileId { get; set; }

        /// <summary>
        /// Gets or sets the type of the attachment.
        /// </summary>
        /// <value>
        /// The type of the attachment.
        /// </value>
        [BsonRequired]
        public AttachmentType AttachmentType { get; set; }

        /// <summary>
        /// Gets or sets the type of the content.
        /// </summary>
        /// <value>
        /// The type of the content.
        /// </value>
        public string ContentType { get; set; }

        /// <summary>
        /// Gets or sets the length of the content.
        /// </summary>
        /// <value>
        /// The length of the content.
        /// </value>
        public long ContentLength { get; set; }

        /// <summary>
        /// Gets or sets the name of the file.
        /// </summary>
        /// <value>
        /// The name of the file.
        /// </value>
        [BsonRequired]
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [BsonIgnoreIfNull]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the file.
        /// </summary>
        /// <value>
        /// The file.
        /// </value>
        [BsonIgnore]
        public byte[] File { get; set; }

        /// <summary>
        /// Gets or sets the attachment's case.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Case Case
        {
            get { return GetReferenceValue<Case>(this.CaseId); }
            set { CaseId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the document.
        /// </summary>
        /// <value>
        /// The document.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public Document Document
        {
            get { return GetReferenceValue<Document>(this.FileId); }
            set { FileId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the name of the created by.
        /// </summary>
        /// <value>
        /// The name of the created by.
        /// </value>
        public string CreatedByName { get; set; }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        /// <returns></returns>
        public override Attachment Save()
        {
            if (IsNew) CreatedByName = User.Current == null ? null : User.Current.DisplayName;
            return base.Save();
        }

        /// <summary>
        /// Sets whether this Attachment is public or not
        /// </summary>
        [BsonDefaultValue(true)]
        public bool Public { get; set; }
    }
}
