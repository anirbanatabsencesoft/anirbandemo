﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Communications
{
    /// <summary>
    /// Represents a group of paperwork criteria applied using the specified criteria rule.
    /// </summary>
    [Serializable]
    public class PaperworkCriteriaGroup : BaseNonEntity
    {
        public PaperworkCriteriaGroup()
        {
            SuccessType = RuleGroupSuccessType.And;
            Criteria = new List<PaperworkCriteria>();
        }

        /// <summary>
        /// Gets or sets the expression in plain English text for an end user's benefit or
        /// gives the rule group a plain-text name for display.
        /// </summary>
        [BsonIgnoreIfNull]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description of the rule group in plain English text that will explain to
        /// the end user the details of how the rule group works
        /// </summary>
        [BsonIgnoreIfNull]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the rules collection for this rule group that will each be
        /// evaluated and the used to either pass or fail the group based on <c>SuccessType</c>.
        /// </summary>
        public List<PaperworkCriteria> Criteria { get; set; }

        /// <summary>
        /// Gets or sets the success evaluation type for the entire rule group itself.
        /// </summary>
        public RuleGroupSuccessType SuccessType { get; set; }
    }
}
