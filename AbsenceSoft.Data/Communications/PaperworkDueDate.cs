﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Security;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using MongoDB.Bson;

namespace AbsenceSoft.Data.Communications
{
    [Serializable]
    public class PaperworkDueDate : BaseNonEntity
    {

        /// <summary>
        /// Gets or sets Paperwork due date
        /// </summary>
        public DateTime DueDate { get; set; }


        /// <summary>
        /// Gets or sets when due date is set 
        /// </summary>
        public DateTime DateSet { get; set; }

        /// <summary>
        /// Gets or sets user Id that created this entity
        /// </summary>
        [
            BsonElement("cby"),
            BsonRepresentation(BsonType.ObjectId),
            JsonProperty("cby")
        ]
        public string CreatedById { get; set; }

        /// <summary>
        /// Gets or sets the user account that created this entity.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public User CreatedBy
        {
            get { return CreatedById == User.DefaultUserId ? User.System : GetReferenceValue<User>(CreatedById); }
            set { CreatedById = SetReferenceValue(value); }
        }
    }
}
