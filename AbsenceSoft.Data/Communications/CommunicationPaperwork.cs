﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Security;

namespace AbsenceSoft.Data.Communications
{
    [Serializable]
    public class CommunicationPaperwork : BaseNonEntity
    {
        public CommunicationPaperwork()
        {
            Status = PaperworkReviewStatus.NotApplicable;
            StatusDate = DateTime.UtcNow;
            Fields = new List<PaperworkField>();
        }

        /// <summary>
        /// Gets or sets orignal and new due date of Paperwork history
        /// </summary>  
        public List<PaperworkDueDate> DueDateHistory { get; set; }

        /// <summary>
        /// Gets or sets the ID for the attachment that this paperwork was attached
        /// to the case as.
        /// </summary>
        [BsonRepresentation(BsonType.ObjectId)]
        public string AttachmentId { get; set; }

        /// <summary>
        /// Gets or sets the ID for the attachment that this paperwork was returned,
        /// scaned, uploaded or by other means returned by the employee and re-associated
        /// during paperwork review.
        /// </summary>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string ReturnAttachmentId { get; set; }

        /// <summary>
        /// Gets or sets the status of the paperwork review.
        /// </summary>
        [BsonRequired, BsonDefaultValue(PaperworkReviewStatus.NotApplicable)]
        public PaperworkReviewStatus Status { get; set; }

        [BsonIgnore]
        public DateTime? FirstCasePaperworkDueDate { get; set; } 

        /// <summary>
        /// Gets or sets the date the last status was set.
        /// </summary>
        public DateTime StatusDate { get; set; }

        /// <summary>
        /// Gets or sets the optional due date for when the paperwork is expected to be returned.
        /// </summary>
        [BsonIgnoreIfNull]
        public DateTime? DueDate { get; set; }

        /// <summary>
        /// Gets or sets a copy of the paperwork for this communication.
        /// </summary>
        [BsonIgnoreIfNull]
        public Paperwork Paperwork { get; set; }

        /// <summary>
        /// Gets or sets a collection of fields that are represented by this
        /// paperwork, and if it requires a review, these fields are used
        /// to determine whether or not the paperwork is complete.
        /// </summary>
        [BsonIgnoreIfNull]
        public List<PaperworkField> Fields { get; set; }

        /// <summary>
        /// Gets or sets the communication attachment, if one was created.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Attachment Attachment
        {
            get { return GetReferenceValue<Attachment>(AttachmentId); }
            set { AttachmentId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the communication attachment, if one was created.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Attachment ReturnAttachment
        {
            get { return GetReferenceValue<Attachment>(ReturnAttachmentId); }
            set { ReturnAttachmentId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Set new due date of paperwork and add it in DueDateHistory 
        /// </summary>
        /// <param name="dueDate">The new due date of paperwork.</param>
        /// <param name="user"></param>
        /// <returns>CommunicationPaperwork</returns>
        public CommunicationPaperwork SetDueDate(DateTime? dueDate, User user = null)
        {
            if (!dueDate.HasValue)
            {
                return this;
            }

            DueDate = dueDate;
            if (DueDateHistory == null)
            {
                DueDateHistory = new List<PaperworkDueDate>();
            }
            if (!DueDateHistory.Exists(pd => pd.DueDate == dueDate))
            {
                DueDateHistory.Add(new PaperworkDueDate() { DueDate = dueDate.Value.ToMidnight(), DateSet = DateTime.UtcNow, CreatedById = user?.Id ?? User.Current?.Id ?? User.DefaultUserId });
            }
            return this;
        }

        /// <summary>
        /// Cleans this instance.
        /// </summary>
        public override void Clean()
        {
            base.Clean();
            Status = PaperworkReviewStatus.NotApplicable;
            StatusDate = DateTime.UtcNow;
            ReturnAttachmentId = null;
            if (DueDateHistory != null)
            {
                DueDateHistory.Clear();
            }
            if (Paperwork != null)
                DueDate = Paperwork.DueDateFromNow;
        }
    }
}
