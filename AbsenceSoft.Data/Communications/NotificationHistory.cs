﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Communications
{
    [Serializable]
    /// <summary>
    /// Stores a notification run
    /// </summary>
    /// <seealso cref="AbsenceSoft.Data.BaseEntity{AbsenceSoft.Data.Communications.NotificationHistory}" />
    public class NotificationHistory : BaseEntity<NotificationHistory>
    {

        public NotificationHistory()
        {
            NotificationsSent = new List<UserNotificationHistory>();
        }

        /// <summary>
        /// Gets or sets the notifications sent.
        /// </summary>
        /// <value>
        /// The notifications sent.
        /// </value>
        public List<UserNotificationHistory> NotificationsSent { get; set; }

        /// <summary>
        /// Adds to the notification history for the specified user.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="entityIds">The entity ids.</param>
        public void AddNotificationHistory(string userId, params string[] entityIds)
        {
            UserNotificationHistory userHistory = NotificationsSent.FirstOrDefault(ns => ns.UserId == userId);
            if (userHistory == null)
            {
                NotificationsSent.Add(new UserNotificationHistory(userId, entityIds.ToList()));
            }
            else
            {
                userHistory.EntityIds.AddRange(entityIds);
            }
        }


        /// <summary>
        /// Gets the notification history.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public List<string> GetNotificationHistory(string userId)
        {
            UserNotificationHistory userHistory = NotificationsSent.FirstOrDefault(ns => ns.UserId == userId);
            if (userHistory == null || userHistory.EntityIds == null)
            {
                return new List<string>();
            }

            return userHistory.EntityIds;
        }
    }
}
