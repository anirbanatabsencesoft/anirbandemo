﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Communications
{
    [Serializable]
    public class Template : BaseEmployerOverrideableEntity<Template>
    {
        public Template()
        {
            PaperworkCodes = new List<string>();
        }

        /// <summary>
        /// Gets or Sets the associated document id
        /// </summary>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string DocumentId { get; set; }

        /// <summary>
        /// Gets or sets the associated documents name
        /// </summary>
        [BsonIgnoreIfNull]
        public string DocumentName { get; set; }

        /// <summary>
        /// The type of document this template is
        /// </summary>
        public DocumentType DocType { get; set; }

        /// <summary>
        /// Gets or sets the template name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the subject of the template, typically for Fax cover sheets
        /// or Email subject lines.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the message of the template, typically for Text-SMS
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the body of the template.
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Gets or sets the list of possible paperwork codes for paperwork that may
        /// or may not be included for this communication template.
        /// </summary>
        public List<string> PaperworkCodes { get; set; }

        /// <summary>
        /// Gets or sets the AttachLatestIncompletePaperwork flag for the template.
        /// </summary>
        public bool AttachLatestIncompletePaperwork { get; set; }

        /// <summary>
        /// Gets or sets whether this communication is LOA case specific 
        /// </summary>
        public bool IsLOASpecific { get; set; }

        /// <summary>
        /// Gets or sets the associated Document
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Document Document
        {
            get { return GetReferenceValue<Document>(this.DocumentId); }
            set {
                DocumentId = SetReferenceValue(value);
                DocumentName = value.FileName;
            }
        }
    }
}
