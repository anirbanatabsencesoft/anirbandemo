﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Communications
{
    [Serializable]
    public class Paperwork : BaseEmployerOverrideableEntity<Paperwork>
    {
        public Paperwork()
        {
            DocType = DocumentType.None;
            Criteria = new List<PaperworkCriteriaGroup>();
            Fields = new List<PaperworkField>();
        }

        /// <summary>
        /// Gets or sets the ObjectId of the file as stored in the database
        /// for the actual PDF/document file to be attached.
        /// </summary>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string FileId { get; set; }

        /// <summary>
        /// Gets or sets the paperwork name.
        /// </summary>
        [BsonRequired]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the paperwork file name.
        /// </summary>
        [BsonIgnoreIfNull]
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the paperwork file document type.
        /// </summary>
        public DocumentType DocType { get; set; }

        /// <summary>
        /// Gets or sets the paperwork/enclosure description.
        /// </summary>
        [BsonIgnoreIfNull]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the enclosure text as it would describe this enclosure or
        /// attachment in the body of a communication.
        /// </summary>
        [BsonIgnoreIfNull]
        public string EnclosureText { get; set; }

        /// <summary>
        /// Gets or sets the list of criteria this paperwork requires
        /// in order to be attached or expected back for a given communication.
        /// </summary>
        public List<PaperworkCriteriaGroup> Criteria { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not this paperwork is 
        /// a form that needs to be completed by the Employee or physician and
        /// returned back for review.
        /// </summary>
        [BsonDefaultValue(false), BsonIgnoreIfDefault]
        public bool RequiresReview { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not this paperwork is 
        /// a form that needs to be completed by the Employee or physician and
        /// returned back for review.
        /// </summary>
        [BsonDefaultValue(false), BsonIgnoreIfDefault]
        public bool IsForApprovalDecision { get; set; }


        /// <summary>
        /// Gets or sets the expected return date for the paperwork to be completed
        /// and sent back by the employee for review.
        /// If it is not expected back, no review, then this should be <c>null</c>.
        /// </summary>
        [BsonIgnoreIfNull]
        public TimeSpan? ReturnDateAdjustment { get; set; }

        /// <summary>
        /// Gets or sets ReturnDateAdjustment based on the number of days
        /// Used because MVC doesn't like binding ints to TimeSpan
        /// </summary>
        [BsonIgnore]
        public int? ReturnDateAdjustmentDays
        {
            get
            {
                if (ReturnDateAdjustment == null)
                    return null;

                return Convert.ToInt32(Math.Ceiling(ReturnDateAdjustment.Value.TotalDays));
            }
            set
            {
                if (value != null)
                    ReturnDateAdjustment = TimeSpan.FromDays(value.Value);
                else
                    ReturnDateAdjustment = null;
            }
        }

        /// <summary>
        /// Gets or sets a collection of fields that are represented by this
        /// paperwork, and if it requires a review, these fields are used
        /// to determine whether or not the paperwork is complete.
        /// </summary>
        [BsonIgnoreIfNull]
        public List<PaperworkField> Fields { get; set; }

        /// <summary>
        /// Gets or sets the document.
        /// </summary>
        /// <value>
        /// The document.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public Document Document
        {
            get { return GetReferenceValue<Document>(this.FileId); }
            set { FileId = SetReferenceValue(value); }
        }

        /// <summary>
        /// return the due date for this item, as if we were applying the due date
        /// to right now. Includes logic to:
        ///     If the timespan is less than a day then it is the current time plus the timespan
        ///     If the timespan is longer than a day then assume it is a number of days
        ///         and add that to midnight from today
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public DateTime? DueDateFromNow
        {
            get
            {
                if (!this.ReturnDateAdjustment.HasValue)
                    return null;

                if (this.ReturnDateAdjustment.Value.TotalDays < 1)
                    return DateTime.UtcNow.Add(this.ReturnDateAdjustment.Value);

                return DateTime.UtcNow.ToMidnight().Add(this.ReturnDateAdjustment.Value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [fill PDF].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [fill PDF]; otherwise, <c>false</c>.
        /// </value>
        public bool FillPdf { get; set; }

        /// <summary>
        /// Gets or sets the PDF fields.
        /// </summary>
        /// <value>
        /// The PDF fields.
        /// </value>
        [BsonIgnoreIfNull]
        public Dictionary<string, List<string>> PdfFields { get; set; }
         
    }
}
