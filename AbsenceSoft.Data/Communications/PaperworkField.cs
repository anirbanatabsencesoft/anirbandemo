﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Communications
{
    [Serializable]
    public class PaperworkField : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PaperworkField"/> class.
        /// </summary>
        public PaperworkField() : base()
        {
            Type = PaperworkFieldType.Text;
            Status = PaperworkFieldStatus.Incomplete;
            Required = false;
            Order = null;
        }

        /// <summary>
        /// Gets or sets the field name as displayed in the UI or
        /// would be referenced in other communications, etc. when
        /// referencing the field.
        /// </summary>
        [BsonRequired]
        public string Name { get; set; }

        /// <summary>
        /// Gets or  sets the field status.
        /// </summary>
        public PaperworkFieldStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public PaperworkFieldType Type { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the field is required
        /// to be completed by the employer, physician, etc. and that must be checked
        /// when performing a paperwork review.
        /// </summary>
        [BsonRequired, BsonDefaultValue(false)]
        public bool Required { get; set; }

        /// <summary>
        /// Gets or sets the order.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        public int? Order { get; set; }

        /// <summary>
        /// Gets or sets the actual value supplied by the employee or physician as
        /// entered through form review. For configured fields, this will be null
        /// until placed on a communication.
        /// </summary>
        [BsonIgnoreIfNull]
        public string Value { get; set; }
    }
}
