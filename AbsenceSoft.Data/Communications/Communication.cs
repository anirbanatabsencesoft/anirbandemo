﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Communications
{
    [Serializable]
    public class Communication : BaseEmployeeEntity<Communication>
    {
        public Communication()
        {
            CommunicationType = CommunicationType.Mail;
            Recipients = new List<Contact>();
            CCRecipients = new List<Contact>();
            Paperwork = new List<CommunicationPaperwork>();
            CaseAttachments = new List<Attachment>();
			LastIncompletePaperwork = new List<CommunicationPaperwork>();
            SentDates = new List<DateTime>();
        }

        /// <summary>
        /// Gets or sets the related case Id for the communication.
        /// </summary>
        [BsonRequired, BsonRepresentation(BsonType.ObjectId)]
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the type of the email replies.
        /// </summary>
        /// <value>
        /// The type of the email replies.
        /// </value>
        [BsonIgnoreIfNull]
        public EmailReplies? EmailRepliesType { get; set; }
        /// <summary>
        /// Gets or sets the name of the communication, or subject for emails.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the subject of the communication, typically for Fax cover sheets
        /// or Email subject lines.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the final communication body, in HTML, that
        /// either is used for PDF generation or is the body of an email, etc.
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Gets or sets the template this communication was based on.
        /// </summary>
        /// <value>
        /// The template.
        /// </value>
        public string Template { get; set; }

        /// <summary>
        /// Gets or sets the list of recipients for the communication.
        /// </summary>
        public List<Contact> Recipients { get; set; }

        /// <summary>
        /// Gets or sets the list of CC recipients for the communication.
        /// </summary>
        public List<Contact> CCRecipients { get; set; }

        /// <summary>
        /// Gets or sets the communication type. The default value is <c>Mail</c>.
        /// </summary>
        public CommunicationType CommunicationType { get; set; }

        /// <summary>
        /// Whether or not this communication is a draft
        /// </summary>
        public bool IsDraft { get; set; }

        /// <summary>
        /// The ToDoItem that this Communication is associated with
        /// </summary>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string ToDoItemId { get; set; }

        /// <summary>
        /// Gets or sets the dates the communication was sent. If not sent
        /// or unknown, then this should be <c>null</c> or an empty collection.
        /// </summary>
        /// <remarks>In the event that a communication must be re-sent, this
        /// date will simply get appended to.</remarks>
        [BsonIgnoreIfNull, BsonDateTimeOptions(Kind = DateTimeKind.Utc, Representation = BsonType.DateTime)]        
        public List<DateTime> SentDates { get; set; }

        /// <summary>
        /// Gets or sets the last date this communication was sent. Setting this
        /// value simply adds it to the SentDates collection, it may or may not
        /// represent the latest date in the list.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public DateTime? SentDate
        {
            get
            {
                return SentDates == null || !SentDates.Any() ? null : new DateTime?(SentDates.Max());
            }
            set
            {
                if (SentDates == null)
                    SentDates = new List<DateTime>();
                SentDates.AddIfNotExists(value.Value);
            }
        }

        /// <summary>
        /// Gets or sets the list of paperwork that was sent out with the
        /// communication that is expected back.
        /// </summary>
        public List<CommunicationPaperwork> Paperwork { get; set; }

        /// <summary>
        /// Gets or sets the list of case attachments that was sent out with the
        /// communication that is expected back.
        /// </summary>
        public List<Attachment> CaseAttachments { get; set; }

		/// <summary>
		/// Gets or sets the list of last incomplete paperwork that was sent out
		/// </summary>
		public List<CommunicationPaperwork> LastIncompletePaperwork { get; set; }

        /// <summary>
        /// Gets a value indicating whether or not this communication has any pending paperwork.
        /// </summary>
        [BsonIgnore]
        public bool HasPendingPaperwork
        {
            get
            {
                return Paperwork == null ? false : Paperwork.Any(p => p.Status != PaperworkReviewStatus.NotApplicable && p.Status != PaperworkReviewStatus.Complete);
            }
        }

        /// <summary>
        /// Gets the first due date for any due back paperwork for this communication.
        /// </summary>
        [BsonIgnore]
        public DateTime? FirstPaperworkDueDate
        {
            get
            {
                return Paperwork == null ? null : Paperwork.Where(p => p.DueDate.HasValue).Any() ? Paperwork.Where(p => p.DueDate.HasValue).Min(p => p.DueDate) : null;
            }
        }

		/// <summary>
		/// Get first due date of last incomplete paperwork
		/// </summary>
		[BsonIgnore]
		public DateTime? LastIncompletePaperworkDueDate
		{
			get
			{
				return LastIncompletePaperwork == null ? null : LastIncompletePaperwork.Where(p => p.DueDate.HasValue).Any() ? LastIncompletePaperwork.Where(p => p.DueDate.HasValue).Min(p => p.DueDate) : null;
			}
		}


        /// <summary>
        /// Gets or sets the ID for the attachment that this paperwork was attached
        /// to the case as.
        /// </summary>
        [BsonRepresentation(BsonType.ObjectId)]
        public string AttachmentId { get; set; }

        /// <summary>
        /// Gets or sets the communication's case.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Case Case
        {
            get { return GetReferenceValue<Case>(this.CaseId); }
            set { CaseId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the communication attachment, if one was created.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Attachment Attachment
        {
            get { return GetReferenceValue<Attachment>(AttachmentId); }
            set { AttachmentId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the name of the created by.
        /// </summary>
        /// <value>
        /// The name of the created by.
        /// </value>
        [BsonIgnoreIfNull]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the name of the created by email.
        /// </summary>
        /// <value>
        /// The name of the created by email.
        /// </value>
        [BsonIgnoreIfNull]
        public string CreatedByEmail { get; set; }

        /// <summary>
        /// Gets or sets whether the Communication is public or not
        /// </summary>
        [BsonDefaultValue(true)]
        public bool Public { get; set; }

        /// <summary>
        /// Cleans this instance.
        /// </summary>
        public override void Clean()
        {
            base.Clean();
            if (this.Paperwork != null)
                foreach (var pw in this.Paperwork)
                    pw.Clean();
        }
    }
}
