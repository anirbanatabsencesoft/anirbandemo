﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Communications
{
	[Serializable]
	public class NotificationConfiguration : BaseCustomerEntity<NotificationConfiguration>
	{
		public NotificationConfiguration()
		{
			Category = NotificationCategory.ToDo;
			DeliveryMethod = NotificationDeliveryMethod.Email;
		}
			
		/// <summary>
		/// Gets or sets user id that this notification
		/// </summary>
		[BsonRequired, BsonRepresentation(BsonType.ObjectId)]
		public string UserId { get; set; }

		/// <summary>
		/// Gets or sets content type of this notification
		/// </summary>
		[BsonRequired]
		public NotificationCategory Category { get; set; }

		/// <summary>
		/// Gets or sets delivery method of this notification
		/// </summary>
		[BsonRequired]
		public NotificationDeliveryMethod DeliveryMethod { get; set; }

        [BsonDefaultValue(NotificationFrequencyType.Immediately)]
        public NotificationFrequencyType FrequencyType { get; set; }

        /// <summary>
        /// Gets or sets delivery address for this notification
        /// <para>Defaults to the employee email or phone number if not provided</para>
        /// </summary>
        [BsonIgnoreIfNull]
		public string DeliveryAddress { get; set; }

		/// <summary>
		/// Gets or sets value if this notification is active
		/// </summary>
		public bool IsActive {get; set; }

		/// <summary>
		/// Gets or sets User associated with this notification
		/// </summary>
		[BsonIgnore, JsonIgnore]
		public User User
		{
            get { return UserId == User.DefaultUserId ? User.System : GetReferenceValue<User>(UserId); }
			set { UserId = SetReferenceValue(value); }
		}

	}
}
