﻿using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Communications
{
    [Serializable]
    public class UserNotificationHistory : BaseNonEntity
    {

        public UserNotificationHistory()
        {

        }

        public UserNotificationHistory(string userId, List<string> entityIds)
            : base()
        {
            UserId = userId;
            EntityIds = entityIds;
        }

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        /// <value>
        /// The user.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public User User
        {
            get { return GetReferenceValue<User>(UserId); }
            set { UserId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the entity ids that triggered this notification.
        /// </summary>
        /// <value>
        /// The entity ids.
        /// </value>
        [BsonRepresentation(BsonType.ObjectId)]
        public List<string> EntityIds { get; set; }
    }
}
