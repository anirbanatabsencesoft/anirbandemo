﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace AbsenceSoft.Data.Communications
{
    [Serializable]
    public class PaperworkCriteria : BaseNonEntity
    {
        public PaperworkCriteria()
        {
            CriteriaType = PaperworkCriteriaType.None;
        }

        /// <summary>
        /// Gets or sets the criteria type used for evaluating whether this paperwork 
        /// should be included.
        /// </summary>
        [BsonRequired]
        public PaperworkCriteriaType CriteriaType { get; set; }

        /// <summary>
        /// Gets or sets the string representation of the criteria itself, such as a
        /// policy Id, Absence Reason code or Id, a CaseType name or a LeaveOfAbsence
        /// expression.
        /// </summary>
        [BsonIgnoreIfNull]
        public string Criteria { get; set; }
    }
}
