﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using AbsenceSoft.Data.Communications;
using Newtonsoft.Json;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Data.Notes
{
    [Serializable]
    public class CommunicationNote : BaseEmployerEntity<CommunicationNote>, INote
    {
        public CommunicationNote()
        {
            this.Categories = new List<SavedCategory>();
        }

        /// <summary>
        /// Gets or sets the notes.
        /// </summary>
        public string Notes { get; set; }

        /// <summary>
        /// Gets or sets whether or not this is public.
        /// </summary>
        public bool Public { get; set; }

        /// <summary>
        /// Gets or sets the notes category.
        /// </summary>
        public NoteCategoryEnum? Category { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string CommunicationId { get; set; }

        [BsonIgnore, JsonIgnore]
        public Communication Communication
        {
            get { return GetReferenceValue<Communication>(CommunicationId); }
            set { CommunicationId = SetReferenceValue(value); }
        }
        [BsonIgnoreIfNull]
        public string EnteredByName { get; set; }

        [BsonIgnoreIfNull]
        public string EnteredByEmployeeNumber { get; set; }

        public List<SavedCategory> Categories { get; set; }

        [BsonIgnore]
        public string CategoryString
        {
            get
            {
                if (this.Categories != null && this.Categories.Any())
                    return string.Join(",", this.Categories.Select(c => c.CategoryName));

                if (this.Category != null)
                    return this.Category.ToString().SplitCamelCaseString();

                return "Unknown";
            }
        }

    }
}
