﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Notes
{
    [Serializable]
    public class SavedCategory : BaseNonEntity
    {

        public int Order { get; set; }

        public string CategoryName { get; set; }

        public string CategoryCode { get; set; }

    }
}
