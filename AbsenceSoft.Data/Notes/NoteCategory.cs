﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Notes
{
    [Serializable]
    /// <summary>
    /// Allows categories for notes to be end user configurable
    /// </summary>
    public class NoteCategory : BaseEmployerOverrideableEntity<NoteCategory>
    {

        /// <summary>
        /// The name of the category 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The description of this categories purpose
        /// </summary>
        public string Description { get; set; }
        
        /// <summary>
        /// The codes of the parent note categories, if any
        /// </summary>
        [BsonIgnoreIfNull]
        public string ParentCode { get; set; }

        [BsonIgnore]
        public List<NoteCategory> ChildCategories { get; set; }

        /// <summary>
        /// Gets or sets the target.
        /// </summary>
        /// <value>
        /// The target.
        /// </value>
        public EntityTarget Target { get; set; }
        
        /// <summary>
        /// Gets or sets the Hide in ESS
        /// </summary>
        [BsonIgnoreIfNull]
        public bool? HideInESS { get; set; }

        /// <summary>
        /// Gets or sets the list of possible role ids for note category.
        /// </summary>
        [BsonRepresentation(BsonType.ObjectId)]
        public List<string> RoleIds { get; set; }

    }
}
