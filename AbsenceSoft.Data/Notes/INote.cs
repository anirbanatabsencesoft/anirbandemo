﻿using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Notes
{
    public interface INote
    {
        /// <summary>
        /// Gets or sets the employee's employer's customer for this case.
        /// </summary>
        [BsonRepresentation(BsonType.ObjectId)]
        string CustomerId { get; set; }
        
        /// <summary>
        /// Gets or sets the EmployerId
        /// </summary>
        [BsonRepresentation(BsonType.ObjectId)]
        string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the notes.
        /// </summary>
        string Notes { get; set; }

        /// <summary>
        /// Gets or sets whether or not this is public.
        /// </summary>
        bool Public { get; set; }

        /// <summary>
        /// Gets or sets the notes category.
        /// </summary>
        NoteCategoryEnum? Category { get; set; }

        List<SavedCategory> Categories { get; set; }

        string CategoryString { get; }

        string EnteredByName { get; set; }
        string EnteredByEmployeeNumber { get; set; }
    }
}
