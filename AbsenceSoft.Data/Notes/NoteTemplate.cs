﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Notes
{
    [Serializable]
    public class NoteTemplate : BaseCustomerEntity<NoteTemplate>
    {
        public NoteTemplate()
        {
            this.Categories = new List<string>();
        }


        /// <summary>
        /// The specific employer this note template belongs to
        /// </summary>
        [BsonRepresentation(BsonType.ObjectId)]
        public string EmployerId { get; set; }

        /// <summary>
        /// The name of this template
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// A description of this templates purpose
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The template
        /// </summary>
        public string Template { get; set; }

        [BsonIgnore]
        public string RenderedTemplate { get; set; }

        /// <summary>
        /// A list of categories that this template is associated with
        /// </summary>
        public List<string> Categories { get; set; }
    }
}
