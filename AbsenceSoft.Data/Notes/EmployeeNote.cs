﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using AbsenceSoft.Data.Customers;
using Newtonsoft.Json;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Data.Notes
{
    /// <summary>
    /// Gets or sets the EmployeeNotes
    /// </summary>
    [Serializable]
    public class EmployeeNote : BaseEmployeeEntity<EmployeeNote>, INote
    {
        public EmployeeNote()
        {
            this.Categories = new List<SavedCategory>();
        }

        /// <summary>
        /// Gets or sets the notes.
        /// </summary>
        public string Notes { get; set; }

        /// <summary>
        /// Gets or sets whether or not this is public.
        /// </summary>
        public bool Public { get; set; }

        /// <summary>
        /// Gets or sets the notes category.
        /// </summary>
        public NoteCategoryEnum? Category { get; set; }


        [BsonIgnoreIfNull]
        public string EnteredByName { get; set; }

        [BsonIgnoreIfNull]
        public string EnteredByEmployeeNumber { get; set; }

        public List<SavedCategory> Categories { get; set; }

        [BsonIgnore]
        public string CategoryString
        {
            get
            {
                if (this.Categories != null && this.Categories.Any())
                    return string.Join(",", this.Categories.OrderBy(c => c.Order).Select(c => c.CategoryName));

                if (this.Category != null)
                    return this.Category.ToString().SplitCamelCaseString();

                return "Unknown";
            }
        }

        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string TemplateId { get; set; }
    }
}
