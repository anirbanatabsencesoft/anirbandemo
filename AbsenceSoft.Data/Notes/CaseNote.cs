﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.ToDo;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Notes
{
    /// <summary>
    /// Gets or Sets of case notes and optionally a todo item (task) for a case.
    /// </summary>
    [Serializable]
    public class CaseNote : BaseEmployerEntity<CaseNote>, INote
    {
        public CaseNote()
        {
            this.Categories = new List<SavedCategory>();
        }


        /// <summary>
        /// Gets or sets the CertId.
        /// </summary>
        public Guid CertId { get; set; }

        /// <summary>
        /// Gets or sets the notes.
        /// </summary>
        public string Notes { get; set; }

        /// <summary>
        /// Gets or sets whether or not this is public.
        /// </summary>
        public bool Public { get; set; }

        //Gets or sets when itor deleted
        public bool IsItorRemoved { get; set; }

        /// <summary>
        /// Gets or sets the notes category.
        /// </summary>
        public NoteCategoryEnum? Category { get; set; }

        [BsonRequired, BsonRepresentation(BsonType.ObjectId)]
        public string CaseId { get; set; }

        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string ToDoItemId { get; set; }

        [BsonIgnore, JsonIgnore]
        public Case Case
        {
            get { return GetReferenceValue<Case>(CaseId); }
            set { CaseId = SetReferenceValue(value); }
        }

        [BsonIgnore, JsonIgnore]
        public ToDoItem ToDoItem
        {
            get { return GetReferenceValue<ToDoItem>(ToDoItemId); }
            set { ToDoItemId = SetReferenceValue(value); }
        }

        [BsonIgnoreIfNull]
        public string EnteredByName { get; set; }
        
        [BsonIgnoreIfNull]
        public string EnteredByEmployeeNumber { get; set; }

        public List<SavedCategory> Categories { get; set; }

        [BsonIgnore]
        public string CategoryString
        {
            get
            {
                if (this.Categories != null && this.Categories.Any())
                    return string.Join(",", this.Categories.Select(c => c.CategoryName));

                if (this.Category != null)
                    return this.Category.ToString().SplitCamelCaseString();

                return "Unknown";
            }
        }

        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string TemplateId { get; set; }
    }
}
