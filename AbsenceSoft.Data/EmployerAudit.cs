﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Audit;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoRepository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace AbsenceSoft.Data
{
    /// <summary>
    /// The Audit entity is used for auditing other entities.
    /// </summary>
    /// <typeparam name="TEntity">The entity type that this audit is for.</typeparam>
    [Serializable, AuditClassNo]
    public class EmployerAudit<TEntity> : Audit<TEntity> where TEntity : BaseEmployerEntity<TEntity>, new()
    {
        [BsonIgnore]
        private static object _repoSync = new object();
        [BsonIgnore]
        private static MongoRepository<EmployerAudit<TEntity>> _myRepository = null;
        /// <summary>
        /// Gets the repository used for manipulating this instance.
        /// </summary>
        [BsonIgnore]
        public static new MongoRepository<EmployerAudit<TEntity>> Repository
        {
            get
            {
                if (_myRepository != null)
                    return _myRepository;

                lock (_repoSync)
                {
                    if (_myRepository != null)
                        return _myRepository;

                    var connString = ConfigurationManager.ConnectionStrings["MongoServerSettings_Audit"] ?? ConfigurationManager.ConnectionStrings["MongoServerSettings"];
                    if (connString == null)
                        throw new ApplicationException("MongoDB connection string not found.");

                    _myRepository = new MongoRepository<EmployerAudit<TEntity>>(connString.ConnectionString, TypeName);
                }

                return _myRepository;
            }
        }//end: Repository

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching.
        /// </summary>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static new IQueryable<EmployerAudit<TEntity>> AsQueryable()
        {
            return Repository.AsQueryable();
        }

        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching while automatically
        /// applying the appropriate data access filters onto it automagically.
        /// </summary>
        /// <param name="entity">The entity of type <c>TEntity</c> to which to build the filtered queryable off of</param>
        /// <param name="user">The user of which to build data visibility and data access from</param>
        /// <param name="permission">The permission that the user should have access to when filtering the data parameters</param>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static IEnumerable<EmployerAudit<TEntity>> AsQueryable(User user, Permission permission) { return AsQueryable(user, permission.Id); }
        /// <summary>
        /// Gets a queryable version of this entity type's repository for manual searching while automatically
        /// applying the appropriate data access filters onto it automagically.
        /// </summary>
        /// <param name="entity">The entity of type <c>TEntity</c> to which to build the filtered queryable off of</param>
        /// <param name="user">The user of which to build data visibility and data access from</param>
        /// <param name="permission">The permission that the user should have access to when filtering the data parameters</param>
        /// <returns>A queryable version of this entity type's repository for manual searching.</returns>
        public static IEnumerable<EmployerAudit<TEntity>> AsQueryable(User user, string permission = null)
        {
            var query = EmployerAudit<TEntity>.AsQueryable();

            // If no user, then duh, no dice sucker, empty list it
            if (user == null) return new List<EmployerAudit<TEntity>>(0);

            // User must belong to a customer, otherwise empty list it
            if (string.IsNullOrWhiteSpace(user.CustomerId)) return query.Where(e => e.Value.CustomerId == null && e.Value.EmployerId == null);
            // User must have access to at least 1 employer, otherwise empty list it
            if (user.Employers == null || !user.Employers.Any(e => e.IsActive)) return query.Where(e => e.Value.CustomerId == user.CustomerId && e.Value.EmployerId == null);

            // Filter out any that are not active
            var employerIds = user.Employers.Where(e => e.IsActive)
                // Ensure the permissions passed in are empty (meaning just all active) or that 
                //  if we have explicit intent, that the user has the appropriate permissions for that function for that employer
                .Where(e => string.IsNullOrWhiteSpace(permission) || User.Permissions.GetPermissions(user, e.EmployerId).Contains(permission))
                // Project our Employer entity
                .Select(e => e.EmployerId)
                .Distinct()
                // Return the final list
                .ToArray();

            return query.Where(e => employerIds.Contains(e.Value.EmployerId))
                .Where(e => e.Value.CustomerId == user.CustomerId);
        }//end: AsQueryable
    }
}
