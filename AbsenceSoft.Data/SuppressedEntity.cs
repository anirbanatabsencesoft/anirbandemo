﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data
{
    [Serializable]
    public class SuppressedEntity:BaseNonEntity
    {
        /// <summary>
        /// The name of the suppressed entity
        /// </summary>
        public string EntityName { get; set; } 

        /// <summary>
        /// The code of the suppressed entity
        /// </summary>
        public string EntityCode { get; set; }

        /// <summary>
        /// The modify date of the suppressed entity
        /// </summary>
        public DateTime? EntityModifiedDate { get; set; }
    }
}
