﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;

namespace AbsenceSoft.Data
{
    /// <summary>
    /// Represents the physical underlying backing store a document may be located in.
    /// </summary>
    public enum DocumentStore : int { S3 = 0x1 }

    /// <summary>
    /// Represents a document stored in the AbsenceTracker system, in any given store, with a pointer
    /// and information about that document.
    /// </summary>
    [Serializable]
    public class Document : BaseEntity<Document>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Document"/> class.
        /// </summary>
        public Document()
        {
            // Set the store default, should always be S3 (for now, may change, who knows)
            Store = DocumentStore.S3;
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the file content type (or MIME type) for transmission information/meta
        /// when downloading the file.
        /// </summary>
        [BsonElement("Mime"), JsonProperty("Mime")]
        public string ContentType { get; set; }

        /// <summary>
        /// Gets or sets the total file content length, in bytes, for calculating the total file size.
        /// </summary>
        [BsonElement("Size"), JsonProperty("Size")]
        public long ContentLength { get; set; }

        /// <summary>
        /// Gets or sets the representated file name, for display purposes.
        /// </summary>
        [BsonElement("Name"), JsonProperty("Name")]
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the file path/address/key (where the file is physically located or a pointer in the underlying store), in our case could be a
        /// physical server path, S3 file path, etc. for S3, we store the file key.
        /// </summary>
        [BsonElement("Loc"), JsonProperty("Loc")]
        public string Locator { get; set; }

        /// <summary>
        /// Gets or sets the physical file store the file is located in.
        /// </summary>
        [BsonDefaultValue(DocumentStore.S3), BsonRepresentation(BsonType.Int32)]
        public DocumentStore Store { get; set; }

        /// <summary>
        /// Temporarily holds the file contents for use server-side or for transporting to and from the client for storage.
        /// This is not actually saved to the database; ever.
        /// </summary>
        [BsonIgnore]
        public byte[] File { get; set; }

        #endregion Public Properties

        #region FK's

        /// <summary>
        /// Gets or sets the customer id for the document. This is optional.
        /// </summary>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the employer id for the document. This is optional.
        /// </summary>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the employee id for the document. This is optional.
        /// </summary>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the case id for the document. This is optional.
        /// </summary>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string CaseId { get; set; }

        #endregion

        #region Ref Properties

        /// <summary>
        /// Gets or sets the entity's customer.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Customer Customer
        {
            get { return GetReferenceValue<Customer>(this.CustomerId); }
            set { CustomerId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the entity's employer.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Employer Employer
        {
            get { return GetReferenceValue<Employer>(this.EmployerId); }
            set { EmployerId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the entity's employee.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Employee Employee
        {
            get { return GetReferenceValue<Employee>(this.EmployeeId); }
            set { EmployeeId = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the entity's case.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        public Case Case
        {
            get { return GetReferenceValue<Case>(this.CaseId); }
            set { CaseId = SetReferenceValue(value); }
        }

        #endregion Ref Properties
    }
}
