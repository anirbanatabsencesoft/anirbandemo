﻿using AbsenceSoft.Data.Customers;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Pay
{
    /// <summary>
    /// Represents all of the payment info for a case.
    /// </summary>
    [Serializable]
    public class PayInfo : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PayInfo"/> class.
        /// </summary>
        public PayInfo() : base()
        {
            PayPeriods = new List<PayPeriod>();
            WaiveWaitingPeriod = new UserOverrideableValue<bool>(false);
            ApplyOffsetsByDefault = true;
        }

        /// <summary>
        /// Gets or sets the pay periods.
        /// </summary>
        /// <value>
        /// The pay periods.
        /// </value>
        public List<PayPeriod> PayPeriods { get; set; }

        /// <summary>
        /// Gets the total paid on the case so far (at least that has been submitted
        /// to payroll).
        /// </summary>
        /// <value>
        /// The total paid.
        /// </value>
        public decimal TotalPaid
        {
            get
            {
                return PayPeriods == null || !PayPeriods.Any(p => p.IsLocked) ? 0M
                    : PayPeriods.Where(p => p.IsLocked).Sum(p => p.Total);
            }
        }

        /// <summary>
        /// Gets the total offset, if any, for the entire case.
        /// </summary>
        /// <value>
        /// The total offset.
        /// </value>
        public decimal TotalOffset
        {
            get
            {
                return PayPeriods == null || !PayPeriods.Any(p => p.IsLocked && p.HasOffset) ? 0M
                    : PayPeriods.Where(p => p.IsLocked && p.HasOffset).Sum(p => p.Offset);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to apply offsets by default.
        /// </summary>
        /// <value>
        /// <c>true</c> if apply offsets by default; otherwise, <c>false</c>.
        /// </value>
        public bool ApplyOffsetsByDefault { get; set; }

        /// <summary>
        /// Gets or sets the waive waiting period indicator.
        /// </summary>
        /// <value>
        /// The waive waiting period.
        /// </value>
        public UserOverrideableValue<bool> WaiveWaitingPeriod { get; set; }

        /// <summary>
        /// Gets or sets the pay schedule identifier for the effective pay schedule for this leave of absence.
        /// </summary>
        /// <value>
        /// The pay schedule identifier.
        /// </value>
        [BsonRepresentation(BsonType.ObjectId)]
        public string PayScheduleId { get; set; }

        /// <summary>
        /// Gets or sets the pay schedule.
        /// </summary>
        /// <value>
        /// The pay schedule.
        /// </value>
        [BsonIgnore]
        public PaySchedule PaySchedule
        {
            get { return GetReferenceValue<PaySchedule>(PayScheduleId); }
            set { PayScheduleId = SetReferenceValue(value); }
        }
    }
}
