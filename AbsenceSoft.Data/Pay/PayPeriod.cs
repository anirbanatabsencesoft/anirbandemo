﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Pay
{
    /// <summary>
    /// Represents a pay check basically, this is a pay period within the
    /// pay info that contains all of the detail rows that comprise the employee's
    /// compensation for the given case/claim. This is the summary rollup for a
    /// given pay date with a span of dates that define what the compensation is
    /// paying for.... well, you know wtf a pay period is, why are you still reading
    /// this?
    /// </summary>
    [Serializable]
    public class PayPeriod : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PayPeriod"/> class.
        /// </summary>
        public PayPeriod() : base()
        {
            Detail = new List<PayPeriodDetail>();
        }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public PayrollStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the pay period detail rows.
        /// </summary>
        /// <value>
        /// The detail.
        /// </value>
        public List<PayPeriodDetail> Detail { get; set; }

        /// <summary>
        /// Payroll start date is on a fixed schedule and does not depend on acutal
        /// usage. e.g. payroll is cut every two weeks or twice a month
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Payroll end date is on a fixed schedule and does not depend on acutal
        /// usage. e.g. payroll is cut every two weeks or twice a month
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Gets or sets the user identifier that overrode the value.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [BsonIgnoreIfNull]
        [BsonRepresentation(BsonType.ObjectId)]
        public string EndDateOverrideById { get; set; }

        /// <summary>
        /// Gets or sets the override by.
        /// </summary>
        /// <value>
        /// The override by.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public User EndDateOverrideBy
        {
            get { return base.GetReferenceValue<User>(this.EndDateOverrideById); }
            set { this.EndDateOverrideById = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the override date.
        /// </summary>
        /// <value>
        /// The override date.
        /// </value>
        [BsonIgnoreIfNull]
        public DateTime? EndDateOverride { get; set; }

        /// <summary>
        /// Gets or sets the user overrideable payroll date.
        /// </summary>
        /// <value>
        /// The user overrideable payroll date.
        /// </value>
        public UserOverrideableValue<DateTime> PayrollDate { get; set; }

        /// <summary>
        /// Gets or sets the user identifier that overrode the value.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [BsonIgnoreIfNull]
        [BsonRepresentation(BsonType.ObjectId)]
        public string PayrollDateOverrideById { get; set; }

        /// <summary>
        /// Gets or sets the override by.
        /// </summary>
        /// <value>
        /// The override by.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public User PayrollDateOverrideBy
        {
            get { return base.GetReferenceValue<User>(this.PayrollDateOverrideById); }
            set { this.PayrollDateOverrideById = SetReferenceValue(value); }
        }

        /// <summary>
        /// Gets or sets the override date.
        /// </summary>
        /// <value>
        /// The override date.
        /// </value>
        [BsonIgnoreIfNull]
        public DateTime? PayRollDateOverride { get; set; }



        /// <summary>
        /// Gets a value indicating whether this instance is locked.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is locked; otherwise, <c>false</c>.
        /// </value>
        public bool IsLocked { get { return Status != PayrollStatus.Pending; } }

        /// <summary>
        /// Gets or sets the locked by identifier.
        /// </summary>
        /// <value>
        /// The locked by identifier.
        /// </value>
        [BsonRepresentation(BsonType.ObjectId)]
        public string LockedById { get; set; }

        /// <summary>
        /// Gets or sets the locked date.
        /// </summary>
        /// <value>
        /// The locked date.
        /// </value>
        public DateTime? LockedDate { get; set; }

        /// <summary>
        /// Gets or sets the employee's user overrideable base pay.
        /// for this pay period.
        /// </summary>
        /// <value>
        /// The base pay.
        /// </value>
        public UserOverrideableValue<decimal> BasePay { get; set; }

        /// <summary>
        /// Gets or sets the maximum weekly pay amount.
        /// </summary>
        /// <value>
        /// The maximum weekly pay amount.
        /// </value>
        public decimal? MaxWeeklyPayAmount { get; set; }

        /// <summary>
        /// Gets or sets the minimum weekly pay amount.
        /// </summary>
        /// <value>
        /// The minimum weekly pay amount.
        /// </value>
        public decimal? MinWeeklyPayAmount { get; set; }

        /// <summary>
        /// Gets the pay period sub total before offsets.
        /// </summary>
        /// <value>
        /// The sub total to pay for the pay period.
        /// </value>
        public decimal SubTotal
        {
            get
            {
                return Detail == null ? 0M 
                    : !Detail.Any(d => d.PayAmount != null) ? 0M 
                    : Detail.Where(d => d.PayAmount != null).Sum(d => d.PayAmount.Value);
            }
        }

        /// <summary>
        /// Gets the offset amount, if any.
        /// </summary>
        /// <value>
        /// The offset amount.
        /// </value>
        public decimal Offset
        {
            get
            {
                return Detail == null ? 0M 
                    : !Detail.Any(d => d.IsOffset && d.PayAmount != null) ? 0M 
                    : Detail.Where(d => d.IsOffset && d.PayAmount != null).Sum(d => d.PayAmount.Value);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has any offset.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has any offset; otherwise, <c>false</c>.
        /// </value>
        public bool HasOffset { get { return Detail != null && Detail.Any(d => d.IsOffset); } }

        /// <summary>
        /// Gets the total pay amount for the pay period.
        /// </summary>
        /// <value>
        /// The total pay amount for the pay period.
        /// </value>
        public decimal Total
        {
            get
            {
                return Detail == null ? 0M 
                    : !Detail.Any(d => !d.IsOffset && d.PayAmount != null) ? 0M 
                    : Detail.Where(d => !d.IsOffset && d.PayAmount != null).Sum(d => d.PayAmount.Value);
            }
        }

        /// <summary>
        /// Gets the percentage.
        /// </summary>
        /// <value>
        /// The percentage.
        /// </value>
        public decimal Percentage
        {
            get
            {
                return Detail == null ? 0M
                    : !Detail.Any(d => !d.IsOffset && d.PayPercentage != null) ? 0M
                    : Detail.Where(d => !d.IsOffset && d.PayPercentage != null).Sum(d => d.PayPercentage.Value);
            }
        }

        /// <summary>
        /// Adjusts the end date trimming the end date from any detail rows that exceed the new end date.
        /// </summary>
        /// <param name="newEndDate">The new end date.</param>
        /// <param name="user">The user.</param>
        /// <returns>
        /// This instance of the pay period.
        /// </returns>
        public PayPeriod AdjustEndDate(DateTime newEndDate, User user)
        {
            if (!Detail.Any())
                return this;
            var ned = newEndDate.ToMidnight();
            Detail.Where(d => ned.DateInRange(d.StartDate, d.EndDate)).ForEach(d => d.EndDate = ned);
            Detail.RemoveAll(d => d.StartDate > ned);
            EndDateOverrideBy = user;
            EndDateOverride = ned;
            return this;
        }

        public PayPeriod AdjustPayDate(DateTime newPayDate, User user)
        {
            if (!Detail.Any())
                return this;

            newPayDate = newPayDate.ToMidnight();
            PayrollDateOverrideBy = user;
            PayRollDateOverride = newPayDate;
            return this;
        }

        /// <summary>
        /// Locks the pay period with the specified status by the given user.
        /// </summary>
        /// <param name="status">The status to set upon locking.</param>
        /// <param name="user">The user to set the lock by.</param>
        /// <returns>This instance of the pay period.</returns>
        public PayPeriod SetStatus(PayrollStatus status, User user)
        {
            // If the status is pending or the same, we're not going to do anything
            //  but ignore this silly request for useless nonsense of wasted CPU cycles.
            if (status == PayrollStatus.Pending || status == Status)
                return this;

            Status = status;
            LockedById = (user ?? User.System).Id;
            LockedDate = DateTime.UtcNow;
            return this;
        }

        
    }
}
