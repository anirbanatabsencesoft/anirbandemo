﻿using AbsenceSoft.Data.Audit;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Pay
{
    /// <summary>
    /// Represents a pay period detail row which is a unique combination of
    /// a Policy + Payment Tier within a Pay Period.
    /// </summary>
    /// <remarks>
    /// NOTE: There may be times when a policy's pay periods split in the middle
    /// of a pay period, in that case there would be 2 detail rows for that policy
    /// given each payment tier.
    /// </remarks>
    [Serializable]
    [AuditClassIgnoreProperty("Id")]
    public class PayPeriodDetail : BaseNonEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PayPeriodDetail"/> class.
        /// </summary>
        public PayPeriodDetail() : base()
        {
            PayPercentage = new UserOverrideableValue<decimal>();
            PayAmount = new UserOverrideableValue<decimal>();
        }

        /// <summary>
        /// Gets or sets the policy code.
        /// </summary>
        /// <value>
        /// The policy code.
        /// </value>
        public string PolicyCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the policy.
        /// </summary>
        /// <value>
        /// The name of the policy.
        /// </value>
        [AuditClassPrimaryKey(1)]
        public string PolicyName { get; set; }

        /// <summary>
        /// Gets or sets the start date of the pay period detail record.
        /// </summary>
        /// <value>
        /// The start date.
        /// </value>
        [AuditClassPrimaryKey(2)]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date of the pay period detail record.
        /// </summary>
        /// <value>
        /// The end date.
        /// </value>
        [AuditClassPrimaryKey(3)]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is an offset.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is an offset; otherwise, <c>false</c>.
        /// </value>
        public bool IsOffset { get; set; }

        /// <summary>
        /// Gets or sets the salary total for this detail row
        /// which should be the amount they get paid for this policy for this pay period
        /// </summary>
        public decimal SalaryTotal { get; set; }

        /// <summary>
        /// Gets or sets the user overrideable pay percentage.
        /// </summary>
        /// <value>
        /// The user overrideable pay percentage.
        /// </value>
        public UserOverrideableValue<decimal> PayPercentage { get; set; }

        /// <summary>
        /// The original amount from the policy payment tier. 
        /// Not for user override, but used in calculations
        /// </summary>
        public decimal PaymentTierPercentage { get; set; }

        /// <summary>
        /// Gets or sets the user overrideable pay amount.
        /// </summary>
        /// <value>
        /// The user overrideable pay amount.
        /// </value>
        public UserOverrideableValue<decimal> PayAmount { get; set; }

        /// <summary>
        /// Gets or sets the maximum payment amount.
        /// </summary>
        /// <value>
        /// The maximum payment amount.
        /// </value>
        public decimal? MaxPaymentAmount { get; set; }

        /// <summary>
        /// Gets or sets the minimum payment amount.
        /// </summary>
        /// <value>
        /// The minimum payment amount.
        /// </value>
        public decimal? MinPaymentAmount { get; set; }
    }
}
