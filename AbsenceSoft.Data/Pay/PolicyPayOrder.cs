﻿using System;

namespace AbsenceSoft.Data.Pay
{
    [Serializable]
    public class PolicyPayOrder : BaseEmployerEntity<PolicyPayOrder>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PolicyPayOrder"/> class.
        /// </summary>
        public PolicyPayOrder() : base() { }

        /// <summary>
        /// Gets or sets the policy code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the name of the policy.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the order in which the policy gets paid.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        public int Order { get; set; }
    }
}
