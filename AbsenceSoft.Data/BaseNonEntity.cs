﻿using AbsenceSoft.Data.Audit;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data
{
    /// <summary>
    /// Base class for all non-entity entities that are not persisted on thier ownn to the database but are collectively placed
    /// into token lists, etc.
    /// </summary>
    [Serializable, BsonIgnoreExtraElements(false, Inherited = true)]
    public abstract class BaseNonEntity
    {
        /// <summary>
        /// The private accessor for the unique id, this is used so that we don't have
        /// to initialize a base constructor on parent classes, but serialization
        /// will automatically set this value through the setter on <c>Id</c>.
        /// </summary>
        [BsonIgnore]
        private Guid? _id;

        /// <summary>
        /// Gets or sets a unique identifier (GUID) for the instance so it can be easily identified
        /// in a list by external operations or called by external/internal reference.
        /// </summary>
        [BsonRequired, BsonRepresentation(BsonType.String)]
        public virtual Guid Id
        {
            get
            {
                if (_id == null)
                    _id = Guid.NewGuid();
                return _id.Value;
            }
            set { _id = value; }
        }

        [BsonIgnore, JsonIgnore]
        public bool IsNew
        {
            get { return _id == null; }
        }

        /// <summary>
        /// Gets or sets the extra elements/properties for this document so we can handle any unknown or extra elements
        /// that are added before a typed property to an entity and we still persist those properties.
        /// </summary>
        [BsonExtraElements, JsonIgnore]
        public BsonDocument Metadata = new BsonDocument();

        /// <summary>
        /// Gets the dynamic metadata.
        /// </summary>
        /// <value>
        /// The dynamic metadata.
        /// </value>
        [AuditPropertyNo, BsonIgnore, JsonIgnore]
        public dynamic DynamicMetadata { get { return (dynamic)new DynamicAwesome(Metadata); } }

        /// <summary>
        /// Gets or sets the extra elements/properties for this document so we can handle any unknown or extra elements
        /// that are added before a typed property to an entity and we still project those to the UI via JSON and back.
        /// </summary>
        [JsonExtensionData, BsonIgnore]
        private Dictionary<string, object> _UIMetadata = new Dictionary<string, object>();

        #region Protected Methods

        /// <summary>
        /// The protected reference instance dictionary used for caching entity references.
        /// </summary>
        [BsonIgnore]
        protected Dictionary<string, object> _refs = new Dictionary<string, object>();
        private static object _refsSync = new object();

        /// <summary>
        /// Gets the reference value for an Id based on the reference type passed into TRef.
        /// </summary>
        /// <typeparam name="TRef">The entity type the reference Id is referencing</typeparam>
        /// <param name="refId">The Id of the reference type to get by</param>
        /// <returns>An instance of the reference by Id or <c>null</c> if the reference is not found or Id is null.</returns>
        protected virtual TRef GetReferenceValue<TRef>(string refId) where TRef : BaseEntity<TRef>, new()
        {
            string key = string.Concat(typeof(TRef).Name, "-", refId ?? "");

            TRef existingVal = _refs.ContainsKey(key) ? _refs[key] as TRef : null;
            if (!string.IsNullOrWhiteSpace(refId) && (existingVal == null || existingVal.Id != refId))
                existingVal = typeof(TRef).GetMethod("GetById", BindingFlags.Static | BindingFlags.FlattenHierarchy | BindingFlags.Public).Invoke(null, new object[] { refId }) as TRef;
            else if (string.IsNullOrWhiteSpace(refId))
                existingVal = null;

            lock (_refsSync)
            {
                _refs[key] = existingVal;
            }

            return existingVal;
        }//GetReferenceValue

        /// <summary>
        /// Gets the reference value for a key value passed in using the current cache or fetch routine.
        /// </summary>
        /// <typeparam name="TRef">The entity type the reference Id is referencing</typeparam>
        /// <param name="keyValue">The key value for the reference type and keySelector to match cache hits on.</param>
        /// <param name="keySelector">The key selector.</param>
        /// <param name="fetch">The fetch.</param>
        /// <returns>
        /// An instance of the reference by keyValue or <c>null</c> if the reference is not found or Id is null.
        /// </returns>
        protected virtual TRef GetReferenceValue<TRef>(string keyValue, Func<TRef, string> keySelector, Func<string, TRef> fetch) where TRef : BaseEntity<TRef>, new()
        {
            string key = string.Concat(typeof(TRef).Name, "-", keyValue ?? "");

            TRef existingVal = _refs.ContainsKey(key) ? _refs[key] as TRef : null;
            if (!string.IsNullOrWhiteSpace(keyValue) && (existingVal == null || keySelector(existingVal) != keyValue))
                existingVal = fetch(keyValue);
            else if (string.IsNullOrWhiteSpace(keyValue))
                existingVal = null;

            lock (_refsSync)
            {
                _refs[key] = existingVal;
            }

            return existingVal;
        }//GetReferenceValue

        /// <summary>
        /// Sets the reference value for an Id based on the reference type passed into TRef, but also returns
        /// the Id of the set entity for setting the actual Id property value back in the parent entity.
        /// </summary>
        /// <typeparam name="TRef">The type value to be passed in to set</typeparam>
        /// <param name="value">The instance of the type value or <c>null</c> if being cleared out</param>
        /// <returns>The Id of the passed in value or <c>null</c> if no entity passed in</returns>
        protected virtual string SetReferenceValue<TRef>(TRef value) where TRef : BaseEntity<TRef>, new()
        {
            if (value == null)
                return null;

            string key = string.Concat(typeof(TRef).Name, "-", value.Id ?? "");
            lock (_refsSync)
            {
                _refs[key] = value;
            }

            return value.Id;
        }//SetReferenceValue

        /// <summary>
        /// Sets the reference value for an keySelector value based on the reference type passed into TRef, but also returns
        /// the keySelector key value of the set entity for setting the actual related property value back in the parent entity.
        /// </summary>
        /// <typeparam name="TRef">The type value to be passed in to set</typeparam>
        /// <param name="value">The instance of the type value or <c>null</c> if being cleared out</param>
        /// <param name="keySelector">The key selector being used to retrieve the appropriate key value from the entity.</param>
        /// <returns>The key of the passed in value or <c>null</c> if no entity passed in</returns>
        protected virtual string SetReferenceValue<TRef>(TRef value, Func<TRef, string> keySelector) where TRef : BaseEntity<TRef>, new()
        {
            if (value == null)
                return null;

            string keyVal = keySelector(value);
            if (string.IsNullOrWhiteSpace(keyVal))
                return null;

            string key = string.Concat(typeof(TRef).Name, "-", keyVal ?? "");
            lock (_refsSync)
            {
                _refs[key] = value;
            }

            return keyVal;
        }//SetReferenceValue

        #endregion

        /// <summary>
        /// Cleans this instance.
        /// </summary>
        public virtual void Clean()
        {
            Id = Guid.NewGuid();
        }

        #region Serialization Callbacks

        /// <summary>
        /// Called when this instance of this class is serializing (but before
        /// actually being serialized). This sets up the JsonExtensionData
        /// property, _UIMetadata, with any key value pairs from the BsonDocument
        /// Metadata property.
        /// </summary>
        /// <param name="context">The context.</param>
        [OnSerializing]
        internal void OnSerializing(StreamingContext context)
        {
            // Set _UIMetadata to prepare for serialization to JSON
            _UIMetadata.Clear();
            foreach (var key in Metadata.Names)
            {
                BsonValue v = Metadata.GetValue(key);
                object val = BsonTypeMapper.MapToDotNetValue(v);
                _UIMetadata.Add(key, val);
            }
        }

        /// <summary>
        /// Called when this instance of this class has been deserialized. 
        /// This sets up the BsonExtraElements property, Metadata, with any 
        /// key value pairs from the JsonExtensionData _UIMetadata property.
        /// </summary>
        /// <param name="context">The context.</param>
        [OnDeserialized]
        internal void OnDeserialized(StreamingContext context)
        {
            // Set Metadata (BSON) from the extra elements out of our JSON deserialization
            
            foreach (var item in _UIMetadata)
            {
                int intValue = 0;
                /// Special handling for JArrays.  We're going to assume they're all strings for now, since that's all we need support for
                if (item.Value is JArray)
                {
                    var arrayVal = ((JArray)item.Value).Select(jt => jt.ToObject<object>()).ToArray();
                    Metadata.SetRawValue(item.Key, arrayVal);
                }
                /// The int handling is because every number in UI Metadata is set as a long
                /// So lets see if we can turn it into an int before setting it as a long
                else if (item.Value != null && int.TryParse(item.Value.ToString(), out intValue))
                {
                    Metadata.SetRawValue<int?>(item.Key, intValue);
                }
                else
                {
                    Metadata.SetRawValue(item.Key, item.Value);
                }
                    
            }
            Metadata.FixStupid(this);
        }

        #endregion Serialization Callbacks
    }
}
