﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.RiskProfiles
{
    [Serializable]
    public class CalculatedRiskProfile : BaseEmployeeEntity<CalculatedRiskProfile>
    {
        [BsonIgnoreIfNull]
        public RiskProfile RiskProfile { get; set; } 

        [BsonIgnoreIfNull]
        public string CaseId { get; set; }

        [BsonIgnore, JsonIgnore]
        public Case Case
        {
            get { return GetReferenceValue<Case>(CaseId); }
            set { CaseId = SetReferenceValue<Case>(value); }
        }
    }
}
