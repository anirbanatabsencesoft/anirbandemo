﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.RiskProfiles
{
    [Serializable]
    public class RiskProfile:BaseEmployerOverrideableEntity<RiskProfile>
    {
        /// <summary>
        /// The name of the risk profile
        /// </summary>
        public string Name { get; set; } 

        /// <summary>
        /// Hexadecimal representation of the color associated with this risk profile
        /// </summary>
        public string Color { get; set; }

        /// <summary>
        /// The name of the icon associated with this risk profile
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// The description of the risk profile
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The order of the risk profile.  A higher number should indicate a higher risk
        /// </summary>
        public int Order { get; set; }

        public EntityTarget Target { get; set; }

        /// <summary>
        /// The list of rule groups that determine whether or not someone matches this risk profile
        /// </summary>
        public List<RuleGroup> RuleGroups { get; set; }

    }
}
