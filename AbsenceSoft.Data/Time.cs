﻿using AbsenceSoft.Data.Audit;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.ComponentModel;

namespace AbsenceSoft.Data
{
    /// <summary>
    /// Represents a day and total number of minutes comprised of that day
    /// representative of the schedule. Minutes are used as the lowest denominator
    /// of time, since hours is too large and precision calculations would be thrown off
    /// or imperfect otherwise.
    /// </summary>
    [
        Serializable,
        AuditClassIgnoreProperty("Id")
    ]
    public class Time : BaseNonEntity, IEquatable<Time>, IComparable, IComparable<Time>, IEquatable<DateTime>, IComparable<DateTime>
    {
        /// <summary>
        /// Gets or sets the total number of minutes this scheduled date (from SampleDate)
        /// uses, or <c>null</c> if not time is used on the SampleDate.
        /// </summary>
        /// <value>
        /// The total minutes.
        /// </value>
        [DisplayName("Total Minutes")]
        public int? TotalMinutes { get; set; }

        /// <summary>
        /// Gets or sets the sample date that the total minutes applies to.
        /// </summary>
        /// <value>
        /// The sample date.
        /// </value>
        [
            BsonDateTimeOptions(DateOnly = true),
            DisplayName("Sample Date"),
            AuditClassPrimaryKey(0)
        ]
        public DateTime SampleDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is holiday.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is holiday; otherwise, <c>false</c>.
        /// </value>
        [BsonIgnore]
        public bool IsHoliday { get; set; }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.
        /// </returns>
        public bool Equals(Time other)
        {
            if (other == null)
                return false;
            return (this.SampleDate == other.SampleDate && this.TotalMinutes == other.TotalMinutes);
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            Time other = obj as Time;
            if (other == null)
                return this.SampleDate.Equals(obj);
            return this.Equals(other);
        }

        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared. The return value has the following meanings: Value Meaning Less than zero This object is less than the <paramref name="other" /> parameter.Zero This object is equal to <paramref name="other" />. Greater than zero This object is greater than <paramref name="other" />.
        /// </returns>
        public int CompareTo(Time other)
        {
            if (other == null) return 1;
            return this.SampleDate.CompareTo(other.SampleDate);
        }

        /// <summary>
        /// Compares the current instance with another object of the same type and returns an integer that indicates whether the current instance precedes, follows, or occurs in the same position in the sort order as the other object.
        /// </summary>
        /// <param name="obj">An object to compare with this instance.</param>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared. The return value has these meanings: Value Meaning Less than zero This instance precedes <paramref name="obj" /> in the sort order. Zero This instance occurs in the same position in the sort order as <paramref name="obj" />. Greater than zero This instance follows <paramref name="obj" /> in the sort order.
        /// </returns>
        public int CompareTo(object obj)
        {
            Time other = obj as Time;
            if (other == null)
                return this.SampleDate.CompareTo(obj);
            return this.CompareTo(other);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return SampleDate.GetHashCode();
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.
        /// </returns>
        public bool Equals(DateTime other)
        {
            return SampleDate.Equals(other);
        }

        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared. The return value has the following meanings: Value Meaning Less than zero This object is less than the <paramref name="other" /> parameter.Zero This object is equal to <paramref name="other" />. Greater than zero This object is greater than <paramref name="other" />.
        /// </returns>
        public int CompareTo(DateTime other)
        {
            return SampleDate.CompareTo(other);
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return $"{SampleDate.DayOfWeek}, {SampleDate:yyyy-MM-dd}: {(TotalMinutes ?? 0).ToFriendlyTime()}";
        }
    }
}
