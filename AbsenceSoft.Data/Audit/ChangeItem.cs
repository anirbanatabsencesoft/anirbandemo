﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Audit
{
    [Serializable]
    [AuditClassNoAttribute]
    public class ChangeItem
    {
        [BsonRequired]
        public string PropertyName {get; set;}
        [BsonRequired]
        public string ClassType { get; set; }

        [BsonRequired]
        public AuditAction Action { get; set; }
        
        [BsonIgnoreIfNull]
        public string Before { get; set; }
        
        [BsonIgnoreIfNull]
        public string After { get; set; }

        [BsonRequired]
        public string ChangePath { get; set; }

        [BsonIgnoreIfNull]
        public string AuditObjectId { get; set; }

        /// <summary>
        /// used only during processing of the audit object. Used to 
        /// find where a new change begins when it is nested inside
        /// another object
        /// </summary>
        [BsonIgnore]
        public bool NewRoot { get; set; }

    }
}
