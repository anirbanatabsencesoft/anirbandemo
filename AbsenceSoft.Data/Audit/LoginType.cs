﻿using System;

namespace AbsenceSoft.Data.Audit
{
    /// <summary>
    /// The type of audit record enum
    /// </summary>
    public enum LoginType : int
    {
        /// <summary>
        /// Indicates a logout operation, 0x0
        /// </summary>
        Logout = 0x0,
        /// <summary>
        /// Indicates a login operation, 0x1
        /// </summary>
        Login = 0x1,
        /// <summary>
        /// Indicates a failed operation, 0x2
        /// </summary>
        Failed = 0x2,
        /// <summary>
        /// Indicates a locked operation, 0x3
        /// </summary>
        Locked = 0x3,
        /// <summary>
        /// Indicates a disabled operation, 0x4
        /// </summary>
        Disabled = 0x4,
        /// <summary>
        /// Indicates a expired operation, 0x5
        /// </summary>
        Expired = 0x5
    }

}
