﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Audit
{
    /// <summary>
    /// Used as a "mixin" class to implement a change pattern.
    /// The use of this class is not required for tracking changes
    /// and you are free to use whatever implementation you like.
    /// </summary>
    [Serializable]
    public class AuditChangeHelper
    {
        private Dictionary<string,ChangeItem> Changes { get; set; }
        private string ClassName { get; set; }
        private string Id { get; set; }

        /// <summary>
        /// use if new object or you will not have an Id for the object until
        /// a later time
        /// </summary>
        /// <param name="className"></param>
        public AuditChangeHelper(string className)
        {
            Changes = new Dictionary<string, ChangeItem>();
            ClassName = className;
        }

        /// <summary>
        /// Initialze and set the object id
        /// </summary>
        /// <param name="className"></param>
        /// <param name="id"></param>
        public AuditChangeHelper(string className, string id) : this(className)
        {
            Id = id;
        }

        /// <summary>
        /// (re)Set the object id
        /// </summary>
        /// <param name="id"></param>
        public void SetId(string id)
        {
            Id = id;
        }

        /// <summary>
        /// capture the change. Only the last change counts.
        /// On multiple changes the before parm is ignored and the original
        /// value is kept
        /// </summary>
        /// <param name="property">The property name to use for the change</param>
        /// <param name="before">The before value (ignored when called after the first change for this property)</param>
        /// <param name="after">The new value</param>
        /// <param name="action">Adding, updating, deleting</param>
        public void Change(string property, string before, string after, AuditAction action)
        {
            ChangeItem changing;
            if (Changes.ContainsKey(property))
            {
                changing = Changes[property];
            }
            else
            {
                changing = new ChangeItem()
                {
                    Before = before,
                    PropertyName = property
                };
            }

            changing.After = after;
            changing.Action = action;

            Changes[property] = changing;
        }

        /// <summary>
        /// Changed your mind about a change
        /// </summary>
        /// <param name="property"></param>
        public void RemoveChange(string property)
        {
            Changes.Remove(property);
        }

        /// <summary>
        /// Start the list over again
        /// </summary>
        public void Reset()
        {
            Changes = new Dictionary<string, ChangeItem>();
        }

        /// <summary>
        /// Return a change item collection
        /// </summary>
        /// <returns></returns>
        public List<ChangeItem> GetChanges()
        {
            return Changes.Select(c => new ChangeItem()
                {
                    Action = c.Value.Action,
                    After = c.Value.After,
                    Before = c.Value.Before,
                    ChangePath = "",
                    ClassType = ClassName,
                    PropertyName = c.Key,
                    AuditObjectId = Id,
                    NewRoot = false
                }).ToList();
        }
    }
}
