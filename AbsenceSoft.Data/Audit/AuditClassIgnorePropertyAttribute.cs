﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Audit
{
    /// <summary>
    /// Use this attribute to override proprties that cannot be directly overriden
    /// For example if you want some decendants of a derived class to ignore a
    /// property on a base class while allowing it to be used on other
    /// decendants
    /// </summary>
    [AttributeUsage(AttributeTargets.Class,AllowMultiple=true)]
    public class AuditClassIgnorePropertyAttribute : System.Attribute
    {
        public string IgnorePropertyName { get; private set; }

        public AuditClassIgnorePropertyAttribute(string ignoreProperty)
        {
            IgnorePropertyName = ignoreProperty;
        }
    }
}
