﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Audit
{
    /// <summary>
    /// Use this interface to implement your own get audit get change items. 
    /// If this interface is present then only this will be used for audit history
    /// You will be responsible for any nested items that you want to audit from this point in 
    /// the tree down
    /// </summary>
    public interface IAuditOverride
    {
        List<ChangeItem> GetAuditChangeItems(object before, object after);
        void ChangeHistoryReset();
    }
}
