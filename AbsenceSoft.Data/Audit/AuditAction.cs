﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Audit
{
    public enum AuditAction : int
    {
        /// <summary>
        /// Indicates a delete operation, 0x0
        /// </summary>
        Delete = 0x0,
        /// <summary>
        /// Indicates a save operation, 0x1, implies update
        /// </summary>
        Save = 0x1,
        
        /// <summary>
        /// when you know it is new, then it is a create
        /// </summary>
        Create = 2
    }

}
