﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Audit
{
    /// <summary>
    /// Use this interface to include addtional audit information for your class.
    /// This will alllow the standard audit to happen on fields and you may provide
    /// custom audit info when needed
    /// </summary>
    public interface IAuditInclude
    {
        List<ChangeItem> GetAuditChangeItems(object before, object after);
        void ChangeHistoryReset();
    }
}
