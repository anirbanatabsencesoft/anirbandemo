﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace AbsenceSoft.Data.Audit
{
    [Serializable]
    [AuditClassNo]
    [Repository(ConnectionStringName = "MongoServerSettings_Audit")]
    public class PagesViewHistory : BaseEntity<PagesViewHistory>
    {
        /// <summary>
        /// Gets or sets the username for the entity.
        /// </summary>
        [BsonRequired]
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the customer id for the entity.
        /// </summary>
        [BsonRequired, BsonRepresentation(BsonType.ObjectId)]
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the employer id for the entity.
        /// </summary>
        [BsonRequired, BsonRepresentation(BsonType.ObjectId)]
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the current url
        /// </summary>
        [BsonRequired]
        public string Url { get; set; }

        /// <summary>
        /// Gets or sets the route data parameters
        /// </summary>
        [BsonIgnoreIfNull]
        public Dictionary<string, object> RouteData { get; set; }

        /// <summary>
        /// Record's the HTTP method used
        /// </summary>
        [BsonRequired]
        public string HttpMethod { get; set; }

        /// <summary>
        /// Record's the Email for the user
        /// </summary>
        public string Email { get; set; }
    }
}
