﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoRepository;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
//using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Audit
{
    [Serializable]
    [AuditClassNoAttribute]
    public class ChangeHistory : BaseEntity<ChangeHistory>
    {
        [BsonRequired, BsonRepresentation(BsonType.ObjectId)]
        public string PrimaryEntityId { get; set; }
        
        [BsonRequired, BsonRepresentation(BsonType.ObjectId)]
        public string UserId { get; set; }

        [BsonRequired]
        public string UserName { get; set; }
        
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc, Representation = BsonType.DateTime), BsonRequired]
        public DateTime ChangeDate {get; set;}

        public string ClassName { get; set; }

        [BsonRequired]
        public bool IsDelete { get; set; }

        [BsonRequired]
        public List<ChangeItem> Items { get; set; }

        public ChangeHistory()
        {
            Items = new List<ChangeItem>();
            this.SetCreatedDate(DateTime.UtcNow);
            this.SetModifiedDate(DateTime.UtcNow);
        }

        public static void SaveAudit(object before, object after, string userId)
        {
            List<ChangeHistory> ch = GenereateAudit(before, after, userId);

            if (ch.Count == 0)
                return;

            using (new InstrumentationContext("ChangeHistory.SaveAudit"))
            {
                string connectionString = ConfigurationManager.ConnectionStrings["MongoServerSettings_Audit"].ConnectionString;
                foreach (ChangeHistory hist in ch)
                    (new MongoRepository<ChangeHistory>(connectionString, hist.ClassName)).Update(hist);
            }
        }

        /// <summary>
        /// kick off the audit process on it's own thread. if it bombs the UI will never know
        /// but if it is a big object then the UI will return faster
        /// </summary>
        /// <param name="before"></param>
        /// <param name="after"></param>
        /// <param name="userId"></param>
        public static void SaveAuditNoBlock(object before, object after, string userId)
        {
            Task.Run(()=> SaveAudit(before, after, userId))
                .ContinueWith((Task boom)=> {
                    Log.Error(boom.Exception.Flatten());
                }, TaskContinuationOptions.OnlyOnFaulted);
        }

        /// <summary>
        /// Give it the before and after objects and it will return a change history
        /// </summary>
        /// <param name="before"></param>
        /// <param name="after"></param>
        /// <returns></returns>
        public static List<ChangeHistory> GenereateAudit(object before, object after, string userId)
        {
            // there has to be a before or an after
            if (after == null && before == null)
                throw new AbsenceSoftException("GenerateAudit: before and after object maynot both be null");

            Type typeData = (before ?? after).GetType();

            // check to see if there is anything to do here
            if (typeData.GetCustomAttribute<AuditClassNoAttribute>() != null)
                return null;

            bool isDelete = (before != null && after == null);
            string primaryEntityId = (string)(before ?? after).GetType().GetProperty("Id").GetValue(before ?? after);
            string userName = "na";
            if (User.Current != null && !string.IsNullOrWhiteSpace(User.Current.DisplayName))
                userName = User.Current.DisplayName;

            // if before is null, then this is a create and just copy the new values
            // into the change history
            List<ChangeItem> theChanges = new List<ChangeItem>();
            List<ChangeHistory> rtVal = new List<ChangeHistory>();
            GenerateChangeItems(theChanges, before, after, userId, string.Format("/{0}",typeData.Name));

            if(theChanges.Count == 0)
                return rtVal;

            // now split out any new roots
            if (theChanges.Any(c => c.NewRoot))
            {
                ChangeHistory current = new ChangeHistory();

                bool addNext = true;
                foreach (ChangeItem item in theChanges)
                {
                    // new root is empty, the next row down is the new one
                    if(item.NewRoot)
                    {
                        addNext = true;
                        continue;
                    }

                    if (addNext)
                    {
                        addNext = false;
                        current = new ChangeHistory()
                        {
                            UserId = userId,
                            PrimaryEntityId = primaryEntityId,
                            ChangeDate = DateTime.UtcNow,
                            ClassName = typeData.Name,
                            UserName = userName,
                            Items = theChanges,
                            IsDelete = isDelete,
                            ModifiedById = userId,
                            CreatedById = userId
                        };
                        rtVal.Add(current);
                    }

                    current.Items.Add(item);
                }
            }
            else
            {
                // there's just the one level so don't worry about it
                rtVal.Add(new ChangeHistory()
                {
                    UserId = userId,
                    PrimaryEntityId = primaryEntityId,
                    ChangeDate = DateTime.UtcNow,
                    ClassName = typeData.Name,
                    UserName = userName,
                    Items = theChanges,
                    IsDelete = isDelete,
                    ModifiedById = userId,
                    CreatedById = userId
                });
            }

            return rtVal;
        }

        public static void GenerateChangeItems(List<ChangeItem> theItems, object before, object after, string userId, string currentPath)
        {
            object forTypeData = before ?? after;

            // this is bad, but keep on moving
            if(forTypeData == null)
                return;

            // if the property is ID, set it on the change item
            Type typeData = forTypeData.GetType();

            // we have some recursive types. you might need this someday just like I did
            //Debug.WriteLine(currentPath);

            // never auditted skip
            if(typeData.GetCustomAttribute<AuditClassNoAttribute>() != null)
                return;

            // check for never follow (do at the top of this method)
            if (typeData.GetCustomAttribute<AuditClassNeverNestedAttribute>() != null)
                return;

            // or if it's always audited as a root level object then start all over, we throw a blank row in the collection
            // and we will split it all in the method that called this
            if (typeData.GetCustomAttribute<AuditTreatAsRootAttribute>() != null && !string.IsNullOrWhiteSpace(currentPath))
            {
                theItems.Add(new ChangeItem() { NewRoot = true });
                currentPath = "";
            }
            
            string auditObjectId = "";

            // first check to see if there are attributes defined for the primary key
            // and if so, use those otherwise look for a property named Id
            List<PropertyInfo> pks = typeData.GetProperties()
                                .Where(w => w.GetCustomAttribute<AuditClassPrimaryKeyAttribute>() != null)
                                .OrderBy(o=> o.GetCustomAttribute<AuditClassPrimaryKeyAttribute>().KeyOrder)                
                                .ToList();

            if(pks.Count == 0)
            {
                // otherwise check if there is an id on this object and if there is
                PropertyInfo pid = (typeData.GetProperty("Id") ?? typeData.GetProperty("ID")) ?? typeData.GetProperty("id");
                if (pid != null)
                    auditObjectId = (pid.GetValue(forTypeData) ?? "").ToString();
            }
            else
            {
                int cnt = 0;
                foreach (PropertyInfo kpi in pks)
                    auditObjectId += string.Format("{0}{1}", cnt++ > 0 ? "~" : "", (kpi.GetValue(forTypeData) ?? ""));
            }

            if (currentPath.Contains("[{0}]"))
                currentPath = string.Format(currentPath, auditObjectId);

            // if the audit override interface exists then only call that and return its result
            if (after != null && forTypeData is IAuditOverride)
            {
                AuditChangeAddRange(theItems, ((IAuditOverride)after).GetAuditChangeItems(before, after), currentPath);
                return;
            }

            // now included changes
            if (after != null && forTypeData is IAuditInclude)
                AuditChangeAddRange(theItems, ((IAuditInclude)after).GetAuditChangeItems(before, after), currentPath);

            // get a list of properties that have been named at the class level to ignore
            // this is done so that a base class properties can be ignored at a derived level
            List<string> ignoreThese = typeData.GetCustomAttributes<AuditClassIgnorePropertyAttribute>().Select(c => c.IgnorePropertyName).ToList();

            // otherwise, get the list of properties for before and after
            // don't audit things we are told not do, or that don't get saved to the db
            //List<PropertyInfo> pi = typeData.GetProperties()
            //        .Where(pil=> pil.CanRead && (pil.GetCustomAttribute<AuditPropertyNoAttribute>() == null || pil.GetCustomAttribute<BsonIgnoreAttribute>() != null))
            //        .ToList();
            List<PropertyInfo> pi = new List<PropertyInfo>();
            foreach (PropertyInfo ppp in typeData.GetProperties())
            {
                if (!ppp.CanRead)
                    continue;

                if (ppp.GetCustomAttribute<BsonIgnoreAttribute>() != null)
                    continue;

                if (ppp.GetCustomAttribute<AuditPropertyNoAttribute>() != null)
                    continue;

                if (ignoreThese.Any(a => a == ppp.Name))
                    continue;

                pi.Add(ppp);
            }

            // loop across all the properties that are included in the audit
            foreach (PropertyInfo p in pi)
            {
                // figure out if we are adding, deleting or updating and 
                // get the values along the way
                AuditAction action = AuditAction.Save;

                object bv = null;
                if (before == null)
                    action = AuditAction.Create;
                else
                    bv = p.GetValue(before);

                object av = null;
                if (after == null)
                    action = AuditAction.Delete;
                else
                    av = p.GetValue(after);

                ChangeItem ci = new ChangeItem()
                {
                    ClassType = p.PropertyType.Name,
                    PropertyName = p.Name,
                    Action = action,
                    ChangePath = currentPath,
                    AuditObjectId = auditObjectId,
                };

                // fix up the nullable type name
                if (p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    ci.ClassType = Nullable.GetUnderlyingType(p.PropertyType).Name;

                // the first is all the base types
                if(p.PropertyType.IsValueType || (p.PropertyType.IsClass && p.PropertyType.UnderlyingSystemType.Name == "String"))
                {
                    // see if the values have changed
                    if(!object.Equals(bv,av))
                    {
                        if (bv == null)
                            ci.Before = "null";
                        else
                            ci.Before = bv.ToString();

                        if (av == null)
                            ci.After = "null";
                        else
                            ci.After = av.ToString();

                        theItems.Add(ci);
                    }                                       // if(!object.Equals(bv,av))

                    // all done with this one next property
                    continue;
                }                                           // if(p.PropertyType.IsValueType || (p.PropertyType.IsClass && p.PropertyType.UnderlyingSystemType.Name == "String"))

                string newPath = string.Format("{0}/{1}", currentPath, p.Name);

                // check if it is is a class and not a generic, if so, then recurse (because the first curse did not work?)
                if(p.PropertyType.IsClass && !p.PropertyType.IsGenericType)
                {
   
                    // take the current item and hand it down to recurse again
                    GenerateChangeItems(theItems,bv, av, userId, newPath);

                    // we have finished with this property, now onto the next
                    continue;
                }

                // lists... oh yippee, lists
                if(p.PropertyType.IsGenericType && p.PropertyType.UnderlyingSystemType.Name.StartsWith("List"))
                {
                    if (p.PropertyType.GetGenericArguments().Length == 0)
                        continue;

                    AuditTheList(theItems, bv, av, userId, p.PropertyType.GetGenericArguments()[0], newPath);

                    // this maybe the last type but if one were to get added
                    continue;
                }

                // if this is hit there is an issue (because I meant to take it out after debugging)
                //throw new AbsenceSoftException("What type is this?");

            }                                               // foreach (PropertyInfo p in pi)

            return;

        }                                                   // public static List<ChangeItem> GenerateChangeItems(object before, object after, string userId)

        public static void AuditTheList(List<ChangeItem> theItems, object beforeList, object afterList, string userId, Type theType, string currentPath)
        {

            // we can only do lists of objects because we need to be able to map "things" together
            // and then look for changes
            if(theType.IsPrimitive || theType.IsValueType)
                return;
          
            // don't know how we got here if this isn't true
            if (!(beforeList is IEnumerable) && !(afterList is IEnumerable))
                return;

            // do the same checks. see if this is auditted
            // check for never follow (do at the top of this method)
            if (theType.GetCustomAttribute<AuditClassNeverNestedAttribute>() != null)
                return;

            if (theType.GetCustomAttribute<AuditClassNoAttribute>() != null)
                return;

            // or if it's always audited as a root level object then start all over
            if (theType.GetCustomAttribute<AuditTreatAsRootAttribute>() != null)
            {
                theItems.Add(new ChangeItem() { NewRoot = true });
                currentPath = "";
            }

            // build the key we are going to use to go from one to the other
            List<PropertyInfo> keys = theType.GetProperties()
                        .Where(pil => pil.CanRead && pil.GetCustomAttribute<AuditClassPrimaryKeyAttribute>() != null)
                        .ToList();

            // if pi = 0 then default to the id property. if there is nothing that we can use as
            // a key to match up before and after then forget it, we're done
            if(keys.Count == 0)
            {
                PropertyInfo pid = theType.GetProperty("Id");
                if (pid == null)
                    return;

                keys.Add(pid);
            }

            IEnumerable be = (beforeList as IEnumerable) ?? new List<object>();
            IEnumerable ae = (afterList as IEnumerable) ?? new List<object>();

            // just a shortcut, if they key count is 1, then no fancy footwork is required
            // so hopefully this will help performance
            string newPath = currentPath + "[{0}]";
            if (keys.Count == 1)
            {
                foreach (object beforeObj in be)
                {
                    object beforeVal = keys[0].GetValue(beforeObj);
                    object afterObj = null;
                    foreach (object after in ae)
                    {
                        if(object.Equals(beforeVal, keys[0].GetValue(after)))
                        {
                            afterObj = after;
                            break;
                        }
                    }

                    GenerateChangeItems(theItems, beforeObj, afterObj, userId, newPath);

                }                       // foreach (object beforeObj in be)

                // now find the adds. These are the objects in the after list that are
                // not in the before list
                foreach (object afterObj in ae)
                {
                    object afterVal = keys[0].GetValue(afterObj);
                    bool found = false;
                    foreach (object before in be)
                    {
                        if(object.Equals(keys[0].GetValue(before),afterVal))
                        {
                            found = true;
                            break;
                        }
                    }                           // foreach (object before in be)

                    // again, these are the adds. Want the ones we did not find
                    if (!found)
                        GenerateChangeItems(theItems, null, afterObj, userId, newPath);

                }                   // foreach (object afterObj in ae)

            }                           /// if(keys.Count == 1)
            else
            {
                // handle multiple keys
                // compare each before to each after, if it's in the before but not the after it's a delete
                //int x = 0;
                foreach (object beforeObj in be)
                {
                    object afterObj = null;
                    foreach (object after in ae)
                    {
                        int keyCnt = 0;
                        foreach (PropertyInfo kk in keys)
                        {
                            object beforeval = kk.GetValue(beforeObj);

                            // if your before primary key is null then there is something wrong with you
                            // so just give up finding a match
                            if (beforeval == null)
                                break;

                            if (object.Equals(beforeval, kk.GetValue(after)))
                            {
                                keyCnt++;
                                if (keyCnt == keys.Count)
                                    break;
                            }
                        }

                        if(keyCnt == keys.Count)
                        {
                            afterObj = after;
                            break;
                        }
                    }

                    GenerateChangeItems(theItems, beforeObj, afterObj, userId, newPath);

                }                           // foreach (object beforeObj in be)

                // now repeate the process with the after collection driving but only call
                // generate change items when it isn't found because this is the adds
                //x = 0;
                foreach (object afterObj in ae)
                {
                    foreach (object before in be)
                    {
                        int keyCnt = 0;
                        foreach (PropertyInfo kk in keys)
                        {
                            object afterVal = kk.GetValue(afterObj);

                            if (object.Equals(kk.GetValue(before), afterVal))
                            {
                                keyCnt++;
                                if (keyCnt == keys.Count)
                                    break;
                            }
                        }

                        // remember we are only worried about adds so those are the ones we did
                        // not find a match for
                        if (keyCnt != keys.Count)
                        {
                            GenerateChangeItems(theItems, null, afterObj, userId, newPath);
                            break;
                        }
                    }

                }                           // foreach (object beforeObj in be)

            }                           // else if(keys.Count == 1)


            // then do the reverse, find all the ones in the after that did not get processed in the before


            return;
        }                           // public List<ChangeItem> AuditTheList(object before, object after, string userId)

        private static void AuditChangeAddRange(List<ChangeItem> current, List<ChangeItem> append, string path)
        {
            foreach (ChangeItem ci in append)
            {
                if(string.IsNullOrWhiteSpace(ci.ChangePath))
                    ci.ChangePath = path;
                else
                    ci.ChangePath = string.Format("{0}/{1}", path, ci.ChangePath);

                current.Add(ci);
            }
        }
    
    }
}
