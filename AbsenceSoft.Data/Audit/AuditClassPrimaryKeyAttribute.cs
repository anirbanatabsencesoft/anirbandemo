﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Audit
{
    [AttributeUsage(AttributeTargets.Property,AllowMultiple=true)]
    public class AuditClassPrimaryKeyAttribute : System.Attribute
    {
        /// <summary>
        /// though not really required, when there are multiple keys we want to guarentee that
        /// we report on them in a consistent order (only used for formatting the object id)
        /// </summary>
        public int KeyOrder { get; private set; }

        /// <summary>
        /// Use this column for a pk instead of id
        /// </summary>
        /// <param name="order">if a compound key then list them in ascending order (number doesn't matter, they will be in numerical order)</param>
        public AuditClassPrimaryKeyAttribute(int order)
        {
            KeyOrder = order;
        }
    }
}
