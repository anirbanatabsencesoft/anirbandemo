﻿using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace AbsenceSoft.Data.Audit
{
    [Serializable]
    [AuditClassNo]
    [Repository(ConnectionStringName = "MongoServerSettings_Audit")]
    public class LoginHistory : BaseEntity<LoginHistory>
    {
        /// <summary>
        /// Gets or sets the user id for the entity.
        /// </summary>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the customer id for the entity.
        /// </summary>
        [BsonIgnoreIfNull, BsonRepresentation(BsonType.ObjectId)]
        public string CustomerId { get; set; }

        /// <summary>
        /// Records the user's IP Address from where they logged in.
        /// </summary>
        [BsonRequired]
        public string IpAddress { get; set; }

        /// <summary>
        /// Records the user's browser string from when they logged in.
        /// </summary>
        [BsonRequired]
        public string Browser { get; set; }

        /// <summary>
        /// Records the user's roles assigned to him.
        /// </summary>
        [BsonIgnoreIfNull]
        public List<string> UserRoles { get; set; }

        /// <summary>
        /// Records the Employer Access assigned to the user.
        /// </summary>
        [BsonIgnoreIfNull]
        public List<EmployerAccess> EmployerAccess { get; set; }

        /// <summary>
        /// Records Login\logout success or failure
        /// </summary>
        [BsonRequired]
        public bool IsSuccess { get; set; }

        /// <summary>
        /// Records if Sso login
        /// </summary>
        [BsonIgnoreIfNull]
        public bool IsSso { get; set; }

        /// <summary>
        /// Records status information like locked, disabled, etc.
        /// </summary>
        [BsonRequired]
        public LoginType LoginStatus { get; set; }

        /// <summary>
        /// Records email information for user
        /// </summary>
        [BsonIgnoreIfNull]
        public string Email { get; set; }

        /// <summary>
        /// Records subject of Sso login
        /// </summary>
        [BsonIgnoreIfNull]
        public string Subject { get; set; }
    }
}
