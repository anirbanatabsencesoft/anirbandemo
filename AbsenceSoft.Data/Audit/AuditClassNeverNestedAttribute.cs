﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Audit
{
    [AttributeUsage(AttributeTargets.Class,AllowMultiple=false)]
    public class AuditClassNeverNestedAttribute : System.Attribute
    {
    }
}
