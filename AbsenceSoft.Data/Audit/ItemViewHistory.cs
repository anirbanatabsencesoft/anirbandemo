﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace AbsenceSoft.Data.Audit
{
    [Serializable]
    [AuditClassNo]
    [Repository(ConnectionStringName = "MongoServerSettings_Audit")]
    public class ItemViewHistory : BaseCustomerEntity<ItemViewHistory>
    {
        /// <summary>
        /// Gets or sets the name of the class/type.
        /// </summary>
        /// <value>
        /// The name of the class or type of item viewed.
        /// </value>
        public string ItemType { get; set; }

        /// <summary>
        /// Gets or sets the item identifier.
        /// </summary>
        /// <value>
        /// The item identifier.
        /// </value>
        [BsonRequired, BsonRepresentation(BsonType.ObjectId)]
        public string ItemId { get; set; }

        /// <summary>
        /// Gets or sets the employer identifier.
        /// </summary>
        /// <value>
        /// The employer identifier.
        /// </value>
        [BsonRepresentation(BsonType.ObjectId), BsonIgnoreIfNull]
        public string EmployerId { get; set; }
    }


    public static class ItemViewHistoryExtensions
    {
        /// <summary>
        /// Generates the item view audit history record.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entity">The entity this method extends.</param>
        /// <param name="user">The user to create the record for, if null, then the current user will be used.</param>
        /// <param name="metaItems">The meta items as actions that each take the entity and meta-data document of the audit record.</param>
        public static void GenerateItemView<TEntity>(this TEntity entity, User user, params Action<TEntity, BsonDocument>[] metaItems)
            where TEntity : BaseEntity<TEntity>, new()
        {
            if (entity != null && !entity.IsNew)
            {
                if (user == null)
                    user = User.Current;
                if (user == null)
                    return;

                Task.Run(() =>
                {
                    var item = new ItemViewHistory()
                    {
                        ItemType = BaseEntity<TEntity>.ClassTypeName,
                        ItemId = entity.Id,
                        CreatedBy = user,
                        ModifiedBy = user,
                        CustomerId = user.CustomerId
                    };
                    if (metaItems != null && metaItems.Any())
                        foreach (var action in metaItems)
                            action(entity, item.Metadata);
                    var employerIdProp = typeof(TEntity).GetProperty("EmployerId", BindingFlags.Public | BindingFlags.Instance);
                    if (employerIdProp != null)
                        item.EmployerId = employerIdProp.GetValue(entity) as string;
                    item.Save();
                }).ContinueWith((Task boom) =>
                {
                    Log.Error(boom.Exception.Flatten());
                }, TaskContinuationOptions.OnlyOnFaulted);
            }
        }

        /// <summary>
        /// Generates the item view.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="user">The user.</param>
        public static void GenerateCaseView(this Case entity, User user = null)
        {
            entity.GenerateItemView(user,
                (e, m) => m.SetRawValue("CaseNumber", e.CaseNumber),
                (e, m) => m.SetRawValue("Reason", e.Reason != null ? e.Reason.Name : string.Empty),
                (e, m) => m.SetRawValue("ReasonId", e.Reason != null ? e.Reason.Id : string.Empty),
                (e, m) => m.SetRawValue("EmployerName", e.EmployerName),
                (e, m) => m.SetRawValue("EmployeeId", e.Employee.Id),
                (e, m) => m.SetRawValue("EmployeeNumber", e.Employee.EmployeeNumber),
                (e, m) => m.SetRawValue("EmployeeName", e.Employee.FullName),
                (e, m) => m.SetRawValue("FirstName", e.Employee.FirstName),
                (e, m) => m.SetRawValue("LastName", e.Employee.LastName),
                (e, m) => m.SetRawValue("WorkState", e.Employee.WorkState),
                (e, m) => m.SetRawValue("OfficeLocation", e.Employee.GetOfficeLocationDisplay()));
        }

        /// <summary>
        /// Generates the item view.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="user">The user.</param>
        public static void GenerateEmployeeView(this Employee entity, User user = null)
        {
            entity.GenerateItemView(user,
                (e, m) => m.SetRawValue("EmployerName", e.EmployerName),
                (e, m) => m.SetRawValue("EmployeeNumber", e.EmployeeNumber),
                (e, m) => m.SetRawValue("FirstName", e.FirstName),
                (e, m) => m.SetRawValue("LastName", e.LastName),
                (e, m) => m.SetRawValue("EmployeeName", e.FullName),
                (e, m) => m.SetRawValue("WorkState", e.WorkState),
                (e, m) => m.SetRawValue("OfficeLocation", entity.GetOfficeLocationDisplay()),
                (e, m) => m.SetRawValue("HireDate", e.HireDate));
        }
    }
}
