﻿namespace AbsenceSoft.Data
{
    /// <summary>
    /// Encapsulates all constant values used within the data tier or by 
    /// reference of the data tier.
    /// </summary>
    public static class DataConstants
    {
        /// <summary>
        /// The ETL synchronizer sort by index name. This index is built in
        /// the setup utility (which references this) and then used as a hint
        /// in the raw MongoDB queries used by the ETL process when sorting
        /// and fetching results for sync to the data warehouse.
        /// </summary>
        public const string EtlSyncSortByIndexName = "Etl_Sync";
    }
}
