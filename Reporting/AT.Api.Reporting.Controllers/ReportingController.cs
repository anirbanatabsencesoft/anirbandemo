﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Reporting;
using AT.Api.Core;
using AT.Common.Core;
using AT.Common.Log;
using AT.Common.Security;
using AT.Data.Reporting.Arguments;
using AT.Data.Reporting.Model;
using AT.Entities.Reporting.Classes;
using AT.Entities.Reporting.Entities;
using AT.Entities.Reporting.Interfaces;
using AT.Logic.Arguments;
using AT.Logic.Reporting;
using AT.Logic.Reporting.Arguments;
using AT.Logic.Reporting.Reports;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace AT.Api.Reporting.Controllers
{
    /// <summary>
    /// The Reporting Controller Class
    /// </summary>
    [RoutePrefix("v1")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ReportingController : BaseWebApiController<IReport, SearchReportArgs, SaveReportArgs, DeleteReportArgs>
    {
        private const string _userNotAuthorized = "User is not authenticated.";

        /// <summary>
        /// The logic part.
        /// To be instantiated in constructor
        /// </summary>
        public ReportingLogic ReportingLogic { get; private set; }

        /// <summary>
        /// Constructor for the initialization
        /// </summary>
        public ReportingController()
        {
            ReportingLogic = new ReportingLogic(AuthenticatedUser);
        }

        /// <summary>
        /// Get all the reports from the database
        /// </summary>
        /// <returns></returns>
        [HttpGet, Secure]
        [Route("reports/adhoc/getall")]
        public async Task<ResultSet<List<AdHocReportView>>> GetAdhocReports()
        {
            ResultSet<List<AdHocReportView>> result = new ResultSet<List<AdHocReportView>>();
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    result.Data = await ReportingLogic.GetAdhocReportsTypeAsync();
                    result.Success = true;
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.ExpectationFailed;
                result.Message = ex.Message;
                Logger.Error(ex);
            }

            return result;
        }


        /// <summary>
        /// Get all the reports from the database
        /// </summary>
        /// <returns></returns>
        [HttpGet, Secure]
        [Route("reports/adhoc/get/name/{name}")]
        public async Task<ResultSet<Report>> GetAdhocReportByName(string name)
        {
            ResultSet<Report> result = new ResultSet<Report>();
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    result.Data = await ReportingLogic.GetReportDefinitionByNameAsync(name);
                    result.Success = true;
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.ExpectationFailed;
                result.Message = ex.Message;
                Logger.Error(ex);
            }

            return result;
        }

        /// <summary>
        /// Get all the reports from the database
        /// </summary>
        /// <returns></returns>
        [HttpGet, Secure]
        [Route("reports/adhoc/columns/reporttype/{id}")]
        public async Task<ResultSet<List<ReportColumn>>> GetAdhocReportColumns(string id)
        {
            ResultSet<List<ReportColumn>> result = new ResultSet<List<ReportColumn>>();
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    result.Data = await ReportingLogic.GetAdHocReportColumnsAsync(id);
                    result.Success = true;
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.ExpectationFailed;
                result.Message = ex.Message;
                Logger.Error(ex);
            }

            return result;
        }

        #region Abstract Members. This will be mapped later.
        /// <summary>
        /// Get the Report by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override Task<ResultSet<IReport>> GetById(long id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Search a report
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public override Task<ResultSet<IEnumerable<IReport>>> Search(SearchReportArgs arg)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Save a report
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public override Task<ResultSet<IReport>> Save(SaveReportArgs arg)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Delete a report. Set only AdhocReportId in the argument.
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        [HttpDelete, Secure]
        [Route("reports/adhoc/delete")]
        public override async Task<ResultSet<int>> Delete(DeleteReportArgs arg)
        {
            ResultSet<int> result = new ResultSet<int>();
            try
            {
                if (arg == null)
                {
                    result.Message = "Invalid parameter.";
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.BadRequest;
                    return result;
                }

                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    //set the values
                    arg.UserId = AuthenticatedUser.Id;
                    arg.UserKey = AuthenticatedUser.Key;

                    result.Data = await ReportingLogic.DeleteAdhocReportAsync(arg);
                    result.Success = true;
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.StatusCode = HttpStatusCode.ExpectationFailed;
                Logger.Error(ex);
            }

            return result;
        }


        /// <summary>
        /// Get a report by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public override Task<ResultSet<IReport>> GetByKey(string key)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Search a report
        /// </summary>
        /// <returns></returns>
        public override Task<ResultSet<IEnumerable<IReport>>> SearchByQuery()
        {
            throw new NotImplementedException();
        }
        #endregion

        /// <summary>
        /// Returns the list of Reports filtered by Report type id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Secure]
        [Route("reports/adhoc/reporttype/get/id/{id}")]
        public async Task<ResultSet<ReportType>> GetReportTypeById(string id)
        {
            ResultSet<ReportType> result = new ResultSet<ReportType>();
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    result.Data = await ReportingLogic.GetReportTypeByIdAsync(id);
                    result.Success = true;
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.ExpectationFailed;
                result.Message = ex.Message;
                Logger.Error(ex);
            }

            return result;
        }

        /// <summary>
        /// Returns all the report types
        /// </summary>
        /// <returns></returns>
        [HttpGet, Secure]
        [Route("reports/adhoc/reporttype/getall")]
        public async Task<ResultSet<List<ReportType>>> GetAdhocReportTypes()
        {
            ResultSet<List<ReportType>> result = new ResultSet<List<ReportType>>();
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    result.Data = await ReportingLogic.GetReportTypesAsync(adhocOnly: true);
                    result.Success = true;
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.ExpectationFailed;
                result.Message = ex.Message;
                Logger.Error(ex);
            }

            return result;
        }

        /// <summary>
        /// Gets all saved report definitions for the passed UserId.
        /// </summary>
        /// <returns>List of IReport associated with the UserId.</returns>
        [HttpGet, Secure]
        [Route("reports/adhoc/user/savedreports")]
        public async Task<ResultSet<List<Report>>> GetUserSavedReports()
        {
            ResultSet<List<Report>> result = new ResultSet<List<Report>>();
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                   
                        result.Data = await ReportingLogic.GetUserReportDefinitions();
                   
                    
                    result.Success = true;
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.ExpectationFailed;
                result.Message = ex.Message;
                Logger.Error(ex);
            }

            return result;
        }

        /// <summary>
        /// Get the report definition for the passed report Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Secure]
        [Route("reports/adhoc/user/savedreport")]
        public async Task<ResultSet<AbsenceSoft.Data.Reporting.ReportDefinition>> GetUserSavedReport(string id)
        {
            ResultSet<AbsenceSoft.Data.Reporting.ReportDefinition> result = new ResultSet<AbsenceSoft.Data.Reporting.ReportDefinition>();
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                   
                    result.Data = await ReportingLogic.GetUserReportDefinition(id);
                    result.Success = true;
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.ExpectationFailed;
                result.Message = ex.Message;
                Logger.Error(ex);
            }

            return result;
        }

        /// <summary>
        /// Saves the definition of a report.
        /// </summary>
        /// <param name="saveReportArgs">Save argument for report included definition.</param>       
        /// <returns>The report definition.</returns>        
        [HttpPost, Secure]
        [Route("reports/adhoc/save")]
        public async Task<ResultSet<ReportColumnDefinition>> SaveAdHocReport(SaveReportArgs saveReportArgs)
        {
            ResultSet<ReportColumnDefinition> result = new ResultSet<ReportColumnDefinition>();
           
            try
            {
                if (saveReportArgs == null)
                {
                    result.Message = "Invalid parameter.";
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.BadRequest;
                    return result;
                }

                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    var savedata = ReportingLogic.GetAdhocReports();
                    bool isDuplicate = false;
                    foreach (var s in savedata)
                    {
                        if (s.Name == saveReportArgs.ColumnDefinition.Name)
                        {
                            isDuplicate = true;
                        }


                    }
                    if (!isDuplicate)
                    {
                        result.Data = await ReportingLogic.SaveReportDefinitionAsync(saveReportArgs);
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Report Name already exist";
                    }

                    if (result.Data != null)
                    {                       
                        result.Success = true;
                        result.RecordCount = 1;
                    }
                    else
                    {
                        result.Message = "Unable to process the request, please try again later";
                    }
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = System.Net.HttpStatusCode.ExpectationFailed;
                result.Message = ex.ToString();
                Logger.Error(ex);
            }

            return result;
        }

        /// <summary>
        /// Saves the report under a new name. 
        /// </summary>
        /// <param name="saveReportArgs">Save argument for report included definition.</param>       
        /// <returns>The report definition.</returns>
        [HttpPost, Secure]
        [Route("reports/adhoc/saveas")]
        public async Task<ResultSet<ReportColumnDefinition>> SaveAsAdHocReport(SaveReportArgs saveReportArgs)
        {
            ResultSet<ReportColumnDefinition> result = new ResultSet<ReportColumnDefinition>();
            try
            {
                if (saveReportArgs == null)
                {
                    result.Message = "Invalid parameter.";
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.BadRequest;
                    return result;
                }

                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    var data = await ReportingLogic.SaveAsReportDefinitionAsync(saveReportArgs);

                    if (data != null)
                    {
                        result.Data = data;
                        result.Success = true;
                        result.RecordCount = 1;
                    }
                    else
                    {
                        result.Message = "Unable to process the request, please try again later";
                    }
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = System.Net.HttpStatusCode.ExpectationFailed;
                result.Message = ex.ToString();
                Logger.Error(ex);
            }

            return result;
        }

        /// <summary>
        /// Get ESS Reports based on the type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpGet, Secure]
        [Route("reports/ess/type/{type}")]
        public async Task<ResultSet<List<BaseReport>>> GetESSReports(string type)
        {
            ResultSet<List<BaseReport>> result = new ResultSet<List<BaseReport>>();
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    result.Data = await ReportingLogic.GetESSReportAsync(type);
                    result.Success = true;
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = System.Net.HttpStatusCode.ExpectationFailed;
                result.Message = ex.Message;
                Logger.Error(ex);
            }

            return result;
        }

        /// <summary>
        /// Get the list of reports by main category
        /// </summary>
        /// <param name="mainCategory"></param>
        /// <returns></returns>
        [HttpGet, Secure]
        [Route("reports/operation/get/maincategory/{mainCategory}")]
        public async Task<ResultSet<List<BaseReport>>> GetReportsByMainCategory(string mainCategory)
        {
            ResultSet<List<BaseReport>> result = new ResultSet<List<BaseReport>>();
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    result.Data = await ReportingLogic.GetReportByMainCategoryAsync(mainCategory);
                    result.Success = true;
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.ExpectationFailed;
                result.Message = ex.Message;
                Logger.Error(ex);
            }

            return result;
        }

        /// <summary>
        /// Get the list of reports by category
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        [HttpGet, Secure]
        [Route("reports/operation/get/category/{category}")]
        public async Task<ResultSet<List<BaseReport>>> GetReportsByCategory(string category)
        {
            ResultSet<List<BaseReport>> result = new ResultSet<List<BaseReport>>();
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    result.Data = await ReportingLogic.GetReportByCategoryAsync(category);
                    result.Success = true;
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.ExpectationFailed;
                result.Message = ex.Message;
                Logger.Error(ex);
            }

            return result;
        }


        /// <summary>
        /// Returns the report by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Secure]
        [Route("reports/operation/get/id/{id}")]
        public async Task<ResultSet<BaseReport>> GetReportById(string id)
        {
            ResultSet<BaseReport> result = new ResultSet<BaseReport>();
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    result.Data = await ReportingLogic.GetBaseReportByIdAsync(id);
                    result.Success = true;
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.ExpectationFailed;
                result.Message = ex.Message;
                Logger.Error(ex);
            }

            return result;
        }


        /// <summary>
        /// Get the criteria for the report id
        /// </summary>
        /// <param name="reportId"></param>
        /// <returns></returns>
        [HttpGet, Secure]
        [Route("reports/operation/criteria/get/reportid/{reportId}")]
        public async Task<ResultSet<FilterCriteria>> GetCriteria(string reportId)
        {
            ResultSet<FilterCriteria> result = new ResultSet<FilterCriteria>();
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    result.Data = await ReportingLogic.GetCriteria(reportId);
                    result.Success = true;
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.ExpectationFailed;
                result.Message = ex.Message;
                Logger.Error(ex);
            }

            return result;
        }

        /// <summary>
        /// Returns all base reports
        /// </summary>
        /// <returns></returns>
        [HttpGet, Secure]
        [Route("reports/operation/getall")]
        public async Task<ResultSet<List<BaseReport>>> GetReports()
        {
            ResultSet<List<BaseReport>> result = new ResultSet<List<BaseReport>>();
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    result.Data = await ReportingLogic.GetBaseReportsAsync();
                    result.Success = true;
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.ExpectationFailed;
                result.Message = ex.Message;
                Logger.Error(ex);
            }

            return result;
        }

        /// <summary>
        /// Returns all query data for a user
        /// </summary>
        /// <returns></returns>
        [HttpGet, Secure]
        [Route("reports/user/criteria/data")]
        public async Task<ResultSet<UserReportCriteriaData>> GetUserCriteriaData()
        {
            ResultSet<UserReportCriteriaData> result = new ResultSet<UserReportCriteriaData>();
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    result.Data = await ReportingLogic.GetUserReportCriteriaDataAsync();
                    result.Success = true;
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.ExpectationFailed;
                result.Message = ex.Message;
                Logger.Error(ex);
            }

            return result;
        }

        /// <summary>
        /// Returns all recently used list of adhoc report definition
        /// </summary>
        /// <returns></returns>
        [HttpGet, Secure]
        [Route("reports/adhoc/recent/{limit}")]
        public async Task<ResultSet<List<Report>>> GetRecentReportDefinitionList(int? limit)
        {
            ResultSet<List<Report>> result = new ResultSet<List<Report>>();
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    limit = limit ?? 10;
                    result.Data = await ReportingLogic.GetRecentReportDefinitionListAsync(limit.Value);
                    result.Success = true;
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.ExpectationFailed;
                result.Message = ex.Message;
                Logger.Error(ex);
            }

            return result;
        }

        /// <summary>
        /// Execute a report and returns the respecive result
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        [HttpPost, Secure]
        [Route("reports/operation/execute")]
        public async Task<ResultSet<Logic.Reporting.Reports.ReportResult>> ExecuteReport(ExecuteReportArgs args)
        {
            ResultSet<Logic.Reporting.Reports.ReportResult> result = new ResultSet<Logic.Reporting.Reports.ReportResult>();
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    result.Data = await ReportingLogic.ExecuteReportAsync(args);
                    result.Success = true;
                    result.RecordCount = result.Data?.Items?.Count ?? result.Data?.Groups?.Count;
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.ExpectationFailed;
                result.Message = ex.Message;
                Logger.Error(ex);
            }

            return result;
        }

        /// <summary>
        /// Executes the adhoc report and return result.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        [HttpPost, Secure]
        [Route("reports/adhoc/execute")]
        public async Task<ResultSet<AdHocReportResult>> ExecuteAdhocReport(AdhocExecuteReportArgs args)
        {
            ResultSet<AdHocReportResult> result = new ResultSet<AdHocReportResult>();
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    //check args
                    if (string.IsNullOrWhiteSpace(args.ReportDefinitionId) || args.ReportDefinition == null)
                    {
                        result.Message = "Invalid report definition parameters";
                        result.StatusCode = System.Net.HttpStatusCode.BadRequest;
                    }
                    else
                    {
                        try
                        {
                            if (args.ReportDefinition != null)
                            {
                                result.Data = await ReportingLogic.ExecuteAdhocReportAsync(args.ReportDefinition, args.Fields, args.Limit, args.Offset);
                            }
                            else
                            {
                                result.Data = await ReportingLogic.ExecuteAdhocReportAsync(args.ReportDefinitionId, args.Fields, args.Limit, args.Offset);
                            }

                            //set message
                            if (result.Data.Success == false)
                            {
                                result.Success = false;
                                result.Message = result.Data.Error;
                                result.StatusCode = System.Net.HttpStatusCode.ExpectationFailed;
                            }
                            else
                            {
                                result.Success = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            result.Message = ex.Message;
                            result.StatusCode = System.Net.HttpStatusCode.ExpectationFailed;
                            Logger.Error(ex);
                        }
                    }
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.StatusCode = System.Net.HttpStatusCode.ExpectationFailed;
                Logger.Error(ex);
            }

            return result;
        }

        /// <summary>
        /// Returns all the shared report for the logged-in user
        /// </summary>
        /// <param name="reportId"></param>
        /// <param name="scheduledReportId"></param>
        /// <param name="teamName"></param>
        /// <param name="teamId"></param>
        /// <param name="teamKey"></param>
        /// <param name="roleId"></param>
        /// <param name="roleName"></param>
        /// <returns></returns>
        [HttpGet, Secure, Route("reports/shared/get")]
        public async Task<ResultSet<List<SharedReport>>> GetSharedReport(long? reportId = null, long? scheduledReportId = null, string teamName = null, long? teamId = null, string teamKey = null, long? roleId = null, string roleName = null)
        {
            ResultSet<List<SharedReport>> result = new ResultSet<List<SharedReport>>();
            GetSharedReportArgs args = new GetSharedReportArgs();
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    args.CustomerId = AuthenticatedUser.CustomerId;
                    args.CustomerKey = AuthenticatedUser.CustomerKey;
                    args.UserId = AuthenticatedUser.Id;
                    args.UserKey = AuthenticatedUser.Key;

                    //set get variables
                    args.ReportId = reportId;
                    args.ScheduledReportId = scheduledReportId;
                    args.TeamName = teamName;
                    args.TeamId = teamId;
                    args.TeamKey = teamKey;
                    args.RoleId = roleId;
                    args.RoleName = roleName;

                    if (args.IsValid)
                    {
                        result.Data = await ReportingLogic.GetSharedReport(args);
                        result.Success = true;
                        result.RecordCount = result.Data != null ? result.Data.Count : 0;
                    }
                    else
                    {
                        result.Message = args.Message;
                        result.StatusCode = System.Net.HttpStatusCode.ExpectationFailed;
                        Logger.Error(result.Message);
                    }
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.StatusCode = System.Net.HttpStatusCode.ExpectationFailed;
                Logger.Error(ex);
            }

            return result;
        }

        /// <summary>
        /// Returns the saved operations report for the user
        /// </summary>
        /// <returns></returns>
        [HttpGet, Secure, Route("reports/operation/saved")]
        public async Task<ResultSet<List<SharedReport>>> GetSavedOperationsReport()
        {
            ResultSet<List<SharedReport>> result = new ResultSet<List<SharedReport>>();
            GetSharedReportArgs args = new GetSharedReportArgs();
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    args.CustomerId = AuthenticatedUser.CustomerId;
                    args.CustomerKey = AuthenticatedUser.CustomerKey;
                    args.UserId = AuthenticatedUser.Id;
                    args.UserKey = AuthenticatedUser.Key;
                    args.SharedWith = "SELF";
                    
                    if (args.IsValid)
                    {
                        result.Data = await ReportingLogic.GetSavedOperationReports(args);
                        result.Success = true;
                        result.RecordCount = result.Data != null ? result.Data.Count : 0;
                    }
                    else
                    {
                        result.Message = args.Message;
                        result.StatusCode = System.Net.HttpStatusCode.ExpectationFailed;
                        Logger.Error(result.Message);
                    }
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.StatusCode = System.Net.HttpStatusCode.ExpectationFailed;
                Logger.Error(ex);
            }

            return result;
        } 


        /// <summary>
        /// Defines the method for report sharing
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        [HttpPost, Secure, Route("reports/shared/save")]
        public async Task<ResultSet<SharedReport>> ShareReport(SaveSharedReportArgs args)
        {
            var result = new ResultSet<SharedReport>();
            if (args.IsValid)
            {
                try
                {
                    if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                    {
                        result.Data = await ReportingLogic.SaveSharedReport(args);
                        result.Success = true;
                    }
                    else
                    {
                        result.Message = _userNotAuthorized;
                        result.StatusCode = HttpStatusCode.Unauthorized;
                    }
                }
                catch (Exception ex)
                {
                    result.Message = ex.Message;
                    result.StatusCode = System.Net.HttpStatusCode.ExpectationFailed;
                    Logger.Error(ex);
                }
            }
            else
            {
                result.Message = args.Message;
                result.StatusCode = System.Net.HttpStatusCode.ExpectationFailed;
            }

            return result;
        }

        /// <summary>
        /// Delete a shared report
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete, Secure, Route("reports/shared/delete/{id}")]
        public async Task<ResultSet<bool>> DeleteSharedReport(long id)
        {
            var result = new ResultSet<bool>();
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    var d = await ReportingLogic.DeleteSharedReport(id);
                    if (d > 0)
                    {
                        result.Data = true;
                        result.Success = true;
                    }
                    else
                    {
                        result.Data = false;
                        result.Message = "No data found";
                    }
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.StatusCode = System.Net.HttpStatusCode.ExpectationFailed;
                Logger.Error(ex);
            }

            return result;
        }

        /// <summary>
        /// The method allows making an asynchronous report request.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        [HttpPost, Secure]
        [Route("reports/request")]
        public async Task<ResultSet<ReportStatusResult>> RequestReport(RequestReportArgs args)
        {
            ResultSet<ReportStatusResult> result = new ResultSet<ReportStatusResult>();
            try
            {
                if (AuthenticatedUser != null || (Context?.Response?.StatusCode != 403))
                {
                    result.Data = await ReportingLogic.AddReportRequestAsync(args);
                    if (result.Data.Id != 0)
                    {
                        result.Success = true;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = result.Data.Message;
                    }
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.ExpectationFailed;
                result.Message = ex.Message;
                Logger.Error(ex);
            }
            return result;
        }

        /// <summary>
        /// The method Cancel's a requested report
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, Secure]
        [Route("reports/cancel/{id}")]
        public async Task<ResultSet<ReportStatusResult>> CancelReport(long id)
        {
            ResultSet<ReportStatusResult> result = new ResultSet<ReportStatusResult>();
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    var data = await ReportingLogic.CancelRequestReportAsync(id);
                    if (data != null)
                    {
                        result.Success = true;
                        result.RecordCount = 1;
                    }
                    else
                    {
                        result.Success = false;
                        result.RecordCount = 0;
                        result.Message = "Unable to cancel request";
                    }
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.ExpectationFailed;
                result.Message = ex.Message;
                Logger.Error(ex);
            }
            return result;
        }

        /// <summary>
        /// Allows fetching the status of the report requested.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Secure]
        [Route("reports/status/{id}")]
        public async Task<ResultSet<ReportStatusResult>> GetReportStatus(long id)
        {
            ResultSet<ReportStatusResult> result = new ResultSet<ReportStatusResult>();
            try
            {
                if (AuthenticatedUser != null || (Context?.Response?.StatusCode != 403))
                {
                    result.Data = await ReportingLogic.GetReportStatusResultAsync(id, AuthenticatedUser.Id == 0 ? (long?)null : AuthenticatedUser.Id, AuthenticatedUser.Key);
                    result.Success = true;
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.ExpectationFailed;
                result.Message = ex.Message;
                Logger.Error(ex);
            }
            return result;
        }

        /// <summary>
        /// The method allows fetching the report results of a completed report request.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Secure]
        [Route("reports/results/{id}")]
        public async Task<ResultSet<CompletedReportResult>> GetCompletedReportResult(long id)
        {
            ResultSet<CompletedReportResult> result = new ResultSet<CompletedReportResult>();
            try
            {
                if (AuthenticatedUser != null || (Context?.Response?.StatusCode != 403))
                {
                    result.Data = await ReportingLogic.GetCompletedReportResultAsync(id);
                    result.Success = true;
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.ExpectationFailed;
                result.Message = ex.Message;
                Logger.Error(ex);
            }
            return result;
        }

        /// <summary>
        /// The method allows dowloading the exported report files.
        /// </summary>
        /// <param name="id">Report Id</param>
        /// <param name="format">Export Formate (pdf, csv)</param>
        /// <returns></returns>
        [HttpGet, Secure]
        [Route("reports/download/{id}/{format}")]
        public async Task<HttpResponseMessage> DownloadReportFile(long id, ExportType format)
        {
            try
            {
                if (AuthenticatedUser != null || (Context?.Response?.StatusCode != 403))
                {
                    var fileBytes = await ReportingLogic.DownloadFile(id, format);
                    var result = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new ByteArrayContent(fileBytes)
                    };
                    string filename = "";
                    if (format == ExportType.PDF)
                    {
                        filename = "report.pdf";
                    }
                    else if (format == ExportType.CSV)
                    {
                        filename = "report.csv";
                    }

                    result.Content.Headers.ContentDisposition =
                        new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                        {
                            FileName = filename
                        };
                    result.Content.Headers.ContentType =
                        new MediaTypeHeaderValue("application/octet-stream");

                    return result;
                }
                else
                {
                    return new HttpResponseMessage(HttpStatusCode.Unauthorized);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
            }
        }

        

       /// <summary>
       /// This method to get the file path for saved report in S3
       /// </summary>
       /// <param name="id"></param>
       /// <param name="format"></param>
       /// <returns></returns>
        [HttpGet, Secure]
        [Route("reports/getfilepath/{id}/{format}")]
        public async Task<string> GetFilePath(long id, ExportType format)
        {
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    var fileName = await ReportingLogic.GetFilePath(id, format);
                    return fileName;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return null;
        }

        /// <summary>
        /// The method allows scheduling a report with scheduling parameters
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        [HttpPost, Secure]
        [Route("reports/schedule")]
        public async Task<ResultSet<ReportStatusResult>> ScheduleReport(ScheduleReportArgs args)
        {
            ResultSet<ReportStatusResult> result = new ResultSet<ReportStatusResult>();
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    result.Data = await ReportingLogic.ScheduleReportAsync(args);
                    result.Success = true;
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.ExpectationFailed;
                result.Message = ex.Message;
                Logger.Error(ex);
            }
            return result;
        }

        /// <summary>
        /// The method returns the schedule information for the report.
        /// </summary>
        /// <param name="id">Scheduled Id</param>
        /// <returns></returns>
        [HttpGet, Secure]
        [Route("reports/schedule/{id}")]
        public async Task<ResultSet<ScheduleReportArgs>> GetScheduleReportInfo(long id)
        {
            ResultSet<ScheduleReportArgs> result = new ResultSet<ScheduleReportArgs>();
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    result.Data = await ReportingLogic.GetScheduledReportInfo(id);
                    result.Success = true;
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.ExpectationFailed;
                result.Message = ex.Message;
                Logger.Error(ex);
            }
            return result;
        }

        /// <summary>
        /// The API allows executing the scheduled reports by today
        /// </summary>
        /// <returns>List of ReportStatusResult, for all those reports which were picked and executed.</returns>
        [HttpPost]
        [Route("reports/schedule/run")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<ResultSet<List<ReportStatusResult>>> RunScheduledReports()
        {
            var result = new ResultSet<List<ReportStatusResult>>();
            try
            {
                result.Data = await ReportingLogic.RunScheduledReports();
                result.Success = true;
               
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.ExpectationFailed;
                result.Message = ex.Message;
                Logger.Error(ex);
            }
            return result;
        }

        /// <summary>
        /// The API fetchs the recent history of the authenticated user's 
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="queuedonly"></param>
        /// <returns></returns>
        [HttpGet, Secure]
        [Route("reports/request/history")]
        public async Task<ResultSet<List<ReportRequestHistoryResult>>> GetReportRequestHistory(int limit=10, bool queuedonly=false)        
        {
            var result = new ResultSet<List<ReportRequestHistoryResult>>();
            try
            {
                if (AuthenticatedUser != null && (Context?.Response?.StatusCode != 403))
                {
                    result.Data = await ReportingLogic.GetReportRequestHistoryAsync(limit, queuedonly);
                    result.Success = true;
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.ExpectationFailed;
                result.Message = ex.Message;
                Logger.Error(ex);
            }
            return result;
        }
    }
}