﻿using AT.Entities.Reporting.Interfaces;
using AT.Logic.Reporting;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Common.Core;
using AT.Entities.Reporting.Classes;
using AT.Entities.Reporting.ReportingConstants;
using AT.Provider.Reporting.Base;
using AT.Provider.Reporting.ReportBuilders;
using AbsenceSoft.Data.Reporting;

namespace AT.Provider.Reporting
{
    /// <summary>
    /// The Reporting Provider Class
    /// </summary>
    public class ReportingProvider : BaseReportingProvider
    {
        /// <summary>
        /// Gets all the available reports
        /// </summary>
        /// <returns></returns>
        public override async Task<List<Report>> GetAllReports(ReportBuilderType reportBuilderType)
        {
            var reportBuilder = ReportBuilder.GetReportBuilder(ReportBuilderType.AdHoc);
            return await reportBuilder.GetReports();
        }

        /// <summary>
        /// Gets the report by name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="reportBuilderType"></param>
        /// <returns></returns>
        public override async Task<Report> GetReportByName(string name, ReportBuilderType reportBuilderType)
        {
            var reportBuilder = ReportBuilder.GetReportBuilder(ReportBuilderType.AdHoc);
            return await reportBuilder.GetReportByName(name);
        }

        /// <summary>
        /// Gets the report columns
        /// </summary>
        /// <param name="reportTypeId"></param>
        /// <param name="reportBuilderType"></param>
        /// <returns></returns>
        public override async Task<List<ReportColumn>> GetReportColumns(string reportTypeId, ReportBuilderType reportBuilderType)
        {
            var reportBuilder = ReportBuilder.GetReportBuilder(ReportBuilderType.AdHoc);
            return await reportBuilder.GetReportColumns(reportTypeId);
        }

        
    }
}