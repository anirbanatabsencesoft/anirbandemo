using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading.Tasks;
using AT.Entities.Reporting.Interfaces;
using AT.Entities.Reporting.Classes;
using AT.Entities.Reporting.ReportingConstants;
using AbsenceSoft.Data.Reporting;

namespace AT.Provider.Reporting.Base
{
    /// <summary>
    /// The Base class of the Reporting provider
    /// </summary>
    public abstract class BaseReportingProvider : System.Configuration.Provider.ProviderBase
    {

        /// <summary>
        /// This is to set all config parameters
        /// </summary>
        /// <param name="collection"></param>
        public virtual void SetConfig(NameValueCollection collection)
        {
            
        }

        /// <summary>
        /// Get all the reports
        /// </summary>
        /// <returns></returns>
        public virtual Task<List<Report>> GetAllReports(ReportBuilderType reportBuilderType) => null;

        /// <summary>
        /// get report by name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="reportBuilderType"></param>
        /// <returns></returns>
        public virtual Task<Report> GetReportByName(string name, ReportBuilderType reportBuilderType) => null;

       
        public virtual Task<List<ReportColumn>> GetReportColumns(string reportTypeId, ReportBuilderType reportBuilderType) => null;
    }
}