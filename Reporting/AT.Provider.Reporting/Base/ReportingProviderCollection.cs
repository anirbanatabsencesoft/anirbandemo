using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using AT.Provider.Reporting.Base;

namespace AT.Provider.Reporting.Base
{
	public class ReportingProviderCollection : System.Configuration.Provider.ProviderCollection {
        
		/// <summary>
        /// Returns the notification provider based on name
        /// </summary>
		/// <param name="name"></param>
		new public BaseReportingProvider this[string name]
        {
            get { return (BaseReportingProvider)base[name]; }
        }
	}
}