using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Configuration;

namespace AT.Provider.Reporting.Base
{
	public class ReportingProviderConfiguration : System.Configuration.ConfigurationSection {

		[ConfigurationProperty("providers")]
		public System.Configuration.ProviderSettingsCollection Providers
        {
            get
            {
                return (ProviderSettingsCollection)base["providers"];
            }
        }

		[ConfigurationProperty("default", DefaultValue = "ReportingProvider")]
		public string DefaultProvider
        {
            get
            {
                return base["default"] as string;
            }
        }
	}
}