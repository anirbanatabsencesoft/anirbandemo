using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Configuration;
using System.Web.Configuration;

namespace AT.Provider.Reporting.Base
{
	/// <summary>
	/// This is the starting point for a notification provider as this is going to set
	/// all providers.
	/// </summary>
	public class ReportingProviderManager {
        private static string DATA_PROVIDER_SECTION = "ReportingImplementationProviders";
        private static BaseReportingProvider DefaultProvider;
		private static ReportingProviderCollection ProviderCollection;
		private static ProviderSettingsCollection ProviderSettingsColl;
        
		/// <summary>
		/// Static constructor for getting all providers in a queue
		/// </summary>
		static ReportingProviderManager(){

			Initialize();
		}

		/// <summary>
		/// Initialize the configuration section and set all static private variables.
		/// </summary>
		private static void Initialize()
        {
            //get the configuration
            ReportingProviderConfiguration configSection = (ReportingProviderConfiguration)ConfigurationManager.GetSection(DATA_PROVIDER_SECTION);

            //throw exception if configuration section is not present
            if (configSection == null)
                throw new ConfigurationErrorsException("Reporting provider section is not set in the config.");

            //get provider collections
            ProviderCollection = new ReportingProviderCollection();
            ProvidersHelper.InstantiateProviders(configSection.Providers, ProviderCollection, typeof(BaseReportingProvider));

            //get provider settings
            ProviderSettingsColl = configSection.Providers;

            //if there is no default provider, throw excpetion
            if (ProviderCollection[configSection.DefaultProvider] == null)
                throw new ConfigurationErrorsException("Default provider is not set.");

            //get the default provider
            DefaultProvider = ProviderCollection[configSection.DefaultProvider];
            var defaultSettings = ProviderSettingsColl[configSection.DefaultProvider];
            
            DefaultProvider.SetConfig(defaultSettings.Parameters);
            
        }

		/// <summary>
		/// Returns the Default Instance of the provider.
		/// </summary>
		public static BaseReportingProvider Default => DefaultProvider;

	    public static ReportingProviderCollection Providers => ProviderCollection;

	    public System.Configuration.ProviderSettingsCollection ProviderSettings => ProviderSettingsColl;

	    public static IList<BaseReportingProvider> All
        {
            get
            {
                List<BaseReportingProvider> lstProviders = new List<BaseReportingProvider>();
                foreach (var item in Providers)
                    lstProviders.Add(item as BaseReportingProvider);
                return lstProviders;
            }
        }
    }
}