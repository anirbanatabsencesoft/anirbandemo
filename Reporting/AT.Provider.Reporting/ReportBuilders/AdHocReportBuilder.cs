﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Reporting.Classes;
using AT.Entities.Reporting.Interfaces;
using AT.Logic.Reporting;
using AT.Provider.Reporting.ReportBuilders;
using AT.Entities.Authentication;

namespace AT.Provider.Reporting.ReportProviders
{
    /// <summary>
    /// AdHoc report builder class
    /// </summary>
    public class AdHocReportBuilder : ReportBuilder
    {
        /// <summary>
        /// Reporting Logic class
        /// </summary>
        private readonly ReportingLogic reportingLogic;
        /// <summary>
        /// Constructor
        /// </summary>
        public AdHocReportBuilder()
        {
            reportingLogic = new ReportingLogic();
        }
      
        /// <summary>
        /// Constructor
        /// </summary>
        public AdHocReportBuilder(User user)
        {
            reportingLogic = new ReportingLogic(user);
        }
        /// <summary>
        /// Gets all the AdHoc Reports
        /// </summary>
        /// <returns></returns>
        public override async Task<List<Report>> GetReports()
        {
            return await reportingLogic.GetAdhocReportsAsync();
        }
        /// <summary>
        /// Gets the report by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public override async Task<Report> GetReportByName(string name)
        {
            return await reportingLogic.GetReportDefinitionByNameAsync(name);
        }
        /// <summary>
        /// Get the report columns by report type id
        /// </summary>
        /// <param name="reportTypeId"></param>
        /// <returns></returns>
        public override async Task<List<ReportColumn>> GetReportColumns(string reportTypeId)
        {
            return await reportingLogic.GetAdHocReportColumnsAsync(reportTypeId);
        }
    }
}
