﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Common.Core;
using AT.Entities.Reporting.Interfaces;
using AT.Entities.Reporting.ReportingConstants;
using AT.Provider.Reporting.ReportProviders;
using AT.Entities.Reporting.Classes;
using AT.Entities.Authentication;

namespace AT.Provider.Reporting.ReportBuilders
{
    /// <summary>
    /// Report Builder Factory Class
    /// </summary>
    public abstract class ReportBuilder
    {
        /// <summary>
        /// Gets the list of the reports
        /// </summary>
        /// <returns></returns>
        public abstract Task<List<Report>> GetReports();
        /// <summary>
        /// Gets the report by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public abstract Task<Report> GetReportByName(string name);
        /// <summary>
        /// Get the report columns
        /// </summary>
        /// <param name="reportTypeId"></param>
        /// <returns></returns>
        public abstract Task<List<ReportColumn>> GetReportColumns(string reportTypeId);
        /// <summary>
        /// Factory Method to instantiate Report Builder class
        /// </summary>
        /// <param name="reportType"></param>
        /// <returns></returns>
        public static ReportBuilder GetReportBuilder(ReportBuilderType reportType, User user=null)
        {
            switch (reportType)
            {
                case ReportBuilderType.AdHoc:
                    return user != null ? new AdHocReportBuilder(user) : new AdHocReportBuilder();
                default:
                    throw new NotImplementedException("The appropriate report builder is not implemented");
            }
        }
    }
}
