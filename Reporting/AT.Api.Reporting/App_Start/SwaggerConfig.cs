using System.Web.Http;
using WebActivatorEx;
using Swashbuckle.Application;
using AT.Api.Core;
using AT.Api.Reporting;
using System;
using System.Linq;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register", Order =0)]
namespace AT.Api.Reporting
{
    /// <summary>
    /// Swagger class, currently not pre registered due to Notification API reference in Absencesoft library issue
    /// </summary>    
    public static class SwaggerConfig
    {
        /// <summary>
        /// Register the current assembly in to swagger
        /// </summary>
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;
            BaseSwaggerConfig.Register("Reporting", thisAssembly);            
        }           
    }
}
