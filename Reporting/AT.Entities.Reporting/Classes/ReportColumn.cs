﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Reporting.Interfaces;

namespace AT.Entities.Reporting.Classes
{
    /// <summary>
    /// The Report Column
    /// </summary>
    public class ReportColumn : IReportColumn
    {
        /// <summary>
        /// The name of the report column
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The value of the report column
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// The mapped field of report column
        /// </summary>
        public IField MappedField { get; set; }
    }
}
