﻿using AbsenceSoft.Data.Enums;
using AT.Entities.Reporting.Enums;
using AT.Entities.Reporting.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Classes
{

    /// <summary>
    /// AdHoc Report column filter
    /// </summary>
    public class AdHocReportFilter
    {
        /// <summary>
        /// Filter field name.
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// Filter operator type.
        /// </summary>
        public OperatorType OperatorType { get; set; }
        /// <summary>
        /// Filter control type.
        /// </summary>
        public ControlType ControlType { get; set; }
        /// <summary>
        /// Value of the filter.
        /// </summary>
        public string Value { get; set; }
    }
}
