﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Reporting.Interfaces;
using AT.Entities.Reporting.ReportingConstants;

namespace AT.Entities.Reporting.Classes
{
    /// <summary>
    /// The Pdf Report Formatter Base Class
    /// </summary>
    public class PdfReportFormatter : IReportFormatProvider
    {
        /// <summary>
        /// Format the report in pdf Format
        /// </summary>
        /// <param name="report"></param>
        /// <param name="reportFormat"></param>
        public void Format(IReport report, ReportFormats reportFormat)
        {
            throw new NotSupportedException();
        }
    }
}
