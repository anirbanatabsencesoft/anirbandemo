﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Classes
{
    public class ReportType
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReportType"/> class.
        /// </summary>
        public ReportType()
        {
            Fields = new List<ReportColumn>();
        }

        /// <summary>
        /// Gets or sets the type of the ad hoc report type.
        /// </summary>
        /// <value>
        /// The type of the ad hoc report.
        /// </value>
        public AdHocReportType AdHocReportType { get; set; }

        /// <summary>
        /// Gets or sets the report type key.
        /// </summary>
        /// <value>
        /// The report type key.
        /// </value>
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the fields.
        /// </summary>
        /// <value>
        /// The fields.
        /// </value>
        public List<ReportColumn> Fields { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is customer specific and has a customer_id.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is customer specific and has a customer_id; otherwise, <c>false</c>.
        /// </value>
        public bool IsCustomer { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is employer specific and has a employer_id.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is employer specific and has a employer_id; otherwise, <c>false</c>.
        /// </value>
        public bool IsEmployer { get; set; }
    }
}
