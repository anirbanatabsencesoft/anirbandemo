﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Interfaces;
using MongoDB.Bson;

namespace AT.Entities.Reporting.Classes
{
    /// <summary>
    /// The Report Base Class
    /// </summary>
    public class Report : IReport
    {
        /// <summary>
        /// The Id of the report
        /// </summary>
        public virtual string Id { get; set; }
        /// <summary>
        /// The Name of the report
        /// </summary>
        public virtual string Name { get; set; }
        
        /// <summary>
        /// Maintains CustomerId associated to the Report
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// Maintains UserId associated to the Report
        /// </summary>
        public string UserId{ get; set; }

        /// <summary>
        /// The Report Type
        /// </summary>
        public virtual string ReportType { get; set; }

        /// <summary>
        /// The Report Id
        /// </summary>
        public virtual string ReportId { get; set; }
        /// <summary>
        /// The Report Category
        /// </summary>
        public virtual string ReportCategory { get; set; }
        
        /// <summary>
        /// The query of the report
        /// </summary>
        public virtual IReportQuery Query { get; set; }
        /// <summary>
        /// The list of the sub-report
        /// </summary>
        public virtual List<IReport> SubReports { get; set; }
        /// <summary>
        /// The list of the report columns
        /// </summary>
        public virtual IList<IReportColumn> ReportColumns { get; set; }
        /// <summary>
        /// Generate the report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportQuery"></param>
        /// <returns></returns>
        public virtual IReportResult Generate(Authentication.User user, IReportQuery reportQuery)
        {
            return null;
        }
    }
}
