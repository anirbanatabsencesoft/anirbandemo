﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Reporting.Interfaces;

namespace AT.Entities.Reporting.Classes
{
    /// <summary>
    /// The Report Query Base Class
    /// </summary>
    public class ReportQuery : IReportQuery
    {
        /// <summary>
        /// The projections of the query
        /// </summary>
        public List<IReportColumn> Projections { get; set; }
        /// <summary>
        /// The criteria group of the query
        /// </summary>
        public ICriteriaGroup CriteriaGroup { get; set; }
    }
}
