﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Classes
{
    public class DeliveryParameters
    {
        /// <summary>
        /// Report Name which is requested
        /// </summary>
        public virtual string ReportName { get; set; }

        /// <summary>
        /// The report requested date
        /// </summary>
        public virtual string RequestedDate { get; set; }

        /// <summary>
        /// Site url, which user can check the report at
        /// </summary>
        public virtual string SiteUrl { get; set; }
    }
}
