﻿using AbsenceSoft.Data.Enums;
using AT.Entities.Reporting.ReportingConstants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Classes
{
    /// <summary>
    /// The definition of the report.
    /// </summary>
    public class ReportColumnDefinition
    {
        /// <summary>
        /// Id of the report.
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Name of the report.
        /// </summary>
        public string Name { get; set; }      
        /// <summary>
        /// Type of Adhoc report.
        /// </summary>
        public AdHocReportType? AdHocReportType { get; set; }
        /// <summary>
        /// Whether the report is active or not.
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// List of Report fields.
        /// </summary>
        public IList<AdHocReportField> Fields { get; set; }

        /// <summary>
        /// List of report filters.
        /// </summary>
        public IList<AdHocReportFilter> Filters { get; set; }
    }
}
