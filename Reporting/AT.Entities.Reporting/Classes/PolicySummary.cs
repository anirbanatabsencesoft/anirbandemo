﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Classes
{
   
    /// <summary>
    /// Policy summary class
    /// </summary>
    [Serializable]
    public class PolicySummary
    {
        /// <summary>
        /// Gets or sets the policy code.
        /// </summary>
        public string PolicyCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the policy.
        /// </summary>
        public string PolicyName { get; set; }

        /// <summary>
        /// Gets or sets the time used.
        /// </summary>
        public double TimeUsed { get; set; }

        /// <summary>
        /// Gets or sets the time remaining.
        /// </summary>
        public double TimeRemaining { get; set; }


        /// <summary>
        /// he friendly time display for total hours used from the raw minutes.
        /// </summary>
        public string HoursUsed { get; set; }

        /// <summary>
        /// The friendly time display for total hours remaining from the raw minutes.
        /// </summary>
        public string HoursRemaining { get; set; }

        /// <summary>
        ///  Gets or sets the units.
        /// </summary>
        public Unit Units { get; set; }

        /// <summary>
        /// Absence Reason 
        /// </summary>
        public string AbsenceReason { get; set; }

        /// <summary>
        /// Gets or sets the absence reason identifier.
        /// </summary>
        public string AbsenceReasonId { get; set; }

        /// <summary>
        /// Gets or sets the type of the policy.
        /// </summary>
        public PolicyType PolicyType { get; set; }

        /// <summary>
        /// Gets or sets the whether the policy should exclude from time conversion.
        /// </summary>
        public bool ExcludeFromTimeConversion { get; set; }

    }
}
