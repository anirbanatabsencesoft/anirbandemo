﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Reporting.Interfaces;
using AT.Entities.Reporting.ReportingConstants;

namespace AT.Entities.Reporting.Classes
{
    /// <summary>
    /// The Group Field Base Class
    /// </summary>
    public class GroupField : IGroup
    {
        /// <summary>
        /// The field for Aggregate
        /// </summary>
        public IField Field { get; set; }
        /// <summary>
        /// The aggregate function
        /// </summary>
        public Group Aggregate { get; set; }
    }
}
