﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Reporting.Interfaces;
using AT.Entities.Reporting.ReportingConstants;

namespace AT.Entities.Reporting.Classes
{
    /// <summary>
    /// The Word Report Formatter
    /// </summary>
    public class CsvReportFormatter : IReportFormatProvider
    {
        /// <summary>
        /// Format the report in Csv Format
        /// </summary>
        /// <param name="report"></param>
        /// <param name="reportFormat"></param>
        public void Format(IReport report, ReportFormats reportFormat)
        {
            throw new NotSupportedException();
        }
    }
}
