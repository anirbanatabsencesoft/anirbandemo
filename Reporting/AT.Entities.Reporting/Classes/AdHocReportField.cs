﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Reporting;
using AT.Entities.Reporting.Interfaces;

namespace AT.Entities.Reporting.Classes
{
    /// <summary>
    /// The Field of the AdHoc Report
    /// </summary>
    public class AdHocReportField
    {
        public AdHocReportField()
        { }

        public AdHocReportField(ReportField field)
        {
            this.DisplayName = field.Display;
            this.Name = field.Name;
        }

        public AdHocReportField(SavedReportField field)
        {
            this.Name = field.Name;
            this.Order = field.Order;
        }

        /// <summary>
        /// The Name of the field
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The value of the field
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Display name of the field.
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// Sequence number of the field.
        /// </summary>
        public int? Order { get; set; }
        /// <summary>
        /// Whether to include this field in report.
        /// </summary>
        public bool IncludedInReport { get; set; }
    }
}
