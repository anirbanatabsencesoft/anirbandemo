﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Reporting.Interfaces;
using AT.Entities.Reporting.ReportingConstants;

namespace AT.Entities.Reporting.Classes
{
    /// <summary>
    /// The Custom Criteria
    /// </summary>
    public class CustomCriteria : ICriteria
    {
        /// <summary>
        /// List of the fields
        /// </summary>
        public List<IField> Fields { get; set; }
        /// <summary>
        /// List of group fields
        /// </summary>
        public List<IGroup> GroupFields { get; set; }
        /// <summary>
        /// The fields operator
        /// </summary>
        public CriteriaOperator Operator { get; set; }
        /// <summary>
        /// Function to And the criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public ICriteria And(ICriteria criteria)
        {
            return new CustomCriteria();
        }
        /// <summary>
        /// Function to Or the criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public ICriteria Or(ICriteria criteria)
        {
            return new CustomCriteria();
        }
    }
}
