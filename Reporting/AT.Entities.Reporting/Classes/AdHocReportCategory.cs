﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Classes
{
    public class AdHocReportCategory
    {
        public string Name { get; set; }

        public List<AdHocReportField> Fields { get; set; }
    }
}
