﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Interfaces;

namespace AT.Entities.Reporting.Classes
{
    /// <summary>
    /// The AdHocReport Base Class
    /// </summary>
    public class AdHocReport : Report
    {
        /// <summary>
        /// Constructor for AdHocReport
        /// </summary>
        public AdHocReport()
        {
            
        }
        /// <summary>
        /// The query for the report
        /// </summary>
        public override IReportQuery Query { get; set; }

        /// <summary>
        /// List of sub-reports
        /// </summary>
        public override List<IReport> SubReports { get; set; }

        /// <summary>
        /// Generate the report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportQuery"></param>
        /// <returns></returns>
        public override IReportResult Generate(User user, IReportQuery reportQuery)
        {
            return new ReportResult();
        }
    }
}
