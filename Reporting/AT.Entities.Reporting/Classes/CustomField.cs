﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Reporting.Interfaces;

namespace AT.Entities.Reporting.Classes
{
    /// <summary>
    /// The Custom Field Class
    /// </summary>
    public class CustomField : IField
    {
        /// <summary>
        /// Name of the field
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Value of the field
        /// </summary>
        public string Value { get; set; }
    }
}
