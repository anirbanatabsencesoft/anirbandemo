﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Classes
{
    /// <summary>
    /// Group Result Class
    /// </summary>
    public class GroupResult
    {
        /// <summary>
        /// Key of the Group by
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Value of the group by
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// Total count of the group items
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Items of the each group by
        /// </summary>
        public IEnumerable Items { get; set; }

        /// <summary>
        /// Sub Groups if any
        /// </summary>
        public List<GroupResult> SubGroups { get; set; }

        /// <summary>
        /// Friendly Name 
        /// </summary>
        /// <returns></returns>
        public override string ToString() { return string.Format("{0} ({1})", Key, Count); }
    }
}
