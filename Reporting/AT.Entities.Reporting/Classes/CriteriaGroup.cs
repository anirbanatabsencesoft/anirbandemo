﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Reporting.Interfaces;

namespace AT.Entities.Reporting.Classes
{
    /// <summary>
    /// The Criteria Group
    /// </summary>
    public class CriteriaGroup : ICriteriaGroup
    {
        /// <summary>
        /// List of the criteria
        /// </summary>
        public List<ICriteria> Criteria { get; set; }
        /// <summary>
        /// List of the group fields
        /// </summary>
        public List<IGroup> GroupFields { get; set; }
    }
}
