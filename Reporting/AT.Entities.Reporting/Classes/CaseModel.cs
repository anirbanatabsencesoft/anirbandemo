﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Classes
{
    /// <summary>
    /// Case model class
    /// </summary>
   public class CaseModel
    {
        /// <summary>
        /// Case Number
        /// </summary>
        public string CaseNumber { get; set; }
        /// <summary>
        /// Employee number
        /// </summary>
        public string EmployeeNumber { get; set; }
        /// <summary>
        /// Employee Id
        /// </summary>
        public string EmployeeId { get; set; }
        /// <summary>
        /// Office Location
        /// </summary>
        public string OfficeLocation { get; set; }
        /// <summary>
        ///Employee Name
        /// </summary>
        public string Name { get; set; }

    }
}
