﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Reporting.Interfaces;
using AT.Entities.Reporting.ReportingConstants;

namespace AT.Entities.Reporting.Classes
{
    /// <summary>
    /// The Standard Criteria Class
    /// </summary>
    public class StandardCriteria : ICriteria
    {
        /// <summary>
        /// The Fields data
        /// </summary>
        public List<IField> Fields { get; set; }
        /// <summary>
        /// The Operator for Criteria
        /// </summary>
        public CriteriaOperator Operator { get; set; }
        /// <summary>
        /// The Group Fields data
        /// </summary>
        public List<IGroup> GroupFields { get; set; }
        /// <summary>
        /// The And Criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public ICriteria And(ICriteria criteria)
        {
            return new StandardCriteria()
            {
                Fields = criteria.Fields,
                GroupFields = GroupFields,
                Operator = CriteriaOperator.And
            };
        }
        /// <summary>
        /// The Or Criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public ICriteria Or(ICriteria criteria)
        {
            return new StandardCriteria()
            {
                Fields = criteria.Fields,
                GroupFields = GroupFields,
                Operator = CriteriaOperator.Or
            };
        }
    }
}
