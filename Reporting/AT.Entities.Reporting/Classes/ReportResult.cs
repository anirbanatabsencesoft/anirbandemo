﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Cases;
using AT.Entities.Reporting.Interfaces;

namespace AT.Entities.Reporting.Classes
{
    /// <summary>
    /// The Report Result Base Class
    /// </summary>
    public class ReportResult : IReportResult
    {
        /// <summary>
        /// The results of the sub-reports
        /// </summary>
        public IList<IReportResult> SubReportResults { get; set; }
    }
}
