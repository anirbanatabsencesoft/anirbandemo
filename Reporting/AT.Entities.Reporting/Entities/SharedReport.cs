﻿using AT.Common.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Entities
{
    public class SharedReport : BaseEntity
    {
        /// <summary>
        /// The PK
        /// </summary>
        public long? Id { get; set; }

        /// <summary>
        /// The Report name
        /// </summary>
        public string ReportName { get; set; }

        /// <summary>
        /// The order how the UI will render
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// The category of the report
        /// </summary>
        public string ReportCategory { get; set; }

        /// <summary>
        /// The fully qualified name of the report class
        /// </summary>
        public string ReportObjectFullyQualifiedName { get; set; }

        /// <summary>
        /// The JSON string of the report criteria argument
        /// </summary>
        public string CriteriaJson { get; set; }

        /// <summary>
        /// The customer id for postgres
        /// </summary>
        public long? CustomerId { get; set; }

        /// <summary>
        /// The Mongo object id
        /// </summary>
        public string CustomerKey { get; set; }

        /// <summary>
        /// Shared with. Options are TEAM/USER/ROLE
        /// </summary>
        public string SharedWith { get; set; }

        /// <summary>
        /// Scheduled reports
        /// </summary>
        public long? ScheduledReportId { get; set; }

        /// <summary>
        /// Postgres id for user
        /// </summary>
        public long? CreatedById { get; set; }

        /// <summary>
        /// The mongo id for user
        /// </summary>
        public string CreatedByKey { get; set; }

        /// <summary>
        /// Created on
        /// </summary>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Last modified date time
        /// </summary>
        public DateTime ModifiedOn { get; set; }

        /// <summary>
        /// Is active?
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Set if deleted
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Set the last report request id
        /// </summary>
        public long? LastRequestId { get; set; }

        /// <summary>
        /// Set the list of report recipients
        /// </summary>
        public List<SharedReportRecipient> RecipientList { get; set; }

        /// <summary>
        /// Returns the last ran JSON data as string format
        /// Need to parse the string as JSON in JS
        /// </summary>
        public string LastRunData { get; }
    }
}
