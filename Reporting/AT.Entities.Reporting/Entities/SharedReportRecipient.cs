﻿using AT.Common.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Entities
{
    /// <summary>
    /// List of recipients for the shared report
    /// </summary>
    public class SharedReportRecipient : BaseEntity
    {
        /// <summary>
        /// The PK
        /// </summary>
        public long? Id { get; set; }

        /// <summary>
        /// The shared report id
        /// </summary>
        public long SharedReportId { get; set; }

        /// <summary>
        /// Set the team name if shared with team
        /// </summary>
        public string TeamName { get; set; }

        /// <summary>
        /// Set the Postgres Id of team if available
        /// </summary>
        public long? TeamId { get; set; }

        /// <summary>
        /// Set the team mongo id if available
        /// </summary>
        public string TeamKey { get; set; }

        /// <summary>
        /// Set the Postgres Id for role if available
        /// </summary>
        public long? RoleId { get; set; }

        /// <summary>
        /// Set the role name if shared with a role
        /// </summary>
        public string RoleName { get; set; }

        /// <summary>
        /// Set the user postgres id if available
        /// </summary>
        public long? UserId { get; set; }

        /// <summary>
        /// Set the mongo id of user if available
        /// </summary>
        public string UserKey { get; set; }
    }
}
