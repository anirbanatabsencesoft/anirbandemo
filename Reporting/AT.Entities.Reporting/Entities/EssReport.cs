﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Reporting;

namespace AT.Entities.Reporting.Entities
{
    public class EssReport
    {
        public Dictionary<string, List<string>> AvailableGroupingFields { get; set; }
        public string Category { get; set; }
        public int CategoryOrder { get; set; }
        public string ESSCategoryTitle { get; set; }
        public List<EssReportDetail> ESSReportDetails { get; set; }
        public bool GroupAutoTotals { get; set; }
        public bool HasLinkedReport { get; set; }
        public bool HasSubReport { get; set; }
        public string IconImage { get; set; }
        public string Id { get; set; }
        public bool IncludeOrganizationGrouping { get; set; }
        public bool IsESSReport { get; set; }
        public bool IsExportableToCsv { get; set; }
        public bool IsExportableToPdf { get; set; }
        public bool IsGrouped { get; set; }
        public bool IsViewable { get; set; }
        public LinkedReport LinkedReport { get; set; }
        public string MainCategory { get; set; }
        public string Name { get; set; }
    }
}
