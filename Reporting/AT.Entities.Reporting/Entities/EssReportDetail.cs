﻿using AbsenceSoft.Data.Enums;
using AT.Entities.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Entities
{
    public class EssReportDetail
    {
        public string ReportId { get; set; }
        public string Title { get; set; }
        public ESSReportType Type { get; set; }
        public string ESSReportName { get; set; }
        public ReportGroupType? ReportGroupType { get; set; }
    }
}
