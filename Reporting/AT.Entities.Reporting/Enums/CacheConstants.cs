﻿namespace AT.Entities.Reporting.ReportingConstants
{
    /// <summary>
    /// Cache Constants
    /// </summary>
    public static class CacheConstants
    {
        public const string REPORTING_ADHOC_ALL_REPORTS = "REPORTING_ADHOC_ALL_REPORTS";
        public const string REPORTING_ADHOC_REPORTS_BY_NAME = "REPORTING_ADHOC_REPORTS_BY_NAME";
        public const string REPORTING_ALL_REPORT_DEFINITIONS = "REPORTING_ALL_REPORT_DEFINITIONS";
        public const string REPORTING_ALL_REPORT_TYPE_FIELDS = "REPORTING_ALL_REPORT_TYPE_FIELDS";
        public const string REPORTING_ALL_REPORT_FIELDS = "REPORTING_ALL_REPORT_FIELDS";
        public const string REPORTING_ALL_REPORT_TYPE = "REPORTING_ALL_REPORT_TYPE";
        public const string REPORTING_ALL_BASE_REPORTS = "REPORTING_ALL_BASE_REPORTS";
    }
}
