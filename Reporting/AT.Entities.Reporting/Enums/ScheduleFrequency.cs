﻿namespace AT.Entities.Reporting.ReportingConstants
{
    /// <summary>
    /// Schedule Frequency
    /// </summary>
    public enum ScheduleFrequency
    {
        Daily,
        Weekly,
        Monthly,
        Yearly,
        Custom
    }
}