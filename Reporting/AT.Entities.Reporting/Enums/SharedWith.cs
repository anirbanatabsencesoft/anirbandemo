﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Enums
{
    /// <summary>
    /// Constants for sharing
    /// </summary>
    public class SharedWithConstants
    {
        /// <summary>
        /// Protected constructor
        /// </summary>
        protected SharedWithConstants() { }

        public const string USER = "USER";
        public const string TEAM = "TEAM";
        public const string ROLE = "ROLE";
        public const string SELF = "SELF";
    }
}
