﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Enums
{
    /// <summary>
    /// Report Group Type enum
    /// </summary>
   public enum ReportGroupType
    {   
        Custom=0,
        WorkState=1,
        Reason=2,
        Policy=3,
        Organization=4,
        Location=5,
        Leave=6,
        Duration=7,
        AccommodationType=8,
        CaseManager=9,
        Condition=10,
        ConditionByCondition=11,
        ConditionByOrganization=12,
        DurationByCondition=13,
        DurationByOrganization=14,
        CaseReport=15,
        CaseReportByDay=16,
        CaseReportByOrganization=17,
        NeedleSticks = 18,
        InjuryDetails = 19,       
        BiReportTotalLeavesByReason = 20,
        BiReportTotalLeavesByLocation = 21,
        None=22
    }
}