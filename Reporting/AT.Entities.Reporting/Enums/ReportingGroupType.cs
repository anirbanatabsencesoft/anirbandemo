﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Enums
{
    public enum ReportGroupType
    {
        None = 0,
        WorkState = 1,
        Reason = 2,
        Policy = 3,
        Organization = 4,
        Location = 5,
        Leave = 6,
        Duration = 7,
        AccommodationType = 8,
        CaseManager = 9,
        Condition = 10,
        DurationByCondition = 11,
        DurationByOrganization = 12,
        ConditionByCondition = 13,
        ConditionByOrganization = 14
    }
}
