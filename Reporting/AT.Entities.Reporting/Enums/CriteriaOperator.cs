﻿namespace AT.Entities.Reporting.ReportingConstants
{
    /// <summary>
    /// Criteria Operator
    /// </summary>
    public enum CriteriaOperator
    {
        And,
        Or
    }
}