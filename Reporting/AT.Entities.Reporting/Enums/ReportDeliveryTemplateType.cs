﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Enums
{
    public enum ReportDeliveryTemplateType
    {
        ReportGenerated,
        ScheduledDeliverySubject
    }
}
