﻿namespace AT.Entities.Reporting.ReportingConstants
{
    /// <summary>
    /// Group enum
    /// </summary>
    public enum Group
    {
        Sum,
        Max,
        Min,
        Avg
    }
}