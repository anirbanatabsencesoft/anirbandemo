﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Enums
{
    /// <summary>
    /// Duration criteria type
    /// </summary>
    public enum DurationCriteria
    {
        Monthly,
        DateRange,
        Quarterly,
        Yearly,
        Rolling
    }
}
