﻿namespace AT.Entities.Reporting.ReportingConstants
{
    /// <summary>
    /// Report Builders
    /// </summary>
    public static class ReportBuilders
    {
        public const string NAME = "ReportBuilder";
        public const string ADHOC = "AdHoc";
        public const string OSHA = "OSHA";
        public const string OPERATIONS = "Operations";
    }
}
