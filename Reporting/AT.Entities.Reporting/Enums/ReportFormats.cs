﻿namespace AT.Entities.Reporting.ReportingConstants
{
    /// <summary>
    /// Report Formats
    /// </summary>
    public enum ReportFormats
    {
        Pdf,
        Csv,
        Excel
    }
}