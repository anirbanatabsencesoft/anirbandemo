﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Enums
{
    /// <summary>
    /// Type of the operator for filters
    /// </summary>
    public enum OperatorType
    {
        Equals = 1,
        DoesNotEqual = 2,
        BeginsWith = 3,
        EndsWith = 4,
        Contains = 5
    }
}
