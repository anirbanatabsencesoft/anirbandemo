﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Interfaces
{
    /// <summary>
    /// The Report Result Interface
    /// </summary>
    public interface IReportResult
    {
        /// <summary>
        /// The Sub-report results
        /// </summary>
        IList<IReportResult> SubReportResults { get; set; }
    }
}
