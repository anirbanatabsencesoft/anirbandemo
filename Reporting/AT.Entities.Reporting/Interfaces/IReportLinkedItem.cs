﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Interfaces
{
    /// <summary>
    /// The Report Linked Item
    /// </summary>
    interface IReportLinkedItem : IReportItem
    {
        /// <summary>
        /// The Url of the Linked Item
        /// </summary>
        string Url { get; set; }
    }
}
