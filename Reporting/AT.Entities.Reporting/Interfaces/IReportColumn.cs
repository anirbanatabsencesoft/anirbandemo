﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Interfaces
{
    /// <summary>
    /// The Report Column Interface
    /// </summary>
    public interface IReportColumn
    {
        /// <summary>
        /// The Name Field
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// The Value Field
        /// </summary>
        string Value { get; set; }
    }
}
