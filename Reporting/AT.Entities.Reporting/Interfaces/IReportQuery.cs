﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Interfaces
{
    /// <summary>
    /// The Report Query Interface
    /// </summary>
    public interface IReportQuery
    {
        /// <summary>
        /// The List of the Projection Fields
        /// </summary>
        List<IReportColumn> Projections { get; set; }
        /// <summary>
        /// The Criteria Group
        /// </summary>
        ICriteriaGroup CriteriaGroup { get; set; }
    }
}
