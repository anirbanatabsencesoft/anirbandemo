﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Interfaces
{
    public interface IField
    {
        string Name { get; set; }
        string Value { get; set; }
    }
}
