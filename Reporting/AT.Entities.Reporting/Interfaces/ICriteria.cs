﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Reporting.ReportingConstants;

namespace AT.Entities.Reporting.Interfaces
{
    public interface ICriteria
    {
        /// <summary>
        /// The list of the fields
        /// </summary>
        List<IField> Fields { get; set; }
        /// <summary>
        /// The fields operator
        /// </summary>
        CriteriaOperator Operator { get; set; }
        /// <summary>
        /// The Or function
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        ICriteria And(ICriteria criteria);
        /// <summary>
        /// The And function
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        ICriteria Or(ICriteria criteria);
    }
}
