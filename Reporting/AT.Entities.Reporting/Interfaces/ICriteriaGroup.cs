﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Interfaces
{
    public interface ICriteriaGroup
    {
        /// <summary>
        /// The List of Criteria that Criteria Group Contains
        /// </summary>
        List<ICriteria> Criteria { get; set; }
        /// <summary>
        /// The List of Group that Group Fields Contains
        /// </summary>
        List<IGroup> GroupFields { get; set; }
    }
}
