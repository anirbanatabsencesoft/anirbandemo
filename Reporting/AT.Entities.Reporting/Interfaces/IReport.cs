﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Classes;

namespace AT.Entities.Reporting.Interfaces
{
    /// <summary>
    /// The Report Interface
    /// </summary>
    public interface IReport
    {
        /// <summary>
        /// The Id of the report
        /// </summary>
        string Id { get; set; }
        /// <summary>
        /// The Name of the report
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Maintains CustomerId associated to the Report
        /// </summary>
        string CustomerId { get; set; }

        /// <summary>
        /// Maintains UserId associated to the Report
        /// </summary>
        string UserId { get; set; }
                
        /// <summary>
        /// The Query of the report
        /// </summary>
        IReportQuery Query { get; set; }
        /// <summary>
        /// The list of the sub reports
        /// </summary>
        List<IReport> SubReports { get; set; }
        /// <summary>
        /// The Generate method of the report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportQuery"></param>
        /// <returns></returns>
        IReportResult Generate(User user, IReportQuery reportQuery);
    }


}
