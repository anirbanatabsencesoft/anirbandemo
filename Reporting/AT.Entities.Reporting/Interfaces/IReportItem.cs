﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Interfaces
{
    /// <summary>
    /// Report Item
    /// </summary>
    interface IReportItem
    {
        /// <summary>
        /// Name of the report item
        /// </summary>
        string Name { get; set; }
    }
}
