﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Reporting.ReportingConstants;

namespace AT.Entities.Reporting.Interfaces
{
    interface IReportFormatProvider
    {
        /// <summary>
        /// Format the report in specified format
        /// </summary>
        /// <param name="report"></param>
        /// <param name="reportFormat"></param>
        void Format(IReport report, ReportFormats reportFormat);
    }
}
