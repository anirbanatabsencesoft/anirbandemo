﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Interfaces
{
    /// <summary>
    /// Report Line Item
    /// </summary>
    interface IReportLineItem : IReportItem
    {
        /// <summary>
        /// The Display Order of Line Item
        /// </summary>
        int DisplayOrder { get; set; }
    }
}
