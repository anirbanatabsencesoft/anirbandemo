﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Reporting.ReportingConstants;

namespace AT.Entities.Reporting.Interfaces
{
    /// <summary>
    /// The Interface for Special Report
    /// </summary>
    interface ISchedulableReport
    {
        /// <summary>
        /// The Frequency of the Schedule
        /// </summary>
        ScheduleFrequency Frequency { get; set; }
    }
}
