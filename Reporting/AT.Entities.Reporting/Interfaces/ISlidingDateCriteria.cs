﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Interfaces
{
    /// <summary>
    /// The Sliding Date Criteria
    /// </summary>
    interface ISlidingDateCriteria
    {
        /// <summary>
        /// The Sliding Date Criteria
        /// </summary>
        SlidingDateCriteria SlidingDateCriteria { get; set; }
    }

    public enum SlidingDateCriteria
    {
        ThisWeek,
        LastWeek,
        LastFiveDays
    }
}
