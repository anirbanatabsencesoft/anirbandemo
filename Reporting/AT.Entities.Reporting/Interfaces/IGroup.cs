﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Reporting.ReportingConstants;

namespace AT.Entities.Reporting.Interfaces
{
    public interface IGroup
    {
        IField Field { get; set; }
        Group Aggregate { get; set; }
    }


}
