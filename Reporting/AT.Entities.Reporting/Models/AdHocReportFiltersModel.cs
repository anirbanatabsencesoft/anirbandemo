﻿using AT.Entities.Reporting.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Models
{
    public class AdHocReportFiltersModel
    {
        public AdHocReportFiltersModel()
        {
            Categories = new List<AdHocReportCategory>();
            Filters = new List<AdhocFilterModel>();
        }
        public List<AdHocReportCategory> Categories { get; set; }
        public List<AdhocFilterModel> Filters { get; set; }
    }
}
