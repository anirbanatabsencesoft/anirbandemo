﻿using Insight.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Entities.Reporting.Models
{
    /// <summary>
    /// The model that is defined for shared report api
    /// </summary>
    public class SharedReportModel
    {
        /// <summary>
        /// The Shared Report Id
        /// </summary>
        [Column("id")]
        public long Id { get; set; }

        /// <summary>
        /// The Shared report name to be displayed in UI
        /// </summary>
        [Column("report_name")]
        public string ReportName { get; set; }

        /// <summary>
        /// The sequence how UI will order
        /// </summary>
        [Column("ui_sequence")]
        public int Sequence { get; set; }

        /// <summary>
        /// The report category
        /// </summary>
        [Column("report_category")]
        public string ReportCategory { get; set; }

        /// <summary>
        /// Set the report criteria
        /// </summary>
        [Column("report_class_fqn")]
        public string ReportObjectFQN { get; set; }

        /// <summary>
        /// Shared with. Options: TEAM/USER/ROLE
        /// </summary>
        [Column("report_json")]
        public string ReportJSON { get; set; }

        /// <summary>
        /// The customer id
        /// </summary>
        [Column("customer_id")]
        public long? CustomerId { get; set; }

        /// <summary>
        /// The customer Key
        /// </summary>
        [Column("customer_key")]
        public string CustomerKey { get; set; }

        /// <summary>
        /// Shared with options
        /// </summary>
        [Column("shared_with")]
        public string SharedWith { get; set; }

        /// <summary>
        /// The scheduled report
        /// </summary>
        [Column("scheduled_report_id")]
        public long? ScheduledReportId { get; set; }

        /// <summary>
        /// Created by Id
        /// </summary>
        [Column("created_by_id")]
        public long? CreatedById { get; set; }

        /// <summary>
        /// Created by Key
        /// </summary>
        [Column("created_by_key")]
        public string CreatedByKey { get; set; }

        /// <summary>
        /// Datetime in UTC
        /// </summary>
        [Column("created_on")]
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Datetime in UTC
        /// </summary>
        [Column("modified_on")]
        public DateTime ModifiedOn { get; set; }

        /// <summary>
        /// The recipient id
        /// </summary>
        [Column("recipient_id")]
        public long RecipientId { get; set; }

        /// <summary>
        /// The name of the tem
        /// </summary>
        [Column("team_name")]
        public string TeamName { get; set; }

        /// <summary>
        /// The team id
        /// </summary>
        [Column("team_id")]
        public long? TeamId { get; set; }

        /// <summary>
        /// The team mongo id
        /// </summary>
        [Column("team_key")]
        public string TeamKey { get; set; }

        /// <summary>
        /// The role name
        /// </summary>
        [Column("role_name")]
        public string RoleName { get; set; }

        /// <summary>
        /// The role id
        /// </summary>
        [Column("role_id")]
        public long? RoleId { get; set; }

        /// <summary>
        /// The user key
        /// </summary>
        [Column("user_key")]
        public string UserKey { get; set; }

        /// <summary>
        /// The user id
        /// </summary>
        [Column("user_id")]
        public long? UserId { get; set; }

        /// <summary>
        /// Get the last ran request id
        /// </summary>
        [Column("request_id")]
        public long? RequestId { get; set; }

        /// <summary>
        /// The JSON path of last ran object
        /// </summary>
        [Column("json_path")]
        public string JsonPath { get; set; }
    }
}
