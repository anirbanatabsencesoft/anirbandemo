﻿using AbsenceSoft;
using AbsenceSoft.Common.Postgres;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Reporting;
using AbsenceSoft.Data.ToDo;
using AT.Common.Core;
using AT.Data.Reporting.Model;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Classes;
using AT.Entities.Reporting.Enums;
using AT.Logic.Arguments;
using AT.Logic.Cache;
using AT.Logic.Reporting.Arguments;
using AT.Logic.Reporting.Reports;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AT.Common.Log;

namespace AT.Logic.Reporting
{
    /// <summary>
    /// The Reporting Logic class to provide the data access point of contact
    /// </summary>
    public partial class ReportingLogic
    {
        /// <summary>
        /// Get the data for the user required for report operations
        /// </summary>
        /// <returns></returns>
        public async Task<UserReportCriteriaData> GetUserReportCriteriaDataAsync()
        {
            //get the data from cache
            string key = string.Format("{0}_{1}", AuthenticatedUser.Key, "USER_REPORT_DATA");
            UserReportCriteriaData data = CacheHelper.Get<UserReportCriteriaData>(key);

            if (data == null)
            {
                data = await DataProvider.GetUserReportCriteriaData(new UserReportCriteriaData() { AuthenticatedUser = AuthenticatedUser });
                CacheHelper.Set(key, data, 3600);
            }

            return data;
        }

        /// <summary>
        /// Returns the work state for a user
        /// </summary>
        /// <param name="data"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public async Task<List<string>> GetWorkStatesAsync(UserReportCriteriaData data, DateTime startDate, DateTime endDate)
        {
            //get the data from cache
            string key = $"{data.AuthenticatedUser.UserKey}_USER_WORK_STATE";
            var result = CacheHelper.Get<List<string>>(key);

            if (result == null)
            {
                result = await DataProvider.GetUserWorkState(data, startDate, endDate);
                CacheHelper.Set(key, result);
            }

            return result;
        }

        /// <summary>
        /// Returns the custom fields for a user
        /// </summary>
        /// <param name="data"></param>
        /// <param name="fieldType"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public async Task<List<AbsenceSoft.Data.Customers.CustomField>> GetUserCustomFieldsAsync(UserReportCriteriaData data, CustomFieldType? fieldType = null, EntityTarget? target = null)
        {
            //get the data from cache
            string key = string.Format("{0}_{1}", AuthenticatedUser.UserKey, "USER_CUSTOM_FIELDS");
            var result = CacheHelper.Get<List<AbsenceSoft.Data.Customers.CustomField>>(key);

            if (result == null)
            {
                result = await DataProvider.GetUserCustomField(data, fieldType, target);
                CacheHelper.Set(key, result);
            }

            return result;
        }

        /// <summary>
        /// Gets the return to work date.
        /// </summary>
        public DateTime? GetCaseEventDate(Case c, CaseEventType eventType)
        {
            CaseEvent ev = c.FindCaseEvent(eventType);
            if (ev != null)
            {
                return ev.EventDate;
            }
            return null;
        }

        /// <summary>
        /// Returns all base reports without filters
        /// </summary>
        /// <returns></returns>
        public List<BaseReport> GetBaseReports()
        {
            var key = "ALL_BASE_REPORTS";
            var filter = new Func<BaseReport, bool>(r => r.RequiredFeatures().All(f => (SecurityUser.Customer ?? new Customer()).HasFeature(f)) && r.IsVisible(AuthenticatedUser));

            List<BaseReport> reports = CacheHelper.Get<List<BaseReport>>(key);

            if (reports == null || reports.Count == 0)
            {
                var reportClasses = Assembly.GetAssembly(this.GetType()).GetTypes()
                    .Where(r => !r.IsAbstract && !string.IsNullOrWhiteSpace(r.Namespace) && r.Namespace.StartsWith("AT.Logic.Reporting.Reports") && r.IsSubclassOf(typeof(BaseReport)))
                    .ToList();

                reports = reportClasses.Select(r => Activator.CreateInstance(r) as BaseReport)
                        .OrderBy(r => r.Category)
                        .ThenBy(r => r.CategoryOrder)
                        .ToList();

                CacheHelper.Set(key, reports, 3600);
            }

            return reports.Where(filter).ToList();
        }

        /// <summary>
        /// Returns the base reports in async mode
        /// </summary>
        /// <returns></returns>
        public async Task<List<BaseReport>> GetBaseReportsAsync()
        {
            return await Task.FromResult(GetBaseReports());
        }

        /// <summary>
        /// Returns the ESS Reports
        /// </summary>
        /// <param name="reportType"></param>
        /// <returns></returns>
        public List<BaseReport> GetESSReport(string reportType)
        {
            //define enum based on string query
            ESSReportType rtype = ESSReportType.TeamReport;
            switch (reportType.ToUpper())
            {
                case "ORGANIZATION":
                case "ORGANIZATIONREPORT":
                case "ORGANIZATIONREPORTS":
                    rtype = ESSReportType.OrganizationReport;
                    break;
                default:
                    rtype = ESSReportType.TeamReport;
                    break;
            }

            return GetBaseReports().Where(p => p.IsESSReport
                                            && p.ESSReportDetails != null
                                            && p.ESSReportDetails.Type == rtype).ToList();
        }

        /// <summary>
        /// Retrieve the ESS by async
        /// </summary>
        /// <param name="reportType"></param>
        /// <returns></returns>
        public async Task<List<BaseReport>> GetESSReportAsync(string reportType)
        {
            return await Task.FromResult(GetESSReport(reportType));
        }

        /// <summary>
        /// Returns the main category using async
        /// </summary>
        /// <param name="mainCategory"></param>
        /// <returns></returns>
        public async Task<List<BaseReport>> GetReportByMainCategoryAsync(string mainCategory)
        {
            return await Task.FromResult(GetBaseReports().Where(r => r.MainCategory == mainCategory).ToList());
        }

        /// <summary>
        /// Get all the reports by category
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public async Task<List<BaseReport>> GetReportByCategoryAsync(string category)
        {
            return await Task.FromResult(GetBaseReports().Where(r => r.Category == category).ToList());
        }

        /// <summary>
        /// Get the report by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<BaseReport> GetBaseReportByIdAsync(string id)
        {
            return await Task.FromResult(GetBaseReports().FirstOrDefault(r => r.Id.Equals(id, StringComparison.InvariantCultureIgnoreCase)));
        }

        /// <summary>
        /// Returns the criteria for a given report id
        /// </summary>
        /// <param name="reportId"></param>
        /// <returns></returns>
        public async Task<Arguments.FilterCriteria> GetCriteria(string reportId)
        {
            var report = await GetBaseReportByIdAsync(reportId);
            var userCriteriaData = await GetUserReportCriteriaDataAsync();
            var criteria=report.BuildEmployerCriteria(userCriteriaData, report.GetCriteria(userCriteriaData.AuthenticatedUser));
            //converting the ReportCriteria to FilterCriteria
            return ConvertToFilterCriteria(criteria);
        }

        /// <summary>
        /// Returns the report result in async mode
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public async Task<Reports.ReportResult> ExecuteReportAsync(ExecuteReportArgs args)
        {
            var report = await GetBaseReportByIdAsync(args.ReportId);
            var data = await GetUserReportCriteriaDataAsync();
            var criteria = Utilities.MergeCriteria(args.Criteria, report.BuildEmployerCriteria(data, report.GetCriteria(data.AuthenticatedUser)));
            return await report.RunReportAsync(data.AuthenticatedUser, criteria, args.GroupType, args.PageNumber, args.PageSize, args.SortBy, args.OrderByAsc, args.GroupByColumns);
        }


        /// <summary>
        /// Execute the adhoc report and return result
        /// </summary>
        /// <param name="reportDefinition"></param>
        /// <param name="reportFields"></param>
        /// <param name="limit"></param>z
        /// <param name="offset"></param>
        /// <returns></returns>
        public async Task<AdHocReportResult> ExecuteAdhocReportAsync(AbsenceSoft.Data.Reporting.ReportDefinition reportDefinition, List<ReportField> reportFields = null, int limit = 10, int offset = 0)
        {
            if (reportDefinition == null)
            {
                throw new ArgumentNullException("reportDefinition");
            }

            var userData = await GetUserReportCriteriaDataAsync();

            if (reportFields == null)
            {
                reportFields = GetFields(reportDefinition.ReportType.Id);
            }

            var requestDate = DateTime.UtcNow;
            var adHocReportResult = new AdHocReportResult
            {
                Name = reportDefinition.Name,
                AdHocReportType = GetAdhocReportCategory(reportDefinition),
                IconImage = null,
                RequestDate = requestDate,
                RequestedBy = userData.SecurityUser,
                Success = true,
                Error = null,
                Items = null,
                RecordCount = null
            };

            try
            {
                // Build Query
                var queryResult = await AdhocBuildQueryAsync(reportDefinition, reportFields, limit, offset);

                // Get sql injection safe query
                var db = new CommandRunner(DataWarehouse);
                var formattedResult = db.ExecuteDynamic(queryResult.Query, queryResult.Arguments.ToArray());
                var query = Convert.ToString(formattedResult.First().query);

                // Record query for report.
                adHocReportResult.Query = query;

                // Execute sql injection safe query
                List<DynamicAwesome> results = await ExecuteQueryAsync(query);
                var formattedResults = FormatResults(results);

                adHocReportResult.CompletedIn = DateTime.UtcNow.ToUnixDate() - requestDate.ToUnixDate();
                adHocReportResult.Items = formattedResults.Select(p => new AdHocResultData(p)).ToList();
                adHocReportResult.RecordCount = formattedResults.Count;
                return adHocReportResult;
            }
            catch (Exception ex)
            {

                Logger.Error(ex);

                adHocReportResult.CompletedIn = DateTime.UtcNow.ToUnixDate() - requestDate.ToUnixDate();
                adHocReportResult.Success = false;
                adHocReportResult.Error = ex.Message;
                return adHocReportResult;
            }
        }

        /// <summary>
        /// Execute a adhoc report definition
        /// </summary>
        /// <param name="reportDefinitionId"></param>
        /// <param name="reportFields"></param>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public async Task<AdHocReportResult> ExecuteAdhocReportAsync(string reportDefinitionId, List<ReportField> reportFields = null, int limit = 10, int offset = 0)
        {
            if (string.IsNullOrWhiteSpace(reportDefinitionId))
            {
                throw new ArgumentNullException("reportDefinitionId");
            }

            //get the adhoc report
            var reportDefinition = GetAllReportDefinitions().FirstOrDefault(p => p.Id == reportDefinitionId);
            if (reportDefinition == null)
            {
                throw new ArgumentException("Invalid report");
            }

            return await ExecuteAdhocReportAsync(reportDefinition, reportFields, limit, offset);
        }

        /// <summary>
        /// Returns the Case list for given CaseIds
        /// </summary>
        /// <param name="caseIds"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<Case>> GetCase(List<string> caseIds, int pageNumber, int pageSize)
        {
            return await DataProvider.GetCaseById(caseIds, pageNumber, pageSize);
        }

        /// <summary>
        /// Returns the Case list for given query
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<List<Case>> GetCase(IMongoQuery query, int pageNumber, int pageSize)
        {
            return await DataProvider.GetCase(query, pageNumber, pageSize);
        }

        /// <summary>
        /// Returns list of Reasons for case excluding Accommodation
        /// </summary>
        /// <returns></returns>
        public async Task<List<string>> GetCaseReasonsExcludingAccommodation()
        {
            return await DataProvider.GetCaseReasonsExcludingAccommodation();
        }

        /// <summary>
        /// Gets the WorkState of Cases of a user
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<List<string>> GetCaseWorkState(UserReportCriteriaData data)
        {
            return await DataProvider.GetCaseWorkState(data);
        }

        /// <summary>
        /// Returns Case count of the user
        /// </summary>
        /// <param name="userData"></param>
        /// <returns></returns>
        public async Task<int> UserCaseCount(UserReportCriteriaData userData)
        {
            return await DataProvider.UserCaseCount(userData.SecurityUser);
        }

        /// <summary>
        /// Gets the Employer Audit Case
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<IEnumerable<EmployerAudit<CaseNote>>> GetEmployerAuditCase(UserReportCriteriaData data, int pageNumber, int pageSize)
        {
            return await DataProvider.GetEmployerAuditCase(data, pageNumber, pageSize);

        }

        /// <summary>
        /// Gets the employee policy summary by case identifier.
        /// </summary>
        /// <param name="caseId">The case identifier.</param>
        /// <param name="includeStatus">The include status.</param>
        /// <param name="policyCodes">The policy codes.</param>
        /// <returns></returns>
        /// <exception cref="AbsenceSoftException">Case not found</exception>
        public async Task<List<PolicySummary>> GetEmployeePolicySummaryByCaseIdAsync(string caseId, EligibilityStatus[] includeStatus, params string[] policyCodes)
        {
            return await DataProvider.GetEmployeePolicySummaryByCaseIdAsync(caseId, includeStatus, policyCodes);
        }

        /// <summary>
        /// Build and run Absence status report query
        /// </summary>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async Task<List<AbsenceSoft.Data.Cases.Case>> RunAbsenceReportQueryAsync(ReportCriteria reportCriteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {           
            return await DataProvider.RunAbsenceReportQueryAsync(SecurityUser, Utilities.CreateReportingCriteria(reportCriteria), employerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc);
        }

        /// <summary>
        /// Build and run Accommodation status report query
        /// </summary>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async Task<List<AbsenceSoft.Data.Cases.Case>> RunAccomodationReportQueryAsync(ReportCriteria reportCriteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {           
            return await DataProvider.RunAccommodationReportQueryAsync(SecurityUser, Utilities.CreateReportingCriteria(reportCriteria), employerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc);
        }

        /// <summary>
        /// Build and run Custom Field Accommodation  report query
        /// </summary>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async Task<List<AbsenceSoft.Data.Cases.Case>> RunCustomFieldReportQueryAsync(ReportCriteria reportCriteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            return await DataProvider.RunCustomFieldReportQueryAsync(SecurityUser, Utilities.CreateReportingCriteria(reportCriteria), employerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc);
        }

        /// <summary>
        /// Build and run Custom Accommodation status report query
        /// </summary>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async Task<List<AbsenceSoft.Data.Cases.Case>> RunCustomAccomDetailReportQueryAsync(ReportCriteria reportCriteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            return await DataProvider.RunCustomAccomDetailReportQueryAsync(SecurityUser, Utilities.CreateReportingCriteria(reportCriteria), employerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc);
        }

        /// <summary>
        /// Build and run custom Accommodation status Granted report query
        /// </summary>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async Task<List<AbsenceSoft.Data.Cases.Case>> RunCustomAccomodationReportQueryAsync(ReportCriteria reportCriteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {           
            return await DataProvider.RunCustomAccomodationReportQueryAsync(SecurityUser, Utilities.CreateReportingCriteria(reportCriteria), employerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc);
        }

        /// <summary>
        /// Get all Employees Details of given employee
        /// </summary>
        /// <param name="employeeIds"></param>
        /// <returns>Returns all Employees</returns>
        public async Task<Dictionary<string, AbsenceSoft.Data.Customers.Employee>> GetEmployeeDetailsAsync(IList<string> employeeIds)
        {
            return await DataProvider.GetEmployeeDetailsAsync(employeeIds);
        }

        /// <summary>
        /// Get the Employee contact details of employees by Contact Type
        /// </summary>
        /// <param name="employeeIds"></param>
        /// <param name="contactTypeCode">Contact Type</param>
        /// <returns>Employee Contact details</returns>
        public async Task<Dictionary<string, List<AbsenceSoft.Data.Customers.EmployeeContact>>> GetEmployeeContactDetailsAsync(IList<string> employeeIds, string contactTypeCode)
        {
            return await DataProvider.GetEmployeeContactDetailsAsync(employeeIds, contactTypeCode);
        }

        /// <summary>
        /// Get all the User Details by customer
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns>Returns all user</returns>
        public async Task<Dictionary<string, string>> GetUserDetailsAsync(string customerId)
        {
            return await DataProvider.GetUserDetailsAsync(customerId);
        }

        /// <summary>
        /// Build and run custom Accommodation status granted report query
        /// </summary>
        /// <param name="reportCriteria"></param>
        /// <returns></returns>
        public async Task<List<AbsenceSoft.Data.Cases.Case>> RunAccommodationStatusGrantedQueryAsync(ReportCriteria reportCriteria)
        {           
            return await DataProvider.RunAccommodationStatusGrantedQueryAsync(SecurityUser, Utilities.CreateReportingCriteria(reportCriteria));
        }

        /// <summary>
        /// Gets the User list for given UserIds
        /// </summary>
        /// <param name="userIds"></param>
        /// <returns></returns>
        public async Task<IEnumerable<AbsenceSoft.Data.Security.User>> GetUsersById(List<string> userIds)
        {
            return await DataProvider.GetUsersById(userIds);
        }

        /// <summary>
        /// Gets the User for given User id
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<AbsenceSoft.Data.Security.User> GetSecurityUserById(User user)
        {
            var key = string.Format("USER_DETAILS_{0}", user.Key);
            var securityUser = CacheHelper.Get<AbsenceSoft.Data.Security.User>(key);
            if (securityUser == null)
            {
                securityUser = await DataProvider.GetSecurityUserById(user);
                CacheHelper.Set(key, securityUser);
            }
            return securityUser;
        }
                
        /// <summary>
        /// Get list for Disability report
        /// </summary>
        /// <param name="data"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<Case>> GetCaseListForDisabilityReport(UserReportCriteriaData data, AbsenceSoft.Reporting.ReportCriteria criteria, string employerIdFilterName, int pageNumber = 1, int pageSize = 10)
        {
            return await DataProvider.GetCaseListForDisabilityReport(data, criteria, employerIdFilterName, pageNumber, pageSize);
        }

        /// <summary>
        /// Build and Run custom accommodations status granted report
        /// </summary>       
        /// <param name="criteria">Report Criteria</param>       
        /// <param name="pageNumber">Page number</param>
        /// <param name="pageSize">Page Size</param>
        /// <param name="sortBy">Sort by column name</param>
        /// <param name="orderByAsc">Order by column</param>
        /// <returns>Case list</returns>
        public async Task<List<Case>> RunAccommodationCustomBaseQueryAsync(ReportCriteria criteria, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            return await DataProvider.RunAccommodationCustomBaseQueryAsync(SecurityUser, Utilities.CreateReportingCriteria(criteria), pageNumber, pageSize, sortBy, orderByAsc);
        }

        /// <summary>
        /// Run Communication Report query and get the result
        /// </summary>       
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async Task<List<Communication>> RunCommunicationQueryAsync(ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {            
            return await DataProvider.RunCommunicationQueryAsync(SecurityUser, Utilities.CreateReportingCriteria(criteria), employerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc);
        }

        /// <summary>
        /// Get the Case Notes by Case ids
        /// </summary>
        /// <param name="caseIds"></param>
        /// <returns></returns>
        public async Task<IList<CaseNote>> GetCaseNotesbyCaseIdsAsync(IList<string> caseIds)
        {
            return await DataProvider.GetCaseNotesbyCaseIdsAsync(caseIds);
        }

        /// <summary>
        /// Get the Accommodation questions by customer Ids
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public async Task<IList<AccommodationQuestion>> GetAccommodationQuestionsByCustomerId(string customerId)
        {
            return await DataProvider.GetAccommodationQuestionsByCustomerId(customerId);
        }

        /// <summary>
        /// Gets Case List for Daily Case Report
        /// </summary>
        /// <param name="data"></param>
        /// <param name="criteria"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<Case>> GetCaseListForDailyCaseReport(UserReportCriteriaData data, ReportCriteria criteria, string employerIdFilter, int pageNumber = 1, int pageSize = 10)
        {
            return await DataProvider.GetCaseListForDailyCaseReport(data, Utilities.CreateReportingCriteria(criteria), employerIdFilter, pageNumber, pageSize);
        }

        /// <summary>
        /// Gets Case List for Daily Custom Case Report
        /// </summary>
        /// <param name="data"></param>
        /// <param name="criteria"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<Case>> GetCaseListForCustomDailyCaseReport(UserReportCriteriaData data, ReportCriteria criteria, string employerIdFilter, int pageNumber = 1, int pageSize = 10)
        {
            return await DataProvider.GetCaseListForCustomDailyCaseReport(data, Utilities.CreateReportingCriteria(criteria), employerIdFilter, pageNumber, pageSize);
        }
        

        /// <summary>
        /// Gets Case List for Needle Stick Report
        /// </summary>
        /// <param name="data"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<Case>> GetCaseListForWorkRelatedReport(UserReportCriteriaData data, ReportCriteria criteria, ReportGroupType? reportGroupType, string employerIdFilterName, int pageNumber = 1, int pageSize = 10)
        {
            return await DataProvider.GetCaseListForWorkRelatedReport(data, Utilities.CreateReportingCriteria(criteria), reportGroupType, employerIdFilterName, pageNumber, pageSize);
        }

        /// <summary>
        /// Gets Employee Restriction for Case Ids
        /// </summary>
        /// <param name="data"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="caseIds"></param>
        /// <returns></returns>
        public async Task<List<EmployeeRestriction>> GetEmployeeRestrictionForCaseIds(UserReportCriteriaData data, ReportCriteria reportCriteria, List<string> caseIds)
        {
            return await DataProvider.GetEmployeeRestrictionForCaseIds(data, Utilities.CreateReportingCriteria(reportCriteria), caseIds);
        }

        /// <summary>
        /// Gets Demand for Restriction CaseIds
        /// </summary>
        /// <param name="employeeRestrictions"></param>
        /// <returns></returns>
        public async Task<List<Demand>> GetDemandForRestrictionCaseIds(List<EmployeeRestriction> employeeRestrictions)
        {
            return await DataProvider.GetDemandForRestrictionCaseIds(employeeRestrictions);
        }

        /// <summary>
        /// Gets Employee Contact
        /// </summary>
        /// <param name="empIds"></param>
        /// <param name="contactTypeCode"></param>
        /// <returns></returns>
        public async Task<List<EmployeeContact>> GetEmployeeContact(List<string> empIds, string contactTypeCode = "SUPERVISOR")
        {
            return await DataProvider.GetEmployeeContact(empIds, contactTypeCode);
        }

        /// <summary>
        /// Gets Employee Job for Associated Employee number
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="empNums"></param>
        /// <returns></returns>
        public async Task<List<EmployeeJob>> GetEmployeeJobForAssociatedEmployeeNumber(string customerId, List<string> empNums)
        {
            return await DataProvider.GetEmployeeJobForAssociatedEmployeeNumber(customerId, empNums);
        }

        /// <summary>
        /// Build and run TodoItem report query
        /// </summary>
        /// <param name="reportCriteria">Report Criteria</param>
        /// <param name="employerIdFilterName">Employer Id Filter name</param>
        /// <returns>List of TotoItem</returns>
        public async Task<List<ToDoItem>> RunToDoItemReportQueryAsync(ReportCriteria reportCriteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {           
            return await DataProvider.RunToDoItemReportQueryAsync(SecurityUser, Utilities.CreateReportingCriteria(reportCriteria), employerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc);
        }

        /// <summary>
        /// Build and Run Employee on Leave Report query
        /// </summary>
        /// <param name="user">Current User</param>
        /// <param name="criteria">Report Criteria</param>      
        /// <param name="pageNumber">Page number</param>
        /// <param name="pageSize">Page Size</param>
        /// <param name="sortBy">Sort by column name</param>
        /// <param name="orderByAsc">Order by column</param>
        /// <returns>Case list</returns>
        public async Task<List<Case>> RunEmployeeOnLeaveReportQueryAsync(ReportCriteria criteria, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {           
            return await DataProvider.RunEmployeeOnLeaveReportQueryAsync(SecurityUser, Utilities.CreateReportingCriteria(criteria), pageNumber, pageSize, sortBy, orderByAsc);
        }

        /// <summary>
        /// Method gives the call to the provider's RunCaseWithoutToDoReportQueryAsync method which build and runs the query for cases without to do report.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async Task<List<ToDoMapReduceResultItem>> RunCaseWithoutToDoReportQueryAsync(ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {          
            return await DataProvider.RunCaseWithoutToDoReportQueryAsync(SecurityUser, Utilities.CreateReportingCriteria(criteria), employerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc);
        }

        /// <summary>
        /// Build and Run Employee Survey Report query
        /// </summary>
        /// <param name="user">Current User</param>
        /// <param name="criteria">Report Criteria</param>      
        /// <param name="pageNumber">Page number</param>
        /// <param name="pageSize">Page Size</param>
        /// <param name="sortBy">Sort by column name</param>
        /// <param name="orderByAsc">Order by column</param>
        /// <returns>Case list</returns>
        public async Task<List<Case>> RunEmployeeSurveyReportQueryAsync(ReportCriteria criteria, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {          
            return await DataProvider.RunEmployeeSurveyReportQueryAsync(SecurityUser, Utilities.CreateReportingCriteria(criteria), pageNumber, pageSize, sortBy, orderByAsc);
        }

        /// <summary>
        /// Build and Run User Activity Report
        /// </summary>       
        /// <param name="criteria"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async Task<(List<AbsenceSoft.Data.Security.User>, Dictionary<string, int>, Dictionary<string, int>, Dictionary<string, int>, Dictionary<string, int>)> RunUserActivityReportQueryAsync(ReportCriteria criteria, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {          
            return await DataProvider.RunUserActivityReportQueryAsync(SecurityUser, Utilities.CreateReportingCriteria(criteria), pageNumber, pageSize, sortBy, orderByAsc);
        }

        /// <summary>
        /// Build and Run user Activity Detail Report
        /// </summary>        
        /// <param name="criteria"></param>
        /// <returns></returns>
        public async Task<(Dictionary<string, string>, Dictionary<string, CaseModel>, List<CaseNote>, List<EmployeeNote>, List<Attachment>)> RunUserActivityDetailReportQueryAsync(ReportCriteria criteria)
        {
            var data = await GetUserReportCriteriaDataAsync();
            return await DataProvider.RunUserActivityDetailReportQueryAsync(data.SecurityUser, Utilities.CreateReportingCriteria(criteria));
        }

        /// <summary>
        /// Build and Run Intermittent Pattern utilization report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async Task<List<Case>> RunPatternUtilizationReportQueryAsync(ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {           
            return await DataProvider.RunPatternUtilizationReportQueryAsync(SecurityUser, Utilities.CreateReportingCriteria(criteria), employerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc);
        }

        /// <summary>
        /// Gets Case list of Work Restriction by Location Amazon Report
        /// </summary>
        /// <param name="data"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<Case>> GetCaseListForWorkRestrictionByLocationAmazonReport(UserReportCriteriaData data, ReportCriteria criteria, string employerIdFilterName, int pageNumber = 1, int pageSize = 10)
        {
            return await DataProvider.GetCaseListForWorkRestrictionByLocationAmazonReport(data, Utilities.CreateReportingCriteria(criteria), employerIdFilterName, pageNumber, pageSize);
        }

        /// <summary>
        /// Method builds and run Accomodation By Decision Report query applying the reportcriterias and aggregation
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="EmployerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async Task<List<List<object>>> RunAccomodationByDecisionReportQueryAsync(ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {          
            return await DataProvider.RunAccomodationByDecisionReportQueryAsync(SecurityUser, Utilities.CreateReportingCriteria(criteria), employerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc);
        }

        /// <summary>
        /// Method builds and run Accomodation By Decision Report query applying the reportcriterias and aggregation
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="EmployerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async Task<List<Case>> RunAccomodationBySimilarlySituatedReportQueryAsync(ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            var data = await GetUserReportCriteriaDataAsync();
            return await DataProvider.RunAccomodationBySimilarySituatedQueryAsync(data.SecurityUser, Utilities.CreateReportingCriteria(criteria), employerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc);
        }

        /// <summary>
        /// Method builds and run Accomodation By Decision Report query applying the reportcriterias and aggregation
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="EmployerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async Task<List<List<object>>> RunAccomodationBySummaryReportQueryAsync(ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {             
            return await DataProvider.RunAccomodationBySummaryReportQueryAsync(SecurityUser, Utilities.CreateReportingCriteria(criteria), employerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc);
        }

        /// <summary>
        /// Methods builds and calls the provider's RunCasesExceedFrequencyDurationReportQuery method.
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async Task<List<Case>> RunCasesExceedFrequencyDurationReportQueryAsync(ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            return await DataProvider.RunCasesExceedFrequencyDurationReportQueryAsync(SecurityUser, Utilities.CreateReportingCriteria(criteria), employerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc);
        }

        /// <summary>
        ///  Gets the Result for BI Report by Leave for the user
        /// </summary>
        /// <param name="userReportCriteriaData"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilter"></param>
        /// <returns></returns>
        public async Task<List<List<object>>> GetBiReportByLeaveResult(UserReportCriteriaData userReportCriteriaData, Reports.ReportResult result, string employerIdFilter)
        {
            return await DataProvider.GetBiReportByLeaveResult(userReportCriteriaData, Utilities.CreateReportingCriteria(result.Criteria), employerIdFilter);
        }

        /// <summary>
        ///  Gets the Result for BI Report by Location for the user
        /// </summary>
        /// <param name="userReportCriteriaData"></param>
        /// <param name="result"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilter"></param>
        /// <returns></returns>
        public async Task<List<List<object>>> GetBiReportByLocationResult(UserReportCriteriaData userReportCriteriaData, Reports.ReportResult result, string employerIdFilter)
        {
            return await DataProvider.GetBiReportByLocationResult(userReportCriteriaData, Utilities.CreateReportingCriteria(result.Criteria), employerIdFilter);
        }

        /// <summary>
        /// Methods builds and calls the provider's RunMultipleOpenCasesReportQuery method.
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async Task<List<Case>> RunMultipleOpenCasesReportQueryAsync(ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            return await DataProvider.RunMultipleOpenCasesReportQueryAsync(SecurityUser, Utilities.CreateReportingCriteria(criteria), employerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc);
        }
        /// <summary>
        /// Gets the Result for BI Report by Reason for the user
        /// </summary>
        /// <param name="userReportCriteriaData"></param>
        /// <param name="result"></param>
        /// <param name="employerIdFilter"></param>
        /// <returns></returns>
        public async Task<List<List<object>>> GetBiReportByReasonResult(UserReportCriteriaData userReportCriteriaData, Reports.ReportResult result, string employerIdFilter)
        {
            return await DataProvider.GetBiReportByReasonResult(userReportCriteriaData, Utilities.CreateReportingCriteria(result.Criteria), employerIdFilter);
        }

        /// <summary>
        /// Gets the Result for BI Report by Reason for the user
        /// </summary>
        /// <param name="userReportCriteriaData"></param>
        /// <param name="result"></param>
        /// <param name="employerIdFilter"></param>
        /// <returns></returns>
        public async Task<List<List<object>>> GetBiReportByStatusResult(UserReportCriteriaData userReportCriteriaData, Reports.ReportResult result, string employerIdFilter)
        {
            return await DataProvider.GetBiReportByStatusResult(userReportCriteriaData, Utilities.CreateReportingCriteria(result.Criteria), employerIdFilter);
        }

        /// <summary>
        /// Gets the Result for BI Report by Policy for the user
        /// </summary>
        /// <param name="userReportCriteriaData"></param>
        /// <param name="result"></param>
        /// <param name="employerIdFilter"></param>
        /// <returns></returns>
        public async Task<List<List<object>>> GetBiReportByPolicyResult(UserReportCriteriaData userReportCriteriaData, Reports.ReportResult result, string employerIdFilter)
        {
            return await DataProvider.GetBiReportByPolicyResult(userReportCriteriaData, Utilities.CreateReportingCriteria(result.Criteria), employerIdFilter);
        }
        
        /// <summary>
        /// Gets the Result for BI Report by TotalLeaves by Reason for the user
        /// </summary>
        /// <param name="userReportCriteriaData"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilter"></param>
        /// <returns></returns>
        public async Task<List<List<object>>> GetBiReportTotalLeavesByReasonResult(UserReportCriteriaData userReportCriteriaData, Reports.ReportResult result, string employerIdFilter)
        {
            return await DataProvider.GetBiReportTotalLeavesByReasonResult(userReportCriteriaData, Utilities.CreateReportingCriteria(result.Criteria), employerIdFilter);
        }

        /// <summary>
        /// Gets the Result for BI Report by Total Leaves for the user
        /// </summary>
        /// <param name="userReportCriteriaData"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilter"></param>
        /// <returns></returns>
        public async Task<List<List<object>>> GetBiReportTotalLeavesByTotalResult(UserReportCriteriaData userReportCriteriaData, Reports.ReportResult result, string employerIdFilter)
        {
            return await DataProvider.GetBiReportTotalLeavesByTotalResult(userReportCriteriaData, Utilities.CreateReportingCriteria(result.Criteria), employerIdFilter);
        }
        
        /// <summary>
        /// Run Team Productivity Bi Report
        /// </summary>       
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async Task<List<List<object>>> RunTeamProductivityBiQueryAsync(ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            return await DataProvider.RunTeamProductivityBiQueryAsync(SecurityUser, Utilities.CreateReportingCriteria(criteria), employerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc);
        }

        /// <summary>
        /// Run Team Compliance BI Report
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async Task<List<List<object>>> RunTeamComplianceBiQueryAsync(ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            return await DataProvider.RunTeamComplianceBiQueryAsync(SecurityUser, Utilities.CreateReportingCriteria(criteria), employerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc);
        }

        /// <summary>
        ///  Build and Run Case Processing Metrics BI Report
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async Task<List<List<object>>> RunCaseProcessingMetricsBiQueryAsync(ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            return await DataProvider.RunCaseProcessingMetricsBiQueryAsync(SecurityUser, Utilities.CreateReportingCriteria(criteria), employerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc);
        }

        /// <summary>
        /// Sends Report for given parameters
        /// </summary>
        /// <param name="reportDeliveryArgs"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ResultSet<bool>> SendReportDelivery(EmailReportDeliveryArgs emailReportDeliveryArgs)
        {
            EmailDeliveryProvider reportDeliveryProvider = new EmailDeliveryProvider(emailReportDeliveryArgs, AuthenticatedUser);
            return await reportDeliveryProvider.Send();
        }

        /// <summary>
        /// Runs the intermittent schedule exception query asynchronous.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <param name="employerIdFilterName">Name of the employer identifier filter.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="sortBy">The sort by.</param>
        /// <param name="orderByAsc">if set to <c>true</c> [order by asc].</param>
        /// <returns></returns>
        public async Task<List<IntermittentScheduleExceptionDataRow>> RunIntermittentScheduleExceptionQueryAsync(
            ReportCriteria criteria, string employerIdFilterName, int? pageNumber, int? pageSize, string sortBy, bool orderByAsc)
        {
            var data = await GetUserReportCriteriaDataAsync();
            return await DataProvider.RunIntermittentScheduleExceptionQueryAsync(data.SecurityUser, Utilities.CreateReportingCriteria(criteria), employerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc);
        }
    }
}