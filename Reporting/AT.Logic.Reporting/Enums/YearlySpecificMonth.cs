﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Enums
{
     /// <summary>
     /// Third part of a custom date. This is the Month of the year for which the custom date confines to..
     /// The value of this enum matches the ordinal position of the given month. So Jan = 1, Feb = 2, etc.
     /// </summary>
    public enum YearlySpecificMonth
    {
        January = 1,
        February,
        March,
        April,
        May,
        June,
        July,
        August,
        September,
        October,
        November,
        December
    };
}
