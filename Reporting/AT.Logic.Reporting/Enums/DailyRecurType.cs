﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Enums
{
    public enum DailyRecurType
    {
        OnEveryXDays,
        OnEveryWeekday
    };
}
