﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Enums
{
    /// <summary>
    /// First part of a custom date. This would be First, Second, etc. item of the month.
    /// </summary>
    public enum YearlySpecificDatePartOne
    {
        NotSet = -1,
        First,
        Second,
        Third,
        Fourth,
        Last
    };
}
