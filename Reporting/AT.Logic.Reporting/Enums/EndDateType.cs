﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Enums
{
    public enum EndDateType
    {        
        NoEndDate = 0,
        SpecificDate,
        NumberOfOccurrences
    };
}
