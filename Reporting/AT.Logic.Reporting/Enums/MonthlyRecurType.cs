﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Enums
{
    /// <summary>
    /// Is this on a specific day of the month, a custom date, or after the occurrence is completed.
    /// </summary>
    public enum MonthlyRecurType
    {
        OnSpecificDayOfMonth,
        OnCustomDateFormat,
        AfterOccurrenceCompleted
    };
}
