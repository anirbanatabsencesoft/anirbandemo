﻿namespace AT.Logic.Reporting.Enums
{
    public enum ReportRequestType
    {
        Operations,
        Adhoc,
        BI,
        Forensic,
        ESS
    }
}