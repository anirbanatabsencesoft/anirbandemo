﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Enums
{
    public enum RecurrenceType
    {
        OneTime = 0,
        Daily = 1,
        Weekly,
        Monthly,
        Yearly
    };
}
