using AT.Logic.Reporting.Enums;
using System;

namespace AT.Logic.Reporting.Schedule
{
    
    public class WeeklyRecurrenceSettings : RecurrenceSettings
    {
        #region Constructors
       
        /// <summary>
        /// Get dates by Start and End date boundries.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        public WeeklyRecurrenceSettings(DateTime startDate, DateTime? endDate=null) : base(startDate, endDate) { }
        /// <summary>
        /// Get dates by Start date and number of occurrences.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="numberOfOccurrences"></param>
        public WeeklyRecurrenceSettings(DateTime startDate, int numberOfOccurrences) : base(startDate, numberOfOccurrences) { }
        #endregion

        #region Private Fields
        
        WeeklyRecurType regenType = WeeklyRecurType.OnEveryXWeeks;
        SelectedDayOfWeekValues selectedDays;
        int regenEveryXWeeks;
        bool getNextDateValue;
        DateTime nextDateValue;
#endregion //Private Fields

        #region Public GetValues
        /// <summary>
        /// Get day values. This overload is for every x-weeks.
        /// </summary>
        /// <param name="regenEveryXDays">Interval of weeks. Every x-weeks.</param>
        /// <returns>RecurrenceValues</returns>
        public RecurrenceValues GetValues(int regenEveryXWeeks, SelectedDayOfWeekValues selectedDays)
        {
            this.regenEveryXWeeks = regenEveryXWeeks;
            regenType = WeeklyRecurType.OnEveryXWeeks;
            this.selectedDays = selectedDays;
            return GetValues();
        }
#endregion //Public GetValues

      


        #region Internal GetValues
        /// <summary>
        ///     Final overloaded function that gets the Recurrence Values. 
        ///     This is called from the RecurrenceHelper staic methods only.
        /// </summary>
        /// <returns>
        ///     A AT.Logic.Reporting.Schedule.RecurrenceValues value...
        /// </returns>
        internal override RecurrenceValues GetValues()
        {
            return GetRecurrenceValues();
        }
        internal override RecurrenceValues GetValues(DateTime startDate, DateTime endDate)
        {
            base.StartDate = startDate;
            base.EndDate = endDate;
            // Change the end type to End Date as this original series info
            // may have been set to number of occurrences.
            base.endDateType = EndDateType.SpecificDate;
            return GetRecurrenceValues();
        }

        internal override RecurrenceValues GetValues(DateTime startDate, int numberOfOccurrences)
        {
            base.NumberOfOccurrences = numberOfOccurrences;
            base.StartDate = startDate;
            // Change the end type to number of occurrences. 
            // This must be set because the original starting Series Info may
            // be set to have an End Date type.
            base.endDateType = EndDateType.NumberOfOccurrences;
            return GetRecurrenceValues();
        }
        RecurrenceValues GetRecurrenceValues()
        {
            RecurrenceValues values = null;
            switch (RegenType)
            {
                case WeeklyRecurType.OnEveryXWeeks:
                    values = GetEveryXWeeksValues();
                    break;

            }
            if (values?.Values.Count > 0)
            {
                values.SetStartDate(values.Values[0]);

                // Get the end date if not open-ended
                if (base.TypeOfEndDate != EndDateType.NoEndDate)
                    values.SetEndDate(values.Values[values.Values.Count - 1]);
            }

            return values;
        }
#endregion //Internal GetValues

        #region Internal Procedures

        
        /// <summary>
        /// Set the values in preperation for getting the Next date in the series.
        /// </summary>
        /// <param name="regenEveryXWeeks">Value to regenerate the dates every x-weeks</param>
        /// <param name="selectedDays">Struct of days selected for the week.</param>
        void SetValues(int regenEveryXWeeks, SelectedDayOfWeekValues selectedDays)
        {
            this.regenEveryXWeeks = regenEveryXWeeks;
            regenType = WeeklyRecurType.OnEveryXWeeks;
            this.selectedDays = selectedDays;
        }

        /// <summary>
        ///     Get the next date
        /// </summary>
        /// <param name="currentDate" type="System.DateTime">
        ///     <para>
        ///         
        ///     </para>
        /// </param>
        /// <returns>
        ///     A System.DateTime value...
        /// </returns>
        internal override DateTime GetNextDate(DateTime currentDate)
        {
            getNextDateValue = true;
            nextDateValue = currentDate;
            // Now get the values and return the last date found.
            RecurrenceValues values = GetValues();
            return values.LastDate;
        }

      
        public static WeeklyRecurrenceSettings GetRecurrenceSettings(ScheduleInfo info)
        {
            WeeklyRecurrenceSettings settings = null;
            // Determine the Constructor by the type of End Date.
            // All constructors start with a Start date at a minimum.
            switch (info.EndDateType)
            {
                case EndDateType.NumberOfOccurrences:
                    settings = new WeeklyRecurrenceSettings(info.StartDate, info.NumberOfOccurrences);
                    break;

                case EndDateType.SpecificDate:
                    settings = new WeeklyRecurrenceSettings(info.StartDate, info.EndDate.Value);
                    break;

                case null:
                case EndDateType.NoEndDate:
                    settings = new WeeklyRecurrenceSettings(info.StartDate);
                    break;
            }
            
             settings?.SetValues(info.WeeklyRecurEveryXWeeks, info.WeeklySelectedDays);


            return settings;

        }

        
#endregion //Internal Procedures
        
        #region Private Procedures
       
        void RunNextDateValue(DateTime dt, RecurrenceValues values)
        {
            do
            {
                dt = GetNextDay(dt);
                values.AddDateValue(dt);
                if (values.Values[values.Values.Count - 1] > nextDateValue)
                {
                    break;
                }
            } while (dt <= nextDateValue.AddDays((regenEveryXWeeks * 7) + 7)); // Ensure the last date if greater than what's needed for the next date in the cycle
        }
        RecurrenceValues GetEveryXWeeksValues()
        {
            RecurrenceValues values = new RecurrenceValues();
            DateTime dt = base.StartDate.AddDays(-1); // Backup a day so the first instance of GetNextDay will increment to the next day.

            if (getNextDateValue)
            {
                RunNextDateValue(dt, values);
            }
            else
            {
                switch (base.TypeOfEndDate)
                {
                    
                       

                    case EndDateType.NumberOfOccurrences:

                        for (int i = 0; i < base.NumberOfOccurrences; i++)
                        {
                            dt = GetNextDay(dt);
                            values.AddDateValue(dt);
                        }
                        break;

                    case EndDateType.SpecificDate:
                        do
                        {
                            dt = GetNextDay(dt);
                            // Handle for dates past the end date
                            if (dt > base.EndDate)
                            {
                                break;
                            }
                            values.AddDateValue(dt);
                        } while (dt <= base.EndDate);
                        break;
                    case EndDateType.NoEndDate:
                    default:
                        break;
                }
            }

            return values;
        }

        DateTime? GetSelectedDay(ref DateTime input)
        {
            DateTime? returnDate = null;
            input = input.AddDays(1);
            var propInfo=selectedDays.GetType().GetProperty(input.DayOfWeek.ToString());
            var propValue=(bool)propInfo.GetValue(selectedDays);
            if (propValue)
            {
                returnDate = input;
            }

            if (input.DayOfWeek == DayOfWeek.Saturday)
            {
                if (selectedDays.Saturday)
                {
                    returnDate = input;
                }
                else
                {
                    // Increment by weeks if regenXWeeks has a value 
                    // greater than 1 which is default.
                    // But only increment if we've gone over
                    // at least 7 days already.
                    if (regenEveryXWeeks > 1)
                    {
                        input = input.AddDays((regenEveryXWeeks - 1) * 7);
                    }
                }
            }
            
            return returnDate;
        }
        DateTime GetNextDay(DateTime input)
        {
            DateTime? returnDate = null;

            // Get the return date by incrementing the date
            // and checking the value against the selected days
            // of the week.
            do
            {
                returnDate=GetSelectedDay(ref input);
            } while (!returnDate.HasValue);
            return returnDate.Value;
        }
#endregion //Private Procedures

        #region Public Fields
        /// <summary>
        /// What is the interval to generate dates. This is used to skip weeks in the cycle.
        /// </summary>
        public int RegenEveryXWeeks
        {
            get
            {
                return regenEveryXWeeks;
            }
        }

        public SelectedDayOfWeekValues SelectedDays
        {
            get
            {
                return selectedDays;
            }
        }

        /// <summary>
        /// What is the regeneration type such as Specific day of month, custom date, etc.
        /// </summary>
        public WeeklyRecurType RegenType
        {
            get
            {
                return regenType;
            }
        }

#endregion //Public Fields

    }
}
