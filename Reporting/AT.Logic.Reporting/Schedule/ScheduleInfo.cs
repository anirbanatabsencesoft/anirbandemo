﻿using AT.Logic.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Schedule
{
    [Serializable]
    public class ScheduleInfo
    {
        public ScheduleInfo()
        {
            RecurrenceType = RecurrenceType.OneTime;
        }
        public long? Id { get; set; }

        public int AdjustmentValue { get; set; }

        public RecurrenceType RecurrenceType { get; set; }

        public DateTime? EndDate { get; set; }

        public DateTime StartDate { get; set; }

        public int NumberOfOccurrences { get; set; }

        public EndDateType? EndDateType { get; set; }

        public int DailyRecurEveryXDays { get; set; }

        public DailyRecurType? DailyRecurType { get; set; }

       

        // Weekly-Specific

        public int WeeklyRecurEveryXWeeks { get; set; }

        public WeeklyRecurType? WeeklyRecurType { get; set; }

        public SelectedDayOfWeekValues WeeklySelectedDays { get; set; }




        // Monthly-Specific
        
        /// <summary>
        ///     What is the interval to generate dates. This is used to skip months in the cycle.
        /// </summary>
        /// <value>
        ///     <para>
        ///         Integer of the interval value. 1 = every month, 2 = every other month, etc.
        ///     </para>
        /// </value>
        /// <remarks>
        ///     
        /// </remarks>
        public int MonthlyRecurEveryXMonths { get; set; }


        /// <summary>
        ///     Day of month to regenerate when RecurType = specific day of month.
        /// </summary>
        /// <value>
        ///     <para>
        ///         Integer of the day of the month.
        ///     </para>
        /// </value>
        /// <remarks>
        ///     
        /// </remarks>
        public int MonthlyRecurOnSpecificDateDayValue { get; set; }
        

        /// <summary>
        ///     What is the second part to the Custom date such as which weekday, weekend day, etc.
        /// </summary>
        /// <value>
        ///     <para>
        ///         
        ///     </para>
        /// </value>
        /// <remarks>
        ///     
        /// </remarks>
        public MonthlySpecificWeekday MonthlySpecificDatePartTwo { get; set; }
        

        /// <summary>
        ///     What is the first part to the Custom date such as First, Last.
        /// </summary>
        /// <value>
        ///     <para>
        ///         
        ///     </para>
        /// </value>
        /// <remarks>
        ///     
        /// </remarks>
        public MonthlySpecificDay MonthlySpecificDatePartOne { get; set; }
      

        /// <summary>
        ///     What is the regeneration type such as Specific day of month, custom date, etc.
        /// </summary>        
        public MonthlyRecurType MonthlyRecurType { get; set; }
        

        

        // Yearly-Specific Fields

        public MonthlySpecificDay YearlySpecificDatePartOne { get; set; }
       

        public MonthlySpecificWeekday YearlySpecificDatePartTwo { get; set; }
      

        public YearlySpecificMonth YearlySpecificDatePartThree { get; set; }
      

        public YearlyRecurType? YearlyRecurType { get; set; }
       

        public int SpecificDateDayValue { get; set; }
       
        public int SpecificDateMonthValue { get; set; }
       
        
    }
}
