using AT.Logic.Reporting.Enums;
using System;

namespace AT.Logic.Reporting.Schedule
{

    /// <summary>
    /// The class represents the settings for the Monthly recurrence type.
    /// </summary>
    public class MonthlyRecurrenceSettings : RecurrenceSettings
    {
        #region Constructors
        
        /// <summary>
        /// Get dates by Start and End date boundries.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        public MonthlyRecurrenceSettings(DateTime startDate, DateTime? endDate = null) : base(startDate, endDate) { }
        /// <summary>
        /// Get dates by Start date and number of occurrences.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="numberOfOccurrences"></param>
        public MonthlyRecurrenceSettings(DateTime startDate, int numberOfOccurrences) : base(startDate, numberOfOccurrences) { }
        #endregion

        #region Private Fields
        int regenerateOnSpecificDateDayValue;
        int regenEveryXMonths = 1;
        bool getNextDateValue;
        DateTime nextDateValue;
        MonthlyRecurType? recurType;
        MonthlySpecificDay? specificDatePartOne;
        MonthlySpecificWeekday? specificDatePartTwo;
        #endregion

        #region Private Procedures

        void RunRecurEveryXMonthForSpecificDay(DateTime dt, RecurrenceValues values)
        {
            do
            {
                values.AddDateValue(dt, AdjustmentValue);
                if (values.Values[values.Values.Count - 1] > nextDateValue)
                    break;

                dt = dt.AddMonths(RegenEveryXMonths);
                dt = GetCorrectedDate(dt);
            } while (dt <= nextDateValue.AddMonths(RegenEveryXMonths + 1)); // Ensure the last date if greater than what's needed for the next date in the cycle
        }

        /// <summary>
        /// Get recurring dates from a specific constant date such as 27 July.
        /// </summary>
        /// <returns></returns>
        RecurrenceValues GetSpecificDayOfMonthDates()
        {
            RecurrenceValues values = new RecurrenceValues();
            DateTime dt = base.StartDate;
            int dayValue = regenerateOnSpecificDateDayValue;
            int daysOfMonth = DateTime.DaysInMonth(dt.Year, dt.Month);
            // Get the max days of the month and make sure it's not 
            // less than the specified day value trying to be set.
            if (daysOfMonth < regenerateOnSpecificDateDayValue)
                dayValue = daysOfMonth;

            // Determine if start date is greater than the Day and Month values
            // for a specific date.
            DateTime newDate = new DateTime(dt.Year, dt.Month, dayValue, dt.Hour, dt.Minute, dt.Second);
            // Is the specific date before the start date, if so 
            // then make the specific date next month.
            if (newDate <= dt)
                dt = newDate.AddMonths(1);
            else
                dt = newDate;


            if (getNextDateValue)
            {
                RunRecurEveryXMonthForSpecificDay(dt, values);
            }
            else
            {
                switch (base.TypeOfEndDate)
                {

                    case EndDateType.NumberOfOccurrences:

                        for (int i = 0; i < base.NumberOfOccurrences; i++)
                        {
                            values.AddDateValue(dt, AdjustmentValue);
                            dt = dt.AddMonths(RegenEveryXMonths);
                            dt = GetCorrectedDate(dt);
                        }
                        break;

                    case EndDateType.SpecificDate:
                        do
                        {
                            values.AddDateValue(dt, AdjustmentValue);
                            dt = dt.AddMonths(RegenEveryXMonths);
                            dt = GetCorrectedDate(dt);
                        } while (dt <= base.EndDate);
                        break;
                    case EndDateType.NoEndDate:
                    default:
                        break;
                }
            }

            return values;
        }

        /// <summary>
        /// Correct an input date to be equal to or less than the specified day value.
        /// </summary>
        /// <param name="input">Date to check to ensure it matches the specified day value or the max number of days for that month, whichever comes first.</param>
        /// <returns>DateTime</returns>
        DateTime GetCorrectedDate(DateTime input)
        {
            DateTime dt = input;
            // Ensure the day value hasn't changed.
            // This will occurr if the month is Feb. All
            // dates after that will have the same day.
            if (dt.Day < this.regenerateOnSpecificDateDayValue && DateTime.DaysInMonth(dt.Year, dt.Month) > dt.Day)
            {
                // The Specified day is greater than the number of days in the month.
                if (this.regenerateOnSpecificDateDayValue > DateTime.DaysInMonth(dt.Year, dt.Month))
                    dt = new DateTime(dt.Year, dt.Month, DateTime.DaysInMonth(dt.Year, dt.Month));
                else
                    // The specified date is less than number of days in month.
                    dt = new DateTime(dt.Year, dt.Month, this.regenerateOnSpecificDateDayValue);
            }
            return dt;
        }

        void RunCustomDayNextDayValue(DateTime dt, RecurrenceValues values)
        {
            do
            {
                dt = GetCustomDate(dt);
                // If the date returned is less than the start date
                // then do it again to increment past the start date
                if (dt < base.StartDate)
                {
                    dt = dt.AddMonths(1);
                    dt = GetCustomDate(dt);
                }
                values.AddDateValue(dt, AdjustmentValue);
                if (values.Values[values.Values.Count - 1] > nextDateValue)
                    break;

                dt = dt.AddMonths(RegenEveryXMonths);

            } while (dt <= nextDateValue.AddMonths(RegenEveryXMonths + 1)); // Ensure the last date if greater than what's needed for the next date in the cycle

        }

        DateTime RunCustomDayEveryXMonthValue(DateTime dt, RecurrenceValues values)
        {
            dt = GetCustomDate(dt);
            // If the date returned is less than the start date
            // then do it again to increment past the start date
            if (dt < base.StartDate)
            {
                dt = dt.AddMonths(1);
                dt = GetCustomDate(dt);
            }
            values.AddDateValue(dt, AdjustmentValue);
            dt = dt.AddMonths(RegenEveryXMonths);
            return dt;
        }
        /// <summary>
        /// Get recurring dates from custom date such as First Sunday of July.
        /// </summary>
        /// <returns></returns>
        RecurrenceValues GetCustomDayOfMonthDates()
        {
            if (this.SpecificDatePartOne == null || this.SpecificDatePartTwo ==null)
            {
                return null;
            }

            RecurrenceValues values = new RecurrenceValues();
            DateTime dt = base.StartDate;

            // Get Next Date value only
            if (getNextDateValue)
            {
                RunCustomDayNextDayValue(dt, values);
            }
            else
            {
                switch (base.TypeOfEndDate)
                {

                    case EndDateType.NumberOfOccurrences:
                        for (int i = 0; i < base.NumberOfOccurrences; i++)
                        {
                            dt = RunCustomDayEveryXMonthValue(dt, values);
                        }
                        break;

                    case EndDateType.SpecificDate:
                        do
                        {
                            dt = RunCustomDayEveryXMonthValue(dt, values);
                        } while (dt <= base.EndDate);
                        break;

                    default:
                        break;
                }
            }
            return values;

        }

        /// <summary>
        /// Get the custom value from the 1st, 2nd, and 3rd custom date parts
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        DateTime GetCustomDate(DateTime currentDate)
        {
            int year = currentDate.Year;
            DateTime dt = new DateTime(year, currentDate.Month, 1, currentDate.Hour, currentDate.Minute, currentDate.Second);
            int day = 1;
            int firstPart = (int)SpecificDatePartOne + 1;
            int daysOfMonth = DateTime.DaysInMonth(year, dt.Month);

            switch (SpecificDatePartTwo)
            {
                case MonthlySpecificWeekday.Day:
                    // If only getting the Last day of the month
                    if (SpecificDatePartOne == MonthlySpecificDay.Last)
                        return new DateTime(year, dt.Month, DateTime.DaysInMonth(year, dt.Month), currentDate.Hour, currentDate.Minute, currentDate.Second);
                    else
                        // Get a specific day of the month such as First, Second, Third, Fourth
                        return new DateTime(year, dt.Month, firstPart, currentDate.Hour, currentDate.Minute, currentDate.Second);

                case MonthlySpecificWeekday.Weekday:
                    return GetCustomWeekday(dt, day, daysOfMonth, firstPart);                    

                case MonthlySpecificWeekday.WeekendDay:
                    return GetCustomWeekendDay(dt, day, daysOfMonth, firstPart);

                case MonthlySpecificWeekday.Monday:
                    return GetCustomWeekday(dt, DayOfWeek.Monday, daysOfMonth, firstPart);

                case MonthlySpecificWeekday.Tuesday:
                    return GetCustomWeekday(dt, DayOfWeek.Tuesday, daysOfMonth, firstPart);                    

                case MonthlySpecificWeekday.Wednesday:
                    return GetCustomWeekday(dt, DayOfWeek.Wednesday, daysOfMonth, firstPart);                    

                case MonthlySpecificWeekday.Thursday:
                    return GetCustomWeekday(dt, DayOfWeek.Thursday, daysOfMonth, firstPart);                    

                case MonthlySpecificWeekday.Friday:
                    return GetCustomWeekday(dt, DayOfWeek.Friday, daysOfMonth, firstPart);                    

                case MonthlySpecificWeekday.Saturday:
                    return GetCustomWeekday(dt, DayOfWeek.Saturday, daysOfMonth, firstPart);

                case MonthlySpecificWeekday.Sunday:
                    return GetCustomWeekday(dt, DayOfWeek.Sunday, daysOfMonth, firstPart);                    
            }
            return dt;
        }

        DateTime GetCustomWeekday(DateTime dt, int day, int daysOfMonth, int firstPart)
        {
            int weekDayCount = 0;
            DateTime lastWeekday = dt;
            do
            {
                // Check for anything other than Saturday and Sunday
                if (dt.DayOfWeek != DayOfWeek.Saturday && dt.DayOfWeek != DayOfWeek.Sunday)
                {
                    // Get a specific Weekday of the Month
                    if (SpecificDatePartOne != MonthlySpecificDay.Last)
                    {
                        // Add up the weekday count
                        weekDayCount++;
                        // If the current weekday count matches then exit
                        if (weekDayCount == firstPart)
                            break;
                    }
                    else
                    {
                        // Get the last weekday of the month
                        lastWeekday = dt;
                    }
                }
                dt = dt.AddDays(1);
                day++;
            } while (day <= daysOfMonth);

            // If getting the last weekday of the month then 
            // set the returning value to the last weekday found.
            if (SpecificDatePartOne == MonthlySpecificDay.Last)
            {
                dt = lastWeekday;
            }
            return dt;
        }
        DateTime GetCustomWeekendDay(DateTime dt, int day, int daysOfMonth, int firstPart)
        {
            int weekendDayCount = 0;
            DateTime lastWeekendday = dt;
            do
            {
                // Check for anything other than Saturday and Sunday
                if (dt.DayOfWeek == DayOfWeek.Saturday || dt.DayOfWeek == DayOfWeek.Sunday)
                {
                    // Get a specific Weekday of the Month
                    if (SpecificDatePartOne != MonthlySpecificDay.Last)
                    {
                        // Add up the weekday count
                        weekendDayCount++;
                        // If the current weekday count matches then exit
                        if (weekendDayCount == firstPart)
                            break;
                    }
                    else
                    {
                        // Get the last weekday of the month
                        lastWeekendday = dt;
                    }
                }
                dt = dt.AddDays(1);
                day++;
            } while (day <= daysOfMonth);

            // If getting the last weekday of the month then 
            // set the returning value to the last weekday found.
            if (SpecificDatePartOne == MonthlySpecificDay.Last)
            {
                dt = lastWeekendday;
            }
            return dt;
        }
        DateTime GetCustomWeekday(DateTime startDate, DayOfWeek weekDay, int daysOfMonth, int firstDatePart)
        {
            int day = 1;
            int dayCount = 0;
            DateTime lastDOW = startDate;
            DateTime returnDate = startDate;
            do
            {
                // Check for day of the week
                if (returnDate.DayOfWeek == weekDay)
                {
                    // Get a specific Weekday of the Month
                    if (SpecificDatePartOne != MonthlySpecificDay.Last)
                    {
                        // Add up the days found count
                        dayCount++;
                        // If the current weekday count matches then exit
                        if (dayCount == firstDatePart)
                        {
                            break;
                        }
                    }
                    else
                    {
                        // Get the current date value
                        lastDOW = returnDate;
                    }
                }
                returnDate = returnDate.AddDays(1);
                day++;
            } while (day <= daysOfMonth);

            // If getting the last weekday of the month then 
            // set the returning value to the last weekday found.
            if (SpecificDatePartOne == MonthlySpecificDay.Last)
                returnDate = lastDOW;

            return returnDate;
        }
        #endregion //Private Procedures

        public static MonthlyRecurrenceSettings GetRecurrenceSettings(ScheduleInfo info)
        {

            MonthlyRecurrenceSettings settings = null;

            // Determine the Constructor by the type of End Date.
            // All constructors start with a Start date at a minimum.
            switch (info.EndDateType)
            {
                case EndDateType.NumberOfOccurrences:
                    settings = new MonthlyRecurrenceSettings(info.StartDate, info.NumberOfOccurrences);
                    break;

                case EndDateType.SpecificDate:
                    settings = new MonthlyRecurrenceSettings(info.StartDate, info.EndDate.Value);
                    break;

                case null:
                case EndDateType.NoEndDate:
                    settings = new MonthlyRecurrenceSettings(info.StartDate);
                    break;
            }
            if (settings != null)
            {
                settings.AdjustmentValue = info.AdjustmentValue;
            }

            // Determine the Type of dates to get, specific, custom, etc.
            switch (info.MonthlyRecurType)
            {
                case MonthlyRecurType.OnSpecificDayOfMonth:
                    settings?.SetValues(info.MonthlyRecurOnSpecificDateDayValue, info.MonthlyRecurEveryXMonths);
                    break;

                case MonthlyRecurType.OnCustomDateFormat:
                    settings?.SetValues(info.MonthlySpecificDatePartOne, info.MonthlySpecificDatePartTwo, info.MonthlyRecurEveryXMonths);
                    break;

                case MonthlyRecurType.AfterOccurrenceCompleted:

                    break;
            }

            return settings;
        }

        #region Public GetValues
        /// <summary>
        /// Get Custom dates such as Last Saturday of the month with option as to the increment of every x-months.
        /// </summary>
        /// <param name="customDatePartOne">Corresponds to Part of month such as First, Last.</param>
        /// <param name="customDatePartTwo">Corresponds to day of the week to get such as Tuesday or Weekend Day.</param>
        /// <param name="regenEveryXMonths">How many months to skip, such as 2 for every other month.</param>
        /// <returns></returns>
        public RecurrenceValues GetValues(MonthlySpecificDay customDatePartOne, MonthlySpecificWeekday customDatePartTwo, int regenEveryXMonths)
        {
            this.regenEveryXMonths = regenEveryXMonths;
            specificDatePartOne = customDatePartOne;
            specificDatePartTwo = customDatePartTwo;
            recurType = MonthlyRecurType.OnCustomDateFormat;
            return GetValues();
        }

        /// <summary>
        /// Get values for a specific day of the month. Eg. Every 23rd day. With option to get every x-month.
        /// </summary>
        /// <param name="dayOfMonthToRegen">Day of month you want to set as the value to get from month to month.</param>
        /// <param name="regenEveryXMonths">How many months to skip, such as 2 for every other month.</param>
        /// <returns></returns>
        public RecurrenceValues GetValues(int dayOfMonthToRegen, int regenEveryXMonths)
        {
            this.regenEveryXMonths = regenEveryXMonths;
            regenerateOnSpecificDateDayValue = dayOfMonthToRegen;
            recurType = MonthlyRecurType.OnSpecificDayOfMonth;
            return GetValues();
        }
        #endregion //Public GetValues

        #region Public Fields
        /// <summary>
        /// What is the first part to the Custom date such as First, Last.
        /// </summary>
        public MonthlySpecificDay? SpecificDatePartOne
        {
            get
            {
                return specificDatePartOne;
            }
        }

        /// <summary>
        /// What is the second part to the Custom date such as which weekday, weekend day, etc.
        /// </summary>
        public MonthlySpecificWeekday? SpecificDatePartTwo
        {
            get
            {
                return specificDatePartTwo;
            }
        }

        /// <summary>
        /// What is the regeneration type such as Specific day of month, custom date, etc.
        /// </summary>
        public MonthlyRecurType? RecurType
        {
            get
            {
                return recurType;
            }
        }

        /// <summary>
        ///  Day of month to regenerate when RegenType = specific day of month.
        /// </summary>
        public int RegenerateOnSpecificDateDayValue
        {
            get
            {
                return regenerateOnSpecificDateDayValue;
            }
        }

        /// <summary>
        /// Used to adjust the date plus/minus x-days
        /// </summary>
        public int AdjustmentValue { get; set; }

        /// <summary>
        /// What is the interval to generate dates. This is used to skip months in the cycle.
        /// </summary>
        public int RegenEveryXMonths
        {
            get
            {
                return regenEveryXMonths;
            }
        }
        #endregion //Public Fields

        #region Internal GetValues
        /// <summary>
        ///      Final overloaded function that gets the Recurrence Values. 
        ///      This is called from the RecurrenceHelper staic methods only.
        /// </summary>
        /// <returns>
        ///     A AT.Logic.Reporting.Schedule.RecurrenceValues value...
        /// </returns>
        internal override RecurrenceValues GetValues()
        {
            return GetRecurrenceValues();
        }
        internal override RecurrenceValues GetValues(DateTime startDate, DateTime endDate)
        {
            base.StartDate = startDate;
            base.EndDate = endDate;
            // Change the end type to End Date as this original series info
            // may have been set to number of occurrences.
            base.endDateType = EndDateType.SpecificDate;
            return GetRecurrenceValues();
        }

        internal override RecurrenceValues GetValues(DateTime startDate, int numberOfOccurrences)
        {
            base.NumberOfOccurrences = numberOfOccurrences;
            base.StartDate = startDate;
            // Change the end type to number of occurrences. 
            // This must be set because the original starting Series Info may
            // be set to have an End Date type.
            base.endDateType = EndDateType.NumberOfOccurrences;
            return GetRecurrenceValues();
        }

        RecurrenceValues GetRecurrenceValues()
        {
            RecurrenceValues values = null;
            switch (RecurType)
            {
                case MonthlyRecurType.OnSpecificDayOfMonth:
                    values = GetSpecificDayOfMonthDates();
                    break;

                case MonthlyRecurType.OnCustomDateFormat:
                    values = GetCustomDayOfMonthDates();
                    break;

                case MonthlyRecurType.AfterOccurrenceCompleted:

                    break;

            }
            if (values?.Values.Count > 0)
            {
                values.SetStartDate(values.Values[0]);

                // Get the end date if not open-ended
                if (base.TypeOfEndDate != EndDateType.NoEndDate)
                {
                    values.SetEndDate(values.Values[values.Values.Count - 1]);
                }
            }


            return values;
        }
        #endregion //Internal GetValues

        #region Internal Procedures


        void SetValues(MonthlySpecificDay customDatePartOne, MonthlySpecificWeekday customDatePartTwo, int regenEveryXMonths)
        {
            this.regenEveryXMonths = regenEveryXMonths;
            specificDatePartOne = customDatePartOne;
            specificDatePartTwo = customDatePartTwo;
            recurType = MonthlyRecurType.OnCustomDateFormat;
        }

        void SetValues(int dayOfMonthToRegen, int regenEveryXMonths)
        {
            this.regenEveryXMonths = regenEveryXMonths;
            regenerateOnSpecificDateDayValue = dayOfMonthToRegen;
            recurType = MonthlyRecurType.OnSpecificDayOfMonth;
        }


        internal override DateTime GetNextDate(DateTime currentDate)
        {
            getNextDateValue = true;
            nextDateValue = currentDate;
            // Now get the values and return the last date found.
            RecurrenceValues values = GetValues();
            return values.LastDate;
        }



        #endregion //Internal Procedures




    }
}
