using AT.Logic.Reporting.Enums;
using System;

namespace AT.Logic.Reporting.Schedule
{
    
    public abstract class RecurrenceSettings
    {
       
        protected RecurrenceSettings(DateTime startDate, DateTime? endDate = null)
        {
            this.StartDate = startDate;
            this.endDate = endDate;
            if (!endDate.HasValue)
            {
                endDateType = EndDateType.NoEndDate;
            }
            else
            {
                endDateType = EndDateType.SpecificDate;
            }
            RecurrenceInterval = 1;
        }
        protected RecurrenceSettings(DateTime startDate, int numberOfOccurrences)
        {
            this.StartDate = startDate;
            this.NumberOfOccurrences = numberOfOccurrences;
            endDateType = EndDateType.NumberOfOccurrences;
            RecurrenceInterval = 1;
        }

        DateTime? endDate; // Nullable date because there may or may not be an end date.
       
        protected EndDateType  endDateType;

        internal abstract DateTime GetNextDate(DateTime currentDate);
        internal abstract RecurrenceValues GetValues();
        internal abstract RecurrenceValues GetValues(DateTime startDate, int numberOfOccurrences);
        internal abstract RecurrenceValues GetValues(DateTime startDate, DateTime endDate);

        /// <summary>
        ///     Get/Set the type of End Date. Occurrences, End Date, or no end date at all.
        /// </summary>
        /// <value>
        ///     <para>
        ///         EndDateType enumeration.
        ///     </para>
        /// </value>
        /// <remarks>
        ///     
        /// </remarks>
        public EndDateType TypeOfEndDate { get; set; }
        

        /// <summary>
        /// Regenerate the Occurrence x-amount of days, weeks, etc. 
        /// after the current item is completed.
        /// </summary>
        public int RegenerationAfterCompletedInterval { get; set; }
       

        public int NumberOfOccurrences { get; set; }
        

        /// <summary>
        /// Readonly bool if this recurrence instance
        /// has an End date or note. If it does not then
        /// it means this instance has no ending date and should
        /// only create one IRecurrenceItem object.
        /// </summary>
        public bool HasEndDate
        {
            get
            {
                return endDate.HasValue;
            }
        }


        /// <summary>
        ///     End Date for the recurrence values.
        /// </summary>
        /// <value>
        ///     <para>
        ///         DateTime value.
        ///     </para>
        /// </value>
        /// <remarks>
        ///     
        /// </remarks>
        public DateTime? EndDate
        {
            get
            {
                if (endDate.HasValue)
                    return endDate.Value;
                else
                    return null;
            }
            set
            {
                endDate = value;
            }
        }

        /// <summary>
        ///     Start Date of the rucurence Values.
        /// </summary>
        /// <value>
        ///     <para>
        ///         DateTime value.
        ///     </para>
        /// </value>
        /// <remarks>
        ///     
        /// </remarks>
        public DateTime StartDate { get; set; }
       

        /// <summary>
        /// This is the "Recurr every x-amount of Days, Weeks.
        /// </summary>
        public int RecurrenceInterval { get; set; }
        

    }
}
