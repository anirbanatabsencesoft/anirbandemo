using System;
using AT.Logic.Reporting.Enums;

namespace AT.Logic.Reporting.Schedule
{
   

    public class YearlyRecurrenceSettings : RecurrenceSettings
    {
        #region Constructors
      
        /// <summary>
        /// Get dates by Start and End date boundries.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        public YearlyRecurrenceSettings(DateTime startDate, DateTime? endDate=null) : base(startDate, endDate) { }
        /// <summary>
        /// Get dates by Start date and number of occurrences.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="numberOfOccurrences"></param>
        public YearlyRecurrenceSettings(DateTime startDate, int numberOfOccurrences) : base(startDate, numberOfOccurrences) { }
        #endregion
        
        #region Private Fields
        int regenerateOnSpecificDateDayValue;
        int regenerateOnSpecificDateMonthValue;        
        bool getNextDateValue;
        DateTime nextDateValue;
        YearlyRecurType? regenType;
        MonthlySpecificDay? specificDatePartOne;
        MonthlySpecificWeekday? specificDatePartTwo;
        YearlySpecificMonth? specificDatePartThree;
        #endregion

        #region Public GetValues
        /// <summary>
        /// Get dates for a specific day and month of the year.
        /// </summary>
        /// <param name="specificDateDayValue">Day of the month.</param>
        /// <param name="specificDateMonthValue">Month of the year.</param>
        /// <returns></returns>
        public RecurrenceValues GetValues(int specificDateDayValue, int specificDateMonthValue)
        {
            regenerateOnSpecificDateDayValue = specificDateDayValue;
            regenerateOnSpecificDateMonthValue = specificDateMonthValue;
            regenType = YearlyRecurType.OnSpecificDayOfYear;
            return GetValues();
        }

        /// <summary>
        /// Get dates for a custom formatted date such as First weekend day of July.
        /// </summary>
        /// <param name="customDateFirstPart"></param>
        /// <param name="customDateSecondPart"></param>
        /// <param name="customDateThirdPart"></param>
        /// <returns></returns>
        public RecurrenceValues GetValues(MonthlySpecificDay customDateFirstPart,MonthlySpecificWeekday customDateSecondPart, YearlySpecificMonth customDateThirdPart)
        {
            specificDatePartOne = customDateFirstPart;
            specificDatePartTwo = customDateSecondPart;
            specificDatePartThree = customDateThirdPart;
            regenType = YearlyRecurType.OnCustomDateFormat;
            return GetValues();
        }
#endregion //Public GetValues

        #region Internal GetValues
        /// <summary>
        ///     Final overloaded function that gets the Recurrence Values. 
        ///     This is called from the RecurrenceHelper staic methods only.
        /// </summary>
        /// <returns>
        ///     A AT.Logic.Reporting.Schedule.RecurrenceValues value...
        /// </returns>
        internal override RecurrenceValues GetValues()
        {
            return GetRecurrenceValues();
        }
        
        internal override RecurrenceValues GetValues(DateTime startDate, DateTime endDate)
        {
            base.StartDate = startDate;
            base.EndDate = endDate;
            // Change the end type to End Date as this original series info
            // may have been set to number of occurrences.
            base.endDateType = EndDateType.SpecificDate;
            return GetRecurrenceValues();
        }
        internal override RecurrenceValues GetValues(DateTime startDate, int numberOfOccurrences)
        {
            base.NumberOfOccurrences = numberOfOccurrences;
            base.StartDate = startDate;
            // Change the end type to number of occurrences. 
            // This must be set because the original starting Series Info may
            // be set to have an End Date type.
            base.endDateType = EndDateType.NumberOfOccurrences;
            return GetRecurrenceValues();
        }
        #endregion //Internal GetValues

        public static YearlyRecurrenceSettings GetRecurrenceSettings(ScheduleInfo info)
        {
            // Get the Recurrence Info object. This makes it easy to work with existing series of date patterns.

            YearlyRecurrenceSettings settings = null;

            // Determine the Constructor by the type of End Date.
            // All constructors start with a Start date at a minimum.
            switch (info.EndDateType)
            {
                case EndDateType.NumberOfOccurrences:
                    settings = new YearlyRecurrenceSettings(info.StartDate, info.NumberOfOccurrences);
                    break;

                case EndDateType.SpecificDate:
                    settings = new YearlyRecurrenceSettings(info.StartDate, info.EndDate.Value);
                    break;
                
                default:
                    settings = new YearlyRecurrenceSettings(info.StartDate);
                    break;
            }

            settings.AdjustmentValue = info.AdjustmentValue;

            // Determine the Type of dates to get, specific, custom, etc.
            switch (info.YearlyRecurType)
            {
                case YearlyRecurType.OnSpecificDayOfYear:
                    settings.SetValues(info.SpecificDateDayValue, info.SpecificDateMonthValue);
                    break;

                case YearlyRecurType.OnCustomDateFormat:
                    settings.SetValues(info.YearlySpecificDatePartOne, info.YearlySpecificDatePartTwo, info.YearlySpecificDatePartThree);
                    break;

                case YearlyRecurType.AfterOccurrenceCompleted:

                    break;
            }
            return settings;

        }
       
        #region Internal Procedures
        

        void SetValues(int specificDateDayValue, int specificDateMonthValue)
        {
            regenerateOnSpecificDateDayValue = specificDateDayValue;
            regenerateOnSpecificDateMonthValue = specificDateMonthValue;
            regenType = YearlyRecurType.OnSpecificDayOfYear;
        }

        void  SetValues(MonthlySpecificDay customDateFirstPart, MonthlySpecificWeekday customDateSecondPart, YearlySpecificMonth customDateThirdPart)
        {
            specificDatePartOne = customDateFirstPart;
            specificDatePartTwo = customDateSecondPart;
            specificDatePartThree = customDateThirdPart;
            regenType = YearlyRecurType.OnCustomDateFormat;
        }

        internal override DateTime GetNextDate(DateTime currentDate)
        {
            getNextDateValue = true;
            nextDateValue = currentDate;
            // Now get the values and return the last date found.
            RecurrenceValues values = GetValues();
            return values.LastDate;
        }


        #endregion //Internal Procedures

        #region Private Procedures
        RecurrenceValues GetRecurrenceValues()
        {
            RecurrenceValues values = null;
            switch (RecurType)
            {
                case YearlyRecurType.OnSpecificDayOfYear:
                    values = GetSpecificDayOfYearDates();
                    break;

                case YearlyRecurType.OnCustomDateFormat:
                    values = GetCustomDayOfYearDates();
                    break;

                case YearlyRecurType.AfterOccurrenceCompleted:

                    break;

            }
            if (values?.Values.Count > 0)
            {
                values.SetStartDate(values.Values[0]);

                // Get the end date if not open-ended
                if (base.TypeOfEndDate != EndDateType.NoEndDate)
                {
                    values.SetEndDate(values.Values[values.Values.Count - 1]);
                }
            }
            return values;
        }
        void RunSecificDayOfYearNextDateValue(DateTime dt, RecurrenceValues values)
        {
            do
            {
                values.AddDateValue(dt, AdjustmentValue);
                if (values.Values[values.Values.Count - 1] >= nextDateValue)
                    break;
                dt = dt.AddYears(1);
                dt = GetCorrectedDate(dt);
            } while (dt <= nextDateValue.AddYears(1)); // Ensure the last date if greater than what's needed for the next date in the cycle
          
        }

        /// <summary>
        /// Get recurring dates from a specific constant date such as 27 July.
        /// </summary>
        /// <returns></returns>
        RecurrenceValues GetSpecificDayOfYearDates()
        {
            RecurrenceValues values = new RecurrenceValues();
            DateTime dt = base.StartDate;
            int dayValue = regenerateOnSpecificDateDayValue;
            int daysOfMonth = DateTime.DaysInMonth(dt.Year, regenerateOnSpecificDateMonthValue);
            // Get the max days of the month and make sure it's not 
            // less than the specified day value trying to be set.
            if (daysOfMonth < regenerateOnSpecificDateDayValue)
                dayValue = daysOfMonth;

            // Determine if start date is greater than the Day and Month values
            // for a specific date.
            DateTime newDate = new DateTime(dt.Year, regenerateOnSpecificDateMonthValue, dayValue, dt.Hour,dt.Minute,dt.Second);
            // Is the specific date before the start date, if so 
            // then make the specific date next year.
            if (newDate < dt)
            {
                dt = newDate.AddYears(1);
            }
            else
            {
                dt = newDate;
            }

            if (getNextDateValue)
            {
                RunSecificDayOfYearNextDateValue(dt, values);
            }
            else
            {
                switch (base.TypeOfEndDate)
                {                    

                    case EndDateType.NumberOfOccurrences:
                        for (int i = 0; i < base.NumberOfOccurrences; i++)
                        {
                            values.AddDateValue(GetCorrectedDate(dt.AddYears(i)), AdjustmentValue);
                        }
                        break;

                    case EndDateType.SpecificDate:
                        do
                        {
                            values.AddDateValue(dt, AdjustmentValue);
                            dt = dt.AddYears(1);
                            dt = GetCorrectedDate(dt);
                        } while (dt <= base.EndDate);
                        break;
                    case EndDateType.NoEndDate:
                    default:
                        break;
                }
            }

            return values;
        }

        /// <summary>
        /// Correct an input date to be equal to or less than the specified day value.
        /// </summary>
        /// <param name="input">Date to check to ensure it matches the specified day value or the max number of days for that month, whichever comes first.</param>
        /// <returns>DateTime</returns>
        DateTime GetCorrectedDate(DateTime input)
        {
            DateTime dt = input;
            // Ensure the day value hasn't changed.
            // This will occurr if the month is Feb. All
            // dates after that will have the same day.
            if (dt.Day < this.regenerateOnSpecificDateDayValue && DateTime.DaysInMonth(dt.Year, dt.Month) > dt.Day)
            {
                // The Specified day is greater than the number of days in the month.
                if (this.regenerateOnSpecificDateDayValue > DateTime.DaysInMonth(dt.Year, dt.Month))
                {
                    dt = new DateTime(dt.Year, dt.Month, DateTime.DaysInMonth(dt.Year, dt.Month));
                }
                else
                {
                    // The specified date is less than number of days in month.
                    dt = new DateTime(dt.Year, dt.Month, this.regenerateOnSpecificDateDayValue);
                }
            }
            return dt;
        }

        /// <summary>
        /// Get recurring dates from custom date such as First Sunday of July.
        /// </summary>
        /// <returns></returns>

        DateTime RunAddDateValue(ref int year, RecurrenceValues values)
        {
            DateTime dt = GetCustomDate(year);
            // If the date returned is less than the start date
            // then do it again to increment past the start date
            if (dt < base.StartDate)
            {
                year++;
                dt = GetCustomDate(year);
            }
            year++;
            values.AddDateValue(dt, AdjustmentValue);
            return dt;
        }
        void RunNextDateValue(ref int year, RecurrenceValues values)
        {
            DateTime dt;
            do
            {
                dt=RunAddDateValue(ref year, values);
                if (values.Values[values.Values.Count - 1] > nextDateValue)
                    break;
            } while (dt <= nextDateValue.AddYears(1)); // Ensure the last date if greater than what's needed for the next date in the cycle
        }
        RecurrenceValues GetCustomDayOfYearDates()
        {
            RecurrenceValues values = new RecurrenceValues();
           
          
            DateTime dt = base.StartDate;
            int year = dt.Year;

            if (getNextDateValue)
            {
                RunNextDateValue(ref year, values);
            }
            else
            {
                switch (base.TypeOfEndDate)
                {
                    
                    case EndDateType.NumberOfOccurrences:
                        for (int i = 0; i < base.NumberOfOccurrences; i++)
                        {
                            RunAddDateValue(ref year, values);
                        }
                        break;

                    case EndDateType.SpecificDate:
                        do
                        {
                            dt=RunAddDateValue(ref year, values);
                        } while (dt <= base.EndDate);
                        break;                    
                }
            }
            return values;

        }

        /// <summary>
        /// Get the custom value from the 1st, 2nd, and 3rd custom date parts
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        DateTime GetCustomDate(int year)
        {
            DateTime dt = new DateTime(year, (int)SpecificDatePartThree, 1, StartDate.Hour, StartDate.Minute, StartDate.Second);
            int day = 1;
            int firstPart = (int)SpecificDatePartOne + 1;
            int daysOfMonth = DateTime.DaysInMonth(year, dt.Month);

            switch (SpecificDatePartTwo)
            {
                case MonthlySpecificWeekday.Day:
                    // If only getting the Last day of the month
                    if (SpecificDatePartOne == MonthlySpecificDay.Last)
                        dt = new DateTime(year, dt.Month, DateTime.DaysInMonth(year, dt.Month), StartDate.Hour, StartDate.Minute, StartDate.Second);
                    else
                        // Get a specific day of the month such as First, Second, Third, Fourth
                        dt = new DateTime(year, dt.Month, firstPart, StartDate.Hour, StartDate.Minute, StartDate.Second);
                    
                    break;

                case MonthlySpecificWeekday.Weekday:

                    dt = GetCustomWeekday(dt, day, daysOfMonth, firstPart);
                    break;

                case MonthlySpecificWeekday.WeekendDay:

                    dt = GetCustomWeekendDay(dt, day, daysOfMonth, firstPart);

                    break;
                    
                case MonthlySpecificWeekday.Monday:
                    dt = GetCustomWeekday(dt, DayOfWeek.Monday, daysOfMonth, firstPart);
                    break;

                case MonthlySpecificWeekday.Tuesday:
                    dt = GetCustomWeekday(dt, DayOfWeek.Tuesday, daysOfMonth, firstPart);
                    break;

                case MonthlySpecificWeekday.Wednesday:
                    dt = GetCustomWeekday(dt, DayOfWeek.Wednesday, daysOfMonth, firstPart);
                    break;

                case MonthlySpecificWeekday.Thursday:
                    dt = GetCustomWeekday(dt, DayOfWeek.Thursday, daysOfMonth, firstPart);
                    break;
                    
                case MonthlySpecificWeekday.Friday:
                    dt = GetCustomWeekday(dt, DayOfWeek.Friday, daysOfMonth, firstPart);
                    break;

                case MonthlySpecificWeekday.Saturday:
                    dt = GetCustomWeekday(dt, DayOfWeek.Saturday, daysOfMonth, firstPart);
                    break;

                case MonthlySpecificWeekday.Sunday:
                    dt = GetCustomWeekday(dt, DayOfWeek.Sunday, daysOfMonth, firstPart);
                    break;
            }
            return dt;
        }

        DateTime GetCustomWeekday(DateTime startDate, DayOfWeek weekDay, int daysOfMonth, int firstDatePart)
        {
            int day = 1;
            int dayCount = 0;
            DateTime lastDOW = startDate;
            DateTime returnDate = startDate;
            do
            {
                // Check for day of the week
                if (returnDate.DayOfWeek == weekDay)
                {
                    // Get a specific Weekday of the Month
                    if (SpecificDatePartOne != MonthlySpecificDay.Last)
                    {
                        // Add up the days found count
                        dayCount++;
                        // If the current weekday count matches then exit
                        if (dayCount == firstDatePart)
                        {
                            break;
                        }
                    }
                    else
                    {
                        // Get the current date value
                        lastDOW = returnDate;
                    }
                }
                returnDate = returnDate.AddDays(1);
                day++;
            } while (day <= daysOfMonth);

            // If getting the last weekday of the month then 
            // set the returning value to the last weekday found.
            if (SpecificDatePartOne == MonthlySpecificDay.Last)
                returnDate = lastDOW;

            return returnDate;
        }
      
        DateTime GetCustomWeekday(DateTime dt, int day, int daysOfMonth, int firstPart)
        {
            int weekDayCount = 0;
            DateTime lastWeekday = dt;
            do
            {
                // Check for anything other than Saturday and Sunday
                if (dt.DayOfWeek != DayOfWeek.Saturday && dt.DayOfWeek != DayOfWeek.Sunday)
                {
                    // Get a specific Weekday of the Month
                    if (SpecificDatePartOne != MonthlySpecificDay.Last)
                    {
                        // Add up the weekday count
                        weekDayCount++;
                        // If the current weekday count matches then exit
                        if (weekDayCount == firstPart)
                            break;
                    }
                    else
                    {
                        // Get the last weekday of the month
                        lastWeekday = dt;
                    }
                }
                dt = dt.AddDays(1);
                day++;
            } while (day <= daysOfMonth);

            // If getting the last weekday of the month then 
            // set the returning value to the last weekday found.
            if (SpecificDatePartOne == MonthlySpecificDay.Last)
                dt = lastWeekday;
            return dt;
        }

        DateTime GetCustomWeekendDay(DateTime dt, int day, int daysOfMonth, int firstPart)
        {
            int weekendDayCount = 0;
            DateTime lastWeekendday = dt;
            do
            {
                // Check for anything other than Saturday and Sunday
                if (dt.DayOfWeek == DayOfWeek.Saturday || dt.DayOfWeek == DayOfWeek.Sunday)
                {
                    // Get a specific Weekday of the Month
                    if (SpecificDatePartOne != MonthlySpecificDay.Last)
                    {
                        // Add up the weekday count
                        weekendDayCount++;
                        // If the current weekday count matches then exit
                        if (weekendDayCount == firstPart)
                            break;
                    }
                    else
                    {
                        // Get the last weekday of the month
                        lastWeekendday = dt;
                    }
                }
                dt = dt.AddDays(1);
                day++;
            } while (day <= daysOfMonth);

            // If getting the last weekday of the month then 
            // set the returning value to the last weekday found.
            if (SpecificDatePartOne == MonthlySpecificDay.Last)
            {
                dt = lastWeekendday;
            }
            return dt;
        }
#endregion //Private Procedures

        #region Public Fields
        /// <summary>
        /// Part of the Custom date that equates to the month of year.
        /// </summary>
        public YearlySpecificMonth? SpecificDatePartThree
        {
            get
            {
                return specificDatePartThree;
            }
        }

        /// <summary>
        /// Part of the Custom date that is the day part of the month such as weekend day, Tuesday, Wednesday, weekday, etc.
        /// </summary>
        public MonthlySpecificWeekday? SpecificDatePartTwo
        {
            get
            {
                return specificDatePartTwo;
            }
        }

        /// <summary>
        /// Part of the Custom date that is the part to get such as First, Second, Last, etc.
        /// </summary>
        public MonthlySpecificDay? SpecificDatePartOne
        {
            get
            {
                return specificDatePartOne;
            }
        }

        /// <summary>
        /// Regeneration type such as by Specific day of the year, Custom date, etc.
        /// </summary>
        public YearlyRecurType? RecurType
        {
            get
            {
                return regenType;
            }
        }

        /// <summary>
        /// What day of the month do you want to regenerate dates when by a specific day of the year.
        /// </summary>
        public int RegenerateOnSpecificDateDayValue
        {
            get
            {
                return regenerateOnSpecificDateDayValue;
            }
        }

        /// <summary>
        /// What month of the year do you want to regenerate dates when by a specific day of the year.
        /// </summary>
        public int RegenerateOnSpecificDateMonthValue
        {
            get
            {
                return regenerateOnSpecificDateMonthValue;
            }
        }

        /// <summary>
        /// Used to adjust the date plus/minus x-days
        /// </summary>
        public int AdjustmentValue { get; set; }
       
#endregion //Public Fields
        
    }
}
