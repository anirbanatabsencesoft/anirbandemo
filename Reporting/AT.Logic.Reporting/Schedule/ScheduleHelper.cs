﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Schedule
{
    public class ScheduleHelper
    {
        public ScheduleInfo ScheduleInfo { get; set; }
        public ScheduleHelper(ScheduleInfo schInfo)
        {
            ScheduleInfo = schInfo;
        }

        protected RecurrenceSettings GetRecurrenceSettings()
        {
            RecurrenceSettings returnValues = null;

            switch (ScheduleInfo.RecurrenceType)
            {
                case Enums.RecurrenceType.Yearly:   // Yearly
                    returnValues = YearlyRecurrenceSettings.GetRecurrenceSettings(ScheduleInfo);
                    break;

                case Enums.RecurrenceType.Monthly:   // Monthly
                    returnValues = MonthlyRecurrenceSettings.GetRecurrenceSettings(ScheduleInfo);
                    break;

                case Enums.RecurrenceType.Weekly:   // Weekly
                    returnValues = WeeklyRecurrenceSettings.GetRecurrenceSettings(ScheduleInfo);
                    break;

                case Enums.RecurrenceType.Daily:   // Daily
                    returnValues = DailyRecurrenceSettings.GetRecurrenceSettings(ScheduleInfo);
                    break;

            }
            return returnValues;
        }
        public DateTime GetNextDate(System.DateTime currentDate)
        {
            RecurrenceSettings returnValues = GetRecurrenceSettings();
            // Return just the next date. The function "GetNextDate" is an abstract 
            // method overriden in each of the RecurrenceSettings classes.
            if (returnValues == null) //OneTime
            {
                return ScheduleInfo.StartDate;
            }
            else
            {
                return returnValues.GetNextDate(currentDate);
            }
        }

        public DateTime GetFirstRunDate()
        {
            RecurrenceSettings returnValues = GetRecurrenceSettings();
            if (returnValues == null) //OneTime
            {
                return ScheduleInfo.StartDate;
            }
            else
            {
                return returnValues.GetNextDate(ScheduleInfo.StartDate.AddDays(-1));
            }
        }
        public RecurrenceValues GetRecurrenceValues()
        {
            RecurrenceSettings returnValues = GetRecurrenceSettings();
            return returnValues?.GetValues();
        }
    }
}
