﻿using AbsenceSoft.Logic.Common;
using AT.Logic.Reporting.Arguments;
using AT.Logic.Reporting.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using AT.Data.Reporting.PostgreSqlDataAccess.Model;
using AT.Data.Reporting.PostgreSqlDataAccess;
using AT.Logic.Reporting.Enums;
using AT.Data.Authentication.Provider;
using AT.Logic.Authentication;
using System.IO;
using AbsenceSoft.Common.Properties;
using AbsenceSoft.Data.Reporting;
using AT.Common.Rendering.Helpers;
using AbsenceSoft.Data.Enums;
using AT.Logic.Reporting.Schedule;
using Newtonsoft.Json.Linq;
using AT.Logic.Reporting.Base;
using AT.Entities.Reporting.Enums;
using AT.Entities.Reporting.Classes;
using AbsenceSoft.Common.Security;
using AT.Common.Log;
using AbsenceSoft.Data.Security;
using System.Web;
using AT.Data.Reporting.Model;

namespace AT.Logic.Reporting
{
    /// <summary>
    /// This class provides the implementation of Message Processor calling Data access.
    /// </summary>
    public partial class ReportingLogic
    {
        /// <summary>
        /// The method gives call to data to make an entry for the requested report.
        /// </summary>
        /// <param name="args"></param>
        public async Task<ReportStatusResult> AddReportRequestAsync(RequestReportArgs args, long? scheduledReportId = null)
        {
            ReportStatusResult result = new ReportStatusResult();
            try
            {
                //Save the report request and get the requestId
                RequestReportModel model = new RequestReportModel()
                {
                    RequestMessage = JsonConvert.SerializeObject(args),
                    RequestType = Convert.ToString(args.ReportRequestType),
                    Status = RequestStatus.Pending,
                    CreatedOn = DateTime.UtcNow,
                    UserId = AuthenticatedUser.Id,
                    UserKey = SecurityUser.Id,
                    ScheduledReportId = scheduledReportId,
                    ReportName = args.ReportDefinition.Name,
                    CriteriaPlainEnglish = args.ReportDefinition.Criteria?.PlainEnglish,
                    ReportId = args.ReportId
                };
                PostgresDataAccess dataAccess = new PostgresDataAccess();
                var saveResult = await dataAccess.SaveRequestReportAsync(model);
                //Put the  message inside the SQS for processing 
                if (saveResult?.Id.HasValue == true)
                {
                    AddRequestMessage(saveResult.Id.Value);
                    result.Status = RequestStatus.Pending;
                    result.Id = saveResult.Id.Value;
                }
                else
                {
                    result.Status = RequestStatus.Failed;
                    result.Message = "Report Request failed with data access!";
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);

                result.Status = RequestStatus.Failed;
                result.Message = "Report Request Failed! " + ex.Message.ToString();
            }
            return result;
        }

        /// <summary>
        /// The method makes an entry into SQS for processing.
        /// </summary>
        /// <param name="args"></param>
        public string AddRequestMessage(long requestId)
        {
            using (QueueService svc = new QueueService())
            {
                return svc.Add(MessageQueues.AtCommonQueue, MessageType.ReportRequest, requestId);
            }

        }

        /// <summary>
        /// The method posses a item in the SQS queue
        /// executes the request report
        /// save the results files to S3
        /// updates status to the database.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<bool> ProcessReportRequest(QueueItem item)
        {
            if (item?.QueueName != MessageQueues.AtCommonQueue)
            {
                return false;
            }
            // Get our Id from the message body
            long requestId = 0;
            Int64.TryParse(item?.Body, out requestId);
            PostgresDataAccess dataAccess = new PostgresDataAccess();
            RequestReportModel request = null;
            try
            {
                // Get the report requested item item from the database.
                request = await dataAccess.GetRequestReportByIdAsync(requestId);
                if (request == null || request?.Status != RequestStatus.Pending)
                {
                    return false;
                }
                //setting the status of the report request to processing from pending...
                request.Status = RequestStatus.Processing;
                request.ModifiedOn = DateTime.UtcNow;
                await dataAccess.SaveRequestReportAsync(request);

                await SetAuthenticatedUserByKey(request.UserKey);

                RequestReportArgs args = JsonConvert.DeserializeObject<RequestReportArgs>(request.RequestMessage);
                var limit = GetLimit(args);
                
                // The result should be CompletedReportResults object Serialized to Json file.
                CompletedReportResult completedReportResult = null;
                if (args.ReportRequestType == ReportRequestType.Operations)
                {
                    var opResult = await ExecuteReportAsync(ConvertToExecuteReportArgs(args));
                    completedReportResult = ConvertToCompletedReportResult(opResult);
                }
                else if (args.ReportRequestType == ReportRequestType.Adhoc)
                {
                    AdhocExecuteReportArgs adhocExecuteReportArgs = ConvertToAdhocReportArgs(args);
                    AdHocReportResult adHocReportResult = null;
                    if (adhocExecuteReportArgs.ReportDefinition != null)
                    {
                        adHocReportResult = await ExecuteAdhocReportAsync(adhocExecuteReportArgs.ReportDefinition, null, limit, adhocExecuteReportArgs.Offset);
                    }
                    else
                    {
                        adHocReportResult = await ExecuteAdhocReportAsync(adhocExecuteReportArgs.ReportDefinitionId, null, limit, adhocExecuteReportArgs.Offset);
                    }
                    completedReportResult = ConvertToCompletedReportResult(adHocReportResult);
                }
                if (completedReportResult != null && string.IsNullOrWhiteSpace(completedReportResult.Error))
                {
                    //save the result as Json file with-in S3 storage
                    var jsonFileKey = await SaveJsonFile(completedReportResult, args.CustomerId);
                    //Check for Export
                    string pdfFileKey = null;
                    string csvFileKey = null;
                    if (args.ExportFormat == ExportType.PDF)
                    {
                        var dataPdf = JsonConvert.SerializeObject(completedReportResult.JsonData);
                        var convertedJsonForPdf = JsonConvert.DeserializeObject(dataPdf) as JArray;
                        var pdfResult = PdfHelper.ConvertJsonToPdf(convertedJsonForPdf, completedReportResult.Name, completedReportResult.Criteria.PlainEnglish);
                        pdfFileKey = await SaveFile(pdfResult, args.CustomerId, completedReportResult.Name, ExportType.PDF);
                    }
                    else if (args.ExportFormat == ExportType.CSV)
                    {
                        var dataCsv = JsonConvert.SerializeObject(completedReportResult.JsonData);
                        var convertedJsonForCsv = JsonConvert.DeserializeObject(dataCsv) as JArray;
                        var csvResult = CsvHelper.GenerateCSVFromJson(convertedJsonForCsv, completedReportResult.Name, completedReportResult.Criteria.PlainEnglish);
                        csvFileKey = await SaveFile(csvResult, args.CustomerId, completedReportResult.Name, ExportType.CSV);
                    }

                    request.Status = RequestStatus.Completed;
                    request.ResponseMessage = "Success";
                    request.IsCompleted = true;
                    request.ModifiedOn = DateTime.UtcNow;
                    request.ResultMaxJsonFilePath = jsonFileKey;
                    request.ResultPdfFilePath = pdfFileKey;
                    request.ResultCsvFilePath = csvFileKey;
                }
                else
                {
                    request.IsCompleted = false;
                    request.Status = RequestStatus.Failed;
                    request.ResponseMessage = completedReportResult?.Error;
                    request.ModifiedOn = DateTime.UtcNow;
                }
                //update the request with the file key
                await dataAccess.SaveRequestReportAsync(request);
                await RunScheduleDeliveryNotification(request, args);
                await UpdateNextRunDate(request); //update the next run date for the scheduled reports.
                return true;
            }
            catch (Exception ex)
            {
                await OnFailureUpdateRequest(request, ex.GetBaseException().Message);

                Logger.Error(ex);
            }
            return false;
        }

        /// <summary>
        /// The method updates the scheduled report's next run date
        /// called upon successful processing of the request report.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private async Task UpdateNextRunDate(RequestReportModel request)
        {
            if (request.ScheduledReportId.HasValue)
            {
                PostgresDataAccess dataAccess = new PostgresDataAccess();
                var result = await dataAccess.GetScheduledReportAsync(request.ScheduledReportId.Value);
                if (result != null)
                {
                    //Save the report request and get the requestId
                    var schInfo = JsonConvert.DeserializeObject<ScheduleInfo>(result.ScheduleInfoJson);
                    ScheduleHelper scheduleHelper = new ScheduleHelper(schInfo);
                    if (scheduleHelper.ScheduleInfo.RecurrenceType == RecurrenceType.OneTime)
                    {
                        result.IsActive = false;
                    }
                    else
                    {
                        result.NextRunDate = scheduleHelper.GetNextDate(DateTime.UtcNow);
                    }
                    await dataAccess.SaveScheduleReportAsync(result);
                }
            }
        }

        /// <summary>
        /// The method sends email notifications to the scheduled shared reports to the recipient.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private async Task RunScheduleDeliveryNotification(RequestReportModel request, RequestReportArgs args)
        {
            try
            {
                if (request.ScheduledReportId.HasValue)
                {
                    var reportName = args.ReportDefinition.Name;
                    PostgresDataAccess dataAccess = new PostgresDataAccess();
                    var result = await dataAccess.GetScheduledReportAsync(request.ScheduledReportId.Value);
                    var schDate = result?.NextRunDate;
                    var baseUrl=await DataProvider.GetCustomerBaseUrl(AuthenticatedUser);
                    var url = baseUrl + "/schedule/download/?file="+ GetFilePath(request, args.ExportFormat.Value);
                    //get the sharing info.
                    var sharingInfo = (await GetSharedReport(new Data.Reporting.Arguments.GetSharedReportArgs() { ScheduledReportId = request.ScheduledReportId })).FirstOrDefault();
                    var mailingList = await DataProvider.GetScheduledReportRecipientEmailIds(sharingInfo.RecipientList);
                    mailingList.ForEach(async rcpt => await SendEmailDelivery(rcpt, reportName, schDate, url));
                }
            }
            catch (Exception ex)
            {
                Logger.Error($" Param - ReportName: {args?.ReportDefinition?.Name} ", ex.InnerException);
            }
        }

        private string GetFilePath(RequestReportModel request, ExportType format)
        {
            string fileName = string.Empty;
            if (format==ExportType.PDF)
            {
                fileName=Crypto.Encrypt(request.ResultPdfFilePath);
            }
            else if(format == ExportType.CSV)
            {
                fileName = Crypto.Encrypt(request.ResultCsvFilePath);
            }
            return fileName;
        }
        /// <summary>
        /// The method send the email to the recipient using a template.
        /// </summary>
        /// <param name="recipient"></param>
        /// <param name="reportName"></param>
        /// <param name="schDate"></param>
        /// <param name="Url"></param>
        /// <returns></returns>
        private async Task SendEmailDelivery(string recipient, string reportName, DateTime? schDate, string Url)
        {
            try
            {
                var args = new EmailReportDeliveryArgs()
                {
                    Recipients = recipient,
                    ReplaceKeyValue = new DeliveryParameters
                    {
                        ReportName = reportName,
                        RequestedDate = schDate.Value.ToLongDateString(),
                        SiteUrl = Url
                    },
                    Sender = "admin@absencesoft.com",
                    Template = "ReportGenerated"
                };
                var user = GetAuthenticatedSystemUser();
                BaseReportDeliveryProvider deliveryProvider = new EmailDeliveryProvider(args, user);
                args.Subject = deliveryProvider.GetEmailReportBody(ReportDeliveryTemplateType.ScheduledDeliverySubject);
                deliveryProvider.Parameters = args;
                await deliveryProvider.Send();
            }
            catch (Exception ex)
            {
                Logger.Error($" Param - ReportName: {reportName} ", ex.InnerException);
            }
        }

        private async Task OnFailureUpdateRequest(RequestReportModel request, string message, PostgresDataAccess dataAccess = null)
        {
            if (dataAccess == null)
            {
                dataAccess = new PostgresDataAccess();
            }
            if (request != null)
            {
                request.IsCompleted = false;
                request.Status = RequestStatus.Failed;
                request.ResponseMessage = message;
                request.ModifiedOn = DateTime.UtcNow;
                await dataAccess.SaveRequestReportAsync(request);
            }
        }

        private int GetLimit(RequestReportArgs args)
        {
            if (args.ExportFormat.HasValue)
            {
                return args.PageSize;
            }
            else
            {
                return args.Limit;
            }
        }
        /// <summary>
        /// The method is called from message processor on exception while processing the report request
        /// it updates the report request with the status.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="ex"></param>
        /// <returns></returns>
        public async Task FailedReportRequest(QueueItem item, Exception ex)
        {
            if (item?.QueueName != MessageQueues.AtCommonQueue)
            {
                // Get our Id from the message body
                long requestId = 0;
                Int64.TryParse(item?.Body, out requestId);
                PostgresDataAccess dataAccess = new PostgresDataAccess();

                // Get the report requested item item from the database.
                RequestReportModel request = await dataAccess.GetRequestReportByIdAsync(requestId);
                if (request != null)
                {
                    await OnFailureUpdateRequest(request, ex.GetBaseException().Message, dataAccess);
                }
            }
        }
        
        /// <summary>
        /// The method saves the completed report results as a Json file to S3
        /// </summary>
        /// <param name="result"></param>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        public async Task<string> SaveJsonFile(CompletedReportResult result, string CustomerId)
        {
            string fileKey;
            using (MemoryStream ms = new MemoryStream())
            {
                using (StreamWriter writer = new StreamWriter(ms))
                {
                    using (JsonTextWriter jsonWriter = new JsonTextWriter(writer))
                    {
                        JsonSerializer ser = new JsonSerializer();
                        ser.Serialize(jsonWriter, result);
                        jsonWriter.Flush();


                        var fileName = result.Name + ".json";

                        using (FileService fileService = new FileService())
                        {
                            fileKey = fileService.UploadFile(ms, fileName, Settings.Default.S3BucketName_Report_Files, CustomerId);
                        }
                    }
                }
            }
            return await Task.FromResult(fileKey);
        }

        /// <summary>
        /// The method saves the file to s3 with the desired format
        /// </summary>
        /// <param name="data"></param>
        /// <param name="CustomerId"></param>
        /// <param name="reportName"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public async Task<string> SaveFile(byte[] data, string CustomerId, string reportName, ExportType format)
        {
            string fileKey = "";
            using (MemoryStream ms = new MemoryStream(data))
            {
                var fileName = reportName + "." + format.ToString().ToLower();
                using (FileService fileService = new FileService())
                {
                    fileKey = fileService.UploadFile(ms, fileName, Settings.Default.S3BucketName_Report_Files, CustomerId);
                }
            }
            return await Task.FromResult(fileKey);
        }

        /// <summary>
        /// The method downloads the filename from S3 in text format
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public async Task<string> DownloadJsonFile(string filename)
        {
            string result;
            using (FileService fileService = new FileService())
            {
                result = fileService.DownloadFileToDiskAsText(filename, Settings.Default.S3BucketName_Report_Files);
            }
            return await Task.FromResult(result);
        }

        /// <summary>
        /// The method downloads the filename from S3 in text format
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public async Task<byte[]> DownloadFile(long requestId, ExportType format)
        {
            PostgresDataAccess dataAccess = new PostgresDataAccess();
            var result = await dataAccess.GetRequestReportByIdAsync(requestId);
            string filename = null;
            if (format == ExportType.PDF)
            {
                filename = result.ResultPdfFilePath;
            }
            else if (format == ExportType.CSV)
            {
                filename = result.ResultCsvFilePath;
            }
            byte[] resultFile;
            using (FileService fileService = new FileService())
            {
                resultFile = fileService.DownloadFile(filename, Settings.Default.S3BucketName_Report_Files);
            }
            return resultFile;
        }

        /// <summary>
        /// Get the file path from DB
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public async Task<string> GetFilePath(long requestId, ExportType format)
        {
            PostgresDataAccess dataAccess = new PostgresDataAccess();
            var result = await dataAccess.GetRequestReportByIdAsync(requestId);
            string filename = null;
            if (format == ExportType.PDF)
            {
                filename = result.ResultPdfFilePath;
            }
            else if (format == ExportType.CSV)
            {
                filename = result.ResultCsvFilePath;
            }

            return filename;
        }
        /// <summary>
        /// The method allows setting the autheticated user while giving request from SQS
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task SetAuthenticatedUserByKey(string key)
        {

            //Call the execute report.
            ApplicationUserManager userManager = new ApplicationUserManager(UserStoreProviderManager.Default.Repository);
            var user = await userManager.FindByKey(key);
            AuthenticatedUser = user;
        }
        private Entities.Authentication.User GetAuthenticatedSystemUser()
        {
            User user = User.System;
            HttpContext context = new HttpContext(new HttpRequest(null, "http://tempuri.org", null), new HttpResponse(null));
            HttpContext.Current = context;
            ApplicationUserManager userManager = new ApplicationUserManager(UserStoreProviderManager.Default.Repository);
            var ticket = userManager.GetSystemTicketAsync(user.Email, AT.Entities.Authentication.ApplicationType.Portal).GetAwaiter().GetResult();
            if (ticket != null)
            {
                context.Request.Browser = new HttpBrowserCapabilities() { Capabilities = new Dictionary<string, string> { { "supportsEmptyStringInCookieValue", "false" }, { "cookies", "false" } } };
                context.Items[User.CURRENT_USER_CONTEXT_KEY] = user;
                userManager.SetClaimsPrincipal(ticket, new HttpContextWrapper(context)).GetAwaiter().GetResult();
            }
            var authUser=userManager.FindByNameAsync(user.Email).GetAwaiter().GetResult(); 
            return authUser;
        }
        /// <summary>
        /// The methos allows fetching the status of the report via reqquestId.
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="userId"></param>
        /// <param name="userKey"></param>
        /// <returns></returns>
        public async Task<ReportStatusResult> GetReportStatusResultAsync(long requestId, long? userId = null, string userKey = null)
        {
            PostgresDataAccess dataAccess = new PostgresDataAccess();
            var result = await dataAccess.GetRequestReportByIdAsync(requestId);

            ReportStatusResult statusResult = new ReportStatusResult()
            {
                Id = requestId,
                Status = result.Status,
                Message = result.ResponseMessage,
                ReportName = result.ReportName,
                CriteriaPlainEnglish = result.CriteriaPlainEnglish,
                ReportId = result.ReportId
            };
            return statusResult;
        }

        /// <summary>
        /// Gets the history of the user's report requests
        /// </summary>
        /// <param name="limit"></param>
        /// <returns></returns>
        public async  Task<List<ReportRequestHistoryResult>> GetReportRequestHistoryAsync(int limit, bool queuedOnly)
        {
            List<ReportRequestHistoryResult> result = new List<ReportRequestHistoryResult>();
            PostgresDataAccess dataAccess = new PostgresDataAccess();
            var requests = await dataAccess.GetRequestReportHistoryAsync(AuthenticatedUser.Id, AuthenticatedUser.Key, limit, queuedOnly);

            foreach (var item in requests)
            {
                string reportName = item.ReportName;
                string criteria = item.CriteriaPlainEnglish;
                string reportId = item.ReportId;

                if(string.IsNullOrWhiteSpace(reportName) 
                    && string.IsNullOrWhiteSpace(criteria) 
                    && string.IsNullOrWhiteSpace(reportId))
                {
                    try
                    {
                        var args = JsonConvert.DeserializeObject<RequestReportArgs>(item.RequestMessage);
                        reportName = args.ReportDefinition.Name;
                        reportId = args.ReportId;
                        criteria = args.ReportDefinition.Criteria.PlainEnglish;
                    }
                    catch (Exception ex)
                    {                       
                        Logger.Error("Unable to cast argument", ex);
                    }
                }
                if (item.Status>=RequestStatus.Pending && item.Status <= RequestStatus.Scheduled)
                {
                    ReportRequestHistoryResult statusResult = new ReportRequestHistoryResult()
                    {
                        Id = item.Id.Value,
                        Status = item.Status,
                        Message = item.ResponseMessage,
                        ReportName = reportName,
                        RequestType = item.RequestType,
                        ResultMaxJsonFilePath = item.ResultMaxJsonFilePath,
                        ResultPdfFilePath = item.ResultPdfFilePath,
                        ResultCsvFilePath = item.ResultCsvFilePath,
                        CriteriaPlainEnglish = criteria,
                        ReportId = reportId,
                        LastModified = item.ModifiedOn
                    };
                    result.Add(statusResult);

                }
            }
            return result;
        }

        /// <summary>
        /// This allows fetching the completed report result from UI
        /// expected to provide the record limit to 100 records.
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        public async Task<CompletedReportResult> GetCompletedReportResultAsync(long requestId)
        {
            CompletedReportResult completedReportResult = null;
            // get the max_json file and return the results
            PostgresDataAccess dataAccess = new PostgresDataAccess();
            var result = await dataAccess.GetRequestReportByIdAsync(requestId);
            if (result?.Status == RequestStatus.Completed)
            {
                // download the file from S3 via the keyfile
                var jsonfile = result.ResultMaxJsonFilePath;
                var jsonResult = await DownloadJsonFile(jsonfile);
                using (TextReader reader = File.OpenText(@jsonResult))
                {
                    jsonResult = await reader.ReadToEndAsync();
                }
                completedReportResult = JsonConvert.DeserializeObject<CompletedReportResult>(jsonResult);
            }
            return completedReportResult;
        }

        /// <summary>
        /// The method allows scheduling a report with the schedule parameters info and persist inside the database.
        /// It returns the ReportStatusResult with status as Scheduled and an Id for future references.
        /// </summary>
        /// <param name="args"></param>
        /// <returns>ReportStatusResult</returns>
        public async Task<ReportStatusResult> ScheduleReportAsync(ScheduleReportArgs args)
        {
            //using the scheduling info get the next_run_date calculated to be saved in database.
          
            ScheduleHelper scheduleHelper = new ScheduleHelper(args.ScheduleInfo);
            var NextRunDate = scheduleHelper.GetFirstRunDate();
            ReportStatusResult result = new ReportStatusResult();
            try
            {
                //Save the report request and get the requestId
                ScheduledReportModel model = new ScheduledReportModel()
                {
                    ReportJson = JsonConvert.SerializeObject(args.RequestInfo),
                    ScheduleInfoJson = JsonConvert.SerializeObject(args.ScheduleInfo),
                    NextRunDate = NextRunDate,
                    ReportCategory = args.RequestInfo.ReportRequestType.ToString(),
                    CustomerKey = args.RequestInfo.CustomerId,
                    CreatedOn = DateTime.UtcNow,
                    CreatedById = AuthenticatedUser.Id,
                    CreatedByKey = SecurityUser.Id,
                    IsActive=true
                };
               
                PostgresDataAccess dataAccess = new PostgresDataAccess();

                var saveResult = await dataAccess.SaveScheduleReportAsync(model);
                if (saveResult?.Id.HasValue == true)
                {
                    result.Message = "Report scheduled successfully!";
                    //saving sharingInfo with schedule report Id
                    if (args.SharingInfo != null)
                    {
                        SaveSharedReportArgs sharingArgs = new SaveSharedReportArgs()
                        {
                            ReportName = args.RequestInfo.ReportDefinition?.Name,
                            ScheduledReportId = saveResult?.Id,
                            SharedWith = args.SharingInfo.SharedWith,
                            UserIds = args.SharingInfo.UserIds?.ToArray(),
                            UserKeys = args.SharingInfo.UserKeys?.ToArray(),
                            RoleIds = args.SharingInfo.RoleIds?.ToArray(),
                            RoleNames = args.SharingInfo.RoleNames?.ToArray(),
                            TeamIds = args.SharingInfo.TeamIds?.ToArray(),
                            TeamKeys = args.SharingInfo.TeamKeys?.ToArray(),
                            Argument = args.RequestInfo
                        };
                        await SaveSharedReport(sharingArgs);
                    }
                }
                else
                {
                    result.Status = RequestStatus.Failed;
                    result.Message = "Scheduled report failed with data access!";
                }
                result.Status = RequestStatus.Scheduled;
                result.Id = saveResult.Id.Value;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.InnerException);

                result.Status = RequestStatus.Failed;

                result.Message = "Scheduled report failed! " + ex.Message.ToString();
            }
            return result;
        }

        /// <summary>
        /// The method takes scheduled Id, returns the Report and it's scheduling information 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>ScheduleReportArgs</returns>
        public async Task<ScheduleReportArgs> GetScheduledReportInfo(long Id)
        {
            ScheduleReportArgs args = null;
            PostgresDataAccess dataAccess = new PostgresDataAccess();
            var result = await dataAccess.GetScheduledReportAsync(Id);
            if (result!=null)
            {
                //Save the report request and get the requestId
                args = new ScheduleReportArgs()
                {
                    RequestInfo = JsonConvert.DeserializeObject<RequestReportArgs>(result.ReportJson),
                    ScheduleInfo = JsonConvert.DeserializeObject<ScheduleInfo>(result.ScheduleInfoJson)
                };
            }
            return args;
        }

        /// <summary>
        /// The method fetches the reports scheduled to run by today and make a report request for each of them.
        /// </summary>
        /// <returns></returns>
        public async Task<List<ReportStatusResult>> RunScheduledReports()
        {
            List<ReportStatusResult> result = new List<ReportStatusResult>();
            PostgresDataAccess dataAccess = new PostgresDataAccess();
            var reports = await dataAccess.GetScheduledReportsToRunAsync();
            //Call Add request method  for each one in the list
            foreach (var schRpt in reports)
            {
                var RequestInfo = JsonConvert.DeserializeObject<RequestReportArgs>(schRpt.ReportJson);
                if (RequestInfo.ExportFormat==null)
                {
                    RequestInfo.ExportFormat = ExportType.PDF;
                }
                if (AuthenticatedUser == null)
                {
                    if (schRpt.CreatedByKey != null)
                    {
                        await SetAuthenticatedUserByKey(schRpt.CreatedByKey);
                    }
                    else
                    {
                        AuthenticatedUser = GetAuthenticatedSystemUser();
                    }
                }
                result.Add(await AddReportRequestAsync(RequestInfo, schRpt.Id));
            }
            return result;
        }

        /// <summary>
        /// Cancels the Report request for the given RequestId
        /// </summary>
        /// <param name="reportRequestId"></param>
        /// <returns></returns>
        public async Task<ReportStatusResult> CancelRequestReportAsync(long reportRequestId)
        {
            PostgresDataAccess dataAccess = new PostgresDataAccess();
            var result = await dataAccess.CancelRequestReportAsync(reportRequestId);

            //throw on error
            if (!result)
            {
                return null;
            }
            else
            {
                return new ReportStatusResult()
                {
                    Id = reportRequestId,
                    Status = RequestStatus.Canceled,
                    Message = "Cancel Report request " + RequestStatus.Completed.ToString()
                };
            }
        }

        /// <summary>
        /// Get the list of Adhoc Reports
        /// </summary>
        /// <returns></returns>
        public async Task<List<AdHocReportView>> GetAdhocReportsTypeAsync()
        {
            return await DataProvider.GetAdhocReportTypesAsync();
        }
    }
}
