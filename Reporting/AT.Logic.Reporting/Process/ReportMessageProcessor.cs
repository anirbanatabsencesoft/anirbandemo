﻿using AbsenceSoft.Logic.Processing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Logic.Common;

namespace AT.Logic.Reporting.Process
{
    public class ReportMessageProcessor : IMessageProcessor
    {
        public void AfterProcess(QueueItem item, MessageProcessingContext context)
        {
            // no operations to call and is handelled with-in process or outside implementation.
        }

        public void BeforeProcess(QueueItem item, MessageProcessingContext context)
        {
            // no operations is required before processing the report request.
        }

        public async void HandleException(QueueItem item, Exception ex)
        {
            ReportingLogic reportingLogic = new ReportingLogic();
            await reportingLogic.FailedReportRequest(item, ex);            
        }

        public bool IsProcessorFor(QueueItem item)
        {
            return item.GetMessageType() == MessageType.ReportRequest;
        }

        public async void Process(QueueItem item, MessageProcessingContext context)
        {
            ReportingLogic reportingLogic = new ReportingLogic();
            await reportingLogic.ProcessReportRequest(item);
        }
    }
}
