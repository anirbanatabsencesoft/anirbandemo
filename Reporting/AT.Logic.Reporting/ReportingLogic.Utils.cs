﻿using AbsenceSoft;
using AbsenceSoft.Data.Reporting;
using AT.Common.Log;
using AT.Data.Reporting.Base;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Classes;
using AT.Entities.Reporting.Interfaces;
using AT.Logic.Arguments;
using AT.Logic.Reporting.Reports;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace AT.Logic.Reporting
{
    /// <summary>
    /// The Reporting Logic class to provide the data access point of contact
    /// </summary>
    public partial class ReportingLogic
    {
        /// <summary>
        /// The data provider instance
        /// </summary>
        private readonly BaseDataProvider DataProvider;

        /// <summary>
        /// The authenticated user must be injected through constructor
        /// </summary>
        public User AuthenticatedUser { get; set; }

        /// <summary>
        /// Security User
        /// </summary>
        public AbsenceSoft.Data.Security.User SecurityUser { get => GetSecurityUserById(AuthenticatedUser).GetAwaiter().GetResult(); }

        /// <summary>
        /// Default constructor. Will remove later.
        /// </summary>
        public ReportingLogic()
        {
            DataProvider = DataProviderManager.Default;
        }

        /// <summary>
        /// Constructor to do the initialization & injection
        /// </summary>
        /// <param name="user"></param>
        public ReportingLogic(User user)
        {
            DataProvider = DataProviderManager.Default;
            AuthenticatedUser = user;
        }


        /// <summary>
        /// Convert report definition object to Report object
        /// </summary>
        /// <param name="reportDefinition"></param>
        /// <returns></returns>
        private Report ConvertDefinitionToAdhocReport(ReportDefinition reportDefinition)
        {
            try
            {
                if (reportDefinition != null)
                {
                    Report report = new AdHocReport
                    {
                        Name = reportDefinition.Name,
                        Id = reportDefinition.Id,

                        Query = new ReportQuery
                        {
                            CriteriaGroup = new CriteriaGroup()
                        }
                    };
                    report.Query.CriteriaGroup.Criteria = new List<ICriteria>();

                    CustomCriteria customCriteria = new CustomCriteria
                    {
                        Fields = new List<IField>()
                    };

                    report.ReportColumns = new List<IReportColumn>();

                    foreach (var reportDefinitionField in reportDefinition.Fields)
                    {
                        CustomField customField = new CustomField
                        {
                            Name = reportDefinitionField.Name,
                            Value = reportDefinitionField.Name
                        };

                        customCriteria.Fields.Add(customField);

                        report.ReportColumns.Add(new ReportColumn()
                        {
                            Name = reportDefinitionField.Name,
                            Value = reportDefinitionField.Name
                        });
                    }
                    report.UserId = reportDefinition.UserId;
                    report.ReportId = reportDefinition.ReportType.Id;
                    return report;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }

            //return default
            return null;
        }

        /// <summary>
        /// Convert multiple report definition object to report object
        /// </summary>
        /// <param name="reportDefinitions"></param>
        /// <returns></returns>
        private List<Report> ConvertDefinitionToAdhocReport(IList<ReportDefinition> reportDefinitions)
        {
            var data = reportDefinitions
                .Select(reportDefinition => ConvertDefinitionToAdhocReport(reportDefinition))
                .Where(report => report != null).ToList();
            return data;
        }

        /// <summary>
        /// Convert Report Fields to Report Columns
        /// </summary>
        /// <param name="reportFields"></param>
        /// <returns></returns>
        private List<ReportColumn> ConvertToReportColumns(List<ReportField> reportFields)
        {
            return reportFields
                .Select(reportField => new ReportColumn()
                {
                    Name = reportField.Name,
                    Value = reportField.Display
                })
                .ToList();
        }

        /// <summary>
        /// Builds the arguments for query based on ReportField and ReportType passed
        /// </summary>
        /// <param name="field"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private object[] BuildQueryArguments(ReportField field, ReportType type)
        {
            var args = new object[AuthenticatedUser.Employers.Count + 3];
            args[0] = field.Name;
            args[1] = type.Key;
            args[2] = AuthenticatedUser.CustomerKey;
            for (int i = 3; i < args.Length; i++)
            {
                args[i] = AuthenticatedUser.Employers[i].EmployerId;
            }
            return args;
        }

        /// <summary>
        /// Builds the query format for given ReportType
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private string BuildFormatQuery(ReportType type)
        {
            StringBuilder queryString = new StringBuilder();
            queryString.Append("select format('");
            queryString.Append("select distinct \"%I\" as value from \"%I\" where cust_id = %L");
            if (type.IsEmployer)
            {
                queryString.Append(" and (eplr_id is null or eplr_id in (");
                foreach (var employer in AuthenticatedUser.Employers)
                {
                    queryString.Append("%L");
                    if (AuthenticatedUser.Employers.Last() != employer)
                    {
                        queryString.Append(", ");
                    }
                }
                queryString.Append("))");
            }
            queryString.Append(" order by \"%I\" asc', @0, @1, @2");

            if (type.IsEmployer)
            {
                queryString.Append(", ");
                for (int i = 3; i < AuthenticatedUser.Employers.Count + 3; i++)
                {
                    queryString.AppendFormat("@{0}, ", i);
                }
                queryString.Remove(0, queryString.Length - 2);
            }

            queryString.AppendFormat(", @{0}) as query", 0);

            return queryString.ToString();
        }


        private ExecuteReportArgs ConvertToExecuteReportArgs(Arguments.RequestReportArgs requestArgs)
        {
            ExecuteReportArgs args = new ExecuteReportArgs()
            {
                ReportId = requestArgs.ReportId,
                GroupType = requestArgs?.ReportDefinition?.GroupType,
                GroupByColumns = requestArgs?.ReportDefinition?.GroupByColumns,
                Message = requestArgs.Message,
                Criteria = ConvertToReportCriteria(requestArgs.ReportDefinition.Criteria),
                Offset = requestArgs.Offset,
                OrderByAsc = requestArgs.orderByAsc,
                PageNumber = requestArgs.PageNumber,
                PageSize = requestArgs.PageSize,
                Rows = requestArgs.Rows,
                SortBy = requestArgs.SortBy
            };
            return args;
        }

        private Reports.ReportCriteria ConvertToReportCriteria(Arguments.FilterCriteria criteria)
        {
            var reportCriteria =
                new Reports.ReportCriteria()
                {
                    AgeFrom = criteria.AgeFrom,
                    AgeTo = criteria.AgeTo,
                    StartDate = criteria.StartDate,
                    EndDate = criteria.EndDate,
                    ShowAgeCriteria = criteria.ShowAgeCriteria,
                    PlainEnglish = criteria.PlainEnglish,
                    HasCriterias = criteria.HasCriteria
                };
            foreach (var filter in criteria.Filters)
            {
                var item = new Reports.ReportCriteriaItem()
                {
                    Name = filter.FieldName,
                    Prompt = filter.Prompt,
                    ControlType = filter.ControlType,
                    Tweener = filter.Tweener,
                    Required = filter.Required,
                    KeepOptionAll = filter.KeepOptionAll,
                    Value = filter.Value,
                    Values = filter.Values
                };
                filter.Options.ForEach(p =>
                    item.Options.Add(
                        new Reports.ReportCriteriaItemOption()
                        {
                            Text = p.Text,
                            Value = p.Value,
                            GroupText = p.GroupText
                        }
                    )
                );
                reportCriteria.Filters.Add(item);
            }
            return reportCriteria;
        }

        private Arguments.FilterCriteria ConvertToFilterCriteria(Reports.ReportCriteria criteria)
        {
            var filterCriteria =
                new Arguments.FilterCriteria()
                {
                    AgeFrom = criteria.AgeFrom,
                    AgeTo = criteria.AgeTo,
                    StartDate = criteria.StartDate,
                    EndDate = criteria.EndDate,
                    ShowAgeCriteria = criteria.ShowAgeCriteria,
                    HasCriteria = criteria.HasCriterias
                };
            foreach (var filter in criteria.Filters)
            {
                var item = new ReportFilter()
                {
                    FieldName = filter.Name,
                    Prompt = filter.Prompt,
                    ControlType = filter.ControlType,
                    Tweener=filter.Tweener,
                    Required=filter.Required,
                    KeepOptionAll=filter.KeepOptionAll,
                    Value = filter.Value,
                    Values = filter.Values
                };
                item.Options = new List<ReportFilterOption>();
                if (filter.Options != null)
                {
                    filter.Options.ForEach(p =>
                        item.Options.Add(
                            new ReportFilterOption()
                            {
                                Text = p.Text,
                                Value = p.Value,
                                GroupText = p.GroupText
                            }
                         )
                    );
                }
                filterCriteria.Filters.Add(item);
            }
            return filterCriteria;
        }

        private Arguments.AdhocExecuteReportArgs ConvertToAdhocReportArgs(Arguments.RequestReportArgs requestArgs)
        {
            Arguments.AdhocExecuteReportArgs args = new Arguments.AdhocExecuteReportArgs()
            {
                ReportDefinitionId = requestArgs.ReportId,
                Offset = requestArgs.Offset,
                Limit = requestArgs.Limit,
                ReportDefinition = new AbsenceSoft.Data.Reporting.ReportDefinition()
                {
                    CustomerId = requestArgs.CustomerId,
                    Name = requestArgs.ReportDefinition?.Name,
                    Description = requestArgs.ReportDefinition?.Description,
                    Fields = requestArgs.ReportDefinition?.Fields,
                    Filters = requestArgs.ReportDefinition?.Criteria?.Filters,
                    ReportType= requestArgs.ReportDefinition?.ReportType
                }
            };
            return args;
        }

        private CompletedReportResult ConvertToCompletedReportResult(Reports.ReportResult result)
        {
            CompletedReportResult completedReportResult = new CompletedReportResult()
            {
                Name = result.Name,
                Success = result.Success,
                Category = result.Category,
                Chart = result.Chart,
                CompletedIn = result.CompletedIn,
                Error = result.Error,
                GroupAutoTotals = result.GroupAutoTotals,
                Criteria = ConvertToFilterCriteria(result.Criteria),
                RequestDate = result.RequestDate,
                RequestedBy = result.RequestedBy,
                JsonData = result.JsonData
            };
            return completedReportResult;
        }
        private CompletedReportResult ConvertToCompletedReportResult(AdHocReportResult result)
        {
            CompletedReportResult completedReportResult = new CompletedReportResult()
            {
                Name = result.Name,
                Success = result.Success,
                CompletedIn = result.CompletedIn,
                Error = result.Error,
                RequestDate = result.RequestDate,
                JsonData = result.Items,
                AdHocReportType = result.AdHocReportType,
                Query = result.Query,
                RecordCount = result.RecordCount
            };
            return completedReportResult;
        }
    }
}