﻿using AbsenceSoft;
using AbsenceSoft.Data.Reporting;
using AT.Common.Log;
using AT.Data.Reporting;
using AT.Data.Reporting.Arguments;
using AT.Data.Reporting.Base;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Classes;
using AT.Entities.Reporting.Entities;
using AT.Entities.Reporting.Interfaces;
using AT.Logic.Reporting.Arguments;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting
{
    /// <summary>
    /// The Reporting Logic class to provide the data access point of contact
    /// </summary>
    public partial class ReportingLogic
    {
        /// <summary>
        /// Returns the shared report based on the argument
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public async Task<List<SharedReport>> GetSharedReport(GetSharedReportArgs args)
        {
            PostgresDataAccess pda = new PostgresDataAccess();
            List<SharedReport> result = new List<SharedReport>();
            var dataUser = await pda.GetSharedReport(args);
            AddSharedReportFromModel(dataUser, result);
            //reset to avoid duplicates
            args.UserId = null;
            args.UserKey = null;
            foreach (var role in SecurityUser.Roles)
            {
                args.RoleName = role;
                var d = await pda.GetSharedReport(args);
                AddSharedReportFromModel(d, result);
                args.RoleName = null;
            }
            var teams = await DataProvider.GetTeamsForCurrentUser(SecurityUser);
            if (teams != null && teams.Count > 0)
            {
                foreach (var team in teams)
                {
                    args.TeamKey = team.Id;
                    var d = await pda.GetSharedReport(args);
                    AddSharedReportFromModel(d, result);
                    args.TeamId = null;
                }
            }

            return result;
        }

        /// <summary>
        /// Returns the saved reports for the user
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public async Task<List<SharedReport>> GetSavedOperationReports(GetSharedReportArgs args)
        {
            PostgresDataAccess pda = new PostgresDataAccess();
            List<SharedReport> result = new List<SharedReport>();
            var data = await pda.GetSavedOperationReports(args);
            AddSharedReportFromModel(data, result);
            return result;
        }

        /// <summary>
        /// Iterates through model and generate a shared report
        /// </summary>
        /// <param name="sharedReportList"></param>
        /// <param name="result"></param>
        private void AddSharedReportFromModel(IList<AT.Entities.Reporting.Models.SharedReportModel> sharedReportList, List<SharedReport> result)
        {
            if (sharedReportList.Any())
            {
                List<AT.Entities.Reporting.Models.SharedReportModel> distinctModels = sharedReportList.GroupBy(p => p.Id).Select(g => g.First()).ToList();
                if (distinctModels.Any())
                {
                    foreach (var item in distinctModels)
                    {
                        //get all childs for the distinct
                        var children = sharedReportList.Where(p => p.Id == item.Id).ToList();
                        var report = new SharedReport()
                        {
                            Id = item.Id,
                            CreatedById = item.CreatedById,
                            CreatedByKey = item.CreatedByKey,
                            CreatedOn = item.CreatedOn,
                            CriteriaJson = item.ReportJSON,
                            CustomerId = item.CustomerId,
                            CustomerKey = item.CustomerKey,
                            LastRequestId = item.RequestId,
                            ReportCategory = item.ReportCategory,
                            ReportObjectFullyQualifiedName = item.ReportObjectFQN,
                            ScheduledReportId = item.ScheduledReportId,
                            Order = item.Sequence,
                            ReportName = item.ReportName,
                            SharedWith = item.SharedWith,
                            ModifiedOn = item.ModifiedOn,
                            IsActive = true,
                            IsDeleted = false
                        };
                        var recipientList = new List<SharedReportRecipient>();
                        foreach (var child in children)
                        {
                            recipientList.Add(new SharedReportRecipient()
                            {
                                Id = child.RecipientId,
                                RoleId = child.RoleId,
                                RoleName = child.RoleName,
                                SharedReportId = item.Id,
                                TeamId = child.TeamId,
                                TeamKey = child.TeamKey,
                                TeamName = child.TeamName,
                                UserId = child.UserId,
                                UserKey = child.UserKey
                            });
                        }

                        report.RecipientList = recipientList;
                        result.Add(report);
                    }
                }
            }
        }

        /// <summary>
        /// Save a shared report
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public async Task<SharedReport> SaveSharedReport(SaveSharedReportArgs args)
        {
            SharedReport sr = new SharedReport()
            {
                Id = args.Id,
                CreatedById = AuthenticatedUser.Id,
                CreatedByKey = AuthenticatedUser.Key,
                CreatedOn = DateTime.Now,
                CriteriaJson = args.CriteriaJson,
                CustomerId = AuthenticatedUser.CustomerId,
                CustomerKey = AuthenticatedUser.CustomerKey,
                IsActive = true,
                IsDeleted = false,
                LastRequestId = args.LastRequestId,
                ModifiedOn = DateTime.Now,
                ReportCategory = "Request Argument",
                ReportName = args.ReportName,
                ScheduledReportId = args.ScheduledReportId,
                SharedWith = args.SharedWith,
                Order = args.Order,
                ReportObjectFullyQualifiedName = args.Argument.GetType().FullName
            };

            sr.RecipientList = new List<SharedReportRecipient>();
            if (args.UserIds != null)
            {
                AddRecipientsAgainstUserIds(args.UserIds, sr);
            }

            if (args.UserKeys != null)
            {
                AddRecipientsAgainstUserKey(args.UserKeys, sr);
            }

            if (args.TeamIds != null)
            {
                AddRecipientsAgainstTeamIds(args.TeamIds, sr);
            }

            if (args.TeamKeys != null)
            {
                AddRecipientsAgainstTeamKey(args.TeamKeys, sr);
            }

            if (args.RoleIds != null)
            {
                AddRecipientsAgainstRoleIds(args.RoleIds, sr);
            }

            if (args.RoleNames != null)
            {
                AddRecipientsAgainstRoleNames(args.RoleNames, sr);
            }

            PostgresDataAccess pda = new PostgresDataAccess();
            return await pda.SaveSharedReport(sr);
        }

        /// <summary>
        /// Private method to add user ids to recipients list
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="sr"></param>
        private void AddRecipientsAgainstUserIds(long[] ids, SharedReport sr)
        {
            foreach (var id in ids)
            {
                sr.RecipientList.Add(new SharedReportRecipient()
                {
                    UserId = id
                });
            }
        }

        /// <summary>
        /// Private method to add recipient against user keys
        /// </summary>
        /// <param name="keys"></param>
        /// <param name="sr"></param>
        private void AddRecipientsAgainstUserKey(string[] keys, SharedReport sr)
        {
            foreach (var key in keys)
            {
                sr.RecipientList.Add(new SharedReportRecipient()
                {
                    UserKey = key
                });
            }
        }

        /// <summary>
        /// Private method to add recipients against the team ids
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="sr"></param>
        private void AddRecipientsAgainstTeamIds(long[] ids, SharedReport sr)
        {
            foreach (var id in ids)
            {
                sr.RecipientList.Add(new SharedReportRecipient()
                {
                    TeamId = id
                });
            }
        }

        /// <summary>
        /// Private method to add recipients against team key
        /// </summary>
        /// <param name="keys"></param>
        /// <param name="sr"></param>
        private void AddRecipientsAgainstTeamKey(string[] keys, SharedReport sr)
        {
            foreach (var key in keys)
            {
                sr.RecipientList.Add(new SharedReportRecipient()
                {
                    TeamKey = key
                });
            }
        }

        /// <summary>
        /// Method to add recipients against a role id
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="sr"></param>
        private void AddRecipientsAgainstRoleIds(long[] ids, SharedReport sr)
        {
            foreach (var id in ids)
            {
                sr.RecipientList.Add(new SharedReportRecipient()
                {
                    RoleId = id
                });
            }
        }

        /// <summary>
        /// Method to add recipient against a role name
        /// </summary>
        /// <param name="names"></param>
        /// <param name="sr"></param>
        private void AddRecipientsAgainstRoleNames(string[] names, SharedReport sr)
        {
            foreach (var name in names)
            {
                sr.RecipientList.Add(new SharedReportRecipient()
                {
                    RoleName = name
                });
            }
        }

        /// <summary>
        /// Deletes a shared report
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<int> DeleteSharedReport(long id)
        {
            PostgresDataAccess pda = new PostgresDataAccess();
            return await pda.DeleteSharedReport(id, AuthenticatedUser.Id, AuthenticatedUser.Key);
        }

    }
}