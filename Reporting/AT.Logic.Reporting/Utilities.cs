﻿using AT.Logic.Reporting.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting
{
    /// <summary>
    /// Utility class for handling different reporting utilities
    /// </summary>
    public static class Utilities
    {
        /// <summary>
        /// Merge the criteria with original
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="original"></param>
        /// <returns></returns>
        public static ReportCriteria MergeCriteria(ReportCriteria criteria, ReportCriteria original)
        {
            if (criteria?.Filters != null && original?.Filters != null)
            {
                foreach (var filter in criteria.Filters)
                {
                    var match = original.Filters.FirstOrDefault(f => f.Name == filter.Name);
                    if (match != null)
                    {
                        filter.Options = match.Options;
                        filter.Prompt = match.Prompt;
                        filter.ControlType = match.ControlType;
                    }
                }
            }
            return criteria;
        }
        
        /// <summary>
        /// Converts the user criteria to Reporting Criteria
        /// </summary>
        /// <param name="reportCriteria">Source Criteria</param>
        /// <returns>Reporting Criteria</returns>
        public static AbsenceSoft.Reporting.ReportCriteria CreateReportingCriteria(Reports.ReportCriteria reportCriteria)
        {
            AbsenceSoft.Reporting.ReportCriteria criteria = new AbsenceSoft.Reporting.ReportCriteria
            {
                Id = reportCriteria.Id,
                AgeFrom = reportCriteria.AgeFrom,
                CustomerId = reportCriteria.CustomerId,
                AgeTo = reportCriteria.AgeTo,
                EndDate = reportCriteria.EndDate,
                HasCriterias = reportCriteria.HasCriterias,
                StartDate = reportCriteria.StartDate,
                PlainEnglish = reportCriteria.PlainEnglish,
                ShowAgeCriteria = reportCriteria.ShowAgeCriteria
            };
            if (reportCriteria.Filters != null)
            {
                criteria.Filters = new List<AbsenceSoft.Reporting.ReportCriteriaItem>();
                AbsenceSoft.Reporting.ReportCriteriaItem item;
                foreach (var filter in reportCriteria.Filters)
                {
                    item = new AbsenceSoft.Reporting.ReportCriteriaItem
                    {
                        Name = filter.Name,
                        Value = filter.Value,
                        Values = filter.Values,
                        ControlType = filter.ControlType,
                        Metadata = filter.Metadata,
                        Id = filter.Id,
                        KeepOptionAll = filter.KeepOptionAll,
                        OnChangeMethodName = filter.OnChangeMethodName,
                        Prompt = filter.Prompt,
                        Required = filter.Required,
                        Tweener = filter.Tweener

                    };
                    foreach (var option in filter.Options)
                    {
                        item.Options.Add(new AbsenceSoft.Reporting.ReportCriteriaItemOption { Id=option.Id, GroupText=option.GroupText, Text=option.Text, Value=option.Value, Metadata=option.Metadata });
                    }                    
                    criteria.Filters.Add(item);
                }
            }
            return criteria;
        }
    }
}
