﻿using AbsenceSoft;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AT.Data.Reporting.Model;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AT.Logic.Reporting.Reports.CustomFields
{
    /// <summary>
    /// Custom Field Dates Detail Report
    /// </summary>
    public partial class CustomFieldDatesDetailReport : BaseReport
    {
        private readonly Dictionary<string, Customer> _cust = new Dictionary<string, Customer>();

        /// <summary>
        /// When overridden in a derived class, gets the name of the report main category to bifurcate reports.
        /// </summary>
        public override string MainCategory { get; set; } = "Operations Report";

        /// <summary>
        /// Icon to set for intermittent pattern report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_intermittent.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report category for display.
        /// </summary>
        public override string Category { get; set; } = "Case Custom Date Fields";
        
        public override string ReportDescription
        {
            get { return "Case list provides high level details about cases with custom fields"; }
        }


        /// <summary>
        /// Determines whether the specified report is visible to the given user for any valid reason.
        /// </summary>
        /// <param name="user">The user to check against.</param>
        /// <returns>
        ///   <c>true</c> if the specified report is visible; otherwise, <c>false</c>.
        /// </returns>
        public bool IsVisible(UserReportCriteriaData data)
        {
            if (data.SecurityUser == null)
            {
                return false;
            }
            if (data.SecurityUser.Customer != null && data.SecurityUser.Customer.Id == "546e5097a32aa00d60e3210a")
            {
                return false;
            }
            var criteria = GetCustomFieldsCriteria(data);
            if (criteria == null)
            {
                return false;
            }
            return criteria.Options.Any();
        }

        /// <summary>
        /// Gets Static Data for the given Case
        /// </summary>
        /// <param name="td"></param>
        /// <returns></returns>
        protected ResultData GetStaticData(Case td)
        {
            var cust = _cust.ContainsKey(td.CustomerId) ? _cust[td.CustomerId] : td.Customer;
            _cust[td.CustomerId] = cust;

            string displayCaseNumber = td.CaseNumber;
            string displayFullName = td.Employee.FullName;
            string displayReason = td.Reason.Name;
            string displayLeaveType = td.CaseType.ToString();
            string displayCreatedOn = td.CreatedDate.ToString("MM/dd/yyyy");
            var evt = td.FindCaseEvent(CaseEventType.CaseCreated);
            if (evt != null && evt.EventDate != DateTime.MinValue)
            {
                displayCreatedOn = evt.EventDate.ToString("MM/dd/yyyy");
            }

            dynamic data = new ResultData();
            data.CaseNumber = string.IsNullOrWhiteSpace(displayCaseNumber) ? null : new LinkItem() { Value = td.CaseNumber, Link = string.Format("/Cases/{0}/View", td.Id) };
            data.Name = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = td.Employee.FullName, Link = string.Format("/Employees/{0}/View", td.Employee.Id) };
            if (cust.HasFeature(Feature.ADA) && td.IsAccommodation && td.AccommodationRequest != null && td.AccommodationRequest.Accommodations != null && td.AccommodationRequest.Accommodations.Any())
            {
                data.Duration = string.Join(", ", td.AccommodationRequest.Accommodations.Select(a => a.Duration.ToString().SplitCamelCaseString()).Distinct());
            }
            if (cust.HasFeature(Feature.LOA))
            {
                data.Reason = displayReason;
            }
            data.CaseType = displayLeaveType;
            data.CreatedOn = displayCreatedOn;
            data.StartDate = td.StartDate.ToString("MM/dd/yyyy");
            data.EndDate = td.EndDate.ToString("MM/dd/yyyy");
            data.Status = td.Status.ToString().SplitCamelCaseString();
            if (cust.HasFeature(Feature.ADA) && td.IsAccommodation && td.AccommodationRequest != null && td.AccommodationRequest.Accommodations != null && td.AccommodationRequest.Accommodations.Any())
            {
                data.Accommodations = string.Join(", ", td.AccommodationRequest.Accommodations.Select(a => a.Type.Code).Distinct());
            }
            data.AssignedTo = td.AssignedToName;
            data.WorkState = td.Employee.WorkState;
#pragma warning disable CS0618 // Type or member is obsolete
            data.Location = td.Employee.Info.OfficeLocation;
#pragma warning restore CS0618 // Type or member is obsolete

            return data;
        }
        
    }
}