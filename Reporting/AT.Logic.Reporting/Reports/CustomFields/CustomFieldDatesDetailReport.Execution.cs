﻿using AbsenceSoft;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AT.Common.Core;
using AT.Common.Core.ReportEnums;
using AT.Data.Reporting.Model;
using AT.Entities.Authentication;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.CustomFields
{
    /// <summary>
    /// Custom Field Dates Detail Report
    /// </summary>
    public partial class CustomFieldDatesDetailReport : BaseReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "27AA17A1-13DE-49AF-8761-99D0609331A9"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "Detail"; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }
        
        /// <summary>
        /// Property to configure chart related options
        /// </summary>
        public override ReportChart Chart
        {
            get
            {
                return new ReportChart()
                {
                    Type = "PieChart",
                    Title = "",
                    is3D = true,
                    isStacked = false,
                    Height = 400
                };
            }
        }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportingLogic ReportingLogic = new ReportingLogic(user);
            UserReportCriteriaData data = ReportingLogic.GetUserReportCriteriaDataAsync().GetAwaiter().GetResult();
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = data.SecurityUser.CustomerId.ToString(),
                StartDate = DateTime.UtcNow.Date.AddDays(-7),
                EndDate = DateTime.UtcNow.Date
            };
            ReportCriteriaItem item = GetCustomFieldsCriteria(data, CustomFieldType.Date, EntityTarget.Case);
            criteria.Filters.Add(item);
            return criteria;
        }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override async Task<ReportResult> RunReportAsync(User user, ReportResult result, int pageNumber = 1, int pageSize = 10)
        {
            ReportingLogic ReportingLogic = new ReportingLogic(user);
            UserReportCriteriaData data = ReportingLogic.GetUserReportCriteriaDataAsync().GetAwaiter().GetResult();
            IMongoQuery query = BuildQuery(data, result.Criteria, CustomFieldType.Date);
            List<Case> cases = ReportingLogic.GetCase(query, pageNumber, pageSize).GetAwaiter().GetResult().ToList();

            Dictionary<string, int> reduce = new Dictionary<string, int>();
            List<ResultDataGroup> groupResults = new List<ResultDataGroup>();
            SetGroupResult(cases, reduce, groupResults);

            result.Groups = new List<ResultDataGroup>();
            result.Groups.AddRange(groupResults);

            result.SeriesData = new List<List<object>>(reduce.Count + 1)
            {
                // Define our first row, which contains series labels for the x (col 1) and y (col 2~n) axis.
                // Add our series definition/labels row to the result series data collection.
                new List<object>(2) { "Field", "Count" }
            };

            // Loop through our inline results and pump out the series data results to the series data collection
            //  on the result object
            foreach (var r in reduce)
            {
                result.SeriesData.Add(new List<object>(2) { r.Key, r.Value });
            }

            return await Task.FromResult(result);
        }

        /// <summary>
        /// Sets the Group Result
        /// </summary>
        /// <param name="cases"></param>
        /// <param name="reduce"></param>
        /// <param name="groupResults"></param>
        private void SetGroupResult(List<Case> cases, Dictionary<string, int> reduce, List<ResultDataGroup> groupResults)
        {
            foreach (var c in cases)
            {
                ResultData caseData = GetStaticData(c);
                foreach (var f in (c.CustomFields ?? new List<CustomField>(0))
                    .Where(f => f.DataType == CustomFieldType.Date && f.SelectedValue != null && !string.IsNullOrWhiteSpace(f.SelectedValue.ToString()))
                    .OrderBy(f => f.Name))
                {
                    ResultDataGroup group = groupResults.FirstOrDefault(r => r.Key.ToString() == f.Name);
                    if (group == null)
                    {
                        group = groupResults.AddFluid(new ResultDataGroup()
                        {
                            Key = f.Name,
                            Label = f.Label ?? f.Name,
                            Items = new List<ResultData>()
                        });
                    }

                    if (f.SelectedValue is DateTime)
                    {
                        caseData[f.Name] = ((DateTime)f.SelectedValue).ToString("MM/dd/yyyy");
                    }
                    else
                    {
                        caseData[f.Name] = f.SelectedValue.ToString();
                    }

                    // Add a copy of this data to the result items for this custom field
                    group.Items.Add(caseData);

                    // Ensure we keep count on our field usage (reduce/summary)
                    if (reduce.ContainsKey(f.Name))
                    {
                        reduce[f.Name] += 1;
                    }
                    else
                    {
                        reduce.Add(f.Name, 1);
                    }
                }
            }
        }

        /// <summary>
        /// Builds the query.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="criteria">The criteria.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        protected virtual IMongoQuery BuildQuery(UserReportCriteriaData data, ReportCriteria criteria, CustomFieldType? type = null)
        {
            criteria = criteria ?? GetCriteria(data.AuthenticatedUser);

            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (data.SecurityUser != null)
            {
                ands.Add(data.SecurityUser.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport, "Employee._id"));
            }
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
            {
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            }
            ApplyEmployerFilter<Case>(ands, criteria);

            string fieldNameFilter = (criteria.Filters.FirstOrDefault(f => f.Name == "CustomField") ?? new ReportCriteriaItem()).Value as string;

            List<IMongoQuery> elemMatchAnds = new List<IMongoQuery>
            {
                Query.EQ("Target", new BsonInt32((int)EntityTarget.Case))
            };

            if (type.HasValue)
            {
                elemMatchAnds.Add(Query.EQ("DataType", new BsonInt32((int)type.Value)));
            }
            if (!string.IsNullOrWhiteSpace(fieldNameFilter))
            {
                elemMatchAnds.Add(Query.EQ("Name", new BsonString(fieldNameFilter)));
            }
            if (type == CustomFieldType.Date)
            {
                elemMatchAnds.Add(Query.GTE("SelectedValue", new BsonDateTime(startDate)));
                elemMatchAnds.Add(Query.LTE("SelectedValue", new BsonDateTime(endDate)));
            }
            else
            {
                // Must match a range for dates, not just start and end within that range, it's
                //  any leave that intersects the date range selected by the user.
                ands.Add(Case.Query.Or(
                    Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                    Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
                ));
            }

            ands.Add(Case.Query.ElemMatch(c => c.CustomFields, f => f.And(elemMatchAnds)));            
            return Case.Query.And(ands);
        }
        
        /// <summary>
        /// Applies the employer filter.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="query">The query.</param>
        /// <param name="criteria">The criteria.</param>
        private void ApplyEmployerFilter<TEntity>(ICollection<IMongoQuery> query, ReportCriteria criteria) where TEntity : BaseEmployerEntity<TEntity>, new()
        {
            var employerIdFilter = criteria.Filters.FirstOrDefault(m => m.Name == EmployerIdFilterName);
            if (employerIdFilter != null && employerIdFilter.Values != null && employerIdFilter.Values.Any())
            {
                var val = employerIdFilter.ValuesAs<string>();
                if (val != null && val.Any())
                {
                    query.Add(BaseEmployerEntity<TEntity>.Query.In(c => c.EmployerId, val));
                }
            }
        }
    }
}