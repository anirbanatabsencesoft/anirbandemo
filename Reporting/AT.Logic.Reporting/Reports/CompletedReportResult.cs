
using System;
using System.Collections.Generic;
using AT.Entities.Authentication;
using AT.Logic.Reporting.Arguments;

namespace AT.Logic.Reporting.Reports
{
    /// <summary>
    /// The class represents the report results when the status=Completed for the
    /// requested report.
    /// </summary>
    [Serializable]
    public class CompletedReportResult
    {

        public CompletedReportResult()
        {

        }
       

        /// <summary>
        /// Gets or sets the report name for display.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the report category name for display.
        /// </summary>
        public string Category { get; set; }
       
        
        /// <summary>
        /// Gets or sets the date and time the report was originally requested to be run.
        /// </summary>
        public DateTime RequestDate { get; set; }
      
        /// <summary>
        /// Gets or sets the user that requested the report, also used for data visibility
        /// functions.
        /// </summary>
        public User RequestedBy { get; set; }
        

        /// <summary>
        /// Gets or sets the report criteria (original or modified) that was supplied in
        /// order to run the report.
        /// </summary>
        public FilterCriteria Criteria { get; set; }
      

        /// <summary>
        /// Gets or sets the total time it took to complete in miliseconds.
        /// </summary>
        public double? CompletedIn { get; set; }
      

        /// <summary>
        /// Gets a value whether or not this report request was successfully run.
        /// </summary>
        public bool Success { get; set; }
       
        /// <summary>
        /// Gets or sets an error message for the report run request.
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// Result data in JSON format
        /// </summary>
        public dynamic JsonData { get; set; }


        /// <summary>
        /// chart related options
        /// </summary>
        public ReportChart Chart { get; set; }
       

        /// <summary>
        /// Gets or sets a value indicating whether to show the auto generated total counts
        /// at the bottom of the group page.
        /// </summary>
        /// <value><c>true</c> if group auto totals; otherwise, <c>false</c>.</value>
        public virtual bool GroupAutoTotals { get; set; }
      
        /// <summary>
        /// Adhoc Report Type
        /// </summary>
        public string AdHocReportType { get; set; }
        
        /// <summary>
        /// Represents the query used for the adhoc reports
        /// </summary>
        public string Query { get; set; }
       
        /// <summary>
        /// Shows the no. of records
        /// </summary>
        public long? RecordCount { get; set; }
       

    }

}