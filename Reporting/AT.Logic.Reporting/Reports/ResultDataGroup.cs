﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports
{
    [Serializable]
    public class ResultDataGroup
    {
        public ResultDataGroup()
        {
            Items = new List<ResultData>();
        }

        /// <summary>
        /// Gets or sets the group key value, which may be a number, string, etc.
        /// </summary>
        public object Key { get; set; }

        /// <summary>
        /// Gets or sets the group label that is displayed as the group text. May
        /// be the same as the key, however most of the time this is friendly where
        /// the key is a distinct value to be grouped on and not so friendly.
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Gets or sets the result data items for this group.
        /// </summary>
        public List<ResultData> Items { get; set; }

        /// <summary>
        /// The private accessor for the unique id, this is used so that we don't have
        /// to initialize a base constructor on parent classes, but serialization
        /// will automatically set this value through the setter on <c>Id</c>.
        /// </summary>
        [BsonIgnore]
        private Guid? _id;

        /// <summary>
        /// Gets or sets a unique identifier (GUID) for the instance so it can be easily identified
        /// in a list by external operations or called by external/internal reference.
        /// </summary>
        [BsonRequired, BsonRepresentation(BsonType.String)]
        public virtual Guid Id
        {
            get
            {
                if (_id == null)
                    _id = Guid.NewGuid();
                return _id.Value;
            }
            set { _id = value; }
        }
    }
}
