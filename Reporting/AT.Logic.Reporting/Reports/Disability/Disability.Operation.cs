﻿using AbsenceSoft.Data.Customers;
using AT.Entities.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.Disability
{
    public partial class DisabilityReport : BaseReport
    {

        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "D6DC483F-FC23-4244-8430-89E49ABE2578"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get; set; } = "Disability Report";

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get; set; }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get; set; }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report main category to bifurcate reports.
        /// </summary>
        public override string MainCategory { get; set; } = "Operations Report";
        /// <summary>
        /// return the desciprion 
        /// </summary>
        public override string ReportDescription
        {
            get { return "Lists diagnosis code and description by employee and case"; }
        }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report category for display.
        /// </summary>
        public override string Category { get; set; } = "Disability Report";

        /// <summary>
        /// When overridden in a derived class, gets name of the icon image to set in front end
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// Property to configure chart related options
        /// </summary>
        public override ReportChart Chart { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance is exportable to CSV.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is exportable to CSV; otherwise, <c>false</c>.
        /// </value>
        public override bool IsExportableToCsv { get { return true; } }

        /// <summary>
        /// Gets a value indicating whether this instance is exportable to PDF.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is exportable to PDF; otherwise, <c>false</c>.
        /// </value>
        public override bool IsExportableToPdf { get { return true; } }

        /// <summary>
        /// Gets a value indicating whether this instance is viewable.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is viewable; otherwise, <c>false</c>.
        /// </value>
        public override bool IsViewable { get { return true; } }

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether to show the auto
        /// generated total counts at the bottom of the group page.
        /// </summary>
        /// <value>
        ///   <c>true</c> if group auto totals; otherwise, <c>false</c>.
        /// </value>
        public override bool GroupAutoTotals { get; set; }

        /// <summary>
        /// List of fields for grouping
        /// </summary>
        public override Dictionary<string, List<string>> AvailableGroupingFields => new Dictionary<string, List<string>>(){
            {(ReportGroupType.Condition).ToString(), new List<string>(0)},
            {(ReportGroupType.ConditionByCondition).ToString(), new List<string>(0)},
            {(ReportGroupType.ConditionByOrganization).ToString(), new List<string>(0)},
            {(ReportGroupType.Duration).ToString(), new List<string>(0)},
            {(ReportGroupType.DurationByCondition).ToString(), new List<string>(0)},
            {(ReportGroupType.DurationByOrganization).ToString(), new List<string>(0)}
        };

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected async Task<ReportResult> DefaultRunReport(AT.Entities.Authentication.User user, ReportResult result, int pageNumber, int pageSize)
        {
            var cases = RunQuery(user, result.Criteria, pageNumber, pageSize);
            result.Items = cases.Select(c => GetResultData(user, c)).Where(c => c != null).ToList();
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Report by Condition
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        protected async Task<ReportResult> ReportByCondition(AT.Entities.Authentication.User user, ReportResult result, int pageNumber, int pageSize)
        {
            var cases = RunQuery(user, result.Criteria, pageNumber, pageSize)
           .Where(c => c.IsSTD
               && c.Disability != null
               && c.Disability.PrimaryDiagnosis != null
               && c.Disability.PrimaryDiagnosis != null)
           .OrderBy(c => c.Disability.PrimaryDiagnosis.Code)
           .GroupBy(c => c.Disability.PrimaryDiagnosis.Code)
           .ToList();

            result.Groups = cases.Select(c =>
            {
                var items = c.Select(a => GetResultData(user, a)).Where(a => a != null).ToList();
                if (!items.Any())
                {
                    return null;
                }
                return new ResultDataGroup()
                {
                    Key = c.Key,
                    Label = c.First().Disability.PrimaryDiagnosis.UIDescription,
                    Items = items
                };
            }).Where(g => g != null).ToList();
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Report by Organization
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        protected async Task<ReportResult> ReportByOrganization(AT.Entities.Authentication.User user, ReportResult result, int pageNumber, int pageSize)
        {
            var cases = RunQuery(user, result.Criteria, pageNumber, pageSize);
            var userData = GetUserReportCriteriaData(user);
            List<string> customerIds = cases.Select(cs => cs.CustomerId).Distinct().ToList();
            List<string> employerIds = cases.Select(cs => cs.EmployerId).Distinct().ToList();
            List<string> employeeNumbers = cases.Select(cs => cs.Employee.EmployeeNumber).Distinct().ToList();
            List<EmployeeOrganization> employeeOrganizations = null;

            employeeOrganizations = SetEmployeeOrganization(result, customerIds, employerIds, employeeNumbers);
            List<EmployeeOrganization> employeeUniqueOrganizations = GetUniqueEmployeeOrganizations(employeeOrganizations);
            await SetResultDataGroup(userData.SecurityUser, result, cases, employeeUniqueOrganizations);
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Runs the Disability Duration Report By Organization
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public async Task<ReportResult> RunDisabilityDurationReportByOrganization(AT.Entities.Authentication.User user, ReportResult result, int pageNumber, int pageSize)
        {
            var cases = RunQuery(user, result.Criteria, pageNumber, pageSize);
            var minExceedsDurationByCriteria = (result.Criteria.Filters ?? new List<ReportCriteriaItem>(0)).FirstOrDefault(c => c.Name == "ExceedsDurationBy");
            int? minExceedsDurationBy = minExceedsDurationByCriteria?.ValueAs<int?>();
            var userData = GetUserReportCriteriaData(user);

            string grouptypeCode = null;
            if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "OrganizationTypeGrouping") != null)
            {
                grouptypeCode = result.Criteria.ValueOf<string>("OrganizationTypeGrouping");
            }

            List<string> customerIds = cases.Select(cs => cs.CustomerId).Distinct().ToList();
            List<string> employerIds = cases.Select(cs => cs.EmployerId).Distinct().ToList();
            List<string> employeeNumbers = cases.Select(cs => cs.Employee.EmployeeNumber).Distinct().ToList();
            List<EmployeeOrganization> employeeOrganizations = null;
            if (grouptypeCode != null)
            {
                employeeOrganizations = EmployeeOrganization.AsQueryable().Where(org => org.TypeCode == grouptypeCode && customerIds.Contains(org.CustomerId) && employerIds.Contains(org.EmployerId) && employeeNumbers.Contains(org.EmployeeNumber)).OrderByDescending(o => o.Dates).ToList();
            }
            else
            {
                employeeOrganizations = EmployeeOrganization.AsQueryable().Where(org => customerIds.Contains(org.CustomerId) && employerIds.Contains(org.EmployerId) && employerIds.Contains(org.EmployerId) && employeeNumbers.Contains(org.EmployeeNumber)).OrderByDescending(o => o.Dates).ToList();
            }
            List<EmployeeOrganization> employeeUniqueOrganizations = new List<EmployeeOrganization>();
            foreach (EmployeeOrganization empOrg in employeeOrganizations)
            {
                if (empOrg.TypeCode != OrganizationType.OfficeLocationTypeCode ||
                    ((empOrg.Dates == null || empOrg.Dates.IncludesToday(true))
                    && (!employeeUniqueOrganizations.Any(empUniqueOrg => empUniqueOrg.TypeCode == OrganizationType.OfficeLocationTypeCode && empUniqueOrg.EmployeeNumber == empOrg.EmployeeNumber && empUniqueOrg.CustomerId == empOrg.CustomerId && empUniqueOrg.EmployerId == empOrg.EmployerId))))
                {
                    employeeUniqueOrganizations.Add(empOrg);
                }
            }
            await SetResultDataGroup(result, cases, minExceedsDurationBy, userData, employeeUniqueOrganizations);
            return result;
        }

        /// <summary>
        /// Runs Disability Report By Condition
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public async Task<ReportResult> RunDisabilityReportByCondition(AT.Entities.Authentication.User user, ReportResult result, int pageNumber, int pageSize)
        {
            var cases = RunQuery(user, result.Criteria, pageNumber, pageSize)
                .Where(c => c.IsSTD
                    && c.Disability != null
                    && c.Disability.PrimaryDiagnosis != null
                    && c.Disability.PrimaryDiagnosis != null)
                .OrderBy(c => c.Disability.PrimaryDiagnosis.Code)
                .GroupBy(c => c.Disability.PrimaryDiagnosis.Code)
                .ToList();

            var minExceedsDurationByCriteria = (result.Criteria.Filters ?? new List<ReportCriteriaItem>(0)).FirstOrDefault(c => c.Name == "ExceedsDurationBy");
            int? minExceedsDurationBy = minExceedsDurationByCriteria?.ValueAs<int?>();
            var data = GetUserReportCriteriaData(user);

            SetResultDataGroup(result, cases, minExceedsDurationBy, data);

            var getInts = new Func<string, List<ResultData>, List<int>>((field, items) =>
            {
                if (string.IsNullOrWhiteSpace(field) || items == null || !items.Any())
                {
                    return new List<int>(0);
                }

                return items.Select(i =>
                {
                    string x = (!i.ContainsKey(field) || i[field] == null ? null : i[field].ToString());
                    if (!string.IsNullOrWhiteSpace(x) && int.TryParse(x.Replace(" days", ""), out int e))
                    {
                        return e;
                    }
                    return 0;
                }).ToList();
            });

            result.Groups.Insert(0, new ResultDataGroup()
            {
                Key = "Summary",
                Label = "Summary",
                Items = result.Groups.Select(g =>
                {
                    ResultData rd = new ResultData
                    {
                        ["Diagnosis"] = g.Label,
                        ["TotalCases"] = g.Items.Count.ToString("N0")
                    };
                    List<int> durations = getInts("ActualDuration", g.Items);
                    rd["MinDuration"] = string.Format("{0:N0} days", durations.Min());
                    rd["MaxDuration"] = string.Format("{0:N0} days", durations.Max());
                    rd["AvgDuration"] = string.Format("{0:N2} days", durations.Average());
                    rd["AvgExceeded"] = string.Format("{0:N2} days", getInts("Exceeded", g.Items).Average());
                    return rd;
                }).ToList()
            });
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Runs Disability Report for User and populates ReportResult
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        public async Task<ReportResult> RunDisabilityReport(AT.Entities.Authentication.User user, ReportResult result, int pageNumber, int pageSize)
        {
            var cases = RunQuery(user, result.Criteria, pageNumber, pageSize);
            var minExceedsDurationByCriteria = (result.Criteria.Filters ?? new List<ReportCriteriaItem>(0)).FirstOrDefault(c => c.Name == "ExceedsDurationBy");
            int? minExceedsDurationBy = minExceedsDurationByCriteria?.ValueAs<int?>();
            var data = GetUserReportCriteriaData(user);
            result.Items = cases.Select(c => GetResultData(data.SecurityUser, c, minExceedsDurationBy)).Where(c => c != null).ToList();
            return await Task.FromResult(result);
        }
    }
}