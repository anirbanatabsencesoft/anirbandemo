﻿using AbsenceSoft;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AT.Entities.Reporting.Enums;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.Disability
{
    [Serializable]
    public partial class DisabilityReport : BaseReport
    {
        /// <summary>
        /// Gets the ResultData for the different case conditions
        /// </summary>
        /// <param name="user"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        protected ResultData GetResultData(AT.Entities.Authentication.User user, Case c)
        {
            if (!c.IsSTD || c.Disability == null || c.Disability.PrimaryDiagnosis == null)
            {
                return null;
            }

            var userData = GetUserReportCriteriaData(user);

            dynamic data = new ResultData();

            if (userData.SecurityUser.Customer.HasFeature(Feature.MultiEmployerAccess) && user.Employers.Count > 1)
            {
                data.Employer = c.EmployerName;
            }
            data.EmployeeID = string.IsNullOrWhiteSpace(c.Employee.EmployeeNumber) ? null : new LinkItem() { Value = c.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", c.Employee.Id) };
            data.Employee = string.IsNullOrWhiteSpace(c.Employee.FullName) ? null : new LinkItem() { Value = c.Employee.FullName, Link = string.Format("/Employees/{0}/View", c.Employee.Id) };
            data.CaseNumber = string.IsNullOrWhiteSpace(c.CaseNumber) ? null : new LinkItem() { Value = c.CaseNumber, Link = string.Format("/Cases/{0}/View", c.Id) };
            data.StartDate = c.StartDate.ToString("MM/dd/yyyy");
            data.EndDate = c.EndDate.ToString("MM/dd/yyyy");
            data.Condition = c.Disability.PrimaryDiagnosis.Code ?? "";
            data.Description = c.Disability.PrimaryDiagnosis.Description ?? "";
#pragma warning disable CS0618 // Type or member is obsolete
            data.Location = c.Employee.Info.OfficeLocation;
#pragma warning restore CS0618 // Type or member is obsolete
            data.State = c.Employee.WorkState ?? "";

            return data;
        }

        /// <summary>
        /// Gets the result data.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="c">The c.</param>
        /// <returns></returns>
        protected ResultData GetResultData(AbsenceSoft.Data.Security.User user, Case c, int? minExceedsDurationBy = null)
        {
            if (!c.IsSTD || c.Disability == null || c.Disability.PrimaryDiagnosis == null || c.Summary == null || c.Summary.Policies == null || !c.Summary.Policies.Any())
            {
                return null;
            }

            // Get all approved STD policies
            //  if at any point we don't have any data to use/show, then bail and return null
            // Get all of the STD policy IDs on the case so that we can grab the summary info for them
            var codes = c.Segments.SelectMany(s => s.AppliedPolicies.Where(p => p.Policy.PolicyType == PolicyType.STD)).Select(p => p.Policy.Code).Distinct().ToList();
            if (!codes.Any())
            {
                return null;
            }
            // Get the policy summary for all of the STD policies on the case
            var policies = c.Summary.Policies.Where(p => codes.Contains(p.PolicyCode)).ToList();
            if (!policies.Any())
            {
                return null;
            }
            // Get a merged collection of all policy detail where the status is approved
            var detail = policies.SelectMany(p => p.Detail.Where(d => d.Determination == AdjudicationSummaryStatus.Approved)).ToList();
            if (!detail.Any())
            {
                return null;
            }
            return SetData(user, c, minExceedsDurationBy, detail);
        }
        
        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns>LOA Feature</returns>
        public override IEnumerable<Feature> RequiredFeatures()
        {
            yield return Feature.LOA;
            yield return Feature.ShortTermDisability;
            yield return Feature.GuidelinesData;
        }
                       
        /// <summary>
        /// Runs the query and returns the list of cases that the query applies to.
        /// </summary>
        /// <param name="user">The user to run the query for.</param>
        /// <param name="criteria">The criteria to apply to the query.</param>
        /// <returns>A list of cases found by the query.</returns>
        protected List<Case> RunQuery(AT.Entities.Authentication.User user, ReportCriteria criteria, int pageNumber = 1, int pageSize = 10)
        {
            var data = GetUserReportCriteriaData(user);
            ReportingLogic reportingLogic = new ReportingLogic(user);
            
            return reportingLogic.GetCaseListForDisabilityReport(data, Utilities.CreateReportingCriteria(criteria), EmployerIdFilterName, pageNumber, pageSize).GetAwaiter().GetResult();
        }

        /// <summary>     
        /// Runs Report in Async way
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override async Task<ReportResult> RunReportAsync(AT.Entities.Authentication.User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            switch (reportGroupType)
            {
                default:
                case ReportGroupType.Condition:
                    {
                        result.Name = "Disability Conditions";
                        IsGrouped = false;
                        CategoryOrder = 1;
                        GroupAutoTotals = false;
                        await DefaultRunReport(user, result, pageNumber, pageSize);
                        break;
                    }
                case ReportGroupType.ConditionByCondition:
                    {
                        result.Name = "Disability Conditions by Condition";
                        IsGrouped = true;
                        CategoryOrder = 2;
                        GroupAutoTotals = false;
                        await ReportByCondition(user, result, pageNumber, pageSize);
                        break;
                    }
                case ReportGroupType.ConditionByOrganization:
                    {
                        result.Name = "Disability Conditions by Organization";
                        IsGrouped = true;
                        CategoryOrder = 3;
                        GroupAutoTotals = false;
                        await ReportByOrganization(user, result, pageNumber, pageSize);
                        break;
                    }
                case ReportGroupType.Duration:
                    {
                        result.Name = "Disability Duration";
                        IsGrouped = false;
                        CategoryOrder = 4;
                        GroupAutoTotals = false;
                        await RunDisabilityReport(user, result, pageNumber, pageSize);
                        break;
                    }
                case ReportGroupType.DurationByCondition:
                    {
                        result.Name = "Disability Duration by Condition";
                        IsGrouped = true;
                        CategoryOrder = 5;
                        GroupAutoTotals = false;
                        await RunDisabilityReportByCondition(user, result, pageNumber, pageSize);
                        break;
                    }
                case ReportGroupType.DurationByOrganization:
                    {
                        result.Name = "Disability Duration by Organization";
                        IsGrouped = true;
                        CategoryOrder = 6;
                        GroupAutoTotals = false;
                        await RunDisabilityDurationReportByOrganization(user, result, pageNumber, pageSize);
                        break;
                    }
            }
            return result;
        }

        /// <summary>
        /// Sets the ResultDataGroup
        /// </summary>
        /// <param name="result"></param>
        /// <param name="cases"></param>
        /// <param name="minExceedsDurationBy"></param>
        /// <param name="userData"></param>
        /// <param name="employeeUniqueOrganizations"></param>
        /// <returns></returns>
        private async Task<ReportResult> SetResultDataGroup(ReportResult result, List<Case> cases, int? minExceedsDurationBy, Data.Reporting.Model.UserReportCriteriaData userData, List<EmployeeOrganization> employeeUniqueOrganizations)
        {
            var groupQuery = employeeUniqueOrganizations.GroupBy(org => new { CustomerId = org.CustomerId, EmployerId = org.EmployerId, Code = org.Code })
                       .Select(g => new
                       {
                           label = employeeUniqueOrganizations.FirstOrDefault(empOrgs => empOrgs.Code == g.Key.Code).Name,
                           data = g.Select(x => x).ToList()
                       }).OrderBy(m => m.label);

            result.Groups = new List<ResultDataGroup>();
            List<ResultData> resultItems;
            int keyCount = 1;
            //Avoid displaying cases under more than one office location code.
            List<string> empLocationCase = new List<string>();
            foreach (var t in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (var org in t.data)
                {
                    SetEmployeeLocationCase(cases, minExceedsDurationBy, userData, resultItems, empLocationCase, org);
                }
                if (resultItems.Any())
                {
                    result.Groups.Add(new ResultDataGroup()
                    {
                        Key = keyCount,
                        Label = t.label,
                        Items = new List<ResultData>().AddRangeFluid(resultItems)
                    });
                    keyCount++;
                }
            }
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Sets the Employee Location Case
        /// </summary>
        /// <param name="cases"></param>
        /// <param name="minExceedsDurationBy"></param>
        /// <param name="userData"></param>
        /// <param name="resultItems"></param>
        /// <param name="empLocationCase"></param>
        /// <param name="org"></param>
        private void SetEmployeeLocationCase(List<Case> cases, int? minExceedsDurationBy, Data.Reporting.Model.UserReportCriteriaData userData, List<ResultData> resultItems, List<string> empLocationCase, EmployeeOrganization org)
        {
            foreach (Case td in cases.Where(cs => cs.Employee.EmployeeNumber == org.EmployeeNumber && cs.CustomerId == org.CustomerId && cs.EmployerId == org.EmployerId))
            {
                if (!string.IsNullOrWhiteSpace(org.TypeCode) && org.TypeCode.ToUpper().TrimEnd() == OrganizationType.OfficeLocationTypeCode.ToUpper().TrimEnd() && empLocationCase.Contains(td.Id.ToUpper()))
                {
                    continue;
                }
                ResultData data = GetResultData(userData.SecurityUser, td, minExceedsDurationBy);
                if (data != null)
                {
                    resultItems.Add(data);
                }
                if (!string.IsNullOrWhiteSpace(org.TypeCode) && org.TypeCode.ToUpper().TrimEnd() == OrganizationType.OfficeLocationTypeCode.ToUpper().TrimEnd() && (!empLocationCase.Contains(td.Id.ToUpper())))
                {
                    empLocationCase.Add(td.Id.ToUpper());
                }
            }
        }

        /// <summary>
        ///Sets Employee Organization 
        /// </summary>
        /// <param name="result"></param>
        /// <param name="customerIds"></param>
        /// <param name="employerIds"></param>
        /// <param name="employeeNumbers"></param>
        /// <returns></returns>
        private List<EmployeeOrganization> SetEmployeeOrganization(ReportResult result, List<string> customerIds, List<string> employerIds, List<string> employeeNumbers)
        {
            string grouptypeCode = null;
            List<EmployeeOrganization> employeeOrganizations;
            if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "OrganizationTypeGrouping") != null)
            {
                grouptypeCode = result.Criteria.ValueOf<string>("OrganizationTypeGrouping");
            }
            if (grouptypeCode != null)
            {
                employeeOrganizations = EmployeeOrganization.AsQueryable().Where(org => org.TypeCode == grouptypeCode && customerIds.Contains(org.CustomerId) && employerIds.Contains(org.EmployerId) && employeeNumbers.Contains(org.EmployeeNumber)).OrderByDescending(o => o.Dates).ToList();
            }
            else
            {
                employeeOrganizations = EmployeeOrganization.AsQueryable().Where(org => customerIds.Contains(org.CustomerId) && employerIds.Contains(org.EmployerId) && employerIds.Contains(org.EmployerId) && employeeNumbers.Contains(org.EmployeeNumber)).OrderByDescending(o => o.Dates).ToList();
            }

            return employeeOrganizations;
        }
        
        /// <summary>
        /// Get Unique Employee Organizations from the passed List of Organizations
        /// </summary>
        /// <param name="employeeOrganizations"></param>
        /// <returns></returns>
        private List<EmployeeOrganization> GetUniqueEmployeeOrganizations(List<EmployeeOrganization> employeeOrganizations)
        {
            List<EmployeeOrganization> employeeUniqueOrganizations = new List<EmployeeOrganization>();
            foreach (EmployeeOrganization empOrg in employeeOrganizations)
            {
                if (empOrg.TypeCode != OrganizationType.OfficeLocationTypeCode || ((empOrg.Dates == null || empOrg.Dates.IncludesToday(true))
                    && (!employeeUniqueOrganizations.Select(empUniqueOrg => empUniqueOrg.TypeCode == OrganizationType.OfficeLocationTypeCode && empUniqueOrg.EmployeeNumber == empOrg.EmployeeNumber && empUniqueOrg.CustomerId == empOrg.CustomerId && empUniqueOrg.EmployerId == empOrg.EmployerId).Any())))
                {
                    employeeUniqueOrganizations.Add(empOrg);
                }
            }

            return employeeUniqueOrganizations;
        }
        
        /// <summary>
        /// Sets the Employee Location
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cases"></param>
        /// <param name="resultItems"></param>
        /// <param name="empLocationCase"></param>
        /// <param name="org"></param>
        private void SetEmployeeLocation(User user, List<Case> cases, List<ResultData> resultItems, List<string> empLocationCase, EmployeeOrganization org)
        {
            foreach (Case td in cases.Where(cs => cs.Employee.EmployeeNumber == org.EmployeeNumber && cs.CustomerId == org.CustomerId && cs.EmployerId == org.EmployerId))
            {
                if (org.TypeCode.ToUpper().TrimEnd() == OrganizationType.OfficeLocationTypeCode.ToUpper().TrimEnd() && empLocationCase.Contains(td.Id.ToUpper()))
                {
                    continue;
                }
                ResultData data = GetResultData(user, td);
                if (data != null)
                {
                    resultItems.Add(data);
                }
                if (org.TypeCode.ToUpper().TrimEnd() == OrganizationType.OfficeLocationTypeCode.ToUpper().TrimEnd() && (!empLocationCase.Contains(td.Id.ToUpper())))
                {
                    empLocationCase.Add(td.Id.ToUpper());
                }
            }
        }

        /// <summary>
        /// Sets the ResultDataGroup
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="cases"></param>
        /// <param name="employeeUniqueOrganizations"></param>
        /// <returns></returns>
        private async Task<ReportResult> SetResultDataGroup(User user, ReportResult result, List<Case> cases, List<EmployeeOrganization> employeeUniqueOrganizations)
        {
            List<ResultData> resultItems;
            var groupQuery = employeeUniqueOrganizations.GroupBy(org => new { CustomerId = org.CustomerId, EmployerId = org.EmployerId, Code = org.Code })
               .Select(g => new
               {
                   label = employeeUniqueOrganizations.FirstOrDefault(empOrgs => empOrgs.Code == g.Key.Code).Name,
                   data = g.Select(x => x).ToList()
               }).OrderBy(m => m.label);

            result.Groups = new List<ResultDataGroup>();
            int keyCount = 1;
            //Avoid displaying cases under more than one office location code.
            List<string> empLocationCase = new List<string>();
            foreach (var t in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (var org in t.data)
                {
                    SetEmployeeLocation(user, cases, resultItems, empLocationCase, org);
                }
                if (resultItems.Any())
                {
                    result.Groups.Add(new ResultDataGroup()
                    {
                        Key = keyCount,
                        Label = t.label,
                        Items = new List<ResultData>().AddRangeFluid(resultItems)
                    });
                    keyCount++;
                }
            }
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Sets the ResultDataGroup
        /// </summary>
        /// <param name="result"></param>
        /// <param name="cases"></param>
        /// <param name="minExceedsDurationBy"></param>
        /// <param name="data"></param>
        private void SetResultDataGroup(ReportResult result, List<IGrouping<string, AbsenceSoft.Data.Cases.Case>> cases, int? minExceedsDurationBy, Data.Reporting.Model.UserReportCriteriaData data)
        {
            result.Groups = cases.Select(c =>
            {
                var items = c.Select(a => GetResultData(data.SecurityUser, a, minExceedsDurationBy)).Where(a => a != null).ToList();
                if (!items.Any())
                {
                    return null;
                }
                return new ResultDataGroup()
                {
                    Key = c.Key,
                    Label = c.First().Disability.PrimaryDiagnosis.UIDescription,
                    Items = items
                };
            }).Where(g => g != null).ToList();
        }

        /// <summary>
        /// Set the ResultData
        /// </summary>
        /// <param name="user"></param>
        /// <param name="c"></param>
        /// <param name="minExceedsDurationBy"></param>
        /// <param name="detail"></param>
        /// <returns></returns>
        private ResultData SetData(User user, Case c, int? minExceedsDurationBy, List<CaseSummaryPolicyDetail> detail)
        {
            dynamic data = new ResultData();

            // Standard stuff here
            if (user.Customer.HasFeature(Feature.MultiEmployerAccess) && user.Employers.Count > 1)
            {
                data.Employer = c.EmployerName;
            }
            data.EmployeeID = string.IsNullOrWhiteSpace(c.Employee.EmployeeNumber) ? null : new LinkItem() { Value = c.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", c.Employee.Id) };
            data.Employee = string.IsNullOrWhiteSpace(c.Employee.FullName) ? null : new LinkItem() { Value = c.Employee.FullName, Link = string.Format("/Employees/{0}/View", c.Employee.Id) };
            data.CaseNumber = string.IsNullOrWhiteSpace(c.CaseNumber) ? null : new LinkItem() { Value = c.CaseNumber, Link = string.Format("/Cases/{0}/View", c.Id) };

            // Get the absolutely first approved STD date as our start date
            DateTime stdStartDate = detail.Min(d => d.StartDate);
            // Set the start date, end date, condition and description (standard STD stuff)
            data.StartDate = stdStartDate.ToUIString();
            data.EndDate = detail.Max(d => d.EndDate).ToUIString();
            data.Condition = c.Disability.PrimaryDiagnosis.Code ?? "";
            data.Description = c.Disability.PrimaryDiagnosis.Description ?? "";

            // We need to know what the expected duration in days was for our diagnosis
            int? diagDur = null;
            // First, determine if we even have a max duration value for this diagnosis code, if not, ignore it
            if (c.Disability.PrimaryPathMaxDays.HasValue)
            {
                // Get the total number of days by getting the current max duration + type and doing a unit conversion to days
                diagDur = c.Disability.PrimaryPathMaxDays.Value;
                data.ExpectedDuration = string.Format("{0:N0} days", diagDur);
            }
            else
            {
                data.ExpectedDuration = "";
            }

            // Get the actual duration of all approved STD policies' approval periods only, factoring overlap and gaps to get an accurate range of days
            //  If the leave is perpetual, then it will be zero
            int duration = Convert.ToInt32(Math.Ceiling(Date.GetCumulativeTimeframeMinusGapsAndOverlap(detail.Select(d => new DateRange(d.StartDate, d.EndDate)).ToArray()).TotalDays));
            data.ActualDuration = string.Format("{0:N0} days", duration);

            // Determine if we've exceeded the max duration and if so, note it, otherwise set this to zero
            int diff = 0;
            diff = SetExceeded(data, diagDur, duration, diff);

            // Whoops, if they said AT LEAST this many days exceeding and we don't meet
            //  that criteria, this late binding criteria will filter out the result and we
            //  need to bail and return zero.
            if (minExceedsDurationBy.HasValue && minExceedsDurationBy.Value > diff)
            {
                return null;
            }

            SetExpectedEndDate(data, stdStartDate, diagDur);

            // More typical stuff
#pragma warning disable CS0618 // Type or member is obsolete
            data.Location = c.Employee.Info.OfficeLocation;
#pragma warning restore CS0618 // Type or member is obsolete
            data.State = c.Employee.WorkState ?? "";

            return data;
        }

        /// <summary>
        /// Sets ExpectedEndDate
        /// </summary>
        /// <param name="data"></param>
        /// <param name="stdStartDate"></param>
        /// <param name="diagDur"></param>
        private void SetExpectedEndDate(dynamic data, DateTime stdStartDate, int? diagDur)
        {
            // Calculate our expected end date based on the approved start date + total number of days
            //  from the max duration, if provided, otherwise blank
            if (diagDur.HasValue)
            {
                data.ExpectedEndDate = stdStartDate.AddDays(diagDur.Value).ToUIString();
            }
            else
            {
                data.ExpectedEndDate = "";
            }
        }

        /// <summary>
        /// Sets Exceeded value
        /// </summary>
        /// <param name="data"></param>
        /// <param name="diagDur"></param>
        /// <param name="duration"></param>
        /// <param name="diff"></param>
        /// <returns></returns>
        private int SetExceeded(dynamic data, int? diagDur, int duration, int diff)
        {
            if (diagDur.HasValue)
            {
                diff = duration - diagDur.Value;
                if (diff <= 0)
                {
                    data.Exceeded = "0 days";
                }
                else
                {
                    data.Exceeded = string.Format("{0:N0} days", diff);
                }
            }
            else
            {
                data.Exceeded = "";
            }
            return diff;
        }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(AT.Entities.Authentication.User user)
        {
            var data = GetUserReportCriteriaData(user);

            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerId.ToString(),
                StartDate = DateTime.UtcNow.Date.AddDays(-30),
                EndDate = DateTime.UtcNow.Date
            };

            ReportCriteriaItem item = GetDiagnosisCodeCriteria(data);
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            criteria.Filters.Add(GetWorkStateCriteria(data, criteria.StartDate, criteria.EndDate));
            criteria.Filters.AddRange(GetOrganizationTypeCriteria(data));
            if (IncludeOrganizationGrouping)
            {
                criteria.Filters.Add(GetOrganizationTypeGroupByCriteria(data));
            }
            return criteria;
        }
    }
}