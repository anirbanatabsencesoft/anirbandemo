﻿using AbsenceSoft;
using AT.Data.Reporting.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.ToDo
{
    public partial class CasesWithoutToDoReport : BaseReport
    {
        
        protected virtual ResultData GetResultData(ToDoMapReduceResultItem item)
        {
            var v = item.value.Case;
            dynamic data = new ResultData();
            data.CaseNumber = new LinkItem() { Value = v.CaseNumber, Link = string.Format("/Cases/{0}/View", v.Id) };
            data.Name = new LinkItem() { Value = v.Employee.FullName, Link = string.Format("/Employees/{0}/View", v.Employee.Id) };
            data.EmployeeNumber = new LinkItem() { Value = v.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", v.Employee.Id) };
            data.Reason = v.Reason == null ? string.Empty : v.Reason.Name;
            data.CaseType = v.CaseType;
            data.CreatedOn = v.CreatedDate.ToString("MM/dd/yyyy");
            data.Status = v.Status.ToString().SplitCamelCaseString();
            data.AssignedTo = v.AssignedToName ?? string.Empty;
            #pragma warning disable CS0618
            data.Location = v.Employee.Info.OfficeLocation ?? string.Empty;
            #pragma warning restore CS0618
            return (ResultData)data;
        }
    }
}
