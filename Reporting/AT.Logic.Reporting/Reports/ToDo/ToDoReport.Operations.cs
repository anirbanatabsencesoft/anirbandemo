﻿using AbsenceSoft;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Authentication;
using MongoDB.Driver;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.ToDo;
using AT.Data.Reporting.Model;
using AbsenceSoft.Data;
using MongoDB.Bson;
using AbsenceSoft.Data.Customers;

namespace AT.Logic.Reporting.Reports.ToDo
{
    public partial class ToDoReport : BaseReport
    {
        
        /// <summary>
        /// Gets the result.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        protected virtual ResultData GetResultData(ToDoItem item)
        {
            
            bool isClosed = item.Status == ToDoItemStatus.Complete || item.Status == ToDoItemStatus.Cancelled;

            dynamic data = new ResultData();
            data.CaseNumber = item.CaseId == null ? (object)"" : new LinkItem() { Value = item.CaseNumber ?? "View Case", Link = string.Format("/Cases/{0}/View", item.CaseId) };
            data.EmployeeNumber = new LinkItem() { Value = item.EmployeeNumber ?? "View Employee", Link = string.Format("/Employees/{0}/View", item.EmployeeId) };
            data.EmployeeName = new LinkItem() { Value = item.EmployeeName ?? "View Employee", Link = string.Format("/Employees/{0}/View", item.EmployeeId) };
            data.AssignedTo = item.AssignedToName ?? string.Empty;
            data.ToDo = item.Title ?? item.ItemType.ToString().SplitCamelCaseString();
            data.DueBy = item.DueDate.ToString("MM/dd/yyyy");
            data.ClosedOn = isClosed ? item.ModifiedDate.ToString("MM/dd/yyyy") : string.Empty;
            data.ClosedBy = isClosed ? item.ModifiedBy?.DisplayName :"" ;
            data.Status = item.DueDate.Date < DateTime.Now.Date && item.Status != ToDoItemStatus.Overdue && item.Status != ToDoItemStatus.Complete && item.Status != ToDoItemStatus.Cancelled ? ToDoItemStatus.Overdue.ToString() : item.DueDate.Date >= DateTime.Now.Date && item.Status == ToDoItemStatus.Overdue ? ToDoItemStatus.Pending.ToString() : item.Status.ToString();
            #pragma warning disable CS0618
            data.Location = item.Employee.Info.OfficeLocation?? "";
            #pragma warning restore CS0618
            return data;
        }


        /// <summary>
        /// Report by Organization
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        protected async Task<ReportResult> ReportByOrganization(User user, List<ToDoItem> toDos, ReportResult result)
        {
            List<string> customerIds = toDos?.Select(cs => cs.CustomerId).Distinct().ToList();
            List<string> employerIds = toDos?.Select(cs => cs.EmployerId).Distinct().ToList();
            List<string> employeeNumbers = toDos?.Select(cs => cs.EmployeeNumber).Distinct().ToList();
            List<EmployeeOrganization> employeeOrganizations = null;

            ReportingLogic reportingLogic = new ReportingLogic(user);

            string groupTypeCode = null;
            if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "OrganizationTypeGrouping") != null)
            {
                groupTypeCode = result.Criteria.ValueOf<string>("OrganizationTypeGrouping");
            }
            employeeOrganizations = await reportingLogic.GetEmployeeOrganization(groupTypeCode, customerIds, employerIds, employeeNumbers);
            List<EmployeeOrganization> employeeUniqueOrganizations = GetUniqueEmployeeOrganizations(employeeOrganizations);
            await SetResultDataGroup( result, toDos, employeeUniqueOrganizations, groupTypeCode);
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Default details report function returns the result without grouping.
        /// It does allows populating the employee office location with the result.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="toDos"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        private async Task<ReportResult> ReportDetail(List<ToDoItem> toDos, ReportResult result)
        {
            result.Name = "Detail";
            result.Items = toDos.Select(c => GetResultData(c)).Where(c => c != null).ToList();
            return await Task.FromResult(result);
        }
        /// <summary>
        /// Get Unique Employee Organizations from the passed List of Organizations
        /// </summary>
        /// <param name="employeeOrganizations"></param>
        /// <returns></returns>
        private List<EmployeeOrganization> GetUniqueEmployeeOrganizations(List<EmployeeOrganization> employeeOrganizations)
        {
            List<EmployeeOrganization> employeeUniqueOrganizations = new List<EmployeeOrganization>();
            foreach (EmployeeOrganization empOrg in employeeOrganizations)
            {
                if (empOrg.TypeCode != OrganizationType.OfficeLocationTypeCode || ((empOrg.Dates == null || empOrg.Dates.IncludesToday(true))
                    && (!employeeUniqueOrganizations.Select(empUniqueOrg => empUniqueOrg.TypeCode == OrganizationType.OfficeLocationTypeCode && empUniqueOrg.EmployeeNumber == empOrg.EmployeeNumber && empUniqueOrg.CustomerId == empOrg.CustomerId && empUniqueOrg.EmployerId == empOrg.EmployerId).Any())))
                {
                    employeeUniqueOrganizations.Add(empOrg);
                }
            }

            return employeeUniqueOrganizations;
        }

        /// <summary>
        /// method populates the employee numbers and employee names if they are empty cooresponding to their employeeid.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="toDos"></param>
        /// <returns></returns>
        private async Task SetEmployeeIfNull(User user, List<ToDoItem> toDos)
        {
            List<string> employeeIds = toDos?.Where(p=>string.IsNullOrWhiteSpace(p.EmployeeName) || string.IsNullOrWhiteSpace(p.EmployeeNumber)).Select(cs => cs.EmployeeId).Distinct().ToList();
            if (employeeIds?.Count > 0)
            {
                ReportingLogic reportingLogic = new ReportingLogic(user);
                var employees = await reportingLogic.GetEmployeeDetailsAsync(employeeIds);
                //populate toDos If null
                var empToDos = toDos.Where(p => employeeIds.Contains(p.EmployeeId));
                empToDos.ForEach(p =>
                    {
                        p.EmployeeNumber = employees[p.EmployeeId].EmployeeNumber;
                        p.EmployeeName = employees[p.EmployeeId].FirstName + ", " + employees[p.EmployeeId].LastName;
                    }
                );
            }
        }
        /// <summary>
        /// Sets the Employee Location
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cases"></param>
        /// <param name="resultItems"></param>
        /// <param name="empLocationToDo"></param>
        /// <param name="org"></param>
        private void SetEmployeeLocation(List<ToDoItem> ToDoItems, List<ResultData> resultItems, List<string> empLocationToDo, EmployeeOrganization org, List<string>  empOrganizationToDo, string grouptypeCode)
        {
            foreach (ToDoItem td in ToDoItems?.Where(cs => cs.EmployeeNumber == org.EmployeeNumber && cs.CustomerId == org.CustomerId && cs.EmployerId == org.EmployerId))
            {
                if (org.TypeCode.ToUpper().TrimEnd() == OrganizationType.OfficeLocationTypeCode.ToUpper().TrimEnd() && empLocationToDo.Contains(td.Id.ToUpper()))
                {
                    continue;
                }
                ResultData data = GetResultData(td);
                if (data != null)
                {
                    resultItems.Add(data);
                }
                if (grouptypeCode == null)
                {
                    empOrganizationToDo.Add(td.Id);
                }
                if (org.TypeCode.ToUpper().TrimEnd() == OrganizationType.OfficeLocationTypeCode.ToUpper().TrimEnd() && (!empLocationToDo.Contains(td.Id.ToUpper())))
                {
                    empLocationToDo.Add(td.Id.ToUpper());
                }
            }
        }

        /// <summary>
        /// Sets the ResultDataGroup
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="toDos"></param>
        /// <param name="employeeUniqueOrganizations"></param>
        /// <returns></returns>
        private async Task<ReportResult> SetResultDataGroup( ReportResult result, List<ToDoItem> toDos, List<EmployeeOrganization> employeeUniqueOrganizations,string groupTypeCode)
        {
            List<ResultData> resultItems;
            var groupQuery = employeeUniqueOrganizations.GroupBy(org => new { CustomerId = org.CustomerId, EmployerId = org.EmployerId, Code = org.Code })
               .Select(g => new
               {
                   label = employeeUniqueOrganizations.FirstOrDefault(empOrgs => empOrgs.Code == g.Key.Code).Name,
                   data = g.Select(x => x).ToList()
               }).OrderBy(m => m.label);

            result.Groups = new List<ResultDataGroup>();
            int keyCount = 1;
            //Avoid displaying cases under more than one office location code.
            List<string> empLocationToDo = new List<string>();
            List<string> empOrganizationToDo = new List<string>();
            foreach (var t in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (var org in t.data)
                {
                    SetEmployeeLocation(toDos, resultItems, empLocationToDo, org, empOrganizationToDo, groupTypeCode);
                }
                if (resultItems.Any())
                {
                    result.Groups.Add(new ResultDataGroup()
                    {
                        Key = keyCount,
                        Label = t.label,
                        Items = new List<ResultData>().AddRangeFluid(resultItems)
                    });
                    keyCount++;
                }
            }
            if (groupTypeCode == null && toDos != null && toDos.Any())
            {
                var noOrgTodos = toDos.Where(todo => !empOrganizationToDo.Contains(todo.Id));
                if (noOrgTodos.Any())
                {
                    resultItems = new List<ResultData>();
                    foreach (var todo in noOrgTodos)
                    {
                        dynamic data = GetResultData(todo);
                        resultItems.Add(data);
                    }
                    result.Groups.Add(new ResultDataGroup()
                    {
                        Key = result.Groups.Count + 1,
                        Label = "None",
                        Items = new List<ResultData>().AddRangeFluid(resultItems)
                    });
                }
            }
            return await Task.FromResult(result);
        }



    }
}
