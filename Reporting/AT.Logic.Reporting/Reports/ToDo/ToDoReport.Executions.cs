﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Authentication;
using AbsenceSoft;
using AT.Data.Reporting.Model;
using AT.Entities.Reporting.Enums;

namespace AT.Logic.Reporting.Reports.ToDo
{
    public partial class ToDoReport : BaseReport
    {

        public override string Name { get; set; }
        public override string Category { get; set; }
        public override string MainCategory { get; set; }

        public override string IconImage
        {
            get
            {
                return "absencesoft-temporary-reports-icons_intermittent.png"; 
            }
        }
        /// <summary>
        /// return the desciprion 
        /// </summary>
        public override string ReportDescription
        {
            get { return "Lists To Do's for each case, who they are assigned to, when they are due and their current status"; }
        }
        public override bool IsGrouped { get; set; }

        public override ReportChart Chart { get; set; }

        public ToDoReport()
        {
            //initializing the default values           
            Name = "ToDo Report";
            CategoryOrder = 1;
            IsGrouped = false;
            MainCategory = "Operations Report";
            Category = "ToDo Reports";          
        }

        public override string Id
        {
            get
            {
                return "C4CD5D21-58B3-47EB-BFA6-BEDADB479B7F";
            }
        }

        

        /// <summary>
        /// List of fields for grouping
        /// </summary>
        public override Dictionary<string, List<string>> AvailableGroupingFields => new Dictionary<string, List<string>>(){
            {(ReportGroupType.None).ToString(), new List<string>(0)},
            {(ReportGroupType.Organization).ToString(), new List<string>(0)}            
        };

        /// <summary>
        /// Determines whether the specified report is visible to the given user for any valid reason.
        /// </summary>
        /// <param name="user">The user to check against.</param>
        /// <returns>
        ///   <c>true</c> if the specified report is visible; otherwise, <c>false</c>.
        /// </returns>
        public override bool IsVisible(User user)
        {
            if (user == null)
            {
                return false;
            }
            return true;
        }
        public override ReportCriteria GetCriteria(User user)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            UserReportCriteriaData data = reportingLogic.GetUserReportCriteriaDataAsync().GetAwaiter().GetResult();
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = data.SecurityUser.CustomerId.ToString(),
                StartDate = DateTime.UtcNow.Date.AddDays(-7),
                EndDate = DateTime.UtcNow.Date,
                Filters= new List<ReportCriteriaItem>(GetOrganizationTypeCriteria(data))
                {
                    { GetEmployerCriteria(data) },
                    { GetToDoAssigneeCriteria(data) },
                    { GetToDoTypeCriteria(data) },
                    { GetToDoStatusCriteria(data) }
                }
            };         
            if (IncludeOrganizationGrouping)
            {
                criteria.Filters.Add(GetOrganizationTypeGroupByCriteria(data));
            }         
            return criteria;
        }

        protected async override Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            if (sortBy == null || sortBy == "")
            {
                sortBy = "cdt";
            }
            ReportingLogic reportingLogic = new ReportingLogic(user);
            var toDoList = reportingLogic.RunToDoItemReportQueryAsync(result.Criteria, EmployerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc).GetAwaiter().GetResult();            
            await SetEmployeeIfNull(user, toDoList);
            if (reportGroupType != null)
            {
                result.Name = reportGroupType.ToString();
                IsGrouped = true;
                switch (reportGroupType)
                {
                    case ReportGroupType.Organization:
                        {
                            return await ReportByOrganization(user, toDoList, result);
                        }                   
                }
            }
            else
            {
                return await ReportDetail(toDoList, result);
            }
            return await Task.FromResult(result);
        }
    }
}
