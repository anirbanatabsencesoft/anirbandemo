﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using AbsenceSoft;
using AT.Data.Reporting.Model;

namespace AT.Logic.Reporting.Reports.ToDo
{
    public partial class CasesWithoutToDoReport : BaseReport
    {

        public override string Name { get; set; }
        public override string Category { get; set; }
        public override string MainCategory { get; set; }
        /// <summary>
        /// return the desciprion 
        /// </summary>
        public override string ReportDescription
        {
            get { return "Case list for all cases with no To Do's"; }
        }

        public override string IconImage
        {
            get
            {
                return "absencesoft-temporary-reports-icons_intermittent.png";
            }
        }

        public override bool IsGrouped { get; set; }

        public override ReportChart Chart { get; set; }

        public CasesWithoutToDoReport()
        {
            //initializing the default values           
            Name = "Cases Without";
            CategoryOrder = 4;
            IsGrouped = false;
            MainCategory = "Operations Report";
            Category = "ToDo Reports";
        }

        public override string Id
        {
            get
            {
                return "EA194CE4-7E4D-429D-9D19-CC092A4E307C";
            }
        }
        /// <summary>
        /// Determines whether the specified report is visible to the given user for any valid reason.
        /// </summary>
        /// <param name="user">The user to check against.</param>
        /// <returns>
        ///   <c>true</c> if the specified report is visible; otherwise, <c>false</c>.
        /// </returns>
        public override bool IsVisible(User user)
        {
            if (user == null)
            {
                return false;
            }
            return true;
        }

        public override ReportCriteria GetCriteria(User user)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            UserReportCriteriaData data = reportingLogic.GetUserReportCriteriaDataAsync().GetAwaiter().GetResult();
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = data.SecurityUser.CustomerId.ToString(),
                StartDate = DateTime.UtcNow.GetFirstDayOfYear().Date,
                EndDate = DateTime.UtcNow.Date,
                Filters = new List<ReportCriteriaItem>(GetOrganizationTypeCriteria(data))
            };
            return criteria;
        }

        protected async override Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            var toDoList = reportingLogic.RunCaseWithoutToDoReportQueryAsync(result.Criteria, EmployerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc).GetAwaiter().GetResult();

            result.Items = toDoList.Select(c => GetResultData(c)).Where(c => c != null).ToList();
            return await Task.FromResult(result);
        }
    }
}
