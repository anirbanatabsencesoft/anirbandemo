using AbsenceSoft.Data;
using AbsenceSoft.Data.Notes;
using System;
using System.Collections.Generic;

namespace AT.Logic.Reporting.Reports.AuditReport
{
    /// <summary>
    /// Case Note Audit Report
    /// </summary>
    [Serializable]
    public partial class CaseNoteAuditReport : BaseReport
    {
        public override string Id { get { return "E29BF6C0-C4DA-4CA7-A7B2-9B43092E107C"; } }
        public override string Name { get { return "Case Notes"; } set { Name = value; } }
        public override bool IsGrouped { get { return false; } set { IsGrouped = value; } }

        public IEnumerable<EmployerAudit<CaseNote>> EmployerAuditCaseNote { get; set; }

        /// <summary>
        /// Gets the ReportCriteria for given User
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public override ReportCriteria GetCriteria(AT.Entities.Authentication.User user)
        {
            return new ReportCriteria
            {
                CustomerId = user.CustomerId.ToString(),
                StartDate = DateTime.UtcNow.Date.AddDays(-30),
                EndDate = DateTime.UtcNow.Date
            };
        }
    }
}