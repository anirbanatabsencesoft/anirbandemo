﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Security;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.AuditReport
{
    /// <summary>
    /// Case Note Audit Report
    /// </summary>
    public partial class CaseNoteAuditReport : BaseReport
    {
        /// <summary>
        /// Category for all Absence Status reports is "Absence Status".
        /// </summary>
        public override string MainCategory { get; set; } = "Operations Report";

        /// <summary>
        /// Category for all Absence Status reports is "Absence Status".
        /// </summary>
        public override string Category { get; set; } = "Audit Report";
        /// <summary>
        /// return the desciprion 
        /// </summary>
        public override string ReportDescription
        {
            get { return "Case list provides high level details about cases open at any time during the requested period"; }
        }

        public override bool IsVisible(Entities.Authentication.User user)
        {
            return false;
        }

        /// <summary>
        /// Icon to set for Audit Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_productivity-comparison.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart
        {
            get; set;
        }

        /// <summary>
        /// Runs the report for given user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        protected override async Task<ReportResult> RunReportAsync(AT.Entities.Authentication.User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            var userData = reportingLogic.GetUserReportCriteriaDataAsync().GetAwaiter().GetResult();
            EmployerAuditCaseNote = reportingLogic.GetEmployerAuditCase(userData, pageNumber, pageSize).GetAwaiter().GetResult();

            if (EmployerAuditCaseNote.Any())
            {
                List<ResultData> resultItems = new List<ResultData>();

                List<Case> caseIds = reportingLogic.GetCase(EmployerAuditCaseNote.Select(p => p.Value.CaseId).ToList(), pageNumber, pageSize).GetAwaiter().GetResult();
                IEnumerable<AbsenceSoft.Data.Security.User> userList = reportingLogic.GetUsersById(EmployerAuditCaseNote.Select(p => p.CreatedBy).ToList()).GetAwaiter().GetResult();
                SetEmployerAuditCaseNoteToResultItem(resultItems, caseIds, userList);

                result.Items = resultItems;
            }
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Set Employer Audit Case Not to ResultItem
        /// </summary>
        /// <param name="resultItems"></param>
        /// <param name="caseIds"></param>
        /// <param name="userList"></param>
        private void SetEmployerAuditCaseNoteToResultItem(List<ResultData> resultItems, List<Case> caseIds, IEnumerable<AbsenceSoft.Data.Security.User> userList)
        {
            string displayCaseId = string.Empty, displayCaseNoteId = string.Empty, auditType = string.Empty, currentCaseNumber = string.Empty;
            foreach (var caseNote in EmployerAuditCaseNote)
            {
                dynamic item = new ResultData();

                if (displayCaseId != caseNote.Value.CaseId)
                {
                    displayCaseId = caseNote.Value.CaseId;
                    currentCaseNumber = string.Empty;

                    var @case = caseIds.FirstOrDefault(p => p.Id == displayCaseId);

                    if (@case != null)
                    {
                        currentCaseNumber = @case.CaseNumber;
                    }

                    if (!string.IsNullOrWhiteSpace(currentCaseNumber))
                    {
                        item.CaseNumber = new LinkItem() { Value = currentCaseNumber, Link = $"/Cases/{caseNote.Value.CaseId}/View" };
                    }
                }

                auditType = SetAuditTypeCaseNoteId(ref displayCaseNoteId, caseNote, item);

                item.CaseNote = caseNote.Value.Notes;
                item.Category = caseNote.Value.Category.ToString();
                item.ChangedOn = caseNote.CreatedDate.ToString("MM/dd/yyyy hh:mm:ss tt");
                item.ChangedBy = (userList.FirstOrDefault(p => p.Id.Equals(caseNote.CreatedBy)) ?? AbsenceSoft.Data.Security.User.System).DisplayName;
                item.AuditType = auditType;
                resultItems.Add(item);
            }
        }

        /// <summary>
        /// Gets Audit Type for given caseNote and Item
        /// </summary>
        /// <param name="displayCaseNoteId"></param>
        /// <param name="caseNote"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        private string SetAuditTypeCaseNoteId(ref string displayCaseNoteId, EmployerAudit<CaseNote> caseNote, dynamic item)
        {
            string auditType;
            if (displayCaseNoteId != caseNote.Value.Id)
            {
                displayCaseNoteId = caseNote.Value.Id;
                if (caseNote.AuditType == AuditType.Save)
                {
                    auditType = "Added";
                }
                else
                {
                    auditType = caseNote.AuditType.ToString();
                }
                item.CaseNoteId = caseNote.Value.Id;
            }
            else
            {
                if (caseNote.AuditType == AuditType.Save)
                {
                    auditType = "Updated";
                }
                else
                {
                    auditType = (caseNote.AuditType == AuditType.Delete ? "Deleted" : caseNote.AuditType.ToString());
                }
            }
            return auditType;
        }
    }
}