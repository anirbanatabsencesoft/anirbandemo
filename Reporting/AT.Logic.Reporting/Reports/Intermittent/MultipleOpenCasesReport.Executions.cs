﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using AT.Data.Reporting.Model;
using AbsenceSoft;

namespace AT.Logic.Reporting.Reports.Intermittent
{
    public partial class MultipleOpenCasesReport : BaseReport
    {
        public MultipleOpenCasesReport()
        {
            Name = "Multiple Open Cases";
            MainCategory = "Forensic Report";
            Category = "Intermittent";
            IsGrouped = true;
        }
        public override string Id { get { return "92D6ACC5-A85A-424C-9E5A-CD98037F7D48"; } }

        public override string Name { get; set; }
        public override string Category { get; set; }
        public override string MainCategory { get; set; }
        /// <summary>
        /// return the desciprion 
        /// </summary>
        public override string ReportDescription
        {
            get { return "Case list provides details about employees with multiple open cases"; }
        }
        public override string IconImage
        {
            get { return "absencesoft-temporary-reports-icons_intermittent.png"; }
        }

        public override bool IsGrouped { get; set; }

        public override ReportChart Chart { get; set; }
        /// <summary>
        /// Helps overriding the logic for getting the criteria fields and their deplayed values.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            UserReportCriteriaData data = reportingLogic.GetUserReportCriteriaDataAsync().GetAwaiter().GetResult();
            var startDate = DateTime.UtcNow.Date.SameDayLastYear();
            var endDate = DateTime.UtcNow.Date;
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = data.SecurityUser.CustomerId.ToString(),
                StartDate = startDate,
                EndDate = endDate
            };
            ReportCriteriaItem item = GetCaseStatusCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetAbsenceReasonCriteria(data);
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);
            criteria.Filters.Add(GetWorkStateCriteria(data, criteria.StartDate, criteria.EndDate));
            criteria.Filters.Add(GetLocationCriteria(data));
            criteria.Filters.Add(GetEmployeeCriteria(user));
            return criteria;
        }

        /// <summary>
        /// Helps overriding the logic for executing the query for the report and returning the result back to caller.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="reportGroupType"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        protected async override Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            var cases = await reportingLogic.RunMultipleOpenCasesReportQueryAsync(result.Criteria, EmployerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc);
            result.Groups = GetResultDataGroups(cases, result.Criteria);
            return result;
        }
    }
}
