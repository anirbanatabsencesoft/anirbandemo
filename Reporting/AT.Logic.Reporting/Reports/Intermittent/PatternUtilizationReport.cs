﻿using AbsenceSoft;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AT.Data.Reporting.Model;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.Intermittent
{
    /// <summary>
    /// Pattern Utilization Report
    /// </summary>
    [Serializable]
    public class PatternUtilizationReport : BaseReport
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public PatternUtilizationReport()
        {
            Name = "Pattern Utilization Report";
            MainCategory = "Forensic Report";
            Category = "Intermittent";
            IsGrouped = false;
        }

        /// <summary>
        /// return the desciprion 
        /// </summary>
        public override string ReportDescription
        {
            get { return "Employee list provides details about intermittent patterns detected"; }
        }
        /// <summary>
        /// Id of the report
        /// </summary>
        public override string Id { get { return "99e99082-130b-433b-a053-a8b425636b8d"; } }

        /// <summary>
        /// Category for all Absence Status reports is "Absence Status".
        /// </summary>
        public override string MainCategory { get; set; }

        /// <summary>
        /// Category for all intermittent pattern utilization reports is "Intermittent".
        /// </summary>
        public override string Category { get; set; }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get; set; }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get; set; }

        /// <summary>
        /// Icon to set for intermittent pattern report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_intermittent.png"; } }

        /// <summary>
        /// To be used for ESS category
        /// </summary>
        public override string ESSCategoryTitle { get { return "Intermittent"; } }

        /// <summary>
        /// Ess report details
        /// </summary>
        public override EssReportDetail ESSReportDetails { get { return new EssReportDetail() { Title = "Intermittent Pattern & Usage Report", Type = ESSReportType.TeamReport, ESSReportName = "Intermittent Pattern & Usage Report" }; } }

        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns>LOA Feature</returns>
        public override IEnumerable<Feature> RequiredFeatures()
        {
            yield return Feature.LOA;
        }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get; set; }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            UserReportCriteriaData userReportCriteriaData = reportingLogic.GetUserReportCriteriaDataAsync().GetAwaiter().GetResult();
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerKey,
                StartDate = DateTime.UtcNow.Date.SameDayLastYear(),
                EndDate = DateTime.UtcNow.Date
            };
            criteria.Filters.Add(new ReportCriteriaItem()
            {
                ControlType = ControlType.RadioButtonList,
                Name = "Pattern",
                Prompt = "Pattern Utilization",
                Required = false,
                Tweener = "includes",
                Value = null,
                Options = new List<ReportCriteriaItemOption>()
                {
                    new ReportCriteriaItemOption() { Value = "1", Text = "Monday/Friday" },
                    new ReportCriteriaItemOption() { Value = "2", Text = "1 Hour or Less" },
                    new ReportCriteriaItemOption() { Value = "3", Text = "Weekdays" }
                }
            });
            ReportCriteriaItem item = GetAbsenceReasonCriteria(userReportCriteriaData);          
            criteria.Filters.Add(item);
            criteria.Filters.Add(GetWorkStateCriteria(userReportCriteriaData, criteria.StartDate, criteria.EndDate));           
            criteria.Filters.AddRange(GetOrganizationTypeCriteria(userReportCriteriaData));
            criteria.Filters.Add(GetEmployeeCriteria(user));
            return criteria;
        }

        /// <summary>
        /// Run the query 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="reportGroupType"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <param name="groupByColumns"></param>
        /// <returns></returns>
        protected async override Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            List<ResultData> resultItems = new List<ResultData>();
            ReportingLogic reportingLogic = new ReportingLogic(user);
            var query = reportingLogic.RunPatternUtilizationReportQueryAsync(result.Criteria, EmployerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc).GetAwaiter().GetResult();
            var groups= query.GroupBy(r => r.Employee.Id);           

            string pattern = "0";

            if (result.Criteria.Filters != null && result.Criteria.Filters.Any())
            {
                var patternCriteria = result.Criteria.Filters.FirstOrDefault(m => m.Name == "Pattern");
                if (patternCriteria != null && !string.IsNullOrWhiteSpace(patternCriteria.Value?.ToString()))
                {
                    pattern = patternCriteria.ValueAs<string>();
                }

                if (string.IsNullOrWhiteSpace(pattern))
                {
                    pattern = "0";
                }
            }
            foreach (var group in groups)
            {
                var absences = group
                    .SelectMany(g => g.Segments.Where(s => s.Type == CaseType.Intermittent))
                    .SelectMany(s => s.UserRequests.Where(r => r.RequestDate.DateInRange(result.Criteria.StartDate, result.Criteria.EndDate)))
                    .ToList();

                if (!absences.Any())
                {
                    continue;
                }
                Employee emp = group.First().Employee;
                resultItems = GetPatternBasedItems(resultItems, pattern, absences, emp);
            }

            result.Items = resultItems;
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Get the Item list based on Pattern occurrence
        /// </summary>
        /// <param name="resultItems"></param>
        /// <param name="pattern"></param>
        /// <param name="absences"></param>
        /// <param name="emp"></param>
        /// <returns></returns>
        private List<ResultData> GetPatternBasedItems(List<ResultData> resultItems, string pattern, List<AbsenceSoft.Data.Cases.IntermittentTimeRequest> absences, Employee emp)
        {
            int occurs = 0;
            if (pattern == "0" || pattern == "1")
            {
                occurs = absences.Count(a => a.RequestDate.DayOfWeek == DayOfWeek.Friday || a.RequestDate.DayOfWeek == DayOfWeek.Monday);
                BuildResult(resultItems, absences.Count, emp, occurs, "Monday/Friday");             
            }
            
            if (pattern == "0" || pattern == "2")
            {
                occurs = absences.Count(a => a.TotalMinutes <= 60);
                BuildResult(resultItems, absences.Count, emp, occurs, "1 Hour or Less");               
            }
            
            if (pattern == "0" || pattern == "3")
            {
                occurs = absences.Count(a => a.RequestDate.DayOfWeek == DayOfWeek.Tuesday);
                BuildResult(resultItems, absences.Count, emp, occurs, "Weekday - Tuesday");

                occurs = absences.Count(a => a.RequestDate.DayOfWeek == DayOfWeek.Wednesday);
                BuildResult(resultItems, absences.Count, emp, occurs, "Weekday - Wednesday");               

                occurs = absences.Count(a => a.RequestDate.DayOfWeek == DayOfWeek.Thursday);
                BuildResult(resultItems, absences.Count, emp, occurs, "Weekday - Thursday");                
            }

            return resultItems;
        }

        /// <summary>
        /// Build the Result
        /// </summary>
        /// <param name="resultItems"></param>
        /// <param name="absencesCount"></param>
        /// <param name="emp"></param>
        /// <param name="patternCount"></param>
        /// <param name="pattern"></param>
        private void BuildResult(List<ResultData> resultItems, int absencesCount, Employee emp, int patternCount, string pattern)
        {
            if (patternCount > 1 && (((decimal)patternCount / (decimal)absencesCount) >= 0.3M))
            {
                dynamic data = new ResultData();
                data.EmployeeId = new LinkItem() { Value = emp.EmployeeNumber, Link = string.Format("/Employees/{0}/View", emp.Id) };
                data.EmployeeName = new LinkItem() { Value = emp.FullName, Link = string.Format("/Employees/{0}/View", emp.Id) };
                data.UtilizationPattern = pattern;
                data.TotalIntAbsences = absencesCount.ToString("N0");
                data.TotalPatternOccurences = patternCount.ToString("N0");
                data.PatternRatio = absencesCount == 0 ? "N/A" : ((decimal)patternCount / (decimal)absencesCount).ToString("P0");
                resultItems.Add(data);
            }
        }
    }
}
