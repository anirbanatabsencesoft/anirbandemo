﻿using AbsenceSoft;
using AT.Data.Reporting.Model;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.Intermittent
{
    public partial class IntermittentScheduleExceptionReport : BaseReport
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IntermittentScheduleExceptionReport" /> class.
        /// </summary>
        public IntermittentScheduleExceptionReport() : base() { }

        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id => "F29878F1-B8B7-41DB-B285-642B78868A4E";

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get; set; } = "Schedule Exception Report";

        /// <summary>
        /// When overridden in a derived class, gets the name of the report category for display.
        /// </summary>
        public override string Category { get; set; } = "Intermittent";

        /// <summary>
        /// When overridden in a derived class, gets the name of the report main category to bifurcate reports.
        /// </summary>
        public override string MainCategory { get; set; } = "Operations Report";

        /// <summary>
        /// When overridden in a derived class, gets the report description for display
        /// </summary>
        public override string ReportDescription => string.Concat("Returns a list of invalidated intermittent time off requests which occur on a day ",
                    "not backed by scheduled work time but have a policy that requires scheduled work time (i.e. Time off requests that are approved for a day ",
                    "the employee is not scheduled for work). This is an exceptions reports intended to help leave of absence operations correct invalidated ",
                    "data and ensure accurate calculations.");

        /// <summary>
        /// When overridden in a derived class, gets name of the icon image to set in front end
        /// </summary>
        public override string IconImage => "absencesoft-temporary-reports-icons_intermittent.png";

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get; set; } = false;

        /// <summary>
        /// Property to configure chart related options
        /// </summary>
        public override ReportChart Chart { get; set; } = null;

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            UserReportCriteriaData data = reportingLogic.GetUserReportCriteriaDataAsync().GetAwaiter().GetResult();
            var startDate = Date.UnixEpoch.ToMidnight();
            var endDate = DateTime.UtcNow.Date.GetLastDayOfYear().AddYears(3);
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = data.SecurityUser.CustomerId.ToString(),
                StartDate = startDate,
                EndDate = endDate
            };

            criteria.Filters.Add(GetCaseAssigneeCriteria(data));
            criteria.Filters.Add(GetLocationCriteria(data));
            criteria.Filters.Add(GetWorkStateCriteria(data, criteria.StartDate, criteria.EndDate));

            return criteria;
        }

        /// <summary>
        /// Helps overriding the logic for executing the query for the report and returning the result back to caller.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        /// <param name="reportGroupType"></param>
        /// <param name="pageNumber">Add the page number</param>
        /// <param name="pageSize">Set the page size</param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <param name="groupByColumns"></param>
        /// <returns></returns>
        protected async override Task<ReportResult> RunReportAsync(
            User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber,
            int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            var dataList = await reportingLogic.RunIntermittentScheduleExceptionQueryAsync(result.Criteria, EmployerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc);

            result.Items = dataList.Select(GetResultData).ToList();

            return await Task.FromResult(result);
        }
    }
}
