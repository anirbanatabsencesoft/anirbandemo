﻿using AbsenceSoft.Data.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.Intermittent
{
    public partial class MultipleOpenCasesReport : BaseReport
    {
        /// <summary>
        /// Method helps formation of the ResultData object from Case item object
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected ResultData GetResultData(Case item)
        {
            dynamic data = new ResultData();
            data.CaseNumber = new LinkItem() { Value = item.CaseNumber, Link = string.Format("/Cases/{0}/View", item.Id) };
            data.EmployeeId = new LinkItem() { Value = item.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", item.Employee.Id) };
            data.StartDate = item.StartDate.ToShortDateString();
            data.EndDate = item.StartDate.ToShortDateString();

            if (item.Reason != null && item.Reason.Code == "FHC")
            {
                data.Reason = string.Format(
                    "{0}, {1} {2} ({3}) ",
                    item.Reason.Name,
                    item.Contact.Contact.FirstName,
                    item.Contact.Contact.LastName,
                    item.Contact.ContactTypeName
                );
            }
            else
            {
                data.Reason = item.Reason?.Name;
            }
            return data as ResultData;
        }

        /// <summary>
        /// Method allows the resultset cases for grouping and forming the list of ResultDataGroup.
        /// </summary>
        /// <param name="cases"></param>
        /// <param name="criteria"></param>
        /// <returns></returns>
        protected List<ResultDataGroup> GetResultDataGroups(List<Case> cases, ReportCriteria criteria)
        {
            var multipleOpenCases = cases
                .GroupBy(g => new
                {
                    EmployeeId = g.Employee.Id,
                    EmployeeName = g.Employee.FullName,
                    g.Employee.EmployeeNumber
                })
                .Where(w => w.Count() > 1)
                .ToList();

            var groups = multipleOpenCases.Select(group =>
            {
                var labelkey = string.Format("{0}, {1}", group.Key.EmployeeName, group.Key.EmployeeNumber);
                return new ResultDataGroup
                {
                    Key = labelkey,
                    Label = labelkey,
                    Items = group.Select(@case => GetResultData(@case)).ToList()
                };
            }).ToList();
            return groups;
        }
    }
}
