﻿using AbsenceSoft;
using AT.Data.Reporting.Model;

namespace AT.Logic.Reporting.Reports.Intermittent
{
    public partial class IntermittentScheduleExceptionReport : BaseReport
    {
        /// <summary>
        /// Gets the result data.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <returns></returns>
        private ResultData GetResultData(IntermittentScheduleExceptionDataRow row)
        {
            // Build the result item data
            dynamic data = new ResultData();

            data.EmployeeId = new LinkItem() { Value = row.EmployeeNumber, Link = string.Format("/Employees/{0}/View", row.EmployeeId) };
            data.EmployeeName = new LinkItem() { Value = row.EmployeeName, Link = string.Format("/Employees/{0}/View", row.EmployeeId) };
            data.CaseNumber = new LinkItem() { Value = row.CaseNumber, Link = string.Format("/Cases/{0}/View", row.CaseId) };
            data.RequestedDate = row.RequestedDate.ToUIString();
            data.RequestedTime = row.RequestedTime.ToFriendlyTime();
            data.PolicyCode = row.PolicyCode;
            data.AssignedTo = row.AssignedTo;
            data.EmployeeLastModified = row.EmployeeLastModified.ToUIString();
            data.ScheduleLastEffective = row.ScheduleLastEffective.ToUIString();

            return data;
        }
    }
}
