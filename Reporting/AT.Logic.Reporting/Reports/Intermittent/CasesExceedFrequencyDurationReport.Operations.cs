﻿using AbsenceSoft;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Logic.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.Intermittent
{
    public partial class CasesExceedFrequencyDurationReport : BaseReport
    {
        /// <summary>
        /// Method allows forming the ResultData object from an Anonymous type
        /// </summary>
        /// <param name="item"></param>
        /// <param name="isDetailed"></param>
        /// <returns></returns>
        protected ResultData GetResultData(dynamic item, bool isDetailed)
        {
            dynamic data = new ResultData();

            var emp = item.Case.Employee;
            data.EmployeeId = new LinkItem() { Value = emp.EmployeeNumber, Link = string.Format("/Employees/{0}/View", emp.Id) };
            data.EmployeeName = new LinkItem() { Value = emp.FullName, Link = string.Format("/Employees/{0}/View", emp.Id) };
            data.CaseNumber = new LinkItem() { Value = item.Case.CaseNumber, Link = string.Format("/Cases/{0}/View", item.Case.Id) };
            data.Status = item.Case.Status.ToString();

            var cert = item.Certification;
            data.CertifiedStartDate = cert.StartDate.ToShortDateString();
            data.CertifiedEndDate = cert.EndDate.ToShortDateString();
            data.CertifiedDescription = cert.ToString();
            if (isDetailed)
            {
                data.CertifiedOccurances = cert.Occurances;
                data.CertifiedFrequency = cert.Frequency;
                data.CertifiedFrequencyType = cert.FrequencyType.ToString();
                data.CertifiedDuration = cert.Duration;
                data.CertifiedDurationType = cert.DurationType.ToString();
            }

            data.ExceedsFrequencyCount = item.ExceededFrequenciesCount;
            data.ExceedsDurationCount = item.ExceededDurationsCount;
            data.ExceedsDurationAverage = Math.Round(item.ExceededDurationsAverage, 2);
            if (isDetailed)
            {
                data.ExceedsDurationMin = Math.Round(item.ExceededurationsMin, 2);
                data.ExceedsDurationMax = Math.Round(item.ExceededurationsMax, 2);

                data.ExceededDurationsStdDev = Math.Round(item.ExceededDurationStdDev, 2);
            }

            return data as ResultData;
        }

        /// <summary>
        /// The method allows forming the resultset cases to the list of ResultData back for the report
        /// </summary>
        /// <param name="cases"></param>
        /// <param name="criteria"></param>
        /// <returns></returns>
        protected List<ResultData> GetResultItems(List<Case> cases, ReportCriteria criteria)
        {
            var caseCertifications =
                from c in cases
                from cert in c.Certifications
                let requests = c.Segments.SelectMany(m => m.UserRequests)
                    .Where(ur => ur.RequestDate.IsInRange(cert.StartDate, cert.EndDate))
                    .ToList()
                let exceededDurations = requests.GetExceededDurations(cert.FrequencyType, cert.Duration, cert.DurationType).ToList()
                let exceededDurationDefault = exceededDurations.DefaultIfEmpty(MetricComparison.Zero)
                select new
                {
                    Case = c,
                    Certification = cert,
                    ExceededFrequenciesCount = requests.GetExceededFrequencies(cert.Frequency, cert.FrequencyType).Count(),
                    ExceededDurationsCount = exceededDurations.Count,
                    ExceededDurationsAverage = exceededDurationDefault.Average(a => a.Difference),
                    ExceededurationsMin = exceededDurationDefault.Min(a => a.Difference),
                    ExceededurationsMax = exceededDurationDefault.Max(a => a.Difference),
                    ExceededDurationStdDev = exceededDurationDefault.Select(a => a.Difference).StdDevPop()
                };

            var isDetailed = criteria.ValueOf<string>(DetailLevelName) == DetailLevelFull;
            var minimumExceedsFrequencyCount = criteria.ValueOf<int>(MinimumExceedsFrequencyCountName);
            var minimumExceedsDurationCount = criteria.ValueOf<int>(MinimumExceedsDurationCountName);

            return caseCertifications
                    .Where(w =>
                        w.ExceededFrequenciesCount >= minimumExceedsFrequencyCount
                        && w.ExceededDurationsCount >= minimumExceedsDurationCount
                    )
                    .Select(item => GetResultData(item, isDetailed)
                    ).ToList();
        }
    }
}
