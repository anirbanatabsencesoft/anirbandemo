﻿using AbsenceSoft;
using AbsenceSoft.Logic.Users;
using AT.Data.Reporting.Model;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Classes;
using AT.Entities.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.CommunicationReport
{
    /// <summary>
    /// Communication Report class
    /// </summary>

    [Serializable]
    public class CommunicationDetailReport : BaseReport
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public CommunicationDetailReport()
        {
            Name = "Communication Detail Report";
            IsGrouped = false;
            MainCategory = "Operations Report";
            Category = "Communications";
        }
        /// <summary>
        /// return the desciprion 
        /// </summary>
        public override string ReportDescription
        {
            get { return "Lists when communications were sent for each case and the method used to send them"; }
        }
        /// <summary>
        /// Id of the report
        /// </summary>
        public override string Id { get { return "AEF89837-7575-4495-B07C-2060FDFAD853"; } }

        /// <summary>
        /// List of fields for grouping
        /// </summary>
        public override Dictionary<string, List<string>> AvailableGroupingFields => new Dictionary<string, List<string>>(){
                       {(ReportGroupType.Custom).ToString(), new List<string>(){
                        "CaseId",
                        "CommunicationType",
                        "HasPendingPaperwork",
                        "CreatedByEmail",
                        "CreatedByName",
                        "CustomerId",
                        "Customer.Name",
                        "EmployeeId",
                        "EmployeeNumber",
                        "EmployerId",
                        "cby",
                        "IsDraft",
                        "Case.CaseNumber",
                        "Case.AssignedToId",
                        "Case.CaseType"
                       } } };


        /// <summary>
        /// Name of the Report.
        /// </summary>
        public override string Name { get; set; }
        /// <summary>
        /// Category for all Accommodation Status reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get; set; }
        /// <summary>
        /// Category for all Accommodation Status reports is "Accommodation Status Report".
        /// </summary>
        public override string Category { get; set; }
        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get; set; }
        /// <summary>
        /// Icon to set for Accommodation Status Report
        /// </summary>
        public override string IconImage { get { return "btn-icon-communication-reverse.png"; } }
        /// <summary>
        /// Determine if Report is Grouped
        /// </summary>
        public override bool IsGrouped { get; set; }
        /// <summary>
        /// Set true for Group Auto total
        /// </summary>
        public override bool GroupAutoTotals { get { return false; } }
        /// <summary>
        /// Determines whether the specified user is visible.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>Boolean</returns>
        public override bool IsVisible(User user)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            UserReportCriteriaData userReportCriteriaData = reportingLogic.GetUserReportCriteriaDataAsync().GetAwaiter().GetResult();
            return userReportCriteriaData.SecurityUser != null && userReportCriteriaData.SecurityUser.HasPermission(AbsenceSoft.Data.Security.Permission.RunCommunicationReport);
        }


        /// <summary>
        /// Get the Communication Report criteria
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            UserReportCriteriaData userReportCriteriaData = reportingLogic.GetUserReportCriteriaDataAsync().GetAwaiter().GetResult();

            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerKey,
                StartDate = DateTime.UtcNow.Date.AddDays(-30),
                EndDate = DateTime.UtcNow.Date
            };
            ReportCriteriaItem item;

            item = GetCommTypeCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetAbsenceReasonCriteria(userReportCriteriaData);
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetCaseTypeCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetAdjudicationCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            criteria.Filters.Add(GetWorkStateCriteria(userReportCriteriaData, criteria.StartDate, criteria.EndDate));
            criteria.Filters.AddRange(GetOrganizationTypeCriteria(userReportCriteriaData));

            return criteria;
        }



        /// <summary>
        /// Build and Run the Report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="reportGroupType"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <param name="groupByColumns"></param>
        /// <returns></returns>
        protected override async Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            var query = reportingLogic.RunCommunicationQueryAsync(result.Criteria, EmployerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc).GetAwaiter().GetResult();

            if (reportGroupType != null)
            {
                if (reportGroupType == ReportGroupType.Custom)
                {
                    result.Name = "Custom grouping";
                    IsGrouped = true;
                    var groupQuery = query.GroupBy(groupByColumns.ToArray());
                    result.JsonData = CreateJsonResult(groupQuery);                  
                }
                else
                {
                    throw new NotImplementedException("Group by is not implemented.");
                }
            }
            else
            {
                result.Items = query.Select(comm =>
                {
                    return CreateCommunicationItemList(comm);
                }).ToList();
            }
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Create custom Grouping
        /// </summary>
        /// <param name="groupQuery"></param>
        /// <returns></returns>
        private dynamic CreateJsonResult(List<GroupResult> groupQuery)
        {
            List<ResultData> resultItems= new List<ResultData>();
            var dataGroup = new List<dynamic>();
            if (groupQuery == null || !groupQuery.Any())
            {
                return dataGroup;
            }
            foreach (var queryItem in groupQuery)
            {
                foreach (var item in queryItem.Items)
                {
                    resultItems.Add(CreateCommunicationItemList((AbsenceSoft.Data.Communications.Communication)item));
                }               

                dataGroup.Add(new
                {
                    Label = queryItem.Value,
                    Key = queryItem.Key,
                    Items = new List<ResultData>().AddRangeFluid(resultItems),
                    SubGroups = CreateJsonResult(queryItem.SubGroups)
                });
            }
            return dataGroup;

        }

        /// <summary>
        /// Create Communication item list
        /// </summary>
        /// <param name="communication"></param>
        /// <returns></returns>
        private ResultData CreateCommunicationItemList(AbsenceSoft.Data.Communications.Communication communication)
        {
            dynamic data = new ResultData();
            data.Employer = communication.Employer.SignatureName;
            data.CaseNumber = string.Empty;
            if (!string.IsNullOrEmpty(communication.Case?.Id))
            {
                data.CaseNumber = new LinkItem() { Value = communication.Case.CaseNumber, Link = string.Format("/Cases/{0}/View", communication.CaseId) };
            }
            data.Employee = string.Empty;
            if (!string.IsNullOrEmpty(communication.Employee?.Id))
            {
                data.Employee = new LinkItem() { Value = communication.Employee.FullName, Link = string.Format("/Employees/{0}/View", communication.EmployeeId) };
            }
            data.SentDate = communication.SentDate.HasValue ? communication.SentDate.Value.Date.ToShortDateString() : null;
            data.CommunicationType = communication.CommunicationType.ToString();
            data.CommunicationName = communication.Name ?? string.Empty;
            data.Subject = communication.Subject;
            return (ResultData)data;
        }
    }
}
