
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using AT.Logic.Reporting.Enums;
using AT.Data.Reporting.PostgreSqlDataAccess.Model;

namespace AT.Logic.Reporting.Reports
{
    /// <summary>
    /// Generic class that represents the results for the status of the request as well
    /// as scheduled reports.
    /// </summary>
    public class ReportStatusResult
    {

        public ReportStatusResult()
        {

        }

        /// <summary>
        /// Represents the request Id for the report
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Represents the status of the requested report
        /// </summary>
        public RequestStatus Status { get; set; }
        /// <summary>
        /// Shows "Success" message or Any Failed status error message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// The Mongo Report Id
        /// </summary>
        public string ReportId { get; set; }

        /// <summary>
        /// Set the report name
        /// </summary>
        public string ReportName { get; set; }

        /// <summary>
        /// Set the criteria to show in status page
        /// </summary>
        public string CriteriaPlainEnglish { get; set; }


    }

}