﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using AbsenceSoft.Data.Enums;
using AT.Data.Reporting.Model;

namespace AT.Logic.Reporting.Reports.BusinessIntelligence
{
    public partial class BiAccomodationByDecisionReport : BaseReport
    {
        public BiAccomodationByDecisionReport()
        {
            Name = "by Accommodation Decision";
            MainCategory = "Business Intelligence Reports";
            Category = "Accommodation Reports";
            IsGrouped = false;          
        }
        public override string Id
        {
            get
            {
                return "DC01E524-0B4D-4218-9656-3BF715C08397";
            }
        }
        public override string Name { get; set; }
        public override string Category { get; set; }
        public override string MainCategory { get; set; }

        /// <summary>
        /// return the desciprion forbi accomdation decission
        /// </summary>
        public override string ReportDescription
        {
            get { return "Bar chart - Accommodation decision, comparison by accommodation type"; }
        }

        /// <summary>
        /// Determine if report is exported in CSV
        /// </summary>
        public override bool IsExportableToCsv
        {
            get
            {
                return false;
            }
        }
        /// <summary>
        /// Determine if report is exported in PDF
        /// </summary>
        public override bool IsExportableToPdf
        {
            get
            {
                return false;
            }
        }
        public override string IconImage
        {
            get
            {
                return "absencesoft-temporary-reports-icons_intelligence-report.png";
            }
        }

        public override bool IsGrouped { get; set; }

        /// <summary>
        /// Chart configurations
        /// </summary>
        public override ReportChart Chart { get; set; } = new ReportChart()
        {
            Type = "ColumnChart",
            Title = "Accommodation Decision Report",
            is3D = false,
            isStacked = false,
            Height = 600
        };

        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns>ADA Feature</returns>
        public override IEnumerable<Feature> RequiredFeatures()
        {
            yield return Feature.ADA;
        }

        public override ReportCriteria GetCriteria(User user)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            UserReportCriteriaData data = reportingLogic.GetUserReportCriteriaDataAsync().GetAwaiter().GetResult();
            var startDate = DateTime.UtcNow.Date.AddDays(-30);
            var endDate = DateTime.UtcNow.Date;
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = data.SecurityUser.CustomerId.ToString(),
                StartDate = startDate,
                EndDate = endDate,
                Filters = new List<ReportCriteriaItem>()
                {
                    { GetProductivityCriteria() },
                    { GetWorkStateCriteria(data, startDate, endDate) },
                    { GetLocationCriteria(data) }
                }
            };
            return criteria;
        }

        protected async override Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            var seriesData = await reportingLogic.RunAccomodationByDecisionReportQueryAsync(result.Criteria, EmployerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc);
            result.SeriesData = seriesData;
            return result;
        }
    }
}
