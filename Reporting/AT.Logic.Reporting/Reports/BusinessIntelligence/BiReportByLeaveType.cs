﻿using AbsenceSoft.Data.Enums;
using AT.Data.Reporting.Model;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.BusinessIntelligence
{
    [Serializable]
    public class BiReportByLeaveType : BaseReport
    {
        public BiReportByLeaveType()
        {
            Name = "by Leave Type";
            IsGrouped = false;
            FilterCriteria = "LeaveType";            
            LinkedReport = new LinkedReport()
            {
                ReportId = "117AF888-E65E-44da-A043-61A5896FC316",
                ReportCategory = "Absence Status Report",
                ReportMainCategory = "Operations Report"
            };
            Chart = new ReportChart()
            {
                Type = "PieChart",
                Title = "",
                is3D = true,
                isStacked = false,
                Height = 400
            };
        }
        public override string Id
        {
            get
            {
                return "117AF888-E65E-44da-A043-61A5896FC456";
            }
        }
        
        /// <summary>
        /// Determine if report is exported in CSV
        /// </summary>
        public override bool IsExportableToCsv
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// has any lined detailed report
        /// </summary>
        public override bool HasLinkedReport
        {
            get { return true; }
        }

        /// <summary>
        /// Determine if report is exported in PDF
        /// </summary>
        public override bool IsExportableToPdf
        {
            get
            {
                return false;
            }
        }
        public override string Name { get; set; }
        public override bool IsGrouped { get; set; }
        public string FilterCriteria { get; set; }

        /// <summary>
        /// return the desciprion for leave type bi 
        /// </summary>
        public override string ReportDescription
        {
            get { return "Pie Chart - Percentage of cases by leave type"; }
        }
        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns>LOA Feature</returns>
        public override IEnumerable<Feature> RequiredFeatures()
        {
            yield return Feature.LOA;
        }

        /// <summary>
        /// Chart configurations
        /// </summary>
        public override ReportChart Chart
        {
            get; set;
        }

        /// <summary>
        /// linked report details
        /// </summary>
        public override LinkedReport LinkedReport
        {
            get; set;
        }

        /// <summary>
        /// Category for all Absence Status reports is "Business Intelligence".
        /// </summary>
        public override string MainCategory { get; set; } = "Business Intelligence Reports";

        /// <summary>
        /// Category for all Absence Status reports is "Business Intelligence".
        /// </summary>
        public override string Category { get; set; } = "Business Intelligence Report";

        /// <summary>
        /// Icon to set for Business intelligence report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_intelligence-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerKey
            };
            criteria.Filters.Add(GetProductivityCriteria());
            criteria.Filters.Add(GetReportTypeCriteria());
            return criteria;
        }

        /// <summary>
        /// Runs the report for given user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        protected override async Task<ReportResult> RunReportAsync(AT.Entities.Authentication.User user, ReportResult result, ReportGroupType? reportType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            var userReportCriteria = await reportingLogic.GetUserReportCriteriaDataAsync();

            // checks if any case exists for the user
            if (await reportingLogic.UserCaseCount(userReportCriteria) <= 0)
            {
                return null;
            }

           
            return await ReportByLeave(reportingLogic, userReportCriteria, result);
        }

        /// <summary>
        /// Gets the Report by leave
        /// </summary>
        /// <param name="reportingLogic"></param>
        /// <param name="userReportCriteria"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        private async Task<ReportResult> ReportByLeave(ReportingLogic reportingLogic, UserReportCriteriaData userReportCriteria, ReportResult result)
        {
           
            // Set our series data for the chart
            result.SeriesData = await reportingLogic.GetBiReportByLeaveResult(userReportCriteria, result, EmployerIdFilterName);

            // Define our first row, which contains series labels for the x (col 1) and y (col 2~n) axis.
            List<object> labels = new List<object>
            {
                "LeaveType",
                "Count",
                "ChartIndex"
            };
            // Our Y-Axis series data will be defined as all available To Do item types (could be a busy chart)
            // Add our series definition/labels row to the result series data collection.
            result.SeriesData.Insert(0,labels);

            return result;
        }
    }
}