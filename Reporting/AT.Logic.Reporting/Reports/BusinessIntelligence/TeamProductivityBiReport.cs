﻿using AbsenceSoft.Data.Enums;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.BusinessIntelligence
{
    /// <summary>
    /// Team Productivity BI REport
    /// </summary>
    public class TeamProductivityBiReport: BaseReport
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public TeamProductivityBiReport()
        {
            Name = "by Team";
            IsGrouped = false;
            Category = "Productivity Comparison";
            MainCategory = "Business Intelligence Reports";
        }

        /// <summary>
        /// return the desciprion 
        /// </summary>
        public override string ReportDescription
        {
            get { return "Bar chart - To Do's completed vs overdue by team"; }
        }

        /// <summary>
        /// Category for this "Business Intelligence Report" is "Productivity Comparison"
        /// </summary>
        public override string Category { get; set; }

        /// <summary>
        /// Determine the group by
        /// </summary>
        public override bool IsGrouped { get; set; }
        /// <summary>
        /// Report Id
        /// </summary>
        public override string Id { get { return "C29B0D37-B62D-418A-831B-1AB5BDA5F7A9"; } }

        /// <summary>
        /// Name of the Report
        /// </summary>
        public override string Name { get; set; }
      
        /// <summary>
        /// Determine if report is exported in CSV
        /// </summary>
        public override bool IsExportableToCsv
        {
            get
            {
                return false;
            }
        }
        /// <summary>
        /// Determine if report is exported in PDF
        /// </summary>
        public override bool IsExportableToPdf
        {
            get
            {
                return false;
            }
        }
        /// <summary>
        /// Main Categorty of the report
        /// </summary>
        public override string MainCategory { get; set; }       

        /// <summary>
        /// Icon to set for Productivity Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_productivity-comparison.png"; } }

        /// <summary>
        /// Chart configurations
        /// </summary>
        public override ReportChart Chart { get; set; } = new ReportChart()
        {
            Type = "ColumnChart",
            Title = "Team Productivity Report",
            is3D = false,
            isStacked = true,
            Height = 600
        };            
      
        /// <summary>
        /// Get the Report Criteria
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerKey
            };
            criteria.Filters.Add(GetProductivityCriteria());
            criteria.Filters.Add(GetReportTypeCriteria());
            criteria.Filters.RemoveAll(f => f.Name == "ReportType");
            return criteria;
        }

        /// <summary>
        /// Build and Run Team Productivity BI Report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="reportGroupType"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        protected async override Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            result.SeriesData = reportingLogic.RunTeamProductivityBiQueryAsync(result.Criteria, EmployerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc).GetAwaiter().GetResult();
            return await Task.FromResult(result);
        }
    }
}
