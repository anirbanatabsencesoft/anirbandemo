﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using AbsenceSoft.Data.Enums;
using AT.Data.Reporting.Model;

namespace AT.Logic.Reporting.Reports.BusinessIntelligence
{
    public partial class BiAccomodationBySummaryReport : BaseReport
    {
        public BiAccomodationBySummaryReport()
        {
            Name = "by Accommodation Summary";
            MainCategory = "Business Intelligence Reports";
            Category = "Accommodation Reports";
            IsGrouped = false;
        }
        public override string Id
        {
            get { return "A3878C0F-99B8-41B2-AFDE-0BC666CAE2A5"; }
        }
        public override string Name { get; set; }
        public override string Category { get; set; }
        public override string MainCategory { get; set; }

        public override string IconImage
        {
            get { return "absencesoft-temporary-reports-icons_intelligence-report.png"; }
        }

        public override string ReportDescription
        {
            get { return "Pie chart - Percentage of accommodation cases by accommodation type"; }
        }

        /// <summary>
        /// Determine if report is exported in CSV
        /// </summary>
        public override bool IsExportableToCsv
        {
            get
            {
                return false;
            }
        }
        /// <summary>
        /// Determine if report is exported in PDF
        /// </summary>
        public override bool IsExportableToPdf
        {
            get
            {
                return false;
            }
        }
        public override bool IsGrouped { get; set; }

        /// <summary>
        /// Chart configurations
        /// </summary>
        public override ReportChart Chart { get; set; } = new ReportChart()
        {
            Type = "PieChart",
            Title = "Accommodation Count",
            is3D = true,
            isStacked = false,
            Height = 400
        };

        public override bool HasLinkedReport
        {
            get { return false; }
        }

        /// <summary>
        /// has any sub report
        /// </summary>
        public override bool HasSubReport
        {
            get { return true; }
        }

        /// <summary>
        /// linked report details
        /// </summary>
        public override LinkedReport LinkedReport
        {
            get
            {
                return null;
            }
        }


        public override ReportCriteria GetCriteria(User user)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            UserReportCriteriaData data = reportingLogic.GetUserReportCriteriaDataAsync().GetAwaiter().GetResult();
            var startDate = DateTime.UtcNow.Date.AddDays(-30);
            var endDate = DateTime.UtcNow.Date;
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = data.SecurityUser.CustomerId.ToString(),
                StartDate = startDate,
                EndDate = endDate,
                Filters = new List<ReportCriteriaItem>()
                {
                    { GetProductivityCriteria() }
                }
            };
            return criteria;
        }

        protected async override Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            var seriesData = await reportingLogic.RunAccomodationBySummaryReportQueryAsync(result.Criteria, EmployerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc);
            result.SeriesData = seriesData;
            return result;
        }
    }
}
