﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using AT.Data.Reporting.Model;
using AbsenceSoft.Data.Cases;
using System.Globalization;

namespace AT.Logic.Reporting.Reports.BusinessIntelligence
{
    public class BiAccomodationBySimilarlySituatedReport : BaseReport
    {
        public BiAccomodationBySimilarlySituatedReport()
        {
            Name = "of Similarly Situated Employees";
            MainCategory = "Business Intelligence Reports";
            Category = "Accommodation Reports";
            IsGrouped = false;

        }
        public override string Id
        {
            get { return "19E8D52B-8DD2-4EEE-8B37-4EA9F86EEF46"; }
        }
      
        /// <summary>
        /// Determine if report is exported in CSV
        /// </summary>
        public override bool IsExportableToCsv
        {
            get
            {
                return false;
            }
        }
        /// <summary>
        /// Determine if report is exported in PDF
        /// </summary>
        public override bool IsExportableToPdf
        {
            get
            {
                return false;
            }
        }
        public override string Name { get; set; }
        public override string Category { get; set; }
        public override string MainCategory { get; set; }

        /// <summary>
        /// return the report desciprion 
        /// </summary>
        public override string ReportDescription
        {
            get { return "Case list provides high level details about comparable employees with accommodation cases"; }
        }

        public override string IconImage
        {
            get { return "absencesoft-temporary-reports-icons_intelligence-report.png"; }
        }

        public override bool IsGrouped { get; set; }

        /// <summary>
        /// Chart configurations
        /// </summary>
        public override ReportChart Chart { get; set; }

        public override ReportCriteria GetCriteria(User user)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            UserReportCriteriaData data = reportingLogic.GetUserReportCriteriaDataAsync().GetAwaiter().GetResult();
            var startDate = DateTime.UtcNow.Date.AddDays(-30);
            var endDate = DateTime.UtcNow.Date;
            ReportCriteria criteria = new ReportCriteria
            {
                ShowAgeCriteria = true,
                CustomerId = data.SecurityUser.CustomerId.ToString(),
                StartDate = startDate,
                EndDate = endDate,
                Filters = new List<ReportCriteriaItem>()
                {
                    { GetProductivityCriteria() },
                    { GetWorkStateCriteria(data, startDate, endDate) },
                    { GetLocationCriteria(data) },
                    { GetGenderCriteria() }
                }
            };
            return criteria;
        }

        /// <summary>
        /// Method calling the ReportingLogic to run the query and return the ReportResult.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="reportGroupType"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        protected async override Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            UserReportCriteriaData data = await reportingLogic.GetUserReportCriteriaDataAsync();
            var cases = await reportingLogic.RunAccomodationBySimilarlySituatedReportQueryAsync(result.Criteria, EmployerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc);
            result.Items = await GetResultData(user, cases, result.Criteria.CustomerId, data.SecurityUser.EmployerId);
            return result;
        }

        /// <summary>
        /// Method queries ReportingLogic to get the imtermidiate data and forms the list of ResultData
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cases"></param>
        /// <param name="customerId"></param>
        /// <param name="employerId"></param>
        /// <returns></returns>
        private async Task<List<ResultData>> GetResultData(User user, List<Case> cases, string customerId, string employerId)
        {
            List<ResultData> resultItems = new List<ResultData>();
            ReportingLogic reportingLogic = new ReportingLogic(user);
            var accommodationTypes = await reportingLogic.GetAccomodationTypes(customerId, employerId);
            foreach (var q in cases)
            {
                dynamic data = new ResultData();
                var p = data as IDictionary<String, object>;
                p["CaseNumber"] = new LinkItem() { Value = q.CaseNumber, Link = string.Format("/Cases/{0}/View", q.Id) };
                p["FirstName"] = q.Employee.FirstName;
                p["LastName"] = q.Employee.LastName;
                p["DateOfRequest"] = q.CreatedDate.ToString("MM/dd/yyy");

                foreach (var accomType in accommodationTypes)
                {
                    var cd = accomType.Code;
                    p[cd] = q.AccommodationRequest.Accommodations.Count(m => m.Type.Code == cd).ToString();
                }


                p["Determination"] = q.AccommodationRequest.Determination.ToString();
                p["Duration"] = q.AccommodationRequest.SummaryDurationType;
                // Current currency symbol
                p["US"] = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol;

                // Set number as currency without currency symbol
                NumberFormatInfo nfi = CultureInfo.CurrentCulture.NumberFormat;
                nfi = (NumberFormatInfo)nfi.Clone();
                nfi.CurrencySymbol = string.Empty;
                p["Cost"] = string.Format(nfi, "{0:c}", q.AccommodationRequest.Accommodations.Sum(m => m.Cost ?? 0m));

                p["WorkState"] = q.Employee.WorkState ?? string.Empty;
#pragma warning disable CS0618
                p["Location"] = q.Employee.Info.OfficeLocation ?? string.Empty;
#pragma warning restore CS0618
                p["JobTitle"] = string.IsNullOrWhiteSpace(q.Employee.JobTitle) ? string.Empty : q.Employee.JobTitle;
                p["Gender"] = q.Employee.Gender.ToString();
                p["Age"] = q.Employee.DoB.HasValue ? (DateTime.Now.Year - q.Employee.DoB.Value.Year).ToString() : string.Empty;

                resultItems.Add(data);
            }
            return resultItems;
        }
    }
}
