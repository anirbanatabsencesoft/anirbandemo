﻿using AbsenceSoft.Data.Enums;
using AT.Data.Reporting.Model;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.BusinessIntelligence
{
    [Serializable]
    public class BiReportByTotalLeaves : BaseReport
    {
        public BiReportByTotalLeaves()
        {
            Name = "for Total Leaves";
            IsGrouped = false; 
        }
        public override string Id
        {
            get
            {
                return "FE966012-A293-418A-A3F7-8FD853F03ABA";
            }
        }
        public override string Name { get; set; }
        public override bool IsGrouped { get; set; }
        public string FilterCriteria { get; set; }

        /// <summary>
        /// Chart configurations
        /// </summary>
        public override ReportChart Chart
        {
            get; set;
        }

        /// <summary>
        /// linked report details
        /// </summary>
        public override LinkedReport LinkedReport
        {
            get; set;
        } = null;

        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns>LOA Feature</returns>
        public override IEnumerable<Feature> RequiredFeatures()
        {
            yield return Feature.LOA;
        }

        /// <summary>
        /// Determine if report is exported in CSV
        /// </summary>
        public override bool IsExportableToCsv
        {
            get
            {
                return false;
            }
        }
        /// <summary>
        /// Determine if report is exported in PDF
        /// </summary>
        public override bool IsExportableToPdf
        {
            get
            {
                return false;
            }
        }
        /// <summary>
        /// Category for all Absence Status reports is "Business Intelligence".
        /// </summary>
        public override string MainCategory { get; set; } = "Business Intelligence Reports";
        /// <summary>
        /// return the desciprion 
        /// </summary>
        public override string ReportDescription
        {
            get { return "Line Chart - Number of cases by absence reason, comparison by period"; }
        }

        /// <summary>
        /// Category for all Absence Status reports is "Business Intelligence".
        /// </summary>
        public override string Category { get; set; } = "Business Intelligence Report";

        /// <summary>
        /// Icon to set for Business intelligence report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_intelligence-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerKey
            };
            criteria.Filters.Add(GetProductivityCriteria());
            criteria.Filters.Add(GetReportTypeCriteria());
            return criteria;
        }

        /// <summary>
        /// Runs the report for given user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        protected override async Task<ReportResult> RunReportAsync(AT.Entities.Authentication.User user, ReportResult result, ReportGroupType? reportType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            var userReportCriteria = await reportingLogic.GetUserReportCriteriaDataAsync();
            string typeOfReport = string.Empty;
            // checks if any case exists for the user
            if (await reportingLogic.UserCaseCount(userReportCriteria) <= 0)
            {
                return null;
            }
            if (result.Criteria.Filters.Count != 0 && result.Criteria.Filters.FirstOrDefault(m => m.Name == "ReportType") != null && result.Criteria.Filters.FirstOrDefault(m => m.Name == "ReportType").Value != null)
            {
                typeOfReport = result.Criteria.Filters.FirstOrDefault(m => m.Name == "ReportType").Value.ToString();
            }
            switch (typeOfReport)
            {
                case "ByReason":
                    result.Name = "Total Leaves By Reason";
                    IsGrouped = false;
                    LinkedReport = null;
                    result.Chart = new ReportChart()
                    {
                        Type = "LineChart",
                        Title = "Total Leaves By Reason",
                        is3D = false,
                        isStacked = true,
                        Height = 600
                    };
                    await ReportByTotalLeavesByReason(reportingLogic, userReportCriteria, result);
                    break;
                case "ByLocation":
                    result.Name = "Total Leaves By Location";
                    IsGrouped = false;
                    LinkedReport = null;
                    result.Chart = new ReportChart()
                    {
                        Type = "LineChart",
                        Title = "Total Leaves By Location",
                        is3D = false,
                        isStacked = true,
                        Height = 600
                    };
                    await ReportByTotalLeavesByLocation(reportingLogic, userReportCriteria, result);
                    break;
            }

            return result;
        }

        /// <summary>
        /// Gets Report by Total Leaves Reason
        /// </summary>
        /// <param name="reportingLogic"></param>
        /// <param name="userReportCriteria"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        private async Task<ReportResult> ReportByTotalLeavesByReason(ReportingLogic reportingLogic, UserReportCriteriaData userReportCriteria, ReportResult result)
        {
            // Set our series data for the chart
            result.SeriesData = await reportingLogic.GetBiReportTotalLeavesByReasonResult(userReportCriteria, result, EmployerIdFilterName);

            // Define our first row, which contains series labels for the x (col 1) and y (col 2~n) axis.
            List<object> labels = new List<object>
            {
                "Month"
            };

            List<string> reasonList = await reportingLogic.GetCaseReasonsExcludingAccommodation();
            // Our Y-Axis series data will be defined as Case Absence reason added
            labels.AddRange(reasonList.ToArray());

            // Add our series definition/labels row to the result series data collection.
            result.SeriesData.Insert(0, labels);            
            result.Chart.Title = "Total Leaves By Reason";

            return result;
        }

        /// <summary>
        /// Gets Total Leaves by Location
        /// </summary>
        /// <param name="reportingLogic"></param>
        /// <param name="userReportCriteria"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        private async Task<ReportResult> ReportByTotalLeavesByLocation(ReportingLogic reportingLogic, UserReportCriteriaData userReportCriteria, ReportResult result)
        {
            // Set our series data for the chart
            result.SeriesData = await reportingLogic.GetBiReportTotalLeavesByTotalResult(userReportCriteria, result, EmployerIdFilterName);

            // Define our first row, which contains series labels for the x (col 1) and y (col 2~n) axis.
            List<object> labels = new List<object>
            {
                "Month"
            };

            var workStateList = await reportingLogic.GetCaseWorkState(userReportCriteria);
            // Our Y-Axis series data will be defined as Case Absence reason added
            labels.AddRange(workStateList.ToArray());

            // Add our series definition/labels row to the result series data collection.
            result.SeriesData.Insert(0,labels);

            result.Chart.Title = "Total Leaves By Location";
            return result;
        }
    }
}