﻿using AbsenceSoft.Data.Enums;
using AT.Entities.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Reporting.Enums;

namespace AT.Logic.Reporting.Reports.BusinessIntelligence
{
    /// <summary>
    /// Team Compliance Bi Report
    /// </summary>
    public class TeamComplianceBiReport : BaseReport
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public TeamComplianceBiReport()
        {
            Name = "by Team Compliance";
            IsGrouped = false;
            Category = "Productivity Comparison";
            MainCategory = "Business Intelligence Reports";
        }

        /// <summary>
        /// Category for this "Business Intelligence Report" is "Productivity Comparison"
        /// </summary>
        public override string Category { get; set; }
        
        /// <summary>
        /// Report Id
        /// </summary>
        public override string Id { get { return "2E96F17E-8AFE-4E1D-8D39-B39901383FE1"; } }

        /// <summary>
        /// Name of the report
        /// </summary>
        public override string Name { get; set;}

        /// <summary>
        /// Determine group by
        /// </summary>
        public override bool IsGrouped { get; set; }

        /// <summary>
        /// Main category of the report
        /// </summary>
        public override string MainCategory { get; set; }

        /// <summary>
        /// return the desciprion 
        /// </summary>
        public override string ReportDescription
        {
            get { return "Bar chart - To Do's completed vs overdue by team"; }
        }

        /// <summary>
        /// Icon to set for Productivity Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_productivity-comparison.png"; } }

        /// <summary>
        /// Chart configurations
        /// </summary>
        public override ReportChart Chart { get; set; } = new ReportChart()
        {
            Type = "ColumnChart",
            Title = "Case Manager Compliance Report",
            is3D = false,
            isStacked = false,
            Height = 600
        };
       
        /// <summary>
        /// Determine if report is exported in CSV
        /// </summary>
        public override bool IsExportableToCsv
        {
            get
            {
                return false;
            }
        }
        /// <summary>
        /// Determine if report is exported in PDF
        /// </summary>
        public override bool IsExportableToPdf
        {
            get
            {
                return false;
            }
        }
        /// <summary>
        /// Get the Report Criteria
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public override ReportCriteria GetCriteria(User user)
        {

            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerKey
            };
            criteria.Filters.Add(GetProductivityCriteria());
            criteria.Filters.Add(GetReportTypeCriteria());
            criteria.Filters.RemoveAll(f => f.Name == "Duration");
            criteria.Filters.RemoveAll(f => f.Name == "ReportType");

            return criteria;
        }

        /// <summary>
        /// Build and Run Team Compliance Bi Report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="reportGroupType"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        protected async override Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            result.SeriesData = reportingLogic.RunTeamComplianceBiQueryAsync(result.Criteria, EmployerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc).GetAwaiter().GetResult();
            return await Task.FromResult(result);
        }
    }
}
