﻿using AbsenceSoft;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.DailyCases
{
    public partial class DailyCaseReport : BaseReport
    {
        private Dictionary<string, Customer> _cust = new Dictionary<string, Customer>();

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            var data = GetUserReportCriteriaData(user);
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerKey,
                StartDate = DateTime.UtcNow.Date.AddDays(-7),
                EndDate = DateTime.UtcNow.Date
            };
            criteria.Filters.AddRange(GetOrganizationTypeCriteria(data));
            if (IncludeOrganizationGrouping)
            {
                criteria.Filters.Add(GetOrganizationTypeGroupByCriteria(data));
            }
            return criteria;            
        }

        /// <summary>
        /// Gets formatted Data from passed Case
        /// </summary>
        /// <param name="caseValue"></param>
        /// <returns></returns>
        protected ResultData GetData(Case caseValue)
        {
            var cust = _cust.ContainsKey(caseValue.CustomerId) ? _cust[caseValue.CustomerId] : caseValue.Customer;
            _cust[caseValue.CustomerId] = cust;

            string displayCaseNumber = caseValue.CaseNumber;
            string displayFullName = caseValue.Employee.FullName;
            string displayReason = caseValue.Reason.Name;
            string displayLeaveType = caseValue.CaseType.ToString();
            string displayCreatedOn = caseValue.CreatedDate.ToString("MM/dd/yyyy");
            var evt = caseValue.FindCaseEvent(CaseEventType.CaseCreated);
            if (evt != null && evt.EventDate != DateTime.MinValue)
            {
                displayCreatedOn = evt.EventDate.ToString("MM/dd/yyyy");
            }
            DateTime? displayRTW = GetCaseEventDate(caseValue, CaseEventType.ReturnToWork);
            DateTime? displayERTW = GetCaseEventDate(caseValue, CaseEventType.EstimatedReturnToWork);

            dynamic data = new ResultData();
            data.CaseNumber = string.IsNullOrWhiteSpace(displayCaseNumber) ? null : new LinkItem() { Value = caseValue.CaseNumber, Link = string.Format("/Cases/{0}/View", caseValue.Id) };
            data.EmployeeNumber = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = caseValue.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", caseValue.Employee.Id) };
            data.Name = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = caseValue.Employee.FullName, Link = string.Format("/Employees/{0}/View", caseValue.Employee.Id) };
            if (cust.HasFeature(Feature.ADA) && caseValue.IsAccommodation && caseValue.AccommodationRequest != null && caseValue.AccommodationRequest.Accommodations != null && caseValue.AccommodationRequest.Accommodations.Any())
            {
                data.Duration = string.Join(", ", caseValue.AccommodationRequest.Accommodations.Select(a => a.Duration.ToString().SplitCamelCaseString()).Distinct());
            }
            if (cust.HasFeature(Feature.LOA))
            {
                data.Reason = displayReason;
            }
            data.CaseType = displayLeaveType;
            data.CreatedOn = displayCreatedOn;
            data.StartDate = caseValue.StartDate.ToString("MM/dd/yyyy");
            data.EndDate = caseValue.EndDate.ToString("MM/dd/yyyy");
            data.Status = caseValue.Status.ToString().SplitCamelCaseString();
            if (cust.HasFeature(Feature.ADA) && caseValue.IsAccommodation && caseValue.AccommodationRequest != null && caseValue.AccommodationRequest.Accommodations != null && caseValue.AccommodationRequest.Accommodations.Any())
            {
                data.Accommodations = string.Join(", ", caseValue.AccommodationRequest.Accommodations.Select(a => a.Type.Code).Distinct());
            }
            data.AssignedTo = caseValue.AssignedToName;
            data.WorkState = caseValue.Employee.WorkState;
#pragma warning disable CS0618 // Type or member is obsolete
            data.Location = caseValue.Employee.Info.OfficeLocation;
#pragma warning restore CS0618 // Type or member is obsolete
            data.ReturnToWork = displayRTW != null ? displayRTW.ToString("MM/dd/yyyy") : string.Empty;
            data.EstimatedReturnToWork = displayERTW != null ? displayERTW.ToString("MM/dd/yyyy") : string.Empty;

            return data;
        }

        /// <summary>
        /// Runs Report in Async way
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="reportGroupType"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        protected override async Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            switch (reportGroupType)
            {
                default:
                case ReportGroupType.CaseReport:
                    {
                        result.Name = "Case Report Detail";
                        IsGrouped = false;
                        CategoryOrder = 1;
                        GroupAutoTotals = false;
                        await DayReport(user, result, pageNumber, pageSize);
                        break;
                    }
                case ReportGroupType.CaseReportByDay:
                    {
                        result.Name = "Case Report by Day";
                        IsGrouped = true;
                        CategoryOrder = 2;
                        GroupAutoTotals = false;
                        await DailyCasesByDayReport(user, result, pageNumber, pageSize);
                        break;
                    }
                case ReportGroupType.CaseReportByOrganization:
                    {
                        result.Name = "Case Report by Organization";
                        IsGrouped = true;
                        CategoryOrder = 3;
                        GroupAutoTotals = false;
                        IncludeOrganizationGrouping = true;
                        await DailyCasesByOrganizationReport(user, result, pageNumber, pageSize);
                        break;
                    }
            }
            return result;
        }
    }
}