﻿using AbsenceSoft;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.DailyCases
{
    [Serializable]
    public partial class DailyCaseReport : BaseReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "688BDE87-48AD-4560-84A7-982D5DE8D73A"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get; set; } = "Case Load";

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get; set; }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get; set; }

        /// <summary>
        /// Category for all Absence Status reports is "Absence Status".
        /// </summary>
        public override string MainCategory { get; set; } = "Operations Report";

        /// <summary>
        /// Category for all Daily Cases reports is "New Case Load".
        /// </summary>
        public override string Category { get; set; } = "New Case Load";
        public override string ReportDescription
        {
            get { return "Case list provides high level details about cases open at any time during the requested period"; }
        }

        /// <summary>
        /// Icon to set for Absence Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get; set; }

        /// <summary>
        /// Determines whether the specified user is visible.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public override bool IsVisible(User user)
        {
            // Ensure customer is not parrot.
            return user != null && user.CustomerKey != "546e5097a32aa00d60e3210a";
        }

        /// <summary>
        /// List of fields for grouping
        /// </summary>
        public override Dictionary<string, List<string>> AvailableGroupingFields => new Dictionary<string, List<string>>(){
            {(ReportGroupType.CaseReport).ToString(), new List<string>(0)},
            {(ReportGroupType.CaseReportByDay).ToString(), new List<string>(0)},
            {(ReportGroupType.CaseReportByOrganization).ToString(), new List<string>(0)}
        };


        /// <summary>
        /// Generates Daily Case Report Day
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<ReportResult> DayReport(User user, ReportResult result, int pageNumber = 1, int pageSize = 10)
        {
            var userData = GetUserReportCriteriaData(user);
            ReportingLogic reportingLogic = new ReportingLogic(user);            
            List<Case> caseList = await reportingLogic.GetCaseListForDailyCaseReport(userData, result.Criteria, EmployerIdFilterName, pageNumber, pageSize);
            List<ResultData> resultItems = new List<ResultData>();

            foreach (var caseItem in caseList)
            {
                resultItems.Add(GetData(caseItem));
            }
            result.Items = resultItems.OrderBy(r => ((dynamic)r).CreatedOn).ToList();
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Generates Daily Case Report By Day
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<ReportResult> DailyCasesByDayReport(User user, ReportResult result, int pageNumber = 1, int pageSize = 10)
        {
            var userData = GetUserReportCriteriaData(user);
            ReportingLogic reportingLogic = new ReportingLogic(user);
            List<Case> caseList = await reportingLogic.GetCaseListForDailyCaseReport(userData, result.Criteria, EmployerIdFilterName, pageNumber, pageSize);

            var groupQuery = caseList.GroupBy(n =>
            {
                var evt = n.FindCaseEvent(CaseEventType.CaseCreated);
                if (evt != null)
                {
                    return evt.EventDate.Date;
                }
                return n.CreatedDate.Date;
            })
               .OrderByDescending(g => g.Key)
               .Select(g => new
               {
                   label = g.Key.ToString("MM/dd/yyyy"),
                   data = g.Select(x => x).ToList()
               });

            List<ResultData> resultItems;
            result.Groups = new List<ResultDataGroup>();
            int keyCount = 1;
            foreach (var t in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (var td in t.data)
                {
                    resultItems.Add(GetData(td));
                }

                result.Groups.Add(new ResultDataGroup()
                {
                    Key = keyCount,
                    Label = t.label,
                    Items = new List<ResultData>().AddRangeFluid(resultItems.OrderBy(r => ((dynamic)r).CreatedOn))
                });

                keyCount++;
            }
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Generates Daily Case Report by Organization
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<ReportResult> DailyCasesByOrganizationReport(User user, ReportResult result, int pageNumber = 1, int pageSize = 10)
        {
            var userData = GetUserReportCriteriaData(user);
            ReportingLogic reportingLogic = new ReportingLogic(user);
            List<Case> caseList = await reportingLogic.GetCaseListForDailyCaseReport(userData, result.Criteria, EmployerIdFilterName, pageNumber, pageSize);

            string grouptypeCode = null;
            if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "OrganizationTypeGrouping") != null)
            {
                grouptypeCode = result.Criteria.ValueOf<string>("OrganizationTypeGrouping");
            }

            List<string> customerIds = caseList.Select(cs => cs.CustomerId).Distinct().ToList();
            List<string> employerIds = caseList.Select(cs => cs.EmployerId).Distinct().ToList();
            List<string> employeeNumbers = caseList.Select(cs => cs.Employee.EmployeeNumber).Distinct().ToList();
            List<EmployeeOrganization> employeeOrganizations = null;
            employeeOrganizations = SetEmployeeOrganization(grouptypeCode, customerIds, employerIds, employeeNumbers);

            List<EmployeeOrganization> employeeUniqueOrganizations = new List<EmployeeOrganization>();
            foreach (EmployeeOrganization empOrg in employeeOrganizations)
            {
                if (empOrg.TypeCode != OrganizationType.OfficeLocationTypeCode ||
                    ((empOrg.Dates == null || empOrg.Dates.IncludesToday(true))
                    && (!employeeUniqueOrganizations.Any(empUniqueOrg => empUniqueOrg.TypeCode == OrganizationType.OfficeLocationTypeCode && empUniqueOrg.EmployeeNumber == empOrg.EmployeeNumber && empUniqueOrg.CustomerId == empOrg.CustomerId && empUniqueOrg.EmployerId == empOrg.EmployerId))))
                {
                    employeeUniqueOrganizations.Add(empOrg);
                }
            }
            var groupQuery = employeeUniqueOrganizations.GroupBy(org => new { CustomerId = org.CustomerId, EmployerId = org.EmployerId, Code = org.Code })
           .Select(g => new
           {
               label = employeeUniqueOrganizations.FirstOrDefault(empOrgs => empOrgs.Code == g.Key.Code).Name,
               data = g.Select(x => x).ToList()
           }).OrderBy(m => m.label);

            List<ResultData> resultItems;

            result.Groups = new List<ResultDataGroup>();
            int keyCount = 1;
            //Avoid displaying cases under more than one office location code.
            List<string> empLocationCase = new List<string>();
            foreach (var t in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (var org in t.data)
                {
                    SetEmployeeLocationCase(caseList, resultItems, empLocationCase, org);
                }
                if (resultItems.Any())
                {
                    result.Groups.Add(new ResultDataGroup()
                    {
                        Key = keyCount,
                        Label = t.label,
                        Items = new List<ResultData>().AddRangeFluid(resultItems.OrderBy(r => ((dynamic)r).CreatedOn).ToList())
                    });
                    keyCount++;
                }
            }
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Set Employee Organization
        /// </summary>
        /// <param name="grouptypeCode"></param>
        /// <param name="customerIds"></param>
        /// <param name="employerIds"></param>
        /// <param name="employeeNumbers"></param>
        /// <returns></returns>
        private List<EmployeeOrganization> SetEmployeeOrganization(string grouptypeCode, List<string> customerIds, List<string> employerIds, List<string> employeeNumbers)
        {
            List<EmployeeOrganization> employeeOrganizations;
            if (grouptypeCode != null)
            {
                employeeOrganizations = EmployeeOrganization.AsQueryable().Where(org => org.TypeCode == grouptypeCode && customerIds.Contains(org.CustomerId) && employerIds.Contains(org.EmployerId) && employeeNumbers.Contains(org.EmployeeNumber)).OrderByDescending(o => o.Dates).ToList();
            }
            else
            {
                employeeOrganizations = EmployeeOrganization.AsQueryable().Where(org => customerIds.Contains(org.CustomerId) && employerIds.Contains(org.EmployerId) && employerIds.Contains(org.EmployerId) && employeeNumbers.Contains(org.EmployeeNumber)).OrderByDescending(o => o.Dates).ToList();
            }

            return employeeOrganizations;
        }

        /// <summary>
        /// Set Employee Location Case
        /// </summary>
        /// <param name="caseList"></param>
        /// <param name="resultItems"></param>
        /// <param name="empLocationCase"></param>
        /// <param name="org"></param>
        private void SetEmployeeLocationCase(List<Case> caseList, List<ResultData> resultItems, List<string> empLocationCase, EmployeeOrganization org)
        {
            foreach (Case td in caseList.Where(cs => cs.Employee.EmployeeNumber == org.EmployeeNumber && cs.CustomerId == org.CustomerId && cs.EmployerId == org.EmployerId))
            {
                if (IsOfficeLocationTypeCode(org.TypeCode) && EmployeeLocationCaseExists(empLocationCase, td.Id))
                {
                    continue;
                }
                ResultData data = GetData(td);
                if (data != null)
                {
                    resultItems.Add(data);
                }
                if (IsOfficeLocationTypeCode(org.TypeCode) && !EmployeeLocationCaseExists(empLocationCase, td.Id))
                {
                    empLocationCase.Add(td.Id.ToUpper());
                }
            }
        }

        /// <summary>
        /// Validates Office Location Type Code
        /// </summary>
        /// <param name="orgTypeCode"></param>
        /// <returns></returns>
        private bool IsOfficeLocationTypeCode(string orgTypeCode)
        {
            if (string.IsNullOrWhiteSpace(orgTypeCode))
            {
                return false;
            }
            if (orgTypeCode.ToUpper().TrimEnd() != OrganizationType.OfficeLocationTypeCode.ToUpper().TrimEnd())
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Checks Employee Location Case exists 
        /// </summary>
        /// <param name="empLocationCase"></param>
        /// <param name="caseId"></param>
        /// <returns></returns>
        private bool EmployeeLocationCaseExists(List<string> empLocationCase, string caseId)
        {
            if (empLocationCase == null)
            {
                return false;
            }
            if (empLocationCase.Contains(caseId.ToUpper()))
            {
                return false;
            }
            return true;
        }
    }
}