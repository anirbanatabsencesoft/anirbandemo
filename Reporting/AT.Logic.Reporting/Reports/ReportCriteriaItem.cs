﻿using AbsenceSoft;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#pragma warning disable S3776

namespace AT.Logic.Reporting.Reports
{
    [Serializable]
    public class ReportCriteriaItem : BaseNonEntity
    {
        public ReportCriteriaItem()
        {
            Options = new List<ReportCriteriaItemOption>();
            OnChangeMethodName = null;
            KeepOptionAll = false;
            Values = new List<object>();
        }

        /// <summary>
        /// Gets or sets the formal name of the field/criteria item, typically without spaces, camel-case, e.g. FirstName.
        /// </summary>
        [BsonRequired]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the textual label or prompt used to gather the criteria, e.g. "First Name".
        /// </summary>
        [BsonRequired]
        public string Prompt { get; set; }

        /// <summary>
        /// Gets or sets the type of control for displaying the criteria prompt in the UI.
        /// </summary>
        public ControlType ControlType { get; set; }

        /// <summary>
        /// Gets or sets the list of options, if any, for the criteria prompt in the UI to choose from
        /// for select types, checkbox lists, etc.
        /// </summary>
        public List<ReportCriteriaItemOption> Options { get; set; }

        /// <summary>
        /// Gets or sets whether or not this field is required by the user to be populated in order
        /// to query/run the report.
        /// </summary>
        public bool Required { get; set; }

        /// <summary>
        /// Gets or sets the value (either default value or populated by the user, etc.) for this criteria item.
        /// This value may be a Date, a string, a number, a Boolean, etc.
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// Gets or sets the values when there is a multi-select for this criteria item. This should be
        /// an array of objects such as Dates, strings, numbers, Booleans, etc.
        /// </summary>
        /// <value>
        /// The values.
        /// </value>
        public List<object> Values { get; set; }

        /// <summary>
        /// Gets or sets the tweener text that is used to separate the name and value as a representative
        /// plain English phrase.
        /// </summary>
        public string Tweener { get; set; }

        /// <summary>
        /// Gets the criteria value cast as type T.
        /// </summary>
        /// <typeparam name="T">The type of criteria value expected to be cast and return as</typeparam>
        /// <returns>The current value cast as type T or default(T).</returns>
        public T ValueAs<T>()
        {
            return Convert<T>(Value);
        } // ValueAs<T>

        /// <summary>
        /// Gets the criteria values cast as a list of type T.
        /// </summary>
        /// <typeparam name="T">The type of criteria value expected to be cast and return as</typeparam>
        /// <returns>The current value cast as type T or an empty list.</returns>
        public List<T> ValuesAs<T>()
        {
            return Values == null ? new List<T>(0) : Values.Select(v => Convert<T>(v)).ToList();
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            if (Value == null && (Values == null || !Values.Any()))
            {
                return string.Empty;
            }

            string niceValue = null;
            if (Options != null && Options.Any())
            {
                Func<object, string> getNiceValue = new Func<object, string>(v =>
                {
                    if (v == null)
                        return "";
                    var opt = Options.FirstOrDefault(o => v.Equals(o.Value));
                    if (opt == null)
                        opt = Options.FirstOrDefault(o => v.ToString() == o.Value.ToString());
                    if (opt != null)
                        return opt.Text;
                    return "";
                });
                if (Values != null && Values.Any())
                    niceValue = Values.Count == 1 ? getNiceValue(Values[0]) : string.Join("' or '", Values.Select(v => getNiceValue(v) ?? v));
                else if (Value != null)
                    niceValue = getNiceValue(Value);
            }

            switch (ControlType)
            {
                case ControlType.Date:
                    niceValue = System.Convert.ToDateTime(Value).Date.ToString("MM/dd/yyyy");
                    break;
                case ControlType.Minutes:
                    int min = System.Convert.ToInt32(Value);
                    niceValue = min.ToFriendlyTime();
                    break;
                case ControlType.Numeric:
                    niceValue = Value.ToString();
                    break;
                case ControlType.CheckBox:
                    if (Value.Equals(true))
                        return string.Concat("is ", Prompt);
                    return string.Concat("is not ", Prompt);
                case ControlType.CheckBoxList:
                    if (Values != null && Values.Any())
                        return string.Concat(
                            Prompt, " ",
                            Values.Count == 1 ? "is '" : (Tweener ?? "is ('"),
                            niceValue ?? (Values.Count == 1 ? Values[0].ToString() : string.Join("' or '", Values)),
                            Values.Count == 1 ? "'" : "')");
                    niceValue = niceValue ?? (Value == null ? "" : Value.ToString());
                    break;
            }

            if (niceValue == null && Value is string && string.IsNullOrWhiteSpace(Value.ToString()))
                return string.Empty;

            return string.Format("{0} {1} '{2}'", Prompt, Tweener ?? "is", niceValue ?? Value);
        }// ToStrings

        public string OnChangeMethodName { get; set; }

        public bool KeepOptionAll { get; set; }

        /// <summary>
        /// Convert an object to strongly typed
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="v"></param>
        /// <returns></returns>
        private T Convert<T>(object v)
        {
            // If the value is null, return the default of type T
            if (v == null)
                return default(T);

            // If the type matches, just type-cast it and return the result
            if (v is T)
                return (T)v;

            // Get our type of T so we can use it for other tests and ensure .NET reflection caches the result
            //  as an instance on our memory heap for performance reasons
            Type typeOfT = typeof(T);

            // If it is a nullable type, we have to get the base type
            if (typeOfT.IsGenericType && typeOfT.GetGenericTypeDefinition() == typeof(Nullable<>))
                typeOfT = typeOfT.GetGenericArguments().First();

            // If the type matches, just type-cast it and return the result
            if (v.GetType() == typeOfT) return (T)v;

            // If it is an enum, we can't explicitly use Convert.ChangeType, so we have to use the Enum helpers
            //  for this instead.
            if (typeOfT.IsEnum)
            {
                int iVal;
                if (v is string && int.TryParse(v.ToString(), out iVal))
                {
                    if (!Enum.IsDefined(typeOfT, iVal))
                        return default(T);
                    return (T)Enum.ToObject(typeOfT, iVal);
                }
                else if (v is long)
                    v = System.Convert.ToInt32(v);

                if (!Enum.IsDefined(typeOfT, v))
                    return default(T);
                return (T)Enum.ToObject(typeOfT, v);
            }

            // If it is an assignable type, then use the ChangeType method.
            if (typeOfT.IsInstanceOfType(v))
                return (T)System.Convert.ChangeType(v, typeOfT);

            if (typeOfT == typeof(int))
            {
                int intValue;
                if (int.TryParse(System.Convert.ToString(v), out intValue))
                {
                    return (T)(object)intValue;
                }
            }

            // Can't figure anything else out, so just return the default of T again. Maybe this
            //  should attempt to throw an exception instead, will need to look into that.
            return default(T);
        }
    }
}

#pragma warning restore S3776
