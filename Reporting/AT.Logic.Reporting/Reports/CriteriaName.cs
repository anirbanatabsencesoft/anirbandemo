﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports
{
    /// <summary>
    /// Defines the criteria names as constants
    /// </summary>
    public static class CriteriaName
    {
        public const string CaseStatus = "CaseStatus";
        public const string AbsenceReason = "AbsenceReason";
        public const string WorkState = "WorkState";
        public const string Location = "Location";
        public const string EmployeeNumber = "EmployeeNumber";
    }
}
