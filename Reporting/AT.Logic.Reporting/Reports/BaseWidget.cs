﻿using AbsenceSoft;
using AbsenceSoft.Data.Dashboards;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace AT.Logic.Reporting.Reports
{
    [Serializable]
    public abstract class BaseWidget<TWidgetModel> where TWidgetModel : BaseWidgetModel, new()
    {
        /// <summary>
        /// Gets the unique identifier for this widget so it can be uniquely identified
        /// among any list of widgets. Typically a GUID
        /// </summary>
        public abstract string Id { get; }

        /// <summary>
        /// When overridden in a derived class, gets the name of the widget for display. Also represents
        /// the widget title or a portion of the widget title based on widget requirements.
        /// </summary>
        public abstract string Name { get; }

        /// <summary>
        /// When overridden in a derived class, gets the name of the widget category for display.
        /// </summary>
        public abstract string Category { get; }

        /// <summary>
        /// When overridden in a derived class, gets the name of the widget main category to bifurcate widgets.
        /// </summary>
        public abstract string MainCategory { get; }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this widget should be displayed within it's category
        /// </summary>
        public virtual int CategoryOrder { get { return 0; } }

        /// <summary>
        /// When overridden in a derived class, gets name of the icon image to set in front end
        /// </summary>
        public abstract string IconImage { get; }

        /// <summary>
        /// Gets the settings display.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <returns></returns>
        public virtual string GetSettingsDisplay(List<WidgetSetting> settings)
        {
            return settings == null || !settings.Any() ? null : string.Join(", ", settings);
        }

        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// widget to be run/included in the widgets list.
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<Feature> RequiredFeatures() { return new List<Feature>(0); }

        /// <summary>
        /// Determines whether the specified widget is visible to the given user for any valid reason.
        /// </summary>
        /// <param name="user">The user to check against.</param>
        /// <returns>
        ///   <c>true</c> if the specified widget is visible; otherwise, <c>false</c>.
        /// </returns>
        public virtual bool IsVisible(User user) { return true; }

        /// <summary>
        /// Gets a value indicating whether this instance is viewable.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is viewable; otherwise, <c>false</c>.
        /// </value>
        public virtual bool IsViewable { get { return true; } }

        /// <summary>
        /// Gets the default settings.
        /// </summary>
        /// <value>
        /// The default settings.
        /// </value>
        public abstract List<WidgetSetting> DefaultSettings { get; }

        /// <summary>
        /// Evaluates the widget using the specified user and settings and returns the widget model
        /// fully populated or containing error information etc.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="settings">The settings.</param>
        /// <returns></returns>
        public TWidgetModel Evaluate(User user, List<WidgetSetting> settings = null)
        {
            using (new InstrumentationContext(string.Format("BaseWidget`{0}.Evaluate", typeof(TWidgetModel).Name)))
            {
                Stopwatch timer = new Stopwatch();
                timer.Start();
                TWidgetModel model = new TWidgetModel();
                model.RequestDate = DateTime.UtcNow;
                model.RequestedBy = user ?? User.System;
                model.CustomerId = model.RequestedBy.CustomerId;
                model.Settings = settings ?? DefaultSettings;
                try
                {
                    Evaluate(model);
                }
                catch (AbsenceSoftAggregateException aggEx)
                {
                    aggEx.Messages.ForEach(m => model.AddError(m));
                }
                catch (AbsenceSoftException absEx)
                {
                    model.AddError(absEx.Message);
                }
                catch (Exception ex)
                {
                    model.AddError(ex.Message);
                }
                timer.Stop();
                model.CompletedIn = timer.ElapsedMilliseconds;
                return model;
            }
        }

        /// <summary>
        /// Evaluates the specified model and populates the result into that model.
        /// </summary>
        /// <param name="model">The model to evaluate and populate the result to.</param>
        protected abstract void Evaluate(TWidgetModel model);
    }
}
