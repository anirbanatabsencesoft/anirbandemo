﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using AbsenceSoft;

namespace AT.Logic.Reporting.Reports.CustomReports._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269
{
    /// <summary>
    /// User Activity Report class
    /// </summary>
    [Serializable]
    public class UserActivityReport : AmazonReport
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public UserActivityReport()
        {
            Name = "by User Summary";
        }
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "20BAA0CD-CB4C-4FF7-88A9-24B2AA85F1F7"; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 99; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return false; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Accommodation Status Report".
        /// </summary>
        public override string Category { get { return "User Activity Report"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get; set; }

        /// <summary>
        /// Icon to set for Accommodation Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether to show the auto
        /// generated total counts at the bottom of the group page.
        /// </summary>
        /// <value>
        ///   <c>true</c> if group auto totals; otherwise, <c>false</c>.
        /// </value>
        public override bool GroupAutoTotals { get { return false; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get; set; }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerKey,
                StartDate = DateTime.UtcNow.Date.GetFirstDayOfMonth(),
                EndDate = DateTime.UtcNow.Date
            };
            return criteria;
        }

        
        /// <summary>
        /// Run User Activity Report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="reportGroupType"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        protected async override Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);

            (List<AbsenceSoft.Data.Security.User> users, Dictionary<string, int> toDos, Dictionary<string, int> caseNotes, Dictionary<string, int> empNotes, Dictionary<string, int> attachments) = reportingLogic.RunUserActivityReportQueryAsync(result.Criteria, pageNumber, pageSize, sortBy, orderByAsc).GetAwaiter().GetResult();
           
            List<ResultData> resultItems = new List<ResultData>();
            foreach (var item in users)
            {
                ResultData data = new ResultData
                {
                    ["LastName"] = item.LastName ?? "",
                    ["FirstName"] = item.FirstName ?? "",
                    ["Email"] = item.Email ?? "",
                    ["CompletedToDos"] = (toDos.ContainsKey(item.Id) ? toDos[item.Id] : 0).ToString("N0"),
                    ["Notes"] = ((caseNotes.ContainsKey(item.Id) ? caseNotes[item.Id] : 0) + (empNotes.ContainsKey(item.Id) ? empNotes[item.Id] : 0)).ToString("N0"),
                    ["Attachments"] = (attachments.ContainsKey(item.Id) ? attachments[item.Id] : 0).ToString("N0")
                };
                resultItems.Add(data);
            }
            result.Items = resultItems;
            return await Task.FromResult(result);
        }
    }
}
