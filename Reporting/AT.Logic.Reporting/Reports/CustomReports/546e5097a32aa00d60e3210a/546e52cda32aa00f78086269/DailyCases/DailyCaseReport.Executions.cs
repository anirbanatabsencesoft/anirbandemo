﻿using AbsenceSoft;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.CustomReports._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269.DailyCases
{   
    public partial class DailyCasesReport : AmazonReport
    {
        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerKey,
                StartDate = DateTime.UtcNow.Date.AddDays(-7),
                EndDate = DateTime.UtcNow.Date
            };
            return criteria;
        }


        /// <summary>
        /// Build and run Case report query
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="reportGroupType"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <param name="groupByColumns"></param>
        /// <returns></returns>
        protected async override Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {

            switch (reportGroupType)
            {
                default:
                case ReportGroupType.None:
                    {
                        result.Name = "Case Report Detail";
                        IsGrouped = false;
                        CategoryOrder = 1;
                        GroupAutoTotals = false;
                        await DayReport(user, result, pageNumber, pageSize);
                        break;
                    }
                case ReportGroupType.CaseReportByDay:
                    {
                        result.Name = "Case Report by Day";
                        IsGrouped = true;
                        CategoryOrder = 2;
                        GroupAutoTotals = false;
                        await DailyCasesByDayReport(user, result, pageNumber, pageSize);
                        break;
                    }               
            }
            return result;
        }
        

        

        private Dictionary<string, Customer> customerDictionary = new Dictionary<string, Customer>();

        /// <summary>
        /// Gets the data.
        /// </summary>
        /// <param name="td">The td.</param>
        /// <returns></returns>
        protected virtual ResultData GetData(Case td)
        {

            string displayCreatedOn = td.CreatedDate.ToString("MM/dd/yyyy");
            var evt = td.FindCaseEvent(CaseEventType.CaseCreated);
            if (evt != null && evt.EventDate != DateTime.MinValue)
            {
                displayCreatedOn = evt.EventDate.ToString("MM/dd/yyyy");
            }
            dynamic data = new ResultData();
            data.CaseNumber = string.IsNullOrWhiteSpace(td.CaseNumber) ? null : new LinkItem() { Value = td.CaseNumber, Link = string.Format("/Cases/{0}/View", td.Id) };
            data.EmployeeNumber = string.IsNullOrWhiteSpace(td.Employee.FullName) ? null : new LinkItem() { Value = td.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", td.Employee.Id) };
            data.Name = string.IsNullOrWhiteSpace(td.Employee.FullName) ? null : new LinkItem() { Value = td.Employee.FullName, Link = string.Format("/Employees/{0}/View", td.Employee.Id) };
            CustomField employeeClass = td.Employee.CustomFields.FirstOrDefault(cf => cf.Name == "EmployeeClass");
            data.Class = employeeClass == null ? "" : employeeClass.SelectedValueText;
            data.CreatedOn = displayCreatedOn ?? string.Empty;
            data.CaseType = td.CaseType.ToString() ?? string.Empty;
            SetCustomerRelatedData(td, data);
            data.StartDate = td.StartDate.ToString("MM/dd/yyyy");
            data.EndDate = td.EndDate.HasValue ? td.EndDate.ToString("MM/dd/yyyy") : string.Empty;
            data.Status = td.Status.ToString().SplitCamelCaseString();
            data.AssignedTo = td.AssignedToName ?? string.Empty;
            data.WorkState = td.Employee.WorkState ?? string.Empty;
            data.Location = td.CurrentOfficeLocation ?? "";
            data.Email = td.Employee.Info.Email ?? string.Empty;
            return data;
        }

        /// <summary>
        /// Set Customer specific data in result
        /// </summary>
        /// <param name="td"></param>
        /// <param name="data"></param>
        private void SetCustomerRelatedData(Case td, dynamic data)
        {
            customerDictionary[td.CustomerId] = customerDictionary.ContainsKey(td.CustomerId) ? customerDictionary[td.CustomerId] : td.Customer;
            var cust = customerDictionary[td.CustomerId];

            data.Duration = string.Empty;
            if (cust.HasFeature(Feature.ADA) && td.IsAccommodation && td.AccommodationRequest != null && td.AccommodationRequest.Accommodations != null && td.AccommodationRequest.Accommodations.Any())
            {
                data.Duration = string.Join(", ", td.AccommodationRequest.Accommodations.Select(a => a.Duration.ToString().SplitCamelCaseString()).Distinct());
            }
           
            data.Reason = string.Empty;
            if (cust.HasFeature(Feature.LOA))
            {
                data.Reason = !string.IsNullOrWhiteSpace(td.Reason?.Name) ? td.Reason.Name : string.Empty;
            }            

            data.Accommodations = string.Empty;
            if (cust.HasFeature(Feature.ADA) && td.IsAccommodation && td.AccommodationRequest != null && td.AccommodationRequest.Accommodations != null && td.AccommodationRequest.Accommodations.Any())
            {
                data.Accommodations = string.Join(", ", td.AccommodationRequest.Accommodations.Select(a => a.Type.Code).Distinct());
            }            

            data.WorkRelated = "";
            if (td.AccommodationRequest != null && td.AccommodationRequest.Accommodations != null)
            {
                data.WorkRelated = td.AccommodationRequest.Accommodations.Any(r => r.IsWorkRelated == true) ? "1" : td.AccommodationRequest.Accommodations.Any(r => r.IsWorkRelated == false) ? "0" : "";
            }
            
            data.CostCenter = td.Employee.CostCenterCode ?? "";
        }
    }
}
