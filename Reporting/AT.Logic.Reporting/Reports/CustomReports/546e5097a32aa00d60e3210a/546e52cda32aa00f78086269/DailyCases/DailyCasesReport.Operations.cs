﻿using AbsenceSoft;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.CustomReports._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269.DailyCases
{
    [Serializable]
    public partial class DailyCasesReport : AmazonReport
    {
        public DailyCasesReport()
        {
            Name = "New Case Load";
        }
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "06EE4676-EA21-41F6-9DCF-EDE65D8D3B3E"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get; set; }

        /// <summary>
        /// Determine if Report is Grouped
        /// </summary>
        public override bool IsGrouped { get; set; }

        /// <summary>
        /// Category for all Absence Status reports is "Absence Status".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all Daily Cases reports is "New Case Load".
        /// </summary>
        public override string Category { get { return "New Case Load"; } }

        /// <summary>
        /// Icon to set for Absence Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get { return null; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 1; } }

        /// <summary>
        /// List of fields for grouping
        /// </summary>
        public override Dictionary<string, List<string>> AvailableGroupingFields => new Dictionary<string, List<string>>(){
            {(ReportGroupType.None).ToString(), new List<string>(0)},
            {(ReportGroupType.CaseReportByDay).ToString(), new List<string>(0)}            
        };
        /// <summary>
        /// Generates Daily Case Report Day
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<ReportResult> DayReport(User user, ReportResult result, int pageNumber = 1, int pageSize = 10)
        {
            var userData = GetUserReportCriteriaData(user);
            ReportingLogic reportingLogic = new ReportingLogic(user);
            List<Case> caseList = await reportingLogic.GetCaseListForCustomDailyCaseReport(userData, result.Criteria, EmployerIdFilterName, pageNumber, pageSize);
            List<ResultData> resultItems = new List<ResultData>();

            foreach (var caseItem in caseList)
            {
                resultItems.Add(GetData(caseItem));
            }
            result.Items = resultItems;
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Generates Daily Case Report By Day
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<ReportResult> DailyCasesByDayReport(User user, ReportResult result, int pageNumber = 1, int pageSize = 10)
        {
            var userData = GetUserReportCriteriaData(user);
            ReportingLogic reportingLogic = new ReportingLogic(user);
            List<Case> caseList = await reportingLogic.GetCaseListForCustomDailyCaseReport(userData, result.Criteria, EmployerIdFilterName, pageNumber, pageSize);

            var groupQuery = caseList.GroupBy(n =>
            {
                var evt = n.FindCaseEvent(CaseEventType.CaseCreated);
                if (evt != null)
                {
                    return evt.EventDate.Date;
                }
                return n.CreatedDate.Date;
            })
               .OrderByDescending(g => g.Key)
               .Select(g => new
               {
                   label = g.Key.ToString("MM/dd/yyyy"),
                   data = g.Select(x => x).ToList()
               });

            List<ResultData> resultItems;
            result.Groups = new List<ResultDataGroup>();
            int keyCount = 1;
            foreach (var t in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (var td in t.data)
                {
                    resultItems.Add(GetData(td));
                }

                result.Groups.Add(new ResultDataGroup()
                {
                    Key = keyCount,
                    Label = t.label,
                    Items = new List<ResultData>().AddRangeFluid(resultItems.OrderBy(r => ((dynamic)r).CreatedOn))
                });

                keyCount++;
            }
            return await Task.FromResult(result);
        }

    }

}
