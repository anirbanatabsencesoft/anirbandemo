﻿using AbsenceSoft;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Security;
using AT.Data.Reporting.Model;
using AT.Entities.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.CustomReports._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269
{
    [Serializable]
    public class WorkRestrictionsByLocationReport : AmazonReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "D1F73C2C-9313-4E45-90FB-4A39FABF0BF9"; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 98; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Accommodation Status Report".
        /// </summary>
        public override string Category { get { return "Work Restrictions Report"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get; set; } = "Work Restrictions by Location";

        /// <summary>
        /// Icon to set for Accommodation Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether to show the auto
        /// generated total counts at the bottom of the group page.
        /// </summary>
        /// <value>
        ///   <c>true</c> if group auto totals; otherwise, <c>false</c>.
        /// </value>
        public override bool GroupAutoTotals { get { return false; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get; set; }

        string lastCaseNumber = null;

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected async override Task<ReportResult> RunReportAsync(AT.Entities.Authentication.User user, ReportResult result, ReportGroupType? groupBy, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            var userData = await reportingLogic.GetUserReportCriteriaDataAsync();

            List<Case> cases = await reportingLogic.GetCaseListForWorkRestrictionByLocationAmazonReport(userData, result.Criteria, EmployerIdFilterName, pageNumber, pageSize);
            List<string> caseIds = new List<string>(cases.Count);
            List<string> empIds = new List<string>();
            List<string> empNums = new List<string>();
            List<string> employerIds = new List<string>();
            foreach (var c in cases)
            {
                caseIds.Add(c.Id);
                empIds.AddIfNotExists(c.Employee.Id);
                empNums.AddIfNotExists(c.Employee.EmployeeNumber);
                employerIds.AddIfNotExists(c.EmployerId);
            }

            var employeeRestrictions = await reportingLogic.GetEmployeeRestrictionForCaseIds(userData, result.Criteria, caseIds);
            var demands = await reportingLogic.GetDemandForRestrictionCaseIds(employeeRestrictions);
            
            var employeeContacts = await reportingLogic.GetEmployeeContact(empIds);
            var customerJobs = await reportingLogic.GetEmployeeJobForAssociatedEmployeeNumber(userData.AuthenticatedUser.CustomerKey, empNums);
            

            result.Groups = cases
               .GroupBy(c => c.Employee.Info.OfficeLocation ?? "")
               .Select(q => new ResultDataGroup()
               {
                   Key = q.Key,
                   Label = q.Key,
                   Items = q
                       .SelectMany(c => employeeRestrictions.Where(e => e.CaseId == c.Id).Select(p => p.Restriction).Select(r => GetDetail(c, r, employeeContacts, customerJobs, demands)))
                       .ToList()
               }).Skip((pageNumber -1) * pageSize).Take(pageSize).ToList();

            return await Task.FromResult(result);
        }

        /// <summary>
        /// Gets details and populate ResultData
        /// </summary>
        /// <param name="case"></param>
        /// <param name="restriction"></param>
        /// <param name="employeeContacts"></param>
        /// <param name="customerJobs"></param>
        /// <param name="demands"></param>
        /// <param name="lastCaseNumber"></param>
        /// <returns></returns>
        private ResultData GetDetail(Case @case, WorkRestriction restriction, List<EmployeeContact> employeeContacts, List<EmployeeJob> customerJobs, List<Demand> demands)
        {
            ResultData data = new ResultData();

            if (lastCaseNumber != @case.CaseNumber)
            {
                // Case Number - link
                SetCaseData(@case, employeeContacts, customerJobs, data);
                string workRelated = null;
                string granted = null;
                string grantedDate = null;
                SetAccommodationValue(@case, ref workRelated, ref granted, ref grantedDate);
                data["Granted"] = granted ?? "No";
                data["GrantedDate"] = grantedDate ?? "";
                data["ApprovedProcessPath"] = GetEmployeeJob(customerJobs, @case.EmployerId, @case.Employee.EmployeeNumber, AdjudicationStatus.Approved).GetAwaiter().GetResult() ?? "";
                data["WorkRelated"] = workRelated ?? "No";
            }
            else
            {
                SetAllBlank(data);
            }

            SetRestrictionDemand(restriction, demands, data);
            lastCaseNumber = @case.CaseNumber;
            return data;
        }

        private void SetRestrictionDemand(WorkRestriction restriction, List<Demand> demands, ResultData data)
        {
            if (restriction != null && !string.IsNullOrWhiteSpace(restriction.DemandId))
            {
                var demand = demands.FirstOrDefault(d => d.Id == restriction.DemandId);
                if (demand != null)
                {
                    data["Restriction"] = demand?.Name ?? "";
                }
                else
                {
                    data["Restriction"] = "";
                }
                data["Limitation"] = restriction.ToString();

                if (restriction.Dates == null)
                {
                    data["Duration"] = "";
                    data["From"] = restriction.Dates.StartDate.ToString("MM/dd/yyyy");
                    data["Thru"] = restriction.Dates.EndDate == null ? "" : restriction.Dates.EndDate.ToString("MM/dd/yyyy") ?? "";
                }

                if (restriction.Dates.EndDate.HasValue)
                {
                    data["Duration"] = string.Format("{0} days", restriction.Dates.Timeframe.TotalDays);
                }
                else
                {
                    data["Duration"] = "";
                }
            }
            else
            {
                data["Restriction"] = "";
                data["Limitation"] = "";
                data["From"] = "";
                data["Thru"] = "";
                data["Duration"] = "";
            }
        }

        private void SetAllBlank(ResultData data)
        {
            data["CaseNumber"] = "";
            data["Name"] = "";
            data["EEID"] = "";
            data["CostCenterCode"] = "";
            data["Department"] = "";
            data["CurrentProcessPath"] = "";
            data["Supervisor"] = "";
            data["Class"] = "";
            data["Status"] = "";
            data["StartDate"] = "";
            data["EndDate"] = "";
            data["Location"] = "";
            data["AccommodationType"] = "";
            data["AccommodationStatus"] = "";
            data["Granted"] = "";
            data["GrantedDate"] = "";
            data["ApprovedProcessPath"] = "";
            data["WorkRelated"] = "";
        }

        private void SetCaseData(Case @case, List<EmployeeContact> employeeContacts, List<EmployeeJob> customerJobs, ResultData data)
        {
            data["CaseNumber"] = new LinkItem() { Value = @case.CaseNumber, Link = string.Format("/Cases/{0}/View", @case.Id) };
            // Employee Name - link
            data["Name"] = new LinkItem() { Value = @case.Employee.FullName, Link = string.Format("/Employees/{0}/View", @case.Employee.Id) };
            data["EEID"] = new LinkItem() { Value = @case.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", @case.Employee.Id) };
            data["CostCenterCode"] = @case.Employee.CostCenterCode ?? "";
            data["Department"] = @case.Employee.Department ?? "";
            data["CurrentProcessPath"] = GetEmployeeJob(customerJobs, @case.EmployerId, @case.Employee.EmployeeNumber, null).GetAwaiter().GetResult() ?? "";
            data["Supervisor"] = employeeContacts
                .Where(c => c.ContactTypeCode == "SUPERVISOR" && c.EmployeeId == @case.Employee.Id)
                .Select(c => string.Format("{0}, {1}", c.Contact.LastName, c.Contact.FirstName))
                .DefaultIfEmpty("")
                .First();
            data["Class"] = @case.Employee.CustomFields
                .Where(cf => cf.Name == "EmployeeClass")
                .Select(cf => cf.SelectedValueText)
                .DefaultIfEmpty("")
                .First();

            data["Status"] = @case.Status.ToString().SplitCamelCaseString();
            // Case Begin date
            data["StartDate"] = @case.StartDate.ToString("MM/dd/yyyy");
            // Case End date
            data["EndDate"] = @case.EndDate.ToString("MM/dd/yyyy") ?? "";
            // Location
            data["Location"] = @case.CurrentOfficeLocation ?? "";
            // The accommodation type(s)
            data["AccommodationType"] = @case.AccommodationRequest == null ? "" : string.Join(", ", @case.AccommodationRequest.Accommodations.Select(p => p.Type == null ? "" : p.Type.Name));
            data["AccommodationStatus"] = @case.AccommodationRequest == null ? "" : @case.AccommodationRequest.Status.ToString().SplitCamelCaseString();
        }

        /// <summary>
        /// Get an employee job name.
        /// </summary>
        /// <param name="customerJobs"></param>
        /// <param name="employerId"></param>
        /// <param name="employeeNumber"></param>
        /// <returns></returns>
        private async Task<string> GetEmployeeJob(IEnumerable<EmployeeJob> customerJobs, string employerId, string employeeNumber, AdjudicationStatus? status)
        {
            var currentJobs = customerJobs.Where(e =>
                (e.Dates == null || e.Dates.IsNull || e.Dates.IncludesToday())
                && (status == null || e.Status == null || e.Status.Value == status)
                && e.EmployerId == employerId
                && e.EmployeeNumber == employeeNumber);

            if (!currentJobs.Any())
            {
                return string.Empty;
            }
            if (currentJobs.Count() == 1)
            {
                return currentJobs.First().JobName;
            }

            return await Task.FromResult(currentJobs.OrderByDescending(ej => ej.ModifiedDate).First().JobName);
        }

        private void SetAccommodationValue(Case @case, ref string workRelated, ref string granted, ref string grantedDate)
        {
            if (@case.AccommodationRequest != null && @case.AccommodationRequest.Accommodations != null)
            {
                granted = @case.AccommodationRequest.Accommodations.Any(r => r.Granted) ? "Yes" : "No";
                var grantedAccommodation = @case.AccommodationRequest.Accommodations.FirstOrDefault(r => r.Granted);
                if (grantedAccommodation != null && grantedAccommodation.GrantedDate.HasValue)
                {
                    grantedDate = grantedAccommodation.GrantedDate.ToString("MM/dd/yyyy");
                }
                if (@case.AccommodationRequest.Accommodations.Any(r => r.IsWorkRelated.HasValue))
                {
                    bool isWorkRelated = @case.AccommodationRequest.Accommodations.Any(r => r.IsWorkRelated.HasValue && r.IsWorkRelated.Value);
                    workRelated = isWorkRelated ? "Yes" : "No";
                }
            }
        }

    }
}
