﻿using AbsenceSoft;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Enums;

namespace AT.Logic.Reporting.Reports.CustomReports._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269
{
    public class HrContactCountReport : AmazonReport
    {
        /// <summary>
        /// Constructor 
        /// </summary>
        public HrContactCountReport()
        {
            Name = "HR Contacts Count";

        }
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "20BAA0BB-CB4C-4FF7-88A9-24B2AA85F1F7"; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 99; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Accommodation Status Report".
        /// </summary>
        public override string Category { get { return "Accommodation Interactive Process Report"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get; set; }

        /// <summary>
        /// Icon to set for Accommodation Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether to show the auto
        /// generated total counts at the bottom of the group page.
        /// </summary>
        /// <value>
        ///   <c>true</c> if group auto totals; otherwise, <c>false</c>.
        /// </value>
        public override bool GroupAutoTotals { get { return false; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get; set; }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerKey,
                StartDate = DateTime.UtcNow.Date.GetFirstDayOfMonth(),
                EndDate = DateTime.UtcNow.Date
            };
            return criteria;
        }

        
        /// <summary>
        /// Builds the summary.
        /// </summary>
        /// <param name="results">The results.</param>
        /// <returns></returns>
        protected ResultData BuildSummary(List<ResultData> results)
        {
            dynamic data = new ResultData();
            data.Metric = "HR Contacts";
            data.Count = !results.Any() ? "0" : results.Sum(r => r["Contacts"] == null ? 0 : Convert.ToInt32(r["Contacts"].ToString())).ToString("N0");
            return data;
        }
      

        /// <summary>
        /// Gets the detail.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="questionId">The question identifier.</param>
        /// <param name="answer">The answer.</param>
        /// <param name="allNotes">All notes.</param>
        /// <returns></returns>
        protected virtual ResultData GetDetail(Case theCase, string questionId, object answer = null, List<CaseNote> allNotes = null)
        {
            ResultData data = new ResultData
            {
                ["CaseNumber"] = FormatCaseNumber(theCase),
                ["EmployeeNumber"] = FormatEmployeeNumber(theCase.Employee),
                ["Name"] = FormatEmployeeName(theCase.Employee),
                ["WorkState"] = theCase.Employee?.WorkState ?? "",
                ["Location"] = theCase.CurrentOfficeLocation ?? ""
            };
           
            int count = 0;
            var notes = (allNotes ?? new List<CaseNote>(0)).Where(n => n.CaseId == theCase.Id).ToList();
            if (notes.Any())
            {
                count += notes.Count(n => n.Categories.Any(r => r.CategoryCode.ToUpper() == "HRCONVERSATION"));
                count += notes.Count(n => n.Categories.Any(r => r.CategoryCode.ToUpper()=="INTERACTIVEPROCESS") && n.Metadata.GetRawValue<string>("AccommQuestionId") == "000000000000000000000072");
            }
            data["Contacts"] = count.ToString("N0");
            return data;
        }

        protected async override Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            var caseList = await reportingLogic.RunAccommodationCustomBaseQueryAsync(result.Criteria, pageNumber, pageSize, sortBy, orderByAsc);
            var caseIds = caseList.Select(c => c.Id).ToList();
            var notes = await reportingLogic.GetCaseNotesbyCaseIdsAsync(caseIds);
            List<ResultData> resultItems = new List<ResultData>();

            foreach (var item in caseList)
            {
                resultItems.Add(GetDetail(item, "000000000000000000000072", true, notes.ToList()));
            }
            result.Groups = new List<ResultDataGroup>
            {
                new ResultDataGroup()
                {
                    Key = "Summary",
                    Label = "Summary",
                    Items = new List<ResultData> { BuildSummary(resultItems) }
                },
                new ResultDataGroup()
                {
                    Key = "Detail",
                    Label = "Detail",
                    Items = resultItems
                }
            };
            return await Task.FromResult(result);
        }
    }
}
