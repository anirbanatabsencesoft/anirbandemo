﻿using AbsenceSoft;
using AT.Entities.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Reporting.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Communications;
using AT.Entities.Reporting.Classes;

namespace AT.Logic.Reporting.Reports.CustomReports._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269
{
    /// <summary>
    /// User Activity Report
    /// </summary>
    [Serializable]
    public class UserActivityDetailReport : AmazonReport
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public UserActivityDetailReport()
        {
            Name = "User Activity Detail Report";
        }
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "20BAA0FF-CB4C-4FF7-88A9-24B2AA85F1F7"; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 98; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Accommodation Status Report".
        /// </summary>
        public override string Category { get { return "User Activity Report"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get; set; }

        /// <summary>
        /// Icon to set for Accommodation Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get { return null; } }

        /// <summary>
        /// Gets a value indicating whether this instance is viewable. This report is NOT viewable in the UI. Only exportable.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is viewable; otherwise, <c>false</c>.
        /// </value>
        public override bool IsViewable { get { return false; } }

        /// <summary>
        /// Gets a value indicating whether this instance is exportable to PDF. This report is NOT, only to CSV.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is exportable to PDF; otherwise, <c>false</c>.
        /// </value>
        public override bool IsExportableToPdf { get { return false; } }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerKey,
                StartDate = DateTime.UtcNow.Date.GetFirstDayOfMonth(),
                EndDate = DateTime.UtcNow.Date
            };
            return criteria;
        }

        /// <summary>
        /// Build and Run the User Activity Detail Report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="reportGroupType"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        protected async override Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            (Dictionary<string, string> users, Dictionary<string, CaseModel> caseIds, List <CaseNote> caseNotes, List<EmployeeNote> employeeNotes, List<Attachment> attachments) = reportingLogic.RunUserActivityDetailReportQueryAsync(result.Criteria).GetAwaiter().GetResult();

            // Compile the results
            result.Groups = new List<ResultDataGroup>(2);
            var notesGroup = result.Groups.AddFluid(new ResultDataGroup()
            {
                Label = "Case and Employee Notes",
                Key = "Notes",
                Items = new List<ResultData>()
            });
            var attGroup = result.Groups.AddFluid(new ResultDataGroup()
            {
                Label = "Case Attachments",
                Key = "Attachments",
                Items = new List<ResultData>()
            });

            notesGroup.Items.AddRange(caseNotes.Select(n =>
            {
                return CreateCaseNotes(n, users, caseIds);
            }));
            notesGroup.Items.AddRange(employeeNotes.Select(n =>
            {
                return CreateEmployeeNotes(n, users, caseIds);
            }));

            attGroup.Items.AddRange(attachments.Select(n =>
            {
                return CreateAttachmentDetails(n, caseIds);
            }));
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Create the Attachment Details
        /// </summary>
        /// <param name="attachment"></param>
        /// <param name="caseIds"></param>
        /// <returns></returns>
        private static ResultData CreateAttachmentDetails(Attachment attachment, Dictionary<string, CaseModel> caseIds)
        {
            ResultData data = new ResultData();
            var caseItem = caseIds[attachment.CaseId];
            data["CaseNumber"] = new LinkItem(caseItem == null ? "Case" : caseItem.CaseNumber, $"/Cases/{attachment.CaseId}/View");
            if (caseItem != null)
            {
                data["EmployeeNumber"] = new LinkItem() { Value = caseItem.EmployeeNumber, Link = $"/Employees/{attachment.EmployeeId}/View"};
            }
            data["Name"] = new LinkItem(caseItem == null ? "Employee" : caseItem.Name, $"/Employees/{attachment.EmployeeId}/View");
            data["OfficeLocation"] = caseItem == null ? "" : caseItem.OfficeLocation ?? "";
            data["CreatedOn"] = attachment.CreatedDate.ToString("MM/dd/yyyy");
            data["CreatedBy"] = attachment.CreatedByName ?? "System";
            data["Type"] = attachment.AttachmentType.ToString().SplitCamelCaseString();
            data["Detail"] = attachment.FileName == null || attachment.FileName.Length < 50 ? attachment.FileName : attachment.FileName.Substring(0, 47) + "...";
            return data;
        }

        /// <summary>
        /// Create employee note
        /// </summary>
        /// <param name="employeeNote"></param>
        /// <param name="users"></param>
        /// <param name="caseIds"></param>
        /// <returns></returns>
        private static ResultData CreateEmployeeNotes(EmployeeNote employeeNote, Dictionary<string, string> users, Dictionary<string, CaseModel> caseIds)
        {
            ResultData data = new ResultData();
            var caseItem = caseIds.Values.FirstOrDefault(k => k.EmployeeId == employeeNote.EmployeeId);
            data["CaseNumber"] = "N/A";
            if (caseItem != null)
            {
                data["EmployeeNumber"] = new LinkItem() { Value = caseItem.EmployeeNumber, Link = $"/Employees/{employeeNote.EmployeeId}/View",};
            }
            data["Name"] = new LinkItem(caseItem == null ? "Employee" : caseItem.Name, $"/Employees/{caseItem?.EmployeeId}/View");
            data["OfficeLocation"] = caseItem?.OfficeLocation ?? "";
            data["CreatedOn"] = employeeNote.CreatedDate.ToString("MM/dd/yyyy");
            data["CreatedBy"] = users.ContainsKey(employeeNote.CreatedById) ? users[employeeNote.CreatedById] ?? "System" : "System";
            data["Type"] = employeeNote.Category.HasValue ? employeeNote.Category.Value.ToString().SplitCamelCaseString() : "";
            data["Detail"] = employeeNote.Notes == null || employeeNote.Notes.Length < 50 ? employeeNote.Notes : employeeNote.Notes.Substring(0, 47) + "...";
            return data;
        }

        /// <summary>
        /// Create Case notes
        /// </summary>
        /// <param name="caseNote"></param>
        /// <param name="users"></param>
        /// <param name="caseIds"></param>
        /// <returns></returns>
        private static ResultData CreateCaseNotes(CaseNote caseNote, Dictionary<string, string> users, Dictionary<string, CaseModel> caseIds)
        {
            ResultData data = new ResultData();
            var caseItem = caseIds[caseNote.CaseId];
            data["CaseNumber"] = new LinkItem(caseItem == null ? "Case" : caseItem.CaseNumber, $"/Cases/{caseNote.CaseId}/View");
            if (caseItem != null)
            {
                data["EmployeeNumber"] = new LinkItem() { Value = caseItem.EmployeeNumber, Link = $"/Employees/{caseItem.EmployeeId}/View"};
            }
            data["Name"] = new LinkItem(caseItem == null ? "Employee" : caseItem.Name, $"/Employees/{caseItem?.EmployeeId}/View");
            data["OfficeLocation"] = caseItem?.OfficeLocation ?? "";
            data["CreatedOn"] = caseNote.CreatedDate.ToString("MM/dd/yyyy");
            data["CreatedBy"] = users.ContainsKey(caseNote.CreatedById) ? users[caseNote.CreatedById] ?? "System" : "System";
            data["Type"] = caseNote.Category.HasValue ? caseNote.Category.Value.ToString().SplitCamelCaseString() : "";
            data["Detail"] = caseNote.Notes == null || caseNote.Notes.Length < 50 ? caseNote.Notes : caseNote.Notes.Substring(0, 47) + "...";
            return data;
        }
    }
}
