﻿using AT.Entities.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Reporting.Enums;
using AbsenceSoft;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Cases;

namespace AT.Logic.Reporting.Reports.CustomReports._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269
{
    public class InteractiveProcessReport : AmazonReport
    {
        public InteractiveProcessReport()
        {
            Name = "Question Count";
        }
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "20BAA0BB-CB4C-4FF7-88A9-24B2AA85F1E7"; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 99; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Accommodation Status Report".
        /// </summary>
        public override string Category { get { return "Accommodation Interactive Process Report"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get; set; }

        /// <summary>
        /// Icon to set for Accommodation Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether to show the auto
        /// generated total counts at the bottom of the group page.
        /// </summary>
        /// <value>
        ///   <c>true</c> if group auto totals; otherwise, <c>false</c>.
        /// </value>
        public override bool GroupAutoTotals { get { return false; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get; set; }



        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerKey,
                StartDate = DateTime.UtcNow.Date.GetFirstDayOfMonth(),
                EndDate = DateTime.UtcNow.Date
            };

            var questions = reportingLogic.GetAccommodationQuestionsByCustomerId(user.CustomerKey).GetAwaiter().GetResult();

            ReportCriteriaItem question = new ReportCriteriaItem()
            {
                ControlType = ControlType.SelectList,
                KeepOptionAll = false,
                Name = "Question",
                OnChangeMethodName = "FilterChangeEvent",
                Prompt = "Question",
                Required = true
            };
            question.Options = questions.Select(criteriaItemOption => new ReportCriteriaItemOption()
            {
                GroupText = criteriaItemOption.AccommodationTypeCode ?? "All",
                Value = criteriaItemOption.Id,
                Text = (criteriaItemOption.AccommodationType == null || string.IsNullOrWhiteSpace(criteriaItemOption.AccommodationType.Name)) ? criteriaItemOption.Question : criteriaItemOption.AccommodationType.Name.Trim() + " - " + criteriaItemOption.Question
            }).ToList();
            criteria.Filters.Add(question);

            ReportCriteriaItem answers = new ReportCriteriaItem()
            {
                ControlType = ControlType.SelectList,
                KeepOptionAll = true,
                Name = "Answer",
                OnChangeMethodName = "FilterChangeEvent",
                Prompt = "Answer",
                Tweener = "is counted if"
            };
            answers.Options = questions.SelectMany(q => q.Choices ?? new List<string>(0)).Select(c => new ReportCriteriaItemOption() { Value = c, Text = c }).ToList();
            answers.Options.InsertRange(0, new List<ReportCriteriaItemOption>
            {
                new ReportCriteriaItemOption() { Text = "Yes", Value = true },
                new ReportCriteriaItemOption() { Text = "No", Value = false }
            });
            criteria.Filters.Add(answers);

            return criteria;
        }



        /// <summary>
        /// Builds the summary.
        /// </summary>
        /// <param name="results">The results.</param>
        /// <returns></returns>
        protected ResultData BuildSummary(List<ResultData> results, object answer)
        {
            var textAnswer = answer == null ? "" : answer is bool ? (bool)answer ? "Yes" : "No" : answer.ToString().Replace(" ", "").Trim();
            var col = "Answered" + textAnswer;

            dynamic data = new ResultData();
            data.Metric = col;
            data.Count = !results.Any() ? "0" : results.Sum(r => r[col] == null ? 0 : Convert.ToInt32(r[col].ToString())).ToString("N0");
            return data;
        }


        /// <summary>
        /// Gets the detail.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="questionId">The question identifier.</param>
        /// <param name="answer">The answer.</param>
        /// <returns></returns>
        protected virtual ResultData GetDetail(Case theCase, string questionId, object answer = null)
        {
            ResultData data = new ResultData
            {
                ["CaseNumber"] = FormatCaseNumber(theCase),
                ["EmployeeNumber"] = FormatEmployeeNumber(theCase?.Employee),
                ["Name"] = FormatEmployeeName(theCase?.Employee)
            };
            int count = 0;
            if (theCase?.AccommodationRequest != null)
            {
                count = GetCount(theCase, questionId, answer, count);
            }
            var textAnswer = answer == null ? "" : answer is bool ? (bool)answer ? "Yes" : "No" : answer.ToString().Replace(" ", "").Trim();
            data["Answered" + textAnswer] = count.ToString("N0");
            data["WorkState"] = theCase?.Employee.WorkState ?? "";
            data["Location"] = theCase?.CurrentOfficeLocation ?? "";

            return data;
        }

        /// <summary>
        /// Get the Steps Count 
        /// </summary>
        /// <param name="theCase"></param>
        /// <param name="questionId"></param>
        /// <param name="answer"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        private int GetCount(Case theCase, string questionId, object answer, int count)
        {
            var stepsInfo = theCase?.AccommodationRequest?.AdditionalInfo?.Steps?.FirstOrDefault(s => s.QuestionId == questionId);
            count = Counter(answer, count, stepsInfo);
            if (theCase?.AccommodationRequest?.Accommodations != null)
            {
                foreach (var accomm in theCase.AccommodationRequest.Accommodations)
                {
                    stepsInfo = accomm.InteractiveProcess?.Steps?.FirstOrDefault(s => s.QuestionId == questionId);
                    count = Counter(answer, count, stepsInfo);
                }
            }
            return count;
        }

        /// <summary>
        /// Counter for steps
        /// </summary>
        /// <param name="answer"></param>
        /// <param name="count"></param>
        /// <param name="stepsInfo"></param>
        /// <returns></returns>
        private int Counter(object answer, int count, AccommodationInteractiveProcess.Step stepsInfo)
        {
            if (stepsInfo != null && ((answer == null && stepsInfo.Answer == true) || (answer != null && answer is bool && (bool)answer == stepsInfo.Answer) || (answer != null && stepsInfo.SAnswer == answer.ToString())))
            {
                count++;
            }

            return count;
        }


        /// <summary>
        /// Run the query for Interactive process report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="reportGroupType"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        protected override async Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            var caseList =  reportingLogic.RunAccommodationCustomBaseQueryAsync(result.Criteria, pageNumber, pageSize, sortBy, orderByAsc).GetAwaiter().GetResult();
            List<ResultData> resultItems = new List<ResultData>();
            string questionId = null;
            object answer = null;
            var qFilter = result.Criteria.Filters.FirstOrDefault(m => m.Name == "Question");
            if (qFilter != null && qFilter.Value != null)
            {
                questionId = qFilter.Value.ToString();
            }
            var aFilter = result.Criteria.Filters.FirstOrDefault(m => m.Name == "Answer");
            if (aFilter != null && aFilter.Value != null)
            {
                answer = aFilter.Value;
            }
            foreach (var item in caseList)
            {
                resultItems.Add(GetDetail(item, questionId, answer));
            }
            result.Groups = new List<ResultDataGroup>(2)
            {
                new ResultDataGroup()
                {
                    Key = "Summary",
                    Label = "Summary",
                    Items = new List<ResultData>{
                        BuildSummary(resultItems, answer)
                    }
                },
                new ResultDataGroup()
                {
                    Key = "Detail",
                    Label = "Detail",
                    Items = resultItems
                }
            };
            return await Task.FromResult(result);
        }
    }
}
