﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using AbsenceSoft.Data.Cases;
using AbsenceSoft;

namespace AT.Logic.Reporting.Reports.CustomReports._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269
{
    /// <summary>
    /// Employee Survey Report class
    /// </summary>
    [Serializable]
    public class EmployeeSurveyReport :AmazonReport
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public EmployeeSurveyReport()
        {
            Name = "Survey Detail by Email";
        }
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "B2F0176B-0566-4769-80B1-91BE1A9FB2D0"; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 105; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return false; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Accommodation Status Report".
        /// </summary>
        public override string Category { get { return "Survey Detail Report"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get; set; }

        /// <summary>
        /// Icon to set for Accommodation Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get; set; }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria();
            criteria.CustomerId = user.CustomerKey;
            criteria.StartDate = DateTime.UtcNow.Date.GetFirstDayOfMonth();
            criteria.EndDate = DateTime.UtcNow.Date;
            return criteria;
        }

       
        /// <summary>
        /// Gets the detail.
        /// </summary>
        /// <param name="theItem">The case.</param>
        /// <returns></returns>
        protected virtual ResultData GetDetail(Case theItem)
        {
            ResultData data = new ResultData();
            data["CaseStartDate"] = theItem.StartDate.ToString("MM/dd/yyyy");
            data["CaseCreated"] = theItem.CreatedDate.ToString("MM/dd/yyyy");
            data["WorkLocation"] = theItem.CurrentOfficeLocation ?? "";
            data["EmployeeEmailAddress"] = theItem.Employee.Info.Email ?? "";
            data["EmployeeAlternateEmailAddress"] = theItem.Employee.Info.AltEmail ?? "";
            data["EmployeeName"] = theItem.Employee.FullName;
            data["EEID"] = theItem.Employee.EmployeeNumber;
            data["JobTitle"] = theItem.Employee.JobTitle ?? "";
            return data;
        }

        /// <summary>
        /// Build and Run the report for Employee Survey Report.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="reportGroupType"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        protected async override Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
           
            var caseList =  reportingLogic.RunEmployeeSurveyReportQueryAsync(result.Criteria, pageNumber, pageSize, sortBy, orderByAsc).GetAwaiter().GetResult();

            List<ResultData> resultItems = new List<ResultData>();

            foreach (var item in caseList)
            {
                resultItems.Add(GetDetail(item));
            }
            result.Items = resultItems;
            return await Task.FromResult(result);
        }
    }
}
