﻿using AT.Entities.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.CustomReports._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269.AccommodationStatus
{
   public partial class AccommodationDetailReport : AmazonReport
    {
        public AccommodationDetailReport()
        {
            Name = "Detail";          
        }
        /// <summary>
        /// Category for all Accommodation Status reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Accommodation Status Report".
        /// </summary>
        public override string Category { get { return "Accommodation Status Report"; } }

        /// <summary>
        /// Icon to set for Accommodation Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get { return null; } }

        /// <summary>
        /// List of fields for grouping
        /// </summary>
        public override Dictionary<string, List<string>> AvailableGroupingFields => new Dictionary<string, List<string>>(){
             {(ReportGroupType.Custom).ToString(), new List<string>(){
                       "CaseNumber",
                       "Status",
                       "EmployerName",
                       "RelatedCaseType",
                       "CaseType",
                       "EmployerId",
                       "CurrentOfficeLocation",
                       "CustomerId",
                       "AssignedToId",
                       "AssignedToName",
                       "cby",
                       "Employee.EmployeeNumber",
                        "Employee.FullName",
                        "Employee.Status",
                        "Reason.Name",
                        "Reason.CaseTypes",
                        "Reason.Category",
                        "Reason.Code",
                        "AccommodationRequest.Status"
            } }
        };

        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "0A3B77C2-374F-49ED-859F-54FDFE1066E4"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get; set; }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return false; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 0; } }
    }
}
