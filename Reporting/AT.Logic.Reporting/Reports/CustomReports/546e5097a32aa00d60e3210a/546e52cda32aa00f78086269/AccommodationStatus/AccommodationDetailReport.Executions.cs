﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AT.Data.Reporting.Model;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using MongoDB.Driver;

namespace AT.Logic.Reporting.Reports.CustomReports._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269.AccommodationStatus
{
    public partial class AccommodationDetailReport : AmazonReport
    {
        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            UserReportCriteriaData userReportCriteriaData = reportingLogic.GetUserReportCriteriaDataAsync().GetAwaiter().GetResult();

            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerKey,
                StartDate = DateTime.UtcNow.Date.AddDays(-30),
                EndDate = DateTime.UtcNow.Date
            };

            ReportCriteriaItem item = GetCaseStatusCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetAccomTypeCriteria(userReportCriteriaData);
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetAdjudicationCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            criteria.Filters.Add(GetCaseAssigneeCriteria(userReportCriteriaData));
            criteria.Filters.Add(GetWorkStateCriteria(userReportCriteriaData, criteria.StartDate, criteria.EndDate));
           
            criteria.Filters.Add(GetLocationCriteria(userReportCriteriaData));
            return criteria;
        }

        /// <summary>
        /// Run the report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="reportGroupType"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <param name="groupByColumns"></param>
        /// <returns></returns>
        protected override async Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            var queryList = reportingLogic.RunCustomAccomDetailReportQueryAsync(result.Criteria, EmployerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc).GetAwaiter().GetResult();
          

            AdjudicationStatus? accomStatus = null;
            string accomType = string.Empty;

            if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "Determination") != null && result.Criteria.Filters.FirstOrDefault(m => m.Name == "Determination").Value != null)
            {
                accomStatus = result.Criteria.ValueOf<AdjudicationStatus>("Determination");
            }

            if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "AccomType") != null && result.Criteria.Filters.FirstOrDefault(m => m.Name == "AccomType").Value != null)
            {
                accomType = result.Criteria.Filters.FirstOrDefault(m => m.Name == "AccomType").Value.ToString();
            }

            List<ResultData> resultItems = new List<ResultData>();

            foreach (var q in queryList.Where(c => c.AccommodationRequest != null))
            {
                CreateList(result, accomStatus, accomType, resultItems, q);
            }

            result.Items = resultItems;
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Create the list
        /// </summary>
        /// <param name="result"></param>
        /// <param name="accomStatus"></param>
        /// <param name="accomType"></param>
        /// <param name="resultItems"></param>
        /// <param name="q"></param>
        private void CreateList(ReportResult result, AdjudicationStatus? accomStatus, string accomType, List<ResultData> resultItems, Case q)
        {
            q.AccommodationRequest.Accommodations = q.AccommodationRequest.Accommodations ?? new List<Accommodation>(0);         

            foreach (var accom in q.AccommodationRequest.Accommodations.Where(a => a.Type != null).OrderBy(p => p.Type.Code))
            {
                if (accomStatus.HasValue && accomStatus.Value != accom.Determination)
                {
                    continue;
                }
                if (!string.IsNullOrWhiteSpace(accomType) && accom.Type.Code != accomType)
                {
                    continue;
                }
                foreach (var detail in accom.Usage.Where(u => u.StartDate.DateRangesOverLap(u.EndDate, result.Criteria.StartDate, result.Criteria.EndDate)).OrderBy(u => u.StartDate))
                {
                    resultItems.Add(GetDetail(q, accom, detail));
                }
            }

            if (!q.AccommodationRequest.Accommodations.Any(a => a.Type != null))
            {
                resultItems.Add(GetBlankDetail(q));
            }
        }

        /// <summary>
        /// Gets the detail.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="q">The q.</param>
        /// <param name="detail">The detail.</param>
        /// <param name="displayCaseNumber">The display case number.</param>
        /// <param name="displayFullName">Display name of the full.</param>
        /// <returns></returns>
        protected override ResultData GetDetail(Case theCase, Accommodation q, AccommodationUsage detail)
        {
            dynamic data = new ResultData();
            GetCommonDetails(theCase,data);
            CustomField employeeClass = theCase.Employee.CustomFields.FirstOrDefault(cf => cf.Name == "EmployeeClass");
            data.Class = employeeClass == null ? "" : employeeClass.SelectedValueText;
            data.CaseStatus = theCase.Status.ToString().SplitCamelCaseString();
            data.Duration = q.Duration == 0 ? "Pending" : q.Duration.ToString().SplitCamelCaseString();
            data.Type = q.Type.Name;
            data.Created = q.CreatedDate.ToString("MM/dd/yyyy");
            data.StartDate = detail.StartDate == DateTime.MinValue ? "Pending" : detail.StartDate.ToString("MM/dd/yyyy");
            data.EndDate = detail.EndDate.HasValue ? detail.EndDate.ToString("MM/dd/yyyy") : string.Empty;
            data.Accommodations = string.Concat(q.Type.Code, " - ", detail.Determination.ToString().SplitCamelCaseString());
            data.AccommStatus = q.Status.ToString().SplitCamelCaseString();
            data.Resolved = q.Resolved ? "Yes" : "No";
            data.ResolvedDate = q.ResolvedDate.HasValue ? q.ResolvedDate.ToString("MM/dd/yyyy") : string.Empty;
            data.Granted = q.Granted ? "Yes" : "No";
            data.GrantedDate = q.GrantedDate.HasValue ? q.GrantedDate.ToString("MM/dd/yyyy") : string.Empty;
            data.AssignedTo = theCase.AssignedToName;
            data.WorkState = theCase.Employee.WorkState;
            data.Location = string.IsNullOrWhiteSpace(theCase.CurrentOfficeLocation) ? string.Empty : theCase.CurrentOfficeLocation;
            data.WorkRelated = q.IsWorkRelated == null ? "" : q.IsWorkRelated.Value ? "1" : "0";
            data.CostCenter = theCase.Employee.CostCenterCode ?? "";
            data.ClosedDate = theCase.GetClosedDate().HasValue ? string.Format("{0:MM/dd/yyyy}", theCase.GetClosedDate()) : string.Empty;
            
            return data;
        }

        /// <summary>
        /// Set the common details
        /// </summary>
        /// <param name="theCase"></param>
        /// <param name="data"></param>
        private static void GetCommonDetails(Case theCase, dynamic data)
        {
            data.CaseNumber = string.IsNullOrWhiteSpace(theCase.CaseNumber) ? null : new LinkItem() { Value = theCase.CaseNumber, Link = string.Format("/Cases/{0}/View", theCase.Id) };
            data.EmployeeNumber = string.IsNullOrWhiteSpace(theCase.Employee.EmployeeNumber) ? null : new LinkItem() { Value = theCase.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", theCase.Employee.Id) };
            data.Name = string.IsNullOrWhiteSpace(theCase.Employee.EmployeeNumber) ? null : new LinkItem() { Value = theCase.Employee.FullName, Link = string.Format("/Employees/{0}/View", theCase.Employee.Id) };
        }

        /// <summary>
        /// Gets the blank detail.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="displayCaseNumber">The display case number.</param>
        /// <param name="displayFullName">Display name of the full.</param>
        /// <returns></returns>
        protected override ResultData GetBlankDetail(Case theCase)
        {
            dynamic data = new ResultData();
            GetCommonDetails(theCase, data);
            data.Duration = theCase.AccommodationRequest.SummaryDurationType;
            data.Type = "N/A";
            data.CaseStatus = theCase.Status.ToString().SplitCamelCaseString();
            var evt = theCase.FindCaseEvent(CaseEventType.CaseCreated);
            data.Created = (evt == null ? theCase.CreatedDate : evt.EventDate).ToString("MM/dd/yyyy");
            data.StartDate = theCase.StartDate.ToString("MM/dd/yyyy");
            data.EndDate = theCase.EndDate.ToString("MM/dd/yyyy");
            data.Accommodations = "N/A";
            data.AccommStatus = "N/A";
            data.Resolved = "N/A";
            data.ResolvedDate = "N/A";
            data.Granted = "N/A";
            data.GrantedDate = "N/A";
            data.AssignedTo = theCase.AssignedToName;
            data.WorkState = theCase.Employee.WorkState;
            data.Location = theCase.CurrentOfficeLocation;
            if (theCase.AccommodationRequest != null && theCase.AccommodationRequest.Accommodations != null)
            {
                data.WorkRelated = theCase.AccommodationRequest.Accommodations.Any(r => r.IsWorkRelated == true) ? "1" : theCase.AccommodationRequest.Accommodations.Any(r => r.IsWorkRelated == false) ? "0" : "";
            }
            else
            {
                data.WorkRelated = "";
            }
            data.CostCenter = theCase.Employee.CostCenterCode ?? "";
            return data;
        }
    }
}
