﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AT.Data.Reporting.Model;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;

namespace AT.Logic.Reporting.Reports.CustomReports._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269.AccommodationStatus
{
   public class AccommodationCustomFieldDetailReport : AmazonReport
    {
        public AccommodationCustomFieldDetailReport()
        {
            Name = "Custom Field Detail";
        }
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "20BAA0BB-CB4C-4FF7-88A9-24F2AA85F2E9"; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 99; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return false; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Accommodation Status Report".
        /// </summary>
        public override string Category { get { return "Accommodation Status Report"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get; set; }

        /// <summary>
        /// Icon to set for Accommodation Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get { return null; } }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria();
            criteria.CustomerId = user.CustomerKey;
            criteria.StartDate = DateTime.UtcNow.Date.GetFirstDayOfMonth();
            criteria.EndDate = DateTime.UtcNow.Date;
            return criteria;
        }

        private List<CustomField> _fields = new List<CustomField>();

        /// <summary>
        /// Gets the detail.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <returns></returns>
        protected override ResultData GetDetail(Case theCase, Accommodation q, AccommodationUsage detail)
        {
            dynamic data = new ResultData();
            CreateCommonData(theCase,q, data, detail);
            if (_fields != null && theCase.CustomFields != null)
            {
                foreach (var field in _fields.Where(f => f.EmployerId == theCase.EmployerId))
                {
                    var f = theCase.CustomFields.FirstOrDefault(r => r.Name == field.Name);
                    if (f == null)
                    {
                        continue;
                    }
                    if (f.SelectedValue == null)
                    {
                        data[f.Name] = "";
                    }
                    else if (f.SelectedValue is DateTime)
                    {
                        data[f.Name] = ((DateTime)f.SelectedValue).ToString("MM/dd/yyyy");
                    }
                    else
                    {
                        data[f.Name] = f.SelectedValue.ToString();
                    }
                }
            }
            return data;
        }

        private static void CreateCommonData(Case theCase, Accommodation q, dynamic data, AccommodationUsage detail)
        {
            data.CaseNumber = string.IsNullOrWhiteSpace(theCase.CaseNumber) ? null : new LinkItem() { Value = theCase.CaseNumber, Link = string.Format("/Cases/{0}/View", theCase.Id) };
            data.CaseCreatedDate = theCase.CreatedDate;
            data.Name = string.IsNullOrWhiteSpace(theCase.Employee.FullName) ? null : new LinkItem() { Value = theCase.Employee.FullName, Link = string.Format("/Employees/{0}/View", theCase.Employee.Id) };
            data.EmployeeNumber = string.IsNullOrWhiteSpace(theCase.Employee.FullName) ? null : new LinkItem() { Value = theCase.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", theCase.Employee.Id) };
            CustomField employeeClass = theCase.Employee.CustomFields.FirstOrDefault(cf => cf.Name == "EmployeeClass");
            data.Class = employeeClass == null ? "" : employeeClass.SelectedValueText;
            data.StartDate = detail.StartDate == DateTime.MinValue ? "Pending" : detail.StartDate.ToString("MM/dd/yyyy");
            data.EndDate = detail.EndDate.ToString("MM/dd/yyyy");
            data.Accommodation = q.Type.Code;
            data.AccommStatus = q.Status.ToString().SplitCamelCaseString();
            data.Resolved = q.Resolved ? "Yes" : "No";
            data.ResolvedDate = q.ResolvedDate;
            data.ResolvedReason = q.Resolution;
            data.WorkState = theCase.Employee.WorkState;
            data.Location = theCase.CurrentOfficeLocation;
            data.WorkRelated = q.IsWorkRelated == null ? "" : q.IsWorkRelated.Value ? "1" : "0";
        }

        protected override Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            ReportingLogic logic = new ReportingLogic(user);
            var caseList = logic.RunCustomFieldReportQueryAsync(result.Criteria, EmployerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc).GetAwaiter().GetResult();
            UserReportCriteriaData data = logic.GetUserReportCriteriaDataAsync().GetAwaiter().GetResult();
            _fields = logic.GetUserCustomFieldsAsync(data, null , EntityTarget.Case).GetAwaiter().GetResult().OrderBy(f => f.FileOrder).ToList();

            List<ResultData> resultItems = new List<ResultData>();

            foreach (var q in caseList.Where(c => c.AccommodationRequest != null))
            {
                q.AccommodationRequest.Accommodations = q.AccommodationRequest.Accommodations ?? new List<Accommodation>(0);
              
                foreach (var accom in q.AccommodationRequest.Accommodations.Where(a => a.Type != null && result.Criteria.StartDate.DateRangesOverLap(result.Criteria.EndDate, a.StartDate ?? result.Criteria.StartDate, a.EndDate)).OrderBy(p => p.Type.Code))
                {
                    foreach (var detail in accom.Usage.OrderBy(u => u.StartDate))
                    {
                        resultItems.Add(GetDetail(q, accom, detail));
                    }
                }
            }

            result.Items = resultItems;
            return Task.FromResult(result);
        }
    }
}
