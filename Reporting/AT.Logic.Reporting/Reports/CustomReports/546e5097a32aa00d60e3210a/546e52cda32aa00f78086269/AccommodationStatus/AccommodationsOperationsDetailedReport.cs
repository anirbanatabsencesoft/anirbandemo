﻿using AbsenceSoft;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Classes;
using AT.Entities.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.CustomReports._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269.AccommodationStatus
{
    /// <summary>
    /// Accommodation Operation report class
    /// </summary>
    public class AccommodationsOperationsDetailedReport : AmazonReport
    {

        private Dictionary<string, Employee> employees = null;
        private Dictionary<string, List<EmployeeContact>> employeeContacts = null;
        /// <summary>
        /// constructor
        /// </summary>
        public AccommodationsOperationsDetailedReport()
        {
            Name = "Accommodations Operations Detailed Report";
            Category = "Accommodation Status Report";
            MainCategory = "Operations Report";
        }

        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id
        {
            get { return "28b4c98f-c321-4a1f-b928-e19886a06a3c"; }
        }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name
        {
            get; set;
        }

        /// <summary>
        /// Data heavy report, exportable only.
        /// </summary>
        public override bool IsViewable
        {
            get { return false; }
        }

        /// <summary>
        /// List of fields for grouping
        /// </summary>
        public override Dictionary<string, List<string>> AvailableGroupingFields => new Dictionary<string, List<string>>(){
               {(ReportGroupType.Custom).ToString(), new List<string>(){
                       "CaseNumber",
                       "Status",
                       "EmployerName",
                       "RelatedCaseType",
                       "CaseType",
                       "EmployerId",
                       "CurrentOfficeLocation",
                       "CustomerId",
                       "AssignedToId",
                       "AssignedToName",
                       "cby",
                       "Employee.EmployeeNumber",
                        "Employee.FullName",
                        "Employee.Status",
                        "Reason.Name",
                        "Reason.CaseTypes",
                        "Reason.Category",
                        "Reason.Code",
                        "AccommodationRequest.Status"
            } }
        };

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get; set; }
        
        /// <summary>
        /// Build and run the accommodation report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="groupType"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <param name="groupByColumns"></param>
        /// <returns></returns>
        protected override Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? groupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {

            ReportingLogic reportingLogic = new ReportingLogic(user);
            var caseList = reportingLogic.RunCustomAccomodationReportQueryAsync(result.Criteria, EmployerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc).GetAwaiter().GetResult();
            var employeeIds = caseList.Select(c => c.Employee.Id).ToList();

            employees = reportingLogic.GetEmployeeDetailsAsync(employeeIds).GetAwaiter().GetResult();
            employeeContacts = reportingLogic.GetEmployeeContactDetailsAsync(employeeIds, "SUPERVISOR").GetAwaiter().GetResult();

            List<ResultData> resultItems;
            if (groupType != null)
            {
                if (groupType == ReportGroupType.Custom)
                {
                    result.Name = "Custom grouping";
                    IsGrouped = true;
                    var groupQuery = caseList.GroupBy(groupByColumns.ToArray());
                    result.JsonData = CreateJsonResult(groupQuery);
                }
                else
                {
                    throw new NotImplementedException("Group by is not implemented.");
                }
            }
            else
            {
                resultItems = CreateListItems(caseList);
                result.Items = resultItems;
            }

            return Task.FromResult(result);
        }

        /// <summary>
        /// Create the list of Items
        /// </summary>
        /// <param name="caseList"></param>
        /// <returns></returns>
        private List<ResultData> CreateListItems(List<Case> caseList)
        {
            List<ResultData> resultItems = new List<ResultData>();
            foreach (var theCase in caseList)
            {
                theCase.AccommodationRequest.Accommodations = theCase.AccommodationRequest.Accommodations ?? new List<Accommodation>(0);
                foreach (var accom in theCase.AccommodationRequest.Accommodations.Where(a => a.Type != null).OrderBy(p => p.Type.Code))
                {
                    foreach (var detail in accom.Usage.OrderBy(u => u.StartDate))
                    {
                        resultItems.Add(GetDetail(theCase, accom, detail));
                    }
                }
            }
            return resultItems;
        }

        /// <summary>
        /// Create custom Grouping
        /// </summary>
        /// <param name="groupQuery"></param>
        /// <returns></returns>
        private dynamic CreateJsonResult(List<GroupResult> groupQuery)
        {
            List<ResultData> resultItems;
            var dataGroup = new List<dynamic>();
            if (groupQuery == null || !groupQuery.Any())
            {
                return dataGroup;
            }
            foreach (var queryItem in groupQuery)
            {

                resultItems = CreateListItems((List<Case>)queryItem.Items);

                dataGroup.Add(new
                {
                    Label = queryItem.Value,
                    Key = queryItem.Key,
                    Items = new List<ResultData>().AddRangeFluid(resultItems),
                    SubGroups = CreateJsonResult(queryItem.SubGroups)
                });
            }
            return dataGroup;

        }

        /// <summary>
        /// Gets the detail.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="accommadation">The q.</param>
        /// <param name="detail">The detail.</param>
        /// <param name="displayCaseNumber">The display case number.</param>
        /// <param name="displayFullName">Display name of the full.</param>
        /// <returns></returns>
        /// 
        protected override ResultData GetDetail(Case theCase, Accommodation accommadation, AccommodationUsage detail)
        {
            string employeeId = theCase.Employee.Id;
            Employee employee = employees[employeeId];
            EmployeeContact supervisor = null;
            if (employeeContacts.ContainsKey(employeeId) && employeeContacts[employeeId] != null)
            {
                supervisor = employeeContacts[employeeId].OrderByDescending(ec => ec.CreatedDate).FirstOrDefault();
            }
            dynamic toReturn = new ResultData();
            toReturn.CaseNumber = FormatCaseNumber(theCase);
            toReturn.EmployeeNumber = FormatEmployeeNumber(employee);
            toReturn.FirstName = theCase.Employee.FirstName;
            toReturn.LastName = theCase.Employee.LastName;
            toReturn.EmployeeClass = GetCustomFieldValue(theCase.Employee, "EMPLOYEECLASS");
            toReturn.CaseStatus = theCase.Status.ToString().SplitCamelCaseString();
            toReturn.CaseCreatedDate = theCase.CreatedDate.ToUIString();
            toReturn.CaseStartDate = theCase.StartDate.ToUIString();
            toReturn.CaseEndDate = theCase.EndDate.ToUIString();
            toReturn.Duration = accommadation.Duration == 0 ? "Pending" : accommadation.Duration.ToString().SplitCamelCaseString();
            toReturn.AccommodationType = accommadation.Type.Name;
            toReturn.AccommodationCreatedDate = accommadation.CreatedDate.ToUIString();
            toReturn.AccommodationStartDate = detail.StartDate == DateTime.MinValue ? "Pending" : detail.StartDate.ToUIString();
            toReturn.AccommodationEndDate = detail.EndDate.ToUIString();
            toReturn.AccommodationTypeCode = accommadation.Type.Code;
            toReturn.AccommodationDetermination = detail.Determination.ToString().SplitCamelCaseString();
            toReturn.AccommodationStatus = accommadation.Status.ToString().SplitCamelCaseString();
            toReturn.Resolved = accommadation.Resolved ? "Yes" : "No";
            toReturn.ResolvedDate = accommadation.ResolvedDate.ToUIString();
            toReturn.Granted = accommadation.Granted ? "Yes" : "No";
            toReturn.GrantedDate = accommadation.GrantedDate.ToUIString();
            toReturn.AssignedTo = theCase.AssignedToName;
            toReturn.WorkState = theCase.Employee.WorkState;
            toReturn.OfficeLocation = theCase.CurrentOfficeLocation ?? "";
            toReturn.WorkRelated = accommadation.IsWorkRelated == null ? "" : accommadation.IsWorkRelated.Value ? "1" : "0";
            toReturn.CostCenter = theCase.Employee.CostCenterCode ?? "";
            toReturn.ClosedDate = theCase.GetClosedDate().ToUIString();
            toReturn.DenialReasonCode = detail.DenialReasonCode;
            toReturn.DenialReasonName = detail.DenialReasonName;
            toReturn.Supervisor = supervisor != null && supervisor.Contact != null ? string.Format("{0}, {1}", supervisor.Contact.LastName, supervisor.Contact.FirstName) : "";
            toReturn.FCLMArea = GetCustomFieldValue(theCase.Employee, "FCLMAREA");
            toReturn.ShiftPatternDescription = GetCustomFieldValue(theCase.Employee, "SHIFTPATTERNDESCRIPTION");
            toReturn.Department = GetCustomFieldValue(theCase.Employee, "DEPARTMENT");
            toReturn.BusinessTitle = GetCustomFieldValue(theCase.Employee, "BUSINESSTITLE");
            toReturn.CurrentFCLMArea = GetCustomFieldValue(employee, "FCLMAREA");
            toReturn.CurrentShiftPatternDesc = GetCustomFieldValue(employee, "SHIFTPATTERNDESCRIPTION");
            toReturn.CurrentDepartment = GetCustomFieldValue(employee, "DEPARTMENT");
            toReturn.CurrentBusinessTitle = GetCustomFieldValue(employee, "BUSINESSTITLE");
            return toReturn;
        }
    }
}
