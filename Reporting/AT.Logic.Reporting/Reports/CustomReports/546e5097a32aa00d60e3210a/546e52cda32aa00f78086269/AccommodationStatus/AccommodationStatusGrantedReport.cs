﻿using AbsenceSoft;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Classes;
using AT.Entities.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.CustomReports._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269.AccommodationStatus
{
    /// <summary>
    /// Status Granted Class
    /// </summary>
    public class AccommodationStatusGrantedReport : AmazonReport
    {
        /// <summary>
        /// constructor
        /// </summary>
        public AccommodationStatusGrantedReport()
        {            
                Name = "Accommodations Status Granted Report";
                Category = "Accommodation Status Report";
                MainCategory = "Operations Report";            
        }
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "20BAA0BB-CB4C-4FF7-88A9-24B2AA85F2E8"; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 0; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get; set; }

        /// <summary>
        /// Category for all Accommodation Status reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get; set; }

        /// <summary>
        /// Category for all Accommodation Status reports is "Accommodation Status Report".
        /// </summary>
        public override string Category { get; set; }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get; set; }

        /// <summary>
        /// Icon to set for Accommodation Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// List of fields for grouping
        /// </summary>
        public override Dictionary<string, List<string>> AvailableGroupingFields => new Dictionary<string, List<string>>(){          
            {(ReportGroupType.Custom).ToString(), new List<string>(){
                       "CaseNumber",
                       "Status",
                       "EmployerName",
                       "RelatedCaseType",
                       "CaseType",
                       "EmployerId",
                       "CurrentOfficeLocation",
                       "CustomerId",
                       "AssignedToId",
                       "AssignedToName",
                       "cby",
                       "Employee.EmployeeNumber",
                        "Employee.FullName",
                        "Employee.Status",
                        "Reason.Name",
                        "Reason.CaseTypes",
                        "Reason.Category",
                        "Reason.Code",
                        "AccommodationRequest.Status"
            } }
        };

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get; set; }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerId.ToString(),
                StartDate = DateTime.UtcNow.Date.GetFirstDayOfMonth(),
                EndDate = DateTime.UtcNow.Date
            };
            return criteria;
        }

        /// <summary>
        /// Build query and Run the report 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="groupType"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <param name="groupByColumns"></param>
        /// <returns></returns>
        protected override Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? groupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            ReportingLogic logic = new ReportingLogic(user);
            var caseList = logic.RunAccommodationStatusGrantedQueryAsync(result.Criteria).GetAwaiter().GetResult();
            var users = logic.GetUserDetailsAsync(user.CustomerKey).GetAwaiter().GetResult();
            List<ResultData> resultItems;
            if (groupType != null)
            {
                if (groupType == ReportGroupType.Custom)
                {
                    result.Name = "Custom grouping";
                    IsGrouped = true;
                    var groupQuery = caseList.GroupBy(groupByColumns.ToArray());
                    result.JsonData = CreateJsonResult(groupQuery, users);                   
                }
                else
                {
                    throw new NotImplementedException("Group by is not implemented.");
                }
            }
            else
            {
                resultItems = CreateItemList(caseList, users);
                result.Items = resultItems;
            }

            return Task.FromResult(result);
        }

        /// <summary>
        /// Create custom Grouping
        /// </summary>
        /// <param name="groupQuery"></param>
        /// <param name="users"></param>
        /// <returns></returns>
        private dynamic CreateJsonResult(List<GroupResult> groupQuery, Dictionary<string, string> users)
        {
            List<ResultData> resultItems;
            var dataGroup = new List<dynamic>();
            if (groupQuery == null || !groupQuery.Any())
            {
                return dataGroup;
            }
            foreach (var queryItem in groupQuery)
            {

                resultItems = CreateItemList((List<Case>)queryItem.Items, users);

                dataGroup.Add(new
                {
                    Label = queryItem.Value,
                    Key = queryItem.Key,
                    Items = new List<ResultData>().AddRangeFluid(resultItems),
                    SubGroups = CreateJsonResult(queryItem.SubGroups, users)
                });
            }
            return dataGroup;

        }

        /// <summary>
        /// Create the Item list
        /// </summary>
        /// <param name="caseList"></param>
        /// <param name="users"></param>
        /// <returns></returns>
        private List<ResultData> CreateItemList(List<Case> caseList, Dictionary<string, string> users)
        {
            List<ResultData> resultItems = new List<ResultData>();
            foreach (var caseItem in caseList.Where(c => c.AccommodationRequest != null))
            {
                caseItem.AccommodationRequest.Accommodations = caseItem.AccommodationRequest.Accommodations ?? new List<Accommodation>(0);
                {
                    foreach (var accommodation in caseItem.AccommodationRequest.Accommodations.Where(a => a.Type != null).OrderBy(p => p.Type.Code))
                        foreach (var detail in accommodation.Usage.OrderBy(u => u.StartDate))
                        {
                            resultItems.Add(GetDetail(caseItem, accommodation, detail, users));
                        }
                }
            }

            return resultItems;
        }

        /// <summary>
        /// Gets the detail.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="q">The q.</param>
        /// <param name="detail">The detail.</param>
        /// <param name="displayCaseNumber">The display case number.</param>
        /// <param name="displayFullName">Display name of the full.</param>
        /// <param name="users">The users.</param>
        /// <returns></returns>
        protected virtual ResultData GetDetail(Case theCase, Accommodation q, AccommodationUsage detail, Dictionary<string, string> users)
        {
            dynamic data = new ResultData();            
            data.CaseNumber = FormatCaseNumber(theCase);
            data.EmployeeNumber = FormatEmployeeNumber(theCase.Employee);
            data.Name = string.IsNullOrWhiteSpace(theCase.Employee?.FullName) ? null : new LinkItem() { Value = theCase.Employee.FullName, Link = $"/Employees/{theCase.Employee.Id}/View"};           
            data.Class = theCase.Employee.CustomFields.FirstOrDefault(cf => cf.Name == "EmployeeClass")?.SelectedValueText ?? "";
            data.StartDate = detail.StartDate == DateTime.MinValue ? "Pending" : detail.StartDate.ToString("MM/dd/yyyy");
            data.EndDate = detail.EndDate.HasValue ? detail.EndDate.ToString("MM/dd/yyyy") : string.Empty;
            data.Accommodation = q.Type.Code;
            data.AccommStatus = q.Status.ToString().SplitCamelCaseString();
            data.Granted = q.Status == CaseStatus.Cancelled ? "Cancelled" : detail.Determination.ToString().SplitCamelCaseString();
            data.ReasonNotGranted = detail.Determination == AdjudicationStatus.Denied ? string.IsNullOrWhiteSpace(detail.DenialReasonOther) ? detail.DenialReasonName : detail.DenialReasonOther ?? "" : "";
            data.DecisionDate = detail.DeterminationDate.HasValue ? detail.DeterminationDate.ToString("MM/dd/yyyy") : "";
            data.DecisionBy = string.IsNullOrWhiteSpace(detail.DeterminationById) ? "" : users.ContainsKey(detail.DeterminationById) ? users[detail.DeterminationById] : "";
            data.WorkState = theCase.Employee.WorkState;
            data.Location = string.IsNullOrWhiteSpace(theCase.CurrentOfficeLocation) ? string.Empty : theCase.CurrentOfficeLocation;
            data.WorkRelated = q.IsWorkRelated == null ? "" : q.IsWorkRelated.Value ? "1" : "0";
            return data;
        }        
    }
}
