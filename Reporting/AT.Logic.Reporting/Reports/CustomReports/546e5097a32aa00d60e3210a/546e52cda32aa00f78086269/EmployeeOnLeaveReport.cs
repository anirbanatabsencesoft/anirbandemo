﻿using AbsenceSoft;
using AbsenceSoft.Data.Cases;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Classes;
using AT.Entities.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.CustomReports._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269
{
    /// <summary>
    /// Employee on Leave report class
    /// </summary>
    [Serializable]
    public class EmployeeOnLeaveReport : AmazonReport
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public EmployeeOnLeaveReport()
        {
            Name = "Employee On Leave Report Detail";
        }
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "E8E5E616-557E-4064-BE44-20200F22D025"; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 99; } }

        /// <summary>
        /// List of fields for grouping
        /// </summary>
        public override Dictionary<string, List<string>> AvailableGroupingFields => new Dictionary<string, List<string>>(){
               {(ReportGroupType.Custom).ToString(), new List<string>(){
                       "CaseNumber",
                       "Status",
                       "EmployerName",
                       "RelatedCaseType",
                       "CaseType",
                       "EmployerId",
                       "CurrentOfficeLocation",
                       "CustomerId",
                       "AssignedToId",
                       "AssignedToName",
                       "cby",
                       "Employee.EmployeeNumber",
                        "Employee.FullName",
                        "Employee.Status",
                        "Reason.Name",
                        "Reason.CaseTypes",
                        "Reason.Category",
                        "Reason.Code",
                        "AccommodationRequest.Status"
            } }
        };

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return false; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Accommodation Status Report".
        /// </summary>
        public override string Category { get { return "Employee on Leave Report"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get; set; }

        /// <summary>
        /// Icon to set for Accommodation Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get; set; }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerKey,
                StartDate = DateTime.UtcNow.Date.GetFirstDayOfMonth(),
                EndDate = DateTime.UtcNow.Date
            };
            return criteria;
        }

        /// <summary>
        /// Gets the detail.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <returns></returns>
        protected virtual ResultData GetDetail(Case theCase)
        {
            ResultData data = new ResultData
            {
                ["CaseNumber"] = new LinkItem() { Value = theCase.CaseNumber, Link = string.Format("/Cases/{0}/View", theCase.Id) },
                ["Name"] = new LinkItem() { Value = theCase.Employee.FullName, Link = string.Format("/Employees/{0}/View", theCase.Employee?.Id) },
                ["EEID"] = theCase.Employee?.EmployeeNumber
            };

            List<string> accommodationTypes = new List<string>();
            if (theCase.AccommodationRequest != null && theCase.AccommodationRequest.Accommodations != null)
            {
                accommodationTypes = theCase.AccommodationRequest.Accommodations.Select(a => a.Type.Name).ToList();
            }
            if (accommodationTypes.Any())
            {
                data["AccommodationType"] = string.Join(",", accommodationTypes);
            }
            else
            {
                data["AccommodationType"] = "";
            }
            data["OtherLeaveCaseNumber"] = theCase.Metadata.GetRawValue<string>("OtherLeaveCaseNumber") ?? "";
            return data;
        }

        /// <summary>
        /// Build and Run the Employee Leave Report data
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="reportGroupType"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <param name="groupByColumns"></param>
        /// <returns></returns>
        protected async override Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            var caseList = reportingLogic.RunEmployeeOnLeaveReportQueryAsync(result.Criteria, pageNumber, pageSize, sortBy, orderByAsc).GetAwaiter().GetResult();

            List<ResultData> resultItems = new List<ResultData>();

            if (reportGroupType != null)
            {
                if (reportGroupType == ReportGroupType.Custom)
                {
                    result.Name = "Custom grouping";
                    IsGrouped = true;
                    var groupQuery = caseList.GroupBy(groupByColumns.ToArray());
                    result.JsonData = CreateJsonResult(groupQuery);
                }
                else
                {
                    throw new NotImplementedException("Group by is not implemented.");
                }
            }
            else
            {
                foreach (var item in caseList)
                {
                    resultItems.Add(GetDetail(item));
                }
                result.Items = resultItems;
            }     

            return await Task.FromResult(result);
        }

        /// <summary>
        /// Create custom Grouping
        /// </summary>
        /// <param name="groupQuery"></param>
        /// <returns></returns>
        private dynamic CreateJsonResult(List<GroupResult> groupQuery)
        {
            List<ResultData> resultItems;
            var dataGroup = new List<dynamic>();
            if (groupQuery == null || !groupQuery.Any())
            {
                return dataGroup;
            }
            foreach (var queryItem in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (Case item in queryItem.Items)
                {
                    resultItems.Add(GetDetail(item));
                }               

                dataGroup.Add(new
                {
                    Label = queryItem.Value,
                    Key = queryItem.Key,
                    Items = new List<ResultData>().AddRangeFluid(resultItems),
                    SubGroups = CreateJsonResult(queryItem.SubGroups)
                });
            }
            return dataGroup;

        }
    }
}
