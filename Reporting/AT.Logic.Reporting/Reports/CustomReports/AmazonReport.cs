﻿using AbsenceSoft;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AT.Data.Reporting.Model;
using AT.Entities.Authentication;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AT.Logic.Reporting.Reports.CustomReports
{
    /// <summary>
    /// Base report for all Amazon custom reports.
    /// </summary>
    public abstract class AmazonReport : BaseReport
    {
        /// <summary>
        /// The customer identifier for Amazon in ALL environments
        /// </summary>
        internal const string CustomerId = "546e5097a32aa00d60e3210a";

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get; set; }       

        /// <summary>
        /// Determines whether the specified report is visible to the given user for any valid reason.
        /// </summary>
        /// <param name="user">The user to check against.</param>
        /// <returns>
        ///   <c>true</c> if the specified report is visible; otherwise, <c>false</c>.
        /// </returns>
        public override bool IsVisible(User user)
        {
            return user != null && user.CustomerKey == CustomerId;
        }

        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns>ADA Feature</returns>
        public override IEnumerable<Feature> RequiredFeatures()
        {
            yield return Feature.ADA;
        }

        /// <summary>
        /// Category for all Accommodation Status reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get; set; }

        /// <summary>
        /// Category for all Accommodation Status reports is "Accommodation Status Report".
        /// </summary>
        public override string Category { get; set; }

        /// <summary>
        /// Icon to set for Accommodation Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get; set; }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            UserReportCriteriaData userReportCriteriaData = reportingLogic.GetUserReportCriteriaDataAsync().GetAwaiter().GetResult();
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerKey,
                StartDate = DateTime.UtcNow.Date.AddDays(-30),
                EndDate = DateTime.UtcNow.Date
            };
            ReportCriteriaItem item = GetCaseStatusCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetAccomTypeCriteria(userReportCriteriaData);
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetAdjudicationCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            criteria.Filters.Add(GetCaseAssigneeCriteria(userReportCriteriaData));
            criteria.Filters.Add(GetWorkStateCriteria(userReportCriteriaData, criteria.StartDate, criteria.EndDate));
            criteria.Filters.Add(GetLocationCriteria(userReportCriteriaData));
            return criteria;
        }

        /// <summary>
        /// Gets the detail.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="accommodation">The accommodation.</param>
        /// <param name="detail">The detail.</param>       
        /// <returns></returns>
        protected virtual ResultData GetDetail(Case theCase, Accommodation accommodation, AccommodationUsage detail)
        {
            dynamic data = new ResultData();
            data.CaseNumber = FormatCaseNumber(theCase);
            data.EmployeeNumber = FormatEmployeeNumber(theCase.Employee);
            data.Name = string.IsNullOrWhiteSpace(theCase.Employee?.FullName) ? null : new LinkItem() { Value = theCase.Employee?.FullName, Link = $"/Employees/{theCase.Employee?.Id}/View"};           
            data.Class = theCase.Employee?.CustomFields.FirstOrDefault(cf => cf.Name == "EmployeeClass") == null ? "" : theCase.Employee?.CustomFields.FirstOrDefault(cf => cf.Name == "EmployeeClass").SelectedValueText;
            data.CaseStatus = theCase.Status.ToString().SplitCamelCaseString();
            data.Duration = accommodation.Duration == 0 ? "Pending" : accommodation.Duration.ToString().SplitCamelCaseString();
            data.Type = accommodation.Type.Name;
            data.Created = accommodation.CreatedDate.ToString("MM/dd/yyyy");
            data.StartDate = detail.StartDate == DateTime.MinValue ? "Pending" : detail.StartDate.ToString("MM/dd/yyyy");
            data.EndDate = detail.EndDate.HasValue ? detail.EndDate.ToString("MM/dd/yyyy") : string.Empty;
            data.Accommodations = $"{accommodation.Type.Code} - {detail.Determination.ToString().SplitCamelCaseString()}";
            data.AccommStatus = accommodation.Status.ToString().SplitCamelCaseString();
            data.Resolved = accommodation.Resolved ? "Yes" : "No";
            data.ResolvedDate = accommodation.ResolvedDate.HasValue ? accommodation.ResolvedDate.ToString("MM/dd/yyyy") : string.Empty;
            data.Granted = accommodation.Granted ? "Yes" : "No";
            data.GrantedDate = accommodation.GrantedDate.HasValue ? accommodation.GrantedDate.ToString("MM/dd/yyyy") : string.Empty;
            data.AssignedTo = theCase.AssignedToName;
            data.WorkState = theCase.Employee?.WorkState;
            data.Location = string.IsNullOrWhiteSpace(theCase.CurrentOfficeLocation) ? string.Empty : theCase.CurrentOfficeLocation;
            data.WorkRelated = accommodation.IsWorkRelated == null ? "" : accommodation.IsWorkRelated.Value ? "1" : "0";
            data.CostCenter = theCase.Employee?.CostCenterCode ?? "";
            data.ClosedDate = theCase.GetClosedDate().HasValue ? string.Format("{0:MM/dd/yyyy}", theCase.GetClosedDate()) : string.Empty;
            return data;
        }

        /// <summary>
        /// Gets the blank detail.
        /// </summary>
        /// <param name="theCase">The case.</param>      
        /// <returns></returns>
        protected virtual ResultData GetBlankDetail(Case theCase)
        {
            dynamic data = new ResultData();
            data.CaseNumber = FormatCaseNumber(theCase);
            data.EmployeeNumber = FormatEmployeeNumber(theCase.Employee);
            data.Name = string.IsNullOrWhiteSpace(theCase.Employee?.FullName) ? null : new LinkItem() { Value = theCase.Employee.FullName, Link = string.Format("/Employees/{0}/View", theCase.Employee.Id) };
            data.Duration = theCase.AccommodationRequest?.SummaryDurationType;
            data.Type = "N/A";
            data.CaseStatus = theCase.Status.ToString().SplitCamelCaseString();
            var evt = theCase.FindCaseEvent(CaseEventType.CaseCreated);
            data.Created = (evt == null ? theCase.CreatedDate : evt.EventDate).ToString("MM/dd/yyyy");
            data.StartDate = theCase.StartDate.ToString("MM/dd/yyyy");
            data.EndDate = theCase.EndDate.ToString("MM/dd/yyyy");
            data.Accommodations = "N/A";
            data.AccommStatus = "N/A";
            data.Resolved = "N/A";
            data.ResolvedDate = "N/A";
            data.Granted = "N/A";
            data.GrantedDate = "N/A";
            data.AssignedTo = theCase.AssignedToName;
            data.WorkState = theCase.Employee?.WorkState;
            data.Location = theCase.CurrentOfficeLocation;
            data.WorkRelated = "";
            if (theCase.AccommodationRequest != null && theCase.AccommodationRequest.Accommodations != null)
            {
                data.WorkRelated = theCase.AccommodationRequest.Accommodations.Any(r => r.IsWorkRelated == true) ? "1" : theCase.AccommodationRequest.Accommodations.Any(r => r.IsWorkRelated == false) ? "0" : "";
            }               
            data.CostCenter = theCase.Employee?.CostCenterCode ?? "";
            return data;
        }


        /// <summary>
        /// Format the Case number 
        /// </summary>
        /// <param name="theCase">The case</param>
        /// <returns>Link Item</returns>
        protected LinkItem FormatCaseNumber(Case theCase)
        {
            if (string.IsNullOrEmpty(theCase.CaseNumber))
                return null;

            return new LinkItem() { Value = theCase.CaseNumber, Link = $"/Cases/{theCase.Id}/View" };
        }

        /// <summary>
        /// Format the Employee number
        /// </summary>
        /// <param name="emp">Employee</param>
        /// <returns>Link item</returns>
        protected LinkItem FormatEmployeeNumber(Employee emp)
        {
            if (string.IsNullOrEmpty(emp?.EmployeeNumber))
                return null;

            return new LinkItem() { Value = emp.EmployeeNumber, Link = $"/Employees/{emp.Id}/View" };
        }

        /// <summary>
        /// Format the Employee number
        /// </summary>
        /// <param name="emp">Employee</param>
        /// <returns>Link item</returns>
        protected LinkItem FormatEmployeeName(Employee emp)
        {
            if (string.IsNullOrEmpty(emp?.FullName))
                return null;

            return new LinkItem() { Value = emp.FullName, Link = $"/Employees/{emp.Id}/View" };
        }
       

        /// <summary>
        /// Get the Custom field value
        /// </summary>
        /// <param name="emp">Employee</param>
        /// <param name="customFieldCode">Code</param>
        /// <returns>Custom value</returns>
        protected string GetCustomFieldValue(Employee emp, string customFieldCode)
        {
            string selectedValue = emp.CustomFields
                .Where(cf => cf.Code == customFieldCode)
                .Select(cf => cf.SelectedValueText).FirstOrDefault();

            if (string.IsNullOrWhiteSpace(selectedValue))
                return string.Empty;
            return selectedValue;
        }
    
    }
}
