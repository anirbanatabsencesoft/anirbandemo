﻿using AbsenceSoft;
using AbsenceSoft.Data.Enums;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AT.Logic.Reporting.Reports.WorkRelated
{
    [Serializable]
    public partial class WorkRelated : BaseReport
    {
        public WorkRelated()
        {
            MainCategory = "Operations Report";
            Category = "Work Related";
        }
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "688BDE87-48AD-4560-84A7-982D5DE8D74A"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get; set; } = "Work Related";

        /// <summary>
        /// When overridden in a derived class, gets the name of the report main category to bifurcate reports.
        /// </summary>
        public override string MainCategory { get; set; }

        /// <summary>
        /// return the desciprion 
        /// </summary>
        public override string ReportDescription
        {
            get { return "Case list provides details about each work related case"; }
        }
        /// <summary>
        /// Icon to set for intermittent pattern report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_intermittent.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report category for display.
        /// </summary>
        public override string Category { get; set; }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get; set; }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get; set; }

        /// <summary>
        /// Maintains which type of Report to be generated Needle Sticks or Injury Details
        /// </summary>
        public ReportGroupType ReportGroupType { get; set; }

        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<Feature> RequiredFeatures()
        {
            yield return Feature.WorkRelatedReporting;
        }

        /// <summary>
        /// Determines whether the specified user is visible.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public override bool IsVisible(User user)
        {
            // Ensure customer is not oh.
            return user != null && user.CustomerId.ToString() != "56b90041c5f32f05088a9e8d";
        }

        /// <summary>
        /// List of fields for grouping
        /// </summary>
        public override Dictionary<string, List<string>> AvailableGroupingFields => new Dictionary<string, List<string>>(){
            {(ReportGroupType.NeedleSticks).ToString(), new List<string>(0)},
            {(ReportGroupType.InjuryDetails).ToString(), new List<string>(0)}
        };

        /// <summary>
        /// Gets Enum Option
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <returns></returns>
        protected List<ReportCriteriaItemOption> GetEnumOptions<TEnum>() where TEnum : struct
        {
            if (typeof(TEnum).IsEnum)
            {
                return Enum.GetValues(typeof(TEnum)).OfType<TEnum>().Select(e => new ReportCriteriaItemOption()
                {
                    Text = e.ToString().SplitCamelCaseString(),
                    Value = Convert.ToInt32(e)
                }).ToList();
            }

            return null;
        }

        /// <summary>
        /// Gets Report Criteria Item
        /// </summary>
        /// <param name="name"></param>
        /// <param name="prompt"></param>
        /// <param name="required"></param>
        /// <param name="controlType"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        protected ReportCriteriaItem GetReportCriteriaItem(string name, string prompt, bool required, ControlType controlType, List<ReportCriteriaItemOption> options = null)
        {
            ReportCriteriaItem toReturn = new ReportCriteriaItem
            {
                Name = name,
                Prompt = prompt,
                Required = required,
                ControlType = controlType,
                Options = options
            };

            return toReturn;
        }

        /// <summary>
        /// Gets the Sharps Criteria for the User
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        protected IEnumerable<ReportCriteriaItem> GetSharpsCriteria(User user)
        {
            yield return GetReportCriteriaItem("WorkRelatedSharpsInjuryLocation", "Inury Location", false, ControlType.CheckBoxList, GetEnumOptions<WorkRelatedSharpsInjuryLocation>());
            yield return GetReportCriteriaItem("WorkRelatedSharpsLocation", "Location/Department", false, ControlType.CheckBoxList, GetEnumOptions<WorkRelatedSharpsLocation>());
            yield return GetReportCriteriaItem("WorkRelatedSharpsProcedure", "Procedure", false, ControlType.CheckBoxList, GetEnumOptions<WorkRelatedSharpsProcedure>());
            yield return GetReportCriteriaItem("WorkRelatedSharpsJobClassification", "Job Classification", false, ControlType.CheckBoxList, GetEnumOptions<WorkRelatedSharpsJobClassification>());
            var userData = GetUserReportCriteriaData(user);
            yield return GetLocationCriteria(userData);
        }

        /// <summary>
        /// Gets Work Related Criteria  
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        protected IEnumerable<ReportCriteriaItem> GetWorkRelatedCriteria(User user)
        {
            yield return GetReportCriteriaItem("WorkRelatedCaseClassification", "Case Classification", false, ControlType.CheckBoxList, GetEnumOptions<WorkRelatedCaseClassification>());
            yield return GetReportCriteriaItem("WorkRelatedTypeOfInjury", "Type of Injury", false, ControlType.CheckBoxList, GetEnumOptions<WorkRelatedTypeOfInjury>());
            var userData = GetUserReportCriteriaData(user);
            yield return GetLocationCriteria(userData);
        }
    }
}