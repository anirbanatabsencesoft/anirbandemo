﻿using AbsenceSoft;
using AbsenceSoft.Data.Enums;
using AT.Entities.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.WorkRelated
{
    public partial class WorkRelated : BaseReport
    {
        /// <summary>
        /// Gets the Report Criteria for the User
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public override ReportCriteria GetCriteria(AT.Entities.Authentication.User user)
        {
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerId.ToString(),
                StartDate = DateTime.UtcNow.Date.AddDays(-30),
                EndDate = DateTime.UtcNow.Date
            };

            if (ReportGroupType == ReportGroupType.NeedleSticks)
            {
                criteria.Filters.AddRange(GetSharpsCriteria(user));
            }
            else
            {
                criteria.Filters.AddRange(GetWorkRelatedCriteria(user));
            }

            return criteria;
        }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        /// <param name="pageNumber">Add the page number</param>
        /// <param name="pageSize">Set the page size</param>
        protected async override Task<ReportResult> RunReportAsync(AT.Entities.Authentication.User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            List<ResultData> reportResultItems = new List<ResultData>();
            var userData = GetUserReportCriteriaData(user);
            ReportingLogic reportingLogic = new ReportingLogic(user);
            var caseList = await reportingLogic.GetCaseListForWorkRelatedReport(userData, result.Criteria, reportGroupType, EmployerIdFilterName, pageNumber, pageSize);

            if (reportGroupType == ReportGroupType.NeedleSticks)
            {
                result.Name = "Needlestick";
                IsGrouped = false;
                await SetCaseRowsNeedleStick(reportResultItems, result, caseList);
            }
            else
            {
                result.Name = "Injury Detail";
                IsGrouped = true;
                await SetCaseRowsInjuryDetails(reportResultItems, result, caseList);
            }

            return result;
        }
        
        /// <summary>
        /// Set Case Rows for Injury Details Work Related report
        /// </summary>
        /// <param name="reportResultItems"></param>
        /// <param name="result"></param>
        /// <param name="caseList"></param>
        private async Task<ReportResult> SetCaseRowsInjuryDetails(List<ResultData> reportResultItems, ReportResult result, List<AbsenceSoft.Data.Cases.Case> caseList)
        {
            foreach (var c in caseList)
            {
                dynamic row = new ResultData();
                DateTime? displayRTW = GetCaseEventDate(c, CaseEventType.ReturnToWork);
                DateTime? displayERTW = GetCaseEventDate(c, CaseEventType.EstimatedReturnToWork);

                row.CaseNumber = new LinkItem() { Value = c.CaseNumber, Link = string.Format("/Cases/{0}/View", c.Id) };
                row.EmployeeNumber = new LinkItem() { Value = c.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", c.Employee.Id) };
                row.FirstName = new LinkItem() { Value = c.Employee.FirstName, Link = string.Format("/Employees/{0}/View", c.Employee.Id) };
                row.LastName = new LinkItem() { Value = c.Employee.LastName, Link = string.Format("/Employees/{0}/View", c.Employee.Id) };

                row.StartDate = c.StartDate.ToUIString();
                row.EndDate = c.EndDate.HasValue ? c.EndDate.ToUIString() : string.Empty;
                row.LeaveType = c.CaseType.ToString();
                row.Status = c.GetCaseStatus().ToString().SplitCamelCaseString();
                row.Reason = c.Reason == null ? string.Empty : c.Reason.Name;
                SetIfWorkRelatedExists(c, row);
                row.JobDetailOccupation = c.Employee.JobTitle ?? string.Empty;
                row.TotalClaims = "";
                row.ReturnToWork = displayRTW != null ? displayRTW.ToString("MM/dd/yyyy") : string.Empty;
                row.EstimatedReturnToWork = displayERTW != null ? displayERTW.ToString("MM/dd/yyyy") : string.Empty;

                reportResultItems.Add(row);
            }

            ResultDataGroup group = new ResultDataGroup() { Key = "Details", Items = reportResultItems, Label = "Details" };

            // Report Totals
            var totalClaims = caseList.Count;
            var totalDaysAwayFromWork = caseList.Sum(c => c.WorkRelated.DaysAwayFromWork.GetValueOrDefault());
            var totalDaysOnJobTransferOrRestriction = caseList.Sum(c => c.WorkRelated.DaysOnJobTransferOrRestriction.GetValueOrDefault());

            List<ResultData> reportSummaryItems = new List<ResultData>();
            if (caseList.Count > 0)
            {
                dynamic rowTotals = new ResultData();

                rowTotals.CaseNumber = string.Empty;
                rowTotals.EmployeeNumber = string.Empty;
                rowTotals.FirstName = string.Empty;
                rowTotals.LastName = string.Empty;
                rowTotals.StartDate = string.Empty;
                rowTotals.EndDate = string.Empty;
                rowTotals.LeaveType = string.Empty;
                rowTotals.Status = string.Empty;
                rowTotals.Reason = string.Empty;
                rowTotals.Reportable = string.Empty;
                rowTotals.Classification = string.Empty;
                rowTotals.WhereOccurred = string.Empty;
                rowTotals.TypeOfInjury = string.Empty;
                rowTotals.InjuryOrIllness = string.Empty;
                rowTotals.WhatHappened = string.Empty;
                rowTotals.DaysAwayFromWork = totalDaysAwayFromWork;
                rowTotals.DaysOnJobTransferOrRestriction = totalDaysOnJobTransferOrRestriction;
                rowTotals.ActivityBeforeIncident = string.Empty;
                rowTotals.WhatHarmedTheEmployee = string.Empty;
                rowTotals.JobDetailOccupation = string.Empty;
                rowTotals.TotalClaims = totalClaims;
                rowTotals.ReturnToWork = string.Empty;
                rowTotals.EstimatedReturnToWork = string.Empty;

                reportSummaryItems.Add(rowTotals);
            }
            ResultDataGroup groupSummary = new ResultDataGroup() { Key = "ReportTotals", Items = reportSummaryItems, Label = "Report Totals;" };

            result.Groups = new List<ResultDataGroup>();
            if (caseList.Count > 0)
            {
                result.Groups.Add(group);
                result.Groups.Add(groupSummary);
            }
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Set Case Rows if Work Related data exists
        /// </summary>
        /// <param name="c"></param>
        /// <param name="row"></param>
        private void SetIfWorkRelatedExists(AbsenceSoft.Data.Cases.Case c, dynamic row)
        {
            if (c.WorkRelated != null)
            {
                row.Reportable = c.WorkRelated.Reportable ? "Yes" : "No";
                row.Classification = c.WorkRelated.WhereOccurred ?? string.Empty;
                row.WhereOccurred = c.WorkRelated.WhereOccurred ?? string.Empty;
                if (c.WorkRelated.TypeOfInjury.HasValue)
                {
                    row.TypeOfInjury = c.WorkRelated.TypeOfInjury.ToString().SplitCamelCaseString();
                }
                else
                {
                    row.TypeOfInjury = string.Empty;
                }
                row.InjuryOrIllness = c.WorkRelated.InjuryOrIllness ?? string.Empty;
                row.WhatHappened = c.WorkRelated.WhatHappened ?? string.Empty;
                if (c.WorkRelated.DaysAwayFromWork.HasValue)
                {
                    row.DaysAwayFromWork = c.WorkRelated.DaysAwayFromWork.Value.ToString();
                }
                else
                {
                    row.DaysAwayFromWork = string.Empty;
                }
                if (c.WorkRelated.DaysOnJobTransferOrRestriction.HasValue)
                {
                    row.DaysOnJobTransferOrRestriction = c.WorkRelated.DaysOnJobTransferOrRestriction.Value.ToString();
                }
                else
                {
                    row.DaysOnJobTransferOrRestriction = string.Empty;
                }
                row.ActivityBeforeIncident = c.WorkRelated.ActivityBeforeIncident ?? string.Empty;
                row.WhatHarmedTheEmployee = c.WorkRelated.WhatHarmedTheEmployee ?? string.Empty;
            }
            else
            {
                row.Reportable = string.Empty;
                row.Classification = string.Empty;
                row.WhereOccurred = string.Empty;
                row.TypeOfInjury = string.Empty;
                row.InjuryOrIllness = string.Empty;
                row.WhatHappened = string.Empty;
                row.DaysAwayFromWork = string.Empty;
                row.DaysOnJobTransferOrRestriction = string.Empty;
                row.ActivityBeforeIncident = string.Empty;
                row.WhatHarmedTheEmployee = string.Empty;
            }
        }

        /// <summary>
        /// Sets Case Rows for Needle Stick report
        /// </summary>
        /// <param name="reportResultItems"></param>
        /// <param name="result"></param>
        /// <param name="caseList"></param>
        private async Task<ReportResult> SetCaseRowsNeedleStick(List<ResultData> reportResultItems, ReportResult result, List<AbsenceSoft.Data.Cases.Case> caseList)
        {
            foreach (var c in caseList)
            {
                dynamic row = new ResultData();

                row.CaseNumber = new LinkItem() { Value = c.CaseNumber, Link = string.Format("/Cases/{0}/View", c.Id) };
                row.EmployeeNumber = new LinkItem() { Value = c.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", c.Employee.Id) };
                row.FirstName = new LinkItem() { Value = c.Employee.FirstName, Link = string.Format("/Employees/{0}/View", c.Employee.Id) };
                row.LastName = new LinkItem() { Value = c.Employee.LastName, Link = string.Format("/Employees/{0}/View", c.Employee.Id) };

                row.StartDate = c.StartDate.ToUIString();
                row.EndDate = c.EndDate.ToUIString();
                row.LeaveType = c.CaseType.ToString();
                row.Status = c.GetCaseStatus().ToString().SplitCamelCaseString();
                row.Reason = c.Reason.Name;
                row.Reportable = c.WorkRelated.Reportable ? "Yes" : "No";
                row.Classification = (c.WorkRelated.Classification.HasValue) ? c.WorkRelated.Classification.ToString().SplitCamelCaseString() : string.Empty;
                row.WhereOccurred = c.WorkRelated.WhereOccurred;
                row.TypeOfInjury = (c.WorkRelated.TypeOfInjury.HasValue) ? c.WorkRelated.TypeOfInjury.ToString().SplitCamelCaseString() : string.Empty;
                row.InjuryOrIllness = c.WorkRelated.InjuryOrIllness;
                row.WhatHappened = c.WorkRelated.WhatHappened;
                row.DaysAwayFromWork = c.WorkRelated.DaysAwayFromWork;
                row.DaysOnJobTransferOrRestriction = c.WorkRelated.DaysOnJobTransferOrRestriction;
                row.ActivityBeforeIncident = c.WorkRelated.ActivityBeforeIncident;
                row.WhatHarmedTheEmployee = c.WorkRelated.WhatHarmedTheEmployee;
                row.JobDetailOccupation = c.Employee.JobTitle;
                SetSharpSpecific(c, row);
                reportResultItems.Add(row);
            }
            result.Items = reportResultItems;
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Sets Case Rows for Sharp specific data
        /// </summary>
        /// <param name="c"></param>
        /// <param name="row"></param>
        private void SetSharpSpecific(AbsenceSoft.Data.Cases.Case c, dynamic row)
        {
            // Sharps Specifics
            row.InjuryLocation = string.Join(", ", c.WorkRelated.SharpsInfo.InjuryLocation.Select(i => i.ToString().SplitCamelCaseString()));

            if (c.WorkRelated != null && c.WorkRelated.SharpsInfo != null)
            {
                if (c.WorkRelated.SharpsInfo.JobClassification.HasValue)
                {
                    row.JobClassification = c.WorkRelated.SharpsInfo.JobClassification.ToString().SplitCamelCaseString();
                }
                else
                {
                    row.JobClassification = string.Empty;
                }
                if (c.WorkRelated.SharpsInfo.LocationAndDepartment.HasValue)
                {
                    row.LocationAndDepartment = c.WorkRelated.SharpsInfo.LocationAndDepartment.ToString().SplitCamelCaseString();
                }
                else
                {
                    row.LocationAndDepartment = string.Empty;
                }
                row.SharpType = c.WorkRelated.SharpsInfo.TypeOfSharp ?? string.Empty;
                row.SharpBrand = c.WorkRelated.SharpsInfo.BrandOfSharp ?? string.Empty;

                if (c.WorkRelated.SharpsInfo.Procedure.HasValue)
                {
                    row.Procedure = c.WorkRelated.SharpsInfo.Procedure.ToString().SplitCamelCaseString();
                }
                else
                {
                    row.Procedure = string.Empty;
                }
                row.ExposureDetail = c.WorkRelated.SharpsInfo.ExposureDetail ?? string.Empty;
            }
            else
            {
                row.JobClassification = string.Empty;
                row.LocationAndDepartment = string.Empty;
                row.SharpType = string.Empty;
                row.SharpBrand = string.Empty;
                row.Procedure = string.Empty;
                row.ExposureDetail = string.Empty;
            }
        }
    }
}