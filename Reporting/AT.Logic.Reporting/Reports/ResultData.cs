﻿using AbsenceSoft;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports
{
    /// <summary>
    /// Dynamic Result data class
    /// </summary>
    [Serializable]
    public class ResultData : DynamicAwesome
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public ResultData() :base()
        {
        }

    }
}
