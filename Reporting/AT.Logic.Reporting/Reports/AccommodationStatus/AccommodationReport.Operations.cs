﻿using AbsenceSoft;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Classes;
using AT.Entities.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.AccommodationStatus
{
    /// <summary>
    /// Base report for Accommodation report
    /// </summary> 
    [Serializable]
    public partial class AccommodationReport : BaseReport
    {
        /// <summary>
        /// Constructor 
        /// </summary>
        public AccommodationReport()
        {
            Name = "Accommodation Status Report";
            MainCategory = "Operations Report";
            Category = "Accommodation Status Report";
        }
        /// <summary>
        /// return the desciprion 
        /// </summary>
        public override string ReportDescription
        {
            get { return "Case list provides high level details about each accommodation case"; }
        }
        /// <summary>
        /// To be used for ESS category
        /// </summary>
        public override string ESSCategoryTitle { get { return "ADA Report"; } }

        /// <summary>
        /// Id of the report
        /// </summary>
        public override string Id { get { return "EA3B77C2-374F-49ED-859F-54FDFE1066E4"; } }

        /// <summary>
        /// Name of the Report.
        /// </summary>
        public override string Name { get; set; }

        /// <summary>
        /// Determine if Report is Grouped
        /// </summary>
        public override bool IsGrouped { get ; set ; }

        /// <summary>
        /// Category for all Accommodation Status reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get; set; }

        /// <summary>
        /// Category for all Accommodation Status reports is "Accommodation Status Report".
        /// </summary>
        public override string Category { get; set; }

        /// <summary>
        /// Icon to set for Accommodation Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns>ADA Feature</returns>
        public override IEnumerable<AbsenceSoft.Data.Enums.Feature> RequiredFeatures()
        {
            yield return AbsenceSoft.Data.Enums.Feature.ADA;
        }

        /// <summary>
        /// Determines whether the specified user is visible.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>Boolean</returns>
        public override bool IsVisible(User user)
        {
            // Ensure customer is not parrot.
            return user != null && user.CustomerKey != "546e5097a32aa00d60e3210a";
        }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get; set; }

        /// <summary>
        /// List of fields for grouping
        /// </summary>
        public override Dictionary<string, List<string>> AvailableGroupingFields => new Dictionary<string, List<string>>(){
            {(ReportGroupType.None).ToString(), new List<string>(0)},
            {(ReportGroupType.AccommodationType).ToString(), new List<string>(0)},
            {(ReportGroupType.WorkState).ToString(), new List<string>(0)},
            {(ReportGroupType.Location).ToString(), new List<string>(0)},
            {(ReportGroupType.CaseManager).ToString(), new List<string>(0)},
            {(ReportGroupType.Duration).ToString(), new List<string>(0)},
            {(ReportGroupType.Organization).ToString(), new List<string>(0)},
            {(ReportGroupType.Custom).ToString(), new List<string>(){
                       "CaseNumber",
                       "Status",
                       "EmployerName",
                       "RelatedCaseType",
                       "CaseType",
                       "EmployerId",
                       "CurrentOfficeLocation",
                       "CustomerId",
                       "AssignedToId",
                       "AssignedToName",
                       "cby",
                       "Employee.EmployeeNumber",
                        "Employee.FullName",
                        "Employee.Status",
                        "Reason.Name",
                        "Reason.CaseTypes",
                        "Reason.Category",
                        "Reason.Code",
                        "AccommodationRequest.Status"
            } }
        };

        /// <summary>
        /// Format Employee name
        /// </summary>
        /// <param name="employee">Employee</param>
        /// <returns>Employee name</returns>
        internal string FormatEmployeeName(Employee employee)
        {
            return string.Format("{0}, {1} {2}",
                    employee.LastName,
                    employee.FirstName,
                    !string.IsNullOrWhiteSpace(employee.MiddleName) ? employee.MiddleName.Substring(0, 1) : string.Empty
                );
        }

     /// <summary>
     /// Accommodation Report Group by Type
     /// </summary>
     /// <param name="caseList">Case List</param>
     /// <param name="result">Report Result</param>
     /// <returns></returns>
        private async Task<ReportResult> AccommodationGroupByTypeAsync(List<Case> caseList, ReportResult result)
        {
            result.Name= "by Type";

            var groupQuery = caseList.Where(c => c.AccommodationRequest != null).SelectMany(c => c.AccommodationRequest.Accommodations).GroupBy(n => n.Type.Name)
                .Select(g => new
                {
                    label = g.Key,
                    data = g.Select(x => x).ToList()
                }).OrderBy(m => m.label);

            List<ResultData> resultItems;

            result.Groups = new List<ResultDataGroup>();
            int i = 1;
            foreach (var t in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (var q in t.data)
                {
                    var theCase = caseList.FirstOrDefault(c => c.AccommodationRequest != null &&
                        c.AccommodationRequest.Accommodations != null &&
                        c.AccommodationRequest.Accommodations.Any(a => a.Id == q.Id));

                    string displayCaseNumber = theCase.CaseNumber;
                    string displayFullName = theCase.Employee.FullName;

                    foreach (var detail in q.Usage.Where(u => u.StartDate.DateRangesOverLap(u.EndDate, result.Criteria.StartDate, result.Criteria.EndDate)).OrderBy(u => u.StartDate))
                    {
                        resultItems.Add(GetDetail(theCase, q, detail, displayCaseNumber, displayFullName));
                    }
                }

                result.Groups.Add(new ResultDataGroup()
                {
                    Key = i,
                    Label = t.label,
                    Items = new List<ResultData>().AddRangeFluid(resultItems)
                });

                i++;
            }

            return await Task.FromResult(result);
        }

        /// <summary>
        /// Run the report with custom grouping
        /// </summary>
        /// <param name="cases"></param>
        /// <param name="result"></param>
        /// <param name="groupByColumns"></param>
        /// <returns></returns>
        private async Task<ReportResult> AccommodationGroupByCustomAsync(List<Case> cases, ReportResult result, List<string> groupByColumns)
        {
            result.Name = "Custom grouping";
            var groupQuery = cases.GroupBy(groupByColumns.ToArray());
            result.JsonData = CreateJsonResult(groupQuery, result);
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Create custom Grouping
        /// </summary>
        /// <param name="groupQuery"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        private dynamic CreateJsonResult(List<GroupResult> groupQuery, ReportResult result)
        {
            List<ResultData> resultItems;
            var dataGroup = new List<dynamic>();
            if (groupQuery == null || !groupQuery.Any())
            {
                return dataGroup;
            }
            foreach (var queryItem in groupQuery)
            {
              
                resultItems= CreateListItems(result, (List<Case>)queryItem.Items);
               
                dataGroup.Add(new
                {
                    Label = queryItem.Value,
                    queryItem.Key,
                    Items = new List<ResultData>().AddRangeFluid(resultItems),
                    SubGroups = CreateJsonResult(queryItem.SubGroups, result)
                });
            }
            return dataGroup;

        }

        /// <summary>
        /// Accommodation Report Group by Location
        /// </summary>
        /// <param name="caseList">Case List</param>
        /// <param name="result">Report Result</param>
        /// <returns></returns>
        private async Task<ReportResult> AccommodationGroupByLocationAsync(List<Case> caseList, ReportResult result)
        {
            result.Name= "by Office Location"; 

            var groupQuery = caseList.GroupBy(n => n.CurrentOfficeLocation ?? "None")
             .Select(g => new
             {
                 label = g.Key,
                 data = g.Select(x => x).ToList()
             }).OrderBy(m => m.label);

            List<ResultData> resultItems;

            result.Groups = new List<ResultDataGroup>();
            int i = 1;
            foreach (var t in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (var q in t.data.Where(c => c.AccommodationRequest != null))
                {
                    q.AccommodationRequest.Accommodations = q.AccommodationRequest.Accommodations ?? new List<Accommodation>(0);
                    string displayCaseNumber = q.CaseNumber;
                    string displayFullName = q.Employee.FullName;

                    foreach (var accom in q.AccommodationRequest.Accommodations.Where(a => a.Type != null).OrderBy(p => p.Type.Code))
                    {
                        foreach (var detail in accom.Usage.Where(u => u.StartDate.DateRangesOverLap(u.EndDate, result.Criteria.StartDate, result.Criteria.EndDate)).OrderBy(u => u.StartDate))
                        {
                            resultItems.Add(GetDetail(q, accom, detail, displayCaseNumber, displayFullName));
                        }
                    }

                    if (!q.AccommodationRequest.Accommodations.Any(a => a.Type != null))
                    {
                        resultItems.Add(GetBlankDetail(q, displayCaseNumber, displayFullName));
                    }
                }

                result.Groups.Add(new ResultDataGroup()
                {
                    Key = i,
                    Label = t.label,
                    Items = new List<ResultData>().AddRangeFluid(resultItems)
                });

                i++;
            }
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Accommodation Report Group by Organization
        /// </summary>
        /// <param name="caseList">Case List</param>
        /// <param name="result">Report Result</param>
        /// <returns></returns>
        private async Task<ReportResult> AccommodationGroupByOrganizationAsync(List<Case> caseList, ReportResult result)
        {
            result.Name= "by Organization";

            List<EmployeeOrganization> employeeUniqueOrganizations = GetEmployeeUniqueOrganization(result, caseList);
            var groupQuery = employeeUniqueOrganizations.GroupBy(org => new { org.CustomerId, org.EmployerId, org.Code })
                .Select(g => new
                {
                    label = employeeUniqueOrganizations.FirstOrDefault(empOrgs => empOrgs.Code == g.Key.Code)?.Name,
                    data = g.Select(x => x).ToList()
                }).OrderBy(m => m.label);

            List<ResultData> resultItems;
            result.Groups = new List<ResultDataGroup>();
            int i = 1;

            List<string> empLocationCase = new List<string>();
            foreach (var queryItem in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (var org in queryItem.data)
                {
                    SetAccommadationData(result, caseList, resultItems, empLocationCase, org);
                }

                if (resultItems.Count > 0)
                {
                    result.Groups.Add(new ResultDataGroup()
                    {
                        Key = i,
                        Label = queryItem.label,
                        Items = new List<ResultData>().AddRangeFluid(resultItems)
                    });
                }

                i++;
            }
            return await Task.FromResult(result);
        }
        /// <summary>
        /// Accommodation Report Group by Duration
        /// </summary>
        /// <param name="caseList">Case List</param>
        /// <param name="result">Report Result</param>
        /// <returns></returns>
        private async Task<ReportResult> AccommodationGroupByDurationAsync(List<Case> caseList, ReportResult result)
        {
            result.Name = "by Duration";

            var groupQuery = caseList.Where(c => c.AccommodationRequest != null).SelectMany(c => c.AccommodationRequest.Accommodations)
                 .GroupBy(n => n.Duration == 0 ? "Pending" : n.Duration.ToString().SplitCamelCaseString())
                 .Select(g => new
                 {
                     label = g.Key,
                     data = g.Select(x => x).ToList()
                 }).OrderBy(m => m.label);

            List<ResultData> resultItems;

            result.Groups = new List<ResultDataGroup>();
            int i = 1;
            foreach (var t in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (var q in t.data)
                {
                    var theCase = caseList.FirstOrDefault(c => c.AccommodationRequest != null &&
                        c.AccommodationRequest.Accommodations != null &&
                        c.AccommodationRequest.Accommodations.Any(a => a.Id == q.Id));
                    string displayCaseNumber = theCase.CaseNumber;
                    string displayFullName = theCase.Employee.FullName;
                    foreach (var detail in q.Usage.Where(u => u.StartDate.DateRangesOverLap(u.EndDate, result.Criteria.StartDate, result.Criteria.EndDate)).OrderBy(u => u.StartDate))
                    {
                        resultItems.Add(GetDetail(theCase, q, detail, displayCaseNumber, displayFullName));
                    }
                }

                result.Groups.Add(new ResultDataGroup()
                {
                    Key = i,
                    Label = t.label,
                    Items = new List<ResultData>().AddRangeFluid(resultItems)
                });

                i++;
            }
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Accommodation Report Group by Case manager
        /// </summary>
        /// <param name="caseList">Case List</param>
        /// <param name="result">Report Result</param>
        /// <returns></returns>
        private async Task<ReportResult> AccommodationGroupByCaseManagerAsync(List<Case> caseList, ReportResult result)
        {
            result.Name = "by Case Manager";

            var groupQuery = caseList.GroupBy(n => n.AssignedToName ?? "None")
                  .Select(g => new
                  {
                      label = g.Key,
                      data = g.Select(x => x).ToList()
                  }).OrderBy(m => m.label);

            List<ResultData> resultItems;

            result.Groups = new List<ResultDataGroup>();
            int i = 1;
            foreach (var t in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (var q in t.data.Where(c => c.AccommodationRequest != null))
                {
                    q.AccommodationRequest.Accommodations = q.AccommodationRequest.Accommodations ?? new List<Accommodation>(0);
                    string displayCaseNumber = q.CaseNumber;
                    string displayFullName = q.Employee.FullName;

                    foreach (var accom in q.AccommodationRequest.Accommodations.Where(a => a.Type != null).OrderBy(p => p.Type.Code))
                    {
                        foreach (var detail in accom.Usage.Where(u => u.StartDate.DateRangesOverLap(u.EndDate, result.Criteria.StartDate, result.Criteria.EndDate)).OrderBy(u => u.StartDate))
                        {
                            resultItems.Add(GetDetail(q, accom, detail, displayCaseNumber, displayFullName));
                        }
                    }
                    if (!q.AccommodationRequest.Accommodations.Any(a => a.Type != null))
                    {
                        resultItems.Add(GetBlankDetail(q, displayCaseNumber, displayFullName));
                    }
                }

                result.Groups.Add(new ResultDataGroup()
                {
                    Key = i,
                    Label = t.label,
                    Items = new List<ResultData>().AddRangeFluid(resultItems)
                });

                i++;
            }
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Accommodation Report Group by Work State
        /// </summary>
        /// <param name="caseList">Case List</param>
        /// <param name="result">Report Result</param>
        /// <returns></returns>
        private async Task<ReportResult> AccommodationGroupByWorkStateAsync(List<Case> caseList, ReportResult result)
        {
            result.Name = "by Work State";
            var groupQuery = caseList.GroupBy(n => n.Employee.WorkState)
            .Select(g => new
            {
                label = g.Key,
                data = g.Select(x => x).ToList()
            }).OrderBy(m => m.label);

            List<ResultData> resultItems;

            result.Groups = new List<ResultDataGroup>();
            int i = 1;
            foreach (var t in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (var q in t.data.Where(c => c.AccommodationRequest != null))
                {
                    q.AccommodationRequest.Accommodations = q.AccommodationRequest.Accommodations ?? new List<Accommodation>(0);
                    string displayCaseNumber = q.CaseNumber;
                    string displayFullName = q.Employee.FullName;

                    foreach (var accom in q.AccommodationRequest.Accommodations.Where(a => a.Type != null).OrderBy(p => p.Type.Code))
                        foreach (var detail in accom.Usage.Where(u => u.StartDate.DateRangesOverLap(u.EndDate, result.Criteria.StartDate, result.Criteria.EndDate)).OrderBy(u => u.StartDate))
                            resultItems.Add(GetDetail(q, accom, detail, displayCaseNumber, displayFullName));

                    if (!q.AccommodationRequest.Accommodations.Any(a => a.Type != null))
                    {
                        resultItems.Add(GetBlankDetail(q, displayCaseNumber, displayFullName));
                    }
                }

                result.Groups.Add(new ResultDataGroup()
                {
                    Key = i,
                    Label = t.label,
                    Items = new List<ResultData>().AddRangeFluid(resultItems)
                });

                i++;
            }
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Accommodation Report without any group
        /// </summary>
        /// <param name="caseList">Case List</param>
        /// <param name="result">Report Result</param>
        /// <returns></returns>
        private async Task<ReportResult> AccommodationByNoGroupAsync(ReportResult result, List<Case> caseList)
        {
            result.Name = "Detail";
            result.Items= CreateListItems(result, caseList);
            return await Task.FromResult(result);
        }

        private List<ResultData> CreateListItems(ReportResult result, List<Case> caseList)
        {
            List<ResultData> resultItems = new List<ResultData>();
            foreach (var item in caseList.Where(c => c.AccommodationRequest != null))
            {
                item.AccommodationRequest.Accommodations = item.AccommodationRequest.Accommodations ?? new List<Accommodation>(0);
                string displayCaseNumber = item.CaseNumber;
                string displayFullName = item.Employee.FullName;

                foreach (var accommodationItem in item.AccommodationRequest.Accommodations.Where(a => a.Type != null).OrderBy(p => p.Type.Code))
                {
                    foreach (var detail in accommodationItem.Usage.Where(u => u.StartDate.DateRangesOverLap(u.EndDate, result.Criteria.StartDate, result.Criteria.EndDate)).OrderBy(u => u.StartDate))
                    {
                        resultItems.Add(GetDetail(item, accommodationItem, detail, displayCaseNumber, displayFullName));
                    }
                }
                if (!item.AccommodationRequest.Accommodations.Any(a => a.Type != null))
                {
                    resultItems.Add(GetBlankDetail(item, displayCaseNumber, displayFullName));
                }
            }

            return resultItems;
        }


        /// <summary>
        /// Set Accommodation data
        /// </summary>
        /// <param name="result"></param>
        /// <param name="queryList"></param>
        /// <param name="resultItems"></param>
        /// <param name="empLocationCase"></param>
        /// <param name="org"></param>
        private void SetAccommadationData(ReportResult result, List<Case> queryList, List<ResultData> resultItems, List<string> empLocationCase, EmployeeOrganization org)
        {
            foreach (Case td in queryList.Where(cs => cs.AccommodationRequest != null && cs.Employee.EmployeeNumber == org.EmployeeNumber && cs.CustomerId == org.CustomerId && cs.EmployerId == org.EmployerId))
            {
                if (!string.IsNullOrWhiteSpace(org.TypeCode) && empLocationCase != null && org.TypeCode.ToUpper().TrimEnd() == OrganizationType.OfficeLocationTypeCode.ToUpper().TrimEnd() && empLocationCase.Contains(td.Id.ToUpper()))
                {
                    continue;
                }

                td.AccommodationRequest.Accommodations = td.AccommodationRequest.Accommodations ?? new List<Accommodation>(0);
                string displayCaseNumber = td.CaseNumber;
                string displayFullName = td.Employee.FullName;

                foreach (var accom in td.AccommodationRequest.Accommodations.Where(a => a.Type != null).OrderBy(p => p.Type.Code))
                {
                    foreach (var detail in accom.Usage.Where(u => u.StartDate.DateRangesOverLap(u.EndDate, result.Criteria.StartDate, result.Criteria.EndDate)).OrderBy(u => u.StartDate))
                    {
                        resultItems.Add(GetDetail(td, accom, detail, displayCaseNumber, displayFullName));
                    }
                }
                if (!td.AccommodationRequest.Accommodations.Any(a => a.Type != null))
                {
                    resultItems.Add(GetBlankDetail(td, displayCaseNumber, displayFullName));
                }
                if (!string.IsNullOrWhiteSpace(org.TypeCode)
                    && empLocationCase != null
                    && org.TypeCode.ToUpper().TrimEnd() == OrganizationType.OfficeLocationTypeCode.ToUpper().TrimEnd()
                    && (!empLocationCase.Contains(td.Id.ToUpper())))
                {
                    empLocationCase.Add(td.Id.ToUpper());
                }
            }
        }

        /// <summary>
        /// Get the Unique Organization list
        /// </summary>
        /// <param name="queryList">Case List</param>
        /// <param name="grouptypeCode">Group Type Code</param>
        /// <returns></returns>
        private static List<EmployeeOrganization> GetEmployeeUniqueOrganization(ReportResult result, List<Case> queryList)
        {
            string grouptypeCode = null;
            if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "OrganizationTypeGrouping") != null)
            {
                grouptypeCode = result.Criteria.ValueOf<string>("OrganizationTypeGrouping");
            }
            List<string> customerIds = queryList.Select(cs => cs.CustomerId).Distinct().ToList();
            List<string> employerIds = queryList.Select(cs => cs.EmployerId).Distinct().ToList();
            List<string> employeeNumbers = queryList.Select(cs => cs.Employee.EmployeeNumber).Distinct().ToList();
            List<EmployeeOrganization> employeeOrganizations = null;
            if (grouptypeCode != null)
            {
                employeeOrganizations = EmployeeOrganization.AsQueryable().Where(org => org.TypeCode == grouptypeCode && customerIds.Contains(org.CustomerId) && employerIds.Contains(org.EmployerId) && employeeNumbers.Contains(org.EmployeeNumber)).OrderByDescending(o => o.Dates).ToList();
            }
            else
            {
                employeeOrganizations = EmployeeOrganization.AsQueryable().Where(org => customerIds.Contains(org.CustomerId) && employerIds.Contains(org.EmployerId) && employerIds.Contains(org.EmployerId) && employeeNumbers.Contains(org.EmployeeNumber)).OrderByDescending(o => o.Dates).ToList();
            }
            List<EmployeeOrganization> employeeUniqueOrganizations = new List<EmployeeOrganization>();
            foreach (EmployeeOrganization empOrg in employeeOrganizations)
            {
                if (empOrg.TypeCode != OrganizationType.OfficeLocationTypeCode ||
                    ((empOrg.Dates == null || empOrg.Dates.IncludesToday(true))
                    && (!employeeUniqueOrganizations.Any(empUniqueOrg => empUniqueOrg.TypeCode == OrganizationType.OfficeLocationTypeCode && empUniqueOrg.EmployeeNumber == empOrg.EmployeeNumber && empUniqueOrg.CustomerId == empOrg.CustomerId && empUniqueOrg.EmployerId == empOrg.EmployerId))))
                {
                    employeeUniqueOrganizations.Add(empOrg);
                }
            }

            return employeeUniqueOrganizations;
        }
        
        /// <summary>
        /// Get the Case close date
        /// </summary>
        /// <param name="theCase">Case</param>
        /// <returns>close date</returns>
        internal string GetCaseClosedDate(Case theCase)
        {
            var closedDate = theCase.GetClosedDate();
            return closedDate.HasValue ? closedDate.Value.ToShortDateString() : string.Empty;
        }

        /// <summary>
        /// Get the name of the person who approved the case
        /// </summary>
        /// <param name="theCase">Case</param>
        /// <returns>Name</returns>
        internal string GetCaseApprovedBy(Case theCase)
        {
            return theCase.AssignedToName;
        }

        /// <summary>
        /// Get the Additional questions details
        /// </summary>
        /// <param name="resultData"></param>
        /// <param name="steps"></param>
        internal void ReportAdditionalQuestions(ResultData resultData, ICollection<AccommodationInteractiveProcess.Step> steps)
        {
            foreach (var step in steps.Where(s => !string.IsNullOrWhiteSpace(s.ReportLabel)))
            {
                resultData[step.ReportLabel] = step.SAnswer ?? String.Empty;
            }
        }

        /// <summary>
        /// Create the Result data 
        /// </summary>
        /// <param name="theCase">Case</param>
        /// <param name="accommodation">Accommodation</param>
        /// <param name="detail">Accommodation details</param>
        /// <param name="displayCaseNumber">Case number to display in report</param>
        /// <param name="displayFullName">Full Name of the Employee</param>
        /// <returns></returns>
        private  ResultData GetDetail(Case theCase, Accommodation accommodation, AccommodationUsage detail, string displayCaseNumber, string displayFullName)
        {
            dynamic data = new ResultData();
            DateTime? displayRTW = GetCaseEventDate(theCase, CaseEventType.ReturnToWork);
            DateTime? displayERTW = GetCaseEventDate(theCase, CaseEventType.EstimatedReturnToWork);

            data.CaseNumber = string.IsNullOrWhiteSpace(displayCaseNumber) ? null : new LinkItem() { Value = theCase.CaseNumber, Link = string.Format("/Cases/{0}/View", theCase.Id) };
            data.EmployeeNumber = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = theCase.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", theCase.Employee.Id) };
            data.Name = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = theCase.Employee.FullName, Link = string.Format("/Employees/{0}/View", theCase.Employee.Id) };
            data.CaseStatus = theCase.Status.ToString().SplitCamelCaseString();
            data.Duration = accommodation.Duration == 0 ? "Pending" : accommodation.Duration.ToString().SplitCamelCaseString();
            data.Type = accommodation.Type.Name;
            data.Created = accommodation.CreatedDate.ToString("MM/dd/yyyy");
            data.StartDate = detail.StartDate == DateTime.MinValue ? "Pending" : detail.StartDate.ToString("MM/dd/yyyy");
            data.EndDate = detail.EndDate != null ? detail.EndDate.ToString("MM/dd/yyyy") : string.Empty;
            data.Accommodations = string.Concat(accommodation.Type.Code, " - ", detail.Determination.ToString().SplitCamelCaseString());
            data.AccommStatus = accommodation.Status.ToString().SplitCamelCaseString();
            data.AssignedTo = theCase.AssignedToName;
            data.WorkState = theCase.Employee.WorkState;
            data.Location = theCase.CurrentOfficeLocation ?? string.Empty;
            data.ReturnToWork = displayRTW != null ? displayRTW.ToString("MM/dd/yyyy") : string.Empty;
            data.EstimatedReturnToWork = displayERTW != null ? displayERTW.ToString("MM/dd/yyyy") : string.Empty;
            return data;
        }
        
        /// <summary>
        /// Get the data for blank details
        /// </summary>
        /// <param name="theCase">case</param>
        /// <param name="displayCaseNumber">Case number</param>
        /// <param name="displayFullName">Employee name</param>
        /// <returns>Result Data</returns>
        private ResultData GetBlankDetail(Case theCase,  string displayCaseNumber,  string displayFullName)
        {
            dynamic data = new ResultData();
            DateTime? displayRTW = GetCaseEventDate(theCase, CaseEventType.ReturnToWork);
            DateTime? displayERTW = GetCaseEventDate(theCase, CaseEventType.EstimatedReturnToWork);
            data.CaseNumber = string.IsNullOrWhiteSpace(displayCaseNumber) ? null : new LinkItem() { Value = theCase.CaseNumber, Link = string.Format("/Cases/{0}/View", theCase.Id) };
            data.EmployeeNumber = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = theCase.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", theCase.Employee.Id) };
            data.Name = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = theCase.Employee.FullName, Link = string.Format("/Employees/{0}/View", theCase.Employee.Id) };
            data.Duration = theCase.AccommodationRequest.SummaryDurationType;
            data.Type = "N/A";
            data.CaseStatus = theCase.Status.ToString().SplitCamelCaseString();
            var evt = theCase.FindCaseEvent(CaseEventType.CaseCreated);
            data.Created = (evt == null ? theCase.CreatedDate : evt.EventDate).ToString("MM/dd/yyyy");
            data.StartDate = theCase.StartDate.ToString("MM/dd/yyyy");
            data.EndDate = theCase.EndDate != null ? theCase.EndDate.ToString("MM/dd/yyyy") : string.Empty;
            data.Accommodations = "N/A";
            data.AccommStatus = "N/A";
            data.AssignedTo = theCase.AssignedToName;
            data.WorkState = theCase.Employee.WorkState;           
            data.Location = theCase.CurrentOfficeLocation ?? string.Empty;
            data.ReturnToWork = displayRTW != null ? displayRTW.ToString("MM/dd/yyyy") : string.Empty;
            data.EstimatedReturnToWork = displayERTW != null ? displayERTW.ToString("MM/dd/yyyy") : string.Empty;           
            return data;
        }
    }
}
