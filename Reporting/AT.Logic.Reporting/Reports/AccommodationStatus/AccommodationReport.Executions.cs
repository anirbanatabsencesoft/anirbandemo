﻿using AT.Data.Reporting.Model;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.AccommodationStatus
{
    /// <summary>
    /// Base report for Accommodation report
    /// </summary>
    public partial class AccommodationReport : BaseReport
    {        
        /// <summary>
        /// Get Report criteria for Accommodation report.
        /// </summary>
        /// <param name="user">Current User</param>
        /// <returns>Report criteria</returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            UserReportCriteriaData userReportCriteriaData = reportingLogic.GetUserReportCriteriaDataAsync().GetAwaiter().GetResult();

            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerKey,
                StartDate = DateTime.UtcNow.Date.AddDays(-30),
                EndDate = DateTime.UtcNow.Date
            };

            ReportCriteriaItem item = GetCaseStatusCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetAccomTypeCriteria(userReportCriteriaData);
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetAdjudicationCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            criteria.Filters.Add(GetCaseAssigneeCriteria(userReportCriteriaData));
            criteria.Filters.Add(GetWorkStateCriteria(userReportCriteriaData, criteria.StartDate, criteria.EndDate));

            criteria.Filters.AddRange(GetOrganizationTypeCriteria(userReportCriteriaData));
            if (IncludeOrganizationGrouping)
            {
                criteria.Filters.Add(GetOrganizationTypeGroupByCriteria(userReportCriteriaData));
            }
            return criteria;
        }


        /// <summary>
        /// Build and Run the query.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="groupBy"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <param name="groupByColumns"></param>
        /// <returns></returns>
        protected override async Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? groupBy, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            var caseList = reportingLogic.RunAccomodationReportQueryAsync(result.Criteria, EmployerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc).GetAwaiter().GetResult();           
            if (groupBy != null)
            {
                IsGrouped = true;
                switch (groupBy)
                {
                    case ReportGroupType.Custom:
                        {
                            return await AccommodationGroupByCustomAsync(caseList, result, groupByColumns);
                        }
                    case ReportGroupType.WorkState:
                        {
                            return await AccommodationGroupByWorkStateAsync(caseList, result);                            
                        }
                    case ReportGroupType.CaseManager:
                        {
                            return await AccommodationGroupByCaseManagerAsync(caseList, result);                            
                        }
                    case ReportGroupType.Duration:
                        {
                            return await AccommodationGroupByDurationAsync(caseList, result);                            
                        }
                    case ReportGroupType.Organization:
                        {
                            return await AccommodationGroupByOrganizationAsync(caseList, result);                          
                        }
                    case ReportGroupType.Location:
                        {
                            return await AccommodationGroupByLocationAsync(caseList, result);
                           
                        }
                    case ReportGroupType.AccommodationType:
                        {
                            return await AccommodationGroupByTypeAsync(caseList, result);                          
                        }
                    default:
                        {
                            throw new ArgumentNullException("groupBy", "Group by data is not provided");
                        }
                }
            }
            else
            {
                IsGrouped = false;
                return await AccommodationByNoGroupAsync(result, caseList);
            }
        }
    }
}
