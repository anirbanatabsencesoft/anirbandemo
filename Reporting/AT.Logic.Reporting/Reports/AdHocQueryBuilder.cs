﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Reporting;
using AbsenceSoft.Data.Security;

namespace AT.Logic.Reporting.Reports
{
    /// <summary>
    /// This class builds a sql query and paramaters from a Report Defintion wrapping the initial query in a format operation to prevent sql injection.
    /// Added more of a Template Method design pattern approach to this class. To utilize the class as such make it non-static and make all the static
    /// Build* named methods, virtual protected instance methods and make AdHocQueryBuilder class abstract, deriving off types as needed. The Build
    /// method itself would be the main template method design pattern driver. Format function makes this class sql injection safe as well as passing
    /// everything in as parameters. Check out both definitions for Postgres format():
    /// https://www.postgresql.org/docs/9.1/static/functions-string.html
    /// https://www.postgresql.org/docs/9.3/static/functions-string.html.
    /// The format function is similar to the C function sprintf, but only the following conversion specifications are recognized:
    /// %s interpolates the corresponding argument as a string.
    /// %I escapes its argument as an SQL identifier.
    /// %L escapes its argument as an SQL literal.
    /// %% outputs a literal %.
    /// A conversion can reference an explicit parameter position by preceding the conversion specifier with n$, where n is the argument position.
    /// </summary>
    public static class AdHocQueryBuilder
    {
        /// <summary>
        /// Builds a sql query and paramaters from a Report Defintion wrapping the initial query in a format operation to prevent sql injection.
        /// The resulting query must be run against Postgres database first with the parameters. The first pass to Posgres formats the query and
        /// returns another query named 'query' in a result row that is the acutal query to run and get your results from Postgres. This is
        /// done in this fashion to prevent sql injection on the dynamically built queries. Run the second formatted query without passing any
        /// arguments into the driver as the first pass formats all the paramaters into the query for the second pass.
        /// </summary>
        /// <param name="reportDefinition">The definition of the report to build.</param>
        /// <param name="reportFields">The list of report fields.</param>
        /// <param name="user">The user building the query.</param>
        /// <param name="inherentLimit">Used to limit the rows of the report, if an offset is not speicified in the report definition. Skipped if null, the default.</param>
        /// <param name="inherentOffset">Used to limit the rows of the report, if an offset is not speicified in the report definition. Skipped if null, the default.</param>
        /// <returns>Returns an AdHocQueryBuilderResult which contains the sql query and paramaters.</returns>
        public static AdHocQueryBuilderResult Build(
            ReportDefinition reportDefinition,
            List<ReportField> reportFields,
            User user,
            int? inherentLimit = null,
            int? inherentOffset = null)
        {
            // Validate the parameters for the query builder.
            ValidateParameters(reportDefinition, reportFields, user);

            // Get a distinct list of field name mappings from the report fields list.
            var reportFieldsByName = new Dictionary<string, ReportField>();
            GetDistinctReportFieldMappings(reportFields, reportFieldsByName);

            // Get a list of fields that exist in the report definition, and that did not get filtered out by any security filters.
            var relatedFields = reportDefinition.Fields.Where(p => reportFieldsByName.ContainsKey(p.Name)).ToList();
            if (!relatedFields.Any())
            {
                return new AdHocQueryBuilderResult
                {
                    Query = "select format('select %L as %I', 'No fields selected for report.', 'Result') as query;"
                };
            }
            
            // Start a list of arguments.
            var args = new List<object>(); 

            // Create a StringBuilder object ot build the query with a format function to prevent SQL injection.
            var builder = new StringBuilder(1024);

            // Build the select statement for the format function including the inital selection of all column names
            // with their user friendly display names for aliases.
            BuildSelectStatement(relatedFields, args, builder, reportFieldsByName);
            
            // Build the from statement for the sql query.
            BuildFromStatement(reportDefinition, args, builder);

            // Build the report filters. Add filters by customer id and/or employer id(s) where they exist.
            BuildReportFilters(reportDefinition, user, args, builder, reportFieldsByName);

            // Build the order by statement if one is needed.
            BuildOrderStatement(relatedFields, args, builder);

            // Build the limit and offset if provided.
            BuildLimitAndOffset(builder, inherentLimit, inherentOffset);

            // Build the report parameters and format them in the sql query.
            BuildReportParameters(args, builder);

            return new AdHocQueryBuilderResult
            {
                Query = builder.ToString(),
                Arguments = args
            };
        }

        /// <summary>
        /// Validates the parameters for the query builder.
        /// </summary>
        /// <param name="reportDefinition">The report definition.</param>
        /// <param name="reportFields">The list of report fields.</param>
        /// <param name="user">The user building the query.</param>
        private static void ValidateParameters(
            ReportDefinition reportDefinition,
            List<ReportField> reportFields,
            User user)
        {
            if (reportDefinition == null)
            {
                throw new ArgumentNullException("reportDefinition");
            }

            if (reportFields == null)
            {
                throw new ArgumentNullException("reportFields");
            }

            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
        }

        /// <summary>
        /// Gets a distinct list of field name mappings from the report fields list.
        /// The views in postgres have matching display names in some cases and they must be filtered out.
        /// </summary>
        /// <param name="reportFields">The list of report fields.</param>
        /// <param name="reportFieldsByName">The dictionary to populate with report field mappings by name.</param>
        private static void GetDistinctReportFieldMappings(
            List<ReportField> reportFields,
            Dictionary<string, ReportField> reportFieldsByName)
        {
            // Create a HashSet to hold display names to guarantee uniqueness.
            // DynamicAwesome will not work otherwise as it operates as a dictionary internally.
            var reportFieldDisplayNames = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
            var reportFieldCount = reportFields.Count;
            for (var i = 0; i < reportFieldCount; i++)
            {
                var reportField = reportFields[i];
                if (reportFieldDisplayNames.Contains(reportField.Display))
                {
                    continue;
                }

                // Populate the HashSet to work as a filter against display names.
                reportFieldDisplayNames.Add(reportField.Display);
                // Populate the dictionary of report fields by name mapping.
                reportFieldsByName.Add(reportField.Name, reportField);
            }
        }

        /// <summary>
        /// Builds the select statement for the format function including the inital selection of all column names
        /// with their user friendly display names for aliases.
        /// </summary>
        /// <param name="relatedFields">A list of fields that exist in the report definition, and that did not get filtered out by any security filters.</param>
        /// <param name="args">The arguments to build up for the query.</param>
        /// <param name="builder">The query text string builder instance.</param>
        /// <param name="reportFieldsByName">The dictionary of field mappings.</param>
        private static void BuildSelectStatement(
            List<SavedReportField> relatedFields,
            List<object> args,
            StringBuilder builder,
            Dictionary<string, ReportField> reportFieldsByName)
        {
            // Add the arguments by adding the report field name and then the report field display name as the alias.
            args.AddRange(
                relatedFields.SelectMany(
                    p =>
                    {
                        var reportField = reportFieldsByName[p.Name];
                        return new object[]
                        { 
                            GetNameFormatExpression(reportField), 
                            reportField.Display
                        };
                    }));

            // Start the select statement in the builder with the format function.
            builder.Append("select format('select ");

            // Add all the report field columns with their friendly display names for aliases to the StringBuilder.
            builder.Append(string.Join(", ", relatedFields.Select(p => "%s as %I")));
        }

        /// <summary>
        /// Gets the name format exppression.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <returns></returns>
        private static string GetNameFormatExpression(ReportField field)
        {
            if (field == null)
            {
                return null;
            }

            string expression = field.Name;
            string castType = GetCastTypeForStringLiteral(field);
            string format = null;

            if (field.DataType == DbDataType.Date || field.DataType == DbDataType.DateTime || field.DataType == DbDataType.SmallDateTime)
            {
                format = string.IsNullOrWhiteSpace(field.Format) ? "MM/DD/YYYY" : field.Format;
            }
            else if (!string.IsNullOrWhiteSpace(field.Format))
            {
                format = field.Format;
            }

            if (!string.IsNullOrWhiteSpace(format))
            {
                expression = string.Format("to_char({0}{2}, '{1}')", field.Name, format, castType);
            }

            if (field.Translate == true)
            {
                expression = string.Format("translate({0}, ' ', '')", expression);
            }

            return expression;
        }

        /// <summary>
        /// Build the from statement for the sql query.
        /// </summary>
        /// <param name="reportDefinition">The report definition.</param>
        /// <param name="args">The arguments to build up for the query.</param>
        /// <param name="builder">The query text string builder instance.</param>
        private static void BuildFromStatement(
            ReportDefinition reportDefinition,
            List<object> args,
            StringBuilder builder)
        {
            args.Add(reportDefinition.ReportType.Key);
            builder.Append(" from %I");
        }

        /// <summary>
        /// Build the report filters. Add filters by customer id and/or employer id(s) where they exist.
        /// </summary>
        /// <param name="reportDefinition">The report definition.</param>
        /// <param name="user">The user building the query.</param>
        /// <param name="args">The arguments to build up for the query.</param>
        /// <param name="builder">The query text string builder instance.</param>
        /// <param name="reportFieldsByName">The dictionary of field mappings.</param>
        private static void BuildReportFilters(
            ReportDefinition reportDefinition,
            User user,
            List<object> args,
            StringBuilder builder,
            Dictionary<string, ReportField> reportFieldsByName)
        {
            var isCustomer = reportDefinition.ReportType.IsCustomer;
            var isEmployer = reportDefinition.ReportType.IsEmployer;
            var employerIds = new List<string>();
            if (user.Employers != null)
            {
                employerIds = user.Employers.Select(p => p.EmployerId).ToList();
            }
            var reportFilters = reportDefinition.Filters;

            if (isCustomer && isEmployer)
            {
                // Add cust_id, eplr_id, and report filters.
                AppendCustomerFilter(user.CustomerId, args, builder);
                AppendEmployerFilters(employerIds, args, builder);
                AppendReportFilters(reportFilters, args, builder, reportFieldsByName);
            }
            else if (isCustomer)
            {
                // Add cust_id and report filters.
                AppendCustomerFilter(user.CustomerId, args, builder);
                AppendReportFilters(reportFilters, args, builder, reportFieldsByName);
            }
            else if (isEmployer)
            {
                // Add eplr_id and report filters.
                AppendEmployerFilters(employerIds, args, builder, true);
                AppendReportFilters(reportFilters, args, builder, reportFieldsByName);
            }
            else
            {
                // Add report filters.
                AppendReportFilters(reportFilters, args, builder, reportFieldsByName, true);
            }
        }

        /// <summary>
        /// Builds the order by statement if one is needed.
        /// </summary>
        /// <param name="relatedFields">A list of fields that exist in the report definition, and that did not get filtered out by any security filters.</param>
        /// <param name="args">The arguments to build up for the query.</param>
        /// <param name="builder">The query text string builder instance.</param>
        private static void BuildOrderStatement(
            List<SavedReportField> relatedFields,
            List<object> args,
            StringBuilder builder)
        {
            // Order the fields by the Order property.
            var orderFields = relatedFields.Where(p => p.Order != null).OrderBy(p => p.Order).Select(p => p.Name).ToList();

            // If there are no fields that have a specified order, return.
            if (!orderFields.Any())
            {
                return;
            }

            // Add ordered fields to the parameters list.
            args.AddRange(orderFields);

            // Start the order by statement in the builder.
            builder.Append(" order by ");

            // Add all the report field columns to order.
            builder.Append(string.Join(", ", orderFields.Select(p => "%s")));
        }

        /// <summary>
        /// Build the report limit and offset from the report definition.
        /// Use the inherent limit and offsets if none are provided in the report definition.
        /// </summary>
        /// <param name="builder">The query text string builder instance.</param>
        /// <param name="inherentLimit">Used to limit the rows of the report, if an offset is not speicified in the report definition. Skipped if null, the default.</param>
        /// <param name="inherentOffset">Used to limit the rows of the report, if an offset is not speicified in the report definition. Skipped if null, the default.</param>
        private static void BuildLimitAndOffset(
            StringBuilder builder,
            int? inherentLimit = null,
            int? inherentOffset = null)
        {
            // Currently the report definition has no orderng and so at this time,
            // it will remain as a commented out parameter. In the future,
            // these inherent parameters should be ignored all together if
            // the report definition already has limit and offset specified.
            if (inherentLimit != null)
            {
                builder.AppendFormat(" limit {0}", inherentLimit);
            }

            if (inherentOffset != null)
            {
                builder.AppendFormat(" offset {0}", inherentOffset);
            }
        }

        /// <summary>
        /// Build the report parameters and format them in the sql query.
        /// </summary>
        /// <param name="args">The arguments to build up for the query.</param>
        /// <param name="builder">The query text string builder instance.</param>
        private static void BuildReportParameters(
            List<object> args,
            StringBuilder builder)
        {
            builder.Append("', variadic array[");
            var index = 0;
            builder.Append(string.Join(", ", args.Select(p => string.Format("@{0}", index++))));
            builder.Append("]) as query;");
        }
        
        /// <summary>
        /// Appends a customer filter to the sql query for the ad-hoc report.
        /// </summary>
        /// <param name="customerId">The customerId to run the filter on.</param>
        /// <param name="args">The arguments to build up for the query.</param>
        /// <param name="builder">The query text string builder instance.</param>
        private static void AppendCustomerFilter(
            string customerId,
            List<object> args,
            StringBuilder builder)
        {
            args.Add("cust_id");
            args.Add(customerId);
            builder.Append(" where %s = %L");
        }

        /// <summary>
        /// Appends an employer filter or filters to the sql query for the ad-hoc report.
        /// </summary>
        /// <param name="employerIds">A collection of employer Ids to filter for.</param>
        /// <param name="args">The arguments to build up for the query.</param>
        /// <param name="builder">The query text string builder instance.</param>
        /// <param name="addWhereClause">Append the builder with a where clause or and clause.</param>
        private static void AppendEmployerFilters(
            IReadOnlyCollection<string> employerIds,
            List<object> args,
            StringBuilder builder, bool addWhereClause = false)
        {
            builder.AppendFormat(" {0} ", addWhereClause ? "where" : "and");

            switch (employerIds.Count)
            {
                case 0:
                    args.Add("eplr_id");
                    builder.Append("%I is null");
                    return;
                case 1:
                    args.Add("eplr_id");
                    args.Add("eplr_id");
                    args.AddRange(employerIds); // only adds one
                    builder.Append("(%I is null or %I = %L)");
                    return;
                default:
                    args.Add("eplr_id");
                    args.Add("eplr_id");
                    builder.Append("(%I is null or %I in (");
                    args.AddRange(employerIds);
                    builder.Append(string.Join(", ", employerIds.Select(p => "%L")));
                    builder.Append("))");
                    return;
            }
        }

        /// <summary>
        /// Appends a list of report filters to the sql query for the ad-hoc report.
        /// </summary>
        /// <param name="reportFilters">A list of ReportFilter to pull filter definitions for.</param>
        /// <param name="args">The arguments to build up for the query.</param>
        /// <param name="builder">The query text string builder instance.</param>
        /// <param name="reportFieldsByName">A dictionary of column name to report field mappings.</param>
        /// <param name="addWhereClause">Append the builder with a where clause or and clause.</param>
        private static void AppendReportFilters(
            List<ReportFilter> reportFilters,
            List<object> args,
            StringBuilder builder,
            IReadOnlyDictionary<string, ReportField> reportFieldsByName,
            bool addWhereClause = false)
        {
            if (reportFilters == null || !reportFilters.Any())
            {
                return;
            }

            builder.AppendFormat(" {0} ", addWhereClause ? "where" : "and");
            args.AddRange(reportFilters.SelectMany(p => new[] { p.FieldName, Convert.ToString(p.Value) }));
            builder.Append(string.Join(" and ",
                reportFilters.Select(
                    p => string.Format("%s{0}", BuildOperatorExpression(reportFieldsByName[p.FieldName], p)))));
        }

        /// <summary>
        /// Builds an operator portion of the query for the where clause of a sql query, from the report field and filter.
        /// </summary>
        /// <param name="reportField">The report field to format.</param>
        /// <param name="reportFilter">The filter for the report field.</param>
        /// <returns></returns>
        private static string BuildOperatorExpression(
            ReportField reportField,
            ReportFilter reportFilter)
        {
            // Contains
            if (StringComparer.InvariantCultureIgnoreCase.Compare(reportFilter.Operator, "~~") == 0)
            {
                return " ilike ''%%%s%%''";
            }

            // Begins With
            if (StringComparer.InvariantCultureIgnoreCase.Compare(reportFilter.Operator, "~|") == 0)
            {
                return " ilike ''%s%%''";
            }

            // Ends With
            if (StringComparer.InvariantCultureIgnoreCase.Compare(reportFilter.Operator, "|~") == 0)
            {
                return " ilike ''%%%s''";
            }

            // Otherwise
            return string.Format("{1} {0} %L{1}", reportFilter.Operator, GetCastTypeForStringLiteral(reportField));
        }

        /// <summary>
        /// Gets the cast type of a field for a report field if it needs to be casted.
        /// </summary>
        /// <param name="reportField">The report field to cast.</param>
        /// <returns></returns>
        private static string GetCastTypeForStringLiteral(ReportField reportField)
        {
            // Unfortunately we have to cast back out from a string literal to prevent sql injection from the dynamic sql query.
            switch (reportField.DataType)
            {
                case DbDataType.Bit:
                case DbDataType.Binary:
                    return "::boolean";// Note we should only be using this for boolean
                case DbDataType.Int:
                case DbDataType.TinyInt:
                    return "::int";
                case DbDataType.BigInt:
                    return "::bigint";
                case DbDataType.SmallInt:
                    return "::smallint";
                case DbDataType.Double:
                    return "::float8";
                case DbDataType.Numeric:
                    return "::numeric";
                case DbDataType.Decimal:
                    return "::decimal";
                case DbDataType.Date:
                case DbDataType.DateTime:
                case DbDataType.SmallDateTime:
                    return "::date";
                case DbDataType.Real:
                    return "::real";
                case DbDataType.Time:
                    return "::time";
                case DbDataType.Timestamp:
                    return "::timestamp";
                case DbDataType.UniqueIdentifier:
                    return "::uuid";
                
                default:
                    return string.Empty;
            }
        }

        
    }
}
