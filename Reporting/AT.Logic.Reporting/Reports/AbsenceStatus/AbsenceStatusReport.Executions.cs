﻿using AbsenceSoft.Data.Enums;
using AT.Data.Reporting.Model;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.AbsenceStatus
{
    /// <summary>
    /// Base class for Absence Status Report.
    /// </summary>
    [Serializable]
    public partial class AbsenceStatusReport : BaseReport
    {
        /// <summary>   
        /// Get the report criteria for specified user
        /// </summary>
        /// <param name="user">Current User</param>
        /// <returns>Returns the criteria</returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            UserReportCriteriaData userReportCriteriaData = reportingLogic.GetUserReportCriteriaDataAsync().GetAwaiter().GetResult();

            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerKey,
                StartDate = DateTime.UtcNow.Date.AddDays(-30),
                EndDate = DateTime.UtcNow.Date
            };

            ReportCriteriaItem item = GetCaseStatusCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetAbsenceReasonCriteria(userReportCriteriaData);
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetCaseTypeCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetAdjudicationCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            criteria.Filters.Add(GetWorkStateCriteria(userReportCriteriaData, criteria.StartDate, criteria.EndDate));

            criteria.Filters.AddRange(GetOrganizationTypeCriteria(userReportCriteriaData));
            if (IncludeOrganizationGrouping)
            {
                criteria.Filters.Add(GetOrganizationTypeGroupByCriteria(userReportCriteriaData));
            }
            return criteria;
        }

        /// <summary>
        /// Build and run the query to get the report Data
        /// </summary>
        /// <param name="user"></param>
        /// <param name="result"></param>
        /// <param name="groupBy"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <param name="groupByColumns"></param>
        /// <returns></returns>
        protected override async Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? groupBy, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);            
            var caseList = reportingLogic.RunAbsenceReportQueryAsync(result.Criteria, EmployerIdFilterName, pageNumber, pageSize, sortBy, orderByAsc).GetAwaiter().GetResult();
            var customer = reportingLogic.GetUserReportCriteriaDataAsync().GetAwaiter().GetResult().Customer;
            var isTpa = customer.HasFeature(Feature.MultiEmployerAccess) && user.Employers.Count > 1;
            if (groupBy != null)
            {
                IsGrouped = true;
                switch (groupBy)
                {
                    case ReportGroupType.Custom:
                        {
                            return await AbsenceGroupByCustomAsync(caseList, result, groupByColumns, isTpa);
                        }
                    case ReportGroupType.WorkState:
                        {
                            return await AbsenceGroupByWorkStateAsync(caseList, result);                           
                        }
                    case ReportGroupType.Reason:
                        {
                            return await AbsenceGroupByReasonAsync(caseList, result);                            
                        }
                    case ReportGroupType.Policy:
                        {
                            return await AbsenceGroupByPolicyAsync(user, caseList, result);                            
                        }
                    case ReportGroupType.Organization:
                        {
                            return await AbsenceGroupByOrganizationAsync(user, caseList, result, isTpa);
                            
                        }
                    case ReportGroupType.Location:
                        {
                            return await AbsenceGroupByLocationAsync(caseList, result);
                            
                        }
                    case ReportGroupType.Leave:
                        {
                            return await AbsenceGroupByLeaveAsync(caseList, result);                           
                        }
                    default:
                        {
                            throw new ArgumentNullException("groupBy", "Group by data is not provided");
                        }
                }

            }
            else
            {
                IsGrouped = false;
                return await AbsenceByNoGroupAsync(result, caseList,isTpa);
            }
        }

    }
}
