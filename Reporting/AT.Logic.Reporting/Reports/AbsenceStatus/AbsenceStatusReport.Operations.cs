﻿using AbsenceSoft;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Classes;
using AT.Entities.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports.AbsenceStatus
{
    public partial class AbsenceStatusReport : BaseReport
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public AbsenceStatusReport()
        {
            MainCategory = "Operations Report";
            Name = "Absence Status Report";
            Category = "Absence Status Report";
        }
       
        /// <summary>
        /// Id of the report
        /// </summary>
        public override string Id { get { return "955E6AC2-4C2C-4cb5-A6B7-DF4CCD3BC252"; } }
        /// <summary>
        /// Category for all Absence Status reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get; set; }
        public override string ReportDescription
        {
            get { return "Case list provides high level details about each case"; }
        }

        /// <summary>
        /// Name of the Report.
        /// </summary>
        public override string Name { get; set; }

        /// <summary>
        /// Category for all Absence Status reports is "Absence Status Report".
        /// </summary>
        public override string Category { get; set; }

        /// <summary>
        /// Icon to set for Absence Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

       
        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get; set; }

        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns>LOA Feature</returns>
        public override IEnumerable<AbsenceSoft.Data.Enums.Feature> RequiredFeatures()
        {
            yield return AbsenceSoft.Data.Enums.Feature.LOA;
        }

        /// <summary>
        /// List of fields for grouping
        /// </summary>
        public override Dictionary<string, List<string>> AvailableGroupingFields => new Dictionary<string, List<string>>(){
            {(ReportGroupType.None).ToString(), new List<string>(0)},
            {(ReportGroupType.Leave).ToString(), new List<string>(0)},
            {(ReportGroupType.WorkState).ToString(), new List<string>(0)},
            {(ReportGroupType.Location).ToString(), new List<string>(0)},
            {(ReportGroupType.Policy).ToString(), new List<string>(0)},
            {(ReportGroupType.Reason).ToString(), new List<string>(0)},
            {(ReportGroupType.Organization).ToString(), new List<string>(0)},
            {(ReportGroupType.Custom).ToString(), new List<string>(){
                       "CaseNumber",
                       "Status",
                       "EmployerName",
                       "RelatedCaseType",
                       "CaseType",
                       "EmployerId",
                       "CurrentOfficeLocation",
                       "CustomerId",
                       "AssignedToId",
                       "AssignedToName",
                       "cby",
                       "Employee.EmployeeNumber",
                        "Employee.FullName",
                        "Employee.Status",
                        "Reason.Name",
                        "Reason.CaseTypes" }}
        };

        /// <summary>
        /// To be used for ESS category
        /// </summary>
        public override string ESSCategoryTitle { get { return "Absence Status Reports"; } }

        /// <summary>
        /// Determine id report is grouped 
        /// </summary>
        public override bool IsGrouped { get; set; }

        /// <summary>
        /// Get the report without any group
        /// </summary>
        /// <param name="result"></param>
        /// <param name="caseList"></param>
        /// <param name="resultItems"></param>
        /// <param name="isTpa"></param>
        private async Task<ReportResult> AbsenceByNoGroupAsync(ReportResult result, List<Case> caseList, bool isTpa)
        {
            result.Name = "Detail";
            List<ResultData> resultItems = new List<ResultData>();
            foreach (var caseItem in caseList)
            {
                CreateCaseItem(resultItems, caseItem, isTpa);
            }
            result.Items = resultItems;
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Run the report with custom grouping
        /// </summary>
        /// <param name="cases"></param>
        /// <param name="result"></param>
        /// <param name="groupByColumns"></param>
        /// <returns></returns>
        private async Task<ReportResult> AbsenceGroupByCustomAsync(IList<Case> cases, ReportResult result, List<string> groupByColumns, bool isTpa)
        {
            result.Name = "Custom grouping";
            var groupQuery = cases.GroupBy(groupByColumns.ToArray());        
            result.JsonData = CreateJsonResult(groupQuery, isTpa);
            return await Task.FromResult(result);
        }


        /// <summary>
        /// Create custom Grouping
        /// </summary>
        /// <param name="groupQuery"></param>
        /// <param name="isTpa"></param>
        /// <returns></returns>
        private dynamic CreateJsonResult(List<GroupResult> groupQuery, bool isTpa)
        {
            List<ResultData> resultItems;
            var dataGroup = new List<dynamic>();
            if (groupQuery == null || !groupQuery.Any())
            {
                return dataGroup;
            }
            foreach (var queryItem in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (Case theCase in queryItem.Items)
                {
                    CreateCaseItem(resultItems, theCase, isTpa);
                }
                dataGroup.Add(new
                {
                    Label = queryItem.Value,
                    queryItem.Key,
                    Items = new List<ResultData>().AddRangeFluid(resultItems),
                    SubGroups = CreateJsonResult(queryItem.SubGroups, isTpa)
                });              
            }          
            return dataGroup;
        }

        /// <summary>
        /// Get the result Group by Work state
        /// </summary>
        /// <param name="cases">case list</param>
        /// <param name="result">Report result</param>
        private async Task<ReportResult> AbsenceGroupByWorkStateAsync(IList<Case> cases, ReportResult result)
        {
            result.Name = "by Work State";
            var groupQuery = cases.GroupBy(n => n.Employee.WorkState)
                .Select(g => new
                {
                    label = g.Key,
                    data = g.Select(x => x).ToList()
                }).OrderBy(m => m.label);
            return await Task.FromResult(CreateResult(groupQuery, result));
        }

        /// <summary>
        /// Get the result Group by Location
        /// </summary>
        /// <param name="cases">case list</param>
        /// <param name="result">Report result</param>
        public async Task<ReportResult> AbsenceGroupByLocationAsync(IList<Case> cases, ReportResult result)
        {
            result.Name = "by Office Location";
            var groupQuery = cases.GroupBy(n => n.CurrentOfficeLocation ?? "None")
                .Select(g => new
                {
                    label = g.Key,
                    data = g.Select(x => x).ToList()
                }).OrderBy(m => m.label);

            return await Task.FromResult(CreateResult(groupQuery, result));
        }

        /// <summary>
        /// Get the result group by Leave
        /// </summary>
        /// <param name="cases">case list</param>
        /// <param name="result">Report result</param>
        public async Task<ReportResult> AbsenceGroupByLeaveAsync(IList<Case> query, ReportResult result)
        {
            result.Name = "by Leave Type";
            var groupQuery = query.GroupBy(n => n.CaseType)
                .Select(g => new
                {
                    label = g.Key,
                    data = g.Select(x => x).ToList()
                }).OrderBy(m => m.label);
            return await Task.FromResult(CreateResult(groupQuery, result));
        }

        /// <summary>
        /// Get the result group by reason
        /// </summary>
        /// <param name="cases"></param>
        /// <param name="result"></param>
        public async Task<ReportResult> AbsenceGroupByReasonAsync(IList<Case> cases, ReportResult result)
        {
            result.Name = "by Absence Reason";
            var groupQuery = cases.GroupBy(n => n.Reason.Name)
                .Select(g => new
                {
                    label = g.Key,
                    data = g.Select(x => x).ToList()
                }).OrderBy(m => m.label);
            return await Task.FromResult(CreateResult(groupQuery, result));
        }

        /// <summary>
        /// Get the result group by policy
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cases"></param>
        /// <param name="result"></param>
        public async Task<ReportResult> AbsenceGroupByPolicyAsync(User user, IList<Case> cases, ReportResult result)
        {
            result.Name = "by Policy";
            ReportingLogic reportingLogic = new ReportingLogic(user);
            List<PolicySummary> policySummary;
            var ReportDatabyPolicy = new List<dynamic>();
            foreach (var item in cases)
            {
                policySummary = reportingLogic.GetEmployeePolicySummaryByCaseIdAsync(item.Id, null, null).GetAwaiter().GetResult();
                var caseAppliedPolicies = (from p1 in item.Summary.Policies
                                           join p2 in policySummary on p1.PolicyCode equals p2.PolicyCode
                                           select p2);
                GetDataByPolicy(ReportDatabyPolicy, item, caseAppliedPolicies);
            }

            var groupQuery = ReportDatabyPolicy.GroupBy(n => n.PolicyName)
                .Select(g => new
                {
                    label = g.Key,
                    data = g.Select(x => x).ToList()
                }).OrderBy(m => m.label);

            List<ResultData> resultItems;
            result.Groups = new List<ResultDataGroup>();
            int i = 1;
            foreach (var grp in groupQuery)
            {
                resultItems = new List<ResultData>();

                string prevCaseNumber = string.Empty;

                foreach (var theCase in grp.data)
                {
                    dynamic data = new ResultData();
                    if (prevCaseNumber != theCase.CaseNumber.Value.ToString())
                    {
                        data.CaseNumber = theCase.CaseNumber;
                        prevCaseNumber = theCase.CaseNumber.Value.ToString();
                    }
                    data.EmployeeNumber = theCase.EmployeeNumber;
                    data.Name = theCase.EmployeeName;
                    data.Reason = theCase.Reason;
                    data.LeaveType = theCase.LeaveType;
                    data.StartDate = theCase.StartDate.ToString("MM/dd/yyyy");
                    data.EndDate = theCase.EndDate.ToString("MM/dd/yyyy");
                    data.Status = theCase.Status;
                    data.Determination = theCase.Determination;
                    data.TimeUsed = theCase.TimeUsed;
                    data.TimeAvailable = theCase.TimeAvailable;
                    resultItems.Add(data);

                    if (String.IsNullOrWhiteSpace(prevCaseNumber))
                    {
                        prevCaseNumber = theCase.CaseNumber.Value.ToString();
                    }
                }

                result.Groups.Add(new ResultDataGroup()
                {
                    Key = i,
                    Label = grp.label,
                    Items = new List<ResultData>().AddRangeFluid(resultItems)
                });

                i++;
            }
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Get the result by Organization
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cases"></param>
        /// <param name="result"></param>
        /// <param name="isTpa"></param>
        public async Task<ReportResult> AbsenceGroupByOrganizationAsync(User user, IList<Case> cases, ReportResult result, bool isTpa)
        {
            result.Name = "by Organization";
            List<EmployeeOrganization> employeeUniqueOrganizations = GetEmployeeOrganizations(cases, result);
            var groupQuery = employeeUniqueOrganizations.GroupBy(org => new { org.CustomerId, org.EmployerId, org.Code }).Select(g => new
            {
                label = employeeUniqueOrganizations.FirstOrDefault(empOrgs => empOrgs.Code == g.Key.Code).Name,
                data = g.Select(x => x).ToList()
            }).OrderBy(m => m.label);

            List<ResultData> resultItems;
            result.Groups = new List<ResultDataGroup>();
            int i = 1;
            List<string> empLocationCase = new List<string>();
            foreach (var empOrgItem in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (var org in empOrgItem.data)
                {
                    SetOrganizationGroupData(user, cases, isTpa, resultItems, empLocationCase, org);
                }
                if (resultItems.Count > 0)
                {
                    result.Groups.Add(new ResultDataGroup()
                    {
                        Key = i,
                        Label = empOrgItem.label,
                        Items = new List<ResultData>().AddRangeFluid(resultItems)
                    });
                    i++;
                }
            }
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Create result set
        /// </summary>
        /// <param name="groupQuery">group by query</param>
        /// <param name="result">Report result</param>
        private ReportResult CreateResult(dynamic groupQuery, ReportResult result)
        {
            List<ResultData> resultItems;
            result.Groups = new List<ResultDataGroup>();
            int i = 1;
            foreach (var queryItem in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (var theCase in queryItem.data)
                {
                    CreateCaseItem(resultItems, theCase);
                }
                result.Groups.Add(new ResultDataGroup()
                {
                    Key = i,
                    Label = queryItem.label,
                    Items = new List<ResultData>().AddRangeFluid(resultItems)
                });

                i++;
            }
            return result;

        }

        /// <summary>
        /// Set organization group data
        /// </summary>
        /// <param name="user"></param>
        /// <param name="query"></param>
        /// <param name="isTpa"></param>
        /// <param name="resultItems"></param>
        /// <param name="empLocationCase"></param>
        /// <param name="org"></param>
        private void SetOrganizationGroupData(User user, IList<Case> query, bool isTpa, List<ResultData> resultItems, List<string> empLocationCase, EmployeeOrganization org)
        {
            foreach (Case caseItem in query.Where(cs => cs.Employee.EmployeeNumber == org.EmployeeNumber && cs.CustomerId == org.CustomerId && cs.EmployerId == org.EmployerId).OrderBy(c => c.Employee.FullName))
            {
                if (!string.IsNullOrWhiteSpace(org.TypeCode)
                     && empLocationCase != null
                     && org.TypeCode.ToUpper().TrimEnd() == OrganizationType.OfficeLocationTypeCode.ToUpper().TrimEnd()
                     && empLocationCase.Contains(caseItem.Id.ToUpper()))
                {
                    continue;
                }

                ReportingLogic reportingLogic = new ReportingLogic(user);
                List<PolicySummary> policySummary = reportingLogic.GetEmployeePolicySummaryByCaseIdAsync(caseItem.Id, null, caseItem.Summary.Policies.Select(p => p.PolicyCode).ToArray()).GetAwaiter().GetResult();

                foreach (var policy in caseItem.Summary.Policies.OrderBy(p => p.PolicyCode))
                {
                    SetPolicySummaryData(resultItems, empLocationCase, org, caseItem, policySummary, policy, isTpa);
                }

                if (caseItem.Summary.Policies.Count == 0)
                {
                    dynamic data = new ResultData();
                    SetCommonData(caseItem, data);
                    SetDataForCase(isTpa, caseItem, data);
                    data.TimeUsed = "N/A";
                    data.TimeAvailable = "N/A";
                    resultItems.Add(data);
                }
            }
        }

        /// <summary>
        /// Set policy summary data
        /// </summary>
        /// <param name="resultItems"></param>
        /// <param name="empLocationCase"></param>
        /// <param name="org"></param>
        /// <param name="caseItem"></param>
        /// <param name="policySummary"></param>
        /// <param name="policy"></param>
        private void SetPolicySummaryData(List<ResultData> resultItems, List<string> empLocationCase, EmployeeOrganization org, Case caseItem, List<PolicySummary> policySummary, CaseSummaryPolicy policy, bool isTpa)
        {
            foreach (var detail in policy.Detail.OrderBy(d => d.StartDate))
            {
                dynamic data = new ResultData();
                SetCommonData(caseItem, data);
                SetDataForPolicy(isTpa,caseItem, policy, detail, data);
                var pd = policySummary.FirstOrDefault(m => m.PolicyCode == policy.PolicyCode);
                data.TimeUsed = pd == null ? "N/A" : pd.TimeUsed < 0 ? "reasonable amount" : String.Format("{0:N2} {1}", pd.TimeUsed, pd.Units.ToString().ToLowerInvariant());
                data.TimeAvailable = pd == null ? "N/A" : pd.TimeRemaining < 0 ? "reasonable amount" : String.Format("{0:N2} {1}", pd.TimeRemaining, pd.Units.ToString().ToLowerInvariant());
                resultItems.Add(data);
                if (!string.IsNullOrWhiteSpace(org.TypeCode)
                    && empLocationCase != null
                    && org.TypeCode.ToUpper().TrimEnd() == OrganizationType.OfficeLocationTypeCode.ToUpper().TrimEnd()
                    && (!empLocationCase.Contains(caseItem.Id.ToUpper())))
                {
                    empLocationCase.Add(caseItem.Id.ToUpper());
                }
            }
        }

        /// <summary>
        /// Create policy data
        /// </summary>
        /// <param name="ReportDatabyPolicy"></param>
        /// <param name="theCase"></param>
        /// <param name="caseAppliedPolicies"></param>
        private static void GetDataByPolicy(List<dynamic> ReportDatabyPolicy, AbsenceSoft.Data.Cases.Case theCase, IEnumerable<PolicySummary> caseAppliedPolicies)
        {
            foreach (var policy in caseAppliedPolicies)
            {
                var newPolicyModel = new
                {
                    CaseNumber = new LinkItem() { Value = theCase.CaseNumber, Link = $"/Cases/{theCase.Id}/View" },
                    EmployeeNumber = new LinkItem() { Value = theCase.Employee.EmployeeNumber, Link = $"/Employees/{theCase.Employee.Id}/View" },
                    EmployeeName = new LinkItem() { Value = theCase.Employee.FullName, Link = $"/Employees/{theCase.Employee.Id}/View" },
                    Reason = theCase.Reason.Name,
                    LeaveType = theCase.CaseType.ToString(),
                    theCase.StartDate,
                    theCase.EndDate,
                    Status = theCase.Status.ToString().SplitCamelCaseString(),
                    policy.PolicyName,
                    Determination = theCase.Summary.Policies.FirstOrDefault(m => m.PolicyCode == policy.PolicyCode)?.Detail?.Count() > 0 ? theCase.Summary.Policies.FirstOrDefault(m => m.PolicyCode == policy.PolicyCode).Detail[0].Determination.ToString() : string.Empty,
                    TimeUsed = policy.TimeUsed < 0 ? "reasonable amount" : $"{policy.TimeUsed:N2} {policy.Units.ToString().ToLowerInvariant()}",
                    TimeAvailable = policy.TimeRemaining < 0 ? "reasonable amount" : $"{policy.TimeRemaining:N2} {policy.Units.ToString().ToLowerInvariant()}"
                };
                ReportDatabyPolicy.Add(newPolicyModel);
            }
            if (!caseAppliedPolicies.Any())
            {
                var newCasePolicyModel = new
                {
                    CaseNumber = new LinkItem() { Value = theCase.CaseNumber, Link = $"/Cases/{theCase.Id}/View" },
                    EmployeeNumber = new LinkItem() { Value = theCase.Employee.EmployeeNumber, Link = $"/Employees/{theCase.Employee.Id}/View" },
                    EmployeeName = new LinkItem() { Value = theCase.Employee.FullName, Link = $"/Employees/{theCase.Employee.Id}/View" },
                    Reason = theCase.Reason.Name,
                    LeaveType = theCase.CaseType.ToString(),
                    theCase.StartDate,
                    theCase.EndDate,
                    Status = theCase.Status.ToString().SplitCamelCaseString(),
                    PolicyName = "N/A",
                    Determination = "N/A",
                    TimeUsed = "N/A",
                    TimeAvailable = "N/A"
                };

                ReportDatabyPolicy.Add(newCasePolicyModel);
            }
        }

        /// <summary>
        /// Get unique employee organizations
        /// </summary>
        /// <param name="query"></param>
        /// <param name="grouptypeCode"></param>
        /// <returns></returns>
        private static List<EmployeeOrganization> GetEmployeeOrganizations(IList<Case> query, ReportResult result)
        {
            string grouptypeCode = null;
            if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "OrganizationTypeGrouping") != null)
            {
                grouptypeCode = result.Criteria.ValueOf<string>("OrganizationTypeGrouping");
            }
            List<string> customerIds = query.Select(cs => cs.CustomerId).Distinct().ToList();
            List<string> employerIds = query.Select(cs => cs.EmployerId).Distinct().ToList();
            List<string> employeeNumbers = query.Select(cs => cs.Employee.EmployeeNumber).Distinct().ToList();
            List<EmployeeOrganization> employeeOrganizations = null;
            if (!String.IsNullOrWhiteSpace(grouptypeCode))
            {
                employeeOrganizations = EmployeeOrganization.AsQueryable().Where(org => org.TypeCode == grouptypeCode && customerIds.Contains(org.CustomerId) && employerIds.Contains(org.EmployerId) && employeeNumbers.Contains(org.EmployeeNumber)).OrderByDescending(o => o.Dates).ToList();
            }
            else
            {
                employeeOrganizations = EmployeeOrganization.AsQueryable().Where(org => customerIds.Contains(org.CustomerId) && employerIds.Contains(org.EmployerId) && employerIds.Contains(org.EmployerId) && employeeNumbers.Contains(org.EmployeeNumber)).OrderByDescending(o => o.Dates).ToList();
            }
            List<EmployeeOrganization> employeeUniqueOrganizations = new List<EmployeeOrganization>();
            foreach (EmployeeOrganization empOrg in employeeOrganizations)
            {
                if (empOrg.TypeCode != OrganizationType.OfficeLocationTypeCode ||
                    ((empOrg.Dates == null || empOrg.Dates.IncludesToday(true))
                    && (!employeeUniqueOrganizations.Any(empUniqueOrg => empUniqueOrg.TypeCode == OrganizationType.OfficeLocationTypeCode && empUniqueOrg.EmployeeNumber == empOrg.EmployeeNumber && empUniqueOrg.CustomerId == empOrg.CustomerId && empUniqueOrg.EmployerId == empOrg.EmployerId))))
                {
                    employeeUniqueOrganizations.Add(empOrg);
                }
            }

            return employeeUniqueOrganizations;
        }

        /// <summary>
        /// Create the Case items for the report
        /// </summary>
        /// <param name="user">Current User</param>
        /// <param name="resultItems">List of result data</param>
        /// <param name="isTpa">Determine if TPA employer</param>
        /// <param name="caseItem">The case</param>
        private void CreateCaseItem(List<ResultData> resultItems, Case caseItem, bool tpa = false)
        {
            foreach (var policy in caseItem.Summary.Policies.OrderBy(p => p.PolicyCode))
            {
                foreach (var detail in policy.Detail.OrderBy(d => d.StartDate))
                {
                    dynamic data = new ResultData();
                    SetCommonData(caseItem, data);
                    SetDataForPolicy(tpa,caseItem, policy, detail, data);
                    resultItems.Add(data);
                }
            }

            if (caseItem.Summary.Policies.Count == 0)
            {
                dynamic data = new ResultData();
                SetCommonData(caseItem, data);
                SetDataForCase(tpa, caseItem, data);
                resultItems.Add(data);
            }
        }

        /// <summary>
        /// Set the data for cases
        /// </summary>
        /// <param name="isTpa">Determine if TPA employer</param>
        /// <param name="caseItem">The case</param>
        /// <param name="data">Result data</param>
        private void SetDataForCase(bool isTpa, Case caseItem, dynamic data)
        {
            if (isTpa)
            {
                data.Employer = caseItem.EmployerName;
            }
            data.StartDate = caseItem.StartDate.ToString("MM/dd/yyyy");
            data.EndDate = caseItem.EndDate.ToString("MM/dd/yyyy");
            data.Policies = "N/A";           
        }

        /// <summary>
        /// Set the Data for policies
        /// </summary>
        /// <param name="user">Current User</param>
        /// <param name="caseItem">The Case</param>
        /// <param name="policy">Case summary policy</param>
        /// <param name="detail">Case summary policy detail</param>
        /// <param name="data">Result Data</param>
        private void SetDataForPolicy(bool isTpa, Case caseItem, CaseSummaryPolicy policy, CaseSummaryPolicyDetail detail, dynamic data)
        {
            if (isTpa)
            {
                data.Employer = caseItem.EmployerName;
            }
            data.StartDate = detail.StartDate.ToString("MM/dd/yyyy");
            data.EndDate = detail.EndDate.ToString("MM/dd/yyyy");
            data.Policies = $"{policy.PolicyCode} - {detail.Determination.ToString().SplitCamelCaseString()}";
        }

        /// <summary>
        /// Set the common data for an item
        /// </summary>
        /// <param name="caseItem">The Case</param>
        /// <param name="displayCaseNumber">Case number</param>
        /// <param name="displayFullName">Full name</param>
        /// <param name="displayReason">Absence reason</param>
        /// <param name="displayLeaveType">Leave type</param>
        /// <param name="data">Result Data</param>
        private void SetCommonData(Case caseItem, dynamic data)
        {
            DateTime? displayRTW = GetCaseEventDate(caseItem, CaseEventType.ReturnToWork);
            DateTime? displayERTW = GetCaseEventDate(caseItem, CaseEventType.EstimatedReturnToWork);
            data.CaseNumber = string.IsNullOrWhiteSpace(caseItem.CaseNumber) ? null : new LinkItem() { Value = caseItem.CaseNumber, Link = $"/Cases/{caseItem.Id}/View" };
            data.EmployeeNumber = string.IsNullOrWhiteSpace(caseItem.Employee.EmployeeNumber) ? null : new LinkItem() { Value = caseItem.Employee.EmployeeNumber, Link = $"/Employees/{caseItem.Employee.Id}/View" };
            data.Name = string.IsNullOrWhiteSpace(caseItem.Employee.FullName) ? null : new LinkItem() { Value = caseItem.Employee.FullName, Link = $"/Employees/{caseItem.Employee.Id}/View" };
            data.Reason = caseItem.Reason.Name;
            data.LeaveType = caseItem.CaseType.ToString();
            data.Status = caseItem.Status.ToString().SplitCamelCaseString();
            data.ReturnToWork = displayRTW != null ? displayRTW.ToString("MM/dd/yyyy") : string.Empty;
            data.EstimatedReturnToWork = displayERTW != null ? displayERTW.ToString("MM/dd/yyyy") : string.Empty;
        }
    }
}