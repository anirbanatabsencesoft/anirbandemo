﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Enums;

namespace AT.Logic.Reporting.Reports
{
    public class EssReportDetail
    {
        public string Title { get; set; }
        public ESSReportType Type { get; set; }
        public string ESSReportName { get; set; }
    }
}
