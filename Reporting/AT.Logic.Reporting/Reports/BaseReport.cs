﻿using AbsenceSoft;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AT.Common.Log;
using AT.Data.Reporting.Model;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports
{
    /// <summary>
    /// Base report for all Reports
    /// </summary>
    [Serializable]
    public abstract class BaseReport
    {
        /// <summary>
        /// The employer identifier filter name
        /// </summary>
        public const string EmployerIdFilterName = "EmployerId";

        #region Abstract Properties

        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public abstract string Id { get; }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public abstract string Name { get; set; }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report category for display.
        /// </summary>
        public abstract string Category { get; set; }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report main category to bifurcate reports.
        /// </summary>
        public abstract string MainCategory { get; set; }

        /// <summary>
        /// When overridden in a derived class, gets name of the icon image to set in front end
        /// </summary>
        public abstract string IconImage { get; }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public abstract bool IsGrouped { get; set; }

        /// <summary>
        /// Property to configure chart related options
        /// </summary>
        public abstract ReportChart Chart { get; set; }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.</returns>
        public abstract ReportCriteria GetCriteria(User user);

        /// <summary>
        /// Contains the columns that can be used for Custom group by, this need to change to Abstract later
        /// </summary>
        public virtual Dictionary<string, List<string>> AvailableGroupingFields { get; }

        #endregion

        #region Virtual Properties
        /// <summary>
        /// When overridden in a derived class, gets the report description for display
        /// </summary>
        public virtual string ReportDescription { get; set; }
        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public virtual int CategoryOrder { get; set; }

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether to show the auto 
        /// generated total counts at the bottom of the group page.
        /// </summary>
        /// <value>
        ///   <c>true</c> if group auto totals; otherwise, <c>false</c>.
        /// </value>
        public virtual bool GroupAutoTotals { get; set; }

        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<Feature> RequiredFeatures() { return new List<Feature>(0); }

        /// <summary>
        /// Determines whether the specified report is visible to the given user for any valid reason.
        /// </summary>
        /// <param name="user">The user to check against.</param>
        /// <returns>
        ///   <c>true</c> if the specified report is visible; otherwise, <c>false</c>.
        /// </returns>
        public virtual bool IsVisible(User user) { return true; }

        /// <summary>
        /// Gets a value indicating whether this instance is viewable in the UI (Run Button) after being
        /// generated, or if not viewable, is only exportable.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is viewable; otherwise, <c>false</c>.
        /// </value>
        public virtual bool IsViewable { get { return true; } }

        /// <summary>
        /// Gets a value indicating whether this instance is exportable to CSV.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is exportable to CSV; otherwise, <c>false</c>.
        /// </value>
        public virtual bool IsExportableToCsv { get { return true; } }

        /// <summary>
        /// Gets a value indicating whether this instance is exportable to PDF.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is exportable to PDF; otherwise, <c>false</c>.
        /// </value>
        public virtual bool IsExportableToPdf { get { return true; } }

        /// <summary>
        /// Gets a value indicating whether report should include Organization Grouping Criteria.
        /// </summary>
        public virtual bool IncludeOrganizationGrouping { get; set; }

        #endregion

        #region Execution Methods

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user account that is requesting the report</param>
        /// <param name="criteria">The report criteria to use for building the report.</param>
        /// <param name="groupBy"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>

#pragma warning disable S107 // Methods should not have too many parameters
        /// <returns>A populated report result.</returns>
        public async Task<ReportResult> RunReportAsync(User user, ReportCriteria criteria, ReportGroupType? groupBy, int pageNumber, int pageSize, string sortBy = null, bool orderByAsc = false, List<string> groupByColumns = null)
#pragma warning restore S107 // Methods should not have too many parameters
        {
            if (groupBy != null && groupBy == ReportGroupType.Organization)
            {
                IncludeOrganizationGrouping = true;
            }
            // Prepare our report result
            ReportResult result = PrepareResult(user, criteria);

            try
            {
                result.Success = true;
                result = await RunReportAsync(user, result, groupBy, pageNumber, pageSize, sortBy, orderByAsc, groupByColumns);
            }
            catch (Exception ex)
            {
                var errorKey = Guid.NewGuid().ToString();
                Logger.Error($"Error running report, Key: {errorKey}  Name: {Name} ", ex);
                result.Error = $"{errorKey}: {ex.Message}";
                result.Success = false;
            }

            // Return the finished result product back to the caller, successful or not
            return await Task.FromResult(FinishResult(result));
        }//end: RunReport

#pragma warning disable S107 // Methods should not have too many parameters
        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        /// <param name="pageNumber">Add the page number</param>
        /// <param name="pageSize">Set the page size</param>
        protected abstract Task<ReportResult> RunReportAsync(User user, ReportResult result, ReportGroupType? reportGroupType, int pageNumber, int pageSize, string sortBy, bool orderByAsc, List<string> groupByColumns);
#pragma warning restore S107 // Methods should not have too many parameters

        /// <summary>
        /// Prepares a new report result to be called by the inheriting report implementation for all report results.
        /// </summary>
        /// <param name="user">The user account of the user requesting the report.</param>
        /// <param name="criteria">The report critera for the report request.</param>
        /// <returns>A working report result that gets passed to the report's implementation of RunReport.</returns>
        private ReportResult PrepareResult(User user, ReportCriteria criteria)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user", "Must provide a valid user account or a current logged in user in order to request a report run.");
            }

            if (criteria == null)
            {
                criteria = GetCriteria(user);
            }
            if (string.IsNullOrWhiteSpace(criteria.CustomerId))
            {
                criteria.CustomerId = user.CustomerKey;
            }
            if (criteria.StartDate == default(DateTime))
            {
                criteria.StartDate = DateTime.UtcNow.AddDays(-1).Date;
            }
            if (criteria.EndDate == default(DateTime))
            {
                criteria.EndDate = DateTime.UtcNow.Date;
            }
            if (criteria.Filters == null)
            {
                criteria.Filters = new List<ReportCriteriaItem>(0);
            }


            ReportResult result = new ReportResult
            {
                Name = Name,
                Category = Category,
                Criteria = criteria,
                Chart = Chart,
                IconImage = IconImage,
                RequestDate = DateTime.UtcNow,
                RequestedBy = user,
                GroupAutoTotals = GroupAutoTotals
            };
            return result;
        }

        /// <summary>
        /// Prepares the report result to be returned with results populated or an error
        /// back to the caller of run report. Calculates the total time to complete, etc.
        /// </summary>
        /// <param name="result">The active working report result to complete.</param>
        /// <returns>The passed in ReportResult with altered properties.</returns>
        private ReportResult FinishResult(ReportResult result)
        {
            // Set our time to complete milliseconds
            result.CompletedIn = (DateTime.UtcNow - result.RequestDate).TotalMilliseconds;

            // Clean up any mis-appropriation of object references depending on the report type
            if (IsGrouped)
            {
                result.Items = null;
            }
            else
            {
                result.Groups = null;
            }
            result.JsonData = result.JsonData ?? (result.Items ?? (result.Groups ?? (result.SeriesData ?? result.JsonData)));
            return result;
        }

        #endregion

        #region Global/Common Criteria Item Builders

        /// <summary>
        /// Builds the employer criteria.
        /// </summary>
        /// <param name="data">The user.</param>
        /// <param name="criteria">The criteria.</param>
        protected internal ReportCriteria BuildEmployerCriteria(UserReportCriteriaData data, ReportCriteria criteria)
        {
            // If the criteria already has an employerId filter, then no need to add it if necessary
            if (criteria.HasCriterias && criteria.Filters.Any(f => f.Name == EmployerIdFilterName))
            {
                return criteria;
            }

            // Check to see if the custer has the multi-employer-access feature, and if so, if this particular user
            //  even has access to multiple employers (if they don't, no need to filter by it)
            if (data.Customer != null
                    && data.Customer.HasFeature(Feature.MultiEmployerAccess)
                    && data.Employers.Count > 1)
            {
                criteria.Filters.Insert(0, GetEmployerCriteria(data));
            }

            // Return our working criteria for Fluid style use
            return criteria;
        }

        /// <summary>
        /// Gets the employer criteria.
        /// </summary>
        /// <param name="data">The user.</param>
        /// <returns></returns>
        protected ReportCriteriaItem GetEmployerCriteria(UserReportCriteriaData data)
        {
            ReportCriteriaItem item = new ReportCriteriaItem
            {
                Name = EmployerIdFilterName,
                Prompt = "Employer",
                Required = false,
                ControlType = ControlType.CheckBoxList,
                KeepOptionAll = true,
                Options = data.Employers.Select(e => new ReportCriteriaItemOption()
                {
                    Value = e.Id,
                    Text = e.Name
                }).OrderBy(e => e.Text).ToList()
            };
            return item;
        }

        /// <summary>
        /// Gets the employee criteria.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        protected ReportCriteriaItem GetEmployeeCriteria(User user)
        {
            ReportCriteriaItem item = new ReportCriteriaItem
            {
                Name = "EmployeeNumber",
                Prompt = "Employee Number",
                Required = false,
                ControlType = ControlType.TextBox
            };
            return item;
        }

        /// <summary>
        /// Gets the case status criteria.
        /// </summary>
        /// <returns></returns>
        protected ReportCriteriaItem GetCaseStatusCriteria()
        {
            ReportCriteriaItem item = new ReportCriteriaItem
            {
                Name = "CaseStatus",
                Prompt = "Case Status",
                Required = false,
                ControlType = ControlType.RadioButtonList,
                Value = (int)CaseStatus.Open,
                Options = Enum.GetValues(typeof(CaseStatus)).OfType<CaseStatus>().Select(e => new ReportCriteriaItemOption()
                {
                    Text = e.ToString().SplitCamelCaseString(),
                    Value = (int)e
                }).ToList()
            };
            return item;
        }

        /// <summary>
        /// Gets the absence reason criteria.
        /// </summary>
        /// <param name="data">The user.</param>
        /// <returns></returns>
        protected ReportCriteriaItem GetAbsenceReasonCriteria(UserReportCriteriaData data)
        {
            ReportCriteriaItem item = new ReportCriteriaItem
            {
                Name = "AbsenceReason",
                Prompt = "Absence Reason",
                Required = false,
                ControlType = ControlType.SelectList,
                KeepOptionAll = true,
                Options = data.AbsenceReasons.Select(e => new ReportCriteriaItemOption()
                {
                    Text = e.Name,
                    Value = e.Id,
                    GroupText = e.Category
                }).ToList()
            };
            return item;
        }

        /// <summary>
        /// Gets the case type criteria.
        /// </summary>
        /// <returns></returns>
        protected ReportCriteriaItem GetCaseTypeCriteria()
        {
            ReportCriteriaItem item = new ReportCriteriaItem
            {
                Name = "CaseType",
                Prompt = "Case Type",
                Required = false,
                ControlType = ControlType.RadioButtonList,
                Options = Enum.GetValues(typeof(CaseType)).OfType<CaseType>().Where(t => t != CaseType.Administrative).Select(e => new ReportCriteriaItemOption()
                {
                    Text = e.ToString().SplitCamelCaseString(),
                    Value = (int)e
                }).ToList()
            };
            return item;
        }

        /// <summary>
        /// Communication type criteria items
        /// </summary>
        /// <returns></returns>
        protected ReportCriteriaItem GetCommTypeCriteria()
        {
            ReportCriteriaItem item = new ReportCriteriaItem
            {
                Name = "Comm Type",
                Prompt = "Communication Type",
                Required = false,
                ControlType = ControlType.RadioButtonList,
                Options = Enum.GetValues(typeof(CommunicationType)).OfType<CommunicationType>().Select(e => new ReportCriteriaItemOption()
                {
                    Text = e.ToString().SplitCamelCaseString(),
                    Value = (int)e
                }).ToList()
            };
            return item;
        }

        /// <summary>
        /// Gets the adjudication criteria.
        /// </summary>
        /// <returns></returns>
        protected ReportCriteriaItem GetAdjudicationCriteria()
        {
            ReportCriteriaItem item = new ReportCriteriaItem
            {
                Name = "Determination",
                Prompt = "Approval Status",
                Required = false,
                ControlType = ControlType.RadioButtonList
            };
            // It's optional, no default value
            item.Options.Add(new ReportCriteriaItemOption()
            {
                Text = AdjudicationStatus.Approved.ToString().SplitCamelCaseString(),
                Value = (int)AdjudicationStatus.Approved
            });
            item.Options.Add(new ReportCriteriaItemOption()
            {
                Text = AdjudicationStatus.Pending.ToString().SplitCamelCaseString(),
                Value = (int)AdjudicationStatus.Pending
            });
            item.Options.Add(new ReportCriteriaItemOption()
            {
                Text = AdjudicationStatus.Denied.ToString().SplitCamelCaseString(),
                Value = (int)AdjudicationStatus.Denied
            });
            return item;
        }

        /// <summary>
        /// Gets the location criteria.
        /// </summary>
        /// <param name="data">The user.</param>
        /// <returns></returns>
        protected ReportCriteriaItem GetLocationCriteria(UserReportCriteriaData data)
        {
            ReportCriteriaItem item = new ReportCriteriaItem
            {
                Name = "Location",
                Prompt = "Location",
                Required = false,
                ControlType = ControlType.CheckBoxList,
                KeepOptionAll = true,
                Options = data.Locations.Select(o => new ReportCriteriaItemOption() { Text = o.Name, Value = o.Code })
                .OrderBy(o => o.Value)
                .ToList(),
                OnChangeMethodName = "FilterChangeEvent"
            };
            return item;
        }

        /// <summary>
        /// Gets the work state criteria.
        /// </summary>
        /// <param name="data">The user.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        protected ReportCriteriaItem GetWorkStateCriteria(UserReportCriteriaData data, DateTime startDate, DateTime endDate)
        {
            ReportCriteriaItem item = new ReportCriteriaItem
            {
                Name = "WorkState",
                Prompt = "Work State",
                Required = false,
                ControlType = ControlType.SelectList,
                KeepOptionAll = true
            };
            ReportingLogic logicObject = new ReportingLogic(data.AuthenticatedUser);
            item.Options = logicObject.GetWorkStatesAsync(data, startDate, endDate).GetAwaiter().GetResult()
                           .OrderBy(e => e)
                           .Select(e => new ReportCriteriaItemOption()
                           {
                               Text = e,
                               Value = e
                           }).ToList();

            item.OnChangeMethodName = "FilterChangeEvent";

            return item;
        }

        /// <summary>
        /// Gets the productivity criteria.
        /// </summary>
        /// <returns></returns>
        protected ReportCriteriaItem GetProductivityCriteria()
        {
            ReportCriteriaItem item = new ReportCriteriaItem
            {
                Name = "Duration",
                Prompt = "Duration",
                Required = false,
                ControlType = ControlType.Duration,
                Value = "Monthly",
                Options = new List<ReportCriteriaItemOption>(){
                new ReportCriteriaItemOption()
                {
                    Text = "Monthly",
                    Value = "Monthly"
                },
                new ReportCriteriaItemOption()
                {
                    Text = "Quarterly",
                    Value = "Quarterly"
                },
                new ReportCriteriaItemOption()
                {
                    Text = "Yearly",
                    Value = "Yearly"
                },
                new ReportCriteriaItemOption()
                {
                    Text = "Date Range",
                    Value = "DateRange"
                },
                 new ReportCriteriaItemOption()
                {
                    Text = "Rolling 12 Months",
                    Value = "Rolling"
                }
            },
                OnChangeMethodName = "FilterChangeEvent"
            };
            return item;
        }

        /// <summary>
        /// Gets the report type criteria.
        /// </summary>
        /// <returns></returns>
        protected ReportCriteriaItem GetReportTypeCriteria()
        {
            ReportCriteriaItem item = new ReportCriteriaItem
            {
                Name = "ReportType",
                Prompt = "Report Type",
                Required = false,
                ControlType = ControlType.SelectList,
                Value = "ByReason",
                Options = new List<ReportCriteriaItemOption>(){
                new ReportCriteriaItemOption()
                {
                    Text = "By Reason",
                    Value = "ByReason"
                },
                new ReportCriteriaItemOption()
                {
                    Text = "By Location",
                    Value = "ByLocation"
                }
            },
                OnChangeMethodName = "FilterChangeEvent"
            };
            return item;
        }

        /// <summary>
        /// Gets the accom type criteria.
        /// </summary>
        /// <param name="data">The user.</param>
        /// <returns></returns>
        protected ReportCriteriaItem GetAccomTypeCriteria(UserReportCriteriaData data)
        {
            ReportCriteriaItem item = new ReportCriteriaItem
            {
                Name = "AccomType",
                Prompt = "Accommodation Type",
                Required = false,
                ControlType = ControlType.SelectList,
                KeepOptionAll = true,
                Options = data.AccommodationTypes.Where(m => !string.IsNullOrWhiteSpace(m.Code)).Select(e => new ReportCriteriaItemOption()
                {
                    Text = e.Name,
                    Value = e.Code
                }).OrderBy(g => g.Text).ToList(),
                OnChangeMethodName = "FilterChangeEvent"
            };
            return item;
        }

        /// <summary>
        /// Gets the gender criteria.
        /// </summary>
        /// <returns></returns>
        protected ReportCriteriaItem GetGenderCriteria()
        {
            ReportCriteriaItem item = new ReportCriteriaItem
            {
                Name = "Gender",
                Prompt = "Gender",
                Required = false,
                ControlType = ControlType.SelectList,
                KeepOptionAll = true,
                Options = Enum.GetValues(typeof(Gender)).OfType<Gender>().Select(e => new ReportCriteriaItemOption()
                {
                    Text = e.ToString().SplitCamelCaseString(),
                    Value = (int)e
                }).ToList(),
                OnChangeMethodName = "FilterChangeEvent"
            };
            return item;
        }

        /// <summary>
        /// Gets the custom fields criteria.
        /// </summary>
        /// <param name="data">The user.</param>
        /// <param name="fieldType">Type of the field.</param>
        /// <param name="target">The target.</param>
        /// <returns></returns>
        protected ReportCriteriaItem GetCustomFieldsCriteria(UserReportCriteriaData data, CustomFieldType? fieldType = null, EntityTarget? target = null)
        {
            ReportCriteriaItem item = new ReportCriteriaItem
            {
                ControlType = ControlType.SelectList,
                KeepOptionAll = true,
                Name = "CustomField",
                Prompt = "Custom Field",
                Required = false,
                OnChangeMethodName = "FilterChangeEvent"
            };
            ReportingLogic logic = new ReportingLogic(data.AuthenticatedUser);
            item.Options = logic.GetUserCustomFieldsAsync(data, fieldType, target).GetAwaiter().GetResult()
                                .Select(f => new ReportCriteriaItemOption()
                                {
                                    Value = f.Name,
                                    Text = f.Label
                                })
                                .ToList();
            return item;
        }

        /// <summary>
        /// Gets to do type criteria.
        /// </summary>
        /// <param name="data">The user.</param>
        /// <returns></returns>
        protected ReportCriteriaItem GetToDoTypeCriteria(UserReportCriteriaData data)
        {
            ReportingLogic rl = new ReportingLogic(data.AuthenticatedUser);
            var criteriaData = rl.GetToDoItemTypes().GetAwaiter().GetResult();
            ReportCriteriaItem item = new ReportCriteriaItem
            {
                ControlType = ControlType.SelectList,
                KeepOptionAll = true,
                Name = "ToDoItemType",
                Prompt = "ToDo Type",
                Required = false,
                OnChangeMethodName = "FilterChangeEvent",
                Options = criteriaData?.OrderBy(t => t.Value).Select(t => new ReportCriteriaItemOption()
                {
                    Value = (int)t.Key,
                    Text = t.Value
                }).ToList()
            };
            return item;
        }

        /// <summary>
        /// Returns the case assignee criteria data
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        protected ReportCriteriaItem GetCaseAssigneeCriteria(UserReportCriteriaData data)
        {
            ReportCriteriaItem item = new ReportCriteriaItem
            {
                ControlType = ControlType.SelectList,
                KeepOptionAll = true,
                Name = "CaseAssignee",
                Prompt = "Assigned To",
                Required = false,
                OnChangeMethodName = "FilterChangeEvent",

                Options = data.AssignedToList.OrderBy(t => t.DisplayName).Select(t => new ReportCriteriaItemOption()
                {
                    Value = t.Id,
                    Text = t.DisplayName
                }).ToList()
            };
            return item;
        }

        /// <summary>
        /// Returns the case to do assignee criteria
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        protected ReportCriteriaItem GetToDoAssigneeCriteria(UserReportCriteriaData data)
        {
            var rl = new ReportingLogic(data.AuthenticatedUser);
            var criteriaData = rl.GetToDoAssignedList().GetAwaiter().GetResult();
            ReportCriteriaItem item = new ReportCriteriaItem
            {
                ControlType = ControlType.SelectList,
                KeepOptionAll = true,
                Name = "ToDoItemAssignee",
                Prompt = "Assigned To",
                Required = false,
                OnChangeMethodName = "FilterChangeEvent",

                Options = criteriaData?.OrderBy(t => t.DisplayName).Select(t => new ReportCriteriaItemOption()
                {
                    Value = t.Id,
                    Text = t.DisplayName
                }).ToList()
            };
            return item;
        }

        /// <summary>
        /// Gets to do status criteria.
        /// </summary>
        /// <param name="data">The user.</param>
        /// <returns></returns>
        protected ReportCriteriaItem GetToDoStatusCriteria(UserReportCriteriaData data)
        {
            ReportCriteriaItem item = new ReportCriteriaItem
            {
                ControlType = ControlType.SelectList,
                KeepOptionAll = true,
                Name = "ToDoStatus",
                Prompt = "ToDo Status",
                Required = false,
                OnChangeMethodName = "FilterChangeEvent",
                Options = Enum.GetValues(typeof(ToDoItemStatus)).OfType<ToDoItemStatus>().Select(t => new ReportCriteriaItemOption()
                {
                    Value = (int)t,
                    Text = t.ToString().SplitCamelCaseString()
                }).OrderBy(t => t.Text).ToList()
            };
            return item;
        }

        /// <summary>
        /// Returns the diagnosis code criteria
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        protected ReportCriteriaItem GetDiagnosisCodeCriteria(UserReportCriteriaData data)
        {
            ReportCriteriaItem item = new ReportCriteriaItem
            {
                Name = "DiagnosisCode",
                Prompt = "Diagnosis",
                Required = false,
                ControlType = ControlType.CheckBoxList,
                KeepOptionAll = true,

                Options = data.DiagnosisCodes
                .OrderBy(a => a.Value)
                .ThenBy(a => a.Key)
                .Select(e => new ReportCriteriaItemOption()
                {
                    Text = e.Value,
                    Value = e.Key
                }).ToList()
            };

            return item;
        }

        /// <summary>
        /// Gets Organization Type Criteria
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        protected List<ReportCriteriaItem> GetOrganizationTypeCriteria(UserReportCriteriaData data)
        {
            var items = data.OrganizationTypes?.Select(o => new ReportCriteriaItem()
            {
                Name = "OrganizationType-" + o.OrganizationType.Code,
                Prompt = o.OrganizationType.Name,
                Required = false,
                ControlType = ControlType.CheckBoxList,
                KeepOptionAll = true,
                Options = o.Organizations?.Select(obt => new ReportCriteriaItemOption() { Value = obt.Path, Text = obt.Name }).ToList()
            }).ToList();

            return items;
        }

        /// <summary>
        /// Gets the absence reason criteria.
        /// </summary>
        /// <param name="data">The user.</param>
        /// <returns></returns>
        protected ReportCriteriaItem GetOrganizationTypeGroupByCriteria(UserReportCriteriaData data)
        {
            var item = new ReportCriteriaItem
            {
                Name = "OrganizationTypeGrouping",
                Prompt = "Group By",
                Required = false,
                ControlType = ControlType.SelectList,
                KeepOptionAll = true,
                Options = data.OrganizationTypes.Select(o => new ReportCriteriaItemOption()
                {
                    Text = o.OrganizationType.Name,
                    Value = o.OrganizationType.Code
                }).ToList()
            };

            if (!item.Options.Any())
            {
                item.Options.Add(new ReportCriteriaItemOption() { Value = "OFFICELOCATION", Text = "Office Location" });
            }

            return item;
        }

        /// <summary>
        /// Gets the UserReportCriteriaData for given User
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        protected UserReportCriteriaData GetUserReportCriteriaData(AT.Entities.Authentication.User user)
        {
            ReportingLogic reportingLogic = new ReportingLogic(user);
            UserReportCriteriaData userReportCriteriaData = reportingLogic.GetUserReportCriteriaDataAsync().GetAwaiter().GetResult();
            return userReportCriteriaData;
        }

        /// <summary>
        /// Get the Case event date
        /// </summary>
        /// <param name="caseItem">Case Item</param>
        /// <param name="returnToWork">Event Type</param>
        /// <returns></returns>
        protected DateTime? GetCaseEventDate(Case caseItem, CaseEventType returnToWork)
        {
            CaseEvent ev = caseItem.FindCaseEvent(returnToWork);
            if (ev != null)
            {
                return ev.EventDate;
            }
            return null;
        }

        #endregion Global/Common Criteria Item Builders

        /// <summary>
        /// Gets a value indicating whether this instance has linked report.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has linked report; otherwise, <c>false</c>.
        /// </value>
        public virtual bool HasLinkedReport { get { return false; } }

        /// <summary>
        /// Gets the linked report.
        /// </summary>
        /// <value>
        /// The linked report.
        /// </value>
        public virtual LinkedReport LinkedReport { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance has sub report.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has sub report; otherwise, <c>false</c>.
        /// </value>
        public virtual bool HasSubReport { get { return false; } }

        /// <summary>
        /// Category title to be used in ESS Report List
        /// </summary>
        public virtual string ESSCategoryTitle { get { return string.Empty; } }

        /// <summary>
        /// ESS Report detail
        /// </summary>
        public virtual EssReportDetail ESSReportDetails { get { return null; } }

        /// <summary>
        /// Derived property to be used for ESS report filtering
        /// </summary>
        public bool IsESSReport { get { return ESSReportDetails != null; } }

    }
}