﻿using System.Collections.Generic;

namespace AT.Logic.Reporting.Reports
{
    /// <summary>
    /// This is the result class for an AdHocQueryBuilder.
    /// </summary>
    public class AdHocQueryBuilderResult
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public AdHocQueryBuilderResult()
        {
            Arguments = new List<object>();
        }

        /// <summary>
        /// Stores the query to run against the database.
        /// </summary>
        public string Query { get; set; }

        /// <summary>
        /// A list of arguments to be passed to a query executor. Call .ToArray() method on this property
        /// before passing into a paramter like: params object[] args.
        /// </summary>
        public List<object> Arguments { get; set; }
    }
}
