﻿using AbsenceSoft.Data;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports
{
    [Serializable]
    public class ReportCriteriaItemOption : BaseNonEntity
    {
        public ReportCriteriaItemOption()
        {

        }

        public ReportCriteriaItemOption(string value, string text)
        {
            Text = text;
            Value = value;
        }

        public ReportCriteriaItemOption(string textAndValue)
        {
            Text = textAndValue;
            Value = textAndValue;
        }

        /// <summary>
        /// Gets or sets the name of the option (display text)
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the value of the option (value text)
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// Gets or sets the optional group text (for grouped checkbox lists or grouped select lists, etc.)
        /// </summary>
        public string GroupText { get; set; }
    }
}
