﻿using AT.Data.Reporting.PostgreSqlDataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Reports
{
    /// <summary>
    /// Result class provides the history of recent user's report request.
    /// </summary>
    public class ReportRequestHistoryResult
    {
        /// <summary>
        /// The id (pk) of the processed request
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Report name to show on screen
        /// </summary>
        public string ReportName { get; set; }

        /// <summary>
        /// The request type (whether UI/Scheduler)
        /// </summary>
        public string RequestType { get; set; }        
       
        /// <summary>
        /// Status of the processing
        /// </summary>
        public RequestStatus Status { get; set; }
     
        /// <summary>
        /// Message if there is any (for error)
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// File path of the JSON we want to show data on screen
        /// </summary>
        public string ResultMaxJsonFilePath { get; set; }      

        /// <summary>
        /// PDF File path
        /// </summary>
        public string ResultPdfFilePath { get; set; }
       
        /// <summary>
        /// CSV file path
        /// </summary>
        public string ResultCsvFilePath { get; set; }

        /// <summary>
        /// Criteria to show on screen
        /// </summary>
        public string CriteriaPlainEnglish { get; set; }

        /// <summary>
        /// Report Id
        /// </summary>
        public string ReportId { get; set; }

        /// <summary>
        /// Set the last modification status
        /// </summary>
        public DateTime? LastModified { get; set; }

    }
}
