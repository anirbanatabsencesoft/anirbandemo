﻿using AbsenceSoft;
using AbsenceSoft.Common.Postgres;
using AbsenceSoft.Common.Serializers;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Reporting;
using AT.Data.Reporting.Arguments;
using AT.Entities.Reporting.Classes;
using AT.Entities.Reporting.ReportingConstants;
using AT.Logic.Cache;
using AT.Logic.Reporting.Reports;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Common.Log;

namespace AT.Logic.Reporting
{
    /// <summary>
    /// The Reporting Logic class to provide the data access point of contact
    /// </summary>
    public partial class ReportingLogic
    {
        private const string DataWarehouse = "DataWarehouse";

        /// <summary>
        /// Get all the adhoc reports
        /// Try using cache, if not available get it from database
        /// </summary>
        /// <returns></returns>
        private List<ReportDefinition> GetAllReportDefinitions()
        {
            return DataProvider.GetAllDefinitions().GetAwaiter().GetResult();
        }

        /// <summary>
        /// Returns all Adhoc reports for a user
        /// </summary>
        /// <param name="adhocReportType">Use the report type for only typed reports</param>
        /// <returns></returns>
        public List<Report> GetAdhocReports(AdHocReportType? adhocReportType = null)
        {
            //get all adhoc definitions
            var adhocReportDefinitions = GetAllReportDefinitions()
                                            .Where(r => r.ReportType != null && r.ReportType.AdHocReportType != null)
                                            .Where(r => r.CustomerId == AuthenticatedUser.CustomerKey)
                                            .Where(r => adhocReportType == null || r.ReportType.AdHocReportType == adhocReportType)
                                            .Where(r => !r.IsDeleted)
                                            .OrderByDescending(r => r.ModifiedDate)
                                            .ToList();
           
            return ConvertDefinitionToAdhocReport(adhocReportDefinitions);
        }

        public List<ReportDefinition> GetAdhocReportDefinitions(AdHocReportType? adhocReportType = null)
        {
            //get all adhoc definitions
            var adhocReportDefinitions = GetAllReportDefinitions()
                                            .Where(r => r.ReportType != null && r.ReportType.AdHocReportType != null)
                                            .Where(r => r.CustomerId == AuthenticatedUser.CustomerKey)
                                            .Where(r => adhocReportType == null || r.ReportType.AdHocReportType == adhocReportType)                                            
                                            .ToList();

            return adhocReportDefinitions;
        }

        /// <summary>
        /// Returns the Adhox report definitions async
        /// </summary>
        /// <param name="adhocReportType"></param>
        /// <returns></returns>
        public async Task<List<Report>> GetAdhocReportsAsync(AdHocReportType? adhocReportType = null)
        {
            return await Task.FromResult(GetAdhocReports(adhocReportType));
        }

        /// <summary>
        /// Get the report by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Report GetReportDefinitionByName(string name)
        {
            return ConvertDefinitionToAdhocReport(GetAllReportDefinitions().FirstOrDefault(p => p.Name == name));
        }

        /// <summary>
        /// Get the report by name as async
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<Report> GetReportDefinitionByNameAsync(string name)
        {
            return await Task.FromResult(GetReportDefinitionByName(name));
        }

        /// <summary>
        /// Get the report by name
        /// </summary>
        /// <param name="reportTypeId"></param>
        /// <returns></returns>
        public List<ReportColumn> GetAdHocReportColumns(string reportTypeId)
        {
            //get all report type fields from cache if null the from database
            var reportTypeFields = CacheHelper.Get<List<ReportTypeField>>(CacheConstants.REPORTING_ALL_REPORT_TYPE_FIELDS);
            if (reportTypeFields == null)
            {
                reportTypeFields = DataProvider.GetAllReportTypeFieldsAsync().GetAwaiter().GetResult();
                CacheHelper.Set(CacheConstants.REPORTING_ALL_REPORT_TYPE_FIELDS, reportTypeFields);
            }

            //get all report report fields from cache if not available then from database
            var reportFields = CacheHelper.Get<List<ReportField>>(CacheConstants.REPORTING_ALL_REPORT_FIELDS);
            if (reportFields == null)
            {
                reportFields = DataProvider.GetAllReportFieldsAsync().GetAwaiter().GetResult();
                CacheHelper.Set(CacheConstants.REPORTING_ALL_REPORT_FIELDS, reportFields);
            }

            //filter and return
            var reportTypes = reportTypeFields.Where(t => t.ReportTypeId == reportTypeId).Select(t => t.FieldName).ToList();
            var data = reportFields
                        .Where(f => reportTypes.Contains(f.Name))
                        .Where(f => f.CustomerId == null || f.CustomerId == AuthenticatedUser.CustomerKey)
                        .OrderBy(f => f.Id)
                        .ToList();

            return ConvertToReportColumns(data);
        }

        /// <summary>
        /// Returns the columns as async
        /// </summary>
        /// <param name="reportTypeId"></param>
        /// <returns></returns>
        public async Task<List<ReportColumn>> GetAdHocReportColumnsAsync(string reportTypeId)
        {
            return await Task.FromResult(GetAdHocReportColumns(reportTypeId));
        }

        /// <summary>
        /// Returns all the adhoc report types
        /// </summary>
        /// <returns></returns>
        public virtual List<ReportType> GetReportTypes(bool? adhocOnly = null)
        {
            //get from cache if not available then from database
            var reportTypes = CacheHelper.Get<List<ReportType>>(CacheConstants.REPORTING_ALL_REPORT_TYPE);
            if (reportTypes == null)
            {
                reportTypes = DataProvider.GetReportTypesAsync().GetAwaiter().GetResult();
                CacheHelper.Set(CacheConstants.REPORTING_ALL_REPORT_TYPE, reportTypes);
            }

            //filter the records
            return reportTypes.ToList();
        }

        /// <summary>
        /// Lists the report types in async mode
        /// </summary>
        /// <param name="adhocOnly"></param>
        /// <returns></returns>
        public async Task<List<ReportType>> GetReportTypesAsync(bool? adhocOnly = null)
        {
            return await Task.FromResult(GetReportTypes(adhocOnly));
        }

        /// <summary>
        /// Returns the list of Reports filtered by Report type id
        /// </summary>
        /// <param name="reportTypeId"></param>
        /// <returns></returns>
        public virtual ReportType GetReportTypeById(string reportTypeId)
        {
            var reportTypes = GetReportTypes();
            return reportTypes.FirstOrDefault(p => p.Id == reportTypeId);
        }

        /// <summary>
        /// Get the report by id as async
        /// </summary>
        /// <param name="reportTypeId"></param>
        /// <returns></returns>
        public async Task<ReportType> GetReportTypeByIdAsync(string reportTypeId)
        {
            return await Task.FromResult(GetReportTypeById(reportTypeId));
        }

        /// <summary>
        /// Gets the field filter baseed on the field filter template and populates any options/values for it.
        /// </summary>
        /// <param name="reportTypeId">The report type identifier.</param>
        /// <param name="fieldName">The report field name.</param>
        /// <returns>A populated report filter for use by the UI.</returns>
        public ReportFilter GetFieldFilter(string reportTypeId, string fieldName)
        {
            // 1. Get the report filter
            var reportField = GetFields(reportTypeId).FirstOrDefault(p => p.Name == fieldName);
            if (reportField == null)
            {
                return null;
            }

            // 2. Get the filter from the field
            var reportFilter = reportField.Filter;

            // 3. Check if we need filter options            
            // 4. Get some options for this field for this view
            if (reportFilter.ControlType == ControlType.SelectList ||
                reportFilter.ControlType == ControlType.RadioButtonList ||
                reportFilter.ControlType == ControlType.CheckBoxList)
            {
                // 5. Get the report type
                var reportType = GetReportTypeById(reportTypeId);
                // 6. Build distinct value query
                //    e.g. SELECT DISTINCT <field_name> FROM <report_type_key> WHERE cust_id = <customer_id> AND empl_id = <employer_id> ORDER BY <field_name>
                var args = BuildQueryArguments(reportField, reportType);
                // Note that \"%I\" = @0 does not get executed as a parameter until the second trip to the database.
                string query = BuildFormatQuery(reportType);

                // Run this query against PostgreSQL to get the list of values back
                var db = new CommandRunner(DataWarehouse);
                var formattedResult = db.ExecuteDynamic(query, args);
                string formattedQuery = formattedResult.First().query;
                var results = db.ExecuteDynamic(formattedQuery, AuthenticatedUser.CustomerId);

                foreach (var result in results)
                {
                    reportFilter.Options.Add(new ReportFilterOption
                    {
                        Text = result.value,
                        Value = result.value,
                        GroupText = $"{reportType.Key}_{reportField.Name}"
                    });
                }
            }

            // 7. Return the filter
            return reportFilter;
        }

        /// <summary>
        /// Gets a list of fields valid for a specific report type identifier.
        /// </summary>
        /// <param name="reportTypeId">The report type identifier to fetch.</param>
        /// <returns>A list of ReportField.</returns>
        public List<ReportField> GetFields(string reportTypeId)
        {
            var reportTypeFields = CacheHelper.Get<List<ReportTypeField>>(CacheConstants.REPORTING_ALL_REPORT_TYPE_FIELDS);
            if (reportTypeFields == null)
            {
                reportTypeFields = DataProvider.GetAllReportTypeFieldsAsync().GetAwaiter().GetResult();
                CacheHelper.Set(CacheConstants.REPORTING_ALL_REPORT_TYPE_FIELDS, reportTypeFields);
            }

            var filteredReportTypeFields = reportTypeFields.Where(p => p.ReportTypeId == reportTypeId).Select(p => p.FieldName).ToList();
            //get all report report fields from cache if not available then from database
            var reportFields = CacheHelper.Get<List<ReportField>>(CacheConstants.REPORTING_ALL_REPORT_FIELDS);
            if (reportFields == null)
            {
                reportFields = DataProvider.GetAllReportFieldsAsync().GetAwaiter().GetResult();
                CacheHelper.Set(CacheConstants.REPORTING_ALL_REPORT_FIELDS, reportFields);
            }

            return reportFields.Where(p => filteredReportTypeFields.Contains(p.Name)).ToList();
        }

        /// Returns the list of Reports filtered by Report type id
        /// </summary>
        /// <param name="reportId"></param>
        /// <returns></returns>
        public Report GetReportById(string reportId)
        {
            var reportsDefinitions = GetAllReportDefinitions();
            return ConvertDefinitionToAdhocReport(reportsDefinitions.FirstOrDefault(p => p.Id == reportId));
        }

        /// <summary>
        /// Get report by Id as async
        /// </summary>
        /// <param name="reportId"></param>
        /// <returns></returns>
        public async Task<Report> GetReportByIdAsync(string reportId)
        {
            return await Task.FromResult(GetReportById(reportId));
        }

        /// <summary>
        /// Gets all saved report definitions for the passed UserId.
        /// </summary>
        /// <returns>List of IReport associated with the UserId.</returns>
        public async Task<ReportDefinition> GetUserReportDefinition(string id)
        {
            List<ReportDefinition> reports = GetAdhocReportDefinitions();
            return await Task.FromResult(reports.FirstOrDefault(p => p.UserId == AuthenticatedUser.Key && p.Id == id));
        }

        /// <summary>
        /// Gets all saved report definitions for the passed UserId.
        /// </summary>
        /// <returns>List of IReport associated with the UserId.</returns>
        public async Task<List<Report>> GetUserReportDefinitions()
        {
            List<Report> reports = GetAdhocReports();
            return await Task.FromResult(reports.Where(p => p.UserId == AuthenticatedUser.Key).ToList());
        }

        /// <summary>
        /// Saves the definition of a report.
        /// </summary>
        /// <param name="saveReportArgs">Save argument for report included definition.</param>
        /// <returns>The report definition.</returns>      
        public async Task<ReportColumnDefinition> SaveReportDefinitionAsync(SaveReportArgs saveReportArgs)
        {
            if (!saveReportArgs.IsValid)
            {
                throw new ArgumentNullException(saveReportArgs.Message);
            }

            return await DataProvider.SaveReportDefinitionAsync(saveReportArgs.ColumnDefinition, AuthenticatedUser);
        }

        /// <summary>
        /// Saves the report under a new name. 
        /// </summary>
        /// <param name="saveReportArgs">Save argument for report included definition.</param>
        /// <returns>The report definition.</returns>
        ///         
        public async Task<ReportColumnDefinition> SaveAsReportDefinitionAsync(SaveReportArgs saveReportArgs)
        {
            if (!saveReportArgs.IsValid)
            {
                throw new ArgumentNullException(saveReportArgs.Message);
            }
            return await DataProvider.SaveReportDefinitionAsync(saveReportArgs.ColumnDefinition, AuthenticatedUser, true);

        }

        /// <summary>
        /// Get the recently ran adhoc reports in async mode
        /// </summary>
        /// <param name="limit"></param>
        /// <returns></returns>
        public async Task<List<Report>> GetRecentReportDefinitionListAsync(int limit = 10)
        {
            var data = await GetUserReportCriteriaDataAsync();
            return await Task.FromResult(ConvertDefinitionToAdhocReport(DataProvider.GetRecentReportDefinitionList(data, limit).GetAwaiter().GetResult()));
        }

        /// <summary>
        /// Build the query for Adhoc reports
        /// </summary>
        /// <param name="reportDefinition"></param>
        /// <param name="reportFields"></param>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public async Task<AdHocQueryBuilderResult> AdhocBuildQueryAsync(ReportDefinition reportDefinition, List<ReportField> reportFields, int limit = 10, int offset = 0)
        {
            var data = await GetUserReportCriteriaDataAsync();
            return await Task.FromResult(AdHocQueryBuilder.Build(reportDefinition, reportFields, data.SecurityUser, limit, offset));
        }

        /// <summary>
        /// Returns the report category
        /// </summary>
        /// <param name="reportDefinition"></param>
        /// <returns></returns>
        public string GetAdhocReportCategory(ReportDefinition reportDefinition)
        {
            if (reportDefinition == null)
            {
                throw new ArgumentNullException("reportDefinition");
            }

            if (reportDefinition.ReportType == null || reportDefinition.ReportType.AdHocReportType == null)
            {
                return string.Empty;
            }

            return Enum.GetName(typeof(AdHocReportType), reportDefinition.ReportType.AdHocReportType);
        }

        /// <summary>
        /// Execute query for adhoc reports
        /// </summary>
        /// <param name="query"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        internal async Task<List<DynamicAwesome>> ExecuteQueryAsync(string query, params object[] args)
        {
            var db = new CommandRunner(DataWarehouse);
            return await Task.FromResult(db.ExecuteDynamicAwesome(query, args).ToList());
        }

        internal List<DynamicAwesome> FormatResults(List<DynamicAwesome> results)
        {
            var bsonNullConverter = new BsonNullConverter();
            foreach (var result in results)
            {
                FormatResult(result, bsonNullConverter);
            }
            return results;
        }

        /// <summary>
        /// Formats the dynamic result from the set.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <param name="bsonNullConverter">The bson null converter.</param>
        /// <returns></returns>
        internal DynamicAwesome FormatResult(DynamicAwesome result, BsonNullConverter bsonNullConverter = null)
        {
            bsonNullConverter = bsonNullConverter ?? new BsonNullConverter();
            foreach (var key in result.Keys)
            {
                var value = result[key];
                if (bsonNullConverter.CanConvert(value.GetType()))
                {
                    result[key] = string.Empty;
                }
            }
            return result;
        }

        /// <summary>
        ///  Method calls provider's GetEmployeeOrganization method from Report class.
        /// </summary>
        /// <param name="groupTypeCode"></param>
        /// <param name="customerIds"></param>
        /// <param name="employerIds"></param>
        /// <param name="employeeNumbers"></param>
        /// <returns></returns>
        public async Task<List<EmployeeOrganization>> GetEmployeeOrganization(string groupTypeCode, List<string> customerIds, List<string> employerIds, List<string> employeeNumbers)
        {
            return await DataProvider.GetEmployeeOrganization(groupTypeCode, customerIds, employerIds, employeeNumbers);
        }

        /// <summary>
        /// Delete a saved adhoc report
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public async Task<int> DeleteAdhocReportAsync(DeleteReportArgs args)
        {
            if (!args.IsValid)
            {
                throw new ArgumentNullException(args.Message);
            }
            return await DataProvider.DeleteSavedAdhocReportAsync(args);
        }

        /// <summary>
        /// Method calls provider's GetAccommodationTypes method that fetches Accommodation Type based on corresponding CustomerId and EmpployerId
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="employerId"></param>
        /// <returns></returns>
        public async Task<List<AccommodationType>> GetAccomodationTypes(string customerId, string employerId)
        {
            return await DataProvider.GetAccomodationTypes(customerId, employerId);
        }

        /// <summary>
        /// Returns the To-Do Assigned to list
        /// </summary>
        /// <returns></returns>
        public async Task<List<AbsenceSoft.Data.Security.User>> GetToDoAssignedList()
        {
            return await DataProvider.GetToDoAssignedList(SecurityUser);
        }

        /// <summary>
        /// Returns the to-do item types
        /// </summary>
        /// <returns></returns>
        public async Task<Dictionary<ToDoItemType, string>> GetToDoItemTypes()
        {
            return await DataProvider.GetTodoItemTypes(SecurityUser);
        }

    }
}