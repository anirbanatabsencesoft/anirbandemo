﻿using AT.Api.Core.Common;
using AT.Api.Notification.Controllers;
using AT.Common.Core;
using AT.Common.Core.NotificationEnums;
using AT.Data.Notification.Arguments;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using AT.Logic.Reporting.Arguments;
using AT.Logic.Reporting.Base;
using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;

namespace AT.Logic.Reporting
{
    public class EmailDeliveryProvider : BaseReportDeliveryProvider
    {
        private User AuthenticatedUser { get; set; }
        
        /// <summary>
        /// Constructor which accepts ReportDeliveryArgs and Authenticated User
        /// </summary>
        /// <param name="reportDeliveryArgs"></param>
        /// <param name="user"></param>
        public EmailDeliveryProvider(EmailReportDeliveryArgs emailReportDeliveryArgs, User user)
        {
            Parameters = emailReportDeliveryArgs;
            AuthenticatedUser = user;
        }
        
        /// <summary>
        /// Sends the mail using the given parameters
        /// </summary>
        /// <returns></returns>
        public override async Task<ResultSet<bool>> Send()
        {
            ResultSet<bool> resultSet = new ResultSet<bool>();
            MailMessage msg = new MailMessage
            {
                From = new MailAddress(Parameters.Sender),
                Subject = Parameters.Subject,
                Body = GetEmailReportBody(ReportDeliveryTemplateType.ReportGenerated),
                IsBodyHtml = true
            };
            if (Parameters.Recipients.Length > 0)
            {
                msg.To.Add(Parameters.Recipients);

                var saveNotificationArg = SetNotificationArgument(msg, AuthenticatedUser);
                var result = await CallApi(saveNotificationArg);
                resultSet.Success = result.Success;
            }
            else
            {
                resultSet.Message = "No recipients found, please recheck or contact administrator.";
                resultSet.Success = false;
            }
            return resultSet;
        }

        /// <summary>
        /// Sets the notification arguments
        /// </summary>
        /// <param name="message"></param>
        /// <param name="currentUser"></param>
        /// <param name="creatSaveNotificationArgsedByUserId"></param>
        /// <returns></returns>
        private SaveNotificationArgs SetNotificationArgument(MailMessage message, User currentUser)
        {
            AT.Entities.Notification.Notification notification = new AT.Entities.Notification.Notification
            {
                CustomerKey = currentUser.CustomerKey,
                CustomerId = currentUser.CustomerId ?? 0,
                From = message.From.ToString(),
                NotificationType = NotificationType.Email,
                CreatedById = currentUser.Id,
                CreatedByKey = currentUser.Key,
                ModifiedById = currentUser.Id,
                ModifiedByKey = currentUser.Key,
                MessageFormat = MessageFormat.HTML,
                Subject = message.Subject,
                Message = message.Body
            };
            List<AT.Entities.Notification.NotificationRecipient> NotificationRecipients = new List<AT.Entities.Notification.NotificationRecipient>();
            
            AT.Entities.Notification.NotificationRecipient notificationRecipient = new AT.Entities.Notification.NotificationRecipient()
            {
                RecipientAddress = message.To.ToString(),
                RecipientType = RecipientType.To
            };
            NotificationRecipients.Add(notificationRecipient);
            
            SaveNotificationArgs saveNotificationArgs = new SaveNotificationArgs()
            {
                Notification = notification,
                Recipients = NotificationRecipients
            };
            return saveNotificationArgs;
        } 

        /// <summary>
        /// Calls the Notification Api using Service Factory
        /// <param name="saveNotificationArgs"></param>
        /// <returns></returns>
        /// </summary>
        private async Task<AT.Common.Core.ResultSet<AT.Entities.Notification.Notification>> CallApi(SaveNotificationArgs saveNotificationArgs)
        {
            ServiceFactory factory = new ServiceFactory();
            return await factory.ExecuteService<NotificationController, SaveNotificationArgs, AT.Entities.Notification.Notification>(saveNotificationArgs, nc => nc.Save(saveNotificationArgs));
        }        
    }
}