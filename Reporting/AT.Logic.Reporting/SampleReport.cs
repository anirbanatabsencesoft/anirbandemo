﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Data.Reporting;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Classes;
using AT.Entities.Reporting.Interfaces;

namespace AT.Logic.Reporting
{
    /// <summary>
    /// The Sample Report Class
    /// </summary>
    public class SampleReport : Report
    {
        /// <summary>
        /// Id property
        /// </summary>
        public override string Id => "Sample Report";
        /// <summary>
        /// Name property
        /// </summary>
        public override string Name => "Sample Report";
        /// <summary>
        /// The Generate Method of the Report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportQuery"></param>
        /// <returns></returns>
        public override IReportResult Generate(User user, IReportQuery reportQuery)
        {
            return new ReportResult();
        }
        /// <summary>
        /// List of the Sub-Reports
        /// </summary>
        public override List<IReport> SubReports { get; set; }
    }
}
