﻿using AbsenceSoft;
using AT.Common.Core;
using AT.Common.Resource;
using AT.Entities.Reporting.Classes;
using AT.Entities.Reporting.Enums;
using AT.Logic.Reporting.Arguments;
using System.Reflection;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Base
{
    public abstract class BaseReportDeliveryProvider
    {
        public ReportDeliveryArgs Parameters { get; set; }
        public abstract Task<ResultSet<bool>> Send();

        /// <summary>
        /// Gets the Report Body prepared for Email
        /// </summary>
        /// <returns></returns>
        public string GetEmailReportBody(ReportDeliveryTemplateType reportDeliveryTemplateType)
        {
            string body = string.Empty;
            body = GetFromResources(reportDeliveryTemplateType);

            if (!string.IsNullOrEmpty(body))
            {
                foreach (PropertyInfo prop in typeof(DeliveryParameters).GetProperties())
                {
                    var findKey = $"<{prop.Name.ToUpper()}>";
                    body = body.Replace(findKey, prop.GetValue(Parameters.ReplaceKeyValue, null).ToString());
                }
            }
            else
            {
                throw new AbsenceSoftException("Mail template Report Generated Template is missing");
            }
            return body;
        }

        /// <summary>
        /// Gets the template file from Resource
        /// </summary>
        /// <returns></returns>
        public string GetFromResources(ReportDeliveryTemplateType reportDeliveryTemplateType)
        {
            string resultString = string.Empty;
            resultString = ResourceHelper.Get("Reporting", reportDeliveryTemplateType.ToString());
            return resultString;
        }
    }
}