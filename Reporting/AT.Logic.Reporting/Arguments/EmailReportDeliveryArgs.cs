﻿using AT.Common.Core.ReportEnums;
using AT.Entities.Reporting.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Arguments
{
    [Serializable]
    public class EmailReportDeliveryArgs : ReportDeliveryArgs
    {
        /// <summary>
        /// Identifier or the Argument
        /// </summary>
        public override string Identifier => "DELIVER_REPORT_EMAIL";
                
        /// <summary>
        /// Key values to be replaced in the template
        /// </summary>
        public EmailDeliveryParameters EmailReplaceKeyValue { get; set; }
    }
}