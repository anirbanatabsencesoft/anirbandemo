﻿using AT.Common.Core;
using AT.Entities.Reporting.Enums;
using AT.Logic.Reporting.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AT.Logic.Arguments
{
    /// <summary>
    /// Arguments to be passed to execute a report
    /// </summary>
    [Serializable]
    public class ExecuteReportArgs : BaseArgument
    {
        /// <summary>
        /// Identifier or the Argument
        /// </summary>
        public override string Identifier => "EXECUTE_REPORT";

        /// <summary>
        /// Report Id
        /// </summary>
        [Required]
        public string ReportId { get; set; }

        /// <summary>
        /// Report Group Type
        /// </summary>
        public ReportGroupType? GroupType { get; set; }

        /// <summary>
        /// If Group Type is custom then user has to send the columns names for grouping
        /// </summary>
        public List<string> GroupByColumns { get; set; }

        /// <summary>
        /// Report Criteria
        /// </summary>
        [Required]
        public ReportCriteria Criteria { get; set; }

        /// <summary>
        /// Page Number to be returned
        /// </summary>
        [Required]
        public int PageNumber { get; set; } = 1;

        /// <summary>
        /// Page Size of the Report
        /// </summary>
        [Required]
        public int PageSize { get; set; } = 10;

        /// <summary>
        /// Report sort by
        /// </summary>
        public string SortBy { get; set; }

        /// <summary>
        /// Sort Order for report default is descending
        /// </summary>
        public bool OrderByAsc { get; set; }
    }
}
