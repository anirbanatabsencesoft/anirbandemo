﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Arguments
{
    /// <summary>
    /// The class represents the users, roles or teams a report can be shared with
    /// </summary>
    public class SharingInfo
    {
        public string SharedWith { get; set; }

        public List<long> UserIds { get; set; }
        public List<string> UserKeys { get; set; }

        public List<long> RoleIds { get; set; }
        public List<string> RoleNames { get; set; }

        public List<long> TeamIds { get; set; }
        public List<string> TeamKeys { get; set; }

    }
}
