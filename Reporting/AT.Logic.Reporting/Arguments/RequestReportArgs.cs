﻿using AbsenceSoft.Data.Enums;
using AT.Common.Core;
using AT.Logic.Reporting.Enums;
using System.ComponentModel.DataAnnotations;

namespace AT.Logic.Reporting.Arguments
{
    public class RequestReportArgs : BaseArgument
    {
        public override string Identifier => "REQUEST_REPORT";
        /// <summary>
        /// Report Identifier representing ReportDefinitionId or ReportId.
        /// </summary>
        [Required]
        public string ReportId { get; set; }

        /// <summary>
        /// Customer Id for the report
        /// </summary>
        public string CustomerId { get; set; }

        public ReportRequestType ReportRequestType { get; set; }
        /// <summary>
        /// Report Criteria
        /// </summary>
        [Required]
        public Arguments.ReportDefinition ReportDefinition { get; set; }

        /// <summary>
        /// The property is populated when requested for export otherwise null for run report.
        /// </summary>
        public ExportType? ExportFormat { get; set; }
        /// <summary>
        /// Page Number to be returned
        /// </summary>
        [Required]
        public int PageNumber { get; set; } = 1;

        /// <summary>
        /// Page Size of the Report
        /// </summary>
        [Required]
        public int PageSize { get; set; } = 10;

        /// <summary>
        /// Report sort by
        /// </summary>
        public string SortBy { get; set; }

        /// <summary>
        /// Sort Order for report default is descending
        /// </summary>
        public bool orderByAsc { get; set; }

        public int Limit { get; set; }
    }
}
