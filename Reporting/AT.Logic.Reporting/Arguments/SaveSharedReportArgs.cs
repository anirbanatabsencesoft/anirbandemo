﻿using AT.Common.Core;
using AT.Entities.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Arguments
{
    /// <summary>
    /// Arguments for saving shared report
    /// </summary>
    public class SaveSharedReportArgs : BaseArgument
    {
        public override string Identifier => "SAVE_SHARED_REPORT_ARGS";

        /// <summary>
        /// Set the id if available
        /// </summary>
        public long? Id { get; set; }

        /// <summary>
        /// Get the last execution id
        /// </summary>
        public long? LastRequestId { get; set; }

        /// <summary>
        /// Define the report name
        /// </summary>
        public string ReportName { get; set; }

        /// <summary>
        /// Shared with. Options are USER/TEAM/ROLE
        /// </summary>
        public string SharedWith { get; set; }

        /// <summary>
        /// Set the scheduled report id if shared
        /// </summary>
        public long? ScheduledReportId { get; set; }

        /// <summary>
        /// The UI Sequence
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// The argument
        /// </summary>
        public RequestReportArgs Argument { get; set; }

        /// <summary>
        /// Define the team ids
        /// </summary>
        public long[] TeamIds { get; set; }

        /// <summary>
        /// Define the team keys
        /// </summary>
        public string[] TeamKeys { get; set; }

        /// <summary>
        /// Define the role names
        /// </summary>
        public string[] RoleNames { get; set; }

        /// <summary>
        /// Define the role ids
        /// </summary>
        public long[] RoleIds { get; set; }

        /// <summary>
        /// Define the user ids
        /// </summary>
        public long[] UserIds { get; set; }

        /// <summary>
        /// Define the user mongo ids
        /// </summary>
        public string[] UserKeys { get; set; }

        /// <summary>
        /// Returns the JSON data of the criteria
        /// </summary>
        /// <returns></returns>
        public string CriteriaJson
        {
            get
            {
                return AT.Common.Core.Utilities.SerializeToJson<RequestReportArgs>(Argument);
            }
        }

        /// <summary>
        /// Check whether the argument is valid
        /// </summary>
        public override bool IsValid
        {
            get
            {
                switch (SharedWith.ToUpper())
                {
                    case SharedWithConstants.USER:
                    case SharedWithConstants.SELF:
                        if (UserKeys != null || UserIds != null)
                        {
                            return true;
                        }
                        break;
                    case SharedWithConstants.TEAM:
                        if(TeamIds != null || TeamKeys != null)
                        {
                            return true;
                        }
                        break;
                    case SharedWithConstants.ROLE:
                        if(RoleIds != null || RoleNames != null)
                        {
                            return true;
                        }
                        break;
                }
                Message = "Improper shared with";
                return false;
            }
        }
    }
}
