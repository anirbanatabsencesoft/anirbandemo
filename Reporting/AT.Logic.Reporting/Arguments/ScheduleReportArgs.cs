﻿using AT.Logic.Reporting.Schedule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Arguments
{
    /// <summary>
    /// The class represents the Arguments required for the scheduled reports to schedule & run.
    /// </summary>
    public class ScheduleReportArgs
    {
        /// <summary>
        /// It represents the report request object
        /// </summary>
        public RequestReportArgs RequestInfo { get; set; }
        /// <summary>
        /// It represents the schedule info for report
        /// </summary>
        public ScheduleInfo ScheduleInfo { get; set; }
        /// <summary>
        /// It represents the users to share the scheduled report
        /// </summary>
        public SharingInfo SharingInfo { get; set; }
    }
}
