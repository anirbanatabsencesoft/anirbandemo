﻿using AbsenceSoft.Data.Reporting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;

namespace AT.Logic.Reporting.Arguments
{
    [Serializable]
    public class AdhocExecuteReportArgs
    {
        /// <summary>
        /// The report definition id.
        /// If it's set, get the report definition from database
        /// </summary>
        public string ReportDefinitionId { get; set; }
        
        /// <summary>
        /// If it's set then use this over id
        /// </summary>
        public AbsenceSoft.Data.Reporting.ReportDefinition ReportDefinition { get; set; }

        /// <summary>
        /// Define the list of report fields.
        /// If unavailable, will use default internally
        /// </summary>
        public List<ReportField> Fields { get; set; }

        /// <summary>
        /// The limit of data to be retrieved
        /// </summary>
        public int Limit { get; set; } = 10;

        /// <summary>
        /// The offset from where the data to be retrieved
        /// </summary>
        public int Offset { get; set; } = 0;
    }
}
