﻿using AbsenceSoft.Data.Reporting;
using AT.Entities.Reporting.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Arguments
{
    public class ReportDefinition 
    {
        public long Id { get; set; }
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the type of the report.
        /// </summary>
        /// <value>
        /// The type of the report.
        /// </value>
        
        public ReportType ReportType { get; set; }

        /// <summary>
        /// Gets or sets the fields.
        /// </summary>
        /// <value>
        /// The fields.
        /// </value>
        
        public List<SavedReportField> Fields { get; set; }

        /// <summary>
        /// Gets or sets the filters.
        /// </summary>
        /// <value>
        /// The filters.
        /// </value>

        public FilterCriteria Criteria { get; set; }
        /// <summary>
        /// Report Group Type
        /// </summary>
        public ReportGroupType? GroupType { get; set; }

        /// <summary>
        /// A list of custom Group By Columns 
        /// </summary>
        public List<string> GroupByColumns { get; set; }
    }
}
