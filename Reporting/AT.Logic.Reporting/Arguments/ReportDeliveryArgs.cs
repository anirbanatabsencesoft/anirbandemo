﻿using AT.Common.Core;
using AT.Common.Core.ReportEnums;
using AT.Entities.Reporting.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Reporting.Arguments
{
    [Serializable]
    public class ReportDeliveryArgs : BaseArgument
    {
        /// <summary>
        /// Identifier or the Argument
        /// </summary>
        public override string Identifier => "DELIVER_REPORT";
        
        /// <summary>
        /// Send mail id
        /// </summary>
        public virtual string Sender { get; set; }

        /// <summary>
        /// Recipients of the mail
        /// </summary>
        public virtual string Recipients { get; set; }

        /// <summary>
        /// Subject of the mail
        /// </summary>
        public virtual string Subject { get; set; }

        /// <summary>
        /// Mail template name
        /// </summary>
        public virtual string Template { get; set; }

        /// <summary>
        /// Key values to be replaced in the template
        /// </summary>
        public virtual DeliveryParameters ReplaceKeyValue { get; set; }
    }
}