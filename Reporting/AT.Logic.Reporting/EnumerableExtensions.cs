﻿using AT.Entities.Reporting.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;

namespace AT.Logic.Reporting
{
    /// <summary>
    /// Extension class for Enumerable objects
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Group by columns name
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="elements"></param>
        /// <param name="groupSelectors"></param>
        /// <returns></returns>
        public static List<GroupResult> GroupBy<TElement>(this IEnumerable<TElement> elements, params string[] groupSelectors)
        {
            var selectors = new List<Func<TElement, object>>(groupSelectors.Length);
            foreach (var selector in groupSelectors)
            {
                LambdaExpression lambda = System.Linq.Dynamic.DynamicExpression.ParseLambda(typeof(TElement), typeof(object), selector);
                selectors.Add((Func<TElement, object>)lambda.Compile());
            }
            return elements.GroupByMany(groupSelectors,selectors.ToArray());
        }

        /// <summary>
        ///  method to create the expression and run the group by 
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="elements"></param>
        /// <param name="selectorKeys"></param>
        /// <param name="groupSelectors"></param>
        /// <returns></returns>
        private static List<GroupResult> GroupByMany<TElement>(this IEnumerable<TElement> elements, string[] selectorKeys, params Func<TElement, object>[] groupSelectors)
        {
            if (!groupSelectors.Any())
            {
                return null;
            }
            var selector = groupSelectors.First();
            var nextSelectors = groupSelectors.Skip(1).ToArray(); //reduce the list recursively until zero
            
            //this is a GroupBy Key name
            string selectorKey = selectorKeys.First();
            var nextSelectorKeys = selectorKeys.Skip(1).ToArray();

            return elements.GroupBy(selector).Select(
                  g => new GroupResult
                  {
                      Key = selectorKey, 
                      Value = g.Key.ToString(),
                      Count = g.Count(),
                      Items = g.ToList(),
                      SubGroups = g.GroupByMany(nextSelectorKeys, nextSelectors)?.ToList()
                  }).ToList();
        }
    }
}
