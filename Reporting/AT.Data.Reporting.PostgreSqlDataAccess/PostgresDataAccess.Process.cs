﻿using AT.Common.Log;
using AT.Data.Core;
using Insight.Database;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Data.Reporting.PostgreSqlDataAccess
{
    public partial class PostgresDataAccess
    {
        /// <summary>
        /// Creates a core connection for AT2
        /// </summary>
        /// <returns></returns>
        public NpgsqlConnection GetConnection()
        {
            return PostgresConnection.CreateCustomerConnection("");
        }

        /// <summary>
        /// The method calls the stored procedure to save the RequestReport
        /// </summary>
        /// <param name="args">RequestReportModel</param>
        /// <returns>RequestReportModel, populated with the new saved request id</returns>
        public async Task<Model.RequestReportModel> SaveRequestReportAsync(Model.RequestReportModel args)
        {
            using (var connection = GetConnection())
            {
                try
                {
                    return await connection.SingleAsync<Model.RequestReportModel>("public.fn_save_report_request",
                                                     new
                                                     {
                                                         rr_id = args.Id,
                                                         rr_user_id = args.UserId,
                                                         rr_user_key = args.UserKey,
                                                         rr_request_type = args.RequestType,
                                                         rr_request_message = args.RequestMessage,
                                                         rr_response_message = args.ResponseMessage,
                                                         rr_lookup_status_id = (int)args.Status,
                                                         rr_scheduled_report_id = args.ScheduledReportId,
                                                         rr_result_max_json_file_path = args.ResultMaxJsonFilePath,
                                                         rr_result_all_json_file_path = args.ResultAllJsonFilePath,
                                                         rr_result_pdf_file_path = args.ResultPdfFilePath,
                                                         rr_result_csv_file_path = args.ResultCsvFilePath,
                                                         rr_report_name = args.ReportName,
                                                         rr_criteria_plain_english = args.CriteriaPlainEnglish,
                                                         rr_report_id = args.ReportId,
                                                     },
                                                     System.Data.CommandType.StoredProcedure
                                                     );
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.InnerException);
                    return null;
                }
            }

        }

        /// <summary>
        /// The method allows fetching the report request by id
        /// </summary>
        /// <param name="id">report request id</param>
        /// <param name="userId"></param>
        /// <param name="userKey"></param>
        /// <returns>RequestReportModel</returns>
        public async Task<Model.RequestReportModel> GetRequestReportByIdAsync(long id, long? userId = null, string userKey = null)
        {
            using (var connection = GetConnection())
            {
                try
                {
                    return await connection.SingleAsync<Model.RequestReportModel>("public.fn_get_report_request", 
                                                    new
                                                    {
                                                        rr_id = id,
                                                        rr_user_id = userId,
                                                        rr_user_key = userKey
                                                    },
                                                    System.Data.CommandType.StoredProcedure
                                                    );
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.InnerException);
                    return null;
                }               
            }

        }
        public async Task<IList<Model.RequestReportModel>> GetRequestReportHistoryAsync(long? userId, string userKey, int limit=10, bool queuedOnly=false)
        {
            using (var connection = GetConnection())
            {
                try
                {
                    return await connection.QueryAsync<Model.RequestReportModel>("public.fn_get_user_report_request",
                                                    new
                                                    {
                                                        rr_user_id=userId,
                                                        rr_user_key=userKey,
                                                        rr_limit = limit,
                                                        rr_queued_request_only = queuedOnly
                                                    },
                                                    System.Data.CommandType.StoredProcedure
                                                    );
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.InnerException);
                    return null;
                }
            }

        }
        
        /// <summary>
        /// The method allows saving a schedule for a report.
        /// </summary>
        /// <param name="args">ScheduledReportModel, populated with the new saved id of the schedule report.</param>
        /// <returns>ScheduledReportModel</returns>
        public async Task<Model.ScheduledReportModel> SaveScheduleReportAsync(Model.ScheduledReportModel args)
        {
            using (var connection = GetConnection())
            {
               
                    try
                    {
                    args.Id = await connection.SingleAsync<long>("public.fn_save_scheduled_report",
                                                         new
                                                         {
                                                             sr_id = args.Id,
                                                             sr_schedule_info_json  = args.ScheduleInfoJson,
                                                             sr_next_run_date = args.NextRunDate,
                                                             sr_report_category = args.ReportCategory,
                                                             sr_report_json = args.ReportJson,
                                                             sr_customer_id = args.CustomerId,
                                                             sr_customer_key = args.CustomerKey,
                                                             sr_created_by_id = args.CreatedById,
                                                             sr_created_by_key = args.CreatedByKey,
                                                             sr_modified_by_id = args.ModifiedById,
                                                             sr_modified_by_key = args.ModifiedByKey,                                                            
                                                             sr_is_active= args.IsActive,
                                                             sr_is_deleted = args.IsDeleted
                                                         }
                                                         ,
                                                    System.Data.CommandType.StoredProcedure
                                                    );
                    return args;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    return null;
                }                
            }
        }

        /// <summary>
        /// The method allows fetching the scheduled report info by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Model.ScheduledReportModel> GetScheduledReportAsync(long id)
        {
            using (var connection = GetConnection())
            {
                try
                {
                    return await connection.SingleAsync<Model.ScheduledReportModel>("public.fn_get_scheduled_report_info",
                                                    new
                                                    {
                                                        rr_id = id
                                                    },
                                                    System.Data.CommandType.StoredProcedure
                                                    );
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.InnerException);
                    return null;
                }
            }
        }

        /// <summary>
        /// The method allows fetching the reports those are scheduled to run by today. This includes the ones which were picked but were not successful.
        /// </summary>
        /// <returns>ScheduledReportModel</returns>
        public async Task<IList<Model.ScheduledReportModel>> GetScheduledReportsToRunAsync()
        {
            using (var connection = GetConnection())
            {
                try
                {
                    return await connection.QueryAsync<Model.ScheduledReportModel>("public.fn_get_scheduled_reports_to_run");
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.InnerException);
                    return null;
                }
            }
        }

        /// <summary>
        /// The method Cancel's a requested report
        /// </summary>
        /// <param name="reportRequestId"></param>
        /// <returns></returns>
        public async Task<bool> CancelRequestReportAsync(long reportRequestId)
        {
            using (var connection = GetConnection())
            {
                try
                {
                    return await connection.ExecuteScalarAsync<bool>("public.fn_cancel_report_request",
                                                     new
                                                     {
                                                         rr_id = reportRequestId
                                                     },
                                                     System.Data.CommandType.StoredProcedure
                                                     );
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.InnerException);
                    return false;
                }
            }
        }
    }
}
