﻿using Insight.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Data.Reporting.PostgreSqlDataAccess.Model
{
    /// <summary>
    /// The class represents the model entity used for ORM with report_request table.
    /// </summary>
    public class RequestReportModel
    {
        /// <summary>
        /// The Id... PK
        /// </summary>
        [Column("id")]
        public long? Id { get; set; }
        
        /// <summary>
        /// User Id (Postgres Key) who requested it
        /// </summary>
        [Column("user_id")]
        public long? UserId { get; set; }

        /// <summary>
        /// Mongo Id of the user who requested it
        /// </summary>
        [Column("user_key")]
        public string UserKey { get; set; }

        /// <summary>
        /// Request type. Options are UI/Scheduler
        /// </summary>
        [Column("request_type")]
        public string RequestType { get; set; }

        /// <summary>
        /// The criteria JSON
        /// </summary>
        [Column("request_message")]
        public string  RequestMessage { get; set; }

        /// <summary>
        /// The response message
        /// </summary>
        [Column("response_message")]
        public string ResponseMessage { get; set; }

        /// <summary>
        /// The status
        /// </summary>
        [Column("lookup_status_id")]
        public RequestStatus Status { get; set; }

        /// <summary>
        /// If requested by scheduler then scheduled report id
        /// </summary>
        [Column("scheduled_report_id")]
        public long? ScheduledReportId { get; set; }

        /// <summary>
        /// Boolean to check if is completed
        /// </summary>
        [Column("is_completed")]
        public bool IsCompleted { get; set; }

        /// <summary>
        /// Post processing result (limited row) json path
        /// </summary>
        [Column("result_max_json_file_path")]
        public string   ResultMaxJsonFilePath { get; set; }

        /// <summary>
        /// Post processing result (all rows) json path
        /// </summary>
        [Column("result_all_json_file_path")]
        public string ResultAllJsonFilePath { get; set; }

        /// <summary>
        /// Post processing PDF file path
        /// </summary>
        [Column("result_pdf_file_path")]
        public string ResultPdfFilePath { get; set; }

        /// <summary>
        /// Post processing CSV file path
        /// </summary>
        [Column("result_csv_file_path")]
        public string ResultCsvFilePath { get; set; }

        /// <summary>
        /// Flaf to check whether is deleted
        /// </summary>
        [Column("is_deleted")]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Audit field
        /// </summary>
        [Column("created_on")]
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Audit field
        /// </summary>
        [Column("modified_on")]
        public DateTime? ModifiedOn { get; set; }

        /// <summary>
        /// Report name to show on screen
        /// </summary>
        [Column("report_name")]
        public string ReportName { get; set; }

        /// <summary>
        /// Report criteria to show on screen
        /// </summary>
        [Column("criteria_plain_english")]
        public string CriteriaPlainEnglish { get; set; }

        /// <summary>
        /// Report Id
        /// </summary>
        [Column("report_id")]
        public string ReportId { get; set; }

    }
}
