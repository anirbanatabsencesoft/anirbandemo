﻿using Insight.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Data.Reporting.PostgreSqlDataAccess.Model
{
    /// <summary>
    /// The class represents the model entity used for ORM with scheduled_report table.
    /// </summary>
    public class ScheduledReportModel
    {
        [Column("id")]
        public long? Id { get; set; }
        [Column("schedule_info_json")]
        public string ScheduleInfoJson { get; set; }
        [Column("next_run_date")]
        public DateTime NextRunDate { get; set; }
        [Column("report_category")]
        public string ReportCategory { get; set; }
        [Column("report_json")]
        public string ReportJson { get; set; }
        [Column("customer_id")]
        public long? CustomerId { get; set; }
        [Column("customer_key")]
        public string CustomerKey { get; set; }
        [Column("created_by_id")]
        public long? CreatedById { get; set; }
        [Column("created_by_key")]
        public string CreatedByKey { get; set; }
        [Column("created_on")]
        public DateTime CreatedOn { get; set; }
        [Column("modified_by_id")]
        public long? ModifiedById { get; set; }
        [Column("modified_by_key")]
        public string ModifiedByKey { get; set; }
        [Column("modified_on")]
        public DateTime? ModifiedOn { get; set; }
        [Column("is_active")]
        public bool IsActive { get; set; }
        [Column("is_deleted")]
        public bool IsDeleted { get; set; }

    }
}
