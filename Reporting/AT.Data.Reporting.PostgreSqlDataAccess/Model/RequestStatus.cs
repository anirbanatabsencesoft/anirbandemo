﻿namespace AT.Data.Reporting.PostgreSqlDataAccess.Model
{
    /// <summary>
    /// The Enum represents the statuses for a request for report at a given point of time.
    /// </summary>
    public enum RequestStatus
    {
        Pending=6301,
        Processing,
        Completed,
        Canceled,
        Failed,
        Scheduled
    }
}