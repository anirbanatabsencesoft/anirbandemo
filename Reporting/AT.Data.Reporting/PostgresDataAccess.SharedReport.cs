﻿using AT.Common.Core;
using AT.Common.Log;
using AT.Data.Core;
using AT.Data.Reporting.Arguments;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Entities;
using AT.Entities.Reporting.Models;
using Insight.Database;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Data.Reporting
{
    /// <summary>
    /// This class is intented for Postgres Data Access.
    /// This is not a provider as only Postgres is required.
    /// </summary>
    public partial class PostgresDataAccess
    {

        /// <summary>
        /// Save a shared report and it's recipients
        /// This opens up a transaction for saving all
        /// </summary>
        /// <param name="report"></param>
        /// <returns></returns>
        public async Task<SharedReport> SaveSharedReport(SharedReport report)
        {
            using (var connection = GetConnection())
            {
                connection.Open();
                using (var tx = connection.BeginTransaction())
                {
                    try
                    {
                        //save and set the id of the object
                        report.Id = connection.Single<long>("public.fn_save_shared_report",
                                                    new
                                                    {
                                                        sr_id = report.Id,
                                                        sr_report_name = report.ReportName,
                                                        sr_ui_sequence = report.Order,
                                                        sr_report_category = report.ReportCategory,
                                                        sr_report_class_fqn = report.ReportObjectFullyQualifiedName,
                                                        sr_report_json = report.CriteriaJson,
                                                        sr_shared_with = report.SharedWith,
                                                        sr_created_by_id = report.CreatedById,
                                                        sr_created_by_key = report.CreatedByKey,
                                                        sr_customer_id = report.CustomerId,
                                                        sr_customer_key = report.CustomerKey,
                                                        sr_scheduled_report_id = report.ScheduledReportId,
                                                        sr_is_active = report.IsActive,
                                                        sr_last_requested_id=report.LastRequestId
                                                    },
                                    System.Data.CommandType.StoredProcedure
                                                    );

                        NpgsqlCommand npgsqlCommand = new NpgsqlCommand("DELETE FROM shared_report_team_role_user WHERE shared_report_id = @ReportId", connection);
                        npgsqlCommand.Parameters.AddWithValue("@ReportId", report.Id);
                        npgsqlCommand.ExecuteScalar();

                        //Save the recipients
                        foreach (var srr in report.RecipientList)
                        {
                            srr.SharedReportId = report.Id.Value;
                            var resultRecipient = await SaveSharedReportRecipients(srr, connection);
                            if (resultRecipient == 0)
                            {
                                Logger.Error(string.Format("Failed saving recipients. The details: {0}", Utilities.SerializeToJson<SharedReportRecipient>(srr)));
                                tx.Rollback();
                                break;
                            }
                            else
                            {
                                srr.Id = resultRecipient;
                            }
                        }
                        tx.Commit();
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex);
                        tx.Rollback();
                    }
                }

                connection.Close();
            }

            return await Task.FromResult(report);
        }

        /// <summary>
        /// Method to save shared report recipients
        /// </summary>
        /// <param name="recipients"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        private async Task<long> SaveSharedReportRecipients(SharedReportRecipient recipients, NpgsqlConnection connection)
        {
            return await connection.SingleAsync<long>("public.fn_save_shared_report_team_role_user",
                                                    new
                                                    {
                                                        srtru_shared_report_id = recipients.SharedReportId,
                                                        srtru_team_name = recipients.TeamName,
                                                        srtru_team_id = recipients.TeamId,
                                                        srtru_team_key = recipients.TeamKey,
                                                        srtru_role_id = recipients.RoleId,
                                                        srtru_role_name = recipients.RoleName,
                                                        srtru_user_id = recipients.UserId,
                                                        srtru_user_key = recipients.UserKey
                                                    },
                                    System.Data.CommandType.StoredProcedure
                                                    );
        }

        /// <summary>
        /// Returns the shared report based on the argument
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public async Task<IList<SharedReportModel>> GetSharedReport(GetSharedReportArgs args)
        {
            using (var connection = GetConnection())
            {
                connection.Open();
                var result = await connection.QueryAsync<SharedReportModel>("public.fn_get_shared_reports",
                                                    new
                                                    {
                                                        customerid = args.CustomerId,
                                                        customerkey = args.CustomerKey,
                                                        reportid = args.ReportId,
                                                        scheduledreportid = args.ScheduledReportId,
                                                        teamname = args.TeamName,
                                                        teamid = args.TeamId,
                                                        teamkey = args.TeamKey,
                                                        roleid = args.RoleId,
                                                        rolename = args.RoleName,
                                                        userid = args.UserId == 0 ? null : args.UserId,
                                                        userkey = args.UserKey
                                                    },
                                    System.Data.CommandType.StoredProcedure
                                                    );

                connection.Close();
                return result;
            }
        }

        /// <summary>
        /// Returns the list of saved reports for the user
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public async Task<IList<SharedReportModel>> GetSavedOperationReports(GetSharedReportArgs args)
        {
            using (var connection = GetConnection())
            {
                connection.Open();
                var result = await connection.QueryAsync<SharedReportModel>("public.fn_get_saved_operation_reports",
                                                    new
                                                    {
                                                        customerid = args.CustomerId,
                                                        customerkey = args.CustomerKey,
                                                        userid = args.UserId == 0 ? null : args.UserId,
                                                        userkey = args.UserKey
                                                    },
                                    System.Data.CommandType.StoredProcedure
                                                    );

                connection.Close();
                return result;
            }
        }

        /// <summary>
        /// Deletes a shared report against it's id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<int> DeleteSharedReport(long id, long? createdById = null, string createdByKey = null)
        {
            int result = 0;
            using (var connection = GetConnection())
            {
                connection.Open();
                try
                {
                    result = await connection.ExecuteScalarAsync<int>("public.fn_delete_shared_report",
                                                        new
                                                        {
                                                            sr_id = id,
                                                            createdbyid = createdById,
                                                            createdbykey = createdByKey
                                                        },
                                        System.Data.CommandType.StoredProcedure
                                                        );
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                }
                finally
                {
                    connection.Close();
                }
                return result;
            }
        }
        
        /// <summary>
        /// Creates a core connection for AT2
        /// </summary>
        /// <returns></returns>
        public NpgsqlConnection GetConnection()
        {
            return PostgresConnection.CreateCustomerConnection(null);
        }
    }
}
