﻿using AT.Common.Core;
using AT.Entities.Reporting.Classes;
using System.ComponentModel.DataAnnotations;

namespace AT.Data.Reporting.Arguments
{
    /// <summary>
    /// Argument class to encapsulate report saving parameters
    /// </summary>
    public class SaveReportArgs : BaseArgument
    {
        /// <summary>
        /// Identifier for Save argument.
        /// </summary>
        public override string Identifier => "SAVE_REPORT_ARGS";

        /// <summary>
        /// Column definition to save
        /// </summary>
        [Required]
        public ReportColumnDefinition ColumnDefinition { get; set; }

    }


}
