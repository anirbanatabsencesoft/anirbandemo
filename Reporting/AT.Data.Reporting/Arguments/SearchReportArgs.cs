﻿using AT.Common.Core;
using AT.Entities.Reporting.ReportingConstants;

namespace AT.Data.Reporting.Arguments
{
    /// <summary>
    /// Argument class for searching a report
    /// </summary>
    public class SearchReportArgs : BaseArgument
    {
        public override string Identifier => "SEARCH_REPORT_ARGS";

        /// <summary>
        /// Set the report type
        /// </summary>
        public ReportBuilderType? ReportType { get; set; }

        /// <summary>
        /// Set this to search report by name
        /// </summary>
        public string ReportName { get; set; }
    }
}
