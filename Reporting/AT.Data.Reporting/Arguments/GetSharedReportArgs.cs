﻿using AT.Common.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Data.Reporting.Arguments
{
    /// <summary>
    /// Defines the parameters for searching shared report
    /// </summary>
    public class GetSharedReportArgs : BaseArgument
    {
        public override string Identifier => "SEARCH_SHARED_REPORT";

        /// <summary>
        /// Set the customer id for postgres if available
        /// </summary>
        public long? CustomerId { get; set; }

        /// <summary>
        /// Set the customer id of mongo if available
        /// </summary>
        public string CustomerKey { get; set; }

        /// <summary>
        /// Set the report id if want to search for that
        /// </summary>
        public long? ReportId { get; set; }

        /// <summary>
        /// Get data for scheduled reports recipients
        /// </summary>
        public long? ScheduledReportId { get; set; }

        /// <summary>
        /// Set the team name if available
        /// </summary>
        public string TeamName { get; set; }

        /// <summary>
        /// Set if Postgres Team Id is available
        /// </summary>
        public long? TeamId { get; set; }

        /// <summary>
        /// Set if Mongo Team id is available
        /// </summary>
        public string TeamKey { get; set; }

        /// <summary>
        /// Set if postgres role pk is available
        /// </summary>
        public long? RoleId { get; set; }

        /// <summary>
        /// Set if role name is available
        /// </summary>
        public string RoleName { get; set; }

        /// <summary>
        /// Set if postgres user pk is available
        /// </summary>
        public long? UserId { get; set; }

        /// <summary>
        /// Set if Mongo User PK is available
        /// </summary>
        public string UserKey { get; set; }

        /// <summary>
        /// Set the shared with
        /// </summary>
        public string SharedWith { get; set; }

        /// <summary>
        /// Overriding base method for customer id/key combination check
        /// </summary>
        /// <returns></returns>
        public override bool IsValid
        {
            get
            {
                if (CustomerId == null && string.IsNullOrWhiteSpace(CustomerKey))
                {
                    this.Message = "Either customer id or customer key has to be set";
                    return false;
                }

                return true;
            }
        }
    }
}
