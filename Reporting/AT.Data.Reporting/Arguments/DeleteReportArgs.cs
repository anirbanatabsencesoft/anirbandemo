﻿using AT.Common.Core;
using System.ComponentModel.DataAnnotations;

namespace AT.Data.Reporting.Arguments
{
    /// <summary>
    /// Arguments to be passed to delete a report
    /// </summary>
    public class DeleteReportArgs : BaseArgument
    {
        //The identifer
        public override string Identifier => "DELETE_ADHOC_REPORT";

        /// <summary>
        /// Define the adhoc report id. Use CSV for multiple ids
        /// </summary>
        [Required]
        public string AdhocReportIds { get; set; }
                
        /// <summary>
        /// Define the user id of the logged in user.
        /// </summary>
        public long? UserId { get; set; }

        /// <summary>
        /// Set the user mongo id
        /// </summary>
        public string UserKey { get; set; }
    }
}
