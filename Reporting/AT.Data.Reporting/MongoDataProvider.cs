﻿using AbsenceSoft;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Reporting;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Tasks;
using AbsenceSoft.Logic.Users;
using AbsenceSoft.Reporting;
using AT.Common.Log;
using AT.Data.Reporting.Arguments;
using AT.Data.Reporting.Base;
using AT.Data.Reporting.Model;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Classes;
using AT.Entities.Reporting.Entities;
using AT.Entities.Reporting.Enums;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AT.Data.Reporting
{
    /// <summary>
    /// Mongo Data Provider Class
    /// </summary>
    public class MongoDataProvider : BaseDataProvider
    {
        /// <summary>
        /// Get all report definitions
        /// </summary>
        /// <returns></returns>
        public override async Task<List<ReportDefinition>> GetAllDefinitions()
        {
#pragma warning disable S1125
            return await Task.FromResult(ReportDefinition.AsQueryable().Where(r => r.IsDeleted != true).ToList());
#pragma warning restore S1125
        }

        /// <summary>
        /// Get report definition by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public override async Task<ReportDefinition> GetDefinitionByName(string name)
        {
            return await Task.FromResult(ReportDefinition.AsQueryable().FirstOrDefault(a => a.Name == name));
        }

        /// <summary>
        /// Get the report columns
        /// </summary>
        /// <param name="reportTypeId"></param>
        /// <returns></returns>
        public override async Task<List<ReportField>> GetReportColumns(string reportTypeId)
        {
            var fields = ReportTypeField.AsQueryable()
                .Where(p => p.ReportTypeId == reportTypeId)
                .Select(p => p.FieldName)
                .ToList();
            var reportFields = ReportField.AsQueryable()
                .Where(f => fields.Contains(f.Name))
                .OrderBy(f => f.Id)
                .ToList();

            return await Task.FromResult(reportFields);
        }

        /// <summary>
        /// Returns all report type fields irrecpective of report id
        /// </summary>
        /// <returns></returns>
        public override async Task<List<ReportTypeField>> GetAllReportTypeFieldsAsync()
        {
            var data = ReportTypeField.AsQueryable()
                        .ToList();
            return await Task.FromResult(data);
        }

        /// <summary>
        /// Get all report fields
        /// </summary>
        /// <returns></returns>
        public override async Task<List<ReportField>> GetAllReportFieldsAsync()
        {
            var data = ReportField.AsQueryable()
                        .OrderBy(f => f.Id)
                       .ToList();
            return await Task.FromResult(data);
        }

        /// <summary>
        /// Return all report types
        /// </summary>
        /// <returns></returns>
        public override async Task<List<ReportType>> GetReportTypesAsync()
        {
            return await Task.FromResult(ReportType.AsQueryable().ToList());
        }

        /// <summary>
        /// Saves the definition of a report.
        /// </summary>
        /// <param name="reportColumnDefinition">The definition of the report or the definition of the report to duplicate.</param>
        /// <param name="currentUser">Current User</param>
        /// <param name="cloneReport">Whether to save the report under a new name and clones the existing report and assigns a new object id.</param>
        /// <returns>The report definition.</returns>
        public override async Task<ReportColumnDefinition> SaveReportDefinitionAsync(ReportColumnDefinition reportColumnDefinition, User currentUser, bool cloneReport = false)
        {

            if (reportColumnDefinition == null)
            {
                throw new ArgumentNullException("reportColumnDefinition");
            }
            ReportDefinition definition = MapToMongoDefinition(reportColumnDefinition, currentUser, cloneReport);
            ReportDefinition newDefinition = await Task.FromResult(definition.Save());

            return await Task.FromResult(MapToReportDefinition(newDefinition, reportColumnDefinition));
        }

        /// <summary>
        /// this method map the entity to the Mongo Entitiy
        /// </summary>
        /// <param name="reportColumnDefinition">The definition of the report or the definition of the report to duplicate.</param>
        /// <param name="currentUser">Current User</param>
        /// <param name="cloneReport">Whether to save the report under a new name and clones the existing report and assigns a new object id.</param>
        /// <returns>Returns Mongo specific entity</returns>
        private ReportDefinition MapToMongoDefinition(ReportColumnDefinition reportColumnDefinition, User currentUser, bool cloneReport)
        {
            ReportDefinition definition;
            if (!cloneReport)
            {
                ReportType type = ReportType.AsQueryable().FirstOrDefault(r => r.AdHocReportType == reportColumnDefinition.AdHocReportType);
                definition = new ReportDefinition(type);
                definition.ReportType.Id = type.Id;
            }
            else
            {
                if (string.IsNullOrWhiteSpace(reportColumnDefinition.Id))
                {
                    throw new ArgumentNullException("reportColumnDefinition");
                }
                definition = ReportDefinition.GetById(reportColumnDefinition.Id);
                definition.Clean();

            }
            definition.CustomerId = currentUser.CustomerKey;
            definition.Name = reportColumnDefinition.Name;
            definition.UserId = currentUser.Key;

            if (reportColumnDefinition.Fields != null)
            {
                definition.Fields = reportColumnDefinition.Fields.Select(f => new SavedReportField { Name = f.Name, Order = f.Order }).ToList();
            }
            if (reportColumnDefinition.Filters != null)
            {
                definition.Filters = reportColumnDefinition.Filters.Select(f => new ReportFilter { FieldName = f.FieldName, Operator = ((int)f.OperatorType).ToString(), Value = f.Value }).ToList();
            }

            return definition;
        }

        /// <summary>
        /// This method converts mongo entity to given entity.
        /// </summary>
        /// <param name="newDefinition">Mongo specific report definition</param>
        /// <param name="reportColumnDefinition">The definition of the report.</param>
        /// <returns>Returns report definition.</returns>
        public ReportColumnDefinition MapToReportDefinition(ReportDefinition newDefinition, ReportColumnDefinition reportColumnDefinition)
        {
            reportColumnDefinition.Id = newDefinition.Id;
            reportColumnDefinition.Active = true;
            if (reportColumnDefinition.Fields == null)
            {
                reportColumnDefinition.Fields = newDefinition.Fields.Select(f => new AdHocReportField { Name = f.Name, Order = f.Order }).ToList();
            }
            if (reportColumnDefinition.Filters == null)
            {
                reportColumnDefinition.Filters = newDefinition.Filters?.Select(f => new AdHocReportFilter { FieldName = f.FieldName, Value = f.Value.ToString(), OperatorType = (OperatorType)int.Parse(f.Operator) }).ToList();
            }

            return reportColumnDefinition;
        }

        /// <summary>
        /// Returns the user data required for report operation
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public override async Task<UserReportCriteriaData> GetUserReportCriteriaData(UserReportCriteriaData data)
        {
            //get the old user object
            data.SecurityUser = AbsenceSoft.Data.Security.User.GetById(data.AuthenticatedUser.Key);

            //get the customer
            data.Customer = await Task.FromResult(Customer.AsQueryable().FirstOrDefault(c => c.Id == data.AuthenticatedUser.CustomerKey));

            //get the employers
            var employerIds = data.AuthenticatedUser.Employers == null ? new string[0] : data.AuthenticatedUser.Employers.Select(e => e.EmployerId).ToArray();
            data.Employers = Employer.AsQueryable(data.SecurityUser).Where(e => employerIds.Contains(e.Id)).ToList();

            //get absence reasons
            data.AbsenceReasons = await Task.FromResult(AbsenceReason.AsQueryable(data.SecurityUser)
                                                .Where(a => a.EmployerId == null || employerIds.Contains(a.EmployerId))
                                                .OrderBy(a => a.Category)
                                                .ThenBy(a => a.Name)
                                                .ToList());

            //Set locations
            data.Locations = await Task.FromResult(Organization.AsQueryable(data.SecurityUser).Where(c => c.CustomerId == data.AuthenticatedUser.CustomerKey
                                                                                                && c.TypeCode == OrganizationType.OfficeLocationTypeCode)
                                                                                              .ToList());

            //Set accommodation types
            data.AccommodationTypes = await Task.FromResult(AccommodationType.DistinctFind(null, data.SecurityUser.CustomerId, data.SecurityUser.EmployerId).AsQueryable().Where(a => !a.PreventNew).ToList());


            //Set the assigned to user list
            var assignedToUserIds = await Task.FromResult(Case.Repository.Collection.Distinct<BsonObjectId>("AssignedToId",
                                            Case.Query.And(
                                                Case.Query.IsNotDeleted(),
                                                Case.Query.EQ(c => c.CustomerId, data.SecurityUser.CustomerId),
                                                Case.Query.NE(c => c.AssignedToId, null))).ToList());
            if (assignedToUserIds.Any())
            {
                data.AssignedToList = await Task.FromResult(AbsenceSoft.Data.Security.User.Query.Find(Query.In("_id", assignedToUserIds)).ToList());
            }
            
            //set the diagnosis codes
            var dcEmployerList = await Task.FromResult(new EmployerService().Using(s => s.GetEmployersForUser(data.SecurityUser)).Select(e => e.Id).ToArray());
            var dcCaseList = await Task.FromResult(Case.AsQueryable().Where(c =>
                                c.CustomerId == data.SecurityUser.CustomerId
                                && (c.EmployerId == null || dcEmployerList.Contains(c.EmployerId))
                                && c.Disability.PrimaryDiagnosis.Code != null)
                                .ToList());

            data.DiagnosisCodes = new Dictionary<string, string>(dcCaseList.Count);
            foreach (var c in dcCaseList)
            {
                if (!data.DiagnosisCodes.ContainsKey(c.Disability.PrimaryDiagnosis.Code))
                {
                    data.DiagnosisCodes.Add(c.Disability.PrimaryDiagnosis.Code, c.Disability.PrimaryDiagnosis.UIDescription);
                }
            }
            
            //get the organization types
            data.OrganizationTypes = await Task.FromResult(GetOrganizationTypeCriteria(data.SecurityUser, employerIds.ToList()));


            //return
            return data;
        }

        /// <summary>
        /// Returns the to-do assigned list
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public override async Task<List<AbsenceSoft.Data.Security.User>> GetToDoAssignedList(AbsenceSoft.Data.Security.User user)
        {
            List<AbsenceSoft.Data.Security.User> lst = new List<AbsenceSoft.Data.Security.User>();
            //set the to-do assignee list
            try
            {
                var todoAssignedToUserIds = await Task.FromResult(Case.Repository.Collection.Distinct<BsonObjectId>("AssignedToId", ToDoItem.Query.And(ToDoItem.Query.IsNotDeleted(),
                                                                    ToDoItem.Query.EQ(c => c.CustomerId, user.CustomerId))).ToList());
                if (todoAssignedToUserIds.Any())
                {
                    lst = await Task.FromResult(AbsenceSoft.Data.Security.User.Query.Find(Query.In("_id", todoAssignedToUserIds)).ToList());
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return lst;
        }


        /// <summary>
        /// Gets Case List for Disability Report
        /// </summary>
        /// <param name="data"></param>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <returns></returns> 
        public override async Task<List<Case>> GetCaseListForDisabilityReport(UserReportCriteriaData data, ReportCriteria criteria, string employerIdFilterName, int pageNumber = 1, int pageSize = 10)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();
            IList<IMongoQuery> ands = new List<IMongoQuery>();
            if (data.SecurityUser != null)
            {
                ands.Add(data.SecurityUser.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport, "Employee._id"));
            }
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
            {
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            }
            ApplyEmployerFilter<Case>(ands, criteria, employerIdFilterName);

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(Date.BuildDatesOverlapRangeQuery(startDate, endDate));

            // Must have an approved period for an STD policy within the start and end date
            ands.Add(Case.Query.ElemMatch(c =>
                c.Segments, s => s.ElemMatch(e =>
                    e.AppliedPolicies, p => Query.And(
                        p.EQ(o => o.Policy.PolicyType, PolicyType.STD),
                        p.ElemMatch(r => r.Usage, u => Query.And(
                            u.EQ(h => h.Determination, AdjudicationStatus.Approved),
                            Query.And(u.GTE(e => e.DateUsed, new BsonDateTime(startDate)), u.LTE(e => e.DateUsed, new BsonDateTime(endDate)))
                        ))
                    )
                )
            ));

            // Do we have filters outside of the typical? Yes? ok, great!
            if (criteria.Filters.Any())
            {
                // Add the diagnosis code filter if any codes were selected
                var diag = criteria.Filters.FirstOrDefault(m => m.Name == "DiagnosisCode");
                if (diag != null && diag.Values != null && diag.Values.Any())
                {
                    var deez = diag.ValuesAs<string>().Where(d => !string.IsNullOrWhiteSpace(d)).ToList();
                    if (deez.Any())
                    {
                        ands.Add(Case.Query.In(e => e.Disability.PrimaryDiagnosis.Code, deez.Distinct()));
                    }
                }

                // Add the work state filter if provided
                var ws = criteria.Filters.FirstOrDefault(m => m.Name == "WorkState");
                if (ws != null && ws.Value != null)
                {
                    ands.Add(Case.Query.EQ(e => e.Employee.WorkState, ws.Value));
                }

                AppendOrganizationFilterQuery(data.SecurityUser, criteria, employerIdFilterName, queries: ands.ToList(), employeeIdField: "Employee._id");
            }

            // Create the Query and execute the Find against the DB
            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null);
            return await Task.FromResult(query.ToList());
        }

        /// <summary>
        /// Applies the employer filter.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="query">The query.</param>
        /// <param name="criteria">The criteria.</param>
        /// <param name="employerIdFilterName"></param>
        private void ApplyEmployerFilter<TEntity>(ICollection<IMongoQuery> query, ReportCriteria criteria, string employerIdFilterName) where TEntity : BaseEmployerEntity<TEntity>, new()
        {
            var employerIdFilter = criteria.Filters.FirstOrDefault(m => m.Name == employerIdFilterName);
            if (employerIdFilter != null && !string.IsNullOrEmpty(employerIdFilter.Value?.ToString()))
            {
                query.Add(BaseEmployerEntity<TEntity>.Query.EQ(c => c.EmployerId, employerIdFilter.Value.ToString()));
            }
            else if (employerIdFilter != null && employerIdFilter.Values != null && employerIdFilter.Values.Any())
            {
                var val = employerIdFilter.ValuesAs<string>();
                if (val != null && val.Any())
                {
                    query.Add(BaseEmployerEntity<TEntity>.Query.In(c => c.EmployerId, val));
                }
            }
        }

        /// <summary>
        /// Set the To-do criteria data for the user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public override async Task<Dictionary<ToDoItemType, string>> GetTodoItemTypes(AbsenceSoft.Data.Security.User user)
        {
            var todoItemTypes = new Dictionary<ToDoItemType, string>();
            var sync = new object();

            if (user.Employers != null && user.Employers.Count > 0)
            {
                Parallel.ForEach(user.Employers, (emp) =>
                {
                    try
                    {
                        using (var svc = new ToDoService())
                        {
                            var todotypes = svc.GetToDoTypes(emp.EmployerId, user);
                            lock (sync)
                            {
                                todoItemTypes = todoItemTypes.Concat(todotypes).GroupBy(d => d.Key).ToDictionary(d => d.Key, d => d.First().Value);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex);
                    }
                });
            }

            return await Task.FromResult(todoItemTypes);
        }

        /// <summary>
        /// method to get the organization type and respective organizations
        /// </summary>
        /// <param name="user"></param>
        /// <param name="employerIds"></param>
        /// <returns></returns>
        private List<OrganizationByType> GetOrganizationTypeCriteria(AbsenceSoft.Data.Security.User user, List<string> employerIds)
        {
            //get the organization types for the customer
            List<OrganizationByType> result = OrganizationType.Query.Find(OrganizationType.Query.And(new List<IMongoQuery>()
            {
                OrganizationType.Query.EQ(o => o.CustomerId, user.CustomerId),
                OrganizationType.Query.In(o => o.EmployerId, employerIds)
            })).Select(p => new OrganizationByType() { OrganizationType = p }).ToList();

            //set location type
            if (!result.Any(type => type.OrganizationType.Code.ToUpper() == OrganizationType.OfficeLocationTypeCode.ToUpper()))
            {
                result.Insert(0, new OrganizationByType() { OrganizationType = new OrganizationType() { Code = OrganizationType.OfficeLocationTypeCode, Name = "Location" } });
            }

            //set the organizations belongs to that type
            var query = Organization.Query.And(new List<IMongoQuery>()
            {
                Organization.Query.EQ(o => o.CustomerId, user.CustomerId),
                Organization.Query.In(o => o.EmployerId, employerIds),
                Organization.Query.In(o => o.TypeCode, result.Select(i => i.OrganizationType.Code))
            });

            var orgs = Organization.Query.Find(query).SetFields(Organization.Query.IncludeFields(o => o.TypeCode, o => o.Code, o => o.Name)).ToList();

            //iterate and add the organizations
            foreach (var item in result)
            {
                item.Organizations = orgs.Where(t => t.TypeCode == item.OrganizationType.Code).OrderBy(t => t.Name).ToList();
            }

            return result;
        }

        /// <summary>
        /// Returns the work state for a user
        /// </summary>
        /// <param name="data"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public async override Task<List<string>> GetUserWorkState(UserReportCriteriaData data, DateTime startDate, DateTime endDate)
        {
            DateTime filterStartDate = startDate.ToMidnight();
            DateTime filterEndDate = endDate.ToMidnight();

            if (Case.Count() > 0)
            {
                // IQueryable doesn't support string.IsNullOrWhiteSpace while query building, so used !=null
                List<string> userWorkState = Case.AsQueryable(data.SecurityUser).Where(e => e.Employee != null && e.Employee.WorkState != null &&
                                  ((e.StartDate >= filterStartDate && e.StartDate <= filterEndDate) || (e.EndDate >= filterStartDate && e.EndDate <= filterEndDate)))
                                   ?.Select(e => e.Employee.WorkState)
                                   .Distinct()
                                   .ToList();

                return await Task.FromResult(userWorkState);
            }
            else
            {
                return new List<string>();
            }
        }

        /// <summary>
        /// Returns the custom fields available for criteria for the user
        /// </summary>
        /// <param name="data"></param>
        /// <param name="fieldType"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public async override Task<List<AbsenceSoft.Data.Customers.CustomField>> GetUserCustomField(UserReportCriteriaData data, CustomFieldType? fieldType = null, EntityTarget? target = null)
        {
            var query = AbsenceSoft.Data.Customers.CustomField.AsQueryable(data.SecurityUser).Where(c => c.CustomerId == data.SecurityUser.CustomerId);
            if (fieldType.HasValue)
            {
                query = query.Where(f => f.DataType == fieldType.Value);
            }
            if (target.HasValue)
            {
                query = query.Where(f => f.Target == target.Value);
            }
            return await Task.FromResult(query.Distinct(c => c.Name).ToList());
        }

        /// <summary>
        /// Gets the EmployerAudit CaseNote for given User
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async override Task<IEnumerable<EmployerAudit<CaseNote>>> GetEmployerAuditCase(UserReportCriteriaData user, int pageNumber, int pageSize)
        {
            return await Task.FromResult(EmployerAudit<CaseNote>.AsQueryable(user.SecurityUser).OrderBy(m => m.Value.CaseId).ThenBy(m => m.Value.Id).Skip((pageNumber - 1) * pageSize).Take(pageSize));
        }

        /// <summary>
        /// Get the Case Notes by Case ids
        /// </summary>
        /// <param name="user"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async override Task<IList<CaseNote>> GetCaseNotesbyCaseIdsAsync(IList<string> caseIds)
        {
            var caseNotes = CaseNote.AsQueryable().Where(n => caseIds.Contains(n.CaseId)).Where(x => x.Categories.Any(r => r.CategoryCode.ToUpper() == "HRCONVERSATION" || r.CategoryCode.ToUpper() == "INTERACTIVEPROCESS"));
            return await Task.FromResult(caseNotes.ToList());
        }

        /// <summary>z
        /// Returns the Case for given CaseId
        /// </summary>      
        /// <param name="caseIds"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async override Task<List<Case>> GetCaseById(List<string> caseIds, int pageNumber, int pageSize)
        {
            return await Task.FromResult(Case.AsQueryable().Where(p => caseIds.Contains(p.Id)).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList());
        }

        /// <summary>
        /// Gets the list of case for the passed parameter query
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async override Task<List<Case>> GetCase(IMongoQuery query, int pageNumber, int pageSize)
        {
            return await Task.FromResult(Case.Query.Find(query).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList());
        }

        /// <summary>
        /// Gets the Reasons for Case excluding Accommodation
        /// </summary>
        /// <returns></returns>
        public async override Task<List<string>> GetCaseReasonsExcludingAccommodation()
        {
            return await Task.FromResult(Case.AsQueryable().Where(m => !m.Reason.Code.Contains("Accommodation")).Select(m => m.Reason.Name).Distinct().ToList());
        }

        /// <summary>
        /// Gets the WorkState of Cases of a user
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async override Task<List<string>> GetCaseWorkState(UserReportCriteriaData data)
        {
            return await Task.FromResult(Case.AsQueryable(data.SecurityUser).Select(m => m.Employee.WorkState).Distinct().ToList());
        }

        /// <summary>
        /// Gets the User list for given UserIds
        /// </summary>
        /// <param name="userIds"></param>
        /// <returns></returns>
        public async override Task<IEnumerable<AbsenceSoft.Data.Security.User>> GetUsersById(List<string> userIds)
        {
            return await Task.FromResult(AbsenceSoft.Data.Security.User.AsQueryable().Where(m => userIds.Contains(m.Id)));
        }

        /// <summary>
        /// Gets the User for given UserId
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async override Task<AbsenceSoft.Data.Security.User> GetSecurityUserById(User user)
        {
            return await Task.FromResult(AbsenceSoft.Data.Security.User.GetById(user.Key));
        }

        /// <summary>
        /// Returns the recently used adhoc reports
        /// </summary>
        /// <param name="data"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public async override Task<List<ReportDefinition>> GetRecentReportDefinitionList(UserReportCriteriaData data, int limit = 10)
        {
            return await Task.FromResult(ReportDefinition.AsQueryable(data.SecurityUser)
                                                        .OrderByDescending(p => p.ModifiedDate)
                                                        .Take(limit).ToList());
        }

        /// <summary>
        /// Build and run Absence status report query 
        /// </summary>
        /// <param name="user">Current user</param>
        /// <param name="reportCriteria">Report criteria</param>        
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async override Task<List<Case>> RunAbsenceReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
            {
                ands.Add(user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport, "Employee._id"));
             
            }
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
            {
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            }

            ApplyEmployerFilter<Case>(ands, criteria, employerIdFilterName);

            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
            ));

            if (criteria.Filters.Count != 0)
            {
                ApplyAbsenceReportCriteria(criteria, ands);
                AppendOrganizationFilterQuery(user, criteria, employerIdFilterName, queries: ands, employeeIdField: "Employee._id");
            }
            if (string.IsNullOrWhiteSpace(sortBy))
            {
                sortBy = "_id";
            }
            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null)?.SetSortOrder(orderByAsc ? SortBy.Ascending(sortBy) : SortBy.Descending(sortBy)).SetLimit(pageSize).SetSkip((pageNumber - 1) * pageSize);
            return await Task.FromResult(query.ToList());

        }

        /// <summary>
        /// Apply the required criteria for Absence Status
        /// </summary>
        /// <param name="criteria">Report Criteria</param>
        /// <param name="ands">The query</param>
        private void ApplyAbsenceReportCriteria(ReportCriteria criteria, List<IMongoQuery> ands)
        {
            if (criteria.Filters.FirstOrDefault(m => m.Name == "CaseStatus") != null && !string.IsNullOrWhiteSpace(criteria.Filters.FirstOrDefault(m => m.Name == "CaseStatus").Value?.ToString()) && Enum.TryParse<CaseStatus>(criteria.Filters.FirstOrDefault(m => m.Name == "CaseStatus").Value.ToString(), out CaseStatus caseStatus))
            {
                ands.Add(Case.Query.EQ(e => e.Status, caseStatus));
            }

            if (criteria.Filters.FirstOrDefault(m => m.Name == "CaseType") != null && !string.IsNullOrWhiteSpace(criteria.Filters.FirstOrDefault(m => m.Name == "CaseType").Value?.ToString()) && Enum.TryParse<CaseType>(criteria.Filters.FirstOrDefault(m => m.Name == "CaseType").Value.ToString(), out CaseType caseType))
            {
                ands.Add(Case.Query.EQ(e => e.Segments[0].Type, caseType));
            }

            if (criteria.Filters.FirstOrDefault(m => m.Name == "Determination") != null && !string.IsNullOrWhiteSpace(criteria.Filters.FirstOrDefault(m => m.Name == "Determination").Value?.ToString()) && Enum.TryParse<AdjudicationStatus>(criteria.Filters.FirstOrDefault(m => m.Name == "Determination").Value.ToString(), out AdjudicationStatus adjStatusType))
            {
                ands.Add(Case.Query.EQ(e => e.Summary.Determination, adjStatusType));
            }

            if (criteria.Filters.FirstOrDefault(m => m.Name == "AbsenceReason") != null && !string.IsNullOrWhiteSpace(criteria.Filters.FirstOrDefault(m => m.Name == "AbsenceReason").Value?.ToString()))
            {
                ands.Add(Case.Query.EQ(e => e.Reason.Id, criteria.Filters.FirstOrDefault(m => m.Name == "AbsenceReason").Value.ToString()));
            }

            if (criteria.Filters.FirstOrDefault(m => m.Name == "WorkState") != null && !string.IsNullOrWhiteSpace(criteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value?.ToString()))
            {
                ands.Add(Case.Query.EQ(e => e.Employee.WorkState, criteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value.ToString()));
            }
        }

        /// <summary>
        /// Build and run Accommodation status report query 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async override Task<List<Case>> RunAccommodationReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            List<IMongoQuery> ands = CreateAccommodationQuery(user, criteria, employerIdFilterName);
            if (string.IsNullOrWhiteSpace(sortBy))
            {
                sortBy = "_id";
            }
            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null)?.SetSortOrder(orderByAsc ? SortBy.Ascending(sortBy) : SortBy.Descending(sortBy)).SetLimit(pageSize).SetSkip((pageNumber - 1) * pageSize);
            return await Task.FromResult(query.ToList());

        }       

        /// <summary>
        /// Build and run Accommodation status report query 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async override Task<List<Case>> RunCustomAccomDetailReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            List<IMongoQuery> ands = CreateAccommodationQuery(user, criteria, employerIdFilterName, true);
            if (string.IsNullOrWhiteSpace(sortBy))
            {
                sortBy = "_id";
            }
            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null)?.SetSortOrder(orderByAsc ? SortBy.Ascending(sortBy) : SortBy.Descending(sortBy)).SetLimit(pageSize).SetSkip((pageNumber - 1) * pageSize);
            return await Task.FromResult(query.ToList());

        }

        /// <summary>
        /// Build and run custome field for custom report report query 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async override Task<List<Case>> RunCustomFieldReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
            {
                ands.Add(user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport, "Employee._id"));
            }
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
            {
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            }
                ands.Add(Case.Query.EQ(e => e.IsAccommodation, true));
            ands.Add(Case.Query.Exists(e => e.AccommodationRequest));
            ands.Add(Case.Query.Exists(e => e.AccommodationRequest.Accommodations));

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
            ));
            if (string.IsNullOrWhiteSpace(sortBy))
            {
                sortBy = "_id";
            }
            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null)?.SetSortOrder(orderByAsc ? SortBy.Ascending(sortBy) : SortBy.Descending(sortBy)).SetLimit(pageSize).SetSkip((pageNumber - 1) * pageSize);
            return await Task.FromResult(query.ToList());
        }



        /// <summary>
        /// Create the Base accommodation query
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="isCustomReport"></param>
        /// <returns></returns>
        private List<IMongoQuery> CreateAccommodationQuery(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, bool isCustomReport=false)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
            {
                ands.Add(user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport, "Employee._id"));
            }
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
            {
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            }
            if (!isCustomReport)
            {
                ApplyEmployerFilter<Case>(ands, criteria, employerIdFilterName);
            }
            ands.Add(Case.Query.EQ(e => e.IsAccommodation, true));
            ands.Add(Case.Query.Or(
               Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
               Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
           ));

            if (criteria.Filters.Count != 0)
            {
                ApplyAccomodationReportCriteria(criteria, ands, isCustomReport);
                if (!isCustomReport)
                {
                    AppendOrganizationFilterQuery(user, criteria, employerIdFilterName, queries: ands, employeeIdField: "Employee._id");
                }
            }

            return ands;
        }

        /// <summary>
        /// Build and run Custom Accommodation status report query 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async override Task<List<Case>> RunCustomAccomodationReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
            {
                ands.Add(user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport, "Employee._id"));
            }
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
            {
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            }
            ands.Add(Case.Query.EQ(e => e.IsAccommodation, true));
            ands.Add(Case.Query.GTE(c => c.CreatedDate, new BsonDateTime(startDate)));
            ands.Add(Case.Query.LTE(c => c.CreatedDate, new BsonDateTime(endDate)));

            if (criteria.Filters.Count != 0)
            {
                ApplyAccomodationReportCriteria(criteria, ands, true);

                var locationFilter = criteria.Filters.FirstOrDefault(m => m.Name == "Location");
                if (locationFilter != null && locationFilter.Values != null && locationFilter.Values.Any())
                {
                    var val = locationFilter.ValuesAs<string>();
                    if (val != null && val.Any())
                    {
                        ands.Add(Case.Query.In(e => e.CurrentOfficeLocation, val));
                    }
                }
            }
            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null);
            return await Task.FromResult(query.ToList());

        }

        /// <summary>
        /// Apply the required criteria for Accommodation Status report
        /// </summary>
        /// <param name="criteria">Report Criteria</param>
        /// <param name="ands">The query</param>
        /// <param name="isCustomReport">Determines if custom Report</param>
        private void ApplyAccomodationReportCriteria(ReportCriteria criteria, List<IMongoQuery> ands, bool isCustomReport = false)
        {
            if (criteria.Filters.FirstOrDefault(m => m.Name == "CaseStatus") != null && !string.IsNullOrWhiteSpace(criteria.Filters.FirstOrDefault(m => m.Name == "CaseStatus").Value?.ToString()) && Enum.TryParse<CaseStatus>(criteria.Filters.FirstOrDefault(m => m.Name == "CaseStatus").Value.ToString(), out CaseStatus caseStatus))
            {
                ands.Add(Case.Query.EQ(e => e.Status, caseStatus));
            }

            List<IMongoQuery> elemMatchAnds = new List<IMongoQuery>();

            if (criteria.Filters.FirstOrDefault(m => m.Name == "Determination") != null && !string.IsNullOrWhiteSpace(criteria.Filters.FirstOrDefault(m => m.Name == "Determination").Value?.ToString()) && Enum.TryParse<AdjudicationStatus>(criteria.Filters.FirstOrDefault(m => m.Name == "Determination").Value.ToString(), out AdjudicationStatus adjStatusType))
            {
                elemMatchAnds.Add(Query<Accommodation>.EQ(a => a.Determination, adjStatusType));
            }

            if (criteria.Filters.FirstOrDefault(m => m.Name == "AccomType") != null && !string.IsNullOrWhiteSpace(criteria.Filters.FirstOrDefault(m => m.Name == "AccomType").Value?.ToString()))
            {
                elemMatchAnds.Add(Query<Accommodation>.EQ(a => a.Type.Code, criteria.Filters.FirstOrDefault(m => m.Name == "AccomType").Value.ToString()));
            }

            if (elemMatchAnds.Count > 0)
            {
                ands.Add(Case.Query.ElemMatch(c => c.AccommodationRequest.Accommodations, q => q.And(elemMatchAnds)));
            }
            if (criteria.Filters.FirstOrDefault(m => m.Name == "WorkState") != null && !string.IsNullOrWhiteSpace(criteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value?.ToString()))
            {
                ands.Add(Case.Query.EQ(e => e.Employee.WorkState, criteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value.ToString()));
            }

            if (criteria.Filters.FirstOrDefault(m => m.Name == "CaseAssignee") != null && !string.IsNullOrWhiteSpace(criteria.Filters.FirstOrDefault(m => m.Name == "CaseAssignee").Value?.ToString()))
            {
                ands.Add(Case.Query.EQ(e => e.AssignedToId, criteria.Filters.FirstOrDefault(m => m.Name == "CaseAssignee").Value.ToString()));
            }

            if (!isCustomReport && criteria.Filters.FirstOrDefault(m => m.Name == "ReasonCode") != null && !string.IsNullOrWhiteSpace(criteria.Filters.FirstOrDefault(m => m.Name == "ReasonCode").Value?.ToString()))
            {
                ands.Add(Case.Query.EQ(c => c.Reason.Code, criteria.Filters.FirstOrDefault(m => m.Name == "ReasonCode").Value.ToString()));
            }

        }

        #region Global/Common Query Builders
        /// <summary>
        ///  Appends Organization Filters
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="queries"></param>
        /// <param name="employeeIdField"></param>
        private void AppendOrganizationFilterQuery(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, List<IMongoQuery> queries, string employeeIdField = null)
        {
            var orgTypeFilter = criteria.Filters.Where(m => m.Name.Contains("OrganizationType") && m.Values?.Count > 0);

            employeeIdField = employeeIdField ?? "EmployeeId";

            var employerIdFilter = criteria.Filters.FirstOrDefault(m => m.Name == employerIdFilterName && m.Values!=null && m.Values.Count>0);
            if (employerIdFilter != null && employerIdFilter.Options != null && employerIdFilter.Options.Any())
            {
                foreach (var emplrId in employerIdFilter.Values)
                {
                    AddEmployeesForOrganizationFilter(user, employeeIdField, emplrId.ToString(), orgTypeFilter, queries);
                }
            }
            else
            {
                AddEmployeesForOrganizationFilter(user, employeeIdField,null, orgTypeFilter, queries);
            }
            
        }

        /// <summary>
        /// Add employees to the query for the organization filter 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="employeeIdField"></param>
        /// <param name="employerId"></param>
        /// <param name="orgTypeFilter"></param>
        /// <param name="queries"></param>
        private void AddEmployeesForOrganizationFilter(AbsenceSoft.Data.Security.User user, string employeeIdField, string employerId,IEnumerable<ReportCriteriaItem> orgTypeFilter, List<IMongoQuery> queries)
        {
            foreach (var type in orgTypeFilter)
            {
                foreach (var orgVal in type.Values)
                {

                    var empOrgs = EmployeeOrganization.AsQueryable(user).Where(org => org.CustomerId == user.CustomerId);
                    if (employerId != null)
                    {
                        empOrgs= empOrgs.Where(org => org.EmployerId == employerId);
                    }                   
                    empOrgs= empOrgs.Where(org => org.Path == orgVal.ToString().Replace(",","/"));
                   
                    var empOrgsList = empOrgs.ToList();
                    if (empOrgsList.Count > 0)
                    {
                        List<string> emps = empOrgsList.Where(org => org.TypeCode.ToUpper().Trim() != OrganizationType.OfficeLocationTypeCode.ToUpper().Trim() || org.Dates == null || org.Dates.IncludesToday(true)).Select(org => org.EmployeeNumber).ToList();
                        List<IMongoQuery> ands = new List<IMongoQuery>();
                        var empQuery = Employee.AsQueryable(user).Where(emp => emp.CustomerId == user.CustomerId);
                        if (employerId != null)
                        {
                            empQuery= empQuery.Where(emp => emp.EmployerId == employerId);
                        }
                        
                        empQuery.Where(emp => emps.Contains(emp.EmployeeNumber));
                        

                        ands.Add(Query.In(employeeIdField, empQuery.Select(emp => new BsonObjectId(new ObjectId(emp.Id))).ToList()));

                        queries.Add(Query.And(ands));
                    }

                }
            }
        }

        #endregion

        /// <summary>
        /// If this is called with a current case, then when it calculates per case usage it will only
        /// use the current case. If the current case is null then it will return all the usage
        /// </summary>
        /// <param name="emp">The emp.</param>
        /// <param name="projectedToDate">The projected to date.</param>
        /// <param name="currentCase">The current case.</param>
        /// <param name="includePerCaseUse">if set to <c>true</c> [include per case use].</param>
        /// <param name="policyCodes">The policy codes.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">Employee may not be null</exception>
        private List<Entities.Reporting.Classes.PolicySummary> GetEmployeePolicySummary(Employee employee, DateTime projectedToDate, Case currentCase, bool includePerCaseUse, params string[] policyCodes)
        {
            if (employee == null)
            {
                throw new ArgumentNullException("employee", "Employee may not be null");
            }
            DateTime projectedDate = projectedToDate.ToMidnight();
            using (var svc = new CaseService())
            {
                return svc.GetEmployeePolicySummary(employee, projectedDate, currentCase, includePerCaseUse, policyCodes)
                    .Select(p => new Entities.Reporting.Classes.PolicySummary
                    {
                        AbsenceReason = p.AbsenceReason,
                        AbsenceReasonId = p.AbsenceReasonId,
                        ExcludeFromTimeConversion = p.ExcludeFromTimeConversion,
                        HoursRemaining = p.HoursRemaining,
                        HoursUsed = p.HoursUsed,
                        PolicyCode = p.PolicyCode,
                        PolicyName = p.PolicyName,
                        PolicyType = p.PolicyType,
                        TimeRemaining = p.TimeRemaining,
                        TimeUsed = p.TimeUsed,
                        Units = p.Units
                    }).ToList();
            }
        }

        /// <summary>
        /// Get the Employee policy summary by case id
        /// </summary>
        /// <param name="caseId">Case identifier</param>
        /// <param name="includeStatus">Other eligibility statuses</param>
        /// <param name="policyCodes">Policy codes</param>
        /// <returns></returns>
        public async override Task<List<Entities.Reporting.Classes.PolicySummary>> GetEmployeePolicySummaryByCaseIdAsync(string caseId, EligibilityStatus[] includeStatus, params string[] policyCodes)
        {
            Case theCase = Case.GetById(caseId);
            if (theCase == null)
            {
                throw new ArgumentNullException("caseId");
            }
            var casePolicies = (theCase.Segments ?? new List<CaseSegment>(0))
            .SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>(0))
            .Where(p => includeStatus != null && includeStatus.Any() && includeStatus.Contains(p.Status))
            .Where(p => p.Policy != null && (policyCodes.Length == 0 || policyCodes.Contains(p.Policy.Code)))
            .Select(p => p.Policy.Code)
            .ToArray();

            return await Task.FromResult(GetEmployeePolicySummary(theCase.Employee, DateTime.Today, theCase, true, casePolicies));
        }

        /// <summary>
        /// Get all Employees Details of given employee
        /// </summary>
        /// <returns>Returns all Employees</returns>
        public async override Task<Dictionary<string, AbsenceSoft.Data.Customers.Employee>> GetEmployeeDetailsAsync(IList<string> employeeIds)
        {
            return await Task.FromResult(Employee.AsQueryable().Where(e => employeeIds.Contains(e.Id))
                  .ToDictionary(e => e.Id, e => e));

        }

        /// <summary>
        /// Get the Employee contact details of employees by Contact Type
        /// </summary>
        /// <param name="employeeIds"></param>
        /// <param name="contactTypeCode">Contact Type</param>
        /// <returns>Employee Contact details</returns>
        public async override Task<Dictionary<string, List<AbsenceSoft.Data.Customers.EmployeeContact>>> GetEmployeeContactDetailsAsync(IList<string> employeeIds, string contactTypeCode)
        {
            var contactList = EmployeeContact.AsQueryable().Where(ec => employeeIds.Contains(ec.EmployeeId)).ToList();
            return await Task.FromResult(contactList.GroupBy(e => e.EmployeeId).ToDictionary(e => e.Key, e => e.Where(ec => ec.ContactTypeCode == contactTypeCode).ToList()));
        }

        /// <summary>
        /// Get all the User Details by customer
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns>Returns all user</returns>
        public async override Task<Dictionary<string, string>> GetUserDetailsAsync(string customerId)
        {
            return await Task.FromResult(AbsenceSoft.Data.Security.User.Repository.Collection.FindAs<BsonDocument>(AbsenceSoft.Data.Security.User.Query.And(AbsenceSoft.Data.Security.User.Query.IsNotDeleted(),
                 AbsenceSoft.Data.Security.User.Query.EQ(u => u.CustomerId, customerId)))
                 .SetFields(Fields.Include("_id", "FirstName", "LastName"))
                 .ToDictionary(k => k["_id"].ToString(), v => string.Concat(v.Contains("FirstName") ? v["FirstName"].ToString() : "", " ", v.Contains("LastName") ? v["LastName"].ToString() : "").Trim()));
        }

        /// <summary>
        /// Build and Run custom accommodations status granted report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public async override Task<List<Case>> RunAccommodationStatusGrantedQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
            {
                ands.Add(user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport, "Employee._id"));
            }
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
            {
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            }
            ands.Add(Case.Query.EQ(e => e.IsAccommodation, true));
            ands.Add(Case.Query.Exists(e => e.AccommodationRequest));
            ands.Add(Case.Query.Exists(e => e.AccommodationRequest.Accommodations));

            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
            ));
            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null);
            return await Task.FromResult(query.ToList());
        }

        /// <summary>
        /// Build and Run accommodations Interactive Process Report HR Contacts Count
        /// </summary>
        /// <param name="user">Current User</param>
        /// <param name="criteria">Report Criteria</param>        
        /// <param name="pageNumber">Page number</param>
        /// <param name="pageSize">Page Size</param>
        /// <param name="sortBy">Sort by column name</param>
        /// <param name="orderByAsc">Order by column</param>
        /// <returns>Case list</returns>
        public async override Task<List<Case>> RunAccommodationCustomBaseQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
            {
                ands.Add(user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport, "Employee._id"));
            }
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
            {
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            }
            ands.Add(Case.Query.EQ(e => e.IsAccommodation, true));

            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
            ));

            if (string.IsNullOrWhiteSpace(sortBy))
            {
                sortBy = "_id";
            }
            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null)?.SetSortOrder(orderByAsc ? SortBy.Ascending(sortBy) : SortBy.Descending(sortBy)).SetLimit(pageSize).SetSkip((pageNumber - 1) * pageSize);
            return await Task.FromResult(query.ToList());
        }

        /// <summary>
        /// Method builds and run the to do query applying the reportcriterias
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns>List<ToDoItem></returns>
        public async override Task<List<ToDoItem>> RunToDoItemReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
            {
                ands.Add(user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport, "Employee._id"));
            }
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
            {
                ands.Add(ToDoItem.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            }
            ApplyEmployerFilter<ToDoItem>(ands, criteria, employerIdFilterName);

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(ToDoItem.Query.GTE(t => t.DueDate, new BsonDateTime(startDate)));
            ands.Add(ToDoItem.Query.LTE(t => t.DueDate, new BsonDateTime(endDate)));

            string assignee = criteria.ValueOf<string>("ToDoItemAssignee");
            ToDoItemType? type = criteria.ValueOf<ToDoItemType?>("ToDoItemType");
            ToDoItemStatus? statusFilter = criteria.ValueOf<ToDoItemStatus?>("ToDoStatus");

            if (!string.IsNullOrWhiteSpace(assignee))
            {
                ands.Add(ToDoItem.Query.EQ(t => t.AssignedToId, assignee));
            }
            if (type.HasValue)
            {
                ands.Add(ToDoItem.Query.EQ(t => t.ItemType, type.Value));
            }
            if (statusFilter.HasValue)
            {
                if (statusFilter == ToDoItemStatus.Open)
                {
                    ands.Add(ToDoItem.Query.In(e => e.Status, new List<ToDoItemStatus>() { ToDoItemStatus.Pending, ToDoItemStatus.Overdue }));
                }
                else if (statusFilter == ToDoItemStatus.Overdue)//overdue
                {
                    // Either it has an explicit status of overdue, OR, it is literally overdue with a different open/active status
                    ands.Add(ToDoItem.Query.Or(
                        ToDoItem.Query.And(ToDoItem.Query.EQ(e => e.Status, ToDoItemStatus.Pending), ToDoItem.Query.LT(e => e.DueDate, DateTime.UtcNow)),
                        ToDoItem.Query.And(ToDoItem.Query.EQ(e => e.Status, ToDoItemStatus.Open), ToDoItem.Query.LT(e => e.DueDate, DateTime.UtcNow)),
                        ToDoItem.Query.EQ(e => e.Status, ToDoItemStatus.Overdue))
                    );
                }
                else
                {
                    ands.Add(ToDoItem.Query.EQ(t => t.Status, statusFilter.Value));
                }
            }
            AppendOrganizationFilterQuery(user, criteria, employerIdFilterName, queries: ands, employeeIdField: "Employee._id");


            var query = ToDoItem.Query.Find(ToDoItem.Query.And(ands))?.SetSortOrder(orderByAsc ? SortBy.Ascending(sortBy) : SortBy.Descending(sortBy)).SetLimit(pageSize).SetSkip((pageNumber - 1) * pageSize);
            return await Task.FromResult(query.ToList());

        }

        /// <summary>
        /// Method queries EmployeeOrganization for a typecode including customerIds, EmployeeIds and EmployeeNumbers.
        /// </summary>
        /// <param name="grouptypeCode"></param>
        /// <param name="customerIds"></param>
        /// <param name="employerIds"></param>
        /// <param name="employeeNumbers"></param>
        /// <returns>List<EmployeeOrganization></returns>
        public async override Task<List<EmployeeOrganization>> GetEmployeeOrganization(string grouptypeCode, List<string> customerIds, List<string> employerIds, List<string> employeeNumbers)
        {
            List<EmployeeOrganization> employeeOrganizations;
            if (grouptypeCode != null)
            {
                employeeOrganizations = EmployeeOrganization.AsQueryable().Where(org => org.TypeCode == grouptypeCode && customerIds.Contains(org.CustomerId) && employerIds.Contains(org.EmployerId) && employeeNumbers.Contains(org.EmployeeNumber)).OrderByDescending(o => o.Dates).ToList();
            }
            else
            {
                employeeOrganizations = EmployeeOrganization.AsQueryable().Where(org => customerIds.Contains(org.CustomerId) && employerIds.Contains(org.EmployerId) && employerIds.Contains(org.EmployerId) && employeeNumbers.Contains(org.EmployeeNumber)).OrderByDescending(o => o.Dates).ToList();
            }
            return await Task.FromResult(employeeOrganizations);
        }

        /// <summary>
        /// Get Accommodation Questions by customer id
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public override async Task<IList<AccommodationQuestion>> GetAccommodationQuestionsByCustomerId(string customerId)
        {
            var query = AccommodationQuestion.AsQueryable().Where(q => q.CustomerId == customerId);
            return await Task.FromResult(query.ToList());
        }

        /// <summary>
        /// Gets Case List for Daily Case Report
        /// </summary>
        /// <param name="data"></param>
        /// <param name="criteria"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public override async Task<List<Case>> GetCaseListForDailyCaseReport(UserReportCriteriaData data, ReportCriteria criteria, string employerIdFilter, int pageNumber = 1, int pageSize = 10)
        {
            List<IMongoQuery> ands = CreateCommonCaseQuery(data, criteria, employerIdFilter);

            var caseList = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            return await Task.FromResult(caseList);
        }

        /// <summary>
        /// Gets Case List for Daily custom Case Report
        /// </summary>
        /// <param name="data"></param>
        /// <param name="criteria"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public override async Task<List<Case>> GetCaseListForCustomDailyCaseReport(UserReportCriteriaData data, ReportCriteria criteria, string employerIdFilter, int pageNumber = 1, int pageSize = 10)
        {
            List<IMongoQuery> ands = CreateCommonCaseQuery(data, criteria, employerIdFilter);

            var caseList = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null).SetSortOrder(SortBy.Ascending("cdt")).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            return await Task.FromResult(caseList);
        }


        /// <summary>
        /// Create Base common case query
        /// </summary>
        /// <param name="data"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilter"></param>
        /// <param name="isCustomReport"></param>
        /// <returns></returns>

        private List<IMongoQuery> CreateCommonCaseQuery(UserReportCriteriaData data, ReportCriteria criteria, string employerIdFilter, bool isCustomReport = false)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (data.SecurityUser != null)
            {
                ands.Add(data.SecurityUser.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport, "Employee._id"));
            }
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
            {
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            }
            
            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.CreatedDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.CreatedDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.ElemMatch(c => c.CaseEvents, e => e.And(Query.EQ("EventType", new BsonInt32(0)),
                    e.Or(Query.GTE("EventDate", new BsonDateTime(startDate)), Query.GTE("EventDate", new BsonDateTime(endDate)))
                    )))
            ));
            if (!isCustomReport)
            {
                ApplyEmployerFilter<Case>(ands, criteria, employerIdFilter);
                AppendOrganizationFilterQuery(data.SecurityUser, criteria, employerIdFilter, queries: ands, employeeIdField: "Employee._id");
            }
            return ands;
        }

        /// <summary>
        /// Build and Run Communication Report query
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async override Task<List<Communication>> RunCommunicationQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
            {
                ands.Add(user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCommunicationReport, "EmployeeId"));
            }
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
            {
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            }
            ApplyEmployerFilter<Case>(ands, criteria, employerIdFilterName);

            if (!user.HasPermission(AbsenceSoft.Data.Security.Permission.ViewConfidentialCommunications))
            {
                ands.Add(Communication.Query.Or(Communication.Query.EQ(c => c.Public, true), Query.EQ("Public", BsonNull.Value), Communication.Query.EQ(e => e.CreatedById, user?.Id)));
            }

            ands.Add(Communication.Query.GTE(e => e.SentDates, startDate));
            ands.Add(Communication.Query.LT(e => e.SentDates, endDate));

            if (criteria.Filters.FirstOrDefault(m => m.Name == "CommType") != null && criteria.Filters.FirstOrDefault(m => m.Name == "CommType").Value != null && Enum.TryParse<CommunicationType>(criteria.Filters.FirstOrDefault(m => m.Name == "CommType").Value.ToString(), out CommunicationType communicationType))
            {
                ands.Add(Query<Communication>.EQ(a => a.CommunicationType, communicationType));
            }

            AppendOrganizationFilterQuery(user, criteria, employerIdFilterName, queries: ands);
            var query = Communication.Query.Find(ands.Count > 0 ? Communication.Query.And(ands) : null).ToList();
            var lookupData = AddLookupData(query.AsQueryable());
            if (criteria.Filters.Any())
            {
                lookupData = ApplyCommunicationDetailReportFilter(criteria, lookupData);
            }
            var result = lookupData.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            if (!string.IsNullOrEmpty(sortBy))
            {
                result = result.OrderBy(sortBy, orderByAsc);
            }
            else
            {
                result = result.OrderByDescending(o => o.Employee.Id);
            }

            return await Task.FromResult(result.ToList());
        }

        /// <summary>
        /// Add Lookup data for report
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        private IQueryable<Communication> AddLookupData(IQueryable<Communication> items)
        {
            var customers = Customer
                          .GetByIds(items.Select(i => i.CustomerId))
                          .ToDictionary(c => c.Id);
            var employers = Employer
                .GetByIds(items.Select(i => i.EmployerId))
                .ToDictionary(d => d.Id);

            var cases = Case
                .GetByIds(items.Select(i => i.CaseId))
                .ToDictionary(d => d.Id);

            var employees = Employee
                .GetByIds(items.Select(i => i.EmployeeId))
                .ToDictionary(d => d.Id);
            items.ForEach(i =>
            {
                i.Customer = customers.ContainsKey(i.CustomerId) ? customers[i.CustomerId] : new Customer();
                i.Employer = employers.ContainsKey(i.EmployerId) ? employers[i.EmployerId] : new Employer();
                i.Case = cases.ContainsKey(i.CaseId) ? cases[i.CaseId] : new Case();
                i.Employee = employees.ContainsKey(i.EmployeeId) ? employees[i.EmployeeId] : new Employee();
            });

            return items;
        }

        /// <summary>
        /// Apply the communication report criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="items"></param>
        /// <returns></returns>
        private IQueryable<Communication> ApplyCommunicationDetailReportFilter(ReportCriteria criteria, IQueryable<Communication> items)
        {
            if (criteria.Filters.FirstOrDefault(m => m.Name == "CaseStatus") != null && !string.IsNullOrWhiteSpace(criteria.Filters.FirstOrDefault(m => m.Name == "CaseStatus").Value?.ToString()) && Enum.TryParse<CaseStatus>(criteria.Filters.FirstOrDefault(m => m.Name == "CaseStatus").Value.ToString(), out CaseStatus caseStatus))
            {
                items = items.Where(i => i.Case.Status == caseStatus);
            }

            if (criteria.Filters.FirstOrDefault(m => m.Name == "CaseType") != null && !string.IsNullOrWhiteSpace(criteria.Filters.FirstOrDefault(m => m.Name == "CaseType").Value?.ToString()) && Enum.TryParse<CaseType>(criteria.Filters.FirstOrDefault(m => m.Name == "CaseType").Value.ToString(), out CaseType caseType))
            {
                items = items.Where(i => i.Case.Segments.Any() && i.Case.Segments[0].Type == caseType);
            }

            if (criteria.Filters.FirstOrDefault(m => m.Name == "Determination") != null && !string.IsNullOrWhiteSpace(criteria.Filters.FirstOrDefault(m => m.Name == "Determination").Value?.ToString()) && Enum.TryParse<AdjudicationStatus>(criteria.Filters.FirstOrDefault(m => m.Name == "Determination").Value.ToString(), out AdjudicationStatus adjStatusType))
            {
                items = items.Where(i => i.Case.Summary.Determination == adjStatusType);
            }

            if (criteria.Filters.FirstOrDefault(m => m.Name == "AbsenceReason") != null && !string.IsNullOrWhiteSpace(criteria.Filters.FirstOrDefault(m => m.Name == "AbsenceReason").Value?.ToString()))
            {
                items = items.Where(i => i.Case.Reason.Id == criteria.Filters.FirstOrDefault(m => m.Name == "AbsenceReason").Value.ToString());
            }

            if (criteria.Filters.FirstOrDefault(m => m.Name == "WorkState") != null && !string.IsNullOrWhiteSpace(criteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value?.ToString()))
            {
                items = items.Where(i => i.Employee.WorkState == criteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value.ToString());
            }

            if (criteria.Filters.FirstOrDefault(m => m.Name == "Location") != null && !string.IsNullOrWhiteSpace(criteria.Filters.FirstOrDefault(m => m.Name == "Location").ValueAs<string>()))
            {
                var location = criteria.Filters.FirstOrDefault(m => m.Name == "Location").Value?.ToString();
                items = items.Where(i => i.Employee.GetOfficeLocation(location.ToUpper()) != null);
            }

            return items;
        }

        /// <summary>
        /// Build and Run Employee on Leave Report query
        /// </summary>
        /// <param name="user">Current User</param>
        /// <param name="criteria">Report Criteria</param>      
        /// <param name="pageNumber">Page number</param>
        /// <param name="pageSize">Page Size</param>
        /// <param name="sortBy">Sort by column name</param>
        /// <param name="orderByAsc">Order by column</param>
        /// <returns>Case list</returns>
        public override async Task<List<Case>> RunEmployeeOnLeaveReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
            {
                ands.Add(user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport, "Employee._id"));
            }
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
            {
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            }
            ands.Add(Case.Query.EQ(e => e.IsAccommodation, true));
            ands.Add(Query.EQ("IsEmployeeOnLeave", true));

            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
            ));

            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null);

            if (string.IsNullOrWhiteSpace(sortBy))
            {
                sortBy = "_id";
            }
            if (orderByAsc)
            {
                query = query.SetSortOrder(SortBy.Ascending(sortBy));
            }
            else
            {
                query = query.SetSortOrder(SortBy.Descending(sortBy));
            }

            return await Task.FromResult(query.SetLimit(pageSize).SetSkip((pageNumber - 1) * pageSize).ToList());
        }

        /// <summary>
        /// Get case list for Work restriction by Location for Amazon report
        /// </summary>
        /// <param name="data"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public override async Task<List<Case>> GetCaseListForWorkRestrictionByLocationAmazonReport(UserReportCriteriaData data, ReportCriteria reportCriteria, string employerIdFilterName, int pageNumber = 1, int pageSize = 10)
        {
            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (data.SecurityUser != null)
            {
                ands.Add(data.SecurityUser.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport));
            }
            else if (!string.IsNullOrWhiteSpace(reportCriteria.CustomerId))
            {
                ands.Add(EmployeeRestriction.Query.EQ(e => e.CustomerId, reportCriteria.CustomerId));
            }

            DateTime startDate = reportCriteria.StartDate.ToMidnight();
            DateTime endDate = reportCriteria.EndDate.EndOfDay();

            ands.Add(EmployeeRestriction.Query.Or(
                EmployeeRestriction.Query.And(EmployeeRestriction.Query.GTE(e => e.Restriction.Dates.StartDate, new BsonDateTime(startDate)), EmployeeRestriction.Query.LTE(e => e.Restriction.Dates.StartDate, new BsonDateTime(endDate))),
                EmployeeRestriction.Query.And(EmployeeRestriction.Query.GTE(e => e.Restriction.Dates.EndDate, new BsonDateTime(startDate)), EmployeeRestriction.Query.LTE(e => e.Restriction.Dates.EndDate, new BsonDateTime(endDate)))
            ));
            ands.Add(EmployeeRestriction.Query.NE(e => e.CaseId, null));

            var employeeRestrictions = EmployeeRestriction.Query.Find(EmployeeRestriction.Query.And(ands)).ToList();
            var caseIds = employeeRestrictions.Select(r => r.CaseId).Distinct().ToList();

            var caseQuery = BuildFilterWorkRestrictionByLocationAmazonReport(data.SecurityUser, reportCriteria, employerIdFilterName);
            caseQuery.Add(Case.Query.In(c => c.Id, caseIds));
            return await Task.FromResult(Case.Query.Find(Case.Query.And(caseQuery)).ToList());
        }

        /// <summary>
        /// Get Employee Restriction for CaseIds
        /// </summary>
        /// <param name="data"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="caseIds"></param>
        /// <returns></returns>
        public override async Task<List<EmployeeRestriction>> GetEmployeeRestrictionForCaseIds(UserReportCriteriaData data, ReportCriteria reportCriteria, List<string> caseIds)
        {
            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (data.SecurityUser != null)
            {
                ands.Add(data.SecurityUser.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport));
            }
            else if (!string.IsNullOrWhiteSpace(reportCriteria.CustomerId))
            {
                ands.Add(EmployeeRestriction.Query.EQ(e => e.CustomerId, reportCriteria.CustomerId));
            }

            DateTime startDate = reportCriteria.StartDate.ToMidnight();
            DateTime endDate = reportCriteria.EndDate.EndOfDay();

            ands.Add(EmployeeRestriction.Query.Or(
                EmployeeRestriction.Query.And(EmployeeRestriction.Query.GTE(e => e.Restriction.Dates.StartDate, new BsonDateTime(startDate)), EmployeeRestriction.Query.LTE(e => e.Restriction.Dates.StartDate, new BsonDateTime(endDate))),
                EmployeeRestriction.Query.And(EmployeeRestriction.Query.GTE(e => e.Restriction.Dates.EndDate, new BsonDateTime(startDate)), EmployeeRestriction.Query.LTE(e => e.Restriction.Dates.EndDate, new BsonDateTime(endDate)))
            ));
            ands.Add(EmployeeRestriction.Query.NE(e => e.CaseId, null));

            var employeeRestrictions = EmployeeRestriction.Query.Find(EmployeeRestriction.Query.And(ands)).ToList();

            return await Task.FromResult(employeeRestrictions.Where(r => caseIds.Contains(r.CaseId)).ToList());
        }

        /// <summary>
        /// Get Demand for Restriction CaseIds
        /// </summary>
        /// <param name="employeeRestrictions"></param>
        /// <returns></returns>
        public override async Task<List<Demand>> GetDemandForRestrictionCaseIds(List<EmployeeRestriction> employeeRestrictions)
        {
            var demandIds = employeeRestrictions.Select(r => r.Restriction.DemandId).Distinct().ToList();
            return await Task.FromResult(Demand.Query.Find(Demand.Query.In(d => d.Id, demandIds)).ToList());
        }

        /// <summary>
        /// Gets Employee contact for given list of employee ids
        /// </summary>
        /// <param name="empIds"></param>
        /// <param name="contactTypeCode"></param>
        /// <returns></returns>
        public override async Task<List<EmployeeContact>> GetEmployeeContact(List<string> empIds, string contactTypeCode)
        {
            return await Task.FromResult(EmployeeContact.AsQueryable().Where(ec => empIds.Contains(ec.EmployeeId) && ec.ContactTypeCode == contactTypeCode).ToList());
        }

        /// <summary>
        /// Get EmployeeJob for Associated Employee Number
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="empNums"></param>
        /// <returns></returns>
        public override async Task<List<EmployeeJob>> GetEmployeeJobForAssociatedEmployeeNumber(string customerId, List<string> empNums)
        {
            return await Task.FromResult(EmployeeJob.AsQueryable().Where(e => e.CustomerId == customerId && empNums.Contains(e.EmployeeNumber)).ToList());
        }

        /// <summary>
        /// Builds the filter.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        private List<IMongoQuery> BuildFilterWorkRestrictionByLocationAmazonReport(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName)
        {
            if (!criteria.Filters.Any())
            {
                return new List<IMongoQuery>();
            }

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
            {
                ands.Add(user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport, "Employee._id"));
            }
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
            {
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            }
            ands.Add(Case.Query.EQ(e => e.IsAccommodation, true));

            ApplyEmployerFilter<Case>(ands, criteria, employerIdFilterName);
            List<IMongoQuery> elemMatchAnds = SetCaseStatusDetermination(ands, criteria);

            SetAccommodationWorkStateLocationFilter(ands, criteria, elemMatchAnds);

            var assignee = criteria.Filters.FirstOrDefault(m => m.Name == "CaseAssignee");
            if (assignee != null && assignee.Value != null && !string.IsNullOrWhiteSpace(assignee.ValueAs<string>()))
            {
                ands.Add(Case.Query.EQ(e => e.AssignedToId, assignee.Value));
            }
            return ands;
        }

        /// <summary>
        /// Set Case Status Determination
        /// </summary>
        /// <param name="query"></param>
        /// <param name="criteria"></param>
        /// <returns></returns>
        private List<IMongoQuery> SetCaseStatusDetermination(ICollection<IMongoQuery> query, ReportCriteria criteria)
        {
            var caseStatus = criteria.Filters.FirstOrDefault(m => m.Name == "CaseStatus");
            if (caseStatus != null && caseStatus.Value != null)
            {
                var enumCaseStatusFilter = criteria.ValueOf<CaseStatus>("CaseStatus");
                query.Add(Case.Query.EQ(c => c.Status, enumCaseStatusFilter));
            }

            var elemMatchAnds = new List<IMongoQuery>();

            var determination = criteria.Filters.FirstOrDefault(m => m.Name == "Determination");
            if (determination != null && determination.Value != null)
            {
                var enumadjStatusFilter = criteria.ValueOf<AdjudicationStatus>("Determination");
                elemMatchAnds.Add(Query<Accommodation>.EQ(a => a.Determination, enumadjStatusFilter));
            }

            return elemMatchAnds;
        }

        /// <summary>
        /// Sets the Accommodation Filter
        /// </summary>
        /// <param name="query"></param>
        /// <param name="criteria"></param>
        /// <param name="elemMatchAnds"></param>
        private void SetAccommodationWorkStateLocationFilter(ICollection<IMongoQuery> query, ReportCriteria criteria, List<IMongoQuery> elemMatchAnds)
        {
            var accomType = criteria.Filters.FirstOrDefault(m => m.Name == "AccomType");
            if (accomType != null && accomType.Value != null)
            {
                elemMatchAnds.Add(Query<Accommodation>.EQ(a => a.Type.Code, accomType.Value));
            }
            if (elemMatchAnds.Any())
            {
                query.Add(Case.Query.ElemMatch(c => c.AccommodationRequest.Accommodations, q => q.And(elemMatchAnds)));
            }

            var workState = criteria.Filters.FirstOrDefault(m => m.Name == "WorkState");
            if (workState != null && workState.Value != null)
            {
                query.Add(Case.Query.EQ(e => e.Employee.WorkState, workState.Value));
            }

            var locationFilter = criteria.Filters.FirstOrDefault(m => m.Name == "Location");
            if (locationFilter != null && locationFilter.Values != null && locationFilter.Values.Any())
            {
                var count = locationFilter.Values.Count;
                var values = locationFilter.Values.Where(p => !string.IsNullOrEmpty(Convert.ToString(p))).ToList();
                if (count != values.Count)
                {
                    values.AddRange(new List<object> { null, string.Empty });
                }
                query.Add(Case.Query.In(e => e.CurrentOfficeLocation, values));
            }
        }

        /// <summary>
        /// Gets Case List for Work Related Report
        /// </summary>
        /// <param name="data"></param>
        /// <param name="criteria"></param>
        /// <param name="reportGroupType"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public override async Task<List<Case>> GetCaseListForWorkRelatedReport(UserReportCriteriaData data, ReportCriteria criteria, ReportGroupType? reportGroupType, string employerIdFilterName, int pageNumber = 1, int pageSize = 10)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();
            List<IMongoQuery> ands = new List<IMongoQuery>();

            if (data.AuthenticatedUser != null)
            {
                ands.Add(data.SecurityUser.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport, "Employee._id"));
            }
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
            {
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            }

            ApplyEmployerFilter<Case>(ands, criteria, employerIdFilterName);
            ands.Add(Case.Query.And(
                        Case.Query.GTE(e => e.WorkRelated.IllnessOrInjuryDate, new BsonDateTime(startDate)),
                        Case.Query.LTE(e => e.WorkRelated.IllnessOrInjuryDate, new BsonDateTime(endDate))
                ));

            if (reportGroupType == ReportGroupType.NeedleSticks)
            {
                // This report requires the Sharps info
                ands.Add(Case.Query.EQ<bool?>(e => e.WorkRelated.Sharps, true));
                ApplySharpsFilters(criteria, ands);
            }
            else
            {
                ands.Add(Case.Query.Exists(c => c.WorkRelated));
                ApplyWorkRelatedCriteria(criteria, ands);
            }

            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null).Skip((pageNumber - 1) * pageSize).Take(pageSize);
            return await Task.FromResult(query.ToList());
        }

        #region Retrieve the selected filters and apply to the Query

        /// <summary>
        /// Applies Sharps Filters
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="ands"></param>
        protected void ApplySharpsFilters(ReportCriteria criteria, List<IMongoQuery> ands)
        {
            AddMultiFilter<IEnumerable<WorkRelatedSharpsInjuryLocation>>(c => c.WorkRelated.SharpsInfo.InjuryLocation, "WorkRelatedCaseClassification", criteria, ands);
            AddMultiFilter<WorkRelatedSharpsLocation?>(c => c.WorkRelated.SharpsInfo.LocationAndDepartment, "WorkRelatedSharpsLocation", criteria, ands);
            AddMultiFilter<WorkRelatedSharpsProcedure?>(c => c.WorkRelated.SharpsInfo.Procedure, "WorkRelatedSharpsProcedure", criteria, ands);
            AddMultiFilter<WorkRelatedSharpsJobClassification?>(c => c.WorkRelated.SharpsInfo.JobClassification, "WorkRelatedSharpsJobClassification", criteria, ands);
#pragma warning disable CS0618 // Type or member is obsolete
            AddMultiFilter<string>(e => e.Employee.Info.OfficeLocation, "Location", criteria, ands);
#pragma warning restore CS0618 // Type or member is obsolete
        }

        /// <summary>
        /// Applies Work Related Criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="ands"></param>
        protected void ApplyWorkRelatedCriteria(ReportCriteria criteria, List<IMongoQuery> ands)
        {
            AddMultiFilter<WorkRelatedCaseClassification?>(e => e.WorkRelated.Classification, "WorkRelatedCaseClassification", criteria, ands);
            AddMultiFilter<WorkRelatedTypeOfInjury?>(e => e.WorkRelated.TypeOfInjury, "WorkRelatedTypeOfInjury", criteria, ands);
#pragma warning disable CS0618 // Type or member is obsolete
            AddMultiFilter<string>(e => e.Employee.Info.OfficeLocation, "Location", criteria, ands);
#pragma warning restore CS0618 // Type or member is obsolete
        }

        /// <summary>
        /// Adds Multi Filter
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="memberExpression"></param>
        /// <param name="filterName"></param>
        /// <param name="criteria"></param>
        /// <param name="ands"></param>
        public void AddMultiFilter<TEnum>(Expression<Func<Case, TEnum>> memberExpression, string filterName, ReportCriteria criteria, List<IMongoQuery> ands)
        {
            var matchingFilter = criteria.Filters.FirstOrDefault(m => m.Name == filterName && (!string.IsNullOrWhiteSpace(m.Value?.ToString()) || (m.Values != null && m.Values.Any())));
            if (matchingFilter != null)
            {
                var value = matchingFilter.ValuesAs<TEnum>();
                ands.Add(Case.Query.In<TEnum>(memberExpression, value));
            }
        }

        /// <summary>
        /// Add EQ Filter
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="memberExpression"></param>
        /// <param name="filterName"></param>
        /// <param name="criteria"></param>
        /// <param name="ands"></param>
        public void AddEQFilter<TEnum>(Expression<Func<Case, TEnum>> memberExpression, string filterName, ReportCriteria criteria, List<IMongoQuery> ands)
        {
            var matchingFilter = criteria.Filters.FirstOrDefault(m => m.Name == filterName && m.Value != null);
            if (matchingFilter != null)
            {
                var value = matchingFilter.ValueAs<TEnum>();
                ands.Add(Case.Query.EQ<TEnum>(memberExpression, value));
            }
        }

        /// <summary>
        /// Adds Yes/ No Filter
        /// </summary>
        /// <param name="memberExpression"></param>
        /// <param name="filterName"></param>
        /// <param name="criteria"></param>
        /// <param name="ands"></param>
        public void AddYesNoBoolFilter(Expression<Func<Case, bool>> memberExpression, string filterName, ReportCriteria criteria, List<IMongoQuery> ands)
        {
            var matchingFilter = criteria.Filters.FirstOrDefault(m => m.Name == filterName && m.Value != null);
            if (matchingFilter != null)
            {
                var value = matchingFilter.Value.ToString() == "Yes";
                ands.Add(Case.Query.EQ(memberExpression, value));
            }
        }

        #endregion

        /// <summary>
        /// Method builds and run the Cse without to-do report query applying the reportcriterias and aggregation
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public override async Task<List<ToDoMapReduceResultItem>> RunCaseWithoutToDoReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> caseAnds = new List<IMongoQuery>();
            if (user != null)
            {
                caseAnds.Add(user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport, "Employee._id"));
            }
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
            {
                caseAnds.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            }
            ApplyEmployerFilter<Case>(caseAnds, criteria, employerIdFilterName);

            // Date Created must match a range for dates
            caseAnds.Add(Case.Query.GTE(e => e.CreatedDate, startDate));
            caseAnds.Add(Case.Query.LTE(e => e.CreatedDate, endDate));
            AppendOrganizationFilterQuery(user, criteria, employerIdFilterName, queries: caseAnds, employeeIdField: "Employee._id");
            // Must be an open or inquiry case
            Case.Query.Or(
                Case.Query.EQ(c => c.Status, CaseStatus.Inquiry),
                Case.Query.EQ(c => c.Status, CaseStatus.Open)
            );
            IMongoQuery casesQuery = caseAnds.Any() ? Case.Query.And(caseAnds) : null;

            // Build To-Do Items Query
            List<IMongoQuery> todoAnds = new List<IMongoQuery>();
            todoAnds.Add(ToDoItem.Query.IsNotDeleted());
            if (user != null)
            {
                todoAnds.Add(user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport, "EmployeeId"));
            }
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
            {
                todoAnds.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            }
            ApplyEmployerFilter<ToDoItem>(todoAnds, criteria, employerIdFilterName);
            todoAnds.Add(ToDoItem.Query.NotIn(t => t.Status, new[] { ToDoItemStatus.Cancelled, ToDoItemStatus.Complete }));
            AppendOrganizationFilterQuery(user, criteria, employerIdFilterName, queries: todoAnds);
            IMongoQuery todosQuery = todoAnds.Any() ? ToDoItem.Query.And(todoAnds) : null;

            // Define our Map functions
            StringBuilder caseMap = new StringBuilder();
            caseMap.AppendLine("function() {");
            caseMap.AppendLine("   emit(this._id, { ToDos: 0, Case: this });");
            caseMap.AppendLine("}");
            StringBuilder todoMap = new StringBuilder();
            todoMap.AppendLine("function() {");
            todoMap.AppendLine("   emit(this.CaseId, { ToDos: 1, Case: null });");
            todoMap.AppendLine("}");

            // Define our reduce function
            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(key, values) {");
            reduce.AppendLine("  var result = { ToDos: 0, Case: null };");
            reduce.AppendLine("  values.forEach(function(value) {");
            reduce.AppendLine("     result.ToDos += (value.ToDos || 0);");
            reduce.AppendLine("     result.Case = result.Case || value.Case;");
            reduce.AppendLine("  });");
            reduce.AppendLine("  return result;");
            reduce.AppendLine("}");

            // Generate a random collection name
            string collectionName = RandomString.Generate(16, true, false, false, false, false);
            // Get a reference to the collection in the same DB as the Case
            MongoCollection collection = Case.Repository.Collection.Database.GetCollection(collectionName);

            // Perform the case mapReduce, replacing that collection by name if it already exists
            Case.Repository.Collection.MapReduce(new MapReduceArgs()
            {
                OutputMode = MapReduceOutputMode.Replace,
                OutputCollectionName = collectionName,
                Query = casesQuery,
                MapFunction = new BsonJavaScript(caseMap.ToString()),
                ReduceFunction = new BsonJavaScript(reduce.ToString())
            });

            // Perform the To-Do mapReduce, merging the results using the Reduce function
            ToDoItem.Repository.Collection.MapReduce(new MapReduceArgs()
            {
                OutputMode = MapReduceOutputMode.Merge,
                OutputCollectionName = collectionName,
                Query = todosQuery,
                MapFunction = new BsonJavaScript(todoMap.ToString()),
                ReduceFunction = new BsonJavaScript(reduce.ToString())
            });

            var query = collection.FindAs<ToDoMapReduceResultItem>(Query.EQ("value.ToDos", new BsonInt32(0)));
            if (!string.IsNullOrWhiteSpace(sortBy))
            {
                query = query?.SetSortOrder(orderByAsc ? SortBy.Ascending(sortBy) : SortBy.Descending(sortBy));
            }
            query = query.SetLimit(pageSize).SetSkip((pageNumber - 1) * pageSize);
            return await Task.FromResult(query.ToList());
        }

        /// <summary>
        /// Build and Run Employee Survey Report query
        /// </summary>
        /// <param name="user">Current User</param>
        /// <param name="criteria">Report Criteria</param>      
        /// <param name="pageNumber">Page number</param>
        /// <param name="pageSize">Page Size</param>
        /// <param name="sortBy">Sort by column name</param>
        /// <param name="orderByAsc">Order by column</param>
        /// <returns>Case list</returns>
        public async override Task<List<Case>> RunEmployeeSurveyReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
            {
                ands.Add(user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport, "Employee._id"));
            }
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
            {
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            }
            ands.Add(Case.Query.EQ(e => e.IsAccommodation, true));
            ands.Add(
                Case.Query.And(Case.Query.GTE(e => e.CreatedDate, new BsonDateTime(startDate)),
                Case.Query.LTE(e => e.CreatedDate, new BsonDateTime(endDate)))
             );
            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null);

            if (string.IsNullOrWhiteSpace(sortBy))
            {
                sortBy = "_id";
            }
            if (orderByAsc)
            {
                query = query.SetSortOrder(SortBy.Ascending(sortBy));
            }
            else
            {
                query = query.SetSortOrder(SortBy.Descending(sortBy));
            }

            return await Task.FromResult(query.SetLimit(pageSize).SetSkip((pageNumber - 1) * pageSize).ToList());
        }

        /// <summary>
        /// Gets the User Case count
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async override Task<int> UserCaseCount(AbsenceSoft.Data.Security.User user)
        {
            return await Task.FromResult(Case.AsQueryable(user).Count());
        }

        /// <summary>
        /// Gets the Result for BI Report by Leave
        /// </summary>
        /// <param name="userReportCriteriaData"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilter"></param>
        /// <returns></returns>
        public async override Task<List<List<object>>> GetBiReportByLeaveResult(UserReportCriteriaData userReportCriteriaData, ReportCriteria reportCriteria, string employerIdFilter)
        {
            List<IMongoQuery> ands = GetCaseForBIReports(userReportCriteriaData, reportCriteria, employerIdFilter);
            IMongoQuery query = ands.Count > 0 ? Case.Query.And(ands) : null;

            // Define our Map function
            StringBuilder map = new StringBuilder();
            map.AppendLine("function() {");
            map.AppendLine("    for (var i=0; i<caseTypes.length; i++) {");
            map.AppendLine("        if (caseTypes[i] === this.Summary.t) {");
            map.AppendLine("              emit(strCaseTypes[i], 1);");
            map.AppendLine("        }");
            map.AppendLine("    }");
            map.AppendLine("}");

            // Define our reduce function
            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(caseType, count) {");
            reduce.AppendLine(" return Array.sum(count); ");
            reduce.AppendLine("}");

            var caseTypes = Enum.GetValues(typeof(CaseType)).OfType<CaseType>().OrderBy(t => (int)t).ToList();
            var strCaseTypes = Enum.GetValues(typeof(CaseType)).OfType<CaseType>().OrderBy(t => (int)t).Select(t => t.ToString().SplitCamelCaseString()).ToList();

            Dictionary<String, object> lstScopeObjects = new Dictionary<string, object>();
            lstScopeObjects.Add("caseTypes", caseTypes.Select(t => (int)t).OrderBy(t => t).ToArray());
            lstScopeObjects.Add("strCaseTypes", strCaseTypes.Select(t => t).ToArray());

            // Define our map reduce arguments
            MapReduceArgs mapReduce = new MapReduceArgs
            {
                OutputMode = MapReduceOutputMode.Inline,
                Query = query,
                Scope = new ScopeDocument(lstScopeObjects),
                MapFunction = new BsonJavaScript(map.ToString()),
                ReduceFunction = new BsonJavaScript(reduce.ToString())
            };

            // Execute our map reduce query operation against the ToDoItem collection
            var response = Case.Repository.Collection.MapReduce(mapReduce);
            List<List<object>> resultList = null;
            if (response != null)
            {
                resultList = SetResultListBIReport(reportCriteria, response);
            }
            return await Task.FromResult(resultList);
        }

        /// <summary>
        ///  Gets the Result for BI Report by Location for the user
        /// </summary>
        /// <param name="userReportCriteriaData"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilter"></param>
        /// <returns></returns>
        public async override Task<List<List<object>>> GetBiReportByLocationResult(UserReportCriteriaData userReportCriteriaData, ReportCriteria reportCriteria, string employerIdFilter)
        {
            List<IMongoQuery> ands = GetCaseForBIReports(userReportCriteriaData, reportCriteria, employerIdFilter);
            IMongoQuery query = ands.Count > 0 ? Case.Query.And(ands) : null;

            // Define our Map function
            StringBuilder map = new StringBuilder();
            map.AppendLine("function() {");
            map.AppendLine("   if(this.Reason.Name.indexOf('Accommodation') < 0) {");
            map.AppendLine("        emit(this.Employee.WorkState, 1);");
            map.AppendLine("    }");
            map.AppendLine("}");

            // Define our reduce function
            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(location, count) {");
            reduce.AppendLine(" return Array.sum(count); ");
            reduce.AppendLine("}");

            // Define our map reduce arguments
            MapReduceArgs mapReduce = new MapReduceArgs
            {
                OutputMode = MapReduceOutputMode.Inline,
                Query = query,
                MapFunction = new BsonJavaScript(map.ToString()),
                ReduceFunction = new BsonJavaScript(reduce.ToString())
            };

            // Execute our map reduce query operation against the ToDoItem collection
            var response = Case.Repository.Collection.MapReduce(mapReduce);
            List<List<object>> resultList = null;
            if (response != null)
            {
                resultList = SetResultListBIReport(reportCriteria, response);
            }
            return await Task.FromResult(resultList);
        }

        /// <summary>
        /// Gets the Result for BI Report by Reason for the user
        /// </summary>
        /// <param name="userReportCriteriaData"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilter"></param>
        /// <returns></returns>
        public async override Task<List<List<object>>> GetBiReportByReasonResult(UserReportCriteriaData userReportCriteriaData, ReportCriteria reportCriteria, string employerIdFilter)
        {
            List<IMongoQuery> ands = GetCaseForBIReports(userReportCriteriaData, reportCriteria, employerIdFilter);
            IMongoQuery query = ands.Count > 0 ? Case.Query.And(ands) : null;

            // Define our Map function
            StringBuilder map = new StringBuilder();
            map.AppendLine("function() {");
            map.AppendLine("   if(this.Reason.Name.indexOf('Accommodation') < 0) {");
            map.AppendLine("        emit(this.Reason.Name, 1);");
            map.AppendLine("   }");
            map.AppendLine("}");

            // Define our reduce function
            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(reason, count) {");
            reduce.AppendLine(" return Array.sum(count); ");
            reduce.AppendLine("}");

            // Define our map reduce arguments
            MapReduceArgs mapReduce = new MapReduceArgs
            {
                OutputMode = MapReduceOutputMode.Inline,
                Query = query,
                MapFunction = new BsonJavaScript(map.ToString()),
                ReduceFunction = new BsonJavaScript(reduce.ToString())
            };

            // Execute our map reduce query operation against the ToDoItem collection
            var response = Case.Repository.Collection.MapReduce(mapReduce);
            List<List<object>> resultList = null;
            if (response != null)
            {
                resultList = SetResultListBIReport(reportCriteria, response);
            }
            return await Task.FromResult(resultList);
        }

        /// <summary>
        /// Gets the Result for BI Report by Status for the user
        /// </summary>
        /// <param name="userReportCriteriaData"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilter"></param>
        /// <returns></returns>
        public async override Task<List<List<object>>> GetBiReportByStatusResult(UserReportCriteriaData userReportCriteriaData, ReportCriteria reportCriteria, string employerIdFilter)
        {
            List<IMongoQuery> ands = GetCaseForBIReports(userReportCriteriaData, reportCriteria, employerIdFilter);

            IMongoQuery query = ands.Count > 0 ? Case.Query.And(ands) : null;

            // Define our Map function
            StringBuilder map = new StringBuilder();
            map.AppendLine("function() {");
            map.AppendLine("    for (var i=0; i<caseStatus.length; i++) {");
            map.AppendLine("        if (caseStatus[i] === this.Status) {");
            map.AppendLine("              emit(strCaseStatus[i], 1);");
            map.AppendLine("        }");
            map.AppendLine("    }");
            map.AppendLine("}");

            // Define our reduce function
            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(status, count) {");
            reduce.AppendLine(" return Array.sum(count); ");
            reduce.AppendLine("}");

            var caseStatues = Enum.GetValues(typeof(CaseStatus)).OfType<CaseStatus>().OrderBy(t => (int)t).ToList();
            var abc = Enum.GetValues(typeof(CaseStatus)).OfType<CaseStatus>().OrderBy(t => (int)t).Select(t => t.ToString().SplitCamelCaseString()).ToList();

            Dictionary<String, object> lstScopeObjects = new Dictionary<string, object>();
            lstScopeObjects.Add("caseStatus", caseStatues.Select(t => (int)t).OrderBy(t => t).ToArray());
            lstScopeObjects.Add("strCaseStatus", abc.Select(t => t).ToArray());

            // Define our map reduce arguments
            MapReduceArgs mapReduce = new MapReduceArgs
            {
                OutputMode = MapReduceOutputMode.Inline,
                Query = query,
                Scope = new ScopeDocument(lstScopeObjects),
                MapFunction = new BsonJavaScript(map.ToString()),
                ReduceFunction = new BsonJavaScript(reduce.ToString())
            };

            // Execute our map reduce query operation against the ToDoItem collection
            var response = Case.Repository.Collection.MapReduce(mapReduce);
            List<List<object>> resultList = null;
            if (response != null)
            {
                resultList = SetResultListBIReport(reportCriteria, response);
            }
            return await Task.FromResult(resultList);
        }

        /// <summary>
        /// Gets the Result for BI Report by Policy for the user
        /// </summary>
        /// <param name="userReportCriteriaData"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilter"></param>
        /// <returns></returns>
        public async override Task<List<List<object>>> GetBiReportByPolicyResult(UserReportCriteriaData userReportCriteriaData, ReportCriteria reportCriteria, string employerIdFilter)
        {
            List<IMongoQuery> ands = GetCaseForBIReports(userReportCriteriaData, reportCriteria, employerIdFilter);

            IMongoQuery query = ands.Count > 0 ? Case.Query.And(ands) : null;

            // Define our Map function
            StringBuilder map = new StringBuilder();
            map.AppendLine("function() {");
            map.AppendLine("    var c = this;");
            map.AppendLine("    if (c.Summary && c.Summary.p && c.Summary.p.length) {");
            map.AppendLine("          for (var i=0; i<c.Summary.p.length; i++) {");
            map.AppendLine("                   emit(c.Summary.p[i].n, 1);");
            map.AppendLine("          }");
            map.AppendLine("    }");
            map.AppendLine("}");

            // Define our reduce function
            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(policy, count) {");
            reduce.AppendLine(" return Array.sum(count); ");
            reduce.AppendLine("}");

            // Define our map reduce arguments
            MapReduceArgs mapReduce = new MapReduceArgs
            {
                OutputMode = MapReduceOutputMode.Inline,
                Query = query,
                MapFunction = new BsonJavaScript(map.ToString()),
                ReduceFunction = new BsonJavaScript(reduce.ToString())
            };

            // Execute our map reduce query operation against the ToDoItem collection
            var response = Case.Repository.Collection.MapReduce(mapReduce);
            List<List<object>> resultList = null;
            if (response != null)
            {
                resultList = SetResultListBIReport(reportCriteria, response);
            }
            return await Task.FromResult(resultList);
        }

        /// <summary>
        /// Gets the Result for BI Report by TotalLeaves by Reason for the user
        /// </summary>
        /// <param name="userReportCriteriaData"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilter"></param>
        /// <returns></returns>
        public async override Task<List<List<object>>> GetBiReportTotalLeavesByReasonResult(UserReportCriteriaData userReportCriteriaData, ReportCriteria reportCriteria, string employerIdFilter)
        {
            List<IMongoQuery> ands = new List<IMongoQuery>
            {
                Case.Query.IsNotDeleted()
            };

            if (userReportCriteriaData.SecurityUser != null)
            {
                ands.Add(userReportCriteriaData.SecurityUser.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunBIReport, "Employee._id"));
            }
            else if (!string.IsNullOrWhiteSpace(reportCriteria.CustomerId))
            {
                ands.Add(Case.Query.EQ(e => e.CustomerId, reportCriteria.CustomerId));
            }

            ApplyEmployerFilter<Case>(ands, reportCriteria, employerIdFilter);

            DateTime startDate = reportCriteria.StartDate.ToMidnight();
            DateTime endDate = reportCriteria.EndDate.EndOfDay();

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
            ));

            IMongoQuery query = ands.Count > 0 ? Case.Query.And(ands) : null;

            // Define our Map function
            StringBuilder map = new StringBuilder();
            map.AppendLine("function() {");
            map.AppendLine("    if(isDateWise) {");
            map.AppendLine("        for (var m=0; m<groupBy.length; m++) {");
            map.AppendLine("            var ret = [groupBy[m]];");
            map.AppendLine("            var dt = new Date(groupBy[m]);");
            map.AppendLine("            for (var i=0; i<reasons.length; i++) {");
            map.AppendLine("                    if (reasons[i] === this.Reason.Name && this.StartDate.getDate() == dt.getDate() && this.StartDate.getMonth() == dt.getMonth() && this.StartDate.getFullYear() == dt.getFullYear()) {");
            map.AppendLine("                        ret.push(1);");
            map.AppendLine("                    } else {");
            map.AppendLine("                        ret.push(0);");
            map.AppendLine("                    }");
            map.AppendLine("            }");
            map.AppendLine("            emit(groupBy[m], { val: ret });");
            map.AppendLine("        }");
            map.AppendLine("    }");
            map.AppendLine("    else {");
            map.AppendLine("        for (var m=0; m<groupBy.length; m++) {");
            map.AppendLine("            var mnth = groupBy[m].split(':')[0];");
            map.AppendLine("            var mnthname = groupBy[m].split(':')[1];");
            map.AppendLine("            var yr = groupBy[m].split(':')[2];");
            map.AppendLine("            var group = mnthname + \" '\" + yr.slice(-2);");
            map.AppendLine("            var ret = [group];");
            map.AppendLine("            for (var i=0; i<reasons.length; i++) {");
            map.AppendLine("                if(this.Reason.Name.indexOf('Accommodation') < 0) {");
            map.AppendLine("                    if (reasons[i] === this.Reason.Name && this.StartDate.getMonth() == (mnth-1) && this.StartDate.getFullYear() == yr) {");
            map.AppendLine("                        ret.push(1);");
            map.AppendLine("                    } else {");
            map.AppendLine("                        ret.push(0);");
            map.AppendLine("                    }");
            map.AppendLine("                }");
            map.AppendLine("            }");
            map.AppendLine("            emit(m, { val: ret });");
            map.AppendLine("        }");
            map.AppendLine("    }");
            map.AppendLine("}");

            // Define our reduce function
            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(key, values) {");
            reduce.AppendLine(" var result = { val: [values[0].val[0]] };");
            reduce.AppendLine(" values.forEach(function(value) {");
            reduce.AppendLine("     for (var i=1; i<value.val.length; i++) {");
            reduce.AppendLine("         result.val[i] = result.val[i] || 0;");
            reduce.AppendLine("         result.val[i] += (value.val[i] || 0);");
            reduce.AppendLine("     }");
            reduce.AppendLine(" });");
            reduce.AppendLine(" return result;");
            reduce.AppendLine("}");

            // make month year array to group data
            List<string> arrMonthYear = new List<string>();
            DateTime nextDate = startDate;
            do
            {
                arrMonthYear.Add(nextDate.Month.ToString() + ":" + nextDate.ToString("MMM") + ":" + nextDate.Year.ToString());
                nextDate = nextDate.AddMonths(1);

            } while (nextDate < endDate);

            // Define scope document for map reduce
            string[] groupBy = CreateGroupByForBIReportTotalLeaves(reportCriteria);

            Dictionary<string, object> scopeDoc = new Dictionary<string, object>();
            var reasonsExcludingAccommodation = await GetCaseReasonsExcludingAccommodation();
            scopeDoc.Add("reasons", reasonsExcludingAccommodation.ToArray());
            scopeDoc.Add("groupBy", groupBy);
            scopeDoc.Add("isDateWise", IsDateWiseComparison(reportCriteria));

            // Define our map reduce arguments
            MapReduceArgs mapReduce = new MapReduceArgs
            {
                OutputMode = MapReduceOutputMode.Inline,
                Query = query,
                Scope = new ScopeDocument(scopeDoc),
                MapFunction = new BsonJavaScript(map.ToString()),
                ReduceFunction = new BsonJavaScript(reduce.ToString())
            };

            // Execute our map reduce query operation against the ToDoItem collection
            var response = Case.Repository.Collection.MapReduce(mapReduce);
            List<List<object>> resultList = null;
            if (response != null)
            {
                resultList = SetResultListBILineReport(response);
            }
            return await Task.FromResult(resultList);
        }

        /// <summary>
        /// Gets the Result for BI Report by Total Leaves for the user
        /// </summary>
        /// <param name="userReportCriteriaData"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilter"></param>
        /// <returns></returns>
        public async override Task<List<List<object>>> GetBiReportTotalLeavesByTotalResult(UserReportCriteriaData userReportCriteriaData, ReportCriteria reportCriteria, string employerIdFilter)
        {
            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (userReportCriteriaData.SecurityUser != null)
            {
                ands.Add(userReportCriteriaData.SecurityUser.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunBIReport, "Employee._id"));
            }
            else if (!string.IsNullOrWhiteSpace(reportCriteria.CustomerId))
            {
                ands.Add(Case.Query.EQ(e => e.CustomerId, reportCriteria.CustomerId));
            }

            DateTime startDate = reportCriteria.StartDate.ToMidnight();
            DateTime endDate = reportCriteria.EndDate.EndOfDay();

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
            ));

            IMongoQuery query = ands.Count > 0 ? Case.Query.And(ands) : null;

            // Define our Map function
            StringBuilder map = new StringBuilder();
            map.AppendLine("function() {");
            map.AppendLine("    if(isDateWise) {");
            map.AppendLine("        for (var m=0; m<groupBy.length; m++) {");
            map.AppendLine("            var ret = [groupBy[m]];");
            map.AppendLine("            var dt = new Date(groupBy[m]);");
            map.AppendLine("            for (var i=0; i<locations.length; i++) {");
            map.AppendLine("                if (locations[i] === this.Employee.WorkState && this.StartDate.getDate() == dt.getDate() && this.StartDate.getMonth() == dt.getMonth() && this.StartDate.getFullYear() == dt.getFullYear()) {");
            map.AppendLine("                    ret.push(1);");
            map.AppendLine("                } else {");
            map.AppendLine("                    ret.push(0);");
            map.AppendLine("                }");
            map.AppendLine("            }");
            map.AppendLine("            emit(groupBy[m], { val: ret });");
            map.AppendLine("        }");
            map.AppendLine("    }");
            map.AppendLine("    else {");
            map.AppendLine("        for (var m=0; m<groupBy.length; m++) {");
            map.AppendLine("            var mnth = groupBy[m].split(':')[0];");
            map.AppendLine("            var mnthname = groupBy[m].split(':')[1];");
            map.AppendLine("            var yr = groupBy[m].split(':')[2];");
            map.AppendLine("            var group = mnthname + \" '\" + yr.slice(-2);");
            map.AppendLine("            var ret = [group];");
            map.AppendLine("            for (var i=0; i<locations.length; i++) {");
            map.AppendLine("                if (locations[i] === this.Employee.WorkState && this.StartDate.getMonth() == (mnth-1) && this.StartDate.getFullYear() == yr) {");
            map.AppendLine("                    ret.push(1);");
            map.AppendLine("                } else {");
            map.AppendLine("                    ret.push(0);");
            map.AppendLine("                }");
            map.AppendLine("            }");
            map.AppendLine("            emit(m, { val: ret });");
            map.AppendLine("        }");
            map.AppendLine("    }");
            map.AppendLine("}");

            // Define our reduce function
            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(key, values) {");
            reduce.AppendLine(" var result = { val: [values[0].val[0]] };");
            reduce.AppendLine(" values.forEach(function(value) {");
            reduce.AppendLine("     for (var i=1; i<value.val.length; i++) {");
            reduce.AppendLine("         result.val[i] = result.val[i] || 0;");
            reduce.AppendLine("         result.val[i] += (value.val[i] || 0);");
            reduce.AppendLine("     }");
            reduce.AppendLine(" });");
            reduce.AppendLine(" return result;");
            reduce.AppendLine("}");

            // make month year array to group data
            List<string> arrMonthYear = new List<string>();
            DateTime nextDate = startDate;
            do
            {
                arrMonthYear.Add(nextDate.Month.ToString() + ":" + nextDate.ToString("MMM") + ":" + nextDate.Year.ToString());
                nextDate = nextDate.AddMonths(1);

            } while (nextDate < endDate);

            // Define scope document for map reduce
            string[] groupBy = CreateGroupByForBIReportTotalLeaves(reportCriteria);

            Dictionary<string, object> scopeDoc = new Dictionary<string, object>
            {
                { "locations", Case.AsQueryable(userReportCriteriaData.SecurityUser).Select(m => m.Employee.WorkState).Distinct().ToArray()},
                { "monthyear", arrMonthYear.ToArray() },
                { "groupBy", groupBy },
                { "isDateWise", IsDateWiseComparison(reportCriteria) }
            };

            // Define our map reduce arguments
            MapReduceArgs mapReduce = new MapReduceArgs
            {
                OutputMode = MapReduceOutputMode.Inline,
                Query = query,
                Scope = new ScopeDocument(scopeDoc),
                MapFunction = new BsonJavaScript(map.ToString()),
                ReduceFunction = new BsonJavaScript(reduce.ToString())
            };

            // Execute our map reduce query operation against the ToDoItem collection
            var response = Case.Repository.Collection.MapReduce(mapReduce);
            List<List<object>> resultList = null;
            if (response != null)
            {
                resultList = SetResultListBILineReport(response);
            }
            return await Task.FromResult(resultList);
        }

        /// <summary>
        /// Sets the BI Report Result for Chart
        /// </summary>
        /// <param name="reportCriteria"></param>
        /// <param name="response"></param>
        /// <returns></returns>
        private List<List<object>> SetResultListBIReport(ReportCriteria reportCriteria, MapReduceResult response)
        {
            List<List<object>> resultList;
            int currIndex = 0;
            if (reportCriteria.Filters.FirstOrDefault(m => m.Name == "CurrReportIndex") != null && reportCriteria.Filters.FirstOrDefault(m => m.Name == "CurrReportIndex").Value != null)
            {
                currIndex = Convert.ToInt16(reportCriteria.ValueOf<string>("CurrReportIndex"));
            }

            resultList = new List<List<object>>();
            // Loop through our inline results and pump out the series data results to the series data collection
            //  on the result object
            foreach (var r in response.InlineResults)
            {
                object[] rData = new object[] { r.GetValue("_id"), r.GetValue("value"), currIndex };
                resultList.Add(rData.ToList());
            }

            return resultList;
        }

        /// <summary>
        /// Sets the BI Report Result for Line Chart
        /// </summary>
        /// <param name="reportCriteria"></param>
        /// <param name="response"></param>
        /// <returns></returns>
        private List<List<object>> SetResultListBILineReport(MapReduceResult response)
        {
            List<List<object>> resultList;           
            resultList = new List<List<object>>();
            // Loop through our inline results and pump out the series data results to the series data collection
            //  on the result object
            foreach (var r in response.InlineResults)
            {
                object[] rData = r.GetValue("value").AsBsonDocument.GetValue("val").AsBsonArray.Select(b => b.GetRawValue<object>()).ToArray();
                if (rData != null)
                    resultList.Add(rData.ToList());
            }

            return resultList;
        }

        /// <summary>
        /// Get Case for BI Reports
        /// </summary>
        /// <param name="userReportCriteriaData"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilter"></param>
        /// <returns></returns>
        private List<IMongoQuery> GetCaseForBIReports(UserReportCriteriaData userReportCriteriaData, ReportCriteria reportCriteria, string employerIdFilter)
        {
            List<IMongoQuery> ands = new List<IMongoQuery>
            {
                Case.Query.IsNotDeleted()
            };
            if (userReportCriteriaData.SecurityUser != null)
            {
                ands.Add(userReportCriteriaData.SecurityUser.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunBIReport, "Employee._id"));
            }
            else if (!string.IsNullOrWhiteSpace(reportCriteria.CustomerId))
            {
                ands.Add(Case.Query.EQ(e => e.CustomerId, reportCriteria.CustomerId));
            }
            ApplyEmployerFilter<Case>(ands, reportCriteria, employerIdFilter);
            return ands;
        }

        /// <summary>
        /// Checks if Date wise comparision [Duration] applied for criteria
        /// </summary>
        /// <param name="reportCriteria"></param>
        /// <returns></returns>
        private bool IsDateWiseComparison(ReportCriteria reportCriteria)
        {
            ReportCriteriaItem durationCriteria = reportCriteria.Filters.FirstOrDefault(m => m.Name == "Duration");

            if (durationCriteria != null && durationCriteria.Value!=null)
            {
                Enum.TryParse(durationCriteria.Value.ToString(), out DurationCriteria criteria);
                switch (criteria)
                {
                    case DurationCriteria.Monthly:
                    case DurationCriteria.DateRange:
                        return true;

                    case DurationCriteria.Quarterly:
                    case DurationCriteria.Yearly:
                    case DurationCriteria.Rolling:
                        return false;
                }
            }
            return false;
        }


        // Cognitive Complexity of methods should not be too high
        /// <summary>
        /// Creates GroupBy for BI Report Total leaves
        /// </summary>
        /// <param name="reportCriteria"></param>
        /// <returns></returns>
        private string[] CreateGroupByForBIReportTotalLeaves(ReportCriteria reportCriteria)
        {
            ReportCriteriaItem durationCriteria = reportCriteria.Filters.FirstOrDefault(m => m.Name == "Duration");
            string[] groupby = new string[] { };          
            string groupLabelFormat = "M:MMM:yyyy";
            int size = 0;
            if (durationCriteria != null && durationCriteria.Value != null)
            {
                switch (durationCriteria.Value.ToString())
                {
                    case "Monthly":
                    case "DateRange":                       
                        groupLabelFormat = "MM/dd/yyy";
                        size = DateTime.DaysInMonth(reportCriteria.StartDate.Year, reportCriteria.StartDate.Month);
                        groupby = CreateGroupByData(reportCriteria, size, groupLabelFormat);
                        break;
                    case "Quarterly":
                        size = 3;
                        groupby = CreateGroupByData(reportCriteria, size, groupLabelFormat, false);
                        break;
                    case "Yearly":
                        size = 12;
                        groupby = CreateGroupByData(reportCriteria, size, groupLabelFormat, false);
                        break;
                    case "Rolling":
                        size = ((reportCriteria.EndDate.Year - reportCriteria.StartDate.Year) * 12) + reportCriteria.EndDate.Month - reportCriteria.StartDate.Month;
                        groupby = CreateGroupByData(reportCriteria, size, groupLabelFormat, false);
                        break;
                }
            }
            return groupby;
        }

        /// <summary>
        /// Build and Run User Activity Report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async override Task<(List<AbsenceSoft.Data.Security.User>, Dictionary<string, int>, Dictionary<string, int>, Dictionary<string, int>, Dictionary<string, int>)> RunUserActivityReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            var startDate = new BsonDateTime(criteria.StartDate.ToMidnight());
            var endDate = new BsonDateTime(criteria.EndDate.EndOfDay());

            // Group results, captures userId + count of
            Dictionary<string, int> toDos = new Dictionary<string, int>();
            Dictionary<string, int> caseNotes = new Dictionary<string, int>();
            Dictionary<string, int> empNotes = new Dictionary<string, int>();
            Dictionary<string, int> attachments = new Dictionary<string, int>();

            var userFilter = user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport);

            var caseIdQuery = Case.Query.And(user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport, "Employee._id"), Case.Query.IsNotDeleted());
            List<string> caseIds = Case.Query.Find(caseIdQuery).Select(c => c.Id).ToList();

            List<Action> operations = new List<Action>(4)
            {
                new Action(() =>
                {
                    toDos = DoAggregation(
                        ToDoItem.Query.And(userFilter,
                        ToDoItem.Query.GTE(e => e.ModifiedDate, startDate),
                        ToDoItem.Query.LTE(e => e.ModifiedDate, endDate),
                        ToDoItem.Query.EQ(e => e.Status, ToDoItemStatus.Complete),
                        ToDoItem.Query.In(e => e.CaseId, caseIds)),
                        ToDoItem.Repository.Collection);
                }),
                new Action(() =>
                {
                    caseNotes = DoAggregation(
                        CaseNote.Query.And(userFilter,
                        CaseNote.Query.GTE(e => e.ModifiedDate, startDate),
                        CaseNote.Query.LTE(e => e.ModifiedDate, endDate),
                        CaseNote.Query.In(e => e.CaseId, caseIds)),
                        CaseNote.Repository.Collection);
                }),
                new Action(() =>
                {
                    empNotes = DoAggregation(
                        EmployeeNote.Query.And(userFilter,
                        EmployeeNote.Query.GTE(e => e.ModifiedDate, startDate),
                        EmployeeNote.Query.LTE(e => e.ModifiedDate, endDate)),
                        EmployeeNote.Repository.Collection);
                }),
                new Action(() =>
                {
                    attachments = DoAggregation(
                        Attachment.Query.And(userFilter,
                        Attachment.Query.GTE(e => e.ModifiedDate, startDate),
                        Attachment.Query.LTE(e => e.ModifiedDate, endDate)),
                        Attachment.Repository.Collection);
                })
            };
            operations.AsParallel().ForAll(a => a());

            var distinctUserIds = toDos.Keys.Union(caseNotes.Keys).Union(empNotes.Keys).Union(attachments.Keys).Distinct().ToList();
            var users = AbsenceSoft.Data.Security.User.AsQueryable().Where(u => distinctUserIds.Contains(u.Id)).Skip((pageNumber - 1) * pageSize).Take(pageSize);

            if (!string.IsNullOrEmpty(sortBy))
            {
                users = users.OrderBy(sortBy, orderByAsc);
            }
            else
            {
                users = users.OrderBy(u => u.LastName).ThenBy(u => u.FirstName);
            }

            return await Task.FromResult((users.ToList(), toDos, caseNotes, empNotes, attachments));
        }

        /// <summary>
        /// Build and Run user Activity Detail Report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public async override Task<(Dictionary<string, string>, Dictionary<string, CaseModel>, List<CaseNote>, List<EmployeeNote>, List<Attachment>)> RunUserActivityDetailReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria)
        {
            var startDate = new BsonDateTime(criteria.StartDate.ToMidnight());
            var endDate = new BsonDateTime(criteria.EndDate.EndOfDay());

            List<CaseNote> caseNotes = new List<CaseNote>();
            List<EmployeeNote> empNotes = new List<EmployeeNote>();
            List<Attachment> attachments = new List<Attachment>();

            var userFilter = user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport);
            var caseIds = Case.Repository.Collection
                .FindAs<BsonDocument>(Query.And(user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport, "Employee._id"), Case.Query.IsNotDeleted()))
                .SetFields(Fields.Include("_id", "CaseNumber", "Employee._id", "Employee.EmployeeNumber", "Employee.FirstName", "Employee.LastName", "CurrentOfficeLocation", "CustomerId", "EmployerId"))
                .ToDictionary(k => k["_id"].ToString(), v => new CaseModel
                {
                    CaseNumber = v["CaseNumber"].ToString(),
                    EmployeeNumber = v["Employee"]["EmployeeNumber"].ToString(),
                    EmployeeId = v["Employee"]["_id"].ToString(),
                    OfficeLocation = v.GetRawValue<string>("CurrentOfficeLocation"),
                    Name = string.Concat(v["Employee"]["FirstName"], " ", v["Employee"]["LastName"])
                });

            var users = AbsenceSoft.Data.Security.User.Repository.Collection
                .FindAs<BsonDocument>(Query.And(AbsenceSoft.Data.Security.User.Query.EQ(u => u.CustomerId, user.CustomerId), AbsenceSoft.Data.Security.User.Query.IsNotDeleted()))
                .SetFields(Fields.Include("_id", "FirstName", "LastName"))
                .ToDictionary(k => k["_id"].ToString(), v => string.Concat(v.Contains("FirstName") ? v["FirstName"] : "", " ", v.Contains("LastName") ? v["LastName"] : "").Trim());

            List<Action> operations = new List<Action>()
            {
                new Action(() =>
                {
                   caseNotes= CaseNote.Query.Find(
                        CaseNote.Query.And(userFilter,
                        CaseNote.Query.GTE(e => e.CreatedDate, startDate),
                        CaseNote.Query.LTE(e => e.CreatedDate, endDate),
                        CaseNote.Query.In(e => e.CaseId, caseIds.Keys)))
                        .ToList();
                   empNotes= EmployeeNote.Query.Find(
                        EmployeeNote.Query.And(userFilter,
                        EmployeeNote.Query.GTE(e => e.CreatedDate, startDate),
                        EmployeeNote.Query.LTE(e => e.CreatedDate, endDate),
                        EmployeeNote.Query.In(e => e.EmployeeId, caseIds.Select(v => v.Value.EmployeeId))))
                        .ToList();
                }),
                new Action(() =>
                {
                   attachments= Attachment.Query.Find(
                        Attachment.Query.And(userFilter,
                        Attachment.Query.GTE(e => e.CreatedDate, startDate),
                        Attachment.Query.LTE(e => e.CreatedDate, endDate),
                        Attachment.Query.In(e => e.CaseId, caseIds.Keys)))
                        .ToList();
                })
            };
            operations.AsParallel().ForAll(a => a());
            return await Task.FromResult((users, caseIds, caseNotes, empNotes, attachments));

        }

        /// <summary>
        /// Does the aggregation.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query.</param>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        private Dictionary<string, int> DoAggregation<T>(IMongoQuery query, MongoCollection<T> collection)
        {
            var args = new AggregateArgs()
            {
                OutputMode = AggregateOutputMode.Inline,
                Pipeline = new List<BsonDocument>
                {
                    BsonDocument.Parse("{ $match: " + query.ToString() + " }"),
                    BsonDocument.Parse("{ $group: { _id: \"$mby\", count: { $sum: 1 } } }")
                }
            };
            return collection.Aggregate(args).ToDictionary(
                        i => i.GetRawValue<string>("_id"),
                        i => i.GetRawValue<int>("count"));
        }

        /// <summary>
        /// Build and Run Intermittent Pattern utilization report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async override Task<List<Case>> RunPatternUtilizationReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            List<IMongoQuery> ands = new List<IMongoQuery>();
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            ands.Add(user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport, "Employee._id"));

            ApplyEmployerFilter<Case>(ands, criteria, employerIdFilterName);

            ands.Add(Case.Query.NE(e => e.Status, CaseStatus.Cancelled));
            ands.Add(Case.Query.ElemMatch(c => c.Segments, q => q.And(q.EQ(s => s.Type, CaseType.Intermittent),
                q.ElemMatch(s => s.UserRequests, q2 => q2.And(q2.GTE(e => e.RequestDate, new BsonDateTime(startDate)),
                q2.LTE(e => e.RequestDate, new BsonDateTime(endDate)))))));

            if (criteria.Filters != null && criteria.Filters.Any())
            {
                if (criteria.Filters.FirstOrDefault(m => m.Name == "AbsenceReason") != null && !string.IsNullOrWhiteSpace(criteria.Filters.FirstOrDefault(m => m.Name == "AbsenceReason").Value?.ToString()))
                {
                    ands.Add(Case.Query.EQ(e => e.Reason.Id, criteria.Filters.FirstOrDefault(m => m.Name == "AbsenceReason").Value.ToString()));
                }

                if (criteria.Filters.FirstOrDefault(m => m.Name == "WorkState") != null && !string.IsNullOrWhiteSpace(criteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value?.ToString()))
                {
                    ands.Add(Case.Query.EQ(e => e.Employee.WorkState, criteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value.ToString()));
                }

                AppendOrganizationFilterQuery(user, criteria, employerIdFilterName, queries: ands, employeeIdField: "Employee._id");

                var employeeCriteria = criteria.Filters.FirstOrDefault(m => m.Name == "EmployeeNumber");
                if (employeeCriteria != null && !string.IsNullOrWhiteSpace(employeeCriteria.Value?.ToString()) && !string.IsNullOrWhiteSpace(employeeCriteria.ValueAs<string>()))
                {
                    ands.Add(Case.Query.EQ(e => e.Employee.EmployeeNumber, employeeCriteria.Value));
                }
            }
            if (string.IsNullOrWhiteSpace(sortBy))
            {
                sortBy = "_id";
            }
            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null)?.SetSortOrder(orderByAsc ? SortBy.Ascending(sortBy) : SortBy.Descending(sortBy)).SetLimit(pageSize).SetSkip((pageNumber - 1) * pageSize);
            return await Task.FromResult(query.ToList());
        }

        /// <summary>
        /// Delete a saved adhoc report
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public override async Task<int> DeleteSavedAdhocReportAsync(DeleteReportArgs args)
        {
            var user = AbsenceSoft.Data.Security.User.GetById(args.UserKey);
            var ids = args.AdhocReportIds.Split(',');
            //defining the rows to be deleted. This is bacause to make sure improper ids are not in count
            int rows = 0;
            foreach (var id in ids)
            {
                if (!string.IsNullOrWhiteSpace(id))
                {
                    var definition = ReportDefinition.AsQueryable(user).FirstOrDefault(d => d.Id == id);
                    if (definition != null)
                    {
                        definition.Delete();
                        rows++;
                    }
                }
            }
            //return default value
            return await Task.FromResult(rows);
        }

        /// <summary>
        /// Method builds and run Accomodation By Decision Report query applying the reportcriterias and aggregation
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="EmployerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async override Task<List<List<object>>> RunAccomodationByDecisionReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            // Set our series data for the chart
            var seriesData = new List<List<object>>();

            // check if there is any case exists
            if (Case.AsQueryable(user).Count() <= 0)
            {
                return await Task.FromResult(seriesData);
            }

            // Define our first row, which contains series labels for the x (col 1) and y (col 2~n) axis.
            List<object> labels = new List<object>();

            labels.Add("AccommodationReq");

            // Our Y-Axis series data will be defined as all available To-Do item types (could be a busy chart)
            labels.AddRange(Enum.GetValues(typeof(AdjudicationStatus)).OfType<AdjudicationStatus>().OrderBy(t => (int)t).Select(t => t.ToString().SplitCamelCaseString()));

            // Add our series definition/labels row to the result series data collection.
            seriesData.Add(labels);

            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(Case.Query.IsNotDeleted());
            if (user != null)
            {
                ands.Add(user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunBIReport, "Employee._id"));
            }
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
            {
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            }
            ApplyEmployerFilter<ToDoItem>(ands, criteria, employerIdFilterName);

            // Only those case should be get considered which has Accommodation Request
            ands.Add(Case.Query.EQ(e => e.IsAccommodation, true));

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
            ));
            if (criteria.Filters.FirstOrDefault(m => m.Name == "WorkState")?.Value != null && !string.IsNullOrWhiteSpace(criteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value.ToString()))
            {
                ands.Add(Case.Query.EQ(e => e.Employee.WorkState, criteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value));
            }
            var locationFilter = criteria.Filters.FirstOrDefault(m => m.Name == "Location");
            if (locationFilter != null && locationFilter.Values != null && locationFilter.Values.Any())
            {
                var val = locationFilter.ValuesAs<string>();
                if (val != null && val.Any())
                {
#pragma warning disable CS0618
                    ands.Add(Case.Query.In(e => e.Employee.Info.OfficeLocation, val));
#pragma warning restore CS0618
                }
            }
            if (criteria.Filters.FirstOrDefault(m => m.Name == "AccomType")?.Value != null)
            {
                AccommodationType enumAccomTypeFilter = criteria.ValueOf<AccommodationType>("AccomType");
                ands.Add(Case.Query.ElemMatch(c => c.AccommodationRequest.Accommodations, q => q.EQ(s => s.Type, enumAccomTypeFilter)));

            }
            IMongoQuery query = ands.Count > 0 ? Case.Query.And(ands) : null;
            return await RunAccomodationByDecisionMapReduce(query, criteria, user, seriesData);
        }

        private async Task<List<List<object>>> RunAccomodationByDecisionMapReduce(IMongoQuery query, ReportCriteria criteria, AbsenceSoft.Data.Security.User user, List<List<object>> seriesData)
        {
            // Define our Map function
            // Define our Map function
            StringBuilder map = new StringBuilder();
            map.AppendLine("function() {");
            map.AppendLine("    var c = this;");
            map.AppendLine("    if (c.Accom != null && c.Accom.Accommodations != null) {");
            map.AppendLine("    for (var i=0; i<c.Accom.Accommodations.length; i++) {");
            map.AppendLine("        var accom = c.Accom.Accommodations[i];");
            map.AppendLine("        for (var k=0; k<types.length; k++) {");
            map.AppendLine("            if (types[k] === accom.Type.Code) {");
            map.AppendLine("                var ret = [strTypes[k]];");
            map.AppendLine("                for (var j=0; j<adjStatus.length; j++) {");
            map.AppendLine("                    if (adjStatus[j] === accom.Determination) {");
            map.AppendLine("                        ret.push(1);");
            map.AppendLine("                    } else {");
            map.AppendLine("                        ret.push(0);");
            map.AppendLine("                    }");
            map.AppendLine("                }");
            map.AppendLine("            emit(accom.Type.Name, { val: ret });");
            map.AppendLine("            }");
            map.AppendLine("        }");
            map.AppendLine("    }");
            map.AppendLine("    }");
            map.AppendLine("}");

            // Define our reduce function
            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(key, values) {");
            reduce.AppendLine(" var result = { val: [values[0].val[0]] };");
            reduce.AppendLine(" values.forEach(function(value) {");
            reduce.AppendLine("     for (var i=1; i<value.val.length; i++) {");
            reduce.AppendLine("         result.val[i] = result.val[i] || 0;");
            reduce.AppendLine("         result.val[i] += (value.val[i] || 0);");
            reduce.AppendLine("     }");
            reduce.AppendLine(" });");
            reduce.AppendLine(" return result;");
            reduce.AppendLine("}");

            var adjStatus = Enum.GetValues(typeof(AdjudicationStatus)).OfType<AdjudicationStatus>().OrderBy(t => (int)t).ToList();
            var accommodationTypes = new AccommodationService().Using(s => s.GetAccommodationTypes(criteria.CustomerId, user.Employers[0].EmployerId));
            var strAccommodationTypes = accommodationTypes.Select(t => t.Name);

            Dictionary<String, object> lstScopeObjects = new Dictionary<string, object>();
            lstScopeObjects.Add("types", accommodationTypes.Select(t => t.Code).OrderBy(t => t).ToArray());
            lstScopeObjects.Add("strTypes", strAccommodationTypes.Select(t => t).OrderBy(t => t).ToArray());
            lstScopeObjects.Add("adjStatus", adjStatus.Select(t => t).ToArray());

            // Define our map reduce arguments
            MapReduceArgs mapReduce = new MapReduceArgs();
            mapReduce.OutputMode = MapReduceOutputMode.Inline;
            mapReduce.Query = query;
            mapReduce.Scope = new ScopeDocument(lstScopeObjects);
            mapReduce.MapFunction = new BsonJavaScript(map.ToString());
            mapReduce.ReduceFunction = new BsonJavaScript(reduce.ToString());

            // Execute our map reduce query operation against the ToDoItem collection
            var response = Case.Repository.Collection.MapReduce(mapReduce);
            // If not OK or we have an error, throw an exception
            if (response.Ok && string.IsNullOrWhiteSpace(response.ErrorMessage))
            {
                // Loop through our inline results and pump out the series data results to the series data collection
                //  on the result object
                foreach (var r in response.InlineResults)
                {
                    object[] rData = r.GetValue("value").AsBsonDocument.GetValue("val").AsBsonArray.Select(b => b.GetRawValue<object>()).ToArray();
                    if (rData != null)
                    {
                        seriesData.Add(rData.ToList());
                    }
                }
            }
            return await Task.FromResult(seriesData);
        }

        /// <summary>
        /// Method builds and run Accomodation By Similary Situated Report query applying the reportcriterias and aggregation
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="EmployerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async override Task<List<Case>> RunAccomodationBySimilarySituatedQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            List<Case> result = null;
            if (Case.AsQueryable(user).Count() <= 0)
            {
                return await Task.FromResult(result);
            }
            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(Case.Query.IsNotDeleted());
            if (user != null)
            {
                ands.Add(user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunBIReport, "Employee._id"));
            }
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
            {
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            }
            ApplyEmployerFilter<ToDoItem>(ands, criteria, employerIdFilterName);

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
            ));

            if (criteria.Filters.FirstOrDefault(m => m.Name == "WorkState")?.Value != null)
            {
                ands.Add(Case.Query.EQ(e => e.Employee.WorkState, criteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value));
            }

            var locationFilter = criteria.Filters.FirstOrDefault(m => m.Name == "Location");
            if (locationFilter != null && locationFilter.Values != null && locationFilter.Values.Any())
            {
                var val = locationFilter.ValuesAs<string>();
                if (val != null && val.Any())
                {
#pragma warning disable CS0618
                    ands.Add(Case.Query.In(e => e.Employee.Info.OfficeLocation, val));
#pragma warning restore CS0618
                }
            }

            if (criteria.Filters.FirstOrDefault(m => m.Name == "Gender")?.Value != null)
            {
                Gender enumGenderFilter = criteria.ValueOf<Gender>("Gender");
                ands.Add(Case.Query.EQ(e => e.Employee.Gender, enumGenderFilter));
            }
            // Age criteria
            if (criteria.AgeFrom.HasValue)
            {
                ands.Add(Case.Query.GTE(m => (DateTime.Now.Year - m.Employee.DoB.Value.Year), criteria.AgeFrom.Value));
            }
            if (criteria.AgeTo.HasValue)
            {
                ands.Add(Case.Query.LTE(m => (DateTime.Now.Year - m.Employee.DoB.Value.Year), criteria.AgeTo.Value));
            }
            // take case which has accommodation request
            ands.Add(Case.Query.NE(e => e.AccommodationRequest, null));

            var query = Case.Query.Find(Case.Query.And(ands));
            if (string.IsNullOrWhiteSpace(sortBy))
            {
                sortBy = "_id";
            }
            if (orderByAsc)
            {
                query = query.SetSortOrder(SortBy.Ascending(sortBy));
            }
            else
            {
                query = query.SetSortOrder(SortBy.Descending(sortBy));
            }

            result = await Task.FromResult(query.SetLimit(pageSize).SetSkip((pageNumber - 1) * pageSize).ToList());
            return result;
        }

        /// <summary>
        ///  Method to fetch Accommodation Type based on corresponding CustomerId and EmpployerId
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="employerId"></param>
        /// <returns></returns>
        public async override Task<List<AccommodationType>> GetAccomodationTypes(string customerId, string employerId)
        {
            List<AccommodationType> result = null;
            using (var acomService = new AccommodationService())
            {
                result = acomService.GetAccommodationTypes(customerId, employerId);
            }
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Method builds and run Accomodation By Decision Report query applying the reportcriterias and aggregation
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="EmployerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async override Task<List<List<object>>> RunAccomodationBySummaryReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            // Set our series data for the chart
            var seriesData = new List<List<object>>();

            // check if there is any case exists
            if (Case.AsQueryable(user).Count() <= 0)
            {
                return await Task.FromResult(seriesData);
            }

            // Define our first row, which contains series labels for the x (col 1) and y (col 2~n) axis.
            List<object> labels = new List<object>();

            labels.Add("AccommodationReq");
            labels.Add("Count");
            labels.Add("ChartIndex");

            // Add our series definition/labels row to the result series data collection.
            seriesData.Add(labels);

            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(Case.Query.IsNotDeleted());
            if (user != null)
            {
                ands.Add(user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunBIReport, "Employee._id"));
            }
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
            {
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            }
            ApplyEmployerFilter<ToDoItem>(ands, criteria, employerIdFilterName);

            // Only those case should be get considered which has Accommodation Request
            ands.Add(Case.Query.EQ(e => e.IsAccommodation, true));

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
            ));

            IMongoQuery query = ands.Count > 0 ? Case.Query.And(ands) : null;
            return await RunAccomodationBySummaryMapReduce(query, criteria, user, seriesData);
        }

        /// <summary>
        /// Method forms the mongo map reduce definition and runs the query.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="criteria"></param>
        /// <param name="user"></param>
        /// <param name="seriesData"></param>
        /// <returns></returns>
        private async Task<List<List<object>>> RunAccomodationBySummaryMapReduce(IMongoQuery query, ReportCriteria criteria, AbsenceSoft.Data.Security.User user, List<List<object>> seriesData)
        {
            // Define our Map function
            StringBuilder map = new StringBuilder();
            map.AppendLine("function() {");
            map.AppendLine("  if (this.Accom != null) {");
            map.AppendLine("  var acc = this.Accom.Accommodations ;");
            map.AppendLine("    for (var c=0; c<acc.length; c++) {");
            map.AppendLine("        for (var i=0; i<types.length; i++) {");
            map.AppendLine("              if (types[i] == acc[c].Type.Code) {");
            map.AppendLine("                    emit(strTypes[i], 1);");
            map.AppendLine("              }");
            map.AppendLine("        }");
            map.AppendLine("    }");
            map.AppendLine("  }");
            map.AppendLine("}");


            // Define our reduce function
            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(status, count) {");
            reduce.AppendLine(" return Array.sum(count); ");
            reduce.AppendLine("}");

            var accommodationTypes = new AccommodationService().Using(s => s.GetAccommodationTypes(criteria.CustomerId, user.Employers[0].EmployerId));
            var strAccommodationTypes = accommodationTypes.Select(t => t.Name);

            Dictionary<String, object> lstScopeObjects = new Dictionary<string, object>();
            lstScopeObjects.Add("types", accommodationTypes.Select(t => t.Code).OrderBy(t => t).ToArray());
            lstScopeObjects.Add("strTypes", strAccommodationTypes.Select(t => t).ToArray());

            // Define our map reduce arguments
            MapReduceArgs mapReduce = new MapReduceArgs();
            mapReduce.OutputMode = MapReduceOutputMode.Inline;
            mapReduce.Query = query;
            mapReduce.Scope = new ScopeDocument(lstScopeObjects);
            mapReduce.MapFunction = new BsonJavaScript(map.ToString());
            mapReduce.ReduceFunction = new BsonJavaScript(reduce.ToString());

            // Execute our map reduce query operation against the ToDoItem collection
            var response = Case.Repository.Collection.MapReduce(mapReduce);
            // If not OK or we have an error, throw an exception
            if (response.Ok && string.IsNullOrWhiteSpace(response.ErrorMessage))
            {
                // Report Index
                int currIndex = 0;
                if (criteria.Filters.FirstOrDefault(m => m.Name == "CurrReportIndex")?.Value != null)
                {
                    currIndex = Convert.ToInt16(criteria.ValueOf<string>("CurrReportIndex"));
                }

                // Loop through our inline results and pump out the series data results to the series data collection
                //  on the result object
                foreach (var r in response.InlineResults)
                {
                    object[] rData = new object[] { r.GetValue("_id"), r.GetValue("value"), currIndex };
                    seriesData.Add(rData.ToList());
                }
            }
            return await Task.FromResult(seriesData);
        }

        /// <summary>
        /// Method builds and run Cases Exceed Frequency Duration Report query applying the reportcriterias and aggregation
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="EmployerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async override Task<List<Case>> RunCasesExceedFrequencyDurationReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport, "Employee._id"));
            ApplyEmployerFilter<Case>(ands, criteria, employerIdFilterName);

            ands.Add(Case.Query.ElemMatch(c => c.Segments, q => q.And(
                q.EQ(s => s.Type, CaseType.Intermittent),
                q.ElemMatch(s => s.UserRequests, q2 => q2.And(
                    q2.GTE(e => e.RequestDate, new BsonDateTime(startDate)),
                    q2.LTE(e => e.RequestDate, new BsonDateTime(endDate)))))));


            criteria.ApplyForOptionalFilter<CaseStatus>("CaseStatus", v => ands.Add(Case.Query.EQ(e => e.Status, v)));
            criteria.ApplyForOptionalFilter<string>("AbsenceReason", v => ands.Add(Case.Query.EQ(e => e.Reason.Id, v)));
            criteria.ApplyForOptionalFilter<string>("WorkState", v => ands.Add(Case.Query.EQ(e => e.Employee.WorkState, v)));
#pragma warning disable CS0618
            criteria.ApplyForOptionalFilter<string>("Location", v => ands.Add(Case.Query.EQ(e => e.Employee.Info.OfficeLocation, v)));
#pragma warning restore CS0618
            criteria.ApplyForOptionalFilter<string>("EmployeeNumber", v => ands.Add(Case.Query.EQ(e => e.Employee.EmployeeNumber, v)));

            var query = Case.Query.Find(Case.Query.And(ands));
            if (string.IsNullOrWhiteSpace(sortBy))
            {
                sortBy = "_id";
            }
            if (orderByAsc)
            {
                query = query.SetSortOrder(SortBy.Ascending(sortBy));
            }
            else
            {
                query = query.SetSortOrder(SortBy.Descending(sortBy));
            }

            var result = await Task.FromResult(query.SetLimit(pageSize).SetSkip((pageNumber - 1) * pageSize).ToList());
            return result;

        }

        /// <summary>
        /// Method builds and run Multiple Open Cases Report query applying the reportcriterias and aggregation
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="EmployerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async override Task<List<Case>> RunMultipleOpenCasesReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport, "Employee._id"));
            ApplyEmployerFilter<Case>(ands, criteria, employerIdFilterName);


            ands.Add(Case.Query.GTE(c => c.StartDate, new BsonDateTime(startDate)));
            ands.Add(Case.Query.LTE(c => c.EndDate, new BsonDateTime(endDate)));


            criteria.ApplyForOptionalFilter<CaseStatus>("CaseStatus", v => ands.Add(Case.Query.EQ(e => e.Status, v)));
            criteria.ApplyForOptionalFilter<string>("AbsenceReason", v => ands.Add(Case.Query.EQ(e => e.Reason.Id, v)));
            criteria.ApplyForOptionalFilter<string>("WorkState", v => ands.Add(Case.Query.EQ(e => e.Employee.WorkState, v)));
#pragma warning disable CS0618
            criteria.ApplyForOptionalFilter<string>("Location", v => ands.Add(Case.Query.EQ(e => e.Employee.Info.OfficeLocation, v)));
#pragma warning restore CS0618
            criteria.ApplyForOptionalFilter<string>("EmployeeNumber", v => ands.Add(Case.Query.EQ(e => e.Employee.EmployeeNumber, v)));

            var query = Case.Query.Find(Case.Query.And(ands));
            if (string.IsNullOrWhiteSpace(sortBy))
            {
                sortBy = "_id";
            }
            if (orderByAsc)
            {
                query = query.SetSortOrder(SortBy.Ascending(sortBy));
            }
            else
            {
                query = query.SetSortOrder(SortBy.Descending(sortBy));
            }

            var result = await Task.FromResult(query.SetLimit(pageSize).SetSkip((pageNumber - 1) * pageSize).ToList());
            return result;

        }


        /// <summary>
        /// Run Team Productivity Bi Report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async override Task<List<List<object>>> RunTeamProductivityBiQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            IEnumerable<string> collection = Enum.GetValues(typeof(ToDoItemType)).OfType<ToDoItemType>().OrderBy(t => (int)t).Select(t => t.ToString().SplitCamelCaseString());

            List<object> labels = new List<object>
            {
                "Employee"
            };

            List<List<object>> seriesData = AddSeriesLabels(collection, labels);

            IMongoQuery query = CreateBaseQuery(user, criteria, employerIdFilterName, true);

            StringBuilder map = new StringBuilder();
            var todoTypes = Enum.GetValues(typeof(ToDoItemType)).OfType<ToDoItemType>().OrderBy(t => (int)t).ToList();
            map.AppendLine("function() {");
            map.AppendLine("    var todo = this;");
            map.AppendLine("    var ret = [todo.AssignedToName];");
            map.AppendLine("    for (var i=0; i<types.length; i++) {");
            map.AppendLine("        if (types[i] === todo.ItemType) {");
            map.AppendLine("            ret.push(1);");
            map.AppendLine("        } else {");
            map.AppendLine("            ret.push(0);");
            map.AppendLine("        }");
            map.AppendLine("    }");
            map.AppendLine("    emit(todo.AssignedToId, { val: ret });");
            map.AppendLine("}");

            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(key, values) {");
            reduce.AppendLine(" var result = { val: [values[0].val[0]] };");
            reduce.AppendLine(" values.forEach(function(value) {");
            reduce.AppendLine("     for (var i=1; i<value.val.length; i++) {");
            reduce.AppendLine("         result.val[i] = result.val[i] || 0;");
            reduce.AppendLine("         result.val[i] += (value.val[i] || 0);");
            reduce.AppendLine("     }");
            reduce.AppendLine(" });");
            reduce.AppendLine(" return result;");
            reduce.AppendLine("}");

            Dictionary<string, object> scopeDocument = new Dictionary<string, object>() { { "types", todoTypes.Select(t => (int)t).OrderBy(t => t).ToArray() } };
            return await GetSeriesData(seriesData, query, map, reduce, scopeDocument);
        }

        /// <summary>
        /// Add the series labels
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="labels"></param>
        /// <returns></returns>
        private List<List<object>> AddSeriesLabels(IEnumerable<string> collection, List<object> labels)
        {
            var seriesData = new List<List<object>>();

            if (ToDoItem.AsQueryable().Count() <= 0)
            {
                return seriesData;
            }

            labels.AddRange(collection);
            seriesData.Add(labels);
            return seriesData;
        }

        /// <summary>
        /// Get the series data
        /// </summary>
        /// <param name="seriesData"></param>
        /// <param name="query"></param>
        /// <param name="map"></param>
        /// <param name="reduce"></param>
        /// <param name="scopeDocument"></param>
        /// <returns></returns>
        private static async Task<List<List<object>>> GetSeriesData(List<List<object>> seriesData, IMongoQuery query, StringBuilder map, StringBuilder reduce, Dictionary<string, object> scopeDocument)
        {
            MapReduceArgs mapReduce = new MapReduceArgs
            {
                OutputMode = MapReduceOutputMode.Inline,
                Query = query,
                Scope = new ScopeDocument(scopeDocument),
                MapFunction = new BsonJavaScript(map.ToString()),
                ReduceFunction = new BsonJavaScript(reduce.ToString())
            };

            var response = ToDoItem.Repository.Collection.MapReduce(mapReduce);
            if (response.Ok)
            {
                foreach (var r in response.InlineResults)
                {
                    object[] rData = r.GetValue("value").AsBsonDocument.GetValue("val").AsBsonArray.Select(b => b.GetRawValue<object>()).ToArray();
                    if (rData != null)
                    {
                        seriesData.Add(rData.ToList());
                    }
                }
            }
            return await Task.FromResult(seriesData);
        }

        /// <summary>
        /// Run Team Compliance BI Report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async override Task<List<List<object>>> RunTeamComplianceBiQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            IEnumerable<string> collection = Enum.GetValues(typeof(ToDoItemStatus)).OfType<ToDoItemStatus>().Where(t => t.Equals(ToDoItemStatus.Overdue) || t.Equals(ToDoItemStatus.Complete)).OrderBy(t => (int)t).Select(t => t.ToString().SplitCamelCaseString());

            List<object> labels = new List<object>
            {
                "Users"
            };

            List<List<object>> seriesData = AddSeriesLabels(collection, labels);

            IMongoQuery query = CreateBaseQuery(user, criteria, employerIdFilterName);

            var todoStatuses = Enum.GetValues(typeof(ToDoItemStatus)).OfType<ToDoItemStatus>().Where(t => t.Equals(ToDoItemStatus.Overdue) || t.Equals(ToDoItemStatus.Complete)).OrderBy(t => (int)t).ToList();

            StringBuilder map = new StringBuilder();
            map.AppendLine("function() {");
            map.AppendLine("    var todo = this;");
            map.AppendLine("    var ret = [todo.AssignedToName];");
            map.AppendLine("    for (var i=0; i<types.length; i++) {");
            map.AppendLine("        if (types[i] === todo.Status) {");
            map.AppendLine("            ret.push(1);");
            map.AppendLine("        } else {");
            map.AppendLine("            ret.push(0);");
            map.AppendLine("        }");
            map.AppendLine("    }");
            map.AppendLine("    emit(todo.AssignedToId, { val: ret });");
            map.AppendLine("}");

            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(key, values) {");
            reduce.AppendLine(" var result = { val: [values[0].val[0]] };");
            reduce.AppendLine(" values.forEach(function(value) {");
            reduce.AppendLine("     for (var i=1; i<value.val.length; i++) {");
            reduce.AppendLine("         result.val[i] = result.val[i] || 0;");
            reduce.AppendLine("         result.val[i] += (value.val[i] || 0);");
            reduce.AppendLine("     }");
            reduce.AppendLine(" });");
            reduce.AppendLine(" return result;");
            reduce.AppendLine("}");

            Dictionary<string, object> scopeDocument = new Dictionary<string, object>() { { "types", todoStatuses.Select(t => (int)t).OrderBy(t => t).ToArray() } };
            return await GetSeriesData(seriesData, query, map, reduce, scopeDocument);
        }

        /// <summary>
        /// Run CaseProcessingMetricsBiReport
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public async override Task<List<List<object>>> RunCaseProcessingMetricsBiQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc)
        {
            IEnumerable<string> collection = Enum.GetValues(typeof(ToDoItemType)).OfType<ToDoItemType>().OrderBy(t => (int)t).Select(t => t.ToString().SplitCamelCaseString());

            List<object> labels = new List<object>
            {
                "Month"
            };

            List<List<object>> seriesData = AddSeriesLabels(collection, labels);

            IMongoQuery query = CreateBaseQuery(user, criteria, employerIdFilterName, true);

            StringBuilder map = new StringBuilder();
            var todoTypes = Enum.GetValues(typeof(ToDoItemType)).OfType<ToDoItemType>().OrderBy(t => (int)t).ToList();
            map.AppendLine("function() {");
            map.AppendLine("    var todo = this;");
            map.AppendLine("    if(isDateWise) {");
            map.AppendLine("        for (var m=0; m<groupBy.length; m++) {");
            map.AppendLine("            var ret = [groupBy[m]];");
            map.AppendLine("            var dt = new Date(groupBy[m]);");
            map.AppendLine("            for (var i=0; i<types.length; i++) {");
            map.AppendLine("                if (types[i] === todo.ItemType && todo.cdt.getDate() == dt.getDate() && todo.cdt.getMonth() == dt.getMonth() && todo.cdt.getFullYear() == dt.getFullYear()) {");
            map.AppendLine("                    ret.push(1);");
            map.AppendLine("                } else {");
            map.AppendLine("                    ret.push(0);");
            map.AppendLine("                }");
            map.AppendLine("            }");
            map.AppendLine("            emit(groupBy[m], { val: ret });");
            map.AppendLine("        }");
            map.AppendLine("    }");
            map.AppendLine("    else {");
            map.AppendLine("        for (var m=0; m<groupBy.length; m++) {");
            map.AppendLine("            var mnth = groupBy[m].split(':')[0];");
            map.AppendLine("            var mnthname = groupBy[m].split(':')[1];");
            map.AppendLine("            var yr = groupBy[m].split(':')[2];");
            map.AppendLine("            var group = mnthname + \" '\" + yr.slice(-2);");
            map.AppendLine("            var ret = [group];");
            map.AppendLine("            for (var i=0; i<types.length; i++) {");
            map.AppendLine("                if (types[i] === todo.ItemType && todo.cdt.getMonth() == (mnth - 1) && todo.cdt.getFullYear() == yr) {");
            map.AppendLine("                    ret.push(1);");
            map.AppendLine("                } else {");
            map.AppendLine("                    ret.push(0);");
            map.AppendLine("                }");
            map.AppendLine("            }");
            map.AppendLine("            emit(m, { val: ret });");
            map.AppendLine("        }");
            map.AppendLine("    }");
            map.AppendLine("}");

            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(key, values) {");
            reduce.AppendLine(" var result = { val: [values[0].val[0]] };");
            reduce.AppendLine(" values.forEach(function(value) {");
            reduce.AppendLine("     for (var i=1; i<value.val.length; i++) {");
            reduce.AppendLine("         result.val[i] = result.val[i] || 0;");
            reduce.AppendLine("         result.val[i] += (value.val[i] || 0);");
            reduce.AppendLine("     }");
            reduce.AppendLine(" });");
            reduce.AppendLine(" return result;");
            reduce.AppendLine("}");

            string[] groupBy = CreateGroupBy(criteria);

            Dictionary<string, object> scopeDocument = new Dictionary<string, object>
            {
                { "types", todoTypes.Select(t => (int)t).OrderBy(t => t).ToArray() },
                { "groupBy", groupBy },
                { "isDateWise", IsDateWiseComparison(criteria) }
            };

            return await GetSeriesData(seriesData, query, map, reduce, scopeDocument);
        }

        /// <summary>
        /// Create Base Query For BI Report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="includeDateCriteria"></param>
        /// <returns></returns>
        private IMongoQuery CreateBaseQuery(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, bool includeDateCriteria = false)
        {
            List<IMongoQuery> ands = new List<IMongoQuery>
            {
                ToDoItem.Query.IsNotDeleted()
            };
            if (user != null)
            {
                ands.Add(user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunBIReport, "EmployeeId"));
            }
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
            {
                ands.Add(ToDoItem.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            }
            ApplyEmployerFilter<ToDoItem>(ands, criteria, employerIdFilterName);

            if (includeDateCriteria)
            {
                DateTime startDate = criteria.StartDate.ToMidnight();
                DateTime endDate = criteria.EndDate.EndOfDay();
                ands.Add(ToDoItem.Query.Or(
                    ToDoItem.Query.And(ToDoItem.Query.GTE(e => e.CreatedDate, new BsonDateTime(startDate)), ToDoItem.Query.LTE(e => e.CreatedDate, new BsonDateTime(endDate)))
                ));
            }
            return ToDoItem.Query.And(ands);
        }

        /// <summary>
        /// Create Group By data
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        private string[] CreateGroupBy(ReportCriteria criteria)
        {
            ReportCriteriaItem durationCriteria = criteria.Filters.FirstOrDefault(m => m.Name == "Duration");
            string[] groupby = new string[] { };
            string groupLabelFormat = "M:MMM:yyyy";
            int size = 0;
            if (durationCriteria != null)
            {
                var duration = durationCriteria.Value != null ? durationCriteria.Value.ToString() : "Monthly";
                switch (duration)
                {
                    case "Monthly":
                        groupLabelFormat = "MM/dd/yyy";
                        size = DateTime.DaysInMonth(criteria.StartDate.Year, criteria.StartDate.Month);
                        groupby = CreateGroupByData(criteria, size, groupLabelFormat);
                        break;
                    case "DateRange":
                        groupLabelFormat = "MM/dd/yyy";
                        size = Convert.ToInt32(Math.Round(criteria.EndDate.Subtract(criteria.StartDate).TotalDays));
                        groupby = CreateGroupByData(criteria, size, groupLabelFormat);
                        break;
                    case "Quarterly":
                        size = 3;
                        groupby = CreateGroupByData(criteria, size, groupLabelFormat, false);
                        break;
                    case "Yearly":
                        size = 12;
                        groupby = CreateGroupByData(criteria, size, groupLabelFormat, false);
                        break;
                    case "Rolling":
                        size = ((criteria.EndDate.Year - criteria.StartDate.Year) * 12) + criteria.EndDate.Month - criteria.StartDate.Month;
                        groupby = CreateGroupByData(criteria, size, groupLabelFormat, false);
                        break;
                }
            }
            return groupby;
        }


        /// <summary>
        /// Create Group by data series
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="size"></param>
        /// <param name="groupLabelFormat"></param>
        /// <param name="dayWise"></param>
        /// <returns></returns>
        private string[] CreateGroupByData(ReportCriteria criteria, int size, string groupLabelFormat, bool dayWise=true)
        {
            string[] groupby = new string[size];
            int i = 0;
            DateTime nxtDate = criteria.StartDate;
            do
            {
                groupby[i] = nxtDate.ToString(groupLabelFormat);
                i++;
                if (i >= size)
                    break;
                if (dayWise)
                {
                    nxtDate = nxtDate.AddDays(1);
                }
                else
                {
                    nxtDate = nxtDate.AddMonths(1);
                }
            } while (nxtDate < criteria.EndDate);

            return groupby;
        }


        /// <summary>
        /// Get the team for current user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public override async Task<List<Team>> GetTeamsForCurrentUser(AbsenceSoft.Data.Security.User user)
        {
            return await Task.FromResult(Team.AsQueryable(user).Where(p => p.CustomerId == user.CustomerId).ToList());
        }

        /// <summary>
        /// Get the emails ids of the users from scheduled shared report recipient list.
        /// </summary>
        /// <param name="rcptList"></param>
        /// <returns></returns>
        public override async Task<List<string>> GetScheduledReportRecipientEmailIds(List<SharedReportRecipient> rcptList)
        {
            List<string> mailingList = new List<string>();
            rcptList.ForEach(rcpt =>
            {
                if (rcpt.UserKey != null)
                {
                    var user = AbsenceSoft.Data.Security.User.GetById(rcpt.UserKey);
                    if (user != null)
                    {
                        mailingList.Add(user.Email);
                    }
                }
                if (rcpt.TeamKey != null)
                {
                    var teamMembers = TeamMember.AsQueryable().Where(p => p.TeamId == rcpt.TeamKey).Select(k => k.User.Email).ToList();
                    if (teamMembers?.Count > 0)
                    {
                        mailingList.AddRange(teamMembers);
                    }
                }
                if (rcpt.RoleName != null)
                {
                    var roleEmails = AbsenceSoft.Data.Security.User.AsQueryable().Where(p => p.Roles.Contains(rcpt.RoleName)).Select(k => k.Email).ToList();
                    mailingList.AddRange(roleEmails);
                }
            });
            return await Task.FromResult(mailingList.Distinct().ToList());
        }

        /// <summary>
        /// Get the user's customer's Base Url
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public override async Task<string> GetCustomerBaseUrl(User user)
        {
            var customer=Customer.GetById(user.CustomerKey);
            return await Task.FromResult(customer?.GetBaseUrl());
        }

        /// <summary>
        /// Get the list of Adhoc Reports
        /// </summary>
        /// <returns></returns>
        public override async Task<List<AdHocReportView>> GetAdhocReportTypesAsync()
        {
           return await Task.FromResult(ReportType.AsQueryable().Select(rt => new AdHocReportView(rt, false)).ToList());
        }

        /// <summary>
        /// Runs the intermittent schedule exception query asynchronous.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="reportCriteria">The report criteria.</param>
        /// <param name="employerIdFilterName">Name of the employer identifier filter.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="sortBy">The sort by.</param>
        /// <param name="orderByAsc">if set to <c>true</c> [order by asc].</param>
        /// <returns></returns>
        public override async Task<List<IntermittentScheduleExceptionDataRow>> RunIntermittentScheduleExceptionQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria reportCriteria, string employerIdFilterName, int? pageNumber, int? pageSize, string sortBy, bool orderByAsc)
        {
            List<IntermittentScheduleExceptionDataRow> returnValue = new List<IntermittentScheduleExceptionDataRow>();

            // Get our start and end date from the criteria
            DateTime startDate = reportCriteria.StartDate.ToMidnight();
            DateTime endDate = reportCriteria.EndDate.EndOfDay();

            // Create the mongo query and apply the data access filters
            List<IMongoQuery> criteria = new List<IMongoQuery>
            {
                user.BuildDataAccessFilters(AbsenceSoft.Data.Security.Permission.RunCaseManagerReport, "Employee._id")
            };

            // Apply the employer filter
            ApplyEmployerFilter<Case>(criteria, reportCriteria, employerIdFilterName);

            // Apply general case filters
            reportCriteria.ApplyForOptionalFilter<string>("CaseAssignee", v => criteria.Add(Case.Query.EQ(e => e.AssignedToId, v)));
            reportCriteria.ApplyForOptionalFilter<string>("WorkState", v => criteria.Add(Case.Query.EQ(e => e.Employee.WorkState, v)));
            reportCriteria.ApplyForOptionalFilter<string>("Location", v => criteria.Add(Case.Query.EQ(e => e.Employee.Info.OfficeLocation, v)));

            // Manual case status = Open filter
            criteria.Add(Case.Query.EQ(c => c.Status, CaseStatus.Open));

            // Now apply date filters against the TORs
            criteria.Add(Case.Query.ElemMatch(c => c.Segments, s =>
                s.And(
                    s.EQ(segment => segment.Type, CaseType.Intermittent),
                    s.ElemMatch(t => t.UserRequests, r => r.And(
                        // Only care where we have approved time over 0
                        r.ElemMatch(b => b.Detail, b => b.GT(f => f.Approved, 0)),
                        r.GTE(e => e.RequestDate, new BsonDateTime(startDate)),
                        r.LTE(e => e.RequestDate, new BsonDateTime(endDate))
                    ))
                )
            ));

            // Query cases
            var cases = Case.Query.Find(Case.Query.And(criteria)).ToList();
            var employeeIds = cases.Select(c => c.Employee.Id).Distinct().ToList();
            var employees = Employee.AsQueryable().Where(e => employeeIds.Contains(e.Id)).ToList();

            foreach (var c in cases)
            {
                // Get the employee from the queried list of impacted employees
                var emp = employees.FirstOrDefault(e => e.Id == c.Employee.Id) ?? c.Employee;

                var usage = c.Segments
                   .Where(s => s.Type == CaseType.Intermittent)
                   .SelectMany(s => s.AppliedPolicies
                       // Ignore calendar type, only worry about "work" types
                       .Where(p => p.PolicyReason.EntitlementType?.CalendarType() != true)
                       // Get only where usage is zero or less
                       .SelectMany(p => p.Usage
                       // Get our container for comparison, minutes will always be zero, leave it default
                       .Select(u => new ReportDataContainerForPolicyTimeUsed()
                       {
                           PolicyCode = p.Policy.Code,
                           DateUsed = u.DateUsed,
                           Scheduled = Convert.ToInt32(u.MinutesInDay)
                       })))
                   .ToList();

                var tors = c.Segments
                    .Where(s => s.Type == CaseType.Intermittent)
                    .SelectMany(s => s.UserRequests)
                    // Filter just to approved minutes, and get data containers
                    .SelectMany(r => r.Detail.Where(m => m.Approved > 0)
                        .Select(d => new ReportDataContainerForPolicyTimeUsed()
                        {
                            PolicyCode = d.PolicyCode,
                            DateUsed = r.RequestDate,
                            // We only care about approved minutes
                            Minutes = d.Approved,
                            // Need to get the scheduled minutes for this date and policy
                            Scheduled = usage.FirstOrDefault(u => u.DateUsed.Date == r.RequestDate.Date && u.PolicyCode == d.PolicyCode && u.Scheduled < d.Approved)?.Scheduled ?? 0
                        }))
                    .ToList();



                // Determine the exceptions where zero days intersect with approved TORs (> 0 time approved)
                var exceptions = tors.Where(t => t.Minutes > t.Scheduled).ToList();

                // This will ensure we only add items to the detail that are actually exceptions
                foreach (var item in exceptions.GroupBy(e => new { e.DateUsed, e.Minutes,e.Scheduled }))
                {
                    // Build the result item data
                    IntermittentScheduleExceptionDataRow data = new IntermittentScheduleExceptionDataRow
                    {
                        EmployeeId = emp.Id,
                        EmployeeNumber = emp.EmployeeNumber,
                        EmployeeName = emp.FullName,
                        CaseNumber = c.CaseNumber,
                        RequestedDate = item.Key.DateUsed,
                        RequestedTime = item.Key.Minutes,
                        PolicyCode = string.Join("; ", item.Select(i => i.PolicyCode)),
                        AssignedTo = c.AssignedToName,
                        EmployeeLastModified = emp.ModifiedDate,
                        ScheduleLastEffective = emp.WorkSchedules.Any() ? (DateTime?)emp.WorkSchedules.Max(s => s.StartDate) : null
                    };
                    returnValue.Add(data);
                }
            }

            // Build the final list, with sort and paging.
            var finalList = returnValue.AsEnumerable();

            if (!string.IsNullOrWhiteSpace(sortBy))
            {
                Func<IntermittentScheduleExceptionDataRow, IComparable> sorter = null;
                switch (sortBy)
                {
                    case "EmployeeId":
                        sorter = new Func<IntermittentScheduleExceptionDataRow, IComparable>(r => r.EmployeeId);
                        break;
                    case "EmployeeNumber":
                        sorter = new Func<IntermittentScheduleExceptionDataRow, IComparable>(r => r.EmployeeNumber);
                        break;
                    case "EmployeeName":
                        sorter = new Func<IntermittentScheduleExceptionDataRow, IComparable>(r => r.EmployeeName);
                        break;
                    case "CaseNumber":
                        sorter = new Func<IntermittentScheduleExceptionDataRow, IComparable>(r => r.CaseNumber);
                        break;
                    case "RequestedDate":
                        sorter = new Func<IntermittentScheduleExceptionDataRow, IComparable>(r => r.RequestedDate);
                        break;
                    case "RequestedTime":
                        sorter = new Func<IntermittentScheduleExceptionDataRow, IComparable>(r => r.RequestedTime);
                        break;
                    case "PolicyCode":
                        sorter = new Func<IntermittentScheduleExceptionDataRow, IComparable>(r => r.PolicyCode);
                        break;
                    case "AssignedTo":
                        sorter = new Func<IntermittentScheduleExceptionDataRow, IComparable>(r => r.AssignedTo);
                        break;
                    case "EmployeeLastModified":
                        sorter = new Func<IntermittentScheduleExceptionDataRow, IComparable>(r => r.EmployeeLastModified);
                        break;
                    case "ScheduleLastEffective":
                        sorter = new Func<IntermittentScheduleExceptionDataRow, IComparable>(r => r.ScheduleLastEffective);
                        break;
                    default:
                        break;
                }
                finalList = orderByAsc ? finalList.OrderBy(sorter) : finalList.OrderByDescending(sorter);
            }
            if (pageNumber.HasValue && pageSize.HasValue)
            {
                finalList = finalList.Skip(pageNumber.Value * pageSize.Value).Take(pageSize.Value);
            }

            return await Task.FromResult(finalList.ToList());
        }
    }
}