﻿using AbsenceSoft.Data.Cases;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Data.Reporting.Model
{
    [Serializable]
    public class ToDoMapReduceResultItemItem
    {
        [BsonDefaultValue(0)]
        public int ToDos { get; set; }

        [BsonIgnoreIfNull]
        public Case Case { get; set; }
    }
}
