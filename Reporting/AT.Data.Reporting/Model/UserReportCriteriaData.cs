﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AT.Entities.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Data.Reporting.Model
{
    /// <summary>
    /// Encapsulated the data required for report operations
    /// </summary>
    public class UserReportCriteriaData
    {
        /// <summary>
        /// Set the logged in user details
        /// </summary>
        public User AuthenticatedUser { get; set; }

        /// <summary>
        /// Get or Set the security user
        /// </summary>
        public AbsenceSoft.Data.Security.User SecurityUser { get; set; }

        /// <summary>
        /// Set the customer info of the logged in user
        /// </summary>
        public Customer Customer { get; set; }

        /// <summary>
        /// Set the employers of the logged in user
        /// </summary>
        public List<Employer> Employers { get; set; }

        /// <summary>
        /// Get & Set the user absence reasons
        /// </summary>
        public List<AbsenceReason> AbsenceReasons { get; set; }

        /// <summary>
        /// Get or Set locations
        /// </summary>
        public List<Organization> Locations { get; set; }

        /// <summary>
        /// Get & Sets Work States those are available for user
        /// </summary>
        public List<string> WorkStates { get; set; }

        /// <summary>
        /// Get/Set the accommodation types for the user
        /// </summary>
        public List<AccommodationType> AccommodationTypes { get; set; }

        /// <summary>
        /// The To DO status list
        /// </summary>
        public List<ToDoItemStatus> ToDoItemStatuses { get; set; }

        /// <summary>
        /// The list of users those are associated with assigned to
        /// </summary>
        public List<AbsenceSoft.Data.Security.User> AssignedToList { get; set; }

        /// <summary>
        /// List of users to whom to-do are assigned
        /// </summary>
        public List<AbsenceSoft.Data.Security.User> ToDoAssignedToList { get; set; }

        /// <summary>
        /// List of diagnosis codes
        /// </summary>
        public Dictionary<string, string> DiagnosisCodes { get; set; }

        /// <summary>
        /// Get & Set the organization types
        /// </summary>
        public List<OrganizationByType> OrganizationTypes { get; set; }

    }
}
