﻿using AT.Entities.Reporting.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Data.Reporting.Model
{
    public class AdHocReportCategory
    {
        public string Name { get; set; }

        public List<AdHocReportField> Fields { get; set; }
    }
}
