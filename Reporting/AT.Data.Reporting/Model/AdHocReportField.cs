﻿using AbsenceSoft.Data.Reporting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Data.Reporting.Model
{
    public class AdHocReportField
    {
        public AdHocReportField(ReportField field)
        {
            this.DisplayName = field.Display;
            this.Name = field.Name;
        }

        public AdHocReportField(SavedReportField field)
        {
            this.Name = field.Name;
            this.Order = field.Order;
        }

        public string Name { get; set; }
        public string DisplayName { get; set; }
        public int? Order { get; set; }
        public bool IncludedInReport { get; set; }
    }
}
