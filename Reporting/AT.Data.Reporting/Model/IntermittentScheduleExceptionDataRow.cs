﻿using System;

namespace AT.Data.Reporting.Model
{
    [Serializable]
    public class IntermittentScheduleExceptionDataRow
    {
        /// <summary>
        /// Gets or sets the employee identifier.
        /// </summary>
        /// <value>
        /// The employee identifier.
        /// </value>
        public string EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the employee number.
        /// </summary>
        /// <value>
        /// The employee number.
        /// </value>
        public string EmployeeNumber { get; set; }

        /// <summary>
        /// Gets or sets the name of the employee.
        /// </summary>
        /// <value>
        /// The name of the employee.
        /// </value>
        public string EmployeeName { get; set; }

        /// <summary>
        /// Gets or sets the case identifier.
        /// </summary>
        /// <value>
        /// The case identifier.
        /// </value>
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the case number.
        /// </summary>
        /// <value>
        /// The case number.
        /// </value>
        public string CaseNumber { get; set; }

        /// <summary>
        /// Gets or sets the requested date.
        /// </summary>
        /// <value>
        /// The requested date.
        /// </value>
        public DateTime RequestedDate { get; set; }

        /// <summary>
        /// Gets or sets the requested time.
        /// </summary>
        /// <value>
        /// The requested time.
        /// </value>
        public int RequestedTime { get; set; }

        /// <summary>
        /// Gets or sets the scheduled time.
        /// </summary>
        /// <value>
        /// The scheduled time.
        /// </value>
        public int ScheduledTime { get; set; }


        /// <summary>
        /// Gets or sets the policy code.
        /// </summary>
        /// <value>
        /// The policy code.
        /// </value>
        public string PolicyCode { get; set; }

        /// <summary>
        /// Gets or sets the assigned to.
        /// </summary>
        /// <value>
        /// The assigned to.
        /// </value>
        public string AssignedTo { get; set; }

        /// <summary>
        /// Gets or sets the employee last modified.
        /// </summary>
        /// <value>
        /// The employee last modified.
        /// </value>
        public DateTime EmployeeLastModified { get; set; }

        /// <summary>
        /// Gets or sets the schedule last effective.
        /// </summary>
        /// <value>
        /// The schedule last effective.
        /// </value>
        public DateTime? ScheduleLastEffective { get; set; }
    }
}
