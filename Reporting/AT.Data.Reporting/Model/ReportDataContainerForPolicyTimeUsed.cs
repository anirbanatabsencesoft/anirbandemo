﻿using System;
using System.Collections.Generic;

namespace AT.Data.Reporting.Model
{
    internal class ReportDataContainerForPolicyTimeUsed : IEqualityComparer<ReportDataContainerForPolicyTimeUsed>
    {
        /// <summary>
        /// Gets or sets the policy code.
        /// </summary>
        /// <value>
        /// The policy code.
        /// </value>
        public string PolicyCode { get; set; }

        /// <summary>
        /// Gets or sets the date used.
        /// </summary>
        /// <value>
        /// The date used.
        /// </value>
        public DateTime DateUsed { get; set; }

        /// <summary>
        /// Gets or sets the minutes.
        /// </summary>
        /// <value>
        /// The minutes.
        /// </value>
        public int Minutes { get; set; }

        /// <summary>
        /// Gets or sets the scheduled.
        /// </summary>
        /// <value>
        /// The scheduled.
        /// </value>
        public int Scheduled { get; set; }
        /// <summary>
        /// Determines whether the specified objects are equal.
        /// </summary>
        /// <param name="x">The first object of type <paramref name="T" /> to compare.</param>
        /// <param name="y">The second object of type <paramref name="T" /> to compare.</param>
        /// <returns>
        /// true if the specified objects are equal; otherwise, false.
        /// </returns>
        public bool Equals(ReportDataContainerForPolicyTimeUsed x, ReportDataContainerForPolicyTimeUsed y)
        {
            return x.DateUsed.Date == y.DateUsed.Date
                && x.PolicyCode == y.PolicyCode;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public int GetHashCode(ReportDataContainerForPolicyTimeUsed obj)
        {
            return base.GetHashCode();
        }
    }
}
