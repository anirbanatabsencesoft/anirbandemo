﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Data.Reporting.Model
{
    [Serializable]
    public class ToDoMapReduceResultItem
    {
        [BsonId, BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }

        public ToDoMapReduceResultItemItem value { get; set; }
    }
}
