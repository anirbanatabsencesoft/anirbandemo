using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using AT.Data.Reporting.Base;

namespace AT.Data.Reporting.Base
{
	public class DataProviderCollection : System.Configuration.Provider.ProviderCollection {
        
		/// <summary>
        /// Returns the notification provider based on name
        /// </summary>
		/// <param name="name"></param>
		new public BaseDataProvider this[string name]
        {
            get { return (BaseDataProvider)base[name]; }
        }
	}
}