using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading.Tasks;
using AbsenceSoft.Data.Reporting;
using AT.Entities.Reporting.Classes;
using AT.Entities.Authentication;
using AT.Data.Reporting.Model;
using System;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Reporting;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Notes;
using MongoDB.Driver;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AT.Entities.Reporting.Enums;
using AbsenceSoft.Data.Jobs;
using System.Linq;
using AT.Entities.Reporting.Entities;
using AT.Entities.Reporting.Models;

namespace AT.Data.Reporting.Base
{
    /// <summary>
    /// The Base Data Provider Class
    /// </summary>
    public abstract class BaseDataProvider : System.Configuration.Provider.ProviderBase
    {
        /// <summary>
        /// This method to be overriden for setting up the config parameters
        /// </summary>
        /// <param name="collection"></param>
        public virtual void SetConfig(NameValueCollection collection)
        {
        }

        /// <summary>
        /// Get the list of the Reports & Report Definition
        /// </summary>
        /// <returns></returns>
        public abstract Task<List<ReportDefinition>> GetAllDefinitions();

        /// <summary>
        /// Get the report and report definition by id
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public abstract Task<ReportDefinition> GetDefinitionByName(string name);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reportTypeId"></param>
        /// <returns></returns>
        public abstract Task<List<ReportField>> GetReportColumns(string reportTypeId);

        /// <summary>
        /// Returns all report type fields
        /// </summary>
        /// <returns></returns>
        public abstract Task<List<ReportTypeField>> GetAllReportTypeFieldsAsync();

        /// <summary>
        /// Returns all report fields
        /// </summary>
        /// <returns></returns>
        public abstract Task<List<ReportField>> GetAllReportFieldsAsync();

        /// <summary>
        /// Get the list of the Report types
        /// </summary>
        /// <returns></returns>
        public abstract Task<List<ReportType>> GetReportTypesAsync();

        /// <summary>
        /// Saves the definition of a report.
        /// </summary>
        /// <param name="reportColumnDefinition">The definition of the report or the definition of the report to duplicate.</param>
        /// <param name="currentUser">Current User</param>
        /// <param name="cloneReport">Whether to save the report under a new name and clones the existing report and assigns a new object id</param>
        /// <returns>The report definition.</returns>
        public abstract Task<ReportColumnDefinition> SaveReportDefinitionAsync(ReportColumnDefinition reportColumnDefinition, User currentUser, bool cloneReport = false);

        /// <summary>
        /// Returns the user data related to a report
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public abstract Task<UserReportCriteriaData> GetUserReportCriteriaData(UserReportCriteriaData data);
        
        /// <summary>
        /// Returns the work state for a user
        /// </summary>
        /// <param name="data"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public abstract Task<List<string>> GetUserWorkState(UserReportCriteriaData data, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Returns the custom fields available for criteria for the user
        /// </summary>
        /// <param name="data"></param>
        /// <param name="fieldType"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public abstract Task<List<AbsenceSoft.Data.Customers.CustomField>> GetUserCustomField(UserReportCriteriaData data, CustomFieldType? fieldType = null, EntityTarget? target = null);

        /// <summary>
        /// Return the case for given CaseId
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public abstract Task<List<Case>> GetCaseById(List<string> caseId, int pageNumber, int pageSize);

        /// <summary>
        /// Gets the EmployerAudit CaseNote for given User
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public abstract Task<IEnumerable<EmployerAudit<CaseNote>>> GetEmployerAuditCase(UserReportCriteriaData user, int pageNumber, int pageSize);

        /// <summary>
        /// Gets the User list for given UserIds
        /// </summary>
        /// <param name="UserIds"></param>
        /// <returns></returns>
        public abstract Task<IEnumerable<AbsenceSoft.Data.Security.User>> GetUsersById(List<string> userIds);

        /// <summary>
        /// Gets the User for given UserId
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public abstract Task<AbsenceSoft.Data.Security.User> GetSecurityUserById(User user);

        /// <summary>
        /// Gets the list of case for the passed parameter query
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public abstract Task<List<Case>> GetCase(IMongoQuery query, int pageNumber, int pageSize);

        /// <summary>
        /// Gets the Reasons for Case excluding Accommodation
        /// </summary>
        /// <returns></returns>
        public abstract Task<List<string>> GetCaseReasonsExcludingAccommodation();

        /// <summary>
        /// Gets the WorkState of Cases of a user
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public abstract Task<List<string>> GetCaseWorkState(UserReportCriteriaData data);

        /// <summary>
        /// Returns the recently used adhoc reports
        /// </summary>
        /// <param name="data"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public abstract Task<List<ReportDefinition>> GetRecentReportDefinitionList(UserReportCriteriaData data, int limit = 10);

        /// <summary>
        /// Build and run Absence status report query 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="EmployerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns>Case List</returns>
        public abstract Task<List<Case>> RunAbsenceReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria reportCriteria, string EmployerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc);

        /// <summary>
        /// Build and run Accommodation status report query 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="EmployerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public abstract Task<List<Case>> RunAccommodationReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria reportCriteria, string EmployerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc);

        /// <summary>
        /// Build and run Custom field Accommodation report query 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="EmployerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public abstract Task<List<Case>> RunCustomFieldReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria reportCriteria, string EmployerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc);

        /// <summary>
        /// Build and run Custome Accommodation status report query 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="EmployerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public abstract Task<List<Case>> RunCustomAccomDetailReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria reportCriteria, string EmployerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc);

        /// <summary>
        /// Build and run Custom Accommodation status report query 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="EmployerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public abstract Task<List<Case>> RunCustomAccomodationReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria reportCriteria, string EmployerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc);

        /// <summary>
        /// Get the Employee policy summary by case id
        /// </summary>
        /// <param name="caseId">Case identifier</param>
        /// <param name="includeStatus">Other eligibility statuses</param>
        /// <param name="policyCodes">Policy codes</param>
        /// <returns></returns>
        public abstract Task<List<PolicySummary>> GetEmployeePolicySummaryByCaseIdAsync(string caseId, EligibilityStatus[] includeStatus, params string[] policyCodes);

        /// <summary>
        /// Get all Employees Details
        /// </summary>
        /// <param name="employeeIds"></param>
        /// <returns>Returns all Employees</returns>
        public abstract Task<Dictionary<string, AbsenceSoft.Data.Customers.Employee>> GetEmployeeDetailsAsync(IList<string> employeeIds);

        /// <summary>
        /// Get the Employee contact details by Contact Type
        /// </summary>
        /// <param name="employeeIds"></param>
        /// <param name="contactTypeCode">Contact Type</param>
        /// <returns>Employee Contact details</returns>
        public abstract Task<Dictionary<string, List<AbsenceSoft.Data.Customers.EmployeeContact>>> GetEmployeeContactDetailsAsync(IList<string> employeeIds, string contactTypeCode);

        /// <summary>
        /// Get all the User Details by Customer
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns>Returns all user</returns>
        public abstract Task<Dictionary<string, string>> GetUserDetailsAsync(string customerId);

        /// <summary>
        /// Build and Run custom accommodations status granted report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public abstract Task<List<Case>> RunAccommodationStatusGrantedQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria);

        /// <summary>
        /// Get case list for disablity report
        /// </summary>
        /// <param name="data"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public abstract Task<List<Case>> GetCaseListForDisabilityReport(UserReportCriteriaData data, ReportCriteria criteria, string employerIdFilterName, int pageNumber = 1, int pageSize = 10);

        /// <summary>
        /// Build and Run custom accommodations status granted report
        /// </summary>
        /// <param name="user">Current User</param>
        /// <param name="criteria">Report Criteria</param>      
        /// <param name="pageNumber">Page number</param>
        /// <param name="pageSize">Page Size</param>
        /// <param name="sortBy">Sort by column name</param>
        /// <param name="orderByAsc">Order by column</param>
        /// <returns>Case list</returns>
        public abstract Task<List<Case>> RunAccommodationCustomBaseQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, int pageNumber, int pageSize, string sortBy, bool orderByAsc);

        /// <summary>
        /// Get the Case Notes by Case ids
        /// </summary>
        /// <param name="caseIds"></param>
        /// <returns></returns>
        public abstract Task<IList<CaseNote>> GetCaseNotesbyCaseIdsAsync(IList<string> caseIds);

        /// <summary>
        /// Get Accommodation Questions by customer id
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public abstract Task<IList<AccommodationQuestion>> GetAccommodationQuestionsByCustomerId(string customerId);

        /// <summary>
        /// Get case list for Daily Case Report
        /// </summary>
        /// <param name="data"></param>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public abstract Task<List<Case>> GetCaseListForDailyCaseReport(UserReportCriteriaData data, ReportCriteria criteria, string employerIdFilter, int pageNumber = 1, int pageSize = 10);

        /// <summary>
        /// Get case list for Daily Custom Case Report
        /// </summary>
        /// <param name="data"></param>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public abstract Task<List<Case>> GetCaseListForCustomDailyCaseReport(UserReportCriteriaData data, ReportCriteria criteria, string employerIdFilter, int pageNumber = 1, int pageSize = 10);

        /// <summary>
        /// Method builds and run the to-do query applying the reportcriterias
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="EmployerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public abstract Task<List<ToDoItem>> RunToDoItemReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria reportCriteria, string EmployerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc);

        /// <summary>
        /// Method queries EmployeeOrganization for a typecode including customerIds, EmployeeIds and EmployeeNumbers.
        /// </summary>
        /// <param name="grouptypeCode"></param>
        /// <param name="customerIds"></param>
        /// <param name="employerIds"></param>
        /// <param name="employeeNumbers"></param>
        /// <returns></returns>
        public abstract Task<List<EmployeeOrganization>> GetEmployeeOrganization(string grouptypeCode, List<string> customerIds, List<string> employerIds, List<string> employeeNumbers);

        /// <summary>
        /// Build and Run Communication Report query
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public abstract Task<List<Communication>> RunCommunicationQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc);

        /// <summary>
        /// Build and Run Employee on Leave Report query
        /// </summary>
        /// <param name="user">Current User</param>
        /// <param name="criteria">Report Criteria</param>      
        /// <param name="pageNumber">Page number</param>
        /// <param name="pageSize">Page Size</param>
        /// <param name="sortBy">Sort by column name</param>
        /// <param name="orderByAsc">Order by column</param>
        /// <returns>Case list</returns>
        public abstract Task<List<Case>> RunEmployeeOnLeaveReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, int pageNumber, int pageSize, string sortBy, bool orderByAsc);

        /// <summary>
        /// Gets Case List for Work Related Report
        /// </summary>
        /// <param name="data"></param>
        /// <param name="criteria"></param>
        /// <param name="reportGroupType"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public abstract Task<List<Case>> GetCaseListForWorkRelatedReport(UserReportCriteriaData data, ReportCriteria criteria, ReportGroupType? reportGroupType, string employerIdFilterName, int pageNumber = 1, int pageSize = 10);

        /// <summary>
        /// Gets Case List for Work Related Report
        /// </summary>
        /// <param name="data"></param>
        /// <param name="criteria"></param>
        /// <param name="reportGroupType"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public abstract Task<List<Case>> GetCaseListForWorkRestrictionByLocationAmazonReport(UserReportCriteriaData data, ReportCriteria reportCriteria, string employerIdFilterName, int pageNumber = 1, int pageSize = 10);

        /// <summary>
        /// Method builds and run the Cse without to-do reprot query applying the reportcriterias and aggregation
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="EmployerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public abstract Task<List<ToDoMapReduceResultItem>> RunCaseWithoutToDoReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria reportCriteria, string EmployerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc);

        /// <summary>
        /// Build and Run Employee Survey Report query
        /// </summary>
        /// <param name="user">Current User</param>
        /// <param name="criteria">Report Criteria</param>      
        /// <param name="pageNumber">Page number</param>
        /// <param name="pageSize">Page Size</param>
        /// <param name="sortBy">Sort by column name</param>
        /// <param name="orderByAsc">Order by column</param>
        /// <returns>Case list</returns>
        public abstract Task<List<Case>> RunEmployeeSurveyReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, int pageNumber, int pageSize, string sortBy, bool orderByAsc);

        /// <summary>
        /// Build and Run User Activity Report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public abstract Task<(List<AbsenceSoft.Data.Security.User>, Dictionary<string, int>, Dictionary<string, int>, Dictionary<string, int>, Dictionary<string, int>)> RunUserActivityReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, int pageNumber, int pageSize, string sortBy, bool orderByAsc);
        
        /// <summary>
        /// Build and Run user Activity Detail Report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public abstract Task<(Dictionary<string, string>, Dictionary<string, CaseModel>, List<CaseNote>, List<EmployeeNote>, List<Attachment>)> RunUserActivityDetailReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria);

        /// <summary>
        /// Build and Run Intermittent Pattern utilization report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public abstract Task<List<Case>> RunPatternUtilizationReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc);

        /// <summary>
        /// Get Employee Restriction for CaseIds
        /// </summary>
        /// <param name="data"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="caseIds"></param>
        /// <returns></returns>
        public abstract Task<List<EmployeeRestriction>> GetEmployeeRestrictionForCaseIds(UserReportCriteriaData data, ReportCriteria reportCriteria, List<string> caseIds);

        /// <summary>
        /// Get Demand for Restriction CaseIds
        /// </summary>
        /// <param name="employeeRestrictions"></param>
        /// <returns></returns>
        public abstract Task<List<Demand>> GetDemandForRestrictionCaseIds(List<EmployeeRestriction> employeeRestrictions);

        /// <summary>
        /// Gets Employee contact for given list of employee ids
        /// </summary>
        /// <param name="empIds"></param>
        /// <param name="contactTypeCode"></param>
        /// <returns></returns>
        public abstract Task<List<EmployeeContact>> GetEmployeeContact(List<string> empIds, string contactTypeCode);

        /// <summary>
        /// Get EmployeeJob for Associated Employee Number
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="empNums"></param>
        /// <returns></returns>
        public abstract Task<List<EmployeeJob>> GetEmployeeJobForAssociatedEmployeeNumber(string customerId, List<string> empNums);


        /// <summary>
        /// Delete the saved adhoc reports
        /// It also check whether the report is created by the user or not
        /// </summary>
        /// <param name="args">The delete argument to be set</param>
        /// <returns></returns>
        public abstract Task<int> DeleteSavedAdhocReportAsync(Arguments.DeleteReportArgs args);

        /// <summary>
        /// Method builds and run Accomodation By Decision Report query applying the reportcriterias and aggregation
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="EmployerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public abstract Task<List<List<object>>> RunAccomodationByDecisionReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria reportCriteria, string EmployerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc);

        /// <summary>
        /// Method builds and run Accomodation By Similary Situated Report query applying the reportcriterias and aggregation
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="EmployerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public abstract Task<List<Case>> RunAccomodationBySimilarySituatedQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria reportCriteria, string EmployerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc);

        /// <summary>
        /// Method to fetch Accommodation Type based on corresponding CustomerId and EmpployerId
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="EmployerId"></param>
        /// <returns></returns>
        public abstract Task<List<AccommodationType>> GetAccomodationTypes(string customerId, string employerId);

        /// <summary>
        /// Method builds and run Accomodation By Summary Report query applying the reportcriterias and aggregation
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="EmployerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public abstract Task<List<List<object>>> RunAccomodationBySummaryReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria reportCriteria, string EmployerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc);

        /// <summary>
        /// Method builds and run Cases Exceed Frequency Duration Report query applying the reportcriterias and aggregation
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="EmployerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public abstract Task<List<Case>> RunCasesExceedFrequencyDurationReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria reportCriteria, string EmployerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc);

        /// <summary>
        /// Method builds and run Multiple Open Cases Report query applying the reportcriterias and aggregation
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="EmployerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public abstract Task<List<Case>> RunMultipleOpenCasesReportQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria reportCriteria, string EmployerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc);



        /// <summary>
        /// Gets the Case count for user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public abstract Task<int> UserCaseCount(AbsenceSoft.Data.Security.User user);

        /// <summary>
        ///  Gets the Result for BI Report by Leave for the user
        /// </summary>
        /// <param name="userReportCriteriaData"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilter"></param>
        /// <returns></returns>
        public abstract Task<List<List<object>>> GetBiReportByLeaveResult(UserReportCriteriaData userReportCriteriaData, ReportCriteria reportCriteria, string employerIdFilter);

        /// <summary>
        ///  Gets the Result for BI Report by Location for the user
        /// </summary>
        /// <param name="userReportCriteriaData"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilter"></param>
        /// <returns></returns>
        public abstract Task<List<List<object>>> GetBiReportByLocationResult(UserReportCriteriaData userReportCriteriaData, ReportCriteria reportCriteria, string employerIdFilter);

        /// <summary>
        /// Gets the Result for BI Report by Reason for the user
        /// </summary>
        /// <param name="userReportCriteriaData"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilter"></param>
        /// <returns></returns>
        public abstract Task<List<List<object>>> GetBiReportByReasonResult(UserReportCriteriaData userReportCriteriaData, ReportCriteria reportCriteria, string employerIdFilter);

        /// <summary>
        /// Gets the Result for BI Report by Status for the user
        /// </summary>
        /// <param name="userReportCriteriaData"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilter"></param>
        /// <returns></returns>
        public abstract Task<List<List<object>>> GetBiReportByStatusResult(UserReportCriteriaData userReportCriteriaData, ReportCriteria reportCriteria, string employerIdFilter);

        /// <summary>
        /// Gets the Result for BI Report by Policy for the user
        /// </summary>
        /// <param name="userReportCriteriaData"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilter"></param>
        /// <returns></returns>
        public abstract Task<List<List<object>>> GetBiReportByPolicyResult(UserReportCriteriaData userReportCriteriaData, ReportCriteria reportCriteria, string employerIdFilter);
        
        /// <summary>
        /// Gets the Result for BI Report for Total leaves by Reason
        /// </summary>
        /// <param name="userReportCriteriaData"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilter"></param>
        /// <returns></returns>
        public abstract Task<List<List<object>>> GetBiReportTotalLeavesByReasonResult(UserReportCriteriaData userReportCriteriaData, ReportCriteria reportCriteria, string employerIdFilter);

        /// <summary>
        /// Gets the Result for BI Report for Total leaves by Total
        /// </summary>
        /// <param name="userReportCriteriaData"></param>
        /// <param name="reportCriteria"></param>
        /// <param name="employerIdFilter"></param>
        /// <returns></returns>
        public abstract Task<List<List<object>>> GetBiReportTotalLeavesByTotalResult(UserReportCriteriaData userReportCriteriaData, ReportCriteria reportCriteria, string employerIdFilter);

        /// <summary>
        /// Run Team Productivity Bi Report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public abstract Task<List<List<object>>> RunTeamProductivityBiQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc);

        /// <summary>
        /// Run Team Compliance BI Report
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public abstract Task<List<List<object>>> RunTeamComplianceBiQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc);


        /// <summary>
        /// Run CaseProcessingMetricsBiReport
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <param name="employerIdFilterName"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="orderByAsc"></param>
        /// <returns></returns>
        public abstract Task<List<List<object>>> RunCaseProcessingMetricsBiQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria criteria, string employerIdFilterName, int pageNumber, int pageSize, string sortBy, bool orderByAsc);

        /// <summary>
        /// Get the team for current user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public abstract Task<List<Team>> GetTeamsForCurrentUser(AbsenceSoft.Data.Security.User user);

        /// <summary>
        /// Returns the to-do assigned list
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public abstract Task<List<AbsenceSoft.Data.Security.User>> GetToDoAssignedList(AbsenceSoft.Data.Security.User user);

        /// <summary>
        /// Returns the to-do items types
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public abstract Task<Dictionary<ToDoItemType, string>> GetTodoItemTypes(AbsenceSoft.Data.Security.User user);

        /// <summary>
        /// Get the emails ids of the users from scheduled shared report recipient list.
        /// </summary>
        /// <param name="rcptList"></param>
        /// <returns></returns>
        public abstract Task<List<string>> GetScheduledReportRecipientEmailIds(List<SharedReportRecipient> rcptList);
        
        /// <summary>
        /// Get the user's customer's Base Url
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public abstract Task<string> GetCustomerBaseUrl(User user);

        /// <summary>
        /// Get the list of Adhoc Reports
        /// </summary>
        /// <returns></returns>
        public abstract Task<List<AdHocReportView>> GetAdhocReportTypesAsync();

        /// <summary>
        /// Runs the intermittent schedule exception query asynchronous.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="reportCriteria">The report criteria.</param>
        /// <param name="employerIdFilterName">Name of the employer identifier filter.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="sortBy">The sort by.</param>
        /// <param name="orderByAsc">if set to <c>true</c> [order by asc].</param>
        /// <returns></returns>
        public abstract Task<List<IntermittentScheduleExceptionDataRow>> RunIntermittentScheduleExceptionQueryAsync(AbsenceSoft.Data.Security.User user, ReportCriteria reportCriteria, string employerIdFilterName, int? pageNumber, int? pageSize, string sortBy, bool orderByAsc);
    }
}
