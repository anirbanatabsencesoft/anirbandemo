using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Configuration;

namespace AT.Data.Reporting.Base
{
	public class DataProviderConfiguration : System.Configuration.ConfigurationSection {
        /// <summary>
        /// Providers list
        /// </summary>
		[ConfigurationProperty("providers")]
		public System.Configuration.ProviderSettingsCollection Providers
        {
            get
            {
                return (ProviderSettingsCollection)base["providers"];
            }
        }
        /// <summary>
        /// Default provider
        /// </summary>
		[ConfigurationProperty("default", DefaultValue = "MongoDataProvider")]
		public string DefaultProvider
        {
            get
            {
                return base["default"] as string;
            }
        }
	}
}