﻿using AT.Entities.Authentication;
using AT.Logic.Arguments;
using AT.Logic.Reporting;
using AT.Logic.Reporting.Reports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Test.Reporting
{
    /// <summary>
    /// The Accommodation Reporting Test Class
    /// </summary>
    [TestClass]
    public class AccommodationReportTest
    {
        /// <summary>
        /// Reporting Logic
        /// </summary>
        private readonly ReportingLogic ReportingLogic;

        /// <summary>
        /// Current user
        /// </summary>
        private readonly User CurrentUser;
        public AccommodationReportTest()
        {
            CurrentUser = GetCurrentUser();
            ReportingLogic = new ReportingLogic(CurrentUser);
        }
        /// <summary>
        /// Test the Accommodation status Report
        /// </summary>
        [TestMethod]
        public void AccommodationBynoGroupReportTest()
        {
            ExecuteReportArgs executeReportArgs = GetAccommodationExecuteReportArg();
            CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Test the AccommodationsReport group by Case Manager
        /// </summary>
        [TestMethod]
        public void AccommodationByCaseManagerReportTest()
        {
            ExecuteReportArgs executeReportArgs = GetAccommodationExecuteReportArg();
            executeReportArgs.GroupType = Entities.Reporting.Enums.ReportGroupType.CaseManager;
            CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Test the AccommodationStatusReport group by OfficeLoacation Report
        /// </summary>
        [TestMethod]
        public void AccommodationByOfficeLoacationReportTest()
        {
            ExecuteReportArgs executeReportArgs = GetAccommodationExecuteReportArg();
            executeReportArgs.GroupType = Entities.Reporting.Enums.ReportGroupType.Location;
            CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Test the AccommodationStatusReport group by Organization Report
        /// </summary>
        [TestMethod]
        public void AccommodationByOrganizationReportTest()
        {
            ExecuteReportArgs executeReportArgs = GetAccommodationExecuteReportArg();
            executeReportArgs.GroupType = Entities.Reporting.Enums.ReportGroupType.Organization;
            CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Test the Accommodation Status Report group by Duration Report
        /// </summary>
        [TestMethod]
        public void AccommodationByDurationReportTest()
        {
            ExecuteReportArgs executeReportArgs = GetAccommodationExecuteReportArg();
            executeReportArgs.GroupType = Entities.Reporting.Enums.ReportGroupType.Duration;
            CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Test the Accommodation Status Report group by Accommodation Type
        /// </summary>
        [TestMethod]
        public void AccommodationByTypeReportTest()
        {
            ExecuteReportArgs executeReportArgs = GetAccommodationExecuteReportArg();
            executeReportArgs.GroupType = Entities.Reporting.Enums.ReportGroupType.AccommodationType;
            CanExecuteReportAsync(executeReportArgs);
        }
        
        /// <summary>
        /// Test the Accommodation Status Report group by Work state Report
        /// </summary>
        [TestMethod]
        public void AccommodationByWorkStateReportTest()
        {
            ExecuteReportArgs executeReportArgs = GetAccommodationExecuteReportArg();
            executeReportArgs.GroupType = Entities.Reporting.Enums.ReportGroupType.WorkState;
            CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Create dummy executeReport Argument to test
        /// </summary>
        /// <returns></returns>
        private ExecuteReportArgs GetAccommodationExecuteReportArg()
        {
            return new ExecuteReportArgs
            {
                ReportId = "EA3B77C2-374F-49ED-859F-54FDFE1066E4",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = Convert.ToDateTime("11-01-2017"),
                    EndDate = Convert.ToDateTime("12-05-2017"),
                    Filters = new List<ReportCriteriaItem>(){
                        new ReportCriteriaItem
                        {
                         Name = "CaseStatus",
                         Value = 0
                        },
                        new ReportCriteriaItem
                        {
                         Name = "AccomType",
                         Value = "EOS"
                        },
                        new ReportCriteriaItem
                        {
                         Name = "CaseAssignee",
                         Value = "000000000000000000000002"
                        },
                        new ReportCriteriaItem
                        {
                         Name = "Determination",
                         Value = 0
                        },
                        new ReportCriteriaItem
                        {
                         Name = "WorkState",
                         Value = "WI"
                        }
                    },
                },
                PageNumber = 1,
                PageSize = 100
            };
        }


        /// <summary>
        /// Executes the report and check if the report was successfully created
        /// </summary>
        /// <param name="args"></param>
        public void CanExecuteReportAsync(ExecuteReportArgs args)
        {
            var ReportResult = ReportingLogic.ExecuteReportAsync(args).GetAwaiter().GetResult();
            Assert.IsTrue(ReportResult.Success);
        }
        /// <summary>
        /// Get dummy user for test
        /// </summary>
        /// <returns></returns>
        private User GetCurrentUser()
        {
            return new User()
            {
                CustomerKey = "000000000000000000000002",
                Email = "dev@absencesoft.com",
                Key = "000000000000000000000001",
                UserName = "dev@absencesoft.com",
                UserStatus = UserStatus.Active
            };
        }
    }
}
