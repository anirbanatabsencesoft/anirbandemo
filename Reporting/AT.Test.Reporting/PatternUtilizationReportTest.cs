﻿using AT.Entities.Authentication;
using AT.Logic.Arguments;
using AT.Logic.Reporting;
using AT.Logic.Reporting.Reports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace AT.Test.Reporting
{
    /// <summary>
    /// The Pattern Utilization Reporting Test Class
    /// </summary>
    [TestClass]
    public class PatternUtilizationReportTest
    {
        /// <summary>
        /// Reporting Logic
        /// </summary>
        private readonly ReportingLogic ReportingLogic;

        /// <summary>
        /// Current user
        /// </summary>
        private readonly User CurrentUser;
        /// <summary>
        /// Constructor
        /// </summary>
        public PatternUtilizationReportTest()
        {
            CurrentUser = GetCurrentUser();
            ReportingLogic = new ReportingLogic(CurrentUser);
        }


        /// <summary>
        /// Test the Intermittent Pattern Utilization Report
        /// </summary>
        [TestMethod]
        public void IntermittentPatternUtilizationReportTest()
        {
            ExecuteReportArgs executeReportArgs = GetPatternUtilizationExecuteReportArg();
            CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Create dummy executeReport Argument to test
        /// </summary>
        /// <returns></returns>
        private ExecuteReportArgs GetPatternUtilizationExecuteReportArg()
        {
            return new ExecuteReportArgs
            {
                ReportId = "99e99082-130b-433b-a053-a8b425636b8d",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = Convert.ToDateTime("11-01-2017"),
                    EndDate = Convert.ToDateTime("12-05-2017"),
                    Filters = new List<ReportCriteriaItem>(){

                        new ReportCriteriaItem
                        {
                         Name = "AbsenceReason",
                         Value = "000000000000000001000000"
                        },

                        new ReportCriteriaItem
                        {
                         Name = "WorkState",
                         Value = "WI"
                        },

                        new ReportCriteriaItem
                        {
                         Name = "Pattern",
                         Value = "1"
                        }

                    },
                },
                PageNumber = 1,
                PageSize = 100
            };
        }


        /// <summary>
        /// Executes the report and check if the report was successfully created
        /// </summary>
        /// <param name="args"></param>
        public void CanExecuteReportAsync(ExecuteReportArgs args)
        {
            var ReportResult = ReportingLogic.ExecuteReportAsync(args).GetAwaiter().GetResult();
            Assert.IsTrue(ReportResult.Success);
        }
        /// <summary>
        /// Get dummy user for test
        /// </summary>
        /// <returns></returns>
        private User GetCurrentUser()
        {
            return new User()
            {
                CustomerKey = "000000000000000000000002",
                Email = "dev@absencesoft.com",
                Key = "000000000000000000000001",
                UserName = "dev@absencesoft.com",
                UserStatus = UserStatus.Active
            };
        }
    }
}
