﻿using AbsenceSoft;
using AT.Entities.Authentication;
using AT.Logic.Arguments;
using AT.Logic.Reporting;
using AT.Logic.Reporting.Reports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Test.Reporting
{
    /// <summary>
    /// Test Class for Custom Report
    /// </summary>
    [TestClass]
    public class CustomReportTest
    {

        /// <summary>
        /// Reporting Logic
        /// </summary>
        private readonly ReportingLogic ReportingLogic;

        /// <summary>
        /// Current user
        /// </summary>
        private readonly User CurrentUser;
        public CustomReportTest()
        {
            CurrentUser = GetCurrentUser();
            ReportingLogic = new ReportingLogic(CurrentUser);
        }
        /// <summary>
        /// Test the Accommodation status Report
        /// </summary>
        [TestMethod]
        public void EmployeeSurveyByEmailReportTest()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "B2F0176B-0566-4769-80B1-91BE1A9FB2D0",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = DateTime.Now.AddDays(-60),
                    EndDate = DateTime.Now
                }
            };
            CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Test the Custom Accommodations Operations Detailed Report detail
        /// </summary>
        [TestMethod]
        public void AccommodationOperationDetailedReportTest()
        {
            ExecuteReportArgs executeReportArgs = GetAccommodationExecuteReportArg();
            executeReportArgs.ReportId = "28b4c98f-c321-4a1f-b928-e19886a06a3c";
            CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Create dummy executeReport Argument to test
        /// </summary>
        /// <returns></returns>
        private ExecuteReportArgs GetAccommodationExecuteReportArg()
        {
            return new ExecuteReportArgs
            {
                ReportId = "EA3B77C2-374F-49ED-859F-54FDFE1066E4",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = Convert.ToDateTime("11-01-2017"),
                    EndDate = Convert.ToDateTime("12-05-2017"),
                    Filters = new List<ReportCriteriaItem>(){
                        new ReportCriteriaItem
                        {
                         Name = "CaseStatus",
                         Value = 0
                        },
                        new ReportCriteriaItem
                        {
                         Name = "AccomType",
                         Value = "EOS"
                        },
                        new ReportCriteriaItem
                        {
                         Name = "CaseAssignee",
                         Value = "000000000000000000000002"
                        },
                        new ReportCriteriaItem
                        {
                         Name = "Determination",
                         Value = 0
                        },
                        new ReportCriteriaItem
                        {
                         Name = "WorkState",
                         Value = "WI"
                        }
                    },
                },
                PageNumber = 1,
                PageSize = 100
            };
        }

        /// <summary>
        /// Test the Custom Accommodations Status Granted Report
        /// </summary>
        [TestMethod]
        public void AccommodationStatusGrantedReportTest()
        {
            
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "20BAA0BB-CB4C-4FF7-88A9-24B2AA85F2E8",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = DateTime.UtcNow.Date.AddDays(-30),
                    EndDate = DateTime.UtcNow.Date
                }
            };
            CanExecuteReportAsync(executeReportArgs);
        }

       
        /// <summary>
        /// Test the Accommodation interactive HR contact count Report
        /// </summary>
        [TestMethod]
        public void AccommodationInteractiveProgressReportTest()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "20BAA0BB-CB4C-4FF7-88A9-24B2AA85F1E7",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = DateTime.Now.AddDays(-60),
                    EndDate = DateTime.Now
                }
            };
            CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Test Employee on leaveReport
        /// </summary>
        [TestMethod]
        public void EmployeeOnLeaveReportTest()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "E8E5E616-557E-4064-BE44-20200F22D025",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = DateTime.Now.AddDays(-60),
                    EndDate = DateTime.Now
                }
            };
            CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Test the Accommodation interactive HR contact count Report
        /// </summary>
        [TestMethod]
        public void AccommodationHrContactCountReportTest()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "20BAA0BB-CB4C-4FF7-88A9-24B2AA85F1F7",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = DateTime.Now.AddDays(-60),
                    EndDate = DateTime.Now
                }
            };
            CanExecuteReportAsync(executeReportArgs);
        }


        /// <summary>
        /// User Activity Report Test
        /// </summary>
        [TestMethod]
        public void UserActivityReportTest()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "20BAA0CD-CB4C-4FF7-88A9-24B2AA85F1F7",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = DateTime.Now.AddDays(-60),
                    EndDate = DateTime.Now
                }
            };
            CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// User Activity detail report test
        /// </summary>
        [TestMethod]
        public void UserActivityDetailReportTest()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "20BAA0FF-CB4C-4FF7-88A9-24B2AA85F1F7",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = DateTime.Now.AddDays(-60),
                    EndDate = DateTime.Now
                }
            };
            CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// User Activity timestamp report test
        /// </summary>
        [TestMethod]
        public void UserActivityTimestampReportTest()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "20BAA0FF-CCCC-4FF7-88A9-24B2AA85F1F8",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = DateTime.Now.AddDays(-60),
                    EndDate = DateTime.Now
                }
            };
            CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Accommodation Custom field report test
        /// </summary>
        [TestMethod]
        public void AccommodationCustomFieldReportTest()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "20BAA0BB-CB4C-4FF7-88A9-24F2AA85F2E9",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = DateTime.UtcNow.Date.GetFirstDayOfMonth(),
                    EndDate = DateTime.UtcNow.Date
                }
            };
            CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Accommodation Custom Details report test
        /// </summary>
        [TestMethod]
        public void AccommodationCustomDetailReportTest()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "0A3B77C2-374F-49ED-859F-54FDFE1066E4",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = DateTime.UtcNow.Date.AddDays(-30),
                    EndDate = DateTime.UtcNow.Date
                }
            };
            CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Custom Daily case report test
        /// </summary>
        [TestMethod]
        public void CustomDailyCaseReportTest()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "06EE4676-EA21-41F6-9DCF-EDE65D8D3B3E",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = DateTime.UtcNow.Date.AddDays(-7),
                    EndDate = DateTime.UtcNow.Date
                }
            };
            CanExecuteReportAsync(executeReportArgs);
        }
        

        /// <summary>
        /// Executes the report and check if the report was successfully created
        /// </summary>
        /// <param name="args"></param>
        public void CanExecuteReportAsync(ExecuteReportArgs args)
        {
            args.Criteria.CustomerId = CurrentUser.CustomerKey;
            var ReportResult = ReportingLogic.ExecuteReportAsync(args).GetAwaiter().GetResult();
            Assert.IsTrue(ReportResult.Success);
        }
        /// <summary>
        /// Get dummy user for test
        /// </summary>
        /// <returns></returns>
        private User GetCurrentUser()
        {
            return new User()
            {
                CustomerKey = "546e5097a32aa00d60e3210a",
                Email = "qa.parrot@absencesoft.com",
                Key = "5714e50c315fd804c4c9dfa0",
                UserName = "qa.parrot@absencesoft.com",
                UserStatus = UserStatus.Active
            };
        }
        
    }
}
