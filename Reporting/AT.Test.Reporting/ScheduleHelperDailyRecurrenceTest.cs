﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AT.Logic.Reporting.Schedule;
using FluentAssertions;
using Newtonsoft.Json;

namespace AT.Test.Reporting
{
    [TestClass]
    public class ScheduleHelperDailyRecurrenceTest
    {
        [TestMethod]
        public void TestGetNextDate_NoEndDate_OneTime()
        {
            var currentDate = new DateTime(2018, 03, 08, 06, 00, 00);
            ScheduleInfo schInfo = new ScheduleInfo()
            {
                StartDate = currentDate,
            };

            ScheduleHelper helper = new ScheduleHelper(schInfo);
            var nextDate=helper.GetNextDate(DateTime.Now);
            nextDate.Should().Be(currentDate);
        }
        [TestMethod]
        public void TestGetNextDate_NoEndDate_Daily_EveryWeekDay_FirstDate()
        {
            var currentDate = new DateTime(2018, 03, 08, 06, 00, 00);
            ScheduleInfo schInfo = new ScheduleInfo()
            {
                StartDate = currentDate,
                RecurrenceType=Logic.Reporting.Enums.RecurrenceType.Daily
            };           
            ScheduleHelper helper = new ScheduleHelper(schInfo);
            var nextDate = helper.GetFirstRunDate();            
            nextDate.Should().Be(currentDate);
        }

        [TestMethod]
        public void TestGetNextDate_NoEndDate_Daily_EveryWeekDay_FirstDate_WeekendDay()
        {
            var currentDate = new DateTime(2018, 03, 10, 06, 00, 00);
            ScheduleInfo schInfo = new ScheduleInfo()
            {
                StartDate = currentDate,
                RecurrenceType = Logic.Reporting.Enums.RecurrenceType.Daily
            };
            ScheduleHelper helper = new ScheduleHelper(schInfo);
            var nextDate = helper.GetFirstRunDate();
            nextDate.Should().Be(new DateTime(2018, 03, 12, 06, 00, 00));//Monday, weekends are skipped
        }

        [TestMethod]
        public void TestGetNextDate_NoEndDate_Daily_EveryWeekDay_NextDate()
        {
            var currentDate = new DateTime(2018, 03, 08, 06, 00, 00);
            ScheduleInfo schInfo = new ScheduleInfo()
            {
                StartDate = currentDate,
                RecurrenceType = Logic.Reporting.Enums.RecurrenceType.Daily
            };
            ScheduleHelper helper = new ScheduleHelper(schInfo);
            var nextDate = helper.GetNextDate(currentDate);
            nextDate.Should().Be(currentDate.AddDays(1));
        }

        [TestMethod]
        public void TestGetNextDate_NoEndDate_Daily_EveryXDays_FirstDate()
        {
            var currentDate = new DateTime(2018, 03, 07, 06, 00, 00);
            ScheduleInfo schInfo = new ScheduleInfo()
            {
                StartDate = currentDate,
                RecurrenceType = Logic.Reporting.Enums.RecurrenceType.Daily,
                DailyRecurType = Logic.Reporting.Enums.DailyRecurType.OnEveryXDays,
                DailyRecurEveryXDays = 2
            };
            ScheduleHelper helper = new ScheduleHelper(schInfo);
            var nextDate = helper.GetFirstRunDate();
            nextDate.Should().Be(currentDate);
        }

        [TestMethod]
        public void TestGetNextDate_NoEndDate_Daily_EveryXDays_NextDate()
        {
            var currentDate = new DateTime(2018, 03, 07, 06, 00, 00);
            ScheduleInfo schInfo = new ScheduleInfo()
            {
                StartDate = currentDate,
                RecurrenceType = Logic.Reporting.Enums.RecurrenceType.Daily,
                DailyRecurType = Logic.Reporting.Enums.DailyRecurType.OnEveryXDays,
                DailyRecurEveryXDays = 2
            };
            ScheduleHelper helper = new ScheduleHelper(schInfo);
            var nextDate = helper.GetNextDate(currentDate);
            nextDate.Should().Be(currentDate.AddDays(2));
        }
    }
}
