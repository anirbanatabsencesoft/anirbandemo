﻿using AT.Entities.Authentication;
using AT.Logic.Arguments;
using AT.Logic.Reporting;
using AT.Logic.Reporting.Reports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace AT.Test.Reporting
{
    /// <summary>
    /// The Absence Status Reporting Test Class
    /// </summary>
    [TestClass]
    public class AbsenceStatusReportTest
    {
        /// <summary>
        /// Reporting Logic
        /// </summary>
        private readonly ReportingLogic ReportingLogic;

        /// <summary>
        /// Current user
        /// </summary>
        private readonly User CurrentUser;
        public AbsenceStatusReportTest()
        {
            CurrentUser = GetCurrentUser();
            ReportingLogic = new ReportingLogic(CurrentUser);
        }
        /// <summary>
        /// Test the AbsenceStatusReport Report
        /// </summary>
        [TestMethod]
        public void AbsenceStatusBynoGroupReportTest()
        {
            ExecuteReportArgs executeReportArgs = GetAbsenceStatusExecuteReportArg();           
            CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Test the AbsenceStatusReport group by leave type Report
        /// </summary>
        [TestMethod]
        public void AbsenceStatusByLeaveTypeReportTest()
        {
            ExecuteReportArgs executeReportArgs = GetAbsenceStatusExecuteReportArg();
            executeReportArgs.GroupType = Entities.Reporting.Enums.ReportGroupType.Leave;
            CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Test the AbsenceStatusReport group by OfficeLoacation Report
        /// </summary>
        [TestMethod]
        public void AbsenceStatusByOfficeLoacationReportTest()
        {
            ExecuteReportArgs executeReportArgs = GetAbsenceStatusExecuteReportArg();
            executeReportArgs.GroupType = Entities.Reporting.Enums.ReportGroupType.Location;
            CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Test the AbsenceStatusReport group by Organization Report
        /// </summary>
        [TestMethod]
        public void AbsenceStatusByOrganizationReportTest()
        {
            ExecuteReportArgs executeReportArgs = GetAbsenceStatusExecuteReportArg();
            executeReportArgs.GroupType = Entities.Reporting.Enums.ReportGroupType.Organization;
            CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Test the AbsenceStatusReport group by Policy Report
        /// </summary>
        [TestMethod]
        public void AbsenceStatusByPolicyReportTest()
        {
            ExecuteReportArgs executeReportArgs = GetAbsenceStatusExecuteReportArg();
            executeReportArgs.GroupType = Entities.Reporting.Enums.ReportGroupType.Policy;
            CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Test the AbsenceStatusReport group by Reason Report
        /// </summary>
        [TestMethod]
        public void AbsenceStatusByReasonReportTest()
        {
            ExecuteReportArgs executeReportArgs = GetAbsenceStatusExecuteReportArg();
            executeReportArgs.GroupType = Entities.Reporting.Enums.ReportGroupType.Reason;
            CanExecuteReportAsync(executeReportArgs);
        }


        /// <summary>
        /// Test the AbsenceStatusReport group by Work state Report
        /// </summary>
        [TestMethod]
        public void AbsenceStatusByWorkStateReportTest()
        {
            ExecuteReportArgs executeReportArgs = GetAbsenceStatusExecuteReportArg();
            executeReportArgs.GroupType = Entities.Reporting.Enums.ReportGroupType.WorkState;
            CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Create dummy executeReport Argument to test
        /// </summary>
        /// <returns></returns>
        private ExecuteReportArgs GetAbsenceStatusExecuteReportArg()
        {
            return new ExecuteReportArgs
            {
                ReportId = "955E6AC2-4C2C-4cb5-A6B7-DF4CCD3BC252",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = Convert.ToDateTime("11-01-2017"),
                    EndDate = Convert.ToDateTime("12-05-2017"),
                    Filters = new List<ReportCriteriaItem>(){
                        new ReportCriteriaItem
                        {
                         Name = "CaseStatus",
                         Value = 0
                        },
                        new ReportCriteriaItem
                        {
                         Name = "AbsenceReason",
                         Value = "000000000000000001000000"
                        },
                        new ReportCriteriaItem
                        {
                         Name = "CaseType",
                         Value = 1
                        },
                        new ReportCriteriaItem
                        {
                         Name = "Determination",
                         Value = 2
                        },
                        new ReportCriteriaItem
                        {
                         Name = "WorkState",
                         Value = "WI"
                        }
                    },
                },
                PageNumber = 1,
                PageSize = 100
            };
        }


        /// <summary>
        /// Executes the report and check if the report was successfully created
        /// </summary>
        /// <param name="args"></param>
        public void CanExecuteReportAsync(ExecuteReportArgs args)
        {
            var ReportResult = ReportingLogic.ExecuteReportAsync(args).GetAwaiter().GetResult();
            Assert.IsTrue(ReportResult.Success);
        }
        /// <summary>
        /// Get dummy user for test
        /// </summary>
        /// <returns></returns>
        private User GetCurrentUser()
        {
            return new User()
            {
                CustomerKey = "000000000000000000000002",
                Email = "dev@absencesoft.com",
                Key = "000000000000000000000001",
                UserName = "dev@absencesoft.com",
                UserStatus = UserStatus.Active
            };
        }
    }
}
