﻿using AT.Common.Core;
using AT.Entities.Reporting.Classes;
using AT.Entities.Reporting.Enums;
using AT.Logic.Reporting.Arguments;
using AT.Logic.Reporting.Base;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AT.Test.Reporting
{
    [TestClass]
    public class ReportDeliveryTest
    {
        private ReportDeliveryArgs EmailReportDeliveryArgs { get; set; }

        public BaseReportDeliveryProvider BaseReportDeliveryProviderRepository;

        public string ReportBody { get; set; }

        public ReportDeliveryTest()
        {
            Mock<BaseReportDeliveryProvider> mockEmailReportDeliveryRepository = new Mock<BaseReportDeliveryProvider>();

            mockEmailReportDeliveryRepository.Setup(mer => mer.Send()).Returns(
                               () =>
                               {
                                   if (!ReportBody.Contains("<REPORTNAME>") && !ReportBody.Contains("<REQUESTEDDATE>") && !ReportBody.Contains("<SITEURL>"))
                                   {
                                       return Task.FromResult(new ResultSet<bool>
                                       {
                                           Success = true
                                       });
                                   }
                                   else
                                   {
                                       return Task.FromResult(new ResultSet<bool>
                                       {
                                           Success = false
                                       });
                                   }
                               }
                               );
            BaseReportDeliveryProviderRepository = mockEmailReportDeliveryRepository.Object;
        }

        [TestMethod]
        public void AbleToDeliverReport()
        {
            EmailReportDeliveryArgs = new ReportDeliveryArgs()
            {
                Message = "Test Message",
                Recipients = "rshankar@absencesoft.com",
                ReplaceKeyValue = new DeliveryParameters
                        {
                            ReportName = "Demo Report",
                            RequestedDate = "02/19/2018",
                            SiteUrl = "www.absencesoft.com" 
                        },
                Subject = "Demo Test Mail",
                Sender = "test@absencesoft.com",
                Template = "ReportGenerated"
            };
            BaseReportDeliveryProviderRepository.Parameters = EmailReportDeliveryArgs;
            ReportDeliveryTemplateType.TryParse(EmailReportDeliveryArgs.Template, out ReportDeliveryTemplateType templateType);

            ReportBody = BaseReportDeliveryProviderRepository.GetEmailReportBody(templateType);
            var result = BaseReportDeliveryProviderRepository.Send();
            Assert.IsTrue(result.Result.Success);
        }
    }
}