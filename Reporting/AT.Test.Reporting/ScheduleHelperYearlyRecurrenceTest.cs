﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AT.Logic.Reporting.Schedule;
using FluentAssertions;
using Newtonsoft.Json;

namespace AT.Test.Reporting
{
    [TestClass]
    public class ScheduleHelperYearlyRecurrenceTest
    {
        [TestMethod]
        public void TestGetNextDate_NoEndDate_OnSpecificDayOfTheMonth_OnCurrentDate()
        {
            var currentDate = new DateTime(2018, 03, 09, 06, 00, 00);
            ScheduleInfo schInfo = new ScheduleInfo()
            {
                StartDate = currentDate,
                RecurrenceType=Logic.Reporting.Enums.RecurrenceType.Yearly ,                             
                YearlyRecurType=Logic.Reporting.Enums.YearlyRecurType.OnSpecificDayOfYear,
                SpecificDateDayValue=9,
                SpecificDateMonthValue=3
            };

            ScheduleHelper helper = new ScheduleHelper(schInfo);
            var nextDate=helper.GetNextDate(currentDate);
            nextDate.Should().Be(new DateTime(2018, 03, 09, 06, 00, 00));

        }


        [TestMethod]
        public void TestGetNextDate_NoEndDate_OnSpecificDayOfTheMonth_OnPastDate()
        {
            var currentDate = new DateTime(2018, 03, 10, 06, 00, 00);
            ScheduleInfo schInfo = new ScheduleInfo()
            {
                StartDate = currentDate,
                RecurrenceType = Logic.Reporting.Enums.RecurrenceType.Yearly,
                YearlyRecurType = Logic.Reporting.Enums.YearlyRecurType.OnSpecificDayOfYear,
                SpecificDateDayValue = 9,
                SpecificDateMonthValue = 3
            };

            ScheduleHelper helper = new ScheduleHelper(schInfo);
            var nextDate = helper.GetNextDate(currentDate);
            nextDate.Should().Be(new DateTime(2019, 03, 09, 06, 00, 00));
        }

        [TestMethod]
        public void TestGetNextDate_NoEndDate_OnCustomDateFormat()
        {
            var currentDate = new DateTime(2018, 03, 10, 06, 00, 00);
            ScheduleInfo schInfo = new ScheduleInfo()
            {
                StartDate = currentDate,
                RecurrenceType = Logic.Reporting.Enums.RecurrenceType.Yearly,
                YearlyRecurType = Logic.Reporting.Enums.YearlyRecurType.OnCustomDateFormat,
                YearlySpecificDatePartOne=Logic.Reporting.Enums.MonthlySpecificDay.Last,
                YearlySpecificDatePartTwo=Logic.Reporting.Enums.MonthlySpecificWeekday.Friday,
                YearlySpecificDatePartThree=Logic.Reporting.Enums.YearlySpecificMonth.March
            };

            ScheduleHelper helper = new ScheduleHelper(schInfo);
            var nextDate = helper.GetNextDate(currentDate);
            nextDate.Should().Be(new DateTime(2018, 03, 30, 06, 00, 00));
        }

    }
}
