﻿using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using AT.Logic.Arguments;
using AT.Logic.Reporting;
using AT.Logic.Reporting.Reports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AT.Test.Reporting
{
    [TestClass]
    public class WorkRelatedReportTest
    {
        /// <summary>
        /// Reporting Logic
        /// </summary>
        private readonly ReportingLogic ReportingLogic;

        /// <summary>
        /// Current user
        /// </summary>
        private readonly User CurrentUser;

        /// <summary>
        /// Report test constructor
        /// </summary>
        public WorkRelatedReportTest()
        {
            CurrentUser = GetCurrentUser();
            ReportingLogic = new ReportingLogic(CurrentUser);
        }

        [TestMethod]
        public async Task WorkRelatedInjuryDetailReport()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs()
            {
                ReportId = "688BDE87-48AD-4560-84A7-982D5DE8D74A",
                Criteria = new ReportCriteria
                {
                    StartDate = Convert.ToDateTime("05-12-2016"),
                    EndDate = Convert.ToDateTime("11-02-2018"),
                    Filters = new List<ReportCriteriaItem>(){
                      new ReportCriteriaItem{
                        Name = "WorkRelatedCaseClassification",
                        Values = new List<object>(){ "3" }
                      },
                      new ReportCriteriaItem{
                          Name = "WorkRelatedTypeOfInjury",
                          Values = new List<object>() { "1" }
                      }
                    }
                },
                PageNumber = 1,
                PageSize = 10,
                GroupType = ReportGroupType.InjuryDetails
            };
            await CanExecuteReportAsync(executeReportArgs);
        }

        [TestMethod]
        public async Task WorkRelatedNeedleStickReport()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs()
            {
                ReportId = "688BDE87-48AD-4560-84A7-982D5DE8D74A",
                Criteria = new ReportCriteria
                {
                    StartDate = Convert.ToDateTime("05-12-2016"),
                    EndDate = Convert.ToDateTime("10-12-2018"),
                    Filters = new List<ReportCriteriaItem>(){
                      new ReportCriteriaItem{
                        Name = "WorkRelatedSharpsInjuryLocation",
                        Values = {"2", "3"}
                      },
                      new ReportCriteriaItem{
                          Name = "WorkRelatedSharpsLocation",
                          Values = {"1", "3"}
                      },
                      new ReportCriteriaItem{
                          Name =  "WorkRelatedSharpsProcedure",
                          Values = {"2", "6"}
                      },
                      new ReportCriteriaItem{
                          Name =  "WorkRelatedSharpsJobClassification",
                          Values = {"1", "7"}
                      }
                    }
                },
                PageNumber = 1,
                PageSize = 10,
                GroupType = ReportGroupType.NeedleSticks
            };
            await CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Executes the report and check if the report was successfully created
        /// </summary>
        /// <param name="args"></param>
        public async Task CanExecuteReportAsync(ExecuteReportArgs args)
        {
            var ReportResult = await ReportingLogic.ExecuteReportAsync(args);
            Assert.IsTrue(ReportResult.Success);
        }

        /// <summary>
        /// Get dummy user for test
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        private User GetCurrentUser()
        {
            return new User()
            {
                Id = 0,
                CustomerKey = "000000000000000000000002",
                Email = "dev@absencesoft.com",
                Key = "000000000000000000000001",
                UserName = "dev@absencesoft.com",
                UserStatus = UserStatus.Active
            };
        }

    }
}
