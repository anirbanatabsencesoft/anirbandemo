﻿using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using AT.Logic.Arguments;
using AT.Logic.Reporting;
using AT.Logic.Reporting.Reports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Test.Reporting
{
    [TestClass]
    public class DisabilityReportTest
    {
        /// <summary>
        /// Reporting Logic
        /// </summary>
        private readonly ReportingLogic ReportingLogic;
        
        /// <summary>
        /// Current user
        /// </summary>
        private readonly User CurrentUser;

        /// <summary>
        /// Report test constructor
        /// </summary>
        public DisabilityReportTest()
        {
            CurrentUser = GetCurrentUser();
            ReportingLogic = new ReportingLogic(CurrentUser);
        }

        /// <summary>
        /// Test Disability Report
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task DisabilityReport()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "D6DC483F-FC23-4244-8430-89E49ABE2578",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = Convert.ToDateTime("05-12-2017"),
                    EndDate = Convert.ToDateTime("10-12-2017"),
                    Filters = new List<ReportCriteriaItem>(){
                        new ReportCriteriaItem
                        {
                            Name = "DiagnosisCode",
                            Value = "TestData"
                        },
                        new ReportCriteriaItem
                        {
                        Name =  "WorkState",
                        Value= ""
                        },
                        new ReportCriteriaItem
                        {
                            Name = "OFFICELOCATION",
                            Value = null
                        }
                    }
                },
                PageNumber = 2,
                PageSize = 10,
                GroupType = ReportGroupType.Condition
            };
            await CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Test Disability Report By Condition
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task DisabilityReportByCondition()
        {
            await DisabilityReport();
        }

        /// <summary>
        /// Test Disability Report By Organization
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task DisabilityReportByOrganization()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "D6DC483F-FC23-4244-8430-89E49ABE2578",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = Convert.ToDateTime("05-12-2017"),
                    EndDate = Convert.ToDateTime("10-12-2017"),
                    Filters = new List<ReportCriteriaItem>(){
                        new ReportCriteriaItem
                        {
                            Name = "DiagnosisCode",
                            Value= "TestData"
                        },
                        new ReportCriteriaItem
                        {
                            Name =  "WorkState",
                            Value= ""
                        },
                        new ReportCriteriaItem
                        {
                            Name = "OFFICELOCATION",
                            Value= null
                        },
                        new ReportCriteriaItem
                        {
                            Name = "OrganizationTypeGrouping",
                            Value= null
                        }
                    }
                },
                PageNumber = 1,
                PageSize = 100,
                GroupType = ReportGroupType.ConditionByOrganization
            };
            await CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Test Disability Report Duration by Condition
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task DisabilityReportDurationByCondition()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "D6DC483F-FC23-4244-8430-89E49ABE2578",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = Convert.ToDateTime("05-12-2017"),
                    EndDate = Convert.ToDateTime("10-12-2017"),
                    Filters = new List<ReportCriteriaItem>(){
                        new ReportCriteriaItem
                        {
                            Name = "DiagnosisCode",
                            Value = ""
                        },
                        new ReportCriteriaItem
                        {
                            Name = "ExceedsDurationBy",
                            Value = ""
                        },
                        new ReportCriteriaItem
                        {
                        Name =  "WorkState",
                        Value= ""
                        },
                        new ReportCriteriaItem
                        {
                            Name = "OFFICELOCATION",
                            Value = null
                        }
                    }
                },
                PageNumber = 1,
                PageSize = 100,
                GroupType = ReportGroupType.ConditionByCondition
            };
            await CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Test Disability Report Duration by Organization
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task DisabilityReportDurationByOrganization()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "D6DC483F-FC23-4244-8430-89E49ABE2578",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = Convert.ToDateTime("05-12-2017"),
                    EndDate = Convert.ToDateTime("10-12-2017"),
                    Filters = new List<ReportCriteriaItem>(){
                        new ReportCriteriaItem
                        {
                            Name = "DiagnosisCode",
                            Value = ""
                        },
                        new ReportCriteriaItem
                        {
                            Name = "ExceedsDurationBy",
                            Value = ""
                        },
                        new ReportCriteriaItem
                        {
                        Name =  "WorkState",
                        Value= ""
                        },
                        new ReportCriteriaItem
                        {
                            Name = "OFFICELOCATION",
                            Value = null
                        },
                        new ReportCriteriaItem
                        {
                            Name = "OrganizationTypeGrouping",
                            Value= ""
                        }
                    }
                },
                PageNumber = 1,
                PageSize = 100,
                GroupType = ReportGroupType.DurationByOrganization
            };
            await CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Executes the report and check if the report was successfully created
        /// </summary>
        /// <param name="args"></param>
        public async Task CanExecuteReportAsync(ExecuteReportArgs args)
        {
            var ReportResult = await ReportingLogic.ExecuteReportAsync(args);
            Assert.IsTrue(ReportResult.Success);
        }

        [TestMethod]
        /// <summary>
        /// Get dummy user for test
        /// </summary>
        /// <returns></returns>
        private User GetCurrentUser()
        {
            return new User()
            {
                Id = 0,
                CustomerKey = "000000000000000000000002",
                Email = "dev@absencesoft.com",
                Key = "000000000000000000000001",
                UserName = "dev@absencesoft.com",
                UserStatus = UserStatus.Active
            };
        }

    }
}
