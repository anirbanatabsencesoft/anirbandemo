﻿using AT.Entities.Authentication;
using AT.Logic.Arguments;
using AT.Logic.Reporting;
using AT.Logic.Reporting.Reports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Test.Reporting
{
    /// <summary>
    /// The Communication Reporting Test Class
    /// </summary>
    [TestClass]
    public class CommunicationReportTest
    {

        /// <summary>
        /// Reporting Logic
        /// </summary>
        private readonly ReportingLogic ReportingLogic;

        /// <summary>
        /// Current user
        /// </summary>
        private readonly User CurrentUser;
        /// <summary>
        /// constructor
        /// </summary>
        public CommunicationReportTest()
        {
            CurrentUser = GetCurrentUser();
            ReportingLogic = new ReportingLogic(CurrentUser);
        }


        /// <summary>
        /// Test the communication details Report
        /// </summary>
        [TestMethod]
        [Ignore]
        public void CommunicationDetailsReportTest()
        {
            ExecuteReportArgs executeReportArgs = GetCommunicationReportArg();
            CanExecuteReportAsync(executeReportArgs);
        }


        /// <summary>
        /// Create dummy executeReport Argument to test
        /// </summary>
        /// <returns></returns>
        private ExecuteReportArgs GetCommunicationReportArg()
        {
            return new ExecuteReportArgs
            {
                ReportId = "AEF89837-7575-4495-B07C-2060FDFAD853",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = Convert.ToDateTime("11-01-2017"),
                    EndDate = Convert.ToDateTime("12-05-2017"),
                    Filters = new List<ReportCriteriaItem>(){
                        new ReportCriteriaItem
                        {
                         Name = "CommType",
                         Value = 1
                        },
                        new ReportCriteriaItem
                        {
                         Name = "CaseStatus",
                         Value = 0
                        },
                        new ReportCriteriaItem
                        {
                         Name = "CaseType",
                         Value = 1
                        },
                        new ReportCriteriaItem
                        {
                         Name = "Determination",
                         Value = 2
                        },
                        new ReportCriteriaItem
                        {
                         Name = "WorkState",
                         Value = "WI"
                        }
                    },
                },
                PageNumber = 1,
                PageSize = 100
            };
        }


        /// <summary>
        /// Executes the report and check if the report was successfully created
        /// </summary>
        /// <param name="args"></param>
        public void CanExecuteReportAsync(ExecuteReportArgs args)
        {
            var ReportResult = ReportingLogic.ExecuteReportAsync(args).GetAwaiter().GetResult();
            Assert.IsTrue(ReportResult.Success);
        }
        /// <summary>
        /// Get dummy user for test
        /// </summary>
        /// <returns></returns>
        private User GetCurrentUser()
        {
            return new User()
            {
                CustomerKey = "000000000000000000000002",
                Email = "dev@absencesoft.com",
                Key = "000000000000000000000001",
                UserName = "dev@absencesoft.com",
                UserStatus = UserStatus.Active
            };
        }
    }
}
