﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Reporting;
using AT.Data.Reporting.Arguments;
using AT.Entities.Authentication;
using AT.Entities.Reporting.Classes;
using AT.Entities.Reporting.Entities;
using AT.Entities.Reporting.Enums;
using AT.Entities.Reporting.Interfaces;
using AT.Entities.Reporting.ReportingConstants;
using AT.Logic.Arguments;
using AT.Logic.Reporting;
using AT.Logic.Reporting.Arguments;
using AT.Logic.Reporting.Reports;
using AT.Provider.Cache.Base;
using AT.Provider.Reporting.ReportBuilders;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AT.Test.Reporting
{
    /// <summary>
    /// The Reporting Test Class
    /// </summary>
    [TestClass]
    public class ReportingTest
    {
        /// <summary>
        /// Report column definition.
        /// </summary>
        private readonly SaveReportArgs SaveReportArgs;

        /// <summary>
        /// Current user
        /// </summary>
        private readonly User CurrentUser;

        /// <summary>
        /// Reporting Logic
        /// </summary>
        private readonly ReportingLogic ReportingLogic;

        /// <summary>
        /// Report test constructor
        /// </summary>
        public ReportingTest()
        {
            SaveReportArgs = GetSaveReportArgs();
            CurrentUser = GetCurrentUser();
            ReportingLogic = new ReportingLogic(CurrentUser);
            MainCache = new Dictionary<string, dynamic>();
            AddReportTypeToCache();
            AddAdHocReportToCache();

            // Mock the Cache Repository using Moq
            Mock<BaseCacheProvider> mockCacheRepository = new Mock<BaseCacheProvider>();

            mockCacheRepository.Setup(mr => mr.Get<dynamic>(
                                      It.IsAny<string>())).Returns(
                                            (string key) =>
                                            {
                                                if (string.IsNullOrEmpty(key))
                                                {
                                                    return MainCache;
                                                }
                                                else
                                                {
                                                    MainCache.TryGetValue(key, out dynamic resultValue);
                                                    return resultValue;
                                                }
                                            }
                                        );

            mockCacheRepository.Setup(mr => mr.Delete(
                                     It.IsAny<string>())).Returns(
                                           (string key) =>
                                           {
                                               if (string.IsNullOrEmpty(key))
                                               {
                                                   return false;
                                               }
                                               else
                                               {
                                                   return MainCache.Remove(key);
                                               }
                                           }
                                       );

            // Allows us to test saving a Cache
            mockCacheRepository.Setup(mr => mr.Set(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<TimeSpan>())).Returns(
                  (string key, dynamic value, TimeSpan expiry) =>
                  {
                      if (!string.IsNullOrEmpty(key) && value != null)
                      {
                          MainCache.Add(key, value);
                          return true;
                      }
                      else
                      {
                          return false;
                      }
                  });

            // Complete the setup of our Mock Cache Repository
            MockCacheRepository = mockCacheRepository.Object;

            Mock<ReportingLogic> mockReportingRepository = new Mock<ReportingLogic>();

            mockReportingRepository.Setup(mr => mr.GetReportTypes(true)).Returns(
                                            (List<ReportType> reportTypes) =>
                                            {
                                                return MockCacheRepository.Get< List<ReportType>>(CacheConstants.REPORTING_ALL_REPORT_TYPE);
                                            }
                                         );

            mockReportingRepository.Setup(mr => mr.GetReportTypeById(It.IsAny<string>())).Returns(
                                            (string reportTypeId) =>
                                            {
                                                Dictionary<string, ReportType> CachedReportType = MockCacheRepository.Get<Dictionary<string, ReportType>>(CacheConstants.REPORTING_ALL_REPORT_TYPE);
                                                var report = CachedReportType.FirstOrDefault(p => p.Key == reportTypeId);
                                                return report.Value;
                                            }
                                        );
            MockReportingLogic = mockReportingRepository.Object;
        }

        /// <summary>
        /// Our Mock Cache Repository for use in testing
        /// </summary>
        public BaseCacheProvider MockCacheRepository;

        /// <summary>
        /// Our Mock Report Repository for use in testing
        /// </summary>
        public ReportingLogic MockReportingLogic;

        /// <summary>
        /// Cache dictionary to be used by Mock cache
        /// </summary>
        Dictionary<string, dynamic> MainCache;

        /// <summary>
        /// Test to check if the system can load the case messages resource
        /// </summary>
        [TestMethod]
        public async Task TestCanLoadReportsList()
        {
            var result = await ReportingLogic.SaveReportDefinitionAsync(SaveReportArgs);
            ReportBuilder reportBuilder = ReportBuilder.GetReportBuilder(ReportBuilderType.AdHoc, CurrentUser);

            var allReports = await reportBuilder.GetReports();

            Assert.IsTrue(allReports.Count > 0);
        }

        /// <summary>
        /// Test to check if the system can load the case messages resource
        /// </summary>
        [TestMethod]
        public async Task TestCanNotLoadReportsList()
        {
            var result = await ReportingLogic.SaveReportDefinitionAsync(SaveReportArgs);
            ReportBuilder reportBuilder = ReportBuilder.GetReportBuilder(ReportBuilderType.AdHoc, CurrentUser);

            var allReports = await reportBuilder.GetReports();

            Assert.IsFalse(allReports.Count == 0);
        }

        /// <summary>
        /// Test to check if the system can load the case messages resource
        /// </summary>
        [TestMethod]
        public async Task AdHocReportBuilderLoadsReportsExecutesFine()
        {
            var result = await ReportingLogic.SaveReportDefinitionAsync(SaveReportArgs);
            ReportBuilder reportBuilder = ReportBuilder.GetReportBuilder(ReportBuilderType.AdHoc, CurrentUser);

            var reports = await reportBuilder.GetReports();

            Assert.IsTrue(reports.Count > 0);
        }

        /// <summary>
        /// Test to check if the system can load the case messages resource
        /// </summary>
        [TestMethod]
        public async Task AdHocReportBuilderLoadsReportsFails()
        {
            var result = await ReportingLogic.SaveReportDefinitionAsync(SaveReportArgs);
            ReportBuilder reportBuilder = ReportBuilder.GetReportBuilder(ReportBuilderType.AdHoc, CurrentUser);

            var reports = await reportBuilder.GetReports();

            Assert.IsFalse(reports.Count == 0);
        }

        /// <summary>
        /// Test to check if the system can load the case messages resource
        /// </summary>
        [TestMethod]
        public async Task AdHocReportBuilderLoadsReportsByNameExecutesFine()
        {
            var result = await ReportingLogic.SaveReportDefinitionAsync(SaveReportArgs);
            ReportBuilder reportBuilder = ReportBuilder.GetReportBuilder(ReportBuilderType.AdHoc, CurrentUser);

            var reports = await reportBuilder.GetReportByName("test Report");

            Assert.AreEqual(reports.Name, "test Report");
        }

        /// <summary>
        /// Test to check if we can report types from Cache if ReportTypes does not exists in Cache, add to ReportTypes to cache and test again.
        /// </summary>
        [TestMethod]
        public void CanGetReportTypes()
        {
            Dictionary<string, ReportType> CachedReportType = MockCacheRepository.Get<dynamic>(CacheConstants.REPORTING_ALL_REPORT_TYPE);
            if (CachedReportType != null)
            {
                Assert.IsTrue(CachedReportType.Count > 0);
            }
            else
            {
                AddReportTypeToCache();
                CanGetReportTypes();
            }
        }

        /// <summary>
        /// Test to check if we can ReportType by Id
        /// </summary>
        [TestMethod]
        public void CanGetReportTypeById()
        {
            string ReportTypeId = "Report 1001";
            Dictionary<string, ReportType> CachedReportType = MockCacheRepository.Get<dynamic>(CacheConstants.REPORTING_ALL_REPORT_TYPE);
            Assert.IsTrue(CachedReportType != null);
            Assert.IsTrue(CachedReportType.Count > 0);

            ReportType reportType = CachedReportType.Where(p => p.Value.Name == ReportTypeId).FirstOrDefault().Value;
            Assert.IsTrue(reportType != null);
            Assert.IsTrue(reportType.Description == "Report 1001");
        }

        /// <summary>
        /// Test to check if we can Report by Id
        /// </summary>
        [TestMethod]
        public void CanGetReportById()
        {
            string ReportId = "2002", UserId = "4587";
            IList<IReport> reports = GetAdHocReports();
            Assert.IsTrue(reports.Count > 0);

            var returnedData = reports.FirstOrDefault(p => p.Id == ReportId);
            Assert.IsTrue(returnedData != null);
            Assert.IsTrue(returnedData.UserId == UserId);
        }

        /// <summary>
        /// Test to check if we can get SavedReports
        /// </summary>
        [TestMethod]
        public void GetSavedReportDefinitions()
        {
            IList<IReport> reports = GetAdHocReports();
            Assert.IsTrue(reports.Count > 0);

            var returnedData = reports.Where(p => p.UserId == "1245").ToList();
            Assert.IsTrue(returnedData != null);
            Assert.IsTrue(returnedData.Count == 3);
        }

       
        /// <summary>
        /// Gets the AdHocReports from MockCacheRepository
        /// </summary>
        /// <returns></returns>
        private IList<IReport> GetAdHocReports()
        {
            Dictionary<string, IReport> AdHocReports = MockCacheRepository.Get<dynamic>(CacheConstants.REPORTING_ADHOC_ALL_REPORTS);
            return AdHocReports.Select(p => p.Value).ToList();
        }

        /// <summary>
        /// Adds mock AdHocReport to Cache
        /// </summary>
        private void AddAdHocReportToCache()
        {
            Dictionary<string, IReport> AdhocReportCache = new Dictionary<string, IReport>
            {
                { "Key2001", new Report(){ Id = "2001", Name = "Adhoc Report 2001", UserId = "1245" } },
                { "Key2002", new Report(){ Id = "2002", Name = "Adhoc Report 2002", UserId = "4587" } },
                { "Key2003", new Report(){ Id = "2003", Name = "Adhoc Report 2003", UserId = "1245" } },
                { "Key2004", new Report(){ Id = "2004", Name = "Adhoc Report 2004", UserId = "8925" } },
                { "Key2005", new Report(){ Id = "2005", Name = "Adhoc Report 2005", UserId = "1245" } }
            };
            MainCache.Add(CacheConstants.REPORTING_ADHOC_ALL_REPORTS, AdhocReportCache);
        }

        /// <summary>
        /// Adds mock ReportType to Cache
        /// </summary>
        private void AddReportTypeToCache()
        {
            Dictionary<string, ReportType> ReportTypeCache = new Dictionary<string, ReportType>
            {
                { "Key1001", new ReportType() { Name="Report 1001", Description = "Report 1001" } },
                { "Key1002", new ReportType() { Name="Report 1002", Description = "Report 1002" } },
                { "Key1003", new ReportType() { Name="Report 1003", Description = "Report 1003" } },
                { "Key1004", new ReportType() { Name="Report 1004", Description = "Report 1004" } },
                { "Key1005", new ReportType() { Name="Report 1005", Description = "Report 1005" } }
            };
            MainCache.Add(CacheConstants.REPORTING_ALL_REPORT_TYPE, ReportTypeCache);
        }

        /// <summary>
        /// Test to check if data is going to database and saved
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SaveAdhocReportDefinitionTest()
        {
            var result = await ReportingLogic.SaveReportDefinitionAsync(SaveReportArgs);
            Assert.IsNotNull(result);
            Assert.IsFalse(string.IsNullOrWhiteSpace(result.Id.ToString()));
        }

        /// <summary>
        /// Test to check if data is going to database and saved
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SaveAsAdhocReportDefinitionTest()
        {
            var firstResult = await ReportingLogic.SaveReportDefinitionAsync(SaveReportArgs);
            string firstReportId = firstResult.Id;
            SaveReportArgs newSaveReportArgs = new SaveReportArgs
            {
                ColumnDefinition = new ReportColumnDefinition()
                {
                    Name = "Save as Test Report",
                    AdHocReportType = AdHocReportType.Employee,
                    Id = firstReportId
                }
            };
            var secondResult = await ReportingLogic.SaveAsReportDefinitionAsync(newSaveReportArgs);
            string secondReportId = secondResult.Id;
            Assert.IsNotNull(secondResult);
            Assert.IsFalse(string.IsNullOrWhiteSpace(secondResult.Id.ToString()));
            Assert.IsFalse(firstReportId == secondReportId);
            Assert.IsTrue(firstResult.Fields.Count == secondResult.Fields.Count);
            Assert.IsTrue(firstResult.Filters.Count == secondResult.Filters.Count);

        }

        /// <summary>
        /// Executes the report and check if the report was successfully created
        /// </summary>
        /// <param name="args"></param>
        public async Task CanExecuteReportAsync(ExecuteReportArgs args)
        {
            var ReportResult = await ReportingLogic.ExecuteReportAsync(args);
            Assert.IsTrue(ReportResult.Success);
        }

        /// <summary>
        /// Test the Case Custom Date Fields Report
        /// </summary>
        [TestMethod]
        public async Task CaseCustomDateFields()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "27AA17A1-13DE-49AF-8761-99D0609331A9",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = Convert.ToDateTime("05-12-2017"),
                    EndDate = Convert.ToDateTime("10-12-2017"),
                    Filters = new List<ReportCriteriaItem>(){new ReportCriteriaItem{  Name = "CustomField",
                  Value = "TestData"} },
                },
                PageNumber = 1,
                PageSize = 100
            };
            await CanExecuteReportAsync(executeReportArgs);
        }

        
        [TestMethod]
        /// <summary>
        /// Get dummy user for test
        /// </summary>
        /// <returns></returns>
        private User GetCurrentUser()
        {
            return new User()
            {
                Id = 0,
                CustomerKey = "000000000000000000000002",
                Email = "dev@absencesoft.com",
                Key = "000000000000000000000001",
                UserName = "dev@absencesoft.com",
                UserStatus = UserStatus.Active
            };
        }

        /// <summary>
        /// Get Report Column definition for test
        /// </summary>
        /// <returns></returns>
        private SaveReportArgs GetSaveReportArgs()
        {
            return new SaveReportArgs()
            {
                ColumnDefinition = new ReportColumnDefinition
                {
                    Name = "test Report",
                    AdHocReportType = AdHocReportType.Employee,
                    Fields = new List<AdHocReportField>{
                 new AdHocReportField{Name= "empl_first_name" , Order =1},
                 new AdHocReportField{Name="eplr_employer_url", Order=2},
                 new AdHocReportField{Name="eplr_employer_name", Order=3},
                 new AdHocReportField{Name="eplr_contact_address2", Order=4},
                 new AdHocReportField{Name="eplr_contact_city", Order=5}
            },
                    Filters = new List<AdHocReportFilter>
                {
                    new AdHocReportFilter{FieldName="empl_first_name", OperatorType= OperatorType.BeginsWith,Value="shy"},
                    new AdHocReportFilter{FieldName="eplr_employer_name", OperatorType= OperatorType.DoesNotEqual,Value="shyam"}
                }
                }
            };
        }
        
        [TestMethod]
        public async Task DeleteSavedAdhocReportTest()
        {
            //create a single positive test
            //save a report first
            var firstReport = await ReportingLogic.SaveReportDefinitionAsync(SaveReportArgs);
            DeleteReportArgs argPositive = new DeleteReportArgs() { AdhocReportIds = firstReport.Id, UserId = CurrentUser.Id, UserKey = CurrentUser.Key };
            var reportsDeleted = await ReportingLogic.DeleteAdhocReportAsync(argPositive);
            Assert.AreEqual(1, reportsDeleted);

            //test negative
            DeleteReportArgs argNegative = new DeleteReportArgs() { AdhocReportIds = "3A51A46B8B025925856A6661", UserId = CurrentUser.Id, UserKey = CurrentUser.Key };
            reportsDeleted = await ReportingLogic.DeleteAdhocReportAsync(argNegative);
            Assert.AreEqual(0, reportsDeleted);

            //test hybrid
            var secondReport = await ReportingLogic.SaveReportDefinitionAsync(SaveReportArgs);
            DeleteReportArgs argHybrid = new DeleteReportArgs() { AdhocReportIds = "3A51A46B8B025925856A6661,259440B175990BA722AA1110," + secondReport.Id, UserId=CurrentUser.Id, UserKey=CurrentUser.Key };
            reportsDeleted = await ReportingLogic.DeleteAdhocReportAsync(argHybrid);
            //the result should be one as other two belongs to another user or invalid
            Assert.AreEqual(reportsDeleted, 1);
        }


        /// <summary>
        /// Private method to save a report
        /// </summary>
        /// <returns></returns>
        private async Task<SharedReport> SaveSharedReport()
        {
            try
            {
                var args = new SaveSharedReportArgs();
                args.ReportName = "Testing";
                args.SharedWith = "USER";
                args.RoleNames = new string[] { "Administrator", "QA" };
                args.UserIds = new long[] { 1, 2 };
                args.UserKeys = new string[] { CurrentUser.Key };
                args.TeamKeys = new string[] { "5A7B64F215A46D426130E2E5", "5A7B6514ECE0E32D17441168" };
                args.Argument = new RequestReportArgs() { ReportDefinition = new Logic.Reporting.Arguments.ReportDefinition() { Fields = new List<SavedReportField>() { new SavedReportField() { Name = "empl_first_name" } } } };

                return await ReportingLogic.SaveSharedReport(args);
            }
            catch
            {
                return await Task.FromResult<SharedReport>(null);
            }
        }

        /// <summary>
        /// Test report saving
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SaveSharedReportTest()
        {
            var data = await SaveSharedReport();
            Assert.AreEqual((data != null && data.Id != null), true);
        }

        /// <summary>
        /// Test whether the get function is working or not
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        [Ignore]
        public async Task GetSharedReportTest()
        {
            var data1 = await SaveSharedReport();

            var getArgs = new GetSharedReportArgs() { ReportId = data1.Id, CustomerId = CurrentUser.CustomerId, CustomerKey = CurrentUser.CustomerKey };
            var dl = await ReportingLogic.GetSharedReport(getArgs);
            var data2 = dl.FirstOrDefault();

            Assert.AreEqual(data1.ReportName, data2.ReportName);
        }

        /// <summary>
        /// Test to delete a shared report
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task DeleteSharedReportTest()
        {
            var data1 = await SaveSharedReport();
            var deleteSuccess = await ReportingLogic.DeleteSharedReport(data1.Id.Value);
            Assert.IsTrue(deleteSuccess > 0);

            var getArgs = new GetSharedReportArgs() { ReportId = data1.Id, CustomerId = CurrentUser.CustomerId, CustomerKey = CurrentUser.CustomerKey };
            var dl = await ReportingLogic.GetSharedReport(getArgs);
            Assert.IsTrue(dl == null || dl.Count == 0);
        }
    }
}