﻿using AT.Entities.Authentication;
using AT.Entities.Reporting.Enums;
using AT.Logic.Arguments;
using AT.Logic.Reporting;
using AT.Logic.Reporting.Reports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AT.Test.Reporting
{
    [TestClass]
    public class BiReportTest
    {
        /// <summary>
        /// Reporting Logic
        /// </summary>
        private readonly ReportingLogic ReportingLogic;

        /// <summary>
        /// Current user
        /// </summary>
        private readonly User CurrentUser;

        /// <summary>
        /// Report test constructor
        /// </summary>
        public BiReportTest()
        {
            CurrentUser = GetCurrentUser();
            ReportingLogic = new ReportingLogic(CurrentUser);
        }

        [TestMethod]
        public async Task BiReportByLeave()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "117AF888-E65E-44da-A043-61A5896FC456",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = Convert.ToDateTime("05-12-2017"),
                    EndDate = Convert.ToDateTime("10-12-2017"),
                    Filters = new List<ReportCriteriaItem>(){
                        new ReportCriteriaItem
                        {
                            Name = "EmployeeId",
                            Values = {
                                        "000000000000000000000002",
                                        "57fd1d0ac5f32f0f58ad2eec",
                                        "5967de0ac5f3342608eb450b"
                                     }
                        }
                    }
                },
                PageNumber = 1,
                PageSize = 100
            };
            await CanExecuteReportAsync(executeReportArgs);
        }

        [TestMethod]
        public async Task BiReportByLocation()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "0771CD34-FC5B-4150-9C63-AC075D7159C7",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = Convert.ToDateTime("05-12-2017"),
                    EndDate = Convert.ToDateTime("10-12-2017"),
                    Filters = new List<ReportCriteriaItem>(){
                        new ReportCriteriaItem
                        {
                            Name = "EmployeeId",
                            Values = {
                                        "000000000000000000000002",
                                        "57fd1d0ac5f32f0f58ad2eec",
                                        "5967de0ac5f3342608eb450b"
                                     }
                        }
                    }
                },
                PageNumber = 1,
                PageSize = 100
            };
            await CanExecuteReportAsync(executeReportArgs);
        }

        [TestMethod]
        public async Task BiReportByReasons()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "9F59DA9A-FE44-4313-93C3-DAEAE51FD784",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = Convert.ToDateTime("05-12-2017"),
                    EndDate = Convert.ToDateTime("10-12-2017"),
                    Filters = new List<ReportCriteriaItem>(){
                        new ReportCriteriaItem
                        {
                            Name = "EmployeeId",
                            Values = {
                                        "000000000000000000000002",
                                        "57fd1d0ac5f32f0f58ad2eec",
                                        "5967de0ac5f3342608eb450b"
                                     }
                        }
                    }
                },
                PageNumber = 1,
                PageSize = 100
            };
            await CanExecuteReportAsync(executeReportArgs);
        }

        [TestMethod]
        public async Task BiReportByStatus()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "8EB71981-BB93-46C5-98B4-07C05CDE748F",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = Convert.ToDateTime("05-12-2017"),
                    EndDate = Convert.ToDateTime("10-12-2017"),
                    Filters = new List<ReportCriteriaItem>(){
                        new ReportCriteriaItem
                        {
                            Name = "EmployeeId",
                            Values = {
                                        "000000000000000000000002",
                                        "57fd1d0ac5f32f0f58ad2eec",
                                        "5967de0ac5f3342608eb450b"
                                     }
                        }
                    }
                },
                PageNumber = 1,
                PageSize = 100
            };
            await CanExecuteReportAsync(executeReportArgs);
        }

        [TestMethod]
        public async Task BiReportByPolicy()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "6524E5D6-E6F8-427F-8BC9-5C8427E18591",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = Convert.ToDateTime("05-12-2017"),
                    EndDate = Convert.ToDateTime("10-12-2017"),
                    Filters = new List<ReportCriteriaItem>(){
                        new ReportCriteriaItem
                        {
                            Name = "EmployeeId",
                            Values = {
                                        "000000000000000000000002",
                                        "57fd1d0ac5f32f0f58ad2eec",
                                        "5967de0ac5f3342608eb450b"
                                     }
                        }
                    }
                },
                PageNumber = 1,
                PageSize = 100
            };
            await CanExecuteReportAsync(executeReportArgs);
        }
        
        [TestMethod]
        public async Task BiReportTotalLeavesByLocation()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "0771CD34-FC5B-4150-9C63-AC075D7159C7",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = Convert.ToDateTime("01-01-2016"),
                    EndDate = Convert.ToDateTime("01-12-2018"),
                    Filters = new List<ReportCriteriaItem>(){
                        new ReportCriteriaItem
                        {
                            Name = "EmployeeId",
                            Values = {
                                        "000000000000000000000002",
                                        "57fd1d0ac5f32f0f58ad2eec",
                                        "5967de0ac5f3342608eb450b"
                                     }
                        },
                        new ReportCriteriaItem
                        {
                            Name = "Duration",
                            Value = "Yearly"
                        },
                        new ReportCriteriaItem
                        {
                            Name = "ReportType",
                            Value = "ByLocation"
                        }
                    }
                },
                PageNumber = 1,
                PageSize = 100,
                GroupType = ReportGroupType.BiReportTotalLeavesByLocation
            };
            await CanExecuteReportAsync(executeReportArgs);
        }

        [TestMethod]
        public async Task BIReportTotalLeavesByReason()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "0771CD34-FC5B-4150-9C63-AC075D7159C7",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = Convert.ToDateTime("01-01-2016"),
                    EndDate = Convert.ToDateTime("01-12-2018"),
                    Filters = new List<ReportCriteriaItem>(){
                        new ReportCriteriaItem
                        {
                            Name = "EmployeeId",
                            Values = {
                                        "000000000000000000000002",
                                        "57fd1d0ac5f32f0f58ad2eec",
                                        "5967de0ac5f3342608eb450b"
                                     }
                        },
                        new ReportCriteriaItem
                        {
                            Name = "Duration",
                            Value = "Yearly"
                        },
                        new ReportCriteriaItem
                        {
                            Name = "ReportType",
                            Value = "ByReason"
                        }
                    }
                },
                PageNumber = 1,
                PageSize = 100,
                GroupType = ReportGroupType.BiReportTotalLeavesByReason
            };
            await CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Test the Team compliance BI Report
        /// </summary>
        [TestMethod]
        public async void TeamComplianceBiReport()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "2E96F17E-8AFE-4E1D-8D39-B39901383FE1"
            };
            await CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Test the Team productivity BI Report
        /// </summary>
        [TestMethod]
        public async void TeamProductivityBiReport()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "C29B0D37-B62D-418A-831B-1AB5BDA5F7A9"
            };
            await CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Test the Case Processing Metrics BI Report
        /// </summary>
        [TestMethod]
        public async void CaseProcessingMetricsBiReport()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "1DF8F65F-F9FE-409E-8786-0A07F064CB6C"
            };
           await CanExecuteReportAsync(executeReportArgs);
        }

        /// <summary>
        /// Executes the report and check if the report was successfully created
        /// </summary>
        /// <param name="args"></param>
        public async Task CanExecuteReportAsync(ExecuteReportArgs args)
        {
            var ReportResult = await ReportingLogic.ExecuteReportAsync(args);
            Assert.IsTrue(ReportResult.Success);
        }

        [TestMethod]
        /// <summary>
        /// Get dummy user for test
        /// </summary>
        /// <returns></returns>
        private User GetCurrentUser()
        {
            var employers = new List<EmployerAccess>()
            {
                new EmployerAccess()
                {
                  EmployerId = "000000000000000000000002"
                }
            };
            return new User()
            {
                Id = 0,
                CustomerKey = "000000000000000000000002",
                Email = "dev@absencesoft.com",
                Key = "000000000000000000000001",
                UserName = "dev@absencesoft.com",
                UserStatus = UserStatus.Active,
                Employers = employers

            };
        }
    }
}