﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AT.Logic.Reporting.Schedule;
using FluentAssertions;
using Newtonsoft.Json;

namespace AT.Test.Reporting
{
    [TestClass]
    public class ScheduleHelperWeeklyRecurrenceTest
    {
        [TestMethod]
        public void TestGetNextDate_NoEndDate_OnEveryXWeeks_Default_1Week()
        {
            var currentDate = new DateTime(2018, 03, 09, 06, 00, 00);
            ScheduleInfo schInfo = new ScheduleInfo()
            {
                StartDate = currentDate,
                RecurrenceType=Logic.Reporting.Enums.RecurrenceType.Weekly,                             
                WeeklySelectedDays = new SelectedDayOfWeekValues() { Monday = true, Thursday=true }
            };

            ScheduleHelper helper = new ScheduleHelper(schInfo);
            var nextDate=helper.GetNextDate(currentDate);
            nextDate.Should().Be(new DateTime(2018, 03, 12, 06, 00, 00));

            nextDate = helper.GetNextDate(nextDate);
            nextDate.Should().Be(new DateTime(2018, 03, 15, 06, 00, 00));

        }

        [TestMethod]
        public void TestGetNextDate_NoEndDate_OnEveryXWeeks_2Weeks()
        {
            var currentDate = new DateTime(2018, 03, 09, 06, 00, 00);
            ScheduleInfo schInfo = new ScheduleInfo()
            {
                StartDate = currentDate,
                RecurrenceType = Logic.Reporting.Enums.RecurrenceType.Weekly,
                WeeklyRecurEveryXWeeks=2,
                WeeklySelectedDays = new SelectedDayOfWeekValues() { Monday = true, Thursday = true }
            };

            ScheduleHelper helper = new ScheduleHelper(schInfo);
            var nextDate = helper.GetNextDate(currentDate);
            nextDate.Should().Be(new DateTime(2018, 03, 19, 06, 00, 00));

            nextDate = helper.GetNextDate(nextDate);
            nextDate.Should().Be(new DateTime(2018, 03, 22, 06, 00, 00));

        }



    }
}
