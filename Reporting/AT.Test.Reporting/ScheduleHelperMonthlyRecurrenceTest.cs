﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AT.Logic.Reporting.Schedule;
using FluentAssertions;
using Newtonsoft.Json;

namespace AT.Test.Reporting
{
    [TestClass]
    public class ScheduleHelperMonthlyRecurrenceTest
    {
        [TestMethod]
        public void TestGetNextDate_NoEndDate_OnSpecificDayOfTheMonth_Default()
        {
            var currentDate = new DateTime(2018, 03, 09, 06, 00, 00);
            ScheduleInfo schInfo = new ScheduleInfo()
            {
                StartDate = currentDate,
                RecurrenceType=Logic.Reporting.Enums.RecurrenceType.Monthly,                             
                MonthlyRecurType=Logic.Reporting.Enums.MonthlyRecurType.OnSpecificDayOfMonth,
                MonthlyRecurOnSpecificDateDayValue=10 //10th day of every month
            };

            ScheduleHelper helper = new ScheduleHelper(schInfo);
            var nextDate=helper.GetNextDate(currentDate);
            nextDate.Should().Be(new DateTime(2018, 03, 10, 06, 00, 00));

        }


        [TestMethod]
        public void TestGetNextDate_NoEndDate_OnSpecificDayOfTheMonth_Every2Months()
        {
            var currentDate = new DateTime(2018, 03, 09, 06, 00, 00);
            ScheduleInfo schInfo = new ScheduleInfo()
            {
                StartDate = currentDate,
                RecurrenceType = Logic.Reporting.Enums.RecurrenceType.Monthly,
                MonthlyRecurType = Logic.Reporting.Enums.MonthlyRecurType.OnSpecificDayOfMonth,
                MonthlyRecurOnSpecificDateDayValue = 9, //9th day of every month
                MonthlyRecurEveryXMonths=2
            };

            ScheduleHelper helper = new ScheduleHelper(schInfo);
            var nextDate = helper.GetNextDate(currentDate);
            nextDate.Should().Be(new DateTime(2018, 04, 09, 06, 00, 00));

        }

        [TestMethod]
        public void TestGetNextDate_NoEndDate_OnCustomDayOfTheMonth_Default()
        {
            var currentDate = new DateTime(2018, 03, 09, 06, 00, 00);
            ScheduleInfo schInfo = new ScheduleInfo()
            {
                StartDate = currentDate,
                RecurrenceType = Logic.Reporting.Enums.RecurrenceType.Monthly,
                MonthlyRecurType = Logic.Reporting.Enums.MonthlyRecurType.OnCustomDateFormat,
                MonthlySpecificDatePartOne=Logic.Reporting.Enums.MonthlySpecificDay.Third,
                MonthlySpecificDatePartTwo=Logic.Reporting.Enums.MonthlySpecificWeekday.Friday
            };

            ScheduleHelper helper = new ScheduleHelper(schInfo);
            var nextDate = helper.GetNextDate(currentDate);
            nextDate.Should().Be(new DateTime(2018, 03, 16, 06, 00, 00));
        }

        [TestMethod]
        public void TestGetNextDate_NoEndDate_OnCustomDayOfTheMonth_Every2Months()
        {
            var currentDate = new DateTime(2018, 03, 09, 06, 00, 00);
            ScheduleInfo schInfo = new ScheduleInfo()
            {
                StartDate = currentDate,
                RecurrenceType = Logic.Reporting.Enums.RecurrenceType.Monthly,
                MonthlyRecurType = Logic.Reporting.Enums.MonthlyRecurType.OnCustomDateFormat,
                MonthlySpecificDatePartOne = Logic.Reporting.Enums.MonthlySpecificDay.Last,
                MonthlySpecificDatePartTwo = Logic.Reporting.Enums.MonthlySpecificWeekday.Friday,
                MonthlyRecurEveryXMonths=2
            };

            ScheduleHelper helper = new ScheduleHelper(schInfo);
            var nextDate = helper.GetNextDate(currentDate);
            //SO instead of 27-Apr-2018 because Even if its every 2 months First Last friday is in future 
            nextDate.Should().Be(new DateTime(2018, 03, 30, 06, 00, 00));

        }



    }
}
