﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AT.Logic.Arguments;
using AT.Logic.Reporting.Reports;
using AT.Entities.Reporting.Enums;
using AT.Logic.Reporting;
using AT.Entities.Authentication;

namespace AT.Test.Reporting
{
    [TestClass]
    public class DailyCaseReportTest
    {
        /// <summary>
        /// Current user
        /// </summary>
        private readonly User CurrentUser;

        /// <summary>
        /// Reporting Logic
        /// </summary>
        private readonly ReportingLogic ReportingLogic;

        /// <summary>
        /// Constructor for the Test class
        /// </summary>
        public DailyCaseReportTest()
        {
            CurrentUser = GetCurrentUser();
            ReportingLogic = new ReportingLogic(CurrentUser);
        }

        /// <summary>
        /// Daily Case Day Report
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task DailyCaseDayReport()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "688BDE87-48AD-4560-84A7-982D5DE8D73A",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = Convert.ToDateTime("05-12-2016"),
                    EndDate = Convert.ToDateTime("10-12-2017"),
                    Filters = new List<ReportCriteriaItem>(){
                        new ReportCriteriaItem
                        {
                            Name = "OFFICELOCATION",
                            Value = null
                        }
                    }
                },
                PageNumber = 1,
                PageSize = 100,
                GroupType = ReportGroupType.CaseReport
            };
            await CanExecuteReportAsync(executeReportArgs);
        }
        
        /// <summary>
        /// Daily Case Day By Day Report
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task DailyCaseDayByDayReport()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "688BDE87-48AD-4560-84A7-982D5DE8D73A",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = Convert.ToDateTime("05-12-2016"),
                    EndDate = Convert.ToDateTime("10-12-2017"),
                    Filters = new List<ReportCriteriaItem>(){
                        new ReportCriteriaItem
                        {
                            Name = "OFFICELOCATION",
                            Value = null
                        }
                    }
                },
                PageNumber = 1,
                PageSize = 100,
                GroupType = ReportGroupType.CaseReportByDay
            };
            await CanExecuteReportAsync(executeReportArgs);
        }

        [TestMethod]
        public async Task DailyCaseDayReportByOrganization()
        {
            ExecuteReportArgs executeReportArgs = new ExecuteReportArgs
            {
                ReportId = "688BDE87-48AD-4560-84A7-982D5DE8D73A",
                Criteria = new Logic.Reporting.Reports.ReportCriteria
                {
                    StartDate = Convert.ToDateTime("05-12-2016"),
                    EndDate = Convert.ToDateTime("10-12-2017"),
                    Filters = new List<ReportCriteriaItem>(){
                        new ReportCriteriaItem
                        {
                            Name = "OFFICELOCATION",
                            Value = null
                        },
                        new ReportCriteriaItem
                        {
                            Name = "OrganizationTypeGrouping",
                            Value = ""
                        }
                    }
                },
                PageNumber = 1,
                PageSize = 100,
                GroupType = ReportGroupType.CaseReportByOrganization
            };
            await CanExecuteReportAsync(executeReportArgs);
        }
        
        /// <summary>
        /// Executes the report and check if the report was successfully created
        /// </summary>
        /// <param name="args"></param>
        public async Task CanExecuteReportAsync(ExecuteReportArgs args)
        {
            var ReportResult = await ReportingLogic.ExecuteReportAsync(args);
            Assert.IsTrue(ReportResult.Success);
        }

        [TestMethod]
        /// <summary>
        /// Get dummy user for test
        /// </summary>
        /// <returns></returns>
        private User GetCurrentUser()
        {
            return new User()
            {
                Id = 0,
                CustomerKey = "000000000000000000000002",
                Email = "dev@absencesoft.com",
                Key = "000000000000000000000001",
                UserName = "dev@absencesoft.com",
                UserStatus = UserStatus.Active
            };
        }
    }
}
