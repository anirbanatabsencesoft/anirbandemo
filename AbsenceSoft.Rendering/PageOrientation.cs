﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Rendering
{
    public enum PageOrientation
    {
        Portrait = 0,
        Landscape = 1
    }
}
