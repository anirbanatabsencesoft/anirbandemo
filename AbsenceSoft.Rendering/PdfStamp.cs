﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Rendering
{
    [Serializable]
    public class PdfStamp
    {
        /// <summary>
        /// Gets or sets the value of the stamp.
        /// </summary>
        /// <value>
        /// The value of the stamp.
        /// </value>
        public object Value { get; set; }

        /// <summary>
        /// Gets or sets the format string for the value, if not provided then the default Value.ToString() is used.
        /// </summary>
        /// <value>
        /// The format string for the value text.
        /// </value>
        public string Format { get; set; }

        /// <summary>
        /// Gets or sets the x-coordinate of this stamp's lower left-hand corner (in points).
        /// </summary>
        /// <value>
        /// The x-coordinate of this stamp's lower left-hand corner (in points).
        /// </value>
        public float X { get; set; }

        /// <summary>
        /// Gets or sets the y-coordinate of this stamp's lower left-hand corner (in points).
        /// </summary>
        /// <value>
        /// The y-coordinate of this stamp's lower left-hand corner (in points).
        /// </value>
        public float Y { get; set; }

        /// <summary>
        /// Gets or sets the maximum width of the stamp region in points.
        /// </summary>
        /// <value>
        /// The width of the stamp region in points.
        /// </value>
        public float? MaxWidth { get; set; }

        /// <summary>
        /// Gets or sets the maximum height of the stamp region in points.
        /// </summary>
        /// <value>
        /// The height of the stamp region in points.
        /// </value>
        public float? MaxHeight { get; set; }

        /// <summary>
        /// Gets or sets the size of the font.
        /// </summary>
        /// <value>
        /// The size of the font.
        /// </value>
        public float FontSize { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the text of this stamp will attempt to automatically fit in the bounds of the stamp region.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this stamp will attempt to automatically fit in the bounds of the stamp region; otherwise, <c>false</c>.
        /// </value>
        public bool AutoFit { get; set; }
    }
}
