﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Rendering.Templating;
using AbsenceSoft.Rendering.Templating.MailMerge;
using Aspose.Words;
using Aspose.Words.MailMerging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using System.Dynamic;

namespace AbsenceSoft.Rendering
{
    public static class Template
    {
        /// <summary>
        /// Instance of our compiler to use for all render requests. Contains all of the registered tag handlers
        /// and rendering logic.
        /// </summary>
        private static FormatCompiler mustacheCompiler = new FormatCompiler();

        /// <summary>
        /// Renders a block of tokenized HTML using the Handlebars/Mustache syntax defined in the Readme.md and
        /// uses the supplied view model in order to format the output and return the resulting HTML document.
        /// </summary>
        /// <param name="templateHtml"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public static string RenderTemplate(string templateHtml, object viewModel)
        {
            Generator generator = mustacheCompiler.Compile(templateHtml);
            return generator.Render(viewModel);
        }

        /// <summary>
        /// Renders a block of tokenized HTML based on Mail Merge functionali t stored in a Word document
        /// Uses the supplied view model in order to format the output and return the resulting HTML document
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public static string RenderTemplateToHTML(Stream s, MailMergeData mergeRecords)
        {
            Document doc = RenderDocument(s, mergeRecords);
            return doc.ToString(SaveFormat.Html);
        }

        /// <summary>
        /// Renders a block of tokenized HTML based on Mail Merge functionality stored in a Word document
        /// Uses the supplied view model in order to format the output and return the resulting HTML document
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public static string RenderTemplateToHTML(Stream s, object viewModel)
        {
            Document doc = RenderDocument(s, viewModel);
            return doc.ToString(SaveFormat.Html);
        }


        /// <summary>
        /// Renders a tokenized Word doc based on Mail Merge functionality
        /// Can be used when we don't want to serialize straight to HTML
        /// </summary>
        /// <param name="s"></param>
        /// <param name="mergeRecords"></param>
        /// <returns>A new stream with the final word doc</returns>
        public static Stream RenderWordTemplate(Stream s, SaveFormat format, MailMergeData mergeRecords)
        {
            MemoryStream stream = new MemoryStream();
            Document doc = RenderDocument(s, mergeRecords);
            doc.Save(stream, format);
            return stream;
        }


        /// <summary>
        /// Renders a tokenized Word doc based on Mail merge functionality
        /// Can be used when we don't want to serialize straight to HTML, and don't have the file stream anymore
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="format"></param>
        /// <param name="mergeRecords"></param>
        /// <returns></returns>
        public static byte[] RenderWordTemplate(byte[] bytes, SaveFormat format, MailMergeData mergeRecords)
        {
            using (MemoryStream stream = new MemoryStream(bytes))
            {
                Document doc = RenderDocument(stream, mergeRecords);
                using (MemoryStream saveStream = new MemoryStream())
                {
                    doc.Save(saveStream, format);
                    return saveStream.ReadAllBytes();
                }
            }
        }

       
        
        /// <summary>
        /// Renders a tokenized pdf based on Mail merge functionality
        /// Can be used when we don't want to serialize straight to HTML, and don't have the file stream anymore 
        /// </summary>
        /// <param name="bytes">The bytes.</param>
        /// <param name="mergeRecords">The merge records.</param>
        /// <param name="pdfFields">The PDF fields.</param>
        /// <returns></returns>
        public static byte[] RenderPdfTemplate(byte[] bytes, MailMergeData mergeRecords, Dictionary<string, List<string>> pdfFields)
        {
            MailMergeDataSource dataSource = new MailMergeDataSource(new List<MailMergeData>() { mergeRecords }, "MailMerge");
            dataSource.MoveNext();
            //We need to create a new dictionary which will hold the actual key and the mergefield values
            //once done we need to pass this dictionary to FillPdf function
            Dictionary<string, string> pdfFieldsValues = new Dictionary<string, string>();
            //loop throug all the Keys of PdfFields collection and get value for each of them
            foreach ( string pdfKey in pdfFields.Keys)
            {
                string mergeValue = string.Empty;
                //Against one pdfKey we can have multipile mergefields which values needs to be concatenate 
                //to make a single string before sending to FillPdf function
                foreach (string mergeKey in pdfFields[pdfKey])
                {
                    object fieldValue;
                    dataSource.GetValue(mergeKey, out fieldValue);
                    if (fieldValue != null)
                    {
                        mergeValue = string.Concat(mergeValue, " ", fieldValue.ToString());
                    }
                }
                pdfFieldsValues.Add(pdfKey, mergeValue);
            }
            //call the FillPdf 
            return Pdf.FillPdf(bytes, pdfFieldsValues);
          
        }

        /// <summary>
        /// Renders a tokenized word doc and returns the finalized document.  
        /// Used internally so we can have multiple different ways of returning the document
        /// </summary>
        /// <param name="s"></param>
        /// <param name="mergeRecords"></param>
        /// <returns></returns>
        private static Document RenderDocument(Stream s, MailMergeData mergeRecords)
        {
            using (InstrumentationContext context = new InstrumentationContext("Template.RenderTemplate"))
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    s.CopyTo(ms);
                    Document doc = new Document(ms);

                    // Clear BuiltIn Properties
                    if (doc.BuiltInDocumentProperties != null)
                    {
                        doc.BuiltInDocumentProperties.Clear();
                    }
                    /// Have to have a special callback to handle image merges
                    doc.MailMerge.FieldMergingCallback = new LogoMergeFromAWS(mergeRecords.CustomerLogo, mergeRecords.EmployerLogo);
                    doc.MailMerge.MergeDuplicateRegions = true;
                    //This will leverage System.Dynamic framework to build dynamic properties which will contain policy code
                    BuildPolicyInfoMergeData(mergeRecords, doc);
                    BuildCaseAssigneeMergeData(mergeRecords, doc);
                    ///in order to properly merge, we need to do three passes, and extract some data from mail merge
                    try
                    {
                        foreach (var prop in mergeRecords.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
                        {
                            if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                            {
                                var list = prop.GetValue(mergeRecords) as System.Collections.IEnumerable;
                                if (list != null)
                                {
                                    MailMergeDataSource listSource = new MailMergeDataSource(list, prop.Name);
                                    doc.MailMerge.ExecuteWithRegions(listSource);
                                }
                            }
                        }

                        MailMergeDataSource contactSource = new MailMergeDataSource(mergeRecords.AdminContacts, "Contacts");
                        doc.MailMerge.CleanupOptions = MailMergeCleanupOptions.RemoveUnusedRegions;
                        doc.MailMerge.ExecuteWithRegions(contactSource);

                        MailMergeDataSource dataSource = new MailMergeDataSource(new List<MailMergeData>() { mergeRecords }, "MailMerge");
                        doc.MailMerge.CleanupOptions = MailMergeCleanupOptions.RemoveEmptyParagraphs | MailMergeCleanupOptions.RemoveUnusedFields | MailMergeCleanupOptions.RemoveContainingFields | MailMergeCleanupOptions.RemoveStaticFields;
                        doc.MailMerge.Execute(dataSource);
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Error performing mail merge from .docx format", ex);
                        throw new AbsenceSoftException("There was a problem when attempting to mail merge your documents");
                    }

                    return doc;
                }
            }
        }

        /// <summary>
        /// Renders a tokenized word doc and returns the finalized document.  
        /// Used internally so we can have multiple different ways of returning the document
        /// </summary>
        /// <param name="s"></param>
        /// <param name="mergeRecords"></param>
        /// <returns></returns>
        private static Document RenderDocument(Stream s, object mergeRecords)
        {
            using (InstrumentationContext context = new InstrumentationContext("Template.RenderTemplate"))
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    s.CopyTo(ms);
                    Document doc = new Document(ms);

                    doc.MailMerge.MergeDuplicateRegions = true;
                    ///in order to properly merge, we need to do three passes, and extract some data from mail merge
                    try
                    {
                        foreach (var prop in mergeRecords.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
                        {
                            if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                            {
                                var list = prop.GetValue(mergeRecords) as System.Collections.IEnumerable;
                                if (list != null)
                                {
                                    MailMergeDataSource listSource = new MailMergeDataSource(list, prop.Name);
                                    doc.MailMerge.ExecuteWithRegions(listSource);
                                }
                            }
                        }

                        MailMergeDataSource dataSource = new MailMergeDataSource(new List<object>() { mergeRecords }, "MailMerge");
                        doc.MailMerge.CleanupOptions = MailMergeCleanupOptions.RemoveEmptyParagraphs | MailMergeCleanupOptions.RemoveUnusedFields | MailMergeCleanupOptions.RemoveContainingFields | MailMergeCleanupOptions.RemoveStaticFields;
                        doc.MailMerge.Execute(dataSource);
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Error performing mail merge from .docx format", ex);
                        throw new AbsenceSoftException("There was a problem when attempting to mail merge your documents");
                    }

                    return doc;
                }
            }
        }

        private static void BuildPolicyInfoMergeData(MailMergeData mergeRecords, Document doc)
        {
            try
            {
                if (doc.MailMerge != null)
                {
                    string[] policyInfoFields = doc.MailMerge.GetFieldNames().Where(field => field.IndexOf("PolicyInfo.") == 0).ToArray<string>();
                    if (policyInfoFields != null && policyInfoFields.Length > 0)
                    {
                        mergeRecords.PolicyInfo = new ExpandoObject();
                        string[] policyCodes = policyInfoFields.Select(field => field.Replace("PolicyInfo.Policy", string.Empty).Replace("Exists", string.Empty).Replace("Approved", string.Empty).Replace("DeniedReasonCode", string.Empty).Replace("DeniedReason", string.Empty).Replace("Denied", string.Empty).Replace("Name", string.Empty).Replace("IsEligible", string.Empty).Replace("IneligibleReason", string.Empty).Replace("IsPending", string.Empty).Replace("StartDate", string.Empty).Replace("EndDate", string.Empty).Replace("TimeUsedText", string.Empty).Replace("TimeRemainingText", string.Empty).Replace("Determination", string.Empty)).Distinct().ToArray<string>();
                        foreach (string policyCode in policyCodes)
                        {
                            if (mergeRecords != null)
                            {                                
                                var dictionaryPolicyFields = (IDictionary<string, object>)mergeRecords.PolicyInfo;
                                if (dictionaryPolicyFields.ContainsKey("Policy" + policyCode + "Exists")) continue;
                                var policyMergeData = mergeRecords.Policies.FirstOrDefault(policy => policy.Code == policyCode);
                                if (policyMergeData != null)
                                {
                                    dictionaryPolicyFields.Add("Policy" + policyCode + "Exists", "YES");
                                    dictionaryPolicyFields.Add("Policy" + policyCode + "Name", policyMergeData.Name);
                                    dictionaryPolicyFields.Add("Policy" + policyCode + "IsEligible", policyMergeData.Status != EligibilityStatus.Ineligible ? "YES" : "NO");
                                    dictionaryPolicyFields.Add("Policy" + policyCode + "IneligibleReason", policyMergeData.IneligibleReason);
                                    dictionaryPolicyFields.Add("Policy" + policyCode + "IsPending", policyMergeData.Determination == AdjudicationSummaryStatus.Pending ? "YES" : "NO");
                                    dictionaryPolicyFields.Add("Policy" + policyCode + "StartDate", policyMergeData.StartDate.ToUIString());
                                    dictionaryPolicyFields.Add("Policy" + policyCode + "EndDate", policyMergeData.EndDate.ToUIString());
                                    dictionaryPolicyFields.Add("Policy" + policyCode + "TimeUsedText", policyMergeData.TimeUsedText);
                                    dictionaryPolicyFields.Add("Policy" + policyCode + "TimeRemainingText", policyMergeData.TimeRemainingText);
                                    dictionaryPolicyFields.Add("Policy" + policyCode + "ProjectedUsageText", policyMergeData.ProjectedUsageText);
                                    if (mergeRecords.ApprovedPolicies != null && mergeRecords.ApprovedPolicies.Where(policy => policy.Code == policyCode).Any())
                                    {
                                        dictionaryPolicyFields.Add("Policy" + policyCode + "Approved", "YES");
                                    }
                                    else
                                    {
                                        dictionaryPolicyFields.Add("Policy" + policyCode + "Approved", "NO");
                                    }
                                    if (mergeRecords.DeniedPolicies != null && mergeRecords.DeniedPolicies.Where(policy => policy.Code == policyCode).Any())
                                    {
                                        dictionaryPolicyFields.Add("Policy" + policyCode + "Denied", "YES");
                                        var deninedReason = mergeRecords.DeniedPolicies.Where(policy => policy.Code == policyCode).FirstOrDefault();
                                        if (deninedReason != null && !string.IsNullOrEmpty(deninedReason.DeniedReason))
                                        {
                                            dictionaryPolicyFields.Add("Policy" + policyCode + "DeniedReason", deninedReason.DeniedReason);
                                            dictionaryPolicyFields.Add("Policy" + policyCode + "DeniedReasonCode", deninedReason.DeniedReasonCode);
                                        }
                                    }
                                    else
                                    {
                                        dictionaryPolicyFields.Add("Policy" + policyCode + "Denied", "NO");
                                        dictionaryPolicyFields.Add("Policy" + policyCode + "DeniedReason", "NA");
                                        dictionaryPolicyFields.Add("Policy" + policyCode + "DeniedReasonCode", "NA");
                                    }

                                }
                                else 
                                { 
                                    dictionaryPolicyFields.Add("Policy" + policyCode + "Exists", "NO");
                                    dictionaryPolicyFields.Add("Policy" + policyCode + "Name", "NA");
                                    dictionaryPolicyFields.Add("Policy" + policyCode + "IsEligible", "NA");
                                    dictionaryPolicyFields.Add("Policy" + policyCode + "IneligibleReason", "NA");
                                    dictionaryPolicyFields.Add("Policy" + policyCode + "IsPending", "NA");
                                    dictionaryPolicyFields.Add("Policy" + policyCode + "StartDate", "NA");
                                    dictionaryPolicyFields.Add("Policy" + policyCode + "EndDate", "NA");
                                    dictionaryPolicyFields.Add("Policy" + policyCode + "TimeUsedText", "NA");
                                    dictionaryPolicyFields.Add("Policy" + policyCode + "TimeRemainingText", "NA");
                                    dictionaryPolicyFields.Add("Policy" + policyCode + "Approved", "NA");
                                    dictionaryPolicyFields.Add("Policy" + policyCode + "Denied", "NA");
                                    dictionaryPolicyFields.Add("Policy" + policyCode + "DeniedReason", "NA");
                                    dictionaryPolicyFields.Add("Policy" + policyCode + "DeniedReasonCode", "NA");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error performing mail merge from .docx format", ex);
                throw new AbsenceSoftException("There was a problem when attempting to add Dynamic Policy Info");
            }
        }

        private static void BuildCaseAssigneeMergeData(MailMergeData mergeRecords, Document doc)
        {
            try
            {
                if (doc.MailMerge != null)
                {
                    string[] CaseAssigneeFields = doc.MailMerge.GetFieldNames().Where(field => field.IndexOf("AssigneeInfo.") == 0).ToArray<string>();
                    if (CaseAssigneeFields != null && CaseAssigneeFields.Length > 0)
                    {
                        mergeRecords.AssigneeInfo = new ExpandoObject();
                        string[] policyCodes = CaseAssigneeFields.Select(field => field.Replace("AssigneeInfo.", string.Empty).Replace("FirstName", string.Empty).Replace("LastName", string.Empty).Replace("Email", string.Empty).Replace("WorkPhone", string.Empty).Replace("Fax", string.Empty).Replace("Title", string.Empty).Replace("Name", string.Empty)).Distinct().ToArray<string>();
                        foreach (string assigneeCode in policyCodes)
                        {
                            if (mergeRecords != null)
                            {
                                var dictionaryPolicyFields = (IDictionary<string, object>)mergeRecords.AssigneeInfo;
                                var assigneeMergeData = mergeRecords.CaseAssignees.FirstOrDefault(c => c.Code == assigneeCode);
                                if (assigneeMergeData != null)
                                {
                                    dictionaryPolicyFields.Add(assigneeCode + "FirstName", assigneeMergeData.FirstName);
                                    dictionaryPolicyFields.Add(assigneeCode + "LastName", assigneeMergeData.LastName);
                                    dictionaryPolicyFields.Add(assigneeCode + "Email", assigneeMergeData.Email ?? "NA" );
                                    dictionaryPolicyFields.Add(assigneeCode + "WorkPhone", assigneeMergeData.WorkPhone ?? "NA");
                                    dictionaryPolicyFields.Add(assigneeCode + "Fax", assigneeMergeData.Fax ?? "NA");
                                    dictionaryPolicyFields.Add(assigneeCode + "Title", assigneeMergeData.Title);
                                    dictionaryPolicyFields.Add(assigneeCode + "Name", assigneeMergeData.Name);
                                }
                                else
                                {
                                    dictionaryPolicyFields.Add(assigneeCode + "FirstName", "NA");
                                    dictionaryPolicyFields.Add(assigneeCode + "LastName", "NA");
                                    dictionaryPolicyFields.Add(assigneeCode + "Email", "NA");
                                    dictionaryPolicyFields.Add(assigneeCode + "WorkPhone", "NA");
                                    dictionaryPolicyFields.Add(assigneeCode + "Fax", "NA");
                                    dictionaryPolicyFields.Add(assigneeCode + "Title", "NA");
                                    dictionaryPolicyFields.Add(assigneeCode + "Name", "NA");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error performing mail merge from .docx format", ex);
                throw new AbsenceSoftException("There was a problem when attempting to add Case Assignee Info");
            }
        }

        public static IEnumerable<string> GetExhaustedFlags()
        {
            //HACK: This won't scale. Refactor if more flags are added.
            yield return "Case.Flags.${PolicyCode}-EXHAUSTED";

            var policyTypes = Enum.GetNames(typeof(PolicyType));
            foreach (var item in policyTypes)
                yield return string.Format("Case.Flags.{0}-EXHAUSTED", item);
        }

        public static IEnumerable<string> GetDeniedReasonFlags(string[] fieldNames)
        {
            if(fieldNames.Contains("Case.FMLAPolicy.DeniedReasonCode"))
                yield return "Case.FMLAPolicy.DeniedReasonCode";
            if(fieldNames.Contains("DeniedReasonCode"))
            {
                var numQuery = fieldNames.Where(num => num.Equals("DeniedReasonCode"));
                for (int i = 0; i <= numQuery.Count(); i++)
                    yield return "DeniedReasonCode";
            }
        }

        /// <summary>
        /// Generates a MS Word Document with employer specific custom fields, if they are passed in
        /// </summary>
        /// <param name="e"></param>
        /// <param name="customFields"></param>
        /// <returns></returns>
        public static Stream GenerateTemplate(List<CustomField> customFields = null, List<string> policyCodes = null, List<DemandType> demandTypes = null, List<string> caseAssigneeTypeCode = null)
        {
            using (InstrumentationContext context = new InstrumentationContext("Template.GenerateTemplate", customFields))
            {
                MemoryStream s = new MemoryStream();

                Document doc = new Document();
                DocumentBuilder builder = new DocumentBuilder(doc);
                List<string> mergeFieldNames = BuildMergeFieldNames(new List<string>() { }, new MailMergeData().GetType(), customFields);

                mergeFieldNames.AddRange(GetExhaustedFlags());

                builder.InsertParagraph();
                builder.Write("Below are the merge fields that you can use when customizing your communications");

                builder.InsertParagraph();
                string[] dateCheckSting = new string[] { "Date", "LastUpdated", "ActualRTW", "ExpectedRTW", "DueDate", "CreatedDate", "StartDate", "EndDate" };
                foreach (string name in mergeFieldNames)
                {
                    string mergeField = name;
                    if (name == "Relapse")
                    {
                        continue;
                    }
                    if (name == "EmployerLogo" || name == "CustomerLogo" || name == "Logo")
                        mergeField = string.Format("Image:{0}", name);

                    if (name.StartsWith("TableStart"))
                    {
                        builder.InsertParagraph();
                    }
                    string mergeFormat = @"\* MERGEFORMAT";
                    if (dateCheckSting.Any(name.Contains))
                    {
                        mergeFormat = @"\@ ""M/d/yyyy""";
                    }
                    builder.InsertField(string.Format(@"MERGEFIELD ""{0}"" {1}", mergeField, mergeFormat));
                    builder.InsertBreak(BreakType.LineBreak);
                }
                if (policyCodes != null)
                {
                    foreach (string policyCode in policyCodes)
                    {
                        string mergeField = "PolicyInfo.Policy" + policyCode + "Exists";
                        builder.InsertField(string.Format(@"MERGEFIELD ""{0}""", mergeField));
                        builder.InsertBreak(BreakType.LineBreak);
                        mergeField = "PolicyInfo.Policy" + policyCode + "Approved";
                        builder.InsertField(string.Format(@"MERGEFIELD ""{0}""", mergeField));
                        builder.InsertBreak(BreakType.LineBreak);
                        mergeField = "PolicyInfo.Policy" + policyCode + "Denied";
                        builder.InsertField(string.Format(@"MERGEFIELD ""{0}""", mergeField));
                        builder.InsertBreak(BreakType.LineBreak);
                        mergeField = "PolicyInfo.Policy" + policyCode + "DeniedReason";
                        builder.InsertField(string.Format(@"MERGEFIELD ""{0}""", mergeField));
                        builder.InsertBreak(BreakType.LineBreak);
                        mergeField = "PolicyInfo.Policy" + policyCode + "DeniedReasonCode";
                        builder.InsertField(string.Format(@"MERGEFIELD ""{0}""", mergeField));
                        builder.InsertBreak(BreakType.LineBreak);
                        mergeField = "PolicyInfo.Policy" + policyCode + "Name";
                        builder.InsertField(string.Format(@"MERGEFIELD ""{0}""", mergeField));
                        builder.InsertBreak(BreakType.LineBreak);
                        mergeField = "PolicyInfo.Policy" + policyCode + "IsEligible";
                        builder.InsertField(string.Format(@"MERGEFIELD ""{0}""", mergeField));
                        builder.InsertBreak(BreakType.LineBreak);
                        mergeField = "PolicyInfo.Policy" + policyCode + "IneligibleReason";
                        builder.InsertField(string.Format(@"MERGEFIELD ""{0}""", mergeField));
                        builder.InsertBreak(BreakType.LineBreak);
                        mergeField = "PolicyInfo.Policy" + policyCode + "IsPending";
                        builder.InsertField(string.Format(@"MERGEFIELD ""{0}""", mergeField));
                        builder.InsertBreak(BreakType.LineBreak);
                        mergeField = "PolicyInfo.Policy" + policyCode + "Determination";
                        builder.InsertField(string.Format(@"MERGEFIELD ""{0}""", mergeField));
                        builder.InsertBreak(BreakType.LineBreak);
                        mergeField = "PolicyInfo.Policy" + policyCode + "StartDate";
                        builder.InsertField(string.Format(@"MERGEFIELD ""{0}"" {1}", mergeField, @"\@ ""M/d/yyyy"""));
                        builder.InsertBreak(BreakType.LineBreak);
                        mergeField = "PolicyInfo.Policy" + policyCode + "EndDate";
                        builder.InsertField(string.Format(@"MERGEFIELD ""{0}"" {1}", mergeField, @"\@ ""M/d/yyyy"""));
                        builder.InsertBreak(BreakType.LineBreak);
                        mergeField = "PolicyInfo.Policy" + policyCode + "TimeUsedText";
                        builder.InsertField(string.Format(@"MERGEFIELD ""{0}""", mergeField));
                        builder.InsertBreak(BreakType.LineBreak);
                        mergeField = "PolicyInfo.Policy" + policyCode + "TimeRemainingText";
                        builder.InsertField(string.Format(@"MERGEFIELD ""{0}""", mergeField));
                        builder.InsertBreak(BreakType.LineBreak);
                        mergeField = "PolicyInfo.Policy" + policyCode + "ProjectedUsageText";
                        builder.InsertField(string.Format(@"MERGEFIELD ""{0}""", mergeField));
                        builder.InsertBreak(BreakType.LineBreak);
                    }
                }
                if (demandTypes != null)
                {
                    foreach (DemandType type in demandTypes)
                    {                        
                        builder.InsertField(string.Format(@"MERGEFIELD ""{0}""", "IS" + type.Code.ToUpper().Trim()));
                        builder.InsertBreak(BreakType.LineBreak);
                        builder.InsertField(string.Format(@"MERGEFIELD ""{0}""", type.Code.ToUpper().Trim() + "VALUE"));
                        builder.InsertBreak(BreakType.LineBreak);                        
                    }
                }
                if (caseAssigneeTypeCode != null)
                {
                    foreach (string code in caseAssigneeTypeCode)
                    {
                        string mergeField = "AssigneeInfo." + code + "FirstName";
                        builder.InsertField(string.Format(@"MERGEFIELD ""{0}""", mergeField));
                        builder.InsertBreak(BreakType.LineBreak);
                        mergeField = "AssigneeInfo." + code + "LastName";
                        builder.InsertField(string.Format(@"MERGEFIELD ""{0}""", mergeField));
                        builder.InsertBreak(BreakType.LineBreak);
                        mergeField = "AssigneeInfo." + code + "Email";
                        builder.InsertField(string.Format(@"MERGEFIELD ""{0}""", mergeField));
                        builder.InsertBreak(BreakType.LineBreak);
                        mergeField = "AssigneeInfo." + code + "WorkPhone";
                        builder.InsertField(string.Format(@"MERGEFIELD ""{0}""", mergeField));
                        builder.InsertBreak(BreakType.LineBreak);
                        mergeField = "AssigneeInfo." + code + "Fax";
                        builder.InsertField(string.Format(@"MERGEFIELD ""{0}""", mergeField));
                        builder.InsertBreak(BreakType.LineBreak);
                        mergeField = "AssigneeInfo." + code + "Title";
                        builder.InsertField(string.Format(@"MERGEFIELD ""{0}""", mergeField));
                        builder.InsertBreak(BreakType.LineBreak);
                        mergeField = "AssigneeInfo." + code + "Name";
                        builder.InsertField(string.Format(@"MERGEFIELD ""{0}""", mergeField));
                        builder.InsertBreak(BreakType.LineBreak);
                    }
                }
                doc.Save(s, SaveFormat.Docx);
                s.Position = 0;
                return s;
            }
        }

        private static List<string> BuildMergeFieldNames(List<string> currentMergeFieldNames, Type t, List<CustomField> customFields = null)
        {
            if (t.IsClass && !t.IsPrimitive && t != typeof(string))
            {
                PropertyInfo[] properties = t.GetProperties();
                foreach (PropertyInfo prop in properties)
                {
                    Type propertyType = prop.PropertyType;
                    if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(List<>))
                    {
                        /// Custom fields are handled differently and should not be put into the word doc
                        if (prop.Name == "CustomFields")
                            continue;

                        Type argument = propertyType.GetTypeInfo().GenericTypeArguments[0];
                        currentMergeFieldNames.Add(string.Format("TableStart:{0}", prop.Name));
                        currentMergeFieldNames.AddRange(BuildMergeFieldNames(new List<string>(), argument));
                        currentMergeFieldNames.Add(string.Format("TableEnd:{0}", prop.Name));
                    }
                    else
                    {
                        currentMergeFieldNames.Add(prop.Name);
                        if (prop.Name == "EmployerLogo" || prop.Name == "CustomerLogo" || prop.Name == "Logo")
                            continue;
                        List<string> childNames = BuildMergeFieldNames(new List<string>(), propertyType);
                        foreach (string s in childNames)
                        {
                            currentMergeFieldNames.Add(string.Format("{0}.{1}", prop.Name, s));
                        }
                        if (prop.Name == "Employee" && customFields != null)
                        {
                            List<string> employeeCustomFields = customFields
                                .Where(cf => cf.Target.HasFlag(Data.Enums.EntityTarget.Employee))
                                .Select(cf => string.Format("{0}.{1}", prop.Name, cf.Name))
                                .ToList();
                            currentMergeFieldNames.AddRange(employeeCustomFields);
                        }
                        else if (prop.Name == "Case" && customFields != null)
                        {
                            List<string> employeeCustomFields = customFields
                                .Where(cf => cf.Target.HasFlag(Data.Enums.EntityTarget.Case))
                                .Select(cf => string.Format("{0}.{1}", prop.Name, cf.Name))
                                .ToList();
                            currentMergeFieldNames.AddRange(employeeCustomFields);
                        }
                    }


                }
            }

            return currentMergeFieldNames;
        }
    }
}
