﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using Aspose.Pdf.Facades;
using AbsenceSoft.Common;
using Aspose.Pdf;
using Aspose.Pdf.Forms;

namespace AbsenceSoft.Rendering
{
    public static class Pdf
    {

        public static byte[] ConvertMsWordToPdf(byte[] currentDoc)
        {

            using (MemoryStream output = new MemoryStream())
            {
                using (MemoryStream input = new MemoryStream(currentDoc))
                {
                    Aspose.Words.Document loadedFromBytes = new Aspose.Words.Document(input);
                    loadedFromBytes.Save(output, Aspose.Words.SaveFormat.Pdf);
                }
                return output.ToArray();
            }

        }//ConvertHtmlToPdf
        /// <summary>
        /// <summary>
        /// Converts HTML (and CSS2 in the head or in-line) to a PDF document and returns the result. Also handles relative URLs
        /// and Image paths with the ones configured or appropriate for the site (hosted or physical file).
        /// </summary>
        /// <param name="html">The html to convert to a PDF document.</param>
        /// <returns>A Pdf document raw binary content as a byte array.</returns>
        public static byte[] ConvertHtmlToPdf(string html, Stream currentDoc = null, PageOrientation orientation = PageOrientation.Portrait)
        {
            int pages;
            return ConvertHtmlToPdf(html, out pages, currentDoc, orientation);
        }//ConvertHtmlToPdf        

        /// <summary>
        /// Converts HTML (and CSS2 in the head or in-line) to a PDF document and returns the result. Also handles relative URLs
        /// and Image paths with the ones configured or appropriate for the site (hosted or physical file).
        /// </summary>
        /// <param name="html">The html to convert to a PDF document.</param>
        /// <param name="pages">The pages.</param>
        /// <param name="orientation">The orientation.</param>
        /// <param name="size">The size.</param>
        /// <returns>
        /// A Pdf document raw binary content as a byte array.
        /// </returns>
        /// <exception cref="System.ArgumentException">Nothing provided to convert to PDF;html</exception>
        public static byte[] ConvertHtmlToPdf(string html, out int pages, Stream currentDoc = null, PageOrientation orientation = PageOrientation.Portrait)
        {
            if (string.IsNullOrWhiteSpace(html))
                throw new ArgumentException("Nothing provided to convert to PDF", "html");



            byte[] doc = null;
            string footerstring = "";
            using (MemoryStream output = new MemoryStream())
            {
                using (MemoryStream input = new MemoryStream())
                {
                    string htmlToLoad = html.StartsWith("<html>") ? html : string.Format("<html><body>{0}</body></html>", html);
                    Aspose.Words.Document asposeDoc = new Aspose.Words.Document();
                    if (currentDoc != null)
                    {
                        asposeDoc = new Aspose.Words.Document(currentDoc);
                        if (asposeDoc.FirstSection.HeadersFooters[Aspose.Words.HeaderFooterType.FooterPrimary] != null)
                        {
                            footerstring = asposeDoc.FirstSection.HeadersFooters[Aspose.Words.HeaderFooterType.FooterPrimary].GetText().Trim();
                        }
                        foreach (Aspose.Words.Section section in asposeDoc)
                        {
                            Aspose.Words.HeaderFooter header;
                            header = section.HeadersFooters[Aspose.Words.HeaderFooterType.HeaderPrimary];
                            if (header != null)
                            {
                                header.Remove();
                            }
                            section.Body.Remove();
                        }
                    }

                    Aspose.Words.DocumentBuilder builder = new Aspose.Words.DocumentBuilder(asposeDoc);
                    
                    if (orientation == PageOrientation.Landscape)
                    {
                        // Set page orientation
                        builder.PageSetup.Orientation = Aspose.Words.Orientation.Landscape;
                        builder.PageSetup.LeftMargin = 5;
                        builder.PageSetup.RightMargin = 10;
                    }              

                    builder.InsertHtml(htmlToLoad);
                    if (footerstring != string.Empty)
                    {
                        foreach (Aspose.Words.Section section in asposeDoc)
                        {
                            section.Body?.Range?.Replace(footerstring, "", false, false);
                        }
                    }
                    asposeDoc.Save(output, Aspose.Words.SaveFormat.Pdf);
                    pages = asposeDoc.PageCount;
                }

                doc = output.ToArray();
            } 
            return doc;
        }//ConvertHtmlToPdf

        /// <summary>
        /// Extracts a range of pages from a pdf document and returns a new pdf document.
        /// </summary>
        /// <returns>A byte array representing the resulting extracted pages merged into a pdf document.</returns>
        public static byte[] ExtractPages(byte[] sourcePDF, int startpage, int endpage)
        {
            try
            {
                Stream inputStream = new MemoryStream(sourcePDF);
                PdfFileEditor pdfEditor = new PdfFileEditor();
                var outputStream = new MemoryStream();
                if (pdfEditor.Extract(inputStream, startpage, endpage, outputStream))
                    return outputStream.ToByteArray();
            }
            catch(Exception ex)
            {
                Log.Error(ex);
            }

            return null;
        }

        /// <summary>
        /// Takes an array of PDF document byte arrays and joins them together into a single PDF document. All documents
        /// passed in the array MUST be valid PDF documents or this method will more than likely throw an exception.
        /// </summary>
        /// <param name="documents">An array of PDF document binary as byte arrays.</param>
        /// <returns>A byte array representing the resulting combined PDF document joining each of the passed in PDF documents.</returns>
        public static byte[] MergePdfDocuments(params byte[][] documents)
        {
            if (documents != null && documents.Length > 0)
            {
                try
                {
                    List<Stream> inputStreams = new List<Stream>();
                    for (var i = 0; i < documents.Length; i++)
                        inputStreams.Add(new MemoryStream(documents[i]));

                    var outputStream = new MemoryStream();
                    PdfFileEditor pdfEditor = new PdfFileEditor();
                    if (pdfEditor.Concatenate(inputStreams.ToArray(), outputStream))
                        return outputStream.ToByteArray();
                }
                catch(Exception ex)
                {
                    Log.Error(ex);
                }
            }

            return null;
        }


        /// <summary>
        /// Takes an array of PDF document byte arrays and joins them together into a single PDF document. All documents
        /// passed in the array MUST be valid PDF documents or this method will more than likely throw an exception.
        /// </summary>
        /// <param name="documents">An array of PDF document binary as byte arrays.</param>
        /// <returns>A byte array representing the resulting combined PDF document joining each of the passed in PDF documents.</returns>
        public static byte[] MergePdfDocumentsWithPageNumbers(params byte[][] documents)
        {
            if (documents != null && documents.Any())
            {
                try
                {
                    var mergedDocumentsStream = MergePdfDocuments(documents).ToStream();
                    var outputStream = new MemoryStream();
                    PdfFileStamp pdfStamp = new PdfFileStamp();
                    pdfStamp.BindPdf(mergedDocumentsStream);
                    pdfStamp.AddPageNumber("#");
                    pdfStamp.Save(outputStream);
                    return outputStream.ToByteArray();
                }
                catch(Exception ex)
                {
                    Log.Error(ex);
                }
            }

            return null;
        }

        /// <summary>
        /// Adds metadata to a PDF
        /// </summary>
        /// <param name="sourceDocument"></param>
        /// <param name="metadataToAdd"></param>
        /// <returns></returns>
        public static byte[] AddPdfMetadata(byte[] sourceDocument, Dictionary<string, string> metadataToAdd)
        {
            if (metadataToAdd == null || metadataToAdd.Count == 0)
            {

                try
                {
                    Document pdfDocument = new Document(sourceDocument.ToStream());
                    pdfDocument.Metadata.RegisterNamespaceUri("xmp", "http:// Ns.adobe.com/xap/1.0/");
                    foreach (var i in metadataToAdd)
                    {
                        pdfDocument.Info[i.Key] = i.Value;
                    }

                    var outputStream = new MemoryStream();
                    pdfDocument.Save(outputStream);
                    return outputStream.ToByteArray();
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }
            }

            return sourceDocument;
        }

        /// <summary>
        /// Stamps the PDF.
        /// </summary>
        /// <param name="sourceDocument">The source document PDF to stamp.</param>
        /// <param name="stamps">The stamps to apply.</param>
        /// <returns>A stamped PDF file as raw binary.</returns>
        /// <remarks>http://stackoverflow.com/questions/15165357/centered-multiline-text-using-itextsharp-columntext</remarks>
        public static byte[] StampPdf(byte[] sourceDocument, IEnumerable<PdfStamp> stamps)
        {
            if (stamps != null && stamps.Any())
            {
                try
                {
                    PdfFileStamp fileStamp = new PdfFileStamp();
                    fileStamp.BindPdf(sourceDocument.ToStream());

                    foreach (var s in stamps)
                    {
                        // Get the text and make sure it's not empty, null and is valid to stamp
                        string text = string.Format(s.Format ?? "{0}", s.Value);
                        // If it's a boolean value, then we need to ensure we represent the "checkbox" appropriately unless explicitly specified otherwise
                        if (s.Value is bool && string.IsNullOrWhiteSpace(s.Format))
                            text = (bool)s.Value == true ? "X" : null;
                        if (string.IsNullOrWhiteSpace(text))
                            continue;

                        var stamp = new Aspose.Pdf.Facades.Stamp();
                        stamp.BindLogo(new FormattedText(text, System.Drawing.Color.Blue, System.Drawing.Color.White, Aspose.Pdf.Facades.FontStyle.Helvetica, EncodingType.Winansi, false, s.FontSize));

                        stamp.SetOrigin(s.X, s.Y);

                        var width = s.MaxWidth.HasValue ? s.X + s.MaxWidth.Value : fileStamp.Document.PageInfo.Width - (text.Length * s.FontSize);
                        var height = s.MaxHeight.HasValue ? s.Y + s.MaxHeight.Value : fileStamp.Document.PageInfo.Height - (text.Split('\n').Length * s.FontSize);

                        stamp.SetImageSize((float)width, (float)height);

                        stamp.IsBackground = false;

                        fileStamp.AddStamp(stamp);
                    }

                    var outputStream = new MemoryStream();
                    fileStamp.Save(outputStream);
                    return outputStream.ToByteArray();
                }
                catch(Exception ex)
                {
                    Log.Error(ex);
                }
            }

            return sourceDocument;
        }

        /// <summary>
        /// Converts the specified inches to points for use in coordinates within a PDF.
        /// </summary>
        /// <param name="inches">The inches.</param>
        /// <returns></returns>
        public static float Points(this float inches)
        {
            return inches * 72;
        }

        /// <summary>
        /// Fill the PDF with given data
        /// </summary>
        /// <param name="sourceDocument">Memory stream array of the PDF</param>
        /// <param name="pdfFields">Dictionary of key value pair</param>
        /// <returns>output memory stream array</returns>
        public static byte[] FillPdf(byte[] sourceDocument, Dictionary<string, string> pdfFields)
        {
            if (pdfFields != null && pdfFields.Any())
            {
                try
                {
                    Aspose.Pdf.Facades.Form form = new Aspose.Pdf.Facades.Form(sourceDocument.ToStream());
                    var fields = form.Document.Form.Fields;

                    foreach (var f in pdfFields)
                    {
                        var field = fields.FirstOrDefault(e => e.FullName == f.Key);
                        if (field != null)
                        {
                            if (field is CheckboxField)
                            {
                                var value = f.Value.ToUpper();
                                ((CheckboxField)field).Checked = (value == "TRUE" || value == "T" || value == "YES");
                            }
                            else if (field is RadioButtonField)
                            {
                                var value = f.Value.ToUpper();
                                if (field.Value == value)
                                    ((RadioButtonField)field).Selected = 0;
                            }
                            else if (field is ComboBoxField)
                            {
                                var cf = field as ComboBoxField;
                                var op = cf.Options.ToList();
                                var values = f.Value.Split(',');
                                var selectedIndexes = new List<int>();
                                for (var i = 0; i < op.Count; i++)
                                {
                                    if (values.Contains(op[i].Value))
                                        selectedIndexes.Add(i + 1);
                                }
                                if (selectedIndexes.Any())
                                {
                                    if (cf.MultiSelect == true)
                                        cf.SelectedItems = selectedIndexes.ToArray();
                                    else
                                        cf.Selected = selectedIndexes[0];
                                }
                            }
                            else if (field is ListBoxField)
                            {
                                var lf = field as ListBoxField;
                                var op = lf.Options.ToList();
                                var values = f.Value.Split(',');
                                var selectedIndexes = new List<int>();
                                for (var i = 0; i < op.Count; i++)
                                {
                                    if (values.Contains(op[i].Value))
                                        selectedIndexes.Add(i + 1);
                                }
                                if (selectedIndexes.Any())
                                {
                                    if (lf.MultiSelect == true)
                                        lf.SelectedItems = selectedIndexes.ToArray();
                                    else
                                        lf.Selected = selectedIndexes[0];
                                }
                            }
                            else
                                field.Value = f.Value;

                            //change the text colour of form as existing
                            if (field is TextBoxField)
                                ((TextBoxField)field).DefaultAppearance.TextColor = System.Drawing.Color.Blue;
                        }
                    }


                    var outputStream = new MemoryStream();
                    form.Save(outputStream);
                    return outputStream.ToByteArray();
                }
                catch(Exception ex)
                {
                    Log.Error(ex);
                }
            }

            return sourceDocument;
        }
    }
}
