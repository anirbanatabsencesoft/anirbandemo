﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AbsenceSoft.Rendering.Templating
{
    internal sealed class PolicyLastDateOfTagDefinition : InlineTagDefinition
    {
        public PolicyLastDateOfTagDefinition() : base("PolicyLastDateOf") { }

        protected override IEnumerable<TagParameter> GetParameters()
        {
            return new TagParameter[]
            {
                new TagParameter("case") { IsRequired = true },
                new TagParameter("policyCode") { IsRequired = true },
                new TagParameter("adjudicationStatus") { IsRequired = true }
            };
        }

        /// <summary>
        /// Gets the text to output.
        /// </summary>
        /// <param name="writer">The writer to write the output to.</param>
        /// <param name="arguments">The arguments passed to the tag.</param>
        /// <param name="context">Extra data passed along with the context.</param>
        public override void GetText(TextWriter writer, Dictionary<string, object> arguments, Scope context)
        {
            Case myCase = arguments["case"] as Case;
            string policyCode = arguments["policyCode"] as string;
            string adjudicationStatus = arguments["adjudicationStatus"] as string;
            string returnValue = string.Empty;
            AdjudicationStatus? status = null;
            DateTime? returnDate = null;
            List<AppliedPolicyUsage> apu = null;
            if (!string.IsNullOrWhiteSpace(policyCode) && !string.IsNullOrWhiteSpace(adjudicationStatus))
            {
                status = (AdjudicationStatus)Enum.Parse(typeof(AdjudicationStatus), adjudicationStatus, true);
                
                if(policyCode.Equals("ALL"))
                    apu = myCase.Segments.SelectMany(s => s.AppliedPolicies).ToList().SelectMany(p => p.Usage).ToList();
                else
                    apu = myCase.Segments.SelectMany(s => s.AppliedPolicies).ToList().Where(p => p.Policy.Code == policyCode).SelectMany(p => p.Usage).ToList();

                if (apu.Where(d => d.Determination == status).Any())
                    returnDate = apu.Where(d => d.Determination == status).Max(d => d.DateUsed);
                if(returnDate.HasValue)
                    returnValue = returnDate.Value.ToShortDateString();
                writer.Write(returnValue);
            }
        }       
    }
}
