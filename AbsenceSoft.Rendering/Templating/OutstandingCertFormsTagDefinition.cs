﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AbsenceSoft.Rendering.Templating
{
    internal sealed class OutstandingCertFormsTagDefinition : InlineTagDefinition
    {
        public OutstandingCertFormsTagDefinition() : base("OutstandingCertForms") { }

        protected override IEnumerable<TagParameter> GetParameters()
        {
            return new TagParameter[]
            {
                new TagParameter("caseId") { IsRequired = true }                
            };
        }

        /// <summary>
        /// Gets the text to output.
        /// </summary>
        /// <param name="writer">The writer to write the output to.</param>
        /// <param name="arguments">The arguments passed to the tag.</param>
        /// <param name="context">Extra data passed along with the context.</param>
        public override void GetText(TextWriter writer, Dictionary<string, object> arguments, Scope context)
        {
            string caseId = arguments["caseId"] as string;
            StringBuilder sb = new StringBuilder();
            sb.Append("<p><table>");
            sb.Append("<tr><th>Name</th><th>Sent Date</th><th>Due Date</th>");
            if (!string.IsNullOrWhiteSpace(caseId))
            {
                List<Communication> comms = Communication.AsQueryable().Where(c => c.CaseId == caseId).ToList();
                foreach (Communication c in comms.Where(x => x.Paperwork.Any()))
                {
                    List<CommunicationPaperwork> cp = c.Paperwork;
                    foreach(CommunicationPaperwork p in cp.Where(y => y.Paperwork.RequiresReview == true))
                    {
                        if (p.Status == PaperworkReviewStatus.Pending || p.Status == PaperworkReviewStatus.NotApplicable)
                        {
                            sb.Append("<tr><td>");
                            sb.Append(p.Paperwork.Name);
                            sb.Append("</td><td>");
                            sb.Append(c.SentDate.HasValue ? c.SentDate.Value.ToShortDateString() : string.Empty);
                            sb.Append("</td><td>");
                            sb.Append(p.DueDate.HasValue ? p.DueDate.Value.ToShortDateString() : string.Empty);
                            sb.Append("</td></tr>");
                        }
                    }
                }
                sb.Append("</table></p>");
                
                writer.Write(sb.ToString());
            }
        }       
    }
}
