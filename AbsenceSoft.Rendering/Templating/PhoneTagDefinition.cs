﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Rendering.Templating
{
    /// <summary>
    /// Defines a tag that outputs a formatted phone number value.
    /// </summary>
    internal sealed class PhoneTagDefinition : InlineTagDefinition
    {
        /// <summary>
        /// Initializes a new instance of an PhoneTagDefinition.
        /// </summary>
        public PhoneTagDefinition() : base("phone") { }

        protected override IEnumerable<TagParameter> GetParameters()
        {
            return new TagParameter[] { new TagParameter("number") };
        }

        /// <summary>
        /// Gets the text to output.
        /// </summary>
        /// <param name="writer">The writer to write the output to.</param>
        /// <param name="arguments">The arguments passed to the tag.</param>
        /// <param name="context">Extra data passed along with the context.</param>
        public override void GetText(TextWriter writer, Dictionary<string, object> arguments, Scope context)
        {
            object val = arguments["number"];
            if (val != null)
                writer.Write(val.ToString().FormatPhone());
        }
    }
}
