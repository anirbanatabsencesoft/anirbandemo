﻿using System.Collections.Generic;
using System.IO;

namespace AbsenceSoft.Rendering.Templating
{
    internal class FormFeedTagDefinition : InlineTagDefinition
    {
        public FormFeedTagDefinition() : base("formfeed") { }

        /// <summary>
        /// Gets the text to output.
        /// </summary>
        /// <param name="writer">The writer to write the output to.</param>
        /// <param name="arguments">The arguments passed to the tag.</param>
        /// <param name="context">Extra data passed along with the context.</param>
        public override void GetText(TextWriter writer, Dictionary<string, object> arguments, Scope context)
        {
            writer.Write((char)0xC);
        }
    }//
}
