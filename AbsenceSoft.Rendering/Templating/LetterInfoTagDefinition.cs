﻿using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Rendering.Templating
{
    /// <summary>
    /// Gives you letter dates :-)
    /// </summary>
    internal sealed class LetterInfoTagDefinition : InlineTagDefinition
    {
        private const string Info_SentDate = "sentdate";
        private const string Info_Name = "name";
        private const string Info_Subject = "subject";
        private const string Info_CommunicationType = "communicationtype";
        private const string Info_FirstPaperworkDueDate = "firstpaperworkduedate";
        private const string Info_LastIncompletePaperworkDueDate = "lastincompletepaperworkduedate";

        /// <summary>
        /// Initializes a new instance of the <see cref="LetterInfoTagDefinition"/> class.
        /// </summary>
        public LetterInfoTagDefinition() : base("letter") { }

        /// <summary>
        /// Specifies which parameters are passed to the tag.
        /// </summary>
        /// <returns>
        /// The tag parameters, letters, code, info and format.
        /// </returns>
        protected override IEnumerable<TagParameter> GetParameters()
        {
            return new TagParameter[]
            {
                new TagParameter("letters"),
                new TagParameter("code"),
                new TagParameter("info")
                {
                    IsRequired = false,
                    DefaultValue = "Name"
                },
                new TagParameter("format")
                {
                    IsRequired = false,
                    DefaultValue = "{0}"
                }
            };
        }

        /// <summary>
        /// Gets the text to output.
        /// </summary>
        /// <param name="writer">The writer to write the output to.</param>
        /// <param name="arguments">The arguments passed to the tag.</param>
        /// <param name="context">Extra data passed along with the context.</param>
        public override void GetText(TextWriter writer, Dictionary<string, object> arguments, Scope context)
        {
            try
            {
                // Get our custom fields (could be employee or case)
                IEnumerable<Communication> letters = arguments["letters"] as IEnumerable<Communication>;

                // Validate we have a fields collection, it contains at least 1 field and we have a field name
                if (letters == null || !letters.Any() || arguments["code"] == null)
                    return;

                // Get the communication code we are looking for
                string code = arguments["code"].ToString();

                // Get the communication in question from the list of communications by code
                var comm = letters.FirstOrDefault(f => (f.Template ?? "").ToLowerInvariant() == code.ToLowerInvariant());

                // Ensure the field name we're looking for exists, otherwise, noop and also ensure it doesn't have a null/empty value
                if (comm == null)
                    return;

                string format = arguments["format"] == null ? "{0}" : arguments["format"].ToString();
                if (string.IsNullOrWhiteSpace(format))
                    format = "{0}";

                object val = null;
                string info = !arguments.ContainsKey("info") || arguments["info"] == null ? "name" : arguments["info"].ToString().ToLowerInvariant();
                switch (info)
                {
                    case Info_SentDate:
                        val = comm.SentDate;
                        if (format.Contains("{0}"))
                            format = format.Replace("{0}", "{0:MM/dd/yyyy}");
                        break;
                    case Info_Name:
                        val = comm.Name;
                        break;
                    case Info_Subject:
                        val = comm.Subject;
                        break;
                    case Info_CommunicationType:
                        val = comm.CommunicationType.ToString().SplitCamelCaseString();
                        break;
                    case Info_FirstPaperworkDueDate:
                        val = comm.FirstPaperworkDueDate;
                        if (format.Contains("{0}"))
                            format = format.Replace("{0}", "{0:MM/dd/yyyy}");
                        break;
                    case Info_LastIncompletePaperworkDueDate:
                        val = comm.LastIncompletePaperworkDueDate;
                        if (format.Contains("{0}"))
                            format = format.Replace("{0}", "{0:MM/dd/yyyy}");
                        break;
                    default:
                        return;
                }

                writer.Write(format, val);
            }
            catch (Exception ex)
            {
                Log.Error("Error formatting letters token in template", ex);
            }
        }
    }
}
