﻿using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Rendering.Templating
{
    /// <summary>
    /// Custom fields 'n' stuff
    /// </summary>
    internal sealed class CustomFieldTagDefinition : InlineTagDefinition
    {
        /// <summary>
        /// Initializes a new instance of an CustomFieldTagDefinition.
        /// </summary>
        public CustomFieldTagDefinition() : base("custom") { }

        /// <summary>
        /// Specifies which parameters are passed to the tag.
        /// </summary>
        /// <returns>
        /// The tag parameters.
        /// </returns>
        protected override IEnumerable<TagParameter> GetParameters()
        {
            return new TagParameter[]
            {
                new TagParameter("fields"),
                new TagParameter("name"),
                new TagParameter("format")
                {
                    IsRequired = false,
                    DefaultValue = "{0}"
                }
            };
        }

        /// <summary>
        /// Gets the text to output.
        /// </summary>
        /// <param name="writer">The writer to write the output to.</param>
        /// <param name="arguments">The arguments passed to the tag.</param>
        /// <param name="context">Extra data passed along with the context.</param>
        public override void GetText(TextWriter writer, Dictionary<string, object> arguments, Scope context)
        {
            try
            {
                // Get our custom fields (could be employee or case)
                IEnumerable<CustomField> fields = arguments["fields"] as IEnumerable<CustomField>;

                // Validate we have a fields collection, it contains at least 1 field and we have a field name
                if (fields == null || !fields.Any() || arguments["name"] == null)
                    return;

                // Get the field name we are looking for
                string name = arguments["name"].ToString();

                // Get the field in question from the list of fields by name
                var field = fields.FirstOrDefault(f => f.Name == name);

                // Ensure the field name we're looking for exists, otherwise, noop and also ensure it doesn't have a null/empty value
                if (field == null || field.SelectedValue == null || string.IsNullOrWhiteSpace(field.SelectedValue.ToString()))
                    return;

                string format = !arguments.ContainsKey("format") || arguments["format"] == null ? "{0}" : arguments["format"].ToString();
                if (string.IsNullOrWhiteSpace(format))
                    format = "{0}";

                // Clean up our format as more predictable depending on the type of value we're dealing with
                if (format.Contains("{0}"))
                    switch (field.DataType)
                    {
                        case CustomFieldType.Number:
                            if (field.SelectedValue.ToString().Contains("."))
                                format = format.Replace("{0}", "{0:N2}");
                            else
                                format = format.Replace("{0}", "{0:N0}");
                            break;
                        case CustomFieldType.Flag:
                            field.SelectedValue = field.SelectedValue;
                            if (field.SelectedValue == (object)true)
                                format = format.Replace("{0}", "Yes");
                            else
                                format = format.Replace("{0}", "No");
                            break;
                        case CustomFieldType.Date:
                            format = format.Replace("{0}", "{0:MM/dd/yyyy}");
                            break;
                    }

                writer.Write(format, field.SelectedValue);
            }
            catch (Exception ex)
            {
                Log.Error("Error formatting custom field token in template", ex);
            }
        }
    }
}
