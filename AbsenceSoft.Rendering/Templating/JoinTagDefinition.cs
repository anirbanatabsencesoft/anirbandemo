﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Rendering.Templating
{
    internal class JoinTagDefinition : InlineTagDefinition
    {
        public JoinTagDefinition() : base("join")
        {
        }

        protected override IEnumerable<TagParameter> GetParameters()
        {
            return new TagParameter[] { new TagParameter("collection") };
        }

        public override void GetText(TextWriter writer, Dictionary<string, object> arguments, Scope context)
        {
            IEnumerable collection = arguments["collection"] as IEnumerable;
            if (collection != null)
            {
                string joined = string.Join(", ", collection.Cast<object>().Select(o => o == null ? "" : o.ToString()));
                writer.Write(joined);
            }
        }
    }
}
