﻿using System;
using System.Text.RegularExpressions;

namespace AbsenceSoft.Rendering.Templating
{
    /// <summary>
    /// Provides utility methods that require regular expressions.
    /// </summary>
    internal static class RegexHelper
    {
        internal const string Key = @"[_\w][_\w\d]*";
        internal const string CompoundKey = Key + @"(\." + Key + ")*";
        internal const string QuotedKey = @"(_""[_\w\d\s'\.\@\!\#\$\%\^\&\*\(\)\-\=\+\~\`\[\]\{\}\:\;\,\<\>\?\/]*"")";
        internal const string ConditionalCompoundQuotedKey = @"((" + CompoundKey + @")|(" + QuotedKey + "))";

        /// <summary>
        /// Determines whether the given name is a legal identifier.
        /// </summary>
        /// <param name="name">The name to check.</param>
        /// <returns>True if the name is a legal identifier; otherwise, false.</returns>
        public static bool IsValidIdentifier(string name)
        {
            if (name == null)
            {
                return false;
            }
            Regex regex = new Regex("^" + Key + "$");
            return regex.IsMatch(name);
        }
    }
}
