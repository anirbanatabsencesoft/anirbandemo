﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AbsenceSoft.Rendering.Templating
{
    internal sealed class DateTagDefinition : InlineTagDefinition
    {
        public DateTagDefinition() : base("date") { }

        protected override IEnumerable<TagParameter> GetParameters()
        {
            return new TagParameter[]
            {
                new TagParameter("numDaysToAdd") { IsRequired = false, DefaultValue = 0.0 }                
            };
        }

        /// <summary>
        /// Gets the text to output.
        /// </summary>
        /// <param name="writer">The writer to write the output to.</param>
        /// <param name="arguments">The arguments passed to the tag.</param>
        /// <param name="context">Extra data passed along with the context.</param>
        public override void GetText(TextWriter writer, Dictionary<string, object> arguments, Scope context)
        {
            object numDays = arguments["numDaysToAdd"];
            string returnValue = string.Empty;
            double days = 0.0;
            if (numDays != null)
                days = Convert.ToDouble(numDays);             
            returnValue = DateTime.UtcNow.AddDays(days).ToShortDateString();
            writer.Write(returnValue);
        }
    }
}
