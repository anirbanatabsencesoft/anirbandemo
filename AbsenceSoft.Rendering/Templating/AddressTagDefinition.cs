﻿using AbsenceSoft.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Rendering.Templating
{
    internal sealed class AddressTagDefinition : InlineTagDefinition
    {
        /// <summary>
        /// Initializes a new instance of an PhoneTagDefinition.
        /// </summary>
        public AddressTagDefinition() : base("address") { }

        protected override IEnumerable<TagParameter> GetParameters()
        {
            return new TagParameter[] { new TagParameter("address") };
        }

        /// <summary>
        /// Gets the text to output.
        /// </summary>
        /// <param name="writer">The writer to write the output to.</param>
        /// <param name="arguments">The arguments passed to the tag.</param>
        /// <param name="context">Extra data passed along with the context.</param>
        public override void GetText(TextWriter writer, Dictionary<string, object> arguments, Scope context)
        {
            Address val = arguments["address"] as Address;
            // If no address, bail out
            if (val == null)
                return;
            // If address is blank or empty, bail out
            if (string.IsNullOrWhiteSpace(val.Address1) && string.IsNullOrWhiteSpace(val.City) && string.IsNullOrWhiteSpace(val.State) && string.IsNullOrWhiteSpace(val.PostalCode))
                return;

            if (!string.IsNullOrWhiteSpace(val.Address1))
                writer.Write(val.Address1);
            if (!string.IsNullOrWhiteSpace(val.Address2))
                writer.Write("{0}{1}", !string.IsNullOrWhiteSpace(val.Address1) ? "<br/>" : "", val.Address2);
            if (!string.IsNullOrWhiteSpace(val.Address1) || !string.IsNullOrWhiteSpace(val.Address2))
                writer.Write("<br/>");
            if (!string.IsNullOrWhiteSpace(val.City))
                writer.Write(val.City);
            if (!string.IsNullOrWhiteSpace(val.State))
                writer.Write("{0}{1}", !string.IsNullOrWhiteSpace(val.City) ? ", " : "", val.State);
            if (!string.IsNullOrWhiteSpace(val.PostalCode))
                writer.Write("{0}{1}", !string.IsNullOrWhiteSpace(val.City) && string.IsNullOrWhiteSpace(val.State) ? ", " : 
                    !string.IsNullOrWhiteSpace(val.State) ? " " : "", val.PostalCode);
            if (!string.IsNullOrWhiteSpace(val.City) || !string.IsNullOrWhiteSpace(val.State) || !string.IsNullOrWhiteSpace(val.PostalCode))
                writer.Write("<br/>");
            if (!string.IsNullOrWhiteSpace(val.Country) && val.Country != "US" && val.Country != "USA")
                writer.Write("{0}<br/>", val.Country);
        }
    }
}
