﻿using AbsenceSoft.Common.Properties;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AbsenceSoft.Rendering.Templating
{
    internal sealed class CompanyLogoTagDefinition : InlineTagDefinition
    {
        public CompanyLogoTagDefinition() : base("logo") { }

        protected override IEnumerable<TagParameter> GetParameters()
        {
            return new TagParameter[]
            {
                new TagParameter("customerId") { IsRequired = true },
                new TagParameter("fileId") { IsRequired = true }
            };
        }

        /// <summary>
        /// Gets the text to output.
        /// </summary>
        /// <param name="writer">The writer to write the output to.</param>
        /// <param name="arguments">The arguments passed to the tag.</param>
        /// <param name="context">Extra data passed along with the context.</param>
        public override void GetText(TextWriter writer, Dictionary<string, object> arguments, Scope context)
        {
            string customerId = arguments["customerId"] as string;
            string fileId = arguments["fileId"] as string;
            if (!string.IsNullOrWhiteSpace(customerId) && !string.IsNullOrWhiteSpace(fileId))
            {
                Document doc = Document.GetById(fileId);
                using (IAmazonS3 client = S3Client())
                {
                    GetObjectRequest request = new GetObjectRequest();
                    request.BucketName = Settings.Default.S3BucketName_Files;
                    request.Key = doc.Locator;
                    var response = client.GetObject(request);
                    using (Stream amazonS3Stream = response.ResponseStream)
                        using (MemoryStream ms = new MemoryStream())
                        {
                            amazonS3Stream.CopyTo(ms);
                            string base64 = Convert.ToBase64String(ms.ToArray());
                            writer.Write(string.Format("data:image/jpg;base64,{0}", base64));
                        }
                }
            }
        }

        private IAmazonS3 S3Client()
        {
            AmazonS3Config config = new AmazonS3Config()
            {
                //RegionEndpoint = RegionEndpoint.GetBySystemName(Settings.Default.AWSRegion),
                ServiceURL = "https://s3.amazonaws.com/",
                ProxyHost = null
            };
            return AWSClientFactory.CreateAmazonS3Client(
                Settings.Default.AWSAccessId,
                Settings.Default.AWSSecretKey,
                config);
        }
    }
}
