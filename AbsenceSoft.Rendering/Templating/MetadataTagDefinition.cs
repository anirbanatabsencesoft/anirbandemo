﻿using AbsenceSoft.Common;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Rendering.Templating
{
    internal sealed class MetadataTagDefinition : InlineTagDefinition
    {
        /// <summary>
        /// Initializes a new instance of an MetadataTagDefinition.
        /// </summary>
        public MetadataTagDefinition() : base("meta") { }

        /// <summary>
        /// Specifies which parameters are passed to the tag.
        /// </summary>
        /// <returns>
        /// The tag parameters.
        /// </returns>
        protected override IEnumerable<TagParameter> GetParameters()
        {
            return new TagParameter[]
            {
                new TagParameter("doc"),
                new TagParameter("name"),
                new TagParameter("format")
                {
                    IsRequired = false,
                    DefaultValue = "{0}"
                }
            };
        }

        /// <summary>
        /// Gets the text to output.
        /// </summary>
        /// <param name="writer">The writer to write the output to.</param>
        /// <param name="arguments">The arguments passed to the tag.</param>
        /// <param name="context">Extra data passed along with the context.</param>
        public override void GetText(TextWriter writer, Dictionary<string, object> arguments, Scope context)
        {
            try
            {
                // Get our custom fields (could be employee or case)
                BsonDocument doc = arguments["doc"] as BsonDocument;

                // Validate we have a fields collection, it contains at least 1 field and we have a field name
                if (doc == null || !doc.Any() || arguments["doc"] == null)
                    return;

                // Get the field name we are looking for
                string name = arguments["name"].ToString();

                // Get the field in question from the list of fields by name
                var val = doc.GetRawValue<object>(name);

                // Ensure the field name we're looking for exists, otherwise, noop and also ensure it doesn't have a null/empty value
                if (val == null)
                    return;

                string format = !arguments.ContainsKey("format") || arguments["format"] == null ? "{0}" : arguments["format"].ToString();
                if (string.IsNullOrWhiteSpace(format))
                    format = "{0}";

                writer.Write(format, val);
            }
            catch (Exception ex)
            {
                Log.Error("Error formatting meta token in template", ex);
            }
        }
    }
}
