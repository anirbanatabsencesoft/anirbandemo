﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Rendering.Templating
{
    /// <summary>
    /// Defines a tag that outputs a prettified Enum value.
    /// </summary>
    internal sealed class EnumTagDefinition : InlineTagDefinition
    {
        /// <summary>
        /// Initializes a new instance of an EnumTagDefinition.
        /// </summary>
        public EnumTagDefinition()
            : base("enum")
        {
        }

        protected override IEnumerable<TagParameter> GetParameters()
        {
            return new TagParameter[] { new TagParameter("enumeration") };
        }

        /// <summary>
        /// Gets the text to output.
        /// </summary>
        /// <param name="writer">The writer to write the output to.</param>
        /// <param name="arguments">The arguments passed to the tag.</param>
        /// <param name="context">Extra data passed along with the context.</param>
        public override void GetText(TextWriter writer, Dictionary<string, object> arguments, Scope context)
        {
            object val = arguments["enumeration"];
            if (val != null && val.GetType().IsEnum)
                writer.Write(Enum.GetName(val.GetType(), val).SplitCamelCaseString());
        }
    }
}
