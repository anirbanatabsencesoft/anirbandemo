﻿using System;

namespace AbsenceSoft.Rendering.Templating
{
    /// <summary>
    /// Holds the value that a context variable is set to.
    /// </summary>
    internal class ValueRequestEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets the value being set.
        /// </summary>
        public object Value { get; set; }
    }
}
