﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AbsenceSoft.Rendering.Templating
{
    internal sealed class ContentImageTagDefinition : InlineTagDefinition
    {
        public ContentImageTagDefinition() : base("img") { }

        protected override IEnumerable<TagParameter> GetParameters()
        {
            return new TagParameter[]
            {
                new TagParameter("imgName") { IsRequired = false, DefaultValue = null }
            };
        }

        /// <summary>
        /// Gets the text to output.
        /// </summary>
        /// <param name="writer">The writer to write the output to.</param>
        /// <param name="arguments">The arguments passed to the tag.</param>
        /// <param name="context">Extra data passed along with the context.</param>
        public override void GetText(TextWriter writer, Dictionary<string, object> arguments, Scope context)
        {
            string imageName = arguments["imgName"] as string;

            var root = GetRootPath();
            var uri = string.Concat(root, "/Content/Images/", imageName ?? "");
            writer.Write(uri);
        }

        private string GetRootPath()
        {
            HttpContext context = HttpContext.Current;
            if (context != null && context.Request != null)
            {
                Uri u = context.Request.Url;
                string port = u.Port != 443 && u.Port != 80 ? string.Concat(":", u.Port) : "";
                return string.Concat(u.Scheme, "://", u.Host, port);
            }
            // Default if we're not in an actual HttpContext (which may happen for unit tests, etc.)
            return "https://absencetracker.com";
        }
    }
}
