﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Rendering.Templating.MailMerge
{
    public class CustomFieldMergeData
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
