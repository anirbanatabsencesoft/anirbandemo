﻿using AbsenceSoft.Data.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Rendering.Templating.MailMerge
{
    public class EmployerMergeData:CustomerMergeData
    {
        public EmployerMergeData()
        {
            this.Address = new AddressMergeData();
        }

        internal void CopyEmployerData(Employer employer)
        {
            if (employer == null)
                return;

            Name = employer.Name;
            SelfServiceURL = employer.Url;
            Email = employer.Contact.Email;
            Phone = employer.Contact.WorkPhone.FormatPhone();
            Fax = employer.Contact.Fax.FormatPhone();
            Address.CopyAddressData(employer.Contact.Address);
            ReferenceCode = employer.ReferenceCode;
        }

        public string SelfServiceURL { get; set; }
        public string ServiceType { get; set; }
        public string ReferenceCode { get; set; }

        

        
    }
}
