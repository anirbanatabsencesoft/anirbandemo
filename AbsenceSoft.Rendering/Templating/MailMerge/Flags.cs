﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Rendering.Templating.MailMerge
{
    /// <summary>
    /// A case insenstive set of strings
    /// </summary>
    public class Flags : HashSet<string>
    {
        public Flags() : base(StringComparer.InvariantCultureIgnoreCase)
        {

        }
        public Flags(IEnumerable<string> collection) : base(collection, StringComparer.InvariantCultureIgnoreCase)
        {
        }

        public Flags AddRange(IEnumerable<string> items)
        {
            foreach (var item in items)
            {
                Add(item);
            }
            return this;
        }
    }
}
