﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Rendering.Templating.MailMerge
{
    /// <summary>
    ///  Holds data for an employee when doing a mail merge
    /// </summary>
    public class EmployeeMergeData
    {
        public EmployeeMergeData()
        {
            AlternateAddress = new AddressMergeData();
            PermanentAddress = new AddressMergeData();
            OfficeLocationAddress = new AddressMergeData();
            WorkSchedule = new WorkScheduleMergeData();
            CustomFields = new List<CustomFieldMergeData>();
        }

        public void CopyEmployeeData(Employee e, Case c)
        {
            if (e == null)
                return;
            
         
            CopyBaseEmployeeInfo(e);
            CopyJobInfo(e);
           
            CalculateMonthsWorked(e);

            if (c != null)
            {
                CalculatePriorHours(e, c.CreatedDate);
                CalculateWorkedPerWeek(e, c.CreatedDate);
            }

            Schedule workSchedule = e.WorkSchedules.OrderByDescending(s => s.StartDate).FirstOrDefault();
            if (workSchedule != null)
                WorkSchedule.CopyScheduleData(workSchedule);

            if (e.CustomFields != null)
            {
                CustomFields = e.CustomFields.Select(s => new CustomFieldMergeData()
                {
                    Name = s.Name,
                    Value = s.SelectedValueText
                }).ToList();
            }
            //Refresh the Employee Object , there could be scenario where Case copy of Employee doesn't 
            //have the Address details , which may have been updated later .In those cases it will never 
            //print the Address . So we need to refresh the contact info . but can't do other stuff like schedule or other calculation
            // as that will create wrong impression for the case
            
            e = Employee.GetById(e.Id);
            CopyContactInfo(e, c);
        }

        private void CopyBaseEmployeeInfo(Employee e)
        {
            EmployeeNumber = e.EmployeeNumber;
            FirstName = e.FirstName;
            LastName = e.LastName;
            DateOfBirth = e.DoB;
            Gender = e.Gender;
            HireDate = e.HireDate;
            EmploymentStatus = e.Status;
            CostCenterCode = e.CostCenterCode;
            JobTitle = e.JobTitle;
            WorkState = e.WorkState;
            WorkCountry = e.WorkCountry;
            KeyEmployee = e.IsKeyEmployee ? "Yes" : "No";
            EmployeeClassCode = e.EmployeeClassCode;
            EmployeeClassName = e.EmployeeClassName;
        }

        private void CopyJobInfo(Employee e)
        {
            EmployeeJob job = e.GetCurrentJob();
            if (job != null)
            {
                JobName = job.JobName;
                JobDescription = job.Job.Description;
            }
           CopyOfficeLocationAddressInfo(e);
        }

        private void CopyContactInfo(Employee e, Case c)
        {
            CopyEmailInfo(e, c);
            CopyPhoneInfo(e, c);
            OfficeLocation = e.GetOfficeLocationDisplay();
            CopyAddressInfo(e, c);
        }

        private void CopyEmailInfo(Employee e, Case c)
        {
            PermanentEmail = e.Info.Email;

            if (c == null || c.Employee == null || c.Employee.Info == null)
                return;

            AlternateEmail = c.Employee.Info.AltEmail;
        }

        private void CopyPhoneInfo(Employee e, Case c)
        {
            WorkPhone = e.Info.WorkPhone.FormatPhone();
            PermanentHomePhone = e.Info.HomePhone.FormatPhone();
            PermanentCellPhone = e.Info.CellPhone.FormatPhone();

            if (c == null || c.Employee == null || c.Employee.Info == null)
                return;

            if (!string.IsNullOrEmpty(c.Employee.Info.AltPhone))
            {
                string formattedCasePhone = c.Employee.Info.AltPhone.FormatPhone();
                AlternateHomePhone = formattedCasePhone;
                AlternateCellPhone = formattedCasePhone;
            }
        }

        private void CopyAddressInfo(Employee e, Case c)
        {
           
            PermanentAddress.CopyAddressData(e.Info.Address);

            if (c == null || c.Employee == null || c.Employee.Info == null)
                return;

            AlternateAddress.CopyAddressData(c.Employee.Info.AltAddress);
        }

        private void CopyOfficeLocationAddressInfo(Employee employee)
        {
            EmployeeOrganization employeeOrganization = employee.GetOfficeLocation();
            if (employeeOrganization != null && employeeOrganization.Organization != null && employeeOrganization.Organization.Address != null)
            {
                OfficeLocationAddress.CopyAddressData(employeeOrganization.Organization.Address);
            }
        }

        private void CalculateMonthsWorked(Employee e)
        {
            if (e.ServiceDate == null)
                return;

            int compMonth = (DateTime.Today.Month + DateTime.Today.Year * 12) - (e.ServiceDate.Value.Month + e.ServiceDate.Value.Year * 12);
            double daysInEndMonth = (DateTime.Today - DateTime.Today.AddMonths(1)).Days;
            MonthsWorked = Math.Round(compMonth + (e.ServiceDate.Value.Day - DateTime.Today.Day) / daysInEndMonth, 1);
        }

        private void CalculatePriorHours(Employee e, DateTime caseCreatedDate)
        {
            PriorHours priorHours = e.PriorHours.Where(h => h.AsOf <= caseCreatedDate).OrderByDescending(h => h.AsOf).FirstOrDefault();
            if (priorHours != null)
                HoursWorked = priorHours.HoursWorked;
        }

        private void CalculateWorkedPerWeek(Employee e, DateTime caseCreatedDate)
        {
            MinutesWorkedPerWeek workedPerWeek = e.MinutesWorkedPerWeek.Where(h => h.AsOf <= caseCreatedDate).OrderByDescending(h => h.AsOf).FirstOrDefault();
            if (workedPerWeek != null)
                MinutesWorkedPerWeek = workedPerWeek.MinutesWorked;
        }

        
        public string EmployeeNumber { get; set; }
        public string Name
        {
            get { return string.Format("{0} {1}", FirstName, LastName); }
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public Gender? Gender { get; set; }


        public DateTime? HireDate { get; set; }
        public EmploymentStatus EmploymentStatus { get; set; }
        public string JobTitle { get; set; }
        public string JobName { get; set; }
        public string JobDescription { get; set; }
        public string CostCenterCode { get; set; }

        public string WorkState { get; set; }
        public string WorkCountry { get; set; }
        public double? HoursWorked { get; set; }
        private int? MinutesWorkedPerWeek { get; set; }
        public double? MonthsWorked { get; set; }
        public string KeyEmployee { get; set; }
        public string EmployeeClassCode { get; set; }
        public string EmployeeClassName { get; set; }

        public string Email
        {
            get
            {
                if (!string.IsNullOrEmpty(AlternateEmail))
                    return AlternateEmail;

                return PermanentEmail;
            }
        }
        public string PermanentEmail { get; set; }
        public string AlternateEmail { get; set; }

        public string WorkPhone { get; set; }

        public string PermanentHomePhone { get; set; }
        public string AlternateHomePhone { get; set; }
        public string HomePhone
        {
            get
            {
                if (!string.IsNullOrEmpty(AlternateHomePhone))
                    return AlternateHomePhone;

                return PermanentHomePhone;
            }
        }



        public string PermanentCellPhone { get; set; }
        public string AlternateCellPhone { get; set; }
        public string CellPhone
        {
            get
            {
                if (!string.IsNullOrEmpty(AlternateCellPhone))
                    return AlternateCellPhone;

                return PermanentCellPhone;
            }
        }

        public string OfficeLocation { get; set; }

        public AddressMergeData OfficeLocationAddress { get; set; }
        public AddressMergeData PermanentAddress { get; set; }
        public AddressMergeData AlternateAddress { get; set; }
        public AddressMergeData Address
        {
            get
            {
                if (!string.IsNullOrEmpty(AlternateAddress.ToString()))
                    return AlternateAddress;

                return PermanentAddress;
            }
        }

        public WorkScheduleMergeData WorkSchedule { get; set; }



        public List<CustomFieldMergeData> CustomFields { get; set; }

        public override string ToString()
        {
            return Name ?? "Unknown";
        }
    }
}
