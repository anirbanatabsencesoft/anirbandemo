﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Rendering.Templating.MailMerge
{
    public class CertificationMergeData
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Notes { get; set; }
        public string IntermittentType { get; set; }
        public int Occurances { get; set; }
        public int Frequency { get; set; }
        public Unit FrequencyType { get; set; }
        public double Duration { get; set; }
        public Unit DurationType { get; set; } 
    }
}
