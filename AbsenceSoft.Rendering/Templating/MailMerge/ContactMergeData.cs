﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Rendering.Templating.MailMerge
{
    public class ContactMergeData
    {
        public ContactMergeData()
        {
        }

        internal void CopyContactData(EmployerContact contact)
        {
            if (contact == null || contact.Contact == null)
                return;

            this.CopyContactData(contact.Contact);
            this.Type = contact.ContactTypeCode;
            this.TypeName = contact.ContactTypeName;
        }

        internal void CopyContactData(EmployeeContact contact)
        {
            if (contact == null || contact.Contact == null)
                return;

            this.CopyContactData(contact.Contact);
            this.Type = contact.ContactTypeCode;
            this.TypeName = contact.ContactTypeName;
        }


        internal void CopyContactData(Contact contact)
        {
            this.FirstName = contact.FirstName;
            this.LastName = contact.LastName;
            this.Email = contact.Email;
            this.WorkPhone = contact.WorkPhone;
            this.Fax = contact.Fax;
        }

        internal void CopyUserData(User user)
        {
            if (user == null)
                return;

            this.FirstName = user.FirstName;
            this.LastName = user.LastName;
            this.Title = user.Title;
            this.Email = user.Email;
            if (user.ContactInfo != null)
            {
                WorkPhone = user.ContactInfo.WorkPhone.FormatPhone();
                Fax = user.ContactInfo.Fax.FormatPhone();
            }
        }
        
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Type { get; set; }
        public string TypeName { get; set; }
        public string WorkPhone { get; set; }
        public string Fax { get; set; }
        public string Title { get; set; }
        public DateTime? DateOfBirth { get; set; }
        
        public string Name
        {
            get
            {
                return string.Format("{0} {1}", FirstName, LastName);
            }
        }

        public override string ToString()
        {
            return Name ?? "Unknown";
        }
    }
}
