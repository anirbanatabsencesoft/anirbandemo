﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Notes;

namespace AbsenceSoft.Rendering.Templating.MailMerge
{
    public class InteractiveProcessMergeData
    {
        public string Question { get; set; }
        public string Answer { get; set; }
        public string Note { get; set; }

        internal void CopyInteractiveProcessData(AccommodationInteractiveProcess.Step step, Dictionary<string, string> interactiveProcessNotes)
        {
            Question = step.Question.Question;
            Answer = (((step.Answer == null) ? step.SAnswer : (step.Answer.Value ? "Yes" : "No")) ?? "N/A");
            if (interactiveProcessNotes != null && interactiveProcessNotes.ContainsKey(step.QuestionId) && interactiveProcessNotes[step.QuestionId] != null)
            {
                Note = interactiveProcessNotes[step.QuestionId].Replace(Question, string.Empty).Trim();
                Note = Note.Substring(Note.IndexOf("-") + 1).Trim();
            }
        }
        public override string ToString()
        {
            return Note ?? "Unknown";
        }

    }
}
