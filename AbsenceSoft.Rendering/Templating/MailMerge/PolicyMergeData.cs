﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;

namespace AbsenceSoft.Rendering.Templating.MailMerge
{
    public class PolicyMergeData
    {
        public void CopyPolicyData(AppliedPolicy ap)
        {
            this.Code = ap.Policy.Code;
            this.Name = ap.Policy.Name;
            this.Type = ap.Policy.PolicyType;
            this.StartDate = ap.StartDate;
            this.EndDate = ap.EndDate;
            this.Status = ap.Status;
                     


            var appliedPolicyUsage = ap.Usage.FirstOrDefault();
            if (appliedPolicyUsage != null)
            {
                this.Determination = appliedPolicyUsage.SummaryStatus;
            }
        }

        public string Code { get; set; }
        public string Name { get; set; }
        public AdjudicationSummaryStatus Determination { get; set; }
        public PolicyType Type { get; set; }
        public string CaseType { get; set; }
        public EligibilityStatus Status { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string TimeUsedText { get; set; }
        public string TimeRemainingText { get; set; }
        public string ProjectedUsageText { get; set; }
        public string DeniedReason { get; set; }
        public string DeniedReasonCode { get; set; }
        public string IneligibleReason { get; set; }
        public bool HasStatePolicy { get; set; }
        public bool StateRuleWorkedHoursNotMet { get; set; }
        public bool StateRule50EmployeesIn75MileRadiusNotMet { get; set; }
        public bool StateRuleWorkedLengthofserviceNotMet { get; set; }
        public bool StateRuleRelationshipToEmployeeNotMet { get; set; }
        public bool StateRuleLeaveWithin12MonthsNotMet { get; set; }
        public double? HoursWorked { get; set; }
    }
}
