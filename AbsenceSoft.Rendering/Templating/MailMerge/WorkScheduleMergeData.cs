﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Rendering.Templating.MailMerge
{
    public class WorkScheduleMergeData
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public ScheduleType Schedule { get; set; }
        public string Summary { get;  set; }

        internal void CopyScheduleData(Schedule workSchedule)
        {
            this.StartDate = workSchedule.StartDate;
            this.EndDate = workSchedule.EndDate;
            this.Schedule = workSchedule.ScheduleType;
            this.Summary = workSchedule.ToString();
        }

        public override string ToString()
        {
            return Summary ?? "";
        }
    }
}
