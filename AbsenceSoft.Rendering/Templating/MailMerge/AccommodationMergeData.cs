﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Notes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AbsenceSoft.Rendering.Templating.MailMerge
{
    public class AccommodationMergeData
    {

        internal void CopyAccommodationData(Accommodation accomm, List<CaseNote> notes)
        {
            Code = accomm.Type.Code;
            Name = accomm.Type.Name;
            StartDate = accomm.StartDate;
            EndDate = accomm.EndDate;
            MaxApprovedDate = accomm.MaxApprovedThruDate;
            CreatedDate = accomm.CreatedDate;
            accommId = accomm.Id;
            accommodationInteractiveProcess = accomm.InteractiveProcess;
            Resolution = accomm.Resolution;
            Decision = accomm.Resolved ? "Yes" : "No";
            Duration = accomm.Duration.ToString();
            Implemented = accomm.Granted ? "Yes" : "No";
            this.notes = notes;
            //this.CopyInteractiveProcessData(accomm.InteractiveProcess, notes);
        }

        public List<InteractiveProcessMergeData> GetInteractiveProcessData()
        {
            List<InteractiveProcessMergeData> interactiveProcess = new List<InteractiveProcessMergeData>();
            if (accommodationInteractiveProcess != null && accommodationInteractiveProcess.Steps != null && accommodationInteractiveProcess.Steps.Count > 0)
            {                
                Dictionary<string, string> interactiveProcessNotes = null;
                if (notes != null)
                {
                    foreach (CaseNote caseNote in notes.OrderByDescending(note=>note.ModifiedDate))
                    {
                        Guid accommId = caseNote.Metadata.GetRawValue<Guid>("AccommId");
                        if (accommId != null && accommId == this.accommId)
                        {
                            interactiveProcessNotes = (interactiveProcessNotes == null ? new Dictionary<string, string>() : interactiveProcessNotes);
                            if (!interactiveProcessNotes.ContainsKey(caseNote.Metadata.GetRawValue<string>("AccommQuestionId")))
                                interactiveProcessNotes.Add(caseNote.Metadata.GetRawValue<string>("AccommQuestionId"), caseNote.Notes);
                        }
                    }
                }
                if (interactiveProcessNotes != null)
                {
                    interactiveProcessNotes = interactiveProcessNotes.OrderBy(note => note.Key).ToDictionary(orderedNote => orderedNote.Key, orderedNote => orderedNote.Value);
                }

                foreach (AccommodationInteractiveProcess.Step step in accommodationInteractiveProcess.Steps)
                {
                    if (step.Question != null && step.Question.Group != null && step.Question.Group == "Interactive Questions")
                    {
                        InteractiveProcessMergeData mergeData = new InteractiveProcessMergeData();
                        mergeData.CopyInteractiveProcessData(step, interactiveProcessNotes);
                        interactiveProcess.Add(mergeData);
                    }
                }
            }
            return interactiveProcess;
        }

        private string RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9]+", "", RegexOptions.Compiled);
        }

        public void SetInteractiveProcessData(List<InteractiveProcessMergeData> intractiveQuestions)
        {
            if (accommodationInteractiveProcess != null && accommodationInteractiveProcess.Steps != null && accommodationInteractiveProcess.Steps.Count > 0)
            {
                Dictionary<string, string> interactiveProcessNotes = null;
                if (notes != null)
                {
                    foreach (CaseNote caseNote in notes.OrderByDescending(note => note.ModifiedDate))
                    {
                        Guid accommId = caseNote.Metadata.GetRawValue<Guid>("AccommId");
                        if (accommId != null && accommId == this.accommId)
                        {
                            interactiveProcessNotes = (interactiveProcessNotes == null ? new Dictionary<string, string>() : interactiveProcessNotes);
                            if (!interactiveProcessNotes.ContainsKey(caseNote.Metadata.GetRawValue<string>("AccommQuestionId")))
                                interactiveProcessNotes.Add(caseNote.Metadata.GetRawValue<string>("AccommQuestionId"), caseNote.Notes);
                        }
                    }
                }
                if (interactiveProcessNotes != null)
                {
                    interactiveProcessNotes = interactiveProcessNotes.OrderBy(note => note.Key).ToDictionary(orderedNote => orderedNote.Key, orderedNote => orderedNote.Value);
                }

                foreach (AccommodationInteractiveProcess.Step step in accommodationInteractiveProcess.Steps)
                {
                    if (step.Question != null)
                    {
                        InteractiveProcessMergeData matchedQuestion = null;
                        foreach (var question in intractiveQuestions)
                        {
                            if(string.Compare(RemoveSpecialCharacters(question.Question), RemoveSpecialCharacters(step.Question.Question)) == 0 )
                            {
                                matchedQuestion = question;
                                break;
                            }
                        }

                        if (matchedQuestion == null)
                            continue;

                        InteractiveProcessMergeData mergeData = new InteractiveProcessMergeData();
                        mergeData.CopyInteractiveProcessData(step, interactiveProcessNotes);
                        matchedQuestion.Answer = mergeData.Answer;
                        matchedQuestion.Note = mergeData.Note;
                    }
                }
            }
        }
  
        public DateTime? StartDate {get; set;}
        public DateTime? EndDate { get; set; }
        public DateTime? MaxApprovedDate { get; set; }
        public DateTime? MaxApprovedDatePlus1
        {
            get
            {
                if (MaxApprovedDate != null)
                    return MaxApprovedDate.Value.AddDays(1);

                return null;
            }
        }
        
        

        public string Code { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Resolution { get; set; }
        public string Duration { get; set; }
        public string Decision { get; set; }

        public string Implemented { get; set; }

        private Guid accommId { get; set; }
        private AccommodationInteractiveProcess accommodationInteractiveProcess { get; set; }
        private List<CaseNote> notes { get; set; }

        public override string ToString()
        {
            return this.Name ?? "Unknown";
        }
    }
}
