﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Cases;

namespace AbsenceSoft.Rendering.Templating.MailMerge
{
    public class TimeOffRequestMergeData
    {
        public string PolicyCode { get; set; }
        public string PolicyName { get; set; }
        public DateTime? RequestDate { get; set; }        
        public string PendingHours { get; set; }        
        public string ApprovedHours { get; set; }        
        public string DeniedHours { get; set; }
        public string DeniedReasonName { get; set; }
        public string DenialReasonCode { get; set; }
        public string Status { get; set; }
        public string StartTimeForLeave { get; set; }
        public string EndTimeForLeave { get; set; }
    }
}
