﻿using AbsenceSoft.Data.Customers;
using Aspose.Words.MailMerging;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Dynamic;

namespace AbsenceSoft.Rendering.Templating.MailMerge
{
    public class MailMergeDataSource : IMailMergeDataSource
    {
        private readonly IEnumerator _Enumerator;
        private readonly string _TableName;

        public MailMergeDataSource(IEnumerable data, string tableName)
        {
            this._Enumerator = data.GetEnumerator();
            this._TableName = tableName;
        }


        public IMailMergeDataSource GetChildDataSource(string tableName)
        {
            return null;
        }

        public bool GetValue(string fieldName, out object fieldValue)
        {
            fieldValue = _Enumerator.Current;
            IEnumerator<string> cursor = fieldName.Split('.')
                .ToList()
                .GetEnumerator();
            while (fieldValue != null && cursor.MoveNext())
            {
                fieldValue = ResolveValue(fieldValue, cursor);
            }
            return fieldValue != null;
        }

        private static object ResolveValue(object obj, IEnumerator<string> fieldcursor)
        {            
            if (obj is ExpandoObject)
            {
                KeyValuePair<string, object> objValue = ((ExpandoObject)obj).SingleOrDefault(e => e.Key == fieldcursor.Current);
                return (objValue.Value == null) ? null : objValue.Value.ToString();
            }
            object result = null;
            var type = obj.GetType();
            PropertyInfo prop = null;
            prop =
                type.GetProperty(fieldcursor.Current, BindingFlags.Instance | BindingFlags.Public) ??
                type.GetProperty("CustomFields", BindingFlags.Instance | BindingFlags.Public);
            
            if (prop != null)
            {
                //
                // Special Handling for Flags
                if (prop.PropertyType == typeof(Flags))
                {
                    var flags = prop.GetValue(obj, null) as Flags;
                    if (fieldcursor.MoveNext())
                    {
                        result = flags.Contains(fieldcursor.Current);
                    }
                }
                //
                // Special Handling for List<CustomFieldMergeData>
                else if (prop.PropertyType == typeof(List<CustomFieldMergeData>))
                {
                    var customFields = prop.GetValue(obj, null) as List<CustomFieldMergeData>;
                    if (customFields != null)
                    {
                        var field = customFields.FirstOrDefault(f => f.Name == fieldcursor.Current);
                        if(field != null)
                            result = field.Value;
                    }
                }
                //
                // All others
                else
                {
                    result = prop.GetValue(obj, null);
                }
            }            
            return result;
        }

        public bool MoveNext()
        {
            return _Enumerator.MoveNext();
        }

        public string TableName
        {
            get { return _TableName; }
        }
    }
}
