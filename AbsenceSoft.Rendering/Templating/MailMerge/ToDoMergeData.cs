﻿using AbsenceSoft.Data.ToDo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Rendering.Templating.MailMerge
{
    public class ToDoMergeData
    {
        public ToDoMergeData()
        {

        }

        public ToDoMergeData(ToDoItem todo)
            : base()
        {
            if (todo == null)
            {
                return;
            }

            DueDate = todo.DueDate;
            CreatedDate = todo.CreatedDate;
            AssignedToName = todo.AssignedToName;
            Title = todo.Title;

            if (todo.Case != null)
            {
                CaseNumber = todo.Case.CaseNumber;
                EmployeeName = todo.Case.Employee.FullName;
            }
        }

        /// <summary>
        /// Gets the due date.
        /// </summary>
        /// <value>
        /// The due date.
        /// </value>
        public DateTime DueDate { get; private set; }

        /// <summary>
        /// Gets the created date.
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        public DateTime CreatedDate { get; private set; }

        /// <summary>
        /// Gets the name of the assigned to.
        /// </summary>
        /// <value>
        /// The name of the assigned to.
        /// </value>
        public string AssignedToName { get; private set; }

        /// <summary>
        /// Gets the case number. We store this on here in case we are sending ToDos associated with multiple cases in one notification.
        /// </summary>
        /// <value>
        /// The case number.
        /// </value>
        public string CaseNumber { get; private set; }

        /// <summary>
        /// Gets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title { get; private set; }

        public string EmployeeName { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return Title;
        }
    }
}
