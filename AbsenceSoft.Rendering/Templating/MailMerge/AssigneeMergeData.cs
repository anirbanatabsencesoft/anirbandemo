﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Rendering.Templating.MailMerge
{
    public class AssigneeMergeData
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string WorkPhone { get; set; }
        public string Fax { get; set; }
        public string Title { get; set; }
        public string Name {
            get
            {
                StringBuilder name = new StringBuilder();
                if (!string.IsNullOrWhiteSpace(Title))
                { 
                    name.AppendFormat("{0}{1}", name.Length > 0 ? " " : "", Title);
                }
                if (!string.IsNullOrWhiteSpace(FirstName))
                {
                    name.AppendFormat("{0}{1}", name.Length > 0 ? " " : "", FirstName);
                }
                if (!string.IsNullOrWhiteSpace(LastName))
                { 
                    name.AppendFormat("{0}{1}", name.Length > 0 ? " " : "", LastName);
                }
                return name.ToString();
            }
            set { }
        }
        public string Code { get; set; }
    }
}
