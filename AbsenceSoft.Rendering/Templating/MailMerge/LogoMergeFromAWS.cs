﻿using Aspose.Words.Fields;
using Aspose.Words.MailMerging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Rendering.Templating.MailMerge
{
    public class LogoMergeFromAWS : IFieldMergingCallback
    {
        public LogoMergeFromAWS(byte[] customerLogo, byte[] employerLogo)
        {
            this.CustomerLogo = customerLogo;
            this.EmployerLogo = employerLogo;
        }

        public byte[] CustomerLogo { get; set; }
        public byte[] EmployerLogo { get; set; }
        private byte[] Logo { get { return EmployerLogo ?? CustomerLogo; } }

        public void FieldMerging(FieldMergingArgs args)
        {

        }

        public void ImageFieldMerging(ImageFieldMergingArgs args)
        {
            if (args.FieldName == "Image:CustomerLogo")
            {
                args.ImageStream = new MemoryStream(CustomerLogo);
            }
            else if (args.FieldName == "Image:EmployerLogo")
            {
                args.ImageStream = new MemoryStream(EmployerLogo);
            }
            else if (args.FieldName == "Image:Logo")
            {
                args.ImageStream = new MemoryStream(Logo);
            }

        }
    }
}
