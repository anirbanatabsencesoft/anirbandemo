﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Dynamic;

namespace AbsenceSoft.Rendering.Templating.MailMerge
{
    /// <summary>
    /// A somewhat flattened class to hold fields for doing a Mail Merge
    /// </summary>
    public class MailMergeData
    {
        public MailMergeData()
        {
            this.Employee = new EmployeeMergeData();
            this.Case = new CaseMergeData();
            this.CaseManager = new ContactMergeData();
            this.Supervisor = new ContactMergeData();
            this.Employer = new EmployerMergeData();
            this.Customer = new CustomerMergeData();
            this.CurrentUser = new ContactMergeData();
            this.AdminContacts = new List<ContactMergeData>();
            this.OutstandingPaperwork = new List<PaperworkMergeData>();
            this.OutstandingPaperworkReviewRequired = new List<PaperworkMergeData>();
            this.OutstandingPaperworkReviewNotRequired = new List<PaperworkMergeData>();
            this.Policies = new List<PolicyMergeData>();
            this.InteractiveProcess = new List<InteractiveProcessMergeData>();
            this.Certifications = new List<CertificationMergeData>();
            this.RelatedPerson = new ContactMergeData();
            this.WorkRestrictions = new List<WorkRestrictionMergeData>();
            this.WorkRestrictionsEntry = new List<dynamic>();
            this.TimeOffRequests = new List<TimeOffRequestMergeData>();
            this.AllPendingPaperworkDue = new List<PaperworkMergeData>();
            this.ToDos = new List<ToDoMergeData>();
            InitInteractiveStepQuestions();
            this.Relapse = new RelapseMergeData();
            this.Relapses = new List<RelapseMergeData>();
            this.CaseAssignees = new List<AssigneeMergeData>();
        }

        public void CopyCaseData(Case c, Employee e, User u, List<EmployeeContact> employeeContacts, List<CommunicationPaperwork> outstandingPaperwork, List<CaseNote> caseNotes, List<Relapse> relapses = null)
        {
            Employee.CopyEmployeeData(e, c);
            Case.CopyCaseData(c, caseNotes);

            if (Case != null && Case.ActiveAccommodation != null)
            {
                this.InteractiveProcess = Case.ActiveAccommodation.GetInteractiveProcessData();
                Case.ActiveAccommodation.SetInteractiveProcessData(intractiveQuestions);
            }

            CaseEvent ce = c.FindLastCaseEvent(CaseEventType.PaperworkReceived);
            if (ce != null)
                LastPaperworkReceivedDate = ce.EventDate.Date;

            CaseManager.CopyUserData(c.AssignedTo);
            Employer.CopyEmployerData(c.Employer);
            Customer.CopyCustomerData(c.Customer);
            CurrentUser.CopyUserData(u);
            RelatedPerson.CopyContactData(c.Contact);

            if (employeeContacts != null)
            {
                this.AdminContacts = employeeContacts.Select(s => new ContactMergeData()
                {
                    FirstName = s.Contact.FirstName,
                    LastName = s.Contact.LastName,
                    Title = u.Title,
                    Type = s.ContactTypeCode,
                    Email = s.Contact.Email,
                    WorkPhone = s.Contact.WorkPhone.FormatPhone(),
                    Fax = s.Contact.Fax.FormatPhone()
                }).ToList();

                this.Supervisor = this.AdminContacts.FirstOrDefault(p => String.Equals(p.Type, "Supervisor", StringComparison.InvariantCultureIgnoreCase));
            }

            foreach (var policy in c.Summary.Policies)
            {
                foreach (var detail in policy.Detail)
                {
                    this.Policies.Add(new PolicyMergeData()
                    {
                        Code = policy.PolicyCode,
                        Name = policy.PolicyName,
                        StartDate = detail.StartDate,
                        EndDate = detail.EndDate,
                        CaseType = detail.CaseType.ToString(),
                        Determination = detail.Determination,
                        Status = detail.Determination == AdjudicationSummaryStatus.Approved ? EligibilityStatus.Approved : EligibilityStatus.Pending,
                        DeniedReason = detail.DenialReasonName ?? detail.DenialReasonName,
                        DeniedReasonCode = detail.DenialReasonCode ?? detail.DenialReasonCode
                    });
                }
            }
            //Case.Summary.Policies only contains value if the Policy is Eligible for Non eleigible policy we need to loop through the Case.Segments.AppliedPolicy

            foreach (var segment in c.Segments.Where(s => s.Status != CaseStatus.Cancelled))
            {
                foreach (var policy in segment.AppliedPolicies.Where(ap => !this.Policies.Any(p => p.Code == ap.Policy.Code)))
                {
                    bool failedPolicy = false;
                    List<AppliedRule> failedRules = policy.RuleGroups.Where(rg => !rg.Pass).SelectMany(rg => rg.Rules).Where(r => !r.Pass).ToList();
                    List<string> failedRulesText = new List<string>();
                    bool hasStatePolicy = false, stateRuleWorkedHoursNotMet = false, stateRule50EmployeesIn75MileRadiusNotMet = false, stateRuleWorkedLengthofserviceNotMet = false, stateRuleRelationshipToEmployeeNotMet = false, stateRuleLeaveWithin12MonthsNotMet = false;
                    object hoursWorked = null;
                    if (policy.Policy.PolicyType == PolicyType.StateFML) hasStatePolicy = true;
                    foreach (AppliedRule failedRule in failedRules)
                    {
                        failedPolicy = true;
                        if (failedRule.Overridden)
                            failedRulesText.Add(string.Concat(failedRule.Rule.Description, " FAILED", ((!string.IsNullOrEmpty(failedRule.OverrideValue)) ? string.Concat(", appended as \"", failedRule.OverrideValue, "\"") : ""), ((!string.IsNullOrEmpty(failedRule.OverrideNotes)) ? string.Concat(", because \"", failedRule.OverrideNotes, ".\"") : "")));
                        else
                        {
                            if (failedRule.Rule.LeftExpression.Contains("Has1YearOfService") || failedRule.Rule.LeftExpression.Contains("HasMinLengthOfService"))
                            { failedRulesText.Add("You have not satisfied the length of service requirement prior to the start of your leave."); stateRuleWorkedLengthofserviceNotMet = true; }
                            else if (failedRule.Rule.LeftExpression.Contains("TotalHoursWorkedLast12Months"))
                            { failedRulesText.Add("You have not satisfied the hours worked for the company requirement in the 12 months preceding the start of your leave. As of the first date of requested leave, you will have worked approximately " + failedRule.ActualValue + " hours toward this requirement."); stateRuleWorkedHoursNotMet = true; hoursWorked = failedRule.ActualValue; }
                            else if (failedRule.Rule.LeftExpression.Contains("Meets50In75MileRule"))
                            { failedRulesText.Add("You do not work or report to a site with 50 or more employees within 75 miles."); stateRule50EmployeesIn75MileRadiusNotMet = true; }
                            else if (failedRule.Rule.LeftExpression.Contains("CaseRelationshipType"))
                            { failedRulesText.Add("The family member you have selected is not a qualifying family member."); stateRuleRelationshipToEmployeeNotMet = true; }
                            else if (failedRule.Rule.LeftExpression.Contains("NumberOfMonthsFromDeliveryDate"))
                            { failedRulesText.Add("Leave not taken within 12 months from the Delivery / Expected Delivery Date."); stateRuleLeaveWithin12MonthsNotMet = true; }
                            else
                            {
                                failedRulesText.Add(string.Concat("You have not satisfied the ", failedRule.Rule.Description));
                            }
                        }
                    }
                    if (failedPolicy)
                    {
                        this.Policies.Add(new PolicyMergeData()
                        {
                            Code = policy.Policy.Code,
                            Name = policy.Policy.Name,
                            CaseType = c.Segments.FirstOrDefault(seg => seg.StartDate == policy.StartDate)?.Type.ToString(),
                            Status = policy.Status,
                            Type = policy.Policy.PolicyType,
                            HasStatePolicy = hasStatePolicy,
                            StateRuleWorkedHoursNotMet = stateRuleWorkedHoursNotMet,
                            StateRule50EmployeesIn75MileRadiusNotMet = stateRule50EmployeesIn75MileRadiusNotMet,
                            StateRuleWorkedLengthofserviceNotMet = stateRuleWorkedLengthofserviceNotMet,
                            StateRuleRelationshipToEmployeeNotMet = stateRuleRelationshipToEmployeeNotMet,
                            StateRuleLeaveWithin12MonthsNotMet = stateRuleLeaveWithin12MonthsNotMet,
                            HoursWorked = hoursWorked != null ? Convert.ToInt32(hoursWorked) : (double?)null,
                            IneligibleReason = string.Join("\n\n", failedRulesText),                           
                        });
                    }
                }
            }

            if (outstandingPaperwork != null)
            {
                this.OutstandingPaperwork = outstandingPaperwork.Select(s => new PaperworkMergeData()
                {
                    Name = s.Paperwork.Name,
                    FirstCasePaperworkDueDate = s.FirstCasePaperworkDueDate,
                    DueDate = s.DueDate
                }).ToList();

                if (outstandingPaperwork.Any(paperwork => paperwork.Paperwork.RequiresReview))
                {
                    this.OutstandingPaperworkReviewRequired = outstandingPaperwork.Where(paperwork => paperwork.Paperwork.RequiresReview).Select(s => new PaperworkMergeData()
                    {
                        Name = s.Paperwork.Name,
                        FirstCasePaperworkDueDate = s.FirstCasePaperworkDueDate,
                        DueDate = s.DueDate
                    }).ToList();
                }
                if (outstandingPaperwork.Any(paperwork => paperwork.Paperwork.RequiresReview == false))
                    this.OutstandingPaperworkReviewNotRequired = outstandingPaperwork.Where(paperwork => paperwork.Paperwork.RequiresReview == false).Select(s => new PaperworkMergeData()
                    {
                        Name = s.Paperwork.Name,
                        FirstCasePaperworkDueDate = s.FirstCasePaperworkDueDate,
                        DueDate = s.DueDate
                    }).ToList();
            }

            CopyCertificationData(c, caseNotes);
            CopyTimeOffRequestData(c);
            CopyToDoData(GetToDoItems(c.CaseNumber));
            CopyRelapseData(relapses);
        }

        private ToDoItem[] GetToDoItems(string caseNumber)
        {
            return ToDoItem.AsQueryable()
                .Where(tdi => tdi.CaseNumber == caseNumber         // Get the ToDos that have been assigned to them, 
                    && tdi.Status != ToDoItemStatus.Complete        // That hasn't been completed
                    && tdi.Status != ToDoItemStatus.Cancelled       // that hasn't been cancelled
                                                                    // Time to check the dates
                                                                    // it's not hidden and were created after the user most recently modified their notification preferences
                    && ((tdi.HideUntil == null)
                        // it's was hidden, but it should be visible now
                        || (tdi.HideUntil <= DateTime.Today)
                        // it was hidden, but it might have been modified (like reassigned to this user) since they elected receive notifications
                        || (tdi.HideUntil <= DateTime.Today)
                       )
                    ).ToArray();
        }

        private void CopyCertificationData(Case c, List<CaseNote> caseNotes)
        {
            foreach (Certification certification in c.Certifications)
            {
                CertificationMergeData certificationMergeData = new CertificationMergeData()
                {
                    StartDate = certification.StartDate.ToShortDateString(),
                    EndDate = certification.EndDate.ToShortDateString(),
                    IntermittentType = certification.IntermittentType == IntermittentType.Incapacity ? "Incapacity" : "Office Visit",
                    Occurances = certification.Occurances,
                    Frequency = certification.Frequency,
                    FrequencyType = certification.FrequencyType,
                    Duration = certification.Duration,
                    DurationType = certification.DurationType
                };

                CaseNote matchingNote = caseNotes.FirstOrDefault(cn => cn.Metadata.GetRawValue<string>("CertId") == certification.Id.ToString());
                if (matchingNote != null && !string.IsNullOrEmpty(matchingNote.Notes))
                {
                    certificationMergeData.Notes = matchingNote.Notes;
                }

                Certifications.Add(certificationMergeData);
            }
        }

        public void CopyLogoData(byte[] customerLogo, byte[] employerLogo)
        {
            this.CustomerLogo = customerLogo;
            this.EmployerLogo = employerLogo;
        }

        public void CopyEmployeeData(Employee e, List<EmployeeContact> employeeContacts, List<EmployeeRestriction> workRestrictions, List<EmployeeJob> jobs, Case theCase, List<Demand> demands, List<DemandType> demandTypes)
        {
            Employee.CopyEmployeeData(e, theCase);
            CopyAdminContacts(employeeContacts);
            CopyWorkRestrictions(workRestrictions);
            CopyWorkRestrictionsEntry(workRestrictions, demands, demandTypes);
        }


        /// <summary>
        /// Copies to do data.
        /// </summary>
        /// <param name="todos">The todos.</param>
        public void CopyToDoData(params ToDoItem[] todos)
        {
            if (todos == null || todos.Length == 0)
            {
                return;
            }

            List<ToDoMergeData> toDoMergeData = todos.Select(t => new ToDoMergeData(t)).ToList();
            if (ToDos == null || !ToDos.Any())
            {
                ToDos = toDoMergeData;
            }
            else
            {
                ToDos.AddRange(toDoMergeData);
            }
        }

        public void UpdatePolicyUtilization(List<ExpandoObject> policySumaries)
        {

            if (this.Policies != null && this.Policies.Count > 0 && policySumaries != null && policySumaries.Count > 0)
            {
                foreach (dynamic policySummary in policySumaries)
                {
                    this.Policies.Where(p => p.Code.ToUpper() == policySummary.PolicyCode.ToUpper()).ForEach(policy =>
                    {
                        policy.TimeUsedText = policySummary.TimeUsedText;
                        policy.TimeRemainingText = policySummary.TimeRemainingText;
                        policy.ProjectedUsageText = policySummary.ProjectedUsageText;
                    });

                    //CopyCaseData , is getting called before this method due to that we are no able to set proper value in TimeUsedText & TimeRemainingText.All policy related calculation  
                    //is done here and accordingly  we are setting below two property here instead of CaseMergeData  
                    if (policySummary.PolicyCode.ToUpper() == "FMLA" && this.Case != null && this.Case.FMLAPolicy != null)
                    {
                        this.Case.FMLAPolicy.TimeUsedText = policySummary.TimeUsedText;
                        this.Case.FMLAPolicy.TimeRemainingText = policySummary.TimeRemainingText;

                    }
                }
            }
        }

        private void CopyAdminContacts(List<EmployeeContact> employeeContacts)
        {
            if (employeeContacts != null)
            {
                this.AdminContacts = employeeContacts.Select(s => new ContactMergeData()
                {
                    FirstName = s.Contact.FirstName,
                    LastName = s.Contact.LastName,
                    Type = s.ContactTypeCode,
                    Email = s.Contact.Email,
                    WorkPhone = s.Contact.WorkPhone.FormatPhone(),
                    Fax = s.Contact.Fax.FormatPhone()
                }).ToList();
            }
        }

        private void CopyWorkRestrictions(List<EmployeeRestriction> workRestrictions)
        {
            if (workRestrictions == null || !workRestrictions.Any())
                return;

            foreach (var wr in workRestrictions.Where(r => r.Restriction != null))
            {
                if (wr.Restriction.Values == null)
                {
                    WorkRestrictions.Add(new WorkRestrictionMergeData()
                    {
                        Name = wr.Restriction.Demand.Name,
                        StartDate = wr.Restriction.Dates.StartDate,
                        EndDate = wr.Restriction.Dates.EndDate,
                        Type = wr.Restriction.Type.ToString()
                    });
                }
                else
                {
                    foreach (var value in wr.Restriction.Values)
                    {
                        this.WorkRestrictions.Add(new WorkRestrictionMergeData()
                        {
                            Name = wr.Restriction.Demand.Name,
                            StartDate = wr.Restriction.Dates.StartDate,
                            EndDate = wr.Restriction.Dates.EndDate,
                            Type = value.DemandType.Name,
                            Value = value.ToString()
                        });
                    }
                }
            }
        }

        private void CopyWorkRestrictionsEntry(List<EmployeeRestriction> workRestrictionsEntry, List<Demand> demands, List<DemandType> demandTypes)
        {
            if (workRestrictionsEntry != null && workRestrictionsEntry.Count > 0)
            {
                List<EmployeeRestriction> restrictions = new List<EmployeeRestriction>();
                foreach (var demand in demands)
                {
                    var matchingRestrictions = workRestrictionsEntry.Where(r => r.Restriction.DemandId == demand.Id).ToList().OrderByDescending(r => r.Restriction.Dates.StartDate).FirstOrDefault();
                    if (matchingRestrictions != null) restrictions.Add(matchingRestrictions);
                }

                foreach (var wr in restrictions.Where(r => r.Restriction != null))
                {
                    dynamic demand = new ExpandoObject();
                    var dictionaryFields = (IDictionary<string, object>)demand;
                    dictionaryFields.Add("Name", wr.Restriction.Demand.Name);
                    dictionaryFields.Add("StartDate", wr.Restriction.Dates.StartDate);
                    dictionaryFields.Add("EndDate", wr.Restriction.Dates.EndDate);
                    if (wr.Restriction.Values != null)
                    {
                        foreach (var type in demandTypes)
                        {
                            AppliedDemandValue<WorkRestrictionValue> demandValue = wr.Restriction.Values.Where(value => value.DemandTypeId == type.Id).FirstOrDefault();
                            if (demandValue != null && demandValue.Value != null && (!string.IsNullOrWhiteSpace(demandValue.Value.ToString())))
                            {
                                dictionaryFields.Add("IS" + type.Code.ToUpper(), "YES");
                                dictionaryFields.Add(type.Code.ToUpper() + "VALUE", demandValue.Value);
                            }
                            else
                            {
                                dictionaryFields.Add("IS" + type.Code.ToUpper(), "NO");
                                dictionaryFields.Add(type.Code.ToUpper() + "VALUE", string.Empty);
                            }
                        }
                    }
                    this.WorkRestrictionsEntry.Add(demand);
                }
            }
        }

        private void CopyEmployeeJobs(List<EmployeeJob> jobs)
        {
            if (jobs != null)
            {
                foreach (var job in jobs)
                {
                    StringBuilder jobRequirements = new StringBuilder();
                    EmployeeJobMergeData jobMergeData = new EmployeeJobMergeData()
                    {
                        Name = job.JobName,
                        Location = job.OfficeLocationCode,
                        Status = job.Status.Value.ToString().SplitCamelCaseString(),
                        StartDate = job.Dates.StartDate,
                        EndDate = job.Dates.EndDate
                    };
                    foreach (var requirement in job.Job.Requirements)
                    {
                        foreach (var value in requirement.Values)
                        {
                            jobRequirements.AppendFormat("{0}: {1} {2}", requirement.Demand.Name, value.DemandType.Name, value.Value);
                            if (jobs.IndexOf(job) != jobs.Count - 1)
                                jobRequirements.Append(", ");
                        }
                    }

                    jobMergeData.Requirements = jobRequirements.ToString();
                    this.Jobs.Add(jobMergeData);
                }
            }
        }

        private void CopyTimeOffRequestData(Case c)
        {

            var getStatus = new Func<IntermittentTimeRequestDetail, string>((field) =>
             {
                 if (field.Approved > 0)
                 {
                     return "Approved";
                 }
                 if (field.Pending > 0)
                 {
                     return "Pending";
                 }
                 if (field.Denied > 0)
                 {
                     return "Denied";
                 }
                 return "Pending";
             });

            var policies = c.Segments.Where(s => s.AppliedPolicies != null).SelectMany(s => s.AppliedPolicies).Select(ap => ap.Policy).ToList();
            c.Segments.Where(s => s.Type == CaseType.Intermittent && s.UserRequests != null && policies != null && policies.Any())
                      .SelectMany(s => s.UserRequests).Where(request => request.Detail != null).ForEach(request =>
                      {
                          request.Detail.ForEach(detail =>
                          {
                              var policy = policies.FirstOrDefault(p => p.Code.Trim().ToUpper() == detail.PolicyCode.Trim().ToUpper());
                              string denialReason = null;
                              if (detail.DenialReasonCode != null)
                              {
                                  if (detail.DenialReasonCode != AdjudicationDenialReason.Other)
                                      denialReason = detail.DenialReasonName;
                                  else
                                      denialReason = detail.DenialReasonCode + " - " + detail.DeniedReasonOther;
                              }
                              string startTime = string.Empty;
                              string endTime = string.Empty;

                              if (request.StartTimeForLeave.HasValue)
                              {
                                  startTime = Convert.ToDateTime(request.StartTimeForLeave.ToString()).Hour > 0 || Convert.ToDateTime(request.StartTimeForLeave.ToString()).Minute > 0 
                                                     ? request.StartTimeForLeave.ToString() : "";
                              }

                              if( request.EndTimeForLeave.HasValue)
                              { 
                              endTime = Convert.ToDateTime(request.EndTimeForLeave.ToString()).Hour > 0 || Convert.ToDateTime(request.EndTimeForLeave.ToString()).Minute > 0
                                                     ? request.StartTimeForLeave.ToString() : "";
                              }
                              if (policy != null)
                                  this.TimeOffRequests.Add(
                                      new TimeOffRequestMergeData()
                                      {
                                          RequestDate = request.RequestDate,
                                          PolicyCode = detail.PolicyCode,
                                          PolicyName = policy.Name,
                                          PendingHours = (detail.Pending / (double)60).ToString("#.##").ZeroIfEmpty() + "h",
                                          ApprovedHours = (detail.Approved / (double)60).ToString("#.##").ZeroIfEmpty() + "h",
                                          DeniedHours = (detail.Denied / (double)60).ToString("#.##").ZeroIfEmpty() + "h",
                                          DeniedReasonName = denialReason,
                                          DenialReasonCode = detail.DenialReasonCode,
                                          Status = getStatus(detail),
                                          StartTimeForLeave = startTime,
                                          EndTimeForLeave = endTime
                                      });
                          });
                      });
        }

        public void CopyDuePaperworkData(List<CommunicationPaperwork> paperworkDue, DateTime? firstPaperworkDueDate, DateTime? lastPaperworkDueDate)
        {
            if (paperworkDue != null && paperworkDue.Any())
            {
                AllPendingPaperworkDue = paperworkDue.Select(dp => new PaperworkMergeData(dp, firstPaperworkDueDate, lastPaperworkDueDate)).ToList();
            }
        }

        public void CopyRelapseData(List<Relapse> relapses)
        {
            if (relapses != null && relapses.Count > 0)
            {
                foreach (var relapse in relapses)
                {
                    this.Relapses.Add(new RelapseMergeData()
                    {
                        StartDate =  relapse.StartDate != DateTime.MinValue ? relapse.StartDate : (Nullable<DateTime>)null,
                        EndDate = relapse.EndDate,
                        CreatedDate = relapse.CreatedDate != DateTime.MinValue ? relapse.CreatedDate : (Nullable<DateTime>)null
                    });
                }
                var recentRelapse = relapses.OrderByDescending(x => x.StartDate).FirstOrDefault();
                this.Relapse = new RelapseMergeData
                {
                    StartDate = recentRelapse.StartDate != DateTime.MinValue ? recentRelapse.StartDate : (Nullable<DateTime>)null,
                    EndDate = recentRelapse.EndDate,
                    CreatedDate = recentRelapse.CreatedDate != DateTime.MinValue ? recentRelapse.CreatedDate : (Nullable<DateTime>)null
                };
            }
        }

        /// <summary>
        /// Copies the case assignee data.
        /// </summary>
        /// <param name="caseAssigneesList">The case assignees list.</param>
        public void CopyCaseAssigneeData(List<CaseAssignee> caseAssigneesList)
        {
            if (caseAssigneesList != null)
            {
                var userIds = caseAssigneesList.Select(a => a.UserId).Distinct().ToList();
                List<User> assigneeUsers = User.AsQueryable().Where(u => userIds.Contains(u.Id)).ToList();
                caseAssigneesList.ForEach(c =>
                {
                    var user = assigneeUsers.FirstOrDefault(u => u.Id == c.UserId);
                    if (user != null)
                    {
                        AssigneeMergeData assignee = new AssigneeMergeData();
                        assignee.FirstName = user.FirstName;
                        assignee.LastName = user.LastName;
                        assignee.Email = user.Email;
                        assignee.WorkPhone = user.ContactInfo.WorkPhone;
                        assignee.Fax = user.ContactInfo.Fax;
                        assignee.Title = user.Title;
                        assignee.Code = c.Code;
                        CaseAssignees.Add(assignee);
                    }
                });
            }
        }

        public byte[] CustomerLogo { get; set; }
        public byte[] EmployerLogo { get; set; }
        public byte[] Logo { get { return EmployerLogo ?? CustomerLogo; } }
        public bool HasCustomerLogo { get { return CustomerLogo != null && CustomerLogo.Length > 0; } }
        public bool HasEmployerLogo { get { return EmployerLogo != null && EmployerLogo.Length > 0; } }

        public EmployeeMergeData Employee { get; set; }
        public CaseMergeData Case { get; set; }
        public ContactMergeData CaseManager { get; set; }
        public ContactMergeData Supervisor { get; set; }
        public EmployerMergeData Employer { get; set; }
        public CustomerMergeData Customer { get; set; }
        public ContactMergeData CurrentUser { get; set; }
        public ContactMergeData RelatedPerson { get; set; }
        public DateTime? LastPaperworkReceivedDate { get; set; }
        public List<PaperworkMergeData> OutstandingPaperwork { get; set; }
        public bool HasPaperworkForReview { get { return OutstandingPaperworkReviewRequired != null && OutstandingPaperworkReviewRequired.Any(); } }
        public List<PaperworkMergeData> OutstandingPaperworkReviewRequired { get; set; }
        public bool HasNoPaperworkForReview { get { return OutstandingPaperworkReviewNotRequired != null && OutstandingPaperworkReviewNotRequired.Any(); } }
        public List<PaperworkMergeData> OutstandingPaperworkReviewNotRequired { get; set; }
        public List<WorkRestrictionMergeData> WorkRestrictions { get; set; }
        public List<EmployeeJobMergeData> Jobs { get; set; }
        public List<PolicyMergeData> Policies { get; set; }
        public List<CertificationMergeData> Certifications { get; set; }
        public List<ContactMergeData> AdminContacts { get; set; }
        public List<InteractiveProcessMergeData> InteractiveProcess { get; set; }
        public List<PolicyMergeData> EligiblePolicies { get { return Policies == null ? null : Policies.Where(p => p.Status == EligibilityStatus.Eligible).ToList(); } }
        public List<PolicyMergeData> IneligiblePolicies { get { return Policies == null ? null : Policies.Where(p => p.Status == EligibilityStatus.Ineligible).ToList(); } }
        public List<PolicyMergeData> ApprovedPolicies { get { return Policies == null ? null : Policies.Where(p => p.Determination == AdjudicationSummaryStatus.Approved).ToList(); } }
        public List<PolicyMergeData> DeniedPolicies { get { return Policies == null ? null : Policies.Where(p => p.Determination == AdjudicationSummaryStatus.Denied).ToList(); } }
        public List<PolicyMergeData> PendingPolicies { get { return Policies == null ? null : Policies.Where(p => p.Determination == AdjudicationSummaryStatus.Pending).ToList(); } }
        public dynamic PolicyInfo { get; set; }
        public dynamic AssigneeInfo { get; set; }
        public List<dynamic> WorkRestrictionsEntry { get; set; }
        public List<TimeOffRequestMergeData> TimeOffRequests { get; set; }

        public List<PaperworkMergeData> AllPendingPaperworkDue { get; set; }

        public List<ToDoMergeData> ToDos { get; set; }
        public RelapseMergeData Relapse { get; set; }
        public List<RelapseMergeData> Relapses { get; set; }
        public List<AssigneeMergeData> CaseAssignees { get; set; }
        public DateTime? FirstCasePaperworkDueDate
        {
            get
            {

                if (OutstandingPaperwork != null && OutstandingPaperwork.Count > 0)
                {
                    DateTime? _firstCasePaperworkDueDate = null;
                    _firstCasePaperworkDueDate = OutstandingPaperwork.Select(p => p.FirstCasePaperworkDueDate).Min();
                    if (_firstCasePaperworkDueDate.HasValue)
                    {
                        return DateExtensions.ToLocalDateTime(_firstCasePaperworkDueDate.Value);
                    }
                    return _firstCasePaperworkDueDate;
                }
                else
                {

                    return null;
                }
            }
        }

        public DateTime? FirstPaperworkDueDate
        {
            get
            {
                if (AllPendingPaperworkDue != null && AllPendingPaperworkDue.Count > 0)
                {
                    DateTime? _paperDueDate = null;
                    _paperDueDate = AllPendingPaperworkDue.Select(p => p.FirstCasePaperworkDueDate).Min();
                    if (_paperDueDate.HasValue)
                    {
                        return DateExtensions.ToLocalDateTime(_paperDueDate.Value);
                    }
                    return _paperDueDate;
                }
                else
                {

                    return null;
                }

            }
        }

        public DateTime? LastPaperworkDueDate
        {


            get
            {
                if (AllPendingPaperworkDue != null && AllPendingPaperworkDue.Count > 0)
                {
                    DateTime? _paperDueDate = null;
                    _paperDueDate = AllPendingPaperworkDue.Select(p => p.LastCasePaperworkDueDate).Max();
                    if (_paperDueDate.HasValue)
                    {
                        return DateExtensions.ToLocalDateTime(_paperDueDate.Value);
                    }
                    return _paperDueDate;
                }
                else
                {

                    return null;
                }
            }
        }

        public string SupervisorName
        {
            get
            {
                ContactMergeData supervisor = AdminContacts.Where(c => c.Type == "Supervisor").FirstOrDefault();
                if (supervisor == null)
                    return null;

                return supervisor.Name;
            }
        }

        public string SupervisorPhone
        {
            get
            {
                ContactMergeData supervisor = AdminContacts.Where(c => c.Type == "Supervisor").FirstOrDefault();
                if (supervisor == null)
                    return null;

                return supervisor.WorkPhone.FormatPhone();
            }
        }

        public string HRName
        {
            get
            {
                ContactMergeData supervisor = AdminContacts.Where(c => c.Type == "HR").FirstOrDefault();
                if (supervisor == null)
                    return null;

                return supervisor.Name;
            }
        }

        public string HRPhone
        {
            get
            {
                ContactMergeData supervisor = AdminContacts.Where(c => c.Type == "HR").FirstOrDefault();
                if (supervisor == null)
                    return null;

                return supervisor.WorkPhone.FormatPhone();
            }
        }

        public string HRFax
        {
            get
            {
                ContactMergeData supervisor = AdminContacts.Where(c => c.Type == "HR").FirstOrDefault();
                if (supervisor == null)
                    return null;

                return supervisor.Fax.FormatPhone();
            }
        }

        public string HREmail
        {
            get
            {
                ContactMergeData supervisor = AdminContacts.Where(c => c.Type == "HR").FirstOrDefault();
                if (supervisor == null)
                    return null;

                return supervisor.Email;
            }
        }

        #region Interactive Questions

        public InteractiveProcessMergeData DidYouTalkToTheEmployee { get; set; }
        public InteractiveProcessMergeData DidYouObtainMedicalDocumentationOfTheNeedForTheAccommodation { get; set; }
        public InteractiveProcessMergeData HaveYouConsideredTheRequestedAccommodations { get; set; }
        public InteractiveProcessMergeData IsTheAccommodationReasonable { get; set; }
        public InteractiveProcessMergeData HaveYouDiscussedItWithHumanResources { get; set; }
        public InteractiveProcessMergeData HaveYouDiscussedItWithOperations { get; set; }
        public InteractiveProcessMergeData DidYouCompleteTheAccommodationDetermination { get; set; }
        public InteractiveProcessMergeData DidYouCommunicateTheAccommodationDeterminationToTheEmployee { get; set; }
        public InteractiveProcessMergeData MonitorTheAccommodation { get; set; }
        public InteractiveProcessMergeData WhatAreTheEmployeesDifficultiesAndRestrictions { get; set; }
        public InteractiveProcessMergeData IsTheEmployeeHavingDifficultyPerformingTheJobFunctionsRightNow { get; set; }
        public InteractiveProcessMergeData DoesTheEmployeeBelieveThereAreAnyOutstandingIssuesOrConcernsWeShouldBeAwareOfToBetterSupportThemOrToAddressThisMatter { get; set; }
        public InteractiveProcessMergeData IsThereAnyAdditionalInformationTheEmployeeWouldLikeToShare { get; set; }
        public InteractiveProcessMergeData WhatIsTheMedicalSituationIsItTemporaryOrLongterm { get; set; }
        public InteractiveProcessMergeData WhatAreYourDifficultiesAndRestrictions { get; set; }
        public InteractiveProcessMergeData DoesThisImpactOrLimitTheEmployeesAbilityToPerformAnyMajorLifeActivities { get; set; }
        public InteractiveProcessMergeData AreYouHavingDifficultyPerformingYourJobFunctionsRightNow { get; set; }
        public InteractiveProcessMergeData WhatSpecificSupportOrAccommodationIsTheEmployeeRequesting { get; set; }
        public InteractiveProcessMergeData HaveYouHadAccommodationsForThisLimitationInThePastIfYesWhatWereTheModificationsAndDoYouThinkTheyWereEffective { get; set; }
        public InteractiveProcessMergeData DoesTheEmployeeHaveAnySuggestionsForAccommodationsTheyBelieveMayBeHelpfulWhatDoTheySeeAsASolutionOrAccommodationToKeepWorking { get; set; }
        public InteractiveProcessMergeData IsYourManagerAwareOfTheEmployeesNeedDoTheyFeelHesheIsSupportiveOfTheNeed { get; set; }
        public InteractiveProcessMergeData IsTheEmployeeOnALeaveOfAbsenceOrUsingALeaveIntermittentlyDoTheyNeedOne { get; set; }
        public InteractiveProcessMergeData IsTheEmployeeAbleToWorkRightNow { get; set; }
        public InteractiveProcessMergeData DoesTheEmployeeFeelTheJobDescriptionListingTheEssentialJobFunctionsIsDescriptiveOfTheirWorkIfNoPleaseListTheDescriptiveOrWorkThatIsMissing { get; set; }
        public InteractiveProcessMergeData WhatIsTheMedicalSituation { get; set; }
        public InteractiveProcessMergeData HasTheEmployeeHadAnyAccommodationsForThisLimitationInThePastIfYesWhatWereTheModificationsAndDoYouThinkTheyWereEffective { get; set; }
        public InteractiveProcessMergeData DoesTheEmployeeHaveAnySuggestionsForAccommodationsTheyBelieveMayBeHelpfulInAllowingThemToWork { get; set; }
        public InteractiveProcessMergeData HasTheEmployeeHadAccommodationsForThisLimitationInThePastIfYesWhatWereTheModificationsAndDoYouThinkTheyWereEffective { get; set; }
        public InteractiveProcessMergeData DidHROrOpsAssessTheEmployeeIfYesAsOfWhatDate { get; set; }
        public InteractiveProcessMergeData IsThereAnyThreatPleaseDescribe { get; set; }
        public InteractiveProcessMergeData DoesTheEmployeeBelieverThereAreAnyOutstandingIssuesOrConcernsWeShouldBeAwareOfToBetterSupportThemOrAddressThisMatter { get; set; }
        public InteractiveProcessMergeData DidYouSpeakWithTheEmployee { get; set; }
        public InteractiveProcessMergeData DoesTheEmployeeNeedAnyAccommodations { get; set; }
        public InteractiveProcessMergeData IsTheAccommodationShortTermLongTermOrPermanent { get; set; }
        public InteractiveProcessMergeData HaveYouConsideredTheRequestedAccommodation { get; set; }
        public InteractiveProcessMergeData AccommodatedWorkAvailableAtTheSiteIfNoCaptureWhyAndThenDocumentNextSteps { get; set; }
        public InteractiveProcessMergeData DoesTheLOAATeamAgreeWithSiteThatNoWorkIsAvailableYesNoifNoThenDocumentIfWeAreReversingInitialResponse { get; set; }
        public InteractiveProcessMergeData IsThereANeedForAnIndependentMedicalEvaluation { get; set; }
        public InteractiveProcessMergeData IsTheAccommodationPhysicalHoursOrBoth { get; set; }
        public InteractiveProcessMergeData DidYouTalkToTheADAContactAndorSupervisordocumentInformationReceivedDuringIntakeOrSubsequentDiscussions { get; set; }
        public InteractiveProcessMergeData DidYouTalkToTheEmployeedocumentAdditionalInformationReceivedWhenCaseManagerOutreachesToEmployeePostemployerIntake { get; set; }
        public InteractiveProcessMergeData HasMedicalInformationBeenReceived { get; set; }
        public InteractiveProcessMergeData WhatAreTheReasonableAccommodationsBeingConsideredincludesEmployeeRequestForAccommodationEmployerSuggestionsAndPrudentialRecommendations { get; set; }
        public InteractiveProcessMergeData HasTheInteractiveDiscussionTakenPlacetranscriptOfTheDiscussion { get; set; }
        public InteractiveProcessMergeData HasAnAccommodationDeterminationBeenMadeOutcomeOfTheEmployersInteractiveDiscussion { get; set; }
        public InteractiveProcessMergeData WhatIsTheAccommodationPlanincludePlanSignoffAndMonitoring { get; set; }

        private List<InteractiveProcessMergeData> intractiveQuestions;
        private void InitInteractiveStepQuestions()
        {

            DidYouTalkToTheEmployee = new InteractiveProcessMergeData()
            {
                Question = "Did you talk to the employee?"
            };

            DidYouObtainMedicalDocumentationOfTheNeedForTheAccommodation = new InteractiveProcessMergeData()
            {
                Question = "Did you obtain medical documentation of the need for the accommodation?"
            };
            HaveYouConsideredTheRequestedAccommodations = new InteractiveProcessMergeData()
            {
                Question = "Have you considered the requested accommodations?"
            };
            IsTheAccommodationReasonable = new InteractiveProcessMergeData()
            {
                Question = "Is the accommodation reasonable?"
            };
            HaveYouDiscussedItWithHumanResources = new InteractiveProcessMergeData()
            {
                Question = "Have you discussed it with Human Resources?"
            };
            HaveYouDiscussedItWithOperations = new InteractiveProcessMergeData()
            {
                Question = "Have you discussed it with Operations?"
            };
            DidYouCompleteTheAccommodationDetermination = new InteractiveProcessMergeData()
            {
                Question = "Did you complete the accommodation determination?"
            };
            DidYouCommunicateTheAccommodationDeterminationToTheEmployee = new InteractiveProcessMergeData()
            {
                Question = "Did you communicate the accommodation determination to the employee?"
            };
            MonitorTheAccommodation = new InteractiveProcessMergeData()
            {
                Question = "Monitor the accommodation?"
            };
            WhatAreTheEmployeesDifficultiesAndRestrictions = new InteractiveProcessMergeData()
            {
                Question = "What are the employee's difficulties and restrictions?"
            };
            IsTheEmployeeHavingDifficultyPerformingTheJobFunctionsRightNow = new InteractiveProcessMergeData()
            {
                Question = "Is the employee having difficulty performing the job functions right now?"
            };
            DoesTheEmployeeBelieveThereAreAnyOutstandingIssuesOrConcernsWeShouldBeAwareOfToBetterSupportThemOrToAddressThisMatter = new InteractiveProcessMergeData()
            {
                Question = "Does the employee believe there are any outstanding issues or concerns we should be aware of to better support them or to address this matter?"
            };
            IsThereAnyAdditionalInformationTheEmployeeWouldLikeToShare = new InteractiveProcessMergeData()
            {
                Question = "Is there any additional information the employee would like to share?"
            };
            WhatIsTheMedicalSituationIsItTemporaryOrLongterm = new InteractiveProcessMergeData()
            {
                Question = "What is the medical situation? Is it temporary or long-term?"
            };
            WhatAreYourDifficultiesAndRestrictions = new InteractiveProcessMergeData()
            {
                Question = "What are your difficulties and restrictions?"
            };
            DoesThisImpactOrLimitTheEmployeesAbilityToPerformAnyMajorLifeActivities = new InteractiveProcessMergeData()
            {
                Question = "Does this impact or limit the employee’s ability to perform any major life activities?"
            };
            AreYouHavingDifficultyPerformingYourJobFunctionsRightNow = new InteractiveProcessMergeData()
            {
                Question = "Are you having difficulty performing your job functions right now?"
            };
            WhatSpecificSupportOrAccommodationIsTheEmployeeRequesting = new InteractiveProcessMergeData()
            {
                Question = "What specific support or accommodation is the employee requesting?"
            };
            HaveYouHadAccommodationsForThisLimitationInThePastIfYesWhatWereTheModificationsAndDoYouThinkTheyWereEffective = new InteractiveProcessMergeData()
            {
                Question = "Have you had accommodations for this limitation in the past? If yes, what were the modifications and do you think they were effective?"
            };
            DoesTheEmployeeHaveAnySuggestionsForAccommodationsTheyBelieveMayBeHelpfulWhatDoTheySeeAsASolutionOrAccommodationToKeepWorking = new InteractiveProcessMergeData()
            {
                Question = "Does the employee have any suggestions for accommodations they believe may be helpful? What do they see as a solution or accommodation to keep working?"
            };
            IsYourManagerAwareOfTheEmployeesNeedDoTheyFeelHesheIsSupportiveOfTheNeed = new InteractiveProcessMergeData()
            {
                Question = "Is your manager aware of the employees need? Do they feel he/she is supportive of the need?"
            };
            IsTheEmployeeOnALeaveOfAbsenceOrUsingALeaveIntermittentlyDoTheyNeedOne = new InteractiveProcessMergeData()
            {
                Question = "Is the employee on a Leave of Absence or using a leave intermittently? Do they need one?"
            };
            IsTheEmployeeAbleToWorkRightNow = new InteractiveProcessMergeData()
            {
                Question = "Is the employee able to work right now?"
            };
            DoesTheEmployeeFeelTheJobDescriptionListingTheEssentialJobFunctionsIsDescriptiveOfTheirWorkIfNoPleaseListTheDescriptiveOrWorkThatIsMissing = new InteractiveProcessMergeData()
            {
                Question = "Does the employee feel the job description listing the essential job functions is descriptive of their work? If no, please list the descriptive or work that is missing."
            };
            WhatIsTheMedicalSituation = new InteractiveProcessMergeData()
            {
                Question = "What is the medical situation?"
            };
            HasTheEmployeeHadAnyAccommodationsForThisLimitationInThePastIfYesWhatWereTheModificationsAndDoYouThinkTheyWereEffective = new InteractiveProcessMergeData()
            {
                Question = "Has the employee had any accommodations for this limitation in the past? If yes, what were the modifications and do you think they were effective?"
            };
            DoesTheEmployeeHaveAnySuggestionsForAccommodationsTheyBelieveMayBeHelpfulInAllowingThemToWork = new InteractiveProcessMergeData()
            {
                Question = "Does the employee have any suggestions for accommodations they believe may be helpful in allowing them to work?"
            };
            HasTheEmployeeHadAccommodationsForThisLimitationInThePastIfYesWhatWereTheModificationsAndDoYouThinkTheyWereEffective = new InteractiveProcessMergeData()
            {
                Question = "Has the employee had accommodations for this limitation in the past? If yes, what were the modifications and do you think they were effective?"
            };
            DidHROrOpsAssessTheEmployeeIfYesAsOfWhatDate = new InteractiveProcessMergeData()
            {
                Question = "Did HR or Ops assess the employee? If yes as of what date?"
            };

            IsThereAnyThreatPleaseDescribe = new InteractiveProcessMergeData()
            {
                Question = "Is there any threat? Please describe"
            };

            DoesTheEmployeeBelieverThereAreAnyOutstandingIssuesOrConcernsWeShouldBeAwareOfToBetterSupportThemOrAddressThisMatter = new InteractiveProcessMergeData()
            {
                Question = "Does the employee believer there are any outstanding issues or concerns we should be aware of to better support them or address this matter?"
            };
            DidYouSpeakWithTheEmployee = new InteractiveProcessMergeData()
            {
                Question = "Did you speak with the employee?"
            };
            DoesTheEmployeeNeedAnyAccommodations = new InteractiveProcessMergeData()
            {
                Question = "Does the employee need any accommodations?"
            };
            IsTheAccommodationShortTermLongTermOrPermanent = new InteractiveProcessMergeData()
            {
                Question = " Is the accommodation Short Term, Long Term or Permanent?"
            };
            HaveYouConsideredTheRequestedAccommodation = new InteractiveProcessMergeData()
            {
                Question = " Have you considered the requested accommodation?"
            };
            AccommodatedWorkAvailableAtTheSiteIfNoCaptureWhyAndThenDocumentNextSteps = new InteractiveProcessMergeData()
            {
                Question = "Accommodated work available at the site? If No, capture why and then document next steps"
            };
            DoesTheLOAATeamAgreeWithSiteThatNoWorkIsAvailableYesNoifNoThenDocumentIfWeAreReversingInitialResponse = new InteractiveProcessMergeData()
            {
                Question = "Does the LOAA team agree with site that no work is available? Yes/No…..if no, then document if we are reversing initial response."
            };
            IsThereANeedForAnIndependentMedicalEvaluation = new InteractiveProcessMergeData()
            {
                Question = "Is there a need for an independent medical evaluation?"
            };
            IsTheAccommodationPhysicalHoursOrBoth = new InteractiveProcessMergeData()
            {
                Question = "Is the accommodation Physical, Hours or Both?"
            };
            DidYouTalkToTheADAContactAndorSupervisordocumentInformationReceivedDuringIntakeOrSubsequentDiscussions = new InteractiveProcessMergeData()
            {
                Question = "Did you talk to the ADA contact and/or supervisor? (document information received during intake or subsequent discussions)"
            };
            DidYouTalkToTheEmployeedocumentAdditionalInformationReceivedWhenCaseManagerOutreachesToEmployeePostemployerIntake = new InteractiveProcessMergeData()
            {
                Question = "Did you talk to the employee (document additional information received when Case Manager outreaches to employee post-employer intake)"
            };
            HasMedicalInformationBeenReceived = new InteractiveProcessMergeData()
            {
                Question = "Has medical information been received?"
            };
            WhatAreTheReasonableAccommodationsBeingConsideredincludesEmployeeRequestForAccommodationEmployerSuggestionsAndPrudentialRecommendations = new InteractiveProcessMergeData()
            {
                Question = "What are the reasonable accommodations being considered? (includes employee request for accommodation, employer suggestions, and Prudential recommendations)"
            };
            HasTheInteractiveDiscussionTakenPlacetranscriptOfTheDiscussion = new InteractiveProcessMergeData()
            {
                Question = "Has the interactive discussion taken place? (\"transcript\" of the discussion)"
            };
            HasAnAccommodationDeterminationBeenMadeOutcomeOfTheEmployersInteractiveDiscussion = new InteractiveProcessMergeData()
            {
                Question = "Has an accommodation determination been made - outcome of the employer's interactive discussion?"
            };
            WhatIsTheAccommodationPlanincludePlanSignoffAndMonitoring = new InteractiveProcessMergeData()
            {
                Question = "What is the accommodation plan? (include plan, sign-off and monitoring)"
            };

            intractiveQuestions = new List<InteractiveProcessMergeData>(){
                            DidYouTalkToTheEmployee,
                            DidYouObtainMedicalDocumentationOfTheNeedForTheAccommodation,
                            HaveYouConsideredTheRequestedAccommodations,
                            IsTheAccommodationReasonable,
                            HaveYouDiscussedItWithHumanResources,
                            HaveYouDiscussedItWithOperations,
                            DidYouCompleteTheAccommodationDetermination,
                            DidYouCommunicateTheAccommodationDeterminationToTheEmployee,
                            MonitorTheAccommodation,
                            WhatAreTheEmployeesDifficultiesAndRestrictions,
                            IsTheEmployeeHavingDifficultyPerformingTheJobFunctionsRightNow,
                            DoesTheEmployeeBelieveThereAreAnyOutstandingIssuesOrConcernsWeShouldBeAwareOfToBetterSupportThemOrToAddressThisMatter,
                            IsThereAnyAdditionalInformationTheEmployeeWouldLikeToShare,
                            WhatIsTheMedicalSituationIsItTemporaryOrLongterm,
                            WhatAreYourDifficultiesAndRestrictions,
                            DoesThisImpactOrLimitTheEmployeesAbilityToPerformAnyMajorLifeActivities,
                            AreYouHavingDifficultyPerformingYourJobFunctionsRightNow,
                            WhatSpecificSupportOrAccommodationIsTheEmployeeRequesting,
                            HaveYouHadAccommodationsForThisLimitationInThePastIfYesWhatWereTheModificationsAndDoYouThinkTheyWereEffective,
                            DoesTheEmployeeHaveAnySuggestionsForAccommodationsTheyBelieveMayBeHelpfulWhatDoTheySeeAsASolutionOrAccommodationToKeepWorking,
                            IsYourManagerAwareOfTheEmployeesNeedDoTheyFeelHesheIsSupportiveOfTheNeed,
                            IsTheEmployeeOnALeaveOfAbsenceOrUsingALeaveIntermittentlyDoTheyNeedOne,
                            IsTheEmployeeAbleToWorkRightNow,
                            DoesTheEmployeeFeelTheJobDescriptionListingTheEssentialJobFunctionsIsDescriptiveOfTheirWorkIfNoPleaseListTheDescriptiveOrWorkThatIsMissing,
                            WhatIsTheMedicalSituation,
                            HasTheEmployeeHadAnyAccommodationsForThisLimitationInThePastIfYesWhatWereTheModificationsAndDoYouThinkTheyWereEffective,
                            DoesTheEmployeeHaveAnySuggestionsForAccommodationsTheyBelieveMayBeHelpfulInAllowingThemToWork,
                            HasTheEmployeeHadAccommodationsForThisLimitationInThePastIfYesWhatWereTheModificationsAndDoYouThinkTheyWereEffective,
                            DidHROrOpsAssessTheEmployeeIfYesAsOfWhatDate,
                            IsThereAnyThreatPleaseDescribe,
                            DoesTheEmployeeBelieverThereAreAnyOutstandingIssuesOrConcernsWeShouldBeAwareOfToBetterSupportThemOrAddressThisMatter,
                            DidYouSpeakWithTheEmployee,
                            DoesTheEmployeeNeedAnyAccommodations,
                            IsTheAccommodationShortTermLongTermOrPermanent,
                            HaveYouConsideredTheRequestedAccommodation,
                            AccommodatedWorkAvailableAtTheSiteIfNoCaptureWhyAndThenDocumentNextSteps,
                            DoesTheLOAATeamAgreeWithSiteThatNoWorkIsAvailableYesNoifNoThenDocumentIfWeAreReversingInitialResponse,
                            IsThereANeedForAnIndependentMedicalEvaluation,
                            IsTheAccommodationPhysicalHoursOrBoth,
                            DidYouTalkToTheADAContactAndorSupervisordocumentInformationReceivedDuringIntakeOrSubsequentDiscussions,
                            DidYouTalkToTheEmployeedocumentAdditionalInformationReceivedWhenCaseManagerOutreachesToEmployeePostemployerIntake,
                            HasMedicalInformationBeenReceived,
                            WhatAreTheReasonableAccommodationsBeingConsideredincludesEmployeeRequestForAccommodationEmployerSuggestionsAndPrudentialRecommendations,
                            HasTheInteractiveDiscussionTakenPlacetranscriptOfTheDiscussion,
                            HasAnAccommodationDeterminationBeenMadeOutcomeOfTheEmployersInteractiveDiscussion,
                            WhatIsTheAccommodationPlanincludePlanSignoffAndMonitoring,
            };
        }

        #endregion
    }
}
