﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Customers;

namespace AbsenceSoft.Rendering.Templating.MailMerge
{
    public class CaseMergeData
    {
        public CaseMergeData()
        {
            CustomFields = new List<CustomFieldMergeData>();
            ActiveAccommodation = new AccommodationMergeData();
            Flags = new Flags();
            CaseReporter = new ContactMergeData();
            AuthorizedSubmitter = new ContactMergeData();
            CompanyAddress = new AddressMergeData();
        }

        public void CopyCaseData(Case c, List<CaseNote> notes)
        {
            if (c == null)
                return;

            this.CaseNumber = c.CaseNumber;
            this.Reason = c.Reason != null ? c.Reason.Name : c.Status == CaseStatus.Inquiry ? "Inquiry" : null;
            this.Code = c.Reason.Code;
            this.Type = c.CaseType;
            this.Eligibility = c.Summary.Eligibility;
            this.Determination = c.Summary.Determination;
            this.CreatedDate = c.CreatedDate;
            this.StartDate = c.StartDate;
            this.ExtendedDate = c.ExtendedDate.HasValue ? (c.ExtendedDate.Value).ToShortDateString() : null;
            this.EndDate = c.EndDate;
            this.LastUpdated = c.ModifiedDate;
            this.ExpectedRTW = GetCaseEventDate(c, CaseEventType.EstimatedReturnToWork);
            this.ActualRTW = GetCaseEventDate(c, CaseEventType.ReturnToWork);
            this.FmlaExhaustDate = c.Summary.FmlaExhaustDate;
            this.MinApprovedThruDate = c.Summary.MinApprovedThruDate;
            this.MaxApprovedThruDate = c.Summary.MaxApprovedThruDate;
            this.ApprovalNotificationDateSent = c.Summary.ApprovalNotificationDateSent.HasValue ? c.Summary.ApprovalNotificationDateSent.Value.ToShortDateString() : null;
            this.IsFMLA = c.IsFMLA;
            this.IsConsecutive = c.ContainsCaseType(CaseType.Consecutive);
            this.IsIntermittent = c.ContainsCaseType(CaseType.Intermittent);
            this.IsReduced = c.ContainsCaseType(CaseType.Reduced);
            this.IsAdministrative = c.ContainsCaseType(CaseType.Administrative);
            this.IsEligible = (c.Summary != null && c.Summary.Eligibility == EligibilityStatus.Eligible);
            this.FMLARuleWorked12MonthsNotMet = NotMeetsRuleExpression(c, "Has1YearOfService()");
            this.FMLARuleWorked1250HoursNotMet = NotMeetsRuleExpression(c, "TotalHoursWorkedLast12Months()");
            this.FMLARuleWorked1250HoursValue = NotMeetsRuleExpressionValue(c, "TotalHoursWorkedLast12Months()");
            this.FMLARule50EmployeesIn75MileRadiusNotMet = NotMeetsRuleExpression(c, "Meets50In75MileRule()");
            this.FMLARuleRelationshipToEmployeeNotMet = NotMeetsRuleExpression(c, "CaseRelationshipType");
            this.FMLARuleLeaveWithin12MonthsNotMet = NotMeetsRuleExpression(c, "NumberOfMonthsFromDeliveryDate()");
            this.BondingStartDate = GetCaseEventDate(c, CaseEventType.BondingStartDate);
            this.BondingEndDate = GetCaseEventDate(c, CaseEventType.BondingEndDate);
            this.StateRuleWorkedLengthofserviceNotMet = NotMeetsRuleExpression(c, "Has1YearOfService()", true);
            this.StateRuleWorkedHoursNotMet = NotMeetsRuleExpression(c, "TotalHoursWorkedLast12Months()", true);
            this.StateRuleWorked1250HoursValue = NotMeetsRuleExpressionValue(c, "TotalHoursWorkedLast12Months()", true);
            this.StateRule50EmployeesIn75MileRadiusNotMet = NotMeetsRuleExpression(c, "Meets50In75MileRule()", true);
            this.StateRuleRelationshipToEmployeeNotMet = NotMeetsRuleExpression(c, "CaseRelationshipType", true);
            this.StateRuleLeaveWithin12MonthsNotMet = NotMeetsRuleExpression(c, "NumberOfMonthsFromDeliveryDate()", true);
            if (c.Summary != null)
            {
                this.FmlaProjectedUsage = c.Summary.FmlaProjectedUsageText;
                this.PolicyProjectedUsage = c.Summary.PolicyProjectedUsageText;
            }

            CopyFmlaPolicy(c);

            if (c.CustomFields != null)
            {
                this.CustomFields = c.CustomFields.Select(s => new CustomFieldMergeData()
                {
                    Name = s.Name,
                    Value = s.SelectedValue == null ? "Unknown" : s.SelectedValue.ToString()
                }).ToList();
            }

            var exhaustedPolicies = from s in c.Segments
                                    from ap in s.AppliedPolicies
                                    where ap.FirstExhaustionDate.HasValue
                                    select ap.Policy;

            var policyCodeExhaustedFlags = exhaustedPolicies
                .Select(s => s.Code)
                .Distinct()
                .Select(s => string.Format("{0}-EXHAUSTED", s));
            var policyTypeExhaustedFlags = exhaustedPolicies
                .Select(s => s.PolicyType.ToString())
                .Distinct()
                .Select(s => string.Format("{0}-EXHAUSTED", s));
            Flags.AddRange(policyCodeExhaustedFlags.Union(policyTypeExhaustedFlags));

            if (c.AccommodationRequest != null && c.AccommodationRequest.Accommodations != null && c.AccommodationRequest.Accommodations.Count > 0)
                this.ActiveAccommodation.CopyAccommodationData(c.AccommodationRequest.Accommodations.OrderByDescending(a => a.CreatedDate).FirstOrDefault(), notes);

            this.CaseClosedDate = GetClosedDate(c);
            this.CaseCloseReason = c.ClosureReason.ToString().SplitCamelCaseString();
            if (c.ClosureReason == CaseClosureReason.Other)
                this.CaseCloseReason = string.IsNullOrWhiteSpace(c.ClosureReasonDetails) ? this.CaseCloseReason : string.Concat(this.CaseCloseReason, " - ", c.ClosureReasonDetails);

            this.CaseReporter.CopyContactData(c.CaseReporter);
            this.AuthorizedSubmitter.CopyContactData(c.AuthorizedSubmitter);
            this.NonFmlaExhaustDate = (DateTime?)c.Segments
                .SelectMany(s => s.AppliedPolicies)
                .Where(ap => ap.Policy.PolicyType != PolicyType.FMLA && ap.Status == EligibilityStatus.Eligible)
                .Select(ap => ap.FirstExhaustionDate)
                .Max();
            CopyContactInformation(c.Customer);
        }

        private void CopyFmlaPolicy(Case c)
        {
            var fmlaPolicy = c.Segments.OrderByDescending(cs => cs.CreatedDate).First().AppliedPolicies.Where(p => p.Policy.Code == "FMLA").FirstOrDefault();
            if (fmlaPolicy == null)
                return;


            this.FMLAPolicy = new PolicyMergeData();
            this.FMLAPolicy.CopyPolicyData(fmlaPolicy);
            //FMLA Policy Start Date End Date should be approved dates.
            this.FMLAPolicy.StartDate = null;
            this.FMLAPolicy.EndDate = null;
            if (fmlaPolicy.Usage != null && fmlaPolicy.Usage.Any(policy => policy.Determination == AdjudicationStatus.Approved))
            {
                this.FMLAPolicy.StartDate = fmlaPolicy.Usage.Where(policy => policy.Determination == AdjudicationStatus.Approved).Min(policy => policy.DateUsed);
                this.FMLAPolicy.EndDate = fmlaPolicy.Usage.Where(policy => policy.Determination == AdjudicationStatus.Approved).Max(policy => policy.DateUsed);
            }

            if (fmlaPolicy.Usage != null && fmlaPolicy.Usage.Any(policy => policy.Determination == AdjudicationStatus.Denied))
            {
                this.FMLAPolicy.DeniedReason = fmlaPolicy.Usage.Where(policy => policy.Determination == AdjudicationStatus.Denied).FirstOrDefault().DenialReasonName;
                this.FMLAPolicy.DeniedReasonCode = fmlaPolicy.Usage.Where(policy => policy.Determination == AdjudicationStatus.Denied).FirstOrDefault().DenialReasonCode;
            }
            if (fmlaPolicy.Status == EligibilityStatus.Ineligible)
            {
                List<AppliedRule> failedRules = fmlaPolicy.RuleGroups.Where(rg => !rg.Pass).SelectMany(rg => rg.Rules).Where(r => !r.Pass).ToList();
                List<string> failedRulesText = new List<string>();
                foreach (AppliedRule failedRule in failedRules)
                {
                    if (failedRule.Overridden)
                        failedRulesText.Add(string.Concat(failedRule.Rule.Description, " FAILED", ((!string.IsNullOrEmpty(failedRule.OverrideValue)) ? string.Concat(", appended as \"", failedRule.OverrideValue, "\"") : ""), ((!string.IsNullOrEmpty(failedRule.OverrideNotes)) ? string.Concat(", because \"", failedRule.OverrideNotes, ".\"") : "")));
                    else
                        failedRulesText.Add(string.Concat(failedRule.Rule.Description, " FAILED, was ", failedRule.ActualValue));
                }
                this.FMLAPolicy.IneligibleReason = string.Join("\n\n", failedRulesText);
            }

        }

        private void CopyContactInformation(Customer customer)
        {
            if (customer == null || customer.Contact == null)
                return;

            CompanyAddress.CopyAddressData(customer.Contact.Address);
            string phone = customer.Contact.WorkPhone;
            string fax = customer.Contact.Fax;
            if (!string.IsNullOrEmpty(phone))
                CompanyPhone = phone.FormatPhone();

            if (!string.IsNullOrEmpty(fax))
                CompanyFax = fax.FormatPhone();
        }


        private DateTime? GetClosedDate(Case c)
        {
            if (c == null || c.CaseEvents == null || c.Status != CaseStatus.Closed)
            {
                return new DateTime?();
            }
            return GetMostRecentEventDate(c, CaseEventType.CaseClosed);
        }

        private DateTime? GetMostRecentEventDate(Case c, CaseEventType eventType)
        {
            if (c == null || c.CaseEvents == null)
            {
                return new DateTime?();
            }

            var events = c.CaseEvents.Where(e => e.EventType == eventType).ToList();
            return events.Any() ? events.Max(e => e.EventDate) : new DateTime?();
        }


      
        private DateTime? GetCaseEventDate(Case c, CaseEventType eventType)
        {
            CaseEvent ev = c.FindCaseEvent(eventType);
            if (ev != null)
                return ev.EventDate;

            return null;
        }

        private bool NotMeetsRuleExpression(Case c, string ruleExpression, bool isStateFMLA = false)
        {
            if (c.Segments == null)
                return true;

            CaseSegment newestSegment = c.Segments.OrderByDescending(s => s.CreatedDate).First();
            if (newestSegment.AppliedPolicies == null)
                return true;

            AppliedPolicy fmlaPolicy = !isStateFMLA ? newestSegment.AppliedPolicies.Where(p => p.Policy.Code == "FMLA").FirstOrDefault() : newestSegment.AppliedPolicies.Where(p => p.Policy.Code != "FMLA").FirstOrDefault();
            if (fmlaPolicy == null)
                return true;

            return fmlaPolicy.RuleGroups.SelectMany(r => r.Rules).Where(r => r.Fail && string.Compare(r.Rule.LeftExpression, ruleExpression, true) == 0).Any();
        }

        /// <summary>
        /// Gets ActualValue String of Any rule associated with a Case
        /// </summary>
        /// <param name="c"></param>
        /// <param name="ruleName"></param>
        /// <returns></returns>
        private string NotMeetsRuleExpressionValue(Case c, string ruleExpression, bool isStateFMLA = false)
        {
            if (c.Segments == null)
                return string.Empty;

            CaseSegment newestSegment = c.Segments.OrderByDescending(s => s.CreatedDate).First();
            if (newestSegment.AppliedPolicies == null)
                return string.Empty;

            AppliedPolicy fmlaPolicy = !isStateFMLA ? newestSegment.AppliedPolicies.Where(p => p.Policy.Code == "FMLA").FirstOrDefault() : newestSegment.AppliedPolicies.Where(p => p.Policy.Code != "FMLA").FirstOrDefault();
            if (fmlaPolicy == null)
                return string.Empty;

            var selectedRule = fmlaPolicy.RuleGroups.SelectMany(r => r.Rules).Where(r => string.Compare(r.Rule.LeftExpression, ruleExpression, true) == 0).FirstOrDefault();
            if (selectedRule != null)
            {
                return selectedRule.ActualValueString;
            }
            return string.Empty;
        }

        public Flags Flags { get; set; }

        public string CaseNumber { get; set; }
        public string Reason { get; set; }
        public string Code { get; set; }
        public string Type { get; set; }
        public EligibilityStatus Eligibility { get; set; }
        public AdjudicationStatus Determination { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? LastUpdated { get; set; }
        public DateTime? ExpectedRTW { get; set; }
        public DateTime? ActualRTW { get; set; }
        public string ApprovalNotificationDateSent { get; set; }
        public DateTime? FmlaExhaustDate { get; set; }
        public DateTime? NonFmlaExhaustDate { get; set; }
        public DateTime? MinApprovedThruDate { get; set; }
        public DateTime? MaxApprovedThruDate { get; set; }

        public DateTime? BondingStartDate { get; set; }
        public DateTime? BondingEndDate { get; set; }
        public string ExtendedDate { get; set; }
        /// <summary>
        /// Gets or sets the leave used at end of leave.
        /// </summary>
        /// <value>
        /// The leave used at end of leave.
        /// </value>
        public string FmlaProjectedUsage { get; set; }

        public string PolicyProjectedUsage { get; set; }
        public int? ExpectedDurationDays
        {
            get
            {
                if (MinApprovedThruDate == null || MaxApprovedThruDate == null)
                    return null;

                return (MaxApprovedThruDate - MinApprovedThruDate).Value.Days;
            }
        }
        public bool IsFMLA { get; set; }
        public bool HasFMLAPolicy
        {
            get
            {
                return FMLAPolicy != null && FMLAPolicy.Status == EligibilityStatus.Eligible;
            }
        }

        public bool IsFMLAPolicyExists
        {
            get
            {
                return FMLAPolicy != null;
            }
        }
        public bool IsConsecutive { get; set; }        
        public bool IsIntermittent { get; set; }
        public bool IsReduced { get; set; }
        public bool IsAdministrative { get; set; }
        public bool IsEligible { get; set; }
        public bool FMLARuleWorked12MonthsNotMet { get; set; }
        public bool FMLARuleWorked1250HoursNotMet { get; set; }
        public string FMLARuleWorked1250HoursValue { get; set; }
        public bool FMLARule50EmployeesIn75MileRadiusNotMet { get; set; }
        public bool FMLARuleRelationshipToEmployeeNotMet { get; set; }
        public bool FMLARuleLeaveWithin12MonthsNotMet { get; set; }

        public PolicyMergeData FMLAPolicy { get; set; }
        public AccommodationMergeData ActiveAccommodation { get; set; }

        public List<CustomFieldMergeData> CustomFields { get; set; }

        public DateTime? CaseClosedDate { get; set; }
        public string CaseCloseReason { get; set; }
        public AddressMergeData CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyFax { get; set; }
        public ContactMergeData CaseReporter { get; set; }
        public ContactMergeData AuthorizedSubmitter { get; set; }
        public override string ToString()
        {
            return CaseNumber ?? "Unknown";
        }
        public bool StateRuleWorkedHoursNotMet { get; set; }
        public bool StateRule50EmployeesIn75MileRadiusNotMet { get; set; }
        public bool StateRuleWorkedLengthofserviceNotMet { get; set; }
        public bool StateRuleRelationshipToEmployeeNotMet { get; set; }
        public bool StateRuleLeaveWithin12MonthsNotMet { get; set; }
        public string StateRuleWorked1250HoursValue { get; set; }
        public bool HasStatePolicy
        {
            get
            {
                return FMLAPolicy != null && FMLAPolicy.Code != "FMLA";
            }
        }
    }
}
