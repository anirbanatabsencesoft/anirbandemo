﻿using AbsenceSoft.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Rendering.Templating.MailMerge
{
    public class AddressMergeData
    {
        public void CopyAddressData(Address a)
        {
            if (a == null)
                return;

            Address1 = a.Address1;
            Address2 = a.Address2;
            City = a.City;
            State = a.State;
            PostalCode = a.PostalCode;
            Country = a.Country;
        }

        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }

        public override string ToString()
        {
            StringBuilder address = new StringBuilder();
            if (!string.IsNullOrWhiteSpace(Address1))
                address.AppendLine(Address1);
            if (!string.IsNullOrWhiteSpace(Address2))
                address.AppendLine(Address2);

            StringBuilder csz = new StringBuilder();
            if (!string.IsNullOrWhiteSpace(City))
                csz.Append(City);
            if (!string.IsNullOrWhiteSpace(State))
                csz.AppendFormat("{0}{1}", csz.Length > 0 ? ", " : "", State);
            if (!string.IsNullOrWhiteSpace(PostalCode))
                csz.AppendFormat("{0}{1}", csz.Length > 0 ? " " : "", PostalCode);
            if (csz.Length > 0)
                address.AppendLine(csz.ToString());

            return address.ToString().Replace(Environment.NewLine, "\v");
        }
    }
}
