﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Communications;

namespace AbsenceSoft.Rendering.Templating.MailMerge
{
    public class PaperworkMergeData
    {
        public PaperworkMergeData()
        {
        }
        public PaperworkMergeData(CommunicationPaperwork communicationPaperwork, DateTime? firstPaperworkDueDate, DateTime? lastPaperworkDueDate)
        {
            Name = communicationPaperwork.Paperwork.Name;
            DueDate = communicationPaperwork.DueDate;
            FirstCasePaperworkDueDate = communicationPaperwork.DueDate;
            LastCasePaperworkDueDate = lastPaperworkDueDate;
        }
        public string Name { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? FirstCasePaperworkDueDate { get; set; }
        public DateTime? LastCasePaperworkDueDate { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }
}
