﻿using AbsenceSoft.Data.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Rendering.Templating.MailMerge
{
    public class CustomerMergeData
    {
        public CustomerMergeData()
        {
            Address = new AddressMergeData();
        }

        internal void CopyCustomerData(Customer customer)
        {
            if (customer == null)
                return;

            Name = customer.Name;
            if (customer.Contact != null)
            {
                Email = customer.Contact.Email;
                Phone = customer.Contact.WorkPhone.FormatPhone();
                Fax = customer.Contact.Fax.FormatPhone();
                Address.CopyAddressData(customer.Contact.Address);
            }
        }

        public AddressMergeData Address { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }

        public override string ToString()
        {
            return Name ?? "Unknown";
        }
    }
}
