﻿using System.Resources;

namespace AT.Data.Resource
{
    /// <summary>
    /// The class for implementing Embedded resources
    /// </summary>
    public class EmbeddedResourceRepository : IResourceRepository
    {
        /// <summary>
        /// Getting the resource based on module & key
        /// </summary>
        /// <param name="module"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        string IResourceRepository.Get(string module, string key)
        {
            module = $"AT.Data.Resource.Resources.{module}";
            var resourceManager = new ResourceManager(module, typeof(EmbeddedResourceRepository).Assembly);
            return resourceManager.GetString(key, System.Threading.Thread.CurrentThread.CurrentCulture);
        }
    }
}
