﻿namespace AT.Data.Resource
{
    /// <summary>
    /// The interface for resource repository
    /// </summary>
    public interface IResourceRepository
    {
        /// <summary>
        /// Get the resource based on the ID
        /// </summary>
        /// <param name="module"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        string Get(string module, string key);
    }
}
