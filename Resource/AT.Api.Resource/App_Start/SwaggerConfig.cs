using System.Web.Http;
using WebActivatorEx;
using AT.Api.Resource;
using Swashbuckle.Application;
using AT.Api.Core;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace AT.Api.Resource
{
    /// <summary>
    /// Resource API Swagger Config
    /// </summary>
    public static class SwaggerConfig
    {
        /// <summary>
        /// Registers the Resource API with Swagger so it can be documented
        /// </summary>
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;
            BaseSwaggerConfig.Register("Resource", thisAssembly);
        }
    }
}
