﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using AT.Api.Core;
using AT.Core.Entities;
using AT.Provider.Resource.Manager;

namespace AT.Api.Resource.Controllers
{
    /// <summary>
    /// The controller for managing resources through Web API
    /// </summary>
    public class ResourceController : BaseApiController
    {
        /// <summary>
        /// The api function to get the resource based on module and key
        /// </summary>
        /// <param name="module"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Resource/Get/{module}/{key}")]
        public async Task<string> Get(string module, string key)
        {
            return await Task.FromResult(ResourceProviderManager.Default.Get(module, key));
        }

        /// <summary>
        /// The api function to get the list of languages
        /// </summary> 
        /// <returns></returns>
        [HttpGet]
        [Route("Resource/Languages")]
        public async Task<IList<Language>> GetLanguages()
        {
            var languages = ResourceProviderManager.Default.GetLanguages();

            if(languages == null)
                return new List<Language>();

            return await Task.FromResult(languages);
        }

        /// <summary>
        /// The api function to get the list of time zones
        /// </summary> 
        /// <returns></returns>
        [HttpGet]
        [Route("Resource/Timezones")]
        public async Task<IList<Timezone>> GetTimeZones()
        {
            var timezones = ResourceProviderManager.Default.GetTimeZones();

            if (timezones == null)
                return new List<Timezone>();

            return await Task.FromResult(timezones);
        }

        /// <summary>
        /// The api function to convert the datetime to local timezone
        /// </summary> 
        /// <returns></returns>
        [HttpGet]
        [Route("Resource/Timezones/ConvertToLocal/{dateTime}/{sourceTimeZoneName}")]
        public async Task<DateTime> ConvertToLocalTimezone(string dateTime, string sourceTimeZoneName)
        {
            if (string.IsNullOrEmpty(dateTime) || string.IsNullOrEmpty(sourceTimeZoneName))
            {
                throw new ArgumentNullException(dateTime, sourceTimeZoneName);
            }

            var sourceTimeZoneInfo = TimeZoneInfo.GetSystemTimeZones().FirstOrDefault(a => a.Id == sourceTimeZoneName);

            if (sourceTimeZoneInfo == null)
                throw new InvalidTimeZoneException();

            DateTime dateTimeOut;

            if (DateTime.TryParse(dateTime, out dateTimeOut))
            {
                DateTime.SpecifyKind(dateTimeOut, DateTimeKind.Unspecified);
                dateTimeOut = TimeZoneInfo.ConvertTime(dateTimeOut, sourceTimeZoneInfo, TimeZoneInfo.Local);
            }
            return await Task.FromResult(dateTimeOut);
        }

        /// <summary>
        /// The api function to convert the local datetime to specified timezone
        /// </summary> 
        /// <returns></returns>
        [HttpGet]
        [Route("Resource/Timezones/ConvertFromLocal/{dateTime}/{destinationTimeZoneName}")]
        public async Task<DateTime> ConvertFromLocalTimezone(string dateTime, string destinationTimeZoneName)
        {
            if (string.IsNullOrEmpty(dateTime) || string.IsNullOrEmpty(destinationTimeZoneName))
            {
                throw new ArgumentNullException(dateTime, destinationTimeZoneName);
            }

            var destinationTimeZoneInfo = TimeZoneInfo.GetSystemTimeZones().FirstOrDefault(a => a.Id == destinationTimeZoneName);


            DateTime dateTimeOut;

            if (DateTime.TryParse(dateTime, out dateTimeOut))
            {
                DateTime.SpecifyKind(dateTimeOut, DateTimeKind.Unspecified);
                dateTimeOut = TimeZoneInfo.ConvertTime(dateTimeOut, TimeZoneInfo.Local, destinationTimeZoneInfo);
            }
            return await Task.FromResult(dateTimeOut);
        }
    }
}

