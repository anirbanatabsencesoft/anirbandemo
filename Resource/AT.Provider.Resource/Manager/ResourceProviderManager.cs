﻿using System.Configuration;
using System.Web.Configuration;
using AT.Provider.Resource.Collection;
using AT.Provider.Resource.Configuration;
using AT.Provider.Resource.Providers;

namespace AT.Provider.Resource.Manager
{
    /// <summary>
    /// The class to manage resource providers
    /// </summary>
    public class ResourceProviderManager
    {

        protected ResourceProviderManager()
        {

        }
        #region private variables
        private static BaseResourceProvider _default;
        private static ProviderSettingsCollection _providerSettings;
        private static ResourceProviderCollection resourceProviderCollection;
        #endregion

        static ResourceProviderManager()
        {
            Initialize();
        }

        /// <summary>
        /// The provider setting property
        /// </summary>
        public static ProviderSettingsCollection ProviderSettings
        {
            get => _providerSettings;
        }

        /// <summary>
        /// Contains all the Providers configured
        /// </summary>
        public static ResourceProviderCollection Providers
        {
            get => resourceProviderCollection;
        }

        /// <summary>
        /// Returns the default configured cache provider
        /// </summary>
        public static BaseResourceProvider Default => _default;

        /// <summary>
        /// Fetches the configuration information and populates cacheProviderCollection,
        /// </summary>
        private static void Initialize()
        {
            ResourceProviderConfiguration configSection = (ResourceProviderConfiguration)ConfigurationManager.GetSection("ResourceProviders");
            if (configSection == null)
                throw new ConfigurationErrorsException("Resource provider section is not set.");

            resourceProviderCollection = new ResourceProviderCollection();
            ProvidersHelper.InstantiateProviders(configSection.Providers, resourceProviderCollection, typeof(BaseResourceProvider));

            _providerSettings = configSection.Providers;

            if (resourceProviderCollection[configSection.DefaultProviderName] == null)
                throw new ConfigurationErrorsException("Default data provider is not set.");
            _default = resourceProviderCollection[configSection.DefaultProviderName];
        }
    }
}
