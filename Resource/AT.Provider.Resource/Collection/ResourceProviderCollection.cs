﻿using System.Configuration.Provider;
using AT.Provider.Resource.Providers;

namespace AT.Provider.Resource.Collection
{
    /// <summary>
    /// The class to store Resource Providers
    /// </summary>
    public class ResourceProviderCollection : ProviderCollection
    {
        /// <summary>
        /// Returns Provider object of given Provider name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        new public BaseResourceProvider this[string name] => (BaseResourceProvider)base[name];
    }
}
