﻿using System.Configuration;

namespace AT.Provider.Resource.Configuration
{
    /// <summary>
    /// The provider configuration class
    /// </summary>
    public class ResourceProviderConfiguration : ConfigurationSection
    {
        /// <summary>
        /// Contains collection of Providers
        /// </summary>
        [ConfigurationProperty("providers")]
        public ProviderSettingsCollection Providers
        {
            get
            {
                return (ProviderSettingsCollection)base["providers"];
            }
        }

        /// <summary>
        /// Shows the Default Provider's Name
        /// </summary>
        [ConfigurationProperty("default", DefaultValue = "Embedded")]
        public string DefaultProviderName
        {
            get
            {
                return base["default"] as string;
            }
        }
    }
}
