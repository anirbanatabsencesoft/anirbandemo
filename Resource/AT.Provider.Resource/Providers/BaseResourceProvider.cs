﻿using System.Collections.Generic;
using System.Configuration.Provider;
using AT.Core.Entities;

namespace AT.Provider.Resource.Providers
{
    /// <summary>
    /// This class is the base class for all the language and resource providers
    /// </summary>
    public abstract class BaseResourceProvider : ProviderBase
    {
        /// <summary>
        /// The function to get the resource based on module and key
        /// </summary>
        /// <param name="module"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public abstract string Get(string module, string key);

        /// <summary>
        /// The function to get the list of languages
        /// </summary>
        /// <returns></returns>
        public abstract IList<Language> GetLanguages();

        /// <summary>
        /// The function to get the list of time zones
        /// </summary>
        /// <returns></returns>
        public abstract IList<Timezone> GetTimeZones();
    }
}
