﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AT.Core.Entities;
using AT.Data.Resource;

namespace AT.Provider.Resource.Providers
{
    /// <summary>
    /// The embedded resource provider
    /// </summary>
    public class EmbeddedResourceProvider : BaseResourceProvider
    {
        /// <summary>
        /// The name of the embedded resource provider
        /// </summary>
        public override string Name => "Embedded";
        /// <summary>
        /// The description of the embedded resource provider
        /// </summary>
        public override string Description => "The provider for reading resource files";
        /// <summary>
        /// The reference to the embedded resource repository
        /// </summary>
        private readonly IResourceRepository EmbeddedResourceRepository;

        public EmbeddedResourceProvider()
        {
            EmbeddedResourceRepository = new EmbeddedResourceRepository();
        }
        /// <summary>
        /// The function to get the embedded resource based on module & key
        /// </summary>
        /// <param name="module"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public override string Get(string module, string key)
        {
            return EmbeddedResourceRepository.Get(module, key);
        }

        /// <summary>
        /// The function to get the list of available languages
        /// </summary>
        /// <returns></returns>
        public override IList<Language> GetLanguages()
        {
            var cultureInfos = CultureInfo.GetCultures(CultureTypes.AllCultures);

            return cultureInfos.Select(cultureInfo => new Language()
                {
                    Id = cultureInfo.LCID,
                    Name = cultureInfo.Name,
                    FullName = cultureInfo.EnglishName,
                    Code = cultureInfo.ThreeLetterWindowsLanguageName
                })
                .ToList();
        }

        /// <summary>
        /// The function to get the list of available timezones
        /// </summary>
        /// <returns></returns>
        public override IList<Timezone> GetTimeZones()
        {
            var timezoneInfos = TimeZoneInfo.GetSystemTimeZones();

            return timezoneInfos.Select(tz => new Timezone()
            {
                Id = tz.Id,
                DisplayName = tz.DisplayName,
                StandardName = tz.StandardName
            }).ToList();
        }
    }
}
