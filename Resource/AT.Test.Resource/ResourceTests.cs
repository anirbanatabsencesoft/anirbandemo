﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Resources;
using AT.Api.Resource.Controllers;
using AT.Provider.Resource.Manager;
using AT.Provider.Resource;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AT.Test.Resource
{
    /// <summary>
    /// The unit tests for Resource Framework
    /// </summary>
    [TestClass]
    public class ResourceTests
    {
        /// <summary>
        /// Test to check if the system can load the case messages resource
        /// </summary>
        [TestMethod]
        public void TestCanLoadCaseMessageResource()
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            var caseText = ResourceProviderManager.Default.Get("Case", "SaveCase");

            Assert.AreEqual("Save Case", caseText);
        }
        /// <summary>
        /// Test to check if the system fails with incorrect case message resource
        /// </summary>
        [TestMethod]
        public void TestIncorrectCaseMessageResource()
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            var caseText = ResourceProviderManager.Default.Get("Case", "SaveCaseSuccessMessage");

            Assert.AreNotEqual("The case has been saved successfully1", caseText);
        }
        /// <summary>
        /// Test to check if the system fails with incorrect message resource
        /// </summary>
        [TestMethod, ExpectedException(typeof(MissingManifestResourceException))]
        public void TestCanNotLoadCaseMessageResource()
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            var caseText = ResourceProviderManager.Default.Get("Case", "SaveCaseSuccessMessage1");

            Assert.AreEqual("The case has been saved successfully", caseText);
        }
    }
}
