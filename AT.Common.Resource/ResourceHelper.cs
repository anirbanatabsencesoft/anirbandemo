﻿using AT.Provider.Resource.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Common.Resource
{
    public static class ResourceHelper
    {
        public const string Notification = "Notification";
        public const string Reporting = "Reporting";
        
        public static string Get(string moduleName, string key)
        {
            EmbeddedResourceProvider embeddedResourceProvider = new EmbeddedResourceProvider();
            return embeddedResourceProvider.Get(moduleName, key);
        }
    }
}
