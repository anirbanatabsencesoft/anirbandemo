﻿using System.Web.Http;
using AT.Api.Core;
using AT.Api.Core.Cors;
using AT.Api.Core.Filters;
using AT.Api.Shared.Filters;
using AT.Common.Security;

namespace AT.Api.Case
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API routes
            config.MapHttpAttributeRoutes();

            // Enable CORS through a policy factory
            config.SetCorsPolicyProviderFactory(new CorsPolicyFactory());
            config.EnableCors();

            // Add global filters
            config.Filters.Add(new ApiExceptionAttribute());
            config.Filters.Add(new SecureAttribute());
            config.Filters.Add(new SanitizeOperationAttribute());
            config.Filters.Add(new ModelStateEvaluatorAttribute());
            config.Filters.Add(new ConvertNullStringToNullAttribute());

            // Add media type for camel case property name resolution
            JsonPFormatterConfig.RegisterFormatters(config.Formatters);
        }
    }
}
