using System.Web.Http;
using AT.Api.Core;

namespace AT.Api.Case
{
    public class SwaggerConfig
    {
        public static void Register(HttpConfiguration configuration)
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;
            BaseSwaggerConfig.RegisterWithConfig(configuration, "Case", thisAssembly);
        }
    }
}
