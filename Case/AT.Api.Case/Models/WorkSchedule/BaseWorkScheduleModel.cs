﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;

namespace AT.Api.Case.Models
{
    public class BaseWorkScheduleModel
    {
        /// <summary>
        /// Gets or sets the ScheduleType .
        /// </summary>
        /// <value>
        /// The Schedule Type 
        /// </value>

        public ScheduleType ScheduleType { get; set; }
        /// <summary>
        /// Gets or sets the StartDate .
        /// </summary>
        /// <value>
        /// the Start date  for Change case
        /// </value>
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// Gets or sets the EndDate .
        /// </summary>
        /// <value>
        /// the End Date  for Change case
        /// </value>
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// Gets or sets the Times .
        /// </summary>
        /// <value>
        /// All  Times Details for Change case
        /// </value>
        public List<BaseTimeModel> Times { get; set; }
    }
}