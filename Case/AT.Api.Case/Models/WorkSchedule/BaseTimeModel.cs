﻿using System;

namespace AT.Api.Case.Models
{
    public class BaseTimeModel
    {
        /// <summary>
        /// Gets or sets the TotalMinutes .
        /// </summary>
        /// <value>
        /// Total Minutes for scheduling . 
        /// </value>
        public string TotalMinutes { get; set; }

        /// <summary>
        /// Gets or sets the SampleDate .
        /// </summary>
        /// <value>
        /// Total Sample Date. 
        /// </value>
        public DateTime SampleDate { get; set; }
    }
}