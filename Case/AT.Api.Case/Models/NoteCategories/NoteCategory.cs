﻿namespace AT.Api.Case.Models.NoteCategories
{
    public class NoteCategory
    {
        public int noteId { get; set; }
        public string Description { get; set; }
    }
}