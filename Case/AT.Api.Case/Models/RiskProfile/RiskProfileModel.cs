﻿namespace AT.Api.Case.Models.RiskProfile
{
    public class RiskProfileModel
    {
        /// <summary>
        /// The name of the risk profile
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Hexadecimal representation of the color associated with this risk profile
        /// </summary>
        public string Color { get; set; }

        /// <summary>
        /// The description of the risk profile
        /// </summary>
        public string Description { get; set; }
    }
}