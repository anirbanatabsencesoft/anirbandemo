﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AT.Api.Case.Models.ReassignCase
{
    public class ReassignCaseModel
    {
        /// <summary>
        /// Gets or Sets Email address of assignee user
        /// </summary>
        [Required, EmailAddress]
        public string AssigneeUserEmail { get; set; }

        /// <summary>
        /// Gets or Sets AssigneeTypeCode
        /// </summary>
        public string AssigneeTypeCode { get; set; }
    }
}