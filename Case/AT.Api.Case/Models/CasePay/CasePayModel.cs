﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AT.Api.Case.Models.CasePay
{
    /// <summary>
    /// Case Pay Model
    /// </summary>
    public class CasePayModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CasePayModel"/> class.
        /// </summary>
        public CasePayModel()
        {
            Policies = new List<CasePayPolicyModel>();
            PayPeriods = new List<CasePayPeriodModel>();
        }
                

        /// <summary>
        /// Gets or sets the employee Number.
        /// </summary>
        /// <value>
        /// The employee identifier.
        /// </value>
        public string EmployeeNumber { get; set; }
        
        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>
        /// The start date.
        /// </value>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>
        /// The end date.
        /// </value>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the approved from.
        /// </summary>
        /// <value>
        /// The approved from.
        /// </value>
        public DateTime? ApprovedFrom { get; set; }

        /// <summary>
        /// Gets or sets the approved thru.
        /// </summary>
        /// <value>
        /// The approved thru.
        /// </value>
        public DateTime? ApprovedThru { get; set; }

        /// <summary>
        /// Gets or sets the pay schedule identifier.
        /// </summary>
        /// <value>
        /// The pay schedule identifier.
        /// </value>
        public string PayScheduleId { get; set; }

        /// <summary>
        /// Gets or sets the name of the pay schedule.
        /// </summary>
        /// <value>
        /// The name of the pay schedule.
        /// </value>
        public string PayScheduleName { get; set; }

        /// <summary>
        /// Gets or sets the base pay.
        /// </summary>
        /// <value>
        /// The base pay.
        /// </value>
        public double? BasePay { get; set; }

        /// <summary>
        /// Gets or sets the pay type
        /// </summary>
        /// <value>
        /// The pay type
        /// </value>
        public int? Pay { get; set; }

        /// <summary>
        /// Returns the Base Pay formatted for display
        /// </summary>
        public string BasePayDisplay
        {
            get
            {
                return string.Format("{0:C}{1}", BasePay, Pay == (int)PayType.Hourly ? "/hour" : "/year");
            }
        }

        /// <summary>
        /// Gets or sets the scheduled hours per week.
        /// </summary>
        /// <value>
        /// The scheduled hours per week.
        /// </value>
        public decimal ScheduledHoursPerWeek { get; set; }

        /// <summary>
        /// Gets or sets the total paid.
        /// </summary>
        /// <value>
        /// The total paid.
        /// </value>
        public decimal TotalPaid { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to apply offsets by default.
        /// </summary>
        /// <value>
        /// <c>true</c> if apply offsets by default; otherwise, <c>false</c>.
        /// </value>
        public bool ApplyOffsetsByDefault { get; set; }

        /// <summary>
        /// Gets or sets the policies.
        /// </summary>
        /// <value>
        /// The policies.
        /// </value>
        public List<CasePayPolicyModel> Policies { get; set; }

        /// <summary>
        /// Gets or sets the pay periods.
        /// </summary>
        /// <value>
        /// The pay periods.
        /// </value>
        public List<CasePayPeriodModel> PayPeriods { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [waive waiting period].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [waive waiting period]; otherwise, <c>false</c>.
        /// </value>
        public bool WaiveWaitingPeriod { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [show waive waiting period].
        /// </summary>
        /// <value>
        /// <c>true</c> if [show waive waiting period]; otherwise, <c>false</c>.
        /// </value>
        public bool ShowWaiveWaitingPeriod { get; set; }
    }

    /// <summary>
    /// Case Pay Policy Model
    /// </summary>
    public class CasePayPolicyModel
    {
        /// <summary>
        /// Gets or sets the policy code.
        /// </summary>
        /// <value>
        /// The policy code.
        /// </value>
        public string PolicyCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the policy.
        /// </summary>
        /// <value>
        /// The name of the policy.
        /// </value>
        public string PolicyName { get; set; }

        /// <summary>
        /// Gets or sets the total paid.
        /// </summary>
        /// <value>
        /// The total paid.
        /// </value>
        public decimal TotalPaid { get; set; }

        /// <summary>
        /// Gets or sets the benefit percentage.
        /// </summary>
        /// <value>
        /// The benefit percentage.
        /// </value>
        public decimal BenefitPercentage { get; set; }

        /// <summary>
        /// Gets or sets the maximum benefit amount.
        /// </summary>
        /// <value>
        /// The maximum benefit amount.
        /// </value>
        public decimal MaxBenefitAmount { get; set; }

        /// <summary>
        /// Gets or sets the minimum benefit amount.
        /// </summary>
        /// <value>
        /// The minimum benefit amount.
        /// </value>
        public decimal MinBenefitAmount { get; set; }

        /// <summary>
        /// Gets or sets the total paid last52 weeks.
        /// </summary>
        /// <value>
        /// The total paid last52 weeks.
        /// </value>
        public decimal TotalPaidLast52Weeks { get; set; }
    }

    /// <summary>
    /// Case Pay Period Model
    /// </summary>
    public class CasePayPeriodModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CasePayPeriodModel"/> class.
        /// </summary>
        public CasePayPeriodModel()
        {
            Detail = new List<CasePayDetailModel>();
        }

        /// <summary>
        /// Gets or sets the pay period identifier.
        /// </summary>
        /// <value>
        /// The pay period identifier.
        /// </value>
        [Required (ErrorMessage = "Please provide PayPeriodId")]
        public string PayPeriodId { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        [Required(ErrorMessage = "Please provide payroll status")]
        public int Status { get; set; }

        /// <summary>
        /// Gets or sets the pay date.
        /// </summary>
        /// <value>
        /// The pay date.
        /// </value>
        public DateTime PayDate { get; set; }

        /// <summary>
        /// Gets or sets the pay from.
        /// </summary>
        /// <value>
        /// The pay from.
        /// </value>
        public DateTime PayFrom { get; set; }

        /// <summary>
        /// Gets or sets the pay thru.
        /// </summary>
        /// <value>
        /// The pay thru.
        /// </value>
        public DateTime PayThru { get; set; }

        /// <summary>
        /// Gets or sets the percentage.
        /// </summary>
        /// <value>
        /// The percentage.
        /// </value>
        public decimal Percentage { get; set; }

        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        /// <value>
        /// The amount.
        /// </value>
        public decimal Amount { get; set; }

        ///// <summary>
        ///// Gets or sets the pay period override.
        ///// </summary>
        ///// <value>
        ///// The pay period override.
        ///// </value>
        //public bool PayPeriodHasOverride { get; set; }

        ///// <summary>
        ///// Gets whether this pay period or any of its detail rows were overriden
        ///// </summary>
        //public bool HasUserOverride
        //{
        //    get
        //    {
        //        return this.PayPeriodHasOverride || Detail.Any(p => p.HasUserOverride);
        //    }
        //}

        /// <summary>
        /// PayPeriodSalary
        /// </summary>
        public decimal PayPeriodSalary { get; set; }

        /// <summary>
        /// Gets or sets the detail row models for this pay period model.
        /// </summary>
        /// <value>
        /// The detail.
        /// </value>
        public List<CasePayDetailModel> Detail { get; set; }
    }

    /// <summary>
    /// Case Pay Detail Model
    /// </summary>
    public class CasePayDetailModel
    {
        /// <summary>
        /// Gets or sets the pay detail identifier.
        /// </summary>
        /// <value>
        /// The pay detail identifier.
        /// </value>
        public string PayDetailId { get; set; }

        /// <summary>
        /// Gets or sets the policy code.
        /// </summary>
        /// <value>
        /// The policy code.
        /// </value>
        public string PolicyCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the policy.
        /// </summary>
        /// <value>
        /// The name of the policy.
        /// </value>
        public string PolicyName { get; set; }

        /// <summary>
        /// Gets or sets the pay from.
        /// </summary>
        /// <value>
        /// The pay from.
        /// </value>
        public DateTime PayFrom { get; set; }

        /// <summary>
        /// Gets or sets the pay thru.
        /// </summary>
        /// <value>
        /// The pay thru.
        /// </value>
        public DateTime PayThru { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is offset.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is offset; otherwise, <c>false</c>.
        /// </value>
        public bool IsOffset { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [allow offset].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [allow offset]; otherwise, <c>false</c>.
        /// </value>
        public bool AllowOffset { get; set; }

        /// <summary>
        /// Gets or sets the total paid.
        /// </summary>
        /// <value>
        /// The total paid.
        /// </value>
        public decimal TotalPaid { get; set; }

        /// <summary>
        /// Gets or sets the benefit percentage.
        /// </summary>
        /// <value>
        /// The benefit percentage.
        /// </value>
        public decimal BenefitPercentage { get; set; }

        ///// <summary>
        ///// Gets or sets the user override.
        ///// </summary>
        ///// <value>
        ///// The user override.
        ///// </value>
        //public bool HasUserOverride { get; set; }
    }
}