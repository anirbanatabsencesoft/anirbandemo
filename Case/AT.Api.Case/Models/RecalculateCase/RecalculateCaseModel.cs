﻿using System.ComponentModel.DataAnnotations;

namespace AT.Api.Case.Models.RecalculateCase
{
    public class RecalculateCaseModel
    {
        /// <summary>
        /// Gets or Sets Save case
        /// If true Case will be recalculated and udpated in database 
        /// else Case will be recalculated only 
        /// </summary>
        public bool SaveCase { get; set; }
    }
}