﻿using System;
using AbsenceSoft.Data.Enums;
using System.ComponentModel.DataAnnotations;

namespace AT.Api.Case.Models.CloseCase
{
    /// <summary>
    /// Close Case Model
    /// </summary>
    public class CloseCaseModel
    {
        /// <summary>
        /// Gets or Sets Case closure reason
        /// </summary>
        [Required]
        public CaseClosureReason ClosureReason { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public String Description { get; set; }
    }
}