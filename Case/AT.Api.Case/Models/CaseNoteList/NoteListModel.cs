

using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AT.Api.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AT.Api.Case.Models.CaseNotes
{
    /// <summary>
    /// CaseNote Model
    /// </summary>
    public class CaseNoteModel
    {
        /// <summary>
        /// Notes
        /// </summary>
        [Required(ErrorMessage = "Please provide notes")]
        public string Notes { get; set; }

        /// <summary>
        /// Category enum NoteCategoryEnum
        /// </summary>
        [Required(ErrorMessage = "Please provide note category")]
        public int Category { get; set; }

        /// <summary>
        /// Public
        /// </summary>
        public bool Public { get; set; }

        /// <summary>
        /// Note created By
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Note created date
        /// </summary>
        public string CreatedDate { get; set; }

        /// <summary>
        /// Note category string
        /// </summary>
        public string NoteCategory { get; set; }
    }
}