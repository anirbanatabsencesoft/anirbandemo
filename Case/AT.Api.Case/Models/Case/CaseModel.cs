﻿using AbsenceSoft.Data.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace AT.Api.Case.Models
{
    /// <summary>
    /// The data for the case
    /// </summary>
    public class CaseModel
    {
        /// <summary>
        /// The unique case number for this case
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// Reporter for the case
        /// </summary>
        public CaseReporterModel CaseReporter { get; set; }

        /// <summary>
        /// Type of the case
        /// </summary>
        [Required]
        public CaseType CaseType { get; set; }

        /// <summary>
        /// Important dates for the case
        /// </summary>
        public CaseDatesModel CaseDates { get; set; }

        /// <summary>
        /// Related contact info for the case
        /// </summary>
        public CaseRelationshipModel CaseRelationship { get; set; }

        /// <summary>
        /// Flags for the case
        /// </summary>
        public CaseFlagModel CaseFlags { get; set; }

        /// <summary>
        /// Unique number for the employee
        /// </summary>
        [Required]
        public string EmployeeNumber { get; set; }

        /// <summary>
        /// The absence reason code
        /// </summary>
        [Required]
        public string ReasonCode { get; set; }

        /// <summary>
        /// Short description for the case
        /// </summary>
        public string ShortDescription { get; set; }

        /// <summary>
        /// The date the leave of absence is supposed to start
        /// </summary>
        [Required]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// The end date of the case
        /// </summary>        
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// The case Summary
        /// </summary>
        public string Summary { get; set; }
    }

}