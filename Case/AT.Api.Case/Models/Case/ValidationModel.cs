﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AT.Api.Case.Models.Case
{
    public class ValidationModel
    {
        /// <summary>
        /// Validation message type (info, warning, error)
        /// </summary>
        public string ValidationType { get; set; }
        //public ValidationType ValidationType { get; set; }

        /// <summary>
        /// Validation message
        /// </summary>
        public string Message { get; set; }
    }
}