﻿using System.Collections.Generic;
using AbsenceSoft.Data.Enums;

namespace AT.Api.Case.Models.Workflow
{
    public class WorkflowCriteriaModel
    {
        public string Name { get; set; }
        public string Prompt { get; set; }
        public ControlType ControlType { get; set; }
        public Dictionary<string, object> Options { get; set; }
        public bool Required { get; set; }
        public object Value { get; set; }
    }
}
