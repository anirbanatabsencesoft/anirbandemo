﻿using System;
using System.ComponentModel.DataAnnotations;
using AbsenceSoft.Data.Enums;

namespace AT.Api.Case.Models.CreateToDo
{
    /// <summary>
    /// Create ToDo model
    /// </summary>
    public class CreateToDoModel
    {
        /// <summary>
        /// Gets or Sets Title
        /// </summary>
        [Required]
        public string Title { get; set; }

        /// <summary>
        /// Gets or Sets Description
        /// </summary>
        [Required]
        public string Description { get; set; }

        /// <summary>
        /// Gets or Sets DueDate
        /// </summary>        
        public DateTime DueDate { get; set; }

        /// <summary>
        /// Gets or Sets AssigneeUserEmail
        /// </summary>
        [EmailAddress]
        public string AssigneeUserEmail { get; set; }
        
    }
}