﻿using AbsenceSoft.Data.Enums;
using System;

namespace AT.Api.Case.Models.CaseRelatedContact
{
    /// <summary>
    /// Case related person contact model
    /// </summary>
    public class CaseRelatedContactModel
    {
        /// <summary>
        /// Contact Type Code
        /// </summary>       
        public string ContactTypeCode { get; set; }

        /// <summary>
        /// Military Status
        /// </summary>
        public MilitaryStatus MilitaryStatus { get; set; }

        /// <summary>
        /// First Name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Last Name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Date Of Birth
        /// </summary>
        public DateTime? DateOfBirth { get; set; }
    }
}