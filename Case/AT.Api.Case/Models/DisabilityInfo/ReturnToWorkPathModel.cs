﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AT.Api.Case.Models.DisabilityInfo
{
    public class ReturnToWorkPathModel
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public DateTime Expiration { get; set; }
        public RtwSummaryGuidelines RtwSummary { get; set; }

        /// <summary>
        /// Return-To-Work Best Practices
        /// </summary>     
        public List<RtwBestPractice> RtwBestPractices { get; set; }
       
        /// <summary>
        /// Chiropractical Guidelines
        /// </summary>       
        public List<ChiropracticalGuideline> Chiropractical { get; set; }

        /// <summary>
        /// Phisical Therapy Guidelines
        /// </summary>       
        public List<PhisicalTherapyGuideline> PhisicalTherapy { get; set; }

        /// <summary>
        /// Activity Modifications/Job Restrictions Guidelines
        /// </summary>        
        public List<ActivityModification> ActivityModifications { get; set; }
    }

    public class RtwSummaryGuidelines
    {
        /// <summary>
        /// Mid Range number of claims
        /// </summary>
        public int ClaimsMidRange { get; set; }

        /// <summary>
        /// At Risk number of claims
        /// </summary>
        public int ClaimsAtRisk { get; set; }

        /// <summary>
        /// Mid Range number of absences
        /// </summary>
        public int AbsencesMidRange { get; set; }

        /// <summary>
        /// At Risk Number of absences
        /// </summary>
        public int AbsencesAtRisk { get; set; }
    }

    public class RtwBestPractice
    {
        /// <summary>
        /// Primary RTW path id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Primary RTW path
        /// </summary>
        public string RtwPath { get; set; }

        /// <summary>
        /// Minimum number of days
        /// </summary>      
        public int? MinDays { get; set; }

        /// <summary>
        /// Max number of days
        /// </summary>       
        public int? MaxDays { get; set; }

        /// <summary>
        /// Returns String represantation of days
        /// </summary>       
        public string Days
        {
            get
            {
                string days = String.Empty;

                if (MinDays == null && MaxDays == null)
                    return days;

                if (MinDays != null && MinDays.HasValue)
                    days = MinDays.Value.ToString();

                if (MaxDays != null && MaxDays.HasValue)
                {
                    if (MaxDays.Value < 999)
                    {
                        if (!String.IsNullOrWhiteSpace(days))
                            days = days + " - ";

                        days = days + MaxDays.Value.ToString() + " days";
                    }
                    else
                    {
                        if (!String.IsNullOrWhiteSpace(days))
                            days = days + " to ";

                        days = days + "indefinite";
                    }
                }
                return days;
            }

        }       
    }

    public class ChiropracticalGuideline
    {
        /// <summary>
        /// Primary path for guideline
        /// </summary>
        public string PrimaryPath { get; set; }

        /// <summary>
        /// Number of visits
        /// </summary>      
        public int? Visits { get; set; }

        /// <summary>
        /// Number of weeks
        /// </summary>       
        public int? Weeks { get; set; }
    }

    public class PhisicalTherapyGuideline
    {
        /// <summary>
        /// Primary path for guideline
        /// </summary>
        public string PrimaryPath { get; set; }

        /// <summary>
        /// Number of visits
        /// </summary>       
        public int? Visits { get; set; }

        /// <summary>
        /// Number of Weeks
        /// </summary>       
        public int? Weeks { get; set; }
    }

    public class ActivityModification
    {
        /// <summary>
        /// Type of Job (Example: Modified work, Regular work)
        /// </summary>
        public string JobType { get; set; }

        /// <summary>
        /// Guidelines for Job modifications
        /// </summary>
        public string JobModifications { get; set; }
    }
}