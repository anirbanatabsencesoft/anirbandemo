﻿using AbsenceSoft.Data.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AT.Api.Case.Models.DisabilityInfo
{
    public class GetDisabilityInfoModel
    {
        /// <summary>
        /// PrimaryDiagnosis
        /// </summary>
        public DiagnosisCodeModel PrimaryDiagnosis { get; set; }

        /// <summary>
        /// list of SecondaryDiagnosis
        /// </summary>
        public List<DiagnosisCodeModel> SecondaryDiagnosis { get; set; }

        /// <summary>
        /// Primary Path Id
        /// </summary>
        public string PrimaryPathId { get; set; }

        /// <summary>
        /// Primary PathText
        /// </summary>
        public string PrimaryPathText { get; set; }

        /// <summary>
        /// Primary Path MinDays
        /// </summary>
        public string PrimaryPathMinDays { get; set; }

        /// <summary>
        /// Primary Path MaxDays
        /// </summary>
        public string PrimaryPathMaxDays { get; set; }

        /// <summary>
        /// Friendly Display
        /// </summary>
        public string FriendlyDisplay { get; set; }  //----------------------------- Need more description for it, remaining task

        /// <summary>
        /// Primary Path Days in UIText
        /// </summary>
        public string PrimaryPathDaysUIText { get; set; }

        /// <summary>
        /// CoMorbidityGuidelines
        /// </summary>
        public CoMorbidityGuideline CoMorbidityGuidelines { get; set; }

    }

    public class DiagnosisCodeModel
    {
        /// <summary>
        /// Code Type
        /// </summary>
        public string CodeType { get; set; } //-----------------Enum

        /// <summary>
        /// Code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// MaxDuration
        /// </summary>
        public int? MaxDuration { get; set; }

        /// <summary>
        /// DurationType
        /// </summary>
        public int? DurationType { get; set; } //--------------Enum

    }

    public class CoMorbidityGuideline
    {
        #region Risk Assessment
        /// <summary>
        /// Risk Assessment Score
        /// </summary>
        public decimal RiskAssessmentScore { get; set; }

        /// <summary>
        /// Risk Assessment Status
        /// </summary>
        public string RiskAssessmentStatus { get; set; }
        #endregion

        #region Adjusted Summary Guidelines
        /// <summary>
        /// Mid range All Absence
        /// </summary>
        public decimal? MidrangeAllAbsence { get; set; }

        /// <summary>
        /// At Risk All Absence
        /// </summary>
        public decimal? AtRiskAllAbsence { get; set; }
        #endregion

        #region Adjusted Duration
        /// <summary>
        /// Best Practices Days
        /// </summary>
        public decimal? BestPracticesDays { get; set; }
        #endregion

        #region Others
        /// <summary>
        /// Actual Days Lost
        /// </summary>
        public decimal? ActualDaysLost { get; set; }
        #endregion
    }
}