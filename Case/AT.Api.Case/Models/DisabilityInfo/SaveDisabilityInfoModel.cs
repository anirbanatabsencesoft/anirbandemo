﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;

namespace AT.Api.Case.Models.DisabilityInfo
{
    public class SaveDisabilityInfoModel
    {
        /// <summary>
        /// Primary Diagnosis Code
        /// </summary>
        public string PrimaryDiagnosisCode { get; set; }

        /// <summary>
        /// list of Secondary Diagnosis Code
        /// </summary>
        public List<string> SecondaryDiagnosisCodes { get; set; }

        /// <summary>
        /// Enum for Employee Job Activity
        /// </summary>
        public JobClassification? EmployeeJobActivity { get; set; } //------------ Change int to enum---- Clarification from Chad

        /// <summary>
        /// Medical Complications
        /// </summary>
        public bool MedicalComplications { get; set; }

        /// <summary>
        /// Hospitalization
        /// </summary>
        public bool Hospitalization { get; set; }

        /// <summary>
        /// General Health Condition
        /// </summary>
        public string GeneralHealthCondition { get; set; }

        /// <summary>
        /// Condition Start Date
        /// </summary>
        public DateTime? ConditionStartDate { get; set; }       

        /// <summary>
        /// Body Part Code
        /// </summary>
        public string BodyPartCode { get; set; }

        /// <summary>
        /// Nature Of Injury Code
        /// </summary>
        public string NatureOfInjuryCode { get; set; }

        /// <summary>
        /// ICDProcedureCodes
        /// </summary>
        public string IcdProcedureCodes { get; set; }                    //-------------List<string>

        /// <summary>
        /// HCPCSCodes
        /// </summary>
        public string HcpcsCodes { get; set; }                       //-------------List<string>

        /// <summary>
        /// CPTPProcedureCodes
        /// </summary>
        public string CptpProcedureCodes { get; set; }            //-------------List<string>

        /// <summary>
        /// NDCProcedureCodes
        /// </summary>
        public string NdcProcedureCodes { get; set; }                //-------------List<string>

        /// <summary>
        /// Depression
        /// </summary>
        public bool Depression { get; set; }

        /// <summary>
        /// Diabetes
        /// </summary>
        public bool Diabetes { get; set; }

        /// <summary>
        /// Hypertension
        /// </summary>
        public bool Hypertension { get; set; }

        /// <summary>
        /// Legal Representation
        /// </summary>
        public bool LegalRepresentation { get; set; }

        /// <summary>
        /// Obesity
        /// </summary>
        public bool Obesity { get; set; }

        /// <summary>
        /// Smoker
        /// </summary>
        public bool Smoker { get; set; }

        /// <summary>
        /// OPIOId
        /// </summary>
        public bool Opioid { get; set; }

        /// <summary>
        /// SubstanceAbuse
        /// </summary>
        public bool SubstanceAbuse { get; set; }

        /// <summary>
        /// Surgery Or HospitalStay
        /// </summary>
        public bool SurgeryOrHospitalStay { get; set; }
    }
}