﻿namespace AT.Api.Case.Models.CaseActivity
{
    public class GetCaseActivityModel
    {
        public string CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string Category { get; set; }

        public string Type { get; set; }

        public string Description { get; set; }
    }
}