﻿using System.ComponentModel.DataAnnotations;

namespace AT.Api.Case.Models.Providers
{
    /// <summary>
    /// Contact Provider Model
    /// </summary>
    public class CaseProviderModel
    {
        /// <summary>
        /// Contact Id
        /// </summary>
        public string ContactId { get; set; }

        /// <summary>
        /// Contact Type Code
        /// </summary>       
        [Required]
        public string ContactTypeCode { get; set; }

        /// <summary>
        /// Company Name
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// First Name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Last Name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Phone number
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Fax
        /// </summary>
        public string Fax { get; set; }

        /// <summary>
        /// Is primary contact
        /// </summary>
        public bool IsPrimary { get; set; }


        /// <summary>
        /// Contact Person Name
        /// </summary>
        public string ContactPersonName { get; set; }
    }
}