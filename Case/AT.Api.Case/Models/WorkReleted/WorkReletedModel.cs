﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Enums;
using AT.Api.Case.Models.CreateContact;
using AT.Api.Case.Models.Sharp;
using System;

namespace AT.Api.Case.Models.WorkReleted
{
    public class WorkReletedModel
    {
        public DateTime? IllnessOrInjuryDate { get; set; }
        public string InjuryLocation { get; set; }
        public string WhereOccurred { get; set; }
        public bool Reportable { get; set; }
        public bool? HealthCarePrivate { get; set; }
        public WorkRelatedCaseClassification? Classification { get; set; } //WorkRelatedCaseClassification enum 

        public WorkRelatedTypeOfInjury? TypeOfInjury { get; set; }
        public string InjuredBodyPart { get; set; }
        public int? DaysAwayFromWork { get; set; }

        public int? OverrideDaysAwayFromWork { get; set; }
        public int? DaysOnJobTransferOrRestriction { get; set; }
        public int? CalculatedDaysOnJobTransferOrRestriction { get; set; }
        public CreateContactModel ProviderContact { get; set; }
        public bool? EmergencyRoom { get; set; }
        public bool? HospitalizedOvernight { get; set; }
        public TimeOfDay? TimeEmployeeBeganWork { get; set; }
        public TimeOfDay? TimeOfEvent { get; set; }
        public string ActivityBeforeIncident { get; set; }
        public string WhatHappened { get; set; }
        public string InjuryOrIllness { get; set; }
        public string WhatHarmedTheEmployee { get; set; }

        public DateTime? DateOfDeath { get; set; }
        public bool? Sharps { get; set; }
        public SharpsInfoModel sharp { get; set; }

    }

}