﻿using AbsenceSoft.Data.Enums;
using System;

namespace AT.Api.Case.Models
{
    public class ChangeCaseModel
    {
        /// <summary>
        ///  Gets the type of the CaseType.
        /// </summary>
        public CaseType CaseType { get; set; }
        /// <summary>
        ///  Gets or sets the date the leave of absence is supposed to start.
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// Gets or sets the end date of the case.
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// Get/Set for Boolean StartdateNew
        /// </summary>
        public bool IsStartDateNew { get; set; }
        /// <summary>
        ///  Get/Set for Boolean IsEndDateNew
        /// </summary>
        public bool IsEndDateNew { get; set; }
        /// <summary>
        /// Get/Set for Boolean ModifyAccommodations
        /// </summary>
        public bool ModifyAccommodations { get; set; }
        /// <summary>
        /// Get/Set for Boolean ModifyDecisions
        /// </summary>
        public bool ModifyDecisions { get; set; }
        /// <summary>
        /// Get/Set for object refernce  work Schedule 
        /// </summary>
        public BaseWorkScheduleModel WorkSchedule { get; set; }
    }
}