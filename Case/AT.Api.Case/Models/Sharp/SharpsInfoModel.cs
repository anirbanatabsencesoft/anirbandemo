﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AT.Api.Case.Models.Sharp
{
    public class SharpsInfoModel
    {
        public List<WorkRelatedSharpsInjuryLocation> InjuryLocation { get; set; }
        public string TypeOfSharp { get; set; }
        public string BrandOfSharp { get; set; }
        public string ModelOfSharp { get; set; }
        public string BodyFluidInvolved { get; set; }
        public bool? HaveEngineeredInjuryProtection { get; set; }
        public bool? WasProtectiveMechanismActivated { get; set; }

        public WorkRelatedSharpsWhen? WhenDidTheInjuryOccur { get; set; }

        public WorkRelatedSharpsJobClassification? JobClassification { get; set; }
        public string JobClassificationOther { get; set; }
        public WorkRelatedSharpsLocation? LocationAndDepartment { get; set; }

        public string LocationAndDepartmentOther { get; set; }

        public WorkRelatedSharpsProcedure? Procedure { get; set; }
        public string ProcedureOther { get; set; }
        public string ExposureDetail { get; set; }

    }
}