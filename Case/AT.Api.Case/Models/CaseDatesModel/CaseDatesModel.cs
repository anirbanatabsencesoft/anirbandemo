﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AT.Api.Case.Models
{
    public class CaseDatesModel
    {
        public DateTime? ActualDeliveryDate { get; set; }
        public DateTime? AdoptionDate { get; set; }
        public DateTime? BondingStartDate { get; set; }
        public DateTime? BondingEndDate { get; set; }
        public DateTime? ExpectedDeliveryDate { get; set; }
    }
}