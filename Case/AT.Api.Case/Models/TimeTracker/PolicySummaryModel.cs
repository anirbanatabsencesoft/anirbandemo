﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AT.Api.Case.Models.TimeTracker
{
    /// <summary>
    /// Time Tracker - Policy Summary Model
    /// </summary>
    public class PolicySummaryModel
    {
        /// <summary>
        /// Gets or sets the policy code.
        /// </summary>
        /// <value>
        /// The policy code.
        /// </value>
        public string PolicyCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the policy.
        /// </summary>
        /// <value>
        /// The name of the policy.
        /// </value>
        public string PolicyName { get; set; }

        /// <summary>
        /// Gets or sets the time used.
        /// </summary>
        /// <value>
        /// The time used.
        /// </value>
        public double TimeUsed { get; set; }

        /// <summary>
        /// Gets or sets the Minutes used.
        /// </summary>
        /// <value>
        /// The time used.
        /// </value>
        public double MinutesUsed { get; set; }

        /// <summary>
        /// Gets or sets the time remaining.
        /// </summary>
        /// <value>
        /// The time remaining.
        /// </value>
        public double TimeRemaining { get; set; }

        // These are easy conversion properties for displaying the time for intermittent
        //  cases as friendly hours + minutes for leave managers who don't want to do the
        //  math of what 0.23 work weeks equals, lol, lazy leave managers, if a computer can
        //  do it, so can you, but whatever!
        /// <summary>
        /// The friendly time display for total hours used from the raw minutes.
        /// </summary>
        /// <value>
        /// The hours used.
        /// </value>
        public string HoursUsed { get; set; }

        /// <summary>
        /// The friendly time display for total hours remaining from the raw minutes.
        /// </summary>
        /// <value>
        /// The hours remaining.
        /// </value>
        public string HoursRemaining { get; set; }

        /// <summary>
        /// Gets or sets the units.
        /// Minutes = 0,
        /// Hours = 1,
        /// Days = 2,
        /// Weeks = 3,
        /// Months = 4,
        /// Years = 5
        /// </summary>
        /// <value>
        /// The units.
        /// </value>
        public int Units { get; set; }

        /// <summary>
        /// Unit text e.g. Weeks, Hours etc.
        /// </summary>
        public string UnitText { get; set; }

        // These get flattened out where we can, otherwise they designate 
        // a difference based on absence reason and are necessary
        /// <summary>
        /// Gets or sets the absence reason.
        /// </summary>
        public string AbsenceReason { get; set; }

        /// <summary>
        /// Gets or sets the absence reason identifier.
        /// </summary>
        /// <value>
        /// The absence reason identifier.
        /// </value>
        public string AbsenceReasonId { get; set; }

        /// <summary>
        /// Gets or sets the type of the policy.
        /// FMLA = 0, The FMLA policy type, (0)
        /// StateFML = 1,The state FML policy type, (1)
        /// STD = 2, The standard policy type, (2)
        /// LTD = 3, The LTD policy type, (3)
        /// WorkersComp = 4, The workers comp policy type, (4)
        /// Company = 5, The company policy type, (5)
        /// Other = 6, The other policy type, (6)
        /// StateDisability = 7, The state disability policy type, (7)
        /// </summary>
        /// <value>
        /// The type of the policy.
        /// </value>
        public int PolicyType { get; set; }

        /// <summary>
        /// PolicyType enum text
        /// </summary>
        public string PolicyTypeText { get; set; }

        /// <summary>
        /// Gets or sets the whether the policy should exclude from time conversion.
        /// </summary>
        public bool ExcludeFromTimeConversion { get; set; }
    }
}