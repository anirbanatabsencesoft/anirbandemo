﻿using AbsenceSoft;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AT.Api.Case.Models.Certifications
{
    /// <summary>
    /// Certification Model
    /// </summary>
    public class CertificationModel
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public CertificationModel()
        {
            // Defaults
            FrequencyType = (int)Unit.Weeks;
            DurationType = (int)Unit.Hours;
            CertificationCreateDate = DateTime.UtcNow;
        }

        /// <summary>
        /// Gets the start date of the certification.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date of the certification.
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the number of occurrances that are expected to occur through the specified frequency.
        /// </summary>
        public int? Occurances { get; set; }

        /// <summary>
        /// Gets or sets the frequency of the occurances as they are certified/expected to occur.
        /// </summary>
        public int? Frequency { get; set; }

        /// <summary>
        /// Gets or sets the units used to measure the frequency of the occurrances.
        /// Frequency Type
        /// Minutes = 0,
        /// Hours = 1,
        /// Days = 2,
        /// Weeks = 3,
        /// Months = 4,
        /// Years = 5
        /// </summary>
        public int? FrequencyType { get; set; }

        /// <summary>
        /// set the Frequency Type to work days or calendar days (used when setting the period window test) 
        /// default is work days.
        /// Workday = 0,
        /// CalendarDay = 1
        /// </summary>
        public int? FrequencyUnitType { get; set; }

        /// <summary>
        /// Gets or sets the certified duration for each occurance to occur within the bounds of.
        /// </summary>
        public double? Duration { get; set; }

        /// <summary>
        /// Gets or sets the units used to measure duration of each occurrance.
        /// </summary>
        public int? DurationType { get; set; }

        /// <summary>
        /// Gets or sets the IntermittentType of the certificate
        /// OfficeVisit = 0,
        /// Incapacity = 1
        /// </summary>
        public int? IntermittentType { get; set; }

        /// <summary>
        /// Gets or sets the Status of Incomplete certificate
        /// </summary>
        public bool IsCertificateIncomplete { get; set; }

        /// <summary>
        /// Gets or sets certificate creation Date
        /// </summary>
        public DateTime CertificationCreateDate { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        public Guid? Id { get; set; }

        /// <summary>
        /// Notes
        /// </summary>
        public string Notes { get; set; }

        ///// <summary>
        ///// Gets a pretty plain-english representation of this certification.
        ///// </summary>
        ///// <returns>A pretty plain-english representation of this certification.</returns>
        //public override string ToString()
        //{
        //    return string.Format("{0} occurance{5} every {1} {2} at {3} {4} per occurance",
        //        Occurances > 0 ? Occurances.ToString() : "No",
        //        Frequency,
        //        FrequencyType,
        //        Duration,
        //        DurationType,
        //        Occurances != 1 ? "s" : "");
        //}
    }

    ///// <summary>
    ///// Certification Detail Model
    ///// </summary>
    //public class CertificationDetailModel
    //{
    //    /// <summary>
    //    /// Id
    //    /// </summary>
    //    public Guid? Id { get; set; }

    //    /// <summary>
    //    /// Certification Start Date
    //    /// </summary>
    //    public DateTime? StartDate { get; set; }

    //    /// <summary>
    //    /// Certification End Date
    //    /// </summary>
    //    public DateTime? EndDate { get; set; }

    //    /// <summary>
    //    /// Occurances
    //    /// </summary>
    //    public int? Occurances { get; set; }

    //    /// <summary>
    //    /// Frequency
    //    /// </summary>
    //    public int? Frequency { get; set; }

    //    /// <summary>
    //    /// Frequency Type
    //    /// Minutes = 0,
    //    /// Hours = 1,
    //    /// Days = 2,
    //    /// Weeks = 3,
    //    /// Months = 4,
    //    /// Years = 5
    //    /// </summary>
    //    public int? FrequencyType { get; set; }

    //    /// <summary>
    //    /// Frequency Unit Type
    //    /// Workday = 0,
    //    /// CalendarDay = 1
    //    /// </summary>
    //    public int? FrequencyUnitType { get; set; }

    //    /// <summary>
    //    /// Duration
    //    /// </summary>
    //    public double? Duration { get; set; }

    //    /// <summary>
    //    /// DurationType same as Frequency Type
    //    /// </summary>
    //    public int? DurationType { get; set; }

    //    /// <summary>
    //    /// Notes
    //    /// </summary>
    //    public string Notes { get; set; }

    //    /// <summary>
    //    /// IntermittentType
    //    /// OfficeVisit = 0,
    //    /// Incapacity = 1
    //    /// </summary>
    //    public int? IntermittentType { get; set; }

    //    /// <summary>
    //    /// Intermittent
    //    /// </summary>
    //    public string Intermittent { get { return !IntermittentType.HasValue ? "" : IntermittentType.ToString().SplitCamelCaseString(); } }
    //}
}