using AbsenceSoft.Data.Enums;
using System;
namespace AT.Api.Case.Models.CaseEvents
{
    /// <summary>
    /// Get Case Event Model
    /// </summary>
    public class GetCaseEventModel
    {
        /// <summary>       
        /// CaseEventType
        /// </summary>
        public CaseEventType EventType { get; set; }

        /// <summary>
        /// StartDate
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// EventDate
        /// </summary>
        public DateTime EventDate { get; set; }

    }

}




