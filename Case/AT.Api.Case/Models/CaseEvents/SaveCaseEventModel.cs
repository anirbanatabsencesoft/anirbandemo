using AbsenceSoft.Data.Enums;
using System;

namespace AT.Api.Case.Models.CaseEvents
{  
      /// <summary>
     /// Save Case Event Model
    /// </summary>
    public class SaveCaseEventModel
    {
        /// <summary>
        /// Gets the Type of CaseEvent .
        /// </summary>
        public CaseEventType EventType { get; set; }
      
        /// <summary>
        /// Gets the end date of the case.
        /// </summary>
        public DateTime EventDate { get; set; }
       
        /// <summary>
        /// Delete the existing events.
        /// </summary>
        public bool Delete { get; set; }
    }
}