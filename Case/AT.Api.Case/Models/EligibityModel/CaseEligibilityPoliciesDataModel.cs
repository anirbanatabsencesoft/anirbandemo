﻿using AbsenceSoft.Models.Cases;
using System.Collections.Generic;

namespace AT.Api.Case.Models.EligibityModel
{
    public class CaseEligibilityPoliciesDataModel
    {
        public CaseEligibilityPoliciesDataModel()
        {
            this.Policies = new List<AppliedPolicyModel>();
            //this.ManuallyAppliedPolicies = new List<AppliedPolicyModel>();
            this.AvailableManualPolicies = new List<AppliedPolicyModel>();
        }
        public List<AppliedPolicyModel> Policies { get; set; }
        //public List<AppliedPolicyModel> ManuallyAppliedPolicies { get; set; }
        public List<AppliedPolicyModel> AvailableManualPolicies { get; set; }
    }
}