﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AT.Api.Case.Models.EligibityModel
{
    public class CasePolicySummaryDetailModel
    {
        public DateTime StartDate { get; set; }
        public string StartDateText { get; set; }
        public DateTime? EndDate { get; set; }
        public string EndDateText { get; set; }
        public string CaseType { get; set; }
        public string Status { get; set; }
        public string DenialReason { get; set; }
        public string DenialExplanation { get; set; }
    }
}