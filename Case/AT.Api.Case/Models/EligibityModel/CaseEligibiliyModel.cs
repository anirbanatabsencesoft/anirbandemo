﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Models;
using AbsenceSoft.Models.Cases;
using AbsenceSoft.Models.Contacts;
using AbsenceSoft.Models.Employees;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AT.Api.Case.Models.EligibityModel
{
    public class CaseEligibiliyModel : CaseEligibilityPoliciesDataModel
    {
        public string EmployeeNumber { get; set; }
        public int? CaseTypeId { get; set; }
        public CaseType CaseType { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public String AbsenceReasonCode { get; set; }
        public String EmployeeRelationshipCode { get; set; }       
        public DateTime? AdoptionDate { get; set; }
        public int? MilitaryStatusId { get; set; }
        public bool? IsWorkRelated { get; set; }
        public DateTime? ExpectedDeliveryDate { get; set; }
        public DateTime? ActualDeliveryDate { get; set; }
        public bool? WillUseBonding { get; set; }
        public DateTime? BondingStartDate { get; set; }
        public DateTime? BondingEndDate { get; set; }
        public string SpouseCaseNumber { get; set; }
        public string SpouseFirstName { get; set; }
        public string SpouseLastName { get; set; }
        public virtual ScheduleViewModel WorkSchedule { get; set; }            

        public WorkRelatedViewModel WorkRelatedInfo { get; set; }

        public Relapse Relapse { get; set; }
    }
}