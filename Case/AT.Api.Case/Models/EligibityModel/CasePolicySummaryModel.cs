﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AT.Api.Case.Models.EligibityModel
{
    public class CasePolicySummaryModel
    {
        public CasePolicySummaryModel() { Messages = new List<string>(); }
        public string Policy { get; set; }
        public string PolicyCode { get; set; }
        public List<CasePolicySummaryDetailModel> Detail { get; set; }
        public List<string> Messages { get; set; }
    }
}