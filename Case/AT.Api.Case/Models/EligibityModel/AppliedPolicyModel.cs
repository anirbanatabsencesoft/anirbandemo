﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AT.Api.Case.Models.EligibityModel
{
    public class AppliedPolicyModel
    {
        public AppliedPolicyModel()
        {
            this.RuleGroups = new List<AppliedRuleGroupModel>();
            this.Adjudications = new List<CasePolicySummaryDetailModel>();
            this.Messages = new List<string>();
        }
        public string PolicyId { get; set; }
        public List<AppliedRuleGroupModel> RuleGroups { get; set; }
        public String PolicyName { get; set; }
        public String PolicyCode { get; set; }
        public String PolicyType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public EligibilityStatus Status { get; set; }
        public bool HasOverriddenRule { get; set; }
        public String ManuallyAppliedPolicyNotes { get; set; }
        public bool UserIsHandlingExpandCollapse { get; set; }
        public bool Expanded { get; set; }
        public DateTime? ExhaustionDate { get; set; }
        public bool CombinedForSpouses { get; set; }
        public bool SeeSpouseCaseFlag { get; set; }
        public bool Paid { get; set; }
        public string AlternateEligibilityMessage { get; set; }
        public List<string> Messages { get; set; }
        public bool AllowChangeEndDate { get; set; }

        public List<CasePolicySummaryDetailModel> Adjudications { get; set; }

        public string StartDateText { get; set; }
        public string EndDateText { get; set; }
        /// <summary>
        /// IsAppliedPolicy true=AppliedPolicies
        /// AppliedPolicies false=ManuallyAppliedPolicies
        /// </summary>
        public bool IsAppliedPolicy { get; set; }
    }
}