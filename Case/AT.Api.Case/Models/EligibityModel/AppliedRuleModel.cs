﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AT.Api.Case.Models.EligibityModel
{
    public class AppliedRuleModel
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public String ActualValueString { get; set; }
        public AppliedRuleEvalResult Result { get; set; }
        public bool Overridden { get; set; }
        public AppliedRuleEvalResult OverrideResult { get; set; }
        public String OverrideValue { get; set; }
        public string OverrideNotes { get; set; }
    }
}