﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AT.Api.Case.Models
{
    public class CaseRelationshipModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }

        public MilitaryStatus? MilitaryStatus { get; set; }
        public string TypeCode { get; set; }
    }
}