﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AT.Api.Case.Models
{
    public class CaseFlagModel
    {
        public bool? IsWorkRelated { get; set; }
        public bool? WillUseBonding { get; set; }
        public bool? MedicalComplications { get; set; }
    }
}