﻿using AT.Api.Case.Models.Address;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AT.Api.Case.Models.CreateContact
{
    public class CreateContactModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactTypeCode { get; set; }

        public AddressModel Address { get; set; }
    }
}