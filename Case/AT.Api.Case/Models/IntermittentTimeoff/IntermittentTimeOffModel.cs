﻿using AbsenceSoft.Common.Serializers;
using AbsenceSoft.Data.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbsenceSoft.Common;

namespace AT.Api.Case.Models.IntermittentTimeoff
{
    /// <summary>
    /// Intermittent TimeOff Model
    /// </summary>
    public class IntermittentTimeOffModel
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public IntermittentTimeOffModel() {
            Details = new List<IntermittentTimeOffRequestDetailModel>();            
        }

        ///// <summary>
        ///// Case Number
        ///// </summary>
        //public string CaseNumber { get; set; }

        /// <summary>
        /// Request Date
        /// </summary>
        public DateTime RequestDate { get; set; }

        /// <summary>
        /// IntermittentType enum
        /// 0 = OfficeVisit, 1 = Incapacity
        /// </summary>
        public int? IntermittentType { get; set; }   //instead of enum can we use string or int in API model?

        /// <summary>
        /// Notes
        /// </summary>
        public string Notes { get; set; }

        /// <summary>
        /// Intermittent Timeoff Details
        /// </summary>
        public List<IntermittentTimeOffRequestDetailModel> Details { get; set; }
       
        /// <summary>
        /// Is Time Equal
        /// </summary>
        public bool IsTimeEqual { get; set; }

        /// <summary>
        /// flag to indicate that warnings exist but the user would like to procede anyway
        /// </summary>
        public bool WarningsApproved { get; set; }

        // Fallback stuff
        /// <summary>
        /// Pending time
        /// </summary>
        //[JsonConverter(typeof(FriendlyTimeSerializer))]
        public int PendingTime { get; set; }

        /// <summary>
        /// Approved time
        /// </summary>
        //[JsonConverter(typeof(FriendlyTimeSerializer))]
        public int ApprovedTime { get; set; }

        /// <summary>
        /// Denial time
        /// </summary>
        //[JsonConverter(typeof(FriendlyTimeSerializer))]
        public int DeniedTime { get; set; }

        /// <summary>
        /// Get or Set Denial Reason
        /// Other = 0,
        /// Exhausted = 1,
        /// NotAValidHealthcareProvider = 2,
        /// NotASeriousHealthCondition = 3,
        /// EliminationPeriod = 4,
        /// PerUseCap = 5,
        /// IntermittentNonAllowed = 6,
        /// PaperworkNotReceived = 7,
        /// NotAnEligibleFamilyMember = 8,
        /// MaxOccurrenceReached =9,
        /// NotApplicable = 10
        /// </summary>
        public int? DenialReason { get; set; }

        /// <summary>
        /// Gets or sets the determination denial reason code. Only required if the determination
        /// status is Denied.
        /// </summary>        
        public string DenialReasonCode { get; set; }

        /// <summary>
        /// Gets or sets the determination denial reason description. Only required if the determination
        /// status is Denied.
        /// </summary>        
        public string DenialReasonName { get; set; }

        /// <summary>
        /// Denial reason other
        /// </summary>
        public string DenialReasonOther { get; set; }

        /// <summary>
        /// Total minutes of request (each detail object should sum to this amount)
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// Used by Workflow to check whether this specific request passed certification
        /// </summary>        
        public bool PassedCertification { get; set; }
           
        /// <summary>
        /// 
        /// </summary>
        public string TotalTime
        {
            get
            {
                int hours = this.TotalMinutes / 60;
                int minutes = this.TotalMinutes % 60;
                if (minutes == 0)
                    return string.Format("{0:0} hours", hours);

                return string.Format("{0:0} hours and {1:0} minutes", hours, minutes);
            }
        }

        // these fields are for self service (when we get it)

        /// <summary>
        /// Did the employee enter the time? 
        /// </summary>
        public bool EmployeeEntered { get; set; }

        /// <summary>
        /// Internal Id for ITOR 
        /// </summary>

        public Guid? Id { get; set; }

        /// <summary>
        /// has a manager approved the time off request
        /// </summary>
        public bool ManagerApproved { get; set; }

        /// <summary>
        /// what date did the manager approve it
        /// </summary>        
        public DateTime? ManagerApprovedDate { get; set; }

        /// <summary>
        /// what manager approved it.
        /// </summary>        
        public string ManagerId { get; set; }

        /// <summary>
        /// Start TIme for TOR.
        /// </summary>        
        public TimeOfDay? StartTimeForLeave { get; set; }

        /// <summary>
        /// End time for TOR .
        /// </summary>        
        public TimeOfDay? EndTimeForLeave { get; set; }

        /// <summary>
        /// Is Intermittent Restriction
        /// </summary>
        public bool IsIntermittentRestriction { get; set; }

        /// <summary>
        /// Created date for ITOR
        /// </summary>

        public DateTime CreatedDate { get; set; }
    }

    /// <summary>
    /// Intermittent TimeOff Detail Model
    /// </summary>
    public class IntermittentTimeOffRequestDetailModel
    {
        /// <summary>
        /// Gets or sets the policy code.
        /// </summary>
        /// <value>
        /// The policy code.
        /// </value>
        public string PolicyCode { get; set; }

        /// <summary>
        /// Minutes to set to pending
        /// </summary>
        //[JsonConverter(typeof(FriendlyTimeSerializer))] // do we need to serialize in API? 
        public int PendingTime { get; set; }

        /// <summary>
        /// Minutes to set to approved
        /// </summary>
        //[JsonConverter(typeof(FriendlyTimeSerializer))]
        public int ApprovedTime { get; set; }

        /// <summary>
        /// Minutes to set to denied
        /// </summary>
        //[JsonConverter(typeof(FriendlyTimeSerializer))]
        public int DeniedTime { get; set; }

        /// <summary>
        /// Get or Set Denial Reason
        /// Other = 0,
        /// Exhausted = 1,
        /// NotAValidHealthcareProvider = 2,
        /// NotASeriousHealthCondition = 3,
        /// EliminationPeriod = 4,
        /// PerUseCap = 5,
        /// IntermittentNonAllowed = 6,
        /// PaperworkNotReceived = 7,
        /// NotAnEligibleFamilyMember = 8,
        /// MaxOccurrenceReached =9,
        /// NotApplicable = 10
        /// </summary>
        public int? DenialReason { get; set; }

        /// <summary>
        /// Denial Reason Code
        /// </summary>
        public string DenialReasonCode { get; set; }

        /// <summary>
        /// Denial Reason Name
        /// </summary>
        public string DenialReasonName { get; set; }

        /// <summary>
        /// Denial reason other
        /// </summary>
        public string DenialReasonOther { get; set; }

        /// <summary>
        /// Policy name
        /// </summary>
        public string PolicyName { get; set; }

        /// <summary>
        /// Available
        /// </summary>
        public string Available { get; set; }
    }

   

}