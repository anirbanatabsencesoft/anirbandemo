﻿using AbsenceSoft.Data.Enums;
using System.Collections.Generic;

namespace AT.Api.Case.Models.CasePolicyChangeStatus
{
    public class DeterminationPolicy
    {
        public string PolicyName { get; set; }
        public string PolicyCode { get; set; }
        public EligibilityStatus EligibilityStatus { get; set; }
        public List<CaseDeteminationPolicySummary> DeteminationStatus { get; set; }
    }
}