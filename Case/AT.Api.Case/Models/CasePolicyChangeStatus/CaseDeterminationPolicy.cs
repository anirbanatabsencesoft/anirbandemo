﻿namespace AT.Api.Case.Models.CaseDeterminationModel
{
    public class CaseDeterminationPolicy
    {
        public string PolicyCode { get; set; }
        public string DeterminationDenialReasonCode { get; set; }
        public string DenialExplanation { get; set; }
    }
}