﻿using System;

namespace AT.Api.Case.Models.CasePolicyChangeStatus
{
    public class CaseDeteminationPolicySummary
    {
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string CaseType { get; set; }
        public string Status { get; set; }
        public string DenialReason { get; set; }
        public string DenialExplanation { get; set; }

    }
}