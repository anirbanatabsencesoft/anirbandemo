﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;

namespace AT.Api.Case.Models.CaseDeterminationModel
{
    public class CaseDeterminationModel
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string DeterminationDenialReasonCode { get; set; }
        public string DeterminationDenialReasonName { get; set; }
        public string DenialExplanation { get; set; }
        public AdjudicationStatus DeterminationStatus { get; set; }
        public List<CaseDeterminationPolicy> Policies { get; set; }
    }

}