﻿using System.ComponentModel.DataAnnotations;

namespace AT.Api.Case.Models.CaseAccommodation
{
    /// <summary>
    /// Data to close case accommodation
    /// </summary>
    public class CloseCaseAccommodationModel
    {        
        /// <summary>
        /// General health condition
        /// </summary>
        public string GeneralHealthCondition { get; set; }

        /// <summary>
        /// Description related to closure
        /// </summary>
        public string Description { get; set; }
    }
}