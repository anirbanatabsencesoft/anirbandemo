﻿using System.Collections.Generic;

namespace AT.Api.Case.Models.CaseAccommodation
{
    /// <summary>
    /// Get interactive process model
    /// </summary>
    public class GetInteractiveProcessModel
    {        
        /// <summary>
        /// Question Id
        /// </summary>
        public string QuestionId { get; set; }

        /// <summary>
        /// QuestionText
        /// </summary>
        public string QuestionText { get; set; }

        /// <summary>
        /// Answer
        /// </summary>
        public string Answer { get; set; }

        /// <summary>
        /// Case note model
        /// </summary>
        public List<InteractiveProcessCaseNoteModel> Notes { get; set; }
    }
}