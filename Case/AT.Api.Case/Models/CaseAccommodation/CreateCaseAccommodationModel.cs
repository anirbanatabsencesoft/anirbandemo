﻿using AbsenceSoft.Data.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace AT.Api.Case.Models.CaseAccommodation
{
    public class CreateCaseAccommodationModel
    {
        [Required]
        public string TypeCode { get; set; }

        [Required]
        public string TypeName { get; set; }

        [Required]
        public AccommodationDuration Duration { get; set; }

        public string Description { get; set; }

        public bool IsResolved { get; set; }

        public bool IsGranted { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public AdjudicationStatus Determination { get; set; }

        public DateTime? MinimumApprovedFromDate { get; set; }

        public DateTime? MaximumApprovedThruDate { get; set; }

        public CaseStatus Status { get; set; }

        public bool IsWorkRelated { get; set; }
    }
}