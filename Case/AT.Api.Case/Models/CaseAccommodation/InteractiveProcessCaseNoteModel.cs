﻿using AbsenceSoft.Data.Enums;
using System;

namespace AT.Api.Case.Models.CaseAccommodation
{
    /// <summary>
    /// Interactive case note model
    /// </summary>
    public class InteractiveProcessCaseNoteModel
    {
        /// <summary>
        /// Created Date
        /// </summary>
        public DateTime? CreateDate { get; set; }

        /// <summary>
        /// Created By
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Created Date
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Note
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Contact Info
        /// </summary>
        public string ContactInfo { get; set; }

        /// <summary>
        /// Not Category
        /// </summary>
        public NoteCategoryEnum? Category { get; set; }

        /// <summary>
        /// Answer
        /// </summary>
        public string Answer { get; set; }        
    }
}