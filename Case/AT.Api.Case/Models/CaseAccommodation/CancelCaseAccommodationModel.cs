﻿using AbsenceSoft.Data.Enums;
using System.ComponentModel.DataAnnotations;

namespace AT.Api.Case.Models.CaseAccommodation
{
    public class CancelCaseAccommodationModel
    {
        public string GeneralHealthCondition { get; set; }

        public string Description { get; set; }

        public CaseStatus Status { get; set; }

        [Required]
        public AccommodationCancelReason CancelReason { get; set; }

        public string OtherCancelDescription { get; set; }
    }
}