﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AT.Api.Case.Models.CaseAccommodation
{
    public class CaseAccommodationModel
    {
        public string AccommodationRequestId { get; set; }

        [Required]
        public string GeneralHealthCondition { get; set; }

        [Required]
        public string Description { get; set; }
                
        public CaseStatus Status { get; set; }

        public List<AccommodationModel> Accommodations { get; set; }

        [Required]
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public AdjudicationStatus Determination { get; set; }

        [Required]
        public string SummaryDuration { get; set; }

        [Required]
        public string SummaryDurationType { get; set; }

        public AccommodationCancelReason? CancelReason { get; set; }

        public string OtherCancelReasonDescription { get; set; }
    }
}