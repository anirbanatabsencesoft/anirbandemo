﻿using System.ComponentModel.DataAnnotations;

namespace AT.Api.Case.Models.CaseAccommodation
{
    /// <summary>
    /// Interactive process model
    /// </summary>
    public class InteractiveProcessStepModel
    {
        /// <summary>
        /// QuetionId
        /// </summary>
        [Required]
        public string QuestionId { get; set; }

        /// <summary>
        /// Answer
        /// </summary>
        public string Answer { get; set; }

        /// <summary>
        /// Note
        /// </summary>
        public string Note { get; set; }
    }
}