﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;

namespace AT.Api.Case.Models.CaseAccommodation
{
    public class AccommodationRequestModel
    { 
        public string CaseNumber { get; set; }

        public CaseStatus Status { get; set; }

        public string GeneralHealthCondition { get; set; }

        public string Description { get; set; }

        public List<AccommodationModel> Accommodations { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public AdjudicationStatus Determination { get; set; }

        public AccommodationCancelReason? CancelReason { get; set; }

        public string OtherReasonDescription { get; set; }               
    }
}