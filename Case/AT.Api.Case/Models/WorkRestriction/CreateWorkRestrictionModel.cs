﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AT.Api.Case.Models.WorkRestriction
{
    /// <summary>
    /// Properties for Create Work Restriction
    /// </summary>
    public class CreateWorkRestrictionModel
    {
        /// <summary>
        /// Employee Restriction Id. If provided would update the work restriction otherwise create new
        /// </summary>
        public string Id { get; set; }
                
        /// <summary>
        /// Employee Restriction Demand Code
        /// </summary>
        [Required]
        public string DemandCode { get; set; }

        /// <summary>
        /// The Start Date of Demand
        /// </summary>
        [Required]
        public DateTime? StartDate { get; set; }
        
        /// <summary>        
        /// The End Date of Demand
        /// </summary>        
        [Required]
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Applied demand values
        /// </summary>
        [Required]
        public List<AppliedDemandValueModel> Values { get; set; }
    }
}