﻿using System;
using System.Collections.Generic;

namespace AT.Api.Case.Models.WorkRestriction
{
    /// <summary>
    /// Model to get work restriction
    /// Employee Restriction model properties
    /// </summary>
    public class WorkRestrictionModel //AppliedDemandModel
    {
        /// <summary>
        /// Employee Restriction Id. If provided would update the work restriction otherwise create new
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Employee Id will be populated from Case
        /// </summary>
        public string EmployeeId { get; set; }

        /// <summary>
        /// Read only Case Id will be populated from Case
        /// </summary>
        public string CaseId { get; set; }

        /// <summary>
        /// Employee Restriction DemandId
        /// </summary>        
        public string DemandId { get; set; }

        /// <summary>
        ///  Employee Restriction Demand name
        /// </summary>
        public string DemandName { get; set; }

        /// <summary>
        /// Employee Restriction date range
        /// </summary>
        public string DateString { get; set; }
        
        /// <summary>
        /// The Start Date of Demand
        /// </summary>        
        public DateTime? StartDate { get; set; }
        
        /// <summary>        
        /// The End Date of Demand
        /// </summary>                
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Applied demand values
        /// </summary>        
        public List<AppliedDemandValueModel> Values { get; set; }
    }

    /// <summary>
    /// Properties for Applied Demand value Model
    /// </summary>
    public class AppliedDemandValueModel
    {
        /// <summary>
        /// Demand Type Code
        /// </summary>
        public string DemandTypeCode { get; set; }
        
        /// <summary>
        /// Demand Type 
        /// </summary>
        /// <value>
        /// Text = 0,
        /// Boolean = 1,
        /// Value = 2,
        /// Schedule = 3
        /// </value>
        public int Type { get; set; }
                
        /// <summary>
        /// DemandItems
        /// </summary>
        public DemandItemModel DemandItem { get; set; }

        /// <summary>
        /// Demand Item Name
        /// </summary>
        public string DemandItemName { get; set; }

        /// <summary>
        /// Text is required if Type = 0 i.e. text
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// To check if demand Value Type = 1 i.e. boolean 
        /// </summary>
        public bool? Applicable { get; set; }

        /// <summary>
        /// Gets or sets the number of occurrances that are expected to occur through the specified frequency.
        /// </summary>
        public int Occurances { get; set; }

        /// <summary>
        /// Gets or sets the frequency of the occurances as they are certified/expected to occur.
        /// </summary>
        public int Frequency { get; set; }

        /// <summary>
        /// Gets or sets the units used to measure the frequency of the occurrances.
        /// </summary>
        /// <value>
        /// Minutes = 0,
        /// Hours = 1,
        /// Days = 2,
        /// Weeks = 3,
        /// Months = 4,
        /// Years = 5
        /// </value>
        public int? FrequencyType { get; set; }

        /// <summary>
        /// set the Frequency Type to work days or calendar days (used when setting the period window test) 
        /// default is work days.
        /// </summary>
        /// <value>
        /// Workday = 0,
        /// CalendarDay = 1
        /// </value>
        public int? FrequencyUnitType { get; set; }

        /// <summary>
        /// Gets or sets the certified duration for each occurance to occur within the bounds of.
        /// </summary>
        public double Duration { get; set; }

        /// <summary>
        /// Gets or sets the units used to measure duration of each occurrance.
        /// </summary>
        /// <value>
        /// Minutes = 0,
        /// Hours = 1,
        /// Days = 2,
        /// Weeks = 3,
        /// Months = 4,
        /// Years = 5
        /// </value>
        public int? DurationType { get; set; }
    }

    /// <summary>
    /// Demand Item Model
    /// </summary>
    public class DemandItemModel
    {
        /// <summary>
        /// Gets or sets the name of the demand item.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>        
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the comparison value of this demand item for quantification. Default is <c>0</c> (zero).
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        /// <remarks>
        /// <para>If a demand item has a value of 1 and another a value of 2, and a restriction is entered that states the Min Value = 2
        /// and a restriction puts the employee at a value of 1, we know the demand item is not met and therefore
        /// the demand type fails and the overall grouping of demands is not met.</para>
        /// </remarks>        
        public int Value { get; set; }
    }
}