﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AT.Api.Case.Models.WorkRestriction
{
    /// <summary>
    /// Work Restriction / Demand Type Lookup Model
    /// </summary>
    public class WorkRestrictionTypeLookupModel
    {
        /// <summary>
        /// Gets the id of the demand type.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the demand type.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>        
        public string Name { get; set; }        

        /// <summary>
        /// Gets or sets an optional and helpful description of the demand type.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>        
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets an code of the demand type.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>        
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the type. The default value is <c>DemandValueType.Text</c> (0).
        /// </summary>
        /// <value>
        /// The type.
        /// </value>        
        public int Type { get; set; }

        /// <summary>
        /// Gets or sets the items. By default this value is <c>null</c> and may be empty
        /// based on the demand type.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>        
        public List<DemandItemModel> Items { get; set; }

        /// <summary>
        /// Gets or sets the order this demand type is displayed in on the UI or in reports when showing
        /// demands in a matrix or other fashion.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>        
        public int? Order { get; set; }

    }
}