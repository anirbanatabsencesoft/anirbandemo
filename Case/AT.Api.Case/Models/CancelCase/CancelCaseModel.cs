﻿using AbsenceSoft.Data.Enums;
using System.ComponentModel.DataAnnotations;

namespace AT.Api.Case.Models.CancelCase
{
    /// <summary>
    /// Cancel Case Model
    /// </summary>
    public class CancelCaseModel
    {
        /// <summary>
        /// Gets or Sets Case cancellation reason
        /// </summary>
        [Required]
        public CaseCancelReason CancelReason { get; set; }
    }
}