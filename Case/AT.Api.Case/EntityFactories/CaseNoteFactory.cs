﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Security;
using Cases = AbsenceSoft.Data.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbsenceSoft;

namespace AT.Api.Case.EntityFactories
{
    /// <summary>
    /// Case note factory
    /// </summary>
    public class CaseNoteFactory
    {
        /// <summary>
        /// Get case note entity for interactive process model
        /// </summary>
        /// <param name="note"></param>
        /// <param name="user"></param>
        /// <param name="customerId"></param>
        /// <param name="employerId"></param>
        /// <param name="question"></param>
        /// <param name="questionId"></param>
        /// <param name="accomId"></param>
        /// <param name="answer"></param>
        /// <param name="sAnswer"></param>
        /// <param name="theCase"></param>
        /// <returns></returns>
        internal static CaseNote FromIntProcCaseNoteModel(
            string note,
            User user,
            string customerId,
            string employerId,
            string question,
            string questionId,
            Guid accomId,
            bool? answer,
            string sAnswer,
            Cases.Case theCase)
        {
            CaseNote caseNote = new CaseNote()
            {
                Category = NoteCategoryEnum.InteractiveProcess,
                CreatedBy = user,
                CustomerId = customerId,
                EmployerId = employerId,
                Public = false,
                Case = theCase,
                ModifiedBy = user,
                Notes = question + " - " + note
            };

            caseNote.Metadata.SetRawValue("AccommQuestionId", questionId);
            caseNote.Metadata.SetRawValue("AccommId", accomId);
            caseNote.Metadata.SetRawValue("Answer", answer);
            caseNote.Metadata.SetRawValue("SAnswer", sAnswer);

            return caseNote;
        }

    }
}