﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AT.Api.Case.Models.Providers;

namespace AT.Api.Case.EntityFactories
{
    internal class ProviderFactory
    {
        internal static EmployeeContact FromEmployeeProviderModel(
            CaseProviderModel model,
            string employerId,
            string customerId,
            string employeeId,
            string codeName,
            EmployeeContact providerContact = null)
        {
            providerContact = providerContact ?? new EmployeeContact();

            providerContact.ContactTypeCode = model.ContactTypeCode;
            providerContact.ContactTypeName = codeName;

            providerContact.EmployerId = employerId;
            providerContact.CustomerId = customerId;
            providerContact.EmployeeId = employeeId;
            providerContact.MilitaryStatus = MilitaryStatus.Civilian;
            providerContact.Contact = new Contact
            {
                CompanyName = model.CompanyName,
                FirstName = model.FirstName,
                LastName = model.LastName,
                WorkPhone = model.Phone,
                Fax = model.Fax,
                IsPrimary = model.IsPrimary,
                ContactPersonName = model.CompanyName
            };

            return providerContact;
        }
    }
}