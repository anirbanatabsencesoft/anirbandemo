﻿using AbsenceSoft.Logic.Workflows;
using AT.Api.Case.Models.Workflow;

namespace AT.Api.Case.EntityFactories
{
    public class WorkflowCriteriaFactory
    {
        public static WorkflowCriteria FromWorkflowCriteriaModel(WorkflowCriteriaModel model)
        {
            return new WorkflowCriteria
            {
                ControlType = model.ControlType,
                Name = model.Name,
                Options = model.Options,
                Prompt = model.Prompt,
                Required = model.Required,
                Value = model.Value
            };
        }
    }
}