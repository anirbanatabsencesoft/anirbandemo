﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.DataExtensions;
using AT.Api.Case.Models.TimeTracker;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Requests.TimeTracker
{
    /// <summary>
    /// Get Time Tracker Request
    /// </summary>
    public class GetTimeTrackerRequest : ApiRequest<GetTimeTrackerParameters, CollectionResultSet<PolicySummaryModel>>
    {
        private readonly ICaseService _CaseService = null;

        /// <summary>
        /// Constructor
        /// </summary>        
        /// <param name="caseService"></param>
        public GetTimeTrackerRequest(ICaseService caseService = null)
        {
            _CaseService = caseService ?? new CaseService();
        }

        /// <summary>
        /// Get Time Tracking Info For Case
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override CollectionResultSet<PolicySummaryModel> FulfillRequest(GetTimeTrackerParameters parameters)
        {
            Cases.Case existingCase = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);
            if (existingCase == null)
            {
                throw new ApiException(HttpStatusCode.ExpectationFailed, "Case not found with the given details");
            }
            
            var policysummary = _CaseService.GetEmployeePolicySummaryByCaseId(existingCase.Id, new EligibilityStatus[] { EligibilityStatus.Eligible});
            
            var policysummaryModels = new List<PolicySummaryModel>();          

            if (policysummary == null)
            {
                return new CollectionResultSet<PolicySummaryModel>(
                    policysummaryModels,
                    "No policy summary found for case",
                    true,
                    HttpStatusCode.NoContent);
            }
            policysummaryModels.AddRange(policysummary.Select(policy => policy.ToPolicySummaryModel()));
            return new CollectionResultSet<PolicySummaryModel>(policysummaryModels);
        }

        /// <summary>
        /// Validate input parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool IsValid(GetTimeTrackerParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrEmpty(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }
            return true;
        }
    }
}