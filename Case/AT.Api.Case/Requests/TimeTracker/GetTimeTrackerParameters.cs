﻿using AbsenceSoft.Data.Enums;

namespace AT.Api.Case.Requests.TimeTracker
{
    /// <summary>
    /// IntermittentTimeoff Request Parameters
    /// </summary>
    public class GetTimeTrackerParameters
    {
        /// <summary>
        /// Property for CaseNumber
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// Property for RequestDate        
        /// </summary>
        public EligibilityStatus[] includeStatus { get; set; }
    }
}