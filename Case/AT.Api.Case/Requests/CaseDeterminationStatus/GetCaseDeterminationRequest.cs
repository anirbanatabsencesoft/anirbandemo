﻿using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.DataExtensions;
using AT.Api.Case.Models.CasePolicyChangeStatus;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Net;

namespace AT.Api.Case.Requests.CaseDeterminationStatus
{
    public class GetCaseDeterminationRequest : ApiRequest<CasePolicyParameters, CollectionResultSet<DeterminationPolicy>>
    {
        private readonly ICaseService _CaseService = null;

        /// <summary>
        /// Constructor
        /// </summary>      
        /// <param name="CaseService"></param>
        public GetCaseDeterminationRequest(ICaseService CaseService = null)
        {
            _CaseService = CaseService ?? new CaseService();
        }
        protected override bool IsValid(CasePolicyParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.CaseNumber))
            {
                message = "Case number was not provided";
                return false;
            }
            return true;
        }

        protected override CollectionResultSet<DeterminationPolicy> FulfillRequest(CasePolicyParameters parameters)
        {
            var _case = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);
            if (_case == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Case not found for the given case number");
            }
            var newListDeteminationPolicy = DeterminationPolicyExtension.toDeteminationPolicy(_case);

            if (newListDeteminationPolicy.Count == 0)
            {
                return new CollectionResultSet<DeterminationPolicy>(
                    newListDeteminationPolicy,
                    "No policy found for case",
                    true,
                    HttpStatusCode.NoContent);
            }
            return new CollectionResultSet<DeterminationPolicy>(newListDeteminationPolicy);
        }


    }
}