﻿using AbsenceSoft;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Requests.CaseDeterminationStatus;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Linq;
using System.Net;

namespace AT.Api.Case.Requests.ChangePolicyStatus
{
    public class SaveCaseDeterminationRequest : ApiRequest<CasePolicyParameters, ResultSet<string>>
    {
        private readonly ICaseService _CaseService = null;

        /// <summary>
        /// Constructor
        /// </summary>      
        /// <param name="CaseService"></param>
        public SaveCaseDeterminationRequest(ICaseService CaseService = null)
        {
            _CaseService = CaseService ?? new CaseService();
        }
        protected override bool IsValid(CasePolicyParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.CaseNumber))
            {
                message = "Case number was not provided";
                return false;
            }
            foreach (var item in parameters.CaseDetermination.Policies)
            {
                if (string.IsNullOrWhiteSpace(item.PolicyCode))
                {
                    message = "Policy code was not provided";
                    return false;
                }
            }

            return true;
        }

        protected override ResultSet<string> FulfillRequest(CasePolicyParameters parameters)
        {
            var result = caseDeterminationPolicy(parameters);
            return (new ResultSet<string>(result, "Case  Determination's Policy Updated successfully"));
        }

        private string caseDeterminationPolicy(CasePolicyParameters parameters)
        {
            var _case = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);
            if (_case == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Case not found for the given case number");
            }

            foreach (var item in parameters.CaseDetermination.Policies)
            {
                string DeterminationDenialReasonCode = null;
                string DeterminationDenialReasonName = null;
                string denialExplanation = null;
                if (_case.Summary.Policies.Where(x => x.PolicyCode.Contains(item.PolicyCode)).FirstOrDefault() == null)
                {
                    throw new ApiException(HttpStatusCode.NotFound, "Supplied PolicyCode not found for the given case number");
                }
                if (parameters.CaseDetermination.DeterminationStatus == AdjudicationStatus.Denied)
                {
                    var determinationDenialReasons = _CaseService.GetAllDenialReasons(DenialReasonTarget.Policy).Select(ct => new { Code = ct.Code, Name = ct.Description });

                    if (item.DeterminationDenialReasonCode?.ToUpper() == "OTHER")
                    {
                        DeterminationDenialReasonCode = item.DeterminationDenialReasonCode;
                        denialExplanation = item.DenialExplanation;
                    }
                    else
                    if(determinationDenialReasons.Where(x => x.Code == item.DeterminationDenialReasonCode?.ToUpper()).FirstOrDefault() != null)
                    {
                        DeterminationDenialReasonCode = item.DeterminationDenialReasonCode;
                        DeterminationDenialReasonName = determinationDenialReasons.Where(x => x.Code== item.DeterminationDenialReasonCode).Select(x=>x.Name).FirstOrDefault();
                    }
                    else
                    {
                        throw new ApiException(HttpStatusCode.BadRequest, "Supplied Determination Denial Reason Code not found");
                    }

                }
                _case = _CaseService.ApplyDetermination(_case, item.PolicyCode, parameters.CaseDetermination.StartDate.Value, parameters.CaseDetermination.EndDate.Value, parameters.CaseDetermination.DeterminationStatus, DeterminationDenialReasonCode, denialExplanation, DeterminationDenialReasonName);

            }
            _case.OnSavedNOnce((o, c) => c.Entity.WfOnCaseAdjudicated(parameters.CaseDetermination.DeterminationStatus, null, null, null));
            _CaseService.UpdateCase(_case);
            _CaseService.EnqueueRecalcFutureCases(_case);
            return _case.CaseNumber;
        }
    }
}