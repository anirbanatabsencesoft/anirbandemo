﻿using AT.Api.Case.Models.CaseDeterminationModel;

namespace AT.Api.Case.Requests.CaseDeterminationStatus
{
    public class CasePolicyParameters
    {
        public string CaseNumber { get; set; }
        public CaseDeterminationModel CaseDetermination { get; set; }
    }
}