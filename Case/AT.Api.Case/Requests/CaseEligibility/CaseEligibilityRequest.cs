﻿using AbsenceSoft;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Case.Models.EligibityModel;
using AT.Api.Core.Request;
using AT.Common.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using Customers = AbsenceSoft.Data.Customers;
using cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Requests.CaseEligibility
{
    public class CaseEligibilityRequest : ApiRequest<CaseEligibilityParameters, ResultSet<CaseEligibiliyModel>>
    {
        private readonly ICaseService _CaseService = null;
        private readonly IEmployeeService _EmployeeService = null;
        private readonly IEligibilityService _EligibilityService = null;
        private readonly IEmployerService _IEmployerService = null;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="EmployeeService"></param>
        /// <param name="CaseService"></param>
        public CaseEligibilityRequest(string EmployerId, IEmployeeService EmployeeService = null, ICaseService CaseService = null, IEligibilityService _IEligibilityService = null, IEmployerService IEmployerService = null)
        {
            _CaseService = CaseService ?? new CaseService();
            _EmployeeService = EmployeeService ?? new EmployeeService();
            _IEmployerService = IEmployerService ?? new EmployerService();
            _EligibilityService = _IEligibilityService ?? new EligibilityService(_IEmployerService.GetById(EmployerId));            
        }
        protected override bool IsValid(CaseEligibilityParameters parameters, out string message)
        {
            message = string.Empty;
            if (string.IsNullOrEmpty(parameters.CaseModel.EmployeeNumber))
            {
                message = "Employee number is required";
                return false;
            }

            if (_EmployeeService.GetEmployeeByEmployeeNumber(parameters.CaseModel.EmployeeNumber, parameters.CustomerId, parameters.EmployerId) == null)
            {
                message = "Employee not found for the given employee number";
                return false;
            }
             
            return true;
        }
        protected override ResultSet<CaseEligibiliyModel> FulfillRequest(CaseEligibilityParameters parameters)
        {
            var result = CaseEligibilityforCase(parameters);
            return (new ResultSet<CaseEligibiliyModel>(result, "Eligibility has been checked successfully"));
        }

        public CaseEligibiliyModel CaseEligibilityforCase(CaseEligibilityParameters parameters)
        {
            var newCase = new cases.Case();
            var employee = _EmployeeService.GetEmployeeByEmployeeNumber(parameters.CaseModel.EmployeeNumber, parameters.CustomerId, parameters.EmployerId);           
            var Model = new CaseEligibiliyModel
            {
                CaseTypeId = (int)(Enum.Parse(typeof(CaseType), parameters.CaseModel.CaseType.ToString())),
                StartDate = parameters.CaseModel.StartDate,
                EndDate = parameters.CaseModel.EndDate,
                AbsenceReasonCode = parameters.CaseModel.ReasonCode,
            };
            Model = CalculateEligibility(Model, employee);
            Model.EmployeeNumber = parameters.CaseModel.EmployeeNumber;
            Model.StartDate = parameters.CaseModel.StartDate;
            Model.EndDate= parameters.CaseModel.EndDate;
            Model.AbsenceReasonCode = parameters.CaseModel.ReasonCode;
            Model.IsWorkRelated = parameters.CaseModel.CaseFlags.IsWorkRelated;
            Model.ExpectedDeliveryDate = parameters.CaseModel.CaseDates.ExpectedDeliveryDate;
            Model.ActualDeliveryDate= parameters.CaseModel.CaseDates.ActualDeliveryDate;            
            Model.WillUseBonding = parameters.CaseModel.CaseFlags.WillUseBonding;
            Model.BondingStartDate = parameters.CaseModel.CaseDates.BondingStartDate;
            Model.BondingEndDate = parameters.CaseModel.CaseDates.BondingEndDate;
            Model.AdoptionDate = parameters.CaseModel.CaseDates.AdoptionDate;
            return Model;
        }
        public CaseEligibiliyModel CalculateEligibility(CaseEligibiliyModel model, Customers.Employee employee)
        {
            var leaveOfAbsence = this.ToLeaveOfAbsence(model, employee);
            var returnModel = new CaseEligibiliyModel();
            this.PopulateCasePoliciesData(returnModel, leaveOfAbsence, model);
            if (returnModel.AvailableManualPolicies.Count > 0)
            {
                AppliedPolicyModel FMLAPolicyModel = returnModel.AvailableManualPolicies.Where(m => m.PolicyCode == "FMLA").FirstOrDefault();
                AppliedPolicyModel STDPolicyModel = returnModel.AvailableManualPolicies.Where(m => m.PolicyCode == "STD").FirstOrDefault();
                AppliedPolicyModel COMPMEDPolicyModel = returnModel.AvailableManualPolicies.Where(m => m.PolicyCode == "COMP-MED").FirstOrDefault();
                AppliedPolicyModel USERRAPolicyModel = returnModel.AvailableManualPolicies.Where(m => m.PolicyCode == "USERRA").FirstOrDefault();

                bool IsStdModelAvailabel = false;
                bool IsFmlaModelAvailabel = false;
                bool IsCompMedModelAvailabel = false;
                bool IsUserraModelAvailabel = false;

                if (STDPolicyModel != null)
                    IsStdModelAvailabel = true;
                if (FMLAPolicyModel != null)
                    IsFmlaModelAvailabel = true;
                if (COMPMEDPolicyModel != null)
                    IsCompMedModelAvailabel = true;
                if (USERRAPolicyModel != null)
                    IsUserraModelAvailabel = true;

                if (IsStdModelAvailabel)
                {
                    returnModel.AvailableManualPolicies.Remove(STDPolicyModel);
                    returnModel.AvailableManualPolicies.Insert(0, STDPolicyModel);
                }
                if (IsFmlaModelAvailabel)
                {
                    returnModel.AvailableManualPolicies.Remove(FMLAPolicyModel);
                    if (IsStdModelAvailabel)
                    {
                        returnModel.AvailableManualPolicies.Insert(1, FMLAPolicyModel);
                    }
                    else
                    {
                        returnModel.AvailableManualPolicies.Insert(0, FMLAPolicyModel);
                    }
                }

                if (IsCompMedModelAvailabel)
                {
                    returnModel.AvailableManualPolicies.Remove(COMPMEDPolicyModel);
                    if (!IsStdModelAvailabel && !IsFmlaModelAvailabel)
                    {
                        returnModel.AvailableManualPolicies.Insert(0, COMPMEDPolicyModel);
                    }
                    if (IsStdModelAvailabel && IsFmlaModelAvailabel)
                    {
                        returnModel.AvailableManualPolicies.Insert(2, COMPMEDPolicyModel);
                    }
                    if ((IsStdModelAvailabel && !IsFmlaModelAvailabel) || (!IsStdModelAvailabel && IsFmlaModelAvailabel))
                    {
                        returnModel.AvailableManualPolicies.Insert(1, COMPMEDPolicyModel);
                    }

                }
                if (IsUserraModelAvailabel)
                {
                    returnModel.AvailableManualPolicies.Remove(USERRAPolicyModel);
                    if (!IsStdModelAvailabel && !IsFmlaModelAvailabel && !IsCompMedModelAvailabel)
                    {
                        returnModel.AvailableManualPolicies.Insert(0, USERRAPolicyModel);
                    }
                    if (IsStdModelAvailabel && IsFmlaModelAvailabel && IsCompMedModelAvailabel)
                    {
                        returnModel.AvailableManualPolicies.Insert(3, USERRAPolicyModel);
                    }
                    if ((IsStdModelAvailabel && !IsFmlaModelAvailabel && !IsCompMedModelAvailabel) || (!IsStdModelAvailabel && IsFmlaModelAvailabel && !IsCompMedModelAvailabel) || (!IsStdModelAvailabel && !IsFmlaModelAvailabel && IsCompMedModelAvailabel))
                    {
                        returnModel.AvailableManualPolicies.Insert(1, USERRAPolicyModel);
                    }
                    if ((IsStdModelAvailabel && IsFmlaModelAvailabel && !IsCompMedModelAvailabel) || (IsStdModelAvailabel && !IsFmlaModelAvailabel && IsCompMedModelAvailabel) || (!IsStdModelAvailabel && IsFmlaModelAvailabel && IsCompMedModelAvailabel))
                    {
                        returnModel.AvailableManualPolicies.Insert(2, USERRAPolicyModel);
                    }
                }
            }
            return returnModel;
        }

        private LeaveOfAbsence ToLeaveOfAbsence(CaseEligibiliyModel model, Customers.Employee employee)
        {
            cases.Case newCase = null;
            if (model is CaseEligibiliyModel)
            {
                var createCaseModel = (CaseEligibiliyModel)model;

                var caseStatus = CaseStatus.Open;

                string absenceReasonCode = string.Empty;
                if (caseStatus != CaseStatus.Inquiry)
                {
                    absenceReasonCode =model.AbsenceReasonCode;
                }
                newCase = _CaseService.CreateCase(caseStatus, employee.Id, createCaseModel.StartDate.Value, createCaseModel.EndDate, (CaseType)createCaseModel.CaseTypeId.Value, absenceReasonCode);

                EmployeeContact contact = null;
                if (!String.IsNullOrWhiteSpace(createCaseModel.EmployeeRelationshipCode))
                {
                    newCase.Contact = new EmployeeContact()
                    {
                        EmployeeId = newCase.Employee.Id,
                        EmployeeNumber = newCase.Employee.EmployeeNumber,
                        EmployerId = newCase.EmployerId,
                        CustomerId = newCase.CustomerId,
                        ContactTypeCode = createCaseModel.EmployeeRelationshipCode,
                        ContactTypeName = ContactType.GetByCode(createCaseModel.EmployeeRelationshipCode).Name,
                        MilitaryStatus = (MilitaryStatus)(createCaseModel.MilitaryStatusId ?? 0)
                    };
                    contact = newCase.Contact;
                }
                if (createCaseModel.IsWorkRelated.HasValue)
                    newCase.Metadata.SetRawValue("IsWorkRelated", createCaseModel.IsWorkRelated.Value);

                if (newCase.Reason != null)
                {
                    switch (newCase.Reason.Code)
                    {
                        case "EHC":
                            break;
                        case "PREGMAT":
                        case "BONDING":
                            if (createCaseModel.ExpectedDeliveryDate.HasValue)
                                newCase.SetCaseEvent(CaseEventType.DeliveryDate, createCaseModel.ExpectedDeliveryDate.Value);
                            if (createCaseModel.ActualDeliveryDate.HasValue)
                                newCase.SetCaseEvent(CaseEventType.DeliveryDate, createCaseModel.ActualDeliveryDate.Value);
                            if (createCaseModel.WillUseBonding.HasValue)
                            {
                                newCase.Metadata.SetRawValue("WillUseBonding", createCaseModel.WillUseBonding.Value);
                                if (createCaseModel.WillUseBonding.Value)
                                {
                                    if (createCaseModel.BondingStartDate.HasValue)
                                        newCase.SetCaseEvent(CaseEventType.BondingStartDate, createCaseModel.BondingStartDate.Value);
                                    if (createCaseModel.BondingEndDate.HasValue)
                                        newCase.SetCaseEvent(CaseEventType.BondingEndDate, createCaseModel.BondingEndDate.Value);
                                }
                            }
                            break;
                        case "ADOPT":
                            if (createCaseModel.AdoptionDate.HasValue)
                                newCase.SetCaseEvent(CaseEventType.AdoptionDate, createCaseModel.AdoptionDate.Value);
                            break;
                        case "MILITARY":
                        case "EXIGENCY":
                        case "CEREMONY":
                        case "RESERVETRAIN":
                            if (contact != null)
                            {
                                contact.MilitaryStatus = createCaseModel.MilitaryStatusId.HasValue ? (MilitaryStatus)createCaseModel.MilitaryStatusId.Value : MilitaryStatus.Civilian;
                            }
                            else if (createCaseModel.MilitaryStatusId.HasValue)
                            {
                                newCase.Employee.MilitaryStatus = (MilitaryStatus)createCaseModel.MilitaryStatusId.Value;
                            }
                            break;
                    }
                }
            }

            if (newCase.Status != CaseStatus.Inquiry)
            {
                _EligibilityService.RunEligibility(newCase);
            }

            if (newCase.Segments != null && newCase.Segments.Any())
            {
                foreach (var policyModel in model.Policies)
                {
                    var segment = newCase.Segments.First();
                    var policy = segment.AppliedPolicies.FirstOrDefault(o => o.Policy.Code == policyModel.PolicyCode);
                    if (policy != null)
                    {
                        foreach (var ruleGroupModel in policyModel.RuleGroups)
                        {
                            var ruleGroup = policy.RuleGroups.FirstOrDefault(o => o.RuleGroup.Id == ruleGroupModel.Id);
                            if (ruleGroup != null)
                            {
                                foreach (var ruleModel in ruleGroupModel.Rules)
                                {
                                    var rule = ruleGroup.Rules.FirstOrDefault(o => o.Rule.Id == ruleModel.Id);
                                    if (rule != null)
                                    {
                                        if (ruleModel.Overridden)
                                        {
                                            rule.Overridden = true;
                                            rule.OverrideFromResult = ruleModel.Result;
                                            rule.Result = ruleModel.OverrideResult;
                                            rule.OverrideNotes = ruleModel.OverrideNotes;
                                            rule.OverrideValue = ruleModel.OverrideValue;
                                        }
                                        else
                                        {
                                            rule.Overridden = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // add manual policies per the model
            if (model.Policies.Any())
            {
                foreach (var manuallyAppliedPolicy in model.Policies.Where(x => x.IsAppliedPolicy == false))
                {
                    newCase = _EligibilityService.AddManualPolicy(newCase, manuallyAppliedPolicy.PolicyCode, manuallyAppliedPolicy.ManuallyAppliedPolicyNotes);

                }
            }

            // re-run eligibility on the newly updated policy data

            var leaveOfAbsence = _EligibilityService.RunEligibility(newCase);

            return leaveOfAbsence;
        }



        private void PopulateCasePoliciesData(CaseEligibilityPoliciesDataModel policyDataModel, LeaveOfAbsence leaveOfAbsence, CaseEligibilityPoliciesDataModel postedBackModel)
        {

            policyDataModel.AvailableManualPolicies.AddRange(_EligibilityService.GetAvailableManualPolicies(leaveOfAbsence.Case).Select(o => new AppliedPolicyModel()
            {
                PolicyId = o.Id,
                PolicyName = o.Name,
                PolicyCode = o.Code
            }));

            policyDataModel.Policies.AddRange(this.MapToModel(leaveOfAbsence.Case, leaveOfAbsence.Case.Segments.SelectMany(s => s.AppliedPolicies.Where(o => !o.ManuallyAdded && !o.Policy.IsDeleted)).ToList()));
            policyDataModel.Policies.AddRange(this.MapToModel(leaveOfAbsence.Case, leaveOfAbsence.Case.Segments.SelectMany(s => s.AppliedPolicies.Where(o => o.ManuallyAdded && !o.Policy.IsDeleted)).ToList()));

            // Set the See Spouse Case Flag
            if (!string.IsNullOrWhiteSpace(leaveOfAbsence.Case.SpouseCaseId))
            {
                policyDataModel.Policies.ForEach(p => { if (p.CombinedForSpouses) p.SeeSpouseCaseFlag = AffectedBySpouseCase(p, leaveOfAbsence.Case.SpouseCase); });
                policyDataModel.Policies.ForEach(p => { if (p.CombinedForSpouses) p.SeeSpouseCaseFlag = AffectedBySpouseCase(p, leaveOfAbsence.Case.SpouseCase); });
            }

            if (postedBackModel != null)
            {
                foreach (var policy in postedBackModel.Policies)
                {
                    if (policy.UserIsHandlingExpandCollapse)
                    {
                        var returnPolicy = policyDataModel.Policies.SingleOrDefault(o => o.PolicyCode == policy.PolicyCode);
                        if (returnPolicy != null)
                        {
                            returnPolicy.UserIsHandlingExpandCollapse = true;
                            returnPolicy.Expanded = policy.Expanded;
                        }
                    }
                }
                foreach (var policy in postedBackModel.Policies)
                {
                    if (policy.UserIsHandlingExpandCollapse)
                    {
                        var returnPolicy = policyDataModel.Policies.SingleOrDefault(o => o.PolicyCode == policy.PolicyCode);
                        if (returnPolicy != null)
                        {
                            returnPolicy.UserIsHandlingExpandCollapse = true;
                            returnPolicy.Expanded = policy.Expanded;
                        }
                    }
                }
            }

            var adjudications = GetCaseAdjudications(leaveOfAbsence.Case);
            if (adjudications != null && adjudications.Any())
            {
                Action<AppliedPolicyModel> getDetail = new Action<AppliedPolicyModel>(p =>
                {
                    var sum = adjudications.FirstOrDefault(a => a.PolicyCode == p.PolicyCode);
                    if (sum != null)
                    {
                        if (sum.Detail.Any())
                        {
                            p.StartDate = sum.Detail.Min(d => d.StartDate);
                            p.EndDate = sum.Detail.Max(d => d.EndDate);
                        }
                        p.Adjudications = sum.Detail;
                        p.Messages.AddRangeIfNotExists(sum.Messages);

                        p.AllowChangeEndDate = AbsenceSoft.Data.Security.User.Permissions.ProjectedPermissions.Contains(Permission.AdjudicateCase.Id) &&
                            leaveOfAbsence.Case.Segments.SelectMany(s => s.AppliedPolicies).Where(s => s.Policy.ConsecutiveTo.Contains(p.PolicyCode)).Any();
                    }
                });
                policyDataModel.Policies.ForEach(getDetail);
                policyDataModel.Policies.ForEach(getDetail);
            }
        }


        private List<AppliedPolicyModel> MapToModel(cases.Case myCase, List<cases.AppliedPolicy> appliedPolicies)
        {
            var list = new List<AppliedPolicyModel>();
            var sortedPolicies = appliedPolicies.OrderBy(p => string.Format("{0}_{1:yyyyMMdd}_{2}",
                     (int)p.Policy.PolicyType, p.StartDate, p.Policy.ConsecutiveTo.Count));
            foreach (var appliedPolicy in sortedPolicies.GroupBy(p => p.Policy.Code))
            {
                var lastEffectivePolicy = appliedPolicy.OrderByDescending(p => p.StartDate).FirstOrDefault();
                var appliedPolicyModel = new AppliedPolicyModel()
                {
                    PolicyId = lastEffectivePolicy.Policy.Id,
                    PolicyName = lastEffectivePolicy.Policy.Name,
                    PolicyCode = lastEffectivePolicy.Policy.Code,
                    StartDate = appliedPolicy.Min(p => p.StartDate),
                    EndDate = appliedPolicy.Max(p => p.EndDate),
                    EndDateText = appliedPolicy.Max(p => p.EndDate).ToUIString(),
                    StartDateText = appliedPolicy.Min(p => p.StartDate).ToUIString(),
                    Status = lastEffectivePolicy.Status,
                    ExhaustionDate = appliedPolicy.Min(p => p.FirstExhaustionDate),
                    CombinedForSpouses = lastEffectivePolicy.PolicyReason.CombinedForSpouses,
                    Paid = lastEffectivePolicy.PolicyReason.Paid && lastEffectivePolicy.Status == EligibilityStatus.Eligible && lastEffectivePolicy.Usage.Any(u => u.Determination == AdjudicationStatus.Approved),
                    IsAppliedPolicy = appliedPolicy.Where(x => x.ManuallyAdded).Any() ? false : true,
                };

                foreach (var ruleGroup in lastEffectivePolicy.RuleGroups.Where(r => r.RuleGroup != null && r.RuleGroup.RuleGroupType == PolicyRuleGroupType.Eligibility))
                {
                    var ruleGroupModel = new AppliedRuleGroupModel()
                    {
                        Id = ruleGroup.RuleGroup.Id,
                        Description = ruleGroup.RuleGroup.Description
                    };
                    appliedPolicyModel.RuleGroups.Add(ruleGroupModel);
                    foreach (var rule in ruleGroup.Rules)
                    {
                        var ruleModel = new AppliedRuleModel()
                        {
                            Id = rule.Rule.Id,
                            Description = rule.Rule.Description,
                            ActualValueString = rule.ActualValue == null ? "-nothing-" : (rule.ActualValueString ?? rule.ActualValue.ToString()),
                            Result = rule.Overridden ? rule.OverrideFromResult : rule.Result,
                            Overridden = rule.Overridden,
                            OverrideNotes = rule.OverrideNotes,
                            OverrideValue = rule.OverrideValue,
                            OverrideResult = rule.Result
                        };
                        appliedPolicyModel.HasOverriddenRule = appliedPolicyModel.HasOverriddenRule || rule.Overridden;

                        ruleGroupModel.Rules.Add(ruleModel);
                    }
                }
                appliedPolicyModel.Expanded = (lastEffectivePolicy.Status == EligibilityStatus.Ineligible || lastEffectivePolicy.Status == EligibilityStatus.Pending || appliedPolicyModel.HasOverriddenRule);


                if (!lastEffectivePolicy.ManuallyAdded && !appliedPolicyModel.RuleGroups.Any() && appliedPolicyModel.Status == EligibilityStatus.Eligible)
                    appliedPolicyModel.AlternateEligibilityMessage = _EligibilityService.GetWhySelected(myCase, lastEffectivePolicy);

                list.Add(appliedPolicyModel);
            }
            return list;
        }

        private bool AffectedBySpouseCase(AppliedPolicyModel policy, cases.Case spouseCase)
        {
            if (!policy.CombinedForSpouses || spouseCase == null)
                return false;

            return spouseCase.Segments.SelectMany(s => s.AppliedPolicies).Any(p => p.Policy.Code == policy.PolicyCode && p.Status != EligibilityStatus.Ineligible/* && p.Usage.Any(u => u.Determination == AdjudicationStatus.Approved)*/);
        }
        protected List<CasePolicySummaryModel> GetCaseAdjudications(cases.Case newCase)
        {
            List<CasePolicySummaryModel> model = new List<CasePolicySummaryModel>();

            foreach (var sm in newCase.Summary.Policies)
            {
                model.Add(new CasePolicySummaryModel()
                {
                    Policy = sm.PolicyName,
                    PolicyCode = sm.PolicyCode,
                    Messages = sm.Messages,
                    Detail = sm.Detail.Select(d => new CasePolicySummaryDetailModel()
                    {
                        CaseType = d.CaseType.ToString().SplitCamelCaseString(),
                        DenialExplanation = d.DenialReasonOther,
                        EndDate = d.EndDate,
                        EndDateText = d.EndDate.ToUIString(),
                        StartDate = d.StartDate,
                        StartDateText = d.StartDate.ToUIString(),
                        Status = d.Determination.ToString().SplitCamelCaseString()
                    }).ToList()
                });
            }

            return model;
        }
    }
}

