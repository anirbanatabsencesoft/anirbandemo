﻿using AT.Api.Case.Models;

namespace AT.Api.Case.Requests.CaseEligibility
{
    public class CaseEligibilityParameters
    {
        public CaseModel CaseModel { get; set; }
        public string CustomerId { get; set; }
        public string EmployerId { get; set; }
    }
}