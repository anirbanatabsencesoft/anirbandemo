﻿using AbsenceSoft;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AbsenceSoft.Models.Cases;
using AT.Api.Core.Request;
using System.Collections.Generic;
using System.Linq;

namespace AT.Api.Case.Requests.AbsenceReason
{
    public class GetAbsenceReasonRequest : ApiRequest<GetAbsenceReasonParameters, IEnumerable<AbsenceReasonModel>>
    {
        private readonly ICaseService _CaseService = null;
        private readonly IEmployeeService _EmployeeService = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="EmployeeService"></param>
        /// <param name="CaseService"></param>
        public GetAbsenceReasonRequest(IEmployeeService EmployeeService = null, ICaseService CaseService = null)
        {
            _CaseService = CaseService ?? new CaseService();
            _EmployeeService = EmployeeService ?? new EmployeeService();
        }
        protected override bool IsValid(GetAbsenceReasonParameters parameters, out string message)
        {
            message = string.Empty;
            if (string.IsNullOrEmpty(parameters.employeeNumber))
            {
                message = "Employee number is required";
                return false;
            }
            if (_EmployeeService.GetEmployeeByEmployeeNumber(parameters.employeeNumber, parameters.CustomerId, parameters.EmployerId) == null)
            {
                message = "Employee not found for the given employee number";
                return false;
            }
            return true;
        }
        protected override IEnumerable<AbsenceReasonModel> FulfillRequest(GetAbsenceReasonParameters parameters)
        {
            return GetAbsenceReasonForCase(parameters);
        }
        public IEnumerable<AbsenceReasonModel> GetAbsenceReasonForCase(GetAbsenceReasonParameters parameters)
        {
            var list = new List<AbsenceReasonModel>();
            //Get employee/employer specific
            var absences = _CaseService.GetAbsenceReasons(_EmployeeService.GetEmployeeByEmployeeNumber(parameters.employeeNumber, parameters.CustomerId, parameters.EmployerId).Id, parameters.CaseType);

            // first get non-hierarchical ones

            absences.Where(o => o.Category == null)
                .ForEach(o => list.Add(DataExtensions.AbsenceReasonModelExtension.ToAbsenceReasonModelNonHirachical(o, false, false)));

            // now get the categories

            absences.Where(o => o.Category != null).Select(o => o.Category).Distinct()
                .ForEach(category => list.Add(new AbsenceReasonModel()
                {
                    HelpText = category,
                    Name = category,
                    Category = category,
                    IsCategory = true,
                    IsChild = false
                }));

            // now get the categories' children

            list.Where(o => o.Id == null).ForEach(o =>
                o.Children.AddRange(
                    absences.Where(child => child.Category == o.Category).Select(child => DataExtensions.AbsenceReasonModelExtension.ToAbsenceReasonModelNonHirachical(child, false, true))));

            var maxIndex = list.Count - 1;

            var item = list.Where(m => m.Name == "Adoption/Foster Care").FirstOrDefault();
            if (item != null && maxIndex >= 3)
            {
                list.Remove(item);
                list.Insert(3, item);
            }

            item = list.Where(m => m.Name == "Military").FirstOrDefault();
            if (item != null && maxIndex >= 4)
            {
                list.Remove(item);
                list.Insert(4, item);
            }

            item = list.Where(m => m.Name == AbsenceReasonCategory.Administrative).FirstOrDefault();
            if (item != null && maxIndex >= 6)
            {
                list.Remove(item);
                list.Insert(6, item);
            }

            return list;
        }


    }
}