﻿using AbsenceSoft.Data.Enums;

namespace AT.Api.Case.Requests.AbsenceReason
{
    public class GetAbsenceReasonParameters
    {
        public string employeeNumber { get; set; }
        public CaseType? CaseType { get; set; }

        public string CustomerId { get; set; }
        public string EmployerId { get; set; }
    }
}