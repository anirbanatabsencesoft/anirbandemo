﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System;
using System.Net;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Requests.WorkRestriction
{
    /// <summary>
    /// Delete Work Restriction Request
    /// </summary>
    public class DeleteWorkRestrictionRequest : ApiRequest<DeleteWorkRestrictionParameters, ResultSet<bool>>
    {
        private readonly ICaseService _CaseService = null;
        private readonly IEmployerService _EmployerService = null;
        private readonly IDemandService _DemandService = null;
        private readonly IAdminUserService _AdminUserService = null;
        private readonly User CurrentUser;

        /// <summary>
        /// Constructor
        /// </summary>        
        /// <param name="userId"></param>
        /// <param name="caseService"></param>
        /// <param name="employerService"></param>
        /// <param name="demandService"></param>
        /// <param name="adminUserService"></param>
        public DeleteWorkRestrictionRequest(string userId = null, ICaseService caseService = null, IEmployerService employerService = null, IDemandService demandService = null, IAdminUserService adminUserService = null)
        {
            _CaseService = caseService ?? new CaseService();
            _EmployerService = employerService ?? new EmployerService();
            _AdminUserService = adminUserService ?? new AdminUserService();
            if (!string.IsNullOrWhiteSpace(userId))
            {
                CurrentUser = _AdminUserService.GetUserById(userId);
                if (CurrentUser == null)
                {
                    throw new ApiException(HttpStatusCode.InternalServerError, "Unable to find user");
                }
            }
            _DemandService = demandService ?? new DemandService(CurrentUser);
        }

        /// <summary>
        /// override abstrat method to fulfillasync request
        /// Delete Work Restriction
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override ResultSet<bool> FulfillRequest(DeleteWorkRestrictionParameters parameters)
        {
            Cases.Case myCase = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);

            if (myCase == null)
            {
                throw new ApiException(HttpStatusCode.ExpectationFailed, "Case not found with the given details");
            }

            _DemandService.DeleteJobRestriction(parameters.WorkRestrictionId);
            
            return (new ResultSet<bool>(true, "Work restriction deleted successfully"));
        }

        /// <summary>
        /// Validate input parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool IsValid(DeleteWorkRestrictionParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.WorkRestrictionId))
            {
                message = "Work restriction id not provided";
                return false;
            }

            if (String.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Missing EmployerId";
                return false;
            }

            if (!string.IsNullOrEmpty(parameters.EmployerId))
            {
                Employer employer = _EmployerService.GetById(parameters.EmployerId);
                if (employer == null)
                {
                    message = "Employer is not valid.";
                    return false;
                }
            }

            return true;
        }
    }
}