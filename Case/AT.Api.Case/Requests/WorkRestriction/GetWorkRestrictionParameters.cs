﻿namespace AT.Api.Case.Requests.WorkRestriction
{
    /// <summary>
    /// GetWorkRestrictionParameters Request Parameters
    /// </summary>
    public class GetWorkRestrictionParameters
    {
        /// <summary>
        /// Case Number for the request
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// EmployerId
        /// </summary>
        public string EmployerId { get; set; }
    }
}