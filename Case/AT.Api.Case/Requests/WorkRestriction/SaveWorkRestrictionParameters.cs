﻿using AT.Api.Case.Models.WorkRestriction;

namespace AT.Api.Case.Requests.WorkRestriction
{
    /// <summary>
    /// Api GetWorkRestrictionParameters Request Parameters
    /// </summary>
    public class SaveWorkRestrictionParameters
    {
        /// <summary>
        /// Case Number for the request
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// EmployerId
        /// </summary>
        public string EmployerId { get; set; }

        /// <summary>
        /// WorkRestrictionModel to save the job restriction against employee
        /// </summary>
        public CreateWorkRestrictionModel WorkRestriction { get; set; }
    }
}