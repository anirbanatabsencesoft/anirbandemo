﻿namespace AT.Api.Case.Requests.WorkRestriction
{
    /// <summary>
    /// Api GetWorkRestrictionParameters Request Parameters
    /// </summary>
    public class DeleteWorkRestrictionParameters
    {
        /// <summary>
        /// Case Number for the request
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// WorkRestrictionId to delete the job restriction
        /// </summary>
        public string WorkRestrictionId { get; set; }

        /// <summary>
        /// EmployerId
        /// </summary>
        public string EmployerId { get; set; }
    }
}