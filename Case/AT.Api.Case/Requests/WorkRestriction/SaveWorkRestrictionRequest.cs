﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Case.Models.WorkRestriction;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Requests.WorkRestriction
{
    /// <summary>
    /// Save Certifications Request
    /// </summary>
    public class SaveWorkRestrictionRequest : ApiRequest<SaveWorkRestrictionParameters, ResultSetBase>
    {
        private readonly IEmployerService _EmployerService = null;
        private readonly ICaseService _CaseService = null;
        private readonly IAdminUserService _AdminUserService = null;
        private readonly IDemandService _DemandService = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userId"></param>     
        /// <param name="caseService"></param>        
        /// <param name="demandService"></param>        
        /// <param name="adminUserService"></param>    
        /// <param name="employerService"></param>    
        public SaveWorkRestrictionRequest(string userId = null, ICaseService caseService = null, IDemandService demandService = null, IAdminUserService adminUserService = null, IEmployerService employerService = null)
        {
            _CaseService = caseService ?? new CaseService();
            _EmployerService = employerService ?? new EmployerService();
            _AdminUserService = adminUserService ?? new AdminUserService();

            if (!string.IsNullOrWhiteSpace(userId))
            {
                var currentUser = _AdminUserService.GetUserById(userId);

                if (currentUser == null)
                {
                    throw new ApiException(HttpStatusCode.InternalServerError, "Unable to find user");
                }

                _DemandService = demandService ?? new DemandService(currentUser);
            }            
        }

        /// <summary>
        /// override abstrat method to fulfillasync request
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override ResultSetBase FulfillRequest(SaveWorkRestrictionParameters parameters)
        {
            Cases.Case theCase = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);
            var model = parameters.WorkRestriction;

            if (theCase == null)
            {
                throw new ApiException(HttpStatusCode.ExpectationFailed, "Case not found with the given details");
            }

            if (theCase.Employee == null)
            {
                throw new ApiException(HttpStatusCode.ExpectationFailed, "Employee not found");
            }

            var theDemand = _DemandService.GetDemandByCode(model.DemandCode, theCase.CustomerId);

            if (theDemand == null)
            {
                throw new ApiException(HttpStatusCode.ExpectationFailed, "Demand not found for the given code");
            }

            EmployeeRestriction workRestriction = !string.IsNullOrWhiteSpace(model.Id) ? _DemandService.GetEmployeeRestrictionById(model.Id) : null;

            if (workRestriction == null)
            {
                workRestriction = new EmployeeRestriction()
                {
                    Restriction = new AbsenceSoft.Data.Jobs.WorkRestriction(),
                };
            }

            workRestriction.CaseId = theCase.Id;
            workRestriction.CaseNumber = theCase.CaseNumber;
            workRestriction.EmployeeId = theCase.Employee.Id;
            workRestriction.CustomerId = theCase.CustomerId;
            workRestriction.EmployerId = theCase.EmployerId;
                        
            workRestriction.Restriction.DemandId = theDemand.Id;

            workRestriction.Restriction.Dates = !model.StartDate.HasValue ? AbsenceSoft.DateRange.Null : new AbsenceSoft.DateRange(model.StartDate.Value, model.EndDate);

            var values = new List<AppliedDemandValue<WorkRestrictionValue>>(model.Values.Count());

            foreach (var value in model.Values)
            {
                values.Add(BuildWorkRestrictionValue(value, theCase.CustomerId));
            }

            workRestriction.Restriction.Values = values;
            _DemandService.SaveJobRestriction(workRestriction);

            return new ResultSetBase("Work restriction information saved successfully");
        }

        /// <summary>
        /// Builds a work restriction value from the UI model
        /// </summary>
        /// <param name="valueModel"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        private AppliedDemandValue<WorkRestrictionValue> BuildWorkRestrictionValue(AppliedDemandValueModel valueModel, string customerId)
        {
            var theDemandType = _DemandService.GetDemandTypeByCode(valueModel.DemandTypeCode, customerId);

            if (theDemandType == null)
            {
                throw new ApiException(HttpStatusCode.ExpectationFailed, "Demand type not found for the given code");
            }

            //var theDemandItem = theDemandType.Items.FirstOrDefault(i => i.Name == valueModel.DemandItemName);

            //if (theDemandItem == null)
            //{
            //    throw new ApiException(HttpStatusCode.ExpectationFailed, "Demand item not found for the given name");
            //}

            AppliedDemandValue<WorkRestrictionValue> value = new AppliedDemandValue<WorkRestrictionValue>()
            {
                Type = (DemandValueType)valueModel.Type,
                //DemandTypeId = theDemandType.Id
                DemandType = theDemandType
            };

            switch ((DemandValueType)valueModel.Type)
            {
                case DemandValueType.Text:
                    value.Text = valueModel.Text;
                    break;
                case DemandValueType.Boolean:
                    value.Applicable = valueModel.Applicable;
                    break;
                case DemandValueType.Value:
                    if (!string.IsNullOrWhiteSpace(valueModel.DemandItemName))
                    {
                        var theDemandItem = theDemandType.Items.FirstOrDefault(i => i.Name == valueModel.DemandItemName);

                        if (theDemandItem == null)
                        {
                            throw new ApiException(HttpStatusCode.ExpectationFailed, "Demand item not found for the given name");
                        }

                        value.DemandItem = value.DemandType.Items.FirstOrDefault(i => i.Name == valueModel.DemandItemName);
                    }
                    break;
                case DemandValueType.Schedule:
                    if (valueModel.DurationType.HasValue && valueModel.FrequencyType.HasValue)
                    {
                        value.Schedule = new ScheduleDemand()
                        {
                            Duration = valueModel.Duration,
                            DurationType = valueModel.DurationType.HasValue ? (Unit)valueModel.DurationType.Value : Unit.Minutes,
                            Frequency = valueModel.Frequency,
                            FrequencyType = valueModel.FrequencyType.HasValue ? (Unit)valueModel.FrequencyType.Value : Unit.Minutes,
                            FrequencyUnitType = (DayUnitType?)valueModel.FrequencyUnitType ?? DayUnitType.Workday,
                            Occurances = valueModel.Occurances
                        };
                    }
                    break;
                default:
                    break;
            }
            return value;
        }

        /// <summary>
        /// Validate input parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool IsValid(SaveWorkRestrictionParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrEmpty(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }

            if (parameters.WorkRestriction == null)
            {
                message = "Work restriction details not provided";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.WorkRestriction.DemandCode))
            {
                message = "DemandCode not provided";
                return false;
            }

            if (!parameters.WorkRestriction.StartDate.HasValue)
            {
                message = "Start date not provided";
                return false;
            }

            if (!parameters.WorkRestriction.EndDate.HasValue)
            {
                message = "End date not provided";
                return false;
            }

            if (parameters.WorkRestriction.StartDate > parameters.WorkRestriction.EndDate)
            {
                message = "End date should be greater than start date";
                return false;
            }

            if (parameters.WorkRestriction.Values == null)
            {
                message = "Work restriction values not provided";
                return false;
            }

            if (parameters.WorkRestriction.Values != null)
            {
                var misingDemandOptionValue = false;
                StringBuilder errors = new StringBuilder();

                foreach (var item in parameters.WorkRestriction.Values)
                {
                    switch (item.Type)
                    {
                        case 0:
                            if (string.IsNullOrWhiteSpace(item.Text))
                            {
                                misingDemandOptionValue = true;
                                errors.AppendLine("Text field is required for Text demand type");
                            }
                            break;
                        case 1:
                            if (!item.Applicable.HasValue)
                            {
                                misingDemandOptionValue = true;
                                errors.AppendLine("Applicable flag is required for Boolean demand type.");
                            }
                            break;
                        case 2:
                            if (string.IsNullOrWhiteSpace(item.DemandItemName))
                            {
                                misingDemandOptionValue = true;
                                errors.AppendLine("DemandItemName is required for Value demand type.");
                            }
                            break;
                        case 3:
                            if (!item.FrequencyType.HasValue || !item.FrequencyUnitType.HasValue || !item.DurationType.HasValue)
                            {
                                misingDemandOptionValue = true;
                                errors.AppendLine("Schedule frequency fields are required for Schedule demand type.");
                            }
                            break;
                    }
                }

                if (misingDemandOptionValue)
                {
                    message = "Must fill the value(s) for the demand item(s)." + Environment.NewLine + errors.ToString();
                    return false;
                }
            }

            if (String.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Missing EmployerId";
                return false;
            }

            if (!string.IsNullOrEmpty(parameters.EmployerId))
            {
                Employer employer = _EmployerService.GetById(parameters.EmployerId);
                if (employer == null)
                {
                    message = "Employer is not valid.";
                    return false;
                }
            }

            return true;
        }
    }
}