﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Case.DataExtensions;
using AT.Api.Case.Models.WorkRestriction;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System;
using System.Collections.Generic;
using System.Net;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Requests.WorkRestriction
{
    /// <summary>
    /// Get Work Restriction Request
    /// </summary>    
    public class GetWorkRestrictionRequest : ApiRequest<GetWorkRestrictionParameters, CollectionResultSet<WorkRestrictionModel>>
    {
        private readonly ICaseService _CaseService = null;
        private readonly IEmployerService _EmployerService = null;
        private readonly IDemandService _DemandService = null;
        private readonly IAdminUserService _AdminUserService = null;
        private readonly User CurrentUser;

        /// <summary>
        /// Constructor
        /// </summary>        
        /// <param name="userId"></param>
        /// <param name="caseService"></param>
        /// <param name="employerService"></param>
        /// <param name="demandService"></param>
        /// <param name="adminUserService"></param>
        public GetWorkRestrictionRequest(string userId = null, ICaseService caseService = null, IEmployerService employerService = null, IDemandService demandService = null, IAdminUserService adminUserService = null)
        {
            _CaseService = caseService ?? new CaseService();
            _EmployerService = employerService ?? new EmployerService();
            _AdminUserService = adminUserService ?? new AdminUserService();
            if (!string.IsNullOrWhiteSpace(userId))
            {
                CurrentUser = _AdminUserService.GetUserById(userId);
                if (CurrentUser == null)
                {
                    throw new ApiException(HttpStatusCode.InternalServerError, "Unable to find user");
                }
                _DemandService = demandService ?? new DemandService(CurrentUser);
            }

        }

        /// <summary>
        /// override abstrat method to fulfillasync request
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override CollectionResultSet<WorkRestrictionModel> FulfillRequest(GetWorkRestrictionParameters parameters)
        {
            return GetWorkRestrictions(parameters.CaseNumber);
        }

        /// <summary>
        /// Get Time Tracking Info For Case
        /// </summary>
        /// <param name="CaseNumber"></param>        
        /// <returns></returns>
        public CollectionResultSet<WorkRestrictionModel> GetWorkRestrictions(string CaseNumber)
        {
            Cases.Case cases = _CaseService.GetCaseByCaseNumber(CaseNumber);
            if (cases == null)
            {
                throw new ApiException(HttpStatusCode.ExpectationFailed, "Case not found with the given details");
            }

            if (cases.Employee == null)
            {
                throw new ApiException(HttpStatusCode.ExpectationFailed, "Employee not found");
            }

            var workRestrictionModels = new List<WorkRestrictionModel>();

            List<EmployeeRestriction> restrictions = _DemandService.GetJobRestrictionsForEmployee(cases.Employee.Id, cases.Id);

            if (restrictions != null && restrictions.Count > 0)
            {
                workRestrictionModels = WorkRestrictionExtensions.ToWorkRestrictionModel(restrictions);

                workRestrictionModels.ForEach(o =>
                {
                    o.CaseId = cases.Id.ToString();
                    o.EmployeeId = cases.Employee.Id.ToString();
                });
            }

            return new CollectionResultSet<WorkRestrictionModel>(workRestrictionModels);
        }

        /// <summary>
        /// Validate input parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool IsValid(GetWorkRestrictionParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrEmpty(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }
            if (String.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Missing EmployerId";
                return false;
            }

            if (!string.IsNullOrEmpty(parameters.EmployerId))
            {
                Employer employer = _EmployerService.GetById(parameters.EmployerId);
                if (employer == null)
                {
                    message = "Employer is not valid.";
                    return false;
                }
            }
            return true;
        }
    }
}