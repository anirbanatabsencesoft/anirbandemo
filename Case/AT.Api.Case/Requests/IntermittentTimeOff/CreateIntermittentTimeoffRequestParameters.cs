﻿using AT.Api.Case.Models.IntermittentTimeoff;

namespace AT.Api.Case.Requests.IntermittentTimeOff
{
    /// <summary>
    /// 
    /// </summary>
    public class CreateIntermittentTimeoffRequestParameters
    {
        /// <summary>
        /// Case Number
        /// </summary>
        public string CaseNumber { get; set; }
        /// <summary>
        /// IntermittentTimeOffModel as a parameter to create timeoff request
        /// </summary>
        public IntermittentTimeOffModel IntermittentTimeOff { get; set; }
    }
}