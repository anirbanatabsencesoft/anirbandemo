﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.DataExtensions;
using AT.Api.Case.Models.IntermittentTimeoff;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Requests.IntermittentTimeOff
{
    /// <summary>
    /// Api GetIntermittentTimeoff Request by case id
    /// </summary>
    public class GetIntermittentTimeOffRequestByCaseRequest : ApiRequest<GetIntermittentTimeOffRequestParameters, CollectionResultSet<IntermittentTimeOffModel>>
    {
        private readonly ICaseService _CaseService = null;
        private readonly IAdminUserService _AdminUserService = null;

        /// <summary>
        /// Constructor
        /// </summary>        
        /// <param name="caseService"></param>
        /// <param name="customerId"></param>
        /// <param name="employerId"></param>
        /// <param name="userId"></param>
        /// <param name="adminUserService"></param>

        public GetIntermittentTimeOffRequestByCaseRequest(
            string userId,
            string employerId,
            string customerId,
            ICaseService caseService = null,
            IAdminUserService adminUserService = null)
        {
            _AdminUserService = adminUserService ?? new AdminUserService();

            var user = _AdminUserService.GetUserById(userId);

            if (user == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Unable to find user");
            }

            _CaseService = caseService ?? new CaseService(customerId, employerId, user);
        }

        /// <summary>
        /// override abstract method to fulfillasync request
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>

        protected override CollectionResultSet<IntermittentTimeOffModel> FulfillRequest(GetIntermittentTimeOffRequestParameters parameters)
        {
            Cases.Case existingCase = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);

            if (existingCase == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Case not found with the given details");
            }

            var requestDate = parameters.RequestDate;

            var segments = existingCase.Segments
                .Where(summary => summary.Type == CaseType.Intermittent &&
                    summary.Status != CaseStatus.Cancelled)
                .FirstOrDefault();

            if (segments == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Invalid request, requested date is not part of an intermittent portion of this case");
            }

            var intermittentTimeRequests = segments.UserRequests;
           
            if (intermittentTimeRequests == null)
            {
                return new CollectionResultSet<IntermittentTimeOffModel>(null, "No time off requests found");
            }

            var policySummaries = _CaseService.GetEmployeePolicySummaryByCaseId(existingCase.Id, null);
            List<IntermittentTimeOffModel> lsttimeoffs = new List<IntermittentTimeOffModel>();
          
            for (int i = intermittentTimeRequests.Count - 1; i >= 0; i--)
            {
                var model = intermittentTimeRequests[i].ToIntermittentTimeOffModel(policySummaries);            
                lsttimeoffs.Add(model);
            }

            lsttimeoffs = lsttimeoffs.OrderByDescending(x => x.CreatedDate).ToList();
            return new CollectionResultSet<IntermittentTimeOffModel>(lsttimeoffs, "Intermittent time off retrieved");
        }

        /// <summary>
        /// Validate input parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>

        protected override bool IsValid(GetIntermittentTimeOffRequestParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrEmpty(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }

            return true;
        }
    }
}