﻿using AbsenceSoft;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Notes;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Api.Case.Models.IntermittentTimeoff;
using AT.Common.Core;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Collections.Generic;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Logic.Cases.Contracts;


namespace AT.Api.Case.Requests.IntermittentTimeOff
{
    /// <summary>
    /// Api Create Intermittent Timeoff Request
    /// </summary>
    public class CreateIntermittentTimeoffRequest : ApiRequest<CreateIntermittentTimeoffRequestParameters, ResultSet<string>>
    {
        private readonly ICaseService _caseService = null;
        private readonly IAdminUserService _AdminUserService = null;
        User CurrentUser;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="adminUserService"></param>
        /// <param name="caseService"></param>
        public CreateIntermittentTimeoffRequest(string userId, IAdminUserService adminUserService = null, ICaseService caseService = null)
        {
            _caseService = caseService ?? new CaseService();
            _AdminUserService = adminUserService ?? new AdminUserService();
            CurrentUser = _AdminUserService.GetUserById(userId);
            if (CurrentUser == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Unable to find user");
            }
        }

        /// <summary>
        /// fulfill Create Intermittent Timeoff Request
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override ResultSet<string> FulfillRequest(CreateIntermittentTimeoffRequestParameters parameters)
        {
            bool result = CreateOrModifyTimeOffRequest(parameters.CaseNumber, parameters.IntermittentTimeOff);
            return (new ResultSet<string>(result.ToString(), "Intermittent timeoff request saved successfully"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CaseNumber"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public bool CreateOrModifyTimeOffRequest(string CaseNumber, IntermittentTimeOffModel viewModel)
        {

            AbsenceSoft.Data.Cases.Case c = _caseService.GetCaseByCaseNumber(CaseNumber);
            if (c == null)
                throw new ApiException(HttpStatusCode.ExpectationFailed, "Case not found");

            var seg = c.Segments
                .Where(s => s.Type == CaseType.Intermittent && s.Status != CaseStatus.Cancelled && viewModel.RequestDate.DateInRange(s.StartDate, s.EndDate)).FirstOrDefault();
            if (seg == null)
                throw new ApiException(HttpStatusCode.ExpectationFailed, "Invalid request, requested date is not part of an intermittent portion of this case");

            if(seg.AppliedPolicies.Count==0)
                throw new ApiException(HttpStatusCode.ExpectationFailed, "Requested Date is Invalid, Cannnot Process this Request");


            List<IntermittentTimeRequest> intermittentTimeRequests = new List<IntermittentTimeRequest>();


            //We need to loop through the list of Applied policy to generate the maximum no of Request date
            //as different policy can have different intermittent settings. 
            foreach (var pol in seg.AppliedPolicies)
            {
                if (pol.PolicyReason.IntermittentRestrictionsMinimum.HasValue && viewModel.ApprovedTime > 0)
                {
                    intermittentTimeRequests = _caseService.GenerateIntermittentRequests(c, pol.PolicyReason, viewModel.RequestDate, intermittentTimeRequests);
                }
                else
                {
                    if (!intermittentTimeRequests.Exists(x => x.RequestDate == viewModel.RequestDate.Date))
                    {
                        intermittentTimeRequests.Add(seg.UserRequests.FirstOrDefault(u => u.RequestDate.Date == viewModel.RequestDate.Date && u.Id== viewModel.Id) ?? new IntermittentTimeRequest()
                        {
                            RequestDate = viewModel.RequestDate,
                            IntermittentType = viewModel.IntermittentType.HasValue ? (IntermittentType)(viewModel.IntermittentType.Value) : (IntermittentType?)null
                        });
                    }
                }
            }
             
            foreach (var r in intermittentTimeRequests)
            {
                r.IntermittentType = r.IntermittentType.HasValue ? (IntermittentType)(r.IntermittentType.Value) : (IntermittentType?)null;
                r.Notes = viewModel.Notes;

                #region Commented for WebAPI
                //// When request from Employee/HR/Sup login, set request made by employee true 
                //// and set TotalMinutes 
                //if (this.IsEmployeeSelfService && new AuthenticationService().Using(m => m.IsEmployee(CurrentUser, CurrentCustomer)))
                //{
                //    r.EmployeeEntered = true;
                //    r.TotalMinutes = viewModel.PendingTime;
                //}
                //else
                //{
                //    r.EmployeeEntered = false;// CurrentUser.EmployeeId == c.Employee.Id;
                //}
                #endregion

                r.EmployeeEntered = false;
                viewModel.Details = viewModel.Details ?? new List<IntermittentTimeOffRequestDetailModel>(0);
                // Uh oh, there are no details, shame on that UI, we'll just build them out for each of the policies 'n' stuff
                if (viewModel.Details.Count == 0)
                {
                    viewModel.Details = seg.AppliedPolicies.Where(p => p.Status == EligibilityStatus.Eligible).Select(p => new IntermittentTimeOffRequestDetailModel()
                    {
                        PolicyCode = p.Policy.Code,
                        ApprovedTime = viewModel.ApprovedTime,
                        PendingTime = viewModel.PendingTime,
                        DeniedTime = viewModel.DeniedTime,
                        //DenialReason = viewModel.DenialReason,
                        DenialReasonCode = viewModel.DenialReasonCode,
                        DenialReasonName = viewModel.DenialReasonName,
                        DenialReasonOther = viewModel.DenialReasonOther
                    }).ToList();
                }

                // Intermitent datetime interval 
                // Time interval changes 
                r.StartTimeForLeave =  viewModel.StartTimeForLeave;
                r.EndTimeForLeave =  viewModel.EndTimeForLeave;

                r.TotalMinutes = viewModel.Details.Count > 0 ? viewModel.Details.Max(d => d.ApprovedTime + d.PendingTime + d.DeniedTime) : 0;
                //need to handle the details for each policy seperately
                //We just need to create the new intermittent request only for the policy which has 
                //intermittent restrictions on.
                List<IntermittentTimeRequestDetail> list = new List<IntermittentTimeRequestDetail>();
                foreach (IntermittentTimeOffRequestDetailModel vmDetail in viewModel.Details)
                {
                    int approvedTime = vmDetail.ApprovedTime;
                    IntermittentTimeRequestDetail intermittentTimeRequestDetail = null;
                    //One case can have multipile applied Policy and everyone may not have IntermittentRestrictionsMinimum set
                    //but all of them will have same policy reason.

                    var appliedPolicy = seg.AppliedPolicies.FirstOrDefault(pol => pol.Policy.Code == vmDetail.PolicyCode);

                    if (approvedTime > 0 && appliedPolicy != null && appliedPolicy.PolicyReason.IntermittentRestrictionsMinimum.HasValue)
                    {
                        //if the Policy is configured to use IntermittentRestrictions then do the calculations
                        approvedTime = _caseService.GetTorTime(c, appliedPolicy.PolicyReason, approvedTime);
                        intermittentTimeRequestDetail = GetUserRequestDetail(vmDetail, approvedTime, r.IsMaxOccurenceReached);
                    }
                    else if (r.RequestDate == viewModel.RequestDate)
                    {
                        intermittentTimeRequestDetail = GetUserRequestDetail(vmDetail, approvedTime, r.IsMaxOccurenceReached);
                    }

                    if (intermittentTimeRequestDetail != null)
                    {
                        list.Add(intermittentTimeRequestDetail);
                        if (approvedTime > vmDetail.ApprovedTime)
                        {
                            r.IsIntermittentRestriction = true;
                        }
                    }
                }

                r.Detail = list;
                c = _caseService.CreateOrModifyTimeOffRequest(c, intermittentTimeRequests);

                if (!string.IsNullOrWhiteSpace(r.Notes))
                {
                    _caseService.IntermittentAbsenceEntryNote(c, r, CurrentUser);
                }
                c = _caseService.UpdateCase(c);
            }
            return true;
        }

        /// <summary>
        /// Get user request details from IntermittentTimeOffRequestDetailModel
        /// </summary>
        /// <param name="vmDetail"></param>
        /// <param name="approvedTime"></param>
        /// <param name="isMaxOccurenceReached"></param>
        /// <returns></returns>
        private IntermittentTimeRequestDetail GetUserRequestDetail(IntermittentTimeOffRequestDetailModel vmDetail, int approvedTime, bool isMaxOccurenceReached)
        {
            int policyTotalMinutes = approvedTime + vmDetail.DeniedTime + vmDetail.PendingTime;
            IntermittentTimeRequestDetail detail = new IntermittentTimeRequestDetail();
            detail.PolicyCode = vmDetail.PolicyCode;
            //HACK: TODO: Validate with Seth and Dave this is accurate here... Not sure.
            // For pending, we know mistakes happen, automatically place any "unaccounted for" time in pending and give the
            //  user the benefit of the doubt instead of throwing nasty error messages.
            detail.Pending = (policyTotalMinutes - (approvedTime + vmDetail.DeniedTime));
            //if MaxOccurenceReached flag is set for any date , we should mark the Denined reason and also ApprovedTime will be set to denied
            if (isMaxOccurenceReached)
            {
                detail.Denied = approvedTime;
                detail.DenialReasonCode = AdjudicationDenialReason.MaxOccurrenceReached;
            }
            else
            {
                detail.Approved = approvedTime;
                detail.Denied = vmDetail.DeniedTime;
                //detail.DeniedReason = vmDetail.DenialReason != null ? (AdjudicationDenialReason)vmDetail.DenialReason : (AdjudicationDenialReason?)null;
                detail.DenialReasonCode = vmDetail.DenialReasonCode;
                detail.DeniedReasonOther = vmDetail.DenialReasonOther;
            }
            return detail;
        }

        /// <summary>
        /// Validate required parameters from model
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool IsValid(CreateIntermittentTimeoffRequestParameters parameters, out string message)
        {
            message = string.Empty;
            if (string.IsNullOrEmpty(parameters.CaseNumber))
            {
                message = "Case number was not provided";
                return false;
            }
            if (parameters.IntermittentTimeOff.RequestDate == DateTime.MinValue)
            {
                message = "Requested date was not provided";
                return false;
            }
            if (parameters.IntermittentTimeOff.IntermittentType == null)
            {
                message = "Intermittent Type was not provided. Set value to 0 = OfficeVisit, 1 = Incapacity";
                return false;
            }

            return true;
        }
    }
}