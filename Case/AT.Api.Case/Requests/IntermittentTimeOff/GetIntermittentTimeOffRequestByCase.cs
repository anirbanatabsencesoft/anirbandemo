﻿using AbsenceSoft;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Models.IntermittentTimeoff;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Cases = AbsenceSoft.Data.Cases;
using AT.Api.Case.DataExtensions;

namespace AT.Api.Case.Requests.IntermittentTimeOff
{
    /// <summary>
    /// Api GetIntermittentTimeoff Request by case id
    /// </summary>
    public class GetIntermittentTimeOffRequestByCase : ApiRequest<GetIntermittentTimeOffRequestParameters, CollectionResultSet<IntermittentTimeOffModel>>
    {
        private readonly ICaseService _CaseService = null;
        /// <summary>
        /// Constructor
        /// </summary>        
        /// <param name="caseService"></param>
        public GetIntermittentTimeOffRequestByCase(ICaseService caseService = null)
        {
            _CaseService = caseService ?? new CaseService();
        }

        /// <summary>
        /// override abstract method to fulfillasync request
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override CollectionResultSet<IntermittentTimeOffModel> FulfillRequest(GetIntermittentTimeOffRequestParameters parameters)
        {
            Cases.Case existingCase = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);

            if (existingCase == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Case not found with the given details");
            }

            var requestDate = parameters.RequestDate;

            var segments = existingCase.Segments
                .Where(summary => summary.Type == CaseType.Intermittent &&
                    summary.Status != CaseStatus.Cancelled)
                .FirstOrDefault();

            if (segments == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Invalid request, requested date is not part of an intermittent portion of this case");
            }

            var intermittentTimeRequests = segments.UserRequests;

            if (intermittentTimeRequests == null)
            {
                return new CollectionResultSet<IntermittentTimeOffModel>(null, "No time off requests found");
            }

            var policySummaries = _CaseService.GetEmployeePolicySummaryByCaseId(existingCase.Id, null);
            List<IntermittentTimeOffModel> lsttimeoffs = new List<IntermittentTimeOffModel>();
            foreach (IntermittentTimeRequest request in intermittentTimeRequests)
            {
                var model = request.ToIntermittentTimeOffModel(policySummaries);
                model.RequestDate = request.RequestDate;
                lsttimeoffs.Add(model);
            }
            return new CollectionResultSet<IntermittentTimeOffModel>(lsttimeoffs, "Intermittent time off retrieved");
        }

        /// <summary>
        /// Validate input parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool IsValid(GetIntermittentTimeOffRequestParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrEmpty(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }

            return true;
        }
    }
}