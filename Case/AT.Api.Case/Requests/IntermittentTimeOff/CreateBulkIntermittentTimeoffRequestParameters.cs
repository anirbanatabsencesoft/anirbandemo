﻿using AT.Api.Case.Models.IntermittentTimeoff;
using System.Collections.Generic;

namespace AT.Api.Case.Requests.IntermittentTimeOff
{
    /// <summary>
    /// 
    /// </summary>
    public class CreateBulkIntermittentTimeoffRequestParameters
    {

        /// <summary>
        /// Case Number
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// Array of IntermittentTimeOffModel as a parameter to create bulk timeoff request
        /// </summary>
        public List<IntermittentTimeOffModel> intermittentTimeOffs { get; set; }
    }
}