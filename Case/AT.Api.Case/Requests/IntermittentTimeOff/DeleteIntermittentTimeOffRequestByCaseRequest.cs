﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Linq;
using System.Net;
using MongoDB.Driver.Linq;
using System;
using AbsenceSoft.Logic.Tasks;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic;
using AbsenceSoft.Data.Notes;
using MongoDB.Bson;
using AbsenceSoft.Data.Cases;
using MongoDB.Driver.Builders;
using System.Collections.Generic;

namespace AT.Api.Case.Requests.IntermittentTimeOff
{
    /// <summary>
    ///  Request to handle deletion of the ITOR request
    /// </summary>
    public class DeleteIntermittentTimeOffRequestByCaseRequest : ApiRequest<DeleteIntermittentTimeOffRequestParameters, ResultSet<string>>
    {
        private readonly ICaseService _CaseService = null;
        private readonly IAdminUserService _AdminUserService = null;
        private readonly User _User = null;

        /// <summary>
        /// Constructor
        /// </summary>        
        /// <param name="caseService"></param>
        /// <param name="customerId"></param>
        /// <param name="employerId"></param>
        /// <param name="userId"></param>
        /// <param name="adminUserService"></param>

        public DeleteIntermittentTimeOffRequestByCaseRequest(
            string userId,
            string employerId,
            string customerId,
            ICaseService caseService = null,
            IAdminUserService adminUserService = null)
        {
            _AdminUserService = adminUserService ?? new AdminUserService();

            _User = _AdminUserService.GetUserById(userId);

            if (_User == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Unable to find user");
            }

            _CaseService = caseService ?? new CaseService(customerId, employerId, _User);
        }

        protected override ResultSet<string> FulfillRequest(DeleteIntermittentTimeOffRequestParameters parameters)
        {
            var existingCase = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);
            
            if (existingCase == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Case not found with the given details");
            }

            var id = parameters.Id;        
           

            var segments = existingCase.Segments
                .Where(summary => summary.Type == CaseType.Intermittent &&
                    summary.Status != CaseStatus.Cancelled)
                .FirstOrDefault();

            if (segments == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Segment not available");
            }

            var interlist = segments.UserRequests.Where(u => u.Id == id).ToList();

            if (interlist == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Intermittent Time Requests not found for given details");
            }       

            // remove ITOR request for given request id            
             segments.UserRequests.RemoveAll(a => a.Id == id);           

            // update case activity record as removed request           
            _CaseService.UpdateTimeOfRequestNoteToRemovedRequest(existingCase, interlist);

            existingCase = _CaseService.RunCalcs(existingCase);          
            existingCase = _CaseService.UpdateCase(existingCase);

            return new ResultSet<string>(parameters.CaseNumber, "Intermittent Time Requests deleted successfully");
        }
        /// <summary>
        /// Validate input parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>

        protected override bool IsValid(DeleteIntermittentTimeOffRequestParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }
            if (parameters.Id == null || parameters.Id == Guid.Empty)
            {
                message = "Intermittent Id is required";
                return false;
            }

            return true;
        }
    }
}