﻿using AbsenceSoft.Common;
using System;

namespace AT.Api.Case.Requests.IntermittentTimeOff
{
    /// <summary>
    /// Get IntermittentTimeoff Request Parameters
    /// </summary>
    public class DeleteIntermittentTimeOffRequestParameters
    {
        /// <summary>
        /// Unique number for the case
        /// </summary>
        public Guid Id { get; set; }

        public string CaseNumber { get; set; }
    }
}