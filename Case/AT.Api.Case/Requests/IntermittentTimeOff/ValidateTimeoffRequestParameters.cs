﻿using AT.Api.Case.Models.IntermittentTimeoff;

namespace AT.Api.Case.Requests.IntermittentTimeOff
{
    /// <summary>
    /// 
    /// </summary>
    public class ValidateTimeoffRequestParameters
    {
        /// <summary>
        /// Case Number
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// List of intermittent timeoff requests
        /// </summary>
        public IntermittentTimeOffModel IntermittentTimeOff { get; set; }
    }
}