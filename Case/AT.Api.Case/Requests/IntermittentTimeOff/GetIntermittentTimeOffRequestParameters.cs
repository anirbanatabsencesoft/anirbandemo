﻿using System;

namespace AT.Api.Case.Requests.IntermittentTimeOff
{
    /// <summary>
    /// Get IntermittentTimeoff Request Parameters
    /// </summary>
    public class GetIntermittentTimeOffRequestParameters
    {
        /// <summary>
        /// Unique number for the case
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// Date of the request
        /// </summary>
        public DateTime RequestDate { get; set; }
    }
}