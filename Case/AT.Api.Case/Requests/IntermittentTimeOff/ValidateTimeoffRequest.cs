﻿using AbsenceSoft;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Models.IntermittentTimeoff;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AT.Api.Case.Requests.IntermittentTimeOff
{
    /// <summary>
    /// Validate Timeoff Request
    /// </summary>
    public class ValidateTimeoffRequest : ApiRequest<ValidateTimeoffRequestParameters, CollectionResultSet<ValidationMessageModel>>
    {
        private readonly ICaseService _caseService = null;
        /// <summary>
        /// Constructor
        /// </summary>        
        /// <param name="caseService"></param>
        public ValidateTimeoffRequest(ICaseService caseService = null)
        {
            _caseService = caseService ?? new CaseService();
        }

        /// <summary>
        /// fullfill validation Request
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override CollectionResultSet<ValidationMessageModel> FulfillRequest(ValidateTimeoffRequestParameters parameters)
        {
            var ValidationMessageModel = new List<ValidationMessageModel>();
            List<ValidationMessage> messages = PreValidateTimeOffRequest(parameters.CaseNumber, parameters.IntermittentTimeOff);
            if (messages != null && messages.Count > 0)
            {
                messages.ForEach(s =>
                {
                    ValidationMessageModel.Add(new Models.IntermittentTimeoff.ValidationMessageModel
                    {
                        Message = s.Message,
                        ValidationType = s.ValidationType.ToString()
                    });
                });
            }
            else
            {
                ValidationMessageModel.Add(new Models.IntermittentTimeoff.ValidationMessageModel
                {
                    Message = "Success",
                    ValidationType = ValidationType.Info.ToString()
                });
            }
            return (new CollectionResultSet<ValidationMessageModel>(ValidationMessageModel.ToArray(), ""));
        }


        /// <summary>
        /// Get validation messages from CaseService
        /// </summary>
        /// <param name="CaseNumber"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public List<ValidationMessage> PreValidateTimeOffRequest(string CaseNumber, IntermittentTimeOffModel viewModel)
        {
            List<ValidationMessage> messages = new List<ValidationMessage>();
            try
            {
                AbsenceSoft.Data.Cases.Case c = _caseService.GetCaseByCaseNumber(CaseNumber);
                if (c == null)
                {
                    messages.Add(new ValidationMessage() { ValidationType = ValidationType.Error, Message = "Case not found" });
                }

                if (!messages.Any())
                {
                    var seg = c.Segments
                        .Where(s => s.Type == CaseType.Intermittent && s.Status != CaseStatus.Cancelled && viewModel.RequestDate.DateInRange(s.StartDate, s.EndDate)).FirstOrDefault();
                    if (seg == null)
                    {
                        messages.Add(new ValidationMessage() { ValidationType = ValidationType.Error, Message = "Invalid request, requested date is not part of an intermittent portion of this case" });
                    }

                    if (!messages.Any())
                    {
                        IntermittentTimeRequest r = seg.UserRequests.FirstOrDefault(u => u.RequestDate.Date == viewModel.RequestDate.Date) ?? new IntermittentTimeRequest()
                        {
                            RequestDate = viewModel.RequestDate,
                        };
                        r.IntermittentType = viewModel.IntermittentType != null ? (IntermittentType)viewModel.IntermittentType : (IntermittentType?)null;
                        r.Notes = viewModel.Notes;
                        //r.EmployeeEntered = Data.Security.User.Current.Employers.First(e => e.EmployerId == c.EmployerId && e.IsActive).EmployeeId == c.Employee.Id;
                        viewModel.Details = viewModel.Details ?? new List<IntermittentTimeOffRequestDetailModel>(0);
                        // Uh oh, there are no details, shame on that UI, we'll just build them out for each of the policies 'n' stuff
                        if (viewModel.Details.Count == 0)
                        {
                            viewModel.Details = seg.AppliedPolicies.Where(p => p.Status == EligibilityStatus.Eligible).Select(p => new IntermittentTimeOffRequestDetailModel()
                            {
                                PolicyCode = p.Policy.Code,
                                ApprovedTime = viewModel.ApprovedTime,
                                PendingTime = viewModel.PendingTime,
                                DeniedTime = viewModel.DeniedTime,
                                //DenialReason = viewModel.DenialReason,
                                DenialReasonCode = viewModel.DenialReasonCode,
                                DenialReasonName = viewModel.DenialReasonName,
                                DenialReasonOther = viewModel.DenialReasonOther
                            }).ToList();
                        }

                        r.TotalMinutes = viewModel.Details.Any() ? viewModel.Details.Max(d => d.ApprovedTime + d.PendingTime + d.DeniedTime) : 0;

                        //need to handle the details for each policy seperately
                        int policyTotalMinutes = 0;
                        List<IntermittentTimeRequestDetail> list = new List<IntermittentTimeRequestDetail>();
                        foreach (IntermittentTimeOffRequestDetailModel vmDetail in viewModel.Details)
                        {
                            policyTotalMinutes = vmDetail.ApprovedTime + vmDetail.DeniedTime + vmDetail.PendingTime;
                            IntermittentTimeRequestDetail detail = new IntermittentTimeRequestDetail();
                            detail.PolicyCode = vmDetail.PolicyCode;
                            //HACK: TODO: Validate with Seth and Dave this is accurate here... Not sure.
                            // For pending, we know mistakes happen, automatically place any "unaccounted for" time in pending and give the
                            //  user the benefit of the doubt instead of throwing nasty error messages.
                            detail.Pending = (policyTotalMinutes - (vmDetail.ApprovedTime + vmDetail.DeniedTime));
                            detail.Approved = vmDetail.ApprovedTime;
                            detail.Denied = vmDetail.DeniedTime;
                            //detail.DeniedReason = vmDetail.DenialReason != null ? (AdjudicationDenialReason)vmDetail.DenialReason : (AdjudicationDenialReason?)null;
                            detail.DenialReasonCode = vmDetail.DenialReasonCode;
                            detail.DenialReasonName = vmDetail.DenialReasonName;
                            detail.DeniedReasonOther = vmDetail.DenialReasonOther;
                            list.Add(detail);
                        }
                        r.Detail = list;

                        // Check the overlapping time
                        if (r.RequestDate == viewModel.RequestDate)
                        {
                            if (Convert.ToDateTime(r.StartTimeForLeave) < Convert.ToDateTime(viewModel.EndTimeForLeave) && Convert.ToDateTime(viewModel.StartTimeForLeave) < Convert.ToDateTime(r.EndTimeForLeave))
                            {
                                messages.Add(new ValidationMessage() { ValidationType = ValidationType.Error, Message = $"Time of request overlaps existing time off request for request date :{viewModel.RequestDate}" });

                            }
                            if (viewModel.StartTimeForLeave == null)
                            {
                                messages.Add(new ValidationMessage() { ValidationType = ValidationType.Error, Message = $"Start Time is required for request date :{viewModel.RequestDate}" });

                            }
                            if (viewModel.EndTimeForLeave == null)
                            {
                                messages.Add(new ValidationMessage() { ValidationType = ValidationType.Error, Message = $"End Time is required for request date :{viewModel.RequestDate}" });
                            }
                        }
                               

                       using (var service = new CaseService())
                        {
                            try
                            {
                                messages = _caseService.PreValidateTimeOffRequest(c.Id, new List<IntermittentTimeRequest>(1) { r });
                            }
                            catch (Exception ex)
                            {
                                messages.Add(new ValidationMessage() { ValidationType = ValidationType.Error, Message = ex.Message });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                messages.Add(new ValidationMessage() { ValidationType = ValidationType.Error, Message = "Error Pre-Validating Time Off Request: " + ex.Message });
            }
            return messages;
        }

        /// <summary>
        /// Check validations
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool IsValid(ValidateTimeoffRequestParameters parameters, out string message)
        {
            message = string.Empty;
            if (string.IsNullOrEmpty(parameters.CaseNumber))
            {
                message = "Case number was not provided";
                return false;
            }
            if (parameters.IntermittentTimeOff.RequestDate == DateTime.MinValue)
            {
                message = "Requested date was not provided";
                return false;
            }
            if (parameters.IntermittentTimeOff.IntermittentType == null)
            {
                message = "Intermittent Type was not provided. Set value to 0 = OfficeVisit, 1 = Incapacity";
                return false;
            }
            return true;
        }
    }
}