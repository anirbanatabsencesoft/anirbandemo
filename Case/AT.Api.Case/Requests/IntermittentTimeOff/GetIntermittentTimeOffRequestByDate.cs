﻿using AbsenceSoft;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.DataExtensions;
using AT.Api.Case.Models.IntermittentTimeoff;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Linq;
using System.Net;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Requests.IntermittentTimeOff
{
    /// <summary>
    /// Gets the intermittent time off
    /// </summary>
    public class GetIntermittentTimeOffRequestByDate : ApiRequest<GetIntermittentTimeOffRequestParameters, ResultSet<IntermittentTimeOffModel>>
    {
        private readonly ICaseService _CaseService = null;

        /// <summary>
        /// Constructor
        /// </summary>        
        /// <param name="caseService">Instance for case service</param>
        public GetIntermittentTimeOffRequestByDate(ICaseService caseService = null)
        {
            _CaseService = caseService ?? new CaseService();
        }

        /// <summary>
        /// Override abstract method
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override ResultSet<IntermittentTimeOffModel> FulfillRequest(GetIntermittentTimeOffRequestParameters parameters)
        {
            Cases.Case existingCase = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);

            if (existingCase == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Case not found with the given details");
            }

            var requestDate = parameters.RequestDate;

            var segments = existingCase.Segments
                .Where(summary => summary.Type == CaseType.Intermittent &&
                    summary.Status != CaseStatus.Cancelled &&
                    requestDate.DateInRange(summary.StartDate, summary.EndDate))
                .FirstOrDefault();

            if (segments == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Invalid request, requested date is not part of an intermittent portion of this case");
            }

            var intermittentTimeRequest = segments.UserRequests.FirstOrDefault(u => u.RequestDate.Date == requestDate.Date);

            if (intermittentTimeRequest == null)
            {
                return new ResultSet<IntermittentTimeOffModel>(null, "No time off requests found");
            }

            var policySummaries = _CaseService.GetEmployeePolicySummaryByCaseId(existingCase.Id, null);
            var model = intermittentTimeRequest.ToIntermittentTimeOffModel(policySummaries);
            model.RequestDate = intermittentTimeRequest.RequestDate;

            return new ResultSet<IntermittentTimeOffModel>(model, "Intermittent time off retrieved");
        }

        /// <summary>
        /// Validate input parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool IsValid(GetIntermittentTimeOffRequestParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrEmpty(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }

            return true;
        }
    }
}