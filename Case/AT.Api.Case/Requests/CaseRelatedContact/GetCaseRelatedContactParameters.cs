﻿namespace AT.Api.Case.Requests.CaseRelatedContact
{
    /// <summary>
    /// Get Case releated person contact
    /// </summary>
    public class GetCaseRelatedContactParameters
    {
        /// <summary>
        /// Case Number
        /// </summary>
        public string CaseNumber { get; set; }
    }
}