﻿using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.DataExtensions;
using AT.Api.Case.Models.CaseRelatedContact;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Net;

namespace AT.Api.Case.Requests.CaseRelatedContact
{
    /// <summary>
    /// Request to get case related person contact
    /// </summary>
    public class GetCaseRelatedContactRequest : ApiRequest<GetCaseRelatedContactParameters, ResultSet<CaseRelatedContactModel>>
    {
        private readonly ICaseService _CaseService = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="caseService"></param>
        public GetCaseRelatedContactRequest(
            ICaseService caseService = null)
        {
            _CaseService = caseService ?? new CaseService();
        }

        /// <summary>
        /// Fulfill to get case related person contact
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override ResultSet<CaseRelatedContactModel> FulfillRequest(GetCaseRelatedContactParameters parameters)
        {
            var caseNumber = parameters.CaseNumber;
            var theCase = _CaseService.GetCaseByCaseNumber(caseNumber);

            if (theCase == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, $"Case was not found for {caseNumber}");
            }

            if (theCase.Contact == null)
            {
                return new ResultSet<CaseRelatedContactModel>(
                    new CaseRelatedContactModel(),
                    "No contact found for case",
                    true,
                    HttpStatusCode.NoContent);
            }

            return new ResultSet<CaseRelatedContactModel>(theCase.Contact.ToCaseRelatedContactModel(), "Case related contact details");
        }

        /// <summary>
        /// Validate the parameters to get the contact
        /// If parameters are not valid then set error message
        /// </summary>
        /// <param name="parameters"></param>        
        /// <param name="message"></param>        
        /// <returns></returns>
        protected override bool IsValid(GetCaseRelatedContactParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }

            return true;
        }
    }
}