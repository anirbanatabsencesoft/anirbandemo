﻿using AbsenceSoft;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Case.Models;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System;
using System.Net;

namespace AT.Api.Case.Requests.Case
{
    public class CreateCaseRequest : ApiRequest<CreateCaseParameters, ResultSet<string>>
    {

        private readonly ICaseService _CaseService = null;
        private readonly IEmployeeService _EmployeeService = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="adminUserService"></param>
        public CreateCaseRequest(EmployeeService EmployeeService = null, CaseService CaseService = null)
        {
            _CaseService = CaseService ?? new CaseService();
            _EmployeeService = EmployeeService ?? new EmployeeService();
        }

        protected override ResultSet<string> FulfillRequest(CreateCaseParameters parameters)
        {
            string result = null;
            try
            {
                result = CreateCaseRequestForCase(parameters.CaseModel, parameters.EmployerId.ToString());
            }

            catch (Exception ex)
            {
                throw new ApiException(HttpStatusCode.ExpectationFailed, ex.Message);
            }
            return (new ResultSet<string>(result, "Case creates successfully"));
        }

        protected override bool IsValid(CreateCaseParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.CaseModel.EmployeeNumber))
            {
                message = "Employee number is required";
                return false;
            }

            if (_EmployeeService.GetEmployeeByEmployeeNumber(parameters.CaseModel.EmployeeNumber, parameters.CustomerId, parameters.EmployerId) == null)
            {
                message = "Employee not found for the given employee number";
                return false;
            }

            if (!string.IsNullOrWhiteSpace(parameters.CaseModel.CaseNumber))
            {
                if (_CaseService.GetCaseByCaseNumber(parameters.CaseModel.CaseNumber) != null)
                {
                    message = "CaseNumber is already exist please try with new CaseNumber";
                    return false;
                }
            }

            if (parameters.CaseModel.CaseType == null)
            {
                message = "CaseType is required";
                return false;
            }

            if (parameters.CaseModel.StartDate == null)
            {
                message = "StartDate is required";
                return false;
            }

            if (parameters.CaseModel.EndDate == null)
            {
                message = "StartDate is required";
                return false;
            }

            if (parameters.CaseModel.EndDate.HasValue && parameters.CaseModel.EndDate < parameters.CaseModel.StartDate)
            {
                message = "The end date may not come before the start date";
                return false;
            }

            if (parameters.CaseModel.ReasonCode == null)
            {
                message = "ReasonCode is required";
                return false;
            }

            return true;

        }

        public string CreateCaseRequestForCase(CaseModel caseModel, string employeeId)
        {

            AbsenceSoft.Data.Cases.Case _case = null;
            _case = _CaseService.CreateCase(CaseStatus.Inquiry, employeeId, caseModel.StartDate.ToMidnight(), caseModel.EndDate, caseModel.CaseType, caseModel.ReasonCode, description: caseModel.ShortDescription, narrative: caseModel.Summary);
            _case = FillAllCaseData(_case, caseModel);
            _CaseService.UpdateCase(_case, CaseEventType.CaseCreated);
            return _case.CaseNumber;
        }
        private AbsenceSoft.Data.Cases.Case FillAllCaseData(AbsenceSoft.Data.Cases.Case _case, CaseModel caseModel)
        {
            if (!string.IsNullOrWhiteSpace(caseModel.CaseNumber))
            {
                _case.CaseNumber = caseModel.CaseNumber;
            }

            if (caseModel.CaseRelationship != null)
            {
                _case.Contact = new EmployeeContact()
                {
                    EmployeeNumber = _case.Employee.EmployeeNumber,
                    EmployerId = _case.EmployerId,
                    CustomerId = _case.CustomerId,
                    ContactTypeCode = caseModel.CaseRelationship.TypeCode,
                    Contact = new Contact()
                    {
                        FirstName = caseModel.CaseRelationship.FirstName,
                        LastName = caseModel.CaseRelationship.LastName,
                        DateOfBirth = caseModel.CaseRelationship.DateOfBirth
                    },
                    MilitaryStatus = (caseModel.CaseRelationship.MilitaryStatus)
                };
            }

            if (caseModel.CaseRelationship != null)
            {
                _case.CaseReporter = new EmployeeContact()
                {
                    EmployeeNumber = caseModel.CaseReporter.EmployeeNumber,
                    EmployerId = _case.EmployerId,
                    CustomerId = _case.CustomerId,
                    ContactTypeCode = caseModel.CaseReporter.ContactTypeCode,
                    Contact = new Contact()
                    {
                        CompanyName = caseModel.CaseReporter.CompanyName,
                        FirstName = caseModel.CaseReporter.FirstName,
                        LastName = caseModel.CaseReporter.LastName,
                        WorkPhone = caseModel.CaseReporter.WorkPhone
                    },
                };
            }

            if (caseModel.CaseFlags.IsWorkRelated.HasValue)
            {
                _case.Metadata.SetRawValue("IsWorkRelated", caseModel.CaseFlags.IsWorkRelated.Value);
            }
            if (caseModel.CaseFlags.WillUseBonding.HasValue)
            {
                _case.Metadata.SetRawValue("WillUseBonding", caseModel.CaseFlags.WillUseBonding.Value);
            }
            if (caseModel.CaseDates.BondingStartDate.HasValue)
            {
                _case.SetCaseEvent(CaseEventType.BondingStartDate, caseModel.CaseDates.BondingStartDate.Value);
            }
            if (caseModel.CaseDates.BondingEndDate.HasValue)
            {
                _case.SetCaseEvent(CaseEventType.BondingEndDate, caseModel.CaseDates.BondingEndDate.Value);
            }
            if (caseModel.CaseDates.AdoptionDate.HasValue)
            {
                _case.SetCaseEvent(CaseEventType.AdoptionDate, caseModel.CaseDates.AdoptionDate.Value);
            }
            if (caseModel.CaseFlags.MedicalComplications.HasValue)
            {
                _case.Disability.MedicalComplications = caseModel.CaseFlags.MedicalComplications.Value;
            }
            if (caseModel.CaseDates.ActualDeliveryDate.HasValue)
            {
                _case.SetCaseEvent(CaseEventType.DeliveryDate, caseModel.CaseDates.ActualDeliveryDate.Value);
            }
            if (caseModel.CaseDates.ExpectedDeliveryDate.HasValue)
            {
                _case.SetCaseEvent(CaseEventType.DeliveryDate, caseModel.CaseDates.ExpectedDeliveryDate.Value);
            }

            return _case;
        }
    }
}