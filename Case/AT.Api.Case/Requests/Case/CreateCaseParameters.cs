﻿using AT.Api.Case.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AT.Api.Case.Requests.Case
{
    public class CreateCaseParameters
    {
        public CaseModel CaseModel { get; set; }
        public string CustomerId { get; set; }
        public string EmployerId { get; set; }


    }
}