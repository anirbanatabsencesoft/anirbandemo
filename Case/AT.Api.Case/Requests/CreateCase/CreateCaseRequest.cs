﻿using AbsenceSoft;
using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Case.DataExtensions;
using AT.Api.Case.Models.EligibityModel;
using AT.Api.Core.Request;
using AT.Common.Core;
using System;
using cases = AbsenceSoft.Data.Cases;
namespace AT.Api.Case.Requests.Case
{
    public class CreateCaseRequest : ApiRequest<CreateCaseParameter, ResultSet<string>>
    {

        private readonly ICaseService _CaseService = null;
        private readonly IEmployeeService _EmployeeService = null;
        private readonly IAdminUserService _AdminUserService = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="EmployeeService"></param>
        /// <param name="CaseService"></param>
        public CreateCaseRequest(string userId, string EmployerId, string customerId, IEmployeeService EmployeeService = null, ICaseService CaseService = null, IAdminUserService AdminUserService = null)
        {
            _AdminUserService = AdminUserService ?? new AdminUserService();
            var user = _AdminUserService.GetUserById(userId);
            _CaseService = CaseService ?? new CaseService(customerId, EmployerId, user);
            _EmployeeService = EmployeeService ?? new EmployeeService();
           
        }

        protected override ResultSet<string> FulfillRequest(CreateCaseParameter parameters)
        {

            var result = CreateCaseRequestforNewCase(parameters);
            return (new ResultSet<string>(result, "Case created successfully"));
        }

        protected override bool IsValid(CreateCaseParameter parameters, out string message)
        {
            message = string.Empty;

            if (parameters == null)
            {
                message = "Case Parameter is null";
                return false;
            }
            if (parameters.CaseModel == null)
            {
                message = "Case Model is null";
                return false;
            }
            if (string.IsNullOrEmpty(parameters.CaseModel.EmployeeNumber))
            {
                message = "Employee number is required";
                return false;
            } 
            if (_EmployeeService.GetEmployeeByEmployeeNumber(parameters.CaseModel.EmployeeNumber, parameters.CustomerId, parameters.EmployerId) == null)
            {
                message = "Employee not found for the given employee number";
                return false;
            }

            if (!string.IsNullOrWhiteSpace(parameters.CaseModel.CaseNumber) && _CaseService.GetCaseByCaseNumber(parameters.CaseModel.CaseNumber) != null)
            {
                message = "Case with the given case number already exists";
                return false;
            }                    

            if (parameters.CaseModel.CaseType!=CaseType.Administrative && parameters.CaseModel.EndDate==null)
            {
                message = "EndDate is required";
                return false;
            }
            return true;
        }

        public string CreateCaseRequestforNewCase(CreateCaseParameter parameters)
        {
            var newCase = this.ToCreateNewCase(parameters);
            newCase = CaseModelsExtesnsion.ToCase(newCase, parameters.CaseModel);
            _CaseService.UpdateCase(newCase, CaseEventType.CaseCreated);
            return newCase.CaseNumber;
        }

        private cases.Case ToCreateNewCase(CreateCaseParameter parameters)
        {
            var employee = _EmployeeService.GetEmployeeByEmployeeNumber(parameters.CaseModel.EmployeeNumber, parameters.CustomerId, parameters.EmployerId);
            cases.Case newCase = null;
            var model = new CaseEligibiliyModel
            {
                CaseTypeId = (int)(Enum.Parse(typeof(CaseType), parameters.CaseModel.CaseType.ToString())),
                StartDate = parameters.CaseModel.StartDate,
                EndDate = parameters.CaseModel.EndDate,
                AbsenceReasonCode = parameters.CaseModel.ReasonCode,
            };
            if (model is CaseEligibiliyModel)
            {
                var createCaseModel = (CaseEligibiliyModel)model;
                var caseStatus = CaseStatus.Open;

                string absenceReasonCode = string.Empty;
                if (caseStatus != CaseStatus.Inquiry)
                {
                    absenceReasonCode = model.AbsenceReasonCode;
                }
                newCase = _CaseService.CreateCase(caseStatus, employee.Id, createCaseModel.StartDate.Value, createCaseModel.EndDate, (CaseType)createCaseModel.CaseTypeId.Value, absenceReasonCode, description: parameters.CaseModel.ShortDescription, narrative: parameters.CaseModel.Summary);


                EmployeeContact contact = null;
                if (!String.IsNullOrWhiteSpace(createCaseModel.EmployeeRelationshipCode))
                {
                    newCase.Contact = new EmployeeContact()
                    {
                        EmployeeId = newCase.Employee.Id,
                        EmployeeNumber = newCase.Employee.EmployeeNumber,
                        EmployerId = newCase.EmployerId,
                        CustomerId = newCase.CustomerId,
                        ContactTypeCode = createCaseModel.EmployeeRelationshipCode,
                        ContactTypeName = ContactType.GetByCode(createCaseModel.EmployeeRelationshipCode).Name,
                        MilitaryStatus = (MilitaryStatus)(createCaseModel.MilitaryStatusId ?? 0)
                    };
                    contact = newCase.Contact;
                }
                if (createCaseModel.IsWorkRelated.HasValue)
                    newCase.Metadata.SetRawValue("IsWorkRelated", createCaseModel.IsWorkRelated.Value);

                if (newCase.Reason != null)
                {
                    switch (newCase.Reason.Code)
                    {
                        case "EHC":
                            break;
                        case "PREGMAT":
                        case "BONDING":
                            if (createCaseModel.ExpectedDeliveryDate.HasValue)
                                newCase.SetCaseEvent(CaseEventType.DeliveryDate, createCaseModel.ExpectedDeliveryDate.Value);
                            if (createCaseModel.ActualDeliveryDate.HasValue)
                                newCase.SetCaseEvent(CaseEventType.DeliveryDate, createCaseModel.ActualDeliveryDate.Value);
                            if (createCaseModel.WillUseBonding.HasValue)
                            {
                                newCase.Metadata.SetRawValue("WillUseBonding", createCaseModel.WillUseBonding.Value);
                                if (createCaseModel.WillUseBonding.Value)
                                {
                                    if (createCaseModel.BondingStartDate.HasValue)
                                        newCase.SetCaseEvent(CaseEventType.BondingStartDate, createCaseModel.BondingStartDate.Value);
                                    if (createCaseModel.BondingEndDate.HasValue)
                                        newCase.SetCaseEvent(CaseEventType.BondingEndDate, createCaseModel.BondingEndDate.Value);
                                }
                            }
                            break;
                        case "ADOPT":
                            if (createCaseModel.AdoptionDate.HasValue)
                                newCase.SetCaseEvent(CaseEventType.AdoptionDate, createCaseModel.AdoptionDate.Value);
                            break;
                        case "MILITARY":
                        case "EXIGENCY":
                        case "CEREMONY":
                        case "RESERVETRAIN":
                            if (contact != null)
                            {
                                contact.MilitaryStatus = createCaseModel.MilitaryStatusId.HasValue ? (MilitaryStatus)createCaseModel.MilitaryStatusId.Value : MilitaryStatus.Civilian;
                            }
                            else if (createCaseModel.MilitaryStatusId.HasValue)
                            {
                                newCase.Employee.MilitaryStatus = (MilitaryStatus)createCaseModel.MilitaryStatusId.Value;
                            }
                            break;
                    }
                }
            }

            return newCase;
        } 
    }
}