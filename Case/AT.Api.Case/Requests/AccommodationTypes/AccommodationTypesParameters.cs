﻿using AbsenceSoft.Data.Enums;

namespace AT.Api.Case.Requests.AccommodationTypes
{
    public class AccommodationTypesParameters
    {
        public CaseType? CaseType { get; set; }
        public string CustomerId { get; set; }
        public string EmployerId { get; set; }
    }
}