﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AT.Api.Case.Requests.AccommodationTypes
{
    public class AccommodationTypesRequest : ApiRequest<AccommodationTypesParameters, CollectionResultSet<AccommodationType>>
    {
        private readonly IEmployeeService _EmployeeService = null;
        private readonly IAccommodationService _AccommodationService = null;
        private readonly IAdminUserService _AdminUserService = null;
        private readonly IEmployerService _IEmployerService = null;


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="employerId"></param>
        /// <param name="customerId"></param>
        /// <param name="AdminUserService"></param>
        /// <param name="AccommodationService"></param>
        /// <param name="IEmployerService"></param>
        public AccommodationTypesRequest(string userId, string employerId, string customerId, IAdminUserService AdminUserService = null, IAccommodationService AccommodationService = null,IEmployerService IEmployerService=null)
        {
            _AdminUserService = AdminUserService ?? new AdminUserService();
            _IEmployerService = IEmployerService ?? new EmployerService();
            var user = _AdminUserService.GetUserById(userId);
            var employer = _IEmployerService.GetById(employerId);
            var customer = _AdminUserService.GetCustomerById(customerId);         
            if (user == null || customer == null || employer == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Unable to find user");
            }
            _AccommodationService = AccommodationService ?? new AccommodationService(customer, employer, user);
        }
        protected override bool IsValid(AccommodationTypesParameters parameters, out string message)
        {
            message = string.Empty;             
            return true;
        }

        /// <summary>
        /// FulfillAsync method 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override CollectionResultSet<AccommodationType> FulfillRequest(AccommodationTypesParameters parameters)
        {
            var filteredAccommodationType = new List<AccommodationType>();
            filteredAccommodationType.AddRange(_AccommodationService.GetAccommodationTypes().Where(a => !a.PreventNew));
            return new CollectionResultSet<AccommodationType>(filteredAccommodationType);
        }
    }
}