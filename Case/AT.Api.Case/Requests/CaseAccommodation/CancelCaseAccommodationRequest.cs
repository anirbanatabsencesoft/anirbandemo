﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System;
using System.Net;

namespace AT.Api.Case.Requests.CaseAccommodation
{
    /// <summary>
    /// Handles request to cancel accommodation of accommodation request
    /// </summary>
    public class CancelCaseAccommodationRequest : ApiRequest<CancelCaseAccommodationParameters, ResultSetBase>
    {
        private readonly ICaseService _CaseService = null;
        private readonly IAccommodationService _AccommodationService = null;

        public CancelCaseAccommodationRequest(ICaseService caseService = null, IAccommodationService accommodationService = null)
        {
            _CaseService = caseService ?? new CaseService();
            _AccommodationService = accommodationService ?? new AccommodationService();
        }

        protected override ResultSetBase FulfillRequest(CancelCaseAccommodationParameters parameters)
        {
            var result = CancelCaseAccommodation(parameters);

            if (result == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Unable to cancel case accommodation");
            }

            return result;
        }

        private ResultSetBase CancelCaseAccommodation(CancelCaseAccommodationParameters parameters)
        {
            var caseData = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);

            if (caseData == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Case not found with the given details");
            }

            if (caseData.AccommodationRequest == null)
            {
                return new ResultSet<string>(
                    null,
                    "Accommodation details not found with the given case",
                    true,
                    HttpStatusCode.NoContent);
            }

            if (caseData.AccommodationRequest.Id == null)
            {
                return new ResultSet<string>(
                    null,
                    "Accommodation request Id not found with the given case",
                    true,
                    HttpStatusCode.NoContent);
            }

            if (caseData.AccommodationRequest.Accommodations.Count == 0)
            {
                return new ResultSet<string>(
                    null,
                    "Accommodations not found with the given case",
                    true,
                    HttpStatusCode.NoContent);
            }
            
            var cancelReason = AccommodationCancelReason.RequestWithdrawn;
            if (string.IsNullOrWhiteSpace(parameters.Model.CancelReason.ToString()))
            {
                Enum.TryParse(parameters.Model.CancelReason.ToString(), true, out cancelReason);
            }
            
            _AccommodationService.CancelAccommodationRequest(caseData, parameters.AccommodationId, cancelReason, parameters.Model.OtherCancelDescription);
                        
            return new ResultSetBase("Case accommodation canceled successfully");
        }

        protected override bool IsValid(CancelCaseAccommodationParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.AccommodationId))
            {
                message = "Accommodation id is required";
                return false;
            }
            
            return true;
        }
    }
}