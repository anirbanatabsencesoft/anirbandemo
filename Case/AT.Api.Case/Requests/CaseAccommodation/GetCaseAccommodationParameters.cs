﻿namespace AT.Api.Case.Requests.CaseAccommodation
{
    public class GetCaseAccommodationParameters
    {
        public string CaseNumber { get; set; }
    }
}