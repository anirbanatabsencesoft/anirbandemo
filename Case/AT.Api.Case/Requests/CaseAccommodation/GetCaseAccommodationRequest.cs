﻿using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Models.CaseAccommodation;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Net;

namespace AT.Api.Case.Requests.CaseAccommodation
{
    /// <summary>
    /// Handles request to get case accommodation data
    /// </summary>
    public class GetCaseAccommodationRequest : ApiRequest<GetCaseAccommodationParameters, ResultSet<CaseAccommodationModel>>
    {
        private readonly ICaseService _CaseService = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="caseNumber">Unique number for the case</param>
        /// <param name="caseService">Instance of case service</param>
        public GetCaseAccommodationRequest(string caseNumber, ICaseService caseService = null)
        {
            _CaseService = caseService ?? new CaseService();
        }

        protected override ResultSet<CaseAccommodationModel> FulfillRequest(GetCaseAccommodationParameters parameters)
        {
            var caseData = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);

            if (caseData == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Case not found with the given details");
            }

            if (caseData.AccommodationRequest == null)
            {
                return new ResultSet<CaseAccommodationModel>(
                    null,
                    "Accommodation details not found with the given case",
                    true,
                    HttpStatusCode.NoContent);
            }

            var result = DataExtensions.CaseExtensions.ToCaseAccommodationModel(caseData.AccommodationRequest);

            return new ResultSet<CaseAccommodationModel>(result, "Case accommodation details retrieved successfully");
        }

        protected override bool IsValid(GetCaseAccommodationParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }

            return true;
        }
    }
}