﻿namespace AT.Api.Case.Requests.CaseAccommodation
{
    /// <summary>
    /// Get accommodation interactive process parameters
    /// </summary>
    public class GetInteractiveProcessParameters
    {
        /// <summary>
        /// Case number
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// Accommodation id
        /// </summary>
        public string AccommodationId { get; set; }

    }
}