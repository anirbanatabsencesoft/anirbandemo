﻿using AbsenceSoft;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Models.CaseAccommodation;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using ATData = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Requests.CaseAccommodation
{
    /// <summary>
    /// Handles request to create accommodation of accommodation request
    /// </summary>
    public class CreateCaseAccommodationRequest : ApiRequest<CreateCaseAccommodationParameters, ResultSetBase>
    {
        private readonly ICaseService _CaseService = null;
        private readonly IAccommodationService _AccommodationService = null;

        public CreateCaseAccommodationRequest(ICaseService caseService = null, IAccommodationService accommodationService = null)
        {
            _CaseService = caseService ?? new CaseService();
            _AccommodationService = accommodationService ?? new AccommodationService();
        }
                
        protected override ResultSetBase FulfillRequest(CreateCaseAccommodationParameters parameters)
        {            
            // Get case from case number and check whether accommodation request is there or not
            var caseData = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);

            if (caseData == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Case not found with the given case number");
            }

            // Validate accommodation type
            // Get all accommodation types
            var accommodationTypeList = _AccommodationService.GetBaseAccommodationTypes();
            
            if (parameters.Model.Accommodations.Count() > 0)
            {
                if (accommodationTypeList != null && accommodationTypeList.Count() > 0)
                {
                    foreach (AccommodationModel item in parameters.Model.Accommodations)
                    {
                        if (!accommodationTypeList.Any(a => a.Code == item.TypeCode && a.Name == item.TypeName))
                        {
                            throw new ApiException(HttpStatusCode.BadRequest, "Accommodation type not found with the given detail");
                        }
                    }
                }
            }

            List<AccommodationModel> accommList = new List<AccommodationModel>();

            // Check caseData object for accommodation related data
            if (caseData.AccommodationRequest != null && caseData.AccommodationRequest.Accommodations != null)
            {
                foreach (Accommodation item in caseData.AccommodationRequest.Accommodations)
                {
                    if (parameters.Model.Accommodations.Count == 0)
                    {
                        item.Status = parameters.Model.Status;
                        item.CancelReason = parameters.Model.CancelReason.HasValue ? parameters.Model.CancelReason.Value : (AccommodationCancelReason?)null; ;
                        if (parameters.Model.CancelReason == AccommodationCancelReason.Other)
                            item.OtherReasonDesc = parameters.Model.OtherReasonDescription;
                        else
                            item.OtherReasonDesc = string.Empty;
                    }
                    accommList.Add(DataExtensions.CaseExtensions.ToAccommodationModel(item));
                }                
            }
                        
            AccommodationRequestModel theRequest = caseData.AccommodationRequest == null 
                ? new AccommodationRequestModel() : DataExtensions.CaseExtensions.ToAccommodationRequestModel(caseData.AccommodationRequest);

            theRequest.GeneralHealthCondition = parameters.Model.GeneralHealthCondition;
            theRequest.Description = parameters.Model.Description;

            // If AccommodationRequest.Status is "Cancelled", but all "Accommodations" under this request is open then change AccommodationRequest Status to open
            if (parameters.Model.Accommodations.Count > 0)
            {
                if (!parameters.Model.Accommodations.Any(m => m.Status == CaseStatus.Cancelled) && parameters.Model.Status == CaseStatus.Cancelled)
                {
                    theRequest.Status = CaseStatus.Open;
                    theRequest.CancelReason = (AccommodationCancelReason?)null;
                    theRequest.OtherReasonDescription = string.Empty;
                }
                else
                {
                    theRequest.Status = parameters.Model.Status;
                    theRequest.CancelReason = parameters.Model.CancelReason.HasValue ? parameters.Model.CancelReason : (AccommodationCancelReason?)null;
                    if (parameters.Model.CancelReason.HasValue && parameters.Model.CancelReason.Value == AccommodationCancelReason.Other)
                        theRequest.OtherReasonDescription = parameters.Model.OtherReasonDescription;
                    else
                        theRequest.OtherReasonDescription = string.Empty;
                }
            }
            else
            {
                if (!accommList.Any(m => m.Status == CaseStatus.Cancelled) && parameters.Model.Status == CaseStatus.Cancelled)
                {
                    theRequest.Status = CaseStatus.Open;
                    theRequest.CancelReason = (AccommodationCancelReason?)null;
                    theRequest.OtherReasonDescription = string.Empty;
                }
                else
                {
                    theRequest.Status = parameters.Model.Status;
                    theRequest.CancelReason = parameters.Model.CancelReason.HasValue ? parameters.Model.CancelReason : (AccommodationCancelReason?)null;
                    if (parameters.Model.CancelReason.HasValue && parameters.Model.CancelReason.Value == AccommodationCancelReason.Other)
                        theRequest.OtherReasonDescription = parameters.Model.OtherReasonDescription;
                    else
                        theRequest.OtherReasonDescription = string.Empty;
                }
            }

            List<AccommodationModel> added = new List<AccommodationModel>();
            List<AccommodationModel> changed = new List<AccommodationModel>();
            
            List<AccommodationModel> newAccommList = new List<AccommodationModel>();
            AccommodationModel accomm = null;
            bool isNew = false;
                                    
            foreach (AccommodationModel item in parameters.Model.Accommodations)
            {
                if (!string.IsNullOrEmpty(item.AccommodationId) && caseData.AccommodationRequest.Accommodations.Any(x => x.Id.ToString().Equals(item.AccommodationId)))
                {
                    // Check for duplicate data
                    var existingAccommodation = caseData.AccommodationRequest.Accommodations.FirstOrDefault(x => x.Type.Name.Equals(item.TypeName) && 
                    x.StartDate >= item.StartDate && x.EndDate <= item.EndDate && x.Id.ToString() != item.AccommodationId);

                    if (existingAccommodation != null)
                    {                        
                        throw new ApiException(HttpStatusCode.BadRequest, "Accommodation Request with the same date range is already existing");
                    }

                    accomm = DataExtensions.CaseExtensions.ToAccommodationModel(caseData.AccommodationRequest.Accommodations.First(x => x.Id.ToString().Equals(item.AccommodationId)));

                    if (accomm != null)
                        accommList.Remove(changed.AddFluid(accomm));
                    isNew = false;
                }
                else
                {
                    if (string.IsNullOrEmpty(item.AccommodationId) && caseData.AccommodationRequest != null && caseData.AccommodationRequest.Accommodations != null)
                    {                        
                        // Check if the new request is for temp duration, since it will have both start & end date
                        if (item.Duration == AccommodationDuration.Temporary)
                        {
                            if (caseData.AccommodationRequest.Accommodations.Any(x => x.Type.Name.Equals(item.TypeName) && (item.StartDate.Value.DateInRange(x.StartDate.Value, x.EndDate) || item.EndDate.Value.DateInRange(x.StartDate.Value, x.EndDate))))
                            {                                
                                throw new ApiException(HttpStatusCode.BadRequest, "An accommodation request with the same date range already exists");
                            }
                        }
                        else // Permanent duration
                        {
                            if (caseData.AccommodationRequest.Accommodations.Any(x => x.Type.Name.Equals(item.TypeName) && item.StartDate.Value.DateInRange(x.StartDate.Value, x.EndDate)))
                            {
                                throw new ApiException(HttpStatusCode.BadRequest, "An accommodation request with the same date range already exists");
                            }
                            else if (caseData.AccommodationRequest.Accommodations.Any(x => x.Type.Name.Equals(item.TypeName) && (item.StartDate <= x.StartDate || x.EndDate >= item.StartDate))) // Handling scenario where the new start date is earlier than current records. 
                            {
                                throw new ApiException(HttpStatusCode.BadRequest, "An accommodation request with the same date range already exists");
                            }
                        }
                    }

                    accomm = added.AddFluid(new AccommodationModel());
                    isNew = true;
                }

                accomm.StartDate = item.StartDate;
                accomm.EndDate = item.EndDate;
                accomm.TypeCode = item.TypeCode;
                accomm.TypeName = item.TypeName;
                accomm.Duration = item.Duration;
                accomm.Description = item.TypeCode == "OTR" ? item.Description : string.Empty;                
                accomm.IsWorkRelated = item.IsWorkRelated;
                accomm.IsResolved = item.IsResolved;                
                accomm.IsGranted = item.IsGranted;                                
                accomm.Status = item.Status;
                accomm.CancelReason = item.CancelReason.HasValue ? item.CancelReason.Value : (AccommodationCancelReason?)null;
                if (item.CancelReason == AccommodationCancelReason.Other)
                    accomm.OtherCancelReasonDescription = item.OtherCancelReasonDescription;
                else
                    accomm.OtherCancelReasonDescription = string.Empty;

                accommList.Add(accomm);
                if (isNew)
                {
                    newAccommList.Add(accomm);
                    caseData.SetCaseEvent(CaseEventType.AccommodationAdded, DateTime.UtcNow);
                }
            }
                                    
            var accommodationRequest = _AccommodationService.CreateOrModifyAccommodationRequest(caseData, theRequest.GeneralHealthCondition, ToAccommodationEntity(newAccommList, caseData));
            theRequest.Accommodations = accommList;

            // If this is an accomm only case update the case dates to match the request
            if (!caseData.Segments.SelectMany(s => s.AppliedPolicies).Any() && caseData.Segments.All(s => s.Type == CaseType.Administrative))
            {
                caseData.StartDate = accommodationRequest.StartDate ?? caseData.StartDate;
                caseData.EndDate = accommodationRequest.EndDate;
                var seg = caseData.Segments.FirstOrDefault();
                if (seg != null)
                {
                    seg.Status = caseData.Status;
                    seg.StartDate = caseData.StartDate;
                    seg.EndDate = caseData.EndDate;
                    if (caseData.Segments.Count > 1)
                        caseData.Segments.RemoveRange(1, caseData.Segments.Count - 1);
                }
            }
            caseData.AccommodationRequest = ToAccommodationRequestEntity(theRequest, caseData);
            var result = _CaseService.UpdateCase(caseData);

            return new ResultSetBase("Case accommodation created successfully");
        }
        
        protected override bool IsValid(CreateCaseAccommodationParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }

            if (parameters.Model == null)
            {
                message = "Accommodation detail is required";
                return false;
            }

            return true;
        }

        private List<ATData.Accommodation> ToAccommodationEntity(List<AccommodationModel> parameters, ATData.Case caseData)
        {
            var resultAccommodations = new List<Accommodation>();

            if (parameters != null)
            {
                foreach (AccommodationModel item in parameters)
                {
                    List<AccommodationUsage> usage = new List<AccommodationUsage>();
                    AccommodationUsage accommodationUsage = new AccommodationUsage();
                    if (item.Duration == AccommodationDuration.Permanent)
                    {
                        accommodationUsage.StartDate = item.StartDate.Value;
                        accommodationUsage.EndDate = null;
                    }

                    if (item.Duration == AccommodationDuration.Temporary)
                    {
                        accommodationUsage.StartDate = item.StartDate ?? caseData.StartDate;
                        accommodationUsage.EndDate = item.EndDate;
                    }

                    usage.Add(accommodationUsage);
                    resultAccommodations.Add(
                    new Accommodation
                    {
                        Type = new AccommodationType { Code = item.TypeCode, Name = item.TypeName },
                        Duration = item.Duration,
                        Description = item.Description,
                        Resolved = item.IsResolved,
                        Granted = item.IsGranted,
                        StartDate = item.StartDate,
                        EndDate = item.EndDate,
                        Determination = item.Determination,
                        MinApprovedFromDate = item.MinimumApprovedFromDate,
                        MaxApprovedThruDate = item.MaximumApprovedThruDate,
                        Status = item.Status,
                        IsWorkRelated = item.IsWorkRelated,
                        Usage = usage,
                        InteractiveProcess = new AccommodationInteractiveProcess()
                    });
                }
            }
            return resultAccommodations;
        }

        private ATData.Case ToAccommodationRequestEntity(CreateCaseAccommodationParameters parameters, ATData.Case caseData)
        {
            var resultCaseData = caseData;

            if (parameters != null)
            {
                resultCaseData.AccommodationRequest = new AccommodationRequest
                {
                    CaseNumber = parameters.CaseNumber,
                    GeneralHealthCondition = parameters.Model.GeneralHealthCondition,
                    Description = parameters.Model.Description,
                    Status = parameters.Model.Status,
                    Accommodations = new List<Accommodation>(ToAccommodationEntity(parameters.Model.Accommodations, caseData)),
                    StartDate = parameters.Model.StartDate,
                    EndDate = parameters.Model.EndDate,
                    Determination = parameters.Model.Determination,
                    CancelReason = parameters.Model.CancelReason,
                    OtherReasonDesc = parameters.Model.OtherReasonDescription,
                    AdditionalInfo = new AccommodationInteractiveProcess()
                };
                resultCaseData.IsAccommodation = true;
            }

            return resultCaseData;
        }

        private ATData.AccommodationRequest ToAccommodationRequestEntity(AccommodationRequestModel model, ATData.Case caseData)
        {
            var resultAccommodationRequest = new AccommodationRequest();

            if (model != null)
            {
                resultAccommodationRequest = new AccommodationRequest
                {
                    CaseNumber = model.CaseNumber,
                    GeneralHealthCondition = model.GeneralHealthCondition,
                    Description = model.Description,
                    Status = model.Status,
                    Accommodations = new List<Accommodation>(ToAccommodationEntity(model.Accommodations, caseData)),
                    StartDate = model.StartDate,
                    EndDate = model.EndDate,
                    Determination = model.Determination,
                    CancelReason = model.CancelReason,
                    OtherReasonDesc = model.OtherReasonDescription,
                    AdditionalInfo = new AccommodationInteractiveProcess()
                };
            }

            return resultAccommodationRequest;
        }
    }
}