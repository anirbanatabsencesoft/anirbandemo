﻿using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Net;
using IAccommodationService = AbsenceSoft.Logic.Cases.Contracts.IAccommodationService;

namespace AT.Api.Case.Requests.CaseAccommodation
{
    /// <summary>
    /// Handles request to close accommodation request
    /// </summary>
    public class CloseCaseAccommodationRequest : ApiRequest<CloseCaseAccommodationParameters, ResultSetBase>
    {
        private readonly ICaseService _CaseService = null;
        private readonly IAccommodationService _AccommodationService = null;

        public CloseCaseAccommodationRequest(ICaseService caseService = null, IAccommodationService accommodationService = null)
        {
            _CaseService = caseService ?? new CaseService();
            _AccommodationService = accommodationService ?? new AccommodationService();
        }
        
        protected override ResultSetBase FulfillRequest(CloseCaseAccommodationParameters parameters)
        {
            var caseData = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);

            if (caseData == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Case not found with the given case number");
            }

            if (caseData.AccommodationRequest == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Accommodation details not found with the given case");
            }

            if (caseData.AccommodationRequest.Id == null)
            {
                return new ResultSet<string>(
                    null,
                    "Accommodation request Id not found with the given case",
                    true,
                    HttpStatusCode.NoContent);
            }

            _AccommodationService.CloseCaseAccommodation(caseData, caseData.AccommodationRequest.Id.ToString(), parameters.AccommodationId);

            return new ResultSetBase("Case accommodation closed successfully");
        }

        protected override bool IsValid(CloseCaseAccommodationParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }
            
            if (string.IsNullOrWhiteSpace(parameters.AccommodationId))
            {
                message = "Case accommodation id is required";
                return false;
            }

            return true;
        }
    }
}