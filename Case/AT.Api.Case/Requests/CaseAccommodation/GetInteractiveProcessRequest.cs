﻿using AbsenceSoft;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.DataExtensions;
using AT.Api.Case.Models.CaseAccommodation;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AT.Api.Case.Requests.CaseAccommodation
{
    /// <summary>
    /// Request to get accommodation interactive process
    /// </summary>
    public class GetInteractiveProcessRequest : ApiRequest<GetInteractiveProcessParameters, CollectionResultSet<GetInteractiveProcessModel>>
    {
        private readonly ICaseService _CaseService = null;
        private readonly IAccommodationService _AccommodationService = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="caseService"></param>
        /// <param name="accommodationService"></param>
        public GetInteractiveProcessRequest(ICaseService caseService = null, IAccommodationService accommodationService = null)
        {
            _CaseService = caseService ?? new CaseService();
            _AccommodationService = accommodationService ?? new AccommodationService();
        }

        /// <summary>
        /// Get interactive process for accommodation
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override CollectionResultSet<GetInteractiveProcessModel> FulfillRequest(GetInteractiveProcessParameters parameters)
        {
            var caseNumber = parameters.CaseNumber;
            var theCase = _CaseService.GetCaseByCaseNumber(caseNumber);

            if (theCase == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, $"Case was not found for {caseNumber}");
            }
          
            var accomodationId = new Guid(parameters.AccommodationId);

            if (theCase.AccommodationRequest == null)
            {
                return new CollectionResultSet<GetInteractiveProcessModel>(
                   new List<GetInteractiveProcessModel>(),
                   "No accommodation request available",
                   true,
                   HttpStatusCode.NoContent);
            }

            var accommodation = theCase.AccommodationRequest.Accommodations.Where(a => a.Id == accomodationId).FirstOrDefault();
            if (accommodation == null)
            {
                return new CollectionResultSet<GetInteractiveProcessModel>(
                   new List<GetInteractiveProcessModel>(),
                   "No accommodation found",
                   true,
                   HttpStatusCode.NoContent);
            }
            
            if (accommodation.InteractiveProcess == null || accommodation.InteractiveProcess.Steps == null || !accommodation.InteractiveProcess.Steps.Any())
            {
                return new CollectionResultSet<GetInteractiveProcessModel>(
                   new List<GetInteractiveProcessModel>(),
                   "No accommodation interactive process found",
                   true,
                   HttpStatusCode.NoContent);
            }

            var interactiveProcess = accommodation.InteractiveProcess.Steps.Select(i => i.ToInteractiveProcessStep()).ToList();

            interactiveProcess = GetIntProceNotes(interactiveProcess, accomodationId, theCase.Id);

            return new CollectionResultSet<GetInteractiveProcessModel>(interactiveProcess);
        }

        private List<GetInteractiveProcessModel> GetIntProceNotes(List<GetInteractiveProcessModel> intProcesses, Guid accommodationId, string caseId)
        {
            var caseNotes = _AccommodationService.GetAccommIntProcCaseNotes(accommodationId, caseId)
                .Select(note => new
                {
                    Id = note.Metadata.GetRawValue<string>("AccommQuestionId"),
                    Contact = note.Metadata.GetRawValue<string>("Contact"),
                    Answer = note.Metadata.GetRawValue<bool?>("Answer"),
                    SAnswer = note.Metadata.GetRawValue<string>("SAnswer"),
                    note.CreatedDate,
                    note.CreatedBy,
                    note.ModifiedDate,
                    note.Category,
                    note.Notes
                });

            intProcesses.ForEach(process =>
            {
                process.Notes = caseNotes.Where(note => note.Id == process.QuestionId)
                .Select(pnote => new InteractiveProcessCaseNoteModel()
                {
                    CreateDate =  pnote.CreatedDate,
                    CreatedBy = pnote.CreatedBy.Email,
                    ModifiedDate = pnote.ModifiedDate,
                    Note = pnote.Notes,
                    Category = pnote.Category,                    
                    Answer = !string.IsNullOrEmpty(pnote.SAnswer)
                            ? pnote.SAnswer
                            : pnote.Answer.ToString(),
                })
                .ToList();
            });

            return intProcesses;
        }

        /// <summary>
        /// Validate  interactive process input parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool IsValid(GetInteractiveProcessParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }
            if (string.IsNullOrWhiteSpace(parameters.AccommodationId))
            {
                message = "Accommodation id is required";
                return false;
            }
            return true;
        }
    }
}