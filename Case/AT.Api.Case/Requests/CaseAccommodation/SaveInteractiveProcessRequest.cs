﻿using AbsenceSoft;
using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Notes.Contracts;
using AbsenceSoft.Logic.Tasks;
using AT.Api.Case.EntityFactories;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System;
using System.Linq;
using System.Net;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Requests.CaseAccommodation
{
    /// <summary>
    /// Request to save interactive process for accommodation
    /// </summary>
    public class SaveInteractiveProcessRequest : ApiRequest<SaveInteractiveProcessParameters, ResultSet<Guid>>
    {
        private readonly ICaseService _CaseService = null;
        private readonly IAccommodationService _AccommodationService = null;
        private readonly INotesService _NoteService = null;
        private readonly IAdminUserService _AdminuserService = null;
        private User CurrentUser;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="caseService"></param>
        /// <param name="accommodationService"></param>
        /// <param name="noteService"></param>
        /// <param name="adminuserService"></param>
        public SaveInteractiveProcessRequest(string userId, ICaseService caseService = null, IAccommodationService accommodationService = null,
            INotesService noteService = null, IAdminUserService adminuserService = null)
        {
            if (string.IsNullOrWhiteSpace(userId))
            {
                throw new ApiException(HttpStatusCode.BadRequest, "User Id is required");
            }

            _AdminuserService = adminuserService ?? new AdminUserService();
            CurrentUser = _AdminuserService.GetUserById(userId);
            if (CurrentUser == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Unable to find user");
            }
            _CaseService = caseService ?? new CaseService();
            _AccommodationService = accommodationService ?? new AccommodationService();
            _NoteService = noteService ?? new NotesService(CurrentUser);
        }

        /// <summary>
        /// Fulfil the method to save accommodation interactive process
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override ResultSet<Guid> FulfillRequest(SaveInteractiveProcessParameters parameters)
        {
            var caseNumber = parameters.CaseNumber;
            var theCase = _CaseService.GetCaseByCaseNumber(caseNumber);

            if (theCase == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, $"Case was not found for {caseNumber}");
            }

            if (theCase.AccommodationRequest == null)
            {
                return new ResultSet<Guid>(
                   Guid.Empty,
                   "No accommodation request available",
                   true,
                   HttpStatusCode.NoContent);
            }

            var accomodationId = new Guid(parameters.AccommodationId);
            if (theCase.AccommodationRequest.Accommodations == null || !theCase.AccommodationRequest.Accommodations.Any(a => a.Id == accomodationId))
            {
                return new ResultSet<Guid>(
                   Guid.Empty,
                   "No accommodation found",
                   true,
                   HttpStatusCode.NoContent);
            }

            var employerIntProcs = _AccommodationService.GetEmployerAccommodationInteractiveProcess(parameters.CustomerId, parameters.EmployerId, null, true);
            if (parameters.Steps.Select(p => p.QuestionId).Except(employerIntProcs.Steps.Select(p => p.QuestionId)).Any())
            {
                return new ResultSet<Guid>(
                   Guid.Empty,
                   "Invalid QuestionId",
                   true,
                   HttpStatusCode.NoContent);
            }

            var interactiveProcessId = UpdateInteractiveProcessSteps(parameters, accomodationId, theCase, employerIntProcs);

            return new ResultSet<Guid>(interactiveProcessId, "Accommodation interactive process saved");
        }
        
        /// TODO: Refactor method
        private Guid UpdateInteractiveProcessSteps(SaveInteractiveProcessParameters parameters,
            Guid accomodationId,
            Cases.Case theCase,
            AccommodationInteractiveProcess employerIntProcs)
        {
            var accommodation = theCase.AccommodationRequest.Accommodations.Find(ac => ac.Id == accomodationId);

            foreach (var step in parameters.Steps)
            {               
                var theStep = accommodation.InteractiveProcess.Steps.Where(x => x.QuestionId.Equals(step.QuestionId)).FirstOrDefault() ??
                              employerIntProcs.Steps.Where(x => x.QuestionId.Equals(step.QuestionId)).FirstOrDefault();

                theStep.Answer = !string.IsNullOrWhiteSpace(step.Answer) && step.Answer.ToLower() != "no";
                theStep.SAnswer = step.Answer;
                theStep.CompletedDate = DateTime.UtcNow.ToMidnight();

                var index = accommodation.InteractiveProcess.Steps.FindIndex(x => x.QuestionId.Equals(step.QuestionId));
                if (index < 0)
                {
                    accommodation.InteractiveProcess.Steps.Add(theStep);
                }
                else
                {
                    accommodation.InteractiveProcess.Steps[index] = theStep;
                }

             
                //save case note
                if (!string.IsNullOrEmpty(step.Note))
                {
                    var caseNote = CaseNoteFactory.FromIntProcCaseNoteModel(
                        step.Note, CurrentUser, parameters.CustomerId,
                        parameters.EmployerId, theStep.Question.Question,
                        theStep.QuestionId, accomodationId, theStep.Answer,
                        theStep.SAnswer, theCase);

                    _NoteService.SaveCaseNote(caseNote);
                }
            }

            //replace accommodation with existing 
            var accomIndex = theCase.AccommodationRequest.Accommodations.FindIndex(ac => ac.Id == accomodationId);
            theCase.AccommodationRequest.Accommodations[accomIndex] = accommodation;

            _CaseService.UpdateCase(theCase, null);

            return accommodation.InteractiveProcess.Id;
        }

        /// <summary>
        /// Validate parameters for save interactive process
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool IsValid(SaveInteractiveProcessParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }
            if (string.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Employer id is required";
                return false;
            }
            if (string.IsNullOrWhiteSpace(parameters.CustomerId))
            {
                message = "Customer id is required";
                return false;
            }
            if (string.IsNullOrWhiteSpace(parameters.AccommodationId))
            {
                message = "Accommodation id is required";
                return false;
            }
            if (parameters.Steps == null)
            {
                message = "No data to save interactive process";
                return false;
            }

            return true;
        }
    }
}