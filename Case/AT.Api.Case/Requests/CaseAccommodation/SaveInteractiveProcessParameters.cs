﻿using AT.Api.Case.Models.CaseAccommodation;
using System.Collections.Generic;

namespace AT.Api.Case.Requests.CaseAccommodation
{
    /// <summary>
    /// Save interactive process parameters
    /// </summary>
    public class SaveInteractiveProcessParameters
    {
        /// <summary>
        /// Employer Id
        /// </summary>
        public string EmployerId { get; set; }
        /// <summary>
        /// Customer Id
        /// </summary>
        
        public string CustomerId { get; set; }
        /// <summary>
        /// Case Number
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// Accommodation id
        /// </summary>
        public string AccommodationId { get; set; }

        /// <summary>
        /// interactive process model
        /// </summary>
        public List<InteractiveProcessStepModel> Steps { get; set; }
    }
}