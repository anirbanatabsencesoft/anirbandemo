﻿using AT.Api.Case.Models.CaseAccommodation;

namespace AT.Api.Case.Requests.CaseAccommodation
{
    public class CreateCaseAccommodationParameters
    {
        public string CaseNumber { get; set; }

        public AccommodationRequestModel Model { get; set; }
    }
}