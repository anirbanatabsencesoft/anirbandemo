﻿using AT.Api.Case.Models.CaseAccommodation;

namespace AT.Api.Case.Requests.CaseAccommodation
{
    /// <summary>
    /// Parameters to close a case accommodation
    /// </summary>
    public class CloseCaseAccommodationParameters
    {
        /// <summary>
        /// Unique number for the case
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// Id for the accommodation
        /// </summary>
        public string AccommodationId { get; set; }

        /// <summary>
        /// Data to close case accommodation
        /// </summary>
        public CloseCaseAccommodationModel Model { get; set; }
    }
}