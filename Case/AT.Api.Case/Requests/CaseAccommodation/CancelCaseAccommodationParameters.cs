﻿using AT.Api.Case.Models.CaseAccommodation;

namespace AT.Api.Case.Requests.CaseAccommodation
{
    public class CancelCaseAccommodationParameters
    {
        public string CaseNumber { get; set; }
        public string AccommodationId { get; set; }

        public CancelCaseAccommodationModel Model { get; set; }
    }
}