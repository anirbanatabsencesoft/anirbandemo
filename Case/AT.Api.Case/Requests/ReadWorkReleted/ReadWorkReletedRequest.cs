﻿using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.DataExtensions;
using AT.Api.Case.Models.WorkReleted;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Net;
using System.Threading.Tasks;

namespace AT.Api.Case.Requests.ReadWorkReleted
{

    public class ReadWorkReletedRequest : ApiRequest<string, ResultSet<WorkReletedModel>>
    {

        private readonly ICaseService _CaseService = null;
        public ReadWorkReletedRequest(ICaseService CaseService = null)
        {
            _CaseService = CaseService ?? new CaseService();
        }
        protected override ResultSet<WorkReletedModel> FulfillRequest(string CaseNumber)
        {
            var result = ReasWorkReletedRequestForCase(CaseNumber);
            return (new ResultSet<WorkReletedModel>(result, "Work Releted Retrieved successfully"));
        }

        protected override bool IsValid(string CaseNumber, out string message)
        {
            message = string.Empty;
            if (string.IsNullOrEmpty(CaseNumber))
            {
                message = "CaseNumber is required";
                return false;
            }
            return true;

        }

        public WorkReletedModel ReasWorkReletedRequestForCase(string CaseNumber)
        {
            var caseData = _CaseService.GetCaseByCaseNumber(CaseNumber);

            if (caseData == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, "Case not found for the given case number");
            }
            return WorkReletedModelExtension.ToWorkRelatedInfo(caseData.WorkRelated);

        }


    }
}