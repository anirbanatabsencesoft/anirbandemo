﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Administration.Contracts;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Tasks;
using AbsenceSoft.Logic.Tasks.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Net;

namespace AT.Api.Case.Requests.CreateToDo
{
    /// <summary>
    /// Create ToDo Request
    /// </summary>
    public class CreateToDoRequest : ApiRequest<CreateToDoParameters, ResultSetBase>
    {
        private readonly IAdminUserService _AdminUserService = null;
        private readonly IAdministrationService _AdministrationService = null;
        private readonly ICaseService _CaseService = null;
        private readonly IToDoService _ToDoService = null;
        private readonly User _CurrentUser = null;

        /// <summary>
        /// CreateToDoRequest constructor
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="adminUserService"></param>
        /// <param name="administrationService"></param>
        /// <param name="caseService"></param>
        /// <param name="toDoService"></param>
        public CreateToDoRequest(string userId,
            IAdminUserService adminUserService = null,
            IAdministrationService administrationService = null,
            ICaseService caseService = null,
            IToDoService toDoService = null)
        {
            if (string.IsNullOrWhiteSpace(userId))
            {
                throw new ApiException(HttpStatusCode.Unauthorized, "Unable to retrieve authorized user detail");
            }

            _AdminUserService = adminUserService ?? new AdminUserService();
            _AdministrationService = administrationService ?? new AdministrationService();
            _CaseService = caseService ?? new CaseService();
            _ToDoService = toDoService ?? new ToDoService();

            _CurrentUser = _AdminUserService.GetUserById(userId);

            if (_CurrentUser == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Unable to find user");
            }
        }

        /// <summary>
        /// Asynchronous method to create to do for case
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override ResultSetBase FulfillRequest(CreateToDoParameters parameters)
        {
            var caseNumber = parameters.CaseNumber;
            var createToDoModel = parameters.CreateToDoModel;
            var theCase = _CaseService.GetCaseByCaseNumber(caseNumber);

            if (theCase == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, $"Case was not found for {caseNumber}");
            }

            User user = null;
            if (!string.IsNullOrWhiteSpace(createToDoModel.AssigneeUserEmail))
            {
                user = _AdministrationService.GetUserByEmail(createToDoModel.AssigneeUserEmail);
            }

            var toDoItem = _ToDoService.CreateManualTodoItem(
                theCase,
                _CurrentUser,
                ToDoItemType.Manual,
                createToDoModel.Title,
                createToDoModel.Description,
                createToDoModel.DueDate,
                ToDoItemPriority.Normal,
                user?.Id);

            if (toDoItem == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, $"Error creating todo item for {caseNumber}");
            }

            return new ResultSetBase("ToDo created successfully");
        }

        /// <summary>
        /// Validate input parameters received by API
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message">Message to return if any request parameter is invalid or not supplied</param>
        /// <returns></returns>
        protected override bool IsValid(CreateToDoParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }

            if (parameters.CreateToDoModel == null)
            {
                message = "Data to create ToDo is required";
                return false;
            }

            return true;
        }
    }
}