﻿using AT.Api.Case.Models.CreateToDo;

namespace AT.Api.Case.Requests.CreateToDo
{
    /// <summary>
    /// Create ToDo Parameters
    /// </summary>
    public class CreateToDoParameters
    {
        /// <summary>
        /// Gets or Sets CaseNumber
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// Gets of Sets CreateToDoModel
        /// </summary>
        public CreateToDoModel CreateToDoModel { get; set; }
    }
}