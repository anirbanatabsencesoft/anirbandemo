﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Administration;
using AT.Api.Case.Models.NoteCategories;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System;
using System.Linq;
using System.Net;

namespace AT.Api.Case.Requests.NoteCategories
{
    public class GetNoteCategoriesRequest : ApiRequest<NoteCategoriesParameter, CollectionResultSet<NoteCategory>>
    {
        /// <summary>
        /// constructor
        /// </summary>
        public GetNoteCategoriesRequest()
        {    
        }
        /// <summary>
        /// FulfillAsync with Empty NoteCategoriesParameter
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override CollectionResultSet<NoteCategory> FulfillRequest(NoteCategoriesParameter parameters)
        {
            var NoteCategoryModel =
              ((NoteCategoryEnum[])Enum.GetValues(typeof(NoteCategoryEnum)))
             .Select(c => new NoteCategory() { noteId = (int)c, Description = c.ToString() }).ToList();
            return new CollectionResultSet<NoteCategory>(NoteCategoryModel);
        }

        /// <summary>
        /// Valid for validation NoteCategoriesParameter parameter
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool IsValid(NoteCategoriesParameter parameters, out string message)
        {
            message = string.Empty;
            return true;
        }
    }
}