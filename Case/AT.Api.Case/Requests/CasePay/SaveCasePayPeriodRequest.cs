﻿using AbsenceSoft.Logic.Cases;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System;
using System.Linq;
using System.Net;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Security;
using AT.Api.Case.Models.CasePay;
using AT.Api.Case.DataExtensions;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers.Contracts;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Pay.Contracts;
using Pay = AbsenceSoft.Logic.Pay;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Requests.CasePay
{
    /// <summary>
    /// Save Case Pay period Status Request
    /// </summary>    
    public class SaveCasePayPeriodRequest : ApiRequest<SaveCasePayPeriodParameters, ResultSet<CasePayPeriodModel>>
    {
        private readonly ICaseService _CaseService = null;
        private readonly IEmployerService _EmployerService = null;
        private readonly IPayService _PayService = null;
        private readonly IAdminUserService _AdminUserService = null;
        User CurrentUser;

        /// <summary>
        /// Constructor
        /// </summary>        
        /// <param name="userId"></param>
        /// <param name="caseService"></param>
        /// <param name="employerService"></param>
        /// <param name="payService"></param>
        /// <param name="adminUserService"></param>
        public SaveCasePayPeriodRequest(string userId = null, ICaseService caseService = null, IEmployerService employerService = null, IPayService payService = null, IAdminUserService adminUserService = null)
        {
            _CaseService = caseService ?? new CaseService(false);
            _EmployerService = employerService ?? new EmployerService();
            _AdminUserService = adminUserService ?? new AdminUserService();
            if (!string.IsNullOrWhiteSpace(userId))
            {
                CurrentUser = _AdminUserService.GetUserById(userId);
                if (CurrentUser == null)
                {
                    throw new ApiException(HttpStatusCode.InternalServerError, "Unable to find user");
                }
            }
            _PayService = payService ?? new Pay.PayService(CurrentUser);
        }

        /// <summary>
        /// override abstrat method to fulfillasync request
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override ResultSet<CasePayPeriodModel> FulfillRequest(SaveCasePayPeriodParameters parameters)
        {
            return Fulfill(parameters);
        }

        /// <summary>
        /// Get updated Pay period for Case
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private ResultSet<CasePayPeriodModel> Fulfill(SaveCasePayPeriodParameters parameters)
        {
            Cases.Case existingCase = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);
            if (existingCase == null)
                throw new ApiException(HttpStatusCode.ExpectationFailed, "Case not found with the given details");
            if (existingCase.Employee == null)
                throw new ApiException(HttpStatusCode.ExpectationFailed, "Employee not found");

            existingCase = _PayService.UpdatePayPeriodStatus(existingCase, new Guid(parameters.PayPeriod.PayPeriodId), (PayrollStatus)parameters.PayPeriod.Status);
            existingCase = _CaseService.UpdateCase(existingCase);
            var paymodel = _PayService.GetCasePayModel(existingCase);
            if (paymodel != null && paymodel.PayPeriods != null && paymodel.PayPeriods.Count > 0)
            {
                Pay.CasePayPeriodModel payperiod = paymodel.PayPeriods.Where(s => s.PayPeriodId == new Guid(parameters.PayPeriod.PayPeriodId)).FirstOrDefault();
                parameters.PayPeriod = CasePayExtensions.ToCasePayPeriodModel(payperiod);
            }
            return new ResultSet<CasePayPeriodModel>(parameters.PayPeriod, "Case pay period status updated");
        }

        /// <summary>
        /// Validate input parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool IsValid(SaveCasePayPeriodParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrEmpty(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }

            if (String.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Missing EmployerId";
                return false;
            }

            if (parameters.PayPeriod == null)
            {
                message = "Case pay period was not provided";
                return false;
            }

            if (String.IsNullOrWhiteSpace(parameters.PayPeriod.PayPeriodId))
            {
                message = "Case PayPeriodId was not provided";
                return false;
            }

            if (parameters.PayPeriod.Status < 0)
            {
                message = "Case payroll status was not provided";
                return false;
            }

            if (!string.IsNullOrEmpty(parameters.EmployerId))
            {
                Employer employer = _EmployerService.GetById(parameters.EmployerId);
                if (employer == null)
                {
                    message = "Employer is not valid.";
                    return false;
                }
            }
            return true;
        }
    }
}