﻿using AbsenceSoft.Data.Enums;
using AT.Api.Case.Models.CasePay;

namespace AT.Api.Case.Requests.CasePay
{
    /// <summary>
    /// Api Save Case Pay Period Parameters
    /// </summary>
    public class SaveCasePayPeriodParameters
    {
        /// <summary>
        /// Case Number for the request
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// EmployerId
        /// </summary>
        public string EmployerId { get; set; }

        /// <summary>
        /// Case pay period model for updating status of provided pay period
        /// </summary>
        public CasePayPeriodModel PayPeriod { get; set; }
    }
}