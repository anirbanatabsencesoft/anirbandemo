﻿using AbsenceSoft.Data.Enums;

namespace AT.Api.Case.Requests.CasePay
{
    /// <summary>
    /// Api GetCasePayParameters Request Parameters
    /// </summary>
    public class GetCasePayParameters
    {
        /// <summary>
        /// Case Number for the request
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// EmployerId
        /// </summary>
        public string EmployerId { get; set; }
    }
}