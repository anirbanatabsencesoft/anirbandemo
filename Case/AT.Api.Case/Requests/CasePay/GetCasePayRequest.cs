﻿using AbsenceSoft.Logic.Cases;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System;
using System.Net;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Security;
using AT.Api.Case.Models.CasePay;
using AT.Api.Case.DataExtensions;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers.Contracts;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Logic.Pay.Contracts;
using Pay = AbsenceSoft.Logic.Pay;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Requests.CasePay
{
    /// <summary>
    /// Get Case Pay Request
    /// </summary>    
    public class GetCasePayRequest : ApiRequest<GetCasePayParameters, ResultSet<CasePayModel>>
    {
        private readonly ICaseService _CaseService = null;
        private readonly IEmployerService _EmployerService = null;
        private readonly IPayService _PayService = null;
        private readonly IAdminUserService _AdminUserService = null;
        User CurrentUser;

        /// <summary>
        /// Constructor
        /// </summary>        
        /// <param name="userId"></param>
        /// <param name="caseService"></param>
        /// <param name="employerService"></param>
        /// <param name="payService"></param>
        /// <param name="adminUserService"></param>
        public GetCasePayRequest(string userId = null, ICaseService caseService = null, IEmployerService employerService = null, IPayService payService = null, IAdminUserService adminUserService = null)
        {
            _CaseService = caseService ?? new CaseService();
            _EmployerService = employerService ?? new EmployerService();
            _AdminUserService = adminUserService ?? new AdminUserService();
            if (!string.IsNullOrWhiteSpace(userId))
            {
                CurrentUser = _AdminUserService.GetUserById(userId);
                if (CurrentUser == null)
                {
                    throw new ApiException(HttpStatusCode.InternalServerError, "Unable to find user");
                }
            }
            _PayService = payService ?? new Pay.PayService(CurrentUser);
        }

        /// <summary>
        /// override abstrat method to fulfillasync request
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override ResultSet<CasePayModel> FulfillRequest(GetCasePayParameters parameters)
        {
            return Fulfill(parameters);
        }

        /// <summary>
        /// Get Pay model for Case
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private ResultSet<CasePayModel> Fulfill(GetCasePayParameters parameters)
        {
            Cases.Case existingCase = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);
            if (existingCase == null)
                throw new ApiException(HttpStatusCode.ExpectationFailed, "Case not found with the given details");
            if (existingCase.Employee == null)
                throw new ApiException(HttpStatusCode.ExpectationFailed, "Employee not found");

            var casePayModel = new CasePayModel();

            Pay.CasePayModel paymodel = _PayService.GetCasePayModel(existingCase);

            if (paymodel != null)
                casePayModel = CasePayExtensions.ToCasePayModel(paymodel, existingCase.Employee != null ? existingCase.Employee.EmployeeNumber : "");

            return new ResultSet<CasePayModel>(casePayModel, "Case pay retrieved");
        }        
        
        /// <summary>
        /// Validate input parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool IsValid(GetCasePayParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrEmpty(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }
            if (String.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Missing EmployerId";
                return false;
            }

            if (!string.IsNullOrEmpty(parameters.EmployerId))
            {
                Employer employer = _EmployerService.GetById(parameters.EmployerId);
                if (employer == null)
                {
                    message = "Employer is not valid.";
                    return false;
                }
            }
            return true;
        }
    }
}