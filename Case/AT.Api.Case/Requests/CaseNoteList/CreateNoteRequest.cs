﻿
using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Notes.Contracts;
using AbsenceSoft.Logic.Tasks;
using AT.Api.Case.Models.CaseNotes;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System;
using System.Net;
using Notes = AbsenceSoft.Data.Notes;

namespace AT.Api.Case.Requests.CaseNotes
{
    /// <summary>
    /// 
    /// </summary>
    public class CreateNoteRequest : ApiRequest<CreateNoteParameters, ResultSet<string>>
    {
        private readonly INotesService _noteService = null;
        private readonly ICaseService _caseService = null;
        private readonly IAdminUserService _adminuserService = null;
        private User CurrentUser;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="noteService"></param>
        /// <param name="caseService"></param>
        /// <param name="adminuserService"></param>
        public CreateNoteRequest(string userId, INotesService noteService = null, ICaseService caseService = null, IAdminUserService adminuserService = null)
        {
            _caseService = caseService ?? new CaseService();
            _adminuserService = adminuserService ?? new AdminUserService();
            if (!string.IsNullOrWhiteSpace(userId))
            {
                CurrentUser = _adminuserService.GetUserById(userId);
                if (CurrentUser == null)
                {
                    throw new ApiException(HttpStatusCode.InternalServerError, "Unable to find user");
                }
            }
            _noteService = noteService ?? new NotesService(CurrentUser);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override ResultSet<string> FulfillRequest(CreateNoteParameters parameters)  //FulFillAsync
        {
            bool result = CreateCaseNote(parameters.CaseNumber, parameters.CaseNoteModel);
            return (new ResultSet<string>(result.ToString(), "Case note saved successfully"));
        }

        /// <summary>
        /// Create Case Note
        /// </summary>
        /// <param name="caseNumber"></param>
        /// <param name="caseNoteModel"></param>
        /// <returns></returns>
        public bool CreateCaseNote(string caseNumber, CaseNoteModel caseNoteModel)
        {
            AbsenceSoft.Data.Cases.Case myCase = _caseService.GetCaseByCaseNumber(caseNumber);

            if (myCase == null)
            {
                throw new ApiException(HttpStatusCode.ExpectationFailed, "Case not found");
            }

            Notes.CaseNote note = new Notes.CaseNote()
            {
                Category = (NoteCategoryEnum)caseNoteModel.Category,
                CreatedBy = CurrentUser,
                CustomerId = myCase.CustomerId,
                EmployerId = myCase.EmployerId,
                Public = caseNoteModel.Public,
                Case = myCase,
                CaseId = myCase.Id
            };
            note.ModifiedBy = CurrentUser;
            note.Notes = caseNoteModel.Notes;
            _noteService.SaveCaseNote(note);
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool IsValid(CreateNoteParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrEmpty(parameters.CaseNumber))
            {
                message = "Case number was not provided";
                return false;
            }

            if (parameters.CaseNoteModel == null)
            {
                message = "Case note model was not provided";
                return false;
            }

            if (string.IsNullOrEmpty(parameters.CaseNoteModel.Notes))
            {
                message = "Case notes was not provided";
                return false;
            }

            if (parameters.CaseNoteModel.Category < 0)
            {
                message = "Case note category was not provided";
                return false;
            }

            return true;
        }

    }
}


