﻿
using AbsenceSoft;
using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Notes.Contracts;
using AbsenceSoft.Logic.Tasks;
using AT.Api.Case.Models.CaseNotes;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace AT.Api.Case.Requests.CaseNotes
{
    /// <summary>
    /// 
    /// </summary>
    public class GetNoteListRequest : ApiRequest<GetNoteListParameters, CollectionResultSet<CaseNoteModel>>
    {
        private readonly INotesService _noteService = null;
        private readonly ICaseService _caseService = null;
        private readonly IAdminUserService _adminuserService = null;
        private readonly User CurrentUser;

        /// <summary>
        /// GetNoteListRequest
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="noteService"></param>        
        /// <param name="caseService"></param>
        /// <param name="adminuserService"></param>
        public GetNoteListRequest(string userId, INotesService noteService = null, ICaseService caseService = null, IAdminUserService adminuserService = null)
        {
            _caseService = caseService ?? new CaseService();
            _adminuserService = adminuserService ?? new AdminUserService();
            if (!string.IsNullOrWhiteSpace(userId))
            {
                CurrentUser = _adminuserService.GetUserById(userId);
                if (CurrentUser == null)
                {
                    throw new ApiException(HttpStatusCode.InternalServerError, "Unable to find user");
                }
            }
            _noteService = noteService ?? new NotesService(CurrentUser);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override CollectionResultSet<CaseNoteModel> FulfillRequest(GetNoteListParameters parameters)  //FulFillAsync
        {
            return GetCaseNotes(parameters.CaseNumber);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool IsValid(GetNoteListParameters parameters, out string message)  //isValid
        {
            message = string.Empty;

            if (string.IsNullOrEmpty(parameters.CaseNumber))
            {
                message = "Case number was not provided";
                return false;
            }

            return true;
        }

        /// <summary>
        /// Get Time Tracking Info For Case
        /// </summary>
        /// <param name="CaseNumber"></param>        
        /// <returns></returns>
        public CollectionResultSet<CaseNoteModel> GetCaseNotes(string CaseNumber)
        {
            AbsenceSoft.Data.Cases.Case c = _caseService.GetCaseByCaseNumber(CaseNumber);
            if (c == null)
            {
                throw new ApiException(HttpStatusCode.ExpectationFailed, "Case not found");
            }

            var caseNoteModels = new List<CaseNoteModel>();

            var criteria = new AbsenceSoft.Logic.ListCriteria()
            {
                PageNumber = 1,
                PageSize = int.MaxValue
            };
            criteria.Set("Id", c.Id);

            AbsenceSoft.Logic.ListResults result = _noteService.GetCaseNoteList(criteria);

            if (result != null && result.Results.Count() > 0)
            {
                result.Results.ForEach(o =>
                {
                    caseNoteModels.Add(ConvertToCaseNoteModel(o));
                });
            }

            return new CollectionResultSet<CaseNoteModel>(caseNoteModels);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public CaseNoteModel ConvertToCaseNoteModel(AbsenceSoft.Logic.ListResult result)
        {
            CaseNoteModel model = new CaseNoteModel();
            model.CreatedBy = result.Get<string>("CreatedBy");
            model.CreatedDate = result.Get<DateTime>("CreatedDate").ToUIString();
            model.Notes = result.Get<string>("Notes");
            model.Category = result.Get<int>("NoteCategoryEnum");
            model.NoteCategory = result.Get<string>("Category");
            model.Public = result.Get<bool>("Public");
            return model;
        }

    }
}


