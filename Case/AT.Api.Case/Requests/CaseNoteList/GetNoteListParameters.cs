﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AT.Api.Case.Requests.CaseNotes
{
    /// <summary>
    /// GetNoteList Parameters
    /// </summary>
    public class GetNoteListParameters
    {
        /// <summary>
        /// Case Number
        /// </summary>
        public string CaseNumber { get; set; }
    }
}