﻿using AT.Api.Case.Models.CaseNotes;

namespace AT.Api.Case.Requests.CaseNotes
{
    /// <summary>
    /// Parameters to create a note
    /// </summary>
    public class CreateNoteParameters
    {
        /// <summary>
        /// 
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// Case note model
        /// </summary>
        public CaseNoteModel CaseNoteModel { get; set; }

    }
}