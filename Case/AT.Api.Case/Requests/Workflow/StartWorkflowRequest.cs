﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Workflows;
using AbsenceSoft.Logic.Workflows.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Net;

namespace AT.Api.Case.Requests.Workflow
{
    /// <summary>
    /// Handles request to start a work-flow
    /// </summary>
    public class StartWorkflowRequest : ApiRequest<StartWorkflowParameters, ResultSetBase>
    {
        private readonly IWorkflowService _WorkflowService = null;
        private readonly ICaseService _CaseService = null;

        /// <summary>
        /// Starts a work-flow
        /// </summary>
        /// <param name="userId">Current logged in user id</param>
        /// <param name="adminUserService"></param>
        /// <param name="workflowService"></param>
        /// <param name="caseService"></param>
        public StartWorkflowRequest(
            string userId,
            IAdminUserService adminUserService = null,
            IWorkflowService workflowService = null,
            ICaseService caseService = null)
        {
            if (string.IsNullOrWhiteSpace(userId))
            {
                throw new ApiException(HttpStatusCode.Unauthorized, "Unable to retrieve authorized user detail");
            }

            var userService = adminUserService ?? new AdminUserService();
            var user = userService.GetUserById(userId);

            if (user == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Unable to find user");
            }

            _WorkflowService = workflowService ?? new WorkflowService(user);
            _CaseService = caseService ?? new CaseService();
        }

        protected override ResultSetBase FulfillRequest(StartWorkflowParameters parameters)
        {
            var caseData = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);

            if (caseData == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Case not found with the given case number");
            }

            _WorkflowService.Run(parameters.WorkflowCode, caseData.Id, null);
            _WorkflowService.Dispose();

            return new ResultSetBase("Workflow started");
        }

        protected override bool IsValid(StartWorkflowParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.WorkflowCode))
            {
                message = "Workflow code is required";
                return false;
            }

            return true;
        }
    }
}