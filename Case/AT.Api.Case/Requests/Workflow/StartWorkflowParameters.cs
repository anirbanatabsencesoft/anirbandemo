﻿namespace AT.Api.Case.Requests.Workflow
{
    public class StartWorkflowParameters
    {
        public string CaseNumber { get; set; }
        public string WorkflowCode { get; set; }
    }
}