﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.DataExtensions;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Net;

namespace AT.Api.Case.Requests.SaveWorkReleted
{
    public class SaveWorkReletedRequest : ApiRequest<SaveWorkReletedParameters, ResultSet<string>>
    {

        private readonly ICaseService _CaseService = null;
        public SaveWorkReletedRequest(ICaseService CaseService = null)
        {
            _CaseService = CaseService ?? new CaseService();
        }
        protected override ResultSet<string> FulfillRequest(SaveWorkReletedParameters parameters)
        {
            string result = SaveWorkReletedRequestForCase(parameters);
            return (new ResultSet<string>(result, "Work Related Info saved successfully"));
        }

        protected override bool IsValid(SaveWorkReletedParameters parameters, out string message)
        {
            message = string.Empty;
            if (string.IsNullOrEmpty(parameters.CaseNumber))
            {
                message = "CaseNumber is required";
                return false;
            }
            return true;
        }

        public string SaveWorkReletedRequestForCase(SaveWorkReletedParameters parameters)
        {

            var caseData = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);

            if (caseData == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, "Case not found for the given case number");
            }
            caseData.WorkRelated = WorkReletedExtension.ToWorkRelatedInfo(parameters.WorkReleted);

            if (parameters.WorkReleted.IllnessOrInjuryDate.HasValue)
                caseData.SetCaseEvent(CaseEventType.IllnessOrInjuryDate, parameters.WorkReleted.IllnessOrInjuryDate.Value);
            else if (caseData != null)
                caseData.WorkRelated.IllnessOrInjuryDate = caseData.GetMostRecentEventDate(CaseEventType.IllnessOrInjuryDate);
            _CaseService.UpdateCase(caseData);
            return caseData.CaseNumber;
        }
    }
}