﻿using AT.Api.Case.Models.WorkReleted;

namespace AT.Api.Case.Requests.SaveWorkReleted
{
    public class SaveWorkReletedParameters
    {
        public string CaseNumber { get; set; }
        public WorkReletedModel WorkReleted { get; set; }
    }
}