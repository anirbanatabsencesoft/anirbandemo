﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AbsenceSoft.Logic.Notes.Contracts;
using AbsenceSoft.Logic.Tasks;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using Notes = AbsenceSoft.Data.Notes;
using AT.Api.Case.DataExtensions;
using AT.Api.Case.Models.Certifications;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Requests.Certifications
{
    /// <summary>
    /// Save Certifications Request
    /// </summary>
    public class SaveCertificationsRequest : ApiRequest<SaveCertificationsParameters, ResultSet<bool>>
    {
        private readonly IEmployerService _EmployerService = null;
        private readonly ICaseService _CaseService = null;
        private readonly IAdminUserService _AdminUserService = null;
        private readonly INotesService _NoteService = null;
        private User CurrentUser;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userId"></param>     
        /// <param name="caseService"></param>        
        /// <param name="noteService"></param>        
        /// <param name="adminUserService"></param>        
        public SaveCertificationsRequest(string userId, ICaseService caseService = null, INotesService noteService = null, IAdminUserService adminUserService = null, IEmployerService employerService = null)
        {
            _NoteService = noteService ?? new NotesService();
            _CaseService = caseService ?? new CaseService();
            _EmployerService = employerService ?? new EmployerService();
            _AdminUserService = adminUserService ?? new AdminUserService();
            CurrentUser = _AdminUserService.GetUserById(userId);
            if (CurrentUser == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Unable to find user");
            }
        }

        /// <summary>
        /// Save Certifications data
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override ResultSet<bool> FulfillRequest(SaveCertificationsParameters parameters)
        {
            Cases.Case existingCase = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);

            if (existingCase == null)
            {
                throw new ApiException(HttpStatusCode.ExpectationFailed, "Case not found with the given details");
            }

            foreach (CertificationModel certification in parameters.Certifications)
            {
                Certification cert = null;
                bool isNewCert = false;
                if (certification.Id != null && existingCase.Certifications.Any(x => x.Id == certification.Id))
                {
                    cert = existingCase.Certifications.First(x => x.Id == certification.Id);
                }
                else
                {
                    cert = new Certification();
                    isNewCert = true;
                }

                cert = certification.FromCertificationModel();

                existingCase = _CaseService.CreateOrModifyCertification(existingCase, cert, isNewCert);

                if (!string.IsNullOrWhiteSpace(certification.Notes))
                {
                    Notes.CaseNote note = new Notes.CaseNote()
                    {
                        Category = NoteCategoryEnum.Certification,
                        CreatedBy = CurrentUser,
                        CustomerId = existingCase.CustomerId,
                        EmployerId = existingCase.EmployerId,
                        Public = false,
                        Case = existingCase
                    };
                    note.ModifiedBy = CurrentUser;
                    note.Notes = certification.Notes;
                    note.CertId = cert.Id;
                    //note.Metadata.SetRawValue("CertId", cert.Id.ToString());
                    _NoteService.SaveCaseNote(note);
                }
            }

            return (new ResultSet<bool>(true, "Certifications information saved successfully"));
        }

        /// <summary>
        /// Validate input parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool IsValid(SaveCertificationsParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrEmpty(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }

            if (parameters.Certifications == null || !parameters.Certifications.Any())
            {
                message = "Certification details not provided";
                return false;
            }

            if (String.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Missing EmployerId";
                return false;
            }

            if (!string.IsNullOrEmpty(parameters.EmployerId))
            {
                Employer employer = _EmployerService.GetById(parameters.EmployerId);
                if (employer == null)
                {
                    message = "Employer is not valid.";
                    return false;
                }
            }

            StringBuilder errors = new StringBuilder();
            foreach (Models.Certifications.CertificationModel certification in parameters.Certifications)
            {
                // Validate the input
                if (!certification.IsCertificateIncomplete)
                {
                    if (certification.Frequency == null)
                    {
                        errors.AppendLine("Frequency is required");
                    }

                    if (certification.Occurances == null)
                    {
                        errors.AppendLine("Frequency episode occurance is required");
                    }

                    if (certification.FrequencyType == null)
                    {
                        errors.AppendLine("Frequency episode occurance type is required");
                    }

                    if (certification.Duration == null)
                    {
                        errors.AppendLine("Duration is required");
                    }

                    if (certification.DurationType == null)
                    {
                        errors.AppendLine("Duration episode occurance is required");
                    }

                    if (errors.Length > 0)
                    {
                        continue;
                    }
                }
            }

            if (errors.ToString().Length > 0)
            {
                message = "Some required certification details are missing : " + Environment.NewLine + errors.ToString();
                return false;
            }

            return true;
        }
    }
}