﻿using AT.Api.Case.Models.Certifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AT.Api.Case.Requests.Certifications
{
    /// <summary>
    /// Get Certifications Parameters
    /// </summary>
    public class GetCertificationsParameters
    {
        /// <summary>
        /// Case Number for the request
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// EmployerId
        /// </summary>
        public string EmployerId { get; set; }        
    }
}