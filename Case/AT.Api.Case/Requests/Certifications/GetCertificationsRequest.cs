﻿using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Notes.Contracts;
using AbsenceSoft.Logic.Tasks;
using AT.Api.Case.DataExtensions;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using CertModel = AT.Api.Case.Models.Certifications;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Requests.Certifications
{
    /// <summary>
    /// Get Certifications Request
    /// </summary>
    public class GetCertificationsRequest : ApiRequest<GetCertificationsParameters, CollectionResultSet<CertModel.CertificationModel>>
    {
        private readonly ICaseService _CaseService = null;
        private readonly INotesService _NoteService = null;

        /// <summary>
        /// Constructor
        /// </summary>        
        /// <param name="caseService"></param>
        /// <param name="noteService"></param>
        public GetCertificationsRequest(ICaseService caseService = null, INotesService noteService = null)
        {
            _CaseService = caseService ?? new CaseService();
            _NoteService = noteService ?? new NotesService();
        }

        /// <summary>
        /// Get Certifications data
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override CollectionResultSet<CertModel.CertificationModel> FulfillRequest(GetCertificationsParameters parameters)
        {
            Cases.Case existingCase = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);

            if (existingCase == null)
            {
                throw new ApiException(HttpStatusCode.ExpectationFailed, "Case not found with the given details");
            }

            List<CertModel.CertificationModel> list = new List<CertModel.CertificationModel>();
            if (existingCase.Certifications != null && existingCase.Certifications.Any())
            {
                list = CertificationExtensions.ToCertificationModel(existingCase.Certifications);
                foreach (CertModel.CertificationModel model in list)
                {
                    model.Notes = model.Id.HasValue ? _NoteService.GetCaseNoteByCertId(model.Id.Value) : null;
                }
                return new CollectionResultSet<CertModel.CertificationModel>(list);
            }
            return new CollectionResultSet<CertModel.CertificationModel>(list);
        }

        /// <summary>
        /// Validate input parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool IsValid(GetCertificationsParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrEmpty(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }

            if (String.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Missing employerId";
                return false;
            }
            return true;
        }
    }
}