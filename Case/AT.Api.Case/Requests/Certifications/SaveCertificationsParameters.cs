﻿using AT.Api.Case.Models.Certifications;
using System.Collections.Generic;

namespace AT.Api.Case.Requests.Certifications
{
    /// <summary>
    /// Save Certifications Parameters
    /// </summary>
    public class SaveCertificationsParameters
    {
        /// <summary>
        /// Case Number for the request
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// EmployerId
        /// </summary>
        public string EmployerId { get; set; }

        /// <summary>
        /// Certification model to add certifications details 
        /// </summary>
        public List<CertificationModel> Certifications { get; set; }
    }
}