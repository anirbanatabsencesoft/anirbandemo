﻿namespace AT.Api.Case.Requests.RecalculateCase
{
    public class RecalculateCaseParameters
    {
        public string CaseNumber { get; set; }
        public bool SaveCase { get; set; }
    }
}