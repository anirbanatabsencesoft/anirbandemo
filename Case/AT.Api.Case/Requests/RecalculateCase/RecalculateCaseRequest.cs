﻿using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Net;

namespace AT.Api.Case.Requests.RecalculateCase
{
    public class RecalculateCaseRequest : ApiRequest<RecalculateCaseParameters, ResultSetBase>
    {
        private readonly ICaseService _CaseService = null;

        public RecalculateCaseRequest(ICaseService caseService = null)
        {
            _CaseService = caseService ?? new CaseService();
        }

        protected override ResultSetBase FulfillRequest(RecalculateCaseParameters parameters)
        {
            var caseNumber = parameters.CaseNumber;
            var theCase = _CaseService.GetCaseByCaseNumber(caseNumber);

            if (theCase == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, $"Case was not found for {caseNumber}");
            }

            theCase = _CaseService.RunCalcs(theCase);

            if (parameters.SaveCase)
            {
                _CaseService.UpdateCase(theCase);
            }

            return new ResultSetBase("Case recalculated successfully");
        }

        protected override bool IsValid(RecalculateCaseParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }

            return true;
        }
    }
}