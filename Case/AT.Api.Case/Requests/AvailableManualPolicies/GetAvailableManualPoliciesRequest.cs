﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Net;
namespace AT.Api.Case.Requests.AvailableManualPolicies
{
    public class GetAvailableManualPoliciesRequest : ApiRequest<string, CollectionResultSet<Policy>>
    {
        private readonly ICaseService _CaseService = null;
        private readonly IEligibilityService _EligibilityService = null;
        private readonly IEmployerService _IEmployerService = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="EligibilityService"></param>
        /// <param name="CaseService"></param>
        public GetAvailableManualPoliciesRequest(string EmployerId, IEligibilityService EligibilityService = null, ICaseService CaseService = null, IEmployerService IEmployerService = null)
        {
            _CaseService = CaseService ?? new CaseService();
            _IEmployerService = IEmployerService ?? new EmployerService();
            _EligibilityService = EligibilityService ?? new EligibilityService(_IEmployerService.GetById(EmployerId));            
        }

        protected override CollectionResultSet<Policy> FulfillRequest(string CaseNumber)
        {

            var newCase = _CaseService.GetCaseByCaseNumber(CaseNumber);
            if (newCase == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, "Case not found for the given case number");
            }
            var getallmanualploicies = _EligibilityService.GetAvailableManualPolicies(newCase);
            return new CollectionResultSet<Policy>(getallmanualploicies);

        }

        protected override bool IsValid(string CaseNumber, out string message)
        {
            message = string.Empty;
            if (string.IsNullOrEmpty(CaseNumber))
            {
                message = "CaseNumber is required";
                return false;
            }
            return true;
        }
    }
}