﻿using AT.Api.Case.Models;

namespace AT.Api.Case.Requests
{
    public class ChangeCaseParameters
    {
        public string CaseNumber { get; set; }
        public ChangeCaseModel ChangeCase { get; set; }
    }
}