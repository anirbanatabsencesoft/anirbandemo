﻿using AbsenceSoft;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System;
using System.Linq;
using System.Net;
using BaseWorkScheduleModel = AT.Api.Case.Models.BaseWorkScheduleModel;

namespace AT.Api.Case.Requests.ChangeCase
{
    public class ChangeCaseRequest : ApiRequest<ChangeCaseParameters, ResultSet<string>>
    {
        private readonly ICaseService _CaseService = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="CaseService"></param>
        public ChangeCaseRequest(ICaseService CaseService = null)
        {
            _CaseService = CaseService ?? new CaseService();
        }

        protected override ResultSet<string> FulfillRequest(ChangeCaseParameters parameters)
        {
            string result = ChangeCaseRequestForCase(parameters);
            return (new ResultSet<string>(result, "Case Changed successfully"));
        }

        protected override bool IsValid(ChangeCaseParameters parameters, out string message)
        {
            message = string.Empty;
            if (string.IsNullOrEmpty(parameters.CaseNumber))
            {
                message = "CaseNumber is required";
                return false;
            }
            return true;

        }

        public string ChangeCaseRequestForCase(ChangeCaseParameters parameters)
        {
            try
            {
                var caseData = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);

                if (caseData == null)
                {
                    throw new ApiException(HttpStatusCode.NotFound, "Case not found for the given case number");
                }

                Schedule workSchedule = null;

                if (CaseType.Reduced==parameters.ChangeCase.CaseType &&  parameters.ChangeCase.WorkSchedule != null)
                {
                    workSchedule = ApplyToDataModel(parameters.ChangeCase.WorkSchedule);
                }
                 caseData = _CaseService.ChangeCase(caseData, parameters.ChangeCase.StartDate, parameters.ChangeCase.EndDate, parameters.ChangeCase.IsStartDateNew, parameters.ChangeCase.IsEndDateNew, parameters.ChangeCase.CaseType, parameters.ChangeCase.ModifyDecisions, parameters.ChangeCase.ModifyAccommodations, workSchedule);

                if (caseData.WorkRelated != null)
                {
                    caseData.WorkRelated.DaysAwayFromWork = _CaseService.CalculateDaysAwayFromWork(caseData);
                }
                caseData = _CaseService.UpdateCase(caseData);

                return caseData.CaseNumber;
            }
            catch (Exception ex)
            {
                throw new ApiException(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        public Schedule ApplyToDataModel(BaseWorkScheduleModel schedule)
        {
            Schedule ScheduleModel = new Schedule
            {
                ScheduleType = schedule.ScheduleType,
                StartDate = schedule.StartDate ?? DateTime.Now.ToMidnight(),
                EndDate = schedule.EndDate,
                Times = schedule.Times.Select(t => new Time()
                {
                    TotalMinutes = !string.IsNullOrEmpty(t.TotalMinutes) ? t.TotalMinutes.ParseFriendlyTime() : 0,
                    SampleDate = t.SampleDate
                }).ToList(),
            };

            return ScheduleModel;
        }

    }
}