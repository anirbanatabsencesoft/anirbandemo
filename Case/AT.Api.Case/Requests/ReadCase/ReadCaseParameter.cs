﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AT.Api.Case.Requests.ReadCase
{
    public class ReadCaseParameter
    {
        public string CaseNumber { get; set; }

    }
}