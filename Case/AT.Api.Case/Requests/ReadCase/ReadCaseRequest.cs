﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Models;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Net;

namespace AT.Api.Case.Requests.ReadCase
{
    /// <summary>
    /// Request to read case details by case number
    /// </summary>
    public class ReadCaseRequest : ApiRequest<string, ResultSet<CaseModel>>
    {

        private readonly ICaseService _CaseService = null;
        private readonly IAdminUserService _AdminUserService = null;

        public ReadCaseRequest(
            string userId,
            string employerId,
            string customerId,
            ICaseService CaseService = null,
            IAdminUserService adminUserService = null)
        {
            _AdminUserService = adminUserService ?? new AdminUserService();

            var user = _AdminUserService.GetUserById(userId);

            if (user == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Unable to find user");
            }

            _CaseService = CaseService ?? new CaseService(customerId, employerId, user);
        }

        protected override ResultSet<CaseModel> FulfillRequest(string caseNumber)
        {
            var caseData = _CaseService.GetCaseByCaseNumber(caseNumber);

            if (caseData == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, "Case not found for the given case number");
            }

            var caseModel = DataExtensions.CaseExtensions.ToCaseModel(caseData);

            return new ResultSet<CaseModel>(caseModel, "Case retrieved  successfully");
        }

        protected override bool IsValid(string caseNumber, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(caseNumber))
            {
                message = "CaseNumber is required";
                return false;
            }

            return true;
        }
    }
}