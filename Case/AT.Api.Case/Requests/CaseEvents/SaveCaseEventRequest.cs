﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Models.CaseEvents;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Collections.Generic;
using System.Net;

namespace AT.Api.Case.Requests.CaseEvents
{
    /// <summary>
    /// Create Case Events Request
    /// </summary>
    public class SaveCaseEventRequest : ApiRequest<SaveCaseEventParameters, ResultSet<string>>
    {
        private readonly ICaseService _caseService = null;
    
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="adminUserService"></param>
        /// <param name="caseService"></param>
        public SaveCaseEventRequest(
           
            ICaseService caseService = null)
        {
            _caseService = caseService ?? new CaseService();

           
        }
        protected override ResultSet<string> FulfillRequest(SaveCaseEventParameters parameters)
        {
            bool result = false;

            result = CreateCaseEvents(parameters.CaseEvents, parameters.CaseNumber);
            return (new ResultSet<string>(result.ToString(), "Case Events created successfully"));
        }
        public bool CreateCaseEvents(IEnumerable<SaveCaseEventModel> updateCaseEvents, string CaseNumber)
        {
            AbsenceSoft.Data.Cases.Case myCase = _caseService.GetCaseByCaseNumber(CaseNumber);

            foreach (var caseEvent in updateCaseEvents)
            {
                if (caseEvent.Delete)
                {
                    myCase.ClearCaseEvent(caseEvent.EventType);
                }
                else
                {
                    myCase.SetCaseEvent(caseEvent.EventType, caseEvent.EventDate);
                }
                _caseService.RunCalcs(myCase);
                _caseService.UpdateCase(myCase);

            }
            return true;
        }
        protected override bool IsValid(SaveCaseEventParameters parameters, out string message)
        {
            message = string.Empty;
            AbsenceSoft.Data.Cases.Case myCase = _caseService.GetCaseByCaseNumber(parameters.CaseNumber);

            if (string.IsNullOrEmpty(parameters.CaseNumber))
            {
                message = "Case Number is required";
                return false;
            }

            if (myCase == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Case not found");
            }
            if (parameters.CaseEvents == null)
            {
                message = "No Case Event data was provided";
                return false;
            }
            return true;
        }
    }
}