﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AT.Api.Case.Requests.CaseEvents
{
    /// <summary>
    /// Get Case Events Parameters
    /// </summary>
    public class GetCaseEventsParameters
    {
        /// <summary>
        /// Case Number for the request
        /// </summary>
        public string CaseNumber { get; set; }

    }
}