﻿using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Models.CaseEvents;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Requests.CaseEvents
{
    /// <summary>
    /// Get Case Events Request
    /// </summary>
    public class GetCaseEventsRequest : ApiRequest<GetCaseEventsParameters, IEnumerable<GetCaseEventModel>>
    {
        private readonly ICaseService CaseServices = null;
        
        /// <summary>
        /// Constructor
        /// </summary>        
        /// <param name="CaseService"></param>   
        public GetCaseEventsRequest(ICaseService CaseService = null)
        {
            CaseServices = CaseService ?? new CaseService();

        }
        protected override IEnumerable<GetCaseEventModel> FulfillRequest(GetCaseEventsParameters parameters)
        {
            var caseNumber = parameters.CaseNumber;
            Cases.Case myCase = CaseServices.GetCaseByCaseNumber(caseNumber);

            if (myCase == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Case not found with the given details");
            }

            return myCase.CaseEvents.Select(evt => new GetCaseEventModel
            {
                EventType = evt.EventType,
                StartDate = myCase.StartDate,
                EventDate = evt.EventDate
            }).ToList();
        }
        protected override bool IsValid(GetCaseEventsParameters parameters, out string message)
        {
            message = string.Empty;
        
            if (string.IsNullOrWhiteSpace(parameters.CaseNumber))
            {
                message = "Case Number is required";
                return false;
            }
            return true;
        }
    }
}



