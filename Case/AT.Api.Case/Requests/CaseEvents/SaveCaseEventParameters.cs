﻿using AT.Api.Case.Models.CaseEvents;
using System.Collections.Generic;

namespace AT.Api.Case.Requests.CaseEvents
{
    public class SaveCaseEventParameters
    {
        /// <summary>
        /// Case Number
        /// </summary>
        public string CaseNumber { get; set; }
        
        /// <summary>
        /// Save Case Event
        /// </summary>
        public List<SaveCaseEventModel> CaseEvents { get; set; }
    }
}