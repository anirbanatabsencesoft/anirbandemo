﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AbsenceSoft.Logic.RiskProfiles;
using AbsenceSoft.Logic.RiskProfiles.Contracts;
using AT.Api.Case.DataExtensions;
using AT.Api.Case.Models.RiskProfile;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System;
using System.Net;
using System.Threading.Tasks;

namespace AT.Api.Case.Requests.RiskProfile
{
    public class CalculateRiskProfileRequest : ApiRequest<CalculateRiskProfileRequestParameters, ResultSet<RiskProfileModel>>
    {
        private readonly ICaseService _caseService = null;
        private readonly IEmployerService _employerService = null;
        private readonly IRiskProfileService _riskProfileService = null;
        private readonly IAdminCustomerService _adminCustomerService = null;
        private readonly IAdminUserService _adminUserService = null;
        private readonly IEmployeeService _employeeService = null;

        /// <summary>
        /// Constructor for CalculateRiskProfileRequest
        /// </summary>
        /// <param name="caseService"></param>                
        /// <param name="employerService"></param>  
        /// <param name="riskProfileService"></param>
        public CalculateRiskProfileRequest(string customerId = null, string employerId = null, string userId = null, IAdminUserService adminUserService = null, IAdminCustomerService adminCustomerService = null, ICaseService caseService = null, IRiskProfileService riskProfileService = null, IEmployerService employerService = null, IEmployeeService employeeService = null)
        {
            _employeeService = employeeService ?? new EmployeeService();
            _caseService = caseService ?? new CaseService();            

            _adminCustomerService = adminCustomerService ?? new AdminCustomerService();
            var customer = _adminCustomerService.GetCustomer(customerId);          
           
            _employerService = employerService ?? new EmployerService();
            var employer = _employerService.GetById(employerId);                    

            _adminUserService = adminUserService ?? new AdminUserService();
            var user = _adminUserService.GetUserById(userId);           

            _riskProfileService = riskProfileService ?? new RiskProfileService(customer, employer, user);
        }
        /// <summary>
        /// Validate input parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool IsValid(CalculateRiskProfileRequestParameters parameters, out string message)
        {
            message = string.Empty;
            if (!string.IsNullOrEmpty(parameters.EmployerId))
            {
                Employer employer = _employerService.GetById(parameters.EmployerId);
                //Check if feature is enabled
                if (employer == null || !employer.HasFeature(Feature.RiskProfile))
                {
                    message = "Employer is not valid or risk profile feature is not enabled";
                    return false;
                }
            }

            if (!string.IsNullOrEmpty(parameters.CustomerId))
            {
                Customer customer = _adminCustomerService.GetCustomer(parameters.CustomerId);
                if (customer == null)
                {
                    message = "Customer is not found";
                    return false;
                }
            }

            if (!string.IsNullOrEmpty(parameters.UserId))
            {
                User user = _adminUserService.GetUserById(parameters.UserId);
                if (user == null)
                {
                    message = "User is not found";
                    return false;
                }
            }
            return true;
        }


        /// <summary>
        /// override abstrat method to fulfillasync request
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override ResultSet<RiskProfileModel> FulfillRequest(CalculateRiskProfileRequestParameters parameters)
        {

            string returnMessage = "";
            var riskProfileModel = new RiskProfileModel();
            try
            {
                riskProfileModel = CalculateRiskProfile(parameters);
            }
            catch (Exception ex)
            {
                throw new ApiException(HttpStatusCode.NotFound, ex.Message);
            }

            returnMessage = "Risk Profile Calculated successfully.";
            if (riskProfileModel == null)
            {
                if (string.IsNullOrEmpty(parameters.CaseNumber))
                    returnMessage = returnMessage + " No risk profile applied to this employee.";
                else
                    returnMessage = returnMessage + " No risk profile applied to this case.";
            }

            return (new ResultSet<RiskProfileModel>(riskProfileModel, returnMessage));
        }       

        
        /// <summary>
        /// Calculate risk profile by passing  caseId and employeeId 
        /// </summary>
        /// <param name="parameters"> CalculateRiskProfileRequestParameters parameters</param>     
        /// <returns>RiskProfileModel</returns>
        private RiskProfileModel CalculateRiskProfile(CalculateRiskProfileRequestParameters parameters)
        {
            string caseId = null;

            if(string.IsNullOrEmpty(parameters.EmployeeNumber))
            {
                throw new Exception("employee number was not provided");
            }

            AbsenceSoft.Data.Customers.Employee objEmployee = _employeeService.GetEmployeeByEmployeeNumber(parameters.EmployeeNumber, parameters.CustomerId, parameters.EmployerId);

            if (objEmployee == null)
            {
                throw new Exception("Employee not found");
            }

            AbsenceSoft.Data.Cases.Case objCase = _caseService.GetCaseByCaseNumber(parameters.CaseNumber);
            if (objCase != null)
            {
                caseId = objCase.Id;
            }

            RiskProfileModel riskModel = null;
            var calculatedProfile = _riskProfileService.CalculateAndSaveRiskProfile(objEmployee.Id, caseId);
            if (calculatedProfile.RiskProfile != null)
            {
                riskModel = RiskProfileExtensions.ToRiskProfileModel(calculatedProfile);
            }

            return riskModel;
        }
    }
}