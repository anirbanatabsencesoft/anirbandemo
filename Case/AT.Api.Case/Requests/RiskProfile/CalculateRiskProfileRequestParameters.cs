﻿using AT.Api.Case.Models.RiskProfile;

namespace AT.Api.Case.Requests.RiskProfile
{
    public class CalculateRiskProfileRequestParameters
    {
        /// <summary>
        /// Employee number
        /// </summary>
        public string EmployeeNumber { get; set; }
        /// <summary>
        /// caseNumber
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// Employer Id
        /// </summary>
        public string EmployerId { get; set; }

        /// <summary>
        /// Customer Id 
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// User id
        /// </summary>
        public string  UserId { get; set; }

        /// <summary>
        /// Risk Profile model used for calculate risk profile.
        /// </summary>
        public RiskProfileModel RiskProfileModel { get; set; }
    }
}