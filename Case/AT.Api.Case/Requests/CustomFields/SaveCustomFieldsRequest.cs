﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Api.Shared.Model.CustomFields;
using AT.Api.Shared.Utitilies;
using AT.Common.Core;
using System.Linq;
using System.Net;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Requests.CustomFields
{
    /// <summary>
    /// Request to save custom fields for case
    /// </summary>
    public class SaveCustomFieldsRequest : ApiRequest<SaveCustomFieldsParameters, ResultSet<string[]>>
    {

        private readonly ICaseService _CaseService = null;
        private readonly IEmployerService _EmployerService = null;
        private readonly IAdminUserService _AdminUserService = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="caseService"></param>
        /// <param name="employerService"></param>
        /// <param name="adminUserService"></param>
        public SaveCustomFieldsRequest(
           string userId,
           ICaseService caseService = null,
           IEmployerService employerService = null,
           IAdminUserService adminUserService = null)
        {
            if (string.IsNullOrWhiteSpace(userId))
            {
                throw new ApiException(HttpStatusCode.Unauthorized, "Unable to retrieve authorized user detail");
            }


            _CaseService = caseService ?? new CaseService();
            _AdminUserService = adminUserService ?? new AdminUserService();

            var user = _AdminUserService.GetUserById(userId);
            if (user == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Unable to find user");
            }

            _EmployerService = employerService ?? new EmployerService(user);
        }

        /// <summary>
        /// fulfill save case custom fields
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override ResultSet<string[]> FulfillRequest(SaveCustomFieldsParameters parameters)
        {
            var caseNumber = parameters.CaseNumber;
            var theCase = _CaseService.GetCaseByCaseNumber(caseNumber);

            if (theCase == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, $"Case was not found for {caseNumber}");
            }

            theCase = ApplyCustomFieldToEmployee(theCase, parameters.CustomFields);

            _CaseService.UpdateCase(theCase, null);

            var customFieldIds = theCase.CustomFields
                .Where(cf => parameters.CustomFields.Any(m => m.Code == cf.Code))
                .Select(cf => cf.Id)
                .ToArray();

            return new ResultSet<string[]>(customFieldIds, "Custom Fields saved successfully");
        }


        /// <summary>
        /// Validate the configurations for custom field
        /// Assign the custom field values to the case
        /// </summary>
        /// <param name="theCase"></param>        
        /// <param name="customFields"></param>        
        /// <returns></returns>
        private Cases.Case ApplyCustomFieldToEmployee(Cases.Case theCase, SaveCustomFieldModel[] customFields)
        {
            var configurationFields = _EmployerService.GetCustomFields(EntityTarget.Case, null);

            if (configurationFields == null || configurationFields.Count == 0)
            {
                throw new ApiException(HttpStatusCode.NoContent, "No configuration fields setting for case");
            }

            if (customFields.Select(p => p.Code).Except(configurationFields.Select(p => p.Code)).Any())
            {
                throw new ApiException(HttpStatusCode.NoContent, "Invalid custom fields configuration code");
            }

            var isValid = CustomFieldValidator.Validate(configurationFields, customFields);

            foreach (var field in customFields)
            {
                var index = configurationFields.FindIndex(cf => cf.Code == field.Code);
                configurationFields[index].SelectedValue = field.Value;
            }

            if (theCase.CustomFields != null)
            {
                configurationFields
                .Where(cf => theCase.CustomFields.Any(f => f.Code == cf.Code && f.SelectedValue != null) &&
                             customFields.Any(f => f.Code != cf.Code))
                .ToList()
                .ForEach(cf =>
                {
                    cf.SelectedValue = theCase.CustomFields.Where(f => f.Code == cf.Code).Select(f => f.SelectedValue).First();
                });

            }
            
            theCase.CustomFields = configurationFields;

            return theCase;
        }

        /// <summary>
        /// Validate the parameters to save the case custom field
        /// If parameters are not valid then set error message
        /// </summary>
        /// <param name="parameters"></param>        
        /// <param name="message"></param>        
        /// <returns></returns>
        protected override bool IsValid(SaveCustomFieldsParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }

            if (parameters.CustomFields == null ||
                parameters.CustomFields.Length == 0 ||
                parameters.CustomFields.Any(c => string.IsNullOrWhiteSpace(c.Code)))
            {
                message = "Invalid custom field code";
                return false;
            }

            return true;
        }
    }
}