﻿using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.DataExtensions;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Api.Shared.Model.CustomFields;
using AT.Common.Core;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace AT.Api.Case.Requests.CustomFields
{
    /// <summary>
    /// Request to get custom fields for case
    /// </summary>
    public class GetCustomFieldsRequest : ApiRequest<GetCustomFieldsParameters, CollectionResultSet<CustomFieldModel>>
    {
        private readonly ICaseService _CaseService = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="caseService"></param>
        public GetCustomFieldsRequest(ICaseService caseService = null)
        {
            _CaseService = caseService ?? new CaseService();
        }

        /// <summary>
        /// fulfill get case custom fields
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override CollectionResultSet<CustomFieldModel> FulfillRequest(GetCustomFieldsParameters parameters)
        {
            var caseNumber = parameters.CaseNumber;
            var theCase = _CaseService.GetCaseByCaseNumber(caseNumber);

            if (theCase == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, $"Case was not found for {caseNumber}");
            }

            var caseCustomFields = theCase.CustomFields;
            var customFields = new List<CustomFieldModel>();
            if (caseCustomFields == null)
            {
                return new CollectionResultSet<CustomFieldModel>(
                   customFields,
                   "No custom fields configuration for case",
                   true,
                   HttpStatusCode.NoContent);
            }

            if (!string.IsNullOrWhiteSpace(parameters.Code))
            {
                caseCustomFields = caseCustomFields.Where(c => c.Code == parameters.Code).ToList();
            }

            if (caseCustomFields.Count() == 0)
            {
                return new CollectionResultSet<CustomFieldModel>(
                   customFields,
                   "No custom fields configuration for case",
                   true,
                   HttpStatusCode.NoContent);
            }

            customFields = caseCustomFields.Select(cf => cf.ToCustomFieldModel()).ToList();
            return new CollectionResultSet<CustomFieldModel>(customFields);
        }


        /// <summary>
        /// Validate the parameters to get the case custom fields
        /// If parameters are not valid set error message
        /// </summary>
        /// <param name="parameters"></param>        
        /// <param name="message"></param>        
        /// <returns></returns>
        protected override bool IsValid(GetCustomFieldsParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }

            return true;
        }
    }
}