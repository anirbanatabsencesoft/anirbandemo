﻿namespace AT.Api.Case.Requests.CustomFields
{

    /// <summary>
    /// Get custom fields parameters
    /// </summary>
    public class GetCustomFieldsParameters
    {
        /// <summary>
        /// Case Number
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// Custom field code 
        /// </summary>
        public string Code { get; set; }
    }
}