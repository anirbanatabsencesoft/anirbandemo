﻿using AT.Api.Shared.Model.CustomFields;

namespace AT.Api.Case.Requests.CustomFields
{
    /// <summary>
    /// 
    /// </summary>
    public class SaveCustomFieldsParameters
    {
        /// <summary>
        /// Case Number
        /// </summary>
        public string CaseNumber { get; set; }


        /// <summary>
        /// Custom field model to case save custom fields
        /// </summary>
        public SaveCustomFieldModel[] CustomFields { get; set; }
    }
}