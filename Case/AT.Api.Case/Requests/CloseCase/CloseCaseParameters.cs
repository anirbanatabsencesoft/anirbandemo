﻿using AbsenceSoft.Data.Enums;
using System;

namespace AT.Api.Case.Requests.CloseCase
{
    public class CloseCaseParameters
    {
        public string CaseNumber { get; set; }
        public CaseClosureReason ClosureReason { get; set; }
        public String Description { get; set; }
    }
}