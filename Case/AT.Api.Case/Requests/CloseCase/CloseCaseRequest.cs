﻿using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Core.Request;
using AT.Common.Core;
using AT.Api.Core.Exceptions;
using System.Net;

namespace AT.Api.Case.Requests.CloseCase
{
    public class CloseCaseRequest : ApiRequest<CloseCaseParameters, ResultSetBase>
    {
        private readonly ICaseService _CaseService = null;

        public CloseCaseRequest(ICaseService caseService = null)
        {
            _CaseService = caseService ?? new CaseService();
        }

        protected override ResultSetBase FulfillRequest(CloseCaseParameters parameters)
        {
            var theCase = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);

            if (theCase == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, $"Case was not found for {parameters.CaseNumber}");
            }          

            _CaseService.CaseClosed(theCase, null, parameters.ClosureReason,null,null,null);            

            if (theCase == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, $"Error closing case for {parameters.CaseNumber}");
            }

            return new ResultSetBase("Case closed successfully");
        }

        protected override bool IsValid(CloseCaseParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }

            return true;
        }
    }
}