﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Case.DataExtensions;
using AT.Api.Case.Models.WorkRestriction;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System;
using System.Collections.Generic;
using System.Net;

namespace AT.Api.Case.Requests.WorkRestrictionTypes
{
    /// <summary>
    /// Get Work Restriction Request
    /// </summary>    
    public class GetWorkRestrictionTypeLookupRequest : ApiRequest<GetWorkRestrictionTypeLookupParameters, CollectionResultSet<WorkRestrictionTypeLookupModel>>
    {        
        private readonly IEmployerService _employerService = null;
        private readonly IDemandService _demandService = null;
        private readonly IAdminUserService _AdminUserService = null;
        private readonly User CurrentUser;

        /// <summary>
        /// Constructor
        /// </summary>        
        /// <param name="userId"></param>        
        /// <param name="employerService"></param>
        /// <param name="demandService"></param>
        /// <param name="adminUserService"></param>
        public GetWorkRestrictionTypeLookupRequest(IEmployerService employerService = null, IDemandService demandService = null, IAdminUserService adminUserService = null)
        {         
            _employerService = employerService ?? new EmployerService();
            _AdminUserService = adminUserService ?? new AdminUserService(); 
            _demandService = demandService ?? new DemandService(CurrentUser);
        }

        /// <summary>
        /// override abstrat method to fulfillasync request
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override CollectionResultSet<WorkRestrictionTypeLookupModel> FulfillRequest(GetWorkRestrictionTypeLookupParameters parameters)
        {
            return GetWorkRestrictionTypes(parameters.CustomerId, parameters.EmployerId);
        }

        /// <summary>
        /// Get Work Restriction Types
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="EmployerId"></param>
        /// <returns></returns>
        public CollectionResultSet<WorkRestrictionTypeLookupModel> GetWorkRestrictionTypes(string CustomerId, string EmployerId)
        {
            var workRestrictionTypesModels = new List<WorkRestrictionTypeLookupModel>();

            List<DemandType> demandTypes = _demandService.GetDemandTypes(CustomerId, EmployerId);

            if (demandTypes != null && demandTypes.Count > 0)
            {
                workRestrictionTypesModels = WorkRestrictionExtensions.ToWorkRestrictionTypeLookupModel(demandTypes);
            }

            return new CollectionResultSet<WorkRestrictionTypeLookupModel>(workRestrictionTypesModels);
        }

        /// <summary>
        /// Validate input parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool IsValid(GetWorkRestrictionTypeLookupParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrEmpty(parameters.CustomerId))
            {
                message = "Missing CustomerId";
                return false;
            }
            if (String.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Missing EmployerId";
                return false;
            }

            if (!string.IsNullOrEmpty(parameters.EmployerId))
            {
                Employer employer = _employerService.GetById(parameters.EmployerId);
                if (employer == null)
                {
                    message = "Employer is not valid.";
                    return false;
                }
            }
            return true;
        }

    }
}