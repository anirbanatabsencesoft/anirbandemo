﻿namespace AT.Api.Case.Requests.WorkRestrictionTypes
{
    /// <summary>
    /// GetWorkRestrictionTypeLookupParameters Request Parameters
    /// </summary>
    public class GetWorkRestrictionTypeLookupParameters
    {
        /// <summary>
        /// Id of the Customer
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// EmployerId
        /// </summary>
        public string EmployerId { get; set; }
    }
}