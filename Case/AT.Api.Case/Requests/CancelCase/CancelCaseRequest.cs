﻿using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Net;

namespace AT.Api.Case.Requests.CancelCase
{
    public class CancelCaseRequest : ApiRequest<CancelCaseParameters, ResultSetBase>
    {
        private readonly ICaseService _CaseService = null;

        public CancelCaseRequest(ICaseService caseService = null)
        {
            _CaseService = caseService ?? new CaseService();
        }

        protected override ResultSetBase FulfillRequest(CancelCaseParameters parameters)
        {
            var caseNumber = parameters.CaseNumber;
            var theCase = _CaseService.GetCaseByCaseNumber(caseNumber);

            if (theCase == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, $"Case was not found for {caseNumber}");
            }

            theCase = _CaseService.CancelCase(theCase.Id, parameters.Reason);

            _CaseService.UpdateCase(theCase);

            return new ResultSetBase("Case canceled successfully");
        }

        protected override bool IsValid(CancelCaseParameters parameters, out string message)
        {
            message = string.Empty;
            if (string.IsNullOrWhiteSpace(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }

            return true;
        }

    }
}