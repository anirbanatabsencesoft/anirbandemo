﻿using AbsenceSoft.Data.Enums;

namespace AT.Api.Case.Requests.CancelCase
{
    public class CancelCaseParameters
    {
        public string CaseNumber { get; set; }
        public CaseCancelReason Reason { get; set; }
    }
}