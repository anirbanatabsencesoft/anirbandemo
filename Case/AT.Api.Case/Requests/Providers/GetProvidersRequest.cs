﻿using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Case.DataExtensions;
using AT.Api.Case.Models.Providers;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AT.Api.Case.Requests.Providers
{
    /// <summary>
    /// Request to get contacts for case
    /// </summary>
    public class GetProvidersRequest : ApiRequest<GetProvidersParameters, CollectionResultSet<CaseProviderModel>>
    {
        private readonly ICaseService _CaseService = null;
        private readonly IEmployeeService _EmployeeService = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="caseService"></param>
        /// <param name="employeeService"></param>
        public GetProvidersRequest(
            ICaseService caseService = null,
            IEmployeeService employeeService = null)
        {
            _CaseService = caseService ?? new CaseService();
            _EmployeeService = employeeService ?? new EmployeeService();
        }

        /// <summary>
        /// fulfill get case contacts
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override CollectionResultSet<CaseProviderModel> FulfillRequest(GetProvidersParameters parameters)
        {
            var caseNumber = parameters.CaseNumber;
            var theCase = _CaseService.GetCaseByCaseNumber(caseNumber);

            if (theCase == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, $"Case was not found for {caseNumber}");
            }

            var providers = _EmployeeService.GetMedicalContacts(parameters.CustomerId, theCase.Employee.Id);
            if (providers == null || providers.Count == 0)
            {
                return new CollectionResultSet<CaseProviderModel>(
                    new List<CaseProviderModel>(),
                    "No providers found",
                    true,
                    HttpStatusCode.NoContent);
            }

            var providerModels = providers.Select(contact => contact.ToProviderModel());

            return new CollectionResultSet<CaseProviderModel>(providerModels);
        }

        /// <summary>
        /// Validate the parameters to get the contacts 
        /// If parameters are not valid then set error message
        /// </summary>
        /// <param name="parameters"></param>        
        /// <param name="message"></param>        
        /// <returns></returns>
        protected override bool IsValid(GetProvidersParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.CustomerId))
            {
                message = "CustomerId is required";
                return false;
            }

            return true;
        }
    }
}