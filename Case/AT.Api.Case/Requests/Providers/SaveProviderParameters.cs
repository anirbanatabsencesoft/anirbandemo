﻿using AT.Api.Case.Models.Providers;

namespace AT.Api.Case.Requests.Providers
{
    /// <summary>
    /// Save Case provider parameters
    /// </summary>
    public class SaveProviderParameters
    {

        /// <summary>
        /// Customer Id
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// Employer Id
        /// </summary>
        public string EmployerId { get; set; }

        /// <summary>
        /// Case Number
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// Case provider model
        /// </summary>
        public CaseProviderModel CaseProvider { get; set; }
    }
}