﻿using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Case.EntityFactories;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Linq;
using System.Net;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Case.Requests.Providers
{
    /// <summary>
    /// Request to get provider contacts for case
    /// </summary>
    public class SaveProviderRequest : ApiRequest<SaveProviderParameters, ResultSet<string>>
    {

        private readonly ICaseService _CaseService = null;
        private readonly IEmployeeService _EmployeeService = null;
        private readonly IContactTypeService _ContactTypeService = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="caseService"></param>
        /// <param name="employeeService"></param>
        /// <param name="contactTypeService"></param>
        public SaveProviderRequest(
           ICaseService caseService = null,
           IEmployeeService employeeService = null,
           IContactTypeService contactTypeService = null)
        {
            _CaseService = caseService ?? new CaseService();
            _EmployeeService = employeeService ?? new EmployeeService();
            _ContactTypeService = contactTypeService ?? new ContactTypeService();
        }

        /// <summary>
        /// Fulfill to save provider contact
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override ResultSet<string> FulfillRequest(SaveProviderParameters parameters)
        {            
            var caseNumber = parameters.CaseNumber;
            var theCase = _CaseService.GetCaseByCaseNumber(caseNumber);

            if (theCase == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, $"Case was not found for {caseNumber}");
            }

            var contactTypes = _ContactTypeService.GetMedicalContactTypes(parameters.EmployerId);
            var contactType = contactTypes.FirstOrDefault(ct => ct.Code == parameters.CaseProvider.ContactTypeCode);
            if (contactType == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Invalid contact type code");
            }

            var employeeId = theCase.Employee.Id;
            var empContact = new Customers.EmployeeContact();
            if (!string.IsNullOrEmpty(parameters.CaseProvider.ContactId))
            {
                empContact = _CaseService.GetCaseProviderContact(parameters.CaseProvider.ContactId);
            }

            empContact = ProviderFactory.FromEmployeeProviderModel(
                parameters.CaseProvider,
                parameters.EmployerId,
                parameters.CustomerId,
                employeeId,
                contactType.Name,
                empContact);

            var employeeContact = _EmployeeService.SaveContact(empContact);

            if (parameters.CaseProvider.IsPrimary)
            {
                _ContactTypeService.UpdateMedicalDesignationTypeContactsIsPrimary(parameters.CustomerId, empContact.Contact.Id, employeeId);
            }
            return new ResultSet<string>(employeeContact.Id, "Provider contact saved successfully");
        }

        /// <summary>
        /// Validate the parameters to save the case contact
        /// If parameters are not valid then set error message
        /// </summary>
        /// <param name="parameters"></param>        
        /// <param name="message"></param>        
        /// <returns></returns>
        protected override bool IsValid(SaveProviderParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }
            if (string.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Employer Id is required";
                return false;
            }
            if (string.IsNullOrWhiteSpace(parameters.CustomerId))
            {
                message = "Customer Id is required";
                return false;
            }
          
            return true;
        }
    }
}