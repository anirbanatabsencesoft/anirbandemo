﻿using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Net;
using System.Threading.Tasks;

namespace AT.Api.Case.Requests.Contacts
{
    /// <summary>
    /// Delete Provider Contact
    /// </summary>
    public class DeleteProviderRequest : ApiRequest<DeleteProviderParameters, ResultSet<string>>
    {
        private readonly IEmployeeService _EmployeeService = null;
        private readonly ICaseService _CaseService = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="employeeService"></param>
        /// <param name="caseService"></param>
        public DeleteProviderRequest(
           ICaseService caseService = null,
           IEmployeeService employeeService = null)
        {
            _EmployeeService = employeeService ?? new EmployeeService();
            _CaseService = caseService ?? new CaseService();
        }

        /// <summary>
        /// Fulfill to delete provider contact
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override ResultSet<string> FulfillRequest(DeleteProviderParameters parameters)
        {
            return DeleteCaseContact(parameters);
        }

        /// <summary>
        /// Check contact is exist 
        /// Delete the contact
        /// </summary>
        /// <param name="parameters"></param>        
        /// <returns></returns>
        private ResultSet<string> DeleteCaseContact(DeleteProviderParameters parameters)
        {
            var empContact = _CaseService.GetCaseProviderContact(parameters.ContactId);

            if (empContact == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "No case provider contact found");
            }

            _EmployeeService.DeleteContact(parameters.ContactId);

            return new ResultSet<string>(parameters.ContactId, "Contact deleted successfully");
        }

        /// <summary>
        /// Validate the parameters to delete the contact 
        /// if parameters are not valid then set error message
        /// </summary>
        /// <param name="parameters"></param>        
        /// <param name="message"></param>        
        /// <returns></returns>
        protected override bool IsValid(DeleteProviderParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrEmpty(parameters.ContactId))
            {
                message = "Contact Id is required";
                return false;
            }
            return true;
        }

    }
}