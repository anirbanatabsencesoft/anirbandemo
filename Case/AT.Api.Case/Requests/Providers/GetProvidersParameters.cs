﻿namespace AT.Api.Case.Requests.Providers
{
    /// <summary>
    /// Get Case Contact parameters
    /// </summary>
    public class GetProvidersParameters
    {
        /// <summary>
        /// Customer Id
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// Case Number
        /// </summary>
        public string CaseNumber { get; set; }
    }
}