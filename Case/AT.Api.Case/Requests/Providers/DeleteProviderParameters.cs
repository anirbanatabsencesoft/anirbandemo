﻿namespace AT.Api.Case.Requests.Contacts
{
    /// <summary>
    /// Delete provider contact parameters
    /// </summary>
    public class DeleteProviderParameters
    {
        /// <summary>
        /// Contact Id
        /// </summary>
        public string ContactId { get; set; }
    }
}