﻿using AbsenceSoft.Data.Enums;

namespace AT.Api.Case.Requests.CustomField
{
    public class CustomFieldParameter
    {
        public string employeeNumber { get; set; }
        public EntityTarget caseTarget { get; set; }
        public string CustomerId { get; set; }
        public string EmployerId { get; set; }
    }
}