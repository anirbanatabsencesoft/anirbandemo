﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Net;

namespace AT.Api.Case.Requests.CustomField
{
    public class GetCustomFieldRequest : ApiRequest<CustomFieldParameter, CollectionResultSet<AbsenceSoft.Data.Customers.CustomField>>
    {
        private readonly IAdminUserService _AdminUserService = null;
        private readonly IEmployerService _EmployerService = null;
        private readonly IEmployerService _IEmployerService = null;

        /// <summary>
        /// Constructor 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="AdminUserService"></param>
        public GetCustomFieldRequest(string userId, string employerId, string customerId, IEmployerService EmployerService = null, IAdminUserService AdminUserService = null)
        {
            _AdminUserService = AdminUserService ?? new AdminUserService();
            _IEmployerService = EmployerService ?? new EmployerService();
            var user = _AdminUserService.GetUserById(userId);
            var employer = _IEmployerService.GetById(employerId);
            var customer = _AdminUserService.GetCustomerById(customerId);
            if (user == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Unable to find user");
            }
            _EmployerService = EmployerService ?? new EmployerService(customer, employer, user);
        }

        protected override CollectionResultSet<AbsenceSoft.Data.Customers.CustomField> FulfillRequest(CustomFieldParameter parameters)
        {
            var customField = _EmployerService.GetCustomFields();
            return new CollectionResultSet<AbsenceSoft.Data.Customers.CustomField>(customField);
        }

        protected override bool IsValid(CustomFieldParameter parameters, out string message)
        {
            message = string.Empty;
            return true;
        }

    }
}