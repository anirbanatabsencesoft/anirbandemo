﻿namespace AT.Api.Case.Requests.CaseActivity
{
    /// <summary>
    /// Parameters for the request
    /// </summary>
    public class GetCaseActivityParameters
    {
        /// <summary>
        /// Case number
        /// </summary>
        public string CaseNumber { get; set; }
    }
}