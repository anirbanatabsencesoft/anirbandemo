﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Notes.Contracts;
using AbsenceSoft.Logic.Tasks;
using AbsenceSoft.Logic.Tasks.Contracts;
using AT.Api.Case.Models.CaseActivity;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using ATLogic = AbsenceSoft.Logic;

namespace AT.Api.Case.Requests.CaseActivity
{
    /// <summary>
    /// GetCaseActivity Request
    /// </summary>
    public class GetCaseActivityRequest : ApiRequest<GetCaseActivityParameters, CollectionResultSet<GetCaseActivityModel>>
    {
        private readonly IAdminUserService _AdminUserService = null;
        private readonly ICaseService _CaseService = null;
        private readonly IToDoService _ToDoService = null;
        private readonly ICommunicationService _CommunicationService = null;
        private readonly INotesService _NotesService = null;
        private readonly IAttachmentService _AttachmentService = null;
        private readonly User _CurrentUser = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="adminUserService"></param>
        /// <param name="caseService"></param>
        /// <param name="toDoService"></param>
        /// <param name="communicationService"></param>
        /// <param name="notesService"></param>
        /// <param name="attachmentService"></param>
        public GetCaseActivityRequest(
            string userId,
            IAdminUserService adminUserService = null,
            ICaseService caseService = null,
            IToDoService toDoService = null,
            ICommunicationService communicationService = null,
            INotesService notesService = null,
            IAttachmentService attachmentService = null)
        {
            _AdminUserService = adminUserService ?? new AdminUserService();
            _CurrentUser = _AdminUserService.GetUserById(userId);

            if (_CurrentUser == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Unable to find user");
            }

            _AdminUserService = adminUserService ?? new AdminUserService();
            _CaseService = caseService ?? new CaseService();
            _ToDoService = toDoService ?? new ToDoService();
            _NotesService = notesService ?? new NotesService(_CurrentUser);
            _CommunicationService = communicationService ?? new CommunicationService();
            _AttachmentService = attachmentService ?? new AttachmentService(_CurrentUser);
        }

        /// <summary>
        /// Fulfill Request
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override CollectionResultSet<GetCaseActivityModel> FulfillRequest(GetCaseActivityParameters parameters)
        {
            var result = GetCaseActivity(parameters);

            if (result == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Unable to find case activities");
            }

            return result;            
        }

        private CollectionResultSet<GetCaseActivityModel> GetCaseActivity(GetCaseActivityParameters parameters)
        {
            AbsenceSoft.Data.Cases.Case theCase = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);

            if (theCase == null)
                throw new ApiException(HttpStatusCode.ExpectationFailed, "Case not found");

            ATLogic.ListCriteria criteria = new ATLogic.ListCriteria();            
            criteria.ExtensionData.Add("CaseId", theCase.Id);
            criteria.ExtensionData.Add("Id", theCase.Id);

            ATLogic.ListResults attachments = new AttachmentService().AttachmentList(criteria);
            ATLogic.ListResults todos = new ToDoService().ToDoItemList(_CurrentUser, criteria);
            ATLogic.ListResults communications = new CommunicationService().CommunicationList(criteria);
            ATLogic.ListResults assignments = new CaseService().GetCaseAssignmentList(criteria);
            ATLogic.ListResults caseActivityResults = new NotesService().GetCaseNoteList(criteria);

            List<ATLogic.ListResult> caseActivity = caseActivityResults.Results.ToList();
            foreach (ATLogic.ListResult note in caseActivity)
            {
                note.ExtensionData["Type"] = "Note";
                note.ExtensionData["Description"] = note.ExtensionData.ContainsKey("Notes") ? note.ExtensionData["Notes"] : "";
            }

            var getCaseActivityModels = new List<GetCaseActivityModel>();
            var caseActivityModel = new GetCaseActivityModel();

            caseActivity.AddRange(assignments.Results.Select(a => new ATLogic.ListResult()
                .Set("CreatedDate", a.ExtensionData.ContainsKey("ModifiedDate") ? a.ExtensionData["ModifiedDate"] : "")
                .Set("Category", a.ExtensionData.ContainsKey("Category") ? a.ExtensionData["Category"] : "")
                .Set("CreatedBy", a.ExtensionData.ContainsKey("ModifiedBy") ? a.ExtensionData["ModifiedBy"] : "")
                .Set("Type", "Assignment")
                .Set("Description", a.ExtensionData.ContainsKey("Description") ? a.ExtensionData["Description"] : "")
                .Set("ModifiedDate", a.ExtensionData.ContainsKey("ModifiedDate") ? a.ExtensionData["ModifiedDate"] : "")
                .Set("StatusId", "")));

            caseActivity.AddRange(attachments.Results.Select(a => new ATLogic.ListResult()
                .Set("CreatedDate", a.ExtensionData.ContainsKey("CreatedDate") ? a.ExtensionData["CreatedDate"] : "")
                .Set("Category", a.ExtensionData.ContainsKey("AttachmentTypeName") ? a.ExtensionData["AttachmentTypeName"] : "")
                .Set("CreatedBy", a.ExtensionData.ContainsKey("AttachedBy") ? a.ExtensionData["AttachedBy"] : "")
                .Set("Type", "Attachment")
                .Set("Description", a.ExtensionData.ContainsKey("Description") ? a.ExtensionData["Description"] : "")
                .Set("ModifiedDate", "")
                .Set("StatusId", "")));

            caseActivity.AddRange(todos.Results.Where(w => w.ExtensionData.ContainsKey("StatusId") == false || Convert.ToInt64(w.ExtensionData["StatusId"]) != Convert.ToInt64(ToDoItemStatus.Cancelled)).Select(t => new ATLogic.ListResult()
                .Set("CreatedDate", t.ExtensionData.ContainsKey("CreatedDate") ? t.ExtensionData["CreatedDate"] : "")
                .Set("Category", "N/A")
                .Set("CreatedBy", t.ExtensionData.ContainsKey("ModifiedByName") ? t.ExtensionData["ModifiedByName"] : "")
                .Set("Type", "ToDo")
                .Set("Description", t.ExtensionData.ContainsKey("Title") ? t.ExtensionData["Title"] : "")
                .Set("CaseCloseReason", t.ExtensionData.ContainsKey("ReasonCause") ? t.ExtensionData["ReasonCause"] : "")
                .Set("ModifiedDate", t.ExtensionData.ContainsKey("ModifiedDate") ? t.ExtensionData["ModifiedDate"] : "")
                .Set("StatusId", t.ExtensionData.ContainsKey("StatusId") ? t.ExtensionData["StatusId"] : "")));

            caseActivity.AddRange(communications.Results.Select(c => new ATLogic.ListResult()
                .Set("CreatedDate", c.ExtensionData.ContainsKey("SentDate") ? c.ExtensionData["SentDate"] : "")
                .Set("Category", "N/A")
                .Set("CreatedBy", c.ExtensionData.ContainsKey("CreatedByName") ? c.ExtensionData["CreatedByName"] : "")
                .Set("Type", "Communication")
                .Set("Description", c.ExtensionData.ContainsKey("Subject") ? c.ExtensionData["Subject"] : "")
                .Set("ModifiedDate", "")
                .Set("StatusId", "")));

            caseActivityResults.Results = caseActivity;

            if (caseActivityResults.Results != null)
            {
                foreach (ATLogic.ListResult item in caseActivityResults.Results)
                {
                    caseActivityModel = new GetCaseActivityModel
                    {
                        Category = item.ExtensionData["Category"] != null ? item.ExtensionData["Category"].ToString() : string.Empty,
                        Description = item.ExtensionData["Description"] != null ? item.ExtensionData["Description"].ToString() : string.Empty,
                        Type = item.ExtensionData["Type"] != null ? item.ExtensionData["Type"].ToString() : string.Empty,
                        CreatedBy = item.ExtensionData["CreatedBy"] != null ? item.ExtensionData["CreatedBy"].ToString() : string.Empty,
                        CreatedDate = item.ExtensionData["CreatedDate"] != null ? item.ExtensionData["CreatedDate"].ToString() : string.Empty
                    };

                    getCaseActivityModels.Add(caseActivityModel);
                }
            }
            return new CollectionResultSet<GetCaseActivityModel>(getCaseActivityModels);
        }

        /// <summary>
        /// Validations
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool IsValid(GetCaseActivityParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.CaseNumber))
            {
                message = "Case number was not provided";
                return false;
            }

            return true;
        }
    }
}