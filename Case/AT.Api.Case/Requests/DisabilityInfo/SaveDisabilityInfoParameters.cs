﻿using AT.Api.Case.Models.DisabilityInfo;

namespace AT.Api.Case.Requests.DisabilityInfo
{
    public class SaveDisabilityInfoParameters
    {
        public string EmployerId { get; set; }
        public string CaseNumber { get; set; }
        public SaveDisabilityInfoModel SaveDisabilityInfo { get; set; }
    }
}