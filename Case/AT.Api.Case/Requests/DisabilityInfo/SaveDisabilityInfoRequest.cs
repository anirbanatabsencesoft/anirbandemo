﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Case.Models.DisabilityInfo;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Requests.DisabilityInfo
{
    /// <summary>
    /// Handles saving of disability info
    /// </summary>
    public class SaveDisabilityInfoRequest : ApiRequest<SaveDisabilityInfoParameters, ResultSetBase>
    {

        private readonly IDiagnosisGuidelinesService _GuidelineService = null;
        private readonly ICaseService _CaseService = null;
        private readonly IEmployerService _EmployerService = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="caseService"></param>        
        /// <param name="diagnosisGuidelinesService"></param>        
        /// <param name="employerService"></param>        
        public SaveDisabilityInfoRequest(
            ICaseService caseService = null,
            IDiagnosisGuidelinesService diagnosisGuidelinesService = null,
            IEmployerService employerService = null)
        {
            _GuidelineService = diagnosisGuidelinesService ?? new DiagnosisGuidelinesService();
            _CaseService = caseService ?? new CaseService();
            _EmployerService = employerService ?? new EmployerService();
        }

        /// <summary>
        /// Override abstract method to fulfillasync request
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override ResultSetBase FulfillRequest(SaveDisabilityInfoParameters parameters)
        {
            SaveDisabilityInfoData(parameters.CaseNumber, parameters.SaveDisabilityInfo);
            return new ResultSetBase("Disability information saved successfully");
        }

        /// <summary>
        /// Save DisabilityInfo data
        /// </summary>
        /// <param name="caseNumber"></param>
        /// <param name="model"></param>
        public void SaveDisabilityInfoData(string caseNumber, SaveDisabilityInfoModel model)
        {
            Cases.Case myCase = _CaseService.GetCaseByCaseNumber(caseNumber);

            if (myCase == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Case not found for the given case number");
            }

            if (myCase.Disability == null)
            {
                myCase.Disability = new Cases.DisabilityInfo();
            }

            myCase.Disability.ConditionStartDate = model.ConditionStartDate;
            myCase.Disability.GeneralHealthCondition = model.GeneralHealthCondition;
            myCase.Disability.Hospitalization = model.Hospitalization;
            myCase.Disability.MedicalComplications = model.MedicalComplications;
            myCase.Disability.EmployeeJobActivity = model.EmployeeJobActivity ?? (myCase.Employee?.JobActivity);

            if (!string.IsNullOrWhiteSpace(model.PrimaryDiagnosisCode))
            {
                var diagnosisCode = _GuidelineService.GetByCode(model.PrimaryDiagnosisCode);
                myCase.Disability.PrimaryDiagnosis = diagnosisCode;
            }

            List<DiagnosisCode> secondaryCodes = new List<DiagnosisCode>();

            if (model.SecondaryDiagnosisCodes != null)
            {
                myCase.Disability.SecondaryDiagnosis = model.SecondaryDiagnosisCodes.Select(dc => _GuidelineService.GetByCode(dc)).ToList();
            }

            if (myCase.Disability.CoMorbidityGuidelineDetail == null)
            {
                myCase.Disability.CoMorbidityGuidelineDetail = new Cases.CoMorbidityGuideline();
            }
          
            myCase.Disability.CoMorbidityGuidelineDetail.Args.BodyPartCode = model.BodyPartCode;           

            myCase.Disability.CoMorbidityGuidelineDetail.Args.CPTProcedureCodeString = model.CptpProcedureCodes;
            myCase.Disability.CoMorbidityGuidelineDetail.Args.HCPCSCodeString = model.HcpcsCodes;
            myCase.Disability.CoMorbidityGuidelineDetail.Args.ICDProcedureCodeString = model.IcdProcedureCodes;
            myCase.Disability.CoMorbidityGuidelineDetail.Args.NDCProcedureCodeString = model.NdcProcedureCodes;

            myCase.Disability.CoMorbidityGuidelineDetail.Args.CF.Depression = Convert.ToInt16(model.Depression);
            myCase.Disability.CoMorbidityGuidelineDetail.Args.CF.Diabetes = Convert.ToInt16(model.Diabetes);
            myCase.Disability.CoMorbidityGuidelineDetail.Args.CF.Hypertension = Convert.ToInt16(model.Hypertension);
            myCase.Disability.CoMorbidityGuidelineDetail.Args.CF.Obesity = Convert.ToInt16(model.Obesity);
            myCase.Disability.CoMorbidityGuidelineDetail.Args.CF.Smoker = Convert.ToInt16(model.Smoker);
            myCase.Disability.CoMorbidityGuidelineDetail.Args.CF.SubstanceAbuse = Convert.ToInt16(model.SubstanceAbuse);
            myCase.Disability.CoMorbidityGuidelineDetail.Args.CF.SurgeryOrHospitalStay = Convert.ToInt16(model.SurgeryOrHospitalStay);
            myCase.Disability.CoMorbidityGuidelineDetail.Args.CF.OPIOId = Convert.ToInt16(model.Opioid);
            myCase.Disability.CoMorbidityGuidelineDetail.Args.CF.LegalRepresentation = Convert.ToInt16(model.LegalRepresentation);

            var updatedCase = _CaseService.UpdateCase(myCase);

            if (updatedCase == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Unable to save disability info");
            }

            myCase.Disability.CoMorbidityGuidelineDetail.Args.PS.CPTProcedureCodeString = model.CptpProcedureCodes;
            myCase.Disability.CoMorbidityGuidelineDetail.Args.PS.HCPCSCodeString = model.HcpcsCodes;
            myCase.Disability.CoMorbidityGuidelineDetail.Args.PS.ICDProcedureCodeString = model.IcdProcedureCodes;
            myCase.Disability.CoMorbidityGuidelineDetail.Args.PS.NDCProcedureCodeString = model.NdcProcedureCodes;

            myCase.Disability.CoMorbidityGuidelineDetail.Args.NatureOfInjuryCode = model.NatureOfInjuryCode;

            // This also saves some information
            var morbidityGuildeline = _GuidelineService.GetCoMorbidityGuideline(myCase.Id, myCase.Disability.CoMorbidityGuidelineDetail.Args);

            if (morbidityGuildeline == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Unable to save disability info");
            }
        }


        /// <summary>
        /// Validate input parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool IsValid(SaveDisabilityInfoParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrEmpty(parameters.CaseNumber))
            {
                message = "Case Number is required";
                return false;
            }

            if (String.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Employer Id is required";
                return false;
            }       
            return true;
        }
    }
}