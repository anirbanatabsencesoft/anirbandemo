﻿
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AbsenceSoft.Models.Cases;
using AT.Api.Case.DataExtensions;
using AT.Api.Case.Models.DisabilityInfo;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System;
using System.Collections.Generic;
using System.Net;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Requests.DisabilityInfo
{
    /// <summary>
    /// 
    /// </summary>
    public class SetReturnToWorkPathRequest : ApiRequest<SetReturnToWorkPathParameters, ResultSet<ReturnToWorkPathModel>>
    {

        private readonly IDiagnosisGuidelinesService _GuidelineService = null;
        private readonly ICaseService _CaseService = null;
        private readonly IEmployerService _EmployerService = null;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="caseService"></param>        
        /// <param name="service"></param>        
        /// <param name="employerService"></param>        
        public SetReturnToWorkPathRequest(ICaseService caseService = null, IDiagnosisGuidelinesService service = null, IEmployerService employerService = null)
        {
            _GuidelineService = service ?? new DiagnosisGuidelinesService();
            _CaseService = caseService ?? new CaseService();
            _EmployerService = employerService ?? new EmployerService();
        }

        /// <summary>
        /// Set RTW Guidelines Data
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override ResultSet<ReturnToWorkPathModel> FulfillRequest(SetReturnToWorkPathParameters parameters)
        {
            Cases.Case existingCase = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);
            if (existingCase == null)
                throw new ApiException(HttpStatusCode.ExpectationFailed, "Case not found with the given details");

            string diagnosisCode = (existingCase.Disability != null && existingCase.Disability.PrimaryDiagnosis != null) ? existingCase.Disability.PrimaryDiagnosis.Code : null;

            if (string.IsNullOrWhiteSpace(diagnosisCode))
            {
                throw new ApiException(HttpStatusCode.ExpectationFailed, "Primary diagnosis code was not found for the provided case");
            }

            DiagnosisGuidlinesModel model = new DiagnosisGuidlinesModel();

            model.Guidelines = _GuidelineService.GetDiagnosisGuidelines(diagnosisCode);

            if (model.Guidelines != null && model.Guidelines.RtwBestPractices != null && model.Guidelines.RtwBestPractices.Count > 0)
            {
                //AbsenceSoft.Data.Cases.RtwBestPractice rtw = model.Guidelines.RtwBestPractices.Find(o => o.RtwPath.Trim().ToLower() == PrimaryPathId.Trim().ToLower());
                AbsenceSoft.Data.Cases.RtwBestPractice rtw = model.Guidelines.RtwBestPractices.Find(o => o.Id == new Guid(parameters.PrimaryPathId));
                if (rtw != null && !string.IsNullOrWhiteSpace(Convert.ToString(rtw.Id)))
                {
                    existingCase.Disability.PrimaryPathId = rtw.Id.ToString();
                    existingCase.Disability.PrimaryPathDaysUIText = string.Format("{0} {1}", rtw.RtwPath, rtw.Days); ;
                    existingCase.Disability.PrimaryPathText = rtw.RtwPath;
                    if (rtw.MaxDays > 0)
                        existingCase.Disability.PrimaryPathMaxDays = Convert.ToInt32(rtw.MaxDays);
                    else
                        existingCase.Disability.PrimaryPathMaxDays = null;

                    if (rtw.MinDays > 0)
                        existingCase.Disability.PrimaryPathMinDays = Convert.ToInt32(rtw.MinDays);
                    else
                        existingCase.Disability.PrimaryPathMinDays = null;

                    existingCase = _CaseService.UpdateCase(existingCase); // save primary return to work path
                }
            }

            ReturnToWorkPathModel returnToWorkPathModel = DisabilityInfoExtensions.ToReturnToWorkPathModel(model.Guidelines);
            returnToWorkPathModel.RtwBestPractices = new List<RtwBestPractice>(0) { returnToWorkPathModel.RtwBestPractices.Find(o => o.Id == existingCase.Disability.PrimaryPathId) };
            return (new ResultSet<ReturnToWorkPathModel>(returnToWorkPathModel, "Primary return to work path updated"));
        }

        /// <summary>
        /// Validate input parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool IsValid(SetReturnToWorkPathParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.PrimaryPathId))
            {
                message = "Primary return to work path was not provided";
                return false;
            }

            if (!string.IsNullOrEmpty(parameters.EmployerId))
            {
                Employer employer = _EmployerService.GetById(parameters.EmployerId);
                //Check if feature is enabled
                if (employer == null || !employer.HasFeature(Feature.GuidelinesData))
                {
                    message = "Employer is not valid or guidelines data feature is not enabled.";
                    return false;
                }
            }

            return true;
        }

    }
}