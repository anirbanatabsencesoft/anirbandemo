﻿namespace AT.Api.Case.Requests.DisabilityInfo
{
    /// <summary>
    /// SetReturnToWorkPathParameters
    /// </summary>
    public class SetReturnToWorkPathParameters
    {
        /// <summary>
        /// caseNumber
        /// </summary>
        public string CaseNumber { get; set; }
        /// <summary>
        /// primary return to work path id/Text
        /// </summary>
        public string PrimaryPathId { get; set; }

        /// <summary>
        /// EmployerId
        /// </summary>
        public string EmployerId { get; set; }
    }
}