﻿
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AbsenceSoft.Models.Cases;
using AT.Api.Case.DataExtensions;
using AT.Api.Case.Models.DisabilityInfo;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using Cases = AbsenceSoft.Data.Cases;
using System.Collections.Generic;
using System.Net;

namespace AT.Api.Case.Requests.DisabilityInfo
{
    /// <summary>
    /// 
    /// </summary>
    public class GetReturnToWorkPathRequest : ApiRequest<GetReturnToWorkPathParameters, ResultSet<ReturnToWorkPathModel>>
    {

        private readonly IDiagnosisGuidelinesService _GuidelineService = null;
        private readonly ICaseService _CaseService = null;
        private readonly IEmployerService _EmployerService = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="caseService"></param>        
        /// <param name="service"></param>  
        /// <param name="employerService"></param>  
        public GetReturnToWorkPathRequest(ICaseService caseService = null, IDiagnosisGuidelinesService service = null, IEmployerService employerService = null)
        {
            _GuidelineService = service ?? new DiagnosisGuidelinesService();
            _CaseService = caseService ?? new CaseService();
            _EmployerService = employerService ?? new EmployerService();
        }

        /// <summary>
        /// Get RTW Guidelines Data
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override ResultSet<ReturnToWorkPathModel> FulfillRequest(GetReturnToWorkPathParameters parameters)
        {
            Cases.Case existingCase = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);
            if (existingCase == null)
            {
                throw new ApiException(HttpStatusCode.ExpectationFailed, "Case not found with the given details");
            }

            if (string.IsNullOrWhiteSpace(parameters.DiagnosisCode))
            {
                parameters.DiagnosisCode = (existingCase.Disability != null && existingCase.Disability.PrimaryDiagnosis != null) ? existingCase.Disability.PrimaryDiagnosis.Code : null;

                if (string.IsNullOrWhiteSpace(parameters.DiagnosisCode))
                {
                    throw new ApiException(HttpStatusCode.ExpectationFailed, "Primary diagnosis code was not found for the provided case");
                }
            }

            if (string.IsNullOrWhiteSpace(existingCase.Disability.PrimaryPathId))
            {
                throw new ApiException(HttpStatusCode.ExpectationFailed, "Primary return to work path was not found for the provided case");
            }

            DiagnosisGuidlinesModel model = new DiagnosisGuidlinesModel();
            model.Guidelines = _GuidelineService.GetDiagnosisGuidelines(parameters.DiagnosisCode);
            if (model.Guidelines == null)
            {
                return new ResultSet<ReturnToWorkPathModel>(null, "No guidelines data found");
            }
            ReturnToWorkPathModel returnToWorkPathModel = DisabilityInfoExtensions.ToReturnToWorkPathModel(model.Guidelines);
            returnToWorkPathModel.RtwBestPractices = new List<RtwBestPractice>(0) { returnToWorkPathModel.RtwBestPractices.Find(o => o.Id == existingCase.Disability.PrimaryPathId) };
            return (new ResultSet<ReturnToWorkPathModel>(returnToWorkPathModel, "Guidelines data retrieved"));
        }

        /// <summary>
        /// Validate input parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool IsValid(GetReturnToWorkPathParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrEmpty(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }

            if (!string.IsNullOrEmpty(parameters.EmployerId))
            {
                Employer employer = _EmployerService.GetById(parameters.EmployerId);

                //Check if feature is enabled
                if (employer == null || !employer.HasFeature(Feature.GuidelinesData))
                {
                    message = "Employer is not valid or guidelines data feature is not enabled.";
                    return false;
                }
            }

            return true;
        }
    }
}