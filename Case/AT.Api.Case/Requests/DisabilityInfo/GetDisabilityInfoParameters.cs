﻿namespace AT.Api.Case.Requests.DisabilityInfo
{
    public class GetDisabilityInfoParameters
    {
        /// <summary>
        /// The unique number for case
        /// </summary>
        public string CaseNumber { get; set; }


        /// <summary>
        /// Unique id of the employer
        /// </summary>
        public string EmployerId { get; set; }
    }
}