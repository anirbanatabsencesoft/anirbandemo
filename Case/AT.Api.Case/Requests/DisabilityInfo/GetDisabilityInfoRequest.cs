﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Case.DataExtensions;
using AT.Api.Case.Models.DisabilityInfo;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System;
using System.Net;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Requests.DisabilityInfo
{
    /// <summary>
    /// Handles the retrieval of disability information
    /// </summary>
    public class GetDisabilityInfoRequest : ApiRequest<GetDisabilityInfoParameters, ResultSet<GetDisabilityInfoModel>>
    {

        private readonly IDiagnosisGuidelinesService _GuidelinesService = null;
        private readonly ICaseService _CaseService = null;
        private readonly IEmployerService _EmployerService = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="caseService"></param>        
        /// <param name="service"></param>  
        /// <param name="employerService"></param>  
        public GetDisabilityInfoRequest(
            ICaseService caseService = null,
            IDiagnosisGuidelinesService service = null,
            IEmployerService employerService = null)
        {
            _GuidelinesService = service ?? new DiagnosisGuidelinesService();
            _CaseService = caseService ?? new CaseService();
            _EmployerService = employerService ?? new EmployerService();
        }

        /// <summary>
        /// Override abstract method to fulfill request
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override ResultSet<GetDisabilityInfoModel> FulfillRequest(GetDisabilityInfoParameters parameters)
        {
            var disabilityInfo = GetDisabilityInfo(parameters.CaseNumber, parameters.EmployerId);
            return new ResultSet<GetDisabilityInfoModel>(disabilityInfo, "Get Disability Info successful");
        }


        /// <summary>
        /// Get DisabilityInfo data
        /// </summary>
        /// <param name="caseNumber"></param>
        /// <param name="employerId"></param>
        /// <returns></returns>
        private GetDisabilityInfoModel GetDisabilityInfo(string caseNumber, string employerId)
        {
            Cases.Case existingCase = _CaseService.GetCaseByCaseNumber(caseNumber);

            if (existingCase == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Case not found for then given case number");
            }

            if (existingCase.Disability == null)
            {
                existingCase.Disability = new Cases.DisabilityInfo();
            }

            var guidlinesModel = new GetDisabilityInfoModel
            {
                PrimaryDiagnosis = existingCase.Disability.PrimaryDiagnosis != null
                    ? DisabilityInfoExtensions.ToDiagnosisCode(existingCase.Disability.PrimaryDiagnosis)
                    : new DiagnosisCodeModel(),

                SecondaryDiagnosis = DisabilityInfoExtensions.ToDiagnosisCode(existingCase.Disability.SecondaryDiagnosis),

                PrimaryPathId = existingCase.Disability.PrimaryPathId,
                PrimaryPathText = existingCase.Disability.PrimaryPathText,
                PrimaryPathMinDays = existingCase.Disability.PrimaryPathMinDays.HasValue ? existingCase.Disability.PrimaryPathMinDays.ToString() : null,
                PrimaryPathMaxDays = existingCase.Disability.PrimaryPathMaxDays.HasValue ? existingCase.Disability.PrimaryPathMaxDays.ToString() : null,
                PrimaryPathDaysUIText = existingCase.Disability.PrimaryPathDaysUIText
            };

            var resultCoMorbidityGuidline = new Cases.CoMorbidityGuideline();
            resultCoMorbidityGuidline = _GuidelinesService.GetCoMorbidityGuideline(
                existingCase.Id,
                existingCase.Disability.CoMorbidityGuidelineDetail.Args);

            guidlinesModel.CoMorbidityGuidelines = resultCoMorbidityGuidline != null
                ? DisabilityInfoExtensions.ToCoMorbidityGuideline(resultCoMorbidityGuidline)
                : new CoMorbidityGuideline();

            return guidlinesModel;
        }

        /// <summary>
        /// Validate input parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool IsValid(GetDisabilityInfoParameters parameters, out string message)
        {
            message = string.Empty;
            if (string.IsNullOrEmpty(parameters.CaseNumber))
            {
                message = "Case number was not provided";
                return false;
            }

            if (String.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Missing employerId";
                return false;
            }

            var employer = _EmployerService.GetById(parameters.EmployerId);

            if (employer == null || !employer.HasFeature(Feature.GuidelinesData))
            {
                message = "Employer is not valid or guidelines data feature is not enabled.";
                return false;
            }

            return true;
        }
    }
}