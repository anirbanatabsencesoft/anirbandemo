﻿namespace AT.Api.Case.Requests.DisabilityInfo
{
    /// <summary>
    /// GetReturnToWorkPathParameters
    /// </summary>
    public class GetReturnToWorkPathParameters
    {
        /// <summary>
        /// caseNumber
        /// </summary>
        public string CaseNumber { get; set; }
        /// <summary>
        /// DiagnosisCode
        /// </summary>
        public string DiagnosisCode { get; set; }

        /// <summary>
        /// EmployerId
        /// </summary>
        public string EmployerId { get; set; }
    }
}