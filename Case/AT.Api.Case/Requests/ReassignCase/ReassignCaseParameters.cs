﻿namespace AT.Api.Case.Requests.ReassignCase
{
    public class ReassignCaseParameters
    {
        public string AssigneeUserEmail { get; set; }
        public string AssigneeTypeCode { get; set; }
        public string CaseNumber { get; set; }
    }
}