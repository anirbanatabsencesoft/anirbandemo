﻿using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Administration.Contracts;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Tasks;
using AbsenceSoft.Logic.Tasks.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Net;

namespace AT.Api.Case.Requests.ReassignCase
{
    public class ReassignCaseRequest : ApiRequest<ReassignCaseParameters, ResultSetBase>
    {
        private readonly IAdministrationService _AdministrationService = null;
        private readonly ICaseService _CaseService = null;
        private readonly IToDoService _ToDoService = null;

        public ReassignCaseRequest(IAdministrationService administrationService = null,
            ICaseService caseService = null,
            IToDoService toDoService = null)
        {
            _AdministrationService = administrationService ?? new AdministrationService();
            _CaseService = caseService ?? new CaseService();
            _ToDoService = toDoService ?? new ToDoService();
        }

        protected override ResultSetBase FulfillRequest(ReassignCaseParameters parameters)
        {
            var user = _AdministrationService.GetUserByEmail(parameters.AssigneeUserEmail);

            if (user == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "User was not found or no valid email was supplied");
            }

            var reassignCase = _CaseService.GetCaseByCaseNumber(parameters.CaseNumber);

            if (reassignCase == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, $"Case was not found for {parameters.CaseNumber}");
            }

            if (string.IsNullOrWhiteSpace(parameters.AssigneeTypeCode))
            {
                parameters.AssigneeTypeCode = null;
            }

            _CaseService.ReassignCase(reassignCase, user, parameters.AssigneeTypeCode);

            return new ResultSetBase("Case assigned to user successfully");
        }

        protected override bool IsValid(ReassignCaseParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.AssigneeUserEmail))
            {
                message = "Assignee user email is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.CaseNumber))
            {
                message = "Case number is required";
                return false;
            }

            return true;
        }

    }
}