﻿using System.Web.Http;
using AT.Api.Core;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(AT.Api.Case.Startup))]

namespace AT.Api.Case
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var configuration = new HttpConfiguration();

            SwaggerConfig.Register(configuration);

            WebApiConfig.Register(configuration);
            app.UseWebApi(configuration);

            app.UseOAuthAuthorizationServer(AuthManager.BuildAuthOptions());

            configuration.EnsureInitialized();
        }
    }
}
