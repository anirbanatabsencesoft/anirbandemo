﻿using AT.Api.Case.Models.CaseNotes;
using AT.Api.Case.Requests.CaseNotes;
using AT.Api.Core;
using System.Web.Http;

namespace AT.Api.Case.Controllers
{
    /// <summary>
    /// Case Note List Controller
    /// </summary>
    [RoutePrefix("api/v1/cases/{caseNumber}/notes")]
    public class NoteListController : BaseApiController
    {
        /// <summary>
        /// CreateNote against caseNumber
        /// </summary>
        /// <param name="caseNumber"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        public IHttpActionResult CreateNote(string caseNumber, CaseNoteModel model)
        {
            var parameters = new CreateNoteParameters
            {
                CaseNumber = caseNumber,
                CaseNoteModel = model
            };

            var request = new CreateNoteRequest(AuthenticatedUser.Key);
            return Ok(request.Handle(parameters));

        }


        /// <summary>
        /// CreateNote against caseNumber
        /// </summary>
        /// <param name="caseNumber"></param>        
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public IHttpActionResult GetCaseNoteList(string caseNumber)
        {
            var parameters = new GetNoteListParameters
            {
                CaseNumber = caseNumber
            };

            var request = new GetNoteListRequest(AuthenticatedUser.Key);
            return Ok(request.Handle(parameters));

        }
    }
}
