﻿using AT.Api.Case.Requests.CaseRelatedContact;
using AT.Api.Core;
using System.Web.Http;

namespace AT.Api.Case.Controllers
{
    /// <summary>
    /// Api for Case Related Contact
    /// </summary>
    [RoutePrefix("api/v1/cases/{caseNumber}/relatedperson")]
    public class CaseRelatedContactController : BaseApiController
    {
        /// <summary>
        /// Get Case Related Person Contact
        /// </summary>
        /// <param name="caseNumber"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public IHttpActionResult GetCaseRelatedPerson(string caseNumber)
        {
            var request = new GetCaseRelatedContactRequest();

            var result = request.Handle(new GetCaseRelatedContactParameters
            {
                CaseNumber = caseNumber
            });

            return Ok(result);
        }
    }
}