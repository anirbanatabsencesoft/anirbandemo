﻿using AT.Api.Case.Models.DisabilityInfo;
using AT.Api.Case.Requests.DisabilityInfo;
using AT.Api.Core;
using System.Web.Http;


namespace AT.Api.Case.Controllers
{
    /// <summary>
    /// Handles operations related to disability info
    /// </summary>
    [RoutePrefix("api/v1/cases/{caseNumber}")]
    public class DisabilityInfoController : BaseApiController
    {
        /// <summary>
        /// Save Disability Info
        /// </summary>
        /// <param name="caseNumber">Unique number for case</param>
        /// <param name="model">Data for the disability information</param>
        /// <returns></returns>
        [HttpPost]
        [Route("disabilityInfo")]
        public IHttpActionResult SaveDisabilityInfoAsync(string caseNumber, SaveDisabilityInfoModel model)
        {
            var parameters = new SaveDisabilityInfoParameters
            {
                EmployerId = EmployerId,
                CaseNumber = caseNumber,
                SaveDisabilityInfo = model
            };

            var request = new SaveDisabilityInfoRequest();
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// API method to get return to work path for CaseNumber pass optionally diagnosisCode, employerReferenceCode to get return to work path
        /// if employerReferenceCode is not sent the default employer for the authenticated user will be used.
        /// if diagnosisCode is not provided then code of primary diagnosis would get used
        /// </summary>
        /// <param name="caseNumber"></param>
        /// <param name="diagnosisCode"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("returnToWorkPath/{diagnosisCode?}")]
        public IHttpActionResult GetReturnToWorkPathGuidelinesData(string caseNumber, string diagnosisCode)
        {
            var parameters = new GetReturnToWorkPathParameters
            {
                CaseNumber = caseNumber,
                DiagnosisCode = diagnosisCode,
                EmployerId = EmployerId
            };

            var request = new GetReturnToWorkPathRequest();
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// API method to set return to work path for CaseNumber pass PrimaryPathId and optionally employerReferenceCode to set return to work path
        /// if employerReferenceCode is not sent the default employer for the authenticated user will be used.        
        /// </summary>
        /// <param name="caseNumber"></param>
        /// <param name="primaryPathId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("returnToWorkPath/{primaryPathId}")]
        public IHttpActionResult SetReturnToWorkPathGuidelinesData(string caseNumber, string primaryPathId)
        {
            var parameters = new SetReturnToWorkPathParameters
            {
                CaseNumber = caseNumber,
                PrimaryPathId = primaryPathId,
                EmployerId = EmployerId
            };

            var request = new SetReturnToWorkPathRequest();
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// Returns the disability information based on case
        /// </summary>
        /// <param name="caseNumber">The unique case number</param>
        /// <returns>The disability information</returns>
        [HttpGet]
        [Route("disabilityInfo")]
        public IHttpActionResult GetDisabilityInfoAsync(string caseNumber)
        {
            var parameters = new GetDisabilityInfoParameters
            {
                CaseNumber = caseNumber,
                EmployerId = EmployerId
            };

            var request = new GetDisabilityInfoRequest();
            return Ok(request.Handle(parameters));
        }

    }
}