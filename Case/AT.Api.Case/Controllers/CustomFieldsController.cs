﻿using AT.Api.Case.Requests.CustomFields;
using AT.Api.Core;
using AT.Api.Shared.Model.CustomFields;
using System.Web.Http;

namespace AT.Api.Case.Controllers
{
    [RoutePrefix("api/v1/cases/{caseNumber}/customfields")]
    public class CustomFieldsController : BaseApiController
    {
        /// <summary>
        /// Get case custom fields
        /// </summary>
        /// <param name="caseNumber"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public IHttpActionResult Get(string caseNumber)
        {
            var request = new GetCustomFieldsRequest();

            var result = request.Handle(new GetCustomFieldsParameters
            {
                CaseNumber = caseNumber
            });

            return Ok(result);
        }

        /// <summary>
        /// Get case custom fields by code
        /// </summary>
        /// <param name="caseNumber"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("code/{code}")]
        public IHttpActionResult Get(string caseNumber, string code)
        {
            var request = new GetCustomFieldsRequest();

            var result = request.Handle(new GetCustomFieldsParameters
            {
                CaseNumber = caseNumber,
                Code = code
            });

            return Ok(result);
        }

        /// <summary>
        /// Save case custom fields
        /// </summary>
        /// <param name="caseNumber"></param>
        /// <param name="models"></param>
        /// <returns></returns>

        [HttpPost]
        [Route("")]
        public IHttpActionResult Save(string caseNumber, SaveCustomFieldModel[] models)
        {
            var request = new SaveCustomFieldsRequest(AuthenticatedUser.Key);

            var result = request.Handle(new SaveCustomFieldsParameters
            {
                CustomFields = models,
                CaseNumber = caseNumber
            });

            return Ok(result);
        }

    }
}
