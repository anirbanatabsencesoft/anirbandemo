﻿using AT.Api.Case.Requests.RiskProfile;
using AT.Api.Core;
using System.Web.Http;

namespace AT.Api.Case.Controllers
{
    [RoutePrefix("api/v1/employees/{employeeNumber}")]
    public class RiskProfileController : BaseApiController
    {
        /// <summary>
        /// API method to get risk profile by passing employee number and case number      
        /// </summary>      
        /// <param name="employeeNumber"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public IHttpActionResult GetRiskProfile(string employeeNumber)
        {
            var parameters = new GetRiskProfileRequestParameters
            {
                EmployerId = EmployerId,
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployeeNumber = employeeNumber
            };

            var request = new GetRiskProfileRequest(AuthenticatedUser.CustomerKey, EmployerId, AuthenticatedUser.Key);
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// API method to get risk profile by passing employee number and case number      
        /// </summary>
        /// <param name="caseNumber"></param>        
        /// <param name="employeeNumber"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("case/{caseNumber}")]
        public IHttpActionResult GetRiskProfileByCase(string employeeNumber, string caseNumber = null)
        {
            var parameters = new GetRiskProfileRequestParameters
            {
                CaseNumber = caseNumber,
                EmployerId = EmployerId,
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployeeNumber = employeeNumber
            };

            var request = new GetRiskProfileRequest(AuthenticatedUser.CustomerKey, EmployerId, AuthenticatedUser.Key);
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// API method to get calculated risk profile by passing employeeNumber and case number        
        /// </summary>
        /// <param name="caseNumber"></param>        
        /// <param name="employeeNumber"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("case/{caseNumber}/calculate")]
        public IHttpActionResult CalculateRiskProfileByCase(string employeeNumber, string caseNumber)
        {
            var parameters = new CalculateRiskProfileRequestParameters
            {
                CaseNumber = caseNumber,
                EmployerId = EmployerId,
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployeeNumber = employeeNumber
            };

            var request = new CalculateRiskProfileRequest(AuthenticatedUser.CustomerKey, EmployerId, AuthenticatedUser.Key);
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// API method to get calculated risk profile by passing employeeNumber and case number        
        /// </summary>      
        /// <param name="employeeNumber"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("calculate")]
        public IHttpActionResult CalculateRiskProfile(string employeeNumber)
        {
            var parameters = new CalculateRiskProfileRequestParameters
            {
                EmployerId = EmployerId,
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployeeNumber = employeeNumber
            };

            var request = new CalculateRiskProfileRequest(AuthenticatedUser.CustomerKey, EmployerId, AuthenticatedUser.Key);
            return Ok(request.Handle(parameters));
        }
    }
}
