﻿using AT.Api.Case.Models.WorkRestriction;
using AT.Api.Case.Requests.WorkRestriction;
using AT.Api.Core;
using System.Web.Http;

namespace AT.Api.Case.Controllers
{
    /// <summary>
    /// Work Restriction Controller
    /// </summary>
    [RoutePrefix("api/v1/cases/{caseNumber}/workRestrictions")]
    public class WorkRestrictionController : BaseApiController
    {
        /// <summary>
        /// API method to get work restrictions for employee's Case,
        /// Pass optionally employerReferenceCode to get array of work restrictions for the employee
        /// if employerReferenceCode is not sent the default employer for the authenticated user will be used.        
        /// </summary>
        /// <param name="caseNumber"></param>        
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public IHttpActionResult GetWorkRestrictions(string caseNumber)
        {
            var parameters = new GetWorkRestrictionParameters
            {
                CaseNumber = caseNumber,
                EmployerId = EmployerId
            };

            var request = new GetWorkRestrictionRequest(AuthenticatedUser.Key);
            return Ok(request.Handle(parameters));
        }


        /// <summary>
        /// API method to save work restriction for employee against a case,
        /// Pass optionally employerReferenceCode to save work restrictions for the employee
        /// if employerReferenceCode is not sent the default employer for the authenticated user will be used.
        /// </summary>
        /// <param name="caseNumber"></param>        
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        public IHttpActionResult SaveWorkRestrictions(string caseNumber, CreateWorkRestrictionModel model)
        {
            var parameters = new SaveWorkRestrictionParameters
            {
                CaseNumber = caseNumber,
                EmployerId = EmployerId,
                WorkRestriction = model
            };

            var request = new SaveWorkRestrictionRequest(AuthenticatedUser.Key);
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// API method to delete work restrictions for employee against a case,
        /// WorkRestrictionId is required parameter along with case number
        /// Pass optionally employerReferenceCode to save work restrictions for the employee
        /// if employerReferenceCode is not sent the default employer for the authenticated user will be used.
        /// </summary>
        /// <param name="caseNumber"></param>        
        /// <param name="workRestrictionId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{workRestrictionId}")]
        public IHttpActionResult DeleteWorkRestriction(string caseNumber, string workRestrictionId)
        {
            var parameters = new DeleteWorkRestrictionParameters
            {
                CaseNumber = caseNumber,
                EmployerId = EmployerId,
                WorkRestrictionId = workRestrictionId
            };

            var request = new DeleteWorkRestrictionRequest();
            return Ok(request.Handle(parameters));
        }

    }
}
