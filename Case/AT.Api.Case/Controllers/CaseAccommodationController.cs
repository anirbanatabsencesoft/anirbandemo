﻿using AT.Api.Case.Models.CaseAccommodation;
using AT.Api.Case.Requests.CaseAccommodation;
using AT.Api.Core;
using System.Collections.Generic;
using System.Web.Http;

namespace AT.Api.Case.Controllers
{
    /// <summary>
    /// Handles operations related to Case Accommodations
    /// </summary>
    [RoutePrefix("api/v1/cases/{caseNumber}/accommodations")]
    public class CaseAccommodationController : BaseApiController
    {
        /// <summary>
        /// Gets accommodations
        /// </summary>
        /// <param name="caseNumber">Unique number for the case</param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public IHttpActionResult Get(string caseNumber)
        {
            var request = new GetCaseAccommodationRequest(caseNumber);

            var result = request.Handle(new GetCaseAccommodationParameters
            {
                CaseNumber = caseNumber
            });

            return Ok(result);
        }

        /// <summary>
        /// Close case accommodation
        /// </summary>
        /// <param name="caseNumber">Unique number for the case</param>
        /// <param name="accommodationId">Id for he accommodation</param>
        /// <param name="model">Data to close case accommodation</param>
        /// <returns></returns>
        [HttpPut]
        [Route("{accommodationId}/close")]
        public IHttpActionResult Close(
            string caseNumber, 
            string accommodationId, 
            CloseCaseAccommodationModel model)
        {
            var request = new CloseCaseAccommodationRequest();

            var result = request.Handle(new CloseCaseAccommodationParameters
            {
                CaseNumber = caseNumber,
                AccommodationId = accommodationId,
                Model = model
            });

            return Ok(result);
        }

        [HttpPut]
        [Route("{accommodationId}/cancel")]
        public IHttpActionResult Cancel(string caseNumber, string accommodationId, CancelCaseAccommodationModel model)
        {
            var request = new CancelCaseAccommodationRequest();

            var result = request.Handle(new CancelCaseAccommodationParameters
            {
                CaseNumber = caseNumber,
                AccommodationId = accommodationId,
                Model = model
            });

            return Ok(result);
        }


        /// <summary>
        /// Get Case Accomodations interactive process
        /// </summary>
        /// <param name="caseNumber"></param>
        /// <param name="accommodationId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{accommodationId}/interactiveprocesses")]
        public IHttpActionResult Get(string caseNumber, string accommodationId)
        {
            var request = new GetInteractiveProcessRequest();

            var result = request.Handle(new GetInteractiveProcessParameters
            {
                CaseNumber = caseNumber,
                AccommodationId = accommodationId
            });

            return Ok(result);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="caseNumber"></param>
        /// <param name="accommodationId"></param>
        /// <param name="steps"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("{accommodationId}/interactiveprocesses")]
        public IHttpActionResult Save(string caseNumber, string accommodationId, List<InteractiveProcessStepModel> steps)
        {
            var request = new SaveInteractiveProcessRequest(AuthenticatedUser.Key);

            var result = request.Handle(new SaveInteractiveProcessParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                CaseNumber = caseNumber,
                AccommodationId = accommodationId,
                Steps = steps
            });

            return Ok(result);
        }
    }
}