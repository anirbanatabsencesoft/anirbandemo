﻿using AT.Api.Case.Models.Workflow;
using AT.Api.Case.Requests.Workflow;
using AT.Api.Core;
using System.Collections.Generic;
using System.Web.Http;

namespace AT.Api.Case.Controllers
{
    /// <summary>
    /// Handles operations on workflow
    /// </summary>
    [RoutePrefix("api/v1/cases/{caseNumber}/workflows/{workflowCode}")]
    public class WorkflowController : BaseApiController
    {
        /// <summary>
        /// Starts a workflow
        /// </summary>
        /// <param name="caseNumber">Unique number for the case</param>
        /// <param name="workflowCode">Code for the workflow</param>
        /// <returns></returns>
        [HttpPost]
        [Route("start")]
        public IHttpActionResult StartWorkflow(string caseNumber, string workflowCode)
        {
            var request = new StartWorkflowRequest(AuthenticatedUser.Key);

            var result = request.Handle(new StartWorkflowParameters
            {
                CaseNumber = caseNumber,
                WorkflowCode = workflowCode
            });

            return Ok(result);
        }
    }
}