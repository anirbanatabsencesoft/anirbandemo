﻿using AbsenceSoft.Data.Enums;
using AT.Api.Case.Requests.AbsenceReason;
using AT.Api.Case.Requests.AccommodationTypes;
using AT.Api.Case.Requests.AvailableManualPolicies;
using AT.Api.Case.Requests.CustomField;
using AT.Api.Case.Requests.NoteCategories;
using AT.Api.Case.Requests.WorkRestrictionTypes;
using AT.Api.Core;
using System.Web.Http;

namespace AT.Api.Case.Controllers
{
    [RoutePrefix("api/v1/lookups")]
    public class LookupsController : BaseApiController
    {
        /// <summary>
        /// GetAbsenceReasons API for getting all leaves data 
        /// employeeNumber is mandotory field
        /// caseType is optional
        /// </summary>
        /// <param name="employeeNumber"></param>
        /// <param name="caseType"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("absenceReasons/{employeeNumber}/{caseType?}")]
        public IHttpActionResult GetAbsenceReasons(string employeeNumber, CaseType? caseType)
        {
            var parameters = new GetAbsenceReasonParameters
            {
                employeeNumber = employeeNumber,
                CaseType = caseType,
                EmployerId = EmployerId,
                CustomerId = AuthenticatedUser.CustomerKey
            };
            var request = new GetAbsenceReasonRequest();
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// GetAccommodationType API for getting all Accommodation Type details
        /// employeeNumber is mandotory field
        /// caseType is optional
        /// </summary>
        /// <param name="employeeNumber"></param>
        /// <param name="caseType"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("accommodationType/{caseType?}")]
        public IHttpActionResult GetAccommodationType(CaseType? caseType)
        {
            var parameters = new AccommodationTypesParameters
            {
                CaseType = caseType,
                EmployerId = EmployerId,
                CustomerId = AuthenticatedUser.CustomerKey
            };
            var request = new AccommodationTypesRequest(AuthenticatedUser.Key, EmployerId, parameters.CustomerId);
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// GetCaseNoteCategories API for getting all Note Catagories
        /// </summary>
        /// <returns></returns>

        [HttpGet]
        [Route("caseNoteCategories")]
        public IHttpActionResult GetCaseNoteCategories()
        {
            var parameters = new NoteCategoriesParameter();
            var request = new GetNoteCategoriesRequest();
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// Get All CustomField 
        /// </summary>
        /// <param name="caseTarget"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("customFields/{caseTarget}")]
        public IHttpActionResult GetCustomField(EntityTarget caseTarget)
        {
            var parameters = new CustomFieldParameter
            {
                caseTarget = caseTarget,
                EmployerId = EmployerId,
                CustomerId = AuthenticatedUser.CustomerKey,
            };
            var request = new GetCustomFieldRequest(AuthenticatedUser.Key, EmployerId, parameters.CustomerId);
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// GetAvailableManualPolicies API by CaseNumber 
        /// </summary>
        /// <param name="caseNumber"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("availableManualPolicies/{caseNumber}")]
        public IHttpActionResult GetAvailableManualPolicies(string caseNumber)
        {
            var request = new GetAvailableManualPoliciesRequest(EmployerId);
            return Ok(request.Handle(caseNumber));
        }

        /// <summary>
        /// API method to get work restrictions types lopup items,
        /// Pass optionally employerReferenceCode to get array of work restrictions types
        /// if employerReferenceCode is not sent the default employer for the authenticated user will be used.        
        /// </summary>        
        /// <param name="employerReferenceCode"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("WorkRestrictionTypes")]
        public IHttpActionResult GetWorkRestrictionTypes()
        {
            var parameters = new GetWorkRestrictionTypeLookupParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId
            };

            var request = new GetWorkRestrictionTypeLookupRequest();
            return Ok(request.Handle(parameters));
        }
    }
}