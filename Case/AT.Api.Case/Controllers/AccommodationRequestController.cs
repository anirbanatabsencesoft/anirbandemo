﻿using AT.Api.Case.Models.CaseAccommodation;
using AT.Api.Case.Requests.CaseAccommodation;
using AT.Api.Core;
using System.Web.Http;

namespace AT.Api.Case.Controllers
{
    /// <summary>
    /// Handles operations related to Case Accommodations
    /// </summary>
    [RoutePrefix("api/v1/cases/{caseNumber}/accommodationrequests")]
    public class AccommodationRequestController : BaseApiController
    {
        /// <summary>
        /// Create a case accommodation
        /// </summary>
        /// <param name="caseNumber">Unique number for the case</param>
        /// <param name="model">Accommodation detail</param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        public IHttpActionResult CreateCaseAccommodations(string caseNumber, AccommodationRequestModel model)
        {
            var request = new CreateCaseAccommodationRequest();

            var result = request.Handle(new CreateCaseAccommodationParameters
            {
                CaseNumber = caseNumber,
                Model = model
            });

            return Ok(result);
        }
    }
}