﻿using AT.Api.Case.Models.Certifications;
using AT.Api.Case.Requests.Certifications;
using AT.Api.Core;
using System.Collections.Generic;
using System.Web.Http;

namespace AT.Api.Case.Controllers
{
    /// <summary>
    /// Case Certifications Controller
    /// </summary>
    [RoutePrefix("api/v1/cases/{caseNumber}/certifications")]
    public class CertificationsController : BaseApiController
    {

        /// <summary>
        /// API method to get Certificates for CaseNumber pass optionally employerReferenceCode to get array of certifications
        /// if employerReferenceCode is not sent the default employer for the authenticated user will be used.        
        /// </summary>
        /// <param name="caseNumber"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public IHttpActionResult GetCertifications(string caseNumber)
        {
            var parameters = new GetCertificationsParameters
            {
                CaseNumber = caseNumber,
                EmployerId = EmployerId
            };

            var request = new GetCertificationsRequest();
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// API method to save array of certificates for CaseNumber pass optionally employerReferenceCode
        /// if employerReferenceCode is not sent the default employer for the authenticated user will be used.        
        /// </summary>
        /// <param name="caseNumber"></param>        
        /// <param name="model"></param>
        /// <param name="employerReferenceCode"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("{employerReferenceCode?}")]
        public IHttpActionResult SaveCertifications(string caseNumber, List<CertificationModel> model, string employerReferenceCode = null)
        {
            var parameters = new SaveCertificationsParameters
            {
                CaseNumber = caseNumber,
                EmployerId = EmployerId,
                Certifications = model
            };
            var request = new SaveCertificationsRequest(AuthenticatedUser.Key);
            return Ok(request.Handle(parameters));
        }

    }
}
