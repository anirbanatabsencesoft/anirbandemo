﻿using AT.Api.Case.Models.Providers;
using AT.Api.Case.Requests.Providers;
using AT.Api.Core;
using System.Web.Http;

namespace AT.Api.Case.Controllers
{
    /// <summary>
    /// Api to handle operation for case providers
    /// </summary>
    [RoutePrefix("api/v1/cases/{caseNumber}/providers")]
    public class CaseProviderController : BaseApiController
    {
        /// <summary>
        /// Get Case providers
        /// </summary>
        /// <param name="caseNumber"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public IHttpActionResult Get(string caseNumber)
        {
            var request = new GetProvidersRequest();

            var result =  request.Handle(new GetProvidersParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                CaseNumber = caseNumber
            });

            return Ok(result);
        }

        /// <summary>
        /// Save a provider contact for an case
        /// </summary>
        /// <param name="caseNumber">case number for retrieving the related contact</param>
        /// <param name="model">Contact data to save</param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        public IHttpActionResult Save(string caseNumber, CaseProviderModel model)
        {
            var request = new SaveProviderRequest();

            var result = request.Handle(new SaveProviderParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                CaseNumber = caseNumber,
                CaseProvider = model
            });

            return Ok(result);
        }
    }
}
