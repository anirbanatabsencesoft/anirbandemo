﻿using AT.Api.Case.Requests.Contacts;
using AT.Api.Core;
using System.Web.Http;

namespace AT.Api.Employee.Controllers
{
    /// <summary>
    /// Handles operations related to case  provider
    /// </summary>
    [RoutePrefix("api/v1/providers/{contactId}")]
    public class ProviderController : BaseApiController
    {

        /// <summary>
        /// Deletes a provider contact by contact id
        /// </summary>
        /// <param name="contactId">Id of the contact to delete</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("")]
        public IHttpActionResult DeleteAsync(string contactId)
        {
            var request = new DeleteProviderRequest();

            var result =  request.Handle(new DeleteProviderParameters
            {
                ContactId = contactId
            });

            return Ok(result);
        }
    }
}
