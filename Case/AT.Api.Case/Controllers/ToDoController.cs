﻿using AT.Api.Case.Models.CreateToDo;
using AT.Api.Case.Requests.CreateToDo;
using AT.Api.Core;
using System.Web.Http;

namespace AT.Api.Case.Controllers
{
    /// <summary>
    /// ToDo Controller
    /// </summary>
    [RoutePrefix("api/v1/cases/{caseNumber}/todos")]
    public class ToDoController : BaseApiController
    {

        /// <summary>
        /// Create ToDo item
        /// </summary>
        /// <param name="caseNumber">Required parameter</param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        public IHttpActionResult CreateToDo(string caseNumber, CreateToDoModel model)
        {
            var request = new CreateToDoRequest(AuthenticatedUser.Key);

            var result = request.Handle(new CreateToDoParameters
            {
                CaseNumber = caseNumber,
                CreateToDoModel = model
            });

            return Ok(result);
        }
    }
}
