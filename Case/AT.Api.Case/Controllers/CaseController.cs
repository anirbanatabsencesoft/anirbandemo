﻿using AT.Api.Case.Models;
using AT.Api.Case.Models.CancelCase;
using AT.Api.Case.Models.CaseDeterminationModel;
using AT.Api.Case.Models.CloseCase;
using AT.Api.Case.Models.ReassignCase;
using AT.Api.Case.Models.RecalculateCase;
using AT.Api.Case.Models.WorkReleted;
using AT.Api.Case.Requests;
using AT.Api.Case.Requests.CancelCase;
using AT.Api.Case.Requests.Case;
using AT.Api.Case.Requests.CaseActivity;
using AT.Api.Case.Requests.CaseDeterminationStatus;
using AT.Api.Case.Requests.CaseEligibility;
using AT.Api.Case.Requests.ChangeCase;
using AT.Api.Case.Requests.ChangePolicyStatus;
using AT.Api.Case.Requests.CloseCase;
using AT.Api.Case.Requests.ReadCase;
using AT.Api.Case.Requests.ReadWorkReleted;
using AT.Api.Case.Requests.ReassignCase;
using AT.Api.Case.Requests.RecalculateCase;
using AT.Api.Case.Requests.SaveWorkReleted;
using AT.Api.Case.Requests.TimeTracker;
using AT.Api.Core;
using System.Web.Http;

namespace AT.Api.Case.Controllers
{
    /// <summary>
    /// Case Controller
    /// </summary>
    [RoutePrefix("api/v1/cases")]
    public class CaseController : BaseApiController
    {
        /// <summary>
        /// API method to create new case
        /// </summary>
        /// <param name="model">Data for creating the case</param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        public IHttpActionResult CreateCase(CaseModel model)
        {
            var parameters = new CreateCaseParameter
            {
                CaseModel = model,
                EmployerId = EmployerId,
                CustomerId = AuthenticatedUser.CustomerKey?.ToString(),
            };

            var request = new CreateCaseRequest(AuthenticatedUser.Key, EmployerId, AuthenticatedUser.CustomerKey);
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// API method to read case details by case number
        /// </summary>
        /// <param name="caseNumber">Unique number for the case</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{caseNumber}")]
        public IHttpActionResult ReadCase(string caseNumber)
        {
            var request = new ReadCaseRequest(AuthenticatedUser.Key, EmployerId, AuthenticatedUser.CustomerKey);
            var response = request.Handle(caseNumber);

            return Ok(response);
        }

        /// <summary>
        ///  API method to ChangeCase request
        /// caseNumber  as required parameter
        /// </summary>
        /// <param name="caseNumber"></param>
        /// <param name="changeCase"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{caseNumber}")]
        public IHttpActionResult ChangeCase(string caseNumber, ChangeCaseModel changeCase)
        {
            var parameters = new ChangeCaseParameters
            {
                CaseNumber = caseNumber,
                ChangeCase = changeCase
            };
            var request = new ChangeCaseRequest();
            return Ok(request.Handle(parameters));
        }
        /// <summary>
        /// API method to Calculating Eligibility against case
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Eligibilty")]
        public IHttpActionResult CalculateEligibility(CaseModel model)
        {
            var parameters = new CaseEligibilityParameters
            {
                CaseModel = model,
                EmployerId = EmployerId,
                CustomerId = AuthenticatedUser.CustomerKey,
            };
            var request = new CaseEligibilityRequest(EmployerId);
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// API method to get Time tracker request
        /// </summary>
        /// <param name="caseNumber"></param>        
        /// <returns></returns>
        [HttpGet]
        [Route("{caseNumber}/timeTracker")]
        public IHttpActionResult GetTimeTracker(string caseNumber)
        {
            var parameters = new GetTimeTrackerParameters
            {
                CaseNumber = caseNumber,
                //includeStatus = (EligibilityStatus)eligibilityStatus
            };
            var request = new GetTimeTrackerRequest();
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// Reassign case to user
        /// </summary>
        /// <param name="caseNumber">Required parameter</param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{caseNumber}/assignment")]
        public IHttpActionResult ReassignCase(string caseNumber, ReassignCaseModel model)
        {
            var request = new ReassignCaseRequest();

            var result = request.Handle(new ReassignCaseParameters
            {
                AssigneeTypeCode = model.AssigneeTypeCode,
                AssigneeUserEmail = model.AssigneeUserEmail,
                CaseNumber = caseNumber
            });

            return Ok(result);
        }

        /// <summary>
        /// Cancel case
        /// </summary>
        /// <param name="caseNumber">Required parameter</param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{caseNumber}/cancel")]
        public IHttpActionResult CancelCase(string caseNumber, CancelCaseModel model)
        {
            var request = new CancelCaseRequest();

            var result = request.Handle(new CancelCaseParameters
            {
                CaseNumber = caseNumber,
                Reason = model.CancelReason
            });

            return Ok(result);
        }

        /// <summary>
        /// Close case
        /// </summary>
        /// <param name="caseNumber">Required parameter</param>
        /// <returns></returns>
        [HttpPut]
        [Route("{caseNumber}/close")]
        public IHttpActionResult CloseCase(string caseNumber, CloseCaseModel model)
        {
            var request = new CloseCaseRequest();

            var result = request.Handle(new CloseCaseParameters
            {
                CaseNumber = caseNumber,
                ClosureReason = model.ClosureReason,
                Description = model.Description
            });

            return Ok(result);
        }

        /// <summary>
        /// Recalculate case
        /// </summary>
        /// <param name="caseNumber">Required parameter</param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{caseNumber}/calculate")]
        public IHttpActionResult RecalculateCase(string caseNumber, RecalculateCaseModel model)
        {
            var request = new RecalculateCaseRequest();

            var result = request.Handle(new RecalculateCaseParameters
            {
                CaseNumber = caseNumber,
                SaveCase = model.SaveCase
            });

            return Ok(result);
        }
        /// <summary>
        /// WorkReleted Save Method
        /// </summary>
        /// <param name="caseNumber"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("{caseNumber}/workRelatedInfo")]
        public IHttpActionResult WorkRelated(string caseNumber, WorkReletedModel model)
        {
            var request = new SaveWorkReletedRequest();
            var result = request.Handle(new SaveWorkReletedParameters
            {
                CaseNumber = caseNumber,
                WorkReleted = model
            });
            return Ok(result);
        }

        /// <summary>
        /// WorkReleted  get method
        /// Required caseNumber 
        /// </summary>
        /// <param name="caseNumber"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{caseNumber}/workRelatedInfo")]
        public IHttpActionResult WorkRelated(string caseNumber)
        {
            var request = new ReadWorkReletedRequest();
            var result = request.Handle(caseNumber);
            return Ok(result);
        }

        /// <summary>
        /// Get case activity details of communications, notes, attachments and todo services of the provided case number
        /// </summary>
        /// <param name="caseNumber"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{caseNumber}/activities")]
        public IHttpActionResult GetCaseActivity(string caseNumber)
        {
            var request = new GetCaseActivityRequest(AuthenticatedUser.Key);

            var result = request.Handle(new GetCaseActivityParameters
            {
                CaseNumber = caseNumber
            });

            return Ok(result);
        }
        /// <summary>
        /// save CaseDetermination details for policies required case number and caseDetemination model
        /// </summary>
        /// <param name="caseNumber"></param>
        /// <param name="caseDetermination"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("{caseNumber}/CaseDeterminationPolicy")]
        public IHttpActionResult CaseDetermination(string caseNumber, CaseDeterminationModel caseDetermination)
        {
            var request = new SaveCaseDeterminationRequest();

            var result = request.Handle(new CasePolicyParameters
            {
                CaseNumber = caseNumber,
                CaseDetermination = caseDetermination

            });
            return Ok(result);
        }

        /// <summary>
        /// Get CaseDetermination details  of case against provided case number 
        /// </summary>
        /// <param name="caseNumber"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{caseNumber}/CaseDeterminationPolicy")]
        public IHttpActionResult CaseDetermination(string caseNumber)
        {
            var request = new GetCaseDeterminationRequest();

            var result = request.Handle(new CasePolicyParameters
            {
                CaseNumber = caseNumber,
            });
            return Ok(result);
        }
    }
}
