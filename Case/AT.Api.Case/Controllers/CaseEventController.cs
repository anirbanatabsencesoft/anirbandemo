﻿using AT.Api.Case.Models.CaseEvents;
using AT.Api.Case.Requests.CaseEvents;
using AT.Api.Core;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
namespace AT.Api.Case.Controllers
{
    /// <summary>
    /// Retrieves Case Events of a particular Case
    /// </summary>
    [RoutePrefix("api/v1/cases/{caseNumber}/events")]
    public class CaseEventController : BaseApiController
    {
        /// <summary>
        /// Gets CaseEvents list by case number
        /// </summary>
        /// <param name="caseNumber">The case number</param>
        /// <returns>The Case Events</returns>
        [HttpGet]
        [Route("")]
        public IHttpActionResult GetCaseEvents(string caseNumber)
        {
            var parameters = new GetCaseEventsParameters
            {
                CaseNumber = caseNumber
            };

            var request = new GetCaseEventsRequest();
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// API method to Create/Update Case Event List 
        /// </summary>
        /// <param name="caseNumber"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        public IHttpActionResult SaveCaseEvent(string caseNumber, List<SaveCaseEventModel> model)
        {
            var parameters = new SaveCaseEventParameters
            {
                CaseNumber = caseNumber,
                CaseEvents = model
            };

            var request = new SaveCaseEventRequest();
            return Ok(request.Handle(parameters));
        }
    }
}
