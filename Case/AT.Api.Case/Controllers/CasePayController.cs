﻿using AT.Api.Case.Models.CasePay;
using AT.Api.Case.Requests.CasePay;
using AT.Api.Core;
using System.Web.Http;

namespace AT.Api.Case.Controllers
{
    /// <summary>
    /// Work Restriction Controller
    /// </summary>
    [RoutePrefix("api/v1/cases/{caseNumber}/payPeriods")]
    public class CasePayController : BaseApiController
    {
        /// <summary>
        /// API method to get case pay for employee's Case,
        /// Pass optionally employerReferenceCode
        /// If employerReferenceCode is not sent the default employer for the authenticated user will be used.
        /// </summary>
        /// <param name="caseNumber"></param>        
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public IHttpActionResult GetCasePay(string caseNumber)
        {
            var parameters = new GetCasePayParameters
            {
                CaseNumber = caseNumber,
                EmployerId = EmployerId
            };

            var request = new GetCasePayRequest();
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// API method to update pay period payroll status for employee against a case,
        /// Pass optionally employerReferenceCode
        /// If employerReferenceCode is not sent the default employer for the authenticated user will be used.
        /// </summary>
        /// <param name="caseNumber"></param>        
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("")]
        public IHttpActionResult UpdateCasePayStatus(string caseNumber, CasePayPeriodModel model)
        {
            var parameters = new SaveCasePayPeriodParameters
            {
                CaseNumber = caseNumber,
                EmployerId = EmployerId,
                PayPeriod = model
            };

            var request = new SaveCasePayPeriodRequest();
            return Ok(request.Handle(parameters));
        }

    }
}
