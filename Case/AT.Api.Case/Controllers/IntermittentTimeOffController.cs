﻿using AbsenceSoft.Common;
using AT.Api.Case.Models.IntermittentTimeoff;
using AT.Api.Case.Requests.IntermittentTimeOff;
using AT.Api.Core;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace AT.Api.Case.Controllers
{
    /// <summary>
    /// Case Controller
    /// </summary>
    [RoutePrefix("api/v1/cases")]
    public class IntermittentTimeOffController : BaseApiController
    {
        /// <summary>
        /// API method to get time off request by date
        /// </summary>
        /// <param name="caseNumber">Unique number for the case</param>
        /// <param name="requestDate">Date of the request</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{caseNumber}/IntermittentTimeoffRequests/{requestDate}")]
        public IHttpActionResult GetIntermittentTimeOffRequestByDate(string caseNumber, DateTime requestDate)
        {
            var parameters = new GetIntermittentTimeOffRequestParameters
            {
                CaseNumber = caseNumber,
                RequestDate = requestDate
            };

            var request = new GetIntermittentTimeOffRequestByDate();
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// API method to get time off request
        /// </summary>
        /// <param name="caseNumber">Unique number for the case</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{caseNumber}/IntermittentTimeoffRequests")]
        public IHttpActionResult GetIntermittentTimeOffRequestByDate(string caseNumber)
        {
            var parameters = new GetIntermittentTimeOffRequestParameters
            {
                CaseNumber = caseNumber
            };

            var request = new GetIntermittentTimeOffRequestByCase();
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// Create or modify intermittent time off request
        /// </summary>
        /// <param name="caseNumber">Unique number for the case</param>
        /// <param name="model">Data to create intermittent time off</param>
        /// <returns></returns>
        [HttpPost]
        [Route("{caseNumber}/IntermittentTimeoffRequest")]
        public IHttpActionResult CreateIntermittentTimeOffRequest(string caseNumber, IntermittentTimeOffModel model)
        {
            var parameters = new CreateIntermittentTimeoffRequestParameters
            {
                CaseNumber = caseNumber,
                IntermittentTimeOff = model
            };

            var request = new CreateIntermittentTimeoffRequest(AuthenticatedUser.Key);
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// Create Bulk intermittent time off request
        /// </summary>
        /// <param name="caseNumber">Unique number for the case</param>
        /// <param name="model">Data to create intermittent time off in bulk</param>
        /// <returns></returns>
        [HttpPost]
        [Route("{caseNumber}/IntermittentTimeoffRequests")]
        public IHttpActionResult CreateBulkIntermittentTimeoffRequest(string caseNumber, List<IntermittentTimeOffModel> model)
        {
            var parameters = new CreateBulkIntermittentTimeoffRequestParameters
            {
                CaseNumber = caseNumber,
                intermittentTimeOffs = model
            };

            var request = new CreateBulkIntermittentTimeoffRequest(AuthenticatedUser.Key);
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// API method to get validation messages before creating timeoff request
        /// if it contains errors then those must be fixed before creating timeoff request
        /// </summary>        
        /// <param name="caseNumber"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("{caseNumber}/IntermittentTimeoffRequest/validation")]
        public IHttpActionResult PreValidateTimeOffRequest(string caseNumber, IntermittentTimeOffModel model)
        {
            var parameters = new ValidateTimeoffRequestParameters
            {
                CaseNumber = caseNumber,
                IntermittentTimeOff = model
            };

            var request = new ValidateTimeoffRequest();
            return Ok(request.Handle(parameters));
        }
        [HttpDelete]
        [Route("{caseNumber}/IntermittentTimeoffRequests/{id}")]
        public IHttpActionResult DeleteIntermittentTimeOffRequestByDate(string caseNumber,Guid id)
        {
            var parameters = new DeleteIntermittentTimeOffRequestParameters
            {
                Id = id,
                CaseNumber = caseNumber
            };

            var request = new DeleteIntermittentTimeOffRequestByCaseRequest(AuthenticatedUser.Key, EmployerId, AuthenticatedUser.CustomerKey);
            return Ok(request.Handle(parameters));
        }
    }
}
