﻿using AbsenceSoft.Data.Customers;
using AT.Api.Case.Models.Providers;

namespace AT.Api.Case.DataExtensions
{
    /// <summary>
    /// Case provider contact extensions
    /// </summary>
    internal static class CaseProviderExtensions
    {
        /// <summary>
        /// Converts employee contact entity to case contact model
        /// </summary>
        /// <param name="employeeContact"></param>
        /// <returns></returns>
        internal static CaseProviderModel ToProviderModel(this EmployeeContact employeeContact)
        {
            var model = new CaseProviderModel
            {
                ContactId = employeeContact.Id,
                ContactTypeCode = employeeContact.ContactTypeCode,
                ContactPersonName = employeeContact.Contact.ContactPersonName,
                CompanyName = employeeContact.Contact.CompanyName,
                Fax = employeeContact.Contact.Fax,
                FirstName = employeeContact.Contact.FirstName,
                LastName = employeeContact.Contact.LastName,
                IsPrimary = employeeContact.Contact.IsPrimary.GetValueOrDefault(),
                Phone = employeeContact.Contact.WorkPhone
            };

            return model;
        }
    }
}