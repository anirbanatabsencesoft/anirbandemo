﻿using AbsenceSoft.Data.Customers;
using AT.Api.Case.Models;

namespace AT.Api.Case.DataExtensions
{
    internal static class EmployeeContactExtensions
    {
        internal static CaseReporterModel ToCaseReporterModel(this EmployeeContact caseReporter)
        {
            if (caseReporter == null)
            {
                return null;
            }

            var model = new CaseReporterModel
            {
                ContactTypeCode = caseReporter.ContactTypeCode,
                EmployeeNumber = caseReporter.EmployeeNumber
            };

            var reporterContact = caseReporter.Contact;

            if (reporterContact == null)
            {
                return model;
            }

            model.CompanyName = caseReporter.Contact.CompanyName;
            model.Title = caseReporter.Contact.Title;
            model.FirstName = caseReporter.Contact.FirstName;
            model.LastName = caseReporter.Contact.LastName;
            model.WorkPhone = caseReporter.Contact.WorkPhone;

            return model;
        }

        internal static CaseRelationshipModel ToCaseRelationshipModel(this EmployeeContact caseRelationContact)
        {
            if (caseRelationContact == null)
            {
                return null;
            }

            var model = new CaseRelationshipModel
            {
                TypeCode = caseRelationContact.ContactTypeCode,
                MilitaryStatus = caseRelationContact.MilitaryStatus
            };

            var contactDetails = caseRelationContact.Contact;

            if (contactDetails == null)
            {
                return model;
            }

            model.FirstName = contactDetails.FirstName;
            model.LastName = contactDetails.LastName;
            model.DateOfBirth = contactDetails.DateOfBirth;

            return model;
        }
    }
}