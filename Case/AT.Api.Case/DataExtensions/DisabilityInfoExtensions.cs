﻿using AbsenceSoft.Data.Cases;
using AT.Api.Case.Models.DisabilityInfo;
using System.Collections.Generic;

namespace AT.Api.Case.DataExtensions
{
    internal static class DisabilityInfoExtensions
    {
        public static ReturnToWorkPathModel ToReturnToWorkPathModel(this DiagnosisGuidelines guidelines)
        {
            var model = new ReturnToWorkPathModel
            {
                Code = guidelines.Code,
                Description = guidelines.Description,
                Expiration = guidelines.ExpirationDate,

                RtwSummary = guidelines.RtwSummary != null 
                    ? ToRtwSummaryGuidelines(guidelines.RtwSummary) 
                    : new Models.DisabilityInfo.RtwSummaryGuidelines(),

                RtwBestPractices = (guidelines.RtwBestPractices != null && guidelines.RtwBestPractices.Count > 0) 
                    ? ToRtwBestPractice(guidelines.RtwBestPractices) 
                    : new List<Models.DisabilityInfo.RtwBestPractice>(),

                Chiropractical = (guidelines.Chiropractical != null && guidelines.Chiropractical.Count > 0) 
                    ? ToChiropracticalGuideline(guidelines.Chiropractical) 
                    : new List<Models.DisabilityInfo.ChiropracticalGuideline>(),

                PhisicalTherapy = (guidelines.PhisicalTherapy != null && guidelines.PhisicalTherapy.Count > 0) 
                    ? ToPhisicalTherapyGuideline(guidelines.PhisicalTherapy) 
                    : new List<Models.DisabilityInfo.PhisicalTherapyGuideline>(),

                ActivityModifications = (guidelines.ActivityModifications != null && guidelines.ActivityModifications.Count > 0) 
                    ? ToActivityModification(guidelines.ActivityModifications) 
                    : new List<Models.DisabilityInfo.ActivityModification>()
            };
            return model;
        }

        public static Models.DisabilityInfo.RtwSummaryGuidelines ToRtwSummaryGuidelines(this AbsenceSoft.Data.Cases.RtwSummaryGuidelines rtwGuidelines)
        {
            var model = new Models.DisabilityInfo.RtwSummaryGuidelines
            {
                ClaimsMidRange = rtwGuidelines.ClaimsMidRange,
                ClaimsAtRisk = rtwGuidelines.ClaimsAtRisk,
                AbsencesMidRange = rtwGuidelines.AbsencesMidRange,
                AbsencesAtRisk = rtwGuidelines.AbsencesAtRisk
            };

            return model;
        }

        public static Models.DisabilityInfo.RtwBestPractice ToRtwBestPractice(this AbsenceSoft.Data.Cases.RtwBestPractice bestPractice)
        {
            var model = new Models.DisabilityInfo.RtwBestPractice
            {
                Id = bestPractice.Id.ToString(),
                RtwPath = bestPractice.RtwPath,
                MinDays = bestPractice.MinDays,
                MaxDays = bestPractice.MaxDays                
            };

            return model;
        }

        public static List<Models.DisabilityInfo.RtwBestPractice> ToRtwBestPractice(this List<AbsenceSoft.Data.Cases.RtwBestPractice> bestPractices)
        {
            List<Models.DisabilityInfo.RtwBestPractice> models = new List<Models.DisabilityInfo.RtwBestPractice>();

            foreach (AbsenceSoft.Data.Cases.RtwBestPractice obj in bestPractices)
            {
                var model = new Models.DisabilityInfo.RtwBestPractice
                {
                    Id = obj.Id.ToString(),
                    RtwPath = obj.RtwPath,
                    MinDays = obj.MinDays,
                    MaxDays = obj.MaxDays
                };
                models.Add(model);
            }

            return models;
        }

        public static Models.DisabilityInfo.ChiropracticalGuideline ToChiropracticalGuideline(this AbsenceSoft.Data.Cases.ChiropracticalGuideline chiroGuidelines)
        {
            var model = new Models.DisabilityInfo.ChiropracticalGuideline
            {
                PrimaryPath = chiroGuidelines.PrimaryPath,
                Visits = chiroGuidelines.Visits,
                Weeks = chiroGuidelines.Weeks
            };

            return model;
        }

        public static List<Models.DisabilityInfo.ChiropracticalGuideline> ToChiropracticalGuideline(this List<AbsenceSoft.Data.Cases.ChiropracticalGuideline> chiroGuidelines)
        {
            List<Models.DisabilityInfo.ChiropracticalGuideline> models = new List<Models.DisabilityInfo.ChiropracticalGuideline>();

            foreach (AbsenceSoft.Data.Cases.ChiropracticalGuideline obj in chiroGuidelines)
            {
                var model = new Models.DisabilityInfo.ChiropracticalGuideline
                {
                    PrimaryPath = obj.PrimaryPath,
                    Visits = obj.Visits,
                    Weeks = obj.Weeks
                };
                models.Add(model);
            }

            return models;
        }

        public static Models.DisabilityInfo.PhisicalTherapyGuideline ToPhisicalTherapyGuideline(this AbsenceSoft.Data.Cases.PhisicalTherapyGuideline ptGuidelines)
        {
            var model = new Models.DisabilityInfo.PhisicalTherapyGuideline
            {
                PrimaryPath = ptGuidelines.PrimaryPath,
                Visits = ptGuidelines.Visits,
                Weeks = ptGuidelines.Weeks
            };

            return model;
        }

        public static List<Models.DisabilityInfo.PhisicalTherapyGuideline> ToPhisicalTherapyGuideline(this List<AbsenceSoft.Data.Cases.PhisicalTherapyGuideline> ptGuidelines)
        {
            List<Models.DisabilityInfo.PhisicalTherapyGuideline> models = new List<Models.DisabilityInfo.PhisicalTherapyGuideline>();

            foreach (AbsenceSoft.Data.Cases.PhisicalTherapyGuideline obj in ptGuidelines)
            {
                var model = new Models.DisabilityInfo.PhisicalTherapyGuideline
                {
                    PrimaryPath = obj.PrimaryPath,
                    Visits = obj.Visits,
                    Weeks = obj.Weeks
                };
                models.Add(model);
            }

            return models;
        }

        public static Models.DisabilityInfo.ActivityModification ToActivityModification(this AbsenceSoft.Data.Cases.ActivityModification activity)
        {
            var model = new Models.DisabilityInfo.ActivityModification
            {
                JobType = activity.JobType,
                JobModifications = activity.JobModifications
            };
            return model;
        }

        public static List<Models.DisabilityInfo.ActivityModification> ToActivityModification(this List<AbsenceSoft.Data.Cases.ActivityModification> activities)
        {
            List<Models.DisabilityInfo.ActivityModification> models = new List<Models.DisabilityInfo.ActivityModification>();

            foreach (AbsenceSoft.Data.Cases.ActivityModification obj in activities)
            {
                var model = new Models.DisabilityInfo.ActivityModification
                {
                    JobType = obj.JobType,
                    JobModifications = obj.JobModifications
                };
                models.Add(model);
            }

            return models;
        }

        public static DiagnosisCodeModel ToDiagnosisCode(this DiagnosisCode diagnosis)
        {
            var model = new DiagnosisCodeModel
            {
                CodeType = diagnosis.CodeType.ToString(),
                Code = diagnosis.Code,
                Description = diagnosis.Description,
                DurationType = diagnosis.DurationType == null ? (int?)null : (int?)diagnosis.DurationType.Value,
                MaxDuration = diagnosis.MaxDuration,
                Name = diagnosis.Name
            };

            return model;
        }


        public static List<DiagnosisCodeModel> ToDiagnosisCode(this List<DiagnosisCode> diagnosiscodes)
        {
            List<DiagnosisCodeModel> models = new List<DiagnosisCodeModel>();

            foreach (DiagnosisCode diagnosis in diagnosiscodes)
            {
                var model = new DiagnosisCodeModel
                {
                    CodeType = diagnosis.CodeType.ToString(),
                    Code = diagnosis.Code,
                    Description = diagnosis.Description,
                    DurationType = (int?)diagnosis.DurationType,
                    MaxDuration = diagnosis.MaxDuration,
                    Name = diagnosis.Name
                };
                models.Add(model);
            }

            return models;
        }

        public static Models.DisabilityInfo.CoMorbidityGuideline ToCoMorbidityGuideline(this AbsenceSoft.Data.Cases.CoMorbidityGuideline coMorbidity)
        {
            var model = new Models.DisabilityInfo.CoMorbidityGuideline
            {
                RiskAssessmentScore = coMorbidity.RiskAssessmentScore,
                RiskAssessmentStatus = coMorbidity.RiskAssessmentStatus,
                MidrangeAllAbsence = coMorbidity.MidrangeAllAbsence,
                AtRiskAllAbsence = coMorbidity.AtRiskAllAbsence,
                BestPracticesDays = coMorbidity.BestPracticesDays,
                ActualDaysLost = coMorbidity.ActualDaysLost,                
            };

            return model;
        }

    }
}