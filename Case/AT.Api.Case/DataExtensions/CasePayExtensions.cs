﻿using AT.Api.Case.Models.CasePay;
using System.Collections.Generic;
using Pay = AbsenceSoft.Logic.Pay;

namespace AT.Api.Case.DataExtensions
{
    internal static class CasePayExtensions
    {
        /// <summary>
        /// map Pay.CasePayModel to API.CasePayModel
        /// </summary>
        /// <param name="payModel"></param>
        /// <param name="EmployeeNumber"></param>
        /// <returns></returns>
        public static CasePayModel ToCasePayModel(Pay.CasePayModel payModel, string EmployeeNumber)
        {
            var model = new CasePayModel
            {
                ApplyOffsetsByDefault = payModel.ApplyOffsetsByDefault,
                ApprovedFrom = payModel.ApprovedFrom,
                ApprovedThru = payModel.ApprovedThru,
                BasePay = payModel.BasePay,
                EmployeeNumber = EmployeeNumber,
                EndDate = payModel.EndDate,
                Pay = payModel.Pay.HasValue ? (int?)payModel.Pay.Value : null,
                PayPeriods = ToCasePayPeriodModel(payModel.PayPeriods),
                PayScheduleId = payModel.PayScheduleId,
                PayScheduleName = payModel.PayScheduleName,
                Policies = ToCasePayPolicyModel(payModel.Policies),
                ScheduledHoursPerWeek = payModel.ScheduledHoursPerWeek,
                ShowWaiveWaitingPeriod = payModel.ShowWaiveWaitingPeriod,
                StartDate = payModel.StartDate,
                TotalPaid = payModel.TotalPaid,
                WaiveWaitingPeriod = payModel.WaiveWaitingPeriod
            };
            return model;
        }

        public static List<CasePayPolicyModel> ToCasePayPolicyModel(List<Pay.CasePayPolicyModel> policies)
        {
            List<CasePayPolicyModel> list = new List<CasePayPolicyModel>();
            foreach (Pay.CasePayPolicyModel policy in policies)
            {
                var model = new CasePayPolicyModel
                {
                    BenefitPercentage = policy.BenefitPercentage,
                    MaxBenefitAmount = policy.MaxBenefitAmount,
                    MinBenefitAmount = policy.MinBenefitAmount,
                    PolicyCode = policy.PolicyCode,
                    PolicyName = policy.PolicyName,
                    TotalPaid = policy.TotalPaid,
                    TotalPaidLast52Weeks = policy.TotalPaidLast52Weeks,
                };
                list.Add(model);
            }
            return list;
        }

        public static List<CasePayPeriodModel> ToCasePayPeriodModel(List<Pay.CasePayPeriodModel> periods)
        {
            List<CasePayPeriodModel> list = new List<CasePayPeriodModel>();
            foreach (Pay.CasePayPeriodModel period in periods)
            {
                var model = new CasePayPeriodModel
                {
                    Amount = period.Amount,
                    Detail = ToCasePayDetailModel(period.Detail),
                    PayDate = period.PayDate,
                    PayFrom = period.PayFrom,
                    PayPeriodId = period.PayPeriodId.ToString(),
                    PayPeriodSalary = period.PayPeriodSalary,
                    PayThru = period.PayThru,
                    Percentage = period.Percentage,
                    Status = (int)period.Status
                };
                list.Add(model);
            }
            return list;
        }

        public static CasePayPeriodModel ToCasePayPeriodModel(Pay.CasePayPeriodModel period)
        {
            var model = new CasePayPeriodModel
            {
                Amount = period.Amount,
                Detail = ToCasePayDetailModel(period.Detail),
                PayDate = period.PayDate,
                PayFrom = period.PayFrom,
                PayPeriodId = period.PayPeriodId.ToString(),
                PayPeriodSalary = period.PayPeriodSalary,
                PayThru = period.PayThru,
                Percentage = period.Percentage,
                Status = (int)period.Status
            };
            return model;
        }

        public static List<CasePayDetailModel> ToCasePayDetailModel(List<Pay.CasePayDetailModel> details)
        {
            List<CasePayDetailModel> list = new List<CasePayDetailModel>();
            foreach (Pay.CasePayDetailModel detail in details)
            {
                var model = new CasePayDetailModel
                {
                    AllowOffset = detail.AllowOffset,
                    BenefitPercentage = detail.BenefitPercentage,
                    IsOffset = detail.IsOffset,
                    PayDetailId = detail.PayDetailId.ToString(),
                    PayFrom = detail.PayFrom,
                    PayThru = detail.PayThru,
                    PolicyCode = detail.PolicyCode,
                    PolicyName = detail.PolicyName,
                    TotalPaid = detail.TotalPaid
                };
                list.Add(model);
            }
            return list;
        }
    }
}