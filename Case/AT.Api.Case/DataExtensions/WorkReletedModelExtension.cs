﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AT.Api.Case.Models.CreateContact;
using AT.Api.Case.Models.Sharp;
using AT.Api.Case.Models.WorkReleted;

namespace AT.Api.Case.DataExtensions
{
    internal static class WorkReletedModelExtension
    {
        public static WorkReletedModel ToWorkRelatedInfo(WorkRelatedInfo WorkRelatedInfo)
        {
            var model = new WorkReletedModel
            {
                IllnessOrInjuryDate = WorkRelatedInfo.IllnessOrInjuryDate,
                Reportable = WorkRelatedInfo.Reportable,
                HealthCarePrivate = WorkRelatedInfo.HealthCarePrivate,
                WhereOccurred = WorkRelatedInfo.WhereOccurred,
                InjuryLocation = WorkRelatedInfo.InjuryLocation,
                InjuredBodyPart = WorkRelatedInfo.InjuredBodyPart,
                Classification = WorkRelatedInfo.Classification,
                TypeOfInjury = WorkRelatedInfo.TypeOfInjury,
                DaysAwayFromWork = WorkRelatedInfo.DaysAwayFromWork,
                OverrideDaysAwayFromWork = WorkRelatedInfo.OverrideDaysAwayFromWork,
                DaysOnJobTransferOrRestriction = WorkRelatedInfo.DaysOnJobTransferOrRestriction,
                CalculatedDaysOnJobTransferOrRestriction = WorkRelatedInfo.CalculatedDaysOnJobTransferOrRestriction,
                ProviderContact = WorkRelatedInfo.Provider == null ? null : ToCreateContactModel(WorkRelatedInfo.Provider),
                EmergencyRoom = WorkRelatedInfo.EmergencyRoom,
                HospitalizedOvernight = WorkRelatedInfo.HospitalizedOvernight,
                TimeEmployeeBeganWork = WorkRelatedInfo.TimeEmployeeBeganWork,// == null ? TimeEmployeeBeganWork : WorkReleted.TimeEmployeeBeganWork.ApplyModel(TimeEmployeeBeganWork);
                TimeOfEvent = WorkRelatedInfo.TimeOfEvent,// ? TimeOfEvent : WorkReleted.TimeOfEvent.ApplyModel(TimeOfEvent);
                ActivityBeforeIncident = WorkRelatedInfo.ActivityBeforeIncident,
                WhatHappened = WorkRelatedInfo.WhatHappened,
                InjuryOrIllness = WorkRelatedInfo.InjuryOrIllness,
                WhatHarmedTheEmployee = WorkRelatedInfo.WhatHarmedTheEmployee,
                DateOfDeath = WorkRelatedInfo.DateOfDeath,
                Sharps = WorkRelatedInfo.Sharps,
                sharp = WorkRelatedInfo.Sharps != null ? toSharpModel(WorkRelatedInfo.SharpsInfo) : null

            };

            return model;
        }

        public static CreateContactModel ToCreateContactModel(EmployeeContact EmployeeContact)
        {
            var model = new CreateContactModel
            {
                FirstName = EmployeeContact.Contact?.FirstName,
                LastName = EmployeeContact.Contact?.LastName,
                ContactTypeCode = EmployeeContact.ContactTypeCode,
                Address = new Models.Address.AddressModel
                {
                    Name = EmployeeContact.Contact.Address.Name,
                    Address1 = EmployeeContact.Contact.Address?.Address1,
                    Address2 = EmployeeContact.Contact.Address?.Address2,
                    City = EmployeeContact.Contact.Address?.City,
                    State = EmployeeContact.Contact.Address?.State,
                    PostalCode = EmployeeContact.Contact.Address?.PostalCode,
                    Country = EmployeeContact.Contact.Address?.Country,
                }
            };
            return model;
        }

        public static SharpsInfoModel toSharpModel(WorkRelatedSharpsInfo WorkRelatedSharpsInfo)
        {
            var model = new SharpsInfoModel
            {
                InjuryLocation = WorkRelatedSharpsInfo.InjuryLocation,
                TypeOfSharp = WorkRelatedSharpsInfo.TypeOfSharp,
                BrandOfSharp = WorkRelatedSharpsInfo.BrandOfSharp,
                ModelOfSharp = WorkRelatedSharpsInfo.ModelOfSharp,
                BodyFluidInvolved = WorkRelatedSharpsInfo.BodyFluidInvolved,
                HaveEngineeredInjuryProtection = WorkRelatedSharpsInfo.HaveEngineeredInjuryProtection,
                WasProtectiveMechanismActivated = WorkRelatedSharpsInfo.WasProtectiveMechanismActivated,
                WhenDidTheInjuryOccur = WorkRelatedSharpsInfo.WhenDidTheInjuryOccur,
                JobClassification = WorkRelatedSharpsInfo.JobClassification,
                JobClassificationOther = WorkRelatedSharpsInfo.JobClassificationOther,
                LocationAndDepartment = WorkRelatedSharpsInfo.LocationAndDepartment,
                LocationAndDepartmentOther = WorkRelatedSharpsInfo.LocationAndDepartmentOther,
                Procedure = WorkRelatedSharpsInfo.Procedure,
                ProcedureOther = WorkRelatedSharpsInfo.ProcedureOther,
                ExposureDetail = WorkRelatedSharpsInfo.ExposureDetail,
            };
            return model;
        }
    }
}