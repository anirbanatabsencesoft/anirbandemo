﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Models.Cases;

namespace AT.Api.Case.DataExtensions
{
    internal static class AbsenceReasonModelExtension
    {
        public static AbsenceReasonModel ToAbsenceReasonModelNonHirachical(this AbsenceReason absenceReason, bool IsCategory, bool IsChild)
        {
            var model = new AbsenceReasonModel
            {
                Id = absenceReason.Id,
                Code = absenceReason.Code,
                HelpText = absenceReason.HelpText,
                Name = absenceReason.Name,
                IsCategory = IsCategory,
                IsChild = IsChild,
                IsForFamilyMember = (absenceReason.Flags & AbsenceReasonFlag.RequiresRelationship) == AbsenceReasonFlag.RequiresRelationship,
                IsHiddenInESSSelect = (absenceReason.Flags & AbsenceReasonFlag.HideInEmployeeSelfServiceSelection) == AbsenceReasonFlag.HideInEmployeeSelfServiceSelection,
                IsToShowAccomodationAlways = (absenceReason.Flags & AbsenceReasonFlag.ShowAccommodation) == AbsenceReasonFlag.ShowAccommodation
            };
            return model;
        }

    }
}