﻿using AbsenceSoft.Data.Customers;
using AT.Api.Case.Models.CaseRelatedContact;

namespace AT.Api.Case.DataExtensions
{
    /// <summary>
    /// case related person contact extensions
    /// </summary>
    internal static class CaseRelatedContactExtensions
    {
        /// <summary>
        /// converts employee contact entity to case related contact model
        /// </summary>
        /// <param name="employeeContact"></param>
        /// <returns></returns>
        internal static CaseRelatedContactModel ToCaseRelatedContactModel(this EmployeeContact employeeContact)
        {
            var model = new CaseRelatedContactModel
            {
                ContactTypeCode = employeeContact.ContactTypeCode,
                FirstName = employeeContact.Contact.FirstName,
                LastName = employeeContact.Contact.LastName,
                DateOfBirth = employeeContact.Contact.DateOfBirth,
                MilitaryStatus = employeeContact.MilitaryStatus
            };

            return model;
        }
    }
}