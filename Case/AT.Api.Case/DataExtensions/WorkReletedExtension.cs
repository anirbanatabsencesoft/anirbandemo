﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AT.Api.Case.Models.CreateContact;
using AT.Api.Case.Models.Sharp;
using AT.Api.Case.Models.WorkReleted;

namespace AT.Api.Case.DataExtensions
{
    internal static class WorkReletedExtension
    {
        public static WorkRelatedInfo ToWorkRelatedInfo(WorkReletedModel WorkReleted)
        {
            var model = new WorkRelatedInfo
            {
                IllnessOrInjuryDate = WorkReleted.IllnessOrInjuryDate,
                Reportable = WorkReleted.Reportable,
                HealthCarePrivate = WorkReleted.HealthCarePrivate,
                WhereOccurred = WorkReleted.WhereOccurred,
                InjuryLocation = WorkReleted.InjuryLocation,
                InjuredBodyPart = WorkReleted.InjuredBodyPart,
                Classification = WorkReleted.Classification,
                TypeOfInjury = WorkReleted.TypeOfInjury,
                DaysAwayFromWork = WorkReleted.DaysAwayFromWork,
                OverrideDaysAwayFromWork = WorkReleted.OverrideDaysAwayFromWork,
                DaysOnJobTransferOrRestriction = WorkReleted.DaysOnJobTransferOrRestriction,
                CalculatedDaysOnJobTransferOrRestriction = WorkReleted.CalculatedDaysOnJobTransferOrRestriction,
                Provider = WorkReleted.ProviderContact == null ? null : ToEmployeeContact(WorkReleted.ProviderContact),
                EmergencyRoom = WorkReleted.EmergencyRoom,
                HospitalizedOvernight = WorkReleted.HospitalizedOvernight,
                TimeEmployeeBeganWork = WorkReleted.TimeEmployeeBeganWork,// == null ? TimeEmployeeBeganWork : WorkReleted.TimeEmployeeBeganWork.ApplyModel(TimeEmployeeBeganWork);
                TimeOfEvent = WorkReleted.TimeOfEvent,// ? TimeOfEvent : WorkReleted.TimeOfEvent.ApplyModel(TimeOfEvent);
                ActivityBeforeIncident = WorkReleted.ActivityBeforeIncident,
                WhatHappened = WorkReleted.WhatHappened,
                InjuryOrIllness = WorkReleted.InjuryOrIllness,
                WhatHarmedTheEmployee = WorkReleted.WhatHarmedTheEmployee,
                DateOfDeath = WorkReleted.DateOfDeath,
                Sharps = WorkReleted.Sharps,
                SharpsInfo = WorkReleted.sharp != null ? ToWorkRelatedSharpsInfo(WorkReleted.sharp) : null

            };

            return model;
        }
        public static EmployeeContact ToEmployeeContact(CreateContactModel ProviderContact)
        {
            var model = new EmployeeContact
            {
                Contact = new AbsenceSoft.Data.Contact
                {
                    FirstName = ProviderContact.FirstName,
                    LastName = ProviderContact.LastName,
                    Address = new AbsenceSoft.Data.Address
                    {
                        Name = ProviderContact.Address?.Name,
                        Address1 = ProviderContact.Address?.Address1,
                        City = ProviderContact.Address?.City,
                        Address2 = ProviderContact.Address?.Address2,
                        State = ProviderContact.Address?.State,
                        PostalCode = ProviderContact.Address?.PostalCode,
                        Country = ProviderContact.Address?.Country
                    },
                },
                ContactTypeCode = ProviderContact.ContactTypeCode

            };
            return model;
        }
        public static WorkRelatedSharpsInfo ToWorkRelatedSharpsInfo(SharpsInfoModel sharp)
        {
            var model = new WorkRelatedSharpsInfo
            {
                InjuryLocation = sharp.InjuryLocation,
                TypeOfSharp = sharp.TypeOfSharp,
                BrandOfSharp = sharp.BrandOfSharp,
                ModelOfSharp = sharp.ModelOfSharp,
                BodyFluidInvolved = sharp.BodyFluidInvolved,
                HaveEngineeredInjuryProtection = sharp.HaveEngineeredInjuryProtection,
                WasProtectiveMechanismActivated = sharp.WasProtectiveMechanismActivated,
                WhenDidTheInjuryOccur = sharp.WhenDidTheInjuryOccur,
                JobClassification = sharp.JobClassification,
                JobClassificationOther = sharp.JobClassificationOther,
                LocationAndDepartment = sharp.LocationAndDepartment,
                LocationAndDepartmentOther = sharp.LocationAndDepartmentOther,
                Procedure = sharp.Procedure,
                ProcedureOther = sharp.ProcedureOther,
                ExposureDetail = sharp.ExposureDetail,
            };
            return model;
        }

    }
}