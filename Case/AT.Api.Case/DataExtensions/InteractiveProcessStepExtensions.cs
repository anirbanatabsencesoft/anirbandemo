﻿using AT.Api.Case.Models.CaseAccommodation;
using static AbsenceSoft.Data.Cases.AccommodationInteractiveProcess;

namespace AT.Api.Case.DataExtensions
{
    /// <summary>
    /// Interactive process extension 
    /// </summary>
    internal static class InteractiveProcessStepExtensions
    {
        /// <summary>
        /// Converts Interactive process step to model
        /// </summary>
        /// <param name="step"></param>
        /// <returns></returns>
        public static GetInteractiveProcessModel ToInteractiveProcessStep(this Step step)
        {
            return new GetInteractiveProcessModel
            { 
                Answer = !string.IsNullOrEmpty(step.SAnswer) 
                            ? step.SAnswer 
                            : step.Answer.ToString(),
                QuestionText = step.Question.Question,
                QuestionId = step.QuestionId,
            };
        }
    }
}