﻿using AbsenceSoft;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AT.Api.Case.Models;
using AT.Api.Case.Models.CaseAccommodation;
using AT.Api.Case.Models.IntermittentTimeoff;
using System;
using System.Collections.Generic;
using System.Linq;
using ATCases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.DataExtensions
{
    internal static class CaseExtensions
    {
        internal static CaseModel ToCaseModel(this ATCases.Case caseData)
        {
            var caseReporter = caseData.CaseReporter.ToCaseReporterModel();
            var caseRelationship = caseData.Contact.ToCaseRelationshipModel();
            var caseDates = caseData.ToCaseDatesModel();
            var caseFlags = caseData.ToCaseFlagModel();

            var model = new CaseModel
            {
                CaseNumber = caseData.CaseNumber,
                EmployeeNumber = caseData.Employee?.EmployeeNumber,
                ReasonCode = caseData.Reason?.Name,
                ShortDescription = caseData.Description,
                Summary = caseData.Narrative,
                StartDate = caseData.StartDate,
                EndDate = caseData.EndDate,
                CaseReporter = caseReporter,
                CaseType = caseData.Segments != null ? caseData.Segments.Select(x => x.Type).FirstOrDefault() : CaseType.None,
                CaseDates = caseDates,
                CaseRelationship = caseRelationship,
                CaseFlags = caseFlags
            };

            return model;
        }

        internal static CaseDatesModel ToCaseDatesModel(this ATCases.Case caseData)
        {
            if (caseData == null)
            {
                return null;
            }

            return new CaseDatesModel
            {
                ActualDeliveryDate = caseData.GetCaseEventDate(CaseEventType.DeliveryDate),
                AdoptionDate = caseData.GetCaseEventDate(CaseEventType.AdoptionDate),
                BondingStartDate = caseData.GetCaseEventDate(CaseEventType.BondingStartDate),
                BondingEndDate = caseData.GetCaseEventDate(CaseEventType.BondingEndDate),
                ExpectedDeliveryDate = caseData.GetCaseEventDate(CaseEventType.DeliveryDate)
            };
        }

        internal static CaseFlagModel ToCaseFlagModel(this ATCases.Case caseData)
        {
            if (caseData == null)
            {
                return null;
            }

            return new CaseFlagModel
            {
                IsWorkRelated = caseData.Metadata.GetRawValue<bool>("IsWorkRelated"),
                WillUseBonding = caseData.Metadata.GetRawValue<bool>("WillUseBonding"),
                MedicalComplications = caseData.Metadata.GetRawValue<bool>("MedicalComplications")
            };
        }

        internal static CaseAccommodationModel ToCaseAccommodationModel(this ATCases.AccommodationRequest accommodationRequest)
        {
            var model = new CaseAccommodationModel
            {
                AccommodationRequestId = accommodationRequest.Id.ToString(),
                GeneralHealthCondition = accommodationRequest.GeneralHealthCondition,
                Description = accommodationRequest.Description,
                Status = accommodationRequest.Status,
                Accommodations = ToAccommodationModel(accommodationRequest.Accommodations),
                StartDate = accommodationRequest.StartDate,
                EndDate = accommodationRequest.EndDate,
                Determination = accommodationRequest.Determination,
                SummaryDuration = accommodationRequest.SummaryDuration,
                SummaryDurationType = accommodationRequest.SummaryDurationType,
                CancelReason = accommodationRequest.CancelReason,
                OtherCancelReasonDescription = accommodationRequest.OtherReasonDesc
            };

            return model;
        }

        internal static List<AccommodationModel> ToAccommodationModel(this List<ATCases.Accommodation> accommodations)
        {
            if (!accommodations.Any())
            {
                return new List<AccommodationModel>();
            }

            return accommodations
                .Select(item => new AccommodationModel
                {
                    AccommodationId = item.Id.ToString(),
                    TypeCode = item.Type.Code,
                    TypeName = item.Type.Name,
                    Duration = item.Duration,
                    Description = item.Description,
                    IsResolved = item.Resolved,
                    IsGranted = item.Granted,
                    StartDate = item.StartDate,
                    EndDate = item.EndDate,
                    Determination = item.Determination,
                    MinimumApprovedFromDate = item.MinApprovedFromDate,
                    MaximumApprovedThruDate = item.MaxApprovedThruDate,
                    Status = item.Status,
                    CancelReason = item.CancelReason,
                    OtherCancelReasonDescription = item.OtherReasonDesc,
                    IsWorkRelated = item.IsWorkRelated.GetValueOrDefault()
                })
                .ToList();
        }

        internal static AccommodationModel ToAccommodationModel(this ATCases.Accommodation accommodation)
        {
            if (accommodation == null)
            {
                return new AccommodationModel();
            }

            return new AccommodationModel
            {
                AccommodationId = accommodation.Id.ToString(),
                TypeCode = accommodation.Type.Code,
                TypeName = accommodation.Type.Name,
                Duration = accommodation.Duration,
                Description = accommodation.Description,
                IsResolved = accommodation.Resolved,
                IsGranted = accommodation.Granted,
                StartDate = accommodation.StartDate,
                EndDate = accommodation.EndDate,
                Determination = accommodation.Determination,
                MinimumApprovedFromDate = accommodation.MinApprovedFromDate,
                MaximumApprovedThruDate = accommodation.MaxApprovedThruDate,
                Status = accommodation.Status,
                CancelReason = accommodation.CancelReason,
                OtherCancelReasonDescription = accommodation.OtherReasonDesc,
                IsWorkRelated = accommodation.IsWorkRelated.GetValueOrDefault()
            };
        }

        internal static AccommodationRequestModel ToAccommodationRequestModel(this ATCases.AccommodationRequest accommodationRequest)
        {
            var model = new AccommodationRequestModel
            {
                CaseNumber = accommodationRequest.CaseNumber,
                Status = accommodationRequest.Status,
                GeneralHealthCondition = accommodationRequest.GeneralHealthCondition,
                Description = accommodationRequest.Description,
                Accommodations = ToAccommodationModel(accommodationRequest.Accommodations),
                StartDate = accommodationRequest.StartDate,
                EndDate = accommodationRequest.EndDate,
                Determination = accommodationRequest.Determination,
                CancelReason = accommodationRequest.CancelReason,
                OtherReasonDescription = accommodationRequest.OtherReasonDesc
            };

            return model;
        }


        internal static IntermittentTimeOffModel ToIntermittentTimeOffModel(
            this IntermittentTimeRequest intermittentTimeRequest,
            List<PolicySummary> policySummaries)
        {
            var intermittentTimeRequestDetail = intermittentTimeRequest.Detail;

            var intermittentTimeOffRequests = intermittentTimeRequestDetail != null ? intermittentTimeRequestDetail
                .Select(req => req.ToIntermittentTimeOffRequestDetailModel(policySummaries))
                .ToList() : null;

            var model = new IntermittentTimeOffModel
            {
                Id = intermittentTimeRequest.Id,
                Notes = intermittentTimeRequest.Notes,
                RequestDate = intermittentTimeRequest.RequestDate,
                CreatedDate = intermittentTimeRequest.CreatedDate,
                IntermittentType = intermittentTimeRequest.IntermittentType.HasValue
                    ? Convert.ToInt16(intermittentTimeRequest.IntermittentType.Value)
                    : (int?)null,

                TotalMinutes = intermittentTimeRequest.TotalMinutes,
                EmployeeEntered = intermittentTimeRequest.EmployeeEntered,
                ManagerApproved = intermittentTimeRequest.ManagerApproved,
                ManagerApprovedDate = intermittentTimeRequest.ManagerApprovedDate,
                ManagerId = intermittentTimeRequest.ManagerId,
                IsIntermittentRestriction = intermittentTimeRequest.IsIntermittentRestriction,
                WarningsApproved = false,
                Details = intermittentTimeOffRequests,
                StartTimeForLeave = intermittentTimeRequest.StartTimeForLeave,
                EndTimeForLeave = intermittentTimeRequest.EndTimeForLeave
            };

            model.IsTimeEqual = IsTimeOffRequestTimeEqual(model);

            return model;
        }

        internal static IntermittentTimeOffRequestDetailModel ToIntermittentTimeOffRequestDetailModel(
            this IntermittentTimeRequestDetail timeOffRequestDetail,
            List<PolicySummary> policySummaries)
        {
            var intermittentTimeOffRequestDetail = new IntermittentTimeOffRequestDetailModel
            {
                PolicyCode = timeOffRequestDetail.PolicyCode,
                PendingTime = timeOffRequestDetail.Pending,
                ApprovedTime = timeOffRequestDetail.Approved,
                DeniedTime = timeOffRequestDetail.Denied,
                DenialReasonCode = timeOffRequestDetail.DenialReasonCode,
                DenialReasonName = timeOffRequestDetail.DenialReasonName,
                DenialReasonOther = timeOffRequestDetail.DeniedReasonOther
            };

            if (policySummaries == null || !policySummaries.Any(summary => summary.PolicyCode == timeOffRequestDetail.PolicyCode))
            {
                return intermittentTimeOffRequestDetail;
            }

            var policySummary = policySummaries.FirstOrDefault(x => x.PolicyCode == timeOffRequestDetail.PolicyCode);

            if (policySummary == null)
            {
                return intermittentTimeOffRequestDetail;
            }

            intermittentTimeOffRequestDetail.PolicyName = policySummary.PolicyName;

            intermittentTimeOffRequestDetail.Available = policySummary.TimeRemaining < 0
                ? "reasonable amount"
                : string.Format("{0:N2} {1}", policySummary.TimeRemaining, policySummary.Units.ToString().ToLowerInvariant());

            return intermittentTimeOffRequestDetail;
        }

        private static bool IsTimeOffRequestTimeEqual(this IntermittentTimeOffModel model)
        {
            if (model == null || model.Details == null || !model.Details.Any())
            {
                return true;
            }
            string denialReasonOther = string.Empty;

            var timeOffRequestDetail = model.Details.First();

            var approvedTime = timeOffRequestDetail.ApprovedTime;
            var deniedTime = timeOffRequestDetail.DeniedTime;
            var pendingTime = timeOffRequestDetail.PendingTime;
            var denialReasonCode = timeOffRequestDetail.DenialReasonCode;
            var otherDenialReason = timeOffRequestDetail.DenialReasonOther;

            if (deniedTime > 0 && denialReasonCode == AdjudicationDenialReason.Other)
            {
                denialReasonOther = otherDenialReason;
            }

            return model.Details.Any(detail =>
                detail.ApprovedTime != approvedTime ||
                detail.DeniedTime != deniedTime ||
                detail.PendingTime != pendingTime ||
                (deniedTime > 0 && denialReasonCode != detail.DenialReasonCode) ||
                (denialReasonCode == AdjudicationDenialReason.Other && denialReasonOther != detail.DenialReasonOther));
        }

    }
}