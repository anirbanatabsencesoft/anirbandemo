﻿using AbsenceSoft;
using AT.Api.Case.Models.CasePolicyChangeStatus;
using System.Collections.Generic;
using System.Linq;
using Cases = AbsenceSoft.Data.Cases;
namespace AT.Api.Case.DataExtensions
{
    internal static class DeterminationPolicyExtension
    {
        public static List<DeterminationPolicy> toDeteminationPolicy(this Cases.Case existingCase)
        {
            var list = new List<DeterminationPolicy>();
            foreach (var appliedPolicy in existingCase.Segments.FirstOrDefault().AppliedPolicies)
            {
                var model = new DeterminationPolicy()
                {
                    PolicyName = appliedPolicy.Policy.Name,
                    PolicyCode = appliedPolicy.Policy.Code,      
                    EligibilityStatus=appliedPolicy.Status,
                    DeteminationStatus = existingCase.Summary.Policies.Where(x => x.PolicyCode == appliedPolicy.Policy.Code).FirstOrDefault()?.Detail.Select(detail => new CaseDeteminationPolicySummary()
                    {
                        CaseType = detail.CaseType.ToString().SplitCamelCaseString(),
                        DenialExplanation = detail.DenialReasonOther,
                        DenialReason = detail.DenialReasonName,
                        EndDate = detail.EndDate,
                        StartDate = detail.StartDate,
                        Status = detail.Determination.ToString().SplitCamelCaseString()
                    }).ToList()
                };
                list.Add(model);
            }
            return list;
        }
    }
}