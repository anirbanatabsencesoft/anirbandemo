﻿using AT.Api.Case.Models.Certifications;
using System;
using System.Collections.Generic;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;

namespace AT.Api.Case.DataExtensions
{
    internal static class CertificationExtensions
    {
        public static CertificationModel ToCertificationModel(this Certification certificationModel)
        {
            var model = new CertificationModel
            {
                StartDate = certificationModel.StartDate,
                EndDate = certificationModel.EndDate,
                Occurances = certificationModel.Occurances,
                Frequency = certificationModel.Frequency,
                FrequencyType = (int)certificationModel.FrequencyType,
                FrequencyUnitType = (int)certificationModel.FrequencyUnitType,
                Duration = certificationModel.Duration,
                DurationType = (int)certificationModel.DurationType,
                IntermittentType = (int)certificationModel.IntermittentType,
                IsCertificateIncomplete = certificationModel.IsCertificationIncomplete,
                CertificationCreateDate = certificationModel.CertificationCreateDate,
                Id = certificationModel.Id               
            };

            return model;
        }

        public static List<CertificationModel> ToCertificationModel(this List<Certification> certificationModel)
        {
            List<CertificationModel> list = new List<CertificationModel>();

            foreach (AbsenceSoft.Data.Cases.Certification obj in certificationModel)
            {
                var model = new CertificationModel
                {
                    StartDate = (obj.StartDate != default(DateTime)) ? obj.StartDate : (DateTime?)null,
                    EndDate = (obj.EndDate != default(DateTime)) ? obj.EndDate : (DateTime?)null,
                    Occurances = obj.Occurances,
                    Frequency = obj.Frequency,
                    FrequencyType = (int)obj.FrequencyType,
                    FrequencyUnitType = (int)obj.FrequencyUnitType,
                    Duration = obj.Duration,
                    DurationType = (int)obj.DurationType,
                    IntermittentType = (int)obj.IntermittentType,
                    IsCertificateIncomplete = obj.IsCertificationIncomplete,
                    CertificationCreateDate = obj.CertificationCreateDate,
                    Id = obj.Id               
                };

                list.Add(model);
            }
            return list;
        }

        internal static Certification FromCertificationModel(this CertificationModel certificationModel)
        {
            var model = new Certification
            {
                StartDate = (certificationModel.StartDate != null && certificationModel.StartDate != DateTime.MinValue) ? certificationModel.StartDate.Value : DateTime.MinValue,
                EndDate = (certificationModel.EndDate != null && certificationModel.EndDate != DateTime.MinValue) ? certificationModel.EndDate.Value : DateTime.MinValue,
                Frequency = certificationModel.Frequency ?? 0,
                FrequencyType = certificationModel.FrequencyType.HasValue ? (Unit)(certificationModel.FrequencyType.Value) : Unit.Days,
                FrequencyUnitType = certificationModel.FrequencyUnitType.HasValue ? (DayUnitType)(certificationModel.FrequencyUnitType.Value) : DayUnitType.Workday,
                Occurances = certificationModel.Occurances ?? 0,
                Duration = certificationModel.Duration ?? 0d,
                DurationType = certificationModel.DurationType.HasValue ? (Unit)(certificationModel.DurationType.Value) : Unit.Days,
                IntermittentType = certificationModel.IntermittentType.HasValue ? (IntermittentType)(certificationModel.IntermittentType.Value) : (IntermittentType?)null,
                IsCertificationIncomplete = certificationModel.IsCertificateIncomplete,
                CertificationCreateDate = certificationModel.CertificationCreateDate,
                Id = certificationModel.Id.HasValue ? certificationModel.Id.Value : Guid.Empty
            };
            return model;
        }

    }
}