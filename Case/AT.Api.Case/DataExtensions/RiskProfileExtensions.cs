﻿using AbsenceSoft.Data.RiskProfiles;
using AT.Api.Case.Models.RiskProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AT.Api.Case.DataExtensions
{
    public static class RiskProfileExtensions
    {
       
        /// <summary>
        /// To CalculateRiskProfileModel from api mapping to CalculatedRiskProfile model from absenceSoft data
        /// </summary>
        /// <param name="calRiskProfile"></param>
        /// <returns></returns>
        public static RiskProfileModel ToRiskProfileModel(this CalculatedRiskProfile calRiskProfile)
        {
            var model = new RiskProfileModel
            {
                Name = calRiskProfile.RiskProfile.Name,
                Color = calRiskProfile.RiskProfile.Color,
                Description = calRiskProfile.RiskProfile.Description
            };
            return model;
        }
    }
}