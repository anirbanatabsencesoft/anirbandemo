﻿using AbsenceSoft;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AT.Api.Case.Models;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.DataExtensions
{
    internal static class CaseModelsExtesnsion
    {
        public static Cases.Case ToCase(Cases.Case newCase, CaseModel caseModel)
        {
            if (!string.IsNullOrWhiteSpace(caseModel.CaseNumber))
                newCase.CaseNumber = caseModel.CaseNumber;
            if (caseModel.CaseRelationship != null)
            {
                newCase.Contact = new EmployeeContact()
                {
                    EmployeeNumber = newCase.Employee.EmployeeNumber,
                    EmployerId = newCase.EmployerId,
                    CustomerId = newCase.CustomerId,
                    ContactTypeCode = caseModel.CaseRelationship.TypeCode,
                    Contact = new Contact()
                    {
                        FirstName = caseModel.CaseRelationship.FirstName,
                        LastName = caseModel.CaseRelationship.LastName,
                        DateOfBirth = caseModel.CaseRelationship.DateOfBirth
                    },
                    MilitaryStatus = caseModel.CaseRelationship.MilitaryStatus != null ? (MilitaryStatus)caseModel.CaseRelationship.MilitaryStatus : MilitaryStatus.Civilian
                };
            }

            if (caseModel.CaseReporter != null)
            {
                newCase.CaseReporter = new EmployeeContact()
                {
                    EmployeeNumber = caseModel.CaseReporter.EmployeeNumber,
                    EmployerId = newCase.EmployerId,
                    CustomerId = newCase.CustomerId,
                    ContactTypeCode = caseModel.CaseReporter.ContactTypeCode,
                    Contact = new Contact()
                    {
                        CompanyName = caseModel.CaseReporter.CompanyName,
                        FirstName = caseModel.CaseReporter.FirstName,
                        LastName = caseModel.CaseReporter.LastName,
                        WorkPhone = caseModel.CaseReporter.WorkPhone
                    },
                };
            }

            if (caseModel.CaseFlags.IsWorkRelated.HasValue)
            {
                newCase.Metadata.SetRawValue("IsWorkRelated", caseModel.CaseFlags.IsWorkRelated.Value);
            }

            if (caseModel.CaseFlags.WillUseBonding.HasValue)
            {
                newCase.Metadata.SetRawValue("WillUseBonding", caseModel.CaseFlags.WillUseBonding.Value);
            }

            if (caseModel.CaseDates.BondingStartDate.HasValue)
            {
                newCase.SetCaseEvent(CaseEventType.BondingStartDate, caseModel.CaseDates.BondingStartDate.Value);
            }

            if (caseModel.CaseDates.BondingEndDate.HasValue)
            {
                newCase.SetCaseEvent(CaseEventType.BondingEndDate, caseModel.CaseDates.BondingEndDate.Value);
            }

            if (caseModel.CaseDates.AdoptionDate.HasValue)
            {
                newCase.SetCaseEvent(CaseEventType.AdoptionDate, caseModel.CaseDates.AdoptionDate.Value);
            }

            if (caseModel.CaseFlags.MedicalComplications.HasValue)
            {
                newCase.Disability.MedicalComplications = caseModel.CaseFlags.MedicalComplications.Value;
            }

            if (caseModel.CaseDates.ActualDeliveryDate.HasValue)
            {
                newCase.SetCaseEvent(CaseEventType.DeliveryDate, caseModel.CaseDates.ActualDeliveryDate.Value);
            }

            if (caseModel.CaseDates.ExpectedDeliveryDate.HasValue)
            {
                newCase.SetCaseEvent(CaseEventType.DeliveryDate, caseModel.CaseDates.ExpectedDeliveryDate.Value);
            }

            return newCase;
        }
    }
}