﻿using AbsenceSoft.Data.Jobs;
using AT.Api.Case.Models.WorkRestriction;
using System;
using System.Collections.Generic;

namespace AT.Api.Case.DataExtensions
{
    internal static class WorkRestrictionExtensions
    {
        public static List<WorkRestrictionModel> ToWorkRestrictionModel(List<EmployeeRestriction> employeeRestriction)
        {
            List<WorkRestrictionModel> list = new List<WorkRestrictionModel>();

            foreach (EmployeeRestriction restriction in employeeRestriction)
            {
                var model = new WorkRestrictionModel
                {
                    Id = restriction.Id,
                    DemandId = restriction.Restriction.DemandId,
                    DemandName = restriction.Restriction.Demand?.Name,
                    StartDate = restriction.Restriction.Dates != null ? restriction.Restriction.Dates.StartDate : (DateTime?)null,
                    EndDate = restriction.Restriction.Dates != null ? restriction.Restriction.Dates.EndDate : null,
                    DateString = restriction.Restriction.Dates.ToString(),
                    Values = (restriction.Restriction.Values != null && restriction.Restriction.Values.Count > 0) ? ToAppliedDemandValueModel(restriction.Restriction.Values) : null,
                };

                list.Add(model);
            }

            return list;
        }

        public static List<AppliedDemandValueModel> ToAppliedDemandValueModel(List<AppliedDemandValue<WorkRestrictionValue>> demandValues)
        {
            List<AppliedDemandValueModel> list = new List<AppliedDemandValueModel>();

            foreach (AppliedDemandValue<WorkRestrictionValue> v in demandValues)
            {
                var model = new AppliedDemandValueModel
                {                    
                    DemandTypeCode = v.DemandType != null ? v.DemandType.Code : string.Empty,
                    Type = (int)v.Type,
                    Text = v.Text,
                    Applicable = v.Applicable,
                    Value = v.ToString(),
                    Duration = v.Schedule != null ? v.Schedule.Duration : 0d,
                    DurationType = v.Schedule != null ? (int?)v.Schedule.DurationType : null,
                    Frequency = v.Schedule != null ? v.Schedule.Frequency : 0,
                    FrequencyType = v.Schedule != null ? (int?)v.Schedule.FrequencyType : null,
                    FrequencyUnitType = v.Schedule != null ? (int?)v.Schedule.FrequencyUnitType : null,
                    Occurances = v.Schedule != null ? v.Schedule.Occurances : 0,
                    DemandItemName = v.DemandItem != null ? v.DemandItem.Name : string.Empty,
                    DemandItem = v.DemandItem != null ? ToDemandItemModel(v.DemandItem) : null,
                };

                list.Add(model);
            }

            return list;
        }

        public static List<DemandItemModel> ToDemandItemModel(List<DemandItem> demandItems)
        {
            List<DemandItemModel> list = new List<DemandItemModel>();
            foreach (DemandItem v in demandItems)
            {
                var model = new DemandItemModel
                {
                    Value = v.Value,
                    Name = v.Name
                };

                list.Add(model);
            }

            return list;
        }

        public static DemandItemModel ToDemandItemModel(DemandItem v)
        {
            var item = new DemandItemModel
            {
                Value = v.Value,
                Name = v.Name,
            };

            return item;
        }

        public static List<WorkRestrictionTypeLookupModel> ToWorkRestrictionTypeLookupModel(List<DemandType> demandTypes)
        {
            List<WorkRestrictionTypeLookupModel> list = new List<WorkRestrictionTypeLookupModel>();

            foreach (DemandType demandtype in demandTypes)
            {
                var model = new WorkRestrictionTypeLookupModel
                {
                    Id = demandtype.Id,
                    Description = demandtype.Description,
                    Name = demandtype.Name,
                    Order = demandtype.Order,
                    Type = (int)demandtype.Type,
                    Code = demandtype.Code,
                    Items = demandtype.Items != null && demandtype.Items.Count > 0 ? ToDemandItemModel(demandtype.Items) : null
                };

                list.Add(model);
            }

            return list;
        }
    }
}