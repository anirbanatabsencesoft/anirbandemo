﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Cases;
using AT.Api.Case.Models.TimeTracker;

namespace AT.Api.Case.DataExtensions
{
    internal static class PolicySummaryExtensions
    {
        public static PolicySummaryModel ToPolicySummaryModel(this PolicySummary policysummary)
        {
            var model = new PolicySummaryModel
            {
                AbsenceReason = policysummary.AbsenceReason,
                AbsenceReasonId = policysummary.AbsenceReasonId,
                ExcludeFromTimeConversion = policysummary.ExcludeFromTimeConversion,
                HoursRemaining = policysummary.HoursRemaining,
                HoursUsed = policysummary.HoursUsed,
                MinutesUsed = policysummary.MinutesUsed,
                PolicyCode = policysummary.PolicyCode,
                PolicyName = policysummary.PolicyName,
                PolicyType = (int)policysummary.PolicyType,
                PolicyTypeText = policysummary.PolicyType.ToString(),
                TimeRemaining = policysummary.TimeRemaining,
                TimeUsed = policysummary.TimeUsed,
                Units = (int)policysummary.Units,
                UnitText = policysummary.Units.ToString()
            };

            return model;
        }
    }
}