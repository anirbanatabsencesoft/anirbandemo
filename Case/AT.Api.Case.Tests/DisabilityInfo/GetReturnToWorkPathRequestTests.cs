﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft;
using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Security;
using AT.Api.Core.Exceptions;
using Moq;
using Customers = AbsenceSoft.Data.Customers;
using AT.Api.Case;
using AT.Api.Case.Requests.DisabilityInfo;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AT.Api.Case.Models.DisabilityInfo;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Data.Notes;
using System.Threading.Tasks;
using System.Net;
using AbsenceSoft.Logic.Customers.Contracts;
using System.Collections.Generic;

namespace AT.Api.Case.Tests.DisabilityInfo
{
    [TestClass]
    public class GetReturnToWorkPathRequestTests
    {
        [TestMethod]
        public void GetReturnToWorkPath_InvalidCaseId_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(null, null);
            var guideService = DiagnosisGuidelinesServiceStub(null);
            var employerService = EmployerServieStub(null);
            
            var request = new GetReturnToWorkPathRequest(caseService.Object, guideService.Object, employerService.Object);

            var parameter = new GetReturnToWorkPathParameters
            {
                CaseNumber = "",
                DiagnosisCode = "E11",
                EmployerId = "2",
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetReturnToWorkPath_CaseNotFound_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(null, null); // null case
            var guideService = DiagnosisGuidelinesServiceStub(EntityHelper.DiagnosisGuidelines());
            var employerService = EmployerServieStub(EntityHelper.Employer());
            
            var request = new GetReturnToWorkPathRequest(caseService.Object, guideService.Object, employerService.Object);

            var parameter = new GetReturnToWorkPathParameters
            {
                CaseNumber = "1390275272",
                DiagnosisCode = "E11",
                EmployerId = "2",
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.ExpectationFailed, response.StatusCode);
        }

        [TestMethod]
        public void GetReturnToWorkPath_PrimaryPathIdNotFound_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            AbsenceSoft.Data.Cases.DisabilityInfo info = new AbsenceSoft.Data.Cases.DisabilityInfo();
            info.PrimaryDiagnosis = new DiagnosisCode();
            info.PrimaryDiagnosis.Code = "E11";
            var caseService = CaseServieStub(EntityHelper.Case("5bb310886e66da7c7c7418b3", "772655213", "123", "ABC", info));

            var guideService = DiagnosisGuidelinesServiceStub(EntityHelper.DiagnosisGuidelines());
            var employerService = EmployerServieStub(EntityHelper.Employer());

            var request = new GetReturnToWorkPathRequest(caseService.Object, guideService.Object, employerService.Object);

            var parameter = new GetReturnToWorkPathParameters
            {
                CaseNumber = "1390275272",
                DiagnosisCode = "E11",
                EmployerId = "2",
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Primary return to work path was not found for the provided case", response.Message);
            Assert.AreEqual(HttpStatusCode.ExpectationFailed, response.StatusCode);
        }

        [TestMethod]
        public void GetReturnToWorkPath_InvalidEmployer_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();

            var caseService = CaseServieStub(EntityHelper.Case()); // null case
            var guideService = DiagnosisGuidelinesServiceStub(EntityHelper.DiagnosisGuidelines());
            var employerService = EmployerServieStub(null);
            
            var request = new GetReturnToWorkPathRequest(caseService.Object, guideService.Object, employerService.Object);

            var parameter = new GetReturnToWorkPathParameters
            {
                CaseNumber = "1390275272",
                DiagnosisCode = "E11",
                EmployerId = "2",
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Employer is not valid or guidelines data feature is not enabled.", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetReturnToWorkPath_Guidelines_DiagnosisGuidelines()
        {
            // Arrange
            var user = EntityHelper.UserStub();

            AbsenceSoft.Data.Cases.DisabilityInfo info = new AbsenceSoft.Data.Cases.DisabilityInfo();
            info.PrimaryDiagnosis = new DiagnosisCode();
            info.PrimaryDiagnosis.Code = "E11";
            info.PrimaryPathId = "2569f6a6-67a7-4752-bc4c-fe24a5a76ae6";

            var caseService = CaseServieStub(EntityHelper.Case("5bb310886e66da7c7c7418b3", "772655213", "123", "ABC", info));
            var guideService = DiagnosisGuidelinesServiceStub(EntityHelper.DiagnosisGuidelines());
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var request = new GetReturnToWorkPathRequest(caseService.Object, guideService.Object, employerService.Object);

            var parameter = new GetReturnToWorkPathParameters
            {
                CaseNumber = "1390275272",
                DiagnosisCode = "E11",
                EmployerId = "2",
            };

            // Act
            var response = request.Handle(parameter);

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("E11", response.Data.Code);
            Assert.AreEqual("Type 2 diabetes mellitus", response.Data.Description);
            Assert.AreEqual(1, response.Data.RtwBestPractices.Count);
        }

        private Mock<ICaseService> CaseServieStub(AbsenceSoft.Data.Cases.Case cAse, List<PolicySummary> ps = null, List<ValidationMessage> pv = null, CaseNote note = null)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);

            caseService
                .Setup(x => x.GetEmployeePolicySummaryByCaseId(It.IsAny<string>(), null))
                .Returns(ps);

            caseService
                .Setup(x => x.PreValidateTimeOffRequest(It.IsAny<string>(), It.IsAny<List<IntermittentTimeRequest>>()))
                .Returns(pv);

            caseService
                .Setup(x => x.UpdateCase(It.IsAny<AbsenceSoft.Data.Cases.Case>(), null))
                .Returns(cAse);

            caseService
                .Setup(x => x.CreateOrModifyTimeOffRequest(It.IsAny<AbsenceSoft.Data.Cases.Case>(), It.IsAny<List<IntermittentTimeRequest>>()))
                .Returns(cAse);

            caseService
                .Setup(x => x.IntermittentAbsenceEntryNote(It.IsAny<AbsenceSoft.Data.Cases.Case>(), It.IsAny<AbsenceSoft.Data.Cases.IntermittentTimeRequest>(), It.IsAny<User>()))
                .Returns(note);

            return caseService;
        }

        private Mock<IDiagnosisGuidelinesService> DiagnosisGuidelinesServiceStub(DiagnosisGuidelines guidelines)
        {
            var guideService = new Mock<IDiagnosisGuidelinesService>();

            guideService
                .Setup(x => x.GetDiagnosisGuidelines(It.IsAny<string>()))
                .Returns(guidelines);

            return guideService;
        }

        private Mock<IEmployerService> EmployerServieStub(Customers.Employer employer)
        {
            var employerService = new Mock<IEmployerService>();

            employerService
                .Setup(x => x.GetById(It.IsAny<string>()))
                .Returns(employer);

            return employerService;
        }

        private Mock<IEmployeeService> EmployeeServieStub(Customers.Employee employee)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee);

            return employeeService;
        }

        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }
    }
}