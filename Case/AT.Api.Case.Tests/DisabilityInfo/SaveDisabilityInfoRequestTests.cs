﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft;
using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Security;
using AT.Api.Core.Exceptions;
using Moq;
using Customers = AbsenceSoft.Data.Customers;
using AT.Api.Case;
using AT.Api.Case.Requests.DisabilityInfo;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AT.Api.Case.Models.DisabilityInfo;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Data.Notes;
using System.Threading.Tasks;
using System.Net;
using AbsenceSoft.Logic.Customers.Contracts;
using System.Collections.Generic;

namespace AT.Api.Case.Tests.DisabilityInfo
{
    [TestClass]
    public class SaveDisabilityInfoRequestTests
    {
        [TestMethod]
        public void SaveDisabilityInfo_InvalidCaseId_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(null, null);
            var guideService = DiagnosisGuidelinesServiceStub(null);
            var employerService = EmployerServieStub(null);
            
            var request = new SaveDisabilityInfoRequest(caseService.Object, guideService.Object, employerService.Object);

            var parameter = new SaveDisabilityInfoParameters
            {
                CaseNumber = "",
                SaveDisabilityInfo = new SaveDisabilityInfoModel() { PrimaryDiagnosisCode = "009", SecondaryDiagnosisCodes = new List<string>(0) { "009.1" }, BodyPartCode = "9", Depression = false, Diabetes = true, Smoker = false, Obesity = false },
                EmployerId = "2",
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case Number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveDisabilityInfo_CaseNotFound_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(null, null); // null case
            var diagnosisInfo = EntityHelper.DisabilityInfo();
            var guideService = DiagnosisGuidelinesServiceStub(EntityHelper.DiagnosisGuidelines(), diagnosisInfo.CoMorbidityGuidelineDetail, diagnosisInfo.PrimaryDiagnosis);
            var employerService = EmployerServieStub(EntityHelper.Employer());
            
            var request = new SaveDisabilityInfoRequest(caseService.Object, guideService.Object, employerService.Object);

            var parameter = new SaveDisabilityInfoParameters
            {
                CaseNumber = "1390275272",
                SaveDisabilityInfo = new SaveDisabilityInfoModel() { PrimaryDiagnosisCode = "009", SecondaryDiagnosisCodes = new List<string>(0) { "009.1" },BodyPartCode = "9", Depression = false, Diabetes = true, Smoker = false, Obesity = false },
                EmployerId = "2",
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case not found for the given case number", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }               

        [TestMethod]
        public void SaveDisabilityInfo_DisabilityInfo_Success()
        {
            // Arrange
            var user = EntityHelper.UserStub();

            var diagnosisInfo = EntityHelper.DisabilityInfo();

            var caseService = CaseServieStub(EntityHelper.Case("5bb310886e66da7c7c7418b3", "772655213", "123", "ABC", diagnosisInfo));
            
            var guideService = DiagnosisGuidelinesServiceStub(EntityHelper.DiagnosisGuidelines(), diagnosisInfo.CoMorbidityGuidelineDetail, diagnosisInfo.PrimaryDiagnosis);

            var employerService = EmployerServieStub(EntityHelper.Employer());
            var request = new SaveDisabilityInfoRequest(caseService.Object, guideService.Object, employerService.Object);

            var parameter = new SaveDisabilityInfoParameters
            {
                CaseNumber = "1390275272",
                SaveDisabilityInfo = new SaveDisabilityInfoModel() { PrimaryDiagnosisCode = "009", SecondaryDiagnosisCodes = new List<string>(0) { "009.1" }, BodyPartCode = "9", Depression = false, Diabetes = true, Smoker = false, Obesity = false },
                EmployerId = "2",
            };

            // Act
            var response = request.Handle(parameter);

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual("Disability information saved successfully", response.Message);
        }

        private Mock<ICaseService> CaseServieStub(AbsenceSoft.Data.Cases.Case cAse, List<PolicySummary> ps = null, List<ValidationMessage> pv = null, CaseNote note = null)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);

            caseService
                .Setup(x => x.GetEmployeePolicySummaryByCaseId(It.IsAny<string>(), null))
                .Returns(ps);

            caseService
                .Setup(x => x.PreValidateTimeOffRequest(It.IsAny<string>(), It.IsAny<List<IntermittentTimeRequest>>()))
                .Returns(pv);

            caseService
                .Setup(x => x.UpdateCase(It.IsAny<AbsenceSoft.Data.Cases.Case>(), null))
                .Returns(cAse);

            caseService
                .Setup(x => x.CreateOrModifyTimeOffRequest(It.IsAny<AbsenceSoft.Data.Cases.Case>(), It.IsAny<List<IntermittentTimeRequest>>()))
                .Returns(cAse);

            caseService
                .Setup(x => x.IntermittentAbsenceEntryNote(It.IsAny<AbsenceSoft.Data.Cases.Case>(), It.IsAny<AbsenceSoft.Data.Cases.IntermittentTimeRequest>(), It.IsAny<User>()))
                .Returns(note);

            return caseService;
        }

        private Mock<IDiagnosisGuidelinesService> DiagnosisGuidelinesServiceStub(DiagnosisGuidelines guidelines = null, AbsenceSoft.Data.Cases.CoMorbidityGuideline coMorbidityguidlines = null, DiagnosisCode code = null)
        {
            var guideService = new Mock<IDiagnosisGuidelinesService>();

            guideService
                .Setup(x => x.GetDiagnosisGuidelines(It.IsAny<string>()))
                .Returns(guidelines);

            guideService
                .Setup(x => x.GetCoMorbidityGuideline(It.IsAny<string>(), It.IsAny<AbsenceSoft.Data.Cases.CoMorbidityGuideline.CoMorbidityArgs>()))
                .Returns(coMorbidityguidlines);

            guideService
                .Setup(x => x.GetByCode(It.IsAny<string>()))
                .Returns(code);

            return guideService;
        }

        private Mock<IEmployerService> EmployerServieStub(Customers.Employer employer)
        {
            var employerService = new Mock<IEmployerService>();

            employerService
                .Setup(x => x.GetById(It.IsAny<string>()))
                .Returns(employer);

            return employerService;
        }

        private Mock<IEmployeeService> EmployeeServieStub(Customers.Employee employee)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee);

            return employeeService;
        }

        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }
    }
}