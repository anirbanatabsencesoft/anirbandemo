﻿using AbsenceSoft;
using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.RiskProfiles;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Pay;
using AbsenceSoft.Models;
using AT.Api.Case.Models;
using AT.Api.Case.Models.CaseDeterminationModel;
using AT.Api.Case.Models.CreateContact;
using AT.Api.Case.Models.EligibityModel;
using AT.Api.Case.Models.Sharp;
using AT.Api.Case.Models.WorkReleted;
using Moq;
using System;
using System.Collections.Generic;
using static AbsenceSoft.Data.Cases.AccommodationInteractiveProcess;
using Cases = AbsenceSoft.Data.Cases;
using Customers = AbsenceSoft.Data.Customers;
using Workflows = AbsenceSoft.Data.Workflows;

namespace AT.Api.Case.Tests
{
    public static class EntityHelper
    {
        public static CaseNote CaseNote(
            string id = "5bb310886e66da7c7c7418b3",
            string Notes = "test Note",
            string customerId = "Test",
            string employerId = "Test",
            Guid accomId = new Guid(),
            string sAnswer = null,
            bool? answer = null,
            string questionId = null,
            Cases.Case theCase = null,
            User user = null,
            DateTime? date = null,
            NoteCategoryEnum? category = null)
        {
            var caseNote = new CaseNote
            {
                Id = id,
                Notes = Notes,
                CustomerId = customerId,
                EmployerId = employerId,
                CreatedBy = user,
                Category = category
            };

            caseNote.Metadata.SetRawValue("AccommQuestionId", questionId);
            caseNote.Metadata.SetRawValue("AccommId", accomId);
            caseNote.Metadata.SetRawValue("Answer", answer);
            caseNote.Metadata.SetRawValue("SAnswer", sAnswer);

            return caseNote;
        }

        public static AccommodationQuestion AccommodationQuestion(
            string questionId = "QuestionId01",
            string question = "Question01")
        {
            return new AccommodationQuestion
            {
                Question = question,
                Id = questionId
            };
        }

        public static Step Step(
            string questionId = "QuestionId01",
            bool? answer = null,
            string sanswer = null,
            AccommodationQuestion accommodationQuestion = null)
        {
            return new Step
            {
                QuestionId = questionId,
                Answer = answer,
                SAnswer = sanswer,
                Question = accommodationQuestion
            };
        }

        public static AccommodationInteractiveProcess AccommodationInteractiveProcess(
            Guid id = new Guid(),
            List<Step> steps = null)
        {
            return new AccommodationInteractiveProcess
            {
                Id = id,
                Steps = steps
            };
        }

        public static Accommodation Accommodation(
            Guid id = new Guid(),
            AccommodationInteractiveProcess interactiveProcess = null)
        {
            return new Accommodation
            {
                Id = id,
                InteractiveProcess = interactiveProcess
            };
        }

        public static AccommodationRequest AccommodationRequest(List<Accommodation> accommodations = null,
            CaseStatus status = CaseStatus.Open)
        {
            return new AccommodationRequest
            {
                Accommodations = accommodations,
                Status = status
            };
        }

        public static CaseSummary caseSummary(
          EligibilityStatus Eligibility = EligibilityStatus.Approved,
          AdjudicationStatus detemination = AdjudicationStatus.Approved,
          CaseType CaseType = CaseType.Intermittent,
          List<CaseSummaryPolicy> Policies = null
            )
        {

            return new CaseSummary
            {
                Eligibility = Eligibility,
                Determination = detemination,
                CaseType = CaseType,
                Policies = Policies
            };
        }
        public static List<CaseSegment> CaseSegmentList(
          DateTime? StartDate = null,
          DateTime? EndDate = null,
          CaseType Type = CaseType.Administrative,
          List<AppliedPolicy> AppliedPolicies = null,
          CaseStatus Status = CaseStatus.Open)
        {
            var list = new List<CaseSegment>();
            var model = new CaseSegment
            {
                StartDate = DateTime.Now.AddMonths(2),
                EndDate = DateTime.Now.AddMonths(2),
                Type = Type,
                AppliedPolicies = AppliedPolicies,
                Status = Status
            };

            list.Add(model);
            return list;
        }
        public static List<AppliedPolicy> AppliedPolicyList(
          Policy Policy = null,
          PolicyAbsenceReason PolicyReason = null,
          EligibilityStatus Status = EligibilityStatus.Eligible,
          DateTime? StartDate = null,
          DateTime? EndDate = null)
        {
            var policy = new Policy { Name = "PolicyName", Code = "PolicyCode" };
            var list = new List<AppliedPolicy>();
            var model = new AppliedPolicy
            {
                StartDate = DateTime.Now.AddMonths(2),
                EndDate = DateTime.Now.AddMonths(2),
                PolicyReason = PolicyReason,
                Policy = policy,
                Status = Status
            };

            list.Add(model);
            return list;
        }
        public static List<CaseSummaryPolicy> CaseSummaryPolicyList(
           string PolicyCode = "PolicyCode",
           string PolicyName = "PolicyName",
           List<CaseSummaryPolicyDetail> Detail = null,
           List<string> Messages = null)
        {
            var list = new List<CaseSummaryPolicy>();
            var model = new CaseSummaryPolicy
            {
                PolicyCode = PolicyCode,
                PolicyName = PolicyName,
                Detail = Detail,
                Messages = Messages
            };

            list.Add(model);
            return list;
        }

        public static List<DenialReason> DenialReasonList(
            string Description= "Elimination Period", 
            int? Order=1,
            string Code= "ELIMINATIONPERIOD",
            DenialReasonTarget Target=DenialReasonTarget.Policy,
            List<string> PolicyType= null,
            List<string> PolicyCodes=null)
        {
            var list = new List<DenialReason>();
            var model = new DenialReason
            {
                Description = Description,
                Order = Order,
                Target = Target,
                PolicyTypes = new List<string> { "Elimination Period" },
                PolicyCodes= new List<string> { "ELIMINATIONPERIOD" },
                Code= Code
            };
            list.Add(model);
            return list;
        }
        public static Cases.Case Case(
            string id = "5bb310886e66da7c7c7418b3",
            string casenumber = "772655213",
            string assignedToId = "123",
            string assignedToName = "ABC",
            Cases.DisabilityInfo info = null,
            List<CustomField> customFields = null,
            Customers.Employee employee = null,
            EmployeeContact contact = null,
            AccommodationRequest accommodationRequest = null,
            EmployeeContact CaseReporter = null,
            AbsenceReason reason = null,
            List<CaseSegment> segments = null,
            CaseSummary CaseSummary = null)
        {
            return new Cases.Case
            {
                Id = id,
                CaseNumber = casenumber,
                AssignedToId = assignedToId,
                AssignedToName = assignedToName,
                Disability = info,
                CustomFields = customFields,
                Employee = employee ?? Employee(),
                CaseReporter = CaseReporter,
                Contact = contact,
                Reason = new AbsenceReason { Name = "None", Code = "EHC" },
                AccommodationRequest = accommodationRequest,
                Segments = segments,
                Summary = CaseSummary
            };
        }

        public static List<CaseDeterminationPolicy> CaseDeterminationPolicyList(
            string PolicyCode = "PolicyCode",
            bool ApplyStatus = false,
            string DeterminationDenialReasonCode = "DeterminationDenialReasonCode",            
            string DenialExplanation = "DenialExplanation")
        {
            var CaseDeterminationPolicyList = new List<CaseDeterminationPolicy>();
            var CaseDeterminationPolicy = new CaseDeterminationPolicy
            {
                PolicyCode = PolicyCode,
                DeterminationDenialReasonCode = DeterminationDenialReasonCode,
                DenialExplanation = DenialExplanation
            };
            CaseDeterminationPolicyList.Add(CaseDeterminationPolicy);

            return CaseDeterminationPolicyList;
        }
        public static CaseDeterminationModel CaseDetermination(
           DateTime? StartDate = null,
           DateTime? EndDate = null,
           string DeterminationDenialReasonCode = "DeterminationDenialReasonCode",
           string DeterminationDenialReasonName = "DeterminationDenialReasonName",
           string DenialExplanation = "DenialExplanation",
           AdjudicationStatus DeterminationStatus = AdjudicationStatus.Approved,
           List<CaseDeterminationPolicy> Policies = null)

        {


            return new CaseDeterminationModel
            {
                StartDate = DateTime.Now.AddMonths(2),
                EndDate = DateTime.Now.AddMonths(3),
                DeterminationStatus=DeterminationStatus,
                Policies = Policies
            };
        }
        public static ChangeCaseModel ChangeCase(
            CaseType caseType = CaseType.Administrative,
            DateTime? StartDate = null,
            DateTime? EndDate = null,
            bool IsStartDateNew = false,
            bool IsEndDateNew = false,
            bool ModifyAccommodations = true,
            bool ModifyDecisions = true,
            BaseWorkScheduleModel BaseWorkSchedule = null)
        {
            return new ChangeCaseModel
            {
                CaseType = caseType,
                StartDate = DateTime.Now.AddMonths(1),
                EndDate = DateTime.Now.AddMonths(2),
                IsStartDateNew = IsStartDateNew,
                IsEndDateNew = IsEndDateNew,
                ModifyAccommodations = ModifyAccommodations,
                ModifyDecisions = ModifyDecisions,
                WorkSchedule = new BaseWorkScheduleModel { Times = new List<BaseTimeModel>() }
            };
        }


        public static EmployeeContact caseReportersAbsenceCase(
            string CompanyName = "SUHAG",
            string ContactTypeCode = "HR",
            string EmployeeNumber = "100000016",
            string Title = "Peggy Bundy",
            string FirstName = "Peggy",
            string LastName = "Bundy",
            string WorkPhone = "3032222222",
            Contact Contact = null)
        {
            return new EmployeeContact
            {
                ContactTypeCode = ContactTypeCode,
                EmployeeNumber = EmployeeNumber,
                Contact = new Contact
                {
                    FirstName = "FirstName01",
                    LastName = "LastName01",
                    CompanyName = "CompanyName01",
                    Title = "Title01",
                    WorkPhone = "WorkPhone01",

                }

            };
        }
        public static EmployeeContact caseConatctAbsenceCase(
            string CompanyName = "SUHAG",
            string ContactTypeCode = "HR",
            string EmployeeNumber = "100000016",
            string Title = "Peggy Bundy",
            string FirstName = "Peggy",
            string LastName = "Bundy",
            string WorkPhone = "3032222222",
            Contact Contact = null)
        {
            return new EmployeeContact
            {
                ContactTypeCode = ContactTypeCode,
                MilitaryStatus = MilitaryStatus.Civilian,
                Contact = new Contact
                {
                    FirstName = "FirstName01",
                    LastName = "LastName01",
                    DateOfBirth = DateTime.Now.AddMonths(-10),
                }
            };
        }

        public static CaseReporterModel caseReporters(
           string CompanyName = "SUHAG",
           string ContactTypeCode = "HR",
           string EmployeeNumber = "100000016",
           string Title = "Peggy Bundy",
           string FirstName = "Peggy",
           string LastName = "Bundy",
           string WorkPhone = "3032222222")
        {
            return new CaseReporterModel
            {
                CompanyName = CompanyName,
                ContactTypeCode = ContactTypeCode,
                EmployeeNumber = EmployeeNumber,
                Title = Title,
                FirstName = FirstName,
                LastName = LastName,
                WorkPhone = WorkPhone
            };
        }

        public static CaseDatesModel caseDates(
            DateTime? ActualDeliveryDate = null,
            DateTime? AdoptionDate = null,
            DateTime? BondingStartDate = null,
            DateTime? BondingEndDate = null,
            DateTime? ExpectedDeliveryDate = null)
        {
            return new CaseDatesModel
            {
                ActualDeliveryDate = DateTime.Now.AddMonths(1),
                AdoptionDate = DateTime.Now.AddMonths(2),
                BondingStartDate = DateTime.Now.AddMonths(2),
                BondingEndDate = DateTime.Now.AddMonths(2),
                ExpectedDeliveryDate = DateTime.Now.AddMonths(3)
            };
        }

        public static CaseModel CaseMockModel(
           string caseNumber = "037",
            CaseType caseType = CaseType.Intermittent,
           string employeeNumber = "000000304",
           string reasonCode = "ECH",
           string shortDescription = "SHORT DESC",
           DateTime? startDate = null,
           DateTime? endDate = null,
           string summary = "LONG DESC",
           CaseReporterModel caseReporters = null,
           CaseDatesModel caseDates = null,
           CaseRelationshipModel caseRelationships = null,
           CaseFlagModel caseFlags = null)
        {
            return new CaseModel
            {
                CaseNumber = caseNumber,
                CaseType = caseType,
                EmployeeNumber = employeeNumber,
                ReasonCode = reasonCode,
                ShortDescription = shortDescription,
                StartDate = DateTime.Now.AddMonths(10),
                EndDate = endDate,
                Summary = summary,
                CaseReporter = caseReporters,
                CaseDates = caseDates,
                CaseRelationship = caseRelationships,
                CaseFlags = caseFlags
            };
        }

        public static CaseFlagModel CaseFlag(
           bool IsWorkRelated = true,
           bool WillUseBonding = false,
           bool MedicalComplications = true)
        {
            return new CaseFlagModel
            {
                IsWorkRelated = IsWorkRelated,
                WillUseBonding = WillUseBonding,
                MedicalComplications = MedicalComplications
            };
        }

        public static CaseRelationshipModel CaseRelationship(
            string FirstName = "PregDisNoQualMale",
            string LastName = "TestHI",
            DateTime? DateOfBirth = null,
            MilitaryStatus MilitaryStatus = MilitaryStatus.Civilian,
            string TypeCode = "2")
        {
            return new CaseRelationshipModel
            {
                FirstName = FirstName,
                LastName = LastName,
                DateOfBirth = DateTime.Now.AddMonths(-20),
                MilitaryStatus = MilitaryStatus,
                TypeCode = TypeCode
            };
        }

        public static Customer Customer(
            string id = "CustomerId01",
            string name = "CustomerName01")
        {
            return new Customer
            {
                Id = id,
                Name = name
            };
        }

        public static List<Policy> PolicyList(
            string Name = "Name01",
            string Description = "Description01",
            DateTime? EffectiveDate = null,
            PolicyType PolicyType = PolicyType.Company,
            string WorkCountry = "US",
            string WorkState = "WorkState01",
            string ResidenceCountry = "US",
            Gender Gender = Gender.Male)
        {
            var PolicyList = new List<Policy>();
            var policy = new Policy
            {
                Name = Name,
                Description = Description,
                EffectiveDate = DateTime.Now.AddMonths(3),
                PolicyType = PolicyType,
                WorkCountry = WorkCountry,
                WorkState = WorkState,
                ResidenceCountry = ResidenceCountry,
                Gender = Gender
            };
            PolicyList.Add(policy);

            return PolicyList;
        }

        public static WorkReletedModel WorkReleted(
           DateTime? IllnessOrInjuryDate = null,
           string InjuryLocation = "InjuryLocation01",
           string WhereOccurred = "WhereOccurred01",
           bool Reportable = true,
           bool? HealthCarePrivate = false,
           WorkRelatedCaseClassification Classification = WorkRelatedCaseClassification.Death,
           WorkRelatedTypeOfInjury? TypeOfInjury = WorkRelatedTypeOfInjury.HearingLoss,
           string InjuredBodyPart = "InjuredBodyPart01",
           int? DaysAwayFromWork = 10,
           int? OverrideDaysAwayFromWork = 12,
           int? DaysOnJobTransferOrRestriction = 15,
           int? CalculatedDaysOnJobTransferOrRestriction = 20,
           CreateContactModel ProviderContact = null,
           bool? EmergencyRoom = false,
           bool? HospitalizedOvernight = false,
           TimeOfDay? TimeEmployeeBeganWork = null,
           TimeOfDay? TimeOfEvent = null,
           string ActivityBeforeIncident = "ActivityBeforeIncident04",
           string WhatHappened = "WhatHappened01",
           string InjuryOrIllness = "InjuryOrIllness01",
           string WhatHarmedTheEmployee = "WhatHarmedTheEmployee01",
           DateTime? DateOfDeath = null,
           bool? Sharps = true,
           SharpsInfoModel sharp = null)
        {
            var model = new WorkReletedModel
            {
                IllnessOrInjuryDate = IllnessOrInjuryDate,
                InjuryLocation = InjuryLocation,
                WhereOccurred = WhereOccurred,
                Reportable = Reportable,
                HealthCarePrivate = HealthCarePrivate,
                Classification = Classification,
                TypeOfInjury = TypeOfInjury,
                InjuredBodyPart = InjuredBodyPart,
                DaysAwayFromWork = DaysAwayFromWork,
                OverrideDaysAwayFromWork = OverrideDaysAwayFromWork,
                DaysOnJobTransferOrRestriction = DaysOnJobTransferOrRestriction,
                ProviderContact = new CreateContactModel
                {
                    FirstName = "FirstName01",
                    LastName = "LastName01",
                    ContactTypeCode = "ContactTypeCode01",
                    Address = new Models.Address.AddressModel
                    {
                        Name = "Name01",
                        Address1 = "Address101",
                        Address2 = "Address202",
                        City = "City01",
                        Country = "Country01",
                        PostalCode = "PostalCode01",
                        State = "State01"
                    }

                },
                EmergencyRoom = EmergencyRoom,
                HospitalizedOvernight = HospitalizedOvernight,
                ActivityBeforeIncident = ActivityBeforeIncident,
                WhatHappened = WhatHappened,
                InjuryOrIllness = InjuryOrIllness,
                WhatHarmedTheEmployee = WhatHarmedTheEmployee,
                DateOfDeath = DateTime.Now.AddMonths(5),
                Sharps = Sharps,
                sharp = new SharpsInfoModel
                {
                    BodyFluidInvolved = "BodyFluidInvolved",
                    BrandOfSharp = "BrandOfSharp",
                    ExposureDetail = "ExposureDetail",
                    HaveEngineeredInjuryProtection = false,
                    InjuryLocation = new List<WorkRelatedSharpsInjuryLocation>(),
                    JobClassification = WorkRelatedSharpsJobClassification.Doctor,
                    JobClassificationOther = "JobClassificationOther",
                    LocationAndDepartment = WorkRelatedSharpsLocation.ClinicalLaboratory,
                    LocationAndDepartmentOther = "LocationAndDepartmentOther",
                    Procedure = WorkRelatedSharpsProcedure.Cutting,
                    ProcedureOther = "ProcedureOther",
                }
            };
            return model;
        }

        public static WorkRelatedInfo WorkRelatedInfo(
           DateTime? IllnessOrInjuryDate = null,
           string InjuryLocation = "InjuryLocation01",
           string WhereOccurred = "WhereOccurred01",
           bool Reportable = true,
           bool? HealthCarePrivate = false,
           WorkRelatedCaseClassification Classification = WorkRelatedCaseClassification.Death,
           WorkRelatedTypeOfInjury? TypeOfInjury = WorkRelatedTypeOfInjury.HearingLoss,
           string InjuredBodyPart = "InjuredBodyPart01",
           int? DaysAwayFromWork = 10,
           int? OverrideDaysAwayFromWork = 12,
           int? DaysOnJobTransferOrRestriction = 15,
           int? CalculatedDaysOnJobTransferOrRestriction = 20,
           CreateContactModel ProviderContact = null,
           bool? EmergencyRoom = false,
           bool? HospitalizedOvernight = false,
           TimeOfDay? TimeEmployeeBeganWork = null,
           TimeOfDay? TimeOfEvent = null,
           string ActivityBeforeIncident = "ActivityBeforeIncident04",
           string WhatHappened = "WhatHappened01",
           string InjuryOrIllness = "InjuryOrIllness01",
           string WhatHarmedTheEmployee = "WhatHarmedTheEmployee01",
           DateTime? DateOfDeath = null,
           bool? Sharps = true,
           SharpsInfoModel sharp = null)
        {
            var model = new WorkRelatedInfo
            {
                IllnessOrInjuryDate = IllnessOrInjuryDate,
                InjuryLocation = InjuryLocation,
                WhereOccurred = WhereOccurred,
                Reportable = Reportable,
                HealthCarePrivate = HealthCarePrivate,
                Classification = Classification,
                TypeOfInjury = TypeOfInjury,
                InjuredBodyPart = InjuredBodyPart,
                DaysAwayFromWork = DaysAwayFromWork,
                OverrideDaysAwayFromWork = OverrideDaysAwayFromWork,
                DaysOnJobTransferOrRestriction = DaysOnJobTransferOrRestriction,
                Provider = new EmployeeContact
                {
                    ContactTypeCode = "ContactTypeCode01",
                    Contact = new Contact
                    {
                        FirstName = "FirstName01",
                        LastName = "LastName01",
                        Address = new Address
                        {
                            Name = "Name01",
                            Address1 = "Address101",
                            Address2 = "Address202",
                            City = "City01",
                            Country = "Country01",
                            PostalCode = "PostalCode01",
                            State = "State01"
                        }
                    }
                },
                EmergencyRoom = EmergencyRoom,
                HospitalizedOvernight = HospitalizedOvernight,
                ActivityBeforeIncident = ActivityBeforeIncident,
                WhatHappened = WhatHappened,
                InjuryOrIllness = InjuryOrIllness,
                WhatHarmedTheEmployee = WhatHarmedTheEmployee,
                DateOfDeath = DateTime.Now.AddMonths(5),
                Sharps = Sharps,
                SharpsInfo = new WorkRelatedSharpsInfo
                {
                    BodyFluidInvolved = "BodyFluidInvolved",
                    BrandOfSharp = "BrandOfSharp",
                    ExposureDetail = "ExposureDetail",
                    HaveEngineeredInjuryProtection = false,
                    InjuryLocation = new List<WorkRelatedSharpsInjuryLocation>(),
                    JobClassification = WorkRelatedSharpsJobClassification.Doctor,
                    JobClassificationOther = "JobClassificationOther",
                    LocationAndDepartment = WorkRelatedSharpsLocation.ClinicalLaboratory,
                    LocationAndDepartmentOther = "LocationAndDepartmentOther",
                    Procedure = WorkRelatedSharpsProcedure.Cutting,
                    ProcedureOther = "ProcedureOther",
                }

            };
            return model;
        }

        public static CustomField CustomField(
            string id = "FieldId01",
            string code = "Code01",
            string name = "Name01",
            CustomFieldType dataType = CustomFieldType.Text,
            CustomFieldValueType valueType = CustomFieldValueType.UserEntered,
            string selectedValue = "Value01",
            List<ListItem> listItems = null,
            string description = "Description01",
            bool isCollectedAtIntake = false)
        {
            var customField = new CustomField
            {
                Id = id,
                Code = code,
                DataType = dataType,
                ValueType = valueType,
                SelectedValue = selectedValue,
                ListValues = listItems,
                Description = description,
                Name = name,
                Target = EntityTarget.Employee,
                IsCollectedAtIntake = isCollectedAtIntake
            };

            return customField;
        }

        public static List<Case.Models.NoteCategories.NoteCategory> NoteCategoryList(
             int id = 1,
             string description = "description01")
        {
            var _NoteCategoryList = new List<Case.Models.NoteCategories.NoteCategory>();
            var _NoteCategory = new Case.Models.NoteCategories.NoteCategory
            {
                noteId = id,
                Description = description
            };

            _NoteCategoryList.Add(_NoteCategory);
            return _NoteCategoryList;
        }

        public static CaseSegment CaseSegment(
            DateTime? StartDate,
            DateTime? EndDate,
            List<IntermittentTimeRequest> UserRequests = null,
            CaseType Type = CaseType.Intermittent,
            CaseStatus Status = CaseStatus.Open
        )
        {
            return new CaseSegment
            {
                StartDate = StartDate ?? DateTime.Now.AddDays(-30),
                EndDate = EndDate ?? DateTime.Now,
                UserRequests = UserRequests,
                Type = Type,
                Status = Status
            };
        }

        public static IntermittentTimeRequest IntermittentTimeRequest(
            string notes = "Test Note",
            string requestDate = "2018-10-10",
            int pending = 0,
            int approved = 60,
            int denied = 0,
            int totalMinutes = 0,
            List<IntermittentTimeRequestDetail> detail = null
            )
        {
            return new IntermittentTimeRequest
            {
                Notes = notes,
                RequestDate = DateTime.Parse(requestDate),
                TotalMinutes = totalMinutes,
                Detail = detail
            };
        }

        public static List<AccommodationType> AccommodationTypeList(
            string name = "name01",
            int order = 2,
            bool PreventNew = false,
            AccommodationDuration duration1 = new AccommodationDuration(),
            CaseType CaseTypeFlag = CaseType.Consecutive,
            bool IsDisabled = false,
            string ParentName = "ParentName01")
        {
            var AccommodationTypeList = new List<AccommodationType>();
            var AccommodationType = new AccommodationType
            {
                Name = name,
                Order = order,
                PreventNew = PreventNew,
                CaseTypeFlag = CaseTypeFlag,
                ParentName = ParentName,
                Children = new List<AccommodationType>()
            };

            AccommodationTypeList.Add(AccommodationType);
            return AccommodationTypeList;
        }

        public static IntermittentTimeRequestDetail IntermittentTimeRequestDetail(
            string policyCode = "FML",
            int pending = 0,
            int approved = 60,
            int denied = 0,
            int totalMinutes = 0
            )
        {
            return new IntermittentTimeRequestDetail
            {
                PolicyCode = policyCode,
                Pending = pending,
                Approved = approved,
                Denied = denied,
                TotalMinutes = totalMinutes
            };
        }
       
        public static PolicySummary PolicySummary(
            string policyCode = "FML",
            string policyName = "Family Leave",
            PolicyType policyType = PolicyType.FMLA
            )
        {
            return new PolicySummary
            {
                PolicyCode = policyCode,
                PolicyName = policyName,
                PolicyType = policyType
            };
        }

        public static ValidationMessage ValidationMessage(
           string message = "Success",
           ValidationType type = ValidationType.Info
           )
        {
            return new ValidationMessage
            {
                Message = message,
                ValidationType = type
            };
        }

        public static User UserStub(
            string userId = "UserId01",
            string firstName = "User",
            string lastName = "Name",
            string email = "email@email.com")
        {
            var user = new Mock<User>(MockBehavior.Loose);
            user.Setup(u => u.Id).Returns(userId);
            user.Object.FirstName = firstName;
            user.Object.LastName = lastName;
            user.Object.Email = email;

            return user.Object;
        }

        public static AppliedPolicy AppliedPolicy(
            Policy Policy = null,
            PolicyAbsenceReason PolicyReason = null,
            EligibilityStatus Status = EligibilityStatus.Eligible
         )
        {
            return new AppliedPolicy
            {
                Policy = new Policy { },
                Status = Status,
                PolicyReason = new PolicyAbsenceReason()
            };
        }

        public static Policy Policy(
            string Code = "WI-FML",
            string Name = "Wisconsin Family and Medical Leave",
            string Description = "Wisconsin Family and Medical Leave"
         )
        {
            return new Policy
            {
                Code = Code,
                Name = Name,
                Description = Description
            };
        }

        public static Employer Employer(
            string id = "EmployerId01",
            string name = "EmployerName01")
        {
            return new Employer
            {
                Id = id,
                Name = name,
                Features = new List<AppFeature>(0)
                {
                    new AppFeature { Feature = Feature.GuidelinesData, Enabled = true, EffectiveDate = DateTime.Now.AddDays(-90) },
                    new AppFeature{Feature = Feature.RiskProfile,Enabled = true,EffectiveDate = DateTime.Now.AddDays(-90)}
                }
            };
        }
        public static CaseEligibiliyModel CaseEligibiliyModel(
           string EmployeeNumber = "EmployeeNumber01",
           int CaseTypeId = 01,
           CaseType CaseType = CaseType.Consecutive,
           DateTime? StartDate = null,
           DateTime? EndDate = null,
           string AbsenceReasonCode = "ECH",
           string EmployeeRelationshipCode = "code01",
           DateTime? AdoptionDate = null,
           int MilitaryStatusId = 02,
           bool IsWorkRelated = true,
           DateTime? ExpectedDeliveryDate = null,
           DateTime? ActualDeliveryDate = null,
           bool WillUseBonding = false,
           DateTime? BondingStartDate = null,
           DateTime? BondingEndDate = null,
           ScheduleViewModel WorkSchedule = null,
           Relapse Relapse = null)
        {
            return new CaseEligibiliyModel
            {
                EmployeeNumber = EmployeeNumber,
                CaseTypeId = CaseTypeId,
                CaseType = CaseType,
                StartDate = DateTime.Now.AddMonths(1),
                EndDate = DateTime.Now.AddMonths(2),
                AbsenceReasonCode = AbsenceReasonCode,
                EmployeeRelationshipCode = EmployeeRelationshipCode,
                AdoptionDate = DateTime.Now.AddMonths(1),
                MilitaryStatusId = MilitaryStatusId,
                IsWorkRelated = IsWorkRelated,
                ExpectedDeliveryDate = DateTime.Now.AddMonths(1),
                ActualDeliveryDate = DateTime.Now.AddMonths(2),
                WillUseBonding = WillUseBonding,
                BondingStartDate = DateTime.Now.AddMonths(1),
                BondingEndDate = DateTime.Now.AddMonths(2),
                WorkSchedule = new ScheduleViewModel { },
                Relapse = new Relapse { }
            };
        }

        public static LeaveOfAbsence LeaveOfAbsence(Cases.Case theCase = null)
        {
            return new LeaveOfAbsence
            {
                Case = theCase ?? Case(segments: new List<CaseSegment>(), CaseSummary: caseSummary(Policies: new List<CaseSummaryPolicy>())),
                ActiveSegment = CaseSegment()
            };
        }

        public static CaseSegment CaseSegment(
             DateTime? StartDate = null,
             DateTime? EndDate = null,
             CaseType type = CaseType.Consecutive,
             List<AppliedPolicy> AppliedPolicies = null,
             CaseStatus Status = CaseStatus.Open,
             List<Schedule> LeaveSchedule = null,
             List<Time> Absences = null,
             List<IntermittentTimeRequest> UserRequests = null)
        {
            return new CaseSegment
            {
                StartDate = DateTime.Now.AddMonths(2),
                EndDate = DateTime.Now.AddMonths(3),
                Type = type,
                AppliedPolicies = new List<AppliedPolicy>(),
                Status = Status,
                LeaveSchedule = new List<Schedule>(),
                Absences = new List<Time>(),
                UserRequests = new List<IntermittentTimeRequest>()
            };
        }
        public static CaseEligibilityPoliciesDataModel CaseEligibilityPoliciesDataModel()
        {
            return new CaseEligibilityPoliciesDataModel
            {
                Policies = PoliciesList(),
                AvailableManualPolicies = PoliciesList()
            };
        }
        public static List<AppliedPolicyModel> PoliciesList(
          string PolicyName = "PolicyName01",
          string PolicyCode = "PolicyCode01",
          string PolicyType = "PolicyType01",
          DateTime? StartDate = null,
          DateTime? EndDate = null,
          EligibilityStatus Status = EligibilityStatus.Eligible,
          bool HasOverriddenRule = true,
          string ManuallyAppliedPolicyNotes = "ManuallyAppliedPolicyNotes01",
          bool UserIsHandlingExpandCollapse = false,
          bool Expanded = false,
          DateTime? ExhaustionDate = null,
          bool CombinedForSpouses = false,
          bool SeeSpouseCaseFlag = true,
          bool Paid = false,
          string AlternateEligibilityMessage = "AlternateEligibilityMessage01",
          List<AppliedRuleGroupModel> RuleGroups = null,
          List<CasePolicySummaryDetailModel> Adjudications = null,
          bool IsAppliedPolicy = true)
        {
            var allPolicies = new List<AppliedPolicyModel>();
            var newpolicies = new AppliedPolicyModel
            {
                PolicyName = PolicyName,
                PolicyCode = PolicyCode,
                PolicyType = PolicyType,
                StartDate = DateTime.Now.AddMonths(3),
                EndDate = DateTime.Now.AddMonths(4),
                Status = Status,
                HasOverriddenRule = HasOverriddenRule,
                ManuallyAppliedPolicyNotes = ManuallyAppliedPolicyNotes,
                Expanded = Expanded,
                ExhaustionDate = DateTime.Now.AddMonths(5),
                CombinedForSpouses = CombinedForSpouses,
                SeeSpouseCaseFlag = SeeSpouseCaseFlag,
                Paid = Paid,
                AlternateEligibilityMessage = AlternateEligibilityMessage,
                RuleGroups = new List<AppliedRuleGroupModel>(),
                Adjudications = new List<CasePolicySummaryDetailModel>(),
                IsAppliedPolicy = IsAppliedPolicy
            };
            allPolicies.Add(newpolicies);
            return allPolicies;

        }
        public static AbsenceReason AbsenceReason(
             string name = "ECH",
             string Category = "Category01",
             string HelpText = "HelpText01",
             AbsenceReasonFlag Flags = AbsenceReasonFlag.None,
             CaseType caseType = CaseType.Intermittent,
             string Country = "Country01",
             bool IsDisabled = false)
        {
            return new AbsenceReason
            {
                Name = name,
                Category = Category,
                HelpText = HelpText,
                Flags = Flags,
                CaseTypes = caseType,
                Country = Country,
                IsDisabled = IsDisabled
            };
        }

        public static List<AbsenceReason> AbsenceReasonList(
             string name = "ECH",
             string Category = "Category01",
             string HelpText = "HelpText01",
             AbsenceReasonFlag Flags = AbsenceReasonFlag.None,
             CaseType caseType = CaseType.Intermittent,
             string Country = "Country01",
             bool IsDisabled = false)
        {
            var AbsenceReasonList = new List<AbsenceReason>();
            var absenceReason = new AbsenceReason
            {
                Name = name,
                Category = Category,
                HelpText = HelpText,
                Flags = Flags,
                CaseTypes = caseType,
                Country = Country,
                IsDisabled = IsDisabled
            };

            AbsenceReasonList.Add(absenceReason);
            return AbsenceReasonList;
        }

        public static Workflows.Workflow Workflow(string id = "WorkflowId01")
        {
            return new Workflows.Workflow
            {
                Id = id
            };
        }

        public static Workflows.Event WorkflowEvent(string id = "WorkflowEventId01")
        {
            return new Workflows.Event
            {
                Id = id
            };
        }

        public static Workflows.WorkflowInstance WorkflowInstance(string id = "WorkflowInstanceId01")
        {
            return new Workflows.WorkflowInstance
            {
                Id = id
            };
        }

        public static DiagnosisGuidelines DiagnosisGuidelines(string Code = "E11", string Description = "Type 2 diabetes mellitus", DateTime? ExpirationDate = null)
        {
            return new DiagnosisGuidelines
            {
                Code = Code,
                Description = Description,
                ExpirationDate = DateTime.Parse("2018/10/10"),
                RtwBestPractices = new List<RtwBestPractice>(0) { new RtwBestPractice { Id = new Guid("2569f6a6-67a7-4752-bc4c-fe24a5a76ae6"), RtwPath = "Without hospitalization", MaxDays = 5, MinDays = 2 } },
                ActivityModifications = new List<ActivityModification>(),
                RtwSummary = new RtwSummaryGuidelines { AbsencesAtRisk = 101, AbsencesMidRange = 5, ClaimsAtRisk = 90, ClaimsMidRange = 5 },
                Chiropractical = new List<ChiropracticalGuideline>(),
                PhisicalTherapy = new List<PhisicalTherapyGuideline>()
            };
        }

        public static Cases.DisabilityInfo DisabilityInfo()
        {
            return new Cases.DisabilityInfo
            {
                PrimaryDiagnosis = new DiagnosisCode { Code = "009", Name = "Ill-defined intestinal infections", UIDescription = "009 - Ill-defined intestinal infections" },
                PrimaryPathId = "2569f6a6-67a7-4752-bc4c-fe24a5a76ae6",
                PrimaryPathText = "Without hospitalization",
                PrimaryPathMinDays = 1,
                PrimaryPathMaxDays = 2,
                PrimaryPathDaysUIText = "Without hospitalization 1 - 2 days",
                SecondaryDiagnosis = new List<DiagnosisCode>(0) { new DiagnosisCode { Code = "009.2", Name = "Infectious diarrhea", UIDescription = "009.2 - Infectious diarrhea" } },
                EmployeeJobActivity = JobClassification.Medium,
                MedicalComplications = false,
                Hospitalization = false,
                GeneralHealthCondition = "Normal",
                ConditionStartDate = DateTime.Now.Date,
                CoMorbidityGuidelineDetail = new CoMorbidityGuideline()
                {
                    Args = new CoMorbidityGuideline.CoMorbidityArgs()
                    {
                        ICDDiagnosisCodeString = "009,009.2,009.0",
                        CountryCode = "US",
                        StateCode = "WI",
                        Age = 36,
                        BodyPartCode = "9",
                        NatureOfInjuryCode = "008",
                        ICDProcedureCodeString = null,
                        HCPCSCodeString = "009",
                        CPTProcedureCodeString = null,
                        NDCProcedureCodeString = "009",
                        CF = new CoMorbidityGuideline.CoMorbidityArgs.ConfoundingFactors()
                        {
                            Depression = 0,
                            Diabetes = 1,
                            Hypertension = 1,
                            LegalRepresentation = 0,
                            Obesity = 0,
                            Smoker = 0,
                            OPIOId = 0,
                            SubstanceAbuse = 0,
                            SurgeryOrHospitalStay = 1
                        },
                        PS = new CoMorbidityGuideline.CoMorbidityArgs.ProcSummary()
                        {
                            ICDProcedureCodeString = null,
                            HCPCSCodeString = "009",
                            CPTProcedureCodeString = null,
                            NDCProcedureCodeString = "009"
                        }
                    },
                    ActualDaysLost = 28,
                    RiskAssessmentScore = 57.97M,
                    RiskAssessmentStatus = "Yellow (Cautionary)",
                    MidrangeAllAbsence = 14M,
                    AtRiskAllAbsence = 71,
                    BestPracticesDays = 7
                }

            };
        }

        public static Cases.Certification Certification()
        {
            return new Certification
            {
                Duration = 1,
                DurationType = Unit.Days,
                Frequency = 1,
                FrequencyType = Unit.Weeks,
                FrequencyUnitType = DayUnitType.Workday,
                IntermittentType = IntermittentType.Incapacity,
                IsCertificationIncomplete = true,
                StartDate = DateTime.Parse("2018/10/02"),
                EndDate = DateTime.Parse("2018/10/31"),
                Occurances = 2,
                Id = new Guid("99404677-74a6-4cd3-9253-e1ed6c7570a5")
            };
        }

        public static ContactType ContactType(string code = "Code01", string name = "CodeName01")
        {
            return new ContactType()
            {
                Code = code,
                Name = name
            };
        }

        public static Customers.Employee Employee(
           string employeeId = "EmployeeId01",
           string employeeNumber = "EmployeeNo01",
           string customerId = "CustomerId01",
           string employerId = "EmployerId01",
           string firstName = "FirstName01",
           string lastName = "LastName01",
           string workCountry = "WorkCountry01",
           string workState = "WorkState01",
           EmployeeInfo employeeInfo = null)
        {
            return new Customers.Employee
            {
                Id = employeeId,
                EmployeeNumber = employeeNumber,
                CustomerId = customerId,
                EmployerId = employerId,
                FirstName = firstName,
                LastName = lastName,
                WorkCountry = workCountry,
                WorkState = workState,
                Info = employeeInfo,
            };
        }

        public static Address Address(
         string name = "AddressName01",
         string address1 = "Address1Value01",
         string address2 = "Address2Value01",
         string city = "City01",
         string state = "State01",
         string postalCode = "PostalCode01",
         string country = "Country01")
        {
            return new Address
            {
                Name = name,
                Address1 = address1,
                Address2 = address2,
                City = city,
                State = state,
                PostalCode = postalCode,
                Country = country
            };
        }

        public static Contact Contact(
        Guid? id = null,
        string title = "Title01",
        string companyName = "Company01",
        string cellphone = "01",
        string firstname = "FirstName01",
        string lastname = "LastName01",
        string workPhone = "02",
        string homePhone = "03",
        string altEmail = "AltEmail01",
        string email = "Email01",
        DateTime? dateOfBirth = null,
        string fax = "xxx01",
        bool? isPrimary = null,
        Address address = null)
        {
            var contact = new Contact()
            {
                Id = id ?? new Guid(),
                CompanyName = companyName,
                CellPhone = cellphone,
                FirstName = firstname,
                Email = email,
                AltEmail = altEmail,
                Address = address ?? Address(),
                DateOfBirth = dateOfBirth ?? new DateTime(1985, 01, 01),
                LastName = lastname,
                HomePhone = homePhone,
                WorkPhone = workPhone,
                Title = title,
                Fax = fax,
                IsPrimary = isPrimary
            };

            return contact;
        }

        public static EmployeeContact EmployeeContact(
            string contactId = "Contact01",
            string employerNumber = "EmployeeNo01",
            string employeeId = "Employee001",
            string relatedEmpNumber = "",
            string contactTypeCode = "Type01",
            int? contactPosition = null,
            Contact contact = null,
            Customers.Employee employee = null)
        {
            var employeeContact = new EmployeeContact
            {
                Id = contactId,
                EmployeeNumber = employerNumber,
                EmployeeId = employeeId,
                ContactTypeCode = contactTypeCode,
                ContactPosition = contactPosition,
                RelatedEmployeeNumber = relatedEmpNumber,
                MilitaryStatus = MilitaryStatus.Civilian,
                Contact = contact ?? Contact(),
                Employee = employee ?? Employee()
            };

            return employeeContact;
        }

        public static EmployeeRestriction EmployeeRestriction()
        {
            var employeeRestriction = new EmployeeRestriction
            {
                CaseId = "5bb310886e66da7c7c7418b3",
                CaseNumber = "772655213",
                Case = Case("5bb310886e66da7c7c7418b3", "772655213", "123", "ABC", null, null, Employee()),
                Employee = Employee(),
                Restriction = new WorkRestriction()
                {
                    Demand = new Demand() { Name = "Walk", Id = "5bb3065a2e30e05dbc8e92fb" },
                    DemandId = "5bb3065a2e30e05dbc8e92fb",
                    Type = WorkRestrictionType.Fully,
                    Description = "Walk",
                    Dates = new AbsenceSoft.DateRange(DateTime.Now, DateTime.Now),
                    Values = new List<AppliedDemandValue<WorkRestrictionValue>>(0) { new AppliedDemandValue<WorkRestrictionValue>
                    {
                        DemandTypeId = "627ca96a-37c8-492a-a141-ee7744dc6d36",
                        DemandType = new DemandType{Name="Restrictions", Id="627ca96a-37c8-492a-a141-ee7744dc6d36",Description="test", Code="code" },
                        Text="Test",
                        Value="Test",
                        Type = DemandValueType.Text,
                        Applicable = false,
                        Schedule = new ScheduleDemand
                        {
                            Duration = 0,
                            Frequency = 1,
                            FrequencyType = Unit.Days,
                            FrequencyUnitType = DayUnitType.Workday,
                            DurationType = Unit.Days,
                            Occurances = 2
                        }
                    }
                    }
                }
            };

            return employeeRestriction;
        }

        public static CaseEvent CaseEvent(
        CaseEventType EventType = CaseEventType.ReturnToWork,
        DateTime? EventDate = null)
        {
            return new CaseEvent
            {
                EventType = EventType,
                EventDate = EventDate ?? DateTime.Now
            };
        }

        public static List<CustomField> CustomFieldList(
            EntityTarget Target = EntityTarget.Case,
            string name = "CustomField",
            string Description = "Description01",
            string Label = "Label01",
            string HelpText = "HelpText01",
            CustomFieldType DataType = CustomFieldType.Number)
        {
            var customFieldList = new List<CustomField>();
            var customfield = new CustomField
            {
                Target = Target,
                Name = name,
                Description = Description,
                Label = Label,
                HelpText = HelpText,
                DataType = DataType
            };
            customFieldList.Add(customfield);

            return customFieldList;
        }
        public static ToDoItem ToDoItem(string caseNumber = "123", ToDoItemType toDoItemType = ToDoItemType.NotSpecified)
        {
            return new ToDoItem
            {
                CaseNumber = caseNumber,
                ItemType = toDoItemType
            };
        }

        public static CasePayModel CasePayModel()
        {
            return new CasePayModel
            {
                CaseNumber = "772655213",
                ApplyOffsetsByDefault = true,
                ApprovedFrom = DateTime.Now,
                ApprovedThru = DateTime.Now.AddDays(15),
                BasePay = 60000,
                Pay = PayType.Salary,
                PayScheduleId = "0000000010",
                PayScheduleName = "PayScheduleName",
                EmployeeName = "Emp01",
                ScheduledHoursPerWeek = 40,
                ShowWaiveWaitingPeriod = false,
                TotalPaid = 45000,
                WaiveWaitingPeriod = true,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(15),
                EmployeeId = "Employee001",
                EmployerId = "Employer001",
                PayPeriods = new List<CasePayPeriodModel>(0) { new CasePayPeriodModel { Amount = 5000, PayDate = DateTime.Today, PayFrom = DateTime.Now, PayThru = DateTime.Now.AddDays(15), PayPeriodId = new Guid("627ca96a-37c8-492a-a141-ee7744dc6d36"), Status = PayrollStatus.Pending } },
                Policies = new List<CasePayPolicyModel>(0) { new CasePayPolicyModel { BenefitPercentage = 10, MaxBenefitAmount = 5000, MinBenefitAmount = 1000, PolicyCode = "001", PolicyName = "My policy", TotalPaid = 45000, TotalPaidLast52Weeks = 6000 } },
            };
        }

        public static DemandType DemandType()
        {
            return new DemandType
            {
                Name = "Restrictions",
                Id = "627ca96a-37c8-492a-a141-ee7744dc6d36",
                Description = "The impact or scope of the restriction(s)",
                Code = "RESTRICTIONS",
                Type = DemandValueType.Text,
                Order = 0,
                Items = new List<DemandItem>(0) { new DemandItem { Name = "Fully" } }
            };
        }

        public static Demand Demand()
        {
            return new Demand
            {
                Name = "Walk",
                Id = "5bb3065a2e30e05dbc8e92fb"
            };
        }

        public static AccommodationRequest Accommodation(
            string GeneralHealthCondition = "Test record for case creation",
            string Description = "Test record for case creation",
            CaseStatus Status = CaseStatus.Closed,
            DateTime? StartDate = null,
            AccommodationDuration SummaryDuration = AccommodationDuration.Permanent,
            string SummaryDurationType = "Temporary"
            )
        {
            return new AccommodationRequest
            {
                GeneralHealthCondition = GeneralHealthCondition,
                Description = Description,
                Status = Status,
                StartDate = StartDate,
                SummaryDuration = SummaryDuration.ToString(),
                SummaryDurationType = SummaryDurationType
            };
        }

        public static AccommodationRequest AccommodationRequest()
        {
            var usage = new AccommodationUsage()
            {
                StartDate = DateTime.UtcNow.AddMonths(-1),
                EndDate = DateTime.UtcNow.AddMonths(1)
            };
            return new AccommodationRequest
            {
                CaseId = "5bb310886e66da7c7c7418b3",
                CaseNumber = "772655213",
                GeneralHealthCondition = "General health condition Test",
                Description = "Description Test",
                Status = CaseStatus.Open,
                Accommodations = new List<Accommodation>(0) { new Accommodation
                {
                    Type = new AccommodationType { Code = "TypeCode01", Name = "TypeName01" },
                    Duration = AccommodationDuration.Temporary,
                    Description = "Description01",
                    Resolved = false,
                    Granted = false,
                    Usage = new List<AccommodationUsage>{ usage},
                    StartDate = DateTime.UtcNow.AddDays(2),
                    EndDate = DateTime.UtcNow.AddDays(5),
                    Determination = AdjudicationStatus.Pending,
                    MinApprovedFromDate = null,
                    MaxApprovedThruDate = null,
                    Status = CaseStatus.Open,
                    IsWorkRelated = false,
                    CancelReason = AccommodationCancelReason.AccommodationNoLongerNeeded,
                    OtherReasonDesc = string.Empty
                } },
                StartDate = DateTime.UtcNow,
                EndDate = DateTime.UtcNow.AddMonths(2),
                Determination = AdjudicationStatus.Pending,
                SummaryDuration = "Pending",
                SummaryDurationType = "Temporary"
            };
        }

        public static Accommodation Accommodation()
        {
            return new Accommodation
            {
                Type = new AccommodationType { Code = "TypeCode01", Name = "TypeName01" },
                Duration = AccommodationDuration.Temporary,
                Description = "Description01",
                Resolved = false,
                Granted = false,
                StartDate = null,
                EndDate = null,
                Determination = AdjudicationStatus.Pending,
                MinApprovedFromDate = null,
                MaxApprovedThruDate = null,
                Status = CaseStatus.Open,
                IsWorkRelated = false
            };
        }

        public static ListResults ListResults()
        {
            return new ListResults
            {
                Results = new List<ListResult>(0)
                {
                    new ListResult()
                        .Set("CreatedDate", "2018/11/15")
                        .Set("Category", "N/A")
                        .Set("CreatedBy", "Dev user")
                        .Set("Description", "Test Description")
                        .Set("CaseCloseReason", "Test CaseCloseReason")
                        .Set("ModifiedDate", "2018/11/15")
                        .Set("StatusId", "7")
                }
            };
        }

        public static CalculatedRiskProfile RiskProfile()
        {
            var riskProfile = new CalculatedRiskProfile
            {
                RiskProfile = new RiskProfile()
                {
                    Color = "Yellow",
                    Name = "Warning",
                    Description = "Description for Warning"
                }
            };
            return riskProfile;
        }
    }
}
