﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AT.Api.Core.Exceptions;
using AT.Api.Shared.Model.CustomFields;
using AT.Api.Shared.Utitilies;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace AT.Api.Case.Tests.Utilities
{
    [TestClass]
    public class CustomFieldValidatorTests
    {
        [TestMethod]
        public void ValidateCustomField_EmptyValue_ReturnsValid()
        {
            //Arrange
            var customField1 = EntityHelper.CustomField();
            var customField2 = EntityHelper.CustomField(code: "Code02", dataType: CustomFieldType.Date, valueType: CustomFieldValueType.UserEntered);
            var customFields = new List<CustomField>() { customField1, customField2 };

            var model = new SaveCustomFieldModel[] { new SaveCustomFieldModel { Code = "Code02", Value = " " } };

            //// Act
            var response = CustomFieldValidator.Validate(customFields, model);

            //Assert

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(true, response);
        }

        [TestMethod]
        public void ValidateCustomField_NullValue_ReturnsValid()
        {
            //Arrange
            var customField1 = EntityHelper.CustomField();
            var customField2 = EntityHelper.CustomField(code: "Code02", dataType: CustomFieldType.Date, valueType: CustomFieldValueType.UserEntered);
            var customFields = new List<CustomField>() { customField1, customField2 };

            var model = new SaveCustomFieldModel[] { new SaveCustomFieldModel { Code = "Code02", Value = null } };

            //// Act
            var response = CustomFieldValidator.Validate(customFields, model);

            //Assert

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(true, response);
        }


        [TestMethod]
        public void ValidateCustomField_InvalidDate_ThrowsException()
        {
            //Arrange
            var customField1 = EntityHelper.CustomField();
            var customField2 = EntityHelper.CustomField(code: "Code02", dataType: CustomFieldType.Date, valueType: CustomFieldValueType.UserEntered);
            var customFields = new List<CustomField>() { customField1, customField2 };

            var model = new SaveCustomFieldModel[] { new SaveCustomFieldModel { Code = "Code02", Value = "1" } };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                CustomFieldValidator.Validate(customFields, model));

            //Assert

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Invalid Configuration value", response.Message);
        }

        [TestMethod]
        public void ValidateCustomField_ValidDate_ReturnsValid()
        {
            //Arrange
            var customField1 = EntityHelper.CustomField();
            var customField2 = EntityHelper.CustomField(code: "Code02", dataType: CustomFieldType.Date, valueType: CustomFieldValueType.UserEntered);
            var customFields = new List<CustomField>() { customField1, customField2 };

            var model = new SaveCustomFieldModel[] { new SaveCustomFieldModel { Code = "Code02", Value = DateTime.UtcNow.ToString("MM-dd-yyyy") } };

            //// Act
            var response = CustomFieldValidator.Validate(customFields, model);

            //Assert

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(true, response);
        }

        [TestMethod]
        public void ValidateCustomField_InvalidNumber_ThrowsException()
        {
            //Arrange
            var customField1 = EntityHelper.CustomField();
            var customField2 = EntityHelper.CustomField(code: "Code02", dataType: CustomFieldType.Number, valueType: CustomFieldValueType.UserEntered);
            var customFields = new List<CustomField>() { customField1, customField2 };

            var model = new SaveCustomFieldModel[] { new SaveCustomFieldModel { Code = "Code02", Value = "text" } };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                CustomFieldValidator.Validate(customFields, model));

            //Assert

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Invalid Configuration value", response.Message);
        }

        [TestMethod]
        public void ValidateCustomField_ValidNumber_ReturnsValid()
        {
            //Arrange
            var customField1 = EntityHelper.CustomField();
            var customField2 = EntityHelper.CustomField(code: "Code02", dataType: CustomFieldType.Number, valueType: CustomFieldValueType.UserEntered);
            var customFields = new List<CustomField>() { customField1, customField2 };

            var model = new SaveCustomFieldModel[] { new SaveCustomFieldModel { Code = "Code02", Value = "1" } };

            //// Act
            var response = CustomFieldValidator.Validate(customFields, model);

            //Assert

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(true, response);
        }


        [TestMethod]
        public void ValidateCustomField_InvalidFlag_ThrowsException()
        {
            //Arrange
            var customField1 = EntityHelper.CustomField();
            var customField2 = EntityHelper.CustomField(code: "Code02", dataType: CustomFieldType.Flag, valueType: CustomFieldValueType.UserEntered);
            var customFields = new List<CustomField>() { customField1, customField2 };

            var model = new SaveCustomFieldModel[] { new SaveCustomFieldModel { Code = "Code02", Value = "text" } };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                CustomFieldValidator.Validate(customFields, model));

            //Assert

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Invalid Configuration value", response.Message);
        }

        [TestMethod]
        public void ValidateCustomField_ValidFlag_ReturnsValid()
        {
            //Arrange
            var customField1 = EntityHelper.CustomField();
            var customField2 = EntityHelper.CustomField(code: "Code02", dataType: CustomFieldType.Flag, valueType: CustomFieldValueType.UserEntered);
            var customFields = new List<CustomField>() { customField1, customField2 };

            var model = new SaveCustomFieldModel[] { new SaveCustomFieldModel { Code = "Code02", Value = "true" } };

            //// Act
            var response = CustomFieldValidator.Validate(customFields, model);

            //Assert

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(true, response);
        }


        [TestMethod]
        public void ValidateCustomField_InvalidListItem_ThrowsException()
        {
            //Arrange
            var listItems = new List<ListItem>()
            {
                new ListItem { Key = "1",Value= "Number1"},
                new ListItem { Key = "2",Value= "Number2"}
            };

            var customField1 = EntityHelper.CustomField();
            var customField2 = EntityHelper.CustomField(code: "Code02", dataType: CustomFieldType.Flag, valueType: CustomFieldValueType.SelectList, listItems: listItems);
            var customFields = new List<CustomField>() { customField1, customField2 };

            var model = new SaveCustomFieldModel[] { new SaveCustomFieldModel { Code = "Code02", Value = "text" } };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                CustomFieldValidator.Validate(customFields, model));

            //Assert

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Invalid Configuration value", response.Message);
        }

        [TestMethod]
        public void ValidateCustomField_ValidListItemKey_ReturnsValid()
        {
            //Arrange
            var listItems = new List<ListItem>()
            {
                new ListItem { Key = "1",Value= "Number1"},
                new ListItem { Key = "2",Value= "Number2"}
            };

            var customField1 = EntityHelper.CustomField();
            var customField2 = EntityHelper.CustomField(code: "Code02", dataType: CustomFieldType.Text, valueType: CustomFieldValueType.SelectList, listItems: listItems);
            var customFields = new List<CustomField>() { customField1, customField2 };

            var model = new SaveCustomFieldModel[] { new SaveCustomFieldModel { Code = "Code02", Value = "1" } };

            //// Act
            var response = CustomFieldValidator.Validate(customFields, model);

            //Assert

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(true, response);
        }

        [TestMethod]
        public void ValidateCustomField_ValidListItemValue_ReturnsValid()
        {
            //Arrange
            var listItems = new List<ListItem>()
            {
                new ListItem { Key = "1",Value= "Number1"},
                new ListItem { Key = "2",Value= "Number2"}
            };

            var customField1 = EntityHelper.CustomField();
            var customField2 = EntityHelper.CustomField(code: "Code02", dataType: CustomFieldType.Text, valueType: CustomFieldValueType.SelectList, listItems: listItems);
            var customFields = new List<CustomField>() { customField1, customField2 };

            var model = new SaveCustomFieldModel[] { new SaveCustomFieldModel { Code = "Code02", Value = "Number1" } };

            //// Act
            var response = CustomFieldValidator.Validate(customFields, model);

            //Assert

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(true, response);
        }
    }
}
