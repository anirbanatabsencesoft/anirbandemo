﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Case.Models;
using AT.Api.Case.Requests.CaseEligibility;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using cases = AbsenceSoft.Data.Cases;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Case.Tests.Requests.CaseEligibility
{
    [TestClass]
    public class CaseEligibilityRequestTest
    {
        [TestMethod]
        public void CaseEligibility_EmptyEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var customer = EntityHelper.Customer();
            var Employer = EntityHelper.Employer();
            var newCase = EntityHelper.Case();
            var LeaveOfAbsence = EntityHelper.LeaveOfAbsence();
            var employeeService = EmployeeServiceStub(employee);
            var Employerservice = EmployerServiceStub(Employer);
            var CaseModel = EntityHelper.CaseMockModel();
            var caseservice = CaseCaseServiceStub(newCase);
            var eligibilityService = CaseEligibilityServiceStub(newCase, LeaveOfAbsence);
            var request = new CaseEligibilityRequest(Employer.Id, employeeService.Object, caseservice.Object, eligibilityService.Object, Employerservice.Object);


            var parameter = new CaseEligibilityParameters
            {
                CaseModel = new CaseModel { EmployeeNumber = "" },
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CaseEligibility_EmployeeNotFound_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var customer = EntityHelper.Customer();
            var Employer = EntityHelper.Employer();
            var newCase = EntityHelper.Case();
            var LeaveOfAbsence = EntityHelper.LeaveOfAbsence();
            var Employerservice = EmployerServiceStub(Employer);
            var CaseModel = EntityHelper.CaseMockModel();
            CaseModel.EmployeeNumber = "EmployeeNumber01";
            var employeeService = EmployeeServiceStub(employee);
            var caseservice = CaseCaseServiceStub(newCase);
            var eligibilityService = CaseEligibilityServiceStub(newCase, LeaveOfAbsence);
            var request = new CaseEligibilityRequest(Employer.Id, employeeService.Object, caseservice.Object, eligibilityService.Object, Employerservice.Object);


            var parameter = new CaseEligibilityParameters
            {
                CaseModel = CaseModel,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee not found for the given employee number", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }


        [TestMethod]
        public void CaseEligibility_CaseEligibility_Success()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var customer = EntityHelper.Customer();
            var Employer = EntityHelper.Employer();
            var CaseEligibiliyModel = EntityHelper.CaseEligibiliyModel();
            var ListCaseSummaryPolicy = EntityHelper.CaseSummaryPolicyList(Detail: new List<CaseSummaryPolicyDetail>());
            var caseSummary = EntityHelper.caseSummary(Policies: ListCaseSummaryPolicy);
            var newCase = EntityHelper.Case(segments: new List<CaseSegment>(), CaseSummary: caseSummary);
            var LeaveOfAbsence = EntityHelper.LeaveOfAbsence();
            var CaseEligibilityPoliciesDataModel = EntityHelper.CaseEligibilityPoliciesDataModel();
            var Employerservice = EmployerServiceStub(Employer);
            var CaseModel = EntityHelper.CaseMockModel();
            var CaseReporter = EntityHelper.caseReporters();
            var CaseDates = EntityHelper.caseDates();
            var CaseFlag = EntityHelper.CaseFlag();
            var caseRelationships = EntityHelper.CaseRelationship();
            var employeeService = EmployeeServiceStub(employee);
            var caseservice = CaseCaseServiceNewStub(newCase);
            var eligibilityService = CaseEligibilityServiceStub(newCase, LeaveOfAbsence);
            var request = new CaseEligibilityRequest(Employer.Id, employeeService.Object, caseservice.Object, eligibilityService.Object, Employerservice.Object);


            var parameter = new CaseEligibilityParameters
            {
                CaseModel = new CaseModel()
                {
                    CaseNumber = "037",
                    EmployeeNumber = "000000304",
                    CaseType = CaseType.None,
                    ReasonCode = "ECH",
                    ShortDescription = "SHORT DESC",
                    Summary = "LONG DESC",
                    StartDate = DateTime.Now.AddMonths(1),
                    EndDate = DateTime.Now.AddMonths(2),
                    CaseReporter = CaseReporter,
                    CaseDates = CaseDates,
                    CaseRelationship = caseRelationships,
                    CaseFlags = CaseFlag
                },
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.AreEqual("Eligibility has been checked successfully", response.Message);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

        }
        private Mock<IEmployerService> EmployerServiceStub(Customers.Employer employer)
        {
            var employerService = new Mock<IEmployerService>();

            employerService.Setup(x => x.GetById(It.IsAny<string>())).Returns(employer);

            return employerService;
        }
        //public Mock<IEmployerService> employerServicestub(Customers.Customer customers, Customers.Employer employer, User user)
        //{
        //    var employerService = new Mock<IEmployerService>();
        //    var CustomFieldlist = EntityHelper.CustomFieldList();
        //    employerService
        //          .Setup(x => x.GetById(It.Is<string>(en => en == "EmployerId01")))
        //          .Returns(employer).Verifiable();

        //    employerService
        //          .Setup(x => x.GetCustomFields(It.IsAny<EntityTarget>(), It.IsAny<bool>()))
        //          .Returns(CustomFieldlist);

        //    employerService
        //          .Setup(x => x.GetCustomFields(null, null))
        //          .Returns(CustomFieldlist);
        //    return employerService;

        //}
        private Mock<IEmployeeService> EmployeeServiceStub(Customers.Employee employee)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.Is<string>(en => en == "000000304"), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee).Verifiable();
            return employeeService;
        }


        private Mock<IEligibilityService> CaseEligibilityServiceStub(cases.Case cases, LeaveOfAbsence LeaveOfAbsence)
        {
            var CaseEligibilityService = new Mock<IEligibilityService>();
            var PolicyList = EntityHelper.PolicyList();
            var AppliedPolicy = EntityHelper.AppliedPolicy();
            CaseEligibilityService.
                Setup(x => x.RunEligibility(cases)).Returns(LeaveOfAbsence);
            CaseEligibilityService
              .Setup(x => x.GetAvailableManualPolicies(cases)).Returns(PolicyList);

            CaseEligibilityService.Setup(x => x.GetWhySelected(cases, AppliedPolicy)).Returns("");
            return CaseEligibilityService;

        }
        private Mock<ICaseService> CaseCaseServiceNewStub(AbsenceSoft.Data.Cases.Case cases)
        {
            var caseService = new Mock<ICaseService>();

            caseService.
                Setup(x => x.GetCaseByCaseNumber(It.Is<string>(en => en == "772655213")))
                .Returns(cases).Verifiable();
            caseService
          .Setup(x => x.CreateCase(It.IsAny<CaseStatus>(), It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<CaseType>(), It.IsAny<string>(), It.IsAny<Schedule>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
          .Returns(cases);
            return caseService;

        }
        private Mock<ICaseService> CaseCaseServiceStub(cases.Case cases)
        {
            var caseService = new Mock<ICaseService>();

            caseService
            .Setup(x => x.GetCaseByCaseNumber(
                  It.Is<string>(en => en == "039")))
                  .Returns(cases)
                  .Verifiable();

            caseService
             .Setup(x => x.CreateCase(It.IsAny<CaseStatus>(), It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<CaseType>(), It.IsAny<string>(), It.IsAny<Schedule>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
             .Returns(cases);

            caseService
              .Setup(x => x.UpdateCase(It.IsAny<cases.Case>(), null))
              .Returns(cases);


            return caseService;
        }
        private Mock<ICaseService> CaseServieStub(cases.Case cAse, List<PolicySummary> ps = null, List<ValidationMessage> pv = null, CaseNote note = null)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);

            caseService
                .Setup(x => x.GetEmployeePolicySummaryByCaseId(It.IsAny<string>(), null))
                .Returns(ps);

            caseService
                .Setup(x => x.PreValidateTimeOffRequest(It.IsAny<string>(), It.IsAny<List<IntermittentTimeRequest>>()))
                .Returns(pv);

            caseService
                .Setup(x => x.UpdateCase(It.IsAny<cases.Case>(), null))
                .Returns(cAse);

            caseService
                .Setup(x => x.CreateOrModifyTimeOffRequest(It.IsAny<cases.Case>(), It.IsAny<List<IntermittentTimeRequest>>()))
                .Returns(cAse);

            caseService
                .Setup(x => x.IntermittentAbsenceEntryNote(It.IsAny<cases.Case>(), It.IsAny<cases.IntermittentTimeRequest>(), It.IsAny<User>()))
                .Returns(note);

            return caseService;
        }
    }



}
