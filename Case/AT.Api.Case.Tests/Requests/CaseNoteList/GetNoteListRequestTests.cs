﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft;
using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Security;
using AT.Api.Core.Exceptions;
using Moq;
using Customers = AbsenceSoft.Data.Customers;
using AT.Api.Case;
using AT.Api.Case.Requests.CaseNotes;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AT.Api.Case.Models.CaseNotes;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Notes.Contracts;
using AbsenceSoft.Data.Notes;
using System.Threading.Tasks;
using System.Net;
using AbsenceSoft.Logic.Customers.Contracts;
using System.Collections.Generic;
using AT.Api.Case.Requests.Certifications;
using AbsenceSoft.Logic;
using System.Linq;

namespace AT.Api.Case.Tests.Requests.CaseNoteList
{
    [TestClass]
    public class GetNoteListRequestTests
    {
        [TestMethod]
        public void GetNoteList_InvalidCaseId_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var adminservice = AdminServiceStub(user);
            var caseService = CaseServieStub(null);
            var employerService = EmployerServieStub(null);
            var noteService = NotesServieStub(EntityHelper.CaseNote());
            var request = new GetNoteListRequest("UserKey", noteService.Object, caseService.Object, adminservice.Object);

            var parameter = new GetNoteListParameters
            {
                CaseNumber = ""
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case number was not provided", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetNoteList_CaseNotFound_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var adminservice = AdminServiceStub(user);
            var caseService = CaseServieStub(null); // null case
            var employerService = EmployerServieStub(EntityHelper.Employer());

            var noteService = NotesServieStub(EntityHelper.CaseNote());
            var request = new GetNoteListRequest("UserKey", noteService.Object, caseService.Object, adminservice.Object);

            var parameter = new GetNoteListParameters
            {
                CaseNumber = "1390275272"
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case not found", response.Message);
            Assert.AreEqual(HttpStatusCode.ExpectationFailed, response.StatusCode);
        }

        [TestMethod]
        public void GetNoteList_Certification_Success()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var adminservice = AdminServiceStub(user);
            var note = EntityHelper.CaseNote();
            var Certification = EntityHelper.Certification();
            var caseentity = EntityHelper.Case();
            caseentity.Certifications = new List<Certification>(0) { EntityHelper.Certification() };
            var caseService = CaseServieStub(caseentity);

            ListResults results = new ListResults();
            results.Results = new List<ListResult>(0){ new ListResult()
                .Set("CreatedBy", "user")
                .Set("CreatedDate", DateTime.Now)
                .Set("Notes", "Test note")
                .Set("NoteCategoryEnum", 0)
                .Set("Category", "Case Summary")
                .Set("Public", true) };

            var noteService = NotesServieStub(note, results);
            var employerService = EmployerServieStub(EntityHelper.Employer());

            var request = new GetNoteListRequest("UserKey", noteService.Object, caseService.Object, adminservice.Object);

            var parameter = new GetNoteListParameters
            {
                CaseNumber = "772655213"
            };

            // Act
            var response = request.Handle(parameter);

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(1, response.Data.Count());
            Assert.AreEqual("user", response.Data.FirstOrDefault().CreatedBy);
            Assert.AreEqual("Case Summary", response.Data.FirstOrDefault().NoteCategory);
        }

        private Mock<ICaseService> CaseServieStub(AbsenceSoft.Data.Cases.Case cAse)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);

            caseService
                .Setup(x => x.CreateOrModifyCertification(It.IsAny<AbsenceSoft.Data.Cases.Case>(), It.IsAny<Certification>(), true))
                .Returns(cAse);

            return caseService;
        }

        private Mock<INotesService> NotesServieStub(CaseNote note, ListResults results = null)
        {
            var noteService = new Mock<INotesService>();

            noteService
                .Setup(x => x.GetCaseNoteByCertId(It.IsAny<Guid>()))
                .Returns("Note");

            noteService
                .Setup(x => x.SaveCaseNote(It.IsAny<CaseNote>()))
                .Returns(note);

            noteService
                .Setup(x => x.GetCaseNoteList(It.IsAny<ListCriteria>()))
                .Returns(results);

            return noteService;
        }

        private Mock<IEmployerService> EmployerServieStub(Customers.Employer employer)
        {
            var employerService = new Mock<IEmployerService>();

            employerService
                .Setup(x => x.GetById(It.IsAny<string>()))
                .Returns(employer);

            return employerService;
        }

        private Mock<IEmployeeService> EmployeeServieStub(Customers.Employee employee)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee);

            return employeeService;
        }

        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }
    }
}
