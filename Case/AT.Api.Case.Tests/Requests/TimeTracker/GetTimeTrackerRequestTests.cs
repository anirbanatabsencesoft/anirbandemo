﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Requests.TimeTracker;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AT.Api.Case.Tests.Requests.TimeTracker
{
    [TestClass]
    public class GetTimeTrackerRequestTests
    {
        [TestMethod]
        public void GetTimeTracker_InvalidCaseId_ThrowsException()
        {
            //// Arrange
            var caseService = new Mock<ICaseService>();


            var request = new GetTimeTrackerRequest();

            var parameter = new GetTimeTrackerParameters
            {
                CaseNumber = ""
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Case number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetTimeTracker_CaseNotFound_ThrowsException()
        {
            //// Arrange
            var caseService = CaseServieStub(null, null);

            var request = new GetTimeTrackerRequest(caseService.Object);

            var parameter = new GetTimeTrackerParameters
            {
                CaseNumber = "1390275272"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Case not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.ExpectationFailed, response.StatusCode);
        }

        [TestMethod]
        public void GetTimeTracker_CaseFound_ReturnsTimeTrackingInfoForCase()
        {
            // Arrange            
            var cAse = EntityHelper.Case();
            var intermittentTimeRequestDetail = EntityHelper.IntermittentTimeRequestDetail();
            var intermittentTimeRequest = EntityHelper.IntermittentTimeRequest();

            var policySummary = EntityHelper.PolicySummary();
            var policySummary2 = EntityHelper.PolicySummary("WA-FML", "Washington Family Leave", PolicyType.StateFML);
            List<PolicySummary> ps = new List<PolicySummary>();
            ps.Add(policySummary);
            ps.Add(policySummary2);

            List<IntermittentTimeRequest> UserRequests = new List<IntermittentTimeRequest>();
            intermittentTimeRequest.Detail = new List<IntermittentTimeRequestDetail>();
            intermittentTimeRequest.Detail.Add(intermittentTimeRequestDetail);
            UserRequests.Add(intermittentTimeRequest);

            var caseSegment = EntityHelper.CaseSegment(DateTime.Parse("2018-10-02"), DateTime.Parse("2018-10-30"), UserRequests);
            cAse.Segments = new List<CaseSegment>();
            cAse.Segments.Add(caseSegment);
            var AppliedPolicy = EntityHelper.AppliedPolicy();
            cAse.Segments.FirstOrDefault().AppliedPolicies = new List<AppliedPolicy>();
            

            cAse.Segments.FirstOrDefault().AppliedPolicies.Add(AppliedPolicy); 

            var caseService = CaseServieStub(cAse, ps);

            var request = new GetTimeTrackerRequest(caseService.Object);

            var parameter = new GetTimeTrackerParameters
            {
                CaseNumber = "1390275272"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert            
            Assert.IsNotNull(response);
            Assert.AreEqual(2, response.Data.Count());
            Assert.AreEqual("FML", response.Data.FirstOrDefault().PolicyCode);
            Assert.AreEqual("Family Leave", response.Data.FirstOrDefault().PolicyName);
        }

        private Mock<ICaseService> CaseServieStub(AbsenceSoft.Data.Cases.Case cAse, List<PolicySummary> ps)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);

            caseService
                .Setup(x => x.GetEmployeePolicySummaryByCaseId(It.IsAny<string>(), new EligibilityStatus[] {EligibilityStatus.Eligible }))
                .Returns(ps);

            return caseService;
        }

        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }
    }
}