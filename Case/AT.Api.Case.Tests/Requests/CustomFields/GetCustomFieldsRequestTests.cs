﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Requests.CustomFields;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Cases = AbsenceSoft.Data.Cases;
using customers = AbsenceSoft.Data.Customers;
namespace AT.Api.Case.Tests.Requests.CustomFields
{
    [TestClass]
    public class GetCustomFieldsRequestTests
    {
        [TestMethod]
        public void GetCustomFields_NullCaseNumber_ThrowsException()
        {
            //// Arrange            
            var caseService = CaseServieStub();

            var request = new GetCustomFieldsRequest(caseService.Object);

            var parameter = new GetCustomFieldsParameters
            {
                CaseNumber = null
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Case number is required", response.Message);
        }

        [TestMethod]
        public void GetCustomFields_EmptyCaseNumber_ThrowsException()
        {
            //// Arrange            
            var caseService = CaseServieStub();

            var request = new GetCustomFieldsRequest(caseService.Object);

            var parameter = new GetCustomFieldsParameters
            {
                CaseNumber = " "
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Case number is required", response.Message);
        }

        [TestMethod]
        public void GetCustomFields_NoMatchingCase_ThrowsException()
        {
            //// Arrange            
            var caseService = CaseServieStub();

            var request = new GetCustomFieldsRequest(caseService.Object);

            var parameter = new GetCustomFieldsParameters
            {
                CaseNumber = "case01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Case was not found for case01", response.Message);
        }

        [TestMethod]
        public void GetCustomFields_NullCustomField_ReturnsEmptyCustomField()
        {
            //// Arrange   
            var theCase = EntityHelper.Case(customFields: null);
            var caseService = CaseServieStub(theCase);

            var request = new GetCustomFieldsRequest(caseService.Object);

            var parameter = new GetCustomFieldsParameters
            {
                CaseNumber = "772655213"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual("No custom fields configuration for case", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        [TestMethod]
        public void GetCustomFields_NoCustomField_ReturnsEmptyCustomField()
        {
            //// Arrange   
            var theCase = EntityHelper.Case(customFields: new List<customers.CustomField>());
            var caseService = CaseServieStub(theCase);

            var request = new GetCustomFieldsRequest(caseService.Object);

            var parameter = new GetCustomFieldsParameters
            {
                CaseNumber = "772655213"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual("No custom fields configuration for case", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        [TestMethod]
        public void GetCustomFields_ValidCase_ReturnsCustomField()
        {
            //// Arrange
            var listItems = new List<ListItem>()
            {
                new ListItem { Key = "1",Value= "Number1"},
                new ListItem { Key = "2",Value= "Number2"}
            };

            var customField1 = EntityHelper.CustomField();

            var customField2 = EntityHelper.CustomField("Field02", "Code02", "Name02", CustomFieldType.Number, CustomFieldValueType.SelectList, "1", listItems, "Description02");
            var customFields = new List<customers.CustomField>() { customField1, customField2 };

            var theCase = EntityHelper.Case(customFields: customFields);
            var caseService = CaseServieStub(theCase);

            var request = new GetCustomFieldsRequest(caseService.Object);

            var parameter = new GetCustomFieldsParameters
            {
                CaseNumber = "772655213"
            };


            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(2, response.Data.Count());

            var result1 = response.Data.ElementAt(0);
            Assert.AreEqual("Code01", result1.Code);
            Assert.AreEqual("Value01", result1.Value);
            Assert.AreEqual("Description01", result1.Description);
            Assert.AreEqual("Name01", result1.Name);
            Assert.AreEqual(0, result1.ListValues.Count);
            Assert.AreEqual(1, result1.ValueType);
            Assert.AreEqual(1, result1.DataType);

            var result2 = response.Data.ElementAt(1);
            Assert.AreEqual("Code02", result2.Code);
            Assert.AreEqual("1", result2.Value);
            Assert.AreEqual("Description02", result2.Description);
            Assert.AreEqual("Name02", result2.Name);
            Assert.AreEqual(2, result2.ListValues.Count);
            Assert.AreEqual(2, result2.ValueType);
            Assert.AreEqual(2, result2.DataType);

            var itemKeys = result2.ListValues.Keys.ToList();
            Assert.AreEqual("1", itemKeys[0]);
            Assert.AreEqual("2", itemKeys[1]);

            var itemValues = result2.ListValues.Values.ToList();
            Assert.AreEqual("Number1", itemValues[0]);
            Assert.AreEqual("Number2", itemValues[1]);
        }


        [TestMethod]
        public void GetCustomFields_FilterByCode_ReturnsCustomField()
        {
            //// Arrange
            var listItems = new List<ListItem>()
            {
                new ListItem { Key = "1",Value= "Number1"},
                new ListItem { Key = "2",Value= "Number2"}
            };

            var customField1 = EntityHelper.CustomField();

            var customField2 = EntityHelper.CustomField("Field02", "Code02", "Name02", CustomFieldType.Number, CustomFieldValueType.SelectList, "1", listItems, "Description02");
            var customFields = new List<customers.CustomField>() { customField1, customField2 };

            var theCase = EntityHelper.Case(customFields: customFields);
            var caseService = CaseServieStub(theCase);

            var request = new GetCustomFieldsRequest(caseService.Object);

            var parameter = new GetCustomFieldsParameters
            {
                CaseNumber = "772655213",
                Code = "Code01"
            };


            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(1, response.Data.Count());

            var result1 = response.Data.ElementAt(0);
            Assert.AreEqual("Code01", result1.Code);
            Assert.AreEqual("Value01", result1.Value);
            Assert.AreEqual("Description01", result1.Description);
            Assert.AreEqual("Name01", result1.Name);
            Assert.AreEqual(0, result1.ListValues.Count);
            Assert.AreEqual(1, result1.ValueType);
            Assert.AreEqual(1, result1.DataType);
        }


        [TestMethod]
        public void GetCustomFields_CallServices_ServicesShouldBeCalled()
        {
            //// Arrange
            var theCase = EntityHelper.Case();

            var caseService = new Mock<ICaseService>();
            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.Is<string>(cn => cn == "772655213")))
                .Returns(theCase)
                .Verifiable();


            var request = new GetCustomFieldsRequest(caseService.Object);

            var parameter = new GetCustomFieldsParameters
            {
                CaseNumber = "772655213"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            //// Assert
            caseService.Verify();
        }
        private Mock<ICaseService> CaseServieStub(Cases.Case theCase = null)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(theCase);

            return caseService;
        }
    }
}
