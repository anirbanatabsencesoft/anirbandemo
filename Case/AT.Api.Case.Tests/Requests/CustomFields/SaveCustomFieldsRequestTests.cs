﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Case.Requests.CustomFields;
using AT.Api.Core.Exceptions;
using AT.Api.Shared.Model.CustomFields;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using Cases = AbsenceSoft.Data.Cases;
using customers = AbsenceSoft.Data.Customers;
namespace AT.Api.Case.Tests.Requests.CustomFields
{
    [TestClass]
    public class SaveCustomFieldsRequestTests
    {
        [TestMethod]
        public void SaveCustomFields_EmptyUserKey_ThrowsException()
        {
            //// Arrange
            var adminService = AdminServiceStub(null);

            //// Act
            var request = Assert.ThrowsException<ApiException>(() =>
                new SaveCustomFieldsRequest(
                    "  ",
                    adminUserService: adminService.Object));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.Unauthorized, request.StatusCode);
            Assert.AreEqual("Unable to retrieve authorized user detail", request.Message);
        }

        [TestMethod]
        public void SaveCustomFields_UserNotFound_ThrowsException()
        {
            //// Arrange
            var adminService = AdminServiceStub(null);

            //// Act
            var request = Assert.ThrowsException<ApiException>(() =>
                new SaveCustomFieldsRequest(
                    "UserKey",
                    adminUserService: adminService.Object));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.InternalServerError, request.StatusCode);
            Assert.AreEqual("Unable to find user", request.Message);
        }

        [TestMethod]
        public void SaveCustomFields_NullCaseNumber_ThrowsException()
        {
            //// Arrange            
            var user = EntityHelper.UserStub();
            var adminService = AdminServiceStub(user);
            var employerService = EmployerServiceStub();
            var caseService = CaseServieStub();

            var request = new SaveCustomFieldsRequest("UserKey", caseService.Object, employerService.Object, adminService.Object);

            var parameter = new SaveCustomFieldsParameters
            {
                CaseNumber = null
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Case number is required", response.Message);
        }

        [TestMethod]
        public void SaveCustomFields_EmptyCaseNumber_ThrowsException()
        {
            //// Arrange            
            var user = EntityHelper.UserStub();
            var adminService = AdminServiceStub(user);
            var employerService = EmployerServiceStub();
            var caseService = CaseServieStub();

            var request = new SaveCustomFieldsRequest("UserKey", caseService.Object, employerService.Object, adminService.Object);

            var parameter = new SaveCustomFieldsParameters
            {
                CaseNumber = " "
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Case number is required", response.Message);
        }

        [TestMethod]
        public void SaveCustomFields_NullCustomFields_ThrowsException()
        {  //// Arrange            
            var user = EntityHelper.UserStub();
            var adminService = AdminServiceStub(user);
            var employerService = EmployerServiceStub();
            var caseService = CaseServieStub();

            var request = new SaveCustomFieldsRequest("UserKey", caseService.Object, employerService.Object, adminService.Object);

            var parameter = new SaveCustomFieldsParameters
            {
                CaseNumber = "772655213",
                CustomFields = null
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Invalid custom field code", response.Message);
        }

        [TestMethod]
        public void SaveCustomFields_NoCustomFields_ThrowsException()
        {
            //// Arrange            
            var user = EntityHelper.UserStub();
            var adminService = AdminServiceStub(user);
            var employerService = EmployerServiceStub();
            var caseService = CaseServieStub();

            var request = new SaveCustomFieldsRequest("UserKey", caseService.Object, employerService.Object, adminService.Object);

            var parameter = new SaveCustomFieldsParameters
            {
                CaseNumber = "772655213",
                CustomFields = new SaveCustomFieldModel[0]
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Invalid custom field code", response.Message);
        }

        [TestMethod]
        public void SaveCustomFields_EmptyCode_ThrowsException()
        {
            //// Arrange            
            var user = EntityHelper.UserStub();
            var adminService = AdminServiceStub(user);
            var employerService = EmployerServiceStub();
            var caseService = CaseServieStub();

            var request = new SaveCustomFieldsRequest("UserKey", caseService.Object, employerService.Object, adminService.Object);

            var parameter = new SaveCustomFieldsParameters
            {
                CaseNumber = "772655213",
                CustomFields = new SaveCustomFieldModel[1] { new SaveCustomFieldModel { Code = " " } }
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Invalid custom field code", response.Message);
        }

        [TestMethod]
        public void SaveCustomFields_nullCode_ThrowsException()
        {
            //// Arrange            
            var user = EntityHelper.UserStub();
            var adminService = AdminServiceStub(user);
            var employerService = EmployerServiceStub();
            var caseService = CaseServieStub();

            var request = new SaveCustomFieldsRequest("UserKey", caseService.Object, employerService.Object, adminService.Object);

            var parameter = new SaveCustomFieldsParameters
            {
                CaseNumber = "772655213",
                CustomFields = new SaveCustomFieldModel[1] { new SaveCustomFieldModel { Code = null } }
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Invalid custom field code", response.Message);
        }


        [TestMethod]
        public void SaveCustomFields_NoMatchingCase_ThrowsException()
        {
            //// Arrange   
            var user = EntityHelper.UserStub();
            var adminService = AdminServiceStub(user);
            var employerService = EmployerServiceStub();
            var caseService = CaseServieStub();

            var request = new SaveCustomFieldsRequest("UserKey", caseService.Object, employerService.Object, adminService.Object);

            var parameter = new SaveCustomFieldsParameters
            {
                CaseNumber = "772655213",
                CustomFields = new SaveCustomFieldModel[1] { new SaveCustomFieldModel { Code = "Test" } }
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Case was not found for 772655213", response.Message);
        }

        [TestMethod]
        public void SaveCustomFields_NullConfigurationCustomFields_ThrowsException()
        {
            //// Arrange            
            var user = EntityHelper.UserStub();
            var cAse = EntityHelper.Case();

            var caseService = CaseServieStub(cAse);
            var adminService = AdminServiceStub(user);
            var employerService = EmployerServiceStub(null);

            var request = new SaveCustomFieldsRequest("UserKey", caseService.Object, employerService.Object, adminService.Object);

            var parameter = new SaveCustomFieldsParameters
            {
                CaseNumber = "772655213",
                CustomFields = new SaveCustomFieldModel[1] { new SaveCustomFieldModel { Code = "Test" } }
            };


            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
               request.Handle(parameter));

            //// Assert
            Assert.AreEqual("No configuration fields setting for case", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        [TestMethod]
        public void SaveCustomFields_NoConfigurationCustomField_ThrowsException()
        {
            //// Arrange            
            var user = EntityHelper.UserStub();
            var cAse = EntityHelper.Case();

            var caseService = CaseServieStub(cAse);
            var adminService = AdminServiceStub(user);
            var employerService = EmployerServiceStub(new List<customers.CustomField>());

            var request = new SaveCustomFieldsRequest("UserKey", caseService.Object, employerService.Object, adminService.Object);

            var parameter = new SaveCustomFieldsParameters
            {
                CaseNumber = "772655213",
                CustomFields = new SaveCustomFieldModel[1] { new SaveCustomFieldModel { Code = "Test" } }
            };


            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
               request.Handle(parameter));

            //// Assert
            Assert.AreEqual("No configuration fields setting for case", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        [TestMethod]
        public void SaveCustomFields_NoMachingCode_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var customField = EntityHelper.CustomField();
            var customFields = new List<customers.CustomField>() { customField };
            var cAse = EntityHelper.Case(customFields: customFields);

            var adminService = AdminServiceStub(user);
            var employerService = EmployerServiceStub(customFields);
            var caseService = CaseServieStub(cAse);

            var request = new SaveCustomFieldsRequest("UserKey", caseService.Object, employerService.Object, adminService.Object);

            var parameter = new SaveCustomFieldsParameters
            {
                CaseNumber = "772655213",
                CustomFields = new SaveCustomFieldModel[1] { new SaveCustomFieldModel { Code = "Test" } }
            };


            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Invalid custom fields configuration code", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        [TestMethod]
        public void SaveCustomFields_ValidCustomFields_ReturnsCustomFieldId()
        {
            //// Arrange
            var user = EntityHelper.UserStub();

            var customField1 = EntityHelper.CustomField();
            var customField2 = EntityHelper.CustomField("FieldId02", "Code02", "Name02", dataType: CustomFieldType.Date, selectedValue: "01-01-2018");
            var customField3 = EntityHelper.CustomField("FieldId03", "Code03", "Name02", dataType: CustomFieldType.Number, selectedValue: "2");
            var customFields = new List<customers.CustomField>() { customField1, customField2, customField3 };

            var cAse = EntityHelper.Case(customFields: customFields);

            var adminService = AdminServiceStub(user);
            var employerService = EmployerServiceStub(customFields);
            var caseService = CaseServieStub(cAse);

            var request = new SaveCustomFieldsRequest("UserKey", caseService.Object, employerService.Object, adminService.Object);

            var parameter = new SaveCustomFieldsParameters
            {
                CaseNumber = "772655213",

                CustomFields = new SaveCustomFieldModel[]
                {
                    new SaveCustomFieldModel { Code = "Code01" , Value = "ChangedValue01"},
                    new SaveCustomFieldModel { Code = "Code02" , Value = DateTime.UtcNow.ToString("MM-dd-yyyy")}
                }
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual(2, response.Data.Length);
            Assert.AreEqual("FieldId01", response.Data[0]);
            Assert.AreEqual("FieldId02", response.Data[1]);
        }

        [TestMethod]
        public void SaveCustomFields_CallServices_ServicesShouldBeCalled()
        {
            //// Arrange
            var user = EntityHelper.UserStub();

            var customField1 = EntityHelper.CustomField();
            var customFields = new List<customers.CustomField>() { customField1 };

            var cAse = EntityHelper.Case(customFields: customFields);

            var caseService = new Mock<ICaseService>();
            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.Is<string>(cn => cn == "772655213")))
                .Returns(cAse)
                .Verifiable();

            caseService
               .Setup(x => x.UpdateCase(It.Is<Cases.Case>(
                         c => c.CaseNumber == "772655213"),
                         It.Is<CaseEventType?>(type => type == null)))
               .Returns(cAse)
               .Verifiable();

            var adminService = new Mock<IAdminUserService>();
            adminService
                .Setup(x => x.GetUserById(It.Is<string>(val => val == "UserKey")))
                .Returns(user)
                .Verifiable();

            var employerService = new Mock<IEmployerService>();
            employerService
             .Setup(x => x.GetCustomFields(
                 It.Is<EntityTarget>(er => er == EntityTarget.Case),
                 It.Is<bool?>(er => er == null)))
             .Returns(customFields)
             .Verifiable();

            var request = new SaveCustomFieldsRequest("UserKey", caseService.Object, employerService.Object, adminService.Object);

            var parameter = new SaveCustomFieldsParameters
            {
                CaseNumber = "772655213",
                CustomFields = new SaveCustomFieldModel[]
                {
                    new SaveCustomFieldModel { Code = "Code01" , Value = "ChangedValue01"}
                }
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            //// Assert
            caseService.Verify();
            adminService.Verify();
            employerService.Verify();
        }
        private Mock<ICaseService> CaseServieStub(Cases.Case cAse = null)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);

            return caseService;
        }

        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }


        private Mock<IEmployerService> EmployerServiceStub(List<customers.CustomField> customFields = null)
        {
            var employerService = new Mock<IEmployerService>(MockBehavior.Loose);

            employerService
                .Setup(x => x.GetCustomFields(It.Is<EntityTarget>(en => en == EntityTarget.Case), It.IsAny<bool?>()))
                .Returns(customFields);

            return employerService;
        }
    }
}
