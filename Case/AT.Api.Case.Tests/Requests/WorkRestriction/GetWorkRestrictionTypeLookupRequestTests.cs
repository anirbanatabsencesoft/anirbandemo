﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Security;
using AT.Api.Core.Exceptions;
using Moq;
using Customers = AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using System.Net;
using AbsenceSoft.Logic.Customers.Contracts;
using System.Collections.Generic;
using System.Linq;
using AbsenceSoft.Data.Jobs;
using AT.Api.Case.Requests.WorkRestrictionTypes;

namespace AT.Api.Case.Tests.Requests.WorkRestriction
{
    [TestClass]
    public class GetWorkRestrictionTypeLookupRequestTests
    {
        private Mock<ICaseService> CaseServieStub(AbsenceSoft.Data.Cases.Case cAse)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);

            caseService
                .Setup(x => x.CreateOrModifyCertification(It.IsAny<AbsenceSoft.Data.Cases.Case>(), It.IsAny<Certification>(), true))
                .Returns(cAse);

            return caseService;
        }        

        private Mock<IEmployerService> EmployerServieStub(Customers.Employer employer)
        {
            var employerService = new Mock<IEmployerService>();

            employerService
                .Setup(x => x.GetById(It.IsAny<string>()))
                .Returns(employer);

            return employerService;
        }

        //private Mock<IEmployeeService> EmployeeServieStub(Customers.Employee employee)
        //{
        //    var employeeService = new Mock<IEmployeeService>();

        //    employeeService
        //        .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
        //        .Returns(employee);

        //    return employeeService;
        //}

        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }

        private Mock<IDemandService> DemandServiceStub(List<EmployeeRestriction> restrictions, EmployeeRestriction restriction, List<DemandType> demandTypes = null)
        {
            var demandService = new Mock<IDemandService>();

            demandService
                .Setup(x => x.DeleteJobRestriction(It.IsAny<string>()));

            demandService
                .Setup(x => x.GetJobRestrictionsForEmployee(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(restrictions);

            demandService
                .Setup(x => x.GetEmployeeRestrictionById(It.IsAny<string>()))
                .Returns(restriction);

            demandService
                .Setup(x => x.SaveJobRestriction(It.IsAny<EmployeeRestriction>()));

            demandService
                .Setup(x => x.GetDemandTypes(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(demandTypes);

            return demandService;
        }

        [TestMethod]
        public void GetWorkRestrictionTypeLookup_InvalidCustomerId_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var adminservice = AdminServiceStub(user);
            var caseService = CaseServieStub(null);
            var employerService = EmployerServieStub(null);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction());
            var request = new GetWorkRestrictionTypeLookupRequest(employerService.Object, demandService.Object, adminservice.Object);

            var parameter = new GetWorkRestrictionTypeLookupParameters
            {
                CustomerId = "",
                EmployerId = "2"
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Missing CustomerId", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }       


        [TestMethod]
        public void GetWorkRestrictionTypeLookup_InvalidEmployerId_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();

            var caseService = CaseServieStub(EntityHelper.Case()); // null case                  
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction());
            var request = new GetWorkRestrictionTypeLookupRequest(employerService.Object, demandService.Object, adminservice.Object);

            var parameter = new GetWorkRestrictionTypeLookupParameters
            {
                CustomerId = "78676979",
                EmployerId = ""
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Missing EmployerId", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }
         

        [TestMethod]
        public void GetWorkRestrictionTypeLookup_DemandTypes_Success()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(EntityHelper.Case()); // null case
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction(), new List<DemandType>(0) { EntityHelper.DemandType() });
            var request = new GetWorkRestrictionTypeLookupRequest(employerService.Object, demandService.Object, adminservice.Object);

            var parameter = new GetWorkRestrictionTypeLookupParameters
            {
                CustomerId = "78676979",
                EmployerId = "2"
            };

            // Act
            var response = request.Handle(parameter);

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(1, response.Data.Count());
            Assert.AreEqual("627ca96a-37c8-492a-a141-ee7744dc6d36", response.Data.FirstOrDefault().Id);
            Assert.AreEqual("Restrictions", response.Data.FirstOrDefault().Name);            
        }

    }
}