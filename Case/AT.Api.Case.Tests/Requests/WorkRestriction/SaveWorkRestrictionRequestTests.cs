﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft;
using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Security;
using AT.Api.Core.Exceptions;
using Moq;
using Customers = AbsenceSoft.Data.Customers;
using AT.Api.Case;
using AT.Api.Case.Requests.WorkRestriction;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AT.Api.Case.Models.WorkRestriction;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Notes.Contracts;
using AbsenceSoft.Data.Notes;
using System.Threading.Tasks;
using System.Net;
using AbsenceSoft.Logic.Customers.Contracts;
using System.Collections.Generic;
using System.Linq;
using AbsenceSoft.Data.Jobs;

namespace AT.Api.Case.Tests.Requests.WorkRestriction
{
    [TestClass]
    public class SaveWorkRestrictionRequestTests
    {
        private Mock<ICaseService> CaseServieStub(AbsenceSoft.Data.Cases.Case cAse)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);

            caseService
                .Setup(x => x.CreateOrModifyCertification(It.IsAny<AbsenceSoft.Data.Cases.Case>(), It.IsAny<Certification>(), true))
                .Returns(cAse);

            return caseService;
        }        

        private Mock<IEmployerService> EmployerServieStub(Customers.Employer employer)
        {
            var employerService = new Mock<IEmployerService>();

            employerService
                .Setup(x => x.GetById(It.IsAny<string>()))
                .Returns(employer);

            return employerService;
        }

        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }

        private Mock<IDemandService> DemandServiceStub(List<EmployeeRestriction> restrictions, EmployeeRestriction restriction, Demand demand, DemandType demandType)
        {
            var demandService = new Mock<IDemandService>();

            demandService
                .Setup(x => x.DeleteJobRestriction(It.IsAny<string>()));

            demandService
                .Setup(x => x.GetJobRestrictionsForEmployee(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(restrictions);

            demandService
                .Setup(x => x.GetEmployeeRestrictionById(It.IsAny<string>()))
                .Returns(restriction);

            demandService
                .Setup(x => x.SaveJobRestriction(It.IsAny<EmployeeRestriction>()));

            demandService
                .Setup(x => x.GetDemandByCode(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(demand);

            demandService
                .Setup(x => x.GetDemandTypeByCode(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(demandType);

            return demandService;
        }

        [TestMethod]
        public void SaveWorkRestriction_InvalidCaseId_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var adminservice = AdminServiceStub(user);
            var caseService = CaseServieStub(null);
            var employerService = EmployerServieStub(null);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction(), EntityHelper.Demand(), EntityHelper.DemandType());
            var request = new SaveWorkRestrictionRequest("UserKey", caseService.Object, demandService.Object, adminservice.Object, employerService.Object);

            var parameter = new SaveWorkRestrictionParameters
            {
                CaseNumber = "",
                EmployerId = "2",
                WorkRestriction = new CreateWorkRestrictionModel
                {
                    DemandCode = "CARRYWOH",
                    Id = "",
                    EndDate = DateTime.Now,
                    StartDate = DateTime.Now.AddDays(-5),
                    Values = new List<AppliedDemandValueModel>(0) { new AppliedDemandValueModel {
                        Applicable = false,
                        Type = 0,
                        Text = "Test",
                        DemandTypeCode = "RESTRICTIONS"
                    } }
                }
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveWorkRestriction_CaseNotFound_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(null); // null case
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction(), EntityHelper.Demand(), EntityHelper.DemandType());
            var request = new SaveWorkRestrictionRequest("UserKey", caseService.Object, demandService.Object, adminservice.Object, employerService.Object);

            var parameter = new SaveWorkRestrictionParameters
            {
                CaseNumber = "772655213",
                EmployerId = "2",
                WorkRestriction = new CreateWorkRestrictionModel
                {
                    DemandCode = "CARRYWOH",
                    Id = "",
                    EndDate = DateTime.Now,
                    StartDate = DateTime.Now.AddDays(-5),
                    Values = new List<AppliedDemandValueModel>(0) { new AppliedDemandValueModel {
                        Applicable = false,
                        Type = 0,
                        Text = "Test",
                        DemandTypeCode = "RESTRICTIONS"
                    } }
                }
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.ExpectationFailed, response.StatusCode);
        }

        [TestMethod]
        public void SaveWorkRestriction_EmployeeNotFound_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var cAse = EntityHelper.Case();
            cAse.Employee = null;
            var caseService = CaseServieStub(cAse); // null case
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction(), EntityHelper.Demand(), EntityHelper.DemandType());
            var request = new SaveWorkRestrictionRequest("UserKey", caseService.Object, demandService.Object, adminservice.Object, employerService.Object);

            var parameter = new SaveWorkRestrictionParameters
            {
                CaseNumber = "772655213",
                EmployerId = "2",
                WorkRestriction = new CreateWorkRestrictionModel
                {
                    DemandCode = "CARRYWOH",
                    Id = "",
                    EndDate = DateTime.Now,
                    StartDate = DateTime.Now.AddDays(-5),
                    Values = new List<AppliedDemandValueModel>(0) { new AppliedDemandValueModel {
                        Applicable = false,
                        Type = 0,
                        Text = "Test",
                        DemandTypeCode = "RESTRICTIONS"
                    } }
                }
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Employee not found", response.Message);
            Assert.AreEqual(HttpStatusCode.ExpectationFailed, response.StatusCode);
        }

        [TestMethod]
        public void SaveWorkRestriction_InvalidEmployerId_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();

            var caseService = CaseServieStub(EntityHelper.Case()); // null case                  
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction(), EntityHelper.Demand(), EntityHelper.DemandType());
            var request = new SaveWorkRestrictionRequest("UserKey", caseService.Object, demandService.Object, adminservice.Object, employerService.Object);

            var parameter = new SaveWorkRestrictionParameters
            {
                CaseNumber = "78676979",
                EmployerId = "",
                WorkRestriction = new CreateWorkRestrictionModel
                {
                    DemandCode = "CARRYWOH",
                    Id = "",
                    EndDate = DateTime.Now,
                    StartDate = DateTime.Now.AddDays(-5),
                    Values = new List<AppliedDemandValueModel>(0) { new AppliedDemandValueModel {
                        Applicable = false,
                        Type = 0,
                        Text = "Test",
                        DemandTypeCode = "RESTRICTIONS"
                    } }
                }
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Missing EmployerId", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveWorkRestriction_WorkRestrictionNotProvided_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();

            var caseService = CaseServieStub(EntityHelper.Case()); // null case                  
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction(), EntityHelper.Demand(), EntityHelper.DemandType());
            var request = new SaveWorkRestrictionRequest("UserKey", caseService.Object, demandService.Object, adminservice.Object, employerService.Object);

            var parameter = new SaveWorkRestrictionParameters
            {
                CaseNumber = "78676979",
                EmployerId = "",
                WorkRestriction = null
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Work restriction details not provided", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveWorkRestriction_DemandCodeNotProvided_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();

            var caseService = CaseServieStub(EntityHelper.Case()); // null case                  
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction(), EntityHelper.Demand(), EntityHelper.DemandType());
            var request = new SaveWorkRestrictionRequest("UserKey", caseService.Object, demandService.Object, adminservice.Object, employerService.Object);

            var parameter = new SaveWorkRestrictionParameters
            {
                CaseNumber = "78676979",
                EmployerId = "",                
                WorkRestriction = new CreateWorkRestrictionModel
                {
                    DemandCode = "",
                    Id = "",
                    EndDate = DateTime.Now,
                    StartDate = DateTime.Now.AddDays(-5),
                    Values = new List<AppliedDemandValueModel>(0) { new AppliedDemandValueModel {
                        Applicable = false,
                        Type = 0,
                        Text = "Test",
                        DemandTypeCode = "RESTRICTIONS"
                    } }
                }
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("DemandCode not provided", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveWorkRestriction_StartDateNotProvided_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();

            var caseService = CaseServieStub(EntityHelper.Case()); // null case                  
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction(), EntityHelper.Demand(), EntityHelper.DemandType());
            var request = new SaveWorkRestrictionRequest("UserKey", caseService.Object, demandService.Object, adminservice.Object, employerService.Object);

            var parameter = new SaveWorkRestrictionParameters
            {
                CaseNumber = "78676979",
                EmployerId = "",
                WorkRestriction = new CreateWorkRestrictionModel
                {
                    DemandCode = "CARRYWOH",
                    Id = "",
                    EndDate = DateTime.Now,
                    StartDate = null,
                    Values = new List<AppliedDemandValueModel>(0) { new AppliedDemandValueModel {
                        Applicable = false,
                        Type = 0,
                        Text = "Test",
                        DemandTypeCode = "RESTRICTIONS"
                    } }
                }
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Start date not provided", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveWorkRestriction_InvalidEndDate_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();

            var caseService = CaseServieStub(EntityHelper.Case()); // null case                  
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction(), EntityHelper.Demand(), EntityHelper.DemandType());
            var request = new SaveWorkRestrictionRequest("UserKey", caseService.Object, demandService.Object, adminservice.Object, employerService.Object);

            var parameter = new SaveWorkRestrictionParameters
            {
                CaseNumber = "78676979",
                EmployerId = "",
                WorkRestriction = new CreateWorkRestrictionModel
                {
                    DemandCode = "CARRYWOH",
                    Id = "",
                    EndDate = DateTime.Now,
                    StartDate = DateTime.Now.AddDays(2),
                    Values = new List<AppliedDemandValueModel>(0) { new AppliedDemandValueModel {
                        Applicable = false,
                        Type = 0,
                        Text = "Test",
                        DemandTypeCode = "RESTRICTIONS"
                    } }
                }
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("End date should be greater than start date", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveWorkRestriction_InvalidValues_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();

            var caseService = CaseServieStub(EntityHelper.Case()); // null case                  
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction(), EntityHelper.Demand(), EntityHelper.DemandType());
            var request = new SaveWorkRestrictionRequest("UserKey", caseService.Object, demandService.Object, adminservice.Object, employerService.Object);

            var parameter = new SaveWorkRestrictionParameters
            {
                CaseNumber = "78676979",
                EmployerId = "",
                WorkRestriction = new CreateWorkRestrictionModel
                {
                    DemandCode = "CARRYWOH",
                    Id = "",
                    EndDate = DateTime.Now,
                    StartDate = DateTime.Now.AddDays(-5),
                    Values = null
                }
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Work restriction values not provided", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveWorkRestriction_InvalidSchedule_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();

            var caseService = CaseServieStub(EntityHelper.Case()); // null case                  
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction(), EntityHelper.Demand(), EntityHelper.DemandType());
            var request = new SaveWorkRestrictionRequest("UserKey", caseService.Object, demandService.Object, adminservice.Object, employerService.Object);

            var parameter = new SaveWorkRestrictionParameters
            {
                CaseNumber = "78676979",
                EmployerId = "",
                WorkRestriction = new CreateWorkRestrictionModel
                {
                    DemandCode = "CARRYWOH",
                    Id = "",
                    EndDate = DateTime.Now,
                    StartDate = DateTime.Now.AddDays(-9),
                    Values = new List<AppliedDemandValueModel>(0) { new AppliedDemandValueModel {
                        Applicable = false,
                        Type = 3,
                        Text = "",
                        DemandTypeCode = "RESTRICTIONS"
                    } }
                }
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Must fill the value(s) for the demand item(s)." + Environment.NewLine + "Schedule frequency fields are required for Schedule demand type." + Environment.NewLine, response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveWorkRestriction_Restrictions_Success()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(EntityHelper.Case()); // null case
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction(), EntityHelper.Demand(), EntityHelper.DemandType());
            var request = new SaveWorkRestrictionRequest("UserKey", caseService.Object, demandService.Object, adminservice.Object, employerService.Object);

            var parameter = new SaveWorkRestrictionParameters
            {
                CaseNumber = "78676979",
                EmployerId = "2",                
                WorkRestriction = new CreateWorkRestrictionModel
                {
                    DemandCode = "CARRYWOH",
                    Id = "",
                    EndDate = DateTime.Now,
                    StartDate = DateTime.Now.AddDays(-5),
                    Values = new List<AppliedDemandValueModel>(0) { new AppliedDemandValueModel {
                        DemandTypeCode = "RESTRICTIONS",
                        Type = 0,
                        DemandItem = null,
                        DemandItemName = null,
                        Text = "Test01",
                        Value = "Test01",
                        Applicable = false,                        
                        Occurances = 0,
                        Frequency = 0,
                        FrequencyType = 0,
                        FrequencyUnitType = 0,
                        Duration = 0,
                        DurationType = 0
                    } }
                }
            };

            // Act
            var response = request.Handle(parameter);

            // Assert
            Assert.IsNotNull(response);
            Assert.IsTrue(response.Success);
            Assert.AreEqual("Work restriction information saved successfully", response.Message);
        }

        [TestMethod]
        public void SaveWorkRestriction_RestrictionsForTypeValue_Success()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(EntityHelper.Case()); // null case
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction(), EntityHelper.Demand(), EntityHelper.DemandType());
            var request = new SaveWorkRestrictionRequest("UserKey", caseService.Object, demandService.Object, adminservice.Object, employerService.Object);

            var parameter = new SaveWorkRestrictionParameters
            {
                CaseNumber = "78676979",
                EmployerId = "2",
                WorkRestriction = new CreateWorkRestrictionModel
                {
                    DemandCode = "CARRYWOH",
                    Id = "",
                    EndDate = DateTime.Now,
                    StartDate = DateTime.Now.AddDays(-5),
                    Values = new List<AppliedDemandValueModel>(0) { new AppliedDemandValueModel {
                        DemandTypeCode = "RESTRICTIONS",
                        Type = 2,
                        DemandItem = null,
                        DemandItemName = "Fully",
                        Text = "",
                        Value = "",
                        Applicable = false,
                        Occurances = 0,
                        Frequency = 0,
                        FrequencyType = 0,
                        FrequencyUnitType = 0,
                        Duration = 0,
                        DurationType = 0
                    } }
                }
            };

            // Act
            var response = request.Handle(parameter);

            // Assert
            Assert.IsNotNull(response);
            Assert.IsTrue(response.Success);
            Assert.AreEqual("Work restriction information saved successfully", response.Message);
        }
    }
}