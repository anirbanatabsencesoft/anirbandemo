﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft;
using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Security;
using AT.Api.Core.Exceptions;
using Moq;
using Customers = AbsenceSoft.Data.Customers;
using AT.Api.Case;
using AT.Api.Case.Requests.WorkRestriction;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AT.Api.Case.Models.WorkRestriction;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Notes.Contracts;
using AbsenceSoft.Data.Notes;
using System.Threading.Tasks;
using System.Net;
using AbsenceSoft.Logic.Customers.Contracts;
using System.Collections.Generic;
using AbsenceSoft.Data.Jobs;

namespace AT.Api.Case.Tests.Requests.WorkRestriction
{
    [TestClass]
    public class DeleteWorkRestrictionRequestTests
    {
        private Mock<ICaseService> CaseServieStub(AbsenceSoft.Data.Cases.Case cAse)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);

            caseService
                .Setup(x => x.CreateOrModifyCertification(It.IsAny<AbsenceSoft.Data.Cases.Case>(), It.IsAny<Certification>(), true))
                .Returns(cAse);

            return caseService;
        }        

        private Mock<IEmployerService> EmployerServieStub(Customers.Employer employer)
        {
            var employerService = new Mock<IEmployerService>();

            employerService
                .Setup(x => x.GetById(It.IsAny<string>()))
                .Returns(employer);

            return employerService;
        }

        //private Mock<IEmployeeService> EmployeeServieStub(Customers.Employee employee)
        //{
        //    var employeeService = new Mock<IEmployeeService>();

        //    employeeService
        //        .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
        //        .Returns(employee);

        //    return employeeService;
        //}

        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }

        private Mock<IDemandService> DemandServiceStub(List<EmployeeRestriction> restrictions, EmployeeRestriction restriction)
        {
            var demandService = new Mock<IDemandService>();

            demandService
                .Setup(x => x.DeleteJobRestriction(It.IsAny<string>()));

            demandService
                .Setup(x => x.GetJobRestrictionsForEmployee(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(restrictions);

            demandService
                .Setup(x => x.GetEmployeeRestrictionById(It.IsAny<string>()))
                .Returns(restriction);

            demandService
                .Setup(x => x.SaveJobRestriction(It.IsAny<EmployeeRestriction>()));                

            return demandService;
        }

        [TestMethod]
        public void DeleteWorkRestriction_InvalidCaseId_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var adminservice = AdminServiceStub(user);
            var caseService = CaseServieStub(null);
            var employerService = EmployerServieStub(null);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction());
            var request = new DeleteWorkRestrictionRequest("UserKey", caseService.Object, employerService.Object, demandService.Object, adminservice.Object);

            var parameter = new DeleteWorkRestrictionParameters
            {
                CaseNumber = "",
                EmployerId = "2",
                WorkRestrictionId = "ID"
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void DeleteWorkRestriction_CaseNotFound_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(null); // null case
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction());
            var request = new DeleteWorkRestrictionRequest("UserKey", caseService.Object, employerService.Object, demandService.Object, adminservice.Object);

            var parameter = new DeleteWorkRestrictionParameters
            {
                CaseNumber = "772655213",
                EmployerId = "2",
                WorkRestrictionId = "ID"                
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.ExpectationFailed, response.StatusCode);
        }

        [TestMethod]
        public void DeleteWorkRestriction_InvalidEmployerId_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();

            var caseService = CaseServieStub(EntityHelper.Case()); // null case                  
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction());
            var request = new DeleteWorkRestrictionRequest("UserKey", caseService.Object, employerService.Object, demandService.Object, adminservice.Object);

            var parameter = new DeleteWorkRestrictionParameters
            {
                CaseNumber = "78676979",
                EmployerId = "",
                WorkRestrictionId = "ID"
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Missing EmployerId", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void DeleteWorkRestriction_InvalidWorkRestrictionId_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();

            var caseService = CaseServieStub(EntityHelper.Case()); // null case                  
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction());
            var request = new DeleteWorkRestrictionRequest("UserKey", caseService.Object, employerService.Object, demandService.Object, adminservice.Object);

            var parameter = new DeleteWorkRestrictionParameters
            {
                CaseNumber = "78676979",
                EmployerId = "2",
                WorkRestrictionId = ""
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Work restriction id not provided", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void DeleteWorkRestriction_WorkRestrictionId_Success()
        {
            // Arrange
            var user = EntityHelper.UserStub();

            var caseService = CaseServieStub(EntityHelper.Case()); // null case                  
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction());
            var request = new DeleteWorkRestrictionRequest("UserKey", caseService.Object, employerService.Object, demandService.Object, adminservice.Object);

            var parameter = new DeleteWorkRestrictionParameters
            {
                CaseNumber = "78676979",
                EmployerId = "2",
                WorkRestrictionId = "76238236"
            };

            // Act
            var response = request.Handle(parameter);

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(true, response.Data);
            Assert.AreEqual("Work restriction deleted successfully", response.Message);
        }
    }
}