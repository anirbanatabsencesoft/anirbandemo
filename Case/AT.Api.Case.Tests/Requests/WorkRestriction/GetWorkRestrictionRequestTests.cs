﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft;
using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Security;
using AT.Api.Core.Exceptions;
using Moq;
using Customers = AbsenceSoft.Data.Customers;
using AT.Api.Case;
using AT.Api.Case.Requests.WorkRestriction;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AT.Api.Case.Models.WorkRestriction;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Notes.Contracts;
using AbsenceSoft.Data.Notes;
using System.Threading.Tasks;
using System.Net;
using AbsenceSoft.Logic.Customers.Contracts;
using System.Collections.Generic;
using System.Linq;
using AbsenceSoft.Data.Jobs;

namespace AT.Api.Case.Tests.Requests.WorkRestriction
{
    [TestClass]
    public class GetWorkRestrictionRequestTests
    {
        private Mock<ICaseService> CaseServieStub(AbsenceSoft.Data.Cases.Case cAse)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);

            caseService
                .Setup(x => x.CreateOrModifyCertification(It.IsAny<AbsenceSoft.Data.Cases.Case>(), It.IsAny<Certification>(), true))
                .Returns(cAse);

            return caseService;
        }        

        private Mock<IEmployerService> EmployerServieStub(Customers.Employer employer)
        {
            var employerService = new Mock<IEmployerService>();

            employerService
                .Setup(x => x.GetById(It.IsAny<string>()))
                .Returns(employer);

            return employerService;
        }

        //private Mock<IEmployeeService> EmployeeServieStub(Customers.Employee employee)
        //{
        //    var employeeService = new Mock<IEmployeeService>();

        //    employeeService
        //        .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
        //        .Returns(employee);

        //    return employeeService;
        //}

        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }

        private Mock<IDemandService> DemandServiceStub(List<EmployeeRestriction> restrictions, EmployeeRestriction restriction)
        {
            var demandService = new Mock<IDemandService>();

            demandService
                .Setup(x => x.DeleteJobRestriction(It.IsAny<string>()));

            demandService
                .Setup(x => x.GetJobRestrictionsForEmployee(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(restrictions);

            demandService
                .Setup(x => x.GetEmployeeRestrictionById(It.IsAny<string>()))
                .Returns(restriction);

            demandService
                .Setup(x => x.SaveJobRestriction(It.IsAny<EmployeeRestriction>()));                

            return demandService;
        }

        [TestMethod]
        public void GetWorkRestriction_InvalidCaseId_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var adminservice = AdminServiceStub(user);
            var caseService = CaseServieStub(null);
            var employerService = EmployerServieStub(null);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction());
            var request = new GetWorkRestrictionRequest("UserKey", caseService.Object, employerService.Object, demandService.Object, adminservice.Object);

            var parameter = new GetWorkRestrictionParameters
            {
                CaseNumber = "",
                EmployerId = "2"
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetWorkRestriction_CaseNotFound_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(null); // null case
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction());
            var request = new GetWorkRestrictionRequest("UserKey", caseService.Object, employerService.Object, demandService.Object, adminservice.Object);

            var parameter = new GetWorkRestrictionParameters
            {
                CaseNumber = "772655213",
                EmployerId = "2"
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.ExpectationFailed, response.StatusCode);
        }

        [TestMethod]
        public void GetWorkRestriction_EmployeeNotFound_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var cAse = EntityHelper.Case();
            cAse.Employee = null;
            var caseService = CaseServieStub(cAse); // null case
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction());
            var request = new GetWorkRestrictionRequest("UserKey", caseService.Object, employerService.Object, demandService.Object, adminservice.Object);

            var parameter = new GetWorkRestrictionParameters
            {
                CaseNumber = "772655213",
                EmployerId = "2"
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Employee not found", response.Message);
            Assert.AreEqual(HttpStatusCode.ExpectationFailed, response.StatusCode);
        }

        [TestMethod]
        public void GetWorkRestriction_InvalidEmployerId_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();

            var caseService = CaseServieStub(EntityHelper.Case()); // null case                  
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction());
            var request = new GetWorkRestrictionRequest("UserKey", caseService.Object, employerService.Object, demandService.Object, adminservice.Object);

            var parameter = new GetWorkRestrictionParameters
            {
                CaseNumber = "78676979",
                EmployerId = ""
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Missing EmployerId", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }        

        [TestMethod]
        public void GetWorkRestriction_Restrictions_Success()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(EntityHelper.Case()); // null case
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var demandService = DemandServiceStub(new List<EmployeeRestriction>(0) { EntityHelper.EmployeeRestriction() }, EntityHelper.EmployeeRestriction());
            var request = new GetWorkRestrictionRequest("UserKey", caseService.Object, employerService.Object, demandService.Object, adminservice.Object);

            var parameter = new GetWorkRestrictionParameters
            {
                CaseNumber = "78676979",
                EmployerId = "2"
            };

            // Act
            var response = request.Handle(parameter);

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(1, response.Data.Count());
            Assert.AreEqual("5bb310886e66da7c7c7418b3", response.Data.FirstOrDefault().CaseId);
            Assert.AreEqual("Walk", response.Data.FirstOrDefault().DemandName);
            Assert.AreEqual("5bb3065a2e30e05dbc8e92fb", response.Data.FirstOrDefault().DemandId);
        }


    }
}