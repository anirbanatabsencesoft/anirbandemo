﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Requests.CaseDeterminationStatus;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using cases = AbsenceSoft.Data.Cases;
namespace AT.Api.Case.Tests.Requests.CaseDetermination
{
    [TestClass]
    public class GetCaseDeterminationRequestTest
    {
        [TestMethod]
        public void GetCaseDeterminationRequest_CaseNumberEmpty_ThrowsException()
        {
            var caseNumber = "";
            var newCase = EntityHelper.Case();
            var caseDeteminationModel = EntityHelper.CaseDetermination();
            var caseService = CaseServiceStub(newCase);
            var request = new GetCaseDeterminationRequest(caseService.Object);
            var parameter = new CasePolicyParameters
            {
                CaseNumber = caseNumber
            };
            //// Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Case number was not provided", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetCaseDeterminationRequest_CaseNumberNotFound_ThrowsException()
        {
            var caseNumber = "0334";
            var newCase = EntityHelper.Case();
            var CaseDeterminationPolicyList = EntityHelper.CaseDeterminationPolicyList();
            var caseDeteminationModel = EntityHelper.CaseDetermination(Policies: CaseDeterminationPolicyList);
            var caseService = CaseServiceStub(newCase);
            var request = new GetCaseDeterminationRequest(caseService.Object);
            var parameter = new CasePolicyParameters
            {
                CaseNumber = caseNumber,
            };
            //// Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Case not found for the given case number", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetCaseDeterminationRequest_NoContent()
        {
            var caseNumber = "039";
            var ListCaseSummaryPolicy = EntityHelper.CaseSummaryPolicyList();
            var casesegment = EntityHelper.CaseSegmentList(AppliedPolicies:new List<AppliedPolicy>());
            var caseSummary = EntityHelper.caseSummary(Policies: new List<CaseSummaryPolicy>());
            var newCase = EntityHelper.Case(CaseSummary: caseSummary,segments: casesegment);
            var CaseDeterminationPolicyList = EntityHelper.CaseDeterminationPolicyList();
            var caseDeteminationModel = EntityHelper.CaseDetermination(Policies: CaseDeterminationPolicyList);
            var caseService = CaseServiceStub(newCase);
            var request = new GetCaseDeterminationRequest(caseService.Object);
            var parameter = new CasePolicyParameters
            {
                CaseNumber = caseNumber,
            };
            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.AreEqual("No policy found for case", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        [TestMethod]
        public void GetCaseDeterminationRequest_success()
        {
            var caseNumber = "039";            
            var AppliedPolicyList = EntityHelper.AppliedPolicyList();
            var casesegment = EntityHelper.CaseSegmentList(AppliedPolicies: AppliedPolicyList);
            var ListCaseSummaryPolicy = EntityHelper.CaseSummaryPolicyList(Detail: new List<CaseSummaryPolicyDetail>());
            var caseSummary = EntityHelper.caseSummary(Policies: ListCaseSummaryPolicy);
            var newCase = EntityHelper.Case(CaseSummary: caseSummary, segments: casesegment);
            var CaseDeterminationPolicyList = EntityHelper.CaseDeterminationPolicyList();
            var caseDeteminationModel = EntityHelper.CaseDetermination(Policies: CaseDeterminationPolicyList);
            var caseService = CaseServiceStub(newCase);
            var request = new GetCaseDeterminationRequest(caseService.Object);
            var parameter = new CasePolicyParameters
            {
                CaseNumber = caseNumber,
            };
            //// Act
            var response = request.Handle(parameter);

            //// Assert

            Assert.IsNotNull(response);
            Assert.AreEqual(1, response.Data.Count());
            Assert.AreEqual("PolicyCode", response.Data.FirstOrDefault().PolicyCode);
            Assert.AreEqual("PolicyName", response.Data.FirstOrDefault().PolicyName);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        private Mock<ICaseService> CaseServiceStub(cases.Case cAse)
        {
            var caseService = new Mock<ICaseService>();

            caseService
            .Setup(x => x.GetCaseByCaseNumber(
                  It.Is<string>(en => en == "039")))
                  .Returns(cAse)
                  .Verifiable();

            caseService
                .Setup(x => x.ApplyDetermination(It.IsAny<cases.Case>(), It.IsAny<string>(), It.IsAny<DateTime>(),
                It.IsAny<DateTime>(), It.IsAny<AdjudicationStatus>(), It.IsAny<string>(), It.IsAny<string>(),
                It.IsAny<string>())).Returns(cAse);
            caseService
                .Setup(x => x.EnqueueRecalcFutureCases(It.IsAny<cases.Case>()));

            caseService
              .Setup(x => x.UpdateCase(It.IsAny<cases.Case>(), null))
              .Returns(cAse);

            return caseService;
        }
    }
}
