﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Requests.CaseDeterminationStatus;
using AT.Api.Case.Requests.ChangePolicyStatus;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Net;
using cases = AbsenceSoft.Data.Cases;
namespace AT.Api.Case.Tests.Requests.CaseDetermination
{
    [TestClass]
    public class SaveCaseDeterminationRequestTest
    {

        [TestMethod]
        public void SaveCaseDeterminationRequest_CaseNumberEmpty_ThrowsException()
        {

            var caseNumber = "";
            var newCase = EntityHelper.Case();
            var caseDeteminationModel = EntityHelper.CaseDetermination();
            var caseService = CaseServiceStub(newCase);            
            var request = new SaveCaseDeterminationRequest(caseService.Object);
            var parameter = new CasePolicyParameters
            {
                CaseNumber= caseNumber,
                CaseDetermination= caseDeteminationModel
            };
            //// Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Case number was not provided", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveCaseDeterminationRequest_PolicyCodeEmpty_ThrowsException()
        {

            var caseNumber = "039";
            var newCase = EntityHelper.Case();
            var CaseDeterminationPolicyList = EntityHelper.CaseDeterminationPolicyList(PolicyCode:null);
            var caseDeteminationModel = EntityHelper.CaseDetermination(Policies: CaseDeterminationPolicyList);
            var caseService = CaseServiceStub(newCase);
            var request = new SaveCaseDeterminationRequest(caseService.Object);
            var parameter = new CasePolicyParameters
            {
                CaseNumber = caseNumber,
                CaseDetermination = caseDeteminationModel
            };
            //// Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Policy code was not provided", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveCaseDeterminationRequest_CaseNumberNotFound_ThrowsException()
        {

            var caseNumber = "0334";
            var newCase = EntityHelper.Case();
            var CaseDeterminationPolicyList = EntityHelper.CaseDeterminationPolicyList();
            var caseDeteminationModel = EntityHelper.CaseDetermination(Policies: CaseDeterminationPolicyList);
            var caseService = CaseServiceStub(newCase);
            var request = new SaveCaseDeterminationRequest(caseService.Object);
            var parameter = new CasePolicyParameters
            {
                CaseNumber = caseNumber,
                CaseDetermination = caseDeteminationModel
            };
            //// Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Case not found for the given case number", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }


        [TestMethod]
        public void SaveCaseDeterminationRequest_PoliceCodeNotFound_ThrowsException()
        {
            var caseNumber = "039";            
            var ListCaseSummaryPolicy = EntityHelper.CaseSummaryPolicyList();
            var caseSummary = EntityHelper.caseSummary(Policies: ListCaseSummaryPolicy);
            var newCase = EntityHelper.Case(CaseSummary: caseSummary);
            var CaseDeterminationPolicyList = EntityHelper.CaseDeterminationPolicyList(PolicyCode:"FMALKS");
            var caseDeteminationModel = EntityHelper.CaseDetermination(Policies: CaseDeterminationPolicyList);
            var caseService =CaseServiceStub(newCase);
            var request = new SaveCaseDeterminationRequest(caseService.Object);
            var parameter = new CasePolicyParameters
            {
                CaseNumber = caseNumber,
                CaseDetermination = caseDeteminationModel
            };
            //// Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Supplied PolicyCode not found for the given case number", response.Message);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        
        [TestMethod]
        public void SaveCaseDeterminationRequest_ThroughException_DenialResonCode()
        {
            var caseNumber = "039";
            var ListCaseSummaryPolicy = EntityHelper.CaseSummaryPolicyList();
            var caseSummary = EntityHelper.caseSummary(Policies: ListCaseSummaryPolicy);
            var newCase = EntityHelper.Case(CaseSummary: caseSummary);
            var CaseDeterminationPolicyList = EntityHelper.CaseDeterminationPolicyList();
            var caseDeteminationModel = EntityHelper.CaseDetermination(Policies: CaseDeterminationPolicyList,DeterminationStatus: AdjudicationStatus.Denied);
            var caseService = CaseServiceStub(newCase);
            var request = new SaveCaseDeterminationRequest(caseService.Object);
            var parameter = new CasePolicyParameters
            {
                CaseNumber = caseNumber,
                CaseDetermination = caseDeteminationModel
            };
            //// Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Supplied Determination Denial Reason Code not found", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }
        [TestMethod]
        public void SaveCaseDeterminationRequest_Successs_DenialResonCodeForOthers()
        {
            var caseNumber = "039";
            var ListCaseSummaryPolicy = EntityHelper.CaseSummaryPolicyList();
            var caseSummary = EntityHelper.caseSummary(Policies: ListCaseSummaryPolicy);
            var newCase = EntityHelper.Case(CaseSummary: caseSummary);
            var CaseDeterminationPolicyList = EntityHelper.CaseDeterminationPolicyList(DeterminationDenialReasonCode: "Other");
            var caseDeteminationModel = EntityHelper.CaseDetermination(Policies: CaseDeterminationPolicyList, DeterminationStatus: AdjudicationStatus.Denied);
            var caseService = CaseServiceStub(newCase);
            var request = new SaveCaseDeterminationRequest(caseService.Object);
            var parameter = new CasePolicyParameters
            {
                CaseNumber = caseNumber,
                CaseDetermination = caseDeteminationModel
            };
            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.AreEqual("Case  Determination's Policy Updated successfully", response.Message);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [TestMethod]
        public void SaveCaseDeterminationRequest_Successs_DenialResonCodeForEliminationPeriod()
        {
            var caseNumber = "039";
            var ListCaseSummaryPolicy = EntityHelper.CaseSummaryPolicyList();
            var caseSummary = EntityHelper.caseSummary(Policies: ListCaseSummaryPolicy);
            var newCase = EntityHelper.Case(CaseSummary: caseSummary);
            var CaseDeterminationPolicyList = EntityHelper.CaseDeterminationPolicyList(DeterminationDenialReasonCode: "ELIMINATIONPERIOD");
            var caseDeteminationModel = EntityHelper.CaseDetermination(Policies: CaseDeterminationPolicyList, DeterminationStatus: AdjudicationStatus.Denied);
            var caseService = CaseServiceStub(newCase);
            var request = new SaveCaseDeterminationRequest(caseService.Object);
            var parameter = new CasePolicyParameters
            {
                CaseNumber = caseNumber,
                CaseDetermination = caseDeteminationModel
            };
            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.AreEqual("Case  Determination's Policy Updated successfully", response.Message);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }
        [TestMethod]
        public void SaveCaseDeterminationRequest_Success()
        {
            var caseNumber = "039";
            var ListCaseSummaryPolicy = EntityHelper.CaseSummaryPolicyList();
            var caseSummary = EntityHelper.caseSummary(Policies: ListCaseSummaryPolicy);
            var newCase = EntityHelper.Case(CaseSummary: caseSummary);
            var CaseDeterminationPolicyList = EntityHelper.CaseDeterminationPolicyList();
            var caseDeteminationModel = EntityHelper.CaseDetermination(Policies: CaseDeterminationPolicyList);
            var caseService = CaseServiceStub(newCase);
            var request = new SaveCaseDeterminationRequest(caseService.Object);
            var parameter = new CasePolicyParameters
            {
                CaseNumber = caseNumber,
                CaseDetermination = caseDeteminationModel
            };
            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.AreEqual("Case  Determination's Policy Updated successfully", response.Message);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        private Mock<ICaseService> CaseServiceStub(cases.Case cAse)
        {
            var caseService = new Mock<ICaseService>();
            var listDenialReason = EntityHelper.DenialReasonList();
            caseService
            .Setup(x => x.GetCaseByCaseNumber(
                  It.Is<string>(en => en == "039")))
                  .Returns(cAse)
                  .Verifiable();
            caseService.Setup(x => x.GetAllDenialReasons(It.IsAny<DenialReasonTarget>())).Returns(listDenialReason);

            caseService
                .Setup(x => x.ApplyDetermination(It.IsAny<cases.Case>(), It.IsAny<string>(), It.IsAny<DateTime>(), 
                It.IsAny<DateTime>(), It.IsAny<AdjudicationStatus>(), It.IsAny<string>(), It.IsAny<string>(), 
                It.IsAny<string>())).Returns(cAse);
            caseService
                .Setup(x => x.EnqueueRecalcFutureCases(It.IsAny<cases.Case>()));

            caseService
              .Setup(x => x.UpdateCase(It.IsAny<cases.Case>(), null))
              .Returns(cAse);


            return caseService;
        }
    }
}
