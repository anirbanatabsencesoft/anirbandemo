﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Models.CaseAccommodation;
using AT.Api.Case.Requests.CaseAccommodation;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Net;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Tests.Requests.CaseAccommodation
{
    [TestClass]
    public class CancelCaseAccommodationRequestTests
    {
        [TestMethod]
        public void CancelCaseAccommodation_CaseNumberEmpty_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServiceStub(null);

            var request = new CancelCaseAccommodationRequest(caseService.Object);

            var parameter = new CancelCaseAccommodationParameters
            {
                CaseNumber = " ",
                
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Case number is required", response.Message);
        }

        [TestMethod]
        public void CancelCaseAccommodation_CaseNumberNull_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServiceStub(null);

            var request = new CancelCaseAccommodationRequest(caseService.Object);

            var parameter = new CancelCaseAccommodationParameters
            {
                CaseNumber = null,
                Model = Model()
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Case number is required", response.Message);
        }

        [TestMethod]
        public void CancelCaseAccommodation_CaseNotFound_ThrowsException()
        {
            //// Arrange            
            var caseService = CaseServiceStub(null);
            var request = new CancelCaseAccommodationRequest(caseService.Object);
            var parameter = new CancelCaseAccommodationParameters
            {
                CaseNumber = "772655213",
                Model = Model()
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Accommodation id is required", response.Message);
        }

        [TestMethod]
        public void CancelCaseAccommodation_CaseAccommodationIdNull_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServiceStub(null);
            
            var request = new CancelCaseAccommodationRequest(caseService.Object);

            var parameter = new CancelCaseAccommodationParameters
            {
                CaseNumber = "522653296",
                AccommodationId = null,
                Model = new CancelCaseAccommodationModel
                {
                    CancelReason = AccommodationCancelReason.Other,
                    Status = CaseStatus.Closed
                }
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Accommodation id is required", response.Message);
        }

        [TestMethod]
        public void CancelCaseAccommodation_NullAccommodationId_ThrowsException()
        {
            //// Arrange            
            var caseService = CaseServiceStub(null);
            var request = new CancelCaseAccommodationRequest(caseService.Object);
         
            var parameter = new CancelCaseAccommodationParameters
            {
                CaseNumber = "522653296",
                AccommodationId = "",
                Model = new CancelCaseAccommodationModel
                {
                    CancelReason= AccommodationCancelReason.Other,
                    Status = CaseStatus.Closed
                }
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Accommodation id is required", response.Message);
        }

        [TestMethod]
        public void CancelCaseAccommodation_CaseAccommodationRequestNotFound_ThrowsException()
        {
            //// Arrange
            var caseService = CaseServiceStub(EntityHelper.Case());
            var request = new CancelCaseAccommodationRequest(caseService.Object);

            var parameters = new CancelCaseAccommodationParameters
            {
                CaseNumber = "CaseNumber01",
                AccommodationId = "TypeCode01",
                Model = new CancelCaseAccommodationModel
                {                                        
                }
            };

            //// Act           
            var response = request.Handle(parameters);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Accommodation details not found with the given case", response.Message);
        }

        [TestMethod]
        public void CancelCaseAccommodation_ValidData_CancelAccommodation()
        {
            //// Arrange
            var caseEntity = EntityHelper.Case();
            var accommodationRequest = EntityHelper.AccommodationRequest();
            caseEntity.AccommodationRequest = accommodationRequest;
            var caseService = CaseServiceStub(caseEntity);
            var accommodationService = AccommodationService(caseEntity);
            var request = new CancelCaseAccommodationRequest(caseService.Object, accommodationService.Object);

            var parameters = new CancelCaseAccommodationParameters
            {
                CaseNumber = "CaseNumber01",
                AccommodationId = "AccommodationId01",
                Model = Model()
            };

            //// Act           
            var response = request.Handle(parameters);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsTrue(response.Success);
            Assert.AreEqual("Case accommodation canceled successfully", response.Message);
        }

        private Mock<ICaseService> CaseServiceStub(AbsenceSoft.Data.Cases.Case cAse)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);

            return caseService;
        }

        private Mock<IAccommodationService> AccommodationService(Cases.Case caseData)
        {
            var accommodationService = new Mock<IAccommodationService>();

            accommodationService
                .Setup(x => x.CancelAccommodationRequest(It.IsAny<Cases.Case>(), It.IsAny<string>(), It.IsAny<AccommodationCancelReason>() , It.IsAny<string>()));

            return accommodationService;
        }

        private CancelCaseAccommodationModel Model()
        {
            return new CancelCaseAccommodationModel
            {
                GeneralHealthCondition = "GeneralHealthCondition01",
                Description = "Description01",
                Status = CaseStatus.Cancelled,
                CancelReason = AccommodationCancelReason.AccommodationNoLongerNeeded,
                OtherCancelDescription = "OtherDescription01"               
            };
        }

    }
}
