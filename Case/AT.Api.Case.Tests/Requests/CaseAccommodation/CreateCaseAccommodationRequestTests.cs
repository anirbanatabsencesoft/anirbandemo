﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Models.CaseAccommodation;
using AT.Api.Case.Requests.CaseAccommodation;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Tests.Requests.CaseAccommodation
{    
    [TestClass]
    public class CreateCaseAccommodationRequestTests
    {
        [TestMethod]
        public void CreateCaseAccommodation_CaseNumberEmpty_ThrowsException()
        {
            // Arrange            
            var caseService = CaseService(null);
            
            var request = new CreateCaseAccommodationRequest(caseService.Object);

            var parameter = new CreateCaseAccommodationParameters
            {
                CaseNumber = " ",
                Model = Model()
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Case number is required", response.Message);            
        }
        
        [TestMethod]
        public void CreateCaseAccommodation_CaseNumberNull_ThrowsException()
        {
            // Arrange
            var caseService = CaseService(null);

            var request = new CreateCaseAccommodationRequest(caseService.Object);

            var parameter = new CreateCaseAccommodationParameters
            {
                CaseNumber = null,
                Model = Model()
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Case number is required", response.Message);
        }

        [TestMethod]
        public void CreateCaseAccommodation_AccommodationRequestDataIsNull_ThrowsException()
        {
            // Arrange
            var caseService = CaseService(null);

            var request = new CreateCaseAccommodationRequest(caseService.Object);

            var parameter = new CreateCaseAccommodationParameters
            {
                CaseNumber = "CaseNumber01",
                Model = null
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Accommodation detail is required", response.Message);
        }

        [TestMethod]
        public void CreateCaseAccommodation_CaseNotFound_ThrowsException()
        {
            //// Arrange
            var request = new CreateCaseAccommodationRequest(CaseService(null).Object);

            var parameters = new CreateCaseAccommodationParameters
            {
                CaseNumber = "CaseNumber01",
                Model = Model()
            };

            //// Act           
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameters));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Case not found with the given case number", response.Message);
        }
        
        [TestMethod]
        public void CreateCaseAccommodation_ValidData_CreatesAccommodation()
        {
            //// Arrange

            var accommodationRequest = EntityHelper.AccommodationRequest();
            var segment = EntityHelper.CaseSegment();
            var caseEntity = EntityHelper.Case(accommodationRequest : accommodationRequest, segments: new List<CaseSegment> { segment});            
            var caseService = CaseService(caseEntity);
            var accommodationService = AccommodationService(caseEntity);
            var request = new CreateCaseAccommodationRequest(caseService.Object, accommodationService.Object);

            var parameters = new CreateCaseAccommodationParameters
            {
                CaseNumber = "CaseNumber01",
                Model = Model()
            };

            //// Act           
            var response = request.Handle(parameters);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsTrue(response.Success);
            Assert.AreEqual("Case accommodation created successfully", response.Message);
        }

        private Mock<ICaseService> CaseService(Cases.Case caseData = null)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(caseData);

            caseService
                .Setup(x => x.UpdateCase(It.IsAny<AbsenceSoft.Data.Cases.Case>(), null))
                .Returns(caseData);

            return caseService;
        }

        private Mock<IAccommodationService> AccommodationService(Cases.Case caseData = null, IQueryable<AccommodationType> accommodationTypeList = null)
        {
            var accommodationService = new Mock<IAccommodationService>();

            accommodationService
                .Setup(x => x.CreateOrModifyAccommodationRequest(It.IsAny<Cases.Case>(), It.IsAny<string>(), It.IsAny<List<Accommodation>>()));

            accommodationService
                .Setup(x => x.GetBaseAccommodationTypes(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(accommodationTypeList);

            return accommodationService;
        }

        private AccommodationRequestModel Model()
        {
            return new AccommodationRequestModel
            {
                CaseNumber = "CaseNumber01",
                Status = 0,
                GeneralHealthCondition = "GeneralHealthCondition01",
                Description = "Description01",
                Accommodations = new List<AccommodationModel>(0){
                    new AccommodationModel {
                        TypeCode = "TypeCode01",
                        TypeName = "TypeName01",
                        Duration = AccommodationDuration.Temporary,
                        Description = "Description01",
                        IsResolved = false,
                        IsGranted = false,
                        StartDate = DateTime.UtcNow.AddDays(32),
                        EndDate = DateTime.UtcNow.AddDays(35),
                        Determination = AdjudicationStatus.Pending,
                        MinimumApprovedFromDate = null,
                        MaximumApprovedThruDate = null,
                        Status = 0,
                        IsWorkRelated = false
                    }},

                StartDate = null,
                EndDate = null,
                Determination = AdjudicationStatus.Pending,
                CancelReason = AccommodationCancelReason.AccommodationNoLongerNeeded,
                OtherReasonDescription = string.Empty

            };
        }
    }
}
