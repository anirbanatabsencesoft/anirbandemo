﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Models.CaseAccommodation;
using AT.Api.Case.Requests.CaseAccommodation;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Tests.Requests.CaseAccommodation
{
    [TestClass]
    public class GetCaseAccommodationRequestTests
    {
        [TestMethod]
        public void GetCaseAccommodation_InvalidCaseNumber_ThrowsException()
        {
            // Arrange        
            var caseService = new Mock<ICaseService>();
            var caseNumber = "772655213";
            var request = new GetCaseAccommodationRequest(caseNumber, caseService.Object);

            var parameter = new GetCaseAccommodationParameters
            {
                CaseNumber = "",
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetCaseAccommodation_CaseNotFound_ThrowsException()
        {
            //// Arrange
            var caseNumber = "772655213";
            var caseService = CaseService(null);
            var request = new GetCaseAccommodationRequest(caseNumber, caseService.Object);
            var parameter = new GetCaseAccommodationParameters
            {
                CaseNumber = "772655213"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Case not found with the given details", response.Message);
        }

        [TestMethod]
        public void GetCaseAccommodation_ValidCase_CaseAccommodation()
        {
            //// Arrange            
            var caseNumber = "772655213";
            var caseData = EntityHelper.Case();
            var accommodationRequest = EntityHelper.AccommodationRequest();
            caseData.AccommodationRequest = accommodationRequest;
            var caseService = CaseService(caseData);

            var request = new GetCaseAccommodationRequest(caseNumber, caseService.Object);

            var parameter = new GetCaseAccommodationParameters
            {
                CaseNumber = "772655213"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.AreEqual("Case accommodation details retrieved successfully", response.Message);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [TestMethod]
        public void GetCaseAccommodation_ValidCase_NoCaseAccommodation()
        {
            //// Arrange        
            var theCase = EntityHelper.Case(accommodationRequest: null);
            var caseService = CaseService(theCase);
            var caseNumber = "772655213";
            var request = new GetCaseAccommodationRequest(caseNumber, caseService.Object);

            var parameter = new GetCaseAccommodationParameters
            {
                CaseNumber = caseNumber
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Accommodation details not found with the given case", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        [TestMethod]
        public void GetCaseAccommodation_AccommodationClosed_NoCaseAccommodation()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var accommodation = EntityHelper.Accommodation();
            var caseNumber = "522653296";
            var caseentity = EntityHelper.Case();
            var caseService = CaseService(caseentity);
            var request = new GetCaseAccommodationRequest(caseNumber, caseService.Object);

            var parameter = new GetCaseAccommodationParameters
            {
                CaseNumber = "522653296"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.AreEqual("Accommodation details not found with the given case", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        private Mock<ICaseService> CaseService(Cases.Case caseData)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(s => s.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(caseData);

            return caseService;
        }

        private CaseAccommodationModel CaseAccommodationModel(
            string accommodationRequestId = "01",
            string generalHealthCondition = "TestCreation",
            string description = "TestCreation",
            CaseStatus status = CaseStatus.Open,
            AdjudicationStatus determination = AdjudicationStatus.Denied,
            string summaryDuration = "02",
            string summaryDurationType = "permanent",
            List<AccommodationModel> accommodations = null,
            DateTime? startDate = null,
            DateTime? endDate = null,
            AccommodationCancelReason cancelReason = AccommodationCancelReason.AccommodationNoLongerNeeded,
            string otherCancelReasonDescription = null)
        {
            return new CaseAccommodationModel
            {
                AccommodationRequestId = accommodationRequestId,
                GeneralHealthCondition = generalHealthCondition,
                Description = description,
                Status = status,
                Accommodations = accommodations,
                StartDate = startDate,
                EndDate = endDate,
                Determination = determination,
                SummaryDuration = summaryDuration,
                SummaryDurationType = summaryDurationType,
                CancelReason = cancelReason,
                OtherCancelReasonDescription = otherCancelReasonDescription
            };
        }
    }
}
