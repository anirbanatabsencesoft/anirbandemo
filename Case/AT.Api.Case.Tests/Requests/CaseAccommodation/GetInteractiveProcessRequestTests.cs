﻿using AbsenceSoft.Data.Notes;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Requests.CaseAccommodation;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using static AbsenceSoft.Data.Cases.AccommodationInteractiveProcess;
using Cases = AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;

namespace AT.Api.Case.Tests.Requests.CaseAccommodation
{
    [TestClass]
    public class GetInteractiveProcessRequestTests
    {
        [TestMethod]
        public void GetIntProc_NullCaseNumber_ThrowsException()
        {
            //// Arrange
            var parameters = new GetInteractiveProcessParameters
            {
                CaseNumber = null,
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(() => new GetInteractiveProcessRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Case number is required", exception.Message);
        }

        [TestMethod]
        public void GetIntProc_EmptyCaseNumber_ThrowsException()
        {
            //// Arrange
            var parameters = new GetInteractiveProcessParameters
            {
                CaseNumber = " ",
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new GetInteractiveProcessRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Case number is required", exception.Message);
        }

        [TestMethod]
        public void GetIntProc_NullAccomId_ThrowsException()
        {
            //// Arrange
            var parameters = new GetInteractiveProcessParameters
            {
                CaseNumber = "CaseNumber01",
                AccommodationId = null
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(() => new GetInteractiveProcessRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Accommodation id is required", exception.Message);
        }

        [TestMethod]
        public void GetIntProc_EmptyAccomId_ThrowsException()
        {
            //// Arrange
            var parameters = new GetInteractiveProcessParameters
            {
                CaseNumber = "Case01",
                AccommodationId = " "
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new GetInteractiveProcessRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Accommodation id is required", exception.Message);
        }

        [TestMethod]
        public void GetIntProc_NoMatchingCase_ThrowsException()
        {
            //// Arrange            
            var caseService = CaseServieStub();

            var request = new GetInteractiveProcessRequest(caseService.Object);

            var parameter = new GetInteractiveProcessParameters
            {
                CaseNumber = "772655213",
                AccommodationId = "AccomId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
               request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Case was not found for 772655213", response.Message);
        }

        [TestMethod]
        public void GetIntProc_NullAccommodationRequest_ReturnsEmpty()
        {
            //// Arrange    
            var accomId = Guid.NewGuid();
            var accommodations = new List<Cases.Accommodation>();
            var theCase = EntityHelper.Case(accommodationRequest: null);
            var caseService = CaseServieStub(theCase);

            var request = new GetInteractiveProcessRequest(caseService.Object);

            var parameter = new GetInteractiveProcessParameters
            {
                CaseNumber = "772655213",
                AccommodationId = accomId.ToString()
            };

            //// Act            
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
            Assert.AreEqual("No accommodation request available", response.Message);
        }

        [TestMethod]
        public void GetIntProc_InvalidAccommodation_ReturnsEmpty()
        {
            //// Arrange    
            var accomId = Guid.NewGuid();
            var accommodations = new List<Cases.Accommodation>();
            var accomRequest = EntityHelper.AccommodationRequest(accommodations);
            var theCase = EntityHelper.Case(accommodationRequest: accomRequest);
            var caseService = CaseServieStub(theCase);

            var request = new GetInteractiveProcessRequest(caseService.Object);

            var parameter = new GetInteractiveProcessParameters
            {
                CaseNumber = "772655213",
                AccommodationId = accomId.ToString()
            };

            //// Act            
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
            Assert.AreEqual("No accommodation found", response.Message);
        }

        [TestMethod]
        public void GetIntProc_NullAccomodationIntProc_ReturnsEmpty()
        {
            //// Arrange     
            var accomId = new Guid();
            var accommodation = EntityHelper.Accommodation(accomId, null);
            var accomRequest = EntityHelper.AccommodationRequest(new List<Cases.Accommodation> { accommodation });
            var theCase = EntityHelper.Case(accommodationRequest: accomRequest);
            var caseService = CaseServieStub(theCase);

            var request = new GetInteractiveProcessRequest(caseService.Object);

            var parameter = new GetInteractiveProcessParameters
            {
                CaseNumber = "772655213",
                AccommodationId = accomId.ToString()
            };

            //// Act            
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
            Assert.AreEqual("No accommodation interactive process found", response.Message);
        }
        [TestMethod]
        public void GetIntProc_NullAccomodationIntProcStep_ReturnsEmpty()
        {
            //// Arrange     
            var accomId = new Guid();
            var interactiveProcess = EntityHelper.AccommodationInteractiveProcess(steps: null);
            var accommodation = EntityHelper.Accommodation(accomId, interactiveProcess);
            var accomRequest = EntityHelper.AccommodationRequest(new List<Cases.Accommodation> { accommodation });
            var theCase = EntityHelper.Case(accommodationRequest: accomRequest);
            var caseService = CaseServieStub(theCase);

            var request = new GetInteractiveProcessRequest(caseService.Object);

            var parameter = new GetInteractiveProcessParameters
            {
                CaseNumber = "772655213",
                AccommodationId = accomId.ToString()
            };

            //// Act            
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
            Assert.AreEqual("No accommodation interactive process found", response.Message);
        }

        [TestMethod]
        public void GetIntProc_NoAccomodationIntProcStep_ReturnsEmpty()
        {
            //// Arrange     
            var accomId = new Guid();

            var interactiveProcess = EntityHelper.AccommodationInteractiveProcess(steps: new List<Cases.AccommodationInteractiveProcess.Step>());
            var accommodation = EntityHelper.Accommodation(accomId, interactiveProcess);
            var accomRequest = EntityHelper.AccommodationRequest(new List<Cases.Accommodation> { accommodation });
            var theCase = EntityHelper.Case(accommodationRequest: accomRequest);
            var caseService = CaseServieStub(theCase);

            var request = new GetInteractiveProcessRequest(caseService.Object);

            var parameter = new GetInteractiveProcessParameters
            {
                CaseNumber = "772655213",
                AccommodationId = accomId.ToString()
            };

            //// Act            
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
            Assert.AreEqual("No accommodation interactive process found", response.Message);
        }

        [TestMethod]
        public void GetIntProc_ValidAccommodation_ReturnsInteractiveProcess()
        {
            //// Arrange     
            var accomId = new Guid();
            var user = EntityHelper.UserStub();
            var question1 = EntityHelper.AccommodationQuestion("QId01", "Question01");
            var question2 = EntityHelper.AccommodationQuestion("QId02", "Question02");

            var step1 = EntityHelper.Step("QId01", true, "Answer01", question1);
            var step2 = EntityHelper.Step("QId02", false, null, question2);

            var steps = new List<Step> { step1, step2 };
            var interactiveProcess = EntityHelper.AccommodationInteractiveProcess(steps: steps);

            var accommodation = EntityHelper.Accommodation(accomId, interactiveProcess);
            var accomRequest = EntityHelper.AccommodationRequest(new List<Cases.Accommodation> { accommodation });
            var theCase = EntityHelper.Case(accommodationRequest: accomRequest);
            var caseNote = EntityHelper.CaseNote(Notes: "AccomNote", accomId: accomId, sAnswer: "Answer01", answer: true,
                questionId: "QId01", theCase: theCase, user: user, category: NoteCategoryEnum.InteractiveProcess);

            var caseService = CaseServieStub(theCase);
            var accomService = AccommodationServiceStub(new List<CaseNote>() { caseNote });

            var request = new GetInteractiveProcessRequest(caseService.Object, accomService.Object);

            var parameter = new GetInteractiveProcessParameters
            {
                CaseNumber = "772655213",
                AccommodationId = accomId.ToString()
            };

            //// Act            
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(2, response.Data.Count());

            var result1 = response.Data.ElementAt(0);
            Assert.AreEqual("QId01", result1.QuestionId);
            Assert.AreEqual("Question01", result1.QuestionText);
            Assert.AreEqual("Answer01", result1.Answer);
            Assert.AreEqual(1, result1.Notes.Count);
            Assert.AreEqual("Answer01", result1.Notes[0].Answer);
            Assert.AreEqual(NoteCategoryEnum.InteractiveProcess, result1.Notes[0].Category);
            Assert.AreEqual("email@email.com", result1.Notes[0].CreatedBy);
            Assert.AreEqual("AccomNote", result1.Notes[0].Note);

            var result2 = response.Data.ElementAt(1);
            Assert.AreEqual("QId02", result2.QuestionId);
            Assert.AreEqual("Question02", result2.QuestionText);
            Assert.AreEqual("False", result2.Answer);
            Assert.AreEqual(0, result2.Notes.Count);
        }

        [TestMethod]
        public void GetIntProc_CallServices_ServicesShouldBeCalled()
        {
            //// Arrange     
            var accomId = new Guid();
            var user = EntityHelper.UserStub();
            var question1 = EntityHelper.AccommodationQuestion("QId01", "Question01");
            var question2 = EntityHelper.AccommodationQuestion("QId02", "Question02");


            var step1 = EntityHelper.Step("QId01", true, "Answer01", question1);
            var step2 = EntityHelper.Step("QId02", false, null, question2);

            var steps = new List<Step> { step1, step2 };
            var interactiveProcess = EntityHelper.AccommodationInteractiveProcess(steps: steps);

            var accommodation = EntityHelper.Accommodation(accomId, interactiveProcess);
            var accomRequest = EntityHelper.AccommodationRequest(new List<Cases.Accommodation> { accommodation });
            var theCase = EntityHelper.Case(accommodationRequest: accomRequest);
            var caseNote = EntityHelper.CaseNote(Notes: "AccomNote", accomId: accomId, sAnswer: "Answer01", answer: true,
                questionId: "QId01", theCase: theCase, user: user, category: NoteCategoryEnum.InteractiveProcess);

            var caseService = new Mock<ICaseService>();
            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.Is<string>(cn => cn == "772655213")))
                .Returns(theCase)
                .Verifiable();

            var accomService = new Mock<IAccommodationService>();
            accomService
                .Setup(x => x.GetAccommIntProcCaseNotes(
                    It.Is<Guid>(aid => aid == accomId),
                    It.Is<string>(cid => cid == "5bb310886e66da7c7c7418b3")))
                .Returns(new List<CaseNote> { caseNote })
                .Verifiable();

            var request = new GetInteractiveProcessRequest(caseService.Object, accomService.Object);

            var parameter = new GetInteractiveProcessParameters
            {
                CaseNumber = "772655213",
                AccommodationId = accomId.ToString()
            };

            //// Act            
            var response = request.Handle(parameter);

            //// Assert
            caseService.Verify();
            accomService.Verify();
        }

        private Mock<ICaseService> CaseServieStub(Cases.Case cAse = null)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);

            return caseService;
        }

        private Mock<IAccommodationService> AccommodationServiceStub(List<CaseNote> caseNotes = null)
        {
            var accomService = new Mock<IAccommodationService>();

            accomService
                .Setup(x => x.GetAccommIntProcCaseNotes(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(caseNotes);

            return accomService;
        }
    }
}
