﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Notes.Contracts;
using AT.Api.Case.Models.CaseAccommodation;
using AT.Api.Case.Requests.CaseAccommodation;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using static AbsenceSoft.Data.Cases.AccommodationInteractiveProcess;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Tests.Requests.CaseAccommodation
{
    [TestClass]
    public class SaveInteractiveProcessRequestTests
    {
        [TestMethod]
        public void SaveIntProc_NullUserId_ThrowsException()
        {
            //// Arrange
            var parameters = new SaveInteractiveProcessParameters
            {
                CaseNumber = null,
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new SaveInteractiveProcessRequest(null).Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("User Id is required", exception.Message);
        }

        [TestMethod]
        public void SaveIntProc_EmptyUserId_ThrowsException()
        {
            //// Arrange
            var parameters = new SaveInteractiveProcessParameters
            {
                CaseNumber = null,
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new SaveInteractiveProcessRequest(" ").Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("User Id is required", exception.Message);
        }

        [TestMethod]
        public void SaveIntProc_UserNotFound_ThrowsException()
        {
            //// Arrange
            var adminService = AdminServiceStub(null);
            var parameters = new SaveInteractiveProcessParameters
            {
                CaseNumber = null,
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new SaveInteractiveProcessRequest("UserKey", adminuserService: adminService.Object).Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.InternalServerError, exception.StatusCode);
            Assert.AreEqual("Unable to find user", exception.Message);
        }


        [TestMethod]
        public void SaveIntProc_NullCaseNumber_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var adminService = AdminServiceStub(user);
            var parameters = new SaveInteractiveProcessParameters
            {
                CaseNumber = null,
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new SaveInteractiveProcessRequest("UserKey", adminuserService: adminService.Object).Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Case number is required", exception.Message);
        }

        [TestMethod]
        public void SaveIntProc_EmptyCaseNumber_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var adminService = AdminServiceStub(user);
            var parameters = new SaveInteractiveProcessParameters
            {
                CaseNumber = " ",
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new SaveInteractiveProcessRequest("UserKey", adminuserService: adminService.Object).Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Case number is required", exception.Message);
        }

        [TestMethod]
        public void SaveIntProc_NullCustomerId_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var adminService = AdminServiceStub(user);
            var parameters = new SaveInteractiveProcessParameters
            {
                CaseNumber = "CaseNumber01",
                CustomerId = null,
                EmployerId = "EmployerId01",
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new SaveInteractiveProcessRequest("UserKey", adminuserService: adminService.Object).Handle(parameters));


            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Customer id is required", exception.Message);
        }

        [TestMethod]
        public void SaveIntProc_EmptyCustomerId_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var adminService = AdminServiceStub(user);
            var parameters = new SaveInteractiveProcessParameters
            {
                CaseNumber = "CaseNumber01",
                CustomerId = " ",
                EmployerId = "EmployerId01",
                AccommodationId = "accomId01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new SaveInteractiveProcessRequest("UserKey", adminuserService: adminService.Object).Handle(parameters));


            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Customer id is required", exception.Message);
        }

        [TestMethod]
        public void SaveIntProc_NullEmployerId_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var adminService = AdminServiceStub(user);
            var parameters = new SaveInteractiveProcessParameters
            {
                CaseNumber = "CaseNumber01",
                CustomerId = "CustomerId01",
                EmployerId = null,
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new SaveInteractiveProcessRequest("UserKey", adminuserService: adminService.Object).Handle(parameters));


            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Employer id is required", exception.Message);
        }

        [TestMethod]
        public void SaveIntProc_EmptyEmployerId_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var adminService = AdminServiceStub(user);
            var parameters = new SaveInteractiveProcessParameters
            {
                CaseNumber = "CaseNumber01",
                CustomerId = "CustomerId01",
                EmployerId = " ",
                AccommodationId = "accomId01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new SaveInteractiveProcessRequest("UserKey", adminuserService: adminService.Object).Handle(parameters));


            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Employer id is required", exception.Message);
        }

        [TestMethod]
        public void SaveIntProc_NullAccomId_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var adminService = AdminServiceStub(user);
            var parameters = new SaveInteractiveProcessParameters
            {
                CaseNumber = "CaseNumber01",
                CustomerId = "CustomerId01",
                EmployerId = "EmployerId01",
                AccommodationId = null
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new SaveInteractiveProcessRequest("UserKey", adminuserService: adminService.Object).Handle(parameters));


            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Accommodation id is required", exception.Message);
        }

        [TestMethod]
        public void SaveIntProc_EmptyAccomId_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var adminService = AdminServiceStub(user);
            var parameters = new SaveInteractiveProcessParameters
            {
                CaseNumber = "CaseNumber01",
                CustomerId = "CustomerId01",
                EmployerId = "EmployerId01",
                AccommodationId = " "
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new SaveInteractiveProcessRequest("UserKey", adminuserService: adminService.Object).Handle(parameters));


            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Accommodation id is required", exception.Message);
        }


        [TestMethod]
        public void SaveIntProc_NullSteps_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var adminService = AdminServiceStub(user);
            var parameters = new SaveInteractiveProcessParameters
            {
                CaseNumber = "CaseNumber01",
                CustomerId = "CustomerId01",
                EmployerId = "EmployerId01",
                AccommodationId = "AccomId",
                Steps = null
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new SaveInteractiveProcessRequest("UserKey", adminuserService: adminService.Object).Handle(parameters));


            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("No data to save interactive process", exception.Message);
        }


        [TestMethod]
        public void SaveIntProc_NoMatchingCase_ThrowsException()
        {
            //// Arrange                      
            var user = EntityHelper.UserStub();

            var parameters = new SaveInteractiveProcessParameters
            {
                CaseNumber = "772655213",
                CustomerId = "CustomerId01",
                EmployerId = "EmployerId01",
                AccommodationId = "AccomId",
                Steps = new List<InteractiveProcessStepModel>()
            };

            var caseService = CaseServieStub(null);
            var adminService = AdminServiceStub(user);

            var request = new SaveInteractiveProcessRequest("UserKey", caseService.Object, adminuserService: adminService.Object);

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
               request.Handle(parameters));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Case was not found for 772655213", response.Message);
        }

        [TestMethod]
        public void SaveIntProc_NullAccommodationRequest_ReturnsEmpty()
        {
            //// Arrange    
            var accomId = Guid.NewGuid();
            var user = EntityHelper.UserStub();
            var accommodations = new List<Cases.Accommodation>();
            var theCase = EntityHelper.Case(accommodationRequest: null);

            var caseService = CaseServieStub(theCase);
            var adminService = AdminServiceStub(user);

            var request = new SaveInteractiveProcessRequest("UserKey", caseService.Object, adminuserService: adminService.Object);

            var parameter = new SaveInteractiveProcessParameters
            {
                CaseNumber = "772655213",
                CustomerId = "CustomerId01",
                EmployerId = "EmployerId01",
                AccommodationId = accomId.ToString(),
                Steps = new List<InteractiveProcessStepModel>()
            };

            //// Act            
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
            Assert.AreEqual("No accommodation request available", response.Message);
        }

        [TestMethod]
        public void SaveIntProc_NullAccommodation_ReturnsEmpty()
        {
            //// Arrange    
            var user = EntityHelper.UserStub();

            var accomId = Guid.NewGuid();
            var accomRequest = EntityHelper.AccommodationRequest(null);
            var theCase = EntityHelper.Case(accommodationRequest: accomRequest);
            var caseService = CaseServieStub(theCase);
            var adminService = AdminServiceStub(user);

            var request = new SaveInteractiveProcessRequest("UserKey", caseService.Object,
                adminuserService: adminService.Object);

            var parameter = new SaveInteractiveProcessParameters
            {
                CaseNumber = "772655213",
                CustomerId = "CustomerId01",
                EmployerId = "EmployerId01",
                AccommodationId = accomId.ToString(),
                Steps = new List<InteractiveProcessStepModel>()
            };

            //// Act            
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
            Assert.AreEqual("No accommodation found", response.Message);
        }

        [TestMethod]
        public void SaveIntProc_NoAccommodation_ReturnsEmpty()
        {
            //// Arrange    
            var user = EntityHelper.UserStub();

            var accomId = Guid.NewGuid();
            var accommodations = new List<Cases.Accommodation>();
            var accomRequest = EntityHelper.AccommodationRequest(null);
            var theCase = EntityHelper.Case(accommodationRequest: accomRequest);
            var caseService = CaseServieStub(theCase);
            var adminService = AdminServiceStub(user);

            var request = new SaveInteractiveProcessRequest("UserKey", caseService.Object,
                adminuserService: adminService.Object);

            var parameter = new SaveInteractiveProcessParameters
            {
                CaseNumber = "772655213",
                CustomerId = "CustomerId01",
                EmployerId = "EmployerId01",
                AccommodationId = accomId.ToString(),
                Steps = new List<InteractiveProcessStepModel>()
            };

            //// Act            
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
            Assert.AreEqual("No accommodation found", response.Message);
        }

        [TestMethod]
        public void SaveIntProc_InvalidQuestionId_ReturnsEmpty()
        {
            //// Arrange    
            var user = EntityHelper.UserStub();
            var accomId = Guid.NewGuid();
            var question1 = EntityHelper.AccommodationQuestion("QId01", "Question01");
            var question2 = EntityHelper.AccommodationQuestion("QId02", "Question02");

            var step1 = EntityHelper.Step("QId01", true, "Answer01", question1);
            var step2 = EntityHelper.Step("QId02", false, null, question2);

            var steps = new List<Step> { step1, step2 };
            var interactiveProcess = EntityHelper.AccommodationInteractiveProcess(steps: steps);

            var accommodation = EntityHelper.Accommodation(accomId, interactiveProcess);
            var accomRequest = EntityHelper.AccommodationRequest(new List<Cases.Accommodation> { accommodation });
            var theCase = EntityHelper.Case(accommodationRequest: accomRequest);

            var caseService = CaseServieStub(theCase);
            var adminService = AdminServiceStub(user);
            var accomService = AccommodationServiceStub(interactiveProcess);

            var request = new SaveInteractiveProcessRequest("UserKey", caseService.Object, accomService.Object,
                adminuserService: adminService.Object);

            var parameter = new SaveInteractiveProcessParameters
            {
                CaseNumber = "772655213",
                CustomerId = "CustomerId01",
                EmployerId = "EmployerId01",
                AccommodationId = accomId.ToString(),
                Steps = new List<InteractiveProcessStepModel>()
                {
                    new InteractiveProcessStepModel { QuestionId="Q000"}
                }
            };

            //// Act            
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
            Assert.AreEqual("Invalid QuestionId", response.Message);
        }

        [TestMethod]
        public void SaveIntProc_SaveExistingStep_ReturnsInteractiveProcessId()
        {
            //// Arrange    
            var user = EntityHelper.UserStub();
            var accomId = Guid.NewGuid();
            var procId = Guid.NewGuid();
            var question1 = EntityHelper.AccommodationQuestion("QId01", "Question01");
            var question2 = EntityHelper.AccommodationQuestion("QId02", "Question02");

            var step1 = EntityHelper.Step("QId01", true, "Answer01", question1);
            var step2 = EntityHelper.Step("QId02", false, null, question2);

            var steps = new List<Step> { step1, step2 };
            var interactiveProcess = EntityHelper.AccommodationInteractiveProcess(procId, steps);

            var accommodation = EntityHelper.Accommodation(accomId, interactiveProcess);
            var accomRequest = EntityHelper.AccommodationRequest(new List<Cases.Accommodation> { accommodation });
            var theCase = EntityHelper.Case(accommodationRequest: accomRequest);

            var caseService = CaseServieStub(theCase);
            var adminService = AdminServiceStub(user);
            var accomService = AccommodationServiceStub(interactiveProcess);

            var request = new SaveInteractiveProcessRequest("UserKey", caseService.Object, accomService.Object,
                adminuserService: adminService.Object);

            var parameter = new SaveInteractiveProcessParameters
            {
                CaseNumber = "772655213",
                CustomerId = "CustomerId01",
                EmployerId = "EmployerId01",
                AccommodationId = accomId.ToString(),
                Steps = new List<InteractiveProcessStepModel>()
                {
                    new InteractiveProcessStepModel { QuestionId = "QId01", Answer="Answer01"  },
                }
            };

            //// Act            
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(procId, response.Data);
            Assert.AreEqual("Accommodation interactive process saved", response.Message);

        }

        [TestMethod]
        public void SaveIntProc_SaveNewStep_ReturnsInteractiveProcessId()
        {
            //// Arrange    
            var user = EntityHelper.UserStub();
            var accomId = Guid.NewGuid();
            var procId = Guid.NewGuid();

            var question1 = EntityHelper.AccommodationQuestion("QId01", "Question01");
            var question2 = EntityHelper.AccommodationQuestion("QId02", "Question02");

            var step1 = EntityHelper.Step("QId01", true, "Answer01", question1);
            var step2 = EntityHelper.Step("QId02", false, null, question2);

            var interactiveProcess1 = EntityHelper.AccommodationInteractiveProcess(procId, new List<Step> { step1 });
            var interactiveProcess2 = EntityHelper.AccommodationInteractiveProcess(procId, new List<Step> { step1, step2 });

            var accommodation = EntityHelper.Accommodation(accomId, interactiveProcess1);
            var accomRequest = EntityHelper.AccommodationRequest(new List<Cases.Accommodation> { accommodation });
            var theCase = EntityHelper.Case(accommodationRequest: accomRequest);

            var caseService = CaseServieStub(theCase);
            var adminService = AdminServiceStub(user);
            var accomService = AccommodationServiceStub(interactiveProcess2);
            var noteService = NotesServieStub();

            var request = new SaveInteractiveProcessRequest(
                "UserKey",
                caseService.Object,
                accomService.Object,
                noteService.Object,
                adminService.Object);

            var parameter = new SaveInteractiveProcessParameters
            {
                CaseNumber = "772655213",
                CustomerId = "CustomerId01",
                EmployerId = "EmployerId01",
                AccommodationId = accomId.ToString(),
                Steps = new List<InteractiveProcessStepModel>()
                {
                    new InteractiveProcessStepModel { QuestionId = "QId02", Answer="no", Note="Note02" },
                }
            };

            //// Act            
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(procId, response.Data);
            Assert.AreEqual("Accommodation interactive process saved", response.Message);

        }

        [TestMethod]
        public void SaveIntProc_CallServices_ServicesShouldBeCalled()
        {
            //// Arrange     
            var accomId = new Guid();
            var user = EntityHelper.UserStub();
            var question1 = EntityHelper.AccommodationQuestion("QId01", "Question01");
            var question2 = EntityHelper.AccommodationQuestion("QId02", "Question02");


            var step1 = EntityHelper.Step("QId01", true, "Answer01", question1);
            var step2 = EntityHelper.Step("QId02", false, null, question2);

            var steps = new List<Step> { step1, step2 };
            var interactiveProcess = EntityHelper.AccommodationInteractiveProcess(steps: steps);

            var accommodation = EntityHelper.Accommodation(accomId, interactiveProcess);
            var accomRequest = EntityHelper.AccommodationRequest(new List<Cases.Accommodation> { accommodation });
            var theCase = EntityHelper.Case(accommodationRequest: accomRequest);
            var caseNote = EntityHelper.CaseNote(Notes: "AccomNote", accomId: accomId, sAnswer: "Answer01", answer: true,
                questionId: "QId01", theCase: theCase, user: user, category: NoteCategoryEnum.InteractiveProcess);

            var caseService = new Mock<ICaseService>();
            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.Is<string>(cn => cn == "772655213")))
                .Returns(theCase)
                .Verifiable();

            caseService
                .Setup(x => x.UpdateCase(
                    It.Is<Cases.Case>(c => c.CaseNumber == "772655213"),
                    It.IsAny<CaseEventType?>()))
                .Returns(theCase)
                .Verifiable();

            var accomService = new Mock<IAccommodationService>();
            accomService
                .Setup(x => x.GetEmployerAccommodationInteractiveProcess(
                    It.Is<string>(cid => cid == "CustomerId01"),
                    It.Is<string>(eid => eid == "EmployerId01"),
                    It.Is<string>(aid => aid == null),
                    It.Is<bool>(ip => ip == true)))
                .Returns(interactiveProcess)
                .Verifiable();

            var adminService = new Mock<IAdminUserService>();
            adminService
               .Setup(x => x.GetUserById(
                   It.Is<string>(uid => uid == "UserKey")))
               .Returns(user)
               .Verifiable();

            var noteService = NotesServieStub();

            var request = new SaveInteractiveProcessRequest(
                "UserKey",
                caseService.Object,
                accomService.Object,
                noteService.Object,
                adminService.Object);

            var parameter = new SaveInteractiveProcessParameters
            {
                CaseNumber = "772655213",
                CustomerId = "CustomerId01",
                EmployerId = "EmployerId01",
                AccommodationId = accomId.ToString(),
                Steps = new List<InteractiveProcessStepModel>()
                {
                    new InteractiveProcessStepModel { QuestionId = "QId02", Answer="no", Note="Note02" },
                }
            };

            //// Act            
            var response = request.Handle(parameter);

            //// Assert
            caseService.Verify();
            adminService.Verify();
            noteService.Verify();
            accomService.Verify();
        }

        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }

        private Mock<ICaseService> CaseServieStub(Cases.Case cAse = null)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);

            caseService
            .Setup(x => x.UpdateCase(It.IsAny<Cases.Case>(), It.IsAny<CaseEventType>()))
            .Returns(cAse);

            return caseService;
        }


        private Mock<IAccommodationService> AccommodationServiceStub(AccommodationInteractiveProcess accommodationInteractiveProcess)
        {
            var accomService = new Mock<IAccommodationService>();

            accomService
                .Setup(x => x.GetEmployerAccommodationInteractiveProcess(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>()))
                .Returns(accommodationInteractiveProcess);

            return accomService;
        }

        private Mock<INotesService> NotesServieStub(CaseNote note = null)
        {
            var noteService = new Mock<INotesService>();

            noteService
                .Setup(x => x.SaveCaseNote(It.IsAny<CaseNote>()))
                .Returns(note);

            return noteService;
        }

    }
}
