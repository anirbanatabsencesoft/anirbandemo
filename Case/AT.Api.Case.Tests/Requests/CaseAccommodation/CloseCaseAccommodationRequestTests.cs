﻿using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Models.CaseAccommodation;
using AT.Api.Case.Requests.CaseAccommodation;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Tests.Requests.CaseAccommodation
{
    [TestClass]
    public class CloseCaseAccommodationRequestTests
    {
        [TestMethod]
        public void CloseCaseAccommodation_CaseNumberEmpty_ThrowsException()
        {
            // Arrange            
            var caseService = CaseService(null);

            var request = new CloseCaseAccommodationRequest(caseService.Object);

            var parameter = new CloseCaseAccommodationParameters
            {
                CaseNumber = " ",
                Model = Model()
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Case number is required", response.Message);
        }

        [TestMethod]
        public void CloseCaseAccommodation_CaseNumberNull_ThrowsException()
        {
            // Arrange
            var caseService = CaseService(null);

            var request = new CloseCaseAccommodationRequest();

            var parameter = new CloseCaseAccommodationParameters
            {
                CaseNumber = null,
                Model = Model()
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Case number is required", response.Message);
        }

        [TestMethod]
        public void CloseCaseAccommodation_CaseNotFound_ThrowsException()
        {
            //// Arrange
            var request = new CloseCaseAccommodationRequest(CaseService(null).Object);

            var parameters = new CloseCaseAccommodationParameters
            {
                CaseNumber = "CaseNumber01",
                AccommodationId = "AccommodationId01",
                Model = Model()
            };

            //// Act           
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameters));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Case not found with the given case number", response.Message);
        }
        
        [TestMethod]
        public void CloseCaseAccommodation_CaseAccommodationIdEmpty_ThrowsException()
        {
            // Arrange
            var caseService = CaseService(null);

            var request = new CloseCaseAccommodationRequest(caseService.Object);

            var parameter = new CloseCaseAccommodationParameters
            {
                CaseNumber = "CaseNumber01",
                AccommodationId = " ",
                Model = new CloseCaseAccommodationModel
                {
                    GeneralHealthCondition = "GeneralHealthCondition01",
                    Description = "Description01"
                }
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Case accommodation id is required", response.Message);
        }

        [TestMethod]
        public void CloseCaseAccommodation_CaseAccommodationIdNull_ThrowsException()
        {
            // Arrange
            var caseService = CaseService(null);

            var request = new CloseCaseAccommodationRequest(caseService.Object);

            var parameter = new CloseCaseAccommodationParameters
            {
                CaseNumber = "CaseNumber01",
                AccommodationId = null,
                Model = new CloseCaseAccommodationModel
                {
                    GeneralHealthCondition = "GeneralHealthCondition01",
                    Description = "Description01"
                }
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Case accommodation id is required", response.Message);
        }

        [TestMethod]
        public void CloseCaseAccommodation_CaseAccommodationRequestNotFound_ThrowsException()
        {
            //// Arrange
            var caseService = CaseService(EntityHelper.Case());
            var request = new CloseCaseAccommodationRequest(caseService.Object);

            var parameters = new CloseCaseAccommodationParameters
            {
                CaseNumber = "CaseNumber01",
                AccommodationId = "AccId01",
                Model = Model()
            };

            //// Act           
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameters));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Accommodation details not found with the given case", response.Message);
        }

        [TestMethod]
        public void CloseCaseAccommodation_ValidData_CloseAccommodation()
        {
            //// Arrange
            var caseEntity = EntityHelper.Case();
            var accommodationRequest = EntityHelper.AccommodationRequest();
            caseEntity.AccommodationRequest = accommodationRequest;
            var caseService = CaseService(caseEntity);
            var accommodationService = AccommodationService(caseEntity);
            var request = new CloseCaseAccommodationRequest(caseService.Object, accommodationService.Object);

            var parameters = new CloseCaseAccommodationParameters
            {
                CaseNumber = "CaseNumber01",
                AccommodationId = "AccommodationId01",
                Model = Model()
            };

            //// Act           
            var response = request.Handle(parameters);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsTrue(response.Success);
            Assert.AreEqual("Case accommodation closed successfully", response.Message);
        }

        private Mock<ICaseService> CaseService(Cases.Case caseData = null)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(caseData);

            return caseService;
        }

        private Mock<IAccommodationService> AccommodationService(Cases.Case caseData)
        {
            var accommodationService = new Mock<IAccommodationService>();

            accommodationService
                .Setup(x => x.CloseCaseAccommodation(It.IsAny<Cases.Case>(), It.IsAny<string>(), It.IsAny<string>()));

            return accommodationService;
        }

        private CloseCaseAccommodationModel Model()
        {
            return new CloseCaseAccommodationModel
            {
                GeneralHealthCondition = "GeneralHealthCondition01",
                Description = "Description01"
            };
        }

    }
}
