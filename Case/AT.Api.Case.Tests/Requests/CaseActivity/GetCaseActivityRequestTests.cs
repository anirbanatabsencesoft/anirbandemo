﻿using System;
using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers.Contracts;
using AbsenceSoft.Logic.Notes.Contracts;
using Customers = AbsenceSoft.Data.Customers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Net;
using AbsenceSoft.Data.Security;
using System.Collections.Generic;
using System.Linq;
using AT.Api.Case.Requests.CaseActivity;
using AT.Api.Core.Exceptions;
using AbsenceSoft.Logic.Tasks.Contracts;
using AbsenceSoft.Logic;

namespace AT.Api.Case.Tests.Requests.CaseActivity
{
    [TestClass]
    public class GetCaseActivityRequestTests
    {
        [TestMethod]
        public void GetCaseActivity_InvalidCaseId_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(EntityHelper.Case());
            var todoService = ToDoServieStub(EntityHelper.ListResults());
            var communicationService = CommunicationServiceStub(EntityHelper.ListResults());
            var attachmentService = AttachmentServiceStub(EntityHelper.ListResults());
            var noteService = NotesServiceStub(null, EntityHelper.ListResults());
            var adminService = AdminServiceStub(user);

            var request = new GetCaseActivityRequest("UserKey", adminService.Object, caseService.Object, todoService.Object, communicationService.Object, noteService.Object, attachmentService.Object);

            var parameter = new GetCaseActivityParameters
            {
                CaseNumber = ""
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case number was not provided", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetCaseActivity_CaseNotFound_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(null);
            var todoService = ToDoServieStub(EntityHelper.ListResults());
            var communicationService = CommunicationServiceStub(EntityHelper.ListResults());
            var attachmentService = AttachmentServiceStub(EntityHelper.ListResults());
            var noteService = NotesServiceStub(null, EntityHelper.ListResults());
            var adminService = AdminServiceStub(user);

            var request = new GetCaseActivityRequest("UserKey", adminService.Object, caseService.Object, todoService.Object, communicationService.Object, noteService.Object, attachmentService.Object);

            var parameter = new GetCaseActivityParameters
            {
                CaseNumber = "1390275272"
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case not found", response.Message);
            Assert.AreEqual(HttpStatusCode.ExpectationFailed, response.StatusCode);
        }

        //[TestMethod]
        //public void GetCaseActivity_InvalidEmployerId_ThrowsException()
        //{
        //    // Arrange
        //    var user = EntityHelper.UserStub();

        //    var caseService = CaseServieStub(EntityHelper.Case()); // null case
        //    var diagnosisInfo = EntityHelper.DisabilityInfo();
        //    var employerService = EmployerServieStub(EntityHelper.Employer());

        //    var request = new GetCaseActivityRequest(caseService.Object);
        //    var parameter = new GetCaseActivityParameters
        //    {
        //        CaseNumber = "1390275272"
        //    };

        //    // Act
        //    var response = Assert.ThrowsException<ApiException>(() =>
        //        request.Handle(parameter));

        //    // Assert
        //    Assert.AreEqual("Missing employerId", response.Message);
        //    Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        //}

        //[TestMethod]
        //public void GetCaseActivity_InvalidEmployer_ThrowsException()
        //{
        //    // Arrange
        //    var user = EntityHelper.UserStub();
        //    var caseService = CaseServieStub(EntityHelper.Case());
        //    var employerService = EmployerServieStub(null);
        //    var request = new GetCaseActivityRequest(caseService.Object);
        //    var parameter = new GetCaseActivityParameters
        //    {
        //        CaseNumber = "772655213",
        //        EmployerId = "2"
        //    };

        //    // Act
        //    var response = Assert.ThrowsException<ApiException>(() =>
        //        request.Handle(parameter));

        //    // Assert
        //    Assert.AreEqual("Employer is not valid or guidelines data feature is not enabled.", response.Message);
        //    Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        //}

        [TestMethod]
        public void GetCaseActivity_UserNotFound_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(EntityHelper.Case(), EntityHelper.ListResults());
            var todoService = ToDoServieStub(EntityHelper.ListResults());
            var communicationService = CommunicationServiceStub(EntityHelper.ListResults());
            var attachmentService = AttachmentServiceStub(EntityHelper.ListResults());
            var noteService = NotesServiceStub(null, EntityHelper.ListResults());
            
            var adminService = new Mock<IAdminUserService>();
            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns((User)null);

            //// Act
            var request = Assert.ThrowsException<ApiException>(() =>
                new GetCaseActivityRequest("UserKey", adminService.Object, caseService.Object, todoService.Object, communicationService.Object, noteService.Object, attachmentService.Object));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.InternalServerError, request.StatusCode);
            Assert.AreEqual("Unable to find user", request.Message);
        }

        [TestMethod]
        public void GetCaseActivity_ActivityList_Success()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(EntityHelper.Case(), EntityHelper.ListResults());
            var todoService = ToDoServieStub(EntityHelper.ListResults());
            var communicationService = CommunicationServiceStub(EntityHelper.ListResults());
            var attachmentService = AttachmentServiceStub(EntityHelper.ListResults());
            var noteService = NotesServiceStub(null, EntityHelper.ListResults());
            var adminService = AdminServiceStub(user);

            var request = new GetCaseActivityRequest("UserKey", adminService.Object, caseService.Object, todoService.Object, communicationService.Object, noteService.Object, attachmentService.Object);

            var parameter = new GetCaseActivityParameters
            {
                CaseNumber = "772655213"
            };

            // Act
            var response = request.Handle(parameter);

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(true, response.Success);

        }

        private Mock<ICaseService> CaseServieStub(AbsenceSoft.Data.Cases.Case cAse, ListResults results = null)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);            

            return caseService;
        }

        private Mock<INotesService> NotesServiceStub(CaseNote note, ListResults results = null)
        {
            var noteService = new Mock<INotesService>();

            noteService
                .Setup(x => x.GetCaseNoteByCertId(It.IsAny<Guid>()))
                .Returns("Note");

            noteService
                .Setup(x => x.SaveCaseNote(It.IsAny<CaseNote>()))
                .Returns(note);

            noteService
                .Setup(x => x.GetCaseNoteListByContext(It.IsAny<ListCriteria>()))
                .Returns(results);
            

            return noteService;
        }

        private Mock<IEmployerService> EmployerServieStub(Customers.Employer employer)
        {
            var employerService = new Mock<IEmployerService>();

            employerService
                .Setup(x => x.GetById(It.IsAny<string>()))
                .Returns(employer);

            return employerService;
        }        

        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }

        private Mock<IToDoService> ToDoServieStub(ListResults results)
        {
            var service = new Mock<IToDoService>();

            service
                .Setup(x => x.ToDoItemList(It.IsAny<User>(), It.IsAny<ListCriteria>()))
                .Returns(results);

            return service;
        }

        private Mock<IAttachmentService> AttachmentServiceStub(ListResults results)
        {
            var service = new Mock<IAttachmentService>();

            service
                .Setup(x => x.GetAttachmentListByContext(It.IsAny<ListCriteria>()))
                .Returns(results);

            return service;
        }

        private Mock<ICommunicationService> CommunicationServiceStub(ListResults results)
        {
            var service = new Mock<ICommunicationService>();

            service
                .Setup(x => x.CommunicationList(It.IsAny<ListCriteria>()))
                .Returns(results);

            return service;
        }
    }
}