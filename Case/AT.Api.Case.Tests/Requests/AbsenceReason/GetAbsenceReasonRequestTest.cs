﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Net;
using System.Threading.Tasks;
using Customers = AbsenceSoft.Data.Customers;
using AT.Api.Case.Requests.AbsenceReason;
namespace AT.Api.Case.Tests.Requests.AbsenceReason
{
    [TestClass]
    public class GetAbsenceReasonRequestTest
    {
        [TestMethod]
        public  void AbsenceReasonRequest_EmptyEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var newCase = EntityHelper.Case();
            var customer = EntityHelper.Customer();
            var Employer = EntityHelper.Employer();
            var employeeService = EmployeeServiceStub(employee);
            var caseService = CaseCaseServiceStub(newCase);

            var request = new GetAbsenceReasonRequest(
                employeeService.Object, caseService.Object);

            var parameter = new GetAbsenceReasonParameters
            {
                employeeNumber = "",
                CaseType = CaseType.Consecutive,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>request.Handle(parameter));
            //// Assert
            Assert.AreEqual("Employee number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }
        [TestMethod]
        public void AbsenceReasonRequest_EmployeeNumberNotFound_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var newCase = EntityHelper.Case();
            var Employer = EntityHelper.Employer();
            var employeeService = EmployeeServiceStub(employee);
            var caseService = CaseCaseServiceStub(newCase);

            var request = new GetAbsenceReasonRequest(
                employeeService.Object, caseService.Object);

            var parameter = new GetAbsenceReasonParameters
            {
                employeeNumber = "21",
                CaseType = CaseType.Consecutive,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
              request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee not found for the given employee number", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }



        [TestMethod]
        public void AbsenceReasonRequest_AbsenceReason_Success()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var newCase = EntityHelper.Case();
            var customer = EntityHelper.Customer();
            var Employer = EntityHelper.Employer();
            var employeeService = EmployeeServiceStub(employee);

            var AbsenceReason = EntityHelper.AbsenceReason();
            var caseService = CaseCaseServiceStub(newCase);
            var request = new GetAbsenceReasonRequest(
                employeeService.Object, caseService.Object);

            var parameter = new GetAbsenceReasonParameters
            {
                employeeNumber = "EmployerId01",
                CaseType = CaseType.Consecutive,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
            };

            //// Act
            //var response = await request.HandleAsync(parameter);
            var response =  request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);

            //Assert.AreEqual(2, response
            //Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }
        private Mock<IEmployeeService> EmployeeServiceStub(Customers.Employee employee)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.Is<string>(en => en == "EmployerId01"), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee).Verifiable();
            return employeeService;
        }
        private Mock<ICaseService> CaseCaseServiceStub(AbsenceSoft.Data.Cases.Case cAse)
        {
            var caseService = new Mock<ICaseService>();
            var AbsenceReasonList = EntityHelper.AbsenceReasonList();

            caseService
              .Setup(x => x.GetAbsenceReasons(It.IsAny<string>(), It.IsAny<CaseType?>()))
              .Returns(AbsenceReasonList);

            return caseService;
        }
    }
}
