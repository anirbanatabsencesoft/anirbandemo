﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Requests.ReadWorkReleted;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AT.Api.Case.Tests.Requests.WorkReleted
{
    [TestClass]
    public class ReadCaseWorkReletedRequestTest
    {
        [TestMethod]
        public void ReadCaseWorkReletedRequest_CaseNumberEmpty_ThrowsException()
        {
            var caseNumber = "";
            var newCase = EntityHelper.Case();
            var workReleted = EntityHelper.WorkReleted();
            var workReletedInfo = EntityHelper.WorkRelatedInfo();
            var caseService = CaseCaseServiceStub(newCase);

            var request = new ReadWorkReletedRequest(
                caseService.Object);

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>request.Handle(caseNumber));

            //// Assert
            Assert.AreEqual("CaseNumber is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void ReadCaseWorkReletedRequest_CaseNumberNotFound_ThrowsException()
        {
            var caseNumber = "035";
            var newCase = EntityHelper.Case();
            var workReleted = EntityHelper.WorkReleted();
            var workReletedInfo = EntityHelper.WorkRelatedInfo();
            var caseService = CaseCaseServiceStub(newCase);

            var request = new ReadWorkReletedRequest(
                caseService.Object);

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(caseNumber));

            //// Assert
            Assert.AreEqual("Case not found for the given case number", response.Message);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }
        [TestMethod]
        public void ReadCaseWorkReletedRequest_ReadWorkReleted_Success()
        {
            var newCase = EntityHelper.Case();
            var workReleted = EntityHelper.WorkReleted();
            var workReletedInfo = EntityHelper.WorkRelatedInfo();
            newCase.WorkRelated = EntityHelper.WorkRelatedInfo();
            var caseService = CaseCaseServiceStub(newCase);

            var request = new ReadWorkReletedRequest(
                caseService.Object);

            //// Act
            var response = request.Handle(newCase.CaseNumber);

            //// Assert
            Assert.AreEqual("Work Releted Retrieved successfully", response.Message);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        private Mock<ICaseService> CaseCaseServiceStub(AbsenceSoft.Data.Cases.Case cAse)
        {
            var caseService = new Mock<ICaseService>();

            caseService
            .Setup(x => x.GetCaseByCaseNumber(
                  It.Is<string>(en => en == "772655213")))
                  .Returns(cAse)
                  .Verifiable();

            caseService
              .Setup(x => x.UpdateCase(It.IsAny<AbsenceSoft.Data.Cases.Case>(), null))
              .Returns(cAse);



            return caseService;
        }
    }
}
