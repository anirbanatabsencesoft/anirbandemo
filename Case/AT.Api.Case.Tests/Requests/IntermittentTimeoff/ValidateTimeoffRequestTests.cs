﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Models.IntermittentTimeoff;
using AT.Api.Case.Requests.IntermittentTimeOff;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AT.Api.Case.Tests.Requests.IntermittentTimeoff
{
    [TestClass]
    public class ValidateTimeoffRequestTests
    {
        [TestMethod]
        public void ValidateTimeoffRequest_InvalidCaseId_ThrowsException()
        {
            //// Arrange
            var caseService = new Mock<ICaseService>();
            IntermittentTimeOffModel model = new IntermittentTimeOffModel();
            model.RequestDate = DateTime.Parse("2018-10-02");

            var request = new ValidateTimeoffRequest();
            var parameter = new ValidateTimeoffRequestParameters
            {
                CaseNumber = "",
                IntermittentTimeOff = model
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Case number was not provided", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void ValidateTimeoffRequest_InvalidRequestDate_ThrowsException()
        {
            //// Arrange
            var caseService = new Mock<ICaseService>();
            IntermittentTimeOffModel model = new IntermittentTimeOffModel();

            model.RequestDate = DateTime.MinValue;

            var request = new ValidateTimeoffRequest();
            var parameter = new ValidateTimeoffRequestParameters
            {
                CaseNumber = "1390275272",
                IntermittentTimeOff = model
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Requested date was not provided", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void ValidateTimeoffRequest_NoIntermittentType_ThrowsException()
        {
            //// Arrange
            var caseService = new Mock<ICaseService>();
            IntermittentTimeOffModel model = new IntermittentTimeOffModel();
            model.RequestDate = DateTime.Parse("2018-10-02");
            model.IntermittentType = null;

            var request = new ValidateTimeoffRequest();
            var parameter = new ValidateTimeoffRequestParameters
            {
                CaseNumber = "1390275272",
                IntermittentTimeOff = model
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Intermittent Type was not provided. Set value to 0 = OfficeVisit, 1 = Incapacity", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void ValidateTimeoffRequest_CaseNotFound_ThrowsException()
        {
            //// Arrange
            var caseService = CaseServieStub(null, null);
            IntermittentTimeOffModel model = new IntermittentTimeOffModel();
            model.RequestDate = DateTime.Parse("2018-10-02");
            model.IntermittentType = 1;

            var request = new ValidateTimeoffRequest(caseService.Object);
            var parameter = new ValidateTimeoffRequestParameters
            {
                CaseNumber = "1390275272",
                IntermittentTimeOff = model
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.AreEqual(1, response.Data.Count());
            Assert.AreEqual("Case not found", response.Data.FirstOrDefault().Message);
            Assert.AreEqual("Error", response.Data.FirstOrDefault().ValidationType.ToString());
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [TestMethod]
        public void ValidateTimeoffRequest_CaseFound_ReturnsValidationMessages()
        {
            // Arrange            
            IntermittentTimeOffModel model = new IntermittentTimeOffModel();

            model.RequestDate = DateTime.Parse("2018-10-02");
            model.IntermittentType = 1;
            model.ApprovedTime = 60;
            model.Notes = "Unit test note";

            var cAse = EntityHelper.Case();
            var intermittentTimeRequestDetail = EntityHelper.IntermittentTimeRequestDetail();
            var intermittentTimeRequest = EntityHelper.IntermittentTimeRequest();

            var policySummary = EntityHelper.PolicySummary();
            var policySummary2 = EntityHelper.PolicySummary("WA-FML", "Washington Family Leave", PolicyType.StateFML);
            List<PolicySummary> ps = new List<PolicySummary>();
            ps.Add(policySummary);
            ps.Add(policySummary2);

            List<IntermittentTimeRequest> UserRequests = new List<IntermittentTimeRequest>();
            intermittentTimeRequest.Detail = new List<IntermittentTimeRequestDetail>();
            intermittentTimeRequest.Detail.Add(intermittentTimeRequestDetail);
            UserRequests.Add(intermittentTimeRequest);

            var caseSegment = EntityHelper.CaseSegment(DateTime.Parse("2018-10-02"), DateTime.Parse("2018-10-30"), UserRequests);
            cAse.Segments = new List<CaseSegment>();
            cAse.Segments.Add(caseSegment);

            List<ValidationMessage> vms = new List<ValidationMessage>();
            vms.Add(EntityHelper.ValidationMessage());
            vms.Add(EntityHelper.ValidationMessage("Error", ValidationType.Error));

            var caseService = CaseServieStub(cAse, ps, vms);

            var request = new ValidateTimeoffRequest(caseService.Object);

            var parameter = new ValidateTimeoffRequestParameters
            {
                CaseNumber = "1390275272",
                IntermittentTimeOff = model
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert            
            Assert.IsNotNull(response);
            Assert.AreEqual(2, response.Data.Count());
            Assert.AreEqual("Success", response.Data.FirstOrDefault().Message);
        }

        private Mock<ICaseService> CaseServieStub(AbsenceSoft.Data.Cases.Case cAse, List<PolicySummary> ps = null, List<ValidationMessage> pv = null)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);

            caseService
                .Setup(x => x.GetEmployeePolicySummaryByCaseId(It.IsAny<string>(), null))
                .Returns(ps);

            caseService
                .Setup(x => x.PreValidateTimeOffRequest(It.IsAny<string>(), It.IsAny<List<IntermittentTimeRequest>>()))
                .Returns(pv);

            return caseService;
        }

        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }
    }
}