﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Models.IntermittentTimeoff;
using AT.Api.Case.Requests.IntermittentTimeOff;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;

namespace AT.Api.Case.Tests.Requests.IntermittentTimeoff
{
    [TestClass]
    public class CreateIntermittentTimeoffRequestTests
    {
        [TestMethod]
        public void CreateIntermittentTimeoffRequest_InvalidCaseId_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(null, null);
            var adminService = AdminServiceStub(user);
            IntermittentTimeOffModel model = new IntermittentTimeOffModel();

            model.RequestDate = DateTime.Parse("2018-10-02");
            var request = new CreateIntermittentTimeoffRequest("UserKey", adminService.Object, caseService.Object);

            var parameter = new CreateIntermittentTimeoffRequestParameters
            {
                CaseNumber = "",
                IntermittentTimeOff = model
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case number was not provided", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateIntermittentTimeoffRequest_InvalidRequestDate_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(null, null);
            var adminService = AdminServiceStub(user);
            IntermittentTimeOffModel model = new IntermittentTimeOffModel();

            model.RequestDate = DateTime.MinValue;

            var request = new CreateIntermittentTimeoffRequest("UserKey", adminService.Object, caseService.Object);
            var parameter = new CreateIntermittentTimeoffRequestParameters
            {
                CaseNumber = "1390275272",
                IntermittentTimeOff = model
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Requested date was not provided", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateIntermittentTimeoffRequest_NoIntermittentType_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(null, null);
            var adminService = AdminServiceStub(user);
            IntermittentTimeOffModel model = new IntermittentTimeOffModel();
            model.RequestDate = DateTime.Parse("2018-10-02");
            model.IntermittentType = null;

            var request = new CreateIntermittentTimeoffRequest("UserKey", adminService.Object, caseService.Object);
            var parameter = new CreateIntermittentTimeoffRequestParameters
            {
                CaseNumber = "1390275272",
                IntermittentTimeOff = model
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Intermittent Type was not provided. Set value to 0 = OfficeVisit, 1 = Incapacity", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateIntermittentTimeoffRequest_CaseNotFound_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(null, null);
            var adminService = AdminServiceStub(user);

            IntermittentTimeOffModel model = new IntermittentTimeOffModel();
            model.RequestDate = DateTime.Parse("2018-10-02");
            model.IntermittentType = 1;
            var request = new CreateIntermittentTimeoffRequest("UserKey", adminService.Object, caseService.Object);
            var parameter = new CreateIntermittentTimeoffRequestParameters
            {
                CaseNumber = "1390275272",
                IntermittentTimeOff = model
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert            
            Assert.AreEqual("Case not found", response.Message);
            Assert.AreEqual(HttpStatusCode.ExpectationFailed, response.StatusCode);
        }

        [TestMethod]
        public void CreateIntermittentTimeoffRequest_AppliedPoliciesNotFound_ThrowsException()
        {
            // Arrange
            var newCase = EntityHelper.Case();
            var caseSegment = EntityHelper.CaseSegment(DateTime.Parse("2018-10-02"), DateTime.Parse("2018-10-30"),null);
            newCase.Segments = new List<CaseSegment>();
            newCase.Segments.Add(caseSegment);
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(newCase, null);
            var adminService = AdminServiceStub(user);
            

            IntermittentTimeOffModel model = new IntermittentTimeOffModel();
            model.RequestDate = DateTime.Parse("2018-10-02");
            model.IntermittentType = 1;
            var request = new CreateIntermittentTimeoffRequest("UserKey", adminService.Object, caseService.Object);
            var parameter = new CreateIntermittentTimeoffRequestParameters
            {
                CaseNumber = newCase.CaseNumber,
                IntermittentTimeOff = model
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert            
            Assert.AreEqual("Requested Date is Invalid, Cannnot Process this Request", response.Message);
            Assert.AreEqual(HttpStatusCode.ExpectationFailed, response.StatusCode);
        }

        [TestMethod]
        public void CreateIntermittentTimeoffRequest_SignleIntermittentRequest_ReturnsSuccessfullyCreatedMessages()
        {
            // Arrange           
            var user = EntityHelper.UserStub();
            var adminService = AdminServiceStub(user);
            IntermittentTimeOffModel model = new IntermittentTimeOffModel();
            model.RequestDate = DateTime.Parse("2018-10-02");
            model.IntermittentType = 1;
            model.ApprovedTime = 60;

            var cAse = EntityHelper.Case();
            var intermittentTimeRequestDetail = EntityHelper.IntermittentTimeRequestDetail();      
            var intermittentTimeRequest = EntityHelper.IntermittentTimeRequest();

            var policySummary = EntityHelper.PolicySummary();
            var policySummary2 = EntityHelper.PolicySummary("WA-FML", "Washington Family Leave", PolicyType.StateFML);
            List<PolicySummary> ps = new List<PolicySummary>();
            ps.Add(policySummary);
            ps.Add(policySummary2);

            List<IntermittentTimeRequest> UserRequests = new List<IntermittentTimeRequest>();
            intermittentTimeRequest.Detail = new List<IntermittentTimeRequestDetail>();
            intermittentTimeRequest.Detail.Add(intermittentTimeRequestDetail);
            UserRequests.Add(intermittentTimeRequest);

            var caseSegment = EntityHelper.CaseSegment(DateTime.Parse("2018-10-02"), DateTime.Parse("2018-10-30"), UserRequests);
            caseSegment.AppliedPolicies.Add(EntityHelper.AppliedPolicy(EntityHelper.Policy()));
            cAse.Segments = new List<CaseSegment>();
            cAse.Segments.Add(caseSegment);


            List<ValidationMessage> vms = new List<ValidationMessage>();
            vms.Add(EntityHelper.ValidationMessage());
            vms.Add(EntityHelper.ValidationMessage("Error", ValidationType.Error));
            var caseNote = EntityHelper.CaseNote();
            var caseService = CaseServieStub(cAse, ps, vms, caseNote);

            var request = new CreateIntermittentTimeoffRequest("User", adminService.Object, caseService.Object);

            var parameter = new CreateIntermittentTimeoffRequestParameters
            {
                CaseNumber = "1390275272",
                IntermittentTimeOff = model
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert            
            Assert.IsNotNull(response);
            Assert.AreEqual("True", response.Data);
            Assert.AreEqual("Intermittent timeoff request saved successfully", response.Message);
        }

        [TestMethod]
        public void CreateIntermittentTimeoffRequest_BulkIntermittentRequest_ReturnsSuccessfullyCreatedMessages()
        {
            // Arrange           
            var user = EntityHelper.UserStub();
            var adminService = AdminServiceStub(user);

            List<IntermittentTimeOffModel> timeoffs = new List<IntermittentTimeOffModel>();
            IntermittentTimeOffModel model = new IntermittentTimeOffModel();
            model.RequestDate = DateTime.Parse("2018-10-02");
            model.IntermittentType = 1;
            model.ApprovedTime = 60;
            IntermittentTimeOffModel model2 = new IntermittentTimeOffModel();
            model2.RequestDate = DateTime.Parse("2018-10-03");
            model2.IntermittentType = 1;
            model2.ApprovedTime = 90;
            timeoffs.Add(model2);

            var cAse = EntityHelper.Case();
            var intermittentTimeRequestDetail = EntityHelper.IntermittentTimeRequestDetail();
            var intermittentTimeRequest = EntityHelper.IntermittentTimeRequest();

            var policySummary = EntityHelper.PolicySummary();
            var policySummary2 = EntityHelper.PolicySummary("WA-FML", "Washington Family Leave", PolicyType.StateFML);
            List<PolicySummary> ps = new List<PolicySummary>();
            ps.Add(policySummary);
            ps.Add(policySummary2);

            List<IntermittentTimeRequest> userRequests = new List<IntermittentTimeRequest>();
            intermittentTimeRequest.Detail = new List<IntermittentTimeRequestDetail>();
            intermittentTimeRequest.Detail.Add(intermittentTimeRequestDetail);
            userRequests.Add(intermittentTimeRequest);

            var caseSegment = EntityHelper.CaseSegment(DateTime.Parse("2018-10-02"), DateTime.Parse("2018-10-30"), userRequests);
            caseSegment.AppliedPolicies.Add(EntityHelper.AppliedPolicy(EntityHelper.Policy()));
            cAse.Segments = new List<CaseSegment>();
            cAse.Segments.Add(caseSegment);


            List<ValidationMessage> vms = new List<ValidationMessage>();
            vms.Add(EntityHelper.ValidationMessage());
            vms.Add(EntityHelper.ValidationMessage("Error", ValidationType.Error));

            var caseNote = EntityHelper.CaseNote();
            var caseService = CaseServieStub(cAse, ps, vms, caseNote);

            var request = new CreateBulkIntermittentTimeoffRequest("User", adminService.Object, caseService.Object);

            var parameter = new CreateBulkIntermittentTimeoffRequestParameters
            {
                CaseNumber = "1390275272",
                intermittentTimeOffs = timeoffs
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert            
            Assert.IsNotNull(response);
            Assert.AreEqual("True", response.Data);
            Assert.AreEqual("Intermittent timeoff request saved successfully", response.Message);
        }

        private Mock<ICaseService> CaseServieStub(AbsenceSoft.Data.Cases.Case cAse, List<PolicySummary> ps = null, List<ValidationMessage> pv = null, CaseNote note = null)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);

            caseService
                .Setup(x => x.GetEmployeePolicySummaryByCaseId(It.IsAny<string>(), null))
                .Returns(ps);

            caseService
                .Setup(x => x.PreValidateTimeOffRequest(It.IsAny<string>(), It.IsAny<List<IntermittentTimeRequest>>()))
                .Returns(pv);

            caseService
                .Setup(x => x.UpdateCase(It.IsAny<AbsenceSoft.Data.Cases.Case>(), null))
                .Returns(cAse);

            caseService
                .Setup(x => x.CreateOrModifyTimeOffRequest(It.IsAny<AbsenceSoft.Data.Cases.Case>(), It.IsAny<List<IntermittentTimeRequest>>()))
                .Returns(cAse);

            caseService
                .Setup(x => x.IntermittentAbsenceEntryNote(It.IsAny<AbsenceSoft.Data.Cases.Case>(), It.IsAny<AbsenceSoft.Data.Cases.IntermittentTimeRequest>(), It.IsAny<User>()))
                .Returns(note);

            return caseService;
        }

        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }
    }
}