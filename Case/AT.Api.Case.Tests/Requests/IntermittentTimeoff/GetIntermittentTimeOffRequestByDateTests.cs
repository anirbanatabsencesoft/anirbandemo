﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using Cases = AbsenceSoft.Data.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using AT.Api.Case.Requests.IntermittentTimeOff;

namespace AT.Api.Case.Tests.Requests.IntermittentTimeoff
{
    [TestClass]
    public class GetIntermittentTimeOffRequestByDateTests
    {
        [TestMethod]
        public void GetIntermittentTimeoff_InvalidCaseId_ThrowsException()
        {
            //// Arrange
            var caseService = new Mock<ICaseService>();

            var request = new GetIntermittentTimeOffRequestByDate();

            var parameter = new GetIntermittentTimeOffRequestParameters
            {
                CaseNumber = "",
                RequestDate = new DateTime(2018, 10, 02)
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Case number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetIntermittentTimeoff_CaseNotFound_ThrowsException()
        {
            //// Arrange
            var dateTime = DateTime.UtcNow;
            var caseData = EntityHelper.Case();
            var intermittentTimeRequest = EntityHelper.IntermittentTimeRequest();
            var intermittentTimeRequestDetail = EntityHelper.IntermittentTimeRequestDetail();

            var caseService = CaseServiceStub(null, null);

            var request = new GetIntermittentTimeOffRequestByDate(caseService.Object);

            var parameter = new GetIntermittentTimeOffRequestParameters
            {
                CaseNumber = "1390275272",
                RequestDate = dateTime
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Case not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetIntermittentTimeoff_CaseFound_ReturnsIntermittentTimeOff()
        {
            //// Arrange  
            var intermittentTimeRequestDetail = new List<IntermittentTimeRequestDetail>
            {
                EntityHelper.IntermittentTimeRequestDetail()
            };

            var userRequests = new List<IntermittentTimeRequest>
            {
                EntityHelper.IntermittentTimeRequest(detail: intermittentTimeRequestDetail)
            };

            var segments = new List<CaseSegment>
            {
                EntityHelper.CaseSegment(DateTime.Parse("2018-10-02"), DateTime.Parse("2018-10-30"), userRequests)
            };

            var caseData = EntityHelper.Case(segments: segments);
            var policySummary1 = EntityHelper.PolicySummary();
            var policySummary2 = EntityHelper.PolicySummary("WA-FML", "Washington Family Leave", PolicyType.StateFML);

            var policySummaries = new List<PolicySummary>
            {
                policySummary1,
                policySummary2
            };

            var caseService = CaseServiceStub(caseData, policySummaries);

            var request = new GetIntermittentTimeOffRequestByDate(caseService.Object);

            var parameter = new GetIntermittentTimeOffRequestParameters
            {
                CaseNumber = "1390275272",
                RequestDate = new DateTime(2018, 10, 03)
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert            
            Assert.IsNotNull(response);
           
            // TODO: check for values getting returned
        }

        // TODO: Write test cases for services being actually called and data being sent in them

        private Mock<ICaseService> CaseServiceStub(Cases.Case caseData, List<PolicySummary> policySummaries)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(caseData);

            caseService
                .Setup(x => x.GetEmployeePolicySummaryByCaseId(It.IsAny<string>(), null))
                .Returns(policySummaries);

            return caseService;
        }

        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }

    }
}
