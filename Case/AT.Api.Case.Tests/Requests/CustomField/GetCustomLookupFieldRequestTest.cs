﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Case.Requests.CustomField;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Net;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Case.Tests.Requests.CustomField
{
    [TestClass]
    public class GetCustomLookupFieldRequestTest
    {
        [TestMethod]
        public void CustomFieldRequest_userNotFound_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            var customer = EntityHelper.Customer();
            var Employer = EntityHelper.Employer();
            var employerService = employerServicestub(customer, Employer, user);
            var adminservice = AdminServiceStub(user);
            var parameter = new CustomFieldParameter
            {
                employeeNumber = "employee01",
                caseTarget = EntityTarget.Case,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
            };
            //Act
            var request = Assert.ThrowsException<ApiException>(() =>
              new GetCustomFieldRequest("UserId0102", Employer.Id, customer.Id, employerService.Object, adminservice.Object));

            //// Assert
            ///
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.InternalServerError, request.StatusCode);
            Assert.AreEqual("Unable to find user", request.Message);
        }

        [TestMethod]
        public void CustomFieldRequest_CustomField_Success()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            var customer = EntityHelper.Customer();
            var Employer = EntityHelper.Employer();
            var employerService = employerServicestub(customer, Employer, user);
            var adminservice = AdminServiceStub(user);
            var CustomFieldlist = EntityHelper.CustomFieldList();
            var parameter = new CustomFieldParameter
            {
                employeeNumber = "employee01",
                caseTarget = EntityTarget.Case,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
            }; 
            var request = new GetCustomFieldRequest(user.Id, Employer.Id, customer.Id, employerService.Object, adminservice.Object);

            var response = request.Handle(parameter);

            //// Assert

            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        private Mock<IEmployeeService> EmployeeServiceStub(Customers.Employee employee)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.Is<string>(en => en == "EmployerId01"), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee).Verifiable();
            return employeeService;
        }

        public Mock<IEmployerService> employerServicestub(Customers.Customer customers, Employer employer, User user)
        {
            var employerService = new Mock<IEmployerService>();
            var CustomFieldlist = EntityHelper.CustomFieldList();
            employerService
                  .Setup(x => x.GetById(It.Is<string>(en => en == "EmployerId01")))
                  .Returns(employer).Verifiable();

            employerService
                  .Setup(x => x.GetCustomFields(It.IsAny<EntityTarget>(), It.IsAny<bool>()))
                  .Returns(CustomFieldlist);

            employerService
                  .Setup(x => x.GetCustomFields(null, null))
                  .Returns(CustomFieldlist);
            return employerService;

        }
        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
           .Setup(x => x.GetUserById(It.Is<string>(userid => userid == "UserId01")))
           .Returns(user).Verifiable();

            return adminService;
        }



    }
}
