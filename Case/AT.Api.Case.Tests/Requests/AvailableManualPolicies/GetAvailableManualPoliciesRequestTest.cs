﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using AT.Api.Case.Requests.AvailableManualPolicies;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AbsenceSoft.Logic.Customers.Contracts;

namespace AT.Api.Case.Tests.Requests.AvailableManualPolicies
{
    [TestClass]
    public class GetAvailableManualPoliciesRequestTest
    {
        private object request;

        [TestMethod]
        public void AvailableManualPoliciesRequest_CaseNumberEmpty_ThrowsException()
        {

            var caseNumber = "";
            var newCase = EntityHelper.Case();
            var Employer = EntityHelper.Employer();
            var user = EntityHelper.UserStub();
            var customer = EntityHelper.Customer();
            var caseService = CaseCaseServiceStub(newCase);
            var empployerService = EmployerServiceStub(Employer);
            var CaseEligibilityService = CaseEligibilityServiceStub(newCase, Employer);
            var adminservice = AdminServiceStub(user, customer);
            var request = new GetAvailableManualPoliciesRequest(customer.Id, CaseEligibilityService.Object, caseService.Object, empployerService.Object);
            //// Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(caseNumber));

            //// Assert
            Assert.AreEqual("CaseNumber is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void AvailableManualPoliciesRequest_CaseNumberNotFound_ThrowsException()
        {
            var caseNumber = "035";
            var newCase = EntityHelper.Case();
            var Employer = EntityHelper.Employer();
            var user = EntityHelper.UserStub();
            var customer = EntityHelper.Customer();
            var caseService = CaseCaseServiceStub(newCase);
            var empployerService = EmployerServiceStub(Employer);
            var CaseEligibilityService = CaseEligibilityServiceStub(newCase, Employer);
            var adminservice = AdminServiceStub(user, customer);
            var request = new GetAvailableManualPoliciesRequest(customer.Id, CaseEligibilityService.Object, caseService.Object, empployerService.Object);
            //// Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(caseNumber));

            //// Assert
            Assert.AreEqual("Case not found for the given case number", response.Message);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [TestMethod]

        public void AvailableManualPoliciesRequest_AvailableManual_succeess()
        {

            var caseNumber = "039";
            var newCase = EntityHelper.Case();
            var Employer = EntityHelper.Employer();
            var user = EntityHelper.UserStub();
            var customer = EntityHelper.Customer();
            var caseService = CaseCaseServiceStub(newCase);
            var empployerService = EmployerServiceStub(Employer);
            var CaseEligibilityService = CaseEligibilityServiceStub(newCase, Employer);
            var adminservice = AdminServiceStub(user, customer);
            var request = new GetAvailableManualPoliciesRequest(customer.Id, CaseEligibilityService.Object, caseService.Object,   empployerService.Object);
            //// Act
            var response =request.Handle(caseNumber);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(1, response.Data.Count());
            Assert.AreEqual("Name01", response.Data.FirstOrDefault().Name);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        private Mock<ICaseService> CaseCaseServiceStub(AbsenceSoft.Data.Cases.Case cAse)
        {
            var caseService = new Mock<ICaseService>();

            caseService
            .Setup(x => x.GetCaseByCaseNumber(
                  It.Is<string>(en => en == "039")))
                  .Returns(cAse)
                  .Verifiable();

            return caseService;
        }
        private Mock<IEmployerService> EmployerServiceStub(Employer employer)
        {
            var employerService = new Mock<IEmployerService>();

            employerService.Setup(x => x.GetById(It.IsAny<string>())).Returns(employer);

            return employerService;
        }

        private Mock<IEligibilityService> CaseEligibilityServiceStub(AbsenceSoft.Data.Cases.Case cAse, Employer employer)
        {
            var CaseEligibilityService = new Mock<IEligibilityService>();
            var PolicyList = EntityHelper.PolicyList();

            CaseEligibilityService.SetupGet
                (x => x.CurrentEmployer).Returns(employer);
            CaseEligibilityService
                .Setup(x => x.GetAvailableManualPolicies(cAse)).Returns(PolicyList);

            return CaseEligibilityService;
        }


        private Mock<IAdminUserService> AdminServiceStub(User user, AbsenceSoft.Data.Customers.Customer customers)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            adminService
                .Setup(x => x.GetCustomerById(It.IsAny<string>()))
                .Returns(customers);
 

            return adminService;
        }

    }
}
