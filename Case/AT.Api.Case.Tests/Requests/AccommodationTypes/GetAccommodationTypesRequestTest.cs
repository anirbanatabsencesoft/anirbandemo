﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Case.Requests.AccommodationTypes;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Net;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Case.Tests.Requests.AccommodationTypes
{
    [TestClass]
    public class GetAccommodationTypesRequestTest
    {
         

        [TestMethod]
        public void AccommodationTypesRequest_AccommodationTypes_Success()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            var customer = EntityHelper.Customer();
            var Employer = EntityHelper.Employer();
            var empployerService = EmployerServiceStub(Employer);
            var employeeService = EmployeeServiceStub(employee);
            var adminservice = AdminServiceStub(user, customer);
            var AccommodationTypeService = AccommodationTypeServiceStub();
            var request = new AccommodationTypesRequest(user.Id, Employer.Id, customer.Id,adminservice.Object, AccommodationTypeService.Object, empployerService.Object); 

            var parameter = new AccommodationTypesParameters
            {
                CaseType = CaseType.Consecutive,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
            }; 

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            //Assert.AreEqual(1, response.StatusCode);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }
        private Mock<IEmployeeService> EmployeeServiceStub(Customers.Employee employee)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.Is<string>(en => en == "EmployerId01"), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee).Verifiable();
            return employeeService;
        }
        private Mock<IEmployerService> EmployerServiceStub(Employer employer)
        {
            var employerService = new Mock<IEmployerService>();

            employerService.Setup(x => x.GetById(It.IsAny<string>())).Returns(employer);

            return employerService;
        }

        private Mock<IAdminUserService> AdminServiceStub(User user, Customers.Customer customers)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            adminService
                .Setup(x => x.GetCustomerById(It.IsAny<string>()))
                .Returns(customers);

            return adminService;
        }
        private Mock<IAccommodationService> AccommodationTypeServiceStub()
        {
            var accommodationService = new Mock<IAccommodationService>();

            var AbsenceReasonList = EntityHelper.AccommodationTypeList();

            accommodationService.Setup
                (x => x.GetAccommodationTypes(It.IsAny<string>(), It.IsAny<string>())).Returns(AbsenceReasonList);

            return accommodationService;
        }
    }
}
