﻿
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Requests.CaseEvents;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;

namespace AT.Api.Case.Tests.Requests.CaseEvents
{
    [TestClass]
    public class GetCaseEventsRequestTests
    {
        [TestMethod]
        public void GetCaseEvents_InvalidCaseNumber_ThrowsException()
        {

            //// Arrange
            var caseService = new Mock<ICaseService>();
            var request = new GetCaseEventsRequest(caseService.Object);

            var parameter = new GetCaseEventsParameters
            {
                CaseNumber = ""
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Case Number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetCaseEvents_CaseNotFound_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var caseNumber = EntityHelper.Case();
            var caseService = CaseServiceStub(null);
            var request = new GetCaseEventsRequest(caseService.Object);

            var parameter = new GetCaseEventsParameters
            {
                CaseNumber = "1390275272"

            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Case not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.InternalServerError, response.StatusCode);
        }

        [TestMethod]
        public void GetCaseEvents_CaseFound_ReturnsCaseEvents()
        {
            //// Arrange
            var Case = EntityHelper.Case();
            var caseService = CaseServiceStub(Case);
            var caseEntity = EntityHelper.Case();
            var CaseEvents = EntityHelper.CaseEvent();
            caseEntity.CaseEvents = new List<CaseEvent>(0) { EntityHelper.CaseEvent() };
            var request = new GetCaseEventsRequest(caseService.Object);

            var parameter = new GetCaseEventsParameters
            {
                CaseNumber = "1390275272"

            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
        }
        private Mock<ICaseService> CaseServiceStub(AbsenceSoft.Data.Cases.Case cAse)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);
            return caseService;
        }
    }
}

