﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Models.CaseEvents;
using AT.Api.Case.Requests.CaseEvents;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace AT.Api.Case.Tests.Requests.CaseEvents
{
    [TestClass]
    public class SaveCaseEventRequestTests
    {
        [TestMethod]
        public void CreateCaseEvents_InvalidCaseNumber_ThrowsException()
        {

            //// Arrange
            var caseService = new Mock<ICaseService>();
            var user = EntityHelper.UserStub();
            var adminservice = AdminServiceStub(user);
            var request = new SaveCaseEventRequest( caseService.Object);
            var parameter = new SaveCaseEventParameters
            {
                CaseNumber = "",
                CaseEvents = new List<SaveCaseEventModel>(0) { new SaveCaseEventModel
                {
                     EventType = CaseEventType.ReturnToWork,
                     EventDate= DateTime.Now,
                     Delete = false

                } }
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
               request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Case Number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateCaseEvents_InvalidCaseEventData_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServiceStub(null);
            var adminService = AdminServiceStub(user);
            List<SaveCaseEventModel> model = new List<SaveCaseEventModel>();
            model = null;
            var request = new SaveCaseEventRequest(caseService.Object);
            var parameter = new SaveCaseEventParameters
            {
                CaseNumber = "1390275272",
                CaseEvents = model
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
               request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Case not found", response.Message);
            Assert.AreEqual(HttpStatusCode.InternalServerError, response.StatusCode);
        }

        [TestMethod]
        public void CreateCaseEvents_CaseNotFound_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServiceStub(null);
            var adminService = AdminServiceStub(user);

            SaveCaseEventModel model = new SaveCaseEventModel();
            model.EventDate = DateTime.Parse("2018-10-02");
            model.EventType = CaseEventType.ReturnToWork;
            model.Delete = false;
            var request = new SaveCaseEventRequest(caseService.Object);
            var parameter = new SaveCaseEventParameters
            {
                CaseNumber = "1390275272",
                CaseEvents= new List<SaveCaseEventModel>(0) { new SaveCaseEventModel
                {
                     EventType = CaseEventType.ReturnToWork,
                     EventDate= DateTime.Now,
                     Delete = false

                } }
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert            
            Assert.AreEqual("Case not found", response.Message);
            Assert.AreEqual(HttpStatusCode.InternalServerError, response.StatusCode);
        }

        [TestMethod]
        public void CreateCaseEvents_CaseEventsList_ReturnsSuccessfullyCreatedMessages()
        {
            //// Arrange           
            var user = EntityHelper.UserStub();
            var adminService = AdminServiceStub(user);

            var caseentity = EntityHelper.Case();
            var caseService = CaseServiceStub(caseentity);
            var request = new SaveCaseEventRequest(caseService.Object);
            List<SaveCaseEventModel> createcaseevent = new List<SaveCaseEventModel>();
            SaveCaseEventModel model = new SaveCaseEventModel();
            model.EventDate = DateTime.Parse("2018-10-03");
            model.EventType = CaseEventType.ReturnToWork;
            model.Delete = false;
            createcaseevent.Add(model);

            var parameters = new SaveCaseEventParameters
            {
                CaseNumber = "1390275272",
                CaseEvents = createcaseevent
            };

            //// Act
            var response = request.Handle(parameters);

            //// Assert          
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual("True", response.Data);
            Assert.AreEqual("Case Events created successfully", response.Message);
        }
        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }
        private Mock<ICaseService> CaseServiceStub(AbsenceSoft.Data.Cases.Case cAse)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);
            caseService
               .Setup(x => x.RunCalcs(It.IsAny<AbsenceSoft.Data.Cases.Case>(), null))
               .Returns(cAse);
            caseService
                .Setup(x => x.UpdateCase(It.IsAny<AbsenceSoft.Data.Cases.Case>(), null))
                .Returns(cAse);
            return caseService;
        }

    }
}
