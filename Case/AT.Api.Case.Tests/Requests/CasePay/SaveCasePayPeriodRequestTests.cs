﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Security;
using AT.Api.Core.Exceptions;
using Moq;
using Customers = AbsenceSoft.Data.Customers;
using AT.Api.Case.Requests.CasePay;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AT.Api.Case.Models.CasePay;
using AbsenceSoft.Logic.Cases.Contracts;
using System.Net;
using AbsenceSoft.Logic.Customers.Contracts;
using AbsenceSoft.Logic.Pay.Contracts;
using Pay = AbsenceSoft.Logic.Pay;

namespace AT.Api.Case.Tests.Requests.CasePay
{
    [TestClass]
    public class SaveCasePayPeriodRequestTests
    {
        private Mock<ICaseService> CaseServieStub(AbsenceSoft.Data.Cases.Case cAse)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);

            caseService
                .Setup(x => x.CreateOrModifyCertification(It.IsAny<AbsenceSoft.Data.Cases.Case>(), It.IsAny<Certification>(), true))
                .Returns(cAse);

            return caseService;
        }

        private Mock<IEmployerService> EmployerServieStub(Customers.Employer employer)
        {
            var employerService = new Mock<IEmployerService>();

            employerService
                .Setup(x => x.GetById(It.IsAny<string>()))
                .Returns(employer);

            return employerService;
        }

        //private Mock<IEmployeeService> EmployeeServieStub(Customers.Employee employee)
        //{
        //    var employeeService = new Mock<IEmployeeService>();

        //    employeeService
        //        .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
        //        .Returns(employee);

        //    return employeeService;
        //}

        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }

        private Mock<IPayService> PayServiceStub(AbsenceSoft.Data.Cases.Case thecase, Pay.CasePayModel paymodel)
        {
            var payService = new Mock<IPayService>();

            payService
                .Setup(x => x.GetCasePayModel(It.IsAny<AbsenceSoft.Data.Cases.Case>()))
                .Returns(paymodel);

            payService
                .Setup(x => x.UpdatePayPeriodStatus(It.IsAny<AbsenceSoft.Data.Cases.Case>(), It.IsAny<Guid>(), It.IsAny<PayrollStatus>()))
                .Returns(thecase);

            return payService;
        }


        [TestMethod]
        public void SaveCasePayPeriod_InvalidCaseId_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var adminservice = AdminServiceStub(user);
            var caseService = CaseServieStub(null);
            var employerService = EmployerServieStub(null);
            var payService = PayServiceStub(EntityHelper.Case(), EntityHelper.CasePayModel());
            var request = new SaveCasePayPeriodRequest("UserKey", caseService.Object, employerService.Object, payService.Object, adminservice.Object);

            var parameter = new SaveCasePayPeriodParameters
            {
                CaseNumber = "",
                EmployerId = "2",
                PayPeriod = new CasePayPeriodModel { PayPeriodId = "627ca96a-37c8-492a-a141-ee7744dc6d36", Status = 0 }
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveCasePayPeriod_CaseNotFound_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(null); // null case
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var payService = PayServiceStub(EntityHelper.Case(), EntityHelper.CasePayModel());
            var request = new SaveCasePayPeriodRequest("UserKey", caseService.Object, employerService.Object, payService.Object, adminservice.Object);

            var parameter = new SaveCasePayPeriodParameters
            {
                CaseNumber = "772655213",
                EmployerId = "2",
                PayPeriod = new CasePayPeriodModel { PayPeriodId = "627ca96a-37c8-492a-a141-ee7744dc6d36", Status = 0 }
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.ExpectationFailed, response.StatusCode);
        }

        [TestMethod]
        public void SaveCasePayPeriod_EmployeeNotFound_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var cAse = EntityHelper.Case();
            cAse.Employee = null;
            var caseService = CaseServieStub(cAse); // null case
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var payService = PayServiceStub(EntityHelper.Case(), EntityHelper.CasePayModel());
            var request = new SaveCasePayPeriodRequest("UserKey", caseService.Object, employerService.Object, payService.Object, adminservice.Object);

            var parameter = new SaveCasePayPeriodParameters
            {
                CaseNumber = "772655213",
                EmployerId = "2",
                PayPeriod = new CasePayPeriodModel { PayPeriodId = "627ca96a-37c8-492a-a141-ee7744dc6d36", Status = 0 }
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            // Assert
            Assert.AreEqual("Employee not found", response.Message);
            Assert.AreEqual(HttpStatusCode.ExpectationFailed, response.StatusCode);
        }

        [TestMethod]
        public void SaveCasePayPeriod_InvalidEmployerId_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();

            var caseService = CaseServieStub(EntityHelper.Case()); // null case                  
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var payService = PayServiceStub(EntityHelper.Case(), EntityHelper.CasePayModel());
            var request = new SaveCasePayPeriodRequest("UserKey", caseService.Object, employerService.Object, payService.Object, adminservice.Object);

            var parameter = new SaveCasePayPeriodParameters
            {
                CaseNumber = "78676979",
                EmployerId = "",
                PayPeriod = new CasePayPeriodModel { PayPeriodId = "627ca96a-37c8-492a-a141-ee7744dc6d36", Status = 0 }
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            // Assert
            Assert.AreEqual("Missing EmployerId", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveCasePayPeriod_InvalidCasePayPeriodModel_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();

            var caseService = CaseServieStub(EntityHelper.Case()); // null case                  
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var payService = PayServiceStub(EntityHelper.Case(), EntityHelper.CasePayModel());
            var request = new SaveCasePayPeriodRequest("UserKey", caseService.Object, employerService.Object, payService.Object, adminservice.Object);

            var parameter = new SaveCasePayPeriodParameters
            {
                CaseNumber = "78676979",
                EmployerId = "000000020",
                PayPeriod = null
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case pay period was not provided", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveCasePayPeriod_InvalidCasePayPeriodId_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();

            var caseService = CaseServieStub(EntityHelper.Case()); // null case                  
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var payService = PayServiceStub(EntityHelper.Case(), EntityHelper.CasePayModel());
            var request = new SaveCasePayPeriodRequest("UserKey", caseService.Object, employerService.Object, payService.Object, adminservice.Object);

            var parameter = new SaveCasePayPeriodParameters
            {
                CaseNumber = "78676979",
                EmployerId = "000000020",
                PayPeriod = new CasePayPeriodModel { PayPeriodId = null, Status = 0 }
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case PayPeriodId was not provided", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveCasePayPeriod_InvalidCasePayRollStatus_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();

            var caseService = CaseServieStub(EntityHelper.Case()); // null case                  
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var payService = PayServiceStub(EntityHelper.Case(), EntityHelper.CasePayModel());
            var request = new SaveCasePayPeriodRequest("UserKey", caseService.Object, employerService.Object, payService.Object, adminservice.Object);

            var parameter = new SaveCasePayPeriodParameters
            {
                CaseNumber = "78676979",
                EmployerId = "000000020",
                PayPeriod = new CasePayPeriodModel { PayPeriodId = "627ca96a-37c8-492a-a141-ee7744dc6d36", Status = -1 }
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case payroll status was not provided", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveCasePayPeriod_UserNotFound_ThrowsException()
        {
            //// Arrange
            var caseService = CaseServieStub(EntityHelper.Case()); // null case                  
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var payService = PayServiceStub(EntityHelper.Case(), EntityHelper.CasePayModel());

            var adminService = new Mock<IAdminUserService>();
            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns((User)null);

            //// Act
            var request = Assert.ThrowsException<ApiException>(() =>
               new SaveCasePayPeriodRequest("UserKey", caseService.Object, employerService.Object, payService.Object, adminService.Object));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.InternalServerError, request.StatusCode);
            Assert.AreEqual("Unable to find user", request.Message);
        }

        [TestMethod]
        public void SaveCasePayPeriod_Restrictions_Success()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(EntityHelper.Case()); // null case
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var payService = PayServiceStub(EntityHelper.Case(), EntityHelper.CasePayModel());
            var request = new SaveCasePayPeriodRequest("UserKey", caseService.Object, employerService.Object, payService.Object, adminservice.Object);

            var parameter = new SaveCasePayPeriodParameters
            {
                CaseNumber = "78676979",
                EmployerId = "2",
                PayPeriod = new CasePayPeriodModel { PayPeriodId = "627ca96a-37c8-492a-a141-ee7744dc6d36", Status = 0 }
            };

            // Act
            var response = request.Handle(parameter);

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(0, response.Data.Status);
            Assert.AreEqual("627ca96a-37c8-492a-a141-ee7744dc6d36", response.Data.PayPeriodId);
            Assert.AreEqual(5000, response.Data.Amount);            
        }

    }
}
