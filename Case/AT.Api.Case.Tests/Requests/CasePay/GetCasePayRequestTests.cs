﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Security;
using AT.Api.Core.Exceptions;
using Moq;
using Customers = AbsenceSoft.Data.Customers;
using AT.Api.Case.Requests.CasePay;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases.Contracts;
using System.Net;
using AbsenceSoft.Logic.Customers.Contracts;
using AbsenceSoft.Logic.Pay.Contracts;
using Pay = AbsenceSoft.Logic.Pay;

namespace AT.Api.Case.Tests.Requests.CasePay
{
    [TestClass]
    public class GetCasePayRequestTests
    {
        private Mock<ICaseService> CaseServieStub(AbsenceSoft.Data.Cases.Case cAse)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);

            caseService
                .Setup(x => x.CreateOrModifyCertification(It.IsAny<AbsenceSoft.Data.Cases.Case>(), It.IsAny<Certification>(), true))
                .Returns(cAse);

            return caseService;
        }

        private Mock<IEmployerService> EmployerServieStub(Customers.Employer employer)
        {
            var employerService = new Mock<IEmployerService>();

            employerService
                .Setup(x => x.GetById(It.IsAny<string>()))
                .Returns(employer);

            return employerService;
        }

        //private Mock<IEmployeeService> EmployeeServieStub(Customers.Employee employee)
        //{
        //    var employeeService = new Mock<IEmployeeService>();

        //    employeeService
        //        .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
        //        .Returns(employee);

        //    return employeeService;
        //}

        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }

        private Mock<IPayService> PayServiceStub(AbsenceSoft.Data.Cases.Case thecase, Pay.CasePayModel paymodel)
        {
            var payService = new Mock<IPayService>();

            payService
                .Setup(x => x.GetCasePayModel(It.IsAny<AbsenceSoft.Data.Cases.Case>()))
                .Returns(paymodel);

            payService
                .Setup(x => x.UpdatePayPeriodStatus(It.IsAny<AbsenceSoft.Data.Cases.Case>(), It.IsAny<Guid>(), It.IsAny<PayrollStatus>()))
                .Returns(thecase);

            return payService;
        }

        [TestMethod]
        public void GetCasePay_InvalidCaseId_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var adminservice = AdminServiceStub(user);
            var caseService = CaseServieStub(null);
            var employerService = EmployerServieStub(null);
            var payService = PayServiceStub(EntityHelper.Case(), EntityHelper.CasePayModel());
            var request = new GetCasePayRequest("UserKey", caseService.Object, employerService.Object, payService.Object, adminservice.Object);

            var parameter = new GetCasePayParameters
            {
                CaseNumber = "",
                EmployerId = "2"
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetCasePay_CaseNotFound_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(null); // null case
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var payService = PayServiceStub(EntityHelper.Case(), EntityHelper.CasePayModel());
            var request = new GetCasePayRequest("UserKey", caseService.Object, employerService.Object, payService.Object, adminservice.Object);

            var parameter = new GetCasePayParameters
            {
                CaseNumber = "772655213",
                EmployerId = "2"
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.ExpectationFailed, response.StatusCode);
        }

        [TestMethod]
        public void GetCasePay_EmployeeNotFound_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var cAse = EntityHelper.Case();
            cAse.Employee = null;
            var caseService = CaseServieStub(cAse); // null case
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var payService = PayServiceStub(EntityHelper.Case(), EntityHelper.CasePayModel());
            var request = new GetCasePayRequest("UserKey", caseService.Object, employerService.Object, payService.Object, adminservice.Object);

            var parameter = new GetCasePayParameters
            {
                CaseNumber = "772655213",
                EmployerId = "2"
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Employee not found", response.Message);
            Assert.AreEqual(HttpStatusCode.ExpectationFailed, response.StatusCode);
        }

        [TestMethod]
        public void GetCasePay_InvalidEmployerId_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();

            var caseService = CaseServieStub(EntityHelper.Case()); // null case                  
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var payService = PayServiceStub(EntityHelper.Case(), EntityHelper.CasePayModel());
            var request = new GetCasePayRequest("UserKey", caseService.Object, employerService.Object, payService.Object, adminservice.Object);

            var parameter = new GetCasePayParameters
            {
                CaseNumber = "78676979",
                EmployerId = ""
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Missing EmployerId", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetCasePay_UserNotFound_ThrowsException()
        {
            //// Arrange
            var caseService = CaseServieStub(EntityHelper.Case()); // null case                  
            var employerService = EmployerServieStub(EntityHelper.Employer());            
            var payService = PayServiceStub(EntityHelper.Case(), EntityHelper.CasePayModel());
            
            var adminService = new Mock<IAdminUserService>();
            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns((User)null);

            //// Act
            var request = Assert.ThrowsException<ApiException>(() =>
                new GetCasePayRequest("UserKey", caseService.Object, employerService.Object, payService.Object, adminService.Object));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.InternalServerError, request.StatusCode);
            Assert.AreEqual("Unable to find user", request.Message);
        }

        [TestMethod]
        public void GetCasePay_ValidRequest_Success()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(EntityHelper.Case()); // null case
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var payService = PayServiceStub(EntityHelper.Case(), EntityHelper.CasePayModel());
            var request = new GetCasePayRequest("UserKey", caseService.Object, employerService.Object, payService.Object, adminservice.Object);

            var parameter = new GetCasePayParameters
            {
                CaseNumber = "78676979",
                EmployerId = "2"
            };

            // Act
            var response = request.Handle(parameter);

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("PayScheduleName", response.Data.PayScheduleName);
            Assert.AreEqual("0000000010", response.Data.PayScheduleId);
            Assert.AreEqual(true, response.Data.ApplyOffsetsByDefault);            
        }
    }
}
