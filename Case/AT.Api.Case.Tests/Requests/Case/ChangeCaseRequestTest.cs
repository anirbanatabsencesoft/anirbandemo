﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Requests;
using AT.Api.Case.Requests.ChangeCase;
using AT.Api.Case.Requests.ReadCase;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace AT.Api.Case.Tests.Requests.Case
{
    [TestClass]
    public class ChangeCaseRequestTest
    {

        [TestMethod]
        public void ChangeCaseRequest_CaseNumberEmpty_ThrowsException()
        {
            
            var caseNumber = "";
            var newCase = EntityHelper.Case();
            var caseService = CaseCaseServiceStub(newCase);
            var ChangeCase = EntityHelper.ChangeCase();

            var request = new ChangeCaseRequest(
                caseService.Object);


            var parameter = new ChangeCaseParameters
            {
                CaseNumber=caseNumber,
                ChangeCase= ChangeCase
            };

            //// Act
            var response =  Assert.ThrowsException<ApiException>(() =>request.Handle(parameter));

            //// Assert
            Assert.AreEqual("CaseNumber is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void ChangeCaseRequest_CaseNumberNotFound_ThrowsException()
        {

            var caseNumber = "35";
            var newCase = EntityHelper.Case();
            var ChangeCase = EntityHelper.ChangeCase();
            var caseService = CaseCaseServiceStub(newCase);

            var request = new ChangeCaseRequest(
                caseService.Object);


            var parameter = new ChangeCaseParameters
            {
                CaseNumber = caseNumber,
                ChangeCase = ChangeCase
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Case not found for the given case number", response.Message);
            Assert.AreEqual(HttpStatusCode.ExpectationFailed, response.StatusCode);
        }



        [TestMethod]
        public async Task ChangeCaseRequest_Case_Success()
        {

            var caseNumber = "039";
            var newCase = EntityHelper.Case();
            var ChangeCase = EntityHelper.ChangeCase();
            var caseService = CaseCaseServiceStub(newCase);

            var request = new ChangeCaseRequest(
                caseService.Object);


            var parameter = new ChangeCaseParameters
            {
                CaseNumber = caseNumber,
                ChangeCase = ChangeCase
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.AreEqual("Case Changed successfully", response.Message);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        private Mock<ICaseService> CaseCaseServiceStub(AbsenceSoft.Data.Cases.Case cAse)
        {
            var caseService = new Mock<ICaseService>();

            caseService
            .Setup(x => x.GetCaseByCaseNumber(
                  It.Is<string>(en => en == "039")))
                  .Returns(cAse)
                  .Verifiable();
             
            caseService
             .Setup(x => x.CreateCase(It.IsAny<CaseStatus>(), It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<CaseType>(), It.IsAny<string>(), It.IsAny<Schedule>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));

            caseService
              .Setup(x => x.UpdateCase(It.IsAny<AbsenceSoft.Data.Cases.Case>(), null))
              .Returns(cAse);

            caseService
                .Setup(x => x.ChangeCase(It.IsAny<AbsenceSoft.Data.Cases.Case>(),It.IsAny<DateTime>(), It.IsAny<DateTime>(),It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<CaseType>(),It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<Schedule>(), It.IsAny<List<AbsenceSoft.Data.Customers.VariableScheduleTime>>()))
                .Returns(cAse);

            return caseService;
        }

    }
}
