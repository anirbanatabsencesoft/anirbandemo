﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Case.Models;
using AT.Api.Case.Requests.Case;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using Customers = AbsenceSoft.Data.Customers;
using cases = AbsenceSoft.Data.Cases;
using AbsenceSoft.Administration.Logic.Contracts;

namespace AT.Api.Case.Tests.Requests.Case
{
    [TestClass]
    public class CreateCaseRequestTests
    {
        [TestMethod]
        public void CreateCaseRequest_EmptyEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            var customer = EntityHelper.Customer();
            var Employer = EntityHelper.Employer();
            var newCase = EntityHelper.Case(info: EntityHelper.DisabilityInfo());
            var employeeService = EmployeeServiceStub(employee);
            var adminservice = AdminServiceStub(user);
            var caseService = CaseCaseServiceStub(newCase);
            var Employerservice = EmployerServiceStub(Employer);
            var LeaveOfAbsence = EntityHelper.LeaveOfAbsence();
            var eligibilityService = CaseEligibilityServiceStub(newCase, LeaveOfAbsence);

            var request = new CreateCaseRequest(user.Id, Employer.Id, customer.Id, employeeService.Object, caseService.Object,adminservice.Object);
                

            var parameter = new CreateCaseParameter
            {
                CaseModel =new CaseModel { EmployeeNumber=""},
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",               
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }
        
        [TestMethod]
        public void CreateCaseRequest_EmployeeNotFound_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            var customer = EntityHelper.Customer();
            var Employer = EntityHelper.Employer();
            var CaseModel = EntityHelper.CaseMockModel();
            var newCase = EntityHelper.Case(info: EntityHelper.DisabilityInfo());
            var employeeService = EmployeeServiceStub(employee);
            var adminservice = AdminServiceStub(user);
            var caseService = CaseCaseServiceStub(newCase);
            var Employerservice = EmployerServiceStub(Employer);
            var LeaveOfAbsence = EntityHelper.LeaveOfAbsence();
            var eligibilityService = CaseEligibilityServiceStub(newCase, LeaveOfAbsence);
            CaseModel.EmployeeNumber = "EmployeeNumber01";

            var request = new CreateCaseRequest(user.Id, Employer.Id, customer.Id, employeeService.Object, caseService.Object, adminservice.Object);

            var parameter = new CreateCaseParameter
            {
                CaseModel = CaseModel,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee not found for the given employee number", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateCase_CaseNumberFound_Exception()
        {
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            var newCase = EntityHelper.Case();
            var CaseModel = EntityHelper.CaseMockModel();
            CaseModel.CaseNumber = "772655213";
            var customer = EntityHelper.Customer();
            var Employer = EntityHelper.Employer();
            var employeeService = EmployeeServiceStub(employee);
            var caseService = CaseCaseServiceNewStub(newCase);
            var LeaveOfAbsence = EntityHelper.LeaveOfAbsence();
            var eligibilityService = CaseEligibilityServiceStub(newCase, LeaveOfAbsence);
            var Employerservice = EmployerServiceStub(Employer);
            var adminservice = AdminServiceStub(user);
            var request = new CreateCaseRequest(user.Id, Employer.Id, customer.Id, employeeService.Object, caseService.Object, adminservice.Object);


            var parameter = new CreateCaseParameter
            {
                CaseModel = CaseModel,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));
            //// Assert
            Assert.AreEqual("Case with the given case number already exists", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);

        }

        [TestMethod]
        public void CreateCaseRequest_CaseReporterFirstNameNotFound_ThrowsException()
        {
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            var customer = EntityHelper.Customer();
            var Employer = EntityHelper.Employer();
            var CaseModel = EntityHelper.CaseMockModel();
            CaseModel.CaseReporter = EntityHelper.caseReporters(FirstName: null);
            var employeeService = EmployeeServiceStub(employee);
            var newCase = EntityHelper.Case(info: new cases.DisabilityInfo());
            var caseService = CaseCaseServiceNewStub(newCase);
            var adminservice = AdminServiceStub(user);            
            var request = new CreateCaseRequest(user.Id, Employer.Id, customer.Id, employeeService.Object, caseService.Object, adminservice.Object);


            var parameter = new CreateCaseParameter
            {
                CaseModel = CaseModel,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));
            //// Assert
            Assert.AreEqual("Case-reporter First Name is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateCaseRequest_CaseReporterLastNameNotFound_ThrowsException()
        {
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            var customer = EntityHelper.Customer();
            var Employer = EntityHelper.Employer();
            var CaseModel = EntityHelper.CaseMockModel();
            CaseModel.CaseReporter = EntityHelper.caseReporters(LastName: null);
            var employeeService = EmployeeServiceStub(employee);
            var newCase = EntityHelper.Case(info: new cases.DisabilityInfo());
            var caseService = CaseCaseServiceNewStub(newCase);
            var adminservice = AdminServiceStub(user);
            var request = new CreateCaseRequest(user.Id, Employer.Id, customer.Id, employeeService.Object, caseService.Object, adminservice.Object);


            var parameter = new CreateCaseParameter
            {
                CaseModel = CaseModel,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));
            //// Assert
            Assert.AreEqual("Case-reporter Last Name is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateCaseRequest_AdministatorEndDateOptional_ThrowsException()
        {
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            var customer = EntityHelper.Customer();
            var Employer = EntityHelper.Employer();
            var CaseModel = EntityHelper.CaseMockModel(endDate:null);
            CaseModel.CaseReporter = EntityHelper.caseReporters();
            var employeeService = EmployeeServiceStub(employee);
            var newCase = EntityHelper.Case(info: new cases.DisabilityInfo());
            var caseService = CaseCaseServiceNewStub(newCase);
            var adminservice = AdminServiceStub(user);
            var request = new CreateCaseRequest(user.Id, Employer.Id, customer.Id, employeeService.Object, caseService.Object, adminservice.Object);


            var parameter = new CreateCaseParameter
            {
                CaseModel = CaseModel,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));
            //// Assert
            Assert.AreEqual("EndDate is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }



        [TestMethod]
        public void SaveCreateCase_Case_Success()
        {
            // Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            var customer = EntityHelper.Customer();
            var Employer = EntityHelper.Employer();
            var CaseModel = EntityHelper.CaseMockModel();
            var CaseReporter = EntityHelper.caseReporters();
            var CaseDates = EntityHelper.caseDates();
            var CaseFlag = EntityHelper.CaseFlag();
            var caseRelationships = EntityHelper.CaseRelationship();
            var newCase = EntityHelper.Case(info: new cases.DisabilityInfo());
            var employeeService = EmployeeServiceStub(employee);
            var adminservice = AdminServiceStub(user);
            var caseService = CaseCaseServiceStub(newCase);
            var Employerservice = EmployerServiceStub(Employer);
            var LeaveOfAbsence = EntityHelper.LeaveOfAbsence(newCase);
            var eligibilityService = CaseEligibilityServiceStub(newCase, LeaveOfAbsence);


            var request = new CreateCaseRequest(user.Id, Employer.Id, customer.Id, employeeService.Object, caseService.Object, adminservice.Object);

            var parameter = new CreateCaseParameter
            {
                CaseModel = new CaseModel()
                {
                    CaseNumber = "037",
                    EmployeeNumber = "000000304",
                    CaseType = CaseType.None,
                    ReasonCode = "ECH",
                    ShortDescription = "SHORT DESC",
                    Summary = "LONG DESC",
                    StartDate=DateTime.Now.AddMonths(1),
                    EndDate=DateTime.Now.AddMonths(2),
                    CaseReporter = CaseReporter,
                    CaseDates = CaseDates,
                    CaseRelationship = caseRelationships,
                    CaseFlags = CaseFlag
                },
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
            };

            // Act
            var response = request.Handle(parameter);

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("037", response.Data);
            Assert.AreEqual("Case created successfully", response.Message);
        }


        private Mock<IEmployeeService> EmployeeServiceStub(Customers.Employee employee)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.Is<string>(en => en== "000000304"), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee).Verifiable();
            return employeeService;
        }
        private Mock<IEmployerService> EmployerServiceStub(Customers.Employer employer)
        {
            var employerService = new Mock<IEmployerService>();

            employerService.Setup(x => x.GetById(It.IsAny<string>())).Returns(employer);

            return employerService;
        }
        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
           .Setup(x => x.GetUserById(It.Is<string>(userid => userid == "UserId01")))
           .Returns(user).Verifiable();

            return adminService;
        }

        private Mock<ICaseService> CaseCaseServiceNewStub(cases.Case cases)
        {
            var caseService = new Mock<ICaseService>();

            caseService.
                Setup(x => x.GetCaseByCaseNumber(It.Is<string>(en => en == "772655213")))
                .Returns(cases).Verifiable();
            
            return caseService;

        }
            private Mock<ICaseService> CaseCaseServiceStub(cases.Case cases)
        {
            var caseService = new Mock<ICaseService>();

              caseService
              .Setup(x => x.GetCaseByCaseNumber(
                    It.Is<string>(en => en == "039")))
                    .Returns(cases)
                    .Verifiable();
 
            caseService
             .Setup(x => x.CreateCase(It.IsAny<CaseStatus>(), It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<CaseType>(), It.IsAny<string>(), It.IsAny<Schedule>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
             .Returns(cases);

            caseService
              .Setup(x => x.UpdateCase(It.IsAny<cases.Case>(), null))
              .Returns(cases);


            return caseService;
        }
        private Mock<ICaseService> CaseServieStub(cases.Case cAse, List<PolicySummary> ps = null, List<ValidationMessage> pv = null, CaseNote note = null)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);

            caseService
                .Setup(x => x.GetEmployeePolicySummaryByCaseId(It.IsAny<string>(), null))
                .Returns(ps);

            caseService
                .Setup(x => x.PreValidateTimeOffRequest(It.IsAny<string>(), It.IsAny<List<IntermittentTimeRequest>>()))
                .Returns(pv);

            caseService
                .Setup(x => x.UpdateCase(It.IsAny<AbsenceSoft.Data.Cases.Case>(), null))
                .Returns(cAse);

            caseService
                .Setup(x => x.CreateOrModifyTimeOffRequest(It.IsAny<AbsenceSoft.Data.Cases.Case>(), It.IsAny<List<IntermittentTimeRequest>>()))
                .Returns(cAse);

            caseService
                .Setup(x => x.IntermittentAbsenceEntryNote(It.IsAny<AbsenceSoft.Data.Cases.Case>(), It.IsAny<AbsenceSoft.Data.Cases.IntermittentTimeRequest>(), It.IsAny<User>()))
                .Returns(note);

            return caseService;
        }
        private Mock<IEligibilityService> CaseEligibilityServiceStub(cases.Case cases, LeaveOfAbsence LeaveOfAbsence)
        {
            var CaseEligibilityService = new Mock<IEligibilityService>();
            var PolicyList = EntityHelper.PolicyList();
            var AppliedPolicy = EntityHelper.AppliedPolicy();
            CaseEligibilityService.
                Setup(x => x.RunEligibility(cases)).Returns(LeaveOfAbsence);
            CaseEligibilityService
              .Setup(x => x.GetAvailableManualPolicies(cases)).Returns(PolicyList);

            CaseEligibilityService.Setup(x => x.GetWhySelected(cases, AppliedPolicy)).Returns("");
            return CaseEligibilityService;

        }

    }
}
