﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Requests.CloseCase;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Net;
using System.Threading.Tasks;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Tests.Requests.Case
{
    [TestClass]
    public class CloseCaseRequestTests
    {
        [TestMethod]
        public void CloseCaseRequest_NullCaseNumber_ThrowsException()
        {
            //// Arrange
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns((AbsenceSoft.Data.Cases.Case)null);

            var closeCaseRequest = new CloseCaseRequest(caseService.Object);

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => closeCaseRequest.Handle(new CloseCaseParameters
            {
                CaseNumber = null,
                ClosureReason = AbsenceSoft.Data.Enums.CaseClosureReason.Terminated,
                Description = ""
            }));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Case number is required", response.Message);
        }

        [TestMethod]
        public void CloseCaseRequest_CaseNotFound_ThrowsException()
        {
            //// Arrange
            var caseService = new Mock<ICaseService>();
            var caseNumber = "123";

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns((AbsenceSoft.Data.Cases.Case)null);

            var closeCaseRequest = new CloseCaseRequest(caseService.Object);

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => closeCaseRequest.Handle(new CloseCaseParameters
            {
                CaseNumber = caseNumber,
                ClosureReason = AbsenceSoft.Data.Enums.CaseClosureReason.Terminated,
                Description = ""
            }));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual($"Case was not found for 123", response.Message);
        }

        

        [TestMethod]
        public void CloseCaseRequest_ValidCase_CaseClosed()
        {
            //// Arrange
            var caseNumber = "123456";
            var theCase = EntityHelper.Case(casenumber: caseNumber);

            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.Is<string>(c => c == caseNumber)))
                .Returns((Cases.Case)theCase)
                .Verifiable();
            caseService.Setup(x => x.CaseClosed(It.IsAny<Cases.Case>(), It.IsAny<DateTime>(), It.IsAny<CaseClosureReason>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())); 

            var closeCaseRequest = new CloseCaseRequest(caseService.Object);

            //// Act
            var response = closeCaseRequest.Handle(new CloseCaseParameters
            {
                CaseNumber = caseNumber,
                ClosureReason = CaseClosureReason.Terminated,
                Description = "test description"
            });

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual("Case closed successfully", response.Message);            
        }

    }
}
