﻿using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Requests.CancelCase;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft.Data.Enums;
using Moq;
using AT.Api.Core.Exceptions;
using System.Threading.Tasks;
using System.Net;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Tests.Requests.Case
{
    [TestClass]
    public class CancelCaseRequestTests
    {
        [TestMethod]
        public void CancelCaseRequest_NullCaseNumber_ThrowsException()
        {
            //// Arrange
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns((AbsenceSoft.Data.Cases.Case)null);

            var cancelCaseRequest = new CancelCaseRequest(caseService.Object);

            var parameters = new CancelCaseParameters
            {
                CaseNumber = null,
                Reason = CaseCancelReason.InquiryClosed
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => cancelCaseRequest.Handle(parameters));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Case number is required", response.Message);
        }

        [TestMethod]
        public void CancelCaseRequest_CaseNotFound_ThrowsException()
        {
            //// Arrange
            var caseService = new Mock<ICaseService>();
            var caseNumber = "123";

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns((AbsenceSoft.Data.Cases.Case)null);

            var cancelCaseRequest = new CancelCaseRequest(caseService.Object);

            var parameters = new CancelCaseParameters
            {
                CaseNumber = caseNumber,
                Reason = CaseCancelReason.InquiryClosed
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => cancelCaseRequest.Handle(parameters));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual($"Case was not found for 123", response.Message);
        }

        [TestMethod]
        public void CancelCaseRequest_ValidCase_CaseCancelled()
        {
            //// Arrange
            var caseNumber = "123456";
            var cancelReason = CaseCancelReason.LeaveNotNeededOrCancelled;
            var theCase = EntityHelper.Case(casenumber: caseNumber);

            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.Is<string>(c => c == caseNumber)))
                .Returns((Cases.Case)theCase)
                .Verifiable();

            caseService
                .Setup(x => x.CancelCase(
                    It.Is<string>(c => c == theCase.Id), 
                    It.Is<CaseCancelReason>(cr => cr == cancelReason)))
                .Returns((Cases.Case)theCase)
                .Verifiable();

            caseService
                .Setup(x => x.UpdateCase(
                    It.Is<Cases.Case>(
                    c => c.Id == theCase.Id &&
                    c.ReleatedCaseId == theCase.ReleatedCaseId &&
                    c.CaseNumber == caseNumber),
                    null))
                .Verifiable();

            var cancelCaseRequest = new CancelCaseRequest(caseService.Object);

            var parameters = new CancelCaseParameters
            {
                CaseNumber = caseNumber,
                Reason = cancelReason
            };

            //// Act
            var response = cancelCaseRequest.Handle(parameters);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual("Case canceled successfully", response.Message);
            caseService.Verify();
        }

    }
}
