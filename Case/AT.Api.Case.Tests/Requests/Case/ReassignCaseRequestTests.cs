﻿using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration.Contracts;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Tasks.Contracts;
using AT.Api.Case.Requests.ReassignCase;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Net;
using System.Threading.Tasks;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Tests.Requests.Case
{
    [TestClass]
    public class ReassignCaseRequestTests
    {
        [TestMethod]
        public void ReassignCaseRequest_AssigneeUserNotFound_ThrowsException()
        {
            //// Arrange

            var administrationService = new Mock<IAdministrationService>();

            administrationService
                .Setup(x => x.GetUserByEmail(It.IsAny<string>()))
                .Returns((User)null);

            var reassignCaseRequest = new ReassignCaseRequest(
                administrationService.Object);
            
            var parameters = new ReassignCaseParameters
            {
                AssigneeUserEmail = "useremail@email.com",
                AssigneeTypeCode = null,
                CaseNumber = "Case01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => reassignCaseRequest.Handle(parameters));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("User was not found or no valid email was supplied", response.Message);
        }

        [TestMethod]
        public void ReassignCaseRequest_NullAssigneeUserEmail_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var administrationService = this.AdministrationServiceStub(user);

            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns((AbsenceSoft.Data.Cases.Case)null);

            var reassignCaseRequest = new ReassignCaseRequest(
                administrationService.Object,
                caseService.Object);

            var parameters = new ReassignCaseParameters
            {
                AssigneeUserEmail = null,
                AssigneeTypeCode = "123",
                CaseNumber = "123465464654646546546546"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => reassignCaseRequest.Handle(parameters));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Assignee user email is required", response.Message);
        }

        [TestMethod]
        public void ReassignCaseRequest_NullCaseNumber_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var administrationService = this.AdministrationServiceStub(user);

            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns((AbsenceSoft.Data.Cases.Case)null);

            var reassignCaseRequest = new ReassignCaseRequest(
                administrationService.Object,
                caseService.Object);

            var parameters = new ReassignCaseParameters
            {
                AssigneeUserEmail = "useremail@email.com",
                AssigneeTypeCode = null,
                CaseNumber = null
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => reassignCaseRequest.Handle(parameters));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Case number is required", response.Message);
        }

        [TestMethod]
        public void ReassignCaseRequest_CaseNotFound_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var administrationService = this.AdministrationServiceStub(user);

            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns((AbsenceSoft.Data.Cases.Case)null);
            
            var reassignCaseRequest = new ReassignCaseRequest(
                administrationService.Object, 
                caseService.Object);

            var parameters = new ReassignCaseParameters
            {
                AssigneeUserEmail = "useremail@email.com",
                AssigneeTypeCode = null,
                CaseNumber = "123465464654646546546546"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => reassignCaseRequest.Handle(parameters));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual($"Case was not found for {parameters.CaseNumber}", response.Message);
        }

        [TestMethod]
        public void ReassignCaseRequest_ValidAssigneeUserAndCase_CaseAssignedToUser()
        {
            //// Arrange
            var caseNumber = "123456";
            var user = EntityHelper.UserStub();
            var theCase = EntityHelper.Case(casenumber: caseNumber);

            var administrationService = this.AdministrationServiceStub(user);

            var caseService = new Mock<ICaseService>();
            var toDoService = new Mock<IToDoService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.Is<string>(c => c == caseNumber)))
                .Returns((Cases.Case)theCase)
                .Verifiable();

             

            var reassignCaseRequest = new ReassignCaseRequest(
                administrationService.Object,
                caseService.Object,
                toDoService.Object);

            var parameters = new ReassignCaseParameters
            {
                AssigneeUserEmail = "useremail@email.com",
                AssigneeTypeCode = null,
                CaseNumber = caseNumber
            };

            //// Act
            var response = reassignCaseRequest.Handle(parameters);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual("Case assigned to user successfully", response.Message);
            
        }

        private Mock<IAdministrationService> AdministrationServiceStub(User user)
        {
            var administrationService = new Mock<IAdministrationService>();

            administrationService
                .Setup(x => x.GetUserByEmail(It.IsAny<string>()))
                .Returns(user);

            return administrationService;
        }
        
    }
}
