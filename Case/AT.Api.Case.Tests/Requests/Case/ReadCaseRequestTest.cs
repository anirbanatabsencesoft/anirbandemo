﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Requests.ReadCase;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using cases = AbsenceSoft.Data.Cases;
namespace AT.Api.Case.Tests.Requests.Case
{
    [TestClass]
    public class ReadCaseRequestTest
    {

        [TestMethod]
        public void ReadCaseRequest_CaseNumberEmpty_ThrowsException()
        {

            var caseNumber = "";
            var newCase = EntityHelper.Case();
            var user = EntityHelper.UserStub();
            var caseService = CaseCaseServiceStub(newCase);
            var adminService = AdminUserServiceStub(user);

            var request = new ReadCaseRequest(
                "UserId01", 
                "EmployerId01", 
                "CustomerId01",
                caseService.Object, 
                adminService.Object);

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(caseNumber));

            //// Assert
            Assert.AreEqual("CaseNumber is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void ReadCaseRequest_CaseNumberNotFound_ThrowsException()
        {

            var caseNumber = "35";
            var newCase = EntityHelper.Case();
            var user = EntityHelper.UserStub();
            var caseService = CaseCaseServiceStub(newCase);
            var adminService = AdminUserServiceStub(user);

            var request = new ReadCaseRequest(
                "UserId01",
                "EmployerId01",
                "CustomerId01",
                caseService.Object,
                adminService.Object);

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(caseNumber));

            //// Assert
            Assert.AreEqual("Case not found for the given case number", response.Message);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }


        [TestMethod]

        public void ReadCaseRequest_Case_succeess()
        {
            var caseNumber = "039";
            var newCase = EntityHelper.Case(segments: new List<cases.CaseSegment>());
            newCase.CaseReporter = EntityHelper.caseReportersAbsenceCase();
            var CaseDates = EntityHelper.caseDates();
            var CaseModel = EntityHelper.CaseMockModel();
            var CaseFlag = EntityHelper.CaseFlag();
            newCase.Contact = EntityHelper.caseConatctAbsenceCase();
            var user = EntityHelper.UserStub();

            var caseService = CaseCaseServiceStub(newCase);
            var adminService = AdminUserServiceStub(user);

            var request = new ReadCaseRequest(
                "UserId01",
                "EmployerId01",
                "CustomerId01",
                caseService.Object,
                adminService.Object);

            //// Act
            var response = request.Handle(caseNumber);

            //// Assert
            Assert.AreEqual("Case retrieved  successfully", response.Message);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }
        
        private Mock<ICaseService> CaseCaseServiceStub(cases.Case caseData)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.Is<string>(en => en == "039")))
                .Returns(caseData);

            caseService
                .Setup(
                    x => x.CreateCase(
                        It.IsAny<CaseStatus>(), 
                        It.IsAny<string>(), 
                        It.IsAny<DateTime>(), 
                        It.IsAny<DateTime>(), 
                        It.IsAny<CaseType>(), 
                        It.IsAny<string>(), 
                        It.IsAny<Schedule>(), 
                        It.IsAny<string>(), 
                        It.IsAny<string>(), 
                        It.IsAny<string>()));

            caseService
              .Setup(x => x.UpdateCase(It.IsAny<cases.Case>(), null))
              .Returns(caseData);


            return caseService;
        }

        private Mock<IAdminUserService> AdminUserServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.Is<string>(en => en == "UserId01")))
                .Returns(user);

            return adminService;
        }

    }
}
