﻿using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Requests.RecalculateCase;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Net;
using System.Threading.Tasks;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Tests.Requests.Case
{
    [TestClass]
    public class RecalculateCaseRequestTests
    {
        [TestMethod]
        public void RecalculateCaseRequest_NullCaseNumber_ThrowsException()
        {
            //// Arrange
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns((AbsenceSoft.Data.Cases.Case)null);

            var recalculateCaseRequest = new RecalculateCaseRequest(caseService.Object);

            var parameters = new RecalculateCaseParameters
            {
                CaseNumber = null,
                SaveCase = false
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => recalculateCaseRequest.Handle(parameters));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Case number is required", response.Message);
        }

        [TestMethod]
        public void RecalculateCaseRequest_CaseNotFound_ThrowsException()
        {
            //// Arrange
            var caseService = new Mock<ICaseService>();
            var caseNumber = "123";

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns((AbsenceSoft.Data.Cases.Case)null);

            var recalculateCaseRequest = new RecalculateCaseRequest(caseService.Object);

            var parameters = new RecalculateCaseParameters
            {
                CaseNumber = caseNumber,
                SaveCase = false
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => recalculateCaseRequest.Handle(parameters));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual($"Case was not found for 123", response.Message);
        }

        [TestMethod]
        public void RecalculateCaseRequest_ValidCaseWithSaveAsFalse_CaseRecalculated()
        {
            //// Arrange
            var caseNumber = "123456";
            var theCase = EntityHelper.Case(casenumber: caseNumber);

            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.Is<string>(c => c == caseNumber)))
                .Returns((Cases.Case)theCase)
                .Verifiable();

            caseService
                .Setup(x => x.RunCalcs(
                    It.Is<Cases.Case>(
                        c => c.Id == theCase.Id &&
                        c.ReleatedCaseId == theCase.ReleatedCaseId &&
                        c.CaseNumber == caseNumber),
                    null))
                .Returns((Cases.Case)theCase)
                .Verifiable();

            var recalculateCaseRequest = new RecalculateCaseRequest(caseService.Object);

            var parameters = new RecalculateCaseParameters
            {
                CaseNumber = caseNumber,
                SaveCase = false
            };

            //// Act
            var response = recalculateCaseRequest.Handle(parameters);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual("Case recalculated successfully", response.Message);
            caseService.Verify();
        }

        [TestMethod]
        public void RecalculateCaseRequest_ValidCaseWithSaveAsTrue_CaseRecalculatedAndSaved()
        {
            //// Arrange
            var caseNumber = "123456";
            var theCase = EntityHelper.Case(casenumber: caseNumber);

            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.Is<string>(c => c == caseNumber)))
                .Returns((Cases.Case)theCase)
                .Verifiable();

            caseService
                .Setup(x => x.RunCalcs(
                    It.Is<Cases.Case>(
                        c => c.Id == theCase.Id &&
                        c.ReleatedCaseId == theCase.ReleatedCaseId &&
                        c.CaseNumber == caseNumber),
                    null))
                .Returns((Cases.Case)theCase)
                .Verifiable();

            caseService
                .Setup(x => x.UpdateCase(
                    It.Is<Cases.Case>(
                        c => c.Id == theCase.Id &&
                        c.ReleatedCaseId == theCase.ReleatedCaseId &&
                        c.CaseNumber == theCase.CaseNumber),
                    null))
                .Verifiable();

            var recalculateCaseRequest = new RecalculateCaseRequest(caseService.Object);

            var parameters = new RecalculateCaseParameters
            {
                CaseNumber = caseNumber,
                SaveCase = true
            };

            //// Act
            var response = recalculateCaseRequest.Handle(parameters);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual("Case recalculated successfully", response.Message);
            caseService.Verify();
        }

    }
}
