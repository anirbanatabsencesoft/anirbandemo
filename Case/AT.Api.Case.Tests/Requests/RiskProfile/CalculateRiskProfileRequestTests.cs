﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.RiskProfiles;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers.Contracts;
using AbsenceSoft.Logic.RiskProfiles.Contracts;
using AT.Api.Case.Requests.RiskProfile;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Net;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Case.Tests.Requests.RiskProfile
{
    [TestClass]
    public class CalculateRiskProfileRequestTests
    {
        [TestMethod]
        public void CalculateRiskProfile_EmployerNumberNotFound_ThrowsException()
        {
            // Arrange     
            var objCase = EntityHelper.Case();
            var caseService = CaseServieStub(objCase);

            var employerObj = EntityHelper.Employer();
            var employerService = EmployerServieStub(employerObj);

            var riskProfileObj = EntityHelper.RiskProfile();
            var riskProfileService = RiskProfileServiceStub(riskProfileObj);

            var cutomerObj = EntityHelper.Customer();
            var adminCustomerService = AdminCustomerServiceStub(cutomerObj);

            var userObj = EntityHelper.UserStub();
            var adminUserService = AdminUserServiceStub(userObj);

            var employeeObj = EntityHelper.Employee();
            employeeObj.EmployeeNumber = "";
            var employeeService = EmployeeServieStub(employeeObj);

            var request = new CalculateRiskProfileRequest(cutomerObj.Id, employerObj.Id, userObj.Id, adminUserService.Object, adminCustomerService.Object, caseService.Object, riskProfileService.Object, employerService.Object, employeeService.Object);

            var parameter = new CalculateRiskProfileRequestParameters
            {
                EmployeeNumber = employeeObj.EmployeeNumber,
                CaseNumber = objCase.CaseNumber,
                EmployerId = employerObj.Id,
                CustomerId = cutomerObj.Id,
                UserId = userObj.Id
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            // Assert
            Assert.AreEqual("employee number was not provided", response.Message);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [TestMethod]
        public void CalculateRiskProfile_EmployerIdNotFound_ThrowsException()
        {
            // Arrange     
            var objCase = EntityHelper.Case();
            var caseService = CaseServieStub(objCase);

            var employerObj = EntityHelper.Employer();
            employerObj.Id = "EmployerId02";
            var employerService = EmployerServieStub(employerObj);

            var riskProfileObj = EntityHelper.RiskProfile();
            var riskProfileService = RiskProfileServiceStub(riskProfileObj);

            var cutomerObj = EntityHelper.Customer();
            var adminCustomerService = AdminCustomerServiceStub(cutomerObj);

            var userObj = EntityHelper.UserStub();
            var adminUserService = AdminUserServiceStub(userObj);

            var employeeObj = EntityHelper.Employee();
            var employeeService = EmployeeServieStub(employeeObj);

            var request = new CalculateRiskProfileRequest(cutomerObj.Id, employerObj.Id, userObj.Id, adminUserService.Object, adminCustomerService.Object, caseService.Object, riskProfileService.Object, employerService.Object, employeeService.Object);

            var parameter = new CalculateRiskProfileRequestParameters
            {
                EmployeeNumber = employeeObj.EmployeeNumber,
                CaseNumber = objCase.CaseNumber,
                EmployerId = employerObj.Id,
                CustomerId = cutomerObj.Id,
                UserId = userObj.Id
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            // Assert
            Assert.AreEqual("Employer is not valid or risk profile feature is not enabled", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CalculateRiskProfile_CustomerNotFound_ThrowsException()
        {
            // Arrange     
            var objCase = EntityHelper.Case();
            var caseService = CaseServieStub(objCase);

            var employerObj = EntityHelper.Employer();            
            var employerService = EmployerServieStub(employerObj);

            var riskProfileObj = EntityHelper.RiskProfile();
            var riskProfileService = RiskProfileServiceStub(riskProfileObj);

            var cutomerObj = EntityHelper.Customer();
            cutomerObj.Id = "CustomerId02";
            var adminCustomerService = AdminCustomerServiceStub(cutomerObj);

            var userObj = EntityHelper.UserStub();
            var adminUserService = AdminUserServiceStub(userObj);

            var employeeObj = EntityHelper.Employee();
            var employeeService = EmployeeServieStub(employeeObj);

            var request = new CalculateRiskProfileRequest(cutomerObj.Id, employerObj.Id, userObj.Id, adminUserService.Object, adminCustomerService.Object, caseService.Object, riskProfileService.Object, employerService.Object, employeeService.Object);

            var parameter = new CalculateRiskProfileRequestParameters
            {
                EmployeeNumber = employeeObj.EmployeeNumber,
                CaseNumber = objCase.CaseNumber,
                EmployerId = employerObj.Id,
                CustomerId = cutomerObj.Id,
                UserId = userObj.Id
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            // Assert
            Assert.AreEqual("Customer is not found", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CalculateRiskProfile_UserNotFound_ThrowsException()
        {
            // Arrange     
            var objCase = EntityHelper.Case();
            var caseService = CaseServieStub(objCase);

            var employerObj = EntityHelper.Employer();
            var employerService = EmployerServieStub(employerObj);

            var riskProfileObj = EntityHelper.RiskProfile();
            var riskProfileService = RiskProfileServiceStub(riskProfileObj);

            var cutomerObj = EntityHelper.Customer();           
            var adminCustomerService = AdminCustomerServiceStub(cutomerObj);

            var userObj = EntityHelper.UserStub();
            userObj.Id = "UserId02";
            var adminUserService = AdminUserServiceStub(userObj);

            var employeeObj = EntityHelper.Employee();
            var employeeService = EmployeeServieStub(employeeObj);

            var request = new CalculateRiskProfileRequest(cutomerObj.Id, employerObj.Id, userObj.Id, adminUserService.Object, adminCustomerService.Object, caseService.Object, riskProfileService.Object, employerService.Object, employeeService.Object);

            var parameter = new CalculateRiskProfileRequestParameters
            {
                EmployeeNumber = employeeObj.EmployeeNumber,
                CaseNumber = objCase.CaseNumber,
                EmployerId = employerObj.Id,
                CustomerId = cutomerObj.Id,
                UserId = "UserId02"
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            // Assert
            Assert.AreEqual("User is not found", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CalculateRiskProfile_EmployeeNotFound_ThrowsException()
        {
            // Arrange     
            var objCase = EntityHelper.Case();
            var caseService = CaseServieStub(objCase);

            var employerObj = EntityHelper.Employer();
            var employerService = EmployerServieStub(employerObj);

            var riskProfileObj = EntityHelper.RiskProfile();
            var riskProfileService = RiskProfileServiceStub(riskProfileObj);

            var cutomerObj = EntityHelper.Customer();
            cutomerObj.Id = "";
            var adminCustomerService = AdminCustomerServiceStub(cutomerObj);

            var userObj = EntityHelper.UserStub();           
            var adminUserService = AdminUserServiceStub(userObj);

            var employeeObj = EntityHelper.Employee();            
            var employeeService = EmployeeServieStub(employeeObj);

            var request = new CalculateRiskProfileRequest(cutomerObj.Id, employerObj.Id, userObj.Id, adminUserService.Object, adminCustomerService.Object, caseService.Object, riskProfileService.Object, employerService.Object, employeeService.Object);

            var parameter = new CalculateRiskProfileRequestParameters
            {
                EmployeeNumber = employeeObj.EmployeeNumber,
                CaseNumber = objCase.CaseNumber,
                EmployerId = employerObj.Id,
                CustomerId = cutomerObj.Id,
                UserId = userObj.Id
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            // Assert
            Assert.AreEqual("Employee not found", response.Message);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }


        /// <summary>
        /// Create stub for caseService - GetCaseByCaseNumber
        /// </summary>
        /// <param name="cAse"></param>
        /// <returns></returns>
        private Mock<ICaseService> CaseServieStub(AbsenceSoft.Data.Cases.Case cAse)
        {
            var caseService = new Mock<ICaseService>();

            caseService
          .Setup(x => x.GetCaseByCaseNumber(
                It.Is<string>(en => en == "772655213")))
                .Returns(cAse)
                .Verifiable();
            return caseService;
        }

        /// <summary>
        /// craete stub for employerService - GetById
        /// </summary>
        /// <param name="employer"></param>
        /// <returns></returns>
        private Mock<IEmployerService> EmployerServieStub(Customers.Employer employer)
        {
            var employerService = new Mock<IEmployerService>();

            employerService
           .Setup(x => x.GetById(
                 It.Is<string>(en => en == "EmployerId01")))
                 .Returns(employer)
                 .Verifiable();
            return employerService;
        }

        /// <summary>
        /// Create stub for adminService - GetUserById
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private Mock<IAdminUserService> AdminUserServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
          .Setup(x => x.GetUserById(
                It.Is<string>(en => en == "UserId01")))
                .Returns(user)
                .Verifiable();
            return adminService;            
        }

        /// <summary>
        /// Create stub for employeeService - GetEmployeeByEmployeeNumber
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        private Mock<IEmployeeService> EmployeeServieStub(Customers.Employee employee)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
         .Setup(x => x.GetEmployeeByEmployeeNumber(
               It.Is<string>(en => en == "EmployeeNo01"), It.Is<string>(en => en == "CustomerId01"), It.Is<string>(en => en == "EmployerId01")))
               .Returns(employee)
               .Verifiable();
            return employeeService;          
        }

        /// <summary>
        /// Create stub for adminCustomerService - GetCustomer
        /// </summary>
        /// <param name="cutomer"></param>
        /// <returns></returns>
        private Mock<IAdminCustomerService> AdminCustomerServiceStub(Customer cutomer)
        {
            var adminCustomerService = new Mock<IAdminCustomerService>();

            adminCustomerService
        .Setup(x => x.GetCustomer(
              It.Is<string>(en => en == "CustomerId01")))
              .Returns(cutomer)
              .Verifiable();
            return adminCustomerService;
        }


        /// <summary>
        /// Create stub for riskProfileService - CalculateAndSaveRiskProfile
        /// </summary>
        /// <param name="riskProfile"></param>
        /// <returns></returns>
        private Mock<IRiskProfileService> RiskProfileServiceStub(CalculatedRiskProfile riskProfile)
        {
            var riskProfileService = new Mock<IRiskProfileService>();

            riskProfileService
       .Setup(x => x.CalculateAndSaveRiskProfile(
             It.Is<string>(en => en == "EmployeeNo01"), It.Is<string>(en => en == "5bb310886e66da7c7c7418b3")))
             .Returns(riskProfile)
             .Verifiable();
            return riskProfileService;
        }
    }
}
