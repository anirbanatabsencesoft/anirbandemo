﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft;
using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Security;
using AT.Api.Core.Exceptions;
using Moq;
using Customers = AbsenceSoft.Data.Customers;
using AT.Api.Case;
using AT.Api.Case.Requests.DisabilityInfo;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AT.Api.Case.Models.Certifications;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Notes.Contracts;
using AbsenceSoft.Data.Notes;
using System.Threading.Tasks;
using System.Net;
using AbsenceSoft.Logic.Customers.Contracts;
using System.Collections.Generic;
using AT.Api.Case.Requests.Certifications;

namespace AT.Api.Case.Tests.Requests.Certifications
{
    [TestClass]
    public class GetCertificationsRequestTests
    {
        [TestMethod]
        public void GetCertifications_InvalidCaseId_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(null);            
            var employerService = EmployerServieStub(null);
            var request = new GetCertificationsRequest(caseService.Object);

            var parameter = new GetCertificationsParameters
            {
                CaseNumber = "",
                EmployerId = "2",
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetCertifications_CaseNotFound_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(null); // null case
            var employerService = EmployerServieStub(EntityHelper.Employer());

            var request = new GetCertificationsRequest(caseService.Object);

            var parameter = new GetCertificationsParameters
            {
                CaseNumber = "1390275272",
                EmployerId = "2"
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.ExpectationFailed, response.StatusCode);
        }

        [TestMethod]
        public void GetCertifications_InvalidEmployerId_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();

            var caseService = CaseServieStub(EntityHelper.Case()); // null case
            var diagnosisInfo = EntityHelper.DisabilityInfo();            
            var employerService = EmployerServieStub(EntityHelper.Employer());

            var request = new GetCertificationsRequest(caseService.Object);
            var parameter = new GetCertificationsParameters
            {
                CaseNumber = "1390275272",
                EmployerId = ""
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Missing employerId", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        //[TestMethod]
        //public void GetCertifications_InvalidEmployer_ThrowsException()
        //{
        //    // Arrange
        //    var user = EntityHelper.UserStub();
        //    var caseService = CaseServieStub(EntityHelper.Case());
        //    var employerService = EmployerServieStub(null);
        //    var request = new GetCertificationsRequest(caseService.Object);
        //    var parameter = new GetCertificationsParameters
        //    {
        //        CaseNumber = "772655213",
        //        EmployerId = "2"
        //    };

        //    // Act
        //    var response = Assert.ThrowsException<ApiException>(() =>
        //        request.Handle(parameter));

        //    // Assert
        //    Assert.AreEqual("Employer is not valid or guidelines data feature is not enabled.", response.Message);
        //    Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        //}

        [TestMethod]
        public void GetCertifications_Certification_Success()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var note = EntityHelper.CaseNote();
            var Certification = EntityHelper.Certification();
            var caseentity = EntityHelper.Case();
            caseentity.Certifications = new List<Certification>(0) { EntityHelper.Certification() };

            var caseService = CaseServieStub(caseentity);
            var noteService = NotesServieStub(note);

            var employerService = EmployerServieStub(EntityHelper.Employer());
            var request = new GetCertificationsRequest(caseService.Object, noteService.Object);

            var parameter = new GetCertificationsParameters
            {
                CaseNumber = "772655213",
                EmployerId = "2"
            };

            // Act
            var response = request.Handle(parameter);

            // Assert
            Assert.IsNotNull(response);
            //Assert.AreEqual(1, response.Data.Count());
            //Assert.AreEqual("Without hospitalization", response.Data);
        }

        private Mock<ICaseService> CaseServieStub(AbsenceSoft.Data.Cases.Case cAse)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);            

            return caseService;
        }

        private Mock<INotesService> NotesServieStub(CaseNote note)
        {
            var noteService = new Mock<INotesService>();

            noteService
                .Setup(x => x.GetCaseNoteByCertId(It.IsAny<Guid>()))
                .Returns("Note");

            noteService
                .Setup(x => x.SaveCaseNote(It.IsAny<CaseNote>()))
                .Returns(note);

            return noteService;
        }

        private Mock<IEmployerService> EmployerServieStub(Customers.Employer employer)
        {
            var employerService = new Mock<IEmployerService>();

            employerService
                .Setup(x => x.GetById(It.IsAny<string>()))
                .Returns(employer);

            return employerService;
        }

        private Mock<IEmployeeService> EmployeeServieStub(Customers.Employee employee)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee);

            return employeeService;
        }

        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }
    }
}
