﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbsenceSoft;
using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Security;
using AT.Api.Core.Exceptions;
using Moq;
using Customers = AbsenceSoft.Data.Customers;
using AT.Api.Case;
using AT.Api.Case.Requests.DisabilityInfo;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AT.Api.Case.Models.Certifications;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Notes.Contracts;
using AbsenceSoft.Data.Notes;
using System.Threading.Tasks;
using System.Net;
using AbsenceSoft.Logic.Customers.Contracts;
using System.Collections.Generic;
using AT.Api.Case.Requests.Certifications;

namespace AT.Api.Case.Tests.Requests.Certifications
{
    [TestClass]
    public class SaveCertificationsRequestTests
    {
        [TestMethod]
        public void SaveCertifications_InvalidCaseId_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var adminservice = AdminServiceStub(user);
            var caseService = CaseServieStub(null);            
            var employerService = EmployerServieStub(null);
            var noteService = NotesServieStub(EntityHelper.CaseNote());
            var request = new SaveCertificationsRequest("UserKey", caseService.Object, noteService.Object, adminservice.Object, employerService.Object);

            var parameter = new SaveCertificationsParameters
            {
                CaseNumber = "",
                EmployerId = "2",
                Certifications = new List<CertificationModel>(0) { new CertificationModel
                {
                    Frequency = 1,
                    FrequencyType = 3,
                    FrequencyUnitType = 0,
                    Duration = 2,
                    DurationType = 1,
                    IntermittentType = 1,
                    Occurances = 2,
                    IsCertificateIncomplete = false,
                    Notes = "Note"
                } }
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveCertifications_CaseNotFound_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(null); // null case
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var noteService = NotesServieStub(EntityHelper.CaseNote());
            var request = new SaveCertificationsRequest("UserKey", caseService.Object, noteService.Object, adminservice.Object, employerService.Object);

            var parameter = new SaveCertificationsParameters
            {
                CaseNumber = "772655213",
                EmployerId = "2",
                Certifications = new List<CertificationModel>(0) { new CertificationModel
                {
                    Frequency = 1,
                    FrequencyType = 3,
                    FrequencyUnitType = 0,
                    Duration = 2,
                    DurationType = 1,
                    IntermittentType = 1,
                    Occurances = 2,
                    IsCertificateIncomplete = false,
                    Notes = "Note"
                } }
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Case not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.ExpectationFailed, response.StatusCode);
        }

        [TestMethod]
        public void SaveCertifications_InvalidEmployerId_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();

            var caseService = CaseServieStub(EntityHelper.Case()); // null case                  
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var adminservice = AdminServiceStub(user);
            var noteService = NotesServieStub(EntityHelper.CaseNote());
            var request = new SaveCertificationsRequest("UserKey", caseService.Object, noteService.Object, adminservice.Object, employerService.Object);
            var parameter = new SaveCertificationsParameters
            {
                CaseNumber = "772655213",
                EmployerId = "",
                Certifications = new List<CertificationModel>(0) { new CertificationModel
                {
                    Frequency = 1,
                    FrequencyType = 3,
                    FrequencyUnitType = 0,
                    Duration = 2,
                    DurationType = 1,
                    IntermittentType = 1,
                    Occurances = 2,
                    IsCertificateIncomplete = false,
                    Notes = "Note"
                } }
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Missing EmployerId", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveCertifications_InvalidCertification_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(EntityHelper.Case());
            var employerService = EmployerServieStub(null);
            var noteService = NotesServieStub(EntityHelper.CaseNote());
            var adminservice = AdminServiceStub(user);
            var request = new SaveCertificationsRequest("UserKey", caseService.Object, noteService.Object, adminservice.Object, employerService.Object);
            var parameter = new SaveCertificationsParameters
            {
                CaseNumber = "772655213",
                EmployerId = "2",
                Certifications = new List<CertificationModel>()
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Certification details not provided", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveCertifications_InvalidCertificationDetails_ThrowsException()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var caseService = CaseServieStub(EntityHelper.Case());
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var noteService = NotesServieStub(EntityHelper.CaseNote());
            var adminservice = AdminServiceStub(user);
            var request = new SaveCertificationsRequest("UserKey", caseService.Object, noteService.Object, adminservice.Object, employerService.Object);
            var parameter = new SaveCertificationsParameters
            {
                CaseNumber = "772655213",
                EmployerId = "2",
                Certifications = new List<CertificationModel>(0) { new CertificationModel { Frequency = null, IsCertificateIncomplete = false } }
            };

            // Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            // Assert
            Assert.AreEqual("Some required certification details are missing : \r\nFrequency is required\r\nFrequency episode occurance is required\r\nDuration is required\r\n", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveCertifications_Certification_Success()
        {
            // Arrange
            var user = EntityHelper.UserStub();
            var note = EntityHelper.CaseNote();
            var Certification = EntityHelper.Certification();

            var caseentity = EntityHelper.Case();
            caseentity.Certifications = new List<Certification>(0) { EntityHelper.Certification() };

            var caseService = CaseServieStub(caseentity);
            var noteService = NotesServieStub(note);
            var adminservice = AdminServiceStub(user);
            var employerService = EmployerServieStub(EntityHelper.Employer());
            var request = new SaveCertificationsRequest("UserKey", caseService.Object, noteService.Object, adminservice.Object, employerService.Object);

            var parameter = new SaveCertificationsParameters
            {
                CaseNumber = "772655213",
                EmployerId = "2",
                Certifications = new List<CertificationModel>(0) { new CertificationModel
                {
                    Frequency = 1,
                    FrequencyType = 3,
                    FrequencyUnitType = 0,
                    Duration = 2,
                    DurationType = 1,
                    IntermittentType = 1,
                    Occurances = 2,
                    IsCertificateIncomplete = false,
                    Notes = "Note"
                } }
            };

            // Act
            var response = request.Handle(parameter);

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(true, response.Data);
            Assert.AreEqual("Certifications information saved successfully", response.Message);
        }

        private Mock<ICaseService> CaseServieStub(AbsenceSoft.Data.Cases.Case cAse)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);

            caseService
                .Setup(x => x.CreateOrModifyCertification(It.IsAny<AbsenceSoft.Data.Cases.Case>(), It.IsAny<Certification>(), true))
                .Returns(cAse);

            return caseService;
        }

        private Mock<INotesService> NotesServieStub(CaseNote note)
        {
            var noteService = new Mock<INotesService>();

            noteService
                .Setup(x => x.GetCaseNoteByCertId(It.IsAny<Guid>()))
                .Returns("Note");

            noteService
                .Setup(x => x.SaveCaseNote(It.IsAny<CaseNote>()))
                .Returns(note);

            return noteService;
        }

        private Mock<IEmployerService> EmployerServieStub(Customers.Employer employer)
        {
            var employerService = new Mock<IEmployerService>();

            employerService
                .Setup(x => x.GetById(It.IsAny<string>()))
                .Returns(employer);

            return employerService;
        }

        private Mock<IEmployeeService> EmployeeServieStub(Customers.Employee employee)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee);

            return employeeService;
        }

        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }
    }
}
