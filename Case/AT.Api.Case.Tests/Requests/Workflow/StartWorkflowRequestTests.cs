﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Workflows.Contracts;
using AT.Api.Case.Requests.Workflow;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Net;
using Cases = AbsenceSoft.Data.Cases;
using Workflows = AbsenceSoft.Data.Workflows;

namespace AT.Api.Case.Tests.Requests.Workflow
{
    [TestClass]
    public class StartWorkflowRequestTests
    {
        [TestMethod]
        public void StartWorkflow_Valid_StartsWorkflow()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var caseData = EntityHelper.Case();
            var workflow = EntityHelper.Workflow();

            var request = new StartWorkflowRequest(
                "UserId01",
                AdminServiceStub(user).Object,
                WorkflowService(workflow).Object,
                CaseService(caseData).Object);

            var parameters = new StartWorkflowParameters
            {
                CaseNumber = "CaseNumber01",
                WorkflowCode = "WorkflowCode01"
            };

            //// Act
            var response = request.Handle(parameters);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsTrue(response.Success);
            Assert.AreEqual("Workflow started", response.Message);
        }

        [TestMethod]
        public void StartWorkflow_CallService_ServicesCalledWithData()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var caseData = EntityHelper.Case();
            var workflow = EntityHelper.Workflow();

            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.Is<string>(id => id == "UserId01")))
                .Returns(user)
                .Verifiable();

            var workflowService = new Mock<IWorkflowService>();

            workflowService
                .Setup(s => s.Run(
                    It.Is<string>(code => code == "WorkflowCode01"),
                    It.Is<string>(caseId => caseId == "5bb310886e66da7c7c7418b3"),
                    null))
                .Verifiable();

            workflowService
                .Setup(s => s.Dispose())
                .Verifiable();

            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(s => s.GetCaseByCaseNumber(It.Is<string>(cn => cn == "CaseNumber01")))
                .Returns(caseData);

            var request = new StartWorkflowRequest(
                "UserId01",
                adminService.Object,
                workflowService.Object,
                caseService.Object);

            var parameters = new StartWorkflowParameters
            {
                CaseNumber = "CaseNumber01",
                WorkflowCode = "WorkflowCode01"
            };

            //// Act
            var response = request.Handle(parameters);

            //// Assert
            adminService.Verify();
            workflowService.Verify();
            caseService.Verify();
        }

        [TestMethod]
        public void StartWorkflow_CaseNumberNull_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var caseData = EntityHelper.Case();

            var request = new StartWorkflowRequest(
                "UserId01",
                AdminServiceStub(user).Object,
                WorkflowService().Object,
                CaseService(caseData).Object);

            var parameters = new StartWorkflowParameters
            {
                CaseNumber = null,
                WorkflowCode = "WorkflowCode01",
            };

            //// Act           
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameters));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Case number is required", response.Message);
        }

        [TestMethod]
        public void StartWorkflow_CaseNumberEmpty_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var caseData = EntityHelper.Case();

            var request = new StartWorkflowRequest(
                "UserId01",
                AdminServiceStub(user).Object,
                WorkflowService().Object,
                CaseService(caseData).Object);

            var parameters = new StartWorkflowParameters
            {
                CaseNumber = " ",
                WorkflowCode = "WorkflowCode01"
            };

            //// Act           
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameters));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Case number is required", response.Message);
        }

        [TestMethod]
        public void StartWorkflow_WorkflowCodeNull_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var caseData = EntityHelper.Case();

            var request = new StartWorkflowRequest(
                "UserId01",
                AdminServiceStub(user).Object,
                WorkflowService().Object,
                CaseService(caseData).Object);

            var parameters = new StartWorkflowParameters
            {
                CaseNumber = "CaseNumber01",
                WorkflowCode = null,
            };

            //// Act           
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameters));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Workflow code is required", response.Message);
        }

        [TestMethod]
        public void StartWorkflow_WorkflowCodeEmpty_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var caseData = EntityHelper.Case();

            var request = new StartWorkflowRequest(
                "UserId01",
                AdminServiceStub(user).Object,
                WorkflowService().Object,
                CaseService(caseData).Object);

            var parameters = new StartWorkflowParameters
            {
                CaseNumber = "CaseNumber01",
                WorkflowCode = " "
            };

            //// Act           
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameters));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Workflow code is required", response.Message);
        }

        [TestMethod]
        public void StartWorkflow_CaseNotFound_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var caseData = EntityHelper.Case();

            var request = new StartWorkflowRequest(
                "UserId01",
                AdminServiceStub(user).Object,
                WorkflowService().Object,
                CaseService(null).Object);

            var parameters = new StartWorkflowParameters
            {
                CaseNumber = "CaseNumber01",
                WorkflowCode = "WorkflowCode01",
            };

            //// Act           
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameters));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Case not found with the given case number", response.Message);
        }

        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }

        private Mock<IWorkflowService> WorkflowService(Workflows.Workflow workflow = null)
        {
            var workflowService = new Mock<IWorkflowService>();

            workflowService
                .Setup(s => s.Run(It.IsAny<string>(), It.IsAny<string>(), null));

            workflowService
                .Setup(s => s.Dispose());

            return workflowService;
        }

        private Mock<ICaseService> CaseService(Cases.Case caseData = null)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(s => s.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(caseData);

            return caseService;
        }
    }
}
