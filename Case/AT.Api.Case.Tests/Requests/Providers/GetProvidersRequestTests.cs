﻿using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Case.Requests.Providers;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Cases = AbsenceSoft.Data.Cases;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Case.Tests.Requests.Providers
{
    [TestClass]
    public class GetProvidersRequestTests
    {
        [TestMethod]
        public void GetProviders_NullCaseNumber_ThrowsException()
        {
            //// Arrange
            var parameters = new GetProvidersParameters
            {
                CaseNumber = null,
                CustomerId = "Customer01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(() => new GetProvidersRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Case number is required", exception.Message);
        }

        [TestMethod]
        public void GetProviders_EmptyCaseNumber_ThrowsException()
        {
            //// Arrange
            var parameters = new GetProvidersParameters
            {
                CaseNumber = " ",
                CustomerId = "Customer01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(() => new GetProvidersRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Case number is required", exception.Message);
        }

        [TestMethod]
        public void GetProviders_NullCustomerId_ThrowsException()
        {
            //// Arrange
            var parameters = new GetProvidersParameters
            {
                CaseNumber = "772655213",
                CustomerId = null
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(() => new GetProvidersRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("CustomerId is required", exception.Message);
        }

        [TestMethod]
        public void GetProviders_EmptyCustomerId_ThrowsException()
        {
            //// Arrange
            var parameters = new GetProvidersParameters
            {
                CaseNumber = "772655213",
                CustomerId = " "
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(() => new GetProvidersRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("CustomerId is required", exception.Message);
        }

        [TestMethod]
        public void GetProviders_NoMatchingCase_ThrowsException()
        {
            //// Arrange            
            var caseService = CaseServieStub();

            var employeeService = EmployeeServiceStub();

            var request = new GetProvidersRequest(caseService.Object);

            var parameter = new GetProvidersParameters
            {
                CaseNumber = "772655213",
                CustomerId = "Customer01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
               request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Case was not found for 772655213", response.Message);
        }

        [TestMethod]
        public void GetProviders_EmptyContacts_ReturnsEmptyContacts()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var cAse = EntityHelper.Case(employee: employee);

            var caseService = CaseServieStub(cAse);
            var employeeService = EmployeeServiceStub(new List<Customers.EmployeeContact>());

            var request = new GetProvidersRequest(caseService.Object, employeeService.Object);

            var parameter = new GetProvidersParameters
            {
                CaseNumber = "772655213",
                CustomerId = "Customer01"
            };

            //// Act
            var response =  request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(0, response.Data.Count());
            Assert.AreEqual("No providers found", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        [TestMethod]
        public void GetProviders_NoContacts_ReturnsEmptyContacts()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var cAse = EntityHelper.Case(employee: employee);

            var caseService = CaseServieStub(cAse);
            var employeeService = EmployeeServiceStub(null);

            var request = new GetProvidersRequest(caseService.Object, employeeService.Object);

            var parameter = new GetProvidersParameters
            {
                CaseNumber = "772655213",
                CustomerId = "Customer01"
            };

            //// Act
            var response =  request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(0, response.Data.Count());
            Assert.AreEqual("No providers found", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        [TestMethod]
        public void GetProviders_ValidEmployee_ReturnsContacts()
        {
            //// Arrange           
            var employee = EntityHelper.Employee();

            var dob = new DateTime(1978, 01, 01);
            var address = EntityHelper.Address("AddressName02", "Address1Value02", "Address2Value02", "City02", "State02", "PostalCode02", "Country02");
            var contact = EntityHelper.Contact(new Guid(), "Title02", "Company02", "012", "FirstName02", "LastName02", "021", "031", fax: "xxx02", isPrimary: true);

            var employeeContact1 = EntityHelper.EmployeeContact(employee: employee);
            var employeeContact2 = EntityHelper.EmployeeContact("Contact02", "EmployeeNo01", "Employee001", "RelatedEmp02", "Type02", 1234, contact, employee);

            var employeeContacts = new List<Customers.EmployeeContact>
            {
                employeeContact1, employeeContact2
            };

            var cAse = EntityHelper.Case(employee: employee);
            var caseService = CaseServieStub(cAse);
            var employeeService = EmployeeServiceStub(employeeContacts);

            var request = new GetProvidersRequest(caseService.Object, employeeService.Object);

            var parameter = new GetProvidersParameters
            {
                CaseNumber = "772655213",
                CustomerId = "Customer01"
            };

            //// Act
            var response =  request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(2, response.Data.Count());

            var result1 = response.Data.ElementAt(0);
            Assert.AreEqual("Contact01", result1.ContactId);
            Assert.AreEqual("Type01", result1.ContactTypeCode);
            Assert.AreEqual("Company01", result1.CompanyName);
            Assert.AreEqual("FirstName01", result1.FirstName);
            Assert.AreEqual("LastName01", result1.LastName);
            Assert.AreEqual("Company01", result1.CompanyName);
            Assert.AreEqual("xxx01", result1.Fax);
            Assert.AreEqual(false, result1.IsPrimary);
            Assert.AreEqual("02", result1.Phone);

            var result2 = response.Data.ElementAt(1);
            Assert.AreEqual("Contact02", result2.ContactId);
            Assert.AreEqual("Type02", result2.ContactTypeCode);
            Assert.AreEqual("Company02", result2.CompanyName);
            Assert.AreEqual("FirstName02", result2.FirstName);
            Assert.AreEqual("LastName02", result2.LastName);
            Assert.AreEqual("Company02", result2.CompanyName);
            Assert.AreEqual("xxx02", result2.Fax);
            Assert.AreEqual(true, result2.IsPrimary);
            Assert.AreEqual("021", result2.Phone);
        }

        [TestMethod]
        public void GetProviders_CallServices_ServicesShouldBeCalled()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var cAse = EntityHelper.Case(employee: employee);
            var employeeContact = EntityHelper.EmployeeContact();
            var contacts = new List<Customers.EmployeeContact>();

            var caseService = new Mock<ICaseService>();
            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.Is<string>(cn => cn == "772655213")))
                .Returns(cAse)
                .Verifiable();

            var employeeService = new Mock<IEmployeeService>();
            employeeService
               .Setup(x => x.GetMedicalContacts(
                   It.Is<string>(cid => cid == "CustomerId01"),
                   It.Is<string>(ei => ei == "EmployeeId01")))
               .Returns(contacts)
               .Verifiable();

            var request = new GetProvidersRequest(caseService.Object, employeeService.Object);

            var parameter = new GetProvidersParameters
            {
                CaseNumber = "772655213",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response =  request.Handle(parameter);

            //// Assert
            caseService.Verify();
            employeeService.Verify();
        }

        private Mock<IEmployeeService> EmployeeServiceStub(List<Customers.EmployeeContact> employeeContacts = null)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetMedicalContacts(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employeeContacts);

            return employeeService;
        }

        private Mock<ICaseService> CaseServieStub(Cases.Case cAse = null)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);

            return caseService;
        }
    }
}
