﻿using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Case.Models.Providers;
using AT.Api.Case.Requests.Providers;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using Cases = AbsenceSoft.Data.Cases;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Case.Tests.Requests.Providers
{
    [TestClass]
    public class SaveProviderRequestTests
    {
        [TestMethod]
        public void SaveContact_NullCaseNumber_ThrowsException()
        {
            //// Arrange
            var parameters = new SaveProviderParameters
            {
                CaseNumber = null
            };

            //// Act
            var exception =  Assert.ThrowsException<ApiException>(
              () =>  new SaveProviderRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Case number is required", exception.Message);
        }

        [TestMethod]
        public void SaveContact_EmptyCaseNumber_ThrowsException()
        {
            //// Arrange
            var parameters = new SaveProviderParameters
            {
                CaseNumber = " "
            };

            //// Act
            var exception =  Assert.ThrowsException<ApiException>(
              () =>  new SaveProviderRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Case number is required", exception.Message);
        }

        [TestMethod]
        public void SaveContact_NullCustomerId_ThrowsException()
        {
            //// Arrange
            var parameters = new SaveProviderParameters
            {
                CaseNumber = "772655213",
                EmployerId = "EmployerId01",
                CustomerId = null
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(() => new SaveProviderRequest().Handle(parameters));
          
            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Customer Id is required", exception.Message);
        }

        [TestMethod]
        public void SaveContact_EmptyCustomerId_ThrowsException()
        {
            //// Arrange
            var parameters = new SaveProviderParameters
            {
                CaseNumber = "772655213",
                EmployerId = "EmployerId01",
                CustomerId = " "
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(() =>
            new SaveProviderRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Customer Id is required", exception.Message);
        }

        [TestMethod]
        public void SaveContact_NullEmployerId_ThrowsException()
        {
            //// Arrange
            var parameters = new SaveProviderParameters
            {
                CaseNumber = "772655213",
                EmployerId = null,
                CustomerId = "CustomerId01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(() => 
            new SaveProviderRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Employer Id is required", exception.Message);
        }

        [TestMethod]
        public void SaveContact_EmptyEmployerId_ThrowsException()
        {
            //// Arrange
            var parameters = new SaveProviderParameters
            {
                CaseNumber = "772655213",
                EmployerId = " ",
                CustomerId = "CustomerId01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(() => 
            new SaveProviderRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Employer Id is required", exception.Message);
        }     

        [TestMethod]
        public void SaveContact_NoMatchingCase_ReturnsEmptyContact()
        {
            //// Arrange            
            var caseService = CaseServieStub();

            var request = new SaveProviderRequest(caseService.Object);

            var parameter = new SaveProviderParameters
            {
                CaseNumber = "772655213",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                CaseProvider = new CaseProviderModel { ContactTypeCode = "Type01" }
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Case was not found for 772655213", exception.Message);
        }        

        [TestMethod]
        public void SaveContact_ValidContact_ReturnsContact()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var contactType1 = EntityHelper.ContactType();
            var contactType2 = EntityHelper.ContactType("Code02", "CodeName02");

            var theCase = EntityHelper.Case(employee: employee);
            var employeeContact = EntityHelper.EmployeeContact();

            var caseService = CaseServieStub(theCase);
            var employeeService = EmployeeServiceStub(employeeContact);
            var contactTypeService = ContactTypeStub(new List<Customers.ContactType>() { contactType1, contactType2 });

            var contactModel = new CaseProviderModel
            {
                FirstName = "FirstName01",
                LastName = "LastName01",
                CompanyName = "Company01",
                ContactPersonName = string.Empty,
                Fax = "Fax01",
                ContactTypeCode = "Code01",
                IsPrimary = false,
                Phone = "02",
            };

            var parameters = new SaveProviderParameters
            {
                CaseNumber = "772655213",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                CaseProvider = contactModel
            };
            var request = new SaveProviderRequest(caseService.Object, employeeService.Object, contactTypeService.Object);

            //// Act
            var response =  request.Handle(parameters);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual("Contact01", response.Data);
            Assert.AreEqual("Provider contact saved successfully", response.Message);
        }

        [TestMethod]
        public void SaveContact_UpdateExistingContact_ReturnsUpdatedContactId()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var contactType1 = EntityHelper.ContactType();
            var contactType2 = EntityHelper.ContactType("Code02");

            var theCase = EntityHelper.Case(employee: employee);
            var employeeContact = EntityHelper.EmployeeContact();

            var caseService = CaseServieStub(theCase);
            var employeeService = EmployeeServiceStub(employeeContact);
            var contactTypeService = ContactTypeStub(new List<Customers.ContactType>() { contactType1, contactType2 });

            var contactModel = new CaseProviderModel
            {
                ContactId = "Contact01",
                FirstName = "FirstName01",
                LastName = "LastName01",
                CompanyName = "Company01",
                ContactPersonName = string.Empty,
                Fax = "Fax01",
                ContactTypeCode = "Code01",
                IsPrimary = false,
                Phone = "02",
            };

            var parameters = new SaveProviderParameters
            {
                CaseNumber = "772655213",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                CaseProvider = contactModel
            };
            var request = new SaveProviderRequest(caseService.Object, employeeService.Object, contactTypeService.Object);

            //// Act
            var response =  request.Handle(parameters);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual("Contact01", response.Data);
            Assert.AreEqual("Provider contact saved successfully", response.Message);
        }

        [TestMethod]
        public void SaveContact_CallServices_ServicesShouldBeCalled()
        {
            //// Arrange
            var contactId = new Guid();
            var employee = EntityHelper.Employee();
            var contactType1 = EntityHelper.ContactType();
            var contactType2 = EntityHelper.ContactType("Code02");
            var contactTypes = new List<Customers.ContactType>() { contactType1, contactType2 };

            var theCase = EntityHelper.Case(employee: employee);
            var contact = EntityHelper.Contact(contactId);
            var employeeContact = EntityHelper.EmployeeContact();

            var contactModel = new CaseProviderModel
            {
                ContactId = "Contact01",
                FirstName = "FirstName01",
                LastName = "LastName01",
                CompanyName = "Company01",
                ContactPersonName = string.Empty,
                Fax = "Fax01",
                ContactTypeCode = "Code01",
                IsPrimary = false,
                Phone = "02",
            };

            var parameters = new SaveProviderParameters
            {
                CaseNumber = "772655213",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                CaseProvider = contactModel
            };

            var caseService = new Mock<ICaseService>();
            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.Is<string>(cn => cn == "772655213")))
                .Returns(theCase)
                .Verifiable();

            caseService
               .Setup(x => x.GetCaseProviderContact(
                   It.Is<string>(id => id == "Contact01")))
               .Verifiable();

            var employeeService = new Mock<IEmployeeService>();
            employeeService
               .Setup(x => x.SaveContact(It.Is<Customers.EmployeeContact>(
                   ec => ec.ContactTypeCode == "Code01")))
               .Returns(employeeContact)
               .Verifiable();

            var contactTypeService = new Mock<IContactTypeService>();
            contactTypeService
             .Setup(x => x.GetMedicalContactTypes(
                 It.Is<string>(id => id == "EmployerId01")))
             .Returns(contactTypes)
             .Verifiable();

            contactTypeService
             .Setup(x => x.UpdateMedicalDesignationTypeContactsIsPrimary(
                 It.Is<string>(cid => cid == "CustomerId01"),
                 It.Is<Guid?>(cid => cid == contactId),
                 It.Is<string>(cid => cid == "CustomerId01")))
             .Verifiable();

            var request = new SaveProviderRequest(caseService.Object, employeeService.Object, contactTypeService.Object);

            //// Act
            var response =  request.Handle(parameters);

            //// Assert
            caseService.Verify();
            employeeService.Verify();
        }

        private Mock<IEmployeeService> EmployeeServiceStub(Customers.EmployeeContact employeeContact = null)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
               .Setup(x => x.SaveContact(It.IsAny<Customers.EmployeeContact>()))
               .Returns(employeeContact);

            return employeeService;
        }

        private Mock<ICaseService> CaseServieStub(Cases.Case theCase = null, Customers.EmployeeContact contact = null)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(theCase);

            caseService
                .Setup(x => x.GetCaseProviderContact(It.IsAny<string>()))
                .Returns(contact);

            return caseService;
        }

        private Mock<IContactTypeService> ContactTypeStub(List<Customers.ContactType> contactTypes = null)
        {
            var contactTypeService = new Mock<IContactTypeService>();

            contactTypeService
                .Setup(x => x.GetMedicalContactTypes(It.IsAny<string>()))
                .Returns(contactTypes);

            contactTypeService
                .Setup(x => x.UpdateMedicalDesignationTypeContactsIsPrimary(It.IsAny<string>(), It.IsAny<Guid?>(), It.IsAny<string>()));

            return contactTypeService;
        }
    }
}
