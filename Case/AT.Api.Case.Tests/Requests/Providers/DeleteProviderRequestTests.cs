﻿using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Case.Requests.Contacts;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Net;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Case.Tests.Requests.Providers
{
    [TestClass]
    public class DeleteProviderRequestTests
    {
        [TestMethod]
        public  void DeleteContact_EmptyContactId_ThrowsException()
        {
            /// Arrange          
            var parameters = new DeleteProviderParameters
            {
                ContactId = string.Empty
            };

            var request = new DeleteProviderRequest();

            /// Act
            var exception =  Assert.ThrowsException<ApiException>(
                 () =>  request.Handle(parameters));

            /// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Contact Id is required", exception.Message);
        }

        [TestMethod]
        public  void DeleteContact_NullContactId_ThrowsException()
        {
            /// Arrange          
            var parameters = new DeleteProviderParameters
            {
                ContactId = null
            };

            var request = new DeleteProviderRequest();

            /// Act
            var exception =  Assert.ThrowsException<ApiException>(
                () =>  request.Handle(parameters));

            /// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Contact Id is required", exception.Message);
        }

        [TestMethod]
        public  void DeleteContact_NoMatchingContact_ReturnsEmptyContact()
        {
            /// Arrange                             
            var caseService = CaseServieStub();

            var request = new DeleteProviderRequest(caseService.Object);

            var parameters = new DeleteProviderParameters
            {
                ContactId = "contact001"
            };

            /// Act
            var exception =  Assert.ThrowsException<ApiException>(() =>
                 request.Handle(parameters));

            /// Assert
            Assert.AreEqual("No case provider contact found", exception.Message);
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
        }


        [TestMethod]
        public  void DeleteContact_ValidContactId_ReturnsContact()
        {
            /// Arrange            
            var employeeContact = EntityHelper.EmployeeContact();

            var parameters = new DeleteProviderParameters
            {
                ContactId = "empContact01"
            };

            var caseService = CaseServieStub(employeeContact);
            var employeeService = EmployeeServiceStub();

            var request = new DeleteProviderRequest(caseService.Object, employeeService.Object);

            /// Act
            var response =  request.Handle(parameters);

            /// Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual("empContact01", response.Data);
            Assert.AreEqual("Contact deleted successfully", response.Message);
        }

        [TestMethod]
        public  void DeleteContact_CallServices_ServicesShouldBeCalled()
        {
            /// Arrange
            var employeeContact = EntityHelper.EmployeeContact();

            var caseService = new Mock<ICaseService>();
            caseService
                .Setup(x => x.GetCaseProviderContact(It.Is<string>(en => en == "empContact01")))
                .Returns(employeeContact)
                .Verifiable();

            var employeeService = new Mock<IEmployeeService>();
            employeeService
               .Setup(x => x.DeleteContact(It.Is<string>(en => en == "empContact01")))
               .Verifiable();

            var parameters = new DeleteProviderParameters
            {
                ContactId = "empContact01"
            };

            var request = new DeleteProviderRequest(caseService.Object, employeeService.Object);

            /// Act
            var response =  request.Handle(parameters);

            /// Assert
            caseService.Verify();
            employeeService.Verify();
        }

        private Mock<IEmployeeService> EmployeeServiceStub()
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
               .Setup(x => x.DeleteContact(It.IsAny<string>()));

            return employeeService;
        }

        private Mock<ICaseService> CaseServieStub(Customers.EmployeeContact contact = null)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseProviderContact(It.IsAny<string>()))
                .Returns(contact);

            return caseService;
        }
    }
}
