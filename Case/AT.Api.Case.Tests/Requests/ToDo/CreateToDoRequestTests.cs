﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic.Administration.Contracts;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Tasks.Contracts;
using AT.Api.Case.Models.CreateToDo;
using AT.Api.Case.Requests.CreateToDo;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Net;
using System.Threading.Tasks;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Tests.Requests.ToDo
{
    [TestClass]
    public class CreateToDoRequestTests
    {
        [TestMethod]
        public void CreateToDoRequest_EmptyUserKey_ThrowsException()
        {
            //// Arrange
            var adminUserService = AdminServiceStub(null);
            var administrationService = new Mock<IAdministrationService>();
            var toDoService = new Mock<IToDoService>();
            var caseService = new Mock<ICaseService>();

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => new CreateToDoRequest(
                userId: " ",
                adminUserService: adminUserService.Object,
                administrationService: administrationService.Object,
                caseService: caseService.Object,
                toDoService: toDoService.Object));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
            Assert.AreEqual("Unable to retrieve authorized user detail", response.Message);
        }

        [TestMethod]
        public void CreateToDoRequest_UserNotFound_ThrowsException()
        {
            //// Arrange
            var adminUserService = AdminServiceStub(null);
            var administrationService = new Mock<IAdministrationService>();
            var toDoService = new Mock<IToDoService>();
            var caseService = new Mock<ICaseService>();

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => new CreateToDoRequest(
                userId: "UserKey",
                adminUserService: adminUserService.Object,
                administrationService: administrationService.Object,
                caseService: caseService.Object,
                toDoService: toDoService.Object));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.InternalServerError, response.StatusCode);
            Assert.AreEqual("Unable to find user", response.Message);
        }

        [TestMethod]
        public void CreateToDoRequest_NullCaseNumber_ThrowsException()
        {
            //// Arrange            
            var user = EntityHelper.UserStub();
            var adminUserService = AdminServiceStub(user);
            var administrationService = new Mock<IAdministrationService>();
            var toDoService = new Mock<IToDoService>();
            var caseService = CaseServieStub();

            var request = new CreateToDoRequest(
                userId: "UserKey",
                adminUserService: adminUserService.Object,
                administrationService: administrationService.Object,
                caseService: caseService.Object,
                toDoService: toDoService.Object);

            var parameter = new CreateToDoParameters
            {
                CaseNumber = null,
                CreateToDoModel = new CreateToDoModel
                {
                    Title = "Test title",
                    AssigneeUserEmail = "abc@test.com",
                    Description = "Test description",
                    DueDate = DateTime.UtcNow
                }
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Case number is required", response.Message);
        }

        [TestMethod]
        public void CreateToDoRequest_EmptyCaseNumber_ThrowsException()
        {
            //// Arrange            
            var user = EntityHelper.UserStub();
            var adminUserService = AdminServiceStub(user);
            var administrationService = new Mock<IAdministrationService>();
            var toDoService = new Mock<IToDoService>();
            var caseService = CaseServieStub();

            var request = new CreateToDoRequest(
                userId: "UserKey",
                adminUserService: adminUserService.Object,
                administrationService: administrationService.Object,
                caseService: caseService.Object,
                toDoService: toDoService.Object);

            var parameter = new CreateToDoParameters
            {
                CaseNumber = " ",
                CreateToDoModel = new CreateToDoModel
                {
                    Title = "Test title",
                    AssigneeUserEmail = "abc@test.com",
                    Description = "Test description",
                    DueDate = DateTime.UtcNow
                }
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Case number is required", response.Message);
        }

        [TestMethod]
        public void CreateToDoRequest_NullCreateToDoModel_ThrowsException()
        {
            //// Arrange            
            var user = EntityHelper.UserStub();
            var adminUserService = AdminServiceStub(user);
            var administrationService = new Mock<IAdministrationService>();
            var toDoService = new Mock<IToDoService>();
            var caseService = CaseServieStub();

            var request = new CreateToDoRequest(
                userId: "UserKey",
                adminUserService: adminUserService.Object,
                administrationService: administrationService.Object,
                caseService: caseService.Object,
                toDoService: toDoService.Object);

            var parameter = new CreateToDoParameters
            {
                CaseNumber = "123",
                CreateToDoModel = null
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Data to create ToDo is required", response.Message);
        }

        [TestMethod]
        public void CreateToDoRequest_CaseNotFound_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var adminUserService = AdminServiceStub(user);
            var administrationService = new Mock<IAdministrationService>();
            var toDoService = new Mock<IToDoService>();
            var caseService = new Mock<ICaseService>();
            var caseNumber = "123";

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns((Cases.Case)null);

            var request = new CreateToDoRequest(
                userId: "UserKey",
                adminUserService: adminUserService.Object,
                administrationService: administrationService.Object,
                caseService: caseService.Object,
                toDoService: toDoService.Object);

            var parameter = new CreateToDoParameters
            {
                CaseNumber = caseNumber,
                CreateToDoModel = new CreateToDoModel
                {
                    Title = "Test title",
                    AssigneeUserEmail = "abc@test.com",
                    Description = "Test description",
                    DueDate = DateTime.UtcNow
                }
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual($"Case was not found for 123", response.Message);
        }

        [TestMethod]
        public void CreateToDoRequest_ValidCaseEmptyAssigneeUserEmail_ToDoCreated()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var adminUserService = AdminServiceStub(user);
            var administrationService = new Mock<IAdministrationService>();
            var toDoService = new Mock<IToDoService>();
            var caseService = new Mock<ICaseService>();
            var caseNumber = "123";
            var toDoItem = EntityHelper.ToDoItem();
            var theCase = EntityHelper.Case(casenumber: caseNumber);
            var model = new CreateToDoModel
            {
                Title = "Test title",
                AssigneeUserEmail = "",
                Description = "Test description",
                DueDate = DateTime.UtcNow
            };

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.Is<string>(c => c == caseNumber)))
                .Returns(theCase)
                .Verifiable();

            administrationService
                .Setup(x => x.GetUserByEmail(It.IsAny<string>()))
                .Returns((User)null);

            toDoService
                .Setup(x => x.CreateManualTodoItem(
                    It.Is<Cases.Case>(c => c.Id == theCase.Id && c.CaseNumber == caseNumber),
                    It.Is<User>(u => u.Id == user.Id),
                    It.IsAny<ToDoItemType>(),
                    It.Is<String>(t => t == model.Title),
                    It.Is<String>(d => d == model.Description),
                    It.Is<DateTime>(d => d == model.DueDate),
                    It.IsAny<ToDoItemPriority>(),
                    It.Is<String>(s => s == null)))
                .Returns((ToDoItem)toDoItem)
                .Verifiable();

            var request = new CreateToDoRequest(
                userId: "UserKey",
                adminUserService: adminUserService.Object,
                administrationService: administrationService.Object,
                caseService: caseService.Object,
                toDoService: toDoService.Object);

            var parameter = new CreateToDoParameters
            {
                CaseNumber = caseNumber,
                CreateToDoModel = model
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual("ToDo created successfully", response.Message);
            caseService.Verify();
            toDoService.Verify();
        }

        [TestMethod]
        public void CreateToDoRequest_ReturnsNullToDoItem_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var adminUserService = AdminServiceStub(user);
            var administrationService = new Mock<IAdministrationService>();
            var toDoService = new Mock<IToDoService>();
            var caseService = new Mock<ICaseService>();
            var caseNumber = "123";
            var assigneeUserEmail = "test@test.com";
            var theCase = EntityHelper.Case(casenumber: caseNumber);
            var assigneeUser = EntityHelper.UserStub(userId: "123", firstName: "first", lastName: "last", email: assigneeUserEmail);
            var model = new CreateToDoModel
            {
                Title = "Test title",
                AssigneeUserEmail = assigneeUserEmail,
                Description = "Test description",
                DueDate = DateTime.UtcNow
            };

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.Is<string>(c => c == caseNumber)))
                .Returns(theCase)
                .Verifiable();

            administrationService
                .Setup(x => x.GetUserByEmail(It.Is<string>(u => u == model.AssigneeUserEmail)))
                .Returns(assigneeUser)
                .Verifiable();

            toDoService
                .Setup(x => x.CreateManualTodoItem(
                    It.Is<Cases.Case>(c => c.Id == theCase.Id && c.CaseNumber == caseNumber),
                    It.Is<User>(u => u.Id == user.Id),
                    It.IsAny<ToDoItemType>(),
                    It.Is<String>(t => t == model.Title),
                    It.Is<String>(d => d == model.Description),
                    It.Is<DateTime>(d => d == model.DueDate),
                    It.IsAny<ToDoItemPriority>(),
                    It.Is<String>(s => s == assigneeUser.Id)))
                .Returns((ToDoItem)null);

            var request = new CreateToDoRequest(
                userId: "UserKey",
                adminUserService: adminUserService.Object,
                administrationService: administrationService.Object,
                caseService: caseService.Object,
                toDoService: toDoService.Object);

            var parameter = new CreateToDoParameters
            {
                CaseNumber = caseNumber,
                CreateToDoModel = model
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(new CreateToDoParameters
            {
                CaseNumber = caseNumber,
                CreateToDoModel = model
            }));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.InternalServerError, response.StatusCode);
            Assert.AreEqual("Error creating todo item for 123", response.Message);
        }

        [TestMethod]
        public void CreateToDoRequest_ValidCaseAndAssigneeUserEmail_ToDoCreated()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var adminUserService = AdminServiceStub(user);
            var administrationService = new Mock<IAdministrationService>();
            var toDoService = new Mock<IToDoService>();
            var caseService = new Mock<ICaseService>();
            var caseNumber = "123";
            var assigneeUserEmail = "test@test.com";
            var theCase = EntityHelper.Case(casenumber: caseNumber);
            var toDoItem = EntityHelper.ToDoItem();
            var assigneeUser = EntityHelper.UserStub(userId: "123", firstName: "first", lastName: "last", email: assigneeUserEmail);
            var model = new CreateToDoModel
            {
                Title = "Test title",
                AssigneeUserEmail = assigneeUserEmail,
                Description = "Test description",
                DueDate = DateTime.UtcNow
            };

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.Is<string>(c => c == caseNumber)))
                .Returns(theCase)
                .Verifiable();

            administrationService
                .Setup(x => x.GetUserByEmail(It.Is<string>(u => u == model.AssigneeUserEmail)))
                .Returns(assigneeUser)
                .Verifiable();

            toDoService
                .Setup(x => x.CreateManualTodoItem(
                    It.Is<Cases.Case>(c => c.Id == theCase.Id && c.CaseNumber == caseNumber),
                    It.Is<User>(u => u.Id == user.Id),
                    It.IsAny<ToDoItemType>(),
                    It.Is<String>(t => t == model.Title),
                    It.Is<String>(d => d == model.Description),
                    It.Is<DateTime>(d => d == model.DueDate),
                    It.IsAny<ToDoItemPriority>(),
                    It.Is<String>(s => s == assigneeUser.Id)))
                .Returns((ToDoItem)toDoItem)
                .Verifiable();

            var request = new CreateToDoRequest(
                userId: "UserKey",
                adminUserService: adminUserService.Object,
                administrationService: administrationService.Object,
                caseService: caseService.Object,
                toDoService: toDoService.Object);

            var parameter = new CreateToDoParameters
            {
                CaseNumber = caseNumber,
                CreateToDoModel = model
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual("ToDo created successfully", response.Message);
            caseService.Verify();
            toDoService.Verify();
            administrationService.Verify();
        }

        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }

        private Mock<ICaseService> CaseServieStub(Cases.Case cAse = null)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);

            return caseService;
        }

    }
}
