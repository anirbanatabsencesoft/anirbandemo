﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases.Contracts;
using AT.Api.Case.Requests.CaseRelatedContact;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Net;
using Cases = AbsenceSoft.Data.Cases;

namespace AT.Api.Case.Tests.Requests.CaseRelatedContact
{
    [TestClass]
    public class GetCaseRelatedContactRequestTests
    {
        [TestMethod]
        public void GetRelatedContact_NullCaseNumber_ThrowsException()
        {
           //// Arrange
            var parameters = new GetCaseRelatedContactParameters
            {
                CaseNumber = null
            };

           //// Act
            var exception =   Assert.ThrowsException<ApiException>(
                () =>   new GetCaseRelatedContactRequest().Handle(parameters));

           //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Case number is required", exception.Message);
        }

        [TestMethod]
        public void GetRelatedContact_EmptyCaseNumber_ThrowsException()
        {
           //// Arrange
            var parameters = new GetCaseRelatedContactParameters
            {
                CaseNumber = " "
            };

           //// Act
            var exception =   Assert.ThrowsException<ApiException>(
                () =>   new GetCaseRelatedContactRequest().Handle(parameters));

           //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Case number is required", exception.Message);
        }

        [TestMethod]
        public void GetRelatedContact_NoMatchingCase_ThrowsException()
        {
           //// Arrange            
            var caseService = CaseServieStub();

            var request = new GetCaseRelatedContactRequest(caseService.Object);

            var parameter = new GetCaseRelatedContactParameters
            {
                CaseNumber = "772655213"
            };

           //// Act
            var response =   Assert.ThrowsException<ApiException>(() =>
                  request.Handle(parameter));

           //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Case was not found for 772655213", response.Message);
        }

        [TestMethod]
        public void GetRelatedContact_NoContactAvailable_ReturnsEmptyContact()
        {
           //// Arrange      
            var theCase = EntityHelper.Case(contact: null);
            var caseService = CaseServieStub(theCase);

            var request = new GetCaseRelatedContactRequest(caseService.Object);

            var parameter = new GetCaseRelatedContactParameters
            {
                CaseNumber = "772655213"
            };

           //// Act
            var response =   request.Handle(parameter);

           //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
            Assert.AreEqual("No contact found for case", response.Message);
        }

        [TestMethod]
        public void GetRelatedContact_ValidCase_ReturnsContact()
        {
           //// Arrange   

            var employeeContact = EntityHelper.EmployeeContact();
            var theCase = EntityHelper.Case(contact: employeeContact);
            var caseService = CaseServieStub(theCase);

            var request = new GetCaseRelatedContactRequest(caseService.Object);

            var parameter = new GetCaseRelatedContactParameters
            {
                CaseNumber = "772655213"
            };

           //// Act
            var response =   request.Handle(parameter);

           //// Assert
            Assert.IsNotNull(request);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual("Case related contact details", response.Message);
            var result = response.Data;
            Assert.AreEqual("Type01", result.ContactTypeCode);
            Assert.AreEqual(MilitaryStatus.Civilian, result.MilitaryStatus);
            Assert.AreEqual("01-01-1985", result.DateOfBirth.Value.ToString("MM-dd-yyyy"));
            Assert.AreEqual("FirstName01", result.FirstName);
            Assert.AreEqual("LastName01", result.LastName);
        }

        private Mock<ICaseService> CaseServieStub(Cases.Case cAse = null)
        {
            var caseService = new Mock<ICaseService>();

            caseService
                .Setup(x => x.GetCaseByCaseNumber(It.IsAny<string>()))
                .Returns(cAse);

            return caseService;
        }
    }
}
