﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Security;
using AT.Api.Case.Requests.NoteCategories;
using AT.Api.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Net;

namespace AT.Api.Case.Tests.Requests.NoteCategories
{
    [TestClass]
    public class NoteCategoryTest
    {
        
        [TestMethod]
        public void NoteCategoryRequest_NoteCategory_Success()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            var NoteCategoryList = EntityHelper.NoteCategoryList();

            var adminservice = AdminServiceStub(user);

            var request = new GetNoteCategoriesRequest();
            var parameter = new NoteCategoriesParameter();

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            //Assert.AreEqual("EmployeeConversation", response.Data.ToString().Contains("EmployeeConversation"));
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            //Assert.AreEqual(1, response.Data);

            ////// Assert
            //Assert.AreEqual("Unable to find user", response.Message);
            //Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }
        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
           .Setup(x => x.GetUserById(It.Is<string>(userid => userid == "UserId01")))
           .Returns(user).Verifiable();

            return adminService;
        }
    }
}