﻿var runtime = require('./modules/Runtime.js');
var async = require('async');
var AWS = require('aws-sdk');
var dw = require('./modules/DirectoryWatcher');
var Upload = require('./modules/Upload').Upload;
//TODO: Use the Config module to watch and reload configuration at runtime
var config = require('./.config/app-config.json')[runtime()];
var watch = require('./.config/ftp-map.json')[runtime()];
var log4js = require('log4js');

var exit = function (code) {
    process.exit(code || 0);
};
process.on('disconnect', function () { exit(); });
process.on('SIGTERM', function () { exit(); });

// Configuration Logging
var logger = (function (l) {
    l.configure(config.Logging);
    return l.getLogger('default');
})(log4js);

// Handle uncaught exceptions
process.on('uncaughtException', function (e) {
    logger.warn('Uncaught exception: ' + e.message);
    logger.error(e);
});

// Configure AWS
(function (l, c) {
    var awsConfig = c.AWS.Credentials;
    awsConfig.logger = l;
    AWS.config.update(awsConfig);
})(logger, config);

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"; // Avoids DEPTH_ZERO_SELF_SIGNED_CERT error for self-signed certs

// Watch all of our directories
async.each(watch, function (item, callback) {
    if (!item.disabled) {
        item.watcher = function () {
            var me = item;
            logger.info('Starting watching dir "' + me.WatchDirectory + '" for "' + me.Employer + '", matching /' + me.FilePattern + '/' + me.FilePatternOptions);
            var w = dw.DirectoryWatcher(item.WatchDirectory, false);
            w.suppressInitialEvents = false;
            w.start(500);
            w.on('fileAdded', function (fileDetail) {
                var up = Upload(fileDetail.fullPath, config, me);
                up.setLogger(logger);
                if (up.isFileMatch()) {
                    up.logInfo('File found for "' + me.Employer + '": ' + fileDetail.fullPath);
                    return up.ProcessFile(function (err) {
                        if (err)
                            up.logError('Failed to process the file');
                        else
                            up.logInfo('File successfully processed');
                    });
                }
            });
        };
        item.watcher();
    }
    return callback(null);
});