﻿var path = require('path');
var fs = require('fs');
var Guid = require('guid');
var async = require('async');
var AWS = require('aws-sdk');
var mv = require('mv');
var moment = require('moment');
var exec = require('child_process').exec;
var S = require('string');
var util = require('util');
var http = require('http');
var https = require('https');
var nodemailer = require('nodemailer');
var htmlToText = require('nodemailer-html-to-text').htmlToText;

var Upload = function (filePath, config, ftpWatchConfig) {
    var $this = this;
    var $path = filePath;
    var $config = config;
    var $watch = ftpWatchConfig;
    var _instanceGuid = Guid.raw();
    var transporter = null;

    $this.log = null;
    $this.setLogger = function (log) {
        $this.log = log;
    };
    
    var logError = $this.logError = function (err, msg) {
        var disp = (err || '').toString();
        if (msg) disp = msg + ': \n' + disp;
        disp = '[' + _instanceGuid + ']: ' + disp;
        return ($this.log || console).error(disp);
    };
    var logInfo = $this.logInfo = function (msg) {
        if (!msg) return;
        var disp = '[' + _instanceGuid + ']: ' + msg;
        ($this.log || console).info(disp);
    };
    
    var waitNoMod = function (timeout, int, callback) {
        // track whether or not we've exited (helps with crazy call-stack async stuff)
        var exited = false;
        // define a localized callback function wrapper for the callback passed in
        var cb = function (err) {
            // check to ensure we've not already existing the function/routine
            if (exited) {
                return false;
            }
            // Set that we have exited so nothing else invokes our callback (safety first)
            exited = true;
            if (err) {
                logInfo('File stability could not be determined... failed for ' + p);
            }
            else {
                logInfo('File is no longer being modified, let\'s go ahead and proceed');
            }
            return (callback || function () { /*noop*/ })(err);
        };
        // store our local path, so the variable won't get changed while we're looping/waiting
        var p = $path;
        logInfo('Checking for file locks on ' + p);
        // set the interval that each "check" waits before checking mtime again
        //  this is not exact, node.js only has a single call-stack, it's single threaded, so this may
        //  execute after the given interval and that's OK
        var interval = int || 1000;
        // store the last known mtime for the file
        var mtime = null;
        // check how many times our mtime has matched when we've checked
        var nomod = 0;
        // set the timeout local value from the passed in timeout value
        var to = timeout;
        // The minimum timeout is 5 seconds, stop trying to be all stupid, we have to iterate
        //  at least 4 times, at 1 second each, and then who knows when the call stack will allow
        //  our interval callbacks to get executed, so 5 seconds, quit trying to optimize
        //  something that can't be optimized.
        if (to && to < 5000) {
            to = 5000;
        }
        // get our start time when we started this whole check thing, 'cause if we're determining a timeout
        //  we would need to bail if we're past the total number of MS of waiting
        var start = new Date().valueOf();
        // function that determines whether or not we've timed out in our check for no modifications
        //  if a "falsy" or negative value is passed in, then we'll not use any timeout
        var timedOut = function () {
            // If "falsey" or <= 0, then just say no, we've not timed out
            if (!to || to <= 0) {
                return false;
            }
            // see if the current time lapse in MS since UNIX Epoch has passed
            return ((new Date().valueOf()) - start) >= to;
        };
        // This is the recursive check function, it's pretty cool, so respect BOYEE!
        var check = function () {
            // If we've timed out, then bail
            if (timedOut()) {
                return cb('Timeout waiting for file inactivity for ' + p);
            }
            logInfo('Running check for file activity on ' + p);
            return fs.stat(p, function (e, s) {
                // If there is an error, F*** this, bail.
                if (e) {
                    return cb(e);
                }
                // get the current mtime, which may be a Date() or ms integer value
                var myMtime = (s.mtime['valueOf'] ? s.mtime.valueOf() : s.mtime);
                // If the current mtime doesn't match the new mtime
                if (mtime != myMtime) {
                    // Reset the no mod count to zero, 'cause we're starting over again
                    nomod = 0;
                    // Set the last known mod time, duh
                    mtime = myMtime;
                    logInfo('File change detected for ' + p + '; waiting to check again');
                    // Set timeout to our interval ms and call back to our check method
                    return setTimeout(check, interval);
                }
                // if we've matched at least 3 times already and it's still unmodified, then we
                //  should be golden, no write process is that trucking slow
                if (nomod >= 3) {
                    return cb(null);
                }
                logInfo('No change detected for ' + p + '; ' + (3 - nomod).toString() + ' consecutive checks left');
                // Otherwise, just increment our nomod count by 1
                nomod++;
                // Set timeout to our interval ms and call back to our check method
                //  ain't recursion fun ya'll?!
                return setTimeout(check, interval);
            });
        };
        return check();
    };
    
    var EligibilityUpload = {
        Id: null,
        CustomerId: $watch.CustomerId ? $watch.CustomerId : null,
        EmployerId: $watch.EmployerId ? $watch.EmployerId : null,
        FileName: path.basename(filePath),
        FileKey: null,
        Size: 0
    };
    
    var setFileSize = function (callback) {
        logInfo('Setting file size value for ' + $path);
        return fs.stat($path, function (err, stats) {
            if (err) {
                logError(err, 'Error getting file stats for ' + $path);
                return callback(err);
            }
            EligibilityUpload.Size = stats.size;
            logInfo('Successfully set the file size for file located at ' + $path);
            return callback(null);
        });
    };
    
    var generateFileKey = function () {
        return $config.AWS.S3.BasePath + 
            $watch.CustomerId + '/' + 
            $watch.EmployerId + '/' + 
            Guid.raw().replace('-', '') +
            path.extname($path);
    };
    
    var pushFileToS3 = function (callback) {
        if (!$path) {
            logError(err, 'No file to upload to S3, $path is empty, null or nothing');
            return callback(new Error('No file to upload to S3'));
        }
        logInfo('Uploading file "' + $path + '" to S3');
        fs.stat($path, function (err, stats) {

        });
        var s3 = new AWS.S3({ endpoint: $config.AWS.S3.Endpoint || 's3.amazonaws.com', maxRetries: 3 });
        var key = generateFileKey();
        var params = {
            Bucket: $config.AWS.S3.Buckets.Processing,
            ACL: 'private',
            Key: key,
            ContentLength: EligibilityUpload.Size,
            Body: fs.createReadStream($path)
        };
        var ext = path.extname($path);
        switch (ext) {
            case '.txt':
            case '.csv':
                params.ContentEncoding = 'utf8';
                params.ContentType = 'text/csv';
                break;
            default:
                params.ContentEncoding = 'binary';
                params.ContentType = 'application/octet-stream';
                break;
        }
        //see: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property
        var err = null;
        return s3.putObject(params).on('success', function (response) {
            // Success!
            // well.... maybe
            if (response && response.error) {
                err = (response || { error: null }).error || 'Unknown error, data not sent back in S3 response';
                logError(err, 'Error calling putObject to S3');
                return callback(err);
            }
            else {
                EligibilityUpload.FileKey = key;
                logInfo("File uploaded to S3 in bucket '" + $config.AWS.S3.Buckets.Processing + "' as '" + EligibilityUpload.FileKey + "'");
            }
        }).on('error', function (error, response) {
            // Error!
            logError(error, 'Error calling putObject to S3');
            err = error;
        }).on('complete', function (response) {
            // Complete, either way
            logInfo('S3 putObject request complete, it was a huge ' + (err ? 'Failure!' : 'Success!'));
            return callback(err);
        }).send();
    };
    
    var saveToDb = function (callback) {
        if (EligibilityUpload.Id) {
            return callback(null);
        }
        try {
            logInfo('Saving Eligibility upload record to database');
            var cf = $config.Database;
            if (!cf) return callback(new Error('Unable to load Database configuration'));
            var json = JSON.stringify(EligibilityUpload);
            cf.headers['Content-Length'] = Buffer.byteLength(json, 'utf8');
            var isError = false;
            console.log('Calling Database API with ' + json);
            var req = (cf.protocol == 'https:' ? https : http).request(cf, function (res) {
                logInfo("HTTP Status Code from Database API: " + res.statusCode);
                res.setEncoding('utf-8');
                var data = '';
                res.on('data', function (chunk) {
                    data += (chunk || '');
                });
                res.on('end', function () {
                    logInfo(data);
                    if (!isError) {
                        if (res.statusCode != 200) {
                            return callback(new Error(res.statusCode));
                        }
                        data = JSON.parse(data);
                        EligibilityUpload = data;
                        logInfo('Successfully saved Eligibility upload record with Id = ' + EligibilityUpload.Id.toString());
                        return callback(null);
                    }
                    return callback(isError);
                });
            });
            req.on('error', function (e) {
                isError = e;
                console.log('problem with request: ' + e.message);
                return callback(e);
            });
            req.write(json);
            return req.end();
        }
        catch (e) {
            return callback(e);
        }
    };
    
    $this.isFileMatch = function () {
        if (!$watch || !$watch.FilePattern || $watch.FilePattern == '*.*' || $watch.FilePattern == '.*' || $watch.FilePattern == '*') return true;
        if ($watch && $watch.FilePattern) {
            // Determine if the file pattern is a match or not.
            try {
                var reg = new RegExp($watch.FilePattern, $watch.FilePatternOptions || 'g');
                return reg.test(EligibilityUpload.FileName);
            }
            catch (e) {
                $this.logError(e, 'Error parsing file pattern regular expression, "' + $watch.FilePattern + '"');
                return false;
            }
        }
        return false;
    };
    
    var deleteFile = function (callback) {
        logInfo('Attempting to delete the file "' + $path + '"');
        fs.unlink($path, function (err) {
            if (err) {
                logError(err, 'Error deleting the file "' + $path + '"');
                return callback(err);
            }
            return callback(null);
        });
    };
    
    var moveFile = function (callback) {
        if ($watch.MoveTo == false) {
            logInfo('MoveTo specified as false/ignore');
            return callback(null);
        }
        if (!$watch.MoveTo) {
            logInfo('No MoveTo directory specified for the given FTP drop configuration, deleting file instead');
            return deleteFile(callback);
        }
        
        logInfo('Moving file to ' + $watch.MoveTo);
        var dest = path.join($watch.MoveTo, EligibilityUpload.FileName);
        var opt = {
            mkdirp: true,
            clobber: true
        };
        return mv($path, dest, opt, function (err) {
            if (err) {
                logError(err, 'Error moving file');
                return callback(err);
            }
            return callback(null);
        });
    };
    
    var runScript = function (callback) {
        if (!$watch.Script) return callback(null);
        var templateValues = {
            path: $path,
            fullPath: path.resolve(process.cwd(), $path),
            file: path.basename($path),
            ext: path.extname($path),
            fileNoExt: path.basename($path).replace(path.extname($path), ''),
            dir: path.dirname(path.resolve(process.cwd(), $path))
        };
        var runMe = function (script, cb) {
            var cmd = S(script).template(templateValues).toString();
            console.log('Running command: $' + cmd);
            var runOptions = { timeout: $watch.Timeout || 0 };
            return exec(cmd, runOptions, function (err, stdout, stderr) {
                console.log('stderr: ' + (stderr || ''));
                console.log('stdout: ' + (stdout || ''));
                if (err) {
                    logError(err, 'Error running script: ' + cmd);
                    return cb(err);
                }
                console.log('Command successfully completed');
                return cb(null);
            });
        };
        // if this is an array, then run each one in series, otherwise just run the script
        if (util.isArray($watch.Script)) {
            return async.eachSeries($watch.Script, runMe, callback);
        }
        return runMe($watch.Script, callback);
    };
    
    var callAPI = function (callback) {
        var cf = $config.API;
        if (!cf) return callback(new Error('Unable to load API configuration'));
        var model = {
            CustomerId: EligibilityUpload.CustomerId.toString(),
            EmployerId: EligibilityUpload.EmployerId.toString(),
            FileKey: EligibilityUpload.FileKey
        };
        var json = JSON.stringify(model);
        cf.headers['Content-Length'] = Buffer.byteLength(json, 'utf8');
        var isError = false;
        console.log('Calling ParrotELAPI with ' + json);
        var req = http.request(cf, function (res) {
            logInfo("HTTP Status Code from ParrotELAPI: " + res.statusCode);
            if (res.statusCode != 200) {
                return callback(new Error(res.statusCode));
            }
            res.setEncoding('utf-8');
            var data = '';
            res.on('data', function (chunk) {
                data += (chunk || '');
            });
            res.on('end', function () {
                logInfo(data);
                if (!isError) {
                    return callback(null, data);
                }
                return callback(isError);
            });
        });
        req.on('error', function (e) {
            isError = e;
            console.log('problem with request: ' + e.message);
            return callback(e);
        });
        req.write(json);
        return req.end();
    };
    
    /// sends an alert, IF configured
    /// Current supported type is only SMTP
    var sendAlert = function (callback) {
        if (!$watch.SendAlert) {
            return callback(null);
        }

        switch ($watch.SendAlert.Type.toUpperCase()) {
            case "SMTP":
                return sendSMTPAlert(callback);
            break;
            default: 
                logError("The specified type " + $watch.SendAlert.Type + " is not supported.");
                return callback(null);
        }
    };
    
    var sendSMTPAlert = function (callback) {
        logInfo("Sending Email Alert");
        if (!transporter) {
            transporter = nodemailer.createTransport($config.SMTP);
            transporter.use('compile', htmlToText());
        }
        
        fs.readFile($watch.SendAlert.Template, 'utf8', readTemplate(callback)); 
    };
    
    var readTemplate = function (callback){
        return function (error, fileToRead){
            if (error) {
                logError("Unable to read email template: " + error);
                return callback(null);
            }

            var email = transporter.templateSender({
                subject: 'Automated Alert from AbsenceSoft',
                html: fileToRead
            });
            
            var emailInformation = {
                to: $watch.SendAlert.To,
                from: $watch.SendAlert.From,
            };
            
            email(emailInformation, EligibilityUpload, function (error, info) {
                if (error) {
                    logError("Unable to send email:" + error);
                }
                return callback(null);
            });
        }
    }
    
    
    var actions = {
        //see: https://github.com/caolan/async#seriestasks-callback
        EL: function (callback) {
            return async.series([
                /// Alert that the action has started
                sendAlert,
                // Get the file size and set it into our Eligibility upload record
                setFileSize,
                // Push the file to S3, generating the file key and storing it in the cloud
                pushFileToS3,
                // Save our Eligibility Upload record to the database, which is awesome!
                saveToDb,
                // Move the file to the archive/post-processing directory
                moveFile
            ], callback);
        },
        SCRIPT: function (callback) {
            return async.series([
                /// Alert that the action has started
                sendAlert,
                // Run the script
                runScript,
                // Move the file to the archive/post-processing directory
                moveFile
            ], callback);
        },
        API: function (callback) {
            return async.series([
                /// Alert that the action has started
                sendAlert,
                // Get the file size, so when we send it to S3, it won't bomb/timeout
                setFileSize,
                // Push the file to S3, generating the file key and storing it in the cloud
                pushFileToS3,
                // Make a call to the external API passing the API model {CustomerId,EmployerId,FileKey}
                callAPI,
                // Move the file to the archive/post-processing directory
                moveFile
            ], callback);
        }
    };
    
    // HERE IS THE MAGIC!!!!!
    // This is the only publicly available method, 'cause this coordinates all the magic stuff that
    //  has to happen for a file to be processed.
    $this.ProcessFile = function (callback) {
        logInfo('ProcessFile called for file, "' + $path + '"');
        var action = actions[$watch.Action || 'EL'];
        if (!action) {
            return callback(new Error('Action, "' + $watch.Action + '", specified is NOT a valid action'));
        }
        return waitNoMod(null, 1000, function (err) {
            if (err) {
                return callback(err);
            }
            logInfo('Performing the "' + ($watch.Action || 'EL') + '" for file, "' + $path + '"');
            return action(callback);
        });
    };
};

exports.Upload = function (filePath, config, ftpWatchConfig) {
    return new Upload(filePath, config, ftpWatchConfig);
};
