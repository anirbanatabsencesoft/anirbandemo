﻿// Cache the runtime value
var _runtime = null;

module.exports = function () {
    if (_runtime) return _runtime;
    _runtime = (process.argv.length > 2 ? (process.argv[2] || 'Debug') : 'Debug') || 'Debug';
    return _runtime;
};