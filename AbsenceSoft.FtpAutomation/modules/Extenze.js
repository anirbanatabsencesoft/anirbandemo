﻿//===========================================================================
// FILE: Extenze.js
//
// DESCRIPTION:
//    JS object utility and manipulator
//===========================================================================
/**
 * JS Object utility and manipulation library.
 *
 * @name Extenze
 */

var class2type = {
    "[object Boolean]": "boolean",
    "[object Number]": "number",
    "[object String]": "string",
    "[object Function]": "function",
    "[object Array]": "array",
    "[object Date]": "date",
    "[object RegExp]": "regexp",
    "[object Object]": "object",
    "[object Error]": "error"
};

var core_toString = class2type.toString,
    core_hasOwn = class2type.hasOwnProperty;

var _isFunction = exports.isFunction = function (obj) {
    return (typeof obj === "function");
}; //end: isFunction

var _isArray = exports.isArray = Array.isArray;

var _type = exports.type = function (obj) {
    if (obj == null) {
        return String(obj);
    }
    return typeof obj === "object" || typeof obj === "function" ?
        class2type[core_toString.call(obj)] || "object" :
        typeof obj;
}; //end: type

var _isPlainObject = exports.isPlainObject = function (obj) {
    // Not plain objects:
    // - Any object or value whose internal [[Class]] property is not "[object Object]"
    if (_type(obj) !== "object") {
        return false;
    }
    try {
        if (obj.constructor && !core_hasOwn.call(obj.constructor.prototype, "isPrototypeOf")) {
            return false;
        }
    } catch (e) {
        return false;
    }
    // If the function hasn't returned already, we're confident that
    // |obj| is a plain object, created by {} or constructed with new Object
    return true;
}; //end: isPlainObject

/**
 * Totally ripped off jQuery's extend method.
 * see: http://api.jquery.com/jQuery.extend/
 * @returns {*|{}}
 */
var _extend = exports.extend = function () {
    var options, name, src, copy, copyIsArray, clone,
        target = arguments[0] || {},
        i = 1,
        length = arguments.length,
        deep = false;
    
    // Handle a deep copy situation
    if (typeof target === "boolean") {
        deep = target;
        target = arguments[1] || {};
        // skip the boolean and the target
        i = 2;
    }
    
    // Handle case when target is a string or something (possible in deep copy)
    if (typeof target !== "object" && !_isFunction(target)) {
        target = {};
    }
    
    // extend jQuery itself if only one argument is passed
    if (length === i) {
        target = this;
        --i;
    }
    
    for (; i < length; i++) {
        // Only deal with non-null/undefined values
        if ((options = arguments[i]) != null) {
            // Extend the base object
            for (name in options) {
                src = target[name];
                copy = options[name];
                
                // Prevent never-ending loop
                if (target === copy) {
                    continue;
                }
                
                // Recurse if we're merging plain objects or arrays
                if (deep && copy && (_isPlainObject(copy) || (copyIsArray = _isArray(copy)))) {
                    if (copyIsArray) {
                        copyIsArray = false;
                        clone = src && _isArray(src) ? src : [];
                    } else {
                        clone = src && _isPlainObject(src) ? src : {};
                    }
                    
                    // Never move original objects, clone them
                    target[name] = _extend(deep, clone, copy);

                    // Don't bring in undefined values
                } else if (copy !== undefined) {
                    target[name] = copy;
                }
            }
        }
    }
    
    // Return the modified object
    return target;
};