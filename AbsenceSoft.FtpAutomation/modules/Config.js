﻿//===========================================================================
// FILE: Config.js
//
// DESCRIPTION:
//    This file contains the configuration with auto-reload for the application
//      and ftp map configuration file.
//===========================================================================

//
// Includes
//
var fs = require("fs");
var path = require("path");
var Extenze = require("./Extenze");
var Runtime = require("./Runtime.js");

//
// Default configurations, paths, variable names and file names.
//
var defaultBasePath = __dirname + "/../.config/";
var envVariableName = "FTP_AUTOMATION_CONFIG_PATH";
var configFile = "app-config.json";
var ftpMapFile = "ftp-map.json";

/**
 * Type: Configuration; wraps some nice methods around a configuration object
 * @param [config] {*} the optional configuration object to use as a base for this configuration's properties.
 * @constructor
 */
function Configuration(config) {
    // If a configuration was passed in, use the extend method to get a copy of all of its properties into this
    //  configuration object.
    if (config) {
        Extenze.extend(this, config);
    }
} // Configuration

/**
 * Applies a given type name's property overrides from the currently active configuration.
 * @param typeName {String|*} The type name or type instance itself to garner override configuration info for.
 * @returns {Configuration}
 */
Configuration.prototype.applyTypeOverrides = function (typeName) {
    // Scope is fun, don't mess with the 'this'
    var self = this;
    if (typeName) {
        if (typeof typeName !== "string") {
            typeName = typeName.constructor.name;
        }
        if (typeName) {
            var typeConfig = (self['type-overrides'] || {})[typeName];
            if (typeConfig) {
                // Apply and override any properties from the type specific configuration via the property map
                Extenze.extend(true, self, typeConfig);
            }
        }
    }
    return self;
};//applyTypeOverrides


/**
 * Gets configuration data either from a file or from an environment variable.
 * @type {Function}
 * @param {String} fileName Specifies the file name to use for reading the configuration data from file or from an
 *  environment variable.
 * @param {Object} [defaultConfig] The default configuration to use, if any, in place of a successful read operation.
 * @param {Function} [onChange] The onChange event handler to execute in the event the underlying file has changed.
 */
function _getConfig(fileName, defaultConfig, onChange) {
    try {
        if (!fileName)
            return null; // can't help here
        
        if (onChange === undefined && typeof defaultConfig === "function") {
            onChange = defaultConfig;
            defaultConfig = undefined;
        }
        
        // Store our search paths/locations for configuration files
        var searchPaths = [fileName]; // root application directory
        
        if (process.env[envVariableName]) {
            // environment variable directory value
            searchPaths.push(path.join(process.env[envVariableName], fileName));
        }
        // default configuration directory value
        searchPaths.push(path.join(defaultBasePath, fileName));
        
        // For each of our search paths, in order, determine if a config file exists, then read it.
        for (var i = 0, p; p = searchPaths[i++];) {
            // If the configuration file exists
            if (fs.existsSync(p)) {
                // Read the contents of the file
                var contents = fs.readFileSync(p); // helps debugging
                // Parse the contents out, 'cause it should be JSON, to our configuration object
                var loadedConfig = JSON.parse(contents)[Runtime()];
                if (defaultConfig) {
                    // Since we were passed default (yay), we should set any properties of the default that is not
                    //  already in the new config object, smart yeah?
                    loadedConfig = Extenze.extend({}, defaultConfig, loadedConfig);
                }
                
                if (onChange) {
                    var watcher = fs.watch(p, function (event, fname) {
                        console.info("Configuration file changed", p, event, fname);
                        // one time only deal my friend.
                        try { watcher.close(); } catch (e) { console.error("Error closing file watcher", p, e); }
                        onChange();
                    });
                    watcher.on("error", function (err) {
                        console.error("Error in config file watcher", err, p);
                    });
                }
                
                // Return the configuration object instance.
                return loadedConfig;

            } // if exists

        } // for
        
        // Not found, whoops, log it and move on.
        console.log(fileName + " not found. Using defaults. Searched the following paths:\n\t", searchPaths.join("\n\t"));

    } // try
    catch (e) {
        console.error("config.js: Error: " + e);

    } // catch
    
    return defaultConfig;
}; // getConfig


var _cache_activeConfig;
/**
 * Gets the active configuration data either from a file or from an environment variable.
 * @type {Function}
 * @param {String} [fileName="env.json"] Optionally read the configuration from an alternate file name.
 */
var _getActiveConfig = exports.getActiveConfig = function (fileName) {
    if (!_cache_activeConfig) {
        var loadConfig = function () {
            _cache_activeConfig = (_getConfig(fileName || configFile, function () {
                loadConfig();
            }) || {});
        };
        loadConfig();
    }
    
    return new Configuration(JSON.parse(JSON.stringify(_cache_activeConfig)));
}; // _getGlobalConfig



/**
 * Gets my current configuration based on environment, optional type name, section name and file name.
 * @param [typeName] {String} The optional type name of the class calling for its configuration object.
 * @param [name] {String}
 * @param [fileName] {String}
 * @returns {*|{}}
 */
var _getMyConfig = Configuration.prototype.getConfig = exports.getConfig = function (typeName, name, fileName) {
    var config = _getActiveConfig(fileName) || new Configuration();
    // Get our overriding type configuration
    config = config.applyTypeOverrides(typeName);
    if (name) {
        config = config[name];
    }
    return config;
}; // getConfig
