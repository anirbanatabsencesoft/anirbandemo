DROP FUNCTION IF EXISTS PUBLIC.fn_cancel_report_request(bigint);

CREATE OR REPLACE FUNCTION PUBLIC.fn_cancel_report_request(
	rr_id bigint DEFAULT NULL::bigint)
    RETURNS BOOLEAN
    LANGUAGE 'plpgsql'
AS $BODY$
  BEGIN
		UPDATE PUBLIC.report_request rr
		SET lookup_status_id = (SELECT id FROM  lookup WHERE category ='REPORT_REQUEST_STATUS' AND title = 'Canceled')
		WHERE rr.id = rr_id AND lookup_status_id = (SELECT id FROM  lookup WHERE category ='REPORT_REQUEST_STATUS' AND title = 'Pending');
		RETURN TRUE ;
	EXCEPTION WHEN OTHERS THEN 
		RAISE 'Could not cancel report request for request id : %', rr_id USING ERRCODE = 'invalid_parameter_value';
  END  
$BODY$;