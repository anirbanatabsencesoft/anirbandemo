DO
$do$ 
BEGIN  


ALTER TABLE public.Scheduled_Report 
DROP COLUMN If Exists report_class_fqn CASCADE;

ALTER TABLE public.Scheduled_Report 
DROP COLUMN modified_on;

ALTER TABLE public.Scheduled_Report 
Add Column modified_on  timestamp with time zone;

Drop FUNCTION  IF EXISTS public.fn_save_scheduled_report (bigint,text,timestamp with time zone,varchar(120),text,bigint,varchar(24),bigint, varchar(24), bigint,varchar(24), boolean, boolean) ;

CREATE OR REPLACE FUNCTION public.fn_save_scheduled_report
(
    sr_id bigint DEFAULT NULL,
	sr_schedule_info_json text DEFAULT null ,	
	sr_next_run_date timestamp with time zone DEFAULT CURRENT_DATE,
	sr_report_category varchar(120) DEFAULT NULL,
	sr_report_json text DEFAULT NULL ,
	sr_customer_id bigint DEFAULT NULL,
	sr_customer_key varchar(24) DEFAULT NULL,
	sr_created_by_id bigint DEFAULT NULL,
	sr_created_by_key varchar(24) DEFAULT NULL,
	sr_modified_by_id bigint DEFAULT NULL,
	sr_modified_by_key varchar(24) DEFAULT NULL,
	sr_is_active boolean DEFAULT TRUE,
	sr_is_deleted boolean DEFAULT FALSE
)
RETURNS TABLE ( id bigint,
				schedule_info_json text,	
				next_run_date timestamp with time zone,
				report_category varchar(120),				
				report_json text,
				customer_id bigint,
				customer_key varchar(24),
				created_by_id bigint,
				created_by_key varchar(24),
				created_on timestamp with time zone,
				modified_by_id bigint,
				modified_by_key varchar(24),				
				is_active boolean,
				is_deleted boolean,
				modified_on timestamp with time zone
				)
 AS
  $BODY$
  BEGIN
	  IF sr_id IS NULL THEN
	    BEGIN
        	RETURN QUERY
			INSERT INTO public.scheduled_report(schedule_info_json,
											  next_run_date,
											  report_category,
											  report_json,
											  customer_id,
											  customer_key,
											  created_by_id,
											  created_by_key,
											  created_on,
											  modified_by_id,
											  modified_by_key,
											  modified_on,
											  is_active,
											  is_deleted
											  )
									   VALUES(sr_schedule_info_json,
											  sr_next_run_date,
											  sr_report_category,											
											  sr_report_json,
											  sr_customer_id,
											  sr_customer_key,
											  sr_created_by_id,
											  sr_created_by_key,
											  CURRENT_TIMESTAMP,
											  sr_modified_by_id,
											  sr_modified_by_key,
											  CURRENT_TIMESTAMP,
											  sr_is_active,
											  sr_is_deleted
											  )
											RETURNING *;			
			
		END;
	  ELSE
	    BEGIN
        	RETURN QUERY
			UPDATE public.scheduled_report sr
						set schedule_info_json		  = sr_schedule_info_json,						    
						    next_run_date			  = sr_next_run_date,
						    report_category			  = sr_report_category,						 
						    report_json				  = sr_report_json,
						    customer_id				  = sr_customer_id,
						    customer_key			  = sr_customer_key,
						    modified_by_id			  = sr_modified_by_id,
						    modified_by_key			  = sr_modified_by_key,
						    modified_on				  = CURRENT_TIMESTAMP,
						    is_active				  = sr_is_active,
							is_deleted				  = sr_is_deleted
					WHERE sr.id = sr_id 
				RETURNING *;
		
			EXCEPTION WHEN OTHERS THEN 
				RAISE 'Could not update scheduled request for id: %', sr_id USING ERRCODE = 'invalid_parameter_value';    		
		END;        
	  END IF;	
  END
  $BODY$
  LANGUAGE PLPGSQL;
  
 DROP FUNCTION IF EXISTS fn_save_report_request(bigint,bigint,character varying,character varying,text,text,bigint,bigint,character varying,character varying,character varying,character varying) ;
 
CREATE OR REPLACE FUNCTION public.fn_save_report_request(
	rr_id bigint DEFAULT NULL::bigint,
	rr_user_id bigint DEFAULT NULL::bigint,
	rr_user_key character varying(24) DEFAULT NULL::character varying,
	rr_request_type character varying(255) DEFAULT NULL::character varying,
	rr_request_message text DEFAULT NULL::text,
	rr_response_message text DEFAULT NULL::text,
	rr_lookup_status_id bigint DEFAULT NULL::bigint,
	rr_scheduled_report_id bigint DEFAULT NULL::bigint,
	rr_result_max_json_file_path character varying(500) DEFAULT NULL::character varying,
	rr_result_all_json_file_path character varying(500) DEFAULT NULL::character varying,
	rr_result_pdf_file_path character varying(500) DEFAULT NULL::character varying,
	rr_result_csv_file_path character varying(1000) DEFAULT NULL::character varying)
    RETURNS TABLE(id bigint, user_id bigint, user_key character varying(24), request_type character varying(255), request_message text, response_message text, lookup_status_id bigint, scheduled_report_id bigint, created_on timestamp with time zone, modified_on timestamp with time zone, is_completed boolean, result_max_json_file_path character varying(500), result_all_json_file_path character varying(500), result_pdf_file_path character varying(500), result_csv_file_path character varying(1000), is_deleted boolean) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$

  BEGIN
	  IF rr_id IS NULL THEN
	    BEGIN
        	RETURN QUERY
			INSERT INTO public.report_request(user_id,
											  user_key,
											  request_type,
											  request_message,
											  response_message,
											  lookup_status_id,
											  scheduled_report_id,
											  created_on,
											  modified_on,
											  is_completed,
											  result_max_json_file_path,
											  result_all_json_file_path,
											  result_pdf_file_path,
											  result_csv_file_path)
									VALUES (rr_user_id,
											rr_user_key,
											rr_request_type,
											rr_request_message,
											rr_response_message,
											rr_lookup_status_id,
											rr_scheduled_report_id,
											CURRENT_TIMESTAMP,
											CURRENT_TIMESTAMP,
											FALSE,
											result_max_json_file_path,
											result_all_json_file_path,
											result_pdf_file_path,
											result_csv_file_path)
											RETURNING *;
			
			EXCEPTION WHEN OTHERS THEN 
				RAISE 'Could not save report request for request type : %', rr_request_type USING ERRCODE = 'invalid_parameter_value';    
		END;
	  ELSE
	    BEGIN
        	RETURN QUERY
			UPDATE public.report_request rr
						set user_id 				  = rr_user_id,
						    user_key 				  = rr_user_key,
						    request_type			  = rr_request_type,
						    request_message			  = rr_request_message,
						    response_message		  = rr_response_message,
						    lookup_status_id		  = rr_lookup_status_id,
						    scheduled_report_id		  = rr_scheduled_report_id,
						    modified_on				  = CURRENT_TIMESTAMP,
						    result_max_json_file_path = rr_result_max_json_file_path,
						    result_all_json_file_path = rr_result_all_json_file_path,
						    result_pdf_file_path	  = rr_result_pdf_file_path,
						    result_csv_file_path 	  = rr_result_csv_file_path
					WHERE rr.id = rr_id 
				RETURNING *;
		
			EXCEPTION WHEN OTHERS THEN 
				RAISE 'Could not update report request for request type : %', rr_request_type USING ERRCODE = 'invalid_parameter_value';    		
		END;        
	  END IF;	
  END
  
$BODY$;

ALTER FUNCTION public.fn_save_report_request(bigint, bigint, character varying, character varying, text, text, bigint, bigint, character varying, character varying, character varying, character varying)
    OWNER TO postgres;

DROP FUNCTION IF EXISTS public.fn_get_report_request(	 bigint );

CREATE OR REPLACE FUNCTION public.fn_get_report_request(
	rr_id bigint DEFAULT NULL::bigint
	)
    RETURNS TABLE(id bigint, user_id bigint, user_key character varying(24), request_type character varying(255), request_message text, response_message text, lookup_status_id bigint, scheduled_report_id bigint, created_on timestamp with time zone, modified_on timestamp with time zone, is_completed boolean, result_max_json_file_path character varying(500), result_all_json_file_path character varying(500), result_pdf_file_path character varying(500), result_csv_file_path character varying(1000), is_deleted boolean) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$

  BEGIN	  
        RETURN QUERY
        Select rr.id , rr.user_id , rr.user_key , rr.request_type , rr.request_message , rr.response_message , rr.lookup_status_id , rr.scheduled_report_id , rr.created_on , rr.modified_on ,rr.is_completed , rr.result_max_json_file_path , rr.result_all_json_file_path , rr.result_pdf_file_path , rr.result_csv_file_path , rr.is_deleted
		from public.report_request rr where rr.id = rr_id;

        EXCEPTION WHEN OTHERS THEN 
            RAISE 'Could not fetch report request for request Id : %', rr_id USING ERRCODE = 'invalid_parameter_value';    
		 	  
  END
  
$BODY$;

ALTER FUNCTION public.fn_get_report_request(bigint)
    OWNER TO postgres;

Drop function IF EXISTS public.fn_get_scheduled_reports_to_run(
 timestamp with time zone 
);

CREATE OR REPLACE FUNCTION public.fn_get_scheduled_reports_to_run(
rr_run_date timestamp with time zone DEFAULT CURRENT_TIMESTAMP::timestamp with time zone
)  
  RETURNS TABLE ( id bigint,
				schedule_info_json text,	
				next_run_date timestamp with time zone,
				report_category varchar(120),				
				report_json text,
				customer_id bigint,
				customer_key varchar(24),
				created_by_id bigint,
				created_by_key varchar(24),
				created_on timestamp with time zone,
				modified_by_id bigint,
				modified_by_key varchar(24),
				modified_on timestamp with time zone,
				is_active boolean,
				is_deleted boolean
				)
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$

  BEGIN	  
        RETURN QUERY
        SELECT rr.id , rr.schedule_info_json ,	rr.next_run_date ,	rr.report_category , rr.report_json , rr.customer_id , rr.customer_key , 
		rr.created_by_id , rr.created_by_key , rr.created_on ,	rr.modified_by_id , rr.modified_by_key , rr.modified_on , rr.is_active , rr.is_deleted 
		FROM public.scheduled_report rr Where rr.next_run_date <= rr_run_date;

        EXCEPTION WHEN OTHERS THEN 
            RAISE 'Could not fetch scheduled report.';		 	  
  END
  
$BODY$;

ALTER FUNCTION public.fn_get_scheduled_reports_to_run(timestamp with time zone)
    OWNER TO postgres;
	
drop function IF EXISTS public.fn_get_scheduled_report_info(bigint);
	
CREATE OR REPLACE FUNCTION public.fn_get_scheduled_report_info(
	rr_id bigint DEFAULT NULL::bigint
	)
     RETURNS TABLE ( id bigint,
				schedule_info_json text,	
				next_run_date timestamp with time zone,
				report_category varchar(120),				
				report_json text,
				customer_id bigint,
				customer_key varchar(24),
				created_by_id bigint,
				created_by_key varchar(24),
				created_on timestamp with time zone,
				modified_by_id bigint,
				modified_by_key varchar(24),
				modified_on timestamp with time zone,
				is_active boolean,
				is_deleted boolean
				)
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$

  BEGIN	  
        RETURN QUERY
        Select * from public.scheduled_report rr where rr.id = rr_id;

        EXCEPTION WHEN OTHERS THEN 
            RAISE 'Could not fetch scheduled report for Id : %', rr_id USING ERRCODE = 'invalid_parameter_value';    
		 	  
  END
  
$BODY$;

ALTER FUNCTION public.fn_get_scheduled_report_info(bigint)
    OWNER TO postgres;

END 
$do$