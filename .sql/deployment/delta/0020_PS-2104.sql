-- Drop the existing view
DROP VIEW public.vw_notification;

-- Alter table column
ALTER TABLE public.notification ALTER COLUMN subject TYPE varchar;

--Create view again
CREATE OR REPLACE VIEW public.vw_notification AS
 SELECT noti.id,
    noti.customer_id,
    noti.customer_object_id,
    noti.employer_id,
    noti.employer_object_id,
    noti."from",
    noti.subject,
    noti.lookup_message_format_id,
    noti.message,
    noti.lookup_notification_type_id,
    noti.created_date,
    noti.created_by_id,
    noti.created_by_object_id,
    noti.modified_date,
    noti.modified_by_id,
    noti.modified_by_object_id,
    noti.notified_on,
    noti.unique_id,
    noti.message_id,
    noti.no_of_retries,
    noti.allowed_no_of_retries,
    noti.last_retried_on,
    noti.is_archived,
    noti.lookup_notification_status_id,
    noti.failure_reason,
    reci.recipient,
        CASE
            WHEN att.id IS NULL THEN false
            ELSE true
        END AS hasattachment,
    noti.is_deleted
   FROM notification noti
     LEFT JOIN notification_attachment att ON att.notification_id = noti.id
     LEFT JOIN notification_recipient reci ON noti.id = reci.notification_id;

ALTER TABLE public.vw_notification
    OWNER TO postgres;


