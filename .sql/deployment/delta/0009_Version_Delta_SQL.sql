DO 
$do$ 
BEGIN  
--Check DB Version. Scripts to be added for the particular version 
IF NOT EXISTS(SELECT 1 FROM db_version WHERE id >= 9) THEN 

--Drop old function first
DROP FUNCTION IF EXISTS public.fn_get_shared_reports(bigint, character varying, bigint, bigint, character varying, bigint, character varying, bigint, character varying, bigint, character varying);
	
-- Procedure to get shared reports
CREATE OR REPLACE FUNCTION fn_get_shared_reports
(	
	customerid bigint DEFAULT NULL
	,customerkey varchar(24) DEFAULT NULL
	,reportid bigint DEFAULT NULL
	,scheduledreportid bigint DEFAULT NULL
	,teamname character varying (255) DEFAULT NULL
	,teamid bigint DEFAULT NULL
	,teamkey character varying (24) DEFAULT NULL
	,roleid bigint DEFAULT NULL
	,rolename character varying (255) DEFAULT NULL
	,userid bigint DEFAULT NULL
	,userkey character varying (24) DEFAULT NULL
) 
RETURNS TABLE (id bigint,
		report_name varchar(512),
		ui_sequence integer ,
		report_category varchar(120),
		report_class_fqn varchar(512),
		report_json text,
		customer_id bigint,
		customer_key varchar(24),
		shared_with char(4),
		scheduled_report_id bigint,
		created_by_id bigint,
		created_by_key varchar(24),
		created_on timestamp with time zone,
		modified_on timestamp with time zone,
		recipient_id bigint,
		team_name character varying(255),
		team_id bigint,
		team_key character varying(24),
		role_name character varying(255),
		role_id bigint,
		user_key character varying(24),
		user_id bigint,
		request_id bigint,
		json_path varchar(500)
		)
AS 
	$BODY$
		BEGIN
			RETURN QUERY		
				SELECT 
					 sr.id
					,sr.report_name
					,sr.ui_sequence
					,sr.report_category
					,sr.report_class_fqn
					,sr.report_json
					,sr.customer_id
					,sr.customer_key
					,sr.shared_with
					,sr.scheduled_report_id
					,sr.created_by_id
					,sr.created_by_key
					,sr.created_on
					,sr.modified_on
					,srtru.id AS recipient_id
					,srtru.team_name
					,srtru.team_id
					,srtru.team_key
					,srtru.role_name
					,srtru.role_id
					,srtru.user_key
					,srtru.user_id
					,sr.last_request_id
					,rr.result_max_json_file_path
				FROM
					shared_report sr 
					LEFT JOIN shared_report_team_role_user srtru ON sr.id = srtru.shared_report_id
					LEFT JOIN report_request rr ON sr.last_request_id = rr.id
				WHERE
					sr.is_active = True
					AND sr.is_deleted = False
					AND (reportid IS NOT NULL OR sr.shared_with != 'SELF')
					AND (customerid IS NULL OR sr.customer_id = customerid)
					AND (customerkey IS NULL OR sr.customer_key = customerkey)
					AND (reportid IS NULL OR sr.id = reportid)
					AND (teamname IS NULL OR srtru.team_name = teamname)
					AND (teamid IS NULL OR srtru.team_id = teamid)
					AND (teamkey IS NULL OR srtru.team_key = teamkey)
					AND (roleid IS NULL OR srtru.role_id = roleid)
					AND (rolename IS NULL OR srtru.role_name = rolename)
					AND (userid IS NULL OR (srtru.user_id = userid OR sr.created_by_id = userid))
					AND (userkey IS NULL OR (srtru.user_key = userkey OR sr.created_by_key = userkey))
				ORDER BY sr.id DESC;
	END;
$BODY$
LANGUAGE PLPGSQL;

-- This is new so no need to drop
-- Procedure to get saved reports
CREATE OR REPLACE FUNCTION fn_get_saved_operation_reports
(	
	customerid bigint DEFAULT NULL
	,customerkey varchar(24) DEFAULT NULL
	,userid bigint DEFAULT NULL
	,userkey character varying (24) DEFAULT NULL
) 
RETURNS TABLE (id bigint,
		report_name varchar(512),
		ui_sequence integer ,
		report_category varchar(120),
		report_class_fqn varchar(512),
		report_json text,
		customer_id bigint,
		customer_key varchar(24),
		shared_with char(4),
		scheduled_report_id bigint,
		created_by_id bigint,
		created_by_key varchar(24),
		created_on timestamp with time zone,
		modified_on timestamp with time zone,
		recipient_id bigint,
		team_name character varying(255),
		team_id bigint,
		team_key character varying(24),
		role_name character varying(255),
		role_id bigint,
		user_key character varying(24),
		user_id bigint,
		request_id bigint,
		json_path varchar(500)
		)
AS 
	$BODY$
		BEGIN
			RETURN QUERY		
				SELECT 
					 sr.id
					,sr.report_name
					,sr.ui_sequence
					,sr.report_category
					,sr.report_class_fqn
					,sr.report_json
					,sr.customer_id
					,sr.customer_key
					,sr.shared_with
					,sr.scheduled_report_id
					,sr.created_by_id
					,sr.created_by_key
					,sr.created_on
					,sr.modified_on
					,srtru.id AS recipient_id
					,srtru.team_name
					,srtru.team_id
					,srtru.team_key
					,srtru.role_name
					,srtru.role_id
					,srtru.user_key
					,srtru.user_id
					,sr.last_request_id
					,rr.result_max_json_file_path
				FROM
					shared_report sr 
					LEFT JOIN shared_report_team_role_user srtru ON sr.id = srtru.shared_report_id
					LEFT JOIN report_request rr ON sr.last_request_id = rr.id
				WHERE
					sr.is_active = True
					AND sr.is_deleted = False
					AND sr.shared_with = 'SELF'
					AND (customerid IS NULL OR sr.customer_id = customerid)
					AND (customerkey IS NULL OR sr.customer_key = customerkey)
					AND (userid IS NULL OR (srtru.user_id = userid OR sr.created_by_id = userid))
					AND (userkey IS NULL OR (srtru.user_key = userkey OR sr.created_by_key = userkey))
				ORDER BY sr.id DESC;
	END;
$BODY$
LANGUAGE PLPGSQL;

--Update the DB Version to 9
INSERT INTO db_version (id, title, description) VALUES (9, 'Upgrade-VERSION-9', 'Upgrading to version 9'); 
END IF; 
END 
$do$ 
