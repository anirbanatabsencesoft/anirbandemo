DO 
$do$ 
BEGIN  
--Check DB Version. Scripts to be added for the particular version 
IF NOT EXISTS(SELECT 1 FROM db_version WHERE id >= 7) THEN 
-- schemaType: SEQUENCE
-- db1: {at2_dev localhost 5432 postgres postgres sslmode=disable}
-- db2: {at_dev_26_mar_2018 localhost 5432 postgres postgres sslmode=disable}
-- Run the following SQL against db2:
ALTER TABLE shared_report ADD COLUMN IF NOT EXISTS last_request_id bigint;
-- schemaType: VIEW
-- db1: {at2_dev localhost 5432 postgres postgres sslmode=disable}
-- db2: {at_dev_26_mar_2018 localhost 5432 postgres postgres sslmode=disable}
-- Run the following SQL against db2:
-- schemaType: INDEX
-- db1: {at2_dev localhost 5432 postgres postgres sslmode=disable}
-- db2: {at_dev_26_mar_2018 localhost 5432 postgres postgres sslmode=disable}
-- Run the following SQL against db2:
-- Warning, this may drop foreign keys pointing at this column.  Make sure you re-run the FOREIGN_KEY diff after running this SQL.
CREATE INDEX IF NOT EXISTS ixfk_shared_report_report_request ON shared_report USING btree (last_request_id);
-- schemaType: FOREIGN_KEY
-- db1: {at2_dev localhost 5432 postgres postgres sslmode=disable}
-- db2: {at_dev_26_mar_2018 localhost 5432 postgres postgres sslmode=disable}
-- Run the following SQL against db2:
IF NOT EXISTS (select constraint_name 
				from information_schema.constraint_column_usage 
				where constraint_name = 'fk_shared_report_report_request') 
THEN
        ALTER TABLE shared_report ADD CONSTRAINT fk_shared_report_report_request FOREIGN KEY (last_request_id) REFERENCES report_request(id);

END IF;
-- schemaType: TRIGGER
-- db1: {at2_dev localhost 5432 postgres postgres sslmode=disable}
-- db2: {at_dev_26_mar_2018 localhost 5432 postgres postgres sslmode=disable}
-- Run the following SQL against db2:
-- schemaType: FUNCTION
-- db1: {at2_dev localhost 5432 postgres postgres sslmode=disable}
-- db2: {at_dev_26_mar_2018 localhost 5432 postgres postgres sslmode=disable}
-- Run the following SQL against db2:
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.fn_delete_notification(noti_id bigint, noti_modified_by_id bigint)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
    DECLARE
    v_cnt numeric;    
	BEGIN
		UPDATE public.notification
		SET  modified_by_id = noti_modified_by_id
		    ,modified_Date = CURRENT_TIMESTAMP
			,is_deleted = TRUE			
		WHERE Id = noti_id;
		
        GET DIAGNOSTICS v_cnt = ROW_COUNT;
        
		DELETE FROM public.notification_attachment att
		WHERE att.notification_id = noti_id;
		
		DELETE FROM public.notification_recipient rec
		WHERE rec.notification_id = noti_id;
                
		RETURN v_cnt;
		
		EXCEPTION WHEN OTHERS THEN 
			RAISE 'Could not delete notification id : %', noti_id USING ERRCODE = 'invalid_parameter_value';
    
	END;
    $function$;

-- STATEMENT-END
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.fn_delete_report_request(rr_id bigint DEFAULT NULL::bigint)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
    DECLARE
    v_cnt numeric;    
	BEGIN
			UPDATE public.report_request
			SET modified_on = CURRENT_TIMESTAMP
				,is_deleted = TRUE			
			WHERE Id = rr_id;
			
			GET DIAGNOSTICS v_cnt = ROW_COUNT;
			
			RETURN v_cnt;
		COMMIT;

		EXCEPTION WHEN OTHERS THEN 
			BEGIN
				ROLLBACK;
				RAISE 'Could not delete report request id : %', rr_id USING ERRCODE = 'invalid_parameter_value';
			END;
    
	END;
    $function$;

-- STATEMENT-END
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.fn_delete_scheduled_report(sr_id bigint DEFAULT NULL::bigint)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
    DECLARE
    v_cnt numeric;
	BEGIN
			UPDATE public.scheduled_report
			SET modified_on = CURRENT_TIMESTAMP
				,is_deleted = TRUE			
			WHERE Id = sr_id;
						
			GET DIAGNOSTICS v_cnt = ROW_COUNT;
			
			RETURN v_cnt;
		COMMIT;

		EXCEPTION WHEN OTHERS THEN 
			BEGIN
				ROLLBACK;
				RAISE 'Could not delete scheduled report request id : %', sr_id USING ERRCODE = 'invalid_parameter_value';
			END;    
	END;
    $function$;

-- STATEMENT-END
-- Note that CASCADE in the statement below will also drop any triggers depending on this function.
-- Also, if there are two functions with this name, you will need to add arguments to identify the correct one to drop.
-- (See http://www.postgresql.org/docs/9.4/interactive/sql-dropfunction.html) 
DROP FUNCTION fn_delete_shared_report(bigint) CASCADE;
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.fn_delete_shared_report(sr_id bigint, createdbyid bigint DEFAULT NULL::bigint, createdbykey character varying DEFAULT NULL::character varying)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
    DECLARE v_cnt numeric;    
	BEGIN
			UPDATE public.shared_report sr
				SET modified_on = CURRENT_TIMESTAMP
				,is_deleted = TRUE			
			WHERE id = sr_id
			AND (createdbyid IS NULL OR sr.created_by_id = createdbyid) 
			AND (createdbykey IS NULL OR sr.created_by_key = createdbykey)
			AND sr.is_deleted = False;
											
			GET DIAGNOSTICS v_cnt = ROW_COUNT;
			RETURN v_cnt;

			EXCEPTION WHEN OTHERS THEN 
				RAISE 'Could not delete shared report id : %', sr_id USING ERRCODE = 'invalid_parameter_value';
			
    
	END;
    $function$;

-- STATEMENT-END
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.fn_get_notification(notification_id bigint DEFAULT NULL::bigint, noti_customer_id bigint DEFAULT NULL::bigint)
 RETURNS TABLE(id bigint, customer_id bigint, customer_object_id character varying, employer_id bigint, employer_object_id character varying, "from" character varying, subject character varying, lookup_message_format_id bigint, message text, lookup_notification_type_id bigint, created_date timestamp with time zone, created_by_id bigint, created_by_object_id character varying, modified_date timestamp with time zone, modified_by_id bigint, modified_by_object_id character varying, notified_on timestamp with time zone, unique_id character varying, message_id character varying, no_of_retries numeric, allowed_no_of_retries numeric, last_retried_on timestamp with time zone, is_archived boolean, lookup_notification_status_id bigint, failure_reason text)
 LANGUAGE plpgsql
AS $function$
	BEGIN
    	RETURN QUERY
		SELECT  noti.id
        		,noti.customer_id
                ,noti.customer_object_id
                ,noti.employer_id
                ,noti.employer_object_id
			  	,noti."from"
                ,noti.subject
                ,noti.lookup_message_format_id
                ,noti.message
                ,noti.lookup_notification_type_id
                ,noti.created_date
                ,noti.created_by_id
                ,noti.created_by_object_id
                ,noti.modified_date
                ,noti.modified_by_id
                ,noti.modified_by_object_id
                ,noti.notified_on
                ,noti.unique_id
                ,noti.message_id
                ,noti.no_of_retries
                ,noti.allowed_no_of_retries
                ,noti.last_retried_on
                ,noti.is_archived
                ,noti.lookup_notification_status_id
                ,noti.failure_reason				
                FROM public.notification noti
				WHERE (notification_id IS NOT NULL OR noti.is_archived = FALSE) -- AND is_deleted = FALSE 				
				AND (notification_id IS NULL OR noti.id = notification_id) 
				AND (noti_customer_id IS NOT NULL OR noti.customer_id = noti_customer_id)
				AND (coalesce(noti.no_of_retries, 0) <= noti.allowed_no_of_retries);
		EXCEPTION WHEN OTHERS THEN 
			RAISE 'Could not search for given parameters' USING ERRCODE = 'invalid_parameter_value';
	END;
  $function$;

-- STATEMENT-END
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.fn_get_report_request(rr_id bigint DEFAULT NULL::bigint)
 RETURNS TABLE(id bigint, user_id bigint, user_key character varying, request_type character varying, request_message text, response_message text, lookup_status_id bigint, scheduled_report_id bigint, created_on timestamp with time zone, modified_on timestamp with time zone, is_completed boolean, result_max_json_file_path character varying, result_all_json_file_path character varying, result_pdf_file_path character varying, result_csv_file_path character varying, is_deleted boolean)
 LANGUAGE plpgsql
AS $function$

  BEGIN	  
        RETURN QUERY
        Select rr.id , rr.user_id , rr.user_key , rr.request_type , rr.request_message , rr.response_message , rr.lookup_status_id , rr.scheduled_report_id , rr.created_on , rr.modified_on ,rr.is_completed , rr.result_max_json_file_path , rr.result_all_json_file_path , rr.result_pdf_file_path , rr.result_csv_file_path , rr.is_deleted
		from public.report_request rr where rr.id = rr_id;

        EXCEPTION WHEN OTHERS THEN 
            RAISE 'Could not fetch report request for request Id : %', rr_id USING ERRCODE = 'invalid_parameter_value';    
		 	  
  END
  
$function$;

-- STATEMENT-END
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.fn_get_scheduled_report_info(rr_id bigint DEFAULT NULL::bigint)
 RETURNS TABLE(id bigint, schedule_info_json text, next_run_date timestamp with time zone, report_category character varying, report_json text, customer_id bigint, customer_key character varying, created_by_id bigint, created_by_key character varying, created_on timestamp with time zone, modified_by_id bigint, modified_by_key character varying, modified_on timestamp with time zone, is_active boolean, is_deleted boolean)
 LANGUAGE plpgsql
AS $function$

  BEGIN	  
        RETURN QUERY
        Select * from public.scheduled_report rr where rr.id = rr_id;

        EXCEPTION WHEN OTHERS THEN 
            RAISE 'Could not fetch scheduled report for Id : %', rr_id USING ERRCODE = 'invalid_parameter_value';    
		 	  
  END
  
$function$;

-- STATEMENT-END
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.fn_get_scheduled_reports_to_run(rr_run_date timestamp with time zone DEFAULT now())
 RETURNS TABLE(id bigint, schedule_info_json text, next_run_date timestamp with time zone, report_category character varying, report_json text, customer_id bigint, customer_key character varying, created_by_id bigint, created_by_key character varying, created_on timestamp with time zone, modified_by_id bigint, modified_by_key character varying, modified_on timestamp with time zone, is_active boolean, is_deleted boolean)
 LANGUAGE plpgsql
AS $function$

  BEGIN	  
        RETURN QUERY
        SELECT rr.id , rr.schedule_info_json ,	rr.next_run_date ,	rr.report_category , rr.report_json , rr.customer_id , rr.customer_key , 
		rr.created_by_id , rr.created_by_key , rr.created_on ,	rr.modified_by_id , rr.modified_by_key , rr.modified_on , rr.is_active , rr.is_deleted 
		FROM public.scheduled_report rr Where rr.next_run_date <= rr_run_date;

        EXCEPTION WHEN OTHERS THEN 
            RAISE 'Could not fetch scheduled report.';		 	  
  END
  
$function$;

-- STATEMENT-END
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.fn_get_shared_reports(customerid bigint DEFAULT NULL::bigint, customerkey character varying DEFAULT NULL::character varying, reportid bigint DEFAULT NULL::bigint, scheduledreportid bigint DEFAULT NULL::bigint, teamname character varying DEFAULT NULL::character varying, teamid bigint DEFAULT NULL::bigint, teamkey character varying DEFAULT NULL::character varying, roleid bigint DEFAULT NULL::bigint, rolename character varying DEFAULT NULL::character varying, userid bigint DEFAULT NULL::bigint, userkey character varying DEFAULT NULL::character varying)
 RETURNS TABLE(id bigint, report_name character varying, ui_sequence integer, report_category character varying, report_class_fqn character varying, report_json text, customer_id bigint, customer_key character varying, shared_with character, scheduled_report_id bigint, created_by_id bigint, created_by_key character varying, created_on timestamp with time zone, modified_on timestamp with time zone, recipient_id bigint, team_name character varying, team_id bigint, team_key character varying, role_name character varying, role_id bigint, user_key character varying, user_id bigint, request_id bigint, json_path character varying)
 LANGUAGE plpgsql
AS $function$
		BEGIN
			RETURN QUERY		
				SELECT 
					 sr.id
					,sr.report_name
					,sr.ui_sequence
					,sr.report_category
					,sr.report_class_fqn
					,sr.report_json
					,sr.customer_id
					,sr.customer_key
					,sr.shared_with
					,sr.scheduled_report_id
					,sr.created_by_id
					,sr.created_by_key
					,sr.created_on
					,sr.modified_on
					,srtru.id AS recipient_id
					,srtru.team_name
					,srtru.team_id
					,srtru.team_key
					,srtru.role_name
					,srtru.role_id
					,srtru.user_key
					,srtru.user_id
					,sr.last_request_id
					,rr.result_max_json_file_path
				FROM
					shared_report sr 
					LEFT JOIN shared_report_team_role_user srtru ON sr.id = srtru.shared_report_id
					LEFT JOIN report_request rr ON sr.last_request_id = rr.id
				WHERE
					sr.is_active = True
					AND sr.is_deleted = False
					AND sr.shared_with != 'SELF'
					AND (customerid IS NULL OR sr.customer_id = customerid)
					AND (customerkey IS NULL OR sr.customer_key = customerkey)
					AND (reportid IS NULL OR sr.id = reportid)
					AND (teamname IS NULL OR srtru.team_name = teamname)
					AND (teamid IS NULL OR srtru.team_id = teamid)
					AND (teamkey IS NULL OR srtru.team_key = teamkey)
					AND (roleid IS NULL OR srtru.role_id = roleid)
					AND (rolename IS NULL OR srtru.role_name = rolename)
					AND (userid IS NULL OR (srtru.user_id = userid OR sr.created_by_id = userid))
					AND (userkey IS NULL OR (srtru.user_key = userkey OR sr.created_by_key = userkey));
	END;
$function$;

-- STATEMENT-END
-- Note that CASCADE in the statement below will also drop any triggers depending on this function.
-- Also, if there are two functions with this name, you will need to add arguments to identify the correct one to drop.
-- (See http://www.postgresql.org/docs/9.4/interactive/sql-dropfunction.html) 
DROP FUNCTION fn_get_shared_reports(bigint,character varying,bigint,character varying,bigint,character varying,bigint,character varying) CASCADE;
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.fn_save_notification(noti_customer_id bigint DEFAULT NULL::bigint, noti_sender character varying DEFAULT NULL::character varying, noti_lookup_notification_type_id bigint DEFAULT NULL::bigint, noti_created_by_id bigint DEFAULT NULL::bigint, noti_lookup_message_format_id bigint DEFAULT NULL::bigint, noti_id bigint DEFAULT NULL::bigint, noti_customer_object_id character varying DEFAULT NULL::character varying, noti_employer_id bigint DEFAULT NULL::bigint, noti_employer_object_id character varying DEFAULT NULL::character varying, noti_subject character varying DEFAULT NULL::character varying, noti_message text DEFAULT NULL::text, noti_created_by_object_id character varying DEFAULT NULL::character varying, noti_modified_by_id bigint DEFAULT NULL::bigint, noti_modified_by_object_id character varying DEFAULT NULL::character varying, noti_notified_on timestamp with time zone DEFAULT NULL::timestamp with time zone, noti_unique_id character varying DEFAULT NULL::character varying, noti_no_of_retries bigint DEFAULT 3, noti_message_id character varying DEFAULT NULL::character varying, noti_allowed_no_of_retries numeric DEFAULT 3, noti_last_retried_on timestamp without time zone DEFAULT NULL::timestamp without time zone, noti_is_archived boolean DEFAULT false, noti_lookup_notification_status_id bigint DEFAULT NULL::bigint, noti_failure_reason text DEFAULT NULL::text, noti_display_name character varying DEFAULT NULL::character varying)
 RETURNS TABLE(id bigint, customerid bigint, customerobjectid character varying, employerid bigint, employerobjectid character varying, "from" character varying, displayname character varying, subject character varying, lookupmessageformatid bigint, message text, lookupnotificationtypeid bigint, createddate timestamp with time zone, createdbyid bigint, createdbyobjectid character varying, modifieddate timestamp with time zone, modifiedbyid bigint, modifiedbyobjectid character varying, notifiedon timestamp with time zone, uniqueid character varying, messageid character varying, noofretries numeric, allowednoofretries numeric, lastretriedon timestamp with time zone, isarchived boolean, lookupnotificationstatusid bigint, failurereason text, isdeleted boolean)
 LANGUAGE plpgsql
AS $function$
  BEGIN
	  IF noti_id IS NULL THEN
	    BEGIN
        	RETURN QUERY
			INSERT INTO public.notification(
					  customer_id
					 ,customer_object_id
					 ,employer_id
					 ,employer_object_id
					 ,"from"
					 ,display_name
					 ,subject
					 ,lookup_message_format_id
					 ,message
					 ,lookup_notification_type_id
					 ,created_date
					 ,created_by_id
					 ,created_by_object_id
					 ,modified_date
					 ,modified_by_id
					 ,modified_by_object_id
					 ,notified_on
					 ,unique_id
					 ,message_id
					 ,no_of_retries
					 ,allowed_no_of_retries
					 ,last_retried_on
					 ,is_archived
					 ,lookup_notification_status_id
					 ,failure_reason)
			VALUES (noti_customer_id
				,noti_customer_object_id
				,noti_employer_id
				,noti_employer_object_id
				,noti_sender
				,noti_display_name
				,noti_subject
				,noti_lookup_message_format_id
				,noti_message
				,noti_lookup_notification_type_id
				,CURRENT_TIMESTAMP
				,noti_created_by_id
				,noti_created_by_object_id
				,CURRENT_TIMESTAMP
				,noti_modified_by_id
				,noti_modified_by_object_id
				,noti_notified_on
				,noti_unique_id
				,noti_message_id
				,noti_no_of_retries
				,noti_allowed_no_of_retries
				,noti_last_retried_on
				,noti_is_archived
				,noti_lookup_notification_status_id
				,noti_failure_reason)
				RETURNING *;
		EXCEPTION WHEN OTHERS THEN 
	
			RAISE 'Could not save notification for Customer id : %', noti_customer_id USING ERRCODE = 'invalid_parameter_value';
    
		END;
	  ELSE
	    BEGIN
        	RETURN QUERY
			UPDATE public.notification noti
				SET  modified_date = CURRENT_TIMESTAMP
					,modified_by_id = noti_modified_by_id
					,notified_on = noti_notified_on
					,failure_reason = noti_failure_reason
					,lookup_notification_status_id = noti_lookup_notification_status_id
					,message_id = noti_message_id
				WHERE noti.id = noti_id AND noti.customer_id = noti_customer_id
				RETURNING *;
		
		EXCEPTION WHEN OTHERS THEN 
			RAISE 'Could not update notification for Notification id : %', noti_id USING ERRCODE = 'invalid_parameter_value';
    		
		END;        
	  END IF;   
	
	
  END
  $function$;

-- STATEMENT-END
-- This function is different so we'll recreate it:
DROP FUNCTION fn_save_notification_recipient(bigint,character varying,bigint);
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.fn_save_notification_recipient(noti_id bigint, noti_recipient character varying, noti_lookup_recipient_type_id bigint)
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
   DECLARE recipient_id BIGINT;
	  BEGIN
      
        IF EXISTS(SELECT 1 FROM public.notification_recipient rec 
				  WHERE rec.notification_id = noti_id AND rec.recipient = noti_recipient AND rec.lookup_recipient_type_id = noti_lookup_recipient_type_id) THEN
		  BEGIN
			DELETE FROM public.notification_recipient rec 
			WHERE rec.notification_id = noti_id;
		  END;
         END IF;
          
		INSERT INTO public.notification_recipient
						   (notification_id
						   ,recipient
						   ,lookup_recipient_type_id)
					VALUES (noti_id
						   ,noti_recipient
						   ,noti_lookup_recipient_type_id)
					RETURNING id INTO recipient_id;
					RETURN recipient_id;
		
		EXCEPTION WHEN OTHERS THEN 
			RAISE 'Could not save notification recipient for Notification id : %', noti_id USING ERRCODE = 'invalid_parameter_value';
    		
	  END
  $function$;

-- STATEMENT-END
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.fn_save_report_request(rr_id bigint DEFAULT NULL::bigint, rr_user_id bigint DEFAULT NULL::bigint, rr_user_key character varying DEFAULT NULL::character varying, rr_request_type character varying DEFAULT NULL::character varying, rr_request_message text DEFAULT NULL::text, rr_response_message text DEFAULT NULL::text, rr_lookup_status_id bigint DEFAULT NULL::bigint, rr_scheduled_report_id bigint DEFAULT NULL::bigint, rr_result_max_json_file_path character varying DEFAULT NULL::character varying, rr_result_all_json_file_path character varying DEFAULT NULL::character varying, rr_result_pdf_file_path character varying DEFAULT NULL::character varying, rr_result_csv_file_path character varying DEFAULT NULL::character varying)
 RETURNS SETOF report_request
 LANGUAGE plpgsql
AS $function$

  BEGIN
	  IF rr_id IS NULL THEN
	    BEGIN
        	RETURN QUERY
			INSERT INTO public.report_request(user_id,
											  user_key,
											  request_type,
											  request_message,
											  response_message,
											  lookup_status_id,
											  scheduled_report_id,
											  created_on,
											  modified_on,
											  is_completed,
											  result_max_json_file_path,
											  result_all_json_file_path,
											  result_pdf_file_path,
											  result_csv_file_path)
									VALUES (rr_user_id,
											rr_user_key,
											rr_request_type,
											rr_request_message,
											rr_response_message,
											rr_lookup_status_id,
											rr_scheduled_report_id,
											CURRENT_TIMESTAMP,
											CURRENT_TIMESTAMP,
											FALSE,
											rr_result_max_json_file_path,
											rr_result_all_json_file_path,
											rr_result_pdf_file_path,
											rr_result_csv_file_path)
											RETURNING *;
			
			EXCEPTION WHEN OTHERS THEN 
				RAISE 'Could not save report request for request type : %', rr_request_type USING ERRCODE = 'invalid_parameter_value';    
		END;
	  ELSE
	    BEGIN
        	RETURN QUERY
			UPDATE public.report_request rr
						set user_id 				  = rr_user_id,
						    user_key 				  = rr_user_key,
						    request_type			  = rr_request_type,
						    request_message			  = rr_request_message,
						    response_message		  = rr_response_message,
						    lookup_status_id		  = rr_lookup_status_id,
						    scheduled_report_id		  = rr_scheduled_report_id,
						    modified_on				  = CURRENT_TIMESTAMP,
						    result_max_json_file_path = rr_result_max_json_file_path,
						    result_all_json_file_path = rr_result_all_json_file_path,
						    result_pdf_file_path	  = rr_result_pdf_file_path,
						    result_csv_file_path 	  = rr_result_csv_file_path
					WHERE rr.id = rr_id 
				RETURNING *;
		
			EXCEPTION WHEN OTHERS THEN 
				RAISE 'Could not update report request for request type : %', rr_request_type USING ERRCODE = 'invalid_parameter_value';    		
		END;        
	  END IF;	
  END
  
$function$;

-- STATEMENT-END
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.fn_save_scheduled_report(sr_id bigint DEFAULT NULL::bigint, sr_schedule_info_json text DEFAULT NULL::text, sr_next_run_date timestamp with time zone DEFAULT ('now'::text)::date, sr_report_category character varying DEFAULT NULL::character varying, sr_report_json text DEFAULT NULL::text, sr_customer_id bigint DEFAULT NULL::bigint, sr_customer_key character varying DEFAULT NULL::character varying, sr_created_by_id bigint DEFAULT NULL::bigint, sr_created_by_key character varying DEFAULT NULL::character varying, sr_modified_by_id bigint DEFAULT NULL::bigint, sr_modified_by_key character varying DEFAULT NULL::character varying, sr_is_active boolean DEFAULT true, sr_is_deleted boolean DEFAULT false)
 RETURNS TABLE(id bigint, schedule_info_json text, next_run_date timestamp with time zone, report_category character varying, report_json text, customer_id bigint, customer_key character varying, created_by_id bigint, created_by_key character varying, created_on timestamp with time zone, modified_by_id bigint, modified_by_key character varying, is_active boolean, is_deleted boolean, modified_on timestamp with time zone)
 LANGUAGE plpgsql
AS $function$
  BEGIN
	  IF sr_id IS NULL THEN
	    BEGIN
        	RETURN QUERY
			INSERT INTO public.scheduled_report(schedule_info_json,
											  next_run_date,
											  report_category,
											  report_json,
											  customer_id,
											  customer_key,
											  created_by_id,
											  created_by_key,
											  created_on,
											  modified_by_id,
											  modified_by_key,											 
											  is_active,
											  is_deleted,
											   modified_on
											  )
									   VALUES(sr_schedule_info_json,
											  sr_next_run_date,
											  sr_report_category,											
											  sr_report_json,
											  sr_customer_id,
											  sr_customer_key,
											  sr_created_by_id,
											  sr_created_by_key,
											  CURRENT_TIMESTAMP,
											  sr_modified_by_id,
											  sr_modified_by_key,											  
											  sr_is_active,
											  sr_is_deleted,
											  CURRENT_TIMESTAMP
											  )
											RETURNING scheduled_report.id , scheduled_report.schedule_info_json, scheduled_report.next_run_date, scheduled_report.report_category, scheduled_report.report_json , scheduled_report.customer_id , scheduled_report.customer_key, scheduled_report.created_by_id , scheduled_report.created_by_key , scheduled_report.created_on , scheduled_report.modified_by_id , scheduled_report.modified_by_key , scheduled_report.is_active , scheduled_report.is_deleted  , scheduled_report.modified_on;			
			
		END;
	  ELSE
	    BEGIN
        	RETURN QUERY
			UPDATE public.scheduled_report sr
						set schedule_info_json		  = sr_schedule_info_json,						    
						    next_run_date			  = sr_next_run_date,
						    report_category			  = sr_report_category,						 
						    report_json				  = sr_report_json,
						    customer_id				  = sr_customer_id,
						    customer_key			  = sr_customer_key,
						    modified_by_id			  = sr_modified_by_id,
						    modified_by_key			  = sr_modified_by_key,
						    modified_on				  = CURRENT_TIMESTAMP,
						    is_active				  = sr_is_active,
							is_deleted				  = sr_is_deleted
					WHERE sr.id = sr_id 
				RETURNING scheduled_report.id , scheduled_report.schedule_info_json, scheduled_report.next_run_date, scheduled_report.report_category, scheduled_report.report_json , scheduled_report.customer_id , scheduled_report.customer_key, scheduled_report.created_by_id , scheduled_report.created_by_key , scheduled_report.created_on , scheduled_report.modified_by_id , scheduled_report.modified_by_key , scheduled_report.is_active , scheduled_report.is_deleted  , scheduled_report.modified_on;			
		
			EXCEPTION WHEN OTHERS THEN 
				RAISE 'Could not update scheduled request for id: %', sr_id USING ERRCODE = 'invalid_parameter_value';    		
		END;        
	  END IF;	
  END
  $function$;

-- STATEMENT-END
-- Note that CASCADE in the statement below will also drop any triggers depending on this function.
-- Also, if there are two functions with this name, you will need to add arguments to identify the correct one to drop.
-- (See http://www.postgresql.org/docs/9.4/interactive/sql-dropfunction.html) 
DROP FUNCTION fn_save_shared_report(character varying,integer,character varying,text,character,bigint,character varying,bigint,character varying,bigint,character varying,bigint,boolean) CASCADE;
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.fn_save_shared_report(sr_report_name character varying, sr_ui_sequence integer, sr_report_class_fqn character varying, sr_report_json text, sr_shared_with character, sr_created_by_id bigint, sr_created_by_key character varying, sr_id bigint DEFAULT NULL::bigint, sr_report_category character varying DEFAULT NULL::character varying, sr_customer_id bigint DEFAULT NULL::bigint, sr_customer_key character varying DEFAULT NULL::character varying, sr_scheduled_report_id bigint DEFAULT NULL::bigint, sr_is_active boolean DEFAULT true, sr_last_requested_id bigint DEFAULT NULL::bigint)
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
  DECLARE l_id BIGINT;
  BEGIN
	  IF sr_id IS NULL THEN
	    BEGIN
			INSERT INTO public.shared_report(report_name, 
											 ui_sequence, 
											 report_category, 
											 report_class_fqn, 
											 report_json, 
											 customer_id, 
											 customer_key, 
											 shared_with, 
											 scheduled_report_id,
											 created_by_id, 
											 created_by_key, 
											 created_on, 
											 modified_on, 
											 is_active,
											 last_request_id)
									VALUES (sr_report_name, 
											sr_ui_sequence, 
											sr_report_category, 
											sr_report_class_fqn, 
											sr_report_json, 
											sr_customer_id, 
											sr_customer_key, 
											sr_shared_with, 
											sr_scheduled_report_id,
											sr_created_by_id, 
											sr_created_by_key, 
											CURRENT_TIMESTAMP, 
											CURRENT_TIMESTAMP, 
											TRUE,
											sr_last_requested_id)
											RETURNING id INTO l_id;
			
		END;
	  ELSE
	    BEGIN
			UPDATE public.shared_report sr
						set  report_name 		= sr_report_name, 
							 ui_sequence 		= sr_ui_sequence, 
							 report_category 	= sr_report_category, 
							 report_class_fqn 	= sr_report_class_fqn, 
							 report_json 		= sr_report_json, 
							 customer_id 		= sr_customer_id, 
							 customer_key 		= sr_customer_key, 
							 scheduled_report_id = sr_scheduled_report_id,
							 shared_with 		= sr_shared_with, 
							 modified_on 		= CURRENT_TIMESTAMP, 
							 is_active 			= sr_is_active,
							 last_request_id	= sr_last_requested_id
					WHERE sr.id = sr_id 
				RETURNING id INTO l_id;
			
		END;        
	  END IF;	
	  
	  RETURN l_id;
  END
  $function$;

-- STATEMENT-END
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
DROP FUNCTION IF EXISTS fn_save_shared_report_team_role_user(bigint,character varying,bigint,character varying,bigint,character varying,bigint,character varying);
CREATE OR REPLACE FUNCTION public.fn_save_shared_report_team_role_user(srtru_shared_report_id bigint, srtru_team_name character varying DEFAULT NULL::character varying, srtru_team_id bigint DEFAULT NULL::bigint, srtru_team_key character varying DEFAULT NULL::character varying, srtru_role_id bigint DEFAULT NULL::bigint, srtru_role_name character varying DEFAULT NULL::character varying, srtru_user_id bigint DEFAULT NULL::bigint, srtru_user_key character varying DEFAULT NULL::character varying)
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
	DECLARE l_id integer;
		BEGIN
			--Delete all from child first
			DELETE FROM shared_report_team_role_user WHERE shared_report_id = srtru_shared_report_id;

			--Now insert as fresh
			INSERT INTO public.shared_report_team_role_user(shared_report_id, team_name, team_id, team_key, role_id, role_name, user_id, user_key)
													VALUES (srtru_shared_report_id, srtru_team_name, srtru_team_id, srtru_team_key, srtru_role_id, srtru_role_name, srtru_user_id, srtru_user_key)
											RETURNING id INTO l_id;
			RETURN l_id; 
			
			EXCEPTION WHEN OTHERS THEN 
				RAISE 'Could not save shared report team role user with Report name : %', srtru_shared_report_id USING ERRCODE = 'invalid_parameter_value';    
		
		END
  $function$;

-- STATEMENT-END
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
DROP FUNCTION IF EXISTS fn_search_notification(bigint,bigint,character varying,character varying,boolean,character varying,text,boolean,bigint,character varying);
CREATE OR REPLACE FUNCTION public.fn_search_notification(notification_id bigint DEFAULT NULL::bigint, noti_customer_id bigint DEFAULT NULL::bigint, noti_sender character varying DEFAULT NULL::character varying, noti_recipient character varying DEFAULT NULL::character varying, noti_hasattachment boolean DEFAULT NULL::boolean, noti_subject_contains character varying DEFAULT NULL::character varying, noti_message_contains text DEFAULT NULL::text, noti_include_archive boolean DEFAULT NULL::boolean, noti_lookup_notification_status_id bigint DEFAULT NULL::bigint, noti_customer_object_id character varying DEFAULT NULL::character varying)
 RETURNS TABLE(id bigint, customerid bigint, customerkey character varying, employerid bigint, employerobjectid character varying, "from" character varying, recipients text, subject character varying, lookupmessageformatid bigint, message text, lookupnotificationtypeid bigint, createddate timestamp with time zone, createdbyid bigint, createdbyobjectid character varying, modifieddate timestamp with time zone, modifiedbyid bigint, modifiedbyobjectid character varying, notifiedon timestamp with time zone, uniqueid character varying, messageid character varying, noofretries numeric, allowednoofretries numeric, lastretriedon timestamp with time zone, isarchived boolean, lookupnotificationstatusid bigint, failurereason text)
 LANGUAGE plpgsql
AS $function$
	BEGIN
		IF noti_subject_contains IS NOT NULL THEN
			noti_subject_contains := concat('%', noti_subject_contains, '%');
		END IF;
		
		IF noti_message_contains IS NOT NULL THEN
			noti_message_contains := concat('%', noti_message_contains, '%');
		END IF;
	
    	RETURN QUERY
		SELECT  noti.id
        		,noti.customer_id
                ,noti.customer_object_id
                ,noti.employer_id
                ,noti.employer_object_id
			  	,noti."from"
				,noti.recipient
                ,noti.subject
                ,noti.lookup_message_format_id
                ,noti.message
                ,noti.lookup_notification_type_id
                ,noti.created_date
                ,noti.created_by_id
                ,noti.created_by_object_id
                ,noti.modified_date
                ,noti.modified_by_id
                ,noti.modified_by_object_id
                ,noti.notified_on
                ,noti.unique_id
                ,noti.message_id
                ,noti.no_of_retries
                ,noti.allowed_no_of_retries
                ,noti.last_retried_on
                ,noti.is_archived
                ,noti.lookup_notification_status_id
                ,noti.failure_reason				
                FROM public.vw_notification noti
				WHERE (noti_include_archive IS NULL OR noti.is_archived = noti_include_archive) AND is_deleted = FALSE
				AND (notification_id IS NULL OR noti.id = notification_id) 
				AND (noti_customer_id IS NULL OR noti.customer_id = noti_customer_id)
				AND (noti_customer_object_id IS NULL OR noti.customer_object_id = noti_customer_object_id)
				AND (noti_sender IS NULL OR noti."from" = noti_sender)
				AND (noti_recipient IS NULL OR noti.recipient = noti_recipient)
				AND (noti_hasattachment IS NULL OR (noti.hasattachment = noti_hasattachment))
				AND (noti_subject_contains IS NULL OR (LOWER(noti.subject) LIKE LOWER(noti_subject_contains)))
				AND (noti_message_contains IS NULL OR (LOWER(noti.message) LIKE LOWER(noti_message_contains)))
				AND (noti_lookup_notification_status_id IS NULL OR noti.lookup_notification_status_id = noti_lookup_notification_status_id)
				ORDER BY noti.id;
		EXCEPTION WHEN OTHERS THEN 
			RAISE 'Could not search Notification for given parameters' USING ERRCODE = 'invalid_parameter_value';
    		
	END;
  $function$;

-- STATEMENT-END

-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.fn_update_notification(id bigint, modified_by_id bigint, last_retried_on timestamp without time zone, message_id character varying, notified_on character varying DEFAULT NULL::character varying, is_archived boolean DEFAULT false, failure_reason text DEFAULT NULL::text, is_deleted boolean DEFAULT false)
 RETURNS void
 LANGUAGE plpgsql
AS $function$
	BEGIN
		UPDATE public.notification noti
		SET  noti.notified_on = notified_on
			,noti.modified_by_id = modified_by_id
			,noti.no_of_retries = noti.no_of_retries + 1
			,noti.last_retried_on = CURRENT_TIMESTAMP
			,noti.is_archived = is_archived
			,noti.failure_reason = failure_reason
			,noti.is_deleted = is_deleted
			,noti.message_id = message_id
		WHERE noti.ID = id;
		
		EXCEPTION WHEN OTHERS THEN 
			RAISE 'Could not update notification for Notification id : %', id USING ERRCODE = 'invalid_parameter_value';
    
	END;
  $function$;

-- STATEMENT-END
--Update the DB Version to 5 
INSERT INTO db_version (id, title, description) VALUES (7, 'Upgrade-VERSION-7', 'Upgrading to version 7'); 
END IF; 
END 
$do$ 
