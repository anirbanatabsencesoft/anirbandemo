
INSERT INTO lookup_category (category, description) SELECT 'REPORT_REQUEST_STATUS', 'Define the SQL report execution status. Lookup Range: 6301-6399' WHERE NOT EXISTS (SELECT 1 FROM lookup_category WHERE category = 'REPORT_REQUEST_STATUS');

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 6301, 'REPORT_REQUEST_STATUS','Pending', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 6301);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 6302, 'REPORT_REQUEST_STATUS','Processing', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 6302);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 6303, 'REPORT_REQUEST_STATUS','Complete', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 6303);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 6304, 'REPORT_REQUEST_STATUS','Canceled', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 6304);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 6305, 'REPORT_REQUEST_STATUS','Failed', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 6305);
