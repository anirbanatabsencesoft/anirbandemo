

--fn_save_report_request

DROP FUNCTION If EXISTS public.fn_save_report_request(bigint, bigint, character varying, character varying, text, text, bigint, bigint, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.fn_save_report_request(
	rr_id bigint DEFAULT NULL::bigint,
	rr_user_id bigint DEFAULT NULL::bigint,
	rr_user_key character varying(24) DEFAULT NULL::character varying,
	rr_request_type character varying(255) DEFAULT NULL::character varying,
	rr_request_message text DEFAULT NULL::text,
	rr_response_message text DEFAULT NULL::text,
	rr_lookup_status_id bigint DEFAULT NULL::bigint,
	rr_scheduled_report_id bigint DEFAULT NULL::bigint,
	rr_result_max_json_file_path character varying(500) DEFAULT NULL::character varying,
	rr_result_all_json_file_path character varying(500) DEFAULT NULL::character varying,
	rr_result_pdf_file_path character varying(500) DEFAULT NULL::character varying,
	rr_result_csv_file_path character varying(1000) DEFAULT NULL::character varying)
    RETURNS SETOF report_request	
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$

  BEGIN
	  IF rr_id IS NULL THEN
	    BEGIN
        	RETURN QUERY
			INSERT INTO public.report_request(user_id,
											  user_key,
											  request_type,
											  request_message,
											  response_message,
											  lookup_status_id,
											  scheduled_report_id,
											  created_on,
											  modified_on,
											  is_completed,
											  result_max_json_file_path,
											  result_all_json_file_path,
											  result_pdf_file_path,
											  result_csv_file_path)
									VALUES (rr_user_id,
											rr_user_key,
											rr_request_type,
											rr_request_message,
											rr_response_message,
											rr_lookup_status_id,
											rr_scheduled_report_id,
											CURRENT_TIMESTAMP,
											CURRENT_TIMESTAMP,
											FALSE,
											rr_result_max_json_file_path,
											rr_result_all_json_file_path,
											rr_result_pdf_file_path,
											rr_result_csv_file_path)
											RETURNING *;
			
			EXCEPTION WHEN OTHERS THEN 
				RAISE 'Could not save report request for request type : %', rr_request_type USING ERRCODE = 'invalid_parameter_value';    
		END;
	  ELSE
	    BEGIN
        	RETURN QUERY
			UPDATE public.report_request rr
						set user_id 				  = rr_user_id,
						    user_key 				  = rr_user_key,
						    request_type			  = rr_request_type,
						    request_message			  = rr_request_message,
						    response_message		  = rr_response_message,
						    lookup_status_id		  = rr_lookup_status_id,
						    scheduled_report_id		  = rr_scheduled_report_id,
						    modified_on				  = CURRENT_TIMESTAMP,
						    result_max_json_file_path = rr_result_max_json_file_path,
						    result_all_json_file_path = rr_result_all_json_file_path,
						    result_pdf_file_path	  = rr_result_pdf_file_path,
						    result_csv_file_path 	  = rr_result_csv_file_path
					WHERE rr.id = rr_id 
				RETURNING *;
		
			EXCEPTION WHEN OTHERS THEN 
				RAISE 'Could not update report request for request type : %', rr_request_type USING ERRCODE = 'invalid_parameter_value';    		
		END;        
	  END IF;	
  END
  
$BODY$;

ALTER FUNCTION public.fn_save_report_request(bigint, bigint, character varying, character varying, text, text, bigint, bigint, character varying, character varying, character varying, character varying)
    OWNER TO postgres;


--fn_save_scheduled_report

DROP  FUNCTION if Exists public.fn_save_scheduled_report(bigint, timestamp with time zone, bigint, timestamp with time zone, character varying, character varying, text, bigint, character varying, bigint, character varying, bigint, character varying, boolean);

Drop FUNCTION  if exists public.fn_save_scheduled_report (bigint,text,timestamp with time zone,varchar(120),text,bigint,varchar(24),bigint, varchar(24), bigint,varchar(24), boolean, boolean) ;


CREATE OR REPLACE FUNCTION public.fn_save_scheduled_report
(
    sr_id bigint DEFAULT NULL,
	sr_schedule_info_json text DEFAULT null ,	
	sr_next_run_date timestamp with time zone DEFAULT CURRENT_DATE,
	sr_report_category varchar(120) DEFAULT NULL,
	sr_report_json text DEFAULT NULL ,
	sr_customer_id bigint DEFAULT NULL,
	sr_customer_key varchar(24) DEFAULT NULL,
	sr_created_by_id bigint DEFAULT NULL,
	sr_created_by_key varchar(24) DEFAULT NULL,
	sr_modified_by_id bigint DEFAULT NULL,
	sr_modified_by_key varchar(24) DEFAULT NULL,
	sr_is_active boolean DEFAULT TRUE,
	sr_is_deleted boolean DEFAULT FALSE
)
RETURNS TABLE(id bigint , 
schedule_info_json text , 
next_run_date timestamp with time zone,
report_category character varying(120),
report_json text ,
customer_id bigint,
customer_key character varying(24) ,
created_by_id bigint,
created_by_key character varying(24),
created_on timestamp with time zone,
modified_by_id bigint,
modified_by_key character varying(24) ,
is_active boolean ,
is_deleted boolean ,
modified_on timestamp with time zone)

LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
 AS
  $BODY$
  BEGIN
	  IF sr_id IS NULL THEN
	    BEGIN
        	RETURN QUERY
			INSERT INTO public.scheduled_report(schedule_info_json,
											  next_run_date,
											  report_category,
											  report_json,
											  customer_id,
											  customer_key,
											  created_by_id,
											  created_by_key,
											  created_on,
											  modified_by_id,
											  modified_by_key,											 
											  is_active,
											  is_deleted,
											   modified_on
											  )
									   VALUES(sr_schedule_info_json,
											  sr_next_run_date,
											  sr_report_category,											
											  sr_report_json,
											  sr_customer_id,
											  sr_customer_key,
											  sr_created_by_id,
											  sr_created_by_key,
											  CURRENT_TIMESTAMP,
											  sr_modified_by_id,
											  sr_modified_by_key,											  
											  sr_is_active,
											  sr_is_deleted,
											  CURRENT_TIMESTAMP
											  )
											RETURNING scheduled_report.id , scheduled_report.schedule_info_json, scheduled_report.next_run_date, scheduled_report.report_category, scheduled_report.report_json , scheduled_report.customer_id , scheduled_report.customer_key, scheduled_report.created_by_id , scheduled_report.created_by_key , scheduled_report.created_on , scheduled_report.modified_by_id , scheduled_report.modified_by_key , scheduled_report.is_active , scheduled_report.is_deleted  , scheduled_report.modified_on;			
			
		END;
	  ELSE
	    BEGIN
        	RETURN QUERY
			UPDATE public.scheduled_report sr
						set schedule_info_json		  = sr_schedule_info_json,						    
						    next_run_date			  = sr_next_run_date,
						    report_category			  = sr_report_category,						 
						    report_json				  = sr_report_json,
						    customer_id				  = sr_customer_id,
						    customer_key			  = sr_customer_key,
						    modified_by_id			  = sr_modified_by_id,
						    modified_by_key			  = sr_modified_by_key,
						    modified_on				  = CURRENT_TIMESTAMP,
						    is_active				  = sr_is_active,
							is_deleted				  = sr_is_deleted
					WHERE sr.id = sr_id 
				RETURNING scheduled_report.id , scheduled_report.schedule_info_json, scheduled_report.next_run_date, scheduled_report.report_category, scheduled_report.report_json , scheduled_report.customer_id , scheduled_report.customer_key, scheduled_report.created_by_id , scheduled_report.created_by_key , scheduled_report.created_on , scheduled_report.modified_by_id , scheduled_report.modified_by_key , scheduled_report.is_active , scheduled_report.is_deleted  , scheduled_report.modified_on;			
		
			EXCEPTION WHEN OTHERS THEN 
				RAISE 'Could not update scheduled request for id: %', sr_id USING ERRCODE = 'invalid_parameter_value';    		
		END;        
	  END IF;	
  END
  $BODY$;  
  
  ALTER FUNCTION public.fn_save_scheduled_report (bigint,text,timestamp with time zone,varchar(120),text,bigint,varchar(24),bigint, varchar(24), bigint,varchar(24), boolean, boolean)
    OWNER TO postgres;