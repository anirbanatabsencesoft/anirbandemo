DO 
$do$ 
BEGIN 
-- FUNCTION: public.fn_get_scheduled_reports_to_run(timestamp with time zone)

DROP FUNCTION public.fn_get_scheduled_reports_to_run(timestamp with time zone);

CREATE OR REPLACE FUNCTION public.fn_get_scheduled_reports_to_run(
	rr_run_date timestamp with time zone DEFAULT CURRENT_TIMESTAMP)
    RETURNS TABLE(id bigint, schedule_info_json text, next_run_date timestamp with time zone, report_category character varying, report_json text, customer_id bigint, customer_key character varying, created_by_id bigint, created_by_key character varying, created_on timestamp with time zone, modified_by_id bigint, modified_by_key character varying, modified_on timestamp with time zone, is_active boolean, is_deleted boolean) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$

  BEGIN	  
        RETURN QUERY
        SELECT rr.id , rr.schedule_info_json ,	rr.next_run_date ,	rr.report_category , rr.report_json , rr.customer_id , rr.customer_key , 
		rr.created_by_id , rr.created_by_key , rr.created_on ,	rr.modified_by_id , rr.modified_by_key , rr.modified_on , rr.is_active , rr.is_deleted 
		FROM public.scheduled_report rr Where rr.next_run_date <= rr_run_date and rr.is_active=true and rr.is_deleted=false;

        EXCEPTION WHEN OTHERS THEN 
            RAISE 'Could not fetch scheduled report.';		 	  
  END
  

$BODY$;

ALTER FUNCTION public.fn_get_scheduled_reports_to_run(timestamp with time zone)
    OWNER TO postgres;


	

Drop FUNCTION  if exists public.fn_save_scheduled_report (bigint,text,timestamp with time zone,varchar(120),text,bigint,varchar(24),bigint, varchar(24), bigint,varchar(24), boolean, boolean) ;


CREATE OR REPLACE FUNCTION public.fn_save_scheduled_report
(
    sr_id bigint DEFAULT NULL,
	sr_schedule_info_json text DEFAULT null ,	
	sr_next_run_date timestamp with time zone DEFAULT CURRENT_DATE,
	sr_report_category varchar(120) DEFAULT NULL,
	sr_report_json text DEFAULT NULL ,
	sr_customer_id bigint DEFAULT NULL,
	sr_customer_key varchar(24) DEFAULT NULL,
	sr_created_by_id bigint DEFAULT NULL,
	sr_created_by_key varchar(24) DEFAULT NULL,
	sr_modified_by_id bigint DEFAULT NULL,
	sr_modified_by_key varchar(24) DEFAULT NULL,
	sr_is_active boolean DEFAULT TRUE,
	sr_is_deleted boolean DEFAULT FALSE
)
RETURNS TABLE(id bigint , 
schedule_info_json text , 
next_run_date timestamp with time zone,
report_category character varying(120),
report_json text ,
customer_id bigint,
customer_key character varying(24) ,
created_by_id bigint,
created_by_key character varying(24),
created_on timestamp with time zone,
modified_by_id bigint,
modified_by_key character varying(24) ,
is_active boolean ,
is_deleted boolean ,
modified_on timestamp with time zone)

LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
 AS
  $BODY$
  BEGIN
	  IF sr_id IS NULL THEN
	    BEGIN
        	RETURN QUERY
			INSERT INTO public.scheduled_report(schedule_info_json,
											  next_run_date,
											  report_category,
											  report_json,
											  customer_id,
											  customer_key,
											  created_by_id,
											  created_by_key,
											  created_on,
											  modified_by_id,
											  modified_by_key,											 
											  is_active,
											  is_deleted,
											   modified_on
											  )
									   VALUES(sr_schedule_info_json,
											  sr_next_run_date,
											  sr_report_category,											
											  sr_report_json,
											  sr_customer_id,
											  sr_customer_key,
											  sr_created_by_id,
											  sr_created_by_key,
											  CURRENT_TIMESTAMP,
											  sr_modified_by_id,
											  sr_modified_by_key,											  
											  sr_is_active,
											  sr_is_deleted,
											  CURRENT_TIMESTAMP
											  )
											RETURNING scheduled_report.id , scheduled_report.schedule_info_json, scheduled_report.next_run_date, scheduled_report.report_category, scheduled_report.report_json , scheduled_report.customer_id , scheduled_report.customer_key, scheduled_report.created_by_id , scheduled_report.created_by_key , scheduled_report.created_on , scheduled_report.modified_by_id , scheduled_report.modified_by_key , scheduled_report.is_active , scheduled_report.is_deleted  , scheduled_report.modified_on;			
			
		END;
	  ELSE
	    BEGIN
        	RETURN QUERY
			UPDATE public.scheduled_report 
						set schedule_info_json		  = sr_schedule_info_json,						    
						    next_run_date			  = sr_next_run_date,
						    report_category			  = sr_report_category,						 
						    report_json				  = sr_report_json,
						    customer_id				  = sr_customer_id,
						    customer_key			  = sr_customer_key,
						    modified_by_id			  = sr_modified_by_id,
						    modified_by_key			  = sr_modified_by_key,
						    modified_on				  = CURRENT_TIMESTAMP,
						    is_active				  = sr_is_active,
							is_deleted				  = sr_is_deleted
					WHERE scheduled_report.id = sr_id 
				RETURNING scheduled_report.id , scheduled_report.schedule_info_json, scheduled_report.next_run_date, scheduled_report.report_category, scheduled_report.report_json , scheduled_report.customer_id , scheduled_report.customer_key, scheduled_report.created_by_id , scheduled_report.created_by_key , scheduled_report.created_on , scheduled_report.modified_by_id , scheduled_report.modified_by_key , scheduled_report.is_active , scheduled_report.is_deleted  , scheduled_report.modified_on;			
		
			EXCEPTION WHEN OTHERS THEN 
				RAISE 'Could not update scheduled request for id: %', sr_id USING ERRCODE = 'invalid_parameter_value';    		
		END;        
	  END IF;	
  END
  $BODY$;  
  
  ALTER FUNCTION public.fn_save_scheduled_report (bigint,text,timestamp with time zone,varchar(120),text,bigint,varchar(24),bigint, varchar(24), bigint,varchar(24), boolean, boolean)
    OWNER TO postgres;
	
END
$do$ 