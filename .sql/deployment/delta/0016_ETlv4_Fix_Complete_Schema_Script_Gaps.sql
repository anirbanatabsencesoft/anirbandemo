-- schemaType: SEQUENCE
-- db1: {at2_dev absencesoft-pg-dev.cw4ffsf8w7ev.us-west-2.rds.amazonaws.com 5432 postgres !Complexdb sslmode=disable}
-- db2: {at2_qa absencesoft-pg-qa.cw4ffsf8w7ev.us-west-2.rds.amazonaws.com 5432 postgres !Complexdb sslmode=disable}
-- Run the following SQL against db2:
-- schemaType: TABLE
-- db1: {at2_dev absencesoft-pg-dev.cw4ffsf8w7ev.us-west-2.rds.amazonaws.com 5432 postgres !Complexdb sslmode=disable}
-- db2: {at2_qa absencesoft-pg-qa.cw4ffsf8w7ev.us-west-2.rds.amazonaws.com 5432 postgres !Complexdb sslmode=disable}
-- Run the following SQL against db2:
-- schemaType: COLUMN
-- db1: {at2_dev absencesoft-pg-dev.cw4ffsf8w7ev.us-west-2.rds.amazonaws.com 5432 postgres !Complexdb sslmode=disable}
-- db2: {at2_qa absencesoft-pg-qa.cw4ffsf8w7ev.us-west-2.rds.amazonaws.com 5432 postgres !Complexdb sslmode=disable}
-- Run the following SQL against db2:
ALTER TABLE notification ALTER COLUMN customer_id DROP NOT NULL;
-- schemaType: VIEW
-- db1: {at2_dev absencesoft-pg-dev.cw4ffsf8w7ev.us-west-2.rds.amazonaws.com 5432 postgres !Complexdb sslmode=disable}
-- db2: {at2_qa absencesoft-pg-qa.cw4ffsf8w7ev.us-west-2.rds.amazonaws.com 5432 postgres !Complexdb sslmode=disable}
-- Run the following SQL against db2:
-- schemaType: INDEX
-- db1: {at2_dev absencesoft-pg-dev.cw4ffsf8w7ev.us-west-2.rds.amazonaws.com 5432 postgres !Complexdb sslmode=disable}
-- db2: {at2_qa absencesoft-pg-qa.cw4ffsf8w7ev.us-west-2.rds.amazonaws.com 5432 postgres !Complexdb sslmode=disable}
-- Run the following SQL against db2:
-- schemaType: FOREIGN_KEY
-- db1: {at2_dev absencesoft-pg-dev.cw4ffsf8w7ev.us-west-2.rds.amazonaws.com 5432 postgres !Complexdb sslmode=disable}
-- db2: {at2_qa absencesoft-pg-qa.cw4ffsf8w7ev.us-west-2.rds.amazonaws.com 5432 postgres !Complexdb sslmode=disable}
-- Run the following SQL against db2:
-- schemaType: TRIGGER
-- db1: {at2_dev absencesoft-pg-dev.cw4ffsf8w7ev.us-west-2.rds.amazonaws.com 5432 postgres !Complexdb sslmode=disable}
-- db2: {at2_qa absencesoft-pg-qa.cw4ffsf8w7ev.us-west-2.rds.amazonaws.com 5432 postgres !Complexdb sslmode=disable}
-- Run the following SQL against db2:
-- schemaType: FUNCTION
-- db1: {at2_dev absencesoft-pg-dev.cw4ffsf8w7ev.us-west-2.rds.amazonaws.com 5432 postgres !Complexdb sslmode=disable}
-- db2: {at2_qa absencesoft-pg-qa.cw4ffsf8w7ev.us-west-2.rds.amazonaws.com 5432 postgres !Complexdb sslmode=disable}
-- Run the following SQL against db2:
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.fn_comn_get_lookup_categories()
 RETURNS TABLE(category character varying, description character varying)
 LANGUAGE sql
AS $function$ 
	SELECT lc.category, lc.description FROM lookup_category lc WHERE lc.is_active = True;
$function$
;

-- STATEMENT-END
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.fn_comn_log(message character varying, process_info character varying DEFAULT ''::character varying, trace text DEFAULT ''::text, log_type bigint DEFAULT 103, app_type bigint DEFAULT 201, version character varying DEFAULT '2.0'::character varying)
 RETURNS void
 LANGUAGE plpgsql
AS $function$
BEGIN
      INSERT INTO log
      (
		lookup_log_type_id
		, lookup_app_type_id
		, version
		, process_info
		, message
		, trace
	)
	VALUES 
	(
		log_type
		, app_type
		, version
		, process_info
		, message
		, trace
	);

	exception when others then 
		raise notice 'Unable to log';
		raise notice '% %', SQLERRM, SQLSTATE;
END;
$function$
;
-- STATEMENT-END
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.fn_save_notification_attachment(noti_id bigint, file_name character varying, file_content bytea, attachment_header character varying)
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$  
	DECLARE attachment_id BIGINT;
	  BEGIN
		IF EXISTS(SELECT 1 FROM public.notification_attachment att WHERE att.notification_id = noti_id) THEN
		  BEGIN
			DELETE FROM public.notification_attachment att 
			WHERE att.notification_id = noti_id;
		  END;
		END IF;
		  
		INSERT INTO public.notification_attachment
							(notification_id
							,file_name
							,file_content
							,attachment_header)
					VALUES ( noti_id
							,file_name
							,file_content
							,attachment_header)
					RETURNING id INTO attachment_id;
					RETURN attachment_id;
		
		EXCEPTION WHEN OTHERS THEN 
			RAISE 'Could not save notification attachment for Notification id : %', noti_id USING ERRCODE = 'invalid_parameter_value';
    		
	  END
  $function$
;
-- STATEMENT-END
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.fn_save_person(first_name character varying, middle_name character varying, last_name character varying, lookup_gender_id bigint, id bigint DEFAULT NULL::bigint, date_of_birth date DEFAULT NULL::date, ssn character varying DEFAULT NULL::character varying, custom_field_value text DEFAULT NULL::text, is_active boolean DEFAULT true, is_deleted boolean DEFAULT false)
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
BEGIN
   IF id IS NULL THEN
     BEGIN
		INSERT INTO person(first_name, middle_name, last_name, date_of_birth, ssn, lookup_gender_id, custom_field_value, is_active, is_deleted)
					VALUES(first_name, middle_name, last_name, date_of_birth, ssn, lookup_gender_id, custom_field_value, is_active, is_deleted)
					RETURNING id;
	 END;
   ELSE
     BEGIN
		UPDATE person p
		SET p.first_name = first_name, p.middle_name = middle_name, p.last_name = last_name, p.date_of_birth = date_of_birth, p.ssn = ssn, 
			p.lookup_gender_id = lookup_gender_id, p.custom_field_value = custom_field_value, p.is_active = is_active, p.is_deleted = is_deleted
		WHERE p.id = id RETURNING id;
	 END;
   END IF;   
END
$function$
;
-- STATEMENT-END
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.fn_util_authenticate(login_email character varying, user_password character varying)
 RETURNS TABLE(password_must_change boolean, failed_login_attempts numeric, last_failed_login timestamp without time zone, first_name character varying, middle_name character varying, last_name character varying, date_of_birth date, customer_id bigint, customer_name character varying, employer_id bigint, employer_name character varying)
 LANGUAGE plpgsql
AS $function$
  -- delcare variables to reduce DB ound trips
  DECLARE user_login_id BIGINT; password_must_change BOOLEAN; last_failed_login TIMESTAMP; failed_login_attempts NUMERIC;
  

  BEGIN
	--get the id for login
		SELECT user_login_id = ul.id, password_must_change = ul.password_must_chage, last_failed_login = ul.last_failed_login, failed_login_attempts = ul.failed_login_attempts
	    FROM public.user_login ul 
		WHERE ul.login_email = ul.login_email AND ul.password = user_password AND ul.is_active = TRUE;

	--If no data found
	IF user_login_id IS NULL THEN
		RAISE 'Invalid username/password for User: %', login_email USING ERRCODE = 'invalid_authorization_specification';
	END IF;
	
	RETURN QUERY
		SELECT password_must_change, failed_login_attempts, last_failed_login, first_name, middle_name, last_name, date_of_birth, u.customer_id, customer_name, employer_id, employer_name
		FROM fn_util_get_user_details(null, null, user_login_id);
	
  END;
$function$
;
-- STATEMENT-END
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.fn_util_get_user_details(user_id bigint DEFAULT NULL::bigint, email text DEFAULT NULL::text, user_login_id bigint DEFAULT NULL::bigint)
 RETURNS TABLE(first_name character varying, middle_name character varying, last_name character varying, date_of_birth date, customer_id bigint, customer_name character varying, employer_id bigint, employer_name character varying)
 LANGUAGE plpgsql
AS $function$
BEGIN	
	IF user_id IS NULL AND email IS NOT NULL THEN
	  BEGIN
		SELECT u.user_id INTO user_id FROM user u WHERE u.email = email LIMIT 1;
	  END;
	END IF; 
	  
	IF user_id IS NULL AND user_login_id IS NOT NULL THEN
	  BEGIN
		SELECT u.user_id INTO user_id FROM user u WHERE u.user_login_id = user_login_id;
	  END;	
	END IF;
  
    RETURN QUERY   SELECT p.first_name, p.middle_name, p.last_name, p.date_of_birth, u.customer_id, c.name  AS customer_name, u.employer_id, e.name AS employer_name
                    FROM person p
                    INNER JOIN public.user u on p.id = u.person_id
                    INNER JOIN employer e on e.id = u.employer_id
                    INNER JOIN customer c on c.id = u.customer_id
                    WHERE u.user_id = user_id;
END;
$function$
;
-- STATEMENT-END
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.fn_util_save_temp_data(cache_key character varying, company_id bigint, employer_id bigint, user_id bigint, value text, active_till timestamp without time zone)
 RETURNS void
 LANGUAGE sql
AS $function$
 	INSERT INTO temp_data(cache_key, company_id, employer_id, user_id, value, active_from, active_to)
    			VALUES (cache_key, company_id, employer_id, user_id, value, CURRENT_TIMESTAMP, active_till);
$function$
;
