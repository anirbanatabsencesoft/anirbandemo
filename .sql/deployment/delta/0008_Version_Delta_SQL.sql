DO 
$do$ 
BEGIN  
--Check DB Version. Scripts to be added for the particular version 
IF NOT EXISTS(SELECT 1 FROM db_version WHERE id >= 8) THEN 
	-- STATEMENT-BEGIN
	DROP FUNCTION IF EXISTS fn_save_shared_report_team_role_user(bigint,character varying,bigint,character varying,bigint,character varying,bigint,character varying);
	CREATE OR REPLACE FUNCTION public.fn_save_shared_report_team_role_user(srtru_shared_report_id bigint, srtru_team_name character varying DEFAULT NULL::character varying, srtru_team_id bigint DEFAULT NULL::bigint, srtru_team_key character varying DEFAULT NULL::character varying, srtru_role_id bigint DEFAULT NULL::bigint, srtru_role_name character varying DEFAULT NULL::character varying, srtru_user_id bigint DEFAULT NULL::bigint, srtru_user_key character varying DEFAULT NULL::character varying)
	 RETURNS bigint
	 LANGUAGE plpgsql
	AS $function$
		DECLARE l_id integer;
			BEGIN
				--Now insert as fresh
				INSERT INTO public.shared_report_team_role_user(shared_report_id, team_name, team_id, team_key, role_id, role_name, user_id, user_key)
														VALUES (srtru_shared_report_id, srtru_team_name, srtru_team_id, srtru_team_key, srtru_role_id, srtru_role_name, srtru_user_id, srtru_user_key)
												RETURNING id INTO l_id;
				RETURN l_id; 
				
				EXCEPTION WHEN OTHERS THEN 
					RAISE 'Could not save shared report team role user with Report name : %', srtru_shared_report_id USING ERRCODE = 'invalid_parameter_value';    
			
			END
	  $function$;

	-- STATEMENT-END
	
ALTER TABLE shared_report ALTER COLUMN shared_with TYPE char(4);
ALTER TABLE shared_report ALTER COLUMN shared_with SET NOT NULL;

--Update the DB Version to 8
INSERT INTO db_version (id, title, description) VALUES (8, 'Upgrade-VERSION-8', 'Upgrading to version 8'); 
END IF; 
END 
$do$ 
