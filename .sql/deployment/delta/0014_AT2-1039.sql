DO 
$do$ 
BEGIN 

ALTER TABLE report_request 
	ADD IF NOT EXISTS report_name varchar(255),
	ADD IF NOT EXISTS criteria_plain_english text,
	ADD IF NOT EXISTS report_id varchar(50);


--Drop Save Report function
DROP FUNCTION IF EXISTS public.fn_save_report_request(bigint, bigint, character varying, character varying, text, text, bigint, bigint, character varying, character varying, character varying, character varying);

--create
CREATE OR REPLACE FUNCTION public.fn_save_report_request(
	rr_id bigint DEFAULT NULL::bigint,
	rr_user_id bigint DEFAULT NULL::bigint,
	rr_user_key character varying(24) DEFAULT NULL::character varying,
	rr_request_type character varying(255) DEFAULT NULL::character varying,
	rr_request_message text DEFAULT NULL::text,
	rr_response_message text DEFAULT NULL::text,
	rr_lookup_status_id bigint DEFAULT NULL::bigint,
	rr_scheduled_report_id bigint DEFAULT NULL::bigint,
	rr_result_max_json_file_path character varying(500) DEFAULT NULL::character varying,
	rr_result_all_json_file_path character varying(500) DEFAULT NULL::character varying,
	rr_result_pdf_file_path character varying(500) DEFAULT NULL::character varying,
	rr_result_csv_file_path character varying(1000) DEFAULT NULL::character varying,
	rr_report_name character varying(255) DEFAULT NULL::character varying,
	rr_criteria_plain_english text DEFAULT NULL::text,
	rr_report_id character varying(50) DEFAULT NULL::character varying
	)
    RETURNS SETOF report_request	
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$

  BEGIN
	  IF rr_id IS NULL THEN
	    BEGIN
        	RETURN QUERY
			INSERT INTO public.report_request(user_id,
											  user_key,
											  request_type,
											  request_message,
											  response_message,
											  lookup_status_id,
											  scheduled_report_id,
											  created_on,
											  modified_on,
											  is_completed,
											  result_max_json_file_path,
											  result_all_json_file_path,
											  result_pdf_file_path,
											  result_csv_file_path,
											  report_name,
											  criteria_plain_english,
											  report_id)
									VALUES (rr_user_id,
											rr_user_key,
											rr_request_type,
											rr_request_message,
											rr_response_message,
											rr_lookup_status_id,
											rr_scheduled_report_id,
											CURRENT_TIMESTAMP,
											CURRENT_TIMESTAMP,
											FALSE,
											rr_result_max_json_file_path,
											rr_result_all_json_file_path,
											rr_result_pdf_file_path,
											rr_result_csv_file_path,
											rr_report_name,
											rr_criteria_plain_english,
											rr_report_id)
											RETURNING *;
			
			EXCEPTION WHEN OTHERS THEN 
				RAISE 'Could not save report request for request type : %', rr_request_type USING ERRCODE = 'invalid_parameter_value';    
		END;
	  ELSE
	    BEGIN
        	RETURN QUERY
			UPDATE public.report_request rr
						set user_id 				  = rr_user_id,
						    user_key 				  = rr_user_key,
						    request_type			  = rr_request_type,
						    request_message			  = rr_request_message,
						    response_message		  = rr_response_message,
						    lookup_status_id		  = rr_lookup_status_id,
						    scheduled_report_id		  = rr_scheduled_report_id,
						    modified_on				  = CURRENT_TIMESTAMP,
						    result_max_json_file_path = rr_result_max_json_file_path,
						    result_all_json_file_path = rr_result_all_json_file_path,
						    result_pdf_file_path	  = rr_result_pdf_file_path,
						    result_csv_file_path 	  = rr_result_csv_file_path
					WHERE rr.id = rr_id 
					AND (rr_user_id IS NULL OR rr.user_id = rr_user_id)
					AND (rr_user_key IS NULL OR rr.user_key = rr_user_key)
				RETURNING *;
		
			EXCEPTION WHEN OTHERS THEN 
				RAISE 'Could not update report request for request type : %', rr_request_type USING ERRCODE = 'invalid_parameter_value';    		
		END;        
	  END IF;	
  END
  
$BODY$;

--Drop report request function
DROP FUNCTION IF EXISTS public.fn_get_report_request(bigint);

--replace get function
CREATE OR REPLACE FUNCTION public.fn_get_report_request(
	rr_id bigint DEFAULT NULL::bigint,
	rr_user_id bigint DEFAULT NULL::bigint,
	rr_user_key character varying (24) DEFAULT NULL::character varying
	)
    RETURNS TABLE(id bigint
	, user_id bigint
	, user_key character varying(24)
	, request_type character varying(255)
	, request_message text, response_message text
	, lookup_status_id bigint
	, scheduled_report_id bigint
	, created_on timestamp with time zone
	, modified_on timestamp with time zone
	, is_completed boolean
	, result_max_json_file_path character varying(500)
	, result_all_json_file_path character varying(500)
	, result_pdf_file_path character varying(500)
	, result_csv_file_path character varying(1000)
	, is_deleted boolean
	, report_name varchar(255)
	, criteria_plain_english text
	, report_id varchar(50)) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$

  BEGIN	  
        RETURN QUERY
        Select rr.id
				, rr.user_id
				, rr.user_key
				, rr.request_type
				, rr.request_message
				, rr.response_message
				, rr.lookup_status_id
				, rr.scheduled_report_id
				, rr.created_on
				, rr.modified_on 
				, rr.is_completed 
				, rr.result_max_json_file_path 
				, rr.result_all_json_file_path 
				, rr.result_pdf_file_path 
				, rr.result_csv_file_path 
				, rr.is_deleted
				, rr.report_name
				, rr.criteria_plain_english
				, rr.report_id
		FROM public.report_request rr 
		WHERE rr.id = rr_id
		AND (rr_user_id IS NULL OR rr.user_id = rr_user_id)
		AND (rr_user_key IS NULL OR rr.user_key = rr_user_key);

        EXCEPTION WHEN OTHERS THEN 
            RAISE 'Could not fetch report request for request Id : %', rr_id USING ERRCODE = 'invalid_parameter_value';    
		 	  
  END
  
$BODY$;

--drop get user report request
DROP FUNCTION IF EXISTS public.fn_get_user_report_request(bigint, character varying, integer);

--create again
CREATE OR REPLACE FUNCTION public.fn_get_user_report_request(
	rr_user_id bigint DEFAULT NULL::bigint,
	rr_user_key character varying DEFAULT NULL::character varying,
	rr_limit integer DEFAULT 10,
	rr_queued_request_only boolean DEFAULT False)
    RETURNS TABLE(id bigint
	, user_id bigint
	, user_key character varying
	, request_type character varying
	, request_message text
	, response_message text
	, lookup_status_id bigint
	, scheduled_report_id bigint
	, created_on timestamp with time zone
	, modified_on timestamp with time zone
	, is_completed boolean
	, result_max_json_file_path character varying
	, result_all_json_file_path character varying
	, result_pdf_file_path character varying
	, result_csv_file_path character varying
	, is_deleted boolean
	, report_name varchar(255)
	, criteria_plain_english text
	, report_id varchar(50)) 
	 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$

  BEGIN	  
        RETURN QUERY
        Select 
			rr.id 
			, rr.user_id 
			, rr.user_key 
			, rr.request_type 
			, rr.request_message 
			, rr.response_message 
			, rr.lookup_status_id 
			, rr.scheduled_report_id 
			, rr.created_on 
			, rr.modified_on 
			, rr.is_completed 
			, rr.result_max_json_file_path 
			, rr.result_all_json_file_path 
			, rr.result_pdf_file_path 
			, rr.result_csv_file_path 
			, rr.is_deleted
			, rr.report_name
			, rr.criteria_plain_english
			, rr.report_id
		FROM public.report_request rr  
		WHERE 	(rr_user_id IS NULL OR rr.user_id = rr_user_id)
			AND (rr_user_key IS NULL OR rr.user_key = rr_user_key) 
			AND rr.is_deleted = False
			AND (CASE WHEN rr_queued_request_only = True THEN rr.lookup_status_id <=6302 ELSE rr.lookup_status_id > 6302 END)
			
		ORDER BY rr.id DESC
		LIMIT rr_limit;
		
        EXCEPTION WHEN OTHERS THEN 
            RAISE 'Could not fetch report request for user_key : %', rr_user_key USING ERRCODE = 'invalid_parameter_value';    
		 	  
  END
  

$BODY$;

--create index
CREATE INDEX IF NOT EXISTS report_request_search_index
  ON public.report_request
  USING btree
  (user_id, user_key, is_deleted, lookup_status_id);
	
END
$do$ 