CREATE OR REPLACE VIEW vw_notification
AS
  SELECT  noti.id
        		,noti.customer_id
                ,noti.customer_object_id
                ,noti.employer_id
                ,noti.employer_object_id
			  	,noti."from"
                ,noti.subject
                ,noti.lookup_message_format_id
                ,noti.message
                ,noti.lookup_notification_type_id
                ,noti.created_date
                ,noti.created_by_id
                ,noti.created_by_object_id
                ,noti.modified_date
                ,noti.modified_by_id
                ,noti.modified_by_object_id
                ,noti.notified_on
                ,noti.unique_id
                ,noti.message_id
                ,noti.no_of_retries
                ,noti.allowed_no_of_retries
                ,noti.last_retried_on
                ,noti.is_archived
                ,noti.lookup_notification_status_id
                ,noti.failure_reason
				,reci.recipient
				,(CASE WHEN att.Id IS NULL THEN FALSE ELSE TRUE END) hasattachment				
				,noti.is_deleted
                FROM public.notification noti
				LEFT JOIN public.notification_attachment att on att.notification_id = noti.id
                LEFT JOIN public.notification_recipient reci on noti.id = reci.notification_id;
                