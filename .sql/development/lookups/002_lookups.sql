﻿/* Lookup SQL */
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 101, 'LOG_TYPE','Info', NULL, 'Informative logs.', 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 101);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 102, 'LOG_TYPE','Debug', NULL, 'Debug informations', 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 102);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 103, 'LOG_TYPE','Error', NULL, 'Error Information', 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 103);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 104, 'LOG_TYPE','Fatal', NULL, 'Application Fatal Error Info', 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 104);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 201, 'APP_TYPE','Web App', NULL, 'The web application front end', 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 201);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 202, 'APP_TYPE','Web Service', NULL, 'API', 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 202);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 203, 'APP_TYPE','CRON', NULL, 'Scheduled Tasks', 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 203);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 204, 'APP_TYPE','ESS', NULL, 'Employee Self Service', 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 204);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 205, 'APP_TYPE','Administration', NULL, 'Admin App', 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 205);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 206, 'APP_TYPE','Others', NULL, NULL, 6, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 206);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 301, 'TYPE_OF_ENTITY','Absence Reason', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 301);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 302, 'TYPE_OF_ENTITY','Accommodation', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 302);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 303, 'TYPE_OF_ENTITY','Case', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 303);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 304, 'TYPE_OF_ENTITY','Contacts', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 304);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 305, 'TYPE_OF_ENTITY','Customer', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 305);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 306, 'TYPE_OF_ENTITY','Demand', NULL, NULL, 6, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 306);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 307, 'TYPE_OF_ENTITY','Employee', NULL, NULL, 7, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 307);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 308, 'TYPE_OF_ENTITY','Employer', NULL, NULL, 8, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 308);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 309, 'TYPE_OF_ENTITY','Template', NULL, NULL, 9, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 309);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 310, 'TYPE_OF_ENTITY','ToDo', NULL, NULL, 10, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 310);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 311, 'TYPE_OF_ENTITY','User', NULL, NULL, 11, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 311);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 312, 'TYPE_OF_ENTITY','Person', NULL, NULL, 12, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 312);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 501, 'APP_FEATURE','ADA', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 501);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 502, 'APP_FEATURE','ShortTermDisability', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 502);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 503, 'APP_FEATURE','MultiEmployerAccess', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 503);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 504, 'APP_FEATURE','GuidelinesData', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 504);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 505, 'APP_FEATURE','PolicyConfiguration', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 505);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 506, 'APP_FEATURE','CommunicationConfiguration', NULL, NULL, 6, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 506);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 507, 'APP_FEATURE','EmployeeSelfService', NULL, NULL, 7, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 507);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 508, 'APP_FEATURE','LOA', NULL, NULL, 8, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 508);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 509, 'APP_FEATURE','ShortTermDisablityPay', NULL, NULL, 9, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 509);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 510, 'APP_FEATURE','ESSDataVisibilityInPortal', NULL, NULL, 10, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 510);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 511, 'APP_FEATURE','WorkflowConfiguration', NULL, NULL, 11, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 511);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 512, 'APP_FEATURE','CaseReporter', NULL, NULL, 12, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 512);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 513, 'APP_FEATURE','WorkRelatedReporting', NULL, NULL, 13, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 513);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 514, 'APP_FEATURE','IPRestrictions', NULL, NULL, 14, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 514);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 515, 'APP_FEATURE','EmployeeConsults', NULL, NULL, 15, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 515);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 516, 'APP_FEATURE','InquiryCases', NULL, NULL, 16, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 516);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 517, 'APP_FEATURE','OrgDataVisibility', NULL, NULL, 17, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 517);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 518, 'APP_FEATURE','JSA', NULL, NULL, 18, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 518);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 519, 'APP_FEATURE','CustomContent', NULL, NULL, 19, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 519);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 520, 'APP_FEATURE','AdHocReporting', NULL, NULL, 20, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 520);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 521, 'APP_FEATURE','EmployerContact', NULL, NULL, 21, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 521);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 522, 'APP_FEATURE','EmployerServiceOptions', NULL, NULL, 22, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 522);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 523, 'APP_FEATURE','OshaReporting', NULL, NULL, 23, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 523);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 524, 'APP_FEATURE','RiskProfile', NULL, NULL, 24, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 524);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 525, 'APP_FEATURE','JobConfiguration', NULL, NULL, 25, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 525);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 526, 'APP_FEATURE','WorkRestriction', NULL, NULL, 26, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 526);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 527, 'APP_FEATURE','BusinessIntelligenceReport', NULL, NULL, 27, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 527);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 528, 'APP_FEATURE','ContactPreferences', NULL, NULL, 28, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 528);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 529, 'APP_FEATURE','AdminPaySchedules', NULL, NULL, 29, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 529);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 530, 'APP_FEATURE','AdminPhysicalDemands', NULL, NULL, 30, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 530);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 531, 'APP_FEATURE','AdminCustomFields', NULL, NULL, 31, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 531);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 532, 'APP_FEATURE','AdminNecessities', NULL, NULL, 32, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 532);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 533, 'APP_FEATURE','AdminNoteCategories', NULL, NULL, 33, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 533);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 534, 'APP_FEATURE','AdminConsultations', NULL, NULL, 34, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 534);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 535, 'APP_FEATURE','AdminAssignTeams', NULL, NULL, 35, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 535);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 536, 'APP_FEATURE','AdminOrganizations', NULL, NULL, 36, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 536);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 537, 'APP_FEATURE','AdminDataUpload', NULL, NULL, 37, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 537);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 538, 'APP_FEATURE','AdminEmployers', NULL, NULL, 38, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 538);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 539, 'APP_FEATURE','AdminSecuritySettings', NULL, NULL, 39, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 539);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 540, 'APP_FEATURE','AccommodationTypeCategories', NULL, NULL, 40, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 540);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 541, 'APP_FEATURE','AdjustEmployeeStartDayOfWeek', NULL, NULL, 41, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 541);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 542, 'APP_FEATURE','SingleSignOn', NULL, NULL, 42, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 542);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 543, 'APP_FEATURE','PolicyCrosswalk', NULL, NULL, 43, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 543);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 544, 'APP_FEATURE','CaseBulkDownload', NULL, NULL, 44, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 544);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 701, 'ADDRESS_TYPE','Home Address', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 701);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 702, 'ADDRESS_TYPE','Office Address', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 702);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 703, 'ADDRESS_TYPE','Billing Address', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 703);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 704, 'ADDRESS_TYPE','Shipping Address', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 704);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 705, 'ADDRESS_TYPE','Others', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 705);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 801, 'PHONE_TYPE','Home Phone', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 801);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 802, 'PHONE_TYPE','Mobile Phone', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 802);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 803, 'PHONE_TYPE','Work Phone', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 803);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 804, 'PHONE_TYPE','Office Phone', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 804);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 805, 'PHONE_TYPE','Others', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 805);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 901, 'EMAIL_TYPE','Personal Email', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 901);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 902, 'EMAIL_TYPE','Official Email', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 902);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 903, 'EMAIL_TYPE','Others', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 903);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1001, 'BUSINESS_CONTACT_TYPE','Primary Contact', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1001);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1002, 'BUSINESS_CONTACT_TYPE','Billing Contact', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1002);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1003, 'BUSINESS_CONTACT_TYPE','Emergency Contact', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1003);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1004, 'BUSINESS_CONTACT_TYPE','Others', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1004);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1101, 'FML_PERIOD_TYPE','Per Case', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1101);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1102, 'FML_PERIOD_TYPE','Calendar Year', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1102);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1103, 'FML_PERIOD_TYPE','Fixed Year From Service Date', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1103);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1104, 'FML_PERIOD_TYPE','Fixed Reset Period', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1104);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1105, 'FML_PERIOD_TYPE','Rolling Forward', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1105);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1106, 'FML_PERIOD_TYPE','Rolling Back', NULL, NULL, 6, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1106);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1107, 'FML_PERIOD_TYPE','Lifetime', NULL, NULL, 7, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1107);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1108, 'FML_PERIOD_TYPE','Per Occurrence', NULL, NULL, 8, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1108);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1109, 'FML_PERIOD_TYPE','School Year', NULL, NULL, 9, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1109);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1110, 'FML_PERIOD_TYPE','School Year Per Contact', NULL, NULL, 10, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1110);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1201, 'WEEK_DAY','Sunday', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1201);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1202, 'WEEK_DAY','Monday', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1202);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1203, 'WEEK_DAY','Tuesday', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1203);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1204, 'WEEK_DAY','Wednesday', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1204);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1205, 'WEEK_DAY','Thursday', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1205);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1206, 'WEEK_DAY','Friday', NULL, NULL, 6, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1206);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1207, 'WEEK_DAY','Saturday', NULL, NULL, 7, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1207);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1301, 'PAY_PERIOD_TYPE','Weekly', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1301);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1302, 'PAY_PERIOD_TYPE','Bi-Weekly', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1302);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1303, 'PAY_PERIOD_TYPE','Semi-Monthly', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1303);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1304, 'PAY_PERIOD_TYPE','Monthly', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1304);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1401, 'RULE_GROUP_SUCCESS_TYPE','And', NULL, 'All rules in the rule group must pass.', 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1401);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1402, 'RULE_GROUP_SUCCESS_TYPE','Or', NULL, 'At least one rule in the rule group must pass.', 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1402);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1403, 'RULE_GROUP_SUCCESS_TYPE','Not', NULL, 'No rules in the rule group may pass', 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1403);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1404, 'RULE_GROUP_SUCCESS_TYPE','Nor', NULL, 'At least one rule in the rule group must fail', 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1404);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1501, 'GENDER','Male', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1501);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1502, 'GENDER','Female', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1502);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1503, 'GENDER','Unknown', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1503);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1601, 'EMPLOYMENT_STATUS','Active', 'A', NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1601);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1602, 'EMPLOYMENT_STATUS','Inactive', 'I', NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1602);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1603, 'EMPLOYMENT_STATUS','Terminated', 'T', NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1603);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1604, 'EMPLOYMENT_STATUS','Leave', 'L', NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1604);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1701, 'PAY_TYPE','Salary', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1701);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1702, 'PAY_TYPE','Hourly', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1702);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1801, 'WORK_TYPE','FullTime', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1801);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1802, 'WORK_TYPE','PartTime', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1802);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1803, 'WORK_TYPE','PerDiem', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1803);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1901, 'JOB_CLASSIFICATION_TYPE','Sedentary', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1901);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1902, 'JOB_CLASSIFICATION_TYPE','Light', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1902);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1903, 'JOB_CLASSIFICATION_TYPE','Medium', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1903);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1904, 'JOB_CLASSIFICATION_TYPE','Heavy', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1904);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 1905, 'JOB_CLASSIFICATION_TYPE','VeryHeavy', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 1905);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2001, 'MILITARY_STATUS','Civilian', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2001);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2002, 'MILITARY_STATUS','ActiveDuty', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2002);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2003, 'MILITARY_STATUS','Veteran', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2003);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2101, 'FIELD_DATA_TYPE','Text', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2101);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2102, 'FIELD_DATA_TYPE','Number', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2102);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2103, 'FIELD_DATA_TYPE','Flag', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2103);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2104, 'FIELD_DATA_TYPE','Date', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2104);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2201, 'FIELD_CONTROL_TYPE','User Entered', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2201);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2202, 'FIELD_CONTROL_TYPE','Select List', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2202);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2301, 'CONTACT_CATEGORY','None', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2301);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2302, 'CONTACT_CATEGORY','Self', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2302);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2303, 'CONTACT_CATEGORY','Administrative', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2303);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2304, 'CONTACT_CATEGORY','Personal', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2304);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2305, 'CONTACT_CATEGORY','Medical', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2305);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2401, 'ORG_AFFILIATION','Viewer', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2401);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2402, 'ORG_AFFILIATION','Member', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2402);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2403, 'ORG_AFFILIATION','Leader', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2403);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2501, 'CASE_TYPE','None', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2501);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2502, 'CASE_TYPE','Consecutive', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2502);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2503, 'CASE_TYPE','Intermittent', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2503);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2504, 'CASE_TYPE','Reduced', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2504);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2505, 'CASE_TYPE','Administrative', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2505);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2601, 'POLICY_TYPE','FMLA', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2601);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2602, 'POLICY_TYPE','State FML', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2602);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2603, 'POLICY_TYPE','STD', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2603);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2604, 'POLICY_TYPE','LTD', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2604);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2605, 'POLICY_TYPE','Workers Comp', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2605);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2606, 'POLICY_TYPE','Company', NULL, NULL, 6, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2606);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2607, 'POLICY_TYPE','State Disability', NULL, NULL, 7, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2607);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2608, 'POLICY_TYPE','Other', NULL, NULL, 8, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2608);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2701, 'POLICY_RULE_GROUP_TYPE','Selection', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2701);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2702, 'POLICY_RULE_GROUP_TYPE','Eligibility', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2702);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2703, 'POLICY_RULE_GROUP_TYPE','Time', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2703);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2704, 'POLICY_RULE_GROUP_TYPE','Payment', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2704);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2705, 'POLICY_RULE_GROUP_TYPE','Crosswalk', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2705);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2801, 'ABSENCE_REASON_FLAG','None', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2801);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2802, 'ABSENCE_REASON_FLAG','Requires Relationship', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2802);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2803, 'ABSENCE_REASON_FLAG','Hide In Employee SelfService Selection', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2803);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2804, 'ABSENCE_REASON_FLAG','Show Accommodation', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2804);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2805, 'ABSENCE_REASON_FLAG','Requires Pregnancy Details', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2805);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2901, 'ENTITLEMENT_TYPE','Work Weeks', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2901);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2902, 'ENTITLEMENT_TYPE','Working Hours', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2902);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2903, 'ENTITLEMENT_TYPE','Calendar Weeks', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2903);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2904, 'ENTITLEMENT_TYPE','Calendar Days', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2904);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2905, 'ENTITLEMENT_TYPE','Calendar Months', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2905);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2906, 'ENTITLEMENT_TYPE','Calendar Years', NULL, NULL, 6, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2906);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2907, 'ENTITLEMENT_TYPE','Working Months', NULL, NULL, 7, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2907);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2908, 'ENTITLEMENT_TYPE','Work Days', NULL, NULL, 8, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2908);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 2909, 'ENTITLEMENT_TYPE','Reasonable Period', NULL, NULL, 9, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 2909);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3001, 'POLICY_ELIMINATION_TYPE_ID','Inclusive', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3001);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3002, 'POLICY_ELIMINATION_TYPE_ID','Exclusive', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3002);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3101, 'TIME_UNIT','Minutes', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3101);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3102, 'TIME_UNIT','Hours', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3102);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3103, 'TIME_UNIT','Days', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3103);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3104, 'TIME_UNIT','Weeks', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3104);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3105, 'TIME_UNIT','Months', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3105);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3106, 'TIME_UNIT','Years', NULL, NULL, 6, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3106);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3201, 'POLICY_SHOW_TYPE','Always', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3201);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3202, 'POLICY_SHOW_TYPE','OnlyTimeUsed', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3202);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3301, 'CASE_EVENT_TYPE','Case Created', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3301);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3302, 'CASE_EVENT_TYPE','Return To Work', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3302);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3303, 'CASE_EVENT_TYPE','Delivery Date', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3303);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3304, 'CASE_EVENT_TYPE','Bonding Start Date', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3304);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3305, 'CASE_EVENT_TYPE','Bonding End Date', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3305);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3306, 'CASE_EVENT_TYPE','Illness Or Injury Date', NULL, NULL, 6, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3306);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3307, 'CASE_EVENT_TYPE','Hospital Admission Date', NULL, NULL, 7, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3307);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3308, 'CASE_EVENT_TYPE','Hospital Release Date', NULL, NULL, 8, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3308);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3309, 'CASE_EVENT_TYPE','Release Received', NULL, NULL, 9, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3309);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3310, 'CASE_EVENT_TYPE','Paperwork Received', NULL, NULL, 10, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3310);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3311, 'CASE_EVENT_TYPE','Accommodation Added', NULL, NULL, 11, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3311);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3312, 'CASE_EVENT_TYPE','Case Closed', NULL, NULL, 12, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3312);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3313, 'CASE_EVENT_TYPE','Case Cancelled', NULL, NULL, 13, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3313);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3314, 'CASE_EVENT_TYPE','Adoption Date', NULL, NULL, 14, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3314);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3315, 'CASE_EVENT_TYPE','Estimated Return To Work', NULL, NULL, 15, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3315);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3316, 'CASE_EVENT_TYPE','Employer Holiday Schedule Changed', NULL, NULL, 16, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3316);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3317, 'CASE_EVENT_TYPE','Case Start Date', NULL, NULL, 17, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3317);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3318, 'CASE_EVENT_TYPE','Case End Date', NULL, NULL, 18, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3318);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3401, 'EVENT_DATE_TYPE','End Policy Based On Event Date', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3401);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3402, 'EVENT_DATE_TYPE','End Policy Based On Case Start Date', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3402);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3403, 'EVENT_DATE_TYPE','End Policy Based On Case End Date', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3403);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3404, 'EVENT_DATE_TYPE','Start Policy Based On Event Date', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3404);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3405, 'EVENT_DATE_TYPE','End Policy Based On Period', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3405);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3501, 'ELIGIBILITY_STATUS','Pending', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3501);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3502, 'ELIGIBILITY_STATUS','Ineligible', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3502);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3503, 'ELIGIBILITY_STATUS','Eligible', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3503);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3601, 'ADJUDICATION_STATUS','Pending', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3601);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3602, 'ADJUDICATION_STATUS','Approved', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3602);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3603, 'ADJUDICATION_STATUS','Denied', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3603);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3701, 'ADJUDICATION_DENIAL_REASON','Other', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3701);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3702, 'ADJUDICATION_DENIAL_REASON','Exhausted', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3702);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3703, 'ADJUDICATION_DENIAL_REASON','Not A Valid Healthcare Provider', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3703);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3704, 'ADJUDICATION_DENIAL_REASON','Not A Serious Health Condition', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3704);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3705, 'ADJUDICATION_DENIAL_REASON','Elimination Period', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3705);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3706, 'ADJUDICATION_DENIAL_REASON','Per Use Cap', NULL, NULL, 6, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3706);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3707, 'ADJUDICATION_DENIAL_REASON','Intermittent Non Allowed', NULL, NULL, 7, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3707);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3708, 'ADJUDICATION_DENIAL_REASON','Paperwork Not Received', NULL, NULL, 8, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3708);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3709, 'ADJUDICATION_DENIAL_REASON','Not An Eligible Family Member', NULL, NULL, 9, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3709);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3801, 'RULE_EVALUATION_RESULT','Unknown', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3801);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3802, 'RULE_EVALUATION_RESULT','Pass', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3802);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3803, 'RULE_EVALUATION_RESULT','Fail', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3803);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3901, 'CASE_STATUS','Open', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3901);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3902, 'CASE_STATUS','Closed', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3902);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3903, 'CASE_STATUS','Cancelled', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3903);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3904, 'CASE_STATUS','Requested', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3904);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 3905, 'CASE_STATUS','Inquiry', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 3905);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4001, 'SCHEDULE_TYPE','Weekly', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4001);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4002, 'SCHEDULE_TYPE','Rotating', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4002);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4003, 'SCHEDULE_TYPE','Variable', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4003);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4101, 'INTERMITTENT_TYPE','Office Visit', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4101);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4102, 'INTERMITTENT_TYPE','Incapacity', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4102);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4201, 'CASE_EVENT_TYPE','Case Created', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4201);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4202, 'CASE_EVENT_TYPE','Return To Work', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4202);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4203, 'CASE_EVENT_TYPE','Delivery Date', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4203);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4204, 'CASE_EVENT_TYPE','Bonding Start Date', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4204);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4205, 'CASE_EVENT_TYPE','Bonding End Date', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4205);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4206, 'CASE_EVENT_TYPE','Illness Or Injury Date', NULL, NULL, 6, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4206);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4207, 'CASE_EVENT_TYPE','Hospital Admission Date', NULL, NULL, 7, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4207);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4208, 'CASE_EVENT_TYPE','Hospital Release Date', NULL, NULL, 8, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4208);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4209, 'CASE_EVENT_TYPE','Release Received', NULL, NULL, 9, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4209);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4210, 'CASE_EVENT_TYPE','Paperwork Received', NULL, NULL, 10, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4210);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4211, 'CASE_EVENT_TYPE','Accommodation Added', NULL, NULL, 11, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4211);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4212, 'CASE_EVENT_TYPE','Case Closed', NULL, NULL, 12, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4212);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4213, 'CASE_EVENT_TYPE','Case Cancelled', NULL, NULL, 13, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4213);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4214, 'CASE_EVENT_TYPE','Adoption Date', NULL, NULL, 14, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4214);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4215, 'CASE_EVENT_TYPE','Estimated Return To Work', NULL, NULL, 15, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4215);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4216, 'CASE_EVENT_TYPE','Employer Holiday Schedule Changed', NULL, NULL, 16, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4216);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4217, 'CASE_EVENT_TYPE','Case Start Date', NULL, NULL, 17, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4217);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4218, 'CASE_EVENT_TYPE','Case End Date', NULL, NULL, 18, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4218);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4301, 'CASE_CANCEL_REASON','Other', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4301);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4302, 'CASE_CANCEL_REASON','Leave Not Needed Or Cancelled', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4302);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4303, 'CASE_CANCEL_REASON','Entered In Error', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4303);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4304, 'CASE_CANCEL_REASON','Inquiry Closed', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4304);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4401, 'CASE_CLOSURE_REASON','Return To Work', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4401);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4402, 'CASE_CLOSURE_REASON','Terminated', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4402);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4403, 'CASE_CLOSURE_REASON','Leave Cancelled', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4403);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4404, 'CASE_CLOSURE_REASON','Other', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4404);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4501, 'DIAGNOSIS_CODE_TYPE','ICD9', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4501);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4502, 'DIAGNOSIS_CODE_TYPE','ICD10', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4502);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4601, 'DAY_UNIT_TYPE','Workday', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4601);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4602, 'DAY_UNIT_TYPE','CalendarDay', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4602);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4701, 'ADJUDICATION_SUMMARY_STATUS','Pending', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4701);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4702, 'ADJUDICATION_SUMMARY_STATUS','Approved', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4702);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4703, 'ADJUDICATION_SUMMARY_STATUS','Denied', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4703);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4704, 'ADJUDICATION_SUMMARY_STATUS','Allowed', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4704);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4705, 'ADJUDICATION_SUMMARY_STATUS','NotEligible', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4705);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4801, 'PAYROLL_STATUS','Pending', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4801);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4802, 'PAYROLL_STATUS','Sent To Exception', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4802);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4803, 'PAYROLL_STATUS','Sent To Payroll', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4803);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4901, 'ACCOMMODATION_CANCEL_REASON','Other', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4901);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4902, 'ACCOMMODATION_CANCEL_REASON','Accommodation No Longer Needed', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4902);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4903, 'ACCOMMODATION_CANCEL_REASON','Leave No Longer Needed', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4903);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4904, 'ACCOMMODATION_CANCEL_REASON','Request Withdrawn', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4904);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4905, 'ACCOMMODATION_CANCEL_REASON','Entered For Wrong Employee', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4905);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 4906, 'ACCOMMODATION_CANCEL_REASON','Entered In Error', NULL, NULL, 6, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 4906);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5001, 'ACCOMMODATION_DURATION','Temporary', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5001);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5002, 'ACCOMMODATION_DURATION','Permanent', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5002);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5101, 'ACCOM_ADJUDICATION_DENIAL_REASON','Other', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5101);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5102, 'ACCOM_ADJUDICATION_DENIAL_REASON','Not A Reasonable Request', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5102);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5103, 'ACCOM_ADJUDICATION_DENIAL_REASON','Ineligible Health Condition', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5103);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5104, 'ACCOM_ADJUDICATION_DENIAL_REASON','Falsified Documentation', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5104);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5105, 'ACCOM_ADJUDICATION_DENIAL_REASON','Regulatory Prohibition Against Request', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5105);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5106, 'ACCOM_ADJUDICATION_DENIAL_REASON','Essentia Job Function Can Not Accommodate', NULL, NULL, 6, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5106);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5107, 'ACCOM_ADJUDICATION_DENIAL_REASON','Site Cannot Accommodate', NULL, NULL, 7, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5107);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5108, 'ACCOM_ADJUDICATION_DENIAL_REASON','No Position Fit Restrictions', NULL, NULL, 8, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5108);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5109, 'ACCOM_ADJUDICATION_DENIAL_REASON','Cannot Accommodate', NULL, NULL, 9, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5109);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5110, 'ACCOM_ADJUDICATION_DENIAL_REASON','No Paperwork Provided', NULL, NULL, 10, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5110);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5111, 'ACCOM_ADJUDICATION_DENIAL_REASON','Employee Did Not Participate', NULL, NULL, 11, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5111);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5201, 'WORK_RELATED_CASE_CLASSIFICATION','Other', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5201);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5202, 'WORK_RELATED_CASE_CLASSIFICATION','Death', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5202);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5203, 'WORK_RELATED_CASE_CLASSIFICATION','Days Away From Work', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5203);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5204, 'WORK_RELATED_CASE_CLASSIFICATION','Job Transfer Or Restriction', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5204);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5301, 'WORK_RELATED_TYPE_OF_INJURY','Injury', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5301);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5302, 'WORK_RELATED_TYPE_OF_INJURY','Skin Disorder', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5302);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5303, 'WORK_RELATED_TYPE_OF_INJURY','Respiratory Condition', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5303);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5304, 'WORK_RELATED_TYPE_OF_INJURY','Poisoning', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5304);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5305, 'WORK_RELATED_TYPE_OF_INJURY','Hearing Loss', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5305);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5306, 'WORK_RELATED_TYPE_OF_INJURY','Patient Handling', NULL, NULL, 6, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5306);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5307, 'WORK_RELATED_TYPE_OF_INJURY','All Other Illnesses', NULL, NULL, 7, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5307);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5401, 'WR_SHARP_INJURY_LOCATION','Finger', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5401);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5402, 'WR_SHARP_INJURY_LOCATION','Left Hand', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5402);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5403, 'WR_SHARP_INJURY_LOCATION','Right Hand', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5403);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5404, 'WR_SHARP_INJURY_LOCATION','Left Arm', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5404);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5405, 'WR_SHARP_INJURY_LOCATION','Right Arm', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5405);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5406, 'WR_SHARP_INJURY_LOCATION','Face Or Head', NULL, NULL, 6, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5406);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5407, 'WR_SHARP_INJURY_LOCATION','Torso', NULL, NULL, 7, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5407);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5408, 'WR_SHARP_INJURY_LOCATION','Left Leg', NULL, NULL, 8, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5408);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5409, 'WR_SHARP_INJURY_LOCATION','Right Leg', NULL, NULL, 9, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5409);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5410, 'WR_SHARP_INJURY_LOCATION','Other', NULL, NULL, 10, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5410);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5501, 'WR_SHARPS_WHEN','Don’t Know', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5501);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5502, 'WR_SHARPS_WHEN','Before Activation', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5502);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5503, 'WR_SHARPS_WHEN','During Activation', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5503);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5504, 'WR_SHARPS_WHEN','After Activation', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5504);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5601, 'WR_SHARP_JOB_CLASSIFICATION','Other', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5601);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5602, 'WR_SHARP_JOB_CLASSIFICATION','Doctor', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5602);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5603, 'WR_SHARP_JOB_CLASSIFICATION','Nurse', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5603);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5604, 'WR_SHARP_JOB_CLASSIFICATION','Intern Or Resident', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5604);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5605, 'WR_SHARP_JOB_CLASSIFICATION','Patient Care Support Staff', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5605);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5606, 'WR_SHARP_JOB_CLASSIFICATION','Technologist OR', NULL, NULL, 6, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5606);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5607, 'WR_SHARP_JOB_CLASSIFICATION','Technologist RT', NULL, NULL, 7, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5607);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5608, 'WR_SHARP_JOB_CLASSIFICATION','Technologist RAD', NULL, NULL, 8, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5608);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5609, 'WR_SHARP_JOB_CLASSIFICATION','Phlebotomist Or Lab Tech', NULL, NULL, 9, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5609);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5610, 'WR_SHARP_JOB_CLASSIFICATION','Housekeeper Or Laundry Worker', NULL, NULL, 10, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5610);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5611, 'WR_SHARP_JOB_CLASSIFICATION','Trainee', NULL, NULL, 11, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5611);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5701, 'WR_SHARP_LOCATION','Other', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5701);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5702, 'WR_SHARP_LOCATION','Patient Room', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5702);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5703, 'WR_SHARP_LOCATION','ICU', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5703);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5704, 'WR_SHARP_LOCATION','Outside Patient Room', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5704);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5705, 'WR_SHARP_LOCATION','Emergency Department', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5705);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5706, 'WR_SHARP_LOCATION','Operating Room Or PACU', NULL, NULL, 6, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5706);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5707, 'WR_SHARP_LOCATION','Clinical Laboratory', NULL, NULL, 7, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5707);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5708, 'WR_SHARP_LOCATION','Outpatient Clinic Or Office', NULL, NULL, 8, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5708);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5709, 'WR_SHARP_LOCATION','Utility Area', NULL, NULL, 9, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5709);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5801, 'WR_SHARP_PROCEDURE','Other', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5801);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5802, 'WR_SHARP_PROCEDURE','Draw Venous Blood', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5802);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5803, 'WR_SHARP_PROCEDURE','Draw Arterial Blood', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5803);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5804, 'WR_SHARP_PROCEDURE','Injection', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5804);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5805, 'WR_SHARP_PROCEDURE','Start IV Or Central Line', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5805);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5806, 'WR_SHARP_PROCEDURE','Heparin Or Saline Flush', NULL, NULL, 6, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5806);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5807, 'WR_SHARP_PROCEDURE','Obtain Body Fluid Or Tissue Sample', NULL, NULL, 7, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5807);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5808, 'WR_SHARP_PROCEDURE','Cutting', NULL, NULL, 8, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5808);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5809, 'WR_SHARP_PROCEDURE','Suturing', NULL, NULL, 9, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5809);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5901, 'NOTIFICATION_MESSAGE_FORMAT','Text', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5901);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5902, 'NOTIFICATION_MESSAGE_FORMAT','Formatted Text', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5902);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 5903, 'NOTIFICATION_MESSAGE_FORMAT','HTML', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 5903);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 6001, 'NOTIFICATION_TYPE','Email', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 6001);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 6002, 'NOTIFICATION_TYPE','SMS', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 6002);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 6003, 'NOTIFICATION_TYPE','Fax', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 6003);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 6101, 'NOTIFICATION_STATUS','Send', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 6101);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 6102, 'NOTIFICATION_STATUS','Discarded', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 6102);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 6201, 'NOTIFICATION_RECIPIENT_TYPE','Phone', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 6201);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 6202, 'NOTIFICATION_RECIPIENT_TYPE','To', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 6202);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 6203, 'NOTIFICATION_RECIPIENT_TYPE','CC', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 6203);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 6204, 'NOTIFICATION_RECIPIENT_TYPE','BCC', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 6204);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 6301, 'REPORT_REQUEST_STATUS','Pending', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 6301);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 6302, 'REPORT_REQUEST_STATUS','Processing', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 6302);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 6303, 'REPORT_REQUEST_STATUS','Complete', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 6303);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 6304, 'REPORT_REQUEST_STATUS','Canceled', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 6304);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 6305, 'REPORT_REQUEST_STATUS','Failed', NULL, NULL, 5, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 6305);

INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 6401, 'REPORT_SCHEDULE_OCCURRENCE_TYPE','OneTime', NULL, NULL, 1, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 6401);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 6402, 'REPORT_SCHEDULE_OCCURRENCE_TYPE','Daily', NULL, NULL, 2, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 6402);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 6403, 'REPORT_SCHEDULE_OCCURRENCE_TYPE','Weekly', NULL, NULL, 3, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 6403);
INSERT INTO lookup (id, category, title, description, value, sort_order, parent_lookup_id) SELECT 6404, 'REPORT_SCHEDULE_OCCURRENCE_TYPE','Monthly', NULL, NULL, 4, NULL WHERE NOT EXISTS (SELECT 1 FROM lookup WHERE id = 6404);
