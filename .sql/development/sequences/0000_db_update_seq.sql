CREATE SEQUENCE IF NOT EXISTS public.db_update_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.db_update_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.db_update_seq TO postgres;