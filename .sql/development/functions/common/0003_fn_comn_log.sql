﻿-- Procedure to insert a new city
CREATE OR REPLACE FUNCTION fn_comn_log(
	message varchar(512)
	, process_info varchar(255) DEFAULT ''
	, trace text DEFAULT ''
	, log_type bigint DEFAULT 103
	, app_type bigint DEFAULT 201
	, version varchar(50) DEFAULT '2.0'
	) 
RETURNS void AS $$
BEGIN
      INSERT INTO log
      (
		lookup_log_type_id
		, lookup_app_type_id
		, version
		, process_info
		, message
		, trace
	)
	VALUES 
	(
		log_type
		, app_type
		, version
		, process_info
		, message
		, trace
	);

	exception when others then 
		raise notice 'Unable to log';
		raise notice '% %', SQLERRM, SQLSTATE;
END;
$$ LANGUAGE plpgsql;