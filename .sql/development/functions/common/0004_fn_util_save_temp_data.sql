﻿--Saves temp data to temp_data table
CREATE OR REPLACE FUNCTION fn_util_save_temp_data(cache_key VARCHAR(80), company_id BIGINT, employer_id BIGINT, user_id BIGINT, value TEXT, active_till TIMESTAMP)
RETURNS VOID
AS
$BODY$
 	INSERT INTO temp_data(cache_key, company_id, employer_id, user_id, value, active_from, active_to)
    			VALUES (cache_key, company_id, employer_id, user_id, value, CURRENT_TIMESTAMP, active_till);
$BODY$
LANGUAGE 'sql';