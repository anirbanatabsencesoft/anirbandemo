﻿-- Returns all lookup categories
CREATE OR REPLACE FUNCTION fn_comn_get_lookup_categories() 
RETURNS TABLE (category varchar(50), description varchar(255))
AS 
$BODY$ 
	SELECT lc.category, lc.description FROM lookup_category lc WHERE lc.is_active = True;
$BODY$ 
LANGUAGE sql;
