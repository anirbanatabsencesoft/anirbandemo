CREATE OR REPLACE FUNCTION  fn_save_person(first_name VARCHAR(50), middle_name VARCHAR(50), last_name VARCHAR(50), lookup_gender_id BIGINT, id BIGINT DEFAULT NULL, date_of_birth DATE DEFAULT NULL, ssn VARCHAR(50) DEFAULT NULL, custom_field_value TEXT DEFAULT NULL, is_active BOOLEAN DEFAULT TRUE, is_deleted BOOLEAN DEFAULT FALSE)
RETURNS BIGINT
AS
$func$
BEGIN
   IF id IS NULL THEN
     BEGIN
		INSERT INTO person(first_name, middle_name, last_name, date_of_birth, ssn, lookup_gender_id, custom_field_value, is_active, is_deleted)
					VALUES(first_name, middle_name, last_name, date_of_birth, ssn, lookup_gender_id, custom_field_value, is_active, is_deleted)
					RETURNING id;
	 END;
   ELSE
     BEGIN
		UPDATE person p
		SET p.first_name = first_name, p.middle_name = middle_name, p.last_name = last_name, p.date_of_birth = date_of_birth, p.ssn = ssn, 
			p.lookup_gender_id = lookup_gender_id, p.custom_field_value = custom_field_value, p.is_active = is_active, p.is_deleted = is_deleted
		WHERE p.id = id RETURNING id;
	 END;
   END IF;   
END
$func$
LANGUAGE plpgsql;