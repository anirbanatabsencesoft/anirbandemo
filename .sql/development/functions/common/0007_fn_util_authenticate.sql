-- Returns user details for given combination of user_id, email and user_login_id
CREATE OR REPLACE FUNCTION fn_util_authenticate(login_email VARCHAR(150), user_password varchar(30))
RETURNS TABLE(password_must_change BOOLEAN, failed_login_attempts NUMERIC, last_failed_login TIMESTAMP, first_name VARCHAR(50), middle_name VARCHAR(50), last_name VARCHAR(50), date_of_birth DATE, customer_id BIGINT, customer_name VARCHAR(255), employer_id BIGINT, employer_name VARCHAR(255))
AS 
$body$
  -- delcare variables to reduce DB ound trips
  DECLARE user_login_id BIGINT; password_must_change BOOLEAN; last_failed_login TIMESTAMP; failed_login_attempts NUMERIC;
  

  BEGIN
	--get the id for login
		SELECT user_login_id = ul.id, password_must_change = ul.password_must_chage, last_failed_login = ul.last_failed_login, failed_login_attempts = ul.failed_login_attempts
	    FROM public.user_login ul 
		WHERE ul.login_email = ul.login_email AND ul.password = user_password AND ul.is_active = TRUE;

	--If no data found
	IF user_login_id IS NULL THEN
		RAISE 'Invalid username/password for User: %', login_email USING ERRCODE = 'invalid_authorization_specification';
	END IF;
	
	RETURN QUERY
		SELECT password_must_change, failed_login_attempts, last_failed_login, first_name, middle_name, last_name, date_of_birth, u.customer_id, customer_name, employer_id, employer_name
		FROM fn_util_get_user_details(null, null, user_login_id);
	
  END;
$body$
LANGUAGE plpgsql;