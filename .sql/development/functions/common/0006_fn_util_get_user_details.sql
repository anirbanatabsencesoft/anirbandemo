-- Returns user details for given combination of user_id, email and user_login_id
CREATE OR REPLACE FUNCTION fn_util_get_user_details(user_id BIGINT DEFAULT NULL, email TEXT DEFAULT NULL, user_login_id BIGINT DEFAULT NULL)
RETURNS TABLE(first_name VARCHAR(50), middle_name VARCHAR(50), last_name VARCHAR(50), date_of_birth DATE, customer_id BIGINT, customer_name VARCHAR(255), employer_id BIGINT, employer_name VARCHAR(255))
AS 
$BODY$
BEGIN	
	IF user_id IS NULL AND email IS NOT NULL THEN
	  BEGIN
		SELECT u.user_id INTO user_id FROM user u WHERE u.email = email LIMIT 1;
	  END;
	END IF; 
	  
	IF user_id IS NULL AND user_login_id IS NOT NULL THEN
	  BEGIN
		SELECT u.user_id INTO user_id FROM user u WHERE u.user_login_id = user_login_id;
	  END;	
	END IF;
  
    RETURN QUERY   SELECT p.first_name, p.middle_name, p.last_name, p.date_of_birth, u.customer_id, c.name  AS customer_name, u.employer_id, e.name AS employer_name
                    FROM person p
                    INNER JOIN public.user u on p.id = u.person_id
                    INNER JOIN employer e on e.id = u.employer_id
                    INNER JOIN customer c on c.id = u.customer_id
                    WHERE u.user_id = user_id;
END;
$BODY$
LANGUAGE plpgsql;