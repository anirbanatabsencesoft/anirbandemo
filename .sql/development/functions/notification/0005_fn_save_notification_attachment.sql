CREATE OR REPLACE FUNCTION fn_save_notification_attachment
(noti_id BIGINT, file_name VARCHAR(255), file_content BYTEA, attachment_header VARCHAR(255))
RETURNS BIGINT
 AS  
  $BODY$  
	DECLARE attachment_id BIGINT;
	  BEGIN
		IF EXISTS(SELECT 1 FROM public.notification_attachment att WHERE att.notification_id = noti_id) THEN
		  BEGIN
			DELETE FROM public.notification_attachment att 
			WHERE att.notification_id = noti_id;
		  END;
		END IF;
		  
		INSERT INTO public.notification_attachment
							(notification_id
							,file_name
							,file_content
							,attachment_header)
					VALUES ( noti_id
							,file_name
							,file_content
							,attachment_header)
					RETURNING id INTO attachment_id;
					RETURN attachment_id;
		
		EXCEPTION WHEN OTHERS THEN 
			RAISE 'Could not save notification attachment for Notification id : %', noti_id USING ERRCODE = 'invalid_parameter_value';
    		
	  END
  $BODY$
  LANGUAGE PLPGSQL;