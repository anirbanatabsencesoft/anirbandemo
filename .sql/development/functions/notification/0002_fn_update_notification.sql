CREATE OR REPLACE FUNCTION fn_update_notification
(id BIGINT, modified_by_id BIGINT, last_retried_on TIMESTAMP, message_id VARCHAR(2000), notified_on VARCHAR(100) DEFAULT NULL, 
 is_archived BOOLEAN DEFAULT FALSE, failure_reason TEXT = NULL, is_deleted BOOLEAN DEFAULT FALSE)
RETURNS VOID
 AS
  $BODY$
	BEGIN
		UPDATE public.notification noti
		SET  noti.notified_on = notified_on
			,noti.modified_by_id = modified_by_id
			,noti.no_of_retries = noti.no_of_retries + 1
			,noti.last_retried_on = CURRENT_TIMESTAMP
			,noti.is_archived = is_archived
			,noti.failure_reason = failure_reason
			,noti.is_deleted = is_deleted
			,noti.message_id = message_id
		WHERE noti.ID = id;
		
		EXCEPTION WHEN OTHERS THEN 
			RAISE 'Could not update notification for Notification id : %', id USING ERRCODE = 'invalid_parameter_value';
    
	END;
  $BODY$
  LANGUAGE PLPGSQL;