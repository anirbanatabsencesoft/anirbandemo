CREATE OR REPLACE FUNCTION public.fn_save_notification_recipient
(noti_id BIGINT, noti_recipient VARCHAR(1000), noti_lookup_recipient_type_id BIGINT)
RETURNS BIGINT
 AS
  $BODY$
   DECLARE recipient_id BIGINT;
	  BEGIN
      
        IF EXISTS(SELECT 1 FROM public.notification_recipient rec 
				  WHERE rec.notification_id = noti_id AND rec.recipient = noti_recipient AND rec.lookup_recipient_type_id = noti_lookup_recipient_type_id) THEN
		  BEGIN
			DELETE FROM public.notification_recipient rec 
			WHERE rec.notification_id = noti_id;
		  END;
         END IF;
          
		INSERT INTO public.notification_recipient
						   (notification_id
						   ,recipient
						   ,lookup_recipient_type_id)
					VALUES (noti_id
						   ,noti_recipient
						   ,noti_lookup_recipient_type_id)
					RETURNING id INTO recipient_id;
					RETURN recipient_id;
		
		EXCEPTION WHEN OTHERS THEN 
			RAISE 'Could not save notification recipient for Notification id : %', noti_id USING ERRCODE = 'invalid_parameter_value';
    		
	  END
  $BODY$
  LANGUAGE PLPGSQL;