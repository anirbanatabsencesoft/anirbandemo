CREATE OR REPLACE FUNCTION fn_save_notification
(noti_customer_id BIGINT DEFAULT NULL, noti_sender VARCHAR(255) DEFAULT NULL, noti_lookup_notification_type_id BIGINT DEFAULT NULL, noti_created_by_id BIGINT DEFAULT NULL, noti_lookup_message_format_id BIGINT DEFAULT NULL, noti_id BIGINT DEFAULT NULL, noti_customer_object_id  VARCHAR(50) DEFAULT NULL, 
 noti_employer_id BIGINT DEFAULT NULL, noti_employer_object_id VARCHAR(50) DEFAULT NULL, noti_subject VARCHAR(255) DEFAULT NULL, noti_message TEXT = NULL, 
 noti_created_by_object_id character varying(50) DEFAULT NULL, noti_modified_by_id BIGINT DEFAULT NULL, noti_modified_by_object_id character varying(40) DEFAULT NULL, 
 noti_notified_on TIMESTAMP WITH TIME ZONE DEFAULT NULL, noti_unique_id VARCHAR(100) DEFAULT NULL, noti_no_of_retries BIGINT DEFAULT 3, 
 noti_message_id VARCHAR(100) DEFAULT NULL, noti_allowed_no_of_retries NUMERIC DEFAULT 3, noti_last_retried_on TIMESTAMP DEFAULT NULL, 
 noti_is_archived BOOLEAN DEFAULT FALSE, noti_lookup_notification_status_id BIGINT DEFAULT NULL, noti_failure_reason TEXT = NULL,
 noti_display_name character varying(255) = null)
RETURNS TABLE (id bigint, CustomerId bigint, customerObjectId character varying(50), EmployerId bigint, EmployerObjectId character varying(50), 
	   "from" character varying(255), DisplayName character varying(255), subject character varying(255), LookupMessageFormatId bigint, message text, LookupNotificationTypeId bigint, 
	   CreatedDate timestamp with time zone, CreatedById bigint, CreatedByObjectId character varying(40), ModifiedDate timestamp with time zone, 
	   ModifiedById bigint, ModifiedByObjectId character varying(40), NotifiedOn timestamp with time zone, UniqueId character varying(100), 
	   MessageId character varying(100), NoOfRetries numeric, AllowedNoOfRetries numeric, LastRetriedOn timestamp with time zone, 
	   IsArchived boolean, LookupNotificationStatusId bigint, FailureReason text, IsDeleted boolean)
 AS
  $BODY$
  BEGIN
	  IF noti_id IS NULL THEN
	    BEGIN
        	RETURN QUERY
			INSERT INTO public.notification(
					  customer_id
					 ,customer_object_id
					 ,employer_id
					 ,employer_object_id
					 ,"from"
					 ,display_name
					 ,subject
					 ,lookup_message_format_id
					 ,message
					 ,lookup_notification_type_id
					 ,created_date
					 ,created_by_id
					 ,created_by_object_id
					 ,modified_date
					 ,modified_by_id
					 ,modified_by_object_id
					 ,notified_on
					 ,unique_id
					 ,message_id
					 ,no_of_retries
					 ,allowed_no_of_retries
					 ,last_retried_on
					 ,is_archived
					 ,lookup_notification_status_id
					 ,failure_reason)
			VALUES (noti_customer_id
				,noti_customer_object_id
				,noti_employer_id
				,noti_employer_object_id
				,noti_sender
				,noti_display_name
				,noti_subject
				,noti_lookup_message_format_id
				,noti_message
				,noti_lookup_notification_type_id
				,CURRENT_TIMESTAMP
				,noti_created_by_id
				,noti_created_by_object_id
				,CURRENT_TIMESTAMP
				,noti_modified_by_id
				,noti_modified_by_object_id
				,noti_notified_on
				,noti_unique_id
				,noti_message_id
				,noti_no_of_retries
				,noti_allowed_no_of_retries
				,noti_last_retried_on
				,noti_is_archived
				,noti_lookup_notification_status_id
				,noti_failure_reason)
				RETURNING *;
		EXCEPTION WHEN OTHERS THEN 
	
			RAISE 'Could not save notification for Customer id : %', noti_customer_id USING ERRCODE = 'invalid_parameter_value';
    
		END;
	  ELSE
	    BEGIN
        	RETURN QUERY
			UPDATE public.notification noti
				SET  modified_date = CURRENT_TIMESTAMP
					,modified_by_id = noti_modified_by_id
					,notified_on = noti_notified_on
					,failure_reason = noti_failure_reason
					,lookup_notification_status_id = noti_lookup_notification_status_id
					,message_id = noti_message_id
				WHERE noti.id = noti_id AND noti.customer_id = noti_customer_id
				RETURNING *;
		
		EXCEPTION WHEN OTHERS THEN 
			RAISE 'Could not update notification for Notification id : %', noti_id USING ERRCODE = 'invalid_parameter_value';
    		
		END;        
	  END IF;   
	
	
  END
  $BODY$
  LANGUAGE PLPGSQL;