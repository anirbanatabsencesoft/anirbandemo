CREATE OR REPLACE FUNCTION public.fn_delete_notification
(noti_id BIGINT, noti_modified_by_id BIGINT)
RETURNS INT
 AS
   $BODY$
    DECLARE
    v_cnt numeric;    
	BEGIN
		UPDATE public.notification
		SET  modified_by_id = noti_modified_by_id
		    ,modified_Date = CURRENT_TIMESTAMP
			,is_deleted = TRUE			
		WHERE Id = noti_id;
		
        GET DIAGNOSTICS v_cnt = ROW_COUNT;
        
		DELETE FROM public.notification_attachment att
		WHERE att.notification_id = noti_id;
		
		DELETE FROM public.notification_recipient rec
		WHERE rec.notification_id = noti_id;
                
		RETURN v_cnt;
		
		EXCEPTION WHEN OTHERS THEN 
			RAISE 'Could not delete notification id : %', noti_id USING ERRCODE = 'invalid_parameter_value';
    
	END;
    $BODY$
  LANGUAGE PLPGSQL;