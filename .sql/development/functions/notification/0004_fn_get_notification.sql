CREATE OR REPLACE FUNCTION fn_get_notification
(notification_id BIGINT DEFAULT NULL, noti_customer_id BIGINT DEFAULT NULL)
RETURNS TABLE(id BIGINT, customer_id BIGINT, customer_object_id VARCHAR(50), employer_id BIGINT, employer_object_id VARCHAR(50),
			  "from" VARCHAR(255), subject VARCHAR(255), lookup_message_format_id BIGINT, message TEXT,
			  lookup_notification_type_id BIGINT, created_date TIMESTAMP WITH TIME ZONE, created_by_id BIGINT,
			  created_by_object_id VARCHAR(40), modified_date TIMESTAMP WITH TIME ZONE, modified_by_id BIGINT, 
			  modified_by_object_id VARCHAR(40), notified_on TIMESTAMP WITH TIME ZONE, unique_id VARCHAR(100), message_id VARCHAR(100) ,
			  no_of_retries NUMERIC, allowed_no_of_retries NUMERIC, last_retried_on TIMESTAMP WITH TIME ZONE,
			  is_archived BOOLEAN, lookup_notification_status_id BIGINT, failure_reason TEXT)
 AS
  $BODY$
	BEGIN
    	RETURN QUERY
		SELECT  noti.id
        		,noti.customer_id
                ,noti.customer_object_id
                ,noti.employer_id
                ,noti.employer_object_id
			  	,noti."from"
                ,noti.subject
                ,noti.lookup_message_format_id
                ,noti.message
                ,noti.lookup_notification_type_id
                ,noti.created_date
                ,noti.created_by_id
                ,noti.created_by_object_id
                ,noti.modified_date
                ,noti.modified_by_id
                ,noti.modified_by_object_id
                ,noti.notified_on
                ,noti.unique_id
                ,noti.message_id
                ,noti.no_of_retries
                ,noti.allowed_no_of_retries
                ,noti.last_retried_on
                ,noti.is_archived
                ,noti.lookup_notification_status_id
                ,noti.failure_reason				
                FROM public.notification noti
				WHERE (notification_id IS NOT NULL OR noti.is_archived = FALSE) -- AND is_deleted = FALSE 				
				AND (notification_id IS NULL OR noti.id = notification_id) 
				AND (noti_customer_id IS NOT NULL OR noti.customer_id = noti_customer_id)
				AND (coalesce(noti.no_of_retries, 0) <= noti.allowed_no_of_retries);
		EXCEPTION WHEN OTHERS THEN 
			RAISE 'Could not search for given parameters' USING ERRCODE = 'invalid_parameter_value';
	END;
  $BODY$
  LANGUAGE PLPGSQL;