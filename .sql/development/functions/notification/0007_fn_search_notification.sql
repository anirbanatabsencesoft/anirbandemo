CREATE OR REPLACE FUNCTION fn_search_notification
(notification_id BIGINT DEFAULT NULL, noti_customer_id BIGINT DEFAULT NULL, noti_sender VARCHAR(255) DEFAULT NULL, noti_recipient varchar(255) DEFAULT NULL, 
 noti_hasattachment BOOLEAN DEFAULT NULL, noti_subject_contains VARCHAR(255) DEFAULT NULL, noti_message_contains TEXT DEFAULT NULL, 
 noti_include_archive BOOLEAN DEFAULT NULL, noti_lookup_notification_status_id BIGINT DEFAULT NULL, noti_customer_object_id character varying(50) DEFAULT NULL)
RETURNS TABLE (id bigint, CustomerId bigint, CustomerKey character varying(50), EmployerId bigint, EmployerObjectId character varying(50), 
	   "from" character varying(255), Recipients text, subject character varying(255), LookupMessageFormatId bigint, message text, LookupNotificationTypeId bigint, 
	   CreatedDate timestamp with time zone, CreatedById bigint, CreatedByObjectId character varying(40), ModifiedDate timestamp with time zone, 
	   ModifiedById bigint, ModifiedByObjectId character varying(40), NotifiedOn timestamp with time zone, UniqueId character varying(100), 
	   MessageId character varying(100), NoOfRetries numeric, AllowedNoOfRetries numeric, LastRetriedOn timestamp with time zone, 
	   IsArchived boolean, LookupNotificationStatusId bigint, FailureReason text)
 AS
  $BODY$
	BEGIN
		IF noti_subject_contains IS NOT NULL THEN
			noti_subject_contains := concat('%', noti_subject_contains, '%');
		END IF;
		
		IF noti_message_contains IS NOT NULL THEN
			noti_message_contains := concat('%', noti_message_contains, '%');
		END IF;
	
    	RETURN QUERY
		SELECT  noti.id
        		,noti.customer_id
                ,noti.customer_object_id
                ,noti.employer_id
                ,noti.employer_object_id
			  	,noti."from"
				,noti.recipient
                ,noti.subject
                ,noti.lookup_message_format_id
                ,noti.message
                ,noti.lookup_notification_type_id
                ,noti.created_date
                ,noti.created_by_id
                ,noti.created_by_object_id
                ,noti.modified_date
                ,noti.modified_by_id
                ,noti.modified_by_object_id
                ,noti.notified_on
                ,noti.unique_id
                ,noti.message_id
                ,noti.no_of_retries
                ,noti.allowed_no_of_retries
                ,noti.last_retried_on
                ,noti.is_archived
                ,noti.lookup_notification_status_id
                ,noti.failure_reason				
                FROM public.vw_notification noti
				WHERE (noti_include_archive IS NULL OR noti.is_archived = noti_include_archive) AND is_deleted = FALSE
				AND (notification_id IS NULL OR noti.id = notification_id) 
				AND (noti_customer_id IS NULL OR noti.customer_id = noti_customer_id)
				AND (noti_customer_object_id IS NULL OR noti.customer_object_id = noti_customer_object_id)
				AND (noti_sender IS NULL OR noti."from" = noti_sender)
				AND (noti_recipient IS NULL OR noti.recipient = noti_recipient)
				AND (noti_hasattachment IS NULL OR (noti.hasattachment = noti_hasattachment))
				AND (noti_subject_contains IS NULL OR (LOWER(noti.subject) LIKE LOWER(noti_subject_contains)))
				AND (noti_message_contains IS NULL OR (LOWER(noti.message) LIKE LOWER(noti_message_contains)))
				AND (noti_lookup_notification_status_id IS NULL OR noti.lookup_notification_status_id = noti_lookup_notification_status_id)
				ORDER BY noti.id;
		EXCEPTION WHEN OTHERS THEN 
			RAISE 'Could not search Notification for given parameters' USING ERRCODE = 'invalid_parameter_value';
    		
	END;
  $BODY$
  LANGUAGE PLPGSQL;