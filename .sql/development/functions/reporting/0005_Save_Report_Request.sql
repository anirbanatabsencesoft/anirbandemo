-- Function: public.fn_save_report_request(bigint, bigint, character varying, character varying, text, text, bigint, bigint, character varying, character varying, character varying, character varying, character varying, text, character varying)

-- DROP FUNCTION public.fn_save_report_request(bigint, bigint, character varying, character varying, text, text, bigint, bigint, character varying, character varying, character varying, character varying, character varying, text, character varying);

CREATE OR REPLACE FUNCTION public.fn_save_report_request(
    rr_id bigint DEFAULT NULL::bigint,
    rr_user_id bigint DEFAULT NULL::bigint,
    rr_user_key character varying DEFAULT NULL::character varying,
    rr_request_type character varying DEFAULT NULL::character varying,
    rr_request_message text DEFAULT NULL::text,
    rr_response_message text DEFAULT NULL::text,
    rr_lookup_status_id bigint DEFAULT NULL::bigint,
    rr_scheduled_report_id bigint DEFAULT NULL::bigint,
    rr_result_max_json_file_path character varying DEFAULT NULL::character varying,
    rr_result_all_json_file_path character varying DEFAULT NULL::character varying,
    rr_result_pdf_file_path character varying DEFAULT NULL::character varying,
    rr_result_csv_file_path character varying DEFAULT NULL::character varying,
    rr_report_name character varying DEFAULT NULL::character varying,
    rr_criteria_plain_english text DEFAULT NULL::text,
    rr_report_id character varying DEFAULT NULL::character varying)
  RETURNS SETOF report_request AS
$BODY$

  BEGIN
	  IF rr_id IS NULL THEN
	    BEGIN
        	RETURN QUERY
			INSERT INTO public.report_request(user_id,
											  user_key,
											  request_type,
											  request_message,
											  response_message,
											  lookup_status_id,
											  scheduled_report_id,
											  created_on,
											  modified_on,
											  is_completed,
											  result_max_json_file_path,
											  result_all_json_file_path,
											  result_pdf_file_path,
											  result_csv_file_path,
											  report_name,
											  criteria_plain_english,
											  report_id)
									VALUES (rr_user_id,
											rr_user_key,
											rr_request_type,
											rr_request_message,
											rr_response_message,
											rr_lookup_status_id,
											rr_scheduled_report_id,
											CURRENT_TIMESTAMP,
											CURRENT_TIMESTAMP,
											FALSE,
											rr_result_max_json_file_path,
											rr_result_all_json_file_path,
											rr_result_pdf_file_path,
											rr_result_csv_file_path,
											rr_report_name,
											rr_criteria_plain_english,
											rr_report_id)
											RETURNING *;
			
			EXCEPTION WHEN OTHERS THEN 
				RAISE 'Could not save report request for request type : %', rr_request_type USING ERRCODE = 'invalid_parameter_value';    
		END;
	  ELSE
	    BEGIN
        	RETURN QUERY
			UPDATE public.report_request rr
						set user_id 				  = rr_user_id,
						    user_key 				  = rr_user_key,
						    request_type			  = rr_request_type,
						    request_message			  = rr_request_message,
						    response_message		  = rr_response_message,
						    lookup_status_id		  = rr_lookup_status_id,
						    scheduled_report_id		  = rr_scheduled_report_id,
						    modified_on				  = CURRENT_TIMESTAMP,
						    result_max_json_file_path = rr_result_max_json_file_path,
						    result_all_json_file_path = rr_result_all_json_file_path,
						    result_pdf_file_path	  = rr_result_pdf_file_path,
						    result_csv_file_path 	  = rr_result_csv_file_path
					WHERE rr.id = rr_id 
					AND (rr_user_id IS NULL OR rr.user_id = rr_user_id)
					AND (rr_user_key IS NULL OR rr.user_key = rr_user_key)
				RETURNING *;
		
			EXCEPTION WHEN OTHERS THEN 
				RAISE 'Could not update report request for request type : %', rr_request_type USING ERRCODE = 'invalid_parameter_value';    		
		END;        
	  END IF;	
  END
  
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.fn_save_report_request(bigint, bigint, character varying, character varying, text, text, bigint, bigint, character varying, character varying, character varying, character varying, character varying, text, character varying)
  OWNER TO postgres;
