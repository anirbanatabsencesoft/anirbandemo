CREATE OR REPLACE FUNCTION public.fn_get_scheduled_reports_to_run(
rr_run_date timestamp with time zone DEFAULT CURRENT_TIMESTAMP::timestamp with time zone
)  
  RETURNS TABLE ( id bigint,
				schedule_info_json text,	
				next_run_date timestamp with time zone,
				report_category varchar(120),				
				report_json text,
				customer_id bigint,
				customer_key varchar(24),
				created_by_id bigint,
				created_by_key varchar(24),
				created_on timestamp with time zone,
				modified_by_id bigint,
				modified_by_key varchar(24),
				modified_on timestamp with time zone,
				is_active boolean,
				is_deleted boolean
				)
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$

  BEGIN	  
        RETURN QUERY
        SELECT rr.id , rr.schedule_info_json ,	rr.next_run_date ,	rr.report_category , rr.report_json , rr.customer_id , rr.customer_key , 
		rr.created_by_id , rr.created_by_key , rr.created_on ,	rr.modified_by_id , rr.modified_by_key , rr.modified_on , rr.is_active , rr.is_deleted 
		FROM public.scheduled_report rr Where rr.next_run_date <= rr_run_date and rr.is_active=true and rr.is_deleted=false;

        EXCEPTION WHEN OTHERS THEN 
            RAISE 'Could not fetch scheduled report.';		 	  
  END
  
$BODY$;

ALTER FUNCTION public.fn_get_scheduled_reports_to_run(timestamp with time zone)
    OWNER TO postgres;
