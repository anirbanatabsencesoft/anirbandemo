CREATE OR REPLACE FUNCTION fn_delete_scheduled_report
(
    sr_id bigint DEFAULT NULL
)
RETURNS INT
 AS
   $BODY$
    DECLARE
    v_cnt numeric;
	BEGIN
			UPDATE public.scheduled_report
			SET modified_on = CURRENT_TIMESTAMP
				,is_deleted = TRUE			
			WHERE Id = sr_id;
						
			GET DIAGNOSTICS v_cnt = ROW_COUNT;
			
			RETURN v_cnt;
		COMMIT;

		EXCEPTION WHEN OTHERS THEN 
			BEGIN
				ROLLBACK;
				RAISE 'Could not delete scheduled report request id : %', sr_id USING ERRCODE = 'invalid_parameter_value';
			END;    
	END;
    $BODY$
  LANGUAGE PLPGSQL;