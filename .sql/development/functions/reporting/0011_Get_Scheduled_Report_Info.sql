CREATE OR REPLACE FUNCTION public.fn_get_scheduled_report_info(
	rr_id bigint DEFAULT NULL::bigint
	)
     RETURNS TABLE ( id bigint, schedule_info_json text, next_run_date timestamp with time zone, report_category character varying, report_json text, customer_id bigint, customer_key character varying, created_by_id bigint, created_by_key character varying, created_on timestamp with time zone, modified_by_id bigint, modified_by_key character varying, modified_on timestamp with time zone, is_active boolean, is_deleted boolean)				
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$

  BEGIN	  
        RETURN QUERY
        Select rr.id , rr.schedule_info_json , rr.next_run_date , rr.report_category , rr.report_json , rr.customer_id, rr.customer_key , rr.created_by_id , rr.created_by_key , rr.created_on , rr.modified_by_id , rr.modified_by_key , rr.modified_on , rr.is_active , rr.is_deleted  
		from public.scheduled_report rr where rr.id = rr_id;

        EXCEPTION WHEN OTHERS THEN 
            RAISE 'Could not fetch scheduled report for Id : %', rr_id USING ERRCODE = 'invalid_parameter_value';    
		 	  
  END
  
$BODY$;

ALTER FUNCTION public.fn_get_scheduled_report_info(bigint)
    OWNER TO postgres;

