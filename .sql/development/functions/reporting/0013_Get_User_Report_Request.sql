-- FUNCTION: public.fn_get_user_report_request(bigint, character varying, integer)

-- DROP FUNCTION public.fn_get_user_report_request(bigint, character varying, integer);

CREATE OR REPLACE FUNCTION public.fn_get_user_report_request(
	rr_user_id bigint DEFAULT NULL::bigint,
	rr_user_key character varying DEFAULT NULL::character varying,
	rr_limit integer DEFAULT 10,
	rr_queued_request_only boolean DEFAULT False)
    RETURNS TABLE(id bigint
	, user_id bigint
	, user_key character varying
	, request_type character varying
	, request_message text
	, response_message text
	, lookup_status_id bigint
	, scheduled_report_id bigint
	, created_on timestamp with time zone
	, modified_on timestamp with time zone
	, is_completed boolean
	, result_max_json_file_path character varying
	, result_all_json_file_path character varying
	, result_pdf_file_path character varying
	, result_csv_file_path character varying
	, is_deleted boolean
	, report_name varchar(255)
	, criteria_plain_english text
	, report_id varchar(50)) 
	 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$

  BEGIN	  
        RETURN QUERY
        Select 
			rr.id 
			, rr.user_id 
			, rr.user_key 
			, rr.request_type 
			, rr.request_message 
			, rr.response_message 
			, rr.lookup_status_id 
			, rr.scheduled_report_id 
			, rr.created_on 
			, rr.modified_on 
			, rr.is_completed 
			, rr.result_max_json_file_path 
			, rr.result_all_json_file_path 
			, rr.result_pdf_file_path 
			, rr.result_csv_file_path 
			, rr.is_deleted
			, rr.report_name
			, rr.criteria_plain_english
			, rr.report_id
		FROM public.report_request rr  
		WHERE 	(rr_user_id IS NULL OR rr.user_id = rr_user_id)
			AND (rr_user_key IS NULL OR rr.user_key = rr_user_key) 
			AND rr.is_deleted = False
			AND (CASE WHEN rr_queued_request_only = True THEN rr.lookup_status_id <=6302 ELSE rr.lookup_status_id > 6302 END)
			
		ORDER BY rr.id DESC
		LIMIT rr_limit;
		
        EXCEPTION WHEN OTHERS THEN 
            RAISE 'Could not fetch report request for user_key : %', rr_user_key USING ERRCODE = 'invalid_parameter_value';    
		 	  
  END
  

$BODY$;

ALTER FUNCTION public.fn_get_user_report_request(bigint, character varying, integer, boolean)
    OWNER TO postgres;

