CREATE OR REPLACE FUNCTION fn_save_shared_report
(
	sr_report_name varchar(512), 
	sr_ui_sequence INTEGER, 
	sr_report_class_fqn VARCHAR(512), 
	sr_report_json text, 
	sr_shared_with character(4),  
	sr_created_by_id bigint,  
	sr_created_by_key varchar(24),
	sr_id BIGINT DEFAULT NULL, 
	sr_report_category character varying(120) DEFAULT NULL, 
	sr_customer_id bigint DEFAULT NULL, 
	sr_customer_key character varying(24) DEFAULT NULL,
	sr_scheduled_report_id bigint DEFAULT NULL, 
	sr_is_active boolean DEFAULT TRUE,
	sr_last_requested_id bigint DEFAULT NULL
 )	
RETURNS BIGINT
 AS
  $BODY$
  DECLARE l_id BIGINT;
  BEGIN
	  IF sr_id IS NULL THEN
	    BEGIN
			INSERT INTO public.shared_report(report_name, 
											 ui_sequence, 
											 report_category, 
											 report_class_fqn, 
											 report_json, 
											 customer_id, 
											 customer_key, 
											 shared_with, 
											 scheduled_report_id,
											 created_by_id, 
											 created_by_key, 
											 created_on, 
											 modified_on, 
											 is_active,
											 last_request_id)
									VALUES (sr_report_name, 
											sr_ui_sequence, 
											sr_report_category, 
											sr_report_class_fqn, 
											sr_report_json, 
											sr_customer_id, 
											sr_customer_key, 
											sr_shared_with, 
											sr_scheduled_report_id,
											sr_created_by_id, 
											sr_created_by_key, 
											CURRENT_TIMESTAMP, 
											CURRENT_TIMESTAMP, 
											TRUE,
											sr_last_requested_id)
											RETURNING id INTO l_id;
			
		END;
	  ELSE
	    BEGIN
			UPDATE public.shared_report sr
						set  report_name 		= sr_report_name, 
							 ui_sequence 		= sr_ui_sequence, 
							 report_category 	= sr_report_category, 
							 report_class_fqn 	= sr_report_class_fqn, 
							 report_json 		= sr_report_json, 
							 customer_id 		= sr_customer_id, 
							 customer_key 		= sr_customer_key, 
							 scheduled_report_id = sr_scheduled_report_id,
							 shared_with 		= sr_shared_with, 
							 modified_on 		= CURRENT_TIMESTAMP, 
							 is_active 			= sr_is_active,
							 last_request_id	= sr_last_requested_id
					WHERE sr.id = sr_id 
				RETURNING id INTO l_id;
			
		END;        
	  END IF;	
	  
	  RETURN l_id;
  END
  $BODY$
  LANGUAGE PLPGSQL;