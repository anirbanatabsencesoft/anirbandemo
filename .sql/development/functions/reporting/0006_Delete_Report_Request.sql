CREATE OR REPLACE FUNCTION fn_delete_report_request
(
    rr_id bigint DEFAULT NULL
)
RETURNS INT
 AS
   $BODY$
    DECLARE
    v_cnt numeric;    
	BEGIN
			UPDATE public.report_request
			SET modified_on = CURRENT_TIMESTAMP
				,is_deleted = TRUE			
			WHERE Id = rr_id;
			
			GET DIAGNOSTICS v_cnt = ROW_COUNT;
			
			RETURN v_cnt;
		COMMIT;

		EXCEPTION WHEN OTHERS THEN 
			BEGIN
				ROLLBACK;
				RAISE 'Could not delete report request id : %', rr_id USING ERRCODE = 'invalid_parameter_value';
			END;
    
	END;
    $BODY$
  LANGUAGE PLPGSQL;