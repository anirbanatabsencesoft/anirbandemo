CREATE OR REPLACE FUNCTION fn_save_shared_report_team_role_user
(   srtru_shared_report_id bigint,
    srtru_team_name character varying(255) DEFAULT NULL,
    srtru_team_id bigint DEFAULT NULL,
    srtru_team_key character varying(32) DEFAULT NULL,
    srtru_role_id bigint DEFAULT NULL,
    srtru_role_name character varying(255) DEFAULT NULL,
    srtru_user_id bigint DEFAULT NULL,
    srtru_user_key character varying(32) DEFAULT NULL
 )
RETURNS bigint
 AS
  $BODY$
	DECLARE l_id integer;
		BEGIN
			--Now insert as fresh
			INSERT INTO public.shared_report_team_role_user(shared_report_id, team_name, team_id, team_key, role_id, role_name, user_id, user_key)
													VALUES (srtru_shared_report_id, srtru_team_name, srtru_team_id, srtru_team_key, srtru_role_id, srtru_role_name, srtru_user_id, srtru_user_key)
											RETURNING id INTO l_id;
			RETURN l_id; 
			
			EXCEPTION WHEN OTHERS THEN 
				RAISE 'Could not save shared report team role user with Report name : %', srtru_shared_report_id USING ERRCODE = 'invalid_parameter_value';  		
		END
  $BODY$
  LANGUAGE PLPGSQL;