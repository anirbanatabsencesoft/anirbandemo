CREATE OR REPLACE FUNCTION public.fn_delete_shared_report
(
	sr_id BIGINT, 
	createdbyid BIGINT DEFAULT NULL, 
	createdbykey varchar(24) DEFAULT NULL
)
RETURNS INT
 AS
   $BODY$
    DECLARE v_cnt numeric;    
	BEGIN
			UPDATE public.shared_report sr
				SET modified_on = CURRENT_TIMESTAMP
				,is_deleted = TRUE			
			WHERE id = sr_id
			AND (createdbyid IS NULL OR sr.created_by_id = createdbyid) 
			AND (createdbykey IS NULL OR sr.created_by_key = createdbykey)
			AND sr.is_deleted = False;
											
			GET DIAGNOSTICS v_cnt = ROW_COUNT;
			RETURN v_cnt;

			EXCEPTION WHEN OTHERS THEN 
				RAISE 'Could not delete shared report id : %', sr_id USING ERRCODE = 'invalid_parameter_value';
			
    
	END;
    $BODY$
  LANGUAGE PLPGSQL;