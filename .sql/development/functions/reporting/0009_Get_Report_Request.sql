CREATE OR REPLACE FUNCTION public.fn_get_report_request(
	rr_id bigint DEFAULT NULL::bigint,
	rr_user_id bigint DEFAULT NULL::bigint,
	rr_user_key character varying (24) DEFAULT NULL::character varying
	)
    RETURNS TABLE(id bigint
	, user_id bigint
	, user_key character varying(24)
	, request_type character varying(255)
	, request_message text
	, response_message text
	, lookup_status_id bigint
	, scheduled_report_id bigint
	, created_on timestamp with time zone
	, modified_on timestamp with time zone
	, is_completed boolean
	, result_max_json_file_path character varying(500)
	, result_all_json_file_path character varying(500)
	, result_pdf_file_path character varying(500)
	, result_csv_file_path character varying(1000)
	, is_deleted boolean
	, report_name varchar(255)
	, criteria_plain_english text
	, report_id varchar(50)) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$

  BEGIN	  
        RETURN QUERY
        Select rr.id
				, rr.user_id
				, rr.user_key
				, rr.request_type
				, rr.request_message
				, rr.response_message
				, rr.lookup_status_id
				, rr.scheduled_report_id
				, rr.created_on
				, rr.modified_on 
				, rr.is_completed 
				, rr.result_max_json_file_path 
				, rr.result_all_json_file_path 
				, rr.result_pdf_file_path 
				, rr.result_csv_file_path 
				, rr.is_deleted
				, rr.report_name
				, rr.criteria_plain_english
				, rr.report_id
		FROM public.report_request rr 
		WHERE rr.id = rr_id
		AND (rr_user_id IS NULL OR rr.user_id = rr_user_id)
		AND (rr_user_key IS NULL OR rr.user_key = rr_user_key);

        EXCEPTION WHEN OTHERS THEN 
            RAISE 'Could not fetch report request for request Id : %', rr_id USING ERRCODE = 'invalid_parameter_value';    
		 	  
  END
  
$BODY$;

ALTER FUNCTION public.fn_get_report_request(bigint, bigint, character varying)
    OWNER TO postgres;

