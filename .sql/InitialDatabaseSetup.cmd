﻿REM pass in location to base solution directory.
set loc=%1
set at2_db_name=%2

REM - Now run files against database. Environment variables are used to make --no-password switch work for psql
REM - ENV VARIABLES NEEDED: PGDATABASE, PGHOST, PGPASSWORD, PGUSER
psql --file="%loc%\deployment\complete\CompletePostgresScript.sql" --no-password --dbname=%at2_db_name%
