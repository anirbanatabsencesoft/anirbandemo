﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Logic.SSO;
using AbsenceSoft.Web;
using AbsenceSoft.Web.ViewRendering;
using System;
using System.Configuration;
using System.Diagnostics;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace AbsenceSoft.SelfService
{
    public class MvcApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// Handles application start
        /// </summary>
        protected void Application_Start()
        {
            log4net.Config.XmlConfigurator.Configure();
            AbsenceSoft.Common.Serializers.DateTimeSerializer.Register();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            SsoConfiguration.ConfigureSso();
            new Aspose.Words.License().SetLicense("Aspose.Words.lic");

            // Initialize Aspose PDF license object
            Aspose.Pdf.License license = new Aspose.Pdf.License();
            // Set license
            license.SetLicense("Aspose.Pdf.lic");
            // Set the value to indicate that license will be embedded in the application
            license.Embedded = true;

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new CustomerViewEngine());
        }

        /// <summary>
        /// Determine whether to redirect to SSL or not
        /// </summary>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        private bool RedirectToSsl()
        {
            if (Request.IsLocal || Request.IsHealthCheck() || Request.IsSsl())
            {
                return false;
            }

            if (FormsAuthentication.RequireSSL && !Request.IsSsl())
            {
                if (!string.Equals(Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase) && !string.Equals(Request.HttpMethod, "HEAD", StringComparison.OrdinalIgnoreCase))
                {
                    throw new InvalidOperationException(string.Format("The request for resource '{0}' with verb '{1}' must use HTTPS.", Request.RawUrl, Request.HttpMethod));
                }

                string url = string.Concat("https://", Request.Url.Host, Request.RawUrl);
                Response.Redirect(url, true);
                // 301 = bad, should use 302 instead
                return true;
            }

            return false;
        }

        /// <summary>
        /// Handles the BeginRequest event
        /// </summary>
        /// <param name="send">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Application_BeginRequest(object send, EventArgs e)
        {
            //Add CSRF token identifier
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimsIdentity.DefaultNameClaimType;
            AntiForgeryConfig.SuppressIdentityHeuristicChecks = true;

            // Determine if the person is coming in via SSL, if not, determine if they should, and if so, redirect the request.
            if (RedirectToSsl())
            {
                return;
            }

            if (Request.Path.ToLowerInvariant().Contains("/bundles/"))
            {
                return;
            }

            new AuthenticationService().Using(s => s.EnsureSessionId(new HttpContextWrapper(Context)));

            // Create our instrumentation context for use to trace the entire request for this local path/URI
            string methodName = Request.Url.LocalPath;
            Context.Items["__instrumentationContext"] = new InstrumentationContext(methodName);

            // Set the expires header so we're not caching in IE, 'cause IE sucks
            Response.Expires = -1;

            Context.Items[AbsenceSoft.Data.Security.User.CURRENT_USER_PORTAL_CONTEXT] = "ESS";
            if (Context.Request != null && Context.Request.IsLocal)
            {
                string employerId = ConfigurationManager.AppSettings["ESSEmployerId"];
                Current.Employer(Employer.GetById(employerId));
                return;
            }

            string subDomain = Request.Url.Host.Split('.')[0];
            if (Request.Url.HostNameType == UriHostNameType.IPv4 || Request.Url.HostNameType == UriHostNameType.IPv6)
            {
                subDomain = Request.Url.Host.ToLowerInvariant();
            }

            Customer customerByUrl = Customer.GetByUrl(subDomain);
            Employer employerByUrl = Employer.GetByUrl(subDomain);
            SetCurrentCustomer(customerByUrl);
            if(employerByUrl != null)
            {
                Current.Employer(employerByUrl);
            }
        }

        /// <summary>
        /// Handles the Error event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            if (ex != null)
            {
                Log.Error("Unhandled Exception Occurred", ex.GetBaseException());

                //if antiforgery related redirect to login
                if (ex is HttpAntiForgeryException)
                {
                    Response.Clear();
                    Server.ClearError();
                    Response.Redirect("/login", true);
                }
            }
            else
            {
                Log.Error(new
                {
                    Message = "Unknown Exception Occurred",
                    StackTrace = new StackTrace(true).ToString()
                });
            }
        }

        /// <summary>
        /// Handles the EndRequest event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Application_EndRequest(object sender, EventArgs e)
        {
            if (Context.Items["__instrumentationContext"] != null)
            {
                ((InstrumentationContext)Context.Items["__instrumentationContext"]).Dispose();
            }
        }

        /// <summary>
        /// If we have a logged in user, sets customer based on user
        /// Otherwise sets current customer to the passed in customer
        /// </summary>
        /// <param name="customerByUrl"></param>
        private void SetCurrentCustomer(Customer customerByUrl)
        {
            var currentUser = Current.User();
            if (currentUser != null)
            {
                Current.Customer(currentUser.Customer);
                return;
            }
            
            Current.Customer(customerByUrl);
        }
    }
}
