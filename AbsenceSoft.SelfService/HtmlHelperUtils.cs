﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Communications;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Logic.Tasks;
using AbsenceSoft.Web;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using System.Xml;

namespace AbsenceSoft.SelfService
{
    public static class HtmlHelperUtils
    {
        private static List<SelectListItem> LoadSelectListFromXmlData(string filePath, string nodeName, string placeholder, object model, string attributeToMatch = null, string valueToMatch = null)
        {
            var doc = new XmlDocument();
            doc.Load(HttpContext.Current.Server.MapPath(filePath));
            var selectList = new List<SelectListItem> {
                new SelectListItem {
                 Text = placeholder,
                 Selected = model == null,
                 Value = string.Empty
             }
            };

            var xmlNodeList = doc.SelectNodes(nodeName);
            if (xmlNodeList == null)
                return selectList;

            foreach (XmlNode node in xmlNodeList)
            {
                if (!XMLNodeIsValid(node, attributeToMatch, valueToMatch))
                    continue; 

                var value = node.Attributes["code"].Value;
                selectList.Add(
                    new SelectListItem
                    {
                        Text = node.InnerText,
                        Value = value,
                        Selected = value.Equals(model)
                    }
                );
            }

            return selectList;
        }

        private static bool XMLNodeIsValid(XmlNode node, string attributeToMatch, string valueToMatch)
        {
            if (node.Attributes == null)
                return false;

            if (!string.IsNullOrEmpty(attributeToMatch) && !string.IsNullOrEmpty(valueToMatch) && node.Attributes[attributeToMatch].Value != valueToMatch)
                return false;

            return true;
        }

        public static MvcHtmlString CountryDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression) where TModel : class
        {
            return htmlHelper.CountryDropDownListFor(expression, null);
        }

        public static MvcHtmlString CountryDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes) where TModel : class
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var countries = LoadSelectListFromXmlData("~/App_Data/countries.xml", "//country", "Select a Country", metadata.Model);
            return htmlHelper.DropDownListFor(expression, countries, htmlAttributes);
        }

        /// <summary>
        ///  Displays a select list of states loaded from App_Data/states.xml
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="optionLabel"></param>
        /// <param name="selectedValue"></param>
        /// <param name="required"></param>
        /// <param name="cssClass"></param>
        /// <returns></returns>
        public static HtmlString StateDropDown(this HtmlHelper helper, string id, string name, string optionLabel, object selectedValue, bool required, string cssClass = null)
        {
            var doc = new XmlDocument();
            doc.Load(HttpContext.Current.Server.MapPath("~/App_Data/states.xml"));

            var b = new StringBuilder();

            b.AppendFormat("<select name='{0}' {1} id='{2}'{3}>",
                name,
                required
                    ? "required"
                    : string.Empty,
                id,
                string.IsNullOrWhiteSpace(cssClass)
                    ? string.Empty
                    : string.Format(" class='{0}'", cssClass));

            if (!string.IsNullOrEmpty(optionLabel))
                b.AppendFormat("<option value=''>{0}</option>", optionLabel);

            var xmlNodeList = doc.SelectNodes("//state");
            if (xmlNodeList != null)
                foreach (XmlNode node in xmlNodeList)
                {
                    if (node.Attributes["country"].Value != "US")
                        continue;

                    var selected = string.Empty;
                    var code = string.Empty;
                    if (node.Attributes != null && node.Attributes["code"].Value != null)
                        code = node.Attributes["code"].Value;

                    if (code == selectedValue as string)
                        selected = "selected='selected'";

                    b.Append(string.Format("<option value='{0}' {2}>{1}</option>", code, node.InnerText, selected));
                }

            b.Append("</select>");

            return new HtmlString(b.ToString());
        }

        public static MvcHtmlString RegionLabelFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string country, object htmlAttributes)
        {
            var doc = new XmlDocument();
            doc.Load(HttpContext.Current.Server.MapPath("~/App_Data/countries.xml"));
            var node = doc.SelectSingleNode(string.Format("//country[@code='{0}']", country));
            if (node == null)
                return htmlHelper.LabelFor(expression, "Region", htmlAttributes);

            XmlAttribute regionNameAttribute = node.Attributes["region-name"];
            if (regionNameAttribute == null)
                return htmlHelper.LabelFor(expression, "Region", htmlAttributes);

            string regionName = regionNameAttribute.Value;
            if (string.IsNullOrEmpty(regionName))
                return htmlHelper.LabelFor(expression, "Region", htmlAttributes);

            return htmlHelper.LabelFor(expression, regionName, htmlAttributes);
        }

        public static MvcHtmlString StateDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression) where TModel : class
        {
            return htmlHelper.StateDropDownListFor(expression, null);
        }

        public static MvcHtmlString StateDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes) where TModel : class
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            List<SelectListItem> states = LoadSelectListFromXmlData("~/App_Data/states.xml", "//state", "Select a State", metadata.Model);
            return htmlHelper.DropDownListFor(expression, states, htmlAttributes);
        }


        public static MvcHtmlString StateEditorFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression) where TModel : class
        {
            return htmlHelper.StateEditorFor(expression, "US", null);
        }

        public static MvcHtmlString StateEditorFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string country) where TModel : class
        {
            return htmlHelper.StateEditorFor(expression, country, null);
        }

        public static MvcHtmlString StateEditorFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes) where TModel : class
        {
            return htmlHelper.StateEditorFor(expression, "US", htmlAttributes);
        }

        public static MvcHtmlString StateEditorFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string country, object htmlAttributes) where TModel : class
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var states = LoadSelectListFromXmlData("~/App_Data/states.xml", "//state", "Select a State", metadata.Model, "country", country);
            if (states.Count > 1)
                return htmlHelper.DropDownListFor(expression, states, htmlAttributes);

            return htmlHelper.TextBoxFor(expression, htmlAttributes);
        }

        public static MvcHtmlString EmployerDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression) where TModel : class
        {
            return htmlHelper.EmployerDropDownListFor(expression, null);
        }

        public static MvcHtmlString EmployerDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes, string text = "") where TModel : class
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            using (var employerService = new EmployerService())
            {
                var employers = employerService.GetEmployersForUser(Current.User(), Permission.CreateEmployee.ToString());
                var employersDropDown = employers.Select(
                    e => new SelectListItem
                    {
                        Value = e.Id,
                        Text = e.Name,
                        Selected = e.Id.Equals(metadata.Model)
                    }).OrderBy(o => o.Text).ToList();
                employersDropDown.Insert(0, new SelectListItem { Value = string.Empty, Text = text });
                return htmlHelper.DropDownListFor(expression, employersDropDown, htmlAttributes);
            }
        }

        public static MvcHtmlString DemandDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression) where TModel : class
        {
            return htmlHelper.DemandDropDownListFor(expression, null);
        }

        public static MvcHtmlString DemandDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, Dictionary<string, object> htmlAttributes) where TModel : class
        {
            if (htmlAttributes.ContainsKey("readonly"))
                return htmlHelper.DropDownListFor(expression, new List<SelectListItem>(), htmlAttributes);

            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            using (var demandService = new DemandService(Current.Customer(), Current.Employer(), Current.User()))
            {
                var demands = demandService.GetDemands();
                var demandsDropDown = demands.Select(
                e => new SelectListItem
                {
                    Value = e.Id,
                    Text = e.Name,//.Length > 50 ? e.Name.Substring(0, 47) + "..." : e.Name,
                    Selected = (string)metadata.Model == e.Id
                }).ToList();
                return htmlHelper.DropDownListFor(expression, demandsDropDown, "Select a demand", htmlAttributes);
            }
        }

        public static MvcHtmlString DemandTypeDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression) where TModel : class
        {
            return htmlHelper.DemandTypeDropDownListFor(expression, null);
        }

        public static MvcHtmlString DemandTypeDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes) where TModel : class
        {
            dynamic model = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData).Model;
            var selectedDemandTypes = new List<string>();
            if (model != null)
            {
                if (model is List<string>)
                {
                    selectedDemandTypes = model;
                }
                else
                {
                    selectedDemandTypes.Add(model.ToString());
                }
            }

            using (var demandService = new DemandService(Current.Customer(), Current.Employer(), Current.User()))
            {
                var demandTypes = demandService.GetDemandTypes();
                var demandTypesDropDown = demandTypes.Select(
                    e => new SelectListItem
                    {
                        Value = e.Id,
                        Text = e.Name.Length > 50 ? e.Name.Substring(0, 47) + "..." : e.Name,
                        Selected = selectedDemandTypes.Any(s => s == e.Id)
                    }).ToList();
                return htmlHelper.ListBoxFor(expression, demandTypesDropDown, htmlAttributes);
            }
        }

        public static MvcHtmlString PayScheduleDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes) where TModel : class
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            string employerId = null;
            if (Current.Employer() != null)
            {
                employerId = Current.Employer().Id;
            }
            else if (metadata.Container.Get<object>("EmployerId") != null)
            {
                employerId = metadata.Container.Get<string>("EmployerId");
            }
            else
            {
                return htmlHelper.DropDownListFor(expression, new List<SelectListItem>(), htmlAttributes);
            }

            var defaultPayScheduleId = Current.Employer() == null
                ? Employer.GetById(employerId).DefaultPayScheduleId
                : Current.Employer().DefaultPayScheduleId;

            var employerPaySchedules = new PayScheduleService().PayScheduleForEmployer(employerId);
            var paySchedulesDropDown = employerPaySchedules.Select(
                p => new SelectListItem
                {
                    Value = p.Id,
                    Text = p.Name,
                    Selected = (metadata.Model != null && metadata.Model.ToString() == p.Id)
                        || (metadata.Model == null && defaultPayScheduleId == p.Id)
                }).ToList();

            return htmlHelper.DropDownListFor(expression, paySchedulesDropDown, htmlAttributes);
        }

        public static MvcHtmlString OrganizationDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes) where TModel : class
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            string employerId = null;
            if (Current.Employer() != null)
            {
                employerId = Current.Employer().Id;
            }
            else if (metadata.Container.Get<string>("EmployerId") != null)
            {
                employerId = metadata.Container.Get<string>("EmployerId");
            }
            else
            {
                return htmlHelper.DropDownListFor(expression, new List<SelectListItem>(), htmlAttributes);
            }

            var selectedOrganizations = new List<string>();
            if (metadata.Model != null)
            {
                if (metadata.Model is List<string>)
                {
                    selectedOrganizations = (List<string>)metadata.Model;
                }
                else if (metadata.Model != null)
                {
                    selectedOrganizations.Add(metadata.Model.ToString());
                }
            }
            var employerOrganizations = Organization.AsQueryable()
                .Where(o => o.EmployerId == employerId && o.TypeCode == OrganizationType.OfficeLocationTypeCode)
                .ToList();
            var organizationDropDown = employerOrganizations.Select(
                p => new SelectListItem
                {
                    Value = p.Code,
                    Text = p.Name,
                    Selected = selectedOrganizations.Contains(p.Code)
                }).OrderBy(i => i.Text).ToList();
            return htmlHelper.DropDownListFor(expression, organizationDropDown, "Select a Location", htmlAttributes);
        }

        public static MvcHtmlString NoteCategoryDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes) where TModel : class
        {
            using (var notesService = new NotesService(Current.Customer(), Current.Employer(), Current.User()))
            {
                var categories = notesService.GetAllNoteCategories();
                dynamic model = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData).Model;
                var selectedCategoryCodes = new List<string>();
                if (model != null)
                {
                    if (model is List<string>)
                    {
                        selectedCategoryCodes = model;
                    }
                    else
                    {
                        selectedCategoryCodes.Add(model.ToString());
                    }
                }
                var options = new List<SelectListItem>
                {
                    new SelectListItem
                {
                    Value = null,
                    Text = null
                    }
                };
                BuildNoteCategorySelectList(options, categories, selectedCategoryCodes, null);

                return htmlHelper.DropDownListFor(expression, options, htmlAttributes);
            }
        }

        private static List<SelectListItem> BuildNoteCategorySelectList(List<SelectListItem> currentOptions, List<NoteCategory> categories, List<string> selectedCategoryCodes, string prefix = null)
        {
            foreach (var category in categories)
            {
                currentOptions.Add(
                    new SelectListItem
                    {
                        Value = category.Code,
                        Text = string.Format("{0}{1}", prefix, category.Name),
                        Selected = selectedCategoryCodes.Any(s => s == category.Code)
                    });
                BuildNoteCategorySelectList(currentOptions, category.ChildCategories, selectedCategoryCodes, string.Format("{0}{1}", "-", prefix));
            }

            return currentOptions;
        }

        public static MvcHtmlString JobDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string officeLocation, object htmlAttributes) where TModel : class
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            if (metadata == null || metadata.Container == null)
                return htmlHelper.DropDownListFor(expression, new List<SelectListItem>(0), "Select a Job", htmlAttributes);

            var employerId = Current.Employer() == null
                ? metadata.Container.Get<object>("EmployerId") as string
                : Current.Employer().Id;

            if (string.IsNullOrWhiteSpace(officeLocation))
            {
                var eeId = metadata.Container.Get<object>("EmployeeId")
                           ?? metadata.Container.Get<object>("Id");

                var employeeId = eeId == null
                    ? null
                    : eeId.ToString();

                if (!string.IsNullOrWhiteSpace(employeeId))
                {
                    var emp = Employee.GetById(employeeId);
                    if (emp != null)
                    {
                        var loc = emp.GetOfficeLocation();
                        if (loc != null)
                            officeLocation = loc.Code; //TODO: Office Location not used? ^^
                    }
                }
            }

            var jobs = Job.Repository.Collection.FindAs<BsonDocument>(Job.Query.And(
                Job.Query.EQ(j => j.EmployerId, employerId),
                Job.Query.In(j => j.OrganizationCode, new string[] { null, string.Empty, officeLocation }),
                Job.Query.IsNotDeleted()
            )).SetFields(Fields.Include("Code", "Name").Exclude("_id")).ToList();

            var dropDown = jobs.Select(
                p => new SelectListItem
                {
                    Value = p["Code"].ToString(),
                    Text = p["Name"].ToString(),
                    Selected = metadata.Model != null && metadata.Model.ToString() == p["Code"].ToString()
                }).OrderBy(s => s.Text).ToList();

            return htmlHelper.DropDownListFor(expression, dropDown, "Select a Job", htmlAttributes);
        }

        public static MvcHtmlString NecessityDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes) where TModel : class
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var employerId = Current.Employer() == null
                ? metadata.Container.Get<object>("EmployerId").ToString()
                : Current.Employer().Id;

            var necessities = Necessity.AsQueryable().Where(j => j.EmployerId == employerId || j.EmployerId == null).ToList();
            var groups = new Dictionary<NecessityType, SelectListGroup>();
            foreach (var val in Enum.GetValues(typeof(NecessityType)).Cast<NecessityType>().ToList())
                groups.Add(val, new SelectListGroup { Name = val.ToString().SplitCamelCaseString() });
            var dropDown = necessities.Select(
                p => new SelectListItem
                {
                    Value = p.Id,
                    Text = p.Name.Length > 50 ? p.Name.Substring(0, 47) + "..." : p.Name,
                    Selected = metadata.Model != null && metadata.Model.ToString() == p.Id,
                    Group = groups[p.Type]
                }).ToList();
            return htmlHelper.DropDownListFor(expression, dropDown, "Select a Necessity", htmlAttributes);
        }

        public static MvcHtmlString ContactTypeDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            return ContactTypeDropDownListFor(htmlHelper, expression, null);
        }

        public static MvcHtmlString ContactTypeDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes)
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var currentEmployer = Current.Employer();
            string employerId = null;
            if (currentEmployer != null)
                employerId = currentEmployer.Id;

            var customerId = Current.Customer().Id;

            using (var contactTypeService = new ContactTypeService())
            {
                var contactTypes = contactTypeService.GetContactTypes(customerId, employerId);
                var dropDown = contactTypes.Select(
                    ct => new SelectListItem
                    {
                        Value = ct.Code,
                        Text = ct.Name,
                        Selected = ct.Code.Equals(metadata.Model)
                    });

                return htmlHelper.DropDownListFor(expression, dropDown, "Select A Contact Type", htmlAttributes);
            }
        }

        public static MvcHtmlString EmployerContactTypeDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            return EmployerContactTypeDropDownListFor(htmlHelper, expression, null);
        }

        public static MvcHtmlString EmployerContactTypeDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes)
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            using (var contactTypeService = new ContactTypeService(Current.User()))
            {
                var contactTypes = contactTypeService.GetContactTypesForCustomer();
                var dropDown = contactTypes.Select(
                    ct => new SelectListItem
                    {
                        Value = ct.Code,
                        Text = ct.Name,
                        Selected = ct.Code.Equals(metadata.Model)
                    });

                return htmlHelper.DropDownListFor(expression, dropDown, "Select A Contact Type", htmlAttributes);
            }
        }

        public static MvcHtmlString AbsenceReasonCategoryDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            return htmlHelper.AbsenceReasonCategoryDropDownListFor(expression, null);
        }

        public static MvcHtmlString AbsenceReasonCategoryDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes)
        {
            using (var policyService = new PolicyService(Current.Customer(), Current.Employer(), Current.User()))
            {
                var absenceReasons = policyService.GetAllAbsenceReasons();
                var categories = absenceReasons
                    .Where(ar => !string.IsNullOrWhiteSpace(ar.Category))
                    .Select(ar => ar.Category)
                    .Distinct()
                    .ToList();

                categories.Add("New Category");
                var selectList = categories.Select(c => new SelectListItem()
                {
                    Value = c,
                    Text = c
                });

                return htmlHelper.DropDownListFor(expression, selectList, "Select a category", htmlAttributes);
            }
        }

        public static MvcHtmlString CustomerServiceOptionDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            return htmlHelper.CustomerServiceOptionDropDownListFor(expression, null, null);
        }

        public static MvcHtmlString CustomerServiceOptionDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes)
        {
            return htmlHelper.CustomerServiceOptionDropDownListFor(expression, null, htmlAttributes);
        }

        public static MvcHtmlString CustomerServiceOptionDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string optionLabel, object htmlAttributes)
        {
            using (var customerServiceOptionService = new CustomerServiceOptionService(Current.Customer(), Current.Employer(), Current.User()))
            {
                var serviceOptions = customerServiceOptionService.GetCustomerServiceOptions();
                var selectList = serviceOptions.Select(so => new SelectListItem()
                {
                    Text = so.Value,
                    Value = so.Id
                });

                return htmlHelper.DropDownListFor(expression, selectList, optionLabel, htmlAttributes);
            }
        }

        // Creates class attributes in <body> based on the controller and action for CSS purposes
        public static BodyTag Body(this HtmlHelper html)
        {
            return html.Body(null);
        }

        public static BodyTag Body(this HtmlHelper html, object htmlAttributes)
        {
            return BodyHelper(html, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }

        public static BodyTag Body(this HtmlHelper html, IDictionary<string, object> htmlAttributes)
        {
            return BodyHelper(html, htmlAttributes);
        }

        internal static BodyTag BodyHelper(HtmlHelper html, IDictionary<string, object> htmlAttributes = null)
        {
            var tagBuilder = new TagBuilder("body");
            tagBuilder.MergeAttributes(htmlAttributes, true);
            var actionName = html.ViewContext.RouteData.Values["action"];
            var controllerName = html.ViewContext.RouteData.Values["controller"];
            var htmlClassName = string.Format("controller-{0} action-{1} {0}-{1}", controllerName, actionName).ToLowerInvariant();
            tagBuilder.AddCssClass(htmlClassName);
            var htmlText = tagBuilder.ToString(TagRenderMode.StartTag);

            html.ViewContext.Writer.Write(htmlText + "\r\n");

            return new BodyTag(html);
        }

        public class BodyTag : IDisposable
        {
            private readonly HtmlHelper _html;

            public BodyTag(HtmlHelper html)
            {
                _html = html;
            }

            public void Dispose()
            {
                _html.ViewContext.Writer.Write("</body> \r\n");
            }
        }

        public static MvcHtmlString AdministrationLink(this HtmlHelper helper, string linkText, string routeName, object htmlAttributes)
        {
            var currentEmployer = Current.Employer();
            return helper.RouteLink(linkText, routeName, currentEmployer == null
                ? null
                : new { employerId = currentEmployer.Id }, htmlAttributes);
        }

        public static MvcHtmlString AdministrationLink(this HtmlHelper helper, string linkText, string routeName)
        {
            return helper.AdministrationLink(linkText, routeName, null);
        }


        public static string AdministrationRouteUrl(this UrlHelper urlHelper, string routeName)
        {
            return urlHelper.AdministrationRouteUrl(routeName, null);
        }

        public static string AdministrationRouteUrl(this UrlHelper urlHelper, string routeName, object routeValues)
        {
            var currentEmployer = Current.Employer();
            var dictionaryValues = HtmlHelper.AnonymousObjectToHtmlAttributes(routeValues);
            if (currentEmployer != null)
            {
                if (dictionaryValues.ContainsKey("employerId"))
                {
                    dictionaryValues["employerId"] = currentEmployer.Id;
                }
                else
                {
                    dictionaryValues.Add("employerId", currentEmployer.Id);
                }
            }
            return urlHelper.RouteUrl(routeName, dictionaryValues);
        }

        public static MvcHtmlString PaperworkListBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            return htmlHelper.PaperworkListBoxFor(expression, null);
        }

        public static MvcHtmlString PaperworkListBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes)
        {
            using (var templateService = new TemplateService(Current.Customer(), Current.Employer(), Current.User()))
            {
                var paperwork = templateService.GetAllPaperworkTemplates();
                IEnumerable<SelectListItem> selectList = paperwork.Select(p => new SelectListItem()
                {
                    Text = p.Name,
                    Value = p.Code
                });

                return htmlHelper.ListBoxFor(expression, selectList, htmlAttributes);
            }
        }

        /// <summary>
        /// Creates a Drop Down List using all communications for the specified model property.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        public static MvcHtmlString CommunicationDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            return htmlHelper.CommunicationDropDownListFor(expression, null);
        }

        /// <summary>
        /// Creates a drop down list with the specified html attributes using all communications for the specified model property
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="htmlAttributes">The HTML attributes.</param>
        /// <returns></returns>
        public static MvcHtmlString CommunicationDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes)
        {
            return htmlHelper.CommunicationDropDownListFor(expression, null, htmlAttributes);
        }

        /// <summary>
        /// Creates a drop down list with the specified html attributes and option label using all communications for the specified model property
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="optionLabel">The option label.</param>
        /// <param name="htmlAttributes">The HTML attributes.</param>
        /// <returns></returns>
        public static MvcHtmlString CommunicationDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string optionLabel, object htmlAttributes, bool isCustom = false, bool hasWordDocType = false)
        {
            using (var templateService = new TemplateService(Current.Customer(), Current.Employer(), Current.User()))
            {
                var communications = templateService.GetAllCommunicationTemplates().OrderBy(ct => ct.Name);
                if (isCustom)
                {
                    communications = communications.Where(ct => ct.IsCustom == true).OrderBy(ct => ct.Name);
                }
                if (hasWordDocType)
                {
                    communications = communications.Where(ct => ct.DocType == Data.Enums.DocumentType.MSWordDocument).OrderBy(ct => ct.Name);
                }
                IEnumerable<SelectListItem> selectList = communications.Select(p => new SelectListItem()
                {
                    Text = p.Name,
                    Value = p.Code
                });

                return htmlHelper.DropDownListFor(expression, selectList, optionLabel, htmlAttributes);
            }
        }

        public static MvcHtmlString RolesListBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            return htmlHelper.RolesListBoxFor(expression, null);
        }

        public static MvcHtmlString RolesListBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes)
        {
            using (var authService = new AuthenticationService())
            {
                dynamic model = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData).Model;
                var roles = authService.GetRoles(Customer.Current.Id).OrderBy(x => x.Name);

                var selectedRoles = new List<string>();
                if (model != null)
                {
                    if (model is List<string>)
                    {
                        selectedRoles = model;
                    }
                    else
                    {
                        selectedRoles.Add(model.ToString());
                    }
                }

                IEnumerable<SelectListItem> selectList = roles.Select(p => new SelectListItem()
                {
                    Text = p.Name,
                    Value = p.Id,
                    Selected = selectedRoles.Any(s => s == p.Id)
                });

                return htmlHelper.ListBoxFor(expression, selectList, htmlAttributes);
            }
        }

        public static MvcHtmlString AbsenceReasonListBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes)
        {
            using (var policyService = new PolicyService(Current.Customer(), Current.Employer(), Current.User()))
            {
                dynamic model = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData).Model;
                var reasons = policyService.GetAllAbsenceReasons().OrderBy(x => x.Name);

                var selectedReasons = new List<string>();
                if (model != null)
                {
                    if (model is List<string>)
                    {
                        selectedReasons = model;
                    }
                    else
                    {
                        selectedReasons.Add(model.ToString());
                    }
                }

                IEnumerable<SelectListItem> selectList = reasons.Select(p => new SelectListItem()
                {
                    Text = p.Name,
                    Value = p.Id,
                    Selected = selectedReasons.Any(s => s == p.Id)
                });

                return htmlHelper.ListBoxFor(expression, selectList, htmlAttributes);
            }
        }

        public static MvcHtmlString RelationshipDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression) where TModel : class
        {
            return htmlHelper.RelationshipDropDownListFor(expression, null, null);
        }

        public static MvcHtmlString RelationshipDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string optionLabel, object htmlAttributes) where TModel : class
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            using (var contactTypeService = new ContactTypeService())
            {
                var relationships = contactTypeService.GetContactTypes(Current.Customer().Id, null);
                IEnumerable<SelectListItem> items = relationships.Select(r => new SelectListItem()
                {
                    Text = r.Name,
                    Value = r.Code,
                    Selected = r.Code.Equals(metadata.Model)
                });

                return htmlHelper.DropDownListFor(expression, items, optionLabel, htmlAttributes);
            }
        }

        public static MvcHtmlString WorkRelatedFieldEditorFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string formFieldCode, string optionLabel, string placeholder, object htmlAttributes) where TModel : class
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var customerId = Current.Customer().Id;
            string selectedOption = null;
            OshaFormField oshaFormField = new OshaFormField();
            if (metadata.Model != null)
            {
                if (metadata.Model != null)
                {
                    selectedOption = metadata.Model.ToString();
                }
            }
            using (var oshaFormFieldService = new OshaFormFieldService(Current.Customer(), Current.Employer(), Current.User()))
            {
                oshaFormField = oshaFormFieldService.GetOshaFormFieldByCode(formFieldCode);
            }
            var oshaFormFieldOptions = oshaFormField.Options.Select(whatHarmedTheEmployee => new { Code = whatHarmedTheEmployee, Name = whatHarmedTheEmployee }).ToList();
            var oshaFormFieldDropDown = oshaFormFieldOptions.Select(
                p => new SelectListItem
                {
                    Value = p.Code,
                    Text = p.Name,
                    Selected = selectedOption == null ? false : selectedOption.Contains(p.Name.ToString())
                }).ToList();

            if (oshaFormFieldDropDown.Count > 0)
            {
                //Add saved value for previousley created cases, handling backward compatibility
                if (selectedOption != null && !oshaFormFieldDropDown.Select(x => x.Text).ToList().Contains(selectedOption))
                {
                    oshaFormFieldDropDown.Add(new SelectListItem
                    {
                        Value = selectedOption,
                        Text = selectedOption,
                        Selected = true
                    });
                }
                return htmlHelper.DropDownListFor(expression, oshaFormFieldDropDown, optionLabel, htmlAttributes);
            }
            else
            {
                var updatedHtmlAttributes = new RouteValueDictionary(htmlAttributes);
                updatedHtmlAttributes.Add("placeholder", placeholder);
                return htmlHelper.TextBoxFor(expression, updatedHtmlAttributes);
            }
        }
    }
}