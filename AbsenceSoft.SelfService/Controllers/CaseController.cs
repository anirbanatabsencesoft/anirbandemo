﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Tasks;
using AbsenceSoft.SelfService.Models;
using AbsenceSoft.SelfService.Models.Cases;
using AbsenceSoft.Web;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.SelfService.Controllers
{
    public class CaseController : BaseSelfServiceController
    {
        #region Case Attachments

        [Secure("ViewCase")]
        [HttpGet, Route("Case/{caseId}", Name = "ViewCase"), Title("View Case")]
        public ActionResult ViewCase(string caseId, string osha, DateTime? date = null, string viewFrom="")
        {
            if (!date.HasValue)
                date = DateTime.Now;

            using (CaseService caseService = new CaseService())
            {
                string[] caseIds = new string[] { caseId };
                IEnumerable<NoteViewModel> notes = GetCaseNotes(caseIds);
                IEnumerable<AttachmentViewModel> attachments = GetCaseAttachments(caseIds);
                CaseViewModel theCase = new CaseViewModel(caseService.GetCaseById(caseId)) { Notes = notes, Attachments = attachments };
                if (osha == "1")
                {
                    ViewBag.osha = "osha";
                }
                return View(theCase);
            }
        }

        [Secure("ViewCase")]
        [HttpPost, Route("Case/UpdateContactInfo", Name = "UpdateContactInfo"), Title("View Case")]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateContactInfo(CaseViewModel caseContactInfo)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("_CaseContactInfo", caseContactInfo);
            }

            using (CaseService caseService = new CaseService())
            {
                Data.Cases.Case theCase = caseService.GetCaseById(caseContactInfo.Id);
                if (theCase == null)
                {
                    throw new ArgumentNullException("theCase");
                }

                if (caseContactInfo.AlternateInfo != null)
                {
                    caseContactInfo.AlternateInfo.ApplyAlternateInfo(theCase);
                }
                else
                {
                    theCase.Employee.Info.AltAddress = null;
                    theCase.Employee.Info.AltEmail = null;
                    theCase.Employee.Info.AltPhone = null;
                }
                theCase.OnSavedNOnce((o, c) => c.Entity.WfOnContactInfoChangeEvent(Current.User()));
                caseService.UpdateCase(theCase);
            }

            return PartialView("_CaseContactInfo", caseContactInfo);
        }

        [Secure("ViewCase")]
        [HttpPost, Route("Case/UpdateWorkRelatedInfo", Name = "UpdateWorkRelatedInfo"), Title("View Case")]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateWorkRelatedInfo(CaseViewModel caseWorkRelatedViewInfo)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("_CaseContactInfo", caseWorkRelatedViewInfo);
            }

            using (CaseService caseService = new CaseService())
            {
                Data.Cases.Case theCase = caseService.GetCaseById(caseWorkRelatedViewInfo.WorkRelatedInfo.CaseId);
                if (theCase == null)
                {
                    throw new ArgumentNullException("theCase");
                }

                theCase.WorkRelated = caseWorkRelatedViewInfo.WorkRelatedInfo.ApplyModel(theCase);
                if (theCase.WorkRelated != null && caseWorkRelatedViewInfo.WorkRelatedInfo.ProviderContact != null && !string.IsNullOrWhiteSpace(caseWorkRelatedViewInfo.WorkRelatedInfo.ProviderContact.ContactTypeCode))
                {
                    theCase.WorkRelated.Provider.CustomerId = theCase.CustomerId;
                    theCase.WorkRelated.Provider.EmployerId = theCase.EmployerId;
                    theCase.WorkRelated.Provider.EmployeeId = theCase.Employee.Id;
                    theCase.WorkRelated.Provider.Save();
                }
                else
                {
                    theCase.WorkRelated.Provider = null;
                }

                theCase.OnSavedNOnce((o, r) => r.Entity.WfOnWorkRelatedReportingChanged(Current.User()));
                theCase.WorkRelated = caseWorkRelatedViewInfo.WorkRelatedInfo.ApplyModel(theCase);
                caseService.UpdateCase(theCase);
            }

            return PartialView("_WorkRelatedView", caseWorkRelatedViewInfo);
        }


        [Secure("ViewAttachment")]
        [HttpGet, Route("CaseAttachment/{caseId}/Download/{attachmentId}", Name = "DownloadCaseAttachment")]
        public ActionResult DownloadCaseAttachment(string caseId, string attachmentId)
        {
            using (AttachmentService attachmentService = new AttachmentService())
            {
                Data.Communications.Attachment attachment = attachmentService.GetAttachment(attachmentId);
                if (attachment == null || attachment.CaseId != caseId || string.IsNullOrWhiteSpace(attachment.FileId))
                {
                    return HttpNotFound("Attachment or file not found for this case");
                }

                attachment = attachmentService.DownloadAttachment(attachment);

                if (attachment.File == null || attachment.File.LongLength == 0)
                {
                    return HttpNotFound("Attachment or file not found for this case");
                }

                return File(attachment.File, attachment.ContentType, attachment.FileName);
            }
        }

        [Secure("AttachFile")]
        [HttpPost, Route("Cases/{caseId}/Attachments/{attachmentId}", Name = "DeleteAttachment")]
        [ValidateApiAntiForgeryToken]
        public JsonResult DeleteAttachment(string caseId, string attachmentId)
        {
            using (AttachmentService svc = new AttachmentService())
            {
                svc.DeleteAttachment(attachmentId);
                return Json(new { success = true, message = "Attachment successfully deleted" });
            }
        }

        [Secure("AttachFile")]
        [HttpPost, Route("Cases/CreateAttachment", Name = "CreateCaseAttachment")]
        [ValidateApiAntiForgeryToken]
        public JsonResult CreateCaseAttachment(AttachmentViewModel viewModel, HttpPostedFileBase attachment)
        {
            try
            {
                using (AttachmentService svc = new AttachmentService())
                {
                    AttachmentType type = viewModel.Type.HasValue ? viewModel.Type.Value : AttachmentType.Other;
                    svc.CreateAttachment(viewModel.EmployeeId, viewModel.CaseId, type, viewModel.Description, attachment);
                }

                return Json(new JsonResponse()
                {
                    Error = false
                });
            }
            catch (Exception ex)
            {
                return Json(new JsonResponse()
                {
                    Error = true,
                    Messages = new List<string>() { ex.Message }
                });
            }

        }

        #endregion

        #region Case Notes                

        [Secure]
        [HttpPost, Route("Case/CreateNote", Name = "CreateNote")]
        [ValidateAntiForgeryToken]
        public ActionResult CreateNote(NoteViewModel note)
        {
            if (!ModelState.IsValid)
            {
                return Json(new JsonResponse(ModelState));
            }

            try
            {
                Data.Security.User user = Current.User();
                using (NotesService notesService = new NotesService(user))
                {
                    CaseNote newNote = note.ApplyToDataModel(user.CustomerId, user.EmployerId);
                    notesService.SaveNote(newNote);
                    return RedirectViaJavaScript(Url.RouteUrl(HomeRoute));
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return Json(new JsonResponse(ModelState));
            }
        }

        [Secure]
        [HttpDelete, Route("DeleteCaseNote/{noteid}", Name = "DeleteCaseNote")]
        public ActionResult DeleteCaseNote(string noteid)
        {
            using (NotesService noteService = new NotesService(CurrentUser))
            {
                noteService.DeleteCaseNote(noteid);
            }
            return RedirectViaJavaScript(Url.RouteUrl(HomeRoute));
        }
        #endregion

        [HttpGet, Route("Case/ViewHome")]
        public ActionResult ViewHome()
        {
            return RedirectToRoute('/');
        }
    }
}