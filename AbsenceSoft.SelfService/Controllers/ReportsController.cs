﻿using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Reporting;
using AbsenceSoft.SelfService.Models;
using AbsenceSoft.SelfService.Models.Employees;
using AbsenceSoft.Web;
using AbsenceSoft.SelfService.Models.Reports;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.SelfService.Common;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.SelfService.Controllers
{
    public class ReportsController : BaseSelfServiceController
    {
        // GET: Reports
        [Secure("MyEmployeesDashboard")]
        [HttpGet, Route("MyReports", Name = "MyReports"), Title("Reports")]
        public ActionResult MyReports()
        {
            ESSReportModel model = new ESSReportModel();

            //get all available reports
            var rs = new ReportService();

            //group the reports
            model.TeamReports = CategorizeReport(rs.GetAllESSReportsByType(ESSReportType.TeamReport));
            model.OrganizationReports = CategorizeReport(rs.GetAllESSReportsByType(ESSReportType.OrganizationReport));

            return View(model);
        }


        /// <summary>
        /// Private method to process the report list by category
        /// </summary>
        /// <param name="reports"></param>
        /// <returns></returns>
        private List<ReportListModel> CategorizeReport(List<BaseReport> reports)
        {
            List<ReportListModel> rpl = new List<ReportListModel>();

            //get categories
            var categories = reports.GroupBy(p => p.Category).Select(p => p.FirstOrDefault()).ToList();
            //iterate
            foreach(var cat in categories)
            {
                ReportListModel model = new ReportListModel();
                
                //iterate and check about the category list
                var childReports = reports.Where(p => p.Category == cat.Category).ToList();

                //if one one set the base report else add
                if(childReports.Count > 1)
                {
                    model.Title = cat.ESSCategoryTitle;
                    model.Category = cat.Category;
                    List<ReportListModel> cl = new List<ReportListModel>();
                    foreach(var c in childReports)
                        cl.Add(new ReportListModel() { Title = c.ESSReportDetails.Title, Report = c });
                    model.ChildReportList = cl;
                }
                else
                {
                    model.Title = childReports[0].ESSReportDetails.Title;
                    model.Category = cat.Category;
                    model.Report = childReports[0];
                }

                rpl.Add(model);
            }
            
            return rpl;
        }



        [Secure("MyEmployeesDashboard")]
        [HttpGet]
        [Route("Reports/{reportId}/View")]
        public ActionResult ViewReport(string reportId)
        {
            ReportService reportService = new ReportService();
            BaseReport theReport = reportService.GetReport(reportId);
            ReportCriteria criteria = reportService.GetCriteria(reportId, CurrentUser);
            ReportViewModel report = new ReportViewModel(theReport, criteria);
            return View("Report", report);
        }


        [Secure("MyEmployeesDashboard")]
        [FileDownload, HttpPost]
        [Route("Reports/{reportId}/View", Name = "RunReport")]
        [ValidateAntiForgeryToken]
        public ActionResult RunReport(ReportViewModel viewModel, string command)
        {
            ReportService reportService = new ReportService();
            viewModel.BaseReport = reportService.GetReport(viewModel.ReportId);
            viewModel.Criteria = viewModel.ApplyToDataModel();
            if (command == "Export To CSV")
            {
                return Export(ExportType.CSV, viewModel.BaseReport, viewModel.Criteria);
            }
            else if (command == "Export To PDF")
            {
                return Export(ExportType.PDF, viewModel.BaseReport, viewModel.Criteria);
            }
            else if (command == "Run Report")
            {
                viewModel.Results = reportService.RunReport(viewModel.ReportId, viewModel.Criteria, CurrentUser);
                viewModel.IsReportRun = true;
                if (Request.IsAjax())
                    return Json(viewModel);

                return View("Report", viewModel);
            }

            throw new ArgumentException(string.Format("Command {0} is unsupported", command));
        }

        [Secure("MyEmployeesDashboard")]
        [HttpGet]
        [Route("Reports/GetReportById", Name = "GetReportById")]
        public JsonResult GetReportById(string Id)
        {
            BaseReport report = new ReportService().GetReport(Id);
            return Json(report, JsonRequestBehavior.AllowGet);
        }


        private FileResult Export(ExportType exportType, BaseReport report, ReportCriteria criteria)
        {
            ReportService reportService = new ReportService();
            string fileType = null;
            string fileName = string.Format("{0:yyyy-MM-dd} - {1} {2}", DateTime.Today, report.Category, report.Name);
            byte[] output = null;
            switch (exportType)
            {
                case ExportType.PDF:
                    fileType = "text/pdf";
                    fileName = string.Concat(fileName, ".pdf");
                    output = reportService.GeneratePDF(report.Id, criteria, Current.User());
                    break;
                case ExportType.CSV:
                    fileType = "text/comma-separated-values";
                    fileName = string.Concat(fileName, ".csv");
                    output = reportService.GenerateCSV(report.Id, criteria, Current.User());
                    break;
            }


            return File(output, fileType, fileName);
        }
    }
}