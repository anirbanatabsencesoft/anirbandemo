﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Common;
using AbsenceSoft.SelfService.Models;
using AbsenceSoft.SelfService.Models.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Users;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic;
using AbsenceSoft.SelfService.Models.Home;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.SelfService.Common;
using AT.Logic.Authentication;
using System.Threading.Tasks;
using AT.Entities.Authentication;
using AbsenceSoft.Data.Audit;

namespace AbsenceSoft.SelfService.Controllers
{
    public class HomeController : BaseSelfServiceController
    {
        private ApplicationSignInManager _signInManager;
        public HomeController()
        {

        }
        public HomeController(ApplicationSignInManager applicationSignInManager)
        {
            _signInManager = applicationSignInManager;
        }

        /// <summary>
        /// Gets the new object of ApplicationSignInManager.
        /// </summary>
        /// <value>
        /// The sign in manager.
        /// </value>
        protected ApplicationSignInManager SignInManager
        {
            get
            {
                if (_signInManager == null)
                    _signInManager = new ApplicationSignInManager(HttpContext.GetOwinContext().Authentication);
                return _signInManager;
            }
        }

        [ChildActionOnly, Route("Toolbar", Name = "Toolbar")]
        public PartialViewResult Toolbar()
        {
            UserViewModel user = new UserViewModel(Current.User());
            return PartialView(user);
        }

        [ChildActionOnly, Route("Logo", Name="Logo")]
        public PartialViewResult Logo()
        {
            LogoViewModel logo = new LogoViewModel();
            logo.PopulateLogoInformation(Current.Employer(), Current.Customer());
            return PartialView(logo);
        }

        // GET: Home
        [HttpGet, Route("", Name = "Home")]
        [Secure, Title("Dashboard")]
        public ActionResult Index(int? page, long? status, bool? includeApproved, bool? includePending, bool? includeDenied)
        {
            if (!CurrentUserIsManager)
                return RedirectToRoute(HomeRoute);

            if (!page.HasValue)
                page = 1;

            ListCriteria criteria = new ListCriteria()
            {
                PageSize = 5,
                PageNumber = page.Value
            };

            criteria.Set("Status", status);
            criteria.Set("Approved", includeApproved);
            criteria.Set("Pending", includePending);
            criteria.Set("Denied", includeDenied);
            criteria.Set("ListType", "Team");

            using (var caseService = new CaseService())
            {
                ListResults cases = caseService.CaseList(criteria);
                string[] caseIds = cases.Results.Select(c => c.Get<string>("Id")).ToArray();
                IEnumerable<NoteViewModel> notes = GetCaseNotes(caseIds);                
                IEnumerable <AttachmentViewModel> attachments = GetCaseAttachments(caseIds);
                List<CaseViewModel> theCases = cases.Results.Select(c => new CaseViewModel(
                    c, 
                    true, 
                    notes.Where(n => n.CaseId == c.Get<string>("Id")), 
                    attachments.Where(a => a.CaseId == c.Get<string>("Id"))))
                    .Where(c => c != null).ToList();

                if (theCases == null)
                    theCases = new List<CaseViewModel>();

                if (Request.IsAjaxRequest())
                    return PartialView("CaseList", theCases);

                ViewBag.HasMoreCases = cases.Total > 5;
                return View(theCases);
            }
        }

        private bool CustomerLoginIsDisabledForESS(Customer customer)
        {
            return customer != null && customer.SecuritySettings != null && customer.SecuritySettings.DisableESSLoginPage;
        }

        [Title("Login"), HttpGet]
        [Route("Login", Name = "Login")]
        public async Task<ActionResult> Login()
        {
            if (Request != null && Request.IsAuthenticated)
                await LogoutUser();

            if (CustomerLoginIsDisabledForESS(CurrentCustomer))
                return View("DisabledLoginPage");

            using (var ssoService = new SsoService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                LoginViewModel login = new LoginViewModel(ssoService.GetProfile(false));
                return View(login);
            }
        }

        [HttpPost, Title("Login")]
        [Route("Login", Name = "LoginPost")]
        public async Task<ActionResult> Login(LoginViewModel login)
        {
            if (!ModelState.IsValid)
            {
                return View(login);
            }

            Data.Security.User loginUser = null;            

            SignInStatus result = await SignInManager.SignInAsync(login.Email, login.Password,ApplicationType.SelfService);

            using (var service = new AdministrationService())
            {
                loginUser = service.GetUserByEmail(login.Email.ToLowerInvariant());
            }

            if (loginUser != null && CustomerLoginIsDisabledForESS(loginUser.Customer))
            {
                Current.Customer(loginUser.Customer);
                return View("DisabledLoginPage");
            }
            //addtional check
            using (var service = new AuthenticationService())
            {
                if (result == SignInStatus.Ok && !service.ValidEmployeeSelfServiceLogin(loginUser))
                {
                    result = SignInStatus.Error;
                }
            }
            switch (result)
            {
                case SignInStatus.Ok:
                    if (loginUser == null)
                    {
                        AbsenceSoft.Web.AuditLoginHelper.AuditLoginFailure(loginUser, login.Email, LoginType.Failed);
                        ModelState.AddModelError("", "Your login attempt has failed. The username and/or password may be incorrect. If you feel your account is properly registered with us, click on \"Forgot Password\" below to retrieve your account details.");
                        break;
                    }

                    AbsenceSoft.Web.AuditLoginHelper.AuditLogin(loginUser, login.Email);
                    string employeeId = loginUser.EmployeeId;
                    bool isManager = AbsenceSoft.Data.Security.User.Permissions.GetProjectedPermissions(loginUser).Contains(Data.Security.Permission.MyEmployeesDashboard.Id);

                    if (isManager)
                    {
                        return RedirectToRoute("Home");                    
                    }
                    
                    if (!loginUser.Employers.Any(e => e.Roles.Count > 0))
                    {
                        ModelState.AddModelError("", "You do not have permission to access the system and no default role or access has been configured for your account. Please contact your system administrator.");
                        break;
                    }

                    if (Case.AsQueryable().Count(c => c.Employee.Id == employeeId) == 0)
                    {
                        return RedirectToRoute("NewRequest");
                    }

                    return RedirectToRoute("ViewEmployee");
                case SignInStatus.Error:
                    AbsenceSoft.Web.AuditLoginHelper.AuditLoginFailure(loginUser, login.Email, LoginType.Failed);
                    ModelState.AddModelError("", "Your login attempt has failed. The username and/or password may be incorrect. If you feel your account is properly registered with us, click on \"Forgot Password\" below to retrieve your account details.");
                    break;
                case SignInStatus.Locked:
                    AbsenceSoft.Web.AuditLoginHelper.AuditLoginFailure(loginUser, login.Email, LoginType.Locked);
                    ModelState.AddModelError("", "Your account has been locked out for security reasons. You should receive an email with instructions how to unlock your account. Please contact AbsenceSoft team if you still having problem with your login.");
                    break;
                case SignInStatus.Disabled:
                    AbsenceSoft.Web.AuditLoginHelper.AuditLoginFailure(loginUser, login.Email, LoginType.Disabled);
                    ModelState.AddModelError("", "Your account has been locked out for security reasons. You should receive an email with instructions how to unlock your account. Please contact AbsenceSoft team if you still having problem with your login.");
                    break;
                case SignInStatus.Expired:
                    AbsenceSoft.Web.AuditLoginHelper.AuditLoginFailure(loginUser, login.Email, LoginType.Expired);
                    return RedirectToRoute("ExpiredPassword");
            }

            ///If we get this far, something failed and a message should be populated
            return View(login);
            
        }

        [HttpGet, Route("Logout", Name = "Logout")]
        public async Task<ActionResult> Logout()
        {
            AbsenceSoft.Web.AuditLoginHelper.AuditLogout(CurrentUser, CurrentUser.Email);
            if (Request != null && Request.IsAuthenticated)
                await LogoutUser();
            return RedirectToRoute("Login");
        }

        private async Task LogoutUser()
        {
            try
            {
                await SignInManager.SignOutAsync();
            }
            catch (Exception ex)
            {
                Log.Error("Cannot logout the user", ex);
            }

        }

        [Title("Terms and Conditions")]
        public ViewResult TermsAndConditions()
        {
            return View();
        }

        [Title("Forgot Password?")]
        [HttpGet, Route("ForgotPassword", Name ="ForgotPassword")]
        public ViewResult ForgotPassword()
        {
            ForgotPasswordViewModel model = new ForgotPasswordViewModel();
            return View(model);
        }

        [Title("Forgot Password?")]
        [HttpPost, Route("ForgotPassword", Name ="ForgotPasswordSubmit")]
        [ValidateAntiForgeryToken]
        public ViewResult ForgotPasswordSubmit(ForgotPasswordViewModel forgotPassword)
        {
            if (!ModelState.IsValid)
                return View("ForgotPassword", forgotPassword);

            using (var authenticationService = new AuthenticationService())
            {
                authenticationService.ForgotPassword(forgotPassword.Email);
            }   
            return View();
        }

        [HttpGet, Route("ResetPassword/{resetKey}", Name = "ResetPassword")]
        public ActionResult ResetPassword(string resetKey)
        {
            if (string.IsNullOrWhiteSpace(resetKey))
                return RedirectToRoute("Login");

            return View(new ResetPasswordViewModel(resetKey));
        }

        [HttpPost, Route("ResetPassword", Name = "ResetPasswordSubmit")]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPasswordSubmit(ResetPasswordViewModel resetPassword)
        {
            if (!ModelState.IsValid)
                return View("ResetPassword", resetPassword);

            using (var authService = new AuthenticationService())
            {
                authService.ResetPassword(resetPassword.ResetKey, resetPassword.NewPassword);
            }

            return View();
        }

        [HttpGet, Route("CreateAccount/{verificationKey}", Name = "CreateAccount")]
        public ActionResult CreateAccount(string verificationKey)
        {
            if (string.IsNullOrWhiteSpace(verificationKey))
                return RedirectToRoute("Login");

            return View(new CreateAccountViewModel(verificationKey));
        }

        [HttpPost, Route("CreateAccount", Name = "CreateAccountSubmit")]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAccountSubmit(CreateAccountViewModel model)
        {
            if (!ModelState.IsValid)
                return View("CreateAccount", model);

            using (var authService = new AuthenticationService())
            {
                authService.CreateAccount(model.VerificationKey, model.Password);
            }

            return View();
        }

        [Title("Expired Password")]
        [HttpGet, Route("ExpiredPassword", Name = "ExpiredPassword")]
        public ActionResult ExpiredPassword()
        {
            return View();
        }

        [Title("Expired Password")]
        [HttpPost, Route("ExpiredPassword", Name ="ExpiredPasswordSubmit")]
        [ValidateAntiForgeryToken]
        public ActionResult ExpiredPasswordSubmit(ExpiredPasswordViewModel expiredPassword)
        {
            if (!ModelState.IsValid)
                return View("ExpiredPassword", expiredPassword);

            using (var authenticationService = new AuthenticationService())
            using (var adminService = new AdministrationService())
            {
                var user = adminService.GetUserByEmail(expiredPassword.Email);
                if(user != null)
                    adminService.ChangePassword(user, expiredPassword.NewPassword);
            }

            return View();
        }

        [Title("Register Account")]
        [HttpGet, Route("RegisterAccount", Name = "RegisterAccount")]
        public ActionResult RegisterAccount()
        {
            return View();
        }

        [Title("Register Account")]
        [HttpPost, Route("RegisterAccountSubmit", Name = "RegisterAccountSubmit")]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterAccountSubmit(RegisterAccountSubmitViewModel model)
        {
            using (var authenticationService = new AuthenticationService())
            {
                model.Status = authenticationService.RegisterAccount(model.Email, model.Resend, model.ResendToAltEmail, Request.ClientIPAddress());
                model.Resend = false;
                model.ResendToAltEmail = false;
                return View(model);
            }
        }
        
        [Title("Unauthorized")]
        [Route("Home/Unauthorized")]
        public ActionResult Unauthorized()
        {
            return View();
        }
    }
}
