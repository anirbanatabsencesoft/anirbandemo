﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoDB.Bson;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.SelfService.Models;
using AbsenceSoft.Web.Attributes;

namespace AbsenceSoft.SelfService.Controllers
{
    public class LookupsController : BaseSelfServiceController
    {
       
        [HttpGet, Route("Lookups/ListCountries", Name ="ListCountries")]
        public JsonResult GetCountries()
        {
            using (var administrationService = new AdministrationService(CurrentUser))
            {
                var countries = administrationService.GetCountriesFromXML(includeRegions: false);
                return Json(countries.Select(c => new CountryViewModel(c)).ToList());
            }
        }

      
        [HttpGet, Route("Lookups/ListCountriesAndRegions", Name = "ListCountriesAndRegions")]
        public JsonResult GetCountriesAndRegions()
        {
            using (var administrationService = new AdministrationService(CurrentUser))
            {
                var countries = administrationService.GetCountriesFromXML(includeRegions: true);
                return Json(countries.Select(c => new CountryViewModel(c)).ToList(), JsonRequestBehavior.AllowGet);
            }
        }
    }
}