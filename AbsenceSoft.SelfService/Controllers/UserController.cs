﻿using AbsenceSoft.Logic.Customers;
using AbsenceSoft.SelfService.Models;
using AbsenceSoft.Web;
using AbsenceSoft.Web.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.SelfService.Controllers
{
    public class UserController : BaseSelfServiceController
    {
        [Secure]
        [HttpGet, Route("User", Name="User"), Title("My Account")]
        public ActionResult Index()
        {
            UserViewModel user = new UserViewModel(Current.User());
            return View(user);
        }

        [Secure]
        [HttpPost, Route("User/Update", Name="UpdateUser")]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateUser(UserViewModel user)
        {
            if(!ModelState.IsValid)
                return PartialView("DetailsForm", user);

            using (var authenticationService = new AuthenticationService())
            {
                User u = user.ApplyToDataModel(Current.User());
                authenticationService.UpdateUser(u);
                /// update the Current User context
                return RedirectViaJavaScript(Url.RouteUrl(HomeRoute));
            }
        }

        [Secure]
        [HttpPost, Route("User/UpdatePassword", Name = "UpdateUserPassword")]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateUserPassword(PasswordViewModel user)
        {
            if (!ModelState.IsValid)
                return PartialView("PasswordForm", user);

            using (var administrationService = new AdministrationService())
            {
                administrationService.ChangePassword(Current.User(), user.NewPassword);
                return RedirectViaJavaScript(Url.RouteUrl(HomeRoute));
            }
        }

        [Secure("ViewContact", "ViewEmployeeContactInfo")]
        [HttpGet, ChildActionOnly]
        public ActionResult UserContacts()
        {
            using (var employeeService = new EmployeeService())
            {
                IEnumerable<ContactViewModel> contacts = employeeService.GetContacts(Current.User().EmployeeId).Select(c => new ContactViewModel(c));
                return PartialView(contacts);    
            }
        }

        [Secure("EditContact", "EditEmployeeContactInfo", "CreateContact")]
        [HttpPost, Route("User/SaveContact", Name="SaveContact")]
        [ValidateApiAntiForgeryToken]
        public JsonResult SaveContact(ContactViewModel contact)
        {
            if (!ModelState.IsValid)
                return Json(new JsonResponse(ModelState));

            try
            {
                using (var employeeService = new EmployeeService())
                {
                    var user = Current.User();
                    EmployeeContact theContact = contact.ApplyToDataModel(employeeService.GetContact(contact.Id), user.EmployeeId, user.CustomerId, user.EmployerId);
                    employeeService.SaveContact(theContact);
                }
                return Json(new JsonResponse()
                {
                    Error = false,
                    Redirect = true, 
                    RedirectUrl = Url.RouteUrl("User")
                });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return Json(new JsonResponse(ModelState));
            }
        }

        [Secure("DeleteContact")]
        [HttpPost, Route("User/DeleteContact", Name= "DeleteContact")]
        [ValidateApiAntiForgeryToken]
        public JsonResult DeleteContact(ContactViewModel contact)
        {
            if (string.IsNullOrEmpty(contact.Id))
                ModelState.AddModelError("Id", "Id is required to delete the contact");

            try
            {
                using (var employeeService = new EmployeeService())
                {
                    employeeService.DeleteContact(contact.Id);

                    return Json(new JsonResponse()
                    {
                        Error = false,
                        Redirect = true,
                        RedirectUrl = Url.RouteUrl("User")
                    });
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }

            return Json(new JsonResponse(ModelState));
        }
    }
}