﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.SelfService.Models;
using AbsenceSoft.SelfService.Models.Cases;
using AbsenceSoft.SelfService.Models.Employees;
using AbsenceSoft.Web;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.SelfService.Controllers
{
    public class EmployeesController : BaseSelfServiceController
    {
        // GET: Employees
        [Secure("MyEmployeesDashboard")]
        [HttpGet, Route("MyTeam", Name = "MyTeam"), Title("My Team")]
        public ActionResult MyTeam(int? page)
        {
            if (!page.HasValue)
                page = 1;

            ListCriteria criteria = new ListCriteria()
            {
                PageSize = 10,
                PageNumber = page.Value
            };

            using (var caseService = new CaseService())
            using (var employeeService = new EmployeeService())
            {
                ListResults results = employeeService.EmployeeList(criteria);
                List<EmployeeViewModel> employees = results.Results
                    .Select(r => new EmployeeViewModel(r)).ToList();

                ViewBag.CurrentCustomerName = CurrentCustomer?.Name??"";
                ViewBag.CurrentEmployerName = CurrentEmployer?.Name??"";
                ViewBag.CurrentUserEmail = CurrentUser?.Email??"";

                foreach (var employee in employees)
                {
                    employee.Policies = caseService.GetEmployeePolicySummary(employee.Id)
                        .Select(ps => new PolicySummaryViewModel(ps));
                }
                if (Request.IsAjaxRequest())
                    return PartialView("EmployeeList", employees);


                return View(employees);
            }
        }

        [Secure("MyEmployeesDashboard")]
        [HttpGet, Route("MyTeamCases", Name = "MyTeamCases"), Title("My Team Cases")]
        public ActionResult MyTeamCases(int? page)
        {
            if (!page.HasValue)
                page = 1;

            ListCriteria criteria = new ListCriteria()
            {
                PageSize = 5,
                PageNumber = page.Value
            };

            criteria.Set("Status", (long)CaseStatus.Open);
            criteria.Set("ListType", "Team");
            criteria.Set("AccommodationStatus", CaseStatus.Open);

            using (var caseService = new CaseService())
            {
                ListResults cases = caseService.CaseList(criteria);
                string[] caseIds = cases.Results.Select(c => c.Get<string>("Id")).ToArray();
                IEnumerable<NoteViewModel> notes = GetCaseNotes(caseIds);
                IEnumerable<AttachmentViewModel> attachments = GetCaseAttachments(caseIds);
                List<CaseViewModel> theCases = cases.Results.Select(c => new CaseViewModel(
                    c,
                    true,
                    notes.Where(n => n.CaseId == c.Get<string>("Id")),
                    attachments.Where(a => a.CaseId == c.Get<string>("Id"))))
                    .Where(c => c != null).ToList();

                if (theCases == null)
                    theCases = new List<CaseViewModel>();

                if (Request.IsAjaxRequest())
                    return PartialView("CaseList", theCases);

                ViewBag.HasMoreCases = cases.Total > 5;
                return View(theCases);
            }
        }


        [Secure("MyEmployeesDashboard")]
        [HttpGet, Route("SearchTeam", Name = "SearchTeam")]
        public JsonResult SearchTeam(string name)
        {
            using (var employeeService = new EmployeeService())
            {
                ListCriteria criteria = new ListCriteria()
                {
                    PageSize = 5
                };
                criteria.Set("Name", name);
                IEnumerable<EmployeeViewModel> foundEmployees = employeeService.EmployeeList(criteria).Results
                    .Select(r => new EmployeeViewModel(r));
                return Json(foundEmployees, JsonRequestBehavior.AllowGet);
            }
        }

        [Secure("MyEmployeesDashboard")]
        [HttpGet, Route("SearchTeamByEmployeeNameNumberOrCase", Name = "SearchTeamByEmployeeNameNumberOrCase")]
        public JsonResult SearchTeamByEmployeeNameNumberOrCase(string employeeNameNumberOrCase)
        {
            EmployeeCaseViewModel employeeCaseViewModel = new EmployeeCaseViewModel();
            using (var employeeService = new EmployeeService())
            {
                ListCriteria criteria = new ListCriteria()
                {
                    PageSize = 5
                };
                criteria.Set("EmployeeNameNumberOrCase", employeeNameNumberOrCase);
                employeeCaseViewModel.EmployeeViewModels = employeeService.EmployeeList(criteria).Results
                    .Select(r => new EmployeeViewModel(r));
            }
            using (var caseService = new CaseService())
            {
                ListCriteria criteria = new ListCriteria()
                {
                    PageSize = 5
                };
                criteria.Set("CaseNumber", employeeNameNumberOrCase);
                employeeCaseViewModel.CaseViewModels = caseService.CaseList(criteria).Results
                    .Select(r => new CaseViewModel(r, false));
            }
            return Json(employeeCaseViewModel, JsonRequestBehavior.AllowGet);
        }

        [Secure("ViewEmployee")]
        [HttpGet, Route("Employee/{employeeId?}", Name = "ViewEmployee"), Title("My Employee")]
        public ActionResult ViewEmployee(string employeeId = null, int openCasesCount = 0)
        {
            if (employeeId == null)
                employeeId = Current.User().EmployeeId;

            using (var employeeService = new EmployeeService())
            {
                var employee = employeeService.GetEmployee(employeeId);
                EmployeeViewModel emp = new EmployeeViewModel(employee)
                {
                    ShowMyTeam = CurrentUserIsManager,
                    OpenCasesCount = openCasesCount
                };
                using (var caseService = new CaseService())
                {
                    emp.Policies = caseService.GetEmployeePolicySummary(employee)
                        .Select(ps => new PolicySummaryViewModel(ps));

                    emp.HasCase = (openCasesCount == 0) ? (caseService.GetCountOfCasesForEmployee(employeeId, CaseStatus.Open, CaseStatus.Requested, CaseStatus.Closed) > 0): true;
                }
                return View(emp);
            }
        }

        [Secure("ViewEmployee"), ChildActionOnly]
        [HttpGet, Route("Calendar/{employeeId}/", Name = "Calendar")]
        public ActionResult Calendar(string employeeId, bool allowSelection = false)
        {
            return PartialView(new CalendarDataViewModel()
            {
                EmployeeId = employeeId,
                AllowSelection = allowSelection
            });
        }

        [Secure("ViewEmployee")]
        [HttpGet, Route("CalendarInfo/{employeeId}", Name = "CalendarInfo")]
        public ActionResult CalendarInfo(string employeeId, DateTime? startDate, DateTime? endDate)
        {
            if (startDate == null)
                startDate = DateTime.Now.GetFirstDayOfMonth();

            if (endDate == null)
                endDate = DateTime.Now.GetLastDayOfMonth();

            using (var caseService = new CaseService())
            using (var employeeService = new EmployeeService())
            {
                List<CalendarDataViewModel> calendarData = new List<CalendarDataViewModel>();
                List<Case> cases = caseService.GetCasesForEmployee(employeeId, CaseStatus.Open, CaseStatus.Requested, CaseStatus.Closed);
                for (var day = startDate.Value.Date; day.Date <= endDate.Value.Date; day = day.AddDays(1))
                {
                    Case matchingCase = cases.FirstOrDefault(c => c.StartDate <= day && c.EndDate >= day);
                    if (matchingCase == null)
                        continue;
                    /*AT-6571-Added a new variable and used for passing in both the constructor.
                      because variable named "day" is reference type and when value get increament in loop
                      it changes the values in the collection also. Also added a new condition for policy eligibility*/
                    var dayTemp = day;
                    CalendarDataViewModel data = new CalendarDataViewModel(dayTemp, matchingCase.CaseNumber, matchingCase.Reason.Name)
                    {
                        Policies = matchingCase.Segments.SelectMany(s => s.AppliedPolicies.Where(p => p.Status == EligibilityStatus.Eligible)).Select(ap => new PolicyDetailViewModel(ap, dayTemp))
                    };

                    calendarData.Add(data);
                }

                var employer = employeeService.GetEmployee(employeeId).Employer;
                var holidayService = new HolidayService();
                foreach (int year in startDate.Value.GetYears(endDate.Value))
                {
                    foreach (Holiday holiday in employer.Holidays)
                    {
                        DateTime holidayDate;
                        if (holidayService.MakeDate(holiday, year, out holidayDate)
                            && !calendarData.Any(m => m.Date == holidayDate.Date)
                            && holidayDate >= startDate.Value
                            && holidayDate <= endDate.Value)
                        {
                            string holidayName = holiday.Name;
                            if (holiday.OrClosestWeekday && holidayDate.IsWeekend())
                            {
                                holidayDate = holidayDate.ClosestWeekday();
                                holidayName = string.Format("{0} (Observed)", holidayName);
                            }
                            calendarData.Add(new CalendarDataViewModel(holidayDate, holidayName));
                        }
                    }
                }

                return Json(calendarData, JsonRequestBehavior.AllowGet);
            }
        }

        [Secure("ViewEmployee"), ChildActionOnly]
        [HttpGet, Route("TimeOffRequests/{employeeId}/", Name = "TimeOffRequests")]
        public ActionResult TimeOffRequests(string employeeId)
        {
            using (var caseService = new CaseService())
            {
                IEnumerable<TimeOffRequestViewModel> timeOff = caseService.GetCasesForEmployee(employeeId, CaseStatus.Open, CaseStatus.Requested)

                    .SelectMany(c => c.Segments)
                    .SelectMany(s => s.UserRequests)
                    .Select(ur => new TimeOffRequestViewModel(ur));

                List<TimeOffRequestViewModel> TimeOff = timeOff.OrderByDescending(a => a.DateOff).ToList();

                return PartialView(TimeOff);
            }
        }

        [Secure("ViewEmployee"), ChildActionOnly]
        [HttpGet, Route("EmployeeCases/{employeeId}/{employerId?}", Name = "EmployeeCases")]
        public ActionResult EmployeeCases(string employeeId, string employerId, bool openCasesOnly = false)
        {
            using (var caseService = new CaseService())
            {
                ListCriteria criteria = new ListCriteria()
                {
                    PageSize = 10,
                    PageNumber = 1
                };
                if (employeeId == "")
                {
                    employeeId = Current.User().EmployeeId;
                }
                if (employerId == "" || employerId == null)
                {
                    employerId = Current.User().EmployerId;
                }
                
                criteria.Set("EmployeeId", employeeId);
                criteria.Set("EmployerId", employerId);
                if (openCasesOnly)
                {
                    criteria.Set("Status", -2L);
                }

                ListResults cases = caseService.CaseList(criteria);
                string[] caseIds = cases.Results.Select(c => c.Get<string>("Id")).ToArray();

                IEnumerable<NoteViewModel> notes = GetCaseNotes(caseIds, employerId);
                IEnumerable<AttachmentViewModel> attachments = GetCaseAttachments(caseIds);
                List<CaseViewModel> theCases = cases.Results.Select(c => new CaseViewModel(
                    c,
                    false,
                    notes.Where(n => n.CaseId == c.Get<string>("Id")),
                    attachments.Where(a => a.CaseId == c.Get<string>("Id"))))
                    .Where(c => c != null).ToList();
                return PartialView("CaseList", theCases);
            }

        }

        [HttpGet, Route("GetPagedCaseData", Name = "GetPagedCaseData"), Title("")]
        public ActionResult GetPagedCaseData(int pageNumber, string empId)
        {
            List<CaseViewModel> theCases = null;

            using (var employeeService = new EmployeeService())
            {
                using (var caseService = new CaseService())
                {
                    ListCriteria criteria = new ListCriteria()
                    {
                        PageSize = 10,
                        PageNumber = pageNumber
                    };
                    Employee emp = employeeService.GetEmployee(empId);

                    criteria.Set("EmployeeId", empId);
                    criteria.Set("EmployerId", emp.EmployerId);

                    ListResults cases = caseService.CaseList(criteria);
                    string[] caseIds = cases.Results.Select(c => c.Get<string>("Id")).ToArray();
                    IEnumerable<NoteViewModel> notes = GetCaseNotes(caseIds);
                    IEnumerable<AttachmentViewModel> attachments = GetCaseAttachments(caseIds);
                    theCases = cases.Results.Select(c => new CaseViewModel(
                    c,
                    false,
                    notes.Where(n => n.CaseId == c.Get<string>("Id")),
                    attachments.Where(a => a.CaseId == c.Get<string>("Id"))))
                    .Where(c => c != null).ToList();

                }

            }
            if (theCases == null)
                theCases = new List<CaseViewModel>();

            return PartialView("CaseList", theCases);
        }

        [HttpPost, Route("Employees/Schedule")]
        [ValidateApiAntiForgeryToken]
        public ActionResult EmployeeScheduleEditor(ScheduleViewModel schedule)
        {
            EmployeeViewModel emp = new EmployeeViewModel()
            {
                Schedule = schedule
            };
            schedule.Times.Clear();
            if (schedule.TemporaryActualScheduleValuesInString != null)
            {
                schedule.TemporaryActualTimes = JsonConvert.DeserializeObject<List<TimeViewModel>>(schedule.TemporaryActualScheduleValuesInString);
            }
            if (schedule.TemporaryActualScheduleWorkingHoursInString != null)
            {
                schedule.TemporaryVariableScheduleWeeklyTimes = JsonConvert.DeserializeObject<List<TimeViewModel>>(schedule.TemporaryActualScheduleWorkingHoursInString);
            }
            schedule.BuildTimesLists();
            return PartialView(emp);
        }

        [Secure("ViewEmployee")]
        [HttpPost, Route("Employees/Edit/{employeeId?}")]
        [OutputCache(Duration = 0)]
        [ValidateAntiForgeryToken]
        public ActionResult EditEmployee(ScheduleViewModel schedule)
        {
            Employee emp = new Employee();
            if (!string.IsNullOrWhiteSpace(schedule.EmployeeId))
                emp = Employee.GetById(schedule.EmployeeId);
            schedule.ErrorMessage = "";
            try
            {
                using (var employeeService = new EmployeeService())
                {
                    var uiSchedule = schedule;
                    Schedule workSchedule = null;
                    var totalTimes = uiSchedule.Times.Sum(t => !string.IsNullOrEmpty(t.TotalMinutes) ? t.TotalMinutes.ParseFriendlyTime() : 0);
                    if (totalTimes > 0 || uiSchedule.ScheduleType == ScheduleType.FteVariable)
                    {
                        workSchedule = emp.WorkSchedules.FirstOrDefault(t => t.StartDate == uiSchedule.StartDate);
                        if (workSchedule == null)
                            workSchedule = new Schedule();
                        workSchedule = uiSchedule.ApplyToDataModel(workSchedule);

                    }
                    List<VariableScheduleTime> appliedTimes = new List<VariableScheduleTime>();
                    if (schedule.ActualTimes != null && uiSchedule.ScheduleType == ScheduleType.Variable)
                    {
                        foreach (var tempTime in schedule.ActualTimes)
                        {
                            var matchingDate = uiSchedule.ActualTimes.FirstOrDefault(t => t.SampleDate == tempTime.SampleDate);
                            if (matchingDate == null)
                            {
                                uiSchedule.ActualTimes.Add(new TimeViewModel()
                                {
                                    SampleDate = tempTime.SampleDate,
                                    TotalMinutes = tempTime.TotalMinutes

                                });
                            }
                            else
                            {
                                matchingDate.TotalMinutes = tempTime.TotalMinutes;
                            }
                        }

                    }
                    if (uiSchedule.ActualTimes.Count > 0 && uiSchedule.ScheduleType == ScheduleType.Variable)
                    {
                        DateTime startDate = uiSchedule.ActualTimes.Min(t => t.SampleDate);
                        DateTime endDate = uiSchedule.ActualTimes.Max(t => t.SampleDate);
                        if (!string.IsNullOrWhiteSpace(emp.Id))
                        {
                            appliedTimes = employeeService.GetEmployeeVariableSchedule(emp.Id, startDate, endDate);
                        }

                        appliedTimes = uiSchedule.ApplyActualTimesToDataModel(appliedTimes);
                    }

                    emp = employeeService.Update(emp, workSchedule, appliedTimes);
                    if (!string.IsNullOrWhiteSpace(schedule.EmployeeId) && uiSchedule.ActualTimes.Count > 0 && uiSchedule.ScheduleType == ScheduleType.Variable)
                    {
                        employeeService.SetWorkSchedule(emp, workSchedule, appliedTimes);
                    }

                    EmployeeViewModel employeeViewModel = new EmployeeViewModel(emp);
                    employeeViewModel.ShowMyTeam = CurrentUserIsManager;
                    using (var caseService = new CaseService())
                    {
                        employeeViewModel.Policies = caseService.GetEmployeePolicySummary(schedule.EmployeeId)
                            .Select(ps => new PolicySummaryViewModel(ps));

                        employeeViewModel.HasCase = caseService.GetCountOfCasesForEmployee(schedule.EmployeeId, CaseStatus.Open, CaseStatus.Requested, CaseStatus.Closed) > 0;
                    }
                    return PartialView("EmployeeSchedule", employeeViewModel);
                }
            }
            catch (AbsenceSoftException ex)
            {
                EmployeeViewModel employeeViewModel = new EmployeeViewModel(emp);
                employeeViewModel.Schedule.ErrorMessage = ex.Message;                
                return PartialView("EmployeeSchedule", employeeViewModel);
            }
        }
    }
}
