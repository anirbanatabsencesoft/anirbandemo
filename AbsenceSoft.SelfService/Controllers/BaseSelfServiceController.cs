﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Web;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Controllers;
using AbsenceSoft.SelfService.Models.Cases;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Tasks;
using AbsenceSoft.Logic.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.SelfService.Controllers
{
    [RemoteRequireHttps]
    public abstract class BaseSelfServiceController : SharedBaseController
    {
        protected string HomeRoute
        {
            get
            {
                if (CurrentUserIsManager)
                    return "Home";

                return "ViewEmployee";
            }
        }

        protected bool CurrentUserIsManager
        {
            get
            {
                User u = Current.User();
                if (u == null)
                    return false;

                return AbsenceSoft.Data.Security.User.Permissions.GetProjectedPermissions(u).Contains(Permission.MyEmployeesDashboard.Id);
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.IsChildAction && !UserRecordIsValid() && !IsLoggingOut(filterContext))
            {
                filterContext.Result = UserErrorView();
                return;
            }

            if (!filterContext.IsChildAction && UserMustChangePassword() && !IsChangingPassword(filterContext) && !IsLoggingOut(filterContext))
            {
                filterContext.Result = ExpiredPassword();
                return;
            }


            ViewBag.HomeRoute = HomeRoute;
            base.OnActionExecuting(filterContext);
        }

        protected IEnumerable<NoteViewModel> GetCaseNotes(string[] caseIds, string employerId = "")
        {
            if (caseIds.Length == 0)
            {
                return null;
            }
            using (var adminService = new AdministrationService(CurrentUser))
            {
                if (!adminService.UserHasPermission(Permission.ViewNotes))
                    return new List<NoteViewModel>();
            }
            using (var notesService = new NotesService())
            {
                var criteria = new ListCriteria()
                {
                    PageNumber = 1,
                    PageSize = int.MaxValue
                };
                criteria.Set("Ids", caseIds);
                criteria.Set("EmployerId", employerId);

               return notesService.GetCaseNoteList(criteria).Results.Select(r => new NoteViewModel(r));               
            }            
        }

        protected IEnumerable<AttachmentViewModel> GetCaseAttachments(string[] caseIds)
        {
            if (caseIds.Length == 0)
            {
                return null;
            }

            using (var attachmentService = new AttachmentService())
            {
                return attachmentService.GetCaseAttachments(caseIds).Select(a => new AttachmentViewModel(a));
            }
        }

        private bool IsLoggingOut(ActionExecutingContext filterContext)
        {
            string actionName = filterContext.ActionDescriptor.ActionName.ToLower();
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower();
            return actionName == "logout" && controllerName == "home";
        }

        private bool IsChangingPassword(ActionExecutingContext filterContext)
        {
            string actionName = filterContext.ActionDescriptor.ActionName.ToLower();
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower();
            return actionName == "expiredpassword" && controllerName == "home";
        }

        private bool UserRecordIsValid()
        {
            return CurrentUser == null || CurrentUserIsManager || !string.IsNullOrEmpty(CurrentUser.EmployeeId);
        }

        private bool UserMustChangePassword()
        {
            return CurrentUser != null && CurrentUser.MustChangePassword;
        }

        private ViewResult UserErrorView()
        {
            Response.StatusDescription = "Unable to find your employee record";
            return View("Error");
        }

        private ViewResult ExpiredPassword()
        {
            return View("ExpiredPassword");
        }

    }
}