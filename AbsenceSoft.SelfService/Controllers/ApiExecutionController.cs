﻿using AbsenceSoft.Common.Properties;
using AbsenceSoft.Common.Security;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.SelfService.Filters;
using AbsenceSoft.SelfService.Models;
using AbsenceSoft.Web;
using AbsenceSoft.Web.Attributes;
using AT.Api.Core.Common;
using AT.Common.Core;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.IdentityModel.Tokens;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;

namespace AbsenceSoft.SelfService.Controllers
{
    /// <summary>
    /// Controller to handle all JS API operations
    /// </summary>
    public class ApiExecutionController : Controller
    {
        #region JS Framework endpoints

        /// <summary>
        /// Use to validate user, and retrieve the current user Identity value
        /// </summary>
        /// <returns>Auth details with time expiration</returns>
        [DoNotResetCookie]
        [HttpGet]
        [Route("Home/GetUserIsAuthenticatedOrNot", Name = "GetUserIsAuthenticatedOrNot")]
        public JsonResult IsUserAuthenticated()
        {
            var authDetails = new UserIsAuthenticatedViewModel();
            if (!(User.Identity is ClaimsIdentity identity))
            {
                return Json(authDetails, JsonRequestBehavior.AllowGet);
            };
            var token = identity.FindFirstValue("token");
            if (token == null)
            {
                return Json(authDetails, JsonRequestBehavior.AllowGet);
            };
            JwtSecurityToken jwtToken = new JwtSecurityToken(token);
            authDetails.IsUserAuthenticated = identity.IsAuthenticated;
            authDetails.WillExpire = Data.Customers.SecuritySettings.ShowTimeoutDialog(HttpContext, jwtToken.ValidTo);
            authDetails.WillExpireSameDayOrWithinNextMeridiem = Data.Customers.SecuritySettings.WillExpireSameDayOrWithinNextMeridiem(HttpContext, jwtToken.ValidTo);
            authDetails.ExpirationTime = jwtToken.ValidTo.ToUniversalTime().ToUnixDate();
            return Json(authDetails, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get the token of the authenticated user
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("token/get")]
        public async Task<string> GetAuthenticationToken()
        {
            return await Task.FromResult(Utilities.GetTokenFromClaims());
        }
        
        /// <summary>
        /// Get the current user
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("current/user")]
        public async Task<object> GetCurrentUser()
        {
            var user = Current.User();
            return await Task.FromResult(FormattedJSON(new { Id = user.Id, Email = user.Email, CustomerId = user.CustomerId, EmployerId = user.EmployerId, EmployeeId = user.EmployeeId }));
        }

        /// <summary>
        /// Get the current employer for the user
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("current/employer")]
        public async Task<object> GetCurrentEmployer()
        {
            var employer = Current.Employer();
            if (employer != null)
            {
                return await Task.FromResult(FormattedJSON(new { Id = employer.Id, Name = employer.Name }));
            }
            return await Task.FromResult<object>(null);
        }

        /// <summary>
        /// Get the current customer of the user
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("current/customer")]
        public async Task<object> GetCurrentCustomer()
        {
            var customer = Current.Customer();
            return await Task.FromResult(FormattedJSON(new { Id = customer.Id, customer.Name }));
        }

        
        /// <summary>
        /// Returns the host of the component
        /// </summary>
        /// <param name="componentName"></param>
        /// <returns></returns>
        [HttpGet, Route("host/{component}"), OutputCache(Duration = 86400, VaryByParam = "component", Location = OutputCacheLocation.Client, NoStore = true)]
        public async Task<string> GetHost(string component)
        {
            return await Task.FromResult(EndPointManager.Instance.GetHostForComponent(component));
        }

        /// <summary>
        /// Calls a service endpoint
        /// </summary>
        /// <param name="component"></param>
        /// <param name="endPoint"></param>
        /// <param name="data"></param>
        /// <param name="method"></param>
        /// <returns></returns>
        [HttpPost, Route("service/execute")]
        public async Task<dynamic> CallService(ExecuteArgs args)
        {
            var token = Utilities.GetTokenFromClaims();
            if (!string.IsNullOrWhiteSpace(token))
            {
                ServiceFactory factory = new ServiceFactory(token);
                return await factory.CallServiceAsync(args.Component, args.EndPoint, args.PostedData, args.Method);
            }
            else
            {
                return await Task.FromResult<object>(null);
            }
        }

        [HttpGet, Route("schedule/download")]
        [Secure]
        public FileResult DownloadScheduledReport(string file)
        {
            //decrypt the file
            var filePath = Crypto.Decrypt(file.Replace(' ', '+'));
            byte[] resultFile = null;
            string fileType = "application/octet-stream";
            string fileName = "NoData.txt";
            if (!string.IsNullOrWhiteSpace(filePath))
            {
                var ext = filePath.Split('.');
                if (ext.Length >= 2)
                {
                    fileName = string.Format("{0:yyyy-MM-dd} - {1}{2}", DateTime.Today, "Report.", ext[1]);
                }
                using (FileService fileService = new FileService())
                {
                    resultFile = fileService.DownloadFile(filePath, Settings.Default.S3BucketName_Report_Files);
                }
                return File(resultFile, fileType, string.Format("{0:yyyy-MM-dd} - {1}", DateTime.Today, fileName));
            }

            return File(new byte[] { 0x20 }, fileType, string.Format("{0:yyyy-MM-dd} - {1}", DateTime.Today, fileName));
        }

        /// <summary>
        /// Call the Service to get the file path
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        [HttpGet, Route("service/download")]
        public FileResult CallFileService(ExecuteArgs args)
        {
            byte[] resultFile = null;
            var token = Utilities.GetTokenFromClaims();
            string output = string.Empty;
            string fileType = "application/octet-stream";
            string fileName = "NoData.txt";
            if (!string.IsNullOrWhiteSpace(token))
            {
                ServiceFactory factory = new ServiceFactory(token);
                output = factory.CallFileService(args.Component, args.EndPoint, args.PostedData, args.Method);
                if (!string.IsNullOrWhiteSpace(output))
                {
                    var ext = output.Split('.');
                    if (ext.Length >= 2)
                    {
                        fileName = string.Format("{0:yyyy-MM-dd} - {1}{2}", DateTime.Today, "Report.", ext[1]);
                    }

                    using (FileService fileService = new FileService())
                    {
                        resultFile = fileService.DownloadFile(output, Settings.Default.S3BucketName_Report_Files);
                    }
                    return File(resultFile, fileType, string.Format("{0:yyyy-MM-dd} - {1}", DateTime.Today, fileName));
                }
            }
            return File(new byte[] { 0x20 }, fileType, string.Format("{0:yyyy-MM-dd} - {1}", DateTime.Today, fileName));
        }

        /// <summary>
        /// Class to encapsulate parameters
        /// </summary>
        public class ExecuteArgs
        {
            /// <summary>
            /// The name of the component we are trying to access
            /// </summary>
            public string Component { get; set; }

            /// <summary>
            /// The endpoint
            /// </summary>
            public string EndPoint { get; set; }

            /// <summary>
            /// The string Data
            /// </summary>
            public string PostedData { get; set; }

            /// <summary>
            /// The Http Method
            /// </summary>
            public string Method { get; set; }

        }

        /// <summary>
        /// Converts and object to formatted JSON
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private static object FormattedJSON(object obj)
        {
            try
            {
                return JsonConvert.SerializeObject(obj, new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });
            }
            catch
            {
                return null;
            }
        }
        #endregion
    }
}