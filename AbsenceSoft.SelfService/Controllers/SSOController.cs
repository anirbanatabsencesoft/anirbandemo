﻿using AbsenceSoft.Web.Controllers;
using AT.Entities.Authentication;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AbsenceSoft.SelfService.Controllers
{
    public class SSOController : BaseSSOController
    {
        public SSOController() : base(ApplicationType.SelfService)
        {

        }
        // urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST
        [HttpPost]
        [Route("SSO/saml2/AssertionConsumerService")]
        public async override Task<System.Web.Mvc.ActionResult> AssertionConsumerService()
        {
            return await base.AssertionConsumerService();
        }

        // urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact
        [HttpPost]
        [Route("SSO/saml2/AssertionConsumerService.artifact")]
        public async override Task<System.Web.Mvc.ActionResult> AssertionConsumerServiceArtifact()
        {
            return await base.AssertionConsumerServiceArtifact();
        }

        // urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect
        [HttpGet]
        [Route("SSO/saml2/AssertionConsumerService.redirect")]
        public async override Task<System.Web.Mvc.ActionResult> AssertionConsumerServiceRedirect()
        {
            return await base.AssertionConsumerServiceRedirect();
        }

        [HttpGet]
        [Route("SSO/saml2/SLOService")]
        public override System.Web.Mvc.ActionResult SloService()
        {
            return base.SloService();
        }
    }
}