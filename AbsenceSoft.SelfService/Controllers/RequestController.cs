﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.SelfService.Models;
using AbsenceSoft.SelfService.Models.Request;
using AbsenceSoft.Web;
using AbsenceSoft.Web.Attributes;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.SelfService.Models.Cases;
using AbsenceSoft.Web.Filters;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Notes;
using MongoDB.Driver.Builders;
using AbsenceSoft.Logic.Tasks;
using MongoDB.Driver.Linq;

namespace AbsenceSoft.SelfService.Controllers
{
    public class RequestController : BaseSelfServiceController
    {
        // GET: Request
        [Secure("CreateCase")]
        [HttpGet, Route("Request/{employeeId?}", Name = "NewRequest"), Title("New Request")]
        public ActionResult NewRequest(string employeeId)
        {
            // ADA Only
            if (CurrentCustomer.HasFeature(Feature.ADA) && !CurrentCustomer.HasFeature(Feature.LOA) && !CurrentCustomer.HasFeature(Feature.ShortTermDisability))
                return RedirectToRoute("NewAccommodation", new { employeeId = employeeId });

            // LOA Only
            if ((CurrentCustomer.HasFeature(Feature.LOA) || CurrentCustomer.HasFeature(Feature.ShortTermDisability)) && !CurrentCustomer.HasFeature(Feature.ADA))
                return RedirectToRoute("NewCase", new { employeeId = employeeId });

            RequestViewModel newRequest = new RequestViewModel(employeeId);
            return View(newRequest);
        }

        #region New Case
        [Secure("CreateCase")]
        [HttpGet, Route("NewCase/{employeeId?}", Name = "NewCase"), Title("New Case")]
        public ActionResult NewCase(string employeeId)
        {
            NewCaseViewModel newCase = new NewCaseViewModel(employeeId);
            newCase.WorkRelatedInfo = new WorkRelatedViewModel();
            newCase.PopulatePersonalInfo();
            newCase.PopulateAbsenceReasons();
            return View(newCase);
        }

        [HttpGet, Route("CaseTypeSelected/{reasonCode}", Name = "CaseTypeSelection")]
        public ActionResult CaseTypeSelection(string reasonCode)
        {
            using (var caseService = new CaseService())
            {
                var customerId = CurrentCustomer?.Id ?? null;
                var employerId = CurrentEmployer?.Id ?? null;

                AbsenceReasonViewModel absenceReason = new AbsenceReasonViewModel(caseService.AbsenceReasonGetByCode(reasonCode, customerId, employerId));
                if (Request.IsAjaxRequest() || ControllerContext.IsChildAction)
                    return PartialView(absenceReason);

                return View(absenceReason);
            }
        }

        [Secure("CreateCase")]
        [HttpPost, Route("NewCase", Name = "NewCaseSave")]
        [ValidateApiAntiForgeryToken]
        public ActionResult NewCaseSave(NewCaseViewModel newCase)
        {
            if (!ModelState.IsValid)
            {
                return Json(new JsonResponse(ModelState));
            }

            try
            {
                using (var caseService = new CaseService())
                using (var employeeService = new EmployeeService())
                using (var eligibilityService = new EligibilityService())
                {
                    Case theCase = caseService.CreateCase(CaseStatus.Requested, newCase.EmployeeId, newCase.StartDate.Value, newCase.EndDate, newCase.CaseType, newCase.AbsenceReasonCode, null, newCase.Details);
                    if (newCase.AlternateInfo != null)
                    {
                        newCase.AlternateInfo.ApplyAlternateInfo(theCase);
                        theCase.OnSavedNOnce((o, c) => c.Entity.WfOnContactInfoChangeEvent(Current.User()));
                    }


                    //set events for Pregnancy or Maternity
                    if ((theCase.Reason.Flags & AbsenceReasonFlag.RequiresPregnancyDetails) == AbsenceReasonFlag.RequiresPregnancyDetails)
                    {
                        if (newCase.ExpectedDeliveryDate.HasValue)
                            theCase.SetCaseEvent(CaseEventType.DeliveryDate, newCase.ExpectedDeliveryDate.Value);
                        if (newCase.ActualDeliveryDate.HasValue)
                            theCase.SetCaseEvent(CaseEventType.DeliveryDate, newCase.ActualDeliveryDate.Value);

                        //use bonding for pregnancy
                        if (newCase.IsBonding)
                        {
                            theCase.Metadata.SetRawValue("WillUseBonding", newCase.IsBonding);
                            if (newCase.BondingStartDate.HasValue)
                                theCase.SetCaseEvent(CaseEventType.BondingStartDate, newCase.BondingStartDate.Value);
                            if (newCase.BondingEndDate.HasValue)
                                theCase.SetCaseEvent(CaseEventType.BondingEndDate, newCase.BondingEndDate.Value);
                        }

                        //medical complication for pregnancy
                        theCase.Disability.MedicalComplications = newCase.IsMedicalComplication;
                    }


                    theCase.Metadata.SetRawValue("IsWorkRelated", newCase.IsWorkRelated);
                    if (theCase != null && newCase != null)
                    {
                        theCase.WorkRelated = newCase.WorkRelatedInfo != null ? newCase.WorkRelatedInfo.ApplyModel(theCase) : null;
                        if ((theCase.Reason.Flags & AbsenceReasonFlag.RequiresRelationship) == AbsenceReasonFlag.RequiresRelationship)
                        {
                            EmployeeContact contact = newCase.Contact.ApplyToDataModel();
                            if (string.IsNullOrWhiteSpace(contact.Id))
                            {
                                contact.CustomerId = theCase.CustomerId;
                                contact.EmployerId = theCase.EmployerId;
                                contact.EmployeeId = theCase.Employee.Id;
                                contact.EmployeeNumber = theCase.Employee.EmployeeNumber;
                                contact.Save();
                            }
                            theCase.Contact = contact;
                        }

                        var loa = eligibilityService.RunEligibility(theCase);

                        caseService.UpdateCase(loa.Case, CaseEventType.CaseCreated);
                    }
                    return Json(new JsonResponse()
                    {
                        Redirect = true,
                        RedirectUrl = Url.RouteUrl("CaseComplete", new { caseId = theCase.Id })
                    });
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return Json(new JsonResponse(ModelState));
            }
        }

        [Secure]
        [HttpGet, Route("CaseComplete/{caseId}", Name = "CaseComplete"), Title("Case Complete")]
        public ActionResult CaseComplete(string caseId)
        {
            using (var caseService = new CaseService())
            {
                CaseViewModel theCase = new CaseViewModel(caseService.GetCaseById(caseId));
                return View(theCase);
            }

        }

        #endregion

        #region Employee Contacts
        [HttpGet, Secure("EditCase", "CreateCase")]
        [Route("Request/GetEmployeeContacts", Name = "GetEmployeeContacts")]
        public JsonResult GetEmployeeContacts(string employeeId, string absenceReasonCode)
        {
            try
            {
                List<ContactViewModel> employeePersonalContacts = new List<ContactViewModel>();
                using (var employeeService = new EmployeeService())
                {
                    employeePersonalContacts = employeeService.GetEmployeeContactsByAbsenceReasonCode(employeeId, absenceReasonCode).Select(ec => new ContactViewModel(ec)).ToList();
                }
                return Json(employeePersonalContacts, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JsonResponse()
                {
                    Error = true,
                    Messages = new List<string>() { ex.Message }
                });
            }
        }

        [Secure("EditCase", "CreateCase"), HttpGet]
        [Route("Request/EmployeeContactTypes", Name = "EmployeeContactTypes")]
        public JsonResult EmployeeContactTypes(string absenceReasonCode)
        {
            try
            {
                List<ContactTypeViewModel> contactTypes = new List<ContactTypeViewModel>();
                using (var employeeService = new EmployeeService())
                {
                    contactTypes = employeeService.GetEmployeeContactTypesByAbsenceReasonCode(absenceReasonCode).Select(ec => new ContactTypeViewModel(ec)).OrderBy(ct => ct.Name).ToList();
                }

                return Json(contactTypes, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JsonResponse()
                {
                    Error = true,
                    Messages = new List<string>() { ex.Message }
                });
            }

        }
        #endregion

        #region New Accommodation
        [Secure("CreateCase")]
        [HttpGet, Route("NewAccommodation/{employeeId?}", Name = "NewAccommodation"), Title("New Accommodation")]
        public ActionResult NewAccommodation(string employeeId)
        {
            NewAccommodationViewModel newAccommodation = new NewAccommodationViewModel(employeeId);
            newAccommodation.PopulatePersonalInfo();
            newAccommodation.PopulateAccommodations();
            return View(newAccommodation);
        }

        [Secure("CreateCase")]
        [HttpGet, Route("NewAccommodationData/{accomodationName?}", Name = "NewAccommodationData"), Title("NewAccommodationData")]
        public ActionResult NewAccommodationData(string accomodationName)
        {
            NewAccommodationViewModel newAccommodation = new NewAccommodationViewModel();
            newAccommodation.PopulateAccommodations();

            string output = newAccommodation.AccommodationTypeCategories.FirstOrDefault(i => i.Name == accomodationName).Duration.ToString();
            return Json(output, JsonRequestBehavior.AllowGet);
        }

        [Secure("CreateCase")]
        [HttpPost, Route("NewAccommodation", Name = "NewAccommodationSave")]
        [ValidateAntiForgeryToken]
        public ActionResult NewAccommodationSave(NewAccommodationViewModel newAccommodation)
        {
            if (!ModelState.IsValid)
                return Json(new JsonResponse(ModelState));

            try
            {
                using (var caseService = new CaseService())
                using (var employeeService = new EmployeeService())
                using (var eligibilityService = new EligibilityService())
                {
                    AbsenceReason accommodation = caseService.AbsenceReasonGetByCode("ACCOMM");
                    Case theCase = caseService.CreateCase(CaseStatus.Requested, newAccommodation.EmployeeId, newAccommodation.StartDate.Value, newAccommodation.EndDate, CaseType.Administrative, accommodation.Code, null, newAccommodation.Details);
                    if (newAccommodation.AlternateInfo != null)
                        newAccommodation.AlternateInfo.ApplyAlternateInfo(theCase);

                    theCase.AccommodationRequest = newAccommodation.CreateAccommodation(theCase);
                    eligibilityService.RunEligibility(theCase);
                    caseService.UpdateCase(theCase, CaseEventType.CaseCreated);
                    return Json(new JsonResponse()
                    {
                        Redirect = true,
                        RedirectUrl = Url.RouteUrl("CaseComplete", new { caseId = theCase.Id })
                    });
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return Json(new JsonResponse(ModelState));
            }
        }

        #endregion

        #region Time Off Request

        [HttpGet, Route("GetTimeOffRequest/{employeeId?}", Name = "GetTimeOffRequest"), Title("Get Time Off Request")]
        public JsonResult GetTimeOffRequest(string caseId, string requestDate)
        {
            try
            {
                DateTime date;

                if (!DateTime.TryParse(requestDate, out date))
                {
                    return Json(new JsonResponse() { Error = true, Messages = new List<string>() { "Requested date was in an invalid format" } }, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrWhiteSpace(caseId))
                {
                    return Json(new JsonResponse() { Error = true, Messages = new List<string>() { "CaseId was not provided" } }, JsonRequestBehavior.AllowGet);
                }

                Case c = Case.GetById(caseId);
                if (c == null)
                {
                    return Json(new JsonResponse() { Error = true, Messages = new List<string>() { "Case not found" } }, JsonRequestBehavior.AllowGet);
                }

                //Need policy summary data
                List<PolicySummary> ps = null;
                using (var service = new CaseService())
                {
                    ps = service.GetEmployeePolicySummaryByCaseId(caseId, null);
                }

                var seg = c.Segments.Where(s => s.Type == CaseType.Intermittent && s.Status != CaseStatus.Cancelled && date.DateInRange(s.StartDate, s.EndDate)).FirstOrDefault();

                if (seg == null)
                {
                    return Json(new JsonResponse() { Error = true, Messages = new List<string>() { "Invalid request, requested date is not part of an intermittent portion of this case" } }, JsonRequestBehavior.AllowGet);
                }

                List<TimeOffRequestViewModel> modelList = new List<TimeOffRequestViewModel>();

                List<IntermittentTimeRequest> intermittentTorList = seg.UserRequests.Where(u => u.RequestDate.Date == date.Date).ToList();
                if (intermittentTorList != null && intermittentTorList.Any())
                {
                    foreach (var intermittentRequest in intermittentTorList)
                    {
                        TimeOffRequestViewModel model = new TimeOffRequestViewModel();
                        model.CaseId = caseId;
                        model.DateOff = date;
                        model.IsEditMode = true;
                        model.DateOff = intermittentRequest.RequestDate;
                        model.IntermittentType = intermittentRequest.IntermittentType;
                        model.Id = intermittentRequest.Id;
                        model.StartTimeForLeave = intermittentRequest.StartTimeForLeave;
                        model.EndTimeForLeave = intermittentRequest.EndTimeForLeave;
                        model.TimeOff = intermittentRequest.TotalTime;
                        modelList.Add(model);
                    }
                }

                return Json(modelList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JsonResponse() { Error = true, Messages = new List<string>() { ex.Message } }, JsonRequestBehavior.AllowGet);
            }
        }

        [Secure("AddITOR")]
        [HttpGet, Route("TimeOffRequest/{employeeId?}", Name = "NewTimeOffRequest"), Title("New Time Off Request")]
        public ActionResult TimeOffRequest(string employeeId)
        {
            if (string.IsNullOrEmpty(employeeId))
                employeeId = Current.User().EmployeeId;

            TimeOffRequestViewModel model = new TimeOffRequestViewModel()
            {
                EmployeeId = employeeId
            };
            return View(model);
        }

        [HttpGet, Route("GetTimeOffRequestDetails", Name = "GetTimeOffRequestDetails"), Title("GetTimeOffRequestDetails")]
        public ActionResult Details(int Id)
        {
            TimeOffRequestViewModel model = new TimeOffRequestViewModel()
            {
                ItorId = Id
            };
            return PartialView("_TimeOffRequest", model);
        }

        [Secure("AddITOR")]
        [HttpPost, Route("TimeOffRequest/{employeeId?}", Name = "SaveTimeOffRequest")]
        [ValidateApiAntiForgeryToken]
        public ActionResult SaveTimeOffRequest(FormCollection timeOffRequestModel, string employeeId)
        {
            // Check the Collection Values          
            var iTorCounterId = int.Parse(timeOffRequestModel["ActiveItorId"]);

            if (!ModelState.IsValid)
                return Json(new JsonResponse(ModelState));

            try
            {
                if (string.IsNullOrEmpty(employeeId.Trim()))
                {
                    ModelState.AddModelError("", "Employee not found!");
                    return Json(new JsonResponse(ModelState));
                }

                TimeOffRequestViewModel timeOffRequest = new TimeOffRequestViewModel();
                var _IsOverride = timeOffRequestModel["IsOverride"];
                var _IsEditMode = timeOffRequestModel["IsEditMode"];
                var _intermittentType = "IntermittentType_" + iTorCounterId;
                var _startTime = "StartTimeForLeave_" + iTorCounterId;
                var _endTime = "EndTimeForLeave_" + iTorCounterId;
                var _timeOff = "timeOff_" + iTorCounterId;
                var _dateOff = "dateOff_" + iTorCounterId;
                var _iD = "Id_" + iTorCounterId;

                if (_IsOverride != null && _IsOverride != "")
                {
                    timeOffRequest.IsOverride = Convert.ToBoolean(timeOffRequestModel["IsOverride"]);
                }

                if (timeOffRequestModel[_iD] != null && timeOffRequestModel[_iD] != "")
                {
                    timeOffRequest.Id = Guid.Parse(timeOffRequestModel[_iD]);
                }

                if (_IsOverride != null && _IsOverride != "")
                {
                    timeOffRequest.IsOverride = Convert.ToBoolean(timeOffRequestModel["IsOverride"]);
                }
                if (_IsEditMode != null && _IsEditMode != "")
                {
                    timeOffRequest.IsEditMode = Convert.ToBoolean(timeOffRequestModel["IsEditMode"]);
                }
                timeOffRequest.IntermittentType = timeOffRequestModel[_intermittentType] != null ? (IntermittentType)Enum.Parse(typeof(IntermittentType), timeOffRequestModel[_intermittentType]) : 0;
                timeOffRequest.StartTimeForLeave = new TimeOfDay(timeOffRequestModel[_startTime]);
                timeOffRequest.EndTimeForLeave = new TimeOfDay(timeOffRequestModel[_endTime]);
                timeOffRequest.TimeOff = timeOffRequestModel[_timeOff];
                timeOffRequest.DateOff = Convert.ToDateTime(timeOffRequestModel[_dateOff]);
                timeOffRequest.EmployeeId = timeOffRequestModel["EmployeeId"];
                timeOffRequest.CaseId = timeOffRequestModel["CaseId"];                

                if (timeOffRequest.IsOverride.HasValue && timeOffRequest.IsOverride == false)
                {
                    return Json(new JsonResponse()
                    {
                        Redirect = true,
                        RedirectUrl = Url.RouteUrl(HomeRoute)
                    });
                }
                using (var caseService = new CaseService())
                {
                    Case theCase = timeOffRequest.GetCase();
                    theCase = caseService.CreateOrModifyTimeOffRequest(theCase, timeOffRequest.ApplyToDataModel(theCase));
                    caseService.UpdateCase(theCase);
                }

            }
            catch (AbsenceSoftAggregateException aggEx)
            {
                foreach (var msg in aggEx.Messages)
                {
                    ModelState.AddModelError("", msg);
                }
                return Json(new JsonResponse(ModelState));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return Json(new JsonResponse(ModelState));
            }


            return Json(new JsonResponse()
            {
                Redirect = true,
                RedirectUrl = Url.RouteUrl(HomeRoute)
            });
        }


        [HttpDelete]
        [Route("DeleteTimeOffRequestDetails", Name = "DeleteTimeOffRequestDetails"), Title("DeleteTimeOffRequestDetails")]
        public ActionResult DeleteIntermittentTimeOffRequestByDate(FormCollection timeOffRequest)
        {
            // Check the Collection Values          
            var iTorCounterId = int.Parse(timeOffRequest["ActiveItorId"]);

            TimeOffRequestViewModel timeOffRequestModel = new TimeOffRequestViewModel();      
            
            var _dateOff = "dateOff_" + iTorCounterId;
            var _iD = "Id_" + iTorCounterId;
           
            if (timeOffRequest[_iD] != null && timeOffRequest[_iD] != "")
            {
                timeOffRequestModel.Id = Guid.Parse(timeOffRequest[_iD]);
            }

            timeOffRequestModel.DateOff = Convert.ToDateTime(timeOffRequest[_dateOff]);
            timeOffRequestModel.EmployeeId = timeOffRequest["EmployeeId"];
            timeOffRequestModel.CaseId = timeOffRequest["CaseId"];
            timeOffRequestModel.IsDelete = Convert.ToBoolean(timeOffRequest["IsDelete"]);

            if (string.IsNullOrWhiteSpace(timeOffRequestModel.CaseId))
            {
                return Json(new JsonResponse() { Error = true, Messages = new List<string>() { "CaseId was not provided" } }, JsonRequestBehavior.AllowGet);
            }
            try
            {
                using (var _CaseService = new CaseService())
                {
                    Case existingCase = _CaseService.GetCaseById(timeOffRequestModel.CaseId);

                    if (existingCase == null)
                    {
                        return Json(new JsonResponse() { Error = true, Messages = new List<string>() { "Case not found" } }, JsonRequestBehavior.AllowGet);
                    }

                    var segments = existingCase.Segments
                    .Where(summary => summary.Type == CaseType.Intermittent &&
                        summary.Status != CaseStatus.Cancelled)
                    .FirstOrDefault();

                    if (segments == null)
                    {
                        // Show message
                        return Json(new JsonResponse() { Error = true, Messages = new List<string>() { "Intermittent Time Requests not found for given details" } }, JsonRequestBehavior.AllowGet);
                    }

                    List<IntermittentTimeRequest> intermittentTimeRequests = segments.UserRequests.Where(u => u.RequestDate == timeOffRequestModel.DateOff).ToList();

                    if (!intermittentTimeRequests.Any())
                    {
                        return Json(new JsonResponse() { Error = true, Messages = new List<string>() { "Intermittent Time Requests not found for given details" } }, JsonRequestBehavior.AllowGet);
                    }

                    _CaseService.CreateOrModifyTimeOffRequest(existingCase, intermittentTimeRequests);

                    // update case activity record as removed request           
                    _CaseService.UpdateTimeOfRequestNoteToRemovedRequest(existingCase, intermittentTimeRequests);

                    if (intermittentTimeRequests.Any() && timeOffRequestModel.IsDelete)
                    {
                        // Delete all ITOR request for given request date
                        segments.UserRequests.RemoveAll(a => a.RequestDate == timeOffRequestModel.DateOff);
                    }
                    else
                    {
                        // remove ITOR request for given request date
                        segments.UserRequests.RemoveAll(a => a.RequestDate == timeOffRequestModel.DateOff && a.Id == timeOffRequestModel.Id);
                    }
                    existingCase = _CaseService.UpdateCase(existingCase);
                }
            }

            catch (Exception ex)
            {
                return Json(new JsonResponse() { Error = true, Messages = new List<string>() { "Error Deleting Time Off Request" } }, JsonRequestBehavior.AllowGet);
            }
            return Json(new JsonResponse()
            {
                Redirect = true,
                RedirectUrl = Url.RouteUrl(HomeRoute)
            });
        }

        #endregion
    }
}
