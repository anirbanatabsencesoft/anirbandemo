﻿(function (AbsenceSoft, $) {

    var $context = null;
    var employeeRestrictions = [];
    var caseId = null;
    var employeeId = null;
    var caseEndDate = null;

    //Suppress Required Validation on hidden fields.
    jQuery.validator.defaults.ignore = ":hidden";
    $(document).ready(function () {
        $context = AbsenceSoft.WorkRelatedViewModel.$context = $('.work-related-editor');
        if ($context.length == 0)
            return;

        caseId = $context.find('#CaseId').val();
        employeeId = $context.find('#EmployeeId').val();
        $context.find('.sharps-question').on('change', 'input[type=radio]', AbsenceSoft.WorkRelatedViewModel.IsSharpsChanged);
        $context.find('.sharps-protection').on('change', 'input[type=radio]', AbsenceSoft.WorkRelatedViewModel.HasEngineeredProtectionChanged);
        $context.find('.job-classification').on('change', AbsenceSoft.WorkRelatedViewModel.JobClassChanged);
        $context.find('.location-and-dept').on('change', AbsenceSoft.WorkRelatedViewModel.LocationChanged);
        $context.find('.procedure').on('change', AbsenceSoft.WorkRelatedViewModel.ProcedureChanged);
        $context.find('#IllnessOrInjuryDate').on('change', AbsenceSoft.WorkRelatedViewModel.IllnessOrInjuryDateChanged);


        caseEndDate = Date.parse($context.find('#CaseEndDate').val());

        //$.ajax({
        //    url: '/Cases/' + caseId + '/EmployeeRestrictions/' + employeeId,
        //    success: function (result) {
        //        employeeRestrictions = result;
        //    },
        //    failure: function (err) {

        //    }
        //});
    });

    AbsenceSoft.WorkRelatedViewModel = AbsenceSoft.WorkRelatedViewModel || {};

    AbsenceSoft.WorkRelatedViewModel.IsSharpsChanged = function () {
        var $radio = $(this);
        AbsenceSoft.WorkRelatedViewModel.IsSharps = ($radio.val() === true || $radio.val() === "true" || $radio.val() === "True");
        if (AbsenceSoft.WorkRelatedViewModel.IsSharps) {
            $context.find('.i-have-sharps:hidden').slideDown();
        } else {
            $context.find('.i-have-sharps:visible').slideUp();
        }
    };

    AbsenceSoft.WorkRelatedViewModel.HasEngineeredProtectionChanged = function () {
        var $radio = $(this);
        AbsenceSoft.WorkRelatedViewModel.HasEngineeredProtection = ($radio.val() === true || $radio.val() === "true" || $radio.val() === "True");
        if (AbsenceSoft.WorkRelatedViewModel.HasEngineeredProtection) {
            $context.find('.HaveEngineeredInjuryProtection:hidden').slideDown();
        } else {
            $context.find('.HaveEngineeredInjuryProtection:visible').slideUp();
        }
    };

    AbsenceSoft.WorkRelatedViewModel.JobClassChanged = function () {
        var $this = $(this);
        var isOther = $this.val() === "0";
        if (isOther) {
            $context.find('.job-classification-other:hidden').slideDown();
        } else {
            $context.find('.job-classification-other:visible').slideUp();
        }
    };

    AbsenceSoft.WorkRelatedViewModel.LocationChanged = function () {
        var $this = $(this);
        var isOther = $this.val() === "0";
        if (isOther) {
            $context.find('.location-and-dept-other:hidden').slideDown();
        } else {
            $context.find('.location-and-dept-other:visible').slideUp();
        }
    };

    AbsenceSoft.WorkRelatedViewModel.ProcedureChanged = function () {
        var $this = $(this);
        var isOther = $this.val() === "0";
        if (isOther) {
            $context.find('.procedure-other:hidden').slideDown();
        } else {
            $context.find('.procedure-other:visible').slideUp();
        }
    };

    AbsenceSoft.WorkRelatedViewModel.IllnessOrInjuryDateChanged = function () {
        var $this = $(this);
        AbsenceSoft.WorkRelatedViewModel.CalculateDaysOnJobRestriction(($this).val());
    };

    AbsenceSoft.WorkRelatedViewModel.CalculateDaysOnJobRestriction = function () {
        var dateOfInjury = Date.parse($context.find('#IllnessOrInjuryDate').val());
        var injuryDatePlusOne = 0;
        var restriction = null;
        var restrictionStartDate = null;
        var restrictionEndDate = null;
        var accumulatedDaysOnJobRestriction = 0;
        var daysCounted = [];

        if (isNaN(dateOfInjury))
            return; // No Injury Date

        injuryDatePlusOne = dateOfInjury + 24 * 3600 * 1000; // Add one day's worth of milliseconds. (e.g. start counting on the next day)

        if (!isNaN(caseEndDate)) {

            // Iterate through each employee restriction and accumulate the days on job transfer.
            for (var i = 0; i < employeeRestrictions.length; i++) {
                var startCountingDate = injuryDatePlusOne, endCountingDate = caseEndDate;
                var currentDayCountIndex = 0;

                restriction = employeeRestrictions[i].Restriction;
                restrictionStartDate = Date.parse(restriction.Dates.StartDate);
                restrictionEndDate = Date.parse(restriction.Dates.EndDate);

                // Determine the start date to begin counting days on restriction.
                if (!isNaN(restrictionStartDate) && restrictionStartDate > injuryDatePlusOne) {
                    //start date is restriction start date
                    startCountingDate = restrictionStartDate;
                    currentDayCountIndex = parseInt((restrictionStartDate - injuryDatePlusOne) / (24 * 3600 * 1000));
                }

                // Determine the end date to stop counting days on restriction.
                if (!isNaN(restrictionEndDate) && restrictionEndDate < caseEndDate) {
                    //start date is restriction start date
                    endCountingDate = restrictionEndDate;
                }

                // Fill up the days. Then move to the next restriction.
                var range = (endCountingDate - startCountingDate) / (24 * 3600 * 1000);
                if (range < 1) {
                    continue;
                }

                for (var k = currentDayCountIndex; k <= (currentDayCountIndex + range); k++) {
                    // Update the nth day after the injury to 1. These will be summed up later.
                    daysCounted[k] = 1;
                }
            }

            // Sum up all the 1's in the array.
            daysCounted.forEach(function (item) {
                accumulatedDaysOnJobRestriction += item;
            });

            // Add together all the days by summing the entries in the array. Only 1 or 0.
            // Days will not be double counted due to the fact that daysCounted[k] 
            if (accumulatedDaysOnJobRestriction > 180) {
                accumulatedDaysOnJobRestriction = 180;
            }

            $('#DaysOnJobTransferOrRestriction').val(accumulatedDaysOnJobRestriction);
        }
    }
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);