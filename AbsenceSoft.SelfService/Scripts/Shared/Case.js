﻿(function (AbsenceSoft, $) {
    var deleteAttachment = function (e) {
        var deleteLink = $(this);
        $.ajax({
            method: 'POST',
            url: deleteLink.prop('href'),
            success: function (data) {
                if (!data.Error) {
                    deleteLink.parents('li').remove();
                }
            }
        })

        return false;
    }

    var hideNewNoteButton = function () {
        var dataTarget = $(this).attr('id');
        $('.btn-note').each(function (index, element) {
            var btn = $(element);
            if (btn.data('target') == '#' + dataTarget) {
                btn.addClass('hide');
            }
        });
    };

    var showNewNoteButton = function () {
        var dataTarget = $(this).attr('id');
        $('.btn-note').each(function (index, element) {
            var btn = $(element);
            if (btn.data('target') == '#' + dataTarget) {
                btn.removeClass('hide');
            }
        });
    };
    var initializeDeleteEvent = function () {
        $('.attachment-delete').click(deleteAttachment);
    };
    AbsenceSoft.DeleteAttachment = AbsenceSoft.DeleteAttachment || {};
    AbsenceSoft.DeleteAttachment.InitializeDeleteEvent = initializeDeleteEvent;
    $(document).ready(function () {
        AbsenceSoft.DeleteAttachment.InitializeDeleteEvent();
        $('.new-note-collapse').on('show.bs.collapse', hideNewNoteButton);
        $('.new-note-collapse').on('hide.bs.collapse', showNewNoteButton);
    });
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);
