﻿(function (AbsenceSoft, $) {
    var fileData;

    var fileAdded = function (e, data) {
        $(this).find('.attachment-file-name').html(data.files[0].name);
        $(this).find('.attachment-file-name').removeClass('errShow');
        fileData = data;
    };

    var handleFormSubmission = function (e) {
        if (fileData && fileData.submit) {
            fileData.submit().success(function () {
                noty({ type: 'success', layout: 'topRight', text: 'File has been successfully uploaded' });
                window.setTimeout(function () { window.location.reload(true) }, 3000)
            })
        }
        else
        {
            $('.attachment-file-name').html('Please select a file to attach').addClass('errShow'); // Attachment validation
        }
        return false;
    };

    var prepareFileUpload = function () {
        var caseAttachmentForm = $(this).find('.case-attachment-form');
        caseAttachmentForm.fileupload({
            dropZone: caseAttachmentForm,
            autoUpload: false,
            add: fileAdded
        }).submit(handleFormSubmission);
    }

    var removeFileUpload = function () {
        $(this).find('.case-attachment-form').fileupload('destroy');
        $(this).find('.attachment-file-name').html('').removeClass('errShow');
        fileData = null;
    }

    var bindAttachmentModalOpenAndClose = function () {
        $('.attachment-modal').off('shown.bs.modal').on('shown.bs.modal', prepareFileUpload);
        $('.attachment-modal').off('hidden.bs.modal').on('hidden.bs.modal', removeFileUpload);
    }

    AbsenceSoft.UploadAttachment = AbsenceSoft.UploadAttachment || {};
    AbsenceSoft.UploadAttachment.BindModal = bindAttachmentModalOpenAndClose;
    $(document).ready(bindAttachmentModalOpenAndClose);
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);
