﻿(function (AbsenceSoft, $) {
    var textPrefixForCase = "Case # ";
    var searchOptions = {
        placeholder: 'Search for a Team Member or Case',
        maximumSelectionLength: 1,
        minimumInputLength: 3,
        ajax: {
            url: '/SearchTeamByEmployeeNameNumberOrCase',
            data: function(params){
                return {
                    employeeNameNumberOrCase: params.term
                }
            },
            processResults: function (data, params) {
                var results = [];
                if (data.EmployeeViewModels != undefined) {
                    for (var i = 0; i < data.EmployeeViewModels.length; i++) {
                        if (data.EmployeeViewModels[i].DisplayName != undefined) {
                            results.push({
                                id: data.EmployeeViewModels[i].Id,
                                text: data.EmployeeViewModels[i].DisplayName
                            })
                        }
                    };
                }
                if (data.CaseViewModels != undefined) {
                    for (var i = 0; i < data.CaseViewModels.length; i++) {
                        if (data.CaseViewModels[i].CaseNumber != undefined) {
                            results.push({
                                id: data.CaseViewModels[i].Id,
                                text: textPrefixForCase + data.CaseViewModels[i].CaseNumber
                            })
                        }
                    };
                }

                return {
                    results: results
                };
            },
            transport: function (params, success, failure) {  // Search validation
                if (!params.data.employeeNameNumberOrCase.trim().length) {
                    return false;
                }
                var $request = $.ajax(params);
                $request.then(success);
                $request.fail(failure);
                return $request;
            }
        }
    };

    var redirectOnSelection = function () {
        var currentVal = $(this).val();
        var currentText = $(this).text();

        if (currentText != null) {
            if (currentText.indexOf(textPrefixForCase) !== -1) {
                window.location.href = '/case/' + currentVal;
            }
            else {
                if (currentVal)
                    window.location.href = '/employee/' + currentVal;
            }
        }
    };


    $(document).ready(function () {
        $('#searchMyTeam').select2(searchOptions);
        $('#searchMyTeam').change(redirectOnSelection);
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);