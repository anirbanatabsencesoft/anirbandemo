﻿(function (AbsenceSoft, $) {
    var buildFilterData = function () {
        return {
            page: $('#page').val(),
            status: $('.case-status:checked').val(),
            includeApproved: $('#includeApproved').is(':checked'),
            includePending: $('#includePending').is(':checked'),
            includeDenied: $('#includeDenied').is(':checked')
        };
    }

    var loadCases = function (e, reset) {
        $.ajax({
            url: '/',
            data: buildFilterData(),
            success: function (data) {
                if (reset) {
                    $('#caseList').html(data);
                } else {
                    $('#caseList').append(data);
                }
                if (data == '') {
                    $('#loadMoreCases').addClass('hide');
                    $('#noCases').removeClass('hide');
                }
                else
                {
                    $('#loadMoreCases').removeClass('hide');
                    $('#noCases').addClass('hide');
                }

                AbsenceSoft.UploadAttachment.BindModal();
                AbsenceSoft.DeleteAttachment.InitializeDeleteEvent();
            }
        });
    };

    var applyFilters = function (e) {
        $('#page').val(1);
        loadCases(e, true);
    }

    var loadNextCases = function (e) {
        var currentPage = parseInt($('#page').val(), 10);
        $('#page').val(currentPage += 1);
        loadCases(e, false);
    }

    var clearFilters = function (e) {
        $('.case-status').prop('checked', false).parents('.btn').removeClass('active');
        $('.case-adjudication').prop('checked', true);
        $('#page').val(1);
        $('#loadMoreCases').removeClass('hide');
        $('#noCases').addClass('hide');
        loadCases(e, true);
    };

    $(document).ready(function () {
        if ($('#caseFilters').length == 0)
            return;

        $('#applyFilters').click(applyFilters);
        $('#clearFilters').click(clearFilters);
        $('#loadMoreCases').click(loadNextCases);

    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);