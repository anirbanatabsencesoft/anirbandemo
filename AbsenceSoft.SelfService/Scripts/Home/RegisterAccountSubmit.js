﻿(function (AbsenceSoft, $) {
    AbsenceSoft.RegisterAccountSubmit = AbsenceSoft.RegisterAccountSubmit || {};

    var resendClicked = function () {
        $('#actualResendValue').val('True');
        submitForm();
    }

    var resendToAltClicked = function () {
        $('#actualResendToAltValue').val('True');
        resendClicked();
    }

    var submitForm = function () {
        $('#registerAccountSubmitForm').submit();
    }

    $(document).ready(function () {
        $('#resend').click(resendClicked);
        $('#resendToAlt').click(resendToAltClicked);
    })
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);