﻿/// <reference path="../Global/api-framework.js" />

(function (ESS, $) {
    $(document).ready(function () {

        ESS.ReportOperations = ESS.ReportOperations || {};

        //define the module
        ESS.ReportOperations.Module = "reporting";

        //make sure the information box is hidden at the start
        $("#report-saved-alert").hide();

        //trigger change in hidden field
        $('#adHocReportId').change(function () {
            showHideDeleteButton();         
        });

        //show hide delete button
        var showHideDeleteButton = function () {
            var id = $('#adHocReportId').val();
            if (id) {
                $('#deleteImageButton').show();
                //if (window.location.host.includes("stage")) {         // Existing ES6 - includes()
                if (window.location.host.indexOf("stage") >= 0) {        // Added equivalent ES5 line to above line
                    $('#exportImageButton').show();
                } else {
                    $('#exportImageButton').hide();
                } 
            }
            else {
                $('#deleteImageButton').hide();
                $('#exportImageButton').hide();
            }
        }

        showHideDeleteButton();

        //add click event
        $("#deleteAdHocReport").click(function () {
            ESS.ReportOperations.deleteSavedAdhocReport();
        });

        //delete a saved report
        ESS.ReportOperations.deleteSavedAdhocReport = function () {
            var reportId = $('#adHocReportId').val();
            var data = { AdhocReportIds: reportId };
            var endPoint = "reports/adhoc/delete";

            try {
                ESS.HttpUtils.Delete(ESS.ReportOperations.Module, endPoint, data, function (result) {
                    var data = JSON.parse(result);
                    $("#report-saved-alert").show();
                    if (data.success == true) {
                        $("#report-saved-alert").show();
                        $("#report-saved-alert span").text("Report deleted successfully");
                        //close the window
                        window.setTimeout(function () {
                            $(location).attr("href", "/reports/adhocreports");
                        }, 1000);
                    }
                    else {
                        $("#report-saved-alert").show();
                        $("#report-saved-alert span").text("No data deleted");
                    }
                });
            }
            catch (ex) {
                $("#report-saved-alert").show();
                $("#report-saved-alert span").text(ex.message);
            }
        }

        //make sure the information box is hidden at the start
        $("#report-schedule-alert").hide();

        //schedule report
        ESS.ReportOperations.scheduleSavedReport = function () {           
            var endPoint = "reports/schedule";
            var frequency = $("#schedule-frequency").val();
            var scheduleday = $("#schedule-day").val();
            var scheduletime = $("#schedule-time");
            var data = scheduleReportReqObj;

            if (frequency == "0") { data.ScheduleInfo.WeeklyRecurEveryXWeeks = 0; }
            else { data.ScheduleInfo.WeeklyRecurEveryXWeeks = 1; }
            data.ScheduleInfo.WeeklyRecurType = 0;
            
            data.ScheduleInfo.WeeklySelectedDays = {
                "Sunday": scheduleday=="0"? true : false,
                "Monday": scheduleday == "1" ? true : false,
                "Tuesday": scheduleday == "2" ? true : false,
                "Wednesday": scheduleday == "3" ? true : false,
                "Thursday": scheduleday == "4" ? true : false,
                "Friday": scheduleday == "5" ? true : false,
                "Saturday": scheduleday == "6" ? true : false
            };
            
            try {               
                ESS.HttpUtils.Post(ESS.ReportOperations.Module, endPoint, data, function (result) {                   
                    var data = JSON.parse(result);
                    var data = result;                    
                    $("#report-schedule-alert").show();
                    if (JSON.parse(data).success == true) {
                        $("#report-schedule-alert").show();
                        $("#report-schedule-alert span").text("Report scheduled successfully.");                        
                    }
                    else {
                        $("#report-schedule-alert").show();
                        $("#report-schedule-alert span").text("Report scheduling failed.");
                    }
                });
            }
            catch (ex) {
                $("#report-schedule-alert").show();
                $("#report-schedule-alert span").text(ex.message);
            }
        }

        $('.alert .close').on('click', function (e) {
            $(this).parent().hide();
        });

    });

})(window.ESS = window.ESS || {}, jQuery);