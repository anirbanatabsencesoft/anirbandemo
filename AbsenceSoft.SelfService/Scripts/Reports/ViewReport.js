﻿(function (AbsenceSoft, $) {
    $(document).ready(function () {
        if ($('#viewReportForm').length == 0)
            return;

        var isReportRanAtLeastOnce = false;

        AbsenceSoft.Reports = AbsenceSoft.Reports || {};
        AbsenceSoft.Reports.TheReport = {};
        AbsenceSoft.Reports.ReportResults = {};
        AbsenceSoft.Reports.CriteriaString = '';

        AbsenceSoft.Reports.MakeStyledDropdown = function () {
            $('.styledddl').select2();
        }

        AbsenceSoft.Reports.MakeStyledDropdown();

        AbsenceSoft.Reports.Begin = function () {
            $.noty.closeAll();
            $('.run-button').attr('readonly', true).addClass('disabled');
            $('.fa-spinner').removeClass('hidden');
        };

        AbsenceSoft.Reports.Complete = function () {
            $('.run-button').attr('readonly', false).removeClass('disabled');
            $('.fa-spinner').addClass('hidden');
        };

        AbsenceSoft.Reports.Error = function (reportData) {
            AbsenceSoft.Reports.Complete();
            var message = "Unable to run report.";
            if (reportData) {
                message = reportData.Results.Error;
            }
            AbsenceSoft.Reports.SetChartError(message);
        }

        AbsenceSoft.Reports.Submit = function (e) {
            var command = $(document.activeElement).val();
            if (command.toLowerCase() == "export to pdf" || command.toLowerCase() == "export to csv") {
                var formData = $(this).serializeArray(); /// Serialize Array doesn't include the value of the submit button
                formData.push({
                    name: "command",
                    value: command
                });

                $.fileDownload($(this).prop('action'), {
                    httpMethod: 'POST',
                    data: formData,
                    successCallback: AbsenceSoft.Reports.Complete,
                    failCallback: AbsenceSoft.Reports.Error
                });
                AbsenceSoft.Reports.Begin();
                return false;
            } else if (AbsenceSoft.Reports.TheReport.Chart && command.toLowerCase() == "run report") {
                isReportRanAtLeastOnce = true;
                //Clear the Chart
                AbsenceSoft.Reports.ClearChart();

                // Get the chart filters that we will send to the server.
                var formFilters = AbsenceSoft.Reports.BuildChartFilters();
                if (formFilters === undefined || formFilters == null) {
                    return false;
                }

                // Force a command on the formFilters, so the server will know what command to execute.
                formFilters.push({
                    name: "command",
                    value: command
                });

                // Good to go. Make a call to the server with the filters to get the chart series.
                AbsenceSoft.Reports.GetReport(formFilters);

                // Let the user know someting is happening
                AbsenceSoft.Reports.Begin();

                // Overide the browser form submission.
                return false;
            }
            else if (command.toLowerCase() != "run report") {
                $('#runReport').focus();
                /// If we immediately call .click, document.activeElement hasn't updated yet
                /// Giving it a timeout lets the browser update before we call click
                window.setTimeout(function () {
                    $('#runReport').click();
                }, 50);

                return false;
            }

            AbsenceSoft.Reports.Begin();
        }

        AbsenceSoft.Reports.BuildChartFilters = function () {
            var formData = $('#viewReportForm').serializeArray();

            /// now to figure out the start and end date based on the duration
            var startDate = new Date();
            var endDate = new Date();

            if (AbsenceSoft.Reports.Criteria != null) {
                startDate = new Date(AbsenceSoft.Reports.Criteria.StartDate);
                endDate = new Date(AbsenceSoft.Reports.Criteria.EndDate);
            }

            var durationValue = $('.duration-control').val();

            // Find the Duraction Criteria form element, and set the value to monthly, yearly...
            for (var i = 0, len = formData.length; i < len; i++) {
                if (formData[i].value == "Duration") {
                    formData.push({
                        name: formData[i].name.replace('.Name', '.Value'),
                        value: durationValue
                    });
                    break;
                }
            }

            switch (durationValue) {
                case 'Monthly':
                    var selectedMonth = $('#selectedMonth').val();
                    /// Months are 0 based, so month - 1 gets actual month
                    startDate = new Date(startDate.getFullYear(), selectedMonth - 1, 1);
                    /// Setting the date of the next month to 0 gets the last day of the month we want
                    endDate = new Date(endDate.getFullYear(), selectedMonth, 0);
                    break;
                case 'Quarterly':
                    var selectedQuarter = $('#selectedQuarter').val();
                    startDate = new Date(startDate.getFullYear(), selectedQuarter * 3 - 3, 1);
                    endDate = new Date(startDate.getFullYear(), selectedQuarter * 3, 0);
                    break;
                case 'Yearly':
                    var selectedYear = $('#selectedYear').val();
                    startDate = new Date(selectedYear, 0, 1);
                    endDate = new Date(selectedYear, 11, 31);
                    break;
                case 'DateRange':
                    startDate = new Date(Date.parse($('#txtStartDate').val()));
                    if (isNaN(startDate)) {
                        startDate = new Date();
                        $('#txtStartDate').val(startDate.formatMMDDYYYY());
                    }

                    endDate = new Date(Date.parse($('#txtEndDate').val()));
                    if (isNaN(endDate)) {
                        endDate = startDate.addMonths(1);
                        $('#txtEndDate').val(endDate.formatMMDDYYYY());
                    }

                    if (endDate < startDate) {
                        AbsenceSoft.Reports.SetChartError("End Date must come before Start Date.");
                        return;
                    }

                    break;
                case 'Rolling':
                    startDate = new Date(Date.parse($('#rollingStartDate').val()));
                    if (isNaN(startDate)) {
                        startDate = new Date();
                        $('#rollingStartDate').val(startDate.formatMMDDYYYY());
                    }
                    // Rolling Year
                    endDate = new Date(startDate.getFullYear() + 1, startDate.getMonth(), startDate.getDate());
                    break;
                default:
                    endDate = new Date();
                    startDate = new Date(endDate.getFullYear() - 1, endDate.getMonth(), endDate.getDate());
                    break;
            }

            formData.push({
                name: 'Criteria.StartDate',
                value: startDate.toISOString()
            });

            formData.push({
                name: 'Criteria.EndDate',
                value: endDate.toISOString()
            });

            return formData;
        }

        AbsenceSoft.Reports.GetReport = function (postData) {
            // Send the filters to the server and get the report.
            $.ajax({
                method: 'POST',
                url: '/Reports/' + AbsenceSoft.Reports.TheReport.Id + '/View',
                data: postData,
                success: AbsenceSoft.Reports.ReportSuccess,
                error: AbsenceSoft.Reports.Error
            });
        }

        AbsenceSoft.Reports.SetChartMessage = function (message) {
            $('#theChart').html('<div class="text-center"><p>' + message + '<p></div>');
        }

        AbsenceSoft.Reports.SetChartError = function (message) {
            $('#theChart').html('<div class="error text-center"><p>' + message + '</p></div>')
        }

        AbsenceSoft.Reports.ClearChart = function () {
            // Clear the chart
            $('#chartCriteria').text('');
            $('#chartTitle').text('');
            $('#theChart').html('');

            // Clear any messages
            $.noty.closeAll();

            // Implement any other UI logic (add/remove/hide things) below
        };

        AbsenceSoft.Reports.ReportSuccess = function (reportData) {
            AbsenceSoft.Reports.Complete();
            if (reportData.Results.Success) {
                if (reportData.Results.SeriesData != null && reportData.Results.SeriesData.length > 0) {
                    AbsenceSoft.Reports.ReportResults = reportData.Results;
                    AbsenceSoft.Reports.Criteria = reportData.Criteria;
                    AbsenceSoft.Reports.CriteriaString = reportData.Criteria.PlainEnglish;
                    google.load('visualization', '1', { packages: ['corechart'], callback: AbsenceSoft.Reports.DrawChart });
                } else {
                    AbsenceSoft.Reports.NoChartResults();
                }
            } else {
                AbsenceSoft.Reports.Error(reportData);
            }
        }

        AbsenceSoft.Reports.NoChartResults = function () {
            AbsenceSoft.Reports.ClearChart();
            AbsenceSoft.Reports.SetChartMessage("No results found.");
        }

        // This method is called when the google visualization module is loaded and available.
        AbsenceSoft.Reports.DrawChart = function () {
            var seriesData = AbsenceSoft.Reports.ReportResults.SeriesData;
            /// early exit if there is no data for the chart to display
            /// Google pitches a fit if we send empty data
            if (seriesData.length < 1) {
                AbsenceSoft.Reports.NoChartResults();
                return;
            }

            var data = google.visualization.arrayToDataTable(seriesData);

            /// early exit if there is no data for the chart to display
            /// Google pitches a fit if we send empty data
            if (data.Nf == null || data.Nf.length < 1) {
                AbsenceSoft.Reports.NoChartResults();
                return;
            }

            var options = AbsenceSoft.Reports.TheReport.Chart;
            var chartOptions = {
                'height': options.Height,
                //'chartArea': { top: options.Title == "" ? 0 : '200px', right: 0, bottom: 0, width: '80%' },
                'legend': { position: 'bottom', alignment: 'center', maxLines: 5, textStyle: { fontSize: 12 } },
                'is3D': options.is3D,
                'isStacked': options.isStacked,
                'bar': { groupWidth: '60' }
            }

            var chartWrapper = new google.visualization.ChartWrapper({
                'chartType': options.Type,
                'containerId': 'theChart',
                'options': chartOptions,
            });

            chartWrapper.setDataTable(data);
            $('#chartCriteria').text(AbsenceSoft.Reports.CriteriaString);
            $('#chartTitle').text(AbsenceSoft.Reports.TheReport.Category + ' ' + AbsenceSoft.Reports.TheReport.Name);
            chartWrapper.draw();
        }

        AbsenceSoft.Reports.DurationChange = function () {
            var duration = $(this).val().toLowerCase();

            $('.duration-inputs').addClass('hidden').find(':input').prop('disabled', true);
            $('#' + duration + 'Duration').removeClass('hidden').find(':input').prop('disabled', false);
            AbsenceSoft.Reports.ClearChart();

            if (isReportRanAtLeastOnce) {
                AbsenceSoft.Reports.SetChartMessage("click the Run Report button to refresh.");
            }
        }

        AbsenceSoft.Reports.FilterValuesChange = function () {
            AbsenceSoft.Reports.ClearChart();
            if (isReportRanAtLeastOnce) {
                AbsenceSoft.Reports.SetChartMessage("click the Run Report button to refresh.");
            }
        }

        // Get the report definition and wait for the user to click run report (or export).
        $.ajax({
            url: '/Reports/GetReportById?Id=' + $('#reportId').val(),
            success: function (report) {
                AbsenceSoft.Reports.TheReport = report;

                // Wire up Duration changes for interrogation and handling.
                if (AbsenceSoft.Reports.TheReport.Chart) {
                    // Wire up form submission to our implementation.
                    $('.duration-control').change(AbsenceSoft.Reports.DurationChange);
                    $(':input').not('.duration-control').change(AbsenceSoft.Reports.FilterValuesChange);
                    AbsenceSoft.Reports.ClearChart();
                }

                // Always wire up the form submit button click to our own implementation.
                $('#viewReportForm').submit(AbsenceSoft.Reports.Submit);
            }
        });
    });


})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);