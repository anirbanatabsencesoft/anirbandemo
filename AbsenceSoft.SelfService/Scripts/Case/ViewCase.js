﻿(function (AbsenceSoft, $) {
    AbsenceSoft.ViewCase = AbsenceSoft.ViewCase || {};

    var caseContactInfoUpdated = function () {
        noty({ type: 'success', layout: 'topRight', text: 'Your contact info for this case has been successfully updated' });
    }

    AbsenceSoft.ViewCase.ContactInfoUpdated = caseContactInfoUpdated;


})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);