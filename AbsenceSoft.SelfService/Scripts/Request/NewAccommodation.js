﻿(function (AbsenceSoft, $) {
    var validateFormSection = function (e) {
        var currentStep = $(e.target).parents('.wizard-step');
        var validator = $('#newAccommodationForm').validate();

        var endDateElem = $("input[id*=EndDate]");

        if ($('#selectedDuration').text().toLowerCase().trim() == "temporary") {
            endDateElem.rules("add", {
                required: true,
                messages: {
                    required: "Please select ending date"
                }
            });
        }
        else {
            endDateElem.rules("remove");
        }
        
        var valid = true;
        $(':input', currentStep).each(function (i, v) {
            valid = validator.element(v) && valid;
        });

        return valid;
    };

    var copyValueToOtherElements = function (className) {
        return function () {
            $(className).val($(this).val());
        }
    }

    var bindSummaryElements = function (className) {
        $(className).change(copyValueToOtherElements(className));
    }

    var submitNewAccommodation = function () {       
        if (!$('#newAccommodationForm').valid())
            return false;

        $('#submitAccommodationRequestCase').attr('disabled', 'disabled');

        $.ajax({
            url: '/newaccommodation',
            dataType: 'json',
            method: 'POST',
            data: $('#newAccommodationForm').serialize(),
            success: function (response) {
                if (response.Error) {
                    var html = '<div class="server-error">Unable to create the case.  Please see the errors below.</div>';
                    for (var i = 0; i < response.Messages.length; i++) {
                        html += '<div class="server-error">' + response.Messages[i] + '</div>';
                    }
                    $('#errors').html(html).removeClass('hide');
                    $('#submitAccommodationRequestCase')
                        .attr('disabled', false);
                }
            }
        });
        
        return false;
    }

    var durationSelected = function () {
        var type = $(this).val();
        $('.duration-dates').addClass('hide');
        $('.' + type + '-duration').removeClass('hide');

        $('.start-date').datepicker('update', null);
        $('.input-daterange').datepicker('update', null);
        $('.end-date').datepicker('update', null);

        $(".input-daterange input").each(function () {
            $(this).datepicker("clearDates");
        });
        $(".start-date").each(function () {
            $(this).datepicker("clearDates");
        });
        $(".end-date").each(function () {
            $(this).datepicker("clearDates");
        });

        $('#selectedDuration').text(type);
    }

    var accommodationTypeSelected = function () {
        $('#selectedAccommodation').text($(this).text().trim());  
        var accomodName = $(this).text().trim();
        // Accommodation Type is required, but if it failed validation, the error doesn't automatically go away when selected
        $('[data-valmsg-for="AccommodationTypeId"]').removeClass('field-validation-error').addClass('field-validation-valid').html('');

        $.ajax({
            url: '/newaccommodationdata',
            dataType: 'json',
            method: 'GET',
            data: {'accomodationName': accomodName},
            success: function (response) {
                $('.accom').addClass('hide');
                $('.accom').removeClass('active');
                if (!response.Error && response != "Both") {
                    $('#accom' + response).removeClass('hide');
                }
                else
                {
                    $('.accom').removeClass('hide');                    
                }
            }
        });
    }


    $(document).ready(function () {
        $('#newAccommodationForm').wizard({
            onBeforeShowNext: validateFormSection
        });

        
        $('#newAccommodationForm').submit(submitNewAccommodation);
        $('.accommodation-type').click(accommodationTypeSelected);
        $('.duration-input').change(durationSelected)
        bindSummaryElements('.first-name');
        bindSummaryElements('.last-name');
        bindSummaryElements('.phone');
        bindSummaryElements('.email');
        bindSummaryElements('.description');
        bindSummaryElements('.start-date');
        bindSummaryElements('.end-date');
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);
