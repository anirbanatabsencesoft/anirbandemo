﻿(function (AbsenceSoft, $) {
    $(document).ready(function () {
        var nextButtonClickedInContactScreen = false;
        var validateFormSection = function (e) {

            var currentStep = $(e.target).parents('.wizard-step');
            var validator = $('#newCaseForm').validate();
            var endDateElem = $("input[id*=EndDate]"); {
                endDateElem.rules("add", {
                    required: true,
                    messages: {
                        required: "Please select ending date"
                    }
                });
            }
            var valid = true;

            $(':input', currentStep).each(function (i, v) {
                valid = validator.element(v) && valid;
            });

            var currentStep = currentStep.data('step');

            switch (currentStep)
            {
                case 2: /// Step 2 is where they need to select the main reason, child reason , and contact information
                    nextButtonClickedInContactScreen = true;
                    valid = validatePregnancyDetails() && valid;
                    //Added for PS-882 Employees unable to submit Bonding case in ESS ( Code & Script Fix )
                    if (valid && $('#contactId').val() != null && $('#contactId').val().length > 0) {
                        valid = validateContact();
                    }

                    break;
                case 3: /// Step 3 is where they need to select the leave type, titled out of office details
                    valid = validateOutOfOffice() && valid;
                    break;
                default:
            }

            return valid;
        };

        var validateContact = function () {
            var selectedAbsenceReason = $("input[name='AbsenceReasonCode']:checked");
            var requiresContact = selectedAbsenceReason.data('requires-contact');
            var errorElem = $('.customError');
            errorElem.html('');
            if (!requiresContact)
                return true;


            var contactValid = hasSelectedExistingContact() || hasCreatedNewContact();
            if (!contactValid)
                errorElem.html('Contact details missing.  Please select an existing contact or fill out all fields for a new contact.');

            return contactValid;
        }


        var validatePregnancyDetails = function () {
            var selectedAbsenceReason = $("input[name='AbsenceReasonCode']:checked");
            var requiresPregmat = selectedAbsenceReason.data('requires-pregmat');

            if (!requiresPregmat) {
                return true;
            }

            var pregmatValid = hasEnteredPregMatDetails();
            if (!pregmatValid) {
                var errorElem = $('.customError');
                errorElem.html('Maternity details missing.  Please fill out the expected delivery date.');
            }


            return pregmatValid;

        }


        var showBondingDates = function () {
            var isUseBonding = $("input[name=IsBonding][value=true").is(':checked');
            if (isUseBonding)
                $("#bondingDateContainer.collapse").collapse('show');
            else
                $("#bondingDateContainer.in").collapse('hide');
        }

        var hasSelectedExistingContact = function () {
            return $('#contactId').val() != '';
        }

        var hasCreatedNewContact = function () {
            return $('#contactRelationshipCode').val() != ''
                && $('#contactFirstName').val() != ''
                && $('#contactLastName').val() != ''
                && $('#contactDateOfBirth').val() != '';
        }


        var hasEnteredPregMatDetails = function () {
            return $('#expectedDeliveryDate').val() != '';
        }


        var validateOutOfOffice = function () {
            var isSelected = $('#leaveType').find("label").hasClass('active');
            if (isSelected) {
                $('#leaveTypeError').addClass('hide');
            } else {
                $('#leaveTypeError').removeClass('hide');
            }

            return isSelected;
        }

        var getAbsenceReasonCaseTypes = function () {
            var absenceReasons = $("[name='AbsenceReasonCode']:checked");
            var lastIndex = absenceReasons.length - 1;
            var absenceReasonCode = $(absenceReasons[lastIndex]).val();
            if (absenceReasonCode) {
                var url = '/CaseTypeSelected/' + absenceReasonCode;
                $('#leaveType').load(url, bindCaseTypeSelection);
            }
        };

        var hideOtherCollapseElements = function () {

            var target = $(this).data('target');
            if (target == '#contact') {
                $('.child-reason-container.in').collapse('hide');
                $("#pregmatdetails.in").collapse('hide');
            }
            else if (target && target.indexOf('Children') > -1) {
                $('#contact.in').collapse('hide');
                $("#pregmatdetails.in").collapse('hide');
                $('.child-reason-container.in').not(target).collapse('hide');
            } else if (!target) {
                $('#contact.in').collapse('hide');
                $('.child-reason-container.in').collapse('hide');
            }

        };

        var copyValueToOtherElements = function (className) {
            return function () {
                $(className).val($(this).val());
            }
        }

        var bindSummaryElements = function (className) {
            $(className).change(copyValueToOtherElements(className));
        }

        var setSummaryCaseType = function () {
            $('#selectedCaseType').text($(this).text().trim());
            $('.case-type-description.in').collapse('hide');
            $('#leaveTypeError').addClass('hide');
        }

        var bindCaseTypeSelection = function () {
            $('.case-type').unbind().click(setSummaryCaseType);
        }

        var absenceReasonSelected = function (e) {
            var selectedAbsenceReason = $(this);

            if (selectedAbsenceReason.attr('data-isworkrelated')) {
                $('.ess-workrel').removeClass('hide');
            } else {
                $('.ess-workrel').addClass('hide');
            }

            $('#selectedAbsenceReason').text(selectedAbsenceReason.text().trim());
            // Absence Reason is required, but if it failed validation, the error doesn't automatically go away when selected
            $('[data-valmsg-for="AbsenceReasonCode"]').removeClass('field-validation-error').addClass('field-validation-valid').html('');
            $('.customError').html('');
            /// If this absence reason doesn't have a radio button for the absence reason Id, we need to uncheck the one they just clicked away from
            var absenceReasonRadioButton = selectedAbsenceReason.find("[name='AbsenceReasonCode']");
            if (absenceReasonRadioButton.length == 0) {
                // unchecking old radio button
                $("[name='AbsenceReasonCode']:checked").prop('checked', false);
            }

            //4379
            var requiresContact = absenceReasonRadioButton.data('requires-contact');
            if (requiresContact) {
                $("#contact.collapse").collapse('show');
                var absenceReasonCode = selectedAbsenceReason.data('absence-reason-code');
                getEmployeeContacts($('#EmployeeId').val(), absenceReasonCode);
                getEmployeeContactRelationshipCodes(absenceReasonCode);
            }
            else {
                $("#contact.in").collapse('hide');
            }

            //check if pregnancy or maternity option is selected.
            var requirepregmat = absenceReasonRadioButton.data('requires-pregmat');
            if (requirepregmat)
                $("#pregmatdetails.collapse").collapse('show');
            else {
                $("#pregmatdetails.in").collapse('hide');
                $("#expectedDeliveryDate").val("");
                $("#actualDeliveryDate").val("");
                $('input[name=IsBonding][value="false"]').click();
                $('input[name=IsMedicalComplication][value="false"]').click();
                $("#bondingStartOn").val("");
                $("#bondingEndOn").val("");
            }

        }

        var getEmployeeContacts = function (employeeId, absenceReasonCode) {
            $.ajax({
                url: '/Request/GetEmployeeContacts',
                data: {
                    employeeId: employeeId,
                    absenceReasonCode: absenceReasonCode
                },
                success: bindEmployeeContacts,
                error: function () {
                    $("#contactId").empty();
                    $("#contactId").append('<option value=""> Select Existing Contact </option>');
                }
            });
        }

        $("#contactId").change(function () {
            setSelectedContactDetail();
        });

        var setSelectedContactDetail = function () {
            var absenceReasons = $("[name='AbsenceReasonCode']:checked");
            var lastIndex = absenceReasons.length - 1;
            var absenceReasonCode = $(absenceReasons[lastIndex]).val();
            $.ajax({
                url: '/Request/GetEmployeeContacts',
                data: {
                    employeeId: $('#EmployeeId').val(),
                    absenceReasonCode: absenceReasonCode
                },
                success: function (data) {
                    var selectedContactId = $('#contactId').val();
                    $.each(data, function (i) {
                        if (data[i].Id === selectedContactId) {
                            var nowDate = new Date(parseInt(data[i].DateOfBirth.substr(6)));
                            $("#contactFirstName").val(data[i].FirstName);
                            $("#contactLastName").val(data[i].LastName);
                            $("#contactDateOfBirth").val((nowDate.getMonth() + 1 >= 10 ? nowDate.getMonth() + 1 : "0" + (nowDate.getMonth() + 1)) + "/" + (nowDate.getDay() >= 10 ? nowDate.getDay() :"0"+ nowDate.getDay()) + "/" + nowDate.getFullYear());
                        } else if (selectedContactId===""){
                            $("#contactFirstName").val("");
                            $("#contactLastName").val("");
                            $("#contactDateOfBirth").val("");
                        }
                    });
                }
            });
        };

        var bindEmployeeContacts = function (data) {
            $("#contactId").empty();
            var optionHtml = '<option value=""> Select Existing Contact </option>';
            $.each(data, function (i) {
                optionHtml += '<option value="' + data[i].Id + '">' + data[i].RelationshipName + ": " + data[i].FirstName + ' ' + data[i].LastName + '</option>';
            });
            $("#contactId").append(optionHtml);
            $("#contactDataId").val(data);
        }

        var getEmployeeContactRelationshipCodes = function (absenceReasonCode) {
            $.ajax({
                url: '/Request/EmployeeContactTypes',
                data: {
                    absenceReasonCode: absenceReasonCode
                },
                success: bindEmployeeContactRelationshipCode,
                error: function () {
                    $("#contactRelationshipCode").empty();
                    $("#contactRelationshipCode").append('<option value=""> Select Contact Type </option>');
                }
            });
        }

        var bindEmployeeContactRelationshipCode = function (data) {
            $("#contactRelationshipCode").empty();
            var optionHtml = '<option value=""> Select Contact Type </option>';
            $.each(data, function (i) {
                optionHtml += '<option value="' + data[i].Code + '">' + data[i].Name + '</option>';
            });
            $("#contactRelationshipCode").append(optionHtml);
        }
        var setWorkRelated = function () {
            $('.work-related[value=' + $(this).val() + ']').not(':checked').prop('checked', true).parent('label')
                .addClass('active').siblings('.active').removeClass('active');
        };

        var submitNewCase = function () {
            if (!$('#newCaseForm').valid())
                return false;         

            $('#submitRequestCase').attr("disabled", "disabled");

            $.ajax({
                url: '/newcase',
                dataType: 'json',
                method: 'POST',
                data: $('#newCaseForm').serialize(),
                success: function (response) {
                    if (response.Error) {
                        var html = '<div class="server-error">Unable to create the case.  Please see the errors below.</div>';
                        for (var i = 0; i < response.Messages.length; i++) {
                            html += '<div class="server-error">' + response.Messages[i] + '</div>';
                        }
                        $('#errors').html(html).removeClass('hide');
                        $('#submitRequestCase')
                            .attr('disabled', false);
                    }
                }
            });

            return false;
        }

        $('#newCaseForm').wizard({
            onBeforeShowNext: validateFormSection
        });

        $('#absenceReasonSelected').click(getAbsenceReasonCaseTypes);
        $('.parent-absence-reason').click(hideOtherCollapseElements);

        $('.absence-reason').click(absenceReasonSelected);
        $('.work-related').change(setWorkRelated);
        $('#newCaseForm').submit(submitNewCase);
        bindSummaryElements('.first-name');
        bindSummaryElements('.last-name');
        bindSummaryElements('.phone');
        bindSummaryElements('.email');
        bindSummaryElements('.description');
        bindSummaryElements('.start-date');
        bindSummaryElements('.end-date');
        bindCaseTypeSelection();

        $('input[name=IsBonding]').on('change', showBondingDates);
        $('#contact :input').change(function () {
            if (nextButtonClickedInContactScreen)
                validateContact();
        });


        //hide work related div 
        $('#workRelatedDiv').hide();

        //show work related div       
        var workRelatedNoFunction = function () {
            $('#workRelatedDiv').hide();
            $('#reasonforCase').show();
            $('#IsWorkRelatedYes').removeClass("active");
            $('#IsWorkRelatedNo').addClass("active");
        }

        $('#abWorkRelatedSkip').click(workRelatedNoFunction);
        $('#abWorkRelatedNo').click(workRelatedNoFunction);
        $('#IsWorkRelatedNo').click(workRelatedNoFunction);

        //show work related div       
        var workRelatedFunction = function () {
            $('#reasonforCase').hide();
            $('#workRelatedDiv').show();
        }
        $('#abWorkRelated').click(workRelatedFunction);

        $('#facilityDiv').hide();
        var showFacilityFunction = function () {
            $('#facilityDiv').show();
        }
        $('#showFacility').click(showFacilityFunction);      

    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);

