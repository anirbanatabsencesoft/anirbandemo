﻿(function (AbsenceSoft, $) {
    AbsenceSoft.PersonalInfo = AbsenceSoft.PersonalInfo || {};
    var togglePersonalInfo = function () {
        var alternateContactInfo = $('#alternateContactInfo');
        alternateContactInfo.collapse('toggle');
        if (!$(this).is(':checked')) {
            alternateContactInfo.find(':input').val('');
        }
    };

    $(document).ready(function () {
        $('#requireAlternateContactInfo').change(togglePersonalInfo);
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);


