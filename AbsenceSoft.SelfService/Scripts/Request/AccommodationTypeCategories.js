﻿(function (AbsenceSoft, $) {
    AbsenceSoft.AccommodationTypeCategories = AbsenceSoft.AccommodationTypeCategories || {};

    var hideOtherCollapsibleElements = function () {
        $(this).closest('.accommodation-type-container').find('.in').not(this).collapse('hide');
    };

    var bindHideOtherAccommodationTypeCategories = function () {
        $('.accommodation-type-categories').click(hideOtherCollapsibleElements);
    };

    AbsenceSoft.AccommodationTypeCategories.Start = bindHideOtherAccommodationTypeCategories;
    
    $(document).ready(AbsenceSoft.AccommodationTypeCategories.Start)
    
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);


