﻿(function (AbsenceSoft, $) {
    var datepickerBound = false;
    var setDateOff = function (event) {
        $('#dateOff_0').datepicker('update', event.date);
        /// When clicking on an element in the datepicker, the case info permanently becomes shown
        if ($('#dateOff_0').val() !== null || $('#dateOff_0').val() !== undefined || $('#dateOff_0').val() !== "") {

            var caseId = GetQueryStringParams('caseId');
            var reqDate = $('#dateOff_0').val();
            if (caseId !== undefined) {
                getSelectedTimeOffRequest(reqDate, caseId);
            }
        }

        $('.popover').remove();
    };

    var setCalendar = function (event) {
        $('#calendarUsage').datepicker('update', event.date);
    };

    $('#start-time-picker_0').timepicker({
        showMeridian: true,
        defaultTime: 'current',
        showInputs: false
    }).on('changeTime.timepicker', function (ev) {
        $("#StartTimeForLeave_0").val(ev.time.value);
        calculatePendingValue(ev.time, 0);
    });
    $('#start-time-picker_0').val("");

    $('#end-time-picker_0').timepicker({
        showMeridian: true,
        defaultTime: 'current',
        showInputs: false
    }).on('changeTime.timepicker', function (ev) {
        $("#EndTimeForLeave_0").val(ev.time.value);
        calculatePendingValue(ev.time, 0);
    });
    $('#end-time-picker_0').val("");

    var IntermittentValidation = function () {
        var itorCounterId = parseInt($('#ItorIdCounter').val());
        if (itorCounterId > 0) {
            itorCounterId = itorCounterId - 1;
        }
        for (var itrId = 0; itrId <= itorCounterId; itrId++) {
            if ($('#IntermittentType_' + itrId).val() === "") {
                $('.intermittent-type-error_' + itrId).removeClass('hidden');
                return false;
            }

            var hoursCheck = $('#timeOff_' + itrId).val();
            if (hoursCheck === 0 || hoursCheck === "") {
                $('.time-off-error_' + itrId).removeClass('hidden');
                return false;
            }
            var dateCheck = $('#dateOff_' + itrId).val();
            if (dateCheck === 0 || dateCheck === "") {
                $('.date-off-error_' + itrId).removeClass('hidden');
                return false;
            }
            if ($('#start-time-picker_' + itrId).val() !== "" && $('#end-time-picker_' + itrId).val() === "") {
                $('.end-time-error_' + itrId).removeClass('hidden');
                return false;
            }

            $('.start-time-error_' + itrId).addClass("hidden");
            $('.end-time-error_' + itrId).addClass("hidden");
        }
        return true;
    };

    var submitNewTimeOffRequest = function () {
        if (!IntermittentValidation()) {
            return false;
        }
        if ($('#dateOff_0').val() !== null || $('#dateOff_0').val() !== undefined || $('#dateOff_0').val() !== "") {
            var reqDate = $('#dateOff_0').val();
            $('#dateOff_0').datepicker('update', toFakeUTCDay(reqDate));
        }
        $.ajax({
            url: '/TimeOffRequest',
            dataType: 'json',
            method: 'POST',
            data: $('#timeOffRequestForm').serialize(),
            success: function (response) {
                if (response.Error) {
                    var html = '<div class="server-error">Unable to create the time off request.  Please see the errors below.</div>';
                    for (var i = 0; i < response.Messages.length; i++) {
                        html += '<div class="server-error">' + response.Messages[i] + '</div>';
                    }
                    var itorActiveId = parseInt($('#ActiveItorId').val());
                    $('#errors_' + itorActiveId).html(html).removeClass('hide');
                }
            }
        });

        return false;
    };

    var hasCase = function (date) {
        var matchingDay = AbsenceSoft.Calendar.FindMatchingDay(date);
        if (!matchingDay)
            return false;
        return true;
    };

    var getCalendarData = function (event) {
        setCalendar(event);
        AbsenceSoft.Calendar.GetCalendarData(event.date);
    };

    var bindDatePicker = function () {
        if (datepickerBound) {
            $('#dateOff_0').datepicker('update');
        } else {
            $('#dateOff_0').datepicker({
                beforeShowDay: hasCase,
                orientation: 'top auto',
            }).on('changeDate', setCalendar)
            .on('changeMonth', getCalendarData);
            datepickerBound = true;
        }
    };

    function GetQueryStringParams(sParam) {

        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    };

    var getSelectedTimeOffRequest = function (dateoff, caseId) {
        $("#LoadTorPartialPage").empty();
        $('#CaseId').val(caseId);
        $.ajax({
            url: '/GetTimeOffRequest',
            dataType: 'json',
            method: 'GET',
            data: { caseId: caseId, requestDate: dateoff },
            success: function (response) {
                var resp = parseInt(response.length);
                $('#ItorIdCounter').val(resp);
                document.getElementById("IsEditMode").value = "false";
                if (response.length > 0) {
                    document.getElementById("IsEditMode").value = "true";
                }
                if (response.Error) {
                    var html = '<div class="server-error">Unable to create the time off request.  Please see the errors below.</div>';
                    for (var i = 0; i < response.Messages.length; i++) {
                        html += '<div class="server-error">' + response.Messages[i] + '</div>';
                    }
                    $('#errors').html(html).removeClass('hide');
                }
                else {
                    $('#IntermittentType_0').val(response.IntermittentType);
                    $('#start-time-picker_0').val('');
                    $('#end-time-picker_0').val('');
                    $('#timeOff_0').val('');
                    response.forEach(function (response, index) {
                        // Load the partial view    
                        $.ajax({
                            url: '/GetTimeOffRequestDetails?Id=' + index,
                            type: 'GET',
                            error: function (xhr) {
                                alert('Error: ' + xhr.statusText);
                            },
                            success: function (result) {
                                var startTime;
                                if (response.StartTimeForLeave !== null && response.StartTimeForLeave.Hour !== null) {
                                    var _thisStartTime = parseInt(response.StartTimeForLeave.Hour) > 12 ? parseInt(response.StartTimeForLeave.Hour) - 12 : response.StartTimeForLeave.Hour;
                                    var _thisEndtime = parseInt(response.EndTimeForLeave.Hour) > 12 ? parseInt(response.EndTimeForLeave.Hour) - 12 : response.EndTimeForLeave.Hour;

                                    startTime = _thisStartTime + ':' + response.StartTimeForLeave.Minutes + ' ';
                                    if (response.StartTimeForLeave.PM) {
                                        startTime = startTime + 'PM';
                                    }
                                    else {
                                        startTime = startTime + 'AM';
                                    }

                                    // Check if not set 
                                    if (parseInt(response.StartTimeForLeave.Hour) === 0 && parseInt(response.StartTimeForLeave.Minutes) === 0) {
                                        startTime = '';
                                    }
                                }
                                var endTime;
                                if (response.EndTimeForLeave !== null && response.EndTimeForLeave.Hour !== null) {
                                    endTime = _thisEndtime + ':' + response.EndTimeForLeave.Minutes + ' ';
                                    if (response.EndTimeForLeave.PM) {
                                        endTime = endTime + 'PM';
                                    }
                                    else {
                                        endTime = endTime + 'AM';
                                    }

                                    // Check if not set 
                                    if (parseInt(response.EndTimeForLeave.Hour) === 0 && parseInt(response.EndTimeForLeave.Minutes) === 0) {
                                        endTime = '';
                                    }
                                }

                                // Check if not set 
                                if (parseInt(response.StartTimeForLeave.Hour) === 0 && parseInt(response.EndTimeForLeave.Hour) === 0 && parseInt(response.StartTimeForLeave.Minutes) === 0 && parseInt(response.EndTimeForLeave.Minutes) === 0) {
                                    startTime = '';
                                    endTime = '';
                                }                               
                                if (index !== 0) {
                                    $('#LoadTorPartialPage').append(result);

                                    // initialize timepicker
                                    $('#start-time-picker_' + index).timepicker({
                                        showMeridian: true,
                                        defaultTime: 'current',
                                        showInputs: false
                                    }).on('changeTime.timepicker', function (ev) {
                                        calculatePendingValue(ev.time, index);
                                    });

                                    $('#end-time-picker_' + index).timepicker({
                                        showMeridian: true,
                                        defaultTime: 'current',
                                        showInputs: false
                                    }).on('changeTime.timepicker', function (ev) {
                                        calculatePendingValue(ev.time, index);
                                    });

                                    document.getElementById("StartTimeForLeave_" + index).value = startTime;
                                    document.getElementById("EndTimeForLeave_" + index).value = endTime;
                                    $('#dateOff_' + index).val($('#dateOff_0').val());
                                    

                                    $('#timeOff_' + index).keyup(function () {
                                        timeOffValidate(index);
                                    }); 

                                }

                                if (response.IntermittentType !== null) {
                                    // clear form data
                                    clearFormFields(index);
                                    $('#IntermittentType_' + index).val(response.IntermittentType);
                                }

                                $('#start-time-picker_' + index).timepicker('setTime', startTime);
                                $('#end-time-picker_' + index).timepicker('setTime', endTime);

                                document.getElementById("StartTimeForLeave_" + index).value = startTime;
                                document.getElementById("EndTimeForLeave_" + index).value = endTime;
                                document.getElementById("Id_" + index).value = response.Id;

                                var intermittentDate = $('#dateOff_' + index).val();

                                if ($('#dateOff_' + index).val() === "") {
                                    var today = new Date();
                                    intermittentDate = today.getMonth() + 1 + '/' + today.getDate() + '/' + today.getFullYear();
                                }

                                var startTimeOff = intermittentDate + " " + startTime;
                                var endTimeOff = intermittentDate + " " + endTime;

                                var timeStart = new Date(startTimeOff);
                                var timeEnd = new Date(endTimeOff);
                                var diff = (timeEnd - timeStart) / 60000; //dividing by seconds and milliseconds        
                                var minutes = diff % 60;
                                var hours = (diff - minutes) / 60;

                                if (hours !== 0 || minutes !== 0) {
                                    $('#timeOff_' + index).val(hours + "h " + minutes + "m"); // return hours and minutes
                                }
                               
                                else if (response.TimeOff !== null) {
                                    $('#timeOff_' + index).val(response.TimeOff); // return hours and minutes
                                }

                                $('#timeOff_' + index).keyup(function () {
                                    timeOffValidate(index);
                                }); 

                                $('.save-button_' + index).click(function () {
                                    document.getElementById('ActiveItorId').value = index;
                                    $('#timeOffRequestForm').submit();
                                });

                                $('.remove-button_' + index).click(function () {
                                    var caseId = GetQueryStringParams('caseId');
                                    var reqDate = $('#dateOff_0').val();
                                    document.getElementById('ActiveItorId').value = index;
                                    document.getElementById("CaseId").value = caseId;
                                    document.getElementById("IsDelete").value = "false";
                                    document.getElementById("dateOff_0").value = reqDate;

                                    if (caseId !== undefined) {
                                        deleteTimeOffRequest();
                                    }
                                });
                            }
                        });
                    });
                }
            },
            error: function (e) {
                console.log(e);
            }
        });

        return false;
    };

    var checkValue = function (arg) {

        if (arg === "StartHour") {
            if ($('#StartTimeOfEvent_Hour').val() > "12") {
                $('#StartTimeOfEvent_Hour').val("");
            }
        }
        if (arg === "StartMinutes") {
            if ($('#StartTimeOfEvent_Minutes').val() > "60") {
                $('#StartTimeOfEvent_Minutes').val("");
            }
        }
        if (arg === "EndHour") {
            if ($('#EndTimeOfEvent_Hour').val() > "12") {
                $('#EndTimeOfEvent_Hour').val("");
            }
        }
        if (arg === "EndMinutes") {
            if ($('#EndTimeOfEvent_Minutes').val() > "60") {
                $('#EndTimeOfEvent_Minutes').val("");
            }
        }

        calculatePendingValue();
    };

    var calculatePendingValue = function (time, pos) {
        var intermittentDate = $('#dateOff_0').val();
        if ($('#dateOff_0').val() === "") {
            var today = new Date();
            intermittentDate = today.getMonth() + 1 + '/' + today.getDate() + '/' + today.getFullYear();
        }

        var startTime = intermittentDate + " " + $('#start-time-picker_' + pos).val();
        if ($('#end-time-picker_' + pos).val() !== "") {
            var endTime = intermittentDate + " " + $('#end-time-picker_' + pos).val();

            var timeStart = new Date(startTime);
            var timeEnd = new Date(endTime);
            var diff = (timeEnd - timeStart) / 60000; //dividing by seconds and milliseconds        
            var minutes = diff % 60;
            var hours = (diff - minutes) / 60;

            if (hours !== 0 || minutes !== 0) {
                $('#timeOff_' + pos).val(hours + "h " + minutes + "m"); // return hours and minutes
            }

            document.getElementById('EndTimeForLeave_' + pos).value = $('#end-time-picker_' + pos).val();
        }

        document.getElementById('StartTimeForLeave_' + pos).value = $('#start-time-picker_' + pos).val();

    };

    var clearFormFields = function (itaId) {
        $('#IntermittentType_' + itaId).val("");
        $('.start-time-error_' + itaId).hide();
        $('.end-time-error_' + itaId).hide();
        $('#timeOff_' + itaId).val("");
        $('#errors_' + itaId).html('').addClass('hide');
        $('#start-time-picker_' + itaId).val("");
        $('#end-time-picker_' + itaId).val("");
        $('.time-off-error_' + itaId).addClass('hidden');
        $('.date-off-error_' + itaId).addClass('hidden');
    };

    var toFakeUTCDay = function (date) {
        var newDate = new Date(date);
        newDate.setMilliseconds(0);
        newDate.setSeconds(0);
        newDate.setMinutes(0);
        newDate.setHours(0);
        return newDate;
    };

    $(document).ready(function () {
        $('#timeOff').hoursMinutes();
        $('#calendarUsage').on('changeDate', setDateOff)
            .on('dataAvailable', bindDatePicker);
        $('#timeOffRequestForm').submit(submitNewTimeOffRequest);
        $(document).on('click', '#override', function () {
            $('#IsOverride').val('True');
            $('#timeOffRequestForm').submit();
        });
        $(document).on('click', '#not_override', function () {
            $('#IsOverride').val('False');
            $('#timeOffRequestForm').submit();
        });

        $('.dropdown-menu a').click(function () {
            $(this).closest('.dropdown').find('input.eventtime').val($(this).attr('data-value'));
            calculatePendingValue();
            $(this).closest('.dropdown-menu').toggle();
        });

        $('#StartTimeOfEvent_Hour').blur(function () {
            checkValue('StartHour');
        });
        $('#StartTimeOfEvent_Minutes').blur(function () {
            checkValue('StartMinutes');
        });
        $('#EndTimeOfEvent_Hour').blur(function () {
            checkValue('EndHour');
        });
        $('#EndTimeOfEvent_Minutes').blur(function () {
            checkValue('EndMinutes');
        });

        $('.save-button').click(function () {
            $('#timeOffRequestForm').submit();
        });

        $('.delete-button').click(function () {
            var caseId = GetQueryStringParams('caseId');
            var reqDate = $('#dateOff_0').val();
            var reqItor = $('#ItorRowId').val();
            document.getElementById("CaseId").value = caseId;
            document.getElementById("IsDelete").value = "true";
            document.getElementById("dateOff_0").value = reqDate;
            if (caseId !== undefined) {
                deleteTimeOffRequest();
            }
        });

        $('.addNew-button').click(function () {
            var caseId = GetQueryStringParams('caseId');
            var reqDate = $('#dateOff_0').val();
            if (caseId !== undefined) {
                newTimeOffRequest();
            }
        });

        $('.remove-button').click(function () {
            var caseId = GetQueryStringParams('caseId');
            var reqDate = $('#dateOff_0').val();
            document.getElementById('ActiveItorId').value = "0";
            document.getElementById("CaseId").value = caseId;
            document.getElementById("IsDelete").value = "false";
            document.getElementById("dateOff_0").value = reqDate;

            if (caseId !== undefined) {
                deleteTimeOffRequest();
            }
        });

        $('#timeOff_0').keyup(function () {
            var _timeOff = $('#timeOff_0').val();
            if (_timeOff.length === 1) {
                var newTimeOff = _timeOff + 'h 0m';
                $('#timeOff_0').val(newTimeOff);
            }
        }); 

        $('input[type = radio][name=StartTimeOfEventAMPM]').change(function () {

            var isChecked = $('input[type=radio][name=StartTimeOfEventAMPM]').val();

            if (isChecked === "true" || isChecked === "True") {
                $('input[type=radio][name=StartTimeOfEventAMPM]').val(false);
            }
            else {
                $('input[type=radio][name=StartTimeOfEventAMPM]').val(true);
            }
            calculatePendingValue();
        });

        $('input[type=radio][name=EndTimeOfEventAMPM]').change(function () {
            var isChecked = $('input[type=radio][name=EndTimeOfEventAMPM]').val();

            if (isChecked === "true" || isChecked === "True") {
                $('input[type=radio][name=EndTimeOfEventAMPM]').val(false);
            }
            else {
                $('input[type=radio][name=EndTimeOfEventAMPM]').val(true);
            }
            calculatePendingValue();
        });

        $(':input.eventtime').bind('focus blur', function (e) {
            if (e.type == 'blur') {
                $(this).next('.dropdown-menu').toggle('slow');
            }
            else {
                $(this).next('.dropdown-menu').toggle();
            }
        });

        $('.input-group-addon').click(function (e) {
            $(this).closest('.dropdown-menu').toggle();
        });

        initializeValues();

    });

    var deleteTimeOffRequest = function () {
        $.ajax({
            url: '/DeleteTimeOffRequestDetails',
            dataType: 'json',
            method: 'DELETE',
            data: $('#timeOffRequestForm').serialize(),
            success: function (response) {
                if (response.Error) {
                    var html = '<div class="server-error">Unable to delete the time off request.  Please see the errors below.</div>';
                    for (var i = 0; i < response.Messages.length; i++) {
                        html += '<div class="server-error">' + response.Messages[i] + '</div>';
                    }
                    $('#errors').html(html).removeClass('hide');
                }
            }
        });

        return false;
    };

    var newTimeOffRequest = function () {
        // Load the partial view       
        var itor = $('#ItorIdCounter').val();
        var index = 0;
        if (itor === "0") {
            index = parseInt(itor) + 1;
        }
        else {
            index = parseInt(itor);
        }

        $.ajax({
            url: '/GetTimeOffRequestDetails?Id=' + index,
            type: 'GET',
            error: function (xhr) {
                alert('Error: ' + xhr.statusText);
            },
            success: function (result) {
                $('#LoadTorPartialPage').append(result);
                var itorc = parseInt(index) + 1;
                clearFormFields(index);
                document.getElementById('ItorIdCounter').value = itorc;               
                // initialize timepicker
                $('#start-time-picker_' + index).timepicker({
                    showMeridian: true,
                    defaultTime: 'current',
                    showInputs: false
                }).on('changeTime.timepicker', function (ev) {
                    calculatePendingValue(ev.time, index);
                });

                $('#end-time-picker_' + index).timepicker({
                    showMeridian: true,
                    defaultTime: 'current',
                    showInputs: false
                }).on('changeTime.timepicker', function (ev) {
                    calculatePendingValue(ev.time, index);
                });

                $('#start-time-picker_' + index).val('');
                $('#end-time-picker_' + index).val('');
                // Use javascript for seeting hidden field value
                document.getElementById("StartTimeForLeave_" + index).value = "";
                document.getElementById("EndTimeForLeave_" + index).value = "";

                $('#dateOff_' + index).val($('#dateOff_0').val());
                $('#dateOff_' + index).datepicker();

                $('.save-button_' + index).click(function () {
                    document.getElementById('ActiveItorId').value = index;
                    $('#timeOffRequestForm').submit();
                });

                $('.remove-button_' + index).click(function () {
                    var caseId = GetQueryStringParams('caseId');
                    var reqDate = $('#dateOff_0').val();
                    document.getElementById('ActiveItorId').value = index;
                    document.getElementById("CaseId").value = caseId;
                    document.getElementById("IsDelete").value = "false";
                    document.getElementById("dateOff_0").value = reqDate;

                    if (caseId !== undefined) {
                        deleteTimeOffRequest();
                    }
                });
                initializeValues();

                $('#timeOff_' + index).keyup(function () {
                    timeOffValidate(index);
                }); 
            }
        });

        return false;
    };

    var initializeValues = function () {
        var itorCounterId = $('#ItorIdCounter').val();
        if (itorCounterId > 0) {
            itorCounterId = parseInt(itorCounterId) - 1;
        }
        $('#IntermittentType_' + itorCounterId).on('change', function () {
            if ($('#IntermittentType_' + itorCounterId).val() !== "") {
                $('.intermittent-type-error_' + itorCounterId).addClass('hidden');
                return false;
            }
        });
    };

    var timeOffValidate = function (index) {
        var _timeOff = $('#timeOff_' + index).val();
        if (_timeOff.length === 1) {
            var newTimeOff = _timeOff + 'h 0m';
            $('#timeOff_' + index).val(newTimeOff);
        }
    };


})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);
