﻿(function (AbsenceSoft, $) {
    $.validator.setDefaults({
        ignore: ''
    });

    AbsenceSoft.PageStart = function (element) {
        var datePickerConfiguration = {
            orientation: "top auto",
            autoclose: true
        };

        $('.input-group.date.editor', element).datepicker(datePickerConfiguration);
        // Not all datepickers are in an input-group
        $('.date-editor', element).datepicker(datePickerConfiguration);
        $('[data-toggle="tooltip"]', element).tooltip();
        if (AbsenceSoft.HoursMinutes !== undefined) {
            AbsenceSoft.HoursMinutes.Init(element);
        }
        if (AbsenceSoft.ScheduleEditor !== undefined) {
            AbsenceSoft.ScheduleEditor.Init(element);
        }
        $('[data-toggle="tooltip"]', element).tooltip();
    }

    $(document).ajaxError(function (e, jqXHR, ajaxSettings, thrownError) {
        if(thrownError !== 'abort')
            noty({ type: 'error', text: thrownError, timeout:10000 });
    });

    $(document).ajaxSuccess(function (event, request, settings, data) {
        if (settings.dataType == "json" && data.Redirect && data.RedirectUrl) {
            window.location.href = data.RedirectUrl;
        }
    });

    $(document).ready(function () {
        $('.input-daterange').datepicker({
            orientation: 'top auto',
            autoclose: false
        });

        $('.datepicker').datepicker({
            orientation: 'top auto',
            autoclose: true
        });
        AbsenceSoft.PageStart(document);
    });

    $.noty.defaults.timeout = 5 * 1000; // Show notifications for 5 seconds

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);
