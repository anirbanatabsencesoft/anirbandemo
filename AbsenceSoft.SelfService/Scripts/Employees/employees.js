﻿(function (AbsenceSoft, $) {

    var getParameters = function () {
        return {
            pageNumber: $('#page').val(),
            empId: $('#empID').val(),
        };
    }

    var loadCases = function (paramsData, reset) {

        $.ajax({
            url: '/GetPagedCaseData',
            data: paramsData,
            success: function (data) {

                if (reset) {
                    $('#caseList').html(data);
                } else {
                    $('#caseList').append(data);
                }

                if (data == '') {
                    $('#loadMoreEmpCases').addClass('hide');
                    $('#noCases').removeClass('hide');
                }
                AbsenceSoft.DeleteAttachment.InitializeDeleteEvent();
            }
        });
    };

    var loadNextCases = function (e) {

        var currentPage = parseInt($('#page').val(), 10);
        $('#page').val(currentPage += 1);

        loadCases(getParameters(), false);

    }

    var toggleSchedule = function (e) {
        $('#divSchedule').toggle();
        scheduleEditCancel();
        if ($('#divSchedule').is(':visible'))
        {
            $('#Schedule').text('HIDE SCHEDULE');
        }
        else
        {
            $('#Schedule').text('VIEW SCHEDULE');
        }
    }

    var displayScheduleEdit = function (e) {
        $('#divScheduleView').hide();
        $('#divScheduleEdit').show();
    }

    var scheduleEditCancel = function (e) {
        $('#divScheduleView').show();
        $('#divScheduleEdit').hide();
    }

    var hideSchedule = function (e) {
        $('#divSchedule').hide();
    }

    function AfterScheduleEditorRefreshed() {
        $('#scheduleEditor div div div').first().attr("class", "col-sm-2 col-wrap");
        $('#scheduleEditor div div div').eq(1).attr("class", "col-sm-4 col-wrap");
        $('#divScheduleEdit').find('.fte-schedule-type').first().closest(".col-sm-4").siblings('.col-sm-3').attr("class", "col-sm-2 col-wrap");
        $('#Schedule_FteTimePercentage').blur(function () {
            HourPercentageCalculation();
        });
        if ($('#Schedule_FteTimePercentage').length > 0 && $('#Schedule_FteTimePercentage').val() > 0) {
            if ($('#fteHours').val() === null || $('#fteHours').val() === '' || $('#fteHours').val().length <= 0) {
                HourPercentageCalculation();
            }
        }
    }

    function HourPercentageCalculation() {
        var totalHours = $('#Schedule_FTWeeklyWorkTime').val();
        totalHours = Number(totalHours.substring(0, totalHours.indexOf('h')));
        var percentage = $('#Schedule_FteTimePercentage').val();
        $('#fteHours').val((totalHours * (percentage / 100.0)).toFixed(2));
        $('#fteHours').blur();
    }

    $(document).ready(function () {
        AfterScheduleEditorRefreshed();
        $(document).on("ScheduleEditorRefreshed", null, AfterScheduleEditorRefreshed);
        $('#loadMoreEmpCases').click(loadNextCases);
        hideSchedule();
        $('#Schedule').click(toggleSchedule);
        $('#ScheduleEdit').click(displayScheduleEdit);
        $('[data-toggle="tooltip"]').tooltip();
    });

    var success = function (data) {
        AbsenceSoft.PageStart('#EditEmployee');
    }

    var complete = function (data) {
        AbsenceSoft.PageStart('#EditEmployee');
    }

    AbsenceSoft.Employee = AbsenceSoft.Employee || {};
    AbsenceSoft.Employee.Success = success;
    AbsenceSoft.Employee.Complete = complete;

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);