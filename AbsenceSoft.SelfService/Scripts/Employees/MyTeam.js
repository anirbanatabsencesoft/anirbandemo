﻿(function (AbsenceSoft, $) {
    var loadEmployees = function (e) {
        $.ajax({
            url: '/myteam',
            data: {
                page: $('#page').val()
            },
            success: function (data) {
                $('#employeeList').append(data);

                if (data == '') {
                    $('#loadMoreEmployees').addClass('hide');
                    $('#noEmployees').removeClass('hide');
                }
            }
        });
    };

    var loadNextEmployees = function (e) {
        var currentPage = parseInt($('#page').val(), 10);
        $('#page').val(currentPage += 1);
        loadEmployees(e);
    }


    var loadCases = function (e, reset) {
        $.ajax({
            url: '/MyTeamCases',
            data: {
                page: $('#page').val()
            },
            success: function (data) {
                if (reset) {
                    $('#caseList').html(data);
                } else {
                    $('#caseList').append(data);
                }
                if (data == '') {
                    $('#loadMoreMyTeamCases').addClass('hide');
                    $('#noCases').removeClass('hide');
                }
                else {
                    $('#loadMoreMyTeamCases').removeClass('hide');
                    $('#noCases').addClass('hide');
                }

                AbsenceSoft.UploadAttachment.BindModal();
                AbsenceSoft.DeleteAttachment.InitializeDeleteEvent();
            }
        });
    };

    
    var loadNextCases = function (e) {
        var currentPage = parseInt($('#page').val(), 10);
        $('#page').val(currentPage += 1);
        loadCases(e, false);
    }



    $(document).ready(function () {
        $('#loadMoreEmployees').click(loadNextEmployees);
        $('#loadMoreMyTeamCases').click(loadNextCases);
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);

