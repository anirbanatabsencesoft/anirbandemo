﻿(function (AbsenceSoft, $) {
    var earliestDateRetrieved,
        latestDateRetrieved,
        calendarBound = false;

    AbsenceSoft.Calendar = {
        Info: []
    };

    AbsenceSoft.Calendar.FindMatchingDay = function (date) {
        var newDate = new Date(date);
        for (var i = 0; i < AbsenceSoft.Calendar.Info.length; i++) {
            var calendarData = AbsenceSoft.Calendar.Info[i];
            var dateData = parseInt(calendarData.Date.replace("/Date(", "").replace(")/", ""), 10);
            var theDate = new Date(dateData);
            if (theDate.getUTCDate() == newDate.getDate()
                && theDate.getUTCMonth() == newDate.getMonth()
                && theDate.getUTCFullYear() == newDate.getFullYear()) {

                return calendarData;
            }
        }
        return null;
    }

    var dayClass = function (date) {
        return 'day-' + date.getDate() + '-' + date.getMonth() + '-' + date.getFullYear() + ' ';
    }

    var generatePopoverContent = function (matchingDay) {
        if (matchingDay.IsHoliday)
            return matchingDay.HolidayName;

        var policyContent = '';
        for (var i = 0; i < matchingDay.Policies.length; i++) {
            var currentPolicy = matchingDay.Policies[i];
            var policyDescription =
                'Policy: ' + currentPolicy.PolicyName + '<br />' +
                'Status: ' + currentPolicy.DeterminationString + '<br />' +
                'Absent Hours: ' + currentPolicy.AbsentHours + '<br />';

            if (i != matchingDay.Policies.length - 1)
                policyDescription += '<br />';

            policyContent += policyDescription;
        }

        return policyContent;
    }

    var addPolicyInformation = function (matchingDay, date) {
        return function () {
            var popoverTitle = '';
            if (matchingDay.IsHoliday) {
                popoverTitle = 'Holiday';
            } else {
                popoverTitle = 'Case #' + matchingDay.CaseNumber + '<br /> ' + matchingDay.Reason;
            }

            var popoverContent = generatePopoverContent(matchingDay);
            var elementClass = dayClass(date);
            var popoverElement =
            $('td.' + elementClass)
                .data('toggle', 'popover')
                .data('trigger', 'hover')
                .data('title', popoverTitle)
                .data('content', popoverContent)
                .data('container', 'body')
                .data('placement', 'bottom')
                .popover({
                    html: true
                });
        };
    }

    var showClass = function (date) {
        var matchingDay = AbsenceSoft.Calendar.FindMatchingDay(date);
        if (!matchingDay)
            return false;

        var classes = dayClass(date);
        for (var i = 0; i < matchingDay.Policies.length; i++) {
            var determinationClass = matchingDay.Policies[i].DeterminationString.toLowerCase() + ' ';
            classes += determinationClass;
        }

        if (matchingDay.IsHoliday)
            classes += 'holiday';

        setTimeout(addPolicyInformation(matchingDay, date), 300);
        return classes;
    };

    var getFirstAndLastDateOfMonth = function (date) {
        var y = date.getFullYear(),
            m = date.getMonth();

        return {
            firstDate: new Date(y, m, 1),
            lastDate: new Date(y, m + 1, 0)
        }
    }

    ///Has side effect of updating the dates that we use to keep track of whether we've retrieved this calendar month or not
    var needToGetDataForCalendar = function (datesOfMonth) {
        var needToGetData = !earliestDateRetrieved
            || !latestDateRetrieved
            || earliestDateRetrieved > datesOfMonth.firstDate
            || latestDateRetrieved < datesOfMonth.lastDate;

        if (!earliestDateRetrieved || earliestDateRetrieved > datesOfMonth.firstDate)
            earliestDateRetrieved = datesOfMonth.firstDate;

        if (!latestDateRetrieved || latestDateRetrieved < datesOfMonth.lastDate)
            latestDateRetrieved = datesOfMonth.lastDate;

        return needToGetData;
    }

    var getCalendarData = function (date) {
        var datesOfMonth = getFirstAndLastDateOfMonth(date);
        if (needToGetDataForCalendar(datesOfMonth)) {
            $.ajax({
                url: '/CalendarInfo/' + $('#employeeId').val(),
                data: {
                    startDate: formatDate(datesOfMonth.firstDate),
                    endDate: formatDate(datesOfMonth.lastDate)
                },
                success: addDataToCalendar
            });
        }
    }

    AbsenceSoft.Calendar.GetCalendarData = getCalendarData;

    var formatDate = function (date) {
        return (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
    }

    var addDataToCalendar = function (data) {
        for (var i = 0; i < data.length; i++) {
            AbsenceSoft.Calendar.Info.push(data[i]);
        }

        if (!calendarBound) {
            bindCalendar();
        } else {
            $('#calendarUsage').trigger('dataAvailable').datepicker('update');
        }
    }

    var getMonthData = function (e) {
        getCalendarData(e.date);
    }

    var bindCalendar = function () {
        $('#calendarUsage').datepicker({
            beforeShowDay: showClass,
        }).on('changeMonth', getMonthData)
        .trigger('dataAvailable');
        calendarBound = true;
    }

    $(document).ready(function () {
        if ($('#calendarUsage').length == 0)
            return;

        getCalendarData(new Date());
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);