﻿(function (AbsenceSoft, $) {
    var populateContactModal = function () {
        var contactContainer = $(this).parents('.contact');
        var id = contactContainer.find('.contactId').val();
        var firstName = contactContainer.find('.contactFirstName').text();
        var lastName = contactContainer.find('.contactLastName').text();
        var dateOfBirth = contactContainer.find('.contactDateOfBirth').val();
        var relationshipCode = contactContainer.find('.contactRelationshipCode').val();
        $('#contactId').val(id);
        $('#contactFirstName').val(firstName);
        $('#contactLastName').val(lastName);
        $('#contactDateOfBirth').val(dateOfBirth);
        $('#contactRelationship').val(relationshipCode);
        $('#contactModal .modal-title').text('Edit Existing Contact');
    }

    var resetContactModal = function () {
        $('#contactId').val('');
        $('#contactFirstName').val('');
        $('#contactLastName').val('');
        $('#contactDateOfBirth').val('');
        $('#contactRelationship').val('');
        $(this).find('.modal-title').text('Add New Contact');
    }

    var populateDeleteModal = function () {
        var contactContainer = $(this).parents('.contact');
        var id = contactContainer.find('.contactId').val();
        var firstName = contactContainer.find('.contactFirstName').text();
        var lastName = contactContainer.find('.contactLastName').text();
        var relationship = contactContainer.find('.contactRelationship').text();
        var contactInfo = firstName + ' ' + lastName + ' (' + relationship + ')';

        $('#deleteContactId').val(id);
        $('#contactInfo').text(contactInfo);
    }

    var resetDeleteModal = function () {
        $('#deleteContactId').val('');
        $('#contactInfo').text('');
    }

    var deleteContact = function () {
        var contactInfo = {
            Id: $('#deleteContactId').val()
        }

        $.ajax({
            url: '/user/deletecontact',
            data: contactInfo,
            dataType: 'json',
            method: 'POST'
        });
    }

    AbsenceSoft.ajaxSuccess = function (data) {
        if (!data.Error)
        {
            window.location.href = data.RedirectUrl;
        }
    }

    $(document).ready(function(){
        $('.edit-contact').click(populateContactModal);
        $('#contactModal').on('hidden.bs.modal', resetContactModal);
        $('.delete-contact').click(populateDeleteModal);
        $('#deleteContactModal').on('hidden.bs.modal', resetDeleteModal);
        $('#deleteContact').click(deleteContact);
    })
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);