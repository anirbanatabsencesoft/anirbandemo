﻿(function (AbsenceSoft, $) {
    var success = function (html) {
        $('#scheduleEditor').parent().html(html);
        $(document).trigger("ScheduleEditorRefreshed", null);
        AbsenceSoft.PageStart('#scheduleEditor');
    }

    var findArrayIndex = function (array, predicateFunction) {
        var length = array == null ? 0 : array.length;
        if (!length) {
            return -1;
        }
        var index = -1;
        for (var i = 0; i < array.length; ++i) {
            if (predicateFunction(array[i])) {
                index = i;
                break;
            }
        }
        return index;
    }

    var bindScheduleEditorEvents = function (element) {
        if (element !== undefined) {
            $('.schedule-type', element).change(getEmployeeScheduleEditor);
            if ($('.fte-schedule-type', element).length > 0) {
                $('.fte-schedule-type', element).change(getEmployeeScheduleEditor);
            }
            $('.days-in-rotation', element).blur(getEmployeeScheduleEditor);
            $('.month-link', element).click(setVariableTypeActualScheduleIntoHiddenField);
            $('.month-link', element).click(setVariableTypeWeeklyScheduleIntoHiddenField);
            $('.month-link', element).click(getEmployeeScheduleEditor);
        }
        $('#ScheduleEditCancel').click(scheduleEditCancel);
        $('#ScheduleEditSave').click(scheduleEditCancel);
    }

    var scheduleEditCancel = function (e) {
        $('#divScheduleView').show();
        $('#divScheduleEdit').hide();
        $('#fteHoursView').blur();
        //$('#divScheduleView').find('.fte-schedule-type').first().closest(".col-sm-3").siblings('.col-sm-4').attr("class", "col-sm-3 col-wrap");
        //$('#divScheduleView').find('.fte-schedule-type').first().closest(".col-sm-3").attr("class", "col-sm-4 col-wrap");
    }

    var getEmployeeScheduleEditor = function () {
        var variableTimeMonth = $(this).find(':hidden').val();
        $.ajax({
            type: 'POST',
            url: '/Employees/Schedule',
            data: {
                ScheduleType: $($('.schedule-type:checked')[0]).val(),
                StartDate: $('.schedule-start-date').val(),
                DaysInRotation: $('.days-in-rotation').val(),
                EmployeeId: $('#hiddenEmployeeId').val(),
                VariableTimeMonth: variableTimeMonth,
                TemporaryActualScheduleValuesInString: $('#hfVariableTypeActualSchedule').val(),
                TemporaryActualScheduleWorkingHoursInString: $('#hfVariableTypeWeeklySchedule').val(),
                FteWeeklyDuration: this.defaultValue, //$($('.fte-schedule-type:checked')[0]).val(),
                FteAvgTimePerWeek: $('.fte-avg-hours').val(),
                FteTimePercentage: $('.fte-percentage').val()
            },
            success: success
        })
    }

    var variableScheduleActualTimes = [];
    var variableScheduleWeeklyHours = [];
    var setVariableTypeActualScheduleIntoHiddenField = function () {
        $("input[type='text'][name^='Schedule.ActualTimes']").each(function () {
            var actualTime = $(this);
            var actualTimeValue = $(this).val();
            if (actualTimeValue != '' && actualTimeValue != null) {
                var timeViewModel = {};
                timeViewModel.SampleDate = actualTime.next().val();
                timeViewModel.TotalMinutes = actualTimeValue;
                if (variableScheduleActualTimes != null && variableScheduleActualTimes.length > 0) {

                    var previouslySavedData = variableScheduleActualTimes.filter(function (e) {
                        return e.SampleDate == timeViewModel.SampleDate;
                    });

                    if (previouslySavedData != null && previouslySavedData.length > 0) {
                            const index = findArrayIndex(variableScheduleActualTimes, function (o) { return o.SampleDate == previouslySavedData[0].SampleDate; });
                            variableScheduleActualTimes.splice(index, 1);
                            variableScheduleActualTimes.push(timeViewModel);

                    } else {
                        variableScheduleActualTimes.push(timeViewModel);
                    }
               
                    } else {
                    variableScheduleActualTimes.push(timeViewModel);
                }
            }
        });

        $('#hfVariableTypeActualSchedule').val(JSON.stringify(variableScheduleActualTimes));

    }

    var setVariableTypeWeeklyScheduleIntoHiddenField = function () {
        const zeroHour = '0h';
        $(".variable-weekly-hours").each(function () {            
            var actualTime = $(this);
            var actualTimeValue = $(this).val();
            if (actualTimeValue != '' && actualTimeValue != null) {
                var timeViewModel = {};
                timeViewModel.SampleDate = actualTime.next().val();
                timeViewModel.TotalMinutes = actualTimeValue;
                if (actualTimeValue != zeroHour) {
                    if (variableScheduleWeeklyHours != null && variableScheduleWeeklyHours.length > 0) {

                        var previouslySavedData = variableScheduleWeeklyHours.filter(function (e) {
                            return e.SampleDate == timeViewModel.SampleDate;
                        });

                        if (previouslySavedData != null && previouslySavedData.length > 0) {
                            const index = findArrayIndex(variableScheduleWeeklyHours, function (o) { return o.SampleDate == previouslySavedData[0].SampleDate; });
                            variableScheduleWeeklyHours.splice(index, 1);
                            variableScheduleWeeklyHours.push(timeViewModel);


                        } else {
                            variableScheduleWeeklyHours.push(timeViewModel);
                        }

                    } else {
                        variableScheduleWeeklyHours.push(timeViewModel);
                    }
                }
            }
        });
        $('#hfVariableTypeWeeklySchedule').val(JSON.stringify(variableScheduleWeeklyHours));
    }

    AbsenceSoft.ScheduleEditor = AbsenceSoft.ScheduleEditor || {};
    AbsenceSoft.ScheduleEditor.Init = bindScheduleEditorEvents;

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);