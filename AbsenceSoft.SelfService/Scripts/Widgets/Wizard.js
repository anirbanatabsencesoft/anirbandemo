﻿(function (AbsenceSoft, $) {
    var showPreviousStep = function (options) {
        return function (e) {
            if (!options.onBeforeShowPrevious(e))
                return;

            var parentElement = $(this).parents('.wizard-step');
            var currentStep = parentElement.data('step');
            parentElement.addClass('hide');
            showSpecificWizardStep(currentStep -= 1, parentElement.parents('.wizard'));
        }

    }

    var showNextStep = function (options) {
        return function (e) {
            if (!options.onBeforeShowNext(e))
                return false;

            var parentElement = $(this).parents('.wizard-step');
            var currentStep = parentElement.data('step');
            parentElement.addClass('hide');
            showSpecificWizardStep(currentStep += 1, parentElement.parents('.wizard'));
        }
    }


    //need to show a specific step based on attribute
    var showSpecific = function (options) {
        return function (e) {
            if (!options.onBeforeShowPrevious(e))
                return;

            //get the step to go
            var step = $(this).attr('step');

            var parentElement = $(this).parents('.wizard-step');
            var currentStep = parentElement.data('step');
            parentElement.addClass('hide');
            showSpecificWizardStep(step, parentElement.parents('.wizard'));
        }
    }


    var showSpecificWizardStep = function (stepToShow, context) {
        $('.wizard-step[data-step="' + stepToShow + '"]', context).removeClass('hide');
    }

    var bindWizardEvents = function (options, context) {
        $('.previous', context).click(showPreviousStep(options))
        $('.next', context).click(showNextStep(options));

        //some extention for enabling progress toolbar events
        $('.previouslinks', context).click(showSpecific(options));

        $('.wizard-step', context).addClass('hide');
        showSpecificWizardStep(1, context);
        return this;
    }

    $.fn.wizard = function (options) {
        var settings = $.extend({
            onBeforeShowPrevious: function () {
                return true;
            },
            onBeforeShowNext: function () {
                return true;
            }
        }, options);

        bindWizardEvents(settings, this);
        return this;
    }

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);
