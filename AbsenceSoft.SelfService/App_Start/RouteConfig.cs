﻿using System.Web.Mvc;
using System.Web.Routing;

namespace AbsenceSoft.SelfService
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.LowercaseUrls = true;

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}", 
                new { controller = "Home", action = "Index", id = UrlParameter.Optional },
               new string[] { "AbsenceSoft.SelfService.Controllers" }
            );
        }
    }
}
