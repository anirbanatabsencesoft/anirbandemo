﻿using System.Web;
using System.Web.Optimization;

namespace AbsenceSoft.SelfService
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-ui-{version}.js",
                        "~/Scripts/jquery.unobtrusive-ajax.js",
                        "~/Scripts/select2.js",
                        "~/Scripts/jQuery.FileUpload/jquery.fileupload.js",
                        "~/Scripts/jquery.fileDownload.js",
                        "~/Scripts/jQuery.FileUpload/jquery.iframe-transport.js")
                        .IncludeDirectory("~/Scripts/noty", "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/bootstrap-datepicker.js",
                      "~/Scripts/bootstrap-timepicker.min.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/site")
                .IncludeDirectory("~/Scripts/Widgets", "*.js", false)
                .IncludeDirectory("~/Scripts/Shared", "*.js", false)
                .Include("~/Scripts/Home/Index.js")
                .Include("~/Scripts/Home/Toolbar.js")
                .IncludeDirectory("~/Scripts/User", "*.js", false)
                .IncludeDirectory("~/Scripts/Employees", "*.js", false)
                .IncludeDirectory("~/Scripts/Case", "*.js", false)
                .IncludeDirectory("~/Scripts/Request", "*.js", false)
                .IncludeDirectory("~/Scripts/Reports", "*.js", false)
                .IncludeDirectory("~/Scripts/Global", "*.js", false));

            bundles.Add(new ScriptBundle("~/bundles/unsecure")
                .Include("~/Scripts/app.js")
                .Include("~/Scripts/Home/RegisterAccountSubmit.js"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap-datepicker.css",
                      "~/Content/bootstrap-timepicker.min.css",
                      "~/Content/css/select2.css",
                      "~/css/font-awesome.css",
                      "~/Content/site.css"));
        }
    }
}
