﻿using AbsenceSoft.SelfService.Controllers;
using AbsenceSoft.SelfService.Models;
using AbsenceSoft.SelfService.Models.Home;
using AbsenceSoft.SelfService.Models.Reports;
using AbsenceSoft.Web;
using AbsenceSoft.Web.Attributes;
using AT.Common.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AbsenceSoft.SelfService.Areas.Reporting.Controllers
{
    public class HomeController : Controller
    {
        // GET: Reporting/Home
        [Secure("MyEmployeesDashboard")]
        [HttpGet, Title("Reports")]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Gets the toolbar data
        /// </summary>
        /// <returns></returns>
        [ChildActionOnly]
        public ActionResult GetToolbar()
        {
            UserViewModel user = new UserViewModel(Current.User());
            return PartialView("_Toolbar", user);
        }

        /// <summary>
        /// Returns the Logo for Employer
        /// </summary>
        /// <returns></returns>
        [ChildActionOnly]
        public PartialViewResult GetLogo()
        {
            LogoViewModel logo = new LogoViewModel();
            logo.PopulateLogoInformation(Current.Employer(), Current.Customer());
            return PartialView("_Logo", logo);
        }

        /// <summary>
        /// Get the Report List for given Category
        /// </summary>
        /// <param name="categoryName"></param>
        /// <returns></returns>
        private async Task<List<ReportListModel>> GetReportsForCategory(string categoryName)
        {
            ApiExecutionController ctrl = new ApiExecutionController();
            var stringData = await ctrl.CallService(new ApiExecutionController.ExecuteArgs() { Component = "reporting", EndPoint = "reports/ess/type/" + categoryName, Method = "GET", PostedData = "" });
            var jset = new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.All };
            var results = JsonConvert.DeserializeObject<ResultSet<List<AT.Logic.Reporting.Reports.BaseReport>>>(Convert.ToString(stringData), jset);
            return results;
        }
    }
}