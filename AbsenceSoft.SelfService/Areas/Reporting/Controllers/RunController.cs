﻿using AbsenceSoft.SelfService.Areas.Reporting.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.SelfService.Areas.Reporting.Controllers
{
    public class RunController : Controller
    {
        // GET: Reporting/Run
        public ActionResult Index(string reportId, string requestId, string reportType, string reportName, string title)
        {
            if (string.IsNullOrWhiteSpace(reportId) && string.IsNullOrWhiteSpace(requestId))
            {
                return RedirectToAction("Index", "Home", new { area = "Reporting" });
            }

            ReportViewModel reportViewModel = new ReportViewModel()
            {
                ESSReportName = reportName,
                ReportId = reportId,
                Title = title,
                ReportType = reportType,
                RequestId = requestId
            };
            
            return View(reportViewModel);
        }
    }
}