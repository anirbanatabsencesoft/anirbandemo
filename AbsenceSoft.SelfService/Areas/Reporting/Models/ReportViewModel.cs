﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.SelfService.Areas.Reporting.Models
{
    public class ReportViewModel
    {
        public string ReportId { get; set; }
        public string RequestId { get; set; }
        public string ReportType{ get; set; }
        public string Title { get; set; }
        public string ESSReportName { get; set; }
        public string ReportTitle { get { return string.Format("{0} | {1}", Title, ESSReportName); } }
    }
}
