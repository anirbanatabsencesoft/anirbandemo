﻿(function (ESS, $) {
    'use strict';
    ESS.Session = ESS.Session || {};

    //Add
    ESS.Session.Add = function (userid, requestid, data) {
        var ids = ESS.Session.GetAll(userid);
        ids.push({ id: requestid, data: data });
        localStorage.setItem(userid + "_REPORT_DATA", JSON.stringify(ids));      
    }

    //Get all available ids queued
    ESS.Session.GetAll = function (userid) {
        var sd = localStorage.getItem(userid+"_REPORT_DATA");
        if (sd) {
            try {
                return JSON.parse(sd);
            }
            catch (e) { }
        }
        return [];
    }
    //Get
    ESS.Session.GetById = function (userid, requestid) {
        var ids = ESS.Session.GetAll(userid);
        return _.find(ids, 'id', parseInt(requestid));
    }

    ESS.Session.RemoveList = function (userid, lst) {       
        $.each(lst,function (index,id) {
            ESS.Session.Remove(userid, id);
        });
    }

    //remove
    ESS.Session.Remove = function (userid, id) {
        var ids = ESS.Session.GetAll(userid);
        ids = $.grep(ids, function (e, i) {
            return e.id != id;
        });
        localStorage.setItem(userid + "_REPORT_DATA", JSON.stringify(ids));
    }

    //clear
    ESS.Session.Clear = function () {
        localStorage.clear();
    }

    // set criteria data for report for particular report
    ESS.Session.AddCriteria = function (reportid, criteriaData) {
        sessionStorage.setItem(reportid + "_REPORT_CRITERA", JSON.stringify({ criteriaData: criteriaData, expiry: moment() }));
    }

    // get criteria data for report for particular report
    ESS.Session.GetCriteria = function (reportid) {
        var sd = sessionStorage.getItem(reportid + "_REPORT_CRITERA");
        if (sd) {
            try {
                return JSON.parse(sd);
            }
            catch (e) { }
        }
        return null;       
    }


    // set criteria data for report for particular report
    ESS.Session.SetReportData = function (reportid, criteriaData) {
        sessionStorage.setItem(reportid + "_REPORT_DATA", JSON.stringify({ reportData: criteriaData, expiry: moment() }));
    }

    // get criteria data for report for particular report
    ESS.Session.GetReportData = function (reportid) {
        var sd = sessionStorage.getItem(reportid + "_REPORT_DATA");
        if (sd) {
            try {
                return JSON.parse(sd);
            }
            catch (e) { }
        }
        return null;
    }


    $(document).ready(function () {
        if ($(location).attr('href').indexOf("/login") > -1) {
            sessionStorage.clear();
        }
    });

})(window.ESS = window.ESS || {}, jQuery);