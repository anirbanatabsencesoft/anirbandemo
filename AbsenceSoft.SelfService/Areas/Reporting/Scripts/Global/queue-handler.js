﻿(function (ESS, $) {
    'use strict';
    ESS.Queue = ESS.Queue || {};

    //local variables
    var availableTasks = [];
    var executedTaskIds = [];
    var currentUserId = "";
    var runningTasks = {};
    var taskStatus = {};
    var taskMessage = {};
    var taskNames = {};
    var queueInterval = null;
    var requestReportCancelEndPoint = 'reports/cancel';  

    //default settings for timer as 2 secs
    var lookbackTime = 2000;

    //start the queue manager
    var startQueue = function () {
        if (!currentUserId) {
            //get the current user id
            $.get("/current/user", function (data) {
                var d = JSON.parse(data);
                currentUserId = d.id;
                showHistory();
            });
        }

        if (currentUserId) {
            availableTasks = ESS.QueueStorage.GetAll(currentUserId);

            if (availableTasks.length > 0) {
                //check whether the tasks are already running or not
                var exeTasks = $.grep(availableTasks, function (e) {
                    if (e.id) {
                        return executedTaskIds.includes(e.id) === false;
                    }
                });

                if (exeTasks) {
                    $(exeTasks).each(function (i, e) {
                        if (e.id) {
                            startTask(e);
                            executedTaskIds.push(e.id);
                        }
                    });
                }
            }
        }
    }

    //start a new task to check the queue process status
    var startTask = function (e) {
        if (e.id) {
            runningTasks[e.id] = setInterval(function () { checkStatus(e) }, lookbackTime);
        }
    }

    //check status
    var checkStatus = function (e) {
        if (e.id) {
            ESS.HttpUtils.Get("reporting", "reports/status/" + e.id, function (data) {
                var d = JSON.parse(data);
                if (d.success === true) {
                    taskStatus[e.id] = d.data.status;
                    taskMessage[e.id] = e.msg;
                    taskNames[e.id] = e.name;
                    setStatus();

                    if (d.data.status == 6303) {
                        removeCurrentTask(e.id);
                        $.noty.closeAll();
                       
                        //notify
                        var checkFormat = ESS.Session.GetById(currentUserId, e.id);
                        if (checkFormat && checkFormat.data.requestedExportFormat < 3) {
                            var reportFormat = checkFormat.data.requestedExportFormat;
                            var aText = '<b>Report generation completed.<b><br/>';
                            var aLink = '<a href="#" onclick="return ESS.Queue.CreateLink(' + e.id + ',' + reportFormat + ')">Download Report</a>';
                            noty({ type: 'success', text: aText + aLink, layout: 'topRight' });
                        }
                        else {
                            noty({ type: 'success', text: '<b>Report generation completed.<b><br/><a href="/reporting/run/?requestid=' + e.id + '&type=result">Show Result</a>', layout: 'topRight' });
                        }
                    }
                    //add to history
                    if (taskStatus[e.id] == 6303 || taskStatus[e.id] == 6305) 
                    ESS.QueueStorage.AddToHistory(currentUserId, e.id, e.name, ESS.Queue.getStatus(taskStatus[e.id]), ESS.Queue.ShowMessage(taskMessage[e.id]));
                }
            });
        }
    }

    //remove the current task and task id
    var removeCurrentTask = function (id) {
        clearInterval(runningTasks[id]);
        delete runningTasks[id];
        ESS.QueueStorage.Remove(currentUserId, id);
    }

    //set the HTML status
    var setStatus = function () {
        var html = '<table class="table table-hover .table-striped .table-bordered">';
        html += '<thead><tr><td>Report</td><td>Current Status</td><td>Query</td><td>Last Checked</td><td>Action</td></tr></thead>';
        html += '<tbody>';
        Object.keys(taskStatus).forEach(function (id) {
            var idhtm = taskNames[id];
            if (taskStatus[id] === 6303) {
                var rData = ESS.Session.GetById(currentUserId, id);
                if (rData && rData.data.requestedExportFormat < 3) {
                    var reportFormat = rData.data.requestedExportFormat;
                    var reportName = reportFormat == 0 ? taskNames[id] + '-PDF' : taskNames[id] + '-CSV';
                    idhtm = '<a href="#" onclick="return ESS.Queue.CreateLink(' + id + ',' + reportFormat + ')">' + reportName + '</a>';
                }
                else {
                    idhtm = '<a href="/reporting/run/?requestid=' + id + '&type=result">' + taskNames[id] + '</a>';
                }
            }
            var currentStatus = ESS.Queue.getStatus(taskStatus[id]);
            var removeCancelLink = ((currentStatus === 'Pending') ? '<br/><a onclick="ESS.Queue.CancelRequest(' + id + ', this)">Cancel Request</a>' : '<a onclick="ESS.Queue.Remove(' + id + ', this)">Remove</a>');
            html += '<tr><td>' + idhtm + '</td><td>' + currentStatus + '</td><td>' + ESS.Queue.ShowMessage(taskMessage[id]) + '</td><td>' + moment().format('MMMM Do YYYY, h:mm:ss a') + '</td><td>' + removeCancelLink+ '</td></tr>';
        });
        html += '</tbody></table>';

        $("#queue-current-status").html(html);        
    }

    //show modal dialog
    ESS.Queue.ShowStatusModal = function () {
        $("#queueStatusModal").modal();
    }

    //show modal dialog
    ESS.Queue.CreateLink = function (requestId, reportFormat) {
        window.open(window.location.origin + '/service/download/?Component=' + ESS.ReportOperations.Module + '&EndPoint=reports/getfilepath/' + requestId + "/" + reportFormat + '&Method=GET', "_self");
    }

    //show message
    ESS.Queue.ShowMessage = function (msg) {
        if (msg) {
            return msg;
        } else {
            return '';
        }
    }

    //remove a queue
    ESS.Queue.Remove = function (id, obj) {
        //stop queue first
        clearInterval(queueInterval);

        //do operations
        removeCurrentTask(id);
        delete taskStatus[id];
        delete taskNames[id];
        delete taskMessage[id];
        $(obj).parent().parent().remove();
        ESS.Session.Remove(currentUserId, id);
        //start queue again
        queueInterval = setInterval(startQueue, lookbackTime);
    }


    //remove a queue
    ESS.Queue.CancelRequest = function (id, obj) {

        var requestObject = "reportRequestId";//reportRequestId:" + id;
        ESS.HttpUtils.Post(ESS.ReportOperations.Module, requestReportCancelEndPoint + "/" + id, requestObject, function (data) {
            //add the data to queue manager
            var d = JSON.parse(data);
            if (d.success === true) {
                noty({ type: 'success', text: '<b>Report request cancelled.</b>', layout: 'topRight' });
                //do operations
                removeCurrentTask(id);
                delete taskStatus[id];
                delete taskNames[id];
                delete taskMessage[id];
                $(obj).parent().parent().remove();
                ESS.Session.Remove(currentUserId, id);
            }
            else {
                noty({ type: 'success', text: '<b>Could not cancel Report request.</b>', layout: 'topRight' });
            }
        });

        //start queue again
        queueInterval = setInterval(startQueue, lookbackTime);
    }

    //get the status text from enum
    ESS.Queue.getStatus = function (i) {
        switch (i) {
            case 6301:
                return 'Pending';
            case 6302:
                return 'Processing';
            case 6303:
                return 'Completed';
            case 6304:
                return 'Canceled';
            case 6305:
                return 'Failed';
            case 6306:
                return 'Scheduled';
            default:
                return "Unknown status";
        }
    }

    //show the earlier history
    var showHistory = function () {
        var history = ESS.QueueStorage.GetHistory(currentUserId);
        var html = '<table class="table table-hover .table-striped .table-bordered">';
        html += '<thead><tr><td>Request Id</td><td>Current Status</td><td>Comments</td><td>Last Checked</td></tr></thead>';
        html += '<tbody>';
        if (history.length > 0) {
            $(history).each(function (i, h) {
                var id = h.id;
                var idhtm = h.name;
                if (h.status === "Completed") {
                    var rData = ESS.Session.GetById(currentUserId, id);
                    if (rData && rData.data.requestedExportFormat < 3) {
                        var reportFormat = rData.data.requestedExportFormat;
                        var reportName = reportFormat == 0 ? h.name + '-PDF' : h.name + '-CSV';
                        idhtm = '<a href="#" onclick="return ESS.Queue.CreateLink(' + id + ',' + reportFormat + ')">' + reportName + '</a>';
                    }
                    else {
                        idhtm = '<a href="/reporting/run/?requestid=' + id + '&type=result">' + h.name + '</a>';
                    }

                }
                html += '<tr><td>' + idhtm + '</td><td>' + h.status + '</td><td>' + h.comments + '</td><td>' + h.lastUpdated + '</td></tr>';
            });
        } else {
            html += '<tr><td colspan="4">No record(s) found!</td></tr>';
        }
        html += '</tbody></table>';

        $("#queue-history").html(html);
    }

    //clear history
    ESS.Queue.clearHistory = function () {
        var ids = _.map(ESS.QueueStorage.GetHistory(currentUserId), 'id');
        ESS.Session.RemoveList(currentUserId, ids);
        ESS.QueueStorage.ClearHistory(currentUserId);
        var html = '<table class="table table-hover .table-striped .table-bordered">';
        html += '<thead><tr><td>Request Id</td><td>Current Status</td><td>Comments</td><td>Last Checked</td></tr></thead>';
        html += '<tbody>';
        html += '<tr><td colspan="4">No record(s) found!</td></tr>';
        html += '</tbody></table>';
        $("#queue-history").html(html);
    }

    var CheckUserIsAuthenticated = function () {
        $.ajax({
            url: '/Home/GetUserIsAuthenticatedOrNot',
            success: setQueueTask,
            error: function (err) { noty({ type: 'error', text: 'User is not authenticated!', layout: 'topRight' }); console.log(err); }
        });
    }

    var setQueueTask = function (data) {
        if (data.IsUserAuthenticated) {
            queueInterval = setInterval(startQueue, lookbackTime);
            $("#clear-history").click(ESS.Queue.clearHistory);
        }

    }

    //initialize
    $(document).ready(function () {
        if ($(location).attr('href').toLowerCase().indexOf("login") === -1) {
            CheckUserIsAuthenticated();
        }
    });

})(window.ESS = window.ESS || {}, jQuery);