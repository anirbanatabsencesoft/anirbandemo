﻿(function (ESS, $) {
    ESS.Utilities = ESS.Utilities || {};

    //check for null or undefined
    ESS.Utilities.isNullOrWhitespace = function (obj) {
        try {
            if (obj == null) {
                return true;
            } else if (typeof (obj) == 'undefined') {
                return true;
            } else if (ESS.Utilities.isJson(obj)) {
                return false;
            } else if (obj.trim() == '') {
                return true;
            }
        }
        catch (e) {
            return false;
        }
    }

    //check whether is json
    ESS.Utilities.isJson = function (str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    //get the query string param
    ESS.Utilities.getQueryParamFromUrl = function (name) {
        var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    }

})(window.ESS = window.ESS || {}, jQuery);