﻿(function (ESS, $) {
    'use strict';
    ESS.QueueStorage = ESS.QueueStorage || {};

    //Add to queue id to store
    ESS.QueueStorage.Add = function (userid, id, name, msg) {
        var ids = ESS.QueueStorage.GetAll(userid);
        ids.push({id: id, name: name, msg: msg});
        localStorage.setItem(userid, JSON.stringify(ids));
    }

    //Get all available ids queued
    ESS.QueueStorage.GetAll = function (userid) {
        var sd = localStorage.getItem(userid);
        if (sd) {
            try
            {
                return JSON.parse(sd);
            }
            catch (e) {  }
        }
        return [];
    }

    //remove the id from queue once notified
    ESS.QueueStorage.Remove = function (userid, id) {
        var ids = ESS.QueueStorage.GetAll(userid);
        ids = $.grep(ids, function (e, i) {
            return e.id != id;
        });
        localStorage.setItem(userid, JSON.stringify(ids));
    }

    //add data to request history
    ESS.QueueStorage.AddToHistory = function (userid, requestId, name, status, msg) {
        var h = ESS.QueueStorage.GetHistory(userid);
        h.push({ "id": requestId, "name": name, "status": status, "comments": msg, "lastUpdated": moment().format('MMMM Do YYYY, h:mm:ss a') });
        localStorage.setItem(userid + "_REQUEST_HISTORY", JSON.stringify(h));
    }

    //get the history data
    ESS.QueueStorage.GetHistory = function (userid) {
        var historyString = localStorage.getItem(userid + "_REQUEST_HISTORY");
        if (historyString) {
            try {
                return JSON.parse(historyString);
            }
            catch (e) { }
        }
        return [];
    }

    //clear history
    ESS.QueueStorage.ClearHistory = function (userid) {
        localStorage.setItem(userid + "_REQUEST_HISTORY", JSON.stringify([]));
    }

})(window.ESS = window.ESS || {}, jQuery);