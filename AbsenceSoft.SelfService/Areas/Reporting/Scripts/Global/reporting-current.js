﻿(function (ESS, $) {
    'use strict';

    ESS.Current = ESS.Current || { };

    //expose variables
    ESS.Current.User = { };
    ESS.Current.Employer = { };
    ESS.Current.Customer = { };
    ESS.Current.Contacts = { };
    ESS.Current.Roles = { };
    ESS.Current.Teams = {};
    ESS.Current.Users = {};

    //get all users
    var getAllUsers = function () {
        $.get('/Administration/GetUsersList', function (data) {
            ESS.Current.Users = data;
        });
    }
    
    //get current data
    var getCurrentUser = function () {
        $.get("/current/user", function (data) {
            ESS.Current.User = JSON.parse(data);
            if (ESS.Current.User.employeeId != null && typeof (ESS.Current.User.employeeId) != 'undefined') {
                //$.get("/Case/Employee/" + ESS.Current.User.employeeId + "/GetAllEmployeeContacts", function (contacts) {
                //    ESS.Current.Contacts = contacts
                //});
            }
        });
    }

    var getCurrentEmployer = function () {
        $.get("/current/employer", function (data) {
            ESS.Current.Employer = data;
        });
    }

    var getCurrentCustomer = function () {
        $.get("/current/customer", function (data) {
            ESS.Current.Customer = JSON.parse(data);
        });
    }

    var getCurrentRoles = function () {
        $.get("/Administration/GetRoles", function (data) {
            ESS.Current.Roles = data;
        });
    }

    var getCurrentTeams = function () {
        $.get("/current/team", function (data) {
            ESS.Current.Teams = data;
        });
    }
    
    //call on ready
    ESS.Current.getCurrentData = function () {
        getCurrentUser();
        getCurrentEmployer();
        getCurrentCustomer();
        //getCurrentRoles();
        //getCurrentTeams();
        //getAllUsers();
    };

    
})(window.ESS = window.ESS || {}, jQuery);