﻿(function (ESS, $) {
    ESS.RunExportSchedule = ESS.RunExportSchedule || {};

    //class references
    var utils = ESS.Utilities;
    var current = ESS.Current;

    //local variables
    var requestReportEndPoint = 'reports/request';  
    var scheduleReportEndPoint = 'reports/schedule';
    var getCompletedReportEndPoint = 'reports/results/';
    
    ESS.RunExportSchedule.RequestedReportArgs = '';
   

    //return empty string on undefined
    ESS.RunExportSchedule.returnEmptyIfUndefined = function (obj) {
        if (obj) {
            return obj;
        }
        return "";
    }

    //request a report execution
    ESS.RunExportSchedule.requestReport = function (requestedExportFormat) {     
       
        //get the request objects
        ESS.RunExportSchedule.RequestedReportArgs = ESS.BuildCriteria.getRequestObject();
        ESS.RunExportSchedule.RequestedReportArgs.Reportparam = ESS.BuildCriteria.reportData;
        ESS.RunExportSchedule.RequestedReportArgs.ExportFormat = requestedExportFormat;

        //validate start and end date
        if (!ESS.RunExportSchedule.RequestedReportArgs.reportDefinition.Criteria.startDate || !ESS.RunExportSchedule.RequestedReportArgs.reportDefinition.Criteria.endDate) {
            noty({ type: 'error', text: '<b>!Error</b><br/>Please select the date range', layout: 'topRight' });
            return;
        }
       

        if (requestedExportFormat === ESS.Enums.enums.ReportExportFormat.RUN) {
            ESS.RunExportSchedule.RequestedReportArgs.PageNumber = 1;
            ESS.RunExportSchedule.RequestedReportArgs.PageSize = 500;
        }
        else {
            ESS.RunExportSchedule.RequestedReportArgs.PageNumber = 1;
            ESS.RunExportSchedule.RequestedReportArgs.PageSize = 50000;
        }

        //check if request is for show report and set the arguments from storage
        if (utils.getQueryParamFromUrl("requestid")) {
            var requestId = utils.getQueryParamFromUrl("requestid");
            var gType = '';
            var rData = ESS.Session.GetById(current.User.id, requestId);
            if (rData) {
                ESS.RunExportSchedule.RequestedReportArgs.Reportparam.id = rData.data.reportid;
                ESS.RunExportSchedule.RequestedReportArgs.Reportparam.isExportableToCsv = rData.data.isExportableToCsv;
                ESS.RunExportSchedule.RequestedReportArgs.Reportparam.isExportableToPdf = rData.data.isExportableToPdf;
                ESS.RunExportSchedule.RequestedReportArgs.Reportparam.availableGroupingFields = rData.data.availableGroupingFields;
                ESS.RunExportSchedule.RequestedReportArgs.Reportparam.reportType = rData.data.reportType;
            }
        }
        else {
            ESS.RunExportSchedule.RequestedReportArgs.Reportparam.reportType = utils.getQueryParamFromUrl("reportType");
            ESS.RunExportSchedule.RequestedReportArgs.Reportparam.reportName = utils.getQueryParamFromUrl("reportName");
        }

        //call service
        ESS.HttpUtils.Post(ESS.ReportOperations.Module, requestReportEndPoint, ESS.RunExportSchedule.RequestedReportArgs, function (data) {
            //add the data to queue manager
            var d = JSON.parse(data);
            if (d.success === true) {
                if (requestedExportFormat === ESS.Enums.enums.ReportExportFormat.RUN) {
                    noty({ type: 'success', text: '<b>We are processing your report<b/><br/>You can wait or can do some other task.<br/>We will notify once complete.', layout: 'topRight' });
                }
                else {
                    noty({ type: 'success', text: '<b>We are processing your report for exporting data.<b/><br/>You can wait or can do some other task.<br/>We will notify once complete.', layout: 'topRight' });
                }
                var reportParamData = {
                    reportid: ESS.RunExportSchedule.RequestedReportArgs.Reportparam.id,
                    groupby: ESS.RunExportSchedule.RequestedReportArgs.reportDefinition.GroupType,
                    isExportableToCsv: ESS.RunExportSchedule.RequestedReportArgs.Reportparam.isExportableToCsv,
                    isExportableToPdf: ESS.RunExportSchedule.RequestedReportArgs.Reportparam.isExportableToPdf,
                    availableGroupingFields: ESS.RunExportSchedule.RequestedReportArgs.Reportparam.availableGroupingFields,
                    requestedExportFormat: requestedExportFormat,
                    reportType: ESS.RunExportSchedule.RequestedReportArgs.Reportparam.reportType ? ESS.RunExportSchedule.RequestedReportArgs.Reportparam.reportType : utils.getQueryParamFromUrl("type"),
                    reportName: ESS.RunExportSchedule.RequestedReportArgs.Reportparam.reportName
                };

                //add required report data to local storage
                ESS.Session.Add(current.User.id, d.data.id, reportParamData);

                //add to queue manager
                ESS.QueueStorage.Add(current.User.id, d.data.id, ESS.RunExportSchedule.returnEmptyIfUndefined(ESS.RunExportSchedule.RequestedReportArgs.reportDefinition.Name), ESS.RunExportSchedule.returnEmptyIfUndefined(ESS.RunExportSchedule.RequestedReportArgs.reportDefinition.Criteria.plainEnglish));
            } else {
                noty({ type: 'error', text: '<b>!Error</b><br/>' + d.message, layout: 'topRight' });
            }
        });
    }

    //show report
    ESS.RunExportSchedule.showReport = function () {
        var id = utils.getQueryParamFromUrl("requestid");
        if (id) {
            ESS.HttpUtils.Get(ESS.ReportOperations.Module, getCompletedReportEndPoint + id, ESS.RunExportSchedule.renderReport);
        }
    }

    //show the result
    ESS.RunExportSchedule.renderReport = function (reportResult) {
        var reportData = jQuery.parseJSON(reportResult);
        if (reportData.success) {
            //Set Header
            ESS.BuildCriteria.generateHeading(reportData.data);

            //set criteria
            ESS.BuildCriteria.criteriaData = reportData.data.criteria;
            ESS.BuildCriteria.buildCriteriaParam();
            ESS.BuildCriteria.reportData = { category: reportData.data.category, name: reportData.data.name };        
            $(".select2-multiple").select2({ closeOnSelect: true });

            $("#reportCriteriaFilters").collapse('hide');
            $("#reportCriteriaFilters").show();
            $("#reportCriteriaFilters").collapse('show');

            $('#filterCriteria').val(reportData.data.criteria.plainEnglish);
            if (reportData.data && reportData.data.jsonData && reportData.data.jsonData.length > 0) {
                if (reportData.data.chart !== null) {
                    $('#reportResult').drawChart({
                        chartType: 2,
                        isGroup: false,
                        data: reportData.data.jsonData,
                        chart: reportData.data.chart
                    });
                }
                else if (reportData.data.jsonData[0].Key) {
                    $('#reportResult').drawChart({
                        chartType: 1,
                        isGroup: true,
                        data: reportData.data.jsonData
                    });
                }
                else {
                    $('#reportResult').drawChart({
                        chartType: 1,
                        isGroup: false,
                        data: reportData.data.jsonData
                    });
                }
            }
            else {
                $('#reportResult').html('No data found for selected criteria.')
            }

            var reqid = utils.getQueryParamFromUrl("requestid");
            var rData = ESS.Session.GetById(current.User.id, reqid);
            if (rData) {
                $('#export-csv').prop("disabled", !rData.data.isExportableToPdf);
                $('#export-pdf').prop("disabled", !rData.data.isExportableToCsv);
                $('#report-type').html(rData.data.reportType + ' | ');
                $('.essreportname').html(rData.data.reportName);
            }
        }
        else {
            $("#report-saved-alert").show();
            $("#report-saved-alert span").text('Could not get data for the request.');
        }
    }

    //schedule a report
    ESS.RunExportSchedule.scheduleReport = function () {
        if ($.trim($("#schRepSelTeam").val()).length === 0 && $.trim($("#schRepSelRole").val()).length === 0 && $.trim($("#schRepSelUser").val()).length === 0) {
            noty({ type: 'error', text: '<b>!Error</b><br/> Please select User/ Role/ Team.', layout: 'topRight' });
            return;
        }
        else if ($.trim($('#sch-date-picker input').val()).length === 0 || $.trim($('#sch-time-picker input').val()).length === 0)
        {
            noty({ type: 'error', text: '<b>!Error</b><br/> Please select Schedule date and time.', layout: 'topRight' });
            return;
        }

        var requestInfo = ESS.BuildCriteria.getRequestObject();       
        requestInfo.ExportFormat = $('#export-format option:selected').val()
        var sharingInfo = {
            "SharedWith": $('input[name="schRepSharedWith"]:checked').val(),
            "TeamKeys": $("#schRepSelTeam").val(),
            "RoleNames": $("#schRepSelRole").val(),
            "UserKeys": $("#schRepSelUser").val()
        }
        var schDt = moment($('#sch-date-picker input').val() + ' ' + $('#sch-time-picker input').val());        
        var ScheduleReportArgs = {
            "RequestInfo": requestInfo,
            "ScheduleInfo": {
                "RecurrenceType": $('#schedule-frequency option:selected').val(),
                "StartDate": schDt.utc().format(),
                "WeeklySelectedDays": {
                    "Sunday": ($('#schedule-day option:selected').text() === "Sunday"),
                    "Monday": ($('#schedule-day option:selected').text() === "Monday"),
                    "Tuesday": ($('#schedule-day option:selected').text() === "Tuesday"),
                    "Wednesday": ($('#schedule-day option:selected').text() === "Wednesday"),
                    "Thursday": ($('#schedule-day option:selected').text() === "Thursday"),
                    "Friday": ($('#schedule-day option:selected').text() === "Friday"),
                    "Saturday": ($('#schedule-day option:selected').text() === "Saturday")
                }
            },
            "SharingInfo": sharingInfo
        };

        try {
            ESS.HttpUtils.Post(ESS.ReportOperations.Module, scheduleReportEndPoint, ScheduleReportArgs, function (data) {
                var d = JSON.parse(data);
                if (d.success === true) {
                    noty({ type: 'success', text: 'Report scheduled successfully!', layout: 'topRight' });
                } else {
                    noty({ type: 'error', text: '<b>!Error</b><br/>' + d.message, layout: 'topRight' });
                }
            });
        }
        catch (ex) {
            noty({ type: 'error', text: '<b>!Error</b><br/>' + ex, layout: 'topRight' });
        }
    }

    //show hide recipients
    ESS.RunExportSchedule.showRecipientsInput = function () {
        $("#schRepUser").hide();
        $("#schRepTeam").hide();
        $("#schRepRole").hide();

        //attach event
        $('input[name="schRepSharedWith"]').click(ESS.RunExportSchedule.showRespectiveInput);

        //fill the data
        ESS.RunExportSchedule.fillUsers();
        ESS.RunExportSchedule.fillRoles();
        ESS.RunExportSchedule.fillTeams();

        $(".select2-multiple").select2({ closeOnSelect: true });
    }

    //on clicking selection
    ESS.RunExportSchedule.showRespectiveInput = function (sel, obj) {
        selection = $('input[name="schRepSharedWith"]:checked').val();
        if (selection === "USER") {
            $("#schRepUser").show();
            $("#schRepTeam").hide();
            $("#schRepRole").hide();
        } else if (selection === "TEAM") {
            $("#schRepUser").hide();
            $("#schRepTeam").show();
            $("#schRepRole").hide();
        } else if (selection === "ROLE") {
            $("#schRepUser").hide();
            $("#schRepTeam").hide();
            $("#schRepRole").show();
        }
    }

    //fill users
    ESS.RunExportSchedule.fillUsers = function () {
        var users = ESS.Current.Users;

        $.each(users, function (i, item) {
            $('#schRepSelUser').append($('<option>', {
                value: item.Id,
                text: item.DisplayName
            }));
        });
    }

    //fill roles
    ESS.RunExportSchedule.fillRoles = function () {
        var roles = ESS.Current.Roles;

        $.each(roles, function (i, item) {
            $('#schRepSelRole').append($('<option>', {
                value: item.Id,
                text: item.Name
            }));
        });
    }

    //fill team
    ESS.RunExportSchedule.fillTeams = function () {
        var teams = ESS.Current.Teams;

        $.each(teams, function (i, item) {
            $('#schRepSelTeam').append($('<option>', {
                value: item.Id,
                text: item.Name
            }));
        });
    }

    //bind on the respective page
    $(document).ready(function () {

        if ($(location).attr('href').toLowerCase().indexOf("reporting/run") > -1) {
            //get the type and the id
           var type = utils.getQueryParamFromUrl("type");        
         
            //bind export buttons
            $('#export-csv').on('click', function () {
                ESS.RunExportSchedule.requestReport(ESS.Enums.enums.ReportExportFormat.CSV);
            });
            $('#export-pdf').on('click', function () {
                ESS.RunExportSchedule.requestReport(ESS.Enums.enums.ReportExportFormat.PDF);
            });

            if (type === 'result') { 
                //show report
                ESS.RunExportSchedule.showReport();
                $("#run-report-span").text("Re-Run Report");              
            } else {
                $("#run-report-span").text("Run Report");
            }
            
            $('#run-report').on('click', function () {
                ESS.RunExportSchedule.requestReport(ESS.Enums.enums.ReportExportFormat.RUN);
            });            
        }
    });

})(window.ESS = window.ESS || {}, jQuery);