﻿(function (ESS, $) {
    ESS.BuildCriteria = ESS.BuildCriteria || {};
    var current = ESS.Current;
    var utils = ESS.Utilities;
    var type = "";
    var id = "";
    var groupBy = "";
    var isCriteriaLoaded = false;
    var isGroupLoaded = false;
    var interval = null;

    //global variables
    ESS.BuildCriteria.criteriaData = "";
    ESS.BuildCriteria.reportData = "";

    // check if session data is expired 
    var isExpired = function (expiryTime) {
        return parseInt(moment.duration(moment().diff(expiryTime))._milliseconds / 360000) > 60 ? true : false;
    }

    // get the report criteria and load
    var loadReportCriteria = function () {
        var report = ESS.Session.GetReportData(id);
        if (report && !isExpired(report.expiry)) {
            ESS.BuildCriteria.reportData = report.reportData;
            ESS.BuildCriteria.buildReportParam();
        }
        else {
            //get report
            ESS.HttpUtils.Get("reporting", "reports/operation/get/id/" + id, function (result) {
                var d = JSON.parse(result);
                if (d.success === true) {
                    ESS.BuildCriteria.reportData = d.data;
                    ESS.Session.SetReportData(id, d.data);
                    ESS.BuildCriteria.buildReportParam();
                }
            });
        }
    }

    //Load the report information
    var loadReport = function () {
        var cData = ESS.Session.GetCriteria(id);
        if (cData && !isExpired(cData.expiry)) {
            ESS.BuildCriteria.criteriaData = cData.criteriaData;
            ESS.BuildCriteria.buildCriteriaParam();
            $(".select2-multiple").select2({ closeOnSelect: true });
        }
        else {
            //get criteria
            ESS.HttpUtils.Get("reporting", "reports/operation/criteria/get/reportid/" + id, function (result) {
                var d = JSON.parse(result);
                if (d.success === true) {
                    ESS.BuildCriteria.criteriaData = d.data;
                    ESS.Session.AddCriteria(id, d.data);
                    ESS.BuildCriteria.buildCriteriaParam();
                    $(".select2-multiple").select2({ closeOnSelect: true });
                }
            });
        }
    }

    //get the report
    var getReportAndCriteria = function () {
        $("#reportCriteriaFilters").hide();
        $("#group-by").hide();

        loadReportCriteria();
        loadReport();

        interval = setInterval(showLoadingTillCompleteLoad, 100);
    }

    //show the loading till the loading is complete
    var showLoadingTillCompleteLoad = function () {
        if (isCriteriaLoaded === true && isGroupLoaded === true) {
            clearInterval(interval);
            $("#reportCriteriaFilters").show();
            $("#group-by").show();
            $("#loaddiv").hide();

            if (groupBy) {
                setTimeout(function () { $("input[name='groupby'][value='" + ESS.BuildCriteria.GetGroupByType(groupBy) + "']").click(); }, 200);
            }
        }
    }

    //generate criteria
    ESS.BuildCriteria.buildCriteriaParam = function () {
        if (!utils.isNullOrWhitespace(ESS.BuildCriteria.criteriaData)) {
            var reqid = utils.getQueryParamFromUrl("requestid");
            var html = '', rData = '';

            if (ESS.BuildCriteria.criteriaData.showAgeCriteria) {
                html += getAgeCriteria();
            }

            var rData = ESS.Session.GetById(current.User.id, reqid);
            if (ESS.BuildCriteria.criteriaData.hasCriteria == true) {
                var ctrlCount = 0;
                html += '<div class="col-sm-12"><div class="row collapse';
                if (reqid != null && reqid != undefined && reqid.length > 0) {
                    html += ' in';
                }

                html += '" id="criteria"><div class="col-sm-12 ess-caseinputs"><div class="col-sm-12">';                    
                
                if (!ESS.BuildCriteria.criteriaData.isChart) {                    
                    html += '<div class="col-sm-6"><label class="control-label">Date Range</label><table class="criteriafiltertable criteriafilter">';
                    var startDate = new Date(ESS.BuildCriteria.criteriaData.startDate);
                    html += '<tr><td class="dateinput"><input type="textbox" placeholder="MM/DD/YYYY" value="' + (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear() + '" id = "startDate" class = "form-control input-sm datepicker"/></td> ';
                    html += '<td class="to"><label>TO</label></td>';
                    var endDate = new Date(ESS.BuildCriteria.criteriaData.endDate);
                    html += '<td class="dateinput"><input type="textbox" value="' + (endDate.getMonth() + 1) + '/' + endDate.getDate() + '/' + endDate.getFullYear() + '" id = "endDate" class = "form-control input-sm datepicker"/></td>';
                    html += '</tr></table></div>';
                    ctrlCount++;
                }

                for (var i = 0; i < ESS.BuildCriteria.criteriaData.filters.length; i++) {
                    if (ctrlCount == 0) {
                        html += '<div class="col-sm-12">';                        
                    }
                    html += '<div class="col-lg-6 col-xs-12"><input type="hidden" value="' + ESS.BuildCriteria.criteriaData.filters[i].prompt + '"/>';
                    html += '<label class="control-label">' + ESS.BuildCriteria.criteriaData.filters[i].prompt + '</label>';
                    html += '<div class="criteriafilter">';
                    var ctrl = ESS.HtmlHelper.GenerateHtml(ESS.BuildCriteria.criteriaData.filters[i]);
                    if (!utils.isNullOrWhitespace(ctrl)) {
                        html += ctrl;
                        ctrlCount++;
                        if (ctrlCount == 2) {
                            html += '</div>';//closing col-sm-12
                            ctrlCount = 0;
                        }
                    }
                    html += '</div></div>';
                }
                var exportOptions = getExportOptions(ESS.BuildCriteria.criteriaData);
                html += exportOptions;
                $("#reportCriteriaFilters").html(html);

                $("#startenddate").show();
                if (ESS.BuildCriteria.criteriaData.showAgeCriteria === true) {
                    $("#agefromto").show();
                } else {
                    $("#agefromto").hide();
                }
                
                ESS.BuildCriteria.setReportCriteria();
            }
        }
        resetHeight();
        isCriteriaLoaded = true;
    }

    getAgeCriteria = function () {
        var html = '';
        html = '<div class="col-sm-12 col-wrap"><div class="col-sm-offset-1 col-sm-5"><div class="form-horizontal"><div class="form-group"><label class="col-sm-3 control-label">Age From</label>';
        html += '<div class="col-sm-4"><div class="input-group date editor"><input id="criteriaAgeFrom" type="textbox" value="' + ESS.BuildCriteria.ageFrom + '" class="form-control input-sm datepicker"/>';
        html += '<label for="criteriaAgeFrom" class="input-group-addon"><i class="fa fa-calendar"></i></label></div></div><div class="col-sm-1 text-center"><label class="control-label">To</label></div>';
        html += '<div class="col-sm-4"><div class="input-group date editor"><input id="criteriaAgeFrom" type="textbox" value="' + ESS.BuildCriteria.ageTo + '" class="form-control input-sm datepicker"/>';
        html += '<label for="criteriaAgeTo" class="input-group-addon"><i class="fa fa-calendar"></i></label></div></div></div></div></div></div>';
        return html;
    }

    getExportOptions = function (data) {
        var html = '<div class="col-sm-12"><div class="pull-right"><i class="fa fa-spinner fa-spin hidden"></i>';

        if (!data.isChart) {
            html += '<button id="export-csv" value="Export To CSV" class="btn btn-secondary"><span>Export To CSV</span></button>';
            html += '<button id="export-pdf" value="Export To PDF" class="btn btn-secondary"><span>Export To PDF</span></button>';
            html += '<button class="btn btn-secondary" id="view-progress" data-toggle="modal" data-target="#queueStatusModal"><span>View Progress</span></button >';
            html += '<button id="run-report" value="Run Report" class="btn run-button"><i class="fa fa-play-circle" aria-hidden="true"></i><span>Run Report</span></button>';
        }
        html += '</div></div>';
        return html;
    }

    ESS.BuildCriteria.setReportCriteria = function () {

        for (var i = 0; i < ESS.BuildCriteria.criteriaData.filters.length; i++) {
            var d = ESS.BuildCriteria.criteriaData.filters[i];
            switch (d.controlType) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 11:
                    $("#" + d.fieldName).val(d.value);
                    break;
                case 7:
                    $("input[name='" + d.fieldName + "'][value='" + d.value + "']").click();
                    break;
                case 8:
                    $("input[name='" + d.fieldName + "'][value='" + d.value + "']").prop('checked', true);
                    break;
                case 9:
                case 12:
                    var controlIdValues = ESS.BuildCriteria.criteriaData.filters.find(function (x) {
                        return x.fieldName === d.fieldName;
                    });
                    $('#' + d.fieldName + ' option').filter(function () {
                        if ($(this).val()) {
                            if (controlIdValues.values !== null) {
                                return controlIdValues.values.indexOf($(this).val()) > -1;
                            } else {
                                return controlIdValues.value === $(this).val();
                            }
                        }
                    }).prop('selected', true);
                    break;
            }
        }

        var reqid = utils.getQueryParamFromUrl("requestid");
        var rData = null;
        //var rData = ESS.Session.GetById(current.User.id, reqid);
        var groupBy = "none";
        if (rData && rData.data.groupby) {
            groupBy = rData.data.groupby;
        }
        $("input[name='groupby'][value='" + groupBy + "']").attr('checked', true).click();
        //  isGroupLoaded = true;
    }

    //generate the criteria
    ESS.BuildCriteria.buildReportParam = function () {
        if (!utils.isNullOrWhitespace(ESS.BuildCriteria.reportData)) {
            //heading
            ESS.BuildCriteria.generateHeading(ESS.BuildCriteria.reportData);

            //build group by          
            isGroupLoaded = true;

            var enableCsvExport = ESS.BuildCriteria.reportData.isExportableToCsv;
            var enablePdfExport = ESS.BuildCriteria.reportData.isExportableToPdf;
            $('#export-csv').prop("disabled", !enableCsvExport);
            $('#export-pdf').prop("disabled", !enablePdfExport);
        }
    }

    ESS.BuildCriteria.showRespectiveDurationValue = function () {
        ESS.BuildCriteria.hideAllDurationValue();
        var selectedDuration = $('#Duration').val();
        switch (selectedDuration) {
            case "Monthly":
                $('#monthlyDuration').show();
                break;
            case "Quarterly":
                $('#quarterlyDuration').show();
                break;
            case "Yearly":
                $('#yearlyDuration').show();
                break;
            case "DateRange":
                $('#daterangeDuration').show();
                break;
            case "Rolling":
                $('#rollingDuration').show();
                break;
        }
    }

    ESS.BuildCriteria.hideAllDurationValue = function () {
        $('#monthlyDuration').hide();
        $('#yearlyDuration').hide();
        $('#quarterlyDuration').hide();
        $('#daterangeDuration').hide();
        $('#rollingDuration').hide();
    }

    ESS.BuildCriteria.setDurationValue = function () {
        var durationValue = $('#Duration').val();
        switch (durationValue) {
            case 'Monthly':
                var selectedMonth = $('#selectedMonth').val();
                /// Months are 0 based, so month - 1 gets actual month
                startDate = new Date(startDate.getFullYear(), selectedMonth - 1, 1);
                /// Setting the date of the next month to 0 gets the last day of the month we want
                endDate = new Date(endDate.getFullYear(), selectedMonth, 0);
                break;
            case 'Quarterly':
                var selectedQuarter = $('#selectedQuarter').val();
                startDate = new Date(startDate.getFullYear(), selectedQuarter * 3 - 3, 1);
                endDate = new Date(startDate.getFullYear(), selectedQuarter * 3, 0);
                break;
            case 'Yearly':
                var selectedYear = $('#selectedYear').val();
                startDate = new Date(selectedYear, 0, 1);
                endDate = new Date(selectedYear, 11, 31);
                break;
            case 'DateRange':
                startDate = new Date(Date.parse($('#txtStartDate').val()));
                if (isNaN(startDate)) {
                    startDate = new Date();
                    $('#txtStartDate').val(startDate.formatMMDDYYYY());
                }

                endDate = new Date(Date.parse($('#txtEndDate').val()));
                if (isNaN(endDate)) {
                    endDate = startDate.addMonths(1);
                    $('#txtEndDate').val(endDate.formatMMDDYYYY());
                }

                if (endDate < startDate) {
                    ESS.Reports.SetChartError("End Date must come before Start Date.");
                    return;
                }

                break;
            case 'Rolling':
                startDate = new Date(Date.parse($('#rollingStartDate').val()));
                if (isNaN(startDate)) {
                    startDate = new Date();
                    $('#rollingStartDate').val(startDate.formatMMDDYYYY());
                }
                // Rolling Year
                endDate = new Date(startDate.getFullYear() + 1, startDate.getMonth(), startDate.getDate());
                break;
            default:
                endDate = new Date();
                startDate = new Date(endDate.getFullYear() - 1, endDate.getMonth(), endDate.getDate());
                break;
        }
    }

    //generate name
    ESS.BuildCriteria.generateHeading = function (d) {
        if (!utils.isNullOrWhitespace(d.category)) {
            $("#report-category").html(d.category);
        }
        if (!utils.isNullOrWhitespace(d.name)) {
            $("#report-name").html(d.name);
        }
    }

    //set the active class on select
    ESS.BuildCriteria.makeActive = function (obj) {
        var btn = $(obj);
        btn.parent().find('.criteria-selected').each(function (i, e) { $(e).removeClass('criteria-selected') });
        btn.addClass('criteria-selected');
    }

    //set the muti-active
    ESS.BuildCriteria.makeMultiActive = function (obj) {
        var btn = $(obj);
        var cls = 'criteria-selected';
        if (btn.hasClass(cls)) {
            btn.removeClass(cls);
        } else {
            btn.addClass();
        }
    }

    //Get request object
    ESS.BuildCriteria.getRequestObject = function () {
        setCriteriaFilterValues();
        var arg = {};
        arg.CustomerId = ESS.Current.Customer.id;
        arg.ReportId = ESS.BuildCriteria.reportData.id;
        if (!arg.ReportId) {
            if (utils.getQueryParamFromUrl("requestid")) {
                var requestedReportId = utils.getQueryParamFromUrl("requestid");
                var rData = ESS.Session.GetById(current.User.id, requestedReportId);
                arg.ReportId = rData.data.reportid;
            }
        }

        var reportype = 0;// operations

        arg.ReportRequestType = reportype;
        arg.ExportType = 0;
        var grpType = utils.getQueryParamFromUrl("groupType");

        arg.reportDefinition = {
            Name: ESS.BuildCriteria.reportData.name,
            Description: null,
            ReportType: null,
            Fields: null,
            Criteria: ESS.BuildCriteria.criteriaData,
            GroupType: grpType,
            GroupByColumns: null
        };

        arg.pageNumber = 1;
        arg.pageSize = 10;

        console.log(arg);
        return arg;
    }

    //set the criteria values
    var setCriteriaFilterValues = function () {
        ESS.BuildCriteria.criteriaData.startDate = $('#startDate').val();
        ESS.BuildCriteria.criteriaData.endDate = $('#endDate').val();

        var english = "";
        english += "From '" + $('#startDate').val() + "' through '" + $('#endDate').val() + "'";

        if ($('#ageFrom').is(':visible') === true && $('#ageFrom').is(':hidden') === false) {

            if ($('#ageFrom').val().length > 0 && $('#ageTo').val().length > 0) {
                english += " where Age between " + $('#ageFrom').val() + " and " + $('#ageTo').val();
            }
            else {
                if ($('#ageFrom').val().length > 0) {
                    english += " where Age greater than " + $('#ageFrom').val();
                }

                if ($('#ageTo').val().length > 0) {
                    english += " where Age less than " + $('#ageTo').val();
                }
            }
        }

        for (var i = 0; i < ESS.BuildCriteria.criteriaData.filters.length; i++) {
            var d = ESS.BuildCriteria.criteriaData.filters[i];
            switch (d.controlType) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 11:
                    var valSet = $("#" + d.fieldName).val();
                    if (valSet.length > 0) {
                        d.value = valSet;
                        english = setEnglishVal(d, english);
                    }
                    else {
                        d.value = null;
                    }
                    break;
                case 7:
                    d.value = $('input[name="' + d.fieldName + '"]:checked').val();
                    english = setEnglishVal(d, english);
                    break;
                case 8:
                    d.values = $('input[name="' + d.fieldName + '"]:checked').val();
                    english = setEnglishVal(d, english);
                    break;
                case 9:
                case 12:
                    d.values = $('#' + d.fieldName).val();
                    english = setEnglishVal(d, english);
                    break;
            }
        }
        ESS.BuildCriteria.criteriaData.plainEnglish = english;
    }

    var setEnglishVal = function (d, english) {
        if ((d.value && d.value != '') || (d.values && d.values.length > 0)) {
            if (english.indexOf('where') <= 0) {
                english += " where ";
            }
            else {
                english += ", ";
            }
            if (d.options && d.options.length > 0 && d.value) {
                var val = $.isNumeric(d.value) && d.value.length < 5 ? parseInt(d.value) : d.value;
                english += d.prompt + " is [" + (_.find(d.options, "value", val) ? _.find(d.options, "value", val).text : '') + "]";
            }
            else if (d.values && d.values.length > 0) {
                english += d.prompt + " is in ";

                var multiVals = '[';
                for (var item = 0; item < d.values.length; item++) {
                    var vals = $.isNumeric(d.values[item]) && d.values[item].length < 5 ? parseInt(d.values[item]) : d.values[item];
                    multiVals += _.find(d.options, "value", vals) ? _.find(d.options, "value", vals).text + "," : "";
                }
                var currentIndex = multiVals.lastIndexOf(",");
                multiVals = multiVals.substring(0, currentIndex);
                english += multiVals + ']';
            }
            else {
                english += d.prompt + " is [" + d.value + "]";
            }
        }
        return english;
    }

    var resetHeight = function () {
        $(".criteria-result").css({ height: 'auto' });
    }

    //return the group value by index
    ESS.BuildCriteria.GetGroupByType = function (index) {
        var arr = ['custom', 'workState', 'reason', 'policy', 'organization', 'location', 'leave', 'duration', 'accommodationType', 'caseManager', 'condition', 'conditionByCondition', 'conditionByOrganization', 'durationByCondition', 'durationByOrganization', 'caseReport', 'caseReportByDay', 'caseReportByOrganization', 'needleSticks', 'injuryDetails', 'biReportTotalLeavesByReason', 'biReportTotalLeavesByLocation'];
        if (index < arr.length) {
            return arr[index];
        }
        return "";
    }

    //call it in document ready
    $(document).ready(function () {
        if ($(location).attr('href').indexOf("reporting/run") > -1) {
            $("#startenddate").hide();
            $("#agefromto").hide();
            $("#rerunreport").hide();

            type = utils.getQueryParamFromUrl("type");
            id = utils.getQueryParamFromUrl("reportId");

            ESS.Current.getCurrentData();
            getReportAndCriteria();
            $('#reportCriteriaFilters').collapse('show');

            $(".cancel-btn").click(function () {
                $("#report-settings").collapse('hide');
            });
        }
    });

})(window.ESS = window.ESS || {}, jQuery);