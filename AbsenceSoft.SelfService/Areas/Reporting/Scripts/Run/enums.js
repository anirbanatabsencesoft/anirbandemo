﻿(function (ESS, $) {
    ESS.Enums = ESS.Enums || {};

    ESS.Enums.enums = Object.freeze({
        ReportStatus: Object.freeze({
            Pending: 6301, Processing: 6302, Completed: 6303, Canceled: 6304, Failed: 6305, Scheduled: 6306
        }),
        ReportExportFormat: Object.freeze({ RUN: 3, CSV: 1, PDF: 0 }),
        ReportRecurrenceType: Object.freeze({ OneTime: 0, Daily: 1, Weekly: 2, Monthly: 3, Yearly: 4 }),
        GroupType: Object.freeze({"Custom": 0, "By Work State": 1, "By Reason": 2, "By Policy": 3, "By Organization": 4, "By Office Location": 5, "By Leave": 6})
    });

})(window.ESS = window.ESS || {}, jQuery);