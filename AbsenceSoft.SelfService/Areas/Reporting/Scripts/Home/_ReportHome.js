﻿(function (ESS, $) {
    ESS.ReportHome = ESS.ReportHome || {};

    var utils = ESS.Utilities;
    var reportData = "";

    var reportType = {
        TeamReport: 'MY TEAM REPORT', OrganizationReport: 'MY ORGANIZATION REPORT'
    };

    //load the categories on click
    var loadCategory = function (categoryName) {
        var url = "reports/ess/type/" + categoryName;
        callHttp(categoryName, reportData, url);
    };

    //call respective http
    var callHttp = function (category, data, url) {
        if (utils.isNullOrWhitespace(data)) {
            ESS.HttpUtils.Get("reporting", url, function (result) {
                if (!utils.isNullOrWhitespace(result)) {
                    var d = JSON.parse(result);
                    if (d.success === true) {
                        data = d.data;
                        console.log(data);
                        generateReportList(data, category);
                    }
                }
            });
        }
        else {
            generateReportList(data, type);
        }
    }

    //generate the report list
    var generateReportList = function (data, type) {
        var contentArea = $('#' + type);
        var html = generateHtml(data, type);
        contentArea.empty();
        if (html && data.length > 0) {
            contentArea.html(html);
        } else {
            contentArea.html('<ul class="report-list"><li class="single-report-item">No record(s) found</li></ul>');
        }
    }

    //generate the HTML for report list
    var generateHtml = function (dataItems, type) {
        
        var html = '';
        for (var itemRow = 0; itemRow < dataItems.length; itemRow++) {
            var data = dataItems[itemRow];
            if (data.hasChildren) {
                html += '<li class="reportlist"><a class="updownarrow" href="#" id="parent_' + data.id + '" data-toggle="collapse" data-target="#' + data.id + '" aria-expanded="true"><h4>' + data.title + '</h4></a>';
                html += '<ul class="nav reportlistholder collapse in" id="' + data.id + '" role="menu" aria-labelledby="parent_' + data.id + '">';
                for (var rpt = 0; rpt < data.childReportList.length; rpt++) {
                    var reportData = data.childReportList[rpt];
                    var groupTypeInfo = '&groupType=' + ((ESS.Enums.enums.GroupType[reportData.title] != undefined) ? ESS.Enums.enums.GroupType[reportData.title] : 0);
                    html += '<li><a href="/reporting/run?reportId=' + reportData.reportId + '&requestId=&reportType=' + reportType[type] + '&title=' + reportData.title + '&reportName=' + reportData.category + groupTypeInfo + '"><span>' + reportData.title + '</span>';
                    html += '<span class="rightarrow"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></span></a></li>';
                }

                html += '</ul></li >';
            }
            else {
                var appendOrgParam = '';
                if (type === 'OrganizationReport')
                {
                    appendOrgParam = '&groupType=' + ESS.Enums.enums.GroupType["By Organization"];
                }

                html += '<li class="reportlist" id="parent' + data.id + '"><a class="left" role="button" href="/reporting/run?reportId=' + data.reportId + '&requestId=&reportType=' + reportType[type] + '&title=' + data.title + '&reportName=' + escape(data.title) + appendOrgParam + '">';
                html += '<h4>' + data.title + '</h4><span class="rightarrow"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></span></a></li>';
            }
        }
        return html;
    }
    
    $(document).ready(function () {
        if ($('#TeamReport').length > 0) {
            loadCategory('TeamReport');
            loadCategory('OrganizationReport');
        }
    });

})(window.ESS = window.ESS || {}, jQuery);