﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace AbsenceSoft.SelfService
{
    public static class EnumDropDown
    {
        private static Type GetNonNullableModelType(ModelMetadata modelMetadata)
        {
            Type realModelType = modelMetadata.ModelType;

            Type underlyingType = Nullable.GetUnderlyingType(realModelType);
            if (underlyingType != null)
            {
                realModelType = underlyingType;
            }
            return realModelType;
        }

        private static readonly SelectListItem[] SingleEmptyItem = new[] { new SelectListItem { Text = "", Value = "" } };

        public static string GetEnumDescription<TEnum>(TEnum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if ((attributes != null) && (attributes.Length > 0))
                return attributes[0].Description;
            else
                return value.ToString().SplitCamelCaseString();
        }

        public static MvcHtmlString EnumButtonListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression) where TModel : class
        {
            return htmlHelper.EnumButtonListFor(expression, null);
        }

        public static MvcHtmlString EnumButtonListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, object htmlAttributes) where TModel : class
        {
            var htmlAttributesAsDictionary = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var enumType = GetNonNullableModelType(metadata);
            var values = Enum.GetValues(enumType).Cast<TEnum>();
            var enumStringBuilder = new StringBuilder();

            enumStringBuilder.AppendFormat("<div class='btn-group group-{0}' data-toggle='buttons'>", values.Count());
            foreach (var item in values)
            {
                if (item.Equals(metadata.Model))
                    htmlAttributesAsDictionary.Add("checked", "true");
                else htmlAttributesAsDictionary.Remove("checked");

                enumStringBuilder.AppendFormat("<label class='btn btn-info {0}'>{1}{2}</label>",
                    item.Equals(metadata.Model) ? "active" : string.Empty,
                    GetEnumDescription(item),
                    htmlHelper.RadioButtonFor(expression, item, htmlAttributesAsDictionary));
            }
            enumStringBuilder.AppendFormat("</div>");

            return MvcHtmlString.Create(enumStringBuilder.ToString()); ;
        }

        public static string RadioButton(this HtmlHelper htmlHelper, string name, SelectListItem listItem,
                             IDictionary<string, object> htmlAttributes)
        {

            //TODO dit kan beter met htmlHelper.RadioButtonFor(expression, item.Value, new { id = id, @class = htmlAttributes["class"] }).ToHtmlString();

            var id = new StringBuilder();

            id.Append(name)
              .Append("_")
              .Append(listItem.Value);

            var sb = new StringBuilder();

            var label = new TagBuilder("label");

            label.MergeAttribute("class", "radio");

            var radio = new TagBuilder("input");

            radio.MergeAttribute("type", "radio");
            radio.MergeAttribute("value", listItem.Value);
            radio.MergeAttribute("id", id.ToString());
            radio.MergeAttribute("name", name);
            radio.MergeAttributes(htmlAttributes);

            if (listItem.Selected)
            {
                radio.MergeAttribute("checked", "checked");
            }

            label.InnerHtml = radio.ToString() + listItem.Text;

            return label.ToString();
        }

        public static TProperty GetValue<TModel, TProperty>(HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression) where TModel : class
        {
            TModel model = htmlHelper.ViewData.Model;
            if (model == null)
            {
                return default(TProperty);
            }
            Func<TModel, TProperty> func = expression.Compile();
            return func(model);
        }


        public static IEnumerable<SelectListItem> GetEnumSelectListItems(object target, Type type = null)
        {
            var TEnum = type;
            if (TEnum == null)
            {
                try
                {
                    TEnum = target.GetType();
                }
                catch { }
            }
            if (TEnum == null)
                throw new ArgumentNullException("type");

            if (TEnum.IsGenericType && TEnum.GetGenericTypeDefinition() == typeof(Nullable<>))
                TEnum = TEnum.GetGenericArguments().FirstOrDefault() ?? TEnum;

            if (!TEnum.IsEnum)
                throw new ArgumentException("T must be an enumerated type");

            foreach (var val in Enum.GetValues(TEnum))
            {
                var e = val.ToString();
                yield return new SelectListItem
                {
                    Value = e,
                    Text = e.SplitCamelCaseString()
                };
            }
        }

        public static MvcHtmlString EnumDropDownList<TEnum>(this HtmlHelper htmlHelper, string name, object value, string blankValue = null, object htmlAttributes = null) where TEnum : struct, IConvertible
        {
            if (!typeof(TEnum).IsEnum)
                throw new ArgumentException("TEnum must be an enum type");

            var items = Enum.GetValues(typeof(TEnum))
                .Cast<TEnum>()
                .Select(
                    e => new SelectListItem
                    {
                        Value = Convert.ChangeType(e, Enum.GetUnderlyingType(typeof(TEnum))).ToString(),
                        Text = e.ToString(CultureInfo.CurrentCulture).SplitCamelCaseString()
                    })
                .ToList();

            if (blankValue != null)
                items.Insert(0, new SelectListItem { Value = "", Text = blankValue });

            if (value == null)
                return htmlHelper.DropDownList(name, items, htmlAttributes);

            var item = items.FirstOrDefault(i => i.Value == value.ToString());
            if (item != null)
                item.Selected = true;

            return htmlHelper.DropDownList(name, items, htmlAttributes);
        }

        /// <summary>Returns an HTML select element for each value in the enumeration that is represented by the specified expression.</summary>
        /// <returns>An HTML select element for each value in the enumeration that is represented by the expression.</returns>
        /// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name="expression">An expression that identifies the object that contains the values to display.</param>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TEnum">The type of the value.</typeparam>
        public static MvcHtmlString EnumUIDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression)
        {
            return htmlHelper.EnumUIDropDownListFor(expression, (object)null);
        }

        /// <summary>Returns an HTML select element for each value in the enumeration that is represented by the specified expression.</summary>
        /// <returns>An HTML select element for each value in the enumeration that is represented by the expression.</returns>
        /// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name="expression">An expression that identifies the object that contains the values to display.</param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes to set for the element.</param>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TEnum">The type of the value.</typeparam>
        public static MvcHtmlString EnumUIDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.EnumUIDropDownListFor(expression, null, htmlAttributes);
        }

        /// <summary>Returns an HTML select element for each value in the enumeration that is represented by the specified expression.</summary>
        /// <returns>An HTML select element for each value in the enumeration that is represented by the expression.</returns>
        /// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name="expression">An expression that identifies the object that contains the values to display.</param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes to set for the element.</param>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TEnum">The type of the value.</typeparam>
        public static MvcHtmlString EnumUIDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, object htmlAttributes)
        {
            return htmlHelper.EnumUIDropDownListFor(expression, null, htmlAttributes);
        }

        /// <summary>Returns an HTML select element for each value in the enumeration that is represented by the specified expression.</summary>
        /// <returns>An HTML select element for each value in the enumeration that is represented by the expression.</returns>
        /// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name="expression">An expression that identifies the object that contains the values to display.</param>
        /// <param name="optionLabel">The text for a default empty item. This parameter can be null.</param>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TEnum">The type of the value.</typeparam>
        public static MvcHtmlString EnumUIDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, string optionLabel)
        {
            return htmlHelper.EnumUIDropDownListFor(expression, optionLabel, null);
        }

        /// <summary>Returns an HTML select element for each value in the enumeration that is represented by the specified expression.</summary>
        /// <returns>An HTML select element for each value in the enumeration that is represented by the expression.</returns>
        /// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name="expression">An expression that identifies the object that contains the values to display.</param>
        /// <param name="optionLabel">The text for a default empty item. This parameter can be null.</param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes to set for the element.</param>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TEnum">The type of the value.</typeparam>
        public static MvcHtmlString EnumUIDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, string optionLabel, object htmlAttributes)
        {
            return htmlHelper.EnumUIDropDownListFor(expression, optionLabel, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }

        /// <summary>Returns an HTML select element for each value in the enumeration that is represented by the specified expression.</summary>
        /// <returns>An HTML select element for each value in the enumeration that is represented by the expression.</returns>
        /// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name="expression">An expression that identifies the object that contains the values to display.</param>
        /// <param name="optionLabel">The text for a default empty item. This parameter can be null.</param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes to set for the element.</param>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TEnum">The type of the value.</typeparam>
        public static MvcHtmlString EnumUIDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, string optionLabel, IDictionary<string, object> htmlAttributes)
        {
            if (expression == null)
                throw new ArgumentNullException("expression");
            ModelMetadata modelMetadata = ModelMetadata.FromLambdaExpression<TModel, TEnum>(expression, htmlHelper.ViewData);
            if (modelMetadata == null)
                throw new ArgumentException("Model not found for expression", "expression");
            if (modelMetadata.ModelType == null)
                throw new ArgumentException("Model type could not be determined", "expression");
            if (!EnumHelper.IsValidForEnumHelper(modelMetadata.ModelType))
                throw new ArgumentException(string.Format("Invalid Expression, the parameter type '{0}' is not a valid Enum", modelMetadata.ModelType.FullName), "expression");

            string expressionText = ExpressionHelper.GetExpressionText(expression);
            if (htmlAttributes.ContainsKey("name"))
                expressionText = htmlAttributes["name"].ToString();
            if (htmlAttributes.ContainsKey("Name"))
                expressionText = htmlAttributes["Name"].ToString();
            string fullHtmlFieldName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(expressionText);
            Enum @enum = null;
            if (!string.IsNullOrEmpty(fullHtmlFieldName))
            {
                ModelState modelState;
                if (htmlHelper.ViewContext.ViewData.ModelState.TryGetValue(fullHtmlFieldName, out modelState) && modelState.Value != null)
                    @enum = modelState.Value.ConvertTo(modelMetadata.ModelType, null) as Enum;
            }
            if (@enum == null && !string.IsNullOrEmpty(expressionText))
                @enum = (htmlHelper.ViewData.Eval(expressionText) as Enum);
            if (@enum == null)
                @enum = (modelMetadata.Model as Enum);

            IList<SelectListItem> selectList = EnumHelper.GetSelectList(modelMetadata.ModelType, @enum).OrderBy(i => i.Text).ToList();
            selectList.ForEach(l => l.Text = l.Text.SplitCamelCaseString());
            if (!string.IsNullOrEmpty(optionLabel) && selectList.Count != 0 && string.IsNullOrWhiteSpace(selectList[0].Text))
            {
                selectList[0].Text = optionLabel;
                optionLabel = null;
            }

            object obj = null;
            if (obj == null)
            {
                ModelState modelState;
                if (htmlHelper.ViewContext.ViewData.ModelState.TryGetValue(fullHtmlFieldName, out modelState) && modelState.Value != null)
                    obj = modelState.Value.ConvertTo(typeof(string), null);
            }
            if (obj == null && !string.IsNullOrEmpty(expressionText))
                obj = htmlHelper.ViewData.Eval(expressionText);

            StringBuilder stringBuilder = new StringBuilder();
            if (optionLabel != null)
            {
                stringBuilder.AppendLine(ListItemToOption(new SelectListItem
                {
                    Text = optionLabel,
                    Value = string.Empty,
                    Selected = false
                }));
            }
            IEnumerable<IGrouping<int, SelectListItem>> enumerable = selectList.GroupBy(delegate(SelectListItem i)
            {
                if (i.Group != null)
                {
                    return i.Group.GetHashCode();
                }
                return i.GetHashCode();
            });
            foreach (IGrouping<int, SelectListItem> current in enumerable)
            {
                SelectListGroup group = current.First<SelectListItem>().Group;
                TagBuilder _tagBuilder = null;
                if (group != null)
                {
                    _tagBuilder = new TagBuilder("optgroup");
                    if (group.Name != null)
                        _tagBuilder.MergeAttribute("label", group.Name);
                    if (group.Disabled)
                        _tagBuilder.MergeAttribute("disabled", "disabled");
                    stringBuilder.AppendLine(_tagBuilder.ToString(TagRenderMode.StartTag));
                }
                foreach (SelectListItem current2 in current)
                    stringBuilder.AppendLine(ListItemToOption(current2));
                if (group != null)
                    stringBuilder.AppendLine(_tagBuilder.ToString(TagRenderMode.EndTag));
            }

            TagBuilder tagBuilder = new TagBuilder("select");
            tagBuilder.InnerHtml = stringBuilder.ToString();
            TagBuilder tagBuilder2 = tagBuilder;
            tagBuilder2.MergeAttributes<string, object>(htmlAttributes);
            tagBuilder2.MergeAttribute("name", fullHtmlFieldName, true);
            tagBuilder2.GenerateId(fullHtmlFieldName);
            ModelState modelState2 = null;
            if (htmlHelper.ViewData.ModelState.TryGetValue(fullHtmlFieldName, out modelState2) && modelState2.Errors.Count > 0)
                tagBuilder2.AddCssClass(HtmlHelper.ValidationInputCssClassName);
            tagBuilder2.MergeAttributes<string, object>(htmlHelper.GetUnobtrusiveValidationAttributes(expressionText, modelMetadata));
            return new MvcHtmlString(tagBuilder2.ToString());
        }

        private static string ListItemToOption(SelectListItem item)
        {
            TagBuilder tagBuilder = new TagBuilder("option");
            tagBuilder.InnerHtml = HttpUtility.HtmlEncode(item.Text);
            TagBuilder tagBuilder2 = tagBuilder;
            if (item.Value != null)
                tagBuilder2.Attributes["value"] = item.Value;
            if (item.Selected)
                tagBuilder2.Attributes["selected"] = "selected";
            if (item.Disabled)
                tagBuilder2.Attributes["disabled"] = "disabled";
            return tagBuilder2.ToString(0);
        }
    }
}
