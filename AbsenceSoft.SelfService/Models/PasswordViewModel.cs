﻿using AbsenceSoft.Common.Security;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models
{
    public class PasswordViewModel:IValidatableObject
    {
        [Required, DisplayName("Current Password"), DataType(DataType.Password)]
        public string CurrentPassword { get; set; }

        [Required, DisplayName("New Password"), DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Required, DisplayName("Confirm Password"), DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            if(NewPassword != ConfirmPassword)
            {
                results.Add(new ValidationResult("Password and Confirmation do not match"));
            }

            using (var authService = new AuthenticationService())
            using (var adminService = new AdministrationService())
            {
                if (!adminService.CheckPassword(Current.User(), CurrentPassword))
                {
                    results.Add(new ValidationResult("Current password is not correct", new List<string>() { "CurrentPassword" }));
                }

                ICollection<string> errors;
                if (!authService.ValidatePasswordAgainstPolicy(Current.Customer().PasswordPolicy, Current.User(), NewPassword, out errors))
                {
                    results.Add(new ValidationResult(string.Join("\n", errors), new List<string>() { "NewPassword", "ConfirmPassword" }));
                }
            }

            return results;
        }

        public User ApplyToDataModel(User u)
        {
            u.Password = new Data.CryptoString()
            {
                PlainText = NewPassword
            };

            return u;
        }
    }
}