﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models
{
    public class TimeOffRequestViewModel : IValidatableObject
    {
        public TimeOffRequestViewModel()
        {

        }

        public TimeOffRequestViewModel(IntermittentTimeRequest timeOffRequest)
            : this()
        {
            this.DateOff = timeOffRequest.RequestDate;
            this.TimeOff = timeOffRequest.TotalTime;
            this.IsApproved = timeOffRequest.ManagerApproved;
            this.IntermittentType = timeOffRequest.IntermittentType;
        }

        public string EmployeeId { get; set; }

        public Guid Id { get; set; }
        public int ItorId { set; get; }

        public int ActiveItorId { set; get; }

        public int ItorIdCounter { set; get; }

        public string CaseId { get; set; }

        [Required(ErrorMessage = "The \"Select the date of your time off.\" field is required"), DisplayName("Select the date of your time off."), DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime? DateOff { get; set; }

        [Required(ErrorMessage = "The \"How much time off do you need?\" field is required"), DisplayName("How much time off do you need?")]
        public string TimeOff { get; set; }

        [Required(ErrorMessage = "The \"Select the reason for your time off\" field is required"), DisplayName("Select the reason for your time off")]
        public IntermittentType? IntermittentType { get; set; }

        public bool IsApproved { get; set; }

        public bool IsDelete { get; set; }

        public bool? IsOverride { get; set; }
       
        [Display(Name = "Start Time")]
        public TimeOfDay? StartTimeForLeave { get; set; }

        [Display(Name = "End Time")]
        public TimeOfDay? EndTimeForLeave { get; set; }

        public bool? IsEditMode { get; set; }

        public List<IntermittentTimeRequest> ApplyToDataModel(Case c)
        {
            List<IntermittentTimeRequest> intermittentTimeRequests = new List<IntermittentTimeRequest>();
            List<string> oldDates = new List<string>();
            List<string> errors = new List<string>();

            using (var service = new CaseService())
            {
                // check that this is a scheduled day, and by scheduled we mean that they are scheduled to work some                 
                LeaveOfAbsence los = new LeaveOfAbsence();
                los.Employee = c.Employee;
                los.Employer = c.Employee.Employer;
                los.Customer = c.Employee.Customer;
                var regularTimes = los.MaterializeSchedule(c.StartDate, c.EndDate.Value, true);

                var seg = c.Segments.Where(s => s.Type == CaseType.Intermittent && s.Status != CaseStatus.Cancelled && this.DateOff.Value.DateInRange(s.StartDate, s.EndDate)).FirstOrDefault();

                if (seg == null)
                {
                    errors.AddFormat("Invalid request, requested date is not part of an intermittent portion of this case");
                    throw new AbsenceSoftAggregateException(errors[0]);
                }
                foreach (var pol in seg.AppliedPolicies)
                {
                    if (pol.PolicyReason.IntermittentRestrictionsMinimum.HasValue && this.TimeOff.ParseFriendlyTime() > 0)
                    {
                        intermittentTimeRequests = service.GenerateIntermittentRequests(c, pol.PolicyReason, this.DateOff.Value, intermittentTimeRequests);
                    }
                    else
                    {
                        if (!oldDates.Contains(this.DateOff.Value.ToString("dd_MM_yyyy")))
                        {
                            if (seg.UserRequests.FirstOrDefault(u => u.RequestDate.Date == this.DateOff.Value.Date && u.Id == this.Id) == null)
                            {
                                intermittentTimeRequests.Add(new IntermittentTimeRequest()
                                {
                                    RequestDate = DateTime.SpecifyKind(this.DateOff.Value, DateTimeKind.Utc)
                                });
                                oldDates.Add(this.DateOff.Value.ToString("dd_MM_yyyy"));
                            }
                            else
                            {
                                intermittentTimeRequests.Add(seg.UserRequests.FirstOrDefault(u => u.RequestDate.Date == this.DateOff.Value.Date && u.Id == this.Id));                                
                                oldDates.Add(this.DateOff.Value.ToString("dd_MM_yyyy"));
                            }
                        }
                    }
                }


                foreach (var request in intermittentTimeRequests)
                {
                    if (this.DateOff != null && request.RequestDate == this.DateOff)
                    {
                        CaseSegment theSegment = GetMatchingCaseSegment();
                        request.IntermittentType = this.IntermittentType;

                        string startLeaveTimeRequest = this.StartTimeForLeave?.ToString();
                        string endLeaveTimeRequest = this.EndTimeForLeave?.ToString();

                        int totalMinutes = 0;
                        if (!string.IsNullOrWhiteSpace(startLeaveTimeRequest) && !string.IsNullOrWhiteSpace(endLeaveTimeRequest))
                        {
                            TimeSpan currentRequestSpan = DateTime.Parse(endLeaveTimeRequest).Subtract(DateTime.Parse(startLeaveTimeRequest));
                            totalMinutes = currentRequestSpan != null ? (int)currentRequestSpan.TotalMinutes : 0;
                        }
                        else
                        {
                            totalMinutes = this.ParseTimeOffString();
                        }

                        if (!this.IsOverride.HasValue && startLeaveTimeRequest != endLeaveTimeRequest) 
                        {
                            
                           // Check the overlapping time
                            DateTime startTime = DateTime.Parse($"{this.DateOff.Value.Date.ToShortDateString()} {startLeaveTimeRequest}");
                            DateTime? endTime = DateTime.Parse($"{this.DateOff.Value.Date.ToShortDateString()} {endLeaveTimeRequest}");

                            if (seg.UserRequests.Any(u => u.RequestDate.Date.CompareTo(this.DateOff.Value.Date) == 0 && DateTime.Parse($"{u.RequestDate.ToShortDateString()} {u.StartTimeForLeave}").DateInRange(startTime, endTime) ||
                                 DateTime.Parse($"{u.RequestDate.ToShortDateString()} {u.EndTimeForLeave}").DateInRange(startTime, endTime)))
                            {
                                errors.AddFormat("Time of request overlaps existing time off request: Case " + c.CaseNumber + " TOR on " + request.RequestDate.ToString("MM/dd/yyyy") + " from " + startLeaveTimeRequest + " – " + endLeaveTimeRequest);
                            }
                        }                        

                        var rt = regularTimes.FirstOrDefault(t => t.SampleDate == request.RequestDate);

                        if (rt != null && rt.TotalMinutes < totalMinutes && !this.IsOverride.HasValue)
                        {
                            errors.AddFormat("Hours requested on '{0:MM/dd/yyyy}' exceed scheduled hours, which are '{1}'", request.RequestDate, rt.TotalMinutes.Value.ToFriendlyTime());
                        }

                        if (!errors.Any())
                        {
                            request.TotalMinutes = this.ParseTimeOffString();
                            request.StartTimeForLeave = this.StartTimeForLeave;
                            request.EndTimeForLeave = this.EndTimeForLeave;

                            request.EmployeeEntered = true;
                            request.Detail = theSegment.AppliedPolicies
                                .Where(ap => ap.Status == EligibilityStatus.Eligible)
                                .Select(ap => new IntermittentTimeRequestDetail()
                                {
                                    PolicyCode = ap.Policy.Code,
                                    Pending = request.TotalMinutes
                                }).ToList();
                        }
                        else
                        {
                            errors.AddFormat("Do you want to continue? <a href =\"javascript:void(0);\" id=\"override\" class=\"btn btn-success\">Yes</a> or <a href=\"javascript:void(0);\" id=\"not_override\" class=\"btn btn-danger\">No</a>");
                            throw new AbsenceSoftAggregateException(errors.ToArray());
                        }
                    }
                }
            }       
            return intermittentTimeRequests;
        }

        /// <summary>
        /// Parses the time off string to an integer value
        /// </summary>
        /// <returns></returns>
        private int ParseTimeOffString()
        {
            int totalMinutes = 0;

            string[] parts = this.TimeOff.Split(' ');

            foreach (string part in parts)
            {
                int partTotalMinutes = 0;
                if (part.EndsWith("h"))
                {
                    int.TryParse(part.TrimEnd('h'), out partTotalMinutes);
                    partTotalMinutes *= 60;
                }
                else if (part.EndsWith("m"))
                {
                    int.TryParse(part.TrimEnd('m'), out partTotalMinutes);
                }

                totalMinutes += partTotalMinutes;
            }

            return totalMinutes;
        }

        public bool HasEmployeeWorkSchedule()
        {
            if (string.IsNullOrEmpty(this.EmployeeId))
                return false;

            using (var employeeService = new EmployeeService())
            {
                var emp = employeeService.GetEmployee(this.EmployeeId);
                return emp.WorkSchedules != null && emp.WorkSchedules.Any();
            }
        }
        /// <summary>
        /// Gets the first Open or Requested case that matches the date for this time off request
        /// </summary>
        /// <returns></returns>
        internal Case GetCase()
        {
            using (var caseService = new CaseService())
            {
                IEnumerable<Case> employeeCases = caseService
                    .GetCasesForEmployee(this.EmployeeId, CaseStatus.Open, CaseStatus.Requested);
                return employeeCases.FirstOrDefault(c =>
                    this.DateOff.Value.DateInRange(c.StartDate, c.EndDate)
                    && c.Segments.Any(s => s.Type == CaseType.Intermittent));
                
            }
        }

        /// <summary>
        /// Gets the case segment on the case that is intermittent, not cancelled and matches the selected date
        /// </summary>
        /// <param name="theCase"></param>
        /// <returns></returns>
        private CaseSegment GetMatchingCaseSegment(Case theCase = null)
        {
            if (theCase == null)
                theCase = GetCase();

            if (theCase == null)
                return null;

            return theCase.Segments
                        .FirstOrDefault(
                            s => s.Type == CaseType.Intermittent
                                && s.Status != CaseStatus.Cancelled
                                && this.DateOff.Value.DateInRange(s.StartDate, s.EndDate)
                        );
        }

        /// <summary>
        /// Validates that we have a case segment that this time off request can be applied to
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            using (var caseService = new CaseService())
            {
                CaseSegment matchingSegment = GetMatchingCaseSegment();
                if (matchingSegment == null)
                    results.Add(new ValidationResult("There is no matching intermittent case for the selected date", new List<string>() { "DateOff" }));
            }

            return results;
        }

    }

    public class IntermittentTimeOffRequestViewModelDetails
    {
        /// <summary>
        /// Gets or sets the DateOff of event.
        /// </summary> 
        public int Id { set; get; }

        public string EmployeeId { get; set; }

        public int ItorId { set; get; }

        public string CaseId { get; set; }


        /// <summary>
        /// Gets or sets the TimeOffRequestViewModel of event.
        /// </summary> 
        public IList<TimeOffRequestViewModel> IntermittentTimeOffRequestDetails { get; set; }
    }
}
