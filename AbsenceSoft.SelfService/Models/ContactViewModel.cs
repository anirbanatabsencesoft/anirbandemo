﻿using AbsenceSoft.Data.Customers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models
{
    public class ContactViewModel
    {
        public ContactViewModel()
        {

        }

        public ContactViewModel(EmployeeContact contact)
        {
            this.Id = contact.Id;
            this.RelationshipCode = contact.ContactTypeCode;
            this.RelationshipName = contact.ContactTypeName;
            this.FirstName = contact.Contact.FirstName;
            this.LastName = contact.Contact.LastName;
            this.DateOfBirth = contact.Contact.DateOfBirth;
        }

        [DisplayName("Existing Contact")]
        public string Id { get; set; }
        
        [Required(ErrorMessage="Please select Relationship"), DisplayName("Relationship")]
        public string RelationshipCode { get; set; }

        public string RelationshipName { get; set; }

        [Required(ErrorMessage = "Please select FirstName"), DisplayName("First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please select LastName"), DisplayName("Last Name")]
        public string LastName { get; set; }

        [DisplayName("Date Of Birth"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? DateOfBirth { get; set; }

        public EmployeeContact ApplyToDataModel(EmployeeContact contact, string employeeId, string customerId, string employerId)
        {
            if (contact == null)
                contact = new EmployeeContact();

            contact.Contact.FirstName = this.FirstName;
            contact.Contact.LastName = this.LastName;

            //TODO : Ganesh, This is temp. fix, the date selected by user in calendar gets reduced by 1 day upon saving, need to investigate in future.
            if (this.DateOfBirth.HasValue)
                contact.Contact.DateOfBirth = this.DateOfBirth.Value.AddDays(1);
            else
                contact.Contact.DateOfBirth = this.DateOfBirth;

            contact.ContactTypeCode = this.RelationshipCode;
            contact.ContactTypeName = this.RelationshipName;
            contact.EmployeeId = employeeId;
            contact.CustomerId = customerId;
            contact.EmployerId = employerId;

            return contact;
        }
    }
}