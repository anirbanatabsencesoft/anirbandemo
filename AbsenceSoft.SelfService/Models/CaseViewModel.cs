﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic;
using AbsenceSoft.SelfService.Models.Employees;
using AbsenceSoft.SelfService.Models.Request;
using AbsenceSoft.SelfService.Models.Cases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.SelfService.Models.Demands;
using AbsenceSoft.Data.Jobs;

namespace AbsenceSoft.SelfService.Models
{
    public class CaseViewModel
    {
        public CaseViewModel() {
            Policies = new List<PolicySummaryViewModel>();
            AlternateInfo = new PersonalInfoViewModel();
        }

        public CaseViewModel(ListResult result, bool includeEmployee = false)
            :this()
        {
            
            Id = result.Get<string>("Id");
            CaseNumber = result.Get<string>("CaseNumber");
            StartDate = result.Get<DateTime>("StartDate");
            EndDate = result.Get<DateTime?>("EndDate");
            Status = result.Get<string>("Status");
            Type = result.Get<string>("Type");
            Description = result.Get<string>("Description");
            Summary = result.Get<string>("Summary");
            Reason = result.Get<string>("ReasonName");
            EmployeeId = result.Get<string>("EmployeeId");
            ReturnToWorkDate = result.Get<DateTime?>("ReturnToWorkDate");

            if (result.Get<bool>("IsAccommodation"))
            {
                Determination = result.Get<AdjudicationStatus>("AccommodationDetermination");
            }
            else
            {
                Determination = result.Get<AdjudicationStatus>("Determination");
            }
            
            if (includeEmployee)
            {
                Employee = new EmployeeViewModel()
                {
                    FirstName = result.Get<string>("EmployeeFirstName"),
                    LastName = result.Get<string>("EmployeeLastName")
                };
            }
            
        }

        public CaseViewModel(Case theCase, bool includeEmployee = false)
            :this()
        {
            Id = theCase.Id;
            CaseNumber = theCase.CaseNumber;
            StartDate = theCase.StartDate;
            EndDate = theCase.EndDate;
            Status = theCase.GetCaseStatus().ToString();
            Type = theCase.CaseType;
            Description = theCase.Description;
            Summary = theCase.Narrative;
            Reason = theCase.Reason.Name;
            EmployeeId = theCase.Employee.Id;
            Policies = theCase.Summary.Policies.Select(p => new PolicySummaryViewModel(p));
            IsAccommodation = theCase.IsAccommodation;
           
            IsWorkRelated = theCase.Metadata.GetRawValue<bool>("IsWorkRelated");
                        
            if (theCase.IsAccommodation)
            {
                Determination = theCase.AccommodationRequest.Determination; 
            }
            else
            {
                Determination = theCase.Summary.Determination;
            }
            
            CaseEvent estimatedRTW = theCase.FindCaseEvent(CaseEventType.EstimatedReturnToWork);
            if (estimatedRTW != null)
                ReturnToWorkDate = estimatedRTW.EventDate;
            if(includeEmployee)
                Employee = new EmployeeViewModel(theCase.Employee);

            CopyContactInfo(theCase);

            WorkRelatedInfo = new WorkRelatedViewModel(theCase);
        }

        public CaseViewModel(ListResult result, bool includeEmployee = false, IEnumerable<NoteViewModel> notes = null, IEnumerable<AttachmentViewModel> attachments = null)
            : this(result, includeEmployee)
        {
            this.Notes = notes;
            this.Attachments = attachments;
        }

        private void CopyContactInfo(Case theCase)
        {
            if(theCase.Employee.Info.AltAddress != null)
            {
                AlternateInfo.Address1 = theCase.Employee.Info.AltAddress.Address1;
                AlternateInfo.Address2 = theCase.Employee.Info.AltAddress.Address2;
                AlternateInfo.City = theCase.Employee.Info.AltAddress.City;
                AlternateInfo.State = theCase.Employee.Info.AltAddress.State;
                AlternateInfo.Country = theCase.Employee.Info.AltAddress.Country;
                AlternateInfo.ZipCode = theCase.Employee.Info.AltAddress.PostalCode;
            }
            else
            {
                AlternateInfo.Address1 = theCase.Employee.Info.Address.Address1;
                AlternateInfo.Address2 = theCase.Employee.Info.Address.Address2;
                AlternateInfo.City = theCase.Employee.Info.Address.City;
                AlternateInfo.State = theCase.Employee.Info.Address.State;
                AlternateInfo.Country = theCase.Employee.Info.Address.Country;
                AlternateInfo.ZipCode = theCase.Employee.Info.Address.PostalCode;
            }

            if(theCase.Employee.Info.AltPhone != null)
                AlternateInfo.PhoneNumber = theCase.Employee.Info.AltPhone.FormatPhone();

            AlternateInfo.EmailAddress = theCase.Employee.Info.AltEmail;
        }

        public string Id { get; set; }
        public string EmployeeId { get; set; }

        [DisplayName("Case Number")]
        public string CaseNumber { get; set; }
        public string Reason { get; set; }

        [DisplayName("Request Start"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate { get; set; }

        [DisplayName("Request End"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? EndDate { get; set; }

        [DisplayName("Return To Work"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? ReturnToWorkDate { get; set; }
        public string Status { get; set; }
        public AdjudicationStatus Determination { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string Summary { get; set; }
        public bool IsAccommodation { get; set; }
        public EmployeeViewModel Employee { get; set; }
        public PersonalInfoViewModel AlternateInfo { get; set; }

        public IEnumerable<PolicySummaryViewModel> Policies {get; set;}
        public IEnumerable<NoteViewModel> Notes { get; set; }
        public IEnumerable<AttachmentViewModel> Attachments { get; set; }

        public WorkRelatedViewModel WorkRelatedInfo { get; set; }

        public bool IsWorkRelated { get; set; }
        public bool WorkRestrictionFeatureEnabled { get; set; }

        public List<WorkRestrictionViewModel> WorkRestrictions { get; set; }
        public List<EmployeeNecessity> EmployeeNecessities { get; set; }
        public CaseStatus CaseStatus { get; set; }
       

        [DisplayName("Primary Assignee")]
        public string AssignedToName { get; set; }

        [DisplayName("Relationship")]
        public string ContactTypeName { get; set; }

        [DisplayName("DoB"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? DateOfBirth { get; set; }
        public bool IsESS { get; set; }
        public IEnumerable<CustomFieldViewModel> CustomFields { get; set; }
        public IEnumerable<CustomFieldViewModel> EmployeeCustomFields { get; set; }
        public List<CustomField> CustomFieldsConfiguration { get; set; }
        public string ContactId { get; set; }
    }
}