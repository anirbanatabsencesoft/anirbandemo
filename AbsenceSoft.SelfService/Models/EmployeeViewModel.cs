﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.SelfService.Models.Employees;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models
{
    public class EmployeeViewModel
    {
        public EmployeeViewModel()
        {
            this.Policies = new List<PolicySummaryViewModel>();
            this.Schedule = new ScheduleViewModel();
        }

        public EmployeeViewModel(Employee employee)
            : this()
        {
            if (employee == null)
                return;
            this.Id = employee.Id;
            this.EmployeeNumber = employee.EmployeeNumber;
            this.FirstName = employee.FirstName;
            this.LastName = employee.LastName;
            this.HireDate = employee.HireDate;
            this.HoursWorked = LeaveOfAbsence.TotalHoursWorkedLast12Months(employee);
            this.JobTitle = employee.JobTitle;
            this.EmployerId = employee.EmployerId;
            if (employee.WorkSchedules.Any())
            {
                var workSchedule = employee.WorkSchedules.OrderByDescending(p => p.StartDate).First();
                Schedule = new ScheduleViewModel(Id, workSchedule);
            }
            else
            {
                Schedule.BuildTimesLists();
                Schedule.EmployeeId = employee.Id;
            }
        }

        public EmployeeViewModel(ListResult r)
            : this()
        {
            this.Id = r.Get<string>("Id");
            this.FirstName = r.Get<string>("FirstName");
            this.LastName = r.Get<string>("LastName");
            this.EmployeeNumber = r.Get<string>("EmployeeNumber");
            this.HireDate = r.Get<DateTime?>("HireDate");
            this.OpenCasesCount = GetOpenCasesCountByCasesInfo(r);
        }

        private int GetOpenCasesCountByCasesInfo(ListResult r)
        {
            int openCasesCount = 0;
            var EmployeeCaseInfoList = r.Get<List<EmployeeCaseInfo>>("EmployeeCasesInfo");
            if (EmployeeCaseInfoList != null)
            {
                openCasesCount = EmployeeCaseInfoList.Count(c => c.CaseStatus == (int)CaseStatus.Open || c.CaseStatus == (int)CaseStatus.Requested);
            }
            return openCasesCount;
        }

        public string Id { get; set; }

        [DisplayName("Employee ID")]
        public string EmployeeNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public bool  HasCase { get; set; }
        private string _displayName { get; set; }

        public string  EmployerId { get; set; }

        public string DisplayName
        {
            get
            {
                if (string.IsNullOrEmpty(_displayName))
                    return string.Format("{0} {1} ({2})", this.FirstName, this.LastName, this.EmployeeNumber);

                return _displayName;
            }
        }

        public bool HasIntermittentCase()
        {
            if (string.IsNullOrEmpty(this.Id))
            {
                return false;
            }

            using (var caseService = new CaseService())
            {
                return caseService.EmployeeHasOpenIntermittentCase(this.Id);
            }
        }

        public bool HasWorkSchedule()
        {
            if (string.IsNullOrEmpty(this.Id))
            {
                return false;
            }

            using (var employeeService = new EmployeeService())
            {
                var emp = employeeService.GetEmployee(this.Id);
                return emp.WorkSchedules != null && emp.WorkSchedules.Any();
            }
        }

        public bool TimeOffCanBeRequested()
        {
            if (!CanCurrentUserCreateTimeOffRequest())
            {
                return false;
            }

            return HasIntermittentCase() && HasWorkSchedule();
        }

        public bool HasTimeOffRequests()
        {
            if (string.IsNullOrEmpty(this.Id))
                return false;

            using (var caseService = new CaseService())
            {
                return caseService.GetCasesForEmployee(this.Id, CaseStatus.Open, CaseStatus.Requested)
                    .SelectMany(c => c.Segments)
                    .Any(c => c.UserRequests.Count > 0);
            }
        }

        public bool CanCurrentUserCreateTimeOffRequest()
        {
            return User.Permissions.ProjectedPermissions.Contains("AddITOR");
        }
        public bool CanCurrentUserCreateCase()
        {
            return User.Permissions.ProjectedPermissions.Contains("CreateCase");
        }
        

        public string JobTitle { get; set; }

        [DisplayName("Hired"), DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime? HireDate { get; set; }

        [DisplayName("Hours Worked"), DisplayFormat(DataFormatString = "{0:0.0}")]
        public double HoursWorked { get; set; }

        public IEnumerable<PolicySummaryViewModel> Policies { get; set; }

        public bool ShowMyTeam { get; set; }

        public int OpenCasesCount { get; set; }

        [UIHint("ScheduleEditor"), Required]
        public ScheduleViewModel Schedule { get; set; }
    }
}