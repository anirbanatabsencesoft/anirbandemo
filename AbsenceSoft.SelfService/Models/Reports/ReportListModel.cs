﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbsenceSoft.Reporting;

namespace AbsenceSoft.SelfService.Models.Reports
{
    public class ReportListModel
    {
        public string Title { get; set;}
        public string Category { get; set; }
        public List<ReportListModel> ChildReportList { get; set; }
        public BaseReport Report { get; set; }
        
        #region Derived properties
        public bool HasChildren
        {
            get
            {
                if (ChildReportList != null && ChildReportList.Count > 0)
                    return true;
                return false;
            }
        }

        public string Id
        {
            get { return this.Title.Replace(" ", "_") + "_" + Category.Replace(" ", "_") ; }
        }

        #endregion
    }
}