﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Reporting;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace AbsenceSoft.SelfService.Models.Reports
{
    public class ReportViewModel
    {
        public ReportViewModel()
        {

        }

        public ReportViewModel(BaseReport baseReport, ReportCriteria criteria, ReportResult results)
            : this()
        {
            this.ReportId = baseReport.Id;
            this.BaseReport = baseReport;
            this.Results = results;
            this.Criteria = criteria;
        }

        public ReportViewModel(BaseReport baseReport)
            : this(baseReport, null, null)
        {

        }

        public ReportViewModel(BaseReport baseReport, ReportCriteria criteria)
            : this(baseReport, criteria, null)
        {

        }


        public bool IsReportRun { get; set; }

        public bool IsChart
        {
            get
            {
                return this.BaseReport != null && this.BaseReport.Chart != null;
            }
        }

        public bool HasReportData
        {
            get
            {
                return this.IsReportRun && !this.IsChart && this.Results != null &&  /// Report has to be run, not a chart and the results populated
                    ((this.Results.Items != null && this.Results.Items.Count > 0) ||  /// The report has to have items
                    this.Results.Groups != null && this.Results.Groups.Count > 0);  /// or the report has to have groups
            }
        }

        public string ReportId { get; set; }
        public BaseReport BaseReport { get; set; }
        public ReportResult Results { get; set; }
        public ReportCriteria Criteria { get; set; }



        internal ReportCriteria ApplyToDataModel()
        {
            ReportCriteria baseCriteria = new ReportService().GetCriteria(this.ReportId, User.Current);
            baseCriteria.StartDate = this.Criteria.StartDate;
            baseCriteria.EndDate = this.Criteria.EndDate;
            baseCriteria.AgeFrom = this.Criteria.AgeFrom;
            baseCriteria.AgeTo = this.Criteria.AgeTo;

            if (this.Criteria != null && this.Criteria.Filters != null)
            {
                foreach (var filter in this.Criteria.Filters)
                {
                    /// Use Name instead of Id because these aren't saved, so the Ids will be randomly generated each time
                    ReportCriteriaItem item = baseCriteria.Filters.FirstOrDefault(f => f.Name == filter.Name);
                    if (item != null)
                    {
                        if (item.ControlType != ControlType.CheckBoxList)
                        {
                            item.Value = ParseFilterValue(filter.Value);
                        }
                        else
                        {
                            item.Values = filter.Values;
                        }
                    }

                }
            }

            return baseCriteria;
        }

        private object ParseFilterValue(object filterValue)
        {
            object value = null;
            if (filterValue is Array && ((Array)filterValue).Length > 0)
            {
                /// MVC is binding as an array
                /// So we grab the first element
                value = ((Array)filterValue).GetValue(0);

                /// If it's not null, but can be turned into an empty string
                /// set value to null
                /// The || value == null is so we don't run into issues calling value.ToString() in the else
                if ((value != null && (value.ToString() == "" || value.ToString() == "null")) || value == null)
                    return null;
                
                string stringValue = value.ToString();
                int intValue = 0;
                DateTime dateTimeValue = DateTime.Now;
                bool boolValue = false;
                /// lets check if it can be parsed into any of the aforementioned value types
                /// or If it's a BsonId leave it alone
                var bsonIdRegex = new Regex(@"[0-9a-fA-F]{24}");
                if (bsonIdRegex.IsMatch(stringValue))
                    return stringValue;
                /// Ints have next priority
                
                if (int.TryParse(stringValue, out intValue))
                    return intValue;
                
                /// then date times
                if (DateTime.TryParse(stringValue, out dateTimeValue))
                    return dateTimeValue;

                /// then bools
                if (bool.TryParse(stringValue, out boolValue))
                    return boolValue;

                return stringValue;
            }
            return value;
        }
    }
}