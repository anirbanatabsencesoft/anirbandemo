﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models.Reports
{
    public class ESSReportModel
    {
        public List<ReportListModel> TeamReports { get; set; }
        public List<ReportListModel> OrganizationReports { get; set; }
    }
}