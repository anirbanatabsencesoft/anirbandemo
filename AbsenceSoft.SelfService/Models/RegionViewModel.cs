﻿using AbsenceSoft.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models
{
    public class RegionViewModel
    {
        public RegionViewModel()
        {

        }

        public RegionViewModel(Region region)
        {
            if (region == null)
                return;

            Name = region.Name;
            Code = region.Code;
        }

        public string Name { get; set; }
        public string Code { get; set; }
    }
}