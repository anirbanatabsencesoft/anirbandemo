﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Common;
using System.ComponentModel;
using AbsenceSoft.Data.Customers;
using System.Text;

namespace AbsenceSoft.SelfService.Models.Contacts
{
    public class CreateContactViewModel
    {
        public CreateContactViewModel()
        {
            Address = new Address();
        }
        public CreateContactViewModel(EmployeeContact contact) : this()
        {
            if (contact == null)
                return;

            Id = contact.EmployeeId;
            EmployeeContactId = contact.Id;
            ContactTypeCode = contact.ContactTypeCode;
            ContactTypeName = contact.ContactTypeName;
            MilitaryStatus = contact.MilitaryStatus;
            RelatedEmployeeNumber = contact.RelatedEmployeeNumber;

            if (contact.Contact != null)
            {
                CompanyName = contact.Contact.CompanyName;
                CellPhone = contact.Contact.CellPhone;
                FirstName = contact.Contact.FirstName;
                LastName = contact.Contact.LastName;
                Title = contact.Contact.Title;
                MiddleName = contact.Contact.MiddleName;
                HomePhone = contact.Contact.HomePhone;
                WorkPhone = contact.Contact.WorkPhone;
                Email = contact.Contact.Email;
                Address = contact.Contact.Address;
                IsPrimary = contact.Contact.IsPrimary.HasValue && contact.Contact.IsPrimary.Value;
                ContactPersonName = contact.Contact.Metadata.GetRawValue<string>("ContactPersonName");
                ContactDateOfBirth = contact.Contact.DateOfBirth;
            }
        }
        
        public EmployeeContact ApplyModel(EmployeeContact contact)
        {
            if (contact == null)
                contact = new EmployeeContact();

            contact.EmployeeId = Id;
            contact.Id = EmployeeContactId;
            contact.ContactTypeCode = ContactTypeCode;
            contact.ContactTypeName = ContactTypeName;
            contact.MilitaryStatus = MilitaryStatus ?? AbsenceSoft.Data.Enums.MilitaryStatus.Civilian;
            contact.RelatedEmployeeNumber = RelatedEmployeeNumber;
            CopyContactInformation(contact);
            return contact;
        }

        private void CopyContactInformation(EmployeeContact contact)
        {
            contact.Contact = new Contact()
            {
                CompanyName = CompanyName,
                CellPhone = CellPhone,
                FirstName = FirstName,
                LastName = LastName,
                Title = Title,
                MiddleName = MiddleName,
                HomePhone = HomePhone,
                WorkPhone = WorkPhone,
                Email = Email,
                Address = Address ?? new Address(),
                IsPrimary = IsPrimary,
                DateOfBirth = ContactDateOfBirth
            };

            contact.Contact.Metadata.SetRawValue("ContactPersonName", ContactPersonName);
        }

        public string Id { get; set; }
        public string EmployeeContactId { get; set; }
        public string CompanyName { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string WorkPhone { get; set; }
        public string HomePhone { get; set; }
        public string CellPhone { get; set; }
        public string Email { get; set; }
        public bool IsPrimary { get; set; }
        public string ContactPersonName { get; set; }
        public string RelatedEmployeeNumber { get; set; }

        [DisplayName("Relationship")]
        [AngularRequired]
        [AngularInputType(HTML5InputType.Select)]
        public string ContactTypeCode { get; set; }
        public string ContactTypeName { get; set; }

        public Address Address { get; set; }

        [DisplayName("Military Status")]
        [AngularInputType(HTML5InputType.Select)]
        public MilitaryStatus? MilitaryStatus { get; set; }

        public DateTime? ContactDateOfBirth { get; set; }

        /// <summary>
        /// Gets the full name.
        /// </summary>
        /// <value>
        /// The full name.
        /// </value>
        public string FullName
        {
            get
            {
                StringBuilder name = new StringBuilder();
                if (!string.IsNullOrWhiteSpace(LastName))
                    name.Append(LastName);
                if (!string.IsNullOrWhiteSpace(FirstName))
                    name.AppendFormat("{0}{1}", name.Length > 0 ? ", " : "", FirstName);
                if (!string.IsNullOrWhiteSpace(MiddleName))
                    name.AppendFormat("{0}{1}", name.Length > 0 ? ", " : "", MiddleName);
                return name.ToString();
            }
        }
    }
}