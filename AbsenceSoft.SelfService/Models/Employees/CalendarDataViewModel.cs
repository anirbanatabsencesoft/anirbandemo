﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models.Employees
{
    public class CalendarDataViewModel
    {
        public CalendarDataViewModel()
        {
            this.Policies = new List<PolicyDetailViewModel>();
        }

        public CalendarDataViewModel(DateTime date)
            :this()
        {
            Date = date;
        }

        public CalendarDataViewModel(DateTime date, string caseNumber, string reason)
            :this(date)
        {
            CaseNumber = caseNumber;
            Reason = reason;
        }

        public CalendarDataViewModel(DateTime date, string holidayName)
            :this(date)
        {
            HolidayName = holidayName;
            IsHoliday = true;
        }
        public string EmployeeId { get; set; }
        public bool AllowSelection { get; set; }
        public bool IsHoliday { get; set; }
        public string HolidayName { get; set; }
        public DateTime? Date { get; set; }
        public string Reason { get; set; }
        public string CaseNumber { get; set; }
        public IEnumerable<PolicyDetailViewModel> Policies { get; set; }
    }
}