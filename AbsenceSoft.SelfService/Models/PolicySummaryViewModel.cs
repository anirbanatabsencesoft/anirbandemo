﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models
{
    public class PolicySummaryViewModel
    {
        public PolicySummaryViewModel()
        {

        }

        public PolicySummaryViewModel(PolicySummary summary)
            :this()
        {
            this.PolicyName = summary.PolicyName;
            this.Units = summary.Units;
            this.TimeUsed = summary.TimeUsed;
            this.TimeRemaining = summary.TimeRemaining;
            this.HoursUsed = summary.HoursUsed;
            this.HoursRemaining = summary.HoursRemaining;
        }

        public PolicySummaryViewModel(CaseSummaryPolicy summary)
            :this()
        {
            this.PolicyName = summary.PolicyName;
            this.StartDate = summary.Detail[0].StartDate;
            this.EndDate = summary.Detail[0].EndDate;
        }
        
        public string PolicyName { get; set; }
        public Unit Units { get; set; }
        public double TimeUsed { get; set; }
        public double TimeRemaining { get; set; }
        public double TotalTime
        {
            get
            {
                return this.TimeUsed + this.TimeRemaining;
            }
        }
        public string HoursUsed { get; set; }
        public string HoursRemaining { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

    }
}