﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.SelfService.Models.Employees;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.SelfService.Models
{
    public class ScheduleViewModel
    {
        private DateTime currentTime = DateTime.Now.ToMidnight();
        public ScheduleViewModel()
        {
            this.Times = new List<TimeViewModel>();
            this.ActualTimes = new List<TimeViewModel>();
            this.TemporaryActualTimes = new List<TimeViewModel>();
            this.TemporaryVariableScheduleWeeklyTimes = new List<TimeViewModel>();
            this.ScheduleType = Data.Enums.ScheduleType.Weekly;
            this.FTWeeklyWorkTime = (Employer.Current == null ? 2400 : Employer.Current.FTWeeklyWorkHours.HasValue ? ((int)Employer.Current.FTWeeklyWorkHours.Value) * 60 : 2400).ToFriendlyTime();
        }

        public ScheduleViewModel(string employeeId, Schedule schedule)
            : this()
        {
            ErrorMessage = "";
            this.EmployeeId = employeeId;
            if (schedule != null)
            {
                this.ScheduleType = schedule.ScheduleType;
                this.StartDate = schedule.StartDate;
                this.EndDate = schedule.EndDate;
                this.Times = schedule.Times.Select(t => new TimeViewModel()
                {
                    TotalMinutes = t.TotalMinutes.HasValue ? t.TotalMinutes.Value.ToFriendlyTime() : null,
                    SampleDate = t.SampleDate
                }).ToList();
                this.DaysInRotation = this.Times.Count;
                this.VariableTimeMonth = schedule.StartDate;

                if (schedule.ScheduleType == Data.Enums.ScheduleType.Variable)
                    BuildVariableSchedule();

                this.FteWeeklyDuration = schedule.FteWeeklyDuration;
                if (schedule.FteWeeklyDuration == FteWeeklyDuration.FteTimePerWeek)
                {
                    this.FteAvgTimePerWeek = schedule.FteMinutesPerWeek.ToFriendlyTime();
                }
                else if (schedule.FteWeeklyDuration == FteWeeklyDuration.FtePercentage)
                {
                    this.FteTimePercentage = schedule.FteTimePercentage.ToString();
                }
                else
                {
                    this.FTWeeklyWorkTime = (Employer.Current == null ? 2400 : Employer.Current.FTWeeklyWorkHours.HasValue ? ((int)Employer.Current.FTWeeklyWorkHours.Value) * 60 : 2400).ToFriendlyTime();
                }

            }

        }

        public ScheduleViewModel(string employeeId)
            : this(employeeId, null)
        {

        }

        public ScheduleViewModel(Schedule schedule)
            : this(null, schedule)
        {

        }



        [Display(Name = "WORK SCHEDULE"), UIHint("EnumButtons"), Required]
        public ScheduleType ScheduleType { get; set; }

        [Display(Name = "Start Date"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate { get; set; }

        [Display(Name = "End Date"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? EndDate { get; set; }

        [Display(Name = "Days In Rotation")]
        public int? DaysInRotation { get; set; }

        public string EmployeeId { get; set; }
        public string TemporaryActualScheduleValuesInString { get; set; }
        public string TemporaryActualScheduleWorkingHoursInString { get; set; }

        public List<TimeViewModel> TemporaryVariableScheduleWeeklyTimes { get; set; }
        public List<TimeViewModel> TemporaryActualTimes { get; set; }

        public DateTime? VariableTimeMonth { get; set; }

        public List<TimeViewModel> Times { get; set; }

        public List<TimeViewModel> ActualTimes { get; set; }

        [Display(Name = "FTE Time Per Week")]
        public string FTWeeklyWorkTime { get; set; }

        [Display(Name = "FTE Avg Time Per Week")]
        public string FteAvgTimePerWeek { get; set; }

        [Display(Name = "FTE %")]
        public string FteTimePercentage { get; set; }

        [Display(Name = "FTE Weekly Duration"), UIHint("EnumButtons"), Required]
        public FteWeeklyDuration FteWeeklyDuration { get; set; }

        public string ErrorMessage { get; set; }
        public string GetWeeklyWorkTime()
        {
            if (this.FteWeeklyDuration == FteWeeklyDuration.FtePercentage && !String.IsNullOrWhiteSpace(this.FTWeeklyWorkTime))
            {
                decimal percentage;
                int duration;
                duration = this.FTWeeklyWorkTime.ParseFriendlyTime();
                Decimal.TryParse(this.FteTimePercentage, out percentage);
                double totalMinutes = (double)((percentage / 100) * duration);

                string hourMinutes = getHourMinutes(totalMinutes);
                return hourMinutes;
            }
            return "0";
        }
        private string getHourMinutes(double time)
        {
            int minutes = Convert.ToInt32(time);
            int hour = 0;
            while (minutes > 60)
            {
                hour = hour + minutes / 60;
                minutes = minutes % 60;
            }
            if(minutes == 0)
            {
                return string.Format("{0}h", hour); ;
            }
            return string.Format("{0}h {1}m", hour, minutes); ;
        }
        /// <summary>
        /// Applies the UI data to the database model
        /// </summary>
        /// <param name="schedule"></param>
        /// <returns></returns>
        public Schedule ApplyToDataModel(Schedule schedule)
        {
            if (schedule == null)
                schedule = new Schedule();
            schedule.ScheduleType = this.ScheduleType;
            schedule.StartDate = this.StartDate ?? currentTime;
            schedule.EndDate = this.EndDate;
            schedule.Times = this.Times.Select(t => new Time()
            {
                TotalMinutes = !string.IsNullOrEmpty(t.TotalMinutes) ? t.TotalMinutes.ParseFriendlyTime() : 0,
                SampleDate = t.SampleDate
            }).ToList();

            schedule.FteWeeklyDuration = this.FteWeeklyDuration;
            int duration;

            if (this.FteWeeklyDuration == FteWeeklyDuration.FteTimePerWeek && !String.IsNullOrWhiteSpace(this.FteAvgTimePerWeek))
            {
                duration = this.FteAvgTimePerWeek.ParseFriendlyTime();
                schedule.FteMinutesPerWeek = duration;
            }
            else if (this.FteWeeklyDuration == FteWeeklyDuration.FtePercentage && !String.IsNullOrWhiteSpace(this.FTWeeklyWorkTime))
            {
                decimal percentage;
                duration = this.FTWeeklyWorkTime.ParseFriendlyTime();
                Decimal.TryParse(this.FteTimePercentage, out percentage);
                schedule.FteMinutesPerWeek = (int)((percentage / 100) * duration);
                schedule.FteTimePercentage = percentage;
            }
            else
            {
                duration = this.FTWeeklyWorkTime.ParseFriendlyTime();
                schedule.FteMinutesPerWeek = duration;
                schedule.FteWeeklyDuration = FteWeeklyDuration.FteTimePerWeek;
            }

            if (schedule.ScheduleType == ScheduleType.FteVariable && schedule.FteMinutesPerWeek > 10080)
            {
                throw new AbsenceSoftException("Employees work hours in a week should not be more than 168 Hours.");
            }

            return schedule;
        }


        /// <summary>
        /// Takes a list of variable schedule times from the database and updates the actual time worked for that date or adds it to the list if we don't have a date
        /// </summary>
        /// <param name="actualTimes"></param>
        /// <returns></returns>
        public List<VariableScheduleTime> ApplyActualTimesToDataModel(List<VariableScheduleTime> actualTimes)
        {
            foreach (var time in this.ActualTimes)
            {
                var matchingDate = actualTimes.FirstOrDefault(t => t.Time.SampleDate == time.SampleDate);
                int totalMinutes = string.IsNullOrEmpty(time.TotalMinutes) ? 0 : time.TotalMinutes.ParseFriendlyTime();

                if (matchingDate == null)
                {
                    actualTimes.Add(new VariableScheduleTime()
                    {
                        Time = new Time()
                        {
                            SampleDate = time.SampleDate,
                            TotalMinutes = totalMinutes
                        }
                    });
                }
                else
                {
                    matchingDate.Time.TotalMinutes = totalMinutes;
                }
            }

            return actualTimes;
        }

        /// <summary>
        /// Builds an empty schedule based on the type
        /// </summary>
        public void BuildTimesLists()
        {
            /// Set the number of days for the the UI
            int numberOfDatesToAdd = 7;
            if (this.DaysInRotation.HasValue && this.ScheduleType == Data.Enums.ScheduleType.Rotating)
            {
                this.DaysInRotation = Math.Min(this.DaysInRotation.Value, 90); /// 3 month rotation max sounds reasonable
                numberOfDatesToAdd = this.DaysInRotation.Value;
            }

            /// Set what day we're starting - if it's any schedule but rotating, it should be sunday
            DateTime startDate = this.StartDate.HasValue ? this.StartDate.Value : DateTime.Now.ToMidnight();
            if (this.ScheduleType != ScheduleType.Rotating)
                startDate = startDate.GetFirstDayOfWeek();

            /// loop through and build the list of times for the UI
            for (int i = 0; i < numberOfDatesToAdd; i++)
            {
                this.Times.Add(new TimeViewModel()
                {
                    SampleDate = startDate
                });
                startDate = startDate.AddDays(1);
            }

            if (this.ScheduleType == ScheduleType.Variable)
                BuildVariableSchedule();

        }

        /// <summary>
        /// Builds a variable schedule based on 
        /// </summary>
        /// <param name="variableScheduleMonth"></param>
        /// <param name="employeeId"></param>
        private void BuildVariableSchedule()
        {
            this.VariableTimeMonth = this.VariableTimeMonth ?? this.StartDate ?? currentTime;

            /// If we don't know who the employee we're building the data for is, we just need to build an employe list for all the times
            List<DateTime> daysInMonth = this.VariableTimeMonth.Value.AllDatesInMonth().ToList();
            if (string.IsNullOrEmpty(this.EmployeeId))
            {
                this.ActualTimes = daysInMonth.Select(d => new TimeViewModel()
                {
                    SampleDate = d,
                }).ToList();
            }
            else
            {
                /// Otherwise we need to load up the data for the employee and populate the values they saved
                using (var employeeService = new EmployeeService())
                {
                    List<VariableScheduleTime> savedTime = employeeService.GetEmployeeVariableSchedule(this.EmployeeId, daysInMonth.Min(), daysInMonth.Max());
                    foreach (var day in daysInMonth)
                    {
                        string actualWorkedTime = null;
                        var matchingTime = savedTime.FirstOrDefault(t => t.Time.SampleDate == day);
                        var temporaryActualTimes = this.TemporaryActualTimes.FirstOrDefault(a => a.SampleDate == day);
                        if (matchingTime != null && matchingTime.Time.TotalMinutes.HasValue)
                        {
                            actualWorkedTime = matchingTime.Time.TotalMinutes.Value.ToFriendlyTime();
                        }
                        if (temporaryActualTimes != null && temporaryActualTimes.TotalMinutes != null)
                        {
                            actualWorkedTime = temporaryActualTimes.TotalMinutes;
                        }
                        this.ActualTimes.Add(new TimeViewModel()
                        {
                            TotalMinutes = actualWorkedTime,
                            SampleDate = day
                        });
                    }

                    DateTime startDate = this.StartDate.HasValue ? this.StartDate.Value : DateTime.Now.ToMidnight();
                    startDate = startDate.GetFirstDayOfWeek();
                    for (int i = 0; i < 7; i++)
                    {
                        var tempVarableWeeklyHours = this.TemporaryVariableScheduleWeeklyTimes.Where(a => a.SampleDate == startDate).Select(c => c.TotalMinutes).FirstOrDefault();
                        if (tempVarableWeeklyHours != null)
                        {
                            this.Times.Where(c => c.SampleDate == startDate).ForEach(c => c.TotalMinutes = tempVarableWeeklyHours);
                        }

                        startDate.AddDays(1);
                    }
                }
            }
        }
    }
}
