﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.SelfService.Models.Contacts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.SelfService.Models.Cases
{
    public class WorkRelatedViewModel : IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkRelatedViewModel"/> class.
        /// </summary>
        public WorkRelatedViewModel()
        {
            Reportable = false;
            ProviderContact = new CreateContactViewModel();
            SharpsInfo = new WorkRelatedSharpsViewModel();
            TimeEmployeeBeganWork = new TimeOfDayViewModel();
            TimeOfEvent = new TimeOfDayViewModel();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkRelatedViewModel"/> class.
        /// </summary>
        /// <param name="c">The c.</param>
        public WorkRelatedViewModel(Case c) : this()
        {
            if (c == null || c.WorkRelated == null)
                return;

            var info = c.WorkRelated;
            this.CaseId = c.Id;
            this.WorkRelated = info;
            this.IllnessOrInjuryDate = info.IllnessOrInjuryDate ?? c.GetMostRecentEventDate(CaseEventType.IllnessOrInjuryDate);
            this.Reportable = info.Reportable;
            this.HealthCarePrivate = info.HealthCarePrivate;
            this.WhereOccurred = info.WhereOccurred;
            this.InjuryLocation = info.InjuryLocation;
            this.InjuredBodyPart = info.InjuredBodyPart;
            this.Classification = info.Classification;
            this.TypeOfInjury = info.TypeOfInjury;
            this.DaysAwayFromWork = info.DaysAwayFromWork;
            this.DaysOnJobTransferOrRestriction = info.DaysOnJobTransferOrRestriction;
            this.ProviderContact = new CreateContactViewModel(info.Provider);
            this.EmergencyRoom = info.EmergencyRoom;
            this.HospitalizedOvernight = info.HospitalizedOvernight;
            this.TimeEmployeeBeganWork = new TimeOfDayViewModel(info.TimeEmployeeBeganWork);
            this.TimeOfEvent = new TimeOfDayViewModel(info.TimeOfEvent);
            this.ActivityBeforeIncident = info.ActivityBeforeIncident;
            this.WhatHappened = info.WhatHappened;
            this.InjuryOrIllness = info.InjuryOrIllness;
            this.WhatHarmedTheEmployee = info.WhatHarmedTheEmployee;
            this.DateOfDeath = info.DateOfDeath;
            this.Sharps = info.Sharps;
            this.SharpsInfo = new WorkRelatedSharpsViewModel(info.SharpsInfo);
        }

        /// <summary>
        /// Applies the model.
        /// </summary>
        /// <param name="c">The c.</param>
        /// <returns></returns>
        public WorkRelatedInfo ApplyModel(Case c)
        {
            WorkRelatedInfo info = null;
            if (c != null && c.WorkRelated != null)
                info = c.WorkRelated;
            if (info == null)
                info = new WorkRelatedInfo();

            info.IllnessOrInjuryDate = this.IllnessOrInjuryDate;
            if (c != null && this.IllnessOrInjuryDate.HasValue)
                c.SetCaseEvent(CaseEventType.IllnessOrInjuryDate, this.IllnessOrInjuryDate.Value);
            else if (c != null)
                info.IllnessOrInjuryDate = c.GetMostRecentEventDate(CaseEventType.IllnessOrInjuryDate);
            info.Reportable = this.Reportable;
            info.HealthCarePrivate = this.HealthCarePrivate;
            info.WhereOccurred = this.WhereOccurred;
            info.InjuryLocation = this.InjuryLocation;
            info.InjuredBodyPart = this.InjuredBodyPart;
            info.Classification = this.Classification;
            info.TypeOfInjury = this.TypeOfInjury;
            info.DaysAwayFromWork = this.DaysAwayFromWork;
            info.DaysOnJobTransferOrRestriction = this.DaysOnJobTransferOrRestriction;
            info.Provider = this.ProviderContact == null ? info.Provider : this.ProviderContact.ApplyModel(info.Provider);
            info.EmergencyRoom = this.EmergencyRoom;
            info.HospitalizedOvernight = this.HospitalizedOvernight;
            info.TimeEmployeeBeganWork = this.TimeEmployeeBeganWork == null ? info.TimeEmployeeBeganWork : this.TimeEmployeeBeganWork.ApplyModel(info.TimeEmployeeBeganWork);
            info.TimeOfEvent = this.TimeOfEvent == null ? info.TimeOfEvent : this.TimeOfEvent.ApplyModel(info.TimeOfEvent);
            info.ActivityBeforeIncident = this.ActivityBeforeIncident;
            info.WhatHappened = this.WhatHappened;
            info.InjuryOrIllness = this.InjuryOrIllness;
            info.WhatHarmedTheEmployee = this.WhatHarmedTheEmployee;
            info.DateOfDeath = this.DateOfDeath;
            info.Sharps = this.Sharps;
            if (info.Sharps == true)
                info.SharpsInfo = this.SharpsInfo == null ? info.SharpsInfo : this.SharpsInfo.ApplyModel(info.SharpsInfo);
            else
                info.SharpsInfo = null;

            return info;
        }

        /// <summary>
        /// Gets or sets the case identifier.
        /// </summary>
        /// <value>
        /// The case identifier.
        /// </value>
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="WorkRelatedViewModel" /> is reportable.
        /// </summary>
        /// <value>
        ///   <c>true</c> if reportable; otherwise, <c>false</c>.
        /// </value>
        [DisplayName("Reportable (OSHA)")]
        public bool Reportable { get; set; }

        /// <summary>
        /// Gets or sets the health care private.
        /// </summary>
        /// <value>
        /// The health care private.
        /// </value>
        [DisplayName("HealthCare Private")]
        public bool? HealthCarePrivate { get; set; }

        /// <summary>
        /// Gets or sets the illness or injury date.
        /// </summary>
        /// <value>
        /// The illness or injury date.
        /// </value>
        [DisplayName("Date of Injury/Illness"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? IllnessOrInjuryDate { get; set; }

        /// <summary>
        /// Gets or sets the where occurred.
        /// </summary>
        /// <value>
        /// The where occurred.
        /// </value>
        [DisplayName("Where the event occurred?")]
        public string WhereOccurred { get; set; }

        /// <summary>
        /// Gets or sets the injury office location org code.
        /// </summary>
        /// <value>
        /// The injury location org code.
        /// </value>
        [DisplayName("Location of Injury/Illness")]
        public string InjuryLocation { get; set; }

        /// <summary>
        /// Gets or sets the Nature of injury code.
        /// </summary>
        /// <value>
        /// The Injured Body Part code.
        /// </value>
        [DisplayName("Part of Body")]
        public string InjuredBodyPart { get; set; }

        /// <summary>
        /// Gets or sets the classification.
        /// </summary>
        /// <value>
        /// The classification.
        /// </value>
        [DisplayName("Case Classification")]
        public WorkRelatedCaseClassification? Classification { get; set; }

        /// <summary>
        /// Gets or sets the type of injury.
        /// </summary>
        /// <value>
        /// The type of injury.
        /// </value>
        [DisplayName("Type of Injury")]
        public WorkRelatedTypeOfInjury? TypeOfInjury { get; set; }

        /// <summary>
        /// Gets or sets the days away from work.
        /// </summary>
        /// <value>
        /// The days away from work.
        /// </value>
        [DisplayName("Days away from work")]
        public int? DaysAwayFromWork { get; set; }

        /// <summary>
        /// Gets or sets the days on job transfer or restriction.
        /// </summary>
        /// <value>
        /// The days on job transfer or restriction.
        /// </value>
        [DisplayName("Days on job transfer or restriction")]
        public int? DaysOnJobTransferOrRestriction { get; set; }

        /// <summary>
        /// Gets or sets the provider contact.
        /// </summary>
        /// <value>
        /// The provider contact.
        /// </value>
        public CreateContactViewModel ProviderContact { get; set; }

        /// <summary>
        /// Gets or sets the emergency room.
        /// </summary>
        /// <value>
        /// The emergency room.
        /// </value>
        [DisplayName("Employee treated in Emergency Room?")]
        public bool? EmergencyRoom { get; set; }

        /// <summary>
        /// Gets or sets the hospitalized overnight.
        /// </summary>
        /// <value>
        /// The hospitalized overnight.
        /// </value>
        [DisplayName("Employee hospitalized overnight as in-patient?")]
        public bool? HospitalizedOvernight { get; set; }

        /// <summary>
        /// Gets or sets the time employee began work.
        /// </summary>
        /// <value>
        /// The time employee began work.
        /// </value>
        [DisplayName("Time employee began work?")]
        public TimeOfDayViewModel TimeEmployeeBeganWork { get; set; }

        /// <summary>
        /// Gets or sets the time of event.
        /// </summary>
        /// <value>
        /// The time of event.
        /// </value>
        [DisplayName("Time of the event?")]
        public TimeOfDayViewModel TimeOfEvent { get; set; }

        /// <summary>
        /// Gets or sets the activity before incident.
        /// </summary>
        /// <value>
        /// The activity before incident.
        /// </value>
        [DisplayName("What was the employee doing just before the incident occurred?"), UIHint("MultilineText")]
        public string ActivityBeforeIncident { get; set; }

        /// <summary>
        /// Gets or sets the what happened.
        /// </summary>
        /// <value>
        /// The what happened.
        /// </value>
        [DisplayName("What happened?"), UIHint("MultilineText")]
        public string WhatHappened { get; set; }

        /// <summary>
        /// Gets or sets the injury or illness.
        /// </summary>
        /// <value>
        /// The injury or illness.
        /// </value>
        [DisplayName("Injury or illness?")]
        public string InjuryOrIllness { get; set; }

        /// <summary>
        /// Gets or sets the what harmed the employee.
        /// </summary>
        /// <value>
        /// The what harmed the employee.
        /// </value>
        [DisplayName("What caused harm?")]
        public string WhatHarmedTheEmployee { get; set; }

        /// <summary>
        /// Gets or sets the date of death.
        /// </summary>
        /// <value>
        /// The date of death.
        /// </value>
        [DisplayName("Date of Death"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? DateOfDeath { get; set; }

        /// <summary>
        /// Gets or sets the sharps.
        /// </summary>
        /// <value>
        /// The sharps.
        /// </value>
        [DisplayName("Sharps?")]
        public bool? Sharps { get; set; }

        /// <summary>
        /// Gets or sets the sharps information.
        /// </summary>
        /// <value>
        /// The sharps information.
        /// </value>
        public WorkRelatedSharpsViewModel SharpsInfo { get; set; }

        /// <summary>
        /// Work Related Info
        /// </summary>
        public WorkRelatedInfo WorkRelated { get; set; }

        /// <summary>
        /// Determines whether the specified object is valid.
        /// </summary>
        /// <param name="validationContext">The validation context.</param>
        /// <returns>
        /// A collection that holds failed-validation information.
        /// </returns>
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new List<ValidationResult>();
            
        }
    }
}