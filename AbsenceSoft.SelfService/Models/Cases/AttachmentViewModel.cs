﻿using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models.Cases
{
    public class AttachmentViewModel
    {
        public AttachmentViewModel() { }

        public AttachmentViewModel(Attachment attachment)
        {
            this.Id = attachment.Id;
            this.CaseId = attachment.CaseId;
            this.EmployeeId = attachment.EmployeeId;
            this.FileName = attachment.FileName;
            this.Type = attachment.AttachmentType;
        }

        public string Id { get; set; }
        public string EmployeeId { get; set; }
        public string CaseId { get; set; }
        public string FileName { get; set; }
        public AttachmentType? Type { get; set; }
        public string Description { get; set; }
    }
}