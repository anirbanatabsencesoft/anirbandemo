﻿using AbsenceSoft;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Tasks;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models.Cases
{
    public class NoteViewModel
    {
        public NoteViewModel()
        {

        }

        public NoteViewModel(CaseNote note)
            : this()
        {
            this.Id = note.Id;
            this.CreatedBy = string.Format("{0} {1}", note.CreatedBy.FirstName, note.CreatedBy.LastName);
            this.CreatedDate = DateExtensions.ToLocalDateTime(note.CreatedDate);
            this.Notes = note.Notes;
            this.Category = note.CategoryString;
        }

        public NoteViewModel(ListResult result)
        {
            this.Id = result.Get<string>("Id");
            this.CaseId = result.Get<string>("CaseId");
            this.CreatedBy = result.Get<string>("CreatedBy");            
            this.NoteCreatedDateUiString = result.Get<DateTime>("CreatedDate").ToUIString();
            this.Notes = result.Get<string>("Notes");
            this.Category = result.Get<string>("Category");
        }


        public string Id { get; set; }
        public string CaseId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public String NoteCreatedDate { get; set; }
        [Required(ErrorMessage = "Please provide notes")]
        public string Notes { get; set; }
        public string Category { get; set; }

        public string NoteCreatedDateUiString { get; set; }

        internal CaseNote ApplyToDataModel(string customerId, string employerId, CaseNote note = null)
        {
            if (note == null)
                note = new CaseNote();

            note.CaseId = this.CaseId;
            note.Public = true;
            using (var noteService = new NotesService())
            {
                note.Categories = noteService.CreateSavedNoteCategories(this.Category, Data.Enums.EntityTarget.Case);
            }
            note.CustomerId = customerId;
            note.EmployerId = employerId;
            note.Notes = this.Notes;


            return note;
        }
    }
}