﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models.Cases
{
    public class CustomFieldViewModel
    {
        public CustomFieldViewModel() { }

        public CustomFieldViewModel(CustomField customField) 
        {
            this.Id = customField.Id;
            this.EmployerId = customField.EmployerId;  
            this.Name = customField.Name;
            this.SelectedValueText = customField.SelectedValueText;
            this.IsCollectedAtIntake = customField.IsCollectedAtIntake;
            this.Target = customField.Target;
        }

        public CustomFieldViewModel(CustomField customField, List<CustomField> customFieldConfiguration)
        {
            this.Id = customField.Id;
            this.EmployerId = customField.EmployerId;
            this.Name = customField.Name;
            this.SelectedValueText = customField.SelectedValueText;
            this.IsCollectedAtIntake = customField.IsCollectedAtIntake;
            this.Target = customField.Target;
            if (customFieldConfiguration != null)
            {
                var _custom = customFieldConfiguration.FirstOrDefault(c => c.Id == customField.Id);
                if (_custom != null)
                {
                    isESS = _custom.IsESS;
                }
            }
        }

        public string Id { get; set; }
        public string EmployerId { get; set; }
        public string CaseId { get; set; }
        public string Name { get; set; }       
        public string SelectedValueText { get; set; }      
        public bool IsCollectedAtIntake { get; set; }
        public EntityTarget Target { get; set; }

        /// <summary>
        /// Property to check isESS value from customField collection
        /// </summary>
        public bool isESS { get; set; }
    }
}