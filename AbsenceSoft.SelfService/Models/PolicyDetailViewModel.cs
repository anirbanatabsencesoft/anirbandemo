﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models
{
    public class PolicyDetailViewModel
    {
        public PolicyDetailViewModel()
        {

        }

        public PolicyDetailViewModel(AppliedPolicy ap, DateTime date)
            :this()
        {
            this.PolicyName = ap.Policy.Name;
            var matchingUsage = ap.Usage.Where(u => u.DateUsed == date).DefaultIfEmpty(new AppliedPolicyUsage(){
             Determination = AdjudicationStatus.Pending   
            }).FirstOrDefault();
            this.Determination = matchingUsage.Determination;
            this.AbsentHours = (matchingUsage.MinutesUsed / 60).ToString("0.00");
        }

        public string PolicyName { get; set; }
        public AdjudicationStatus Determination { get; set; }
        public string DeterminationString
        {
            get
            {
                return Determination.ToString();
            }
        }
        public string AbsentHours { get; set; }
    }
}