﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models.Request
{
    public class RequestViewModel:IValidatableObject    
    {
        public RequestViewModel()
        {
            PersonalInfo = new PersonalInfoViewModel();
            AlternateInfo = new PersonalInfoViewModel();
        }

        public RequestViewModel(string employeeId)
            :this()
        {
            if (string.IsNullOrEmpty(employeeId))
                employeeId = Current.User().EmployeeId;

            using (var employeeService = new EmployeeService())
            {
                Employee employee = employeeService.GetEmployee(employeeId);
                EmployerId = employee.EmployerId;
                FullName = employee.FullName;
                EmployeeNumber = employee.EmployeeNumber;
            }

            EmployeeId = employeeId;
        }

        [Required]
        public string EmployeeId { get; set; }
        public string FullName { get; set; }
        public string EmployeeNumber { get; set; }
        public string EmployerId { get; set; }

        [Required(ErrorMessage = "Please select starting date"), DisplayName("Start Date"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate { get; set; }

        [DisplayName("End Date"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? EndDate { get; set; }

        [DisplayName("Please provide a description:")]
        public string Details { get; set; }
        
        public PersonalInfoViewModel PersonalInfo { get; set; }

        public PersonalInfoViewModel AlternateInfo { get; set; }

        public void PopulatePersonalInfo()
        {
            if (string.IsNullOrEmpty(EmployeeId))
                return;

            ///Get the personal info from the DB and populate
            using (var employeeService = new EmployeeService())
            {
                Employee emp = employeeService.GetEmployee(EmployeeId);
                PersonalInfo = new PersonalInfoViewModel(emp);
            }
        }



        public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            if (StartDate > EndDate)
                results.Add(new ValidationResult("End Date must be after Start Date", new List<string>() { "EndDate" }));

            return results;
        }
    }
}
