﻿using AbsenceSoft.Data.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models.Request
{
    public class AccommodationTypeViewModel
    {
        public AccommodationTypeViewModel()
        {

        }

        public AccommodationTypeViewModel(AccommodationType type)
        {
            if (type == null)
            {
                return;
            }

            this.Id = type.Id;
            this.Name = type.Name;
        }

        public string Id { get; set; }
        public string Name { get; set; }
    }
}