﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.SelfService.Models.Cases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models.Request
{
    public class NewCaseViewModel : RequestViewModel
    {
        public NewCaseViewModel()
            : base()
        {
            this.AbsenceReasons = new List<AbsenceReasonViewModel>();
        }

        public NewCaseViewModel(string employeeId)
            : base(employeeId)
        {

        }

        public string AbsenceReasonId { get; set; }

        [Required(ErrorMessage = "Absence Reason Is Required")]
        public string AbsenceReasonCode { get; set; }


        [DisplayName("Is this work related?")]
        public bool IsWorkRelated { get; set; }

        public WorkRelatedViewModel WorkRelatedInfo { get; set; }

        public List<AbsenceReasonViewModel> AbsenceReasons { get; set; }

        public CaseContactViewModel Contact { get; set; }

        public CaseType CaseType { get; set; }

        public void PopulateAbsenceReasons()
        {
            if (this.EmployeeId == null)
                return;

            if (this.AbsenceReasons == null)
                this.AbsenceReasons = new List<AbsenceReasonViewModel>();

            using (var caseService = new CaseService())
            {
                IEnumerable<AbsenceReason> reasons = caseService.GetAbsenceReasons(this.EmployeeId)
                    .Where(ar => !((ar.Flags & AbsenceReasonFlag.HideInEmployeeSelfServiceSelection) == AbsenceReasonFlag.HideInEmployeeSelfServiceSelection)
                            && !((ar.CaseTypes & CaseType.Administrative) == CaseType.Administrative)
                    );

                /// Base level Absence Reasons
                this.AbsenceReasons.AddRange(
                        reasons.Where(ar => string.IsNullOrWhiteSpace(ar.Category))
                                .Select(ar => new AbsenceReasonViewModel(ar))
                );

                /// Categories and children
                this.AbsenceReasons.AddRange(
                    reasons.Where(ar => !string.IsNullOrEmpty(ar.Category)).Select(ar => ar.Category).Distinct()
                    .Select(c => new AbsenceReasonViewModel()
                    {
                        Name = c,
                        Code = c,
                        Children = reasons.Where(ar => ar.Category == c).Select(ar => new AbsenceReasonViewModel(ar)).ToList()
                    })
                );
            }
        }

        /// <summary>
        /// New  case view model validation
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = base.Validate(validationContext).ToList();

            var absenceReason = AbsenceReason.GetByCode(AbsenceReasonCode);
            if (absenceReason is null)
            {
                if (EmployeeId != null)
                {
                    using (EmployeeService employeeService = new EmployeeService())
                    {
                        var employeeInfo = employeeService.GetEmployee(EmployeeId);
                        absenceReason = AbsenceReason.GetByCode(AbsenceReasonCode, employeeInfo.CustomerId, employeeInfo.EmployerId);
                    }
                }
                else
                {
                    results.Add(new ValidationResult("Absence Reason not found."));
                }

                if (absenceReason == null && EmployeeId != null)
                {
                    using (var employeeService = new EmployeeService())
                    {
                        var employeeInfo = employeeService.GetEmployee(EmployeeId);

                        if (employeeInfo != null)
                        {
                            absenceReason = AbsenceReason.GetByCode(AbsenceReasonCode, employeeInfo.CustomerId,
                                employeeInfo.EmployerId);
                        }
                    }
                }

                if (absenceReason == null)
                {
                    results.Add(new ValidationResult("Absence reason not found."));
                    return results;
                }

                if ((absenceReason.Flags & AbsenceReasonFlag.RequiresRelationship) == AbsenceReasonFlag.RequiresRelationship
                    && !ContactIsValid())
                {
                    results.Add(new ValidationResult("The selected absence reason requires a contact, but no contact details were provided"));
                }

                if ((absenceReason.Flags & AbsenceReasonFlag.RequiresPregnancyDetails) == AbsenceReasonFlag.RequiresPregnancyDetails)
                {
                    if (!ExpectedDeliveryDate.HasValue)
                        results.Add(new ValidationResult("Maternity details missing.  Please fill out the expected delivery date."));
                }
            }
            return results;

        }

            public bool ContactIsValid()
            {
                if (Contact == null)
                    return false;

                /// Must either select a contact, or provide relationship code and first and last name
                if (string.IsNullOrEmpty(Contact.Id) && (
                        string.IsNullOrEmpty(Contact.RelationshipCode)
                        || string.IsNullOrEmpty(Contact.FirstName)
                        || string.IsNullOrEmpty(Contact.LastName)
                    ))
                    return false;

                return true;
            }

        #region Pregnancy Or Maternity
        [DisplayName("Expected Delivery Date"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? ExpectedDeliveryDate { get; set; }

        [DisplayName("Actual Delivery Date"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? ActualDeliveryDate { get; set; }

        [DisplayName("Use Bonding?")]
        public bool IsBonding { get; set; }

        [DisplayName("Bonding Start Date"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? BondingStartDate { get; set; }

        [DisplayName("Bonding End Date"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? BondingEndDate { get; set; }

        [DisplayName("Medical Complication?")]
        public bool IsMedicalComplication { get; set; }
        #endregion

    
        
    }
}