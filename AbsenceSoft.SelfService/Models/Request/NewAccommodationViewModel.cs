﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models.Request
{
    public class NewAccommodationViewModel : RequestViewModel
    {
        public NewAccommodationViewModel()
            : base()
        {

        }

        public NewAccommodationViewModel(string employeeId)
            : base(employeeId)
        {

        }
        [Required(ErrorMessage = "Reason for Case is required")]
        public string AccommodationTypeId { get; set; }

        public IEnumerable<AccommodationTypeViewModel> AccommodationTypes { get; set; }
        public IEnumerable<AccommodationTypeCategoryViewModel> AccommodationTypeCategories { get; set; }

        internal void PopulateAccommodations()
        {
            
            using( var administrationService = new AdministrationService(Current.Customer(), Current.Employer(), Current.User()))
            using (var accommodationService = new AccommodationService(Current.Customer(), Current.Employer(), Current.User()))
            {
                var accommodationTypes = accommodationService.GetAccommodationTypes();
                if (administrationService.CustomerHasFeature(Feature.AccommodationTypeCategories))
                {
                    AccommodationTypeCategories = accommodationTypes.Select(at => new AccommodationTypeCategoryViewModel(at)); ;
                }



                    AccommodationTypes = accommodationTypes.Select(at => new AccommodationTypeViewModel(at));
            }
        }

        private List<AccommodationTypeCategoryViewModel> GetAccommodationTypeCategories(AccommodationTypeCategoryViewModel category, List<AccommodationTypeCategoryViewModel> categories)
        {
            string parentName = null;
            if (category != null)
                parentName = category.Name;

            List<AccommodationTypeCategoryViewModel> childCategories = categories.Where(c => c.ParentName == parentName).ToList();
            foreach (var cat in childCategories)
            {
                cat.Children = GetAccommodationTypeCategories(cat, categories);
            }

            return childCategories;
        }

        internal AccommodationRequest CreateAccommodation(Case theCase)
        {
            using (var accommodationService = new AccommodationService())
            {
                AccommodationType selectedType = accommodationService.GetAccommodationTypeById(this.AccommodationTypeId);
                List<Accommodation> theAccomms = new List<Accommodation>(1) {
                    new Accommodation()
                    {
                        Status = CaseStatus.Requested,
                        Type = selectedType,
                        Duration = EndDate.HasValue ? AccommodationDuration.Temporary : AccommodationDuration.Permanent,
                        Usage = new List<AccommodationUsage>()
                        {
                            new AccommodationUsage()
                            {
                                StartDate = StartDate.Value,
                                EndDate = EndDate,
                                Determination = AdjudicationStatus.Pending
                            }
                        }
                    }
                };
                AccommodationRequest theRequest = accommodationService.CreateOrModifyAccommodationRequest(theCase, this.Details, theAccomms);
                theRequest.StartDate = StartDate;
                theRequest.EndDate = EndDate;
                
                return theRequest;
            }

        }
    }
}