﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models.Request
{
    public class AbsenceReasonViewModel
    {
        public AbsenceReasonViewModel()
        {
            this.Children = new List<AbsenceReasonViewModel>();
        }


        public AbsenceReasonViewModel(AbsenceReason absenceReason)
            :this()
        {
            if (absenceReason == null)
                return;

            this.Id = absenceReason.Id;
            this.Name = absenceReason.Name;
            this.Code = absenceReason.Code;
            this.IsForFamilyMember = (absenceReason.Flags & AbsenceReasonFlag.RequiresRelationship) == AbsenceReasonFlag.RequiresRelationship;
            this.CanBeConsecutive = (absenceReason.CaseTypes & CaseType.Consecutive) == CaseType.Consecutive;
            this.CanBeIntermittent = (absenceReason.CaseTypes & CaseType.Intermittent) == CaseType.Intermittent;
            this.CanBeReduced = (absenceReason.CaseTypes & CaseType.Reduced) == CaseType.Reduced;
            this.RequiresPregnancyDetails = (absenceReason.Flags & AbsenceReasonFlag.RequiresPregnancyDetails) == AbsenceReasonFlag.RequiresPregnancyDetails;
        }


        public string Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public bool IsForFamilyMember { get; set; }
        public bool CanBeConsecutive { get; set; }
        public bool CanBeIntermittent { get; set; }
        public bool CanBeReduced { get; set; }

        public bool RequiresPregnancyDetails { get; set; }

        public bool HasChildren
        {
            get
            {
                return this.Children != null && this.Children.Count > 0;
            }
        }

        public List<AbsenceReasonViewModel> Children { get; set; }


        #region Utilities
        public string ChildId { get { return string.Format("{0}Children", this.Code.Replace(" ", "")); } }


        public string DataTargetText()
        {
            if (this.HasChildren)
                return string.Format("data-target=#{0} data-toggle=collapse", ChildId);
            else if (this.IsForFamilyMember)
                return "data-target=#contact";

            return "";
        }
        #endregion
    }
}