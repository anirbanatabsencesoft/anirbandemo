﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models.Request
{
    public class AccommodationTypeCategoryViewModel
    {
        public AccommodationTypeCategoryViewModel(AccommodationType type)
        {
            // copy properties
            Name = type.Name;
            ParentName = type.ParentName;
            AccommodationTypeCode = type.Code;
            Order = type.Order;
            AccommodationTypeId = type.Id;
            Duration = type.Duration == 0 ? AccommodationDuration.Both : type.Duration;

            if (type.Children != null && type.Children.Any())
            {
                Children = type.Children.Select(c => new AccommodationTypeCategoryViewModel(c)).ToList();
            }

        }
        public string Name { get; set; }
        public string ParentName { get; set; }
        public string AccommodationTypeId { get; set; }
        public string AccommodationTypeCode { get; set; }

        public AccommodationDuration Duration { get; set; }//Temporary, Permanent
        public int? Order { get; set; }
        public List<AccommodationTypeCategoryViewModel> Children { get; set; }
    }
}