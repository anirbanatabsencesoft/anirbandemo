﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models.Request
{
    public class PersonalInfoViewModel
    {
        public PersonalInfoViewModel() { }

        public PersonalInfoViewModel(Employee emp)
        {
            if (emp == null)
                return;

            FirstName = emp.FirstName;
            LastName = emp.LastName;
            EmailAddress = emp.Info.Email;
            PhoneNumber = emp.Info.HomePhone;
            Address1 = emp.Info.Address.Address1;
            Address2 = emp.Info.Address.Address2;
            City = emp.Info.Address.City;
            State = emp.Info.Address.State;
            ZipCode = emp.Info.Address.PostalCode;
            Country = emp.Info.Address.Country;
        }

        public void ApplyAlternateInfo(Case theCase)
        {
            if (theCase == null)
                throw new ArgumentNullException("theCase");

            theCase.Employee.Info.AltPhone = PhoneNumber;
            theCase.Employee.Info.AltEmail = EmailAddress;
            theCase.Employee.Info.AltAddress = new Data.Address()
            {
                Address1 = Address1,
                Address2 = Address2,
                City = City,
                State = State,
                PostalCode = ZipCode,
                Country = Country
            };
        }

        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [DataType(DataType.EmailAddress), DisplayName("Email Address")]
        public string EmailAddress { get; set; }

        [DisplayName("Phone Number")]
        public string PhoneNumber { get; set; }

        [DisplayName("Street Address")]
        public string Address1 { get; set; }

        [DisplayName("Apt, Suite, Etc. (Optional)")]
        public string Address2 { get; set; }

        [DisplayName("City")]
        public string City { get; set; }

        [DisplayName("State"), UIHint("State")]
        public string State { get; set; }

        [DisplayName("Country"), UIHint("Country")]
        public string Country { get; set; }

        [DisplayName("Zip Code"), RegularExpression(@"^\d{5}(?:-\d{4})?$", ErrorMessage = "Zip Code should be in the format XXXXX.")]
        public string ZipCode { get; set; }
    }
}