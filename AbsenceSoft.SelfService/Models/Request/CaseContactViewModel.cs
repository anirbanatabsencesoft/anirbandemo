﻿using AbsenceSoft.Data.Customers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models.Request
{
    public class CaseContactViewModel
    {
        [DisplayName("Existing Contact")]
        public string Id { get; set; }

        [DisplayName("Relationship")]
        public string RelationshipCode { get; set; }

        public string RelationshipName { get; set; }

        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [DisplayName("Date Of Birth"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? DateOfBirth { get; set; }

        public EmployeeContact ApplyToDataModel(EmployeeContact contact = null)
        {
            if (!string.IsNullOrEmpty(Id))
                return EmployeeContact.GetById(Id);

            if (contact == null)
                contact = new EmployeeContact();

            contact.Contact.FirstName = FirstName;
            contact.Contact.LastName = LastName;
            contact.Contact.DateOfBirth = DateOfBirth;
            contact.ContactTypeCode = RelationshipCode??contact.ContactTypeCode;
            contact.ContactTypeName = RelationshipCode != null ? ContactType.GetByCode(RelationshipCode).Name:null;

            return contact;
        }
    }
}