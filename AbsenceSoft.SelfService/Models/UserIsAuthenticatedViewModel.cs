﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models
{
    public class UserIsAuthenticatedViewModel
    {
        public bool IsUserAuthenticated { get; set; }
        public bool WillExpire { get; set; }
        public long ExpirationTime { get; set; }
        public bool WillExpireSameDayOrWithinNextMeridiem { get; set; }
    }
}