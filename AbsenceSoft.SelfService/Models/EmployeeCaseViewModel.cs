﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models
{
    public class EmployeeCaseViewModel
    {
        public IEnumerable<EmployeeViewModel> EmployeeViewModels { get; set; }
        public IEnumerable<CaseViewModel> CaseViewModels { get; set; }
    }
}