﻿using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models
{
    public class LoginViewModel:IValidatableObject
    {
        public LoginViewModel()
        {
            TermsAndConditions = true;
        }

        public LoginViewModel(SsoProfile profile)
            :this()
        {
            if (profile == null)
                return;

            ShowSsoButton = !string.IsNullOrWhiteSpace(profile.SingleSignOnServiceUrl);
            ShowLogo = profile.LoginSourceShowLogo;
            if (!string.IsNullOrEmpty(profile.SelfServiceLoginSourceName))
            {
                PortalName = profile.SelfServiceLoginSourceName;
            }
            else
            {
                PortalName = "SSO";
            }
             
        }

        [Required, EmailAddress, DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required, DataType(DataType.Password)]
        public string Password { get; set; }

        public bool TermsAndConditions { get; set; }

        public bool ShowSsoButton { get; set; }

        public bool ShowLogo { get; set; }

        public string PortalName { get; set; }
        
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            if (!TermsAndConditions)
            {
                results.Add(new ValidationResult("You must accept the terms and conditions", new List<string>() { "TermsAndConditions" }));
            }
            return results;
        }
    }
}
