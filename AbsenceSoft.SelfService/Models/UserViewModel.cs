﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models
{
    public class UserViewModel
    {
        public UserViewModel() { }

        public UserViewModel(User u)
            :this()
        {
            this.FirstName = u.FirstName;
            this.LastName = u.LastName;
            this.Email = u.Email;
            this.Employer = u.Employers.First().Employer.Name;
            this.ViewEmployees = User.Permissions.GetProjectedPermissions(u).Contains(Permission.MyEmployeesDashboard.Id);
            this.EmployeeId = u.EmployeeId;
            this.HasAccessToReports = User.Permissions.GetProjectedPermissions(u).Contains(Permission.ESSMyReports.Id);
            this.IsSSOUser = u.Metadata.GetRawValue<bool>("SSO");
        }

        public bool IsSSOUser { get; }
        public string EmployeeId { get; set; }

        public bool HasAccessToReports { get; set; }

        [DisplayName("First Name"), Required]
        public string FirstName { get; set; }

        [DisplayName("Last Name"), Required]
        public string LastName { get; set; }

        public bool ViewEmployees { get; set; }

        public bool TimeOffCanBeRequested()
        {
            if (string.IsNullOrEmpty(this.EmployeeId))
            {
                return false;
            }

            using (var employeeService = new EmployeeService())
            {
                var emp = employeeService.GetEmployee(this.EmployeeId);
                EmployeeViewModel model = new EmployeeViewModel(emp);

                return model.TimeOffCanBeRequested();
            }
        }

        [DisplayName("Work Email"), Required]
        public string Email { get; set; }
        
        public string Employer { get; set; }

        public User ApplyToDataModel(User u)
        {
            if (u.Metadata.GetRawValue<bool>("SSO"))
                return u;

            u.FirstName = this.FirstName;
            u.LastName = this.LastName;
            u.Email = this.Email;

            return u;
        }
    }
}