﻿using AbsenceSoft.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models
{
    public class CountryViewModel
    {
        public CountryViewModel()
        {

        }

        public CountryViewModel(Country country)
        {
            if (country == null)
                return;

            Name = country.Name;
            Code = country.Code;
            RegionName = country.RegionName;
            if (country.Regions != null)
                Regions = country.Regions.Select(r => new RegionViewModel(r)).ToList();
        }

        public string Name { get; set; }
        public string Code { get; set; }
        public string RegionName { get; set; }
        public List<RegionViewModel> Regions { get; set; }
    }
}