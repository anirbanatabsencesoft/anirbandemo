﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models.Home
{
    public class LogoViewModel
    {
        public string Url { get; set; }
        public string Name { get; set; }

        public void PopulateLogoInformation(Employer employer, Customer customer)
        {
            if (employer != null && !string.IsNullOrEmpty(employer.LogoId))
                PopulateLogoBasedOnEmployerInfo(employer);
            else if(!string.IsNullOrEmpty(customer.LogoId))
                PopulateLogoBasedOnCustomerInfo(customer);

            if (string.IsNullOrEmpty(this.Name))
                this.Name = customer.Name;
        }

        private void PopulateLogoBasedOnEmployerInfo(Employer employer)
        {
            using (var employerFileService = new EmployerFileService())
            {
                this.Url = employerFileService.DownloadFile(employer.Id, employer.LogoId);
                this.Name = employer.Name;
            }
        }

        private void PopulateLogoBasedOnCustomerInfo(Customer customer)
        {
            using (var customerFileService = new CustomerFileService())
            {
                this.Url = customerFileService.DownloadFile(customer.Id, customer.LogoId);
                this.Name = customer.Name;
            }
        }
    }
}