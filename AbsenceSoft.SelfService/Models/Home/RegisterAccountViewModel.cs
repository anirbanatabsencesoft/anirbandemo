﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.SelfService.Models.Home
{
    public class RegisterAccountViewModel : IValidatableObject
    {
        [Required, DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public string ClientIpAddress { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();

            return results;
        }
    }    
}