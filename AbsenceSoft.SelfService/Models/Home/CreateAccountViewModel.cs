﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models.Home
{
    public class CreateAccountViewModel : IValidatableObject
    {
        public CreateAccountViewModel()
        {
        }

        public CreateAccountViewModel(string verificationKey)
            : this()
        {
            VerificationKey = verificationKey;
        }

        [Required]
        public string VerificationKey { get; set; }

        [Required, DisplayName("Enter Password"), DataType(DataType.Password)]
        public string Password { get; set; }

        [Required, DisplayName("Confirm Password"), DataType(DataType.Password)]
        public string ConfirmNewPassword { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            if (Password != ConfirmNewPassword)
            {
                results.Add(new ValidationResult("Password and Confirmation do not match"));
            }

            return results;
        }
    }
}