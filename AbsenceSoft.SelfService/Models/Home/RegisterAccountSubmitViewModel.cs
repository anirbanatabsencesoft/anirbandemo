﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.SelfService.Models.Home
{
    public class RegisterAccountSubmitViewModel
    {
        [Required, DataType(DataType.EmailAddress)]
        [Display(Name ="Work Email")]
        public string Email { get; set; }

        public RegistrationStatus Status { get; set; }

        public bool Resend { get; set; }

        public bool ResendToAltEmail { get; set; }
    }
}