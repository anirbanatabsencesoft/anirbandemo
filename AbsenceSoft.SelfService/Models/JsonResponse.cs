﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.SelfService.Models
{
    public class JsonResponse
    {
        public JsonResponse()
        {

        }

        public JsonResponse(ModelStateDictionary modelState)
        {
            this.Error = !modelState.IsValid;
            this.Messages = modelState.Values.SelectMany(m => m.Errors).Select(e => e.ErrorMessage);
        }

        public bool Error { get; set; }
        public bool Redirect { get; set; }
        public string RedirectUrl { get; set; }
        public IEnumerable<string> Messages { get; set; }
        public object Data { get; set; }
    }
}