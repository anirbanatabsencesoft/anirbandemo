﻿using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Customers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.SelfService.Models.Demands
{
    public abstract class AppliedDemandViewModel:IValidatableObject
    {
        public AppliedDemandViewModel()
        {
            this.Values = new List<AppliedDemandValueViewModel>();
        }

        public string Id { get; set; }

        [UIHint("Demands"), DisplayName("Demand"), Required]
        public string DemandId { get; set; }

        [DisplayName("Demand")]
        public string DemandName { get; set; }
        
        [DisplayName("Dates")]
        public string DateString { get; set; }

        [DisplayName("Start Date"), Required, DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// The Overridden Start Date, so that we can use the same model for single entry and multi entry
        /// </summary>
        [DisplayName("Start Date"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? OverriddenStartDate { get; set; }

        /// <summary>
        /// The End Date, we don't need to have an override value because this isn't required
        /// </summary>
        [DisplayName("End Date"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? EndDate { get; set; }

        [UIHint("AppliedDemandValue")]
        public List<AppliedDemandValueViewModel> Values { get; set; }

        public bool IsWorkRestrictionEntry { get; set; }

        /// <summary>
        /// Builds the values model.
        /// </summary>
        /// <param name="theDemand">The demand.</param>
        /// <param name="demandTypes">The demand types.</param>
        /// <param name="label">The label.</param>
        internal void BuildValuesModel(Demand theDemand = null, List<DemandType> demandTypes = null, Func<DemandType, string> label = null)
        {
            if (string.IsNullOrEmpty(this.DemandId))
            {
                this.Values = new List<AppliedDemandValueViewModel>();
                return;
            }

            using (var demandService = new DemandService(User.Current))
            {
                List<DemandType> possibleTypes = null;
                if (theDemand == null && demandTypes != null)
                    theDemand = Demand.GetById(this.DemandId);

                if (demandTypes == null)
                {
                    possibleTypes = demandService.GetDemandTypesForDemand(this.DemandId);
                }
                else
                {
                    possibleTypes = demandTypes.Where(d => theDemand.Types.Contains(d.Id)).ToList();
                }

                List<AppliedDemandValueViewModel> appliedValues = new List<AppliedDemandValueViewModel>(possibleTypes.Count());
                if (this.Values == null || this.Values.Count() == 0)
                {
                    this.Values = possibleTypes.Select(dt => new AppliedDemandValueViewModel()
                    {
                        DemandTypeId = dt.Id,
                        Name = label == null ? dt.RestrictionLabel ?? dt.Name : label(dt) ?? dt.Name,
                        DemandItems = dt.Items,
                        Type = dt.Type
                    }).ToList();
                }
                else
                {
                    foreach (var value in this.Values)
                    {
                        DemandType matchingDemandType = possibleTypes.FirstOrDefault(dt => dt.Id == value.DemandTypeId);
                        if (matchingDemandType != null)
                        {
                            value.Name = matchingDemandType.Name;
                            value.DemandItems = matchingDemandType.Items;
                            value.Type = matchingDemandType.Type;
                        }
                    }
                }
            }
        }

        public bool HasValues
        {
            get
            {
                if (this.Values == null)
                    return false;

                if (this.Values.Count == 0)
                    return false;

                if (this.Values.All(v => !v.HasValue))
                    return false;

                return true;
            }

        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            if (this.IsWorkRestrictionEntry)
            {
                if(this.EndDate.HasValue && (!this.OverriddenStartDate.HasValue))
                {
                    results.Add(new ValidationResult("The Start Date field is required.", new List<string>() { "OverriddenStartDate" }));
                }
                else if (this.EndDate.HasValue && this.EndDate < this.OverriddenStartDate)
                {
                    results.Add(new ValidationResult("End Date must be after Start Date.", new List<string>() { "EndDate" }));
                }  
            }            
            else if (this.EndDate.HasValue && this.EndDate < this.StartDate)
            {
                results.Add(new ValidationResult("End Date must be after Start Date", new List<string>() { "EndDate" }));
            }

            return results;
        }
    }
}
