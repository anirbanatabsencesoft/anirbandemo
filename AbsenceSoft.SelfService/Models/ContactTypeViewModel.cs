﻿using AbsenceSoft.Data.Customers;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.SelfService.Models
{
    public class ContactTypeViewModel
    {
        public ContactTypeViewModel()
        {

        }

        public ContactTypeViewModel(ContactType contactType)
        {
            this.Id = contactType.Id;
            this.Code = contactType.Code;
            this.Name = contactType.Name;            
        }

        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Code { get; set; }
    }
}