﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Reporting
{
    public class ReportChart
    {
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [is3 d].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is3 d]; otherwise, <c>false</c>.
        /// </value>
        public bool is3D { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is stacked.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is stacked; otherwise, <c>false</c>.
        /// </value>
        public bool isStacked { get; set; }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        public int Height { get; set; }
    }
}
