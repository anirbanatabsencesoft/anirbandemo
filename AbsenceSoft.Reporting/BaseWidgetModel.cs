﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Dashboards;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Reporting
{
    [Serializable]
    public abstract class BaseWidgetModel : BaseNonEntity
    {
        private List<string> _errors = new List<string>();

        /// <summary>
        /// Gets a value indicating whether this instance is in error.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is in error; otherwise, <c>false</c>.
        /// </value>
        public bool IsError { get { return _errors.Any(); } }

        /// <summary>
        /// Gets the readonly errors collection for this instance.
        /// </summary>
        /// <value>
        /// The errors.
        /// </value>
        public IReadOnlyList<string> Errors { get { return _errors.AsReadOnly(); } }

        /// <summary>
        /// Gets or sets the total time it took to complete in miliseconds.
        /// </summary>
        [BsonIgnoreIfNull]
        public long? CompletedIn { get; set; }

        /// <summary>
        /// Gets or sets the date and time the widget was originally requested to be run.
        /// </summary>
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc, Representation = BsonType.DateTime), BsonIgnoreIfDefault]
        public DateTime RequestDate { get; set; }

        /// <summary>
        /// Gets or sets the user that requested the widget, also used for data visibility
        /// functions.
        /// </summary>
        [BsonIgnoreIfNull]
        public User RequestedBy { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [BsonRepresentation(BsonType.ObjectId)]
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the settings.
        /// </summary>
        /// <value>
        /// The settings.
        /// </value>
        public List<WidgetSetting> Settings { get; set; }

        /// <summary>
        /// Adds the error.
        /// </summary>
        /// <param name="error">The error.</param>
        /// <param name="args">The arguments.</param>
        public void AddError(string error, params object[] args)
        {
            if (args.Length > 0)
                _errors.Add(string.Format(error, args));
            else
                _errors.Add(error);
        }
    }
}
