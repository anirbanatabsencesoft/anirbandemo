﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Reporting.Reports.CustomFields
{
    [Serializable]
    public abstract class CustomFieldReport : BaseReport
    {
        /// <summary>
        /// When overridden in a derived class, gets the name of the report main category to bifurcate reports.
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Icon to set for intermittent pattern report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_intermittent.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report category for display.
        /// </summary>
        public override string Category { get { return "Case Custom Date Fields"; } }

        /// <summary>
        /// Determines whether the specified report is visible to the given user for any valid reason.
        /// </summary>
        /// <param name="user">The user to check against.</param>
        /// <returns>
        ///   <c>true</c> if the specified report is visible; otherwise, <c>false</c>.
        /// </returns>
        public override bool IsVisible(User user)
        {
            if (user == null) return false;
            if(user.Customer != null && user.Customer.Id == "546e5097a32aa00d60e3210a")
            {
                return false;
            }
            var criteria = GetCustomFieldsCriteria(user);
            if (criteria == null) return false;
            return criteria.Options.Any();
        }

        /// <summary>
        /// Builds the query.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="criteria">The criteria.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        protected virtual IMongoQuery BuildQuery(User user, ReportCriteria criteria, CustomFieldType? type = null)
        {
            criteria = criteria ?? GetCriteria(user);

            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "Employee._id"));
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            ApplyEmployerFilter<Case>(ands, criteria);

            string fieldNameFilter = (criteria.Filters.FirstOrDefault(f => f.Name == "CustomField") ?? new ReportCriteriaItem()).Value as string;

            List<IMongoQuery> elemMatchAnds = new List<IMongoQuery>();
            elemMatchAnds.Add(Query.EQ("Target", new BsonInt32((int)EntityTarget.Case)));
            if (type.HasValue)
                elemMatchAnds.Add(Query.EQ("DataType", new BsonInt32((int)type.Value)));
            if (!string.IsNullOrWhiteSpace(fieldNameFilter))
                elemMatchAnds.Add(Query.EQ("Name", new BsonString(fieldNameFilter)));
            if (type == CustomFieldType.Date)
            {
                elemMatchAnds.Add(Query.GTE("SelectedValue", new BsonDateTime(startDate)));
                elemMatchAnds.Add(Query.LTE("SelectedValue", new BsonDateTime(endDate)));
            }
            else
            {
                // Must match a range for dates, not just start and end within that range, it's
                //  any leave that intersects the date range selected by the user.
                ands.Add(Case.Query.Or(
                    Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                    Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
                ));
            }

            ands.Add(Case.Query.ElemMatch(c => c.CustomFields, f => f.And(elemMatchAnds)));

            return Case.Query.And(ands);
        }

        private Dictionary<string, Customer> _cust = new Dictionary<string, Customer>();
        protected virtual ResultData GetStaticData(Case td)
        {
            var cust = _cust[td.CustomerId] = _cust.ContainsKey(td.CustomerId) ? _cust[td.CustomerId] : td.Customer;

            string displayCaseNumber = td.CaseNumber;
            string displayFullName = td.Employee.FullName;
            string displayReason = td.Reason.Name;
            string displayLeaveType = td.CaseType.ToString();
            string displayCreatedOn = td.CreatedDate.ToString("MM/dd/yyyy");
            var evt = td.FindCaseEvent(CaseEventType.CaseCreated);
            if (evt != null && evt.EventDate != DateTime.MinValue)
                displayCreatedOn = evt.EventDate.ToString("MM/dd/yyyy");

            dynamic data = new ResultData();
            data.CaseNumber = string.IsNullOrWhiteSpace(displayCaseNumber) ? null : new LinkItem() { Value = td.CaseNumber, Link = string.Format("/Cases/{0}/View", td.Id) };
            data.Name = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = td.Employee.FullName, Link = string.Format("/Employees/{0}/View", td.Employee.Id) };
            if (cust.HasFeature(Feature.ADA) && td.IsAccommodation && td.AccommodationRequest != null && td.AccommodationRequest.Accommodations != null && td.AccommodationRequest.Accommodations.Any())
                data.Duration = string.Join(", ", td.AccommodationRequest.Accommodations.Select(a => a.Duration.ToString().SplitCamelCaseString()).Distinct());
            if (cust.HasFeature(Feature.LOA))
                data.Reason = displayReason;
            data.CaseType = displayLeaveType;
            data.CreatedOn = displayCreatedOn;
            data.StartDate = td.StartDate.ToString("MM/dd/yyyy");
            data.EndDate = td.EndDate.ToString("MM/dd/yyyy");
            data.Status = td.Status.ToString().SplitCamelCaseString();
            if (cust.HasFeature(Feature.ADA) && td.IsAccommodation && td.AccommodationRequest != null && td.AccommodationRequest.Accommodations != null && td.AccommodationRequest.Accommodations.Any())
                data.Accommodations = string.Join(", ", td.AccommodationRequest.Accommodations.Select(a => a.Type.Code).Distinct());
            data.AssignedTo = td.AssignedToName;
            data.WorkState = td.Employee.WorkState;
            data.Location = td.Employee.Info.OfficeLocation;

            return data;
        }
    }
}
