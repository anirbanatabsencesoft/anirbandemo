﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Reporting.Reports.CustomFields
{
    [Serializable]
    public class CustomFieldDatesDetailReport : CustomFieldReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "27AA17A1-13DE-49AF-8761-99D0609331A9"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "Detail"; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }

        /// <summary>
        /// Property to configure chart related options
        /// </summary>
        public override ReportChart Chart
        {
            get
            {
                return new ReportChart()
                {
                    Type = "PieChart",
                    Title = "",
                    is3D = true,
                    isStacked = false,
                    Height = 400
                };
            }
        }
        
        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria();
            criteria.CustomerId = user.CustomerId;
            criteria.StartDate = DateTime.UtcNow.Date.AddDays(-7);
            criteria.EndDate = DateTime.UtcNow.Date;
            ReportCriteriaItem item = GetCustomFieldsCriteria(user, CustomFieldType.Date, EntityTarget.Case);
            criteria.Filters.Add(item);
            return criteria;
        }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override void RunReport(User user, ReportResult result)
        {
            var query = BuildQuery(user, result.Criteria, CustomFieldType.Date);
            var cases = Case.Query.Find(query).SetBatchSize(100).ToList();

            Dictionary<string, int> reduce = new Dictionary<string, int>();
            List<ResultDataGroup> groupResults = new List<ResultDataGroup>();

            foreach (var c in cases)
            {
                ResultData data = GetStaticData(c);
                foreach (var f in (c.CustomFields ?? new List<CustomField>(0))
                    .Where(f => f.DataType == CustomFieldType.Date && f.SelectedValue != null && !string.IsNullOrWhiteSpace(f.SelectedValue.ToString()))
                    .OrderBy(f => f.Name))
                {
                    ResultDataGroup group = groupResults.FirstOrDefault(r => r.Key.ToString() == f.Name);
                    if (group == null)
                        group = groupResults.AddFluid(new ResultDataGroup()
                        {
                            Key = f.Name,
                            Label = f.Label ?? f.Name,
                            Items = new List<ResultData>()
                        });

                    if (f.SelectedValue is DateTime)
                        data[f.Name] = ((DateTime)f.SelectedValue).ToString("MM/dd/yyyy");
                    else
                        data[f.Name] = f.SelectedValue.ToString();

                    // Add a copy of this data to the result items for this custom field
                    group.Items.Add(data);

                    // Ensure we keep count on our field usage (reduce/summary)
                    if (reduce.ContainsKey(f.Name))
                        reduce[f.Name] += 1;
                    else
                        reduce.Add(f.Name, 1);
                }
            }

            result.Groups = new List<ResultDataGroup>();
            result.Groups.AddRange(groupResults);

            result.SeriesData = new List<List<object>>(reduce.Count + 1);
            // Define our first row, which contains series labels for the x (col 1) and y (col 2~n) axis.
            // Add our series definition/labels row to the result series data collection.
            result.SeriesData.Add(new List<object>(2) { "Field", "Count" });

            // Loop through our inline results and pump out the series data results to the series data collection
            //  on the result object
            foreach (var r in reduce)
                result.SeriesData.Add(new List<object>(2) { r.Key, r.Value });
        }
    }
}
