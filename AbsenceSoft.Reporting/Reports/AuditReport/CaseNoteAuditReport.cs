﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Notes;

namespace AbsenceSoft.Reporting.Reports.AuditReport
{
    [Serializable]
    public class CaseNoteAuditReport : AuditReport
    {
        public override string Id { get { return "E29BF6C0-C4DA-4CA7-A7B2-9B43092E107C"; } }
        public override string Name { get { return "Case Notes"; } }
        public override bool IsGrouped { get { return false; } }

        protected override void RunReport(User user, ReportResult result)
        {
            var auditCaseNotes = EmployerAudit<CaseNote>.AsQueryable(user).ToList().OrderBy(m => m.Value.CaseId).ThenBy(m => m.Value.Id);

            List<ResultData> resultItems = new List<ResultData>();

            string displayCaseNumber = string.Empty;
            string displayCaseNoteId = string.Empty;
            string displayCaseId = string.Empty;

            foreach (var caseNote in auditCaseNotes)
            {
                dynamic item = new ResultData();

                if (displayCaseId != caseNote.Value.CaseId)
                {
                    displayCaseId = caseNote.Value.CaseId;

                    string currentCaseNumber = string.Empty;
                    var cAse = Case.AsQueryable().Where(m => m.Id == caseNote.Value.CaseId).FirstOrDefault();

                    if (cAse != null)
                        currentCaseNumber = cAse.CaseNumber;

                    if (string.IsNullOrWhiteSpace(currentCaseNumber))
                    {
                        item.CaseNumber = "";
                    }
                    else
                    {
                        item.CaseNumber = new LinkItem() { Value = currentCaseNumber, Link = string.Format("/Cases/{0}/View", caseNote.Value.CaseId) }; 
                    }
                }
                else
                {
                    item.CaseNumber = "";
                }

                string auditType = string.Empty;

                if (displayCaseNoteId != caseNote.Value.Id)
                {
                    displayCaseNoteId = caseNote.Value.Id;
                    auditType = caseNote.AuditType == Data.AuditType.Save ? "Added" : caseNote.AuditType.ToString();
                    item.CaseNoteId = caseNote.Value.Id;
                }
                else
                {
                    auditType = caseNote.AuditType == Data.AuditType.Save ? "Updated" : (caseNote.AuditType == Data.AuditType.Delete ? "Deleted" : caseNote.AuditType.ToString());
                }

                item.CaseNote = caseNote.Value.Notes;
                item.Category = caseNote.Value.Category.ToString();
                item.ChangedOn = caseNote.CreatedDate.ToString("MM/dd/yyyy hh:mm:ss tt").ToLower();
                item.ChangedBy = (User.AsQueryable().Where(m => m.Id.Equals(caseNote.CreatedBy)).FirstOrDefault() ?? User.System).DisplayName;
                item.AuditType = auditType;
                resultItems.Add(item);
            }

            result.Items = resultItems;
        }
    }
}
