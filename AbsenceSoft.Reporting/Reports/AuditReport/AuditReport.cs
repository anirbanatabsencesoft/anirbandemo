﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Reporting.Reports.AuditReport
{
    [Serializable]
    public abstract class AuditReport : BaseReport
    {

        /// <summary>
        /// Category for all Absence Status reports is "Absence Status".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all Absence Status reports is "Absence Status".
        /// </summary>
        public override string Category { get { return "Audit Report"; } }

        /// <summary>
        /// Icon to set for Audit Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_productivity-comparison.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart
        {
            get { return null; }
        }

        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria();
            criteria.CustomerId = user.CustomerId;
            criteria.HasCriterias = false;
            criteria.StartDate = DateTime.UtcNow.Date.AddDays(-30);
            criteria.EndDate = DateTime.UtcNow.Date;
            return criteria;
        }
    }
}
