﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Communications;
using MongoDB.Driver;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using MongoDB.Bson;
using AbsenceSoft.Logic.Users;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using MongoDB.Driver.Builders;

namespace AbsenceSoft.Reporting.Reports.CommunicationReports
{
    [Serializable]
    public class CommunicationDetailReport : BaseReport
    {
        public const string CommunicationDetailReportId = "AEF89837-7575-4495-B07C-2060FDFAD853";

        public override string Id { get { return CommunicationDetailReportId; } }

        public override string Name { get { return "Detail"; } }

        public override string MainCategory { get { return "Operations Report"; } }

        public override string Category { get { return "Communications"; } }

        public override ReportChart Chart { get { return null; } }

        public override string IconImage { get { return "btn-icon-communication-reverse.png"; } }

        public override bool IsGrouped { get { return false; } }

        public override bool GroupAutoTotals { get { return false; } }

        public override bool IsVisible(User user)
        {
            return user != null && user.HasPermission(Permission.RunCommunicationReport);
        }

        public override ReportCriteria GetCriteria(User user)
        {
            // 
            // Sent date
            ReportCriteria criteria = new ReportCriteria();
            criteria.CustomerId = user.CustomerId;
            criteria.StartDate = DateTime.UtcNow.Date.AddDays(-30);
            criteria.EndDate = DateTime.UtcNow.Date;
            ReportCriteriaItem item;
            //
            // Comm Type
            item = GetCommTypeCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetAbsenceReasonCriteria(user);
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetCaseTypeCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetAdjudicationCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            criteria.Filters.Add(GetWorkStateCriteria(user, criteria.StartDate, criteria.EndDate));
            //criteria.Filters.Add(GetLocationCriteria(user));
            criteria.Filters.AddRange(GetOrganizationTypeCriteria(user));

            return criteria;
        }

        protected override void RunReport(User user, ReportResult result)
        {
            var items = RunQuery(user, result.Criteria).AsQueryable();

            #region Lookups
            var customers = Customer
                .GetByIds(items.Select(i => i.CustomerId))
                .ToDictionary(c => c.Id);

            var employers = Employer
                .GetByIds(items.Select(i => i.EmployerId))
                .ToDictionary(d => d.Id);

            var cases = Case
                .GetByIds(items.Select(i => i.CaseId))
                .ToDictionary(d => d.Id);

            var employees = Employee
                .GetByIds(items.Select(i => i.EmployeeId))
                .ToDictionary(d => d.Id);
            items.ForEach(i =>
            {
                i.Customer = customers.ContainsKey(i.CustomerId) ? customers[i.CustomerId] : new Customer();
                i.Employer = employers.ContainsKey(i.EmployerId) ? employers[i.EmployerId] : new Employer();
                i.Case = cases.ContainsKey(i.CaseId) ? cases[i.CaseId] : new Case();
                i.Employee = employees.ContainsKey(i.EmployeeId) ? employees[i.EmployeeId] : new Employee();
            });
            #endregion

            #region Post Query Filtering
            var reportCriteria = result.Criteria;
            if (reportCriteria.Filters.Any())
            {
                if (reportCriteria.Filters.FirstOrDefault(m => m.Name == "CaseStatus") != null)
                {
                    if (reportCriteria.Filters.FirstOrDefault(m => m.Name == "CaseStatus").Value != null)
                    {
                        CaseStatus enumCaseStatusFilter = reportCriteria.ValueOf<CaseStatus>("CaseStatus");
                        items = items.Where(i => i.Case.Status == enumCaseStatusFilter);
                    }
                }

                if (reportCriteria.Filters.FirstOrDefault(m => m.Name == "CaseType") != null)
                {
                    if (reportCriteria.Filters.FirstOrDefault(m => m.Name == "CaseType").Value != null)
                    {
                        CaseType enumCaseTypeFilter = reportCriteria.ValueOf<CaseType>("CaseType");
                        items = items.Where(i => i.Case.Segments.Any() && i.Case.Segments[0].Type == enumCaseTypeFilter);
                    }
                }

                if (reportCriteria.Filters.FirstOrDefault(m => m.Name == "Determination") != null)
                {
                    if (reportCriteria.Filters.FirstOrDefault(m => m.Name == "Determination").Value != null)
                    {
                        AdjudicationStatus enumadjStatusFilter = reportCriteria.ValueOf<AdjudicationStatus>("Determination");
                        items = items.Where(i => i.Case.Summary.Determination == enumadjStatusFilter);
                    }
                }

                if (reportCriteria.Filters.FirstOrDefault(m => m.Name == "AbsenceReason") != null)
                {
                    if (reportCriteria.Filters.FirstOrDefault(m => m.Name == "AbsenceReason").Value != null)
                    {
                        var reason = reportCriteria.Filters.FirstOrDefault(m => m.Name == "AbsenceReason").Value;
                        items = items.Where(i => i.Case.Reason.Id == Convert.ToString(reason));
                    }
                }

                if (reportCriteria.Filters.FirstOrDefault(m => m.Name == "WorkState") != null)
                {
                    if (reportCriteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value != null)
                    {
                        var workstate = (reportCriteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value).ToString();
                        items = items.Where(i => i.Employee.WorkState == workstate);
                    }
                }

                if (reportCriteria.Filters.FirstOrDefault(m => m.Name == "Location") != null)
                {
                    if (reportCriteria.Filters.FirstOrDefault(m => m.Name == "Location").Value != null && !string.IsNullOrWhiteSpace(reportCriteria.Filters.FirstOrDefault(m => m.Name == "Location").ValueAs<string>()))
                    {
                        var location = (reportCriteria.Filters.FirstOrDefault(m => m.Name == "Location").Value).ToString();
                        items = items.Where(i => i.Employee.Info.OfficeLocation == location);
                    }
                }
                //var locationFilter = reportCriteria.Filters.FirstOrDefault(m => m.Name == "Location");                
                //if (locationFilter != null && locationFilter.Values != null && locationFilter.Values.Any())
                //{
                //    var val = locationFilter.ValuesAs<string>();
                //    if (val != null && val.Any())                        
                //        items = items.Where(i => val.Contains(i.Case.Employee.Info.OfficeLocation ?? ""));
                //}                

            }

            #endregion

            result.Items = items
                .OrderBy(o => o.Employee.EmployerName)
                .ToList().Select(comm =>
                {
                    dynamic data = new ResultData();
                    var isTpa = comm.Customer.HasFeature(Feature.MultiEmployerAccess);

                    isTpa = true;
                    if (isTpa)
                    {
                        data.Employer = comm.Employer.SignatureName;
                    }

                    data.CaseNumber = string.Empty;
                    if (!string.IsNullOrEmpty(comm.CaseId) && cases.ContainsKey(comm.CaseId))
                        data.CaseNumber = new LinkItem() { Value = cases[comm.CaseId].CaseNumber, Link = string.Format("/Cases/{0}/View", comm.CaseId) };
                    
                    data.Employee = string.Empty;
                    if (!string.IsNullOrEmpty(comm.EmployeeId) && employees.ContainsKey(comm.EmployeeId))
                            data.Employee = new LinkItem() { Value = employees[comm.EmployeeId].FullName, Link = string.Format("/Employees/{0}/View", comm.EmployeeId) };
                        
                    data.SentDate = comm.SentDate.HasValue ? comm.SentDate.Value.Date.ToShortDateString() : null;
                    data.CommunicationType = comm.CommunicationType.ToString();
                    data.CommunicationName = comm.Name ?? string.Empty;
                    data.Subject = comm.Subject;
                    // direct link to comm doesn't work well
                    //data.Subject = new LinkItem() { Value = comm.Subject, Link = string.Format("/Communications/{0}/View/{1}", comm.Case.Id, comm.Id) };
                    return (ResultData)data;
                })
                .ToList();
        }
        
        private List<Communication> RunQuery(User user, ReportCriteria reportCriteria)
        {
            DateTime startDate = reportCriteria.StartDate.ToMidnight();
            DateTime endDate = reportCriteria.EndDate.EndOfDay();

            List<IMongoQuery> criteria = new List<IMongoQuery>();
            if (user != null)
                criteria.Add(user.BuildDataAccessFilters(Permission.RunCommunicationReport, "EmployeeId"));
            else if (!string.IsNullOrWhiteSpace(reportCriteria.CustomerId))
                criteria.Add(Case.Query.EQ(e => e.CustomerId, reportCriteria.CustomerId));
            ApplyEmployerFilter<Case>(criteria, reportCriteria);

            // Add ESS filter
            // Fetch user permission
            // HACK: Copied/pasted from communications service. Needs clean-up
            List<string> userPermissions;
            if (Employer.Current != null)
                userPermissions = User.Permissions.EmployerPermissions(Employer.Current.Id).ToList();
            else
                userPermissions = User.Permissions.ProjectedPermissions.ToList();

            if (!user.HasPermission(Permission.ViewConfidentialCommunications))
            {
                criteria.Add(Communication.Query.Or(Communication.Query.EQ(c => c.Public, true), Query.EQ("Public", BsonNull.Value), Communication.Query.EQ(e => e.CreatedById, User.Current.Id)));
            }

            criteria.Add(Communication.Query.GTE(e => e.SentDates, startDate));
            criteria.Add(Communication.Query.LTE(e => e.SentDates, endDate));

            if (reportCriteria.Filters.Any())
            {
                if (reportCriteria.Filters.FirstOrDefault(m => m.Name == "Comm Type") != null &&
                    reportCriteria.Filters.FirstOrDefault(m => m.Name == "Comm Type").Value != null)
                {
                    var commtype = reportCriteria.ValueOf<CommunicationType>("Comm Type");
                    criteria.Add(Query<Communication>.EQ(a => a.CommunicationType, commtype));
                }
            }
            AppendOrganizationFilterQuery(user, reportCriteria, queries: criteria);
            var query = Communication.Query.Find(Communication.Query.And(criteria));
            return query.ToList();
        }
    }
}
