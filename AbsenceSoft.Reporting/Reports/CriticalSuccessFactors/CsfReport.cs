﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Reporting.Reports.CriticalSuccessFactors
{
    [Serializable]
    public abstract class CsfReport : BaseReport
    {
        /// <summary>
        /// When overridden in a derived class, gets the name of the report main category to bifurcate reports.
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Icon to set for intermittent pattern report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report category for display.
        /// </summary>
        public override string Category { get { return "Critical Success Factors"; } }

        /// <summary>
        /// Determines whether the specified report is visible to the given user for any valid reason.
        /// </summary>
        /// <param name="user">The user to check against.</param>
        /// <returns>
        ///   <c>true</c> if the specified report is visible; otherwise, <c>false</c>.
        /// </returns>
        public override bool IsVisible(User user)
        {
            if (user == null) return false;
            return user.CustomerId == "546e5097a32aa00d60e3210a"; // Parrot Only
        }

        /// <summary>
        /// Property to configure chart related options
        /// </summary>
        public override ReportChart Chart { get { return null; } }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria();
            criteria.HasCriterias = true;
            criteria.ShowAgeCriteria = false;
            criteria.StartDate = DateTime.UtcNow.GetFirstDayOfMonth().Date;
            criteria.EndDate = DateTime.UtcNow.Date;
            criteria.Filters = new List<ReportCriteriaItem>(5);
            criteria.Filters.Add(GetCaseStatusCriteria());
            criteria.Filters.Add(GetAccomTypeCriteria(user));
            criteria.Filters.Add(GetAdjudicationCriteria());
            criteria.Filters.Add(GetLocationCriteria(user));
            criteria.Filters.Add(GetCaseAssigneeCriteria(user));
            return criteria;
        }

        /// <summary>
        /// Builds the query.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="criteria">The criteria.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        protected virtual List<IMongoQuery> BuildQuery(User user, ReportCriteria criteria)
        {
            criteria = criteria ?? GetCriteria(user);

            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "Employee._id"));
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            ApplyEmployerFilter<Case>(ands, criteria);

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
            ));

            ands.Add(Case.Query.EQ(c => c.IsAccommodation, true));
            ands.Add(Case.Query.NE(c => c.AccommodationRequest.Accommodations, null));

            var caseStatusFilter = criteria.ValueOf<CaseStatus?>("CaseStatus");
            var locationFilter = criteria.ValueOf<string>("Location");
            var caseAssigneeFilter = criteria.ValueOf<string>("CaseAssignee");
            var accomTypeFilter = criteria.ValueOf<string>("AccomType");
            var adjudicationFilter = criteria.ValueOf<AdjudicationStatus?>("Determination");

            if (caseStatusFilter.HasValue)
                ands.Add(Case.Query.EQ(c => c.Status, caseStatusFilter.Value));

            if (!string.IsNullOrWhiteSpace(locationFilter))
                ands.Add(Case.Query.EQ(c => c.Employee.Info.OfficeLocation, locationFilter));

            if (!string.IsNullOrWhiteSpace(caseAssigneeFilter))
                ands.Add(Case.Query.EQ(c => c.AssignedToId, caseAssigneeFilter));

            List<IMongoQuery> accomFilter = new List<IMongoQuery>();
            accomFilter.Add(Query<Accommodation>.NE(a => a.Type, null));
            
            if (!string.IsNullOrWhiteSpace(accomTypeFilter))
                accomFilter.Add(Query<Accommodation>.EQ(a => a.Type.Code, accomTypeFilter));

            if (adjudicationFilter.HasValue)
                accomFilter.Add(Query<Accommodation>.EQ(a => a.Determination, adjudicationFilter.Value));

            ands.Add(Case.Query.ElemMatch(c => c.AccommodationRequest.Accommodations, b => b.And(accomFilter)));

            return ands;
        }
    }
}
