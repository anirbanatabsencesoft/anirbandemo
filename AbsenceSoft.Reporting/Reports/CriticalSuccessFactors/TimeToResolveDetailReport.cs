﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Reporting.Reports.CriticalSuccessFactors
{
    [Serializable]
    public class TimeToResolveDetailReport : CsfReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "DEB288F4-329C-4CF7-8B71-9BBA9D9DE43D"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "Time to Resolve"; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }

        public override bool IsVisible(User user)
        {
            return false;
        }

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether to show the auto 
        /// generated total counts at the bottom of the group page.
        /// </summary>
        /// <value>
        ///   <c>true</c> if group auto totals; otherwise, <c>false</c>.
        /// </value>
        public override bool GroupAutoTotals { get { return false; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 1; } }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override void RunReport(User user, ReportResult result)
        {
            var query = Case.Query.Find(Case.Query.And(BuildQuery(user, result.Criteria))).ToList()
                .SelectMany(c => c.AccommodationRequest.Accommodations.Select(a => new {
                    Case = c,
                    Accommodation = a
                })).ToList();

            var groupQuery = query.GroupBy(n => n.Accommodation.Type.Code).OrderByDescending(g => g.Key).Select(g => new
            {
                key = g.First().Accommodation.Type.Code,
                label = g.First().Accommodation.Type.Name,
                data = g.ToList()
            });

            List<ResultData> resultItems = new List<ResultData>();

            result.Groups = new List<ResultDataGroup>(groupQuery.Count() + 1);
            int i = 1;
            int count = 0;
            int days = 0;
            List<Tuple<string, string, int, decimal>> summary = new List<Tuple<string, string, int, decimal>>(groupQuery.Count());
            foreach (var t in groupQuery)
            {
                count = 0;
                days = 0;
                resultItems = new List<ResultData>();
                foreach (var td in t.data)
                {
                    dynamic data = new ResultData();
                    data.CaseNumber = new LinkItem() { Value = td.Case.CaseNumber, Link = string.Format("/Cases/{0}/View", td.Case.Id) };
                    data.EmployeeNumber = new LinkItem() { Value = td.Case.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", td.Case.Employee.Id) };
                    data.Name = new LinkItem() { Value = td.Case.Employee.FullName, Link = string.Format("/Employees/{0}/View", td.Case.Employee.Id) };
                    data.Accommodation = td.Accommodation.Type.Code;
                    data.Status = td.Accommodation.Determination.ToString().SplitCamelCaseString();
                    data.Location = td.Case.Employee.Info.OfficeLocation;
                    data.AssignedTo = td.Case.AssignedToName;
                    data.TimeToResolve = null;
                    if (td.Accommodation.Resolved)
                    {
                        count++;
                        int ttr = Convert.ToInt32(Math.Ceiling(((td.Accommodation.ResolvedDate ?? DateTime.UtcNow) - td.Case.CreatedDate).TotalDays));
                        days += ttr;
                        data.TimeToResolve = string.Format("{0:N0}", ttr);
                    }

                    resultItems.Add(data);
                }

                result.Groups.Add(new ResultDataGroup()
                {
                    Key = t.key,
                    Label = t.label,
                    Items = new List<ResultData>().AddRangeFluid(resultItems.OrderBy(r => ((LinkItem)r["CaseNumber"]).Value ?? "").ThenBy(r => r["TimeToResolve"] == null ? "" : r["TimeToResolve"].ToString()))
                });

                summary.Add(new Tuple<string, string, int, decimal>(t.label, t.key, count, count > 0 ? (decimal)days/(decimal)count : -1M));

                i++;
            }

            result.Groups.Insert(0, new ResultDataGroup()
            {
                Key = "Summary",
                Label = "Summary",
                Items = summary.Select(s =>
                {
                    var rd = new ResultData();
                    rd["Accommodation"] = s.Item1;
                    rd["Code"] = s.Item2;
                    rd["Total Resolved"] = string.Format("{0:N0}", s.Item3 > 0 ? s.Item3 : 0);
                    rd["Average Time to Resolve"] = string.Format("{0:N1}", s.Item4 > 0 ? new decimal?(s.Item4) : null);
                    return rd;
                }).ToList()
            });
        }
    }
}
