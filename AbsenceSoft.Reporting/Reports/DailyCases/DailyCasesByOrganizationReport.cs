﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Reporting.Reports;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;
using MongoDB.Driver.Builders;

namespace AbsenceSoft.Reporting.Reports.DailyCases
{
    [Serializable]
    public class DailyCasesByOrganizationReport : DailyCasesReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "7477CC1F-9B79-4E0D-8C9A-E7E145D307EB"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "by Organization"; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 3; } }

        /// <summary>
        /// Include the Organization Grouping Criteria
        /// </summary>
        public override bool IncludeOrganizationGrouping { get { return true; } }

        protected override void RunReport(User user, ReportResult result)
        {
            var query = RunQuery(user, result.Criteria);            

            string grouptypeCode = null;
            var customer = user.Customer;
            var isTpa = customer.HasFeature(Feature.MultiEmployerAccess) && user.Employers.Count > 1;
            if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "OrganizationTypeGrouping") != null)
                grouptypeCode = result.Criteria.ValueOf<string>("OrganizationTypeGrouping");
            List<string> customerIds = query.Select(cs => cs.CustomerId).Distinct().ToList();
            List<string> employerIds = query.Select(cs => cs.EmployerId).Distinct().ToList();
            List<string> employeeNumbers = query.Select(cs => cs.Employee.EmployeeNumber).Distinct().ToList();
            List<EmployeeOrganization> employeeOrganizations = null;
            if (grouptypeCode != null)
                employeeOrganizations = EmployeeOrganization.AsQueryable().Where(org => org.TypeCode == grouptypeCode && customerIds.Contains(org.CustomerId) && employerIds.Contains(org.EmployerId) && employeeNumbers.Contains(org.EmployeeNumber)).OrderByDescending(o => o.Dates).ToList();
            else
                employeeOrganizations = EmployeeOrganization.AsQueryable().Where(org => customerIds.Contains(org.CustomerId) && employerIds.Contains(org.EmployerId) && employerIds.Contains(org.EmployerId) && employeeNumbers.Contains(org.EmployeeNumber)).OrderByDescending(o => o.Dates).ToList();
            List<EmployeeOrganization> employeeUniqueOrganizations = new List<EmployeeOrganization>();
            foreach (EmployeeOrganization empOrg in employeeOrganizations)
            {
                if (empOrg.TypeCode != OrganizationType.OfficeLocationTypeCode ||
                    ((empOrg.Dates == null || empOrg.Dates.IncludesToday(true))
                    && (!employeeUniqueOrganizations.Where(empUniqueOrg => empUniqueOrg.TypeCode == OrganizationType.OfficeLocationTypeCode && empUniqueOrg.EmployeeNumber == empOrg.EmployeeNumber && empUniqueOrg.CustomerId == empOrg.CustomerId && empUniqueOrg.EmployerId == empOrg.EmployerId).Any())))
                {
                    employeeUniqueOrganizations.Add(empOrg);
                }
            }
            var groupQuery = employeeUniqueOrganizations.GroupBy(org => new { CustomerId = org.CustomerId, EmployerId = org.EmployerId, Code = org.Code })
           .Select(g => new
           {
               label = employeeUniqueOrganizations.Where(empOrgs => empOrgs.Code == g.Key.Code).FirstOrDefault().Name,
               data = g.Select(x => x).ToList()
           }).OrderBy(m => m.label);

            List<ResultData> resultItems = new List<ResultData>();

            result.Groups = new List<ResultDataGroup>();
            int i = 1;
            //Avoid displaying cases under more than one office location code.
            List<string> empLocationCase = new List<string>();
            foreach (var t in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (var org in t.data)
                {
                    foreach (Case td in query.Where(cs => cs.Employee.EmployeeNumber == org.EmployeeNumber && cs.CustomerId == org.CustomerId && cs.EmployerId == org.EmployerId))
                    {
                        if (IsOfficeLocationTypeCode(org.TypeCode) && EmployeeLocationCaseExists(empLocationCase, td.Id))
                        {
                            continue;
                        }
                        ResultData data = GetData(td);
                        if (data != null) resultItems.Add(data);
                        if (IsOfficeLocationTypeCode(org.TypeCode) && !EmployeeLocationCaseExists(empLocationCase, td.Id))
                        {
                            empLocationCase.Add(td.Id.ToUpper());
                        }
                    }
                }
                if (resultItems != null && resultItems.Count > 0)
                {
                    result.Groups.Add(new ResultDataGroup()
                    {
                        Key = i,
                        Label = t.label,
                        Items = new List<ResultData>().AddRangeFluid(resultItems.OrderBy(r => ((dynamic)r).CreatedOn).ToList())
                    });
                    i++;
                }
            }            
        }

        private bool IsOfficeLocationTypeCode(string orgTypeCode)
        {
            if (string.IsNullOrWhiteSpace(orgTypeCode))
            {
                return false;
            }
            if(orgTypeCode.ToUpper().TrimEnd() != OrganizationType.OfficeLocationTypeCode.ToUpper().TrimEnd())
            {
                return false;
            }
            return true;
        }

        private bool EmployeeLocationCaseExists(List<string> empLocationCase, string caseId)
        {
            if (empLocationCase == null)
            {
                return false;
            }
            if (empLocationCase.Contains(caseId.ToUpper()))
            {
                return false;
            }
            return true;
        }
    }
}
