﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Reporting.Reports.Intermittent
{
    [Serializable]
    public class IntermittentScheduleExceptionReport : BaseReport
    {
        /// <summary>
        /// The report identifier
        /// </summary>
        public const string ReportId = "F29878F1-B8B7-41DB-B285-642B78868A4E";

        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id => ReportId;

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name => "Schedule Exception Report";

        /// <summary>
        /// When overridden in a derived class, gets the name of the report category for display.
        /// </summary>
        public override string Category => "Intermittent";

        /// <summary>
        /// When overridden in a derived class, gets the name of the report main category to bifurcate reports.
        /// </summary>
        public override string MainCategory => "Operations Report";

        /// <summary>
        /// When overridden in a derived class, gets name of the icon image to set in front end
        /// </summary>
        public override string IconImage => "absencesoft-temporary-reports-icons_intermittent.png";

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped => false;

        /// <summary>
        /// Property to configure chart related options
        /// </summary>
        public override ReportChart Chart => null;

        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns>LOA Feature</returns>
        public override IEnumerable<Feature> RequiredFeatures()
        {
            yield return Feature.LOA;
        }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerId,
                StartDate = Date.UnixEpoch.ToMidnight(),
                EndDate = DateTime.UtcNow.Date.GetLastDayOfYear().AddYears(3)
            };

            criteria.Filters.Add(GetCaseAssigneeCriteria(user));
            criteria.Filters.Add(GetLocationCriteria(user));
            criteria.Filters.Add(GetWorkStateCriteria(user, criteria.StartDate, criteria.EndDate));

            return criteria;
        }

        [Serializable]
        private class ReportDataContainer : IEqualityComparer<ReportDataContainer>
        {
            /// <summary>
            /// Gets or sets the policy code.
            /// </summary>
            /// <value>
            /// The policy code.
            /// </value>
            public string PolicyCode { get; set; }

            /// <summary>
            /// Gets or sets the date used.
            /// </summary>
            /// <value>
            /// The date used.
            /// </value>
            public DateTime DateUsed { get; set; }

            /// <summary>
            /// Gets or sets the minutes.
            /// </summary>
            /// <value>
            /// The minutes.
            /// </value>
            public int Minutes { get; set; }

            /// <summary>Gets or sets the scheduled minutes.</summary>
            /// <value>The scheduled minutes.</value>
            public int Scheduled { get; set; }
            /// <summary>
            /// Determines whether the specified objects are equal.
            /// </summary>
            /// <param name="x">The first object of type <paramref name="T" /> to compare.</param>
            /// <param name="y">The second object of type <paramref name="T" /> to compare.</param>
            /// <returns>
            /// true if the specified objects are equal; otherwise, false.
            /// </returns>
            public bool Equals(ReportDataContainer x, ReportDataContainer y)
            {
                return x.DateUsed.Date == y.DateUsed.Date
                    && x.PolicyCode == y.PolicyCode;
            }

            /// <summary>
            /// Returns a hash code for this instance.
            /// </summary>
            /// <param name="obj">The object.</param>
            /// <returns>
            /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
            /// </returns>
            public int GetHashCode(ReportDataContainer obj)
            {
                return base.GetHashCode();
            }
        }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override void RunReport(User user, ReportResult result)
        {
            // Get our start and end date from the criteria
            DateTime startDate = result.Criteria.StartDate.ToMidnight();
            DateTime endDate = result.Criteria.EndDate.EndOfDay();

            // Create the mongo query and apply the data access filters
            List<IMongoQuery> criteria = new List<IMongoQuery>
            {
                user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "Employee._id")
            };

            // Apply the employer filter
            ApplyEmployerFilter<Case>(criteria, result.Criteria);

            // Apply general case filters
            result.Criteria.ApplyForOptionalFilter<string>(CriteriaName.CaseAssignee, v => criteria.Add(Case.Query.EQ(e => e.AssignedToId, v)));
            result.Criteria.ApplyForOptionalFilter<string>(CriteriaName.WorkState, v => criteria.Add(Case.Query.EQ(e => e.Employee.WorkState, v)));
            result.Criteria.ApplyForOptionalFilter<string>(CriteriaName.Location, v => criteria.Add(Case.Query.EQ(e => e.Employee.Info.OfficeLocation, v)));

            // Manual case status = Open filter
            criteria.Add(Case.Query.EQ(c => c.Status, CaseStatus.Open));

            // Now apply date filters against the TORs
            criteria.Add(Case.Query.ElemMatch(c => c.Segments, s =>
                s.And(
                    s.EQ(segment => segment.Type, CaseType.Intermittent),
                    s.ElemMatch(t => t.UserRequests, r => r.And(
                        // Only care where we have approved time over 0
                        r.ElemMatch(b => b.Detail, b => b.GT(f => f.Approved, 0)),
                        r.GTE(e => e.RequestDate, new BsonDateTime(startDate)),
                        r.LTE(e => e.RequestDate, new BsonDateTime(endDate))
                    ))
                )
            ));

            // Query cases
            var cases = Case.Query.Find(Case.Query.And(criteria)).ToList();
            var employeeIds = cases.Select(c => c.Employee.Id).Distinct().ToList();
            var employees = Employee.AsQueryable().Where(e => employeeIds.Contains(e.Id)).ToList();

            result.Items = new List<ResultData>(cases.Count);

            foreach (var c in cases)
            {
                // Get the employee from the queried list of impacted employees
                var emp = employees.FirstOrDefault(e => e.Id == c.Employee.Id) ?? c.Employee;

                var usage = c.Segments
                  .Where(s => s.Type == CaseType.Intermittent)
                  .SelectMany(s => s.AppliedPolicies
                      // Ignore calendar type, only worry about "work" types
                      .Where(p => p.PolicyReason.EntitlementType?.CalendarType() != true)
                      // Get only where usage is zero or less
                      .SelectMany(p => p.Usage
                      // Get our container for comparison, minutes will always be zero, leave it default
                      .Select(u => new ReportDataContainer()
                      {
                          PolicyCode = p.Policy.Code,
                          DateUsed = u.DateUsed,
                          Scheduled = Convert.ToInt32(u.MinutesInDay)
                      })))
                  .ToList();

                var tors = c.Segments
                    .Where(s => s.Type == CaseType.Intermittent)
                    .SelectMany(s => s.UserRequests)
                    // Filter just to approved minutes, and get data containers
                    .SelectMany(r => r.Detail.Where(m => m.Approved > 0)
                        .Select(d => new ReportDataContainer()
                        {
                            PolicyCode = d.PolicyCode,
                            DateUsed = r.RequestDate,
                            // We only care about approved minutes
                            Minutes = d.Approved,
                            // Need to get the scheduled minutes for this date and policy
                            Scheduled = usage.FirstOrDefault(u => u.DateUsed.Date == r.RequestDate.Date && u.PolicyCode == d.PolicyCode && u.Scheduled < d.Approved)?.Scheduled ?? 0
                        }))
                    .ToList();



                // Determine the exceptions where zero days intersect with approved TORs (> 0 time approved)
                var exceptions = tors.Where(t => t.Minutes > t.Scheduled).ToList();
                // This will ensure we only add items to the detail that are actually exceptions
                foreach (var item in exceptions.GroupBy(e => new { e.DateUsed, e.Minutes, e.Scheduled }))
                {
                    // Build the result item data
                    dynamic data = new ResultData();

                    data.EmployeeId = new LinkItem() { Value = emp.EmployeeNumber, Link = string.Format("/Employees/{0}/View", emp.Id) };
                    data.EmployeeName = new LinkItem() { Value = emp.FullName, Link = string.Format("/Employees/{0}/View", emp.Id) };
                    data.CaseNumber = new LinkItem() { Value = c.CaseNumber, Link = string.Format("/Cases/{0}/View", c.Id) };
                    data.RequestedDate = item.Key.DateUsed.ToUIString();
                    data.RequestedTime = item.Key.Minutes.ToFriendlyTime();
                    data.ScheduledTime = item.Key.Scheduled.ToFriendlyTime();
                    data.PolicyCode = string.Join("; ", item.Select(i => i.PolicyCode));
                    data.AssignedTo = c.AssignedToName;
                    data.EmployeeLastModified = emp.ModifiedDate.ToUIString();
                    data.ScheduleLastEffective = emp.WorkSchedules.Any() ? emp.WorkSchedules.Max(s => s.StartDate).ToUIString() : "";

                    result.Items.Add(data);
                }
            }
        }
    }
}
