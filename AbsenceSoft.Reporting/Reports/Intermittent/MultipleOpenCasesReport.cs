﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Reporting.Reports.IntermittentPattern
{
    [Serializable]
    public class MultipleOpenCasesReport : BaseReport
    {
        public const string ReportId = "92D6ACC5-A85A-424C-9E5A-CD98037F7D48";

        public override string Id { get { return ReportId; } }

        /// <summary>
        /// Category for all Absence Status reports is "Absence Status".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all intermittent pattern utilization reports is "Intermittent".
        /// </summary>
        public override string Category { get { return "Intermittent"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "Multiple Open Cases"; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }

        /// <summary>
        /// Icon to set for intermittent pattern report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_intermittent.png"; } }

        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns>LOA Feature</returns>
        public override IEnumerable<Feature> RequiredFeatures()
        {
            yield return Feature.LOA;
        }

        /// <summary>
        /// Determines whether the specified report is visible to the given user for any valid reason.
        /// </summary>
        /// <param name="user">The user to check against.</param>
        /// <returns>
        ///   <c>true</c> if the specified report is visible; otherwise, <c>false</c>.
        /// </returns>
        public override bool IsVisible(User user)
        {
            // HACK, AT-534 and AT-535 not yet approved, so for now, never show it.
            return false;
        }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get { return null; } }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria();

            ReportCriteriaItem item = GetCaseStatusCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            criteria.CustomerId = user.CustomerId;
            criteria.StartDate = DateTime.UtcNow.Date.SameDayLastYear();
            criteria.EndDate = DateTime.UtcNow.Date;

            item = GetAbsenceReasonCriteria(user);
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);
            criteria.Filters.Add(GetWorkStateCriteria(user, criteria.StartDate, criteria.EndDate));
            criteria.Filters.Add(GetLocationCriteria(user));
            criteria.Filters.Add(GetEmployeeCriteria(user));
            return criteria;
        }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override void RunReport(User user, ReportResult result)
        {
            DateTime startDate = result.Criteria.StartDate.ToMidnight();
            DateTime endDate = result.Criteria.EndDate.EndOfDay();

            List<IMongoQuery> criteria = new List<IMongoQuery>
            {
                user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "Employee._id")
            };
            ApplyEmployerFilter<Case>(criteria, result.Criteria);

            criteria.Add(Case.Query.GTE(c => c.StartDate, new BsonDateTime(startDate)));
            criteria.Add(Case.Query.LTE(c => c.EndDate, new BsonDateTime(endDate)));

            var rc = result.Criteria;
            rc.ApplyForOptionalFilter<CaseStatus>(CriteriaName.CaseStatus, v => criteria.Add(Case.Query.EQ(e => e.Status, v)));
            rc.ApplyForOptionalFilter<string>(CriteriaName.AbsenceReason, v => criteria.Add(Case.Query.EQ(e => e.Reason.Id, v)));
            rc.ApplyForOptionalFilter<string>(CriteriaName.WorkState, v => criteria.Add(Case.Query.EQ(e => e.Employee.WorkState, v)));
            rc.ApplyForOptionalFilter<string>(CriteriaName.Location, v => criteria.Add(Case.Query.EQ(e => e.Employee.Info.OfficeLocation, v)));
            rc.ApplyForOptionalFilter<string>(CriteriaName.EmployeeNumber, v => criteria.Add(Case.Query.EQ(e => e.Employee.EmployeeNumber, v)));

            var cases = Case.Query.Find(Case.Query.And(criteria)).ToList();
            var multipleOpenCases = cases
                .GroupBy(g => new
                {
                    EmployeeId = g.Employee.Id,
                    EmployeeName = g.Employee.FullName,
                    g.Employee.EmployeeNumber
                })
                .Where(w => w.Count() > 1)
                .ToList();

            result.Groups = multipleOpenCases.Select(group =>
            {
                var labelkey = string.Format("{0}, {1}", group.Key.EmployeeName, group.Key.EmployeeNumber);
                return new ResultDataGroup
                {
                    Key = labelkey,
                    Label = labelkey,
                    Items = group.Select(@case =>
                    {
                        dynamic data = new ResultData();
                        data.CaseNumber = new LinkItem() { Value = @case.CaseNumber, Link = string.Format("/Cases/{0}/View", @case.Id) };
                        data.EmployeeId = new LinkItem() { Value = @case.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", @case.Employee.Id) };
                        data.StartDate = @case.StartDate.ToShortDateString();
                        data.EndDate = @case.StartDate.ToShortDateString();

                        if (@case.Reason != null && @case.Reason.Code == "FHC")
                        {
                            data.Reason = string.Format(
                                "{0}, {1} {2} ({3}) ",
                                @case.Reason.Name,
                                @case.Contact.Contact.FirstName,
                                @case.Contact.Contact.LastName,
                                @case.Contact.ContactTypeName
                            );
                        }
                        else
                        {
                            data.Reason = @case.Reason?.Name;
                        }
                        return data as ResultData;
                    })
                    .ToList()
                };
            }).ToList();
        }
    }
}
