﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Reporting.Reports.IntermittentPattern
{
    [Serializable]
    public class CasesExceedFrequencyDurationReport : BaseReport
    {
        protected const string MinimumExceedsFrequencyCountName = "MinimumExceedsFrequencyCount";
        protected const string MinimumExceedsDurationCountName = "MinimumExceedsDurationCount";
        private const string DetailLevelBasic = "Basic";
        private const string DetailLevelFull = "Full";
        private const string DetailLevelName = "DetailLevel";

        public override string Id { get { return "4A7F5DF2-602B-4844-8FB9-FC6800F56856"; } }

        /// <summary>
        /// Category for all Absence Status reports is "Absence Status".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all intermittent pattern utilization reports is "Intermittent".
        /// </summary>
        public override string Category { get { return "Intermittent"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "Cases Exceeding Frequency or Duration"; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return false; } }

        /// <summary>
        /// Icon to set for intermittent pattern report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_intermittent.png"; } }

        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns>LOA Feature</returns>
        public override IEnumerable<Feature> RequiredFeatures()
        {
            yield return Feature.LOA;
        }

        /// <summary>
        /// Determines whether the specified report is visible to the given user for any valid reason.
        /// </summary>
        /// <param name="user">The user to check against.</param>
        /// <returns>
        ///   <c>true</c> if the specified report is visible; otherwise, <c>false</c>.
        /// </returns>
        public override bool IsVisible(User user)
        {
            // HACK, AT-534 and AT-535 not yet approved, so for now, never show it.
            // HACK for CVS demo in STAGE.
            return user?.CustomerId == "58e694f0c5f32f18b874d7b0"; // Customer = "AS Demo"
        }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get { return null; } }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerId,
                StartDate = DateTime.UtcNow.Date.SameDayLastYear(),
                EndDate = DateTime.UtcNow.Date
            };

            ReportCriteriaItem item = GetCaseStatusCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetAbsenceReasonCriteria(user);
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);
            criteria.Filters.Add(GetWorkStateCriteria(user, criteria.StartDate, criteria.EndDate));
            criteria.Filters.Add(GetLocationCriteria(user));
            criteria.Filters.Add(GetEmployeeCriteria(user));
            criteria.Filters.Add(new ReportCriteriaItem
            {
                Name = MinimumExceedsFrequencyCountName,
                Prompt = "Minimum Exceeds Frequency Count",
                Required = false,
                ControlType = ControlType.TextBox,
                Value = 1
            });
            criteria.Filters.Add(new ReportCriteriaItem
            {
                Name = MinimumExceedsDurationCountName,
                Prompt = "Minimum Exceeds Duration Count",
                Required = false,
                ControlType = ControlType.TextBox,
                Value = 1
            });
            criteria.Filters.Add(new ReportCriteriaItem
            {
                Name = DetailLevelName,
                Prompt = "Report Detail Level",
                Required = true,
                ControlType = ControlType.SelectList,
                Value = DetailLevelBasic,
                Options = new List<ReportCriteriaItemOption>
                {
                    new ReportCriteriaItemOption(DetailLevelBasic),
                    new ReportCriteriaItemOption(DetailLevelFull)
                }
            });
            return criteria;
        }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override void RunReport(User user, ReportResult result)
        {
            DateTime startDate = result.Criteria.StartDate.ToMidnight();
            DateTime endDate = result.Criteria.EndDate.EndOfDay();

            List<IMongoQuery> criteria = new List<IMongoQuery>
            {
                user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "Employee._id")
            };
            ApplyEmployerFilter<Case>(criteria, result.Criteria);

            criteria.Add(Case.Query.ElemMatch(c => c.Segments, q => q.And(
                q.EQ(s => s.Type, CaseType.Intermittent),
                q.ElemMatch(s => s.UserRequests, q2 => q2.And(
                    q2.GTE(e => e.RequestDate, new BsonDateTime(startDate)),
                    q2.LTE(e => e.RequestDate, new BsonDateTime(endDate)))))));

            var rc = result.Criteria;
            var isDetailed = rc.ValueOf<string>(DetailLevelName) == DetailLevelFull;
            var minimumExceedsFrequencyCount = rc.ValueOf<int>(MinimumExceedsFrequencyCountName);
            var minimumExceedsDurationCount = rc.ValueOf<int>(MinimumExceedsDurationCountName);
            rc.ApplyForOptionalFilter<CaseStatus>(CriteriaName.CaseStatus, v => criteria.Add(Case.Query.EQ(e => e.Status, v)));
            rc.ApplyForOptionalFilter<string>(CriteriaName.AbsenceReason, v => criteria.Add(Case.Query.EQ(e => e.Reason.Id, v)));
            rc.ApplyForOptionalFilter<string>(CriteriaName.WorkState, v => criteria.Add(Case.Query.EQ(e => e.Employee.WorkState, v)));
            rc.ApplyForOptionalFilter<string>(CriteriaName.Location, v => criteria.Add(Case.Query.EQ(e => e.Employee.Info.OfficeLocation, v)));
            rc.ApplyForOptionalFilter<string>(CriteriaName.EmployeeNumber, v => criteria.Add(Case.Query.EQ(e => e.Employee.EmployeeNumber, v)));

            var cases = Case.Query.Find(Case.Query.And(criteria)).ToList();
            var caseCertifications =
                from c in cases
                from cert in c.Certifications
                let requests = c.Segments.SelectMany(m => m.UserRequests)
                    .Where(ur => ur.RequestDate.IsInRange(cert.StartDate, cert.EndDate))
                    .ToList()
                let exceededDurations = requests.GetExceededDurations(cert.FrequencyType, cert.Duration, cert.DurationType).ToList()
                let exceededDurationDefault = exceededDurations.DefaultIfEmpty(MetricComparison.Zero)
                select new
                {
                    Case = c,
                    Certification = cert,
                    ExceededFrequenciesCount = requests.GetExceededFrequencies(cert.Frequency, cert.FrequencyType).Count(),
                    ExceededDurationsCount = exceededDurations.Count,
                    ExceededDurationsAverage = exceededDurationDefault.Average(a => a.Difference),
                    ExceededurationsMin = exceededDurationDefault.Min(a => a.Difference),
                    ExceededurationsMax = exceededDurationDefault.Max(a => a.Difference),
                    ExceededDurationStdDev = exceededDurationDefault.Select(a => a.Difference).StdDevPop()
                };

            result.Items = caseCertifications
                .Where(w =>
                    w.ExceededFrequenciesCount >= minimumExceedsFrequencyCount
                    && w.ExceededDurationsCount >= minimumExceedsDurationCount
                )
                .Select(item =>
                {
                    dynamic data = new ResultData();

                    var emp = item.Case.Employee;
                    data.EmployeeId = new LinkItem() { Value = emp.EmployeeNumber, Link = string.Format("/Employees/{0}/View", emp.Id) };
                    data.EmployeeName = new LinkItem() { Value = emp.FullName, Link = string.Format("/Employees/{0}/View", emp.Id) };
                    data.CaseNumber = new LinkItem() { Value = item.Case.CaseNumber, Link = string.Format("/Cases/{0}/View", item.Case.Id) };
                    data.Status = item.Case.Status.ToString();

                    var cert = item.Certification;
                    data.CertifiedStartDate = cert.StartDate.ToShortDateString();
                    data.CertifiedEndDate = cert.EndDate.ToShortDateString();
                    data.CertifiedDescription = cert.ToString();
                    if (isDetailed)
                    {
                        data.CertifiedOccurances = cert.Occurances;
                        data.CertifiedFrequency = cert.Frequency;
                        data.CertifiedFrequencyType = cert.FrequencyType.ToString();
                        data.CertifiedDuration = cert.Duration;
                        data.CertifiedDurationType = cert.DurationType.ToString();
                    }

                    data.ExceedsFrequencyCount = item.ExceededFrequenciesCount;
                    data.ExceedsDurationCount = item.ExceededDurationsCount;
                    data.ExceedsDurationAverage = Math.Round(item.ExceededDurationsAverage, 2);
                    if (isDetailed)
                    {
                        data.ExceedsDurationMin = Math.Round(item.ExceededurationsMin, 2);
                        data.ExceedsDurationMax = Math.Round(item.ExceededurationsMax, 2);

                        data.ExceededDurationsStdDev = Math.Round(item.ExceededDurationStdDev, 2);
                    }

                    return data as ResultData;
                }).ToList();
        }
    }
}
