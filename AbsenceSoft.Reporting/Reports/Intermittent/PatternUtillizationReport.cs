﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Reporting.Reports.IntermittentPattern
{
    [Serializable]
    public class IntermittentPatternReport : BaseReport
    {
        public override string Id { get { return "99e99082-130b-433b-a053-a8b425636b8d"; } }

        /// <summary>
        /// Category for all Absence Status reports is "Absence Status".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all intermittent pattern utilization reports is "Intermittent".
        /// </summary>
        public override string Category { get { return "Intermittent"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "Pattern Utilization Report"; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return false; } }

        /// <summary>
        /// Icon to set for intermittent pattern report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_intermittent.png"; } }

        /// <summary>
        /// To be used for ESS category
        /// </summary>
        public override string ESSCategoryTitle { get { return "Intermittent"; } }
        public override ESSReportDetail ESSReportDetails { get { return new ESSReportDetail() { Title = "Intermittent Pattern & Usage Report", Type = ESSReportType.TeamReport, ESSReportName = "Intermittent Pattern & Usage Report" }; } }

        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns>LOA Feature</returns>
        public override IEnumerable<Feature> RequiredFeatures()
        {
            yield return Feature.LOA;
        }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get { return null; } }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerId,
                StartDate = DateTime.UtcNow.Date.SameDayLastYear(),
                EndDate = DateTime.UtcNow.Date
            };
            criteria.Filters.Add(new ReportCriteriaItem()
            {
                ControlType = ControlType.RadioButtonList,
                Name = "Pattern",
                Prompt = "Pattern Utilization",
                Required = false,
                Tweener = "includes",
                Value = null,
                Options = new List<ReportCriteriaItemOption>()
                {
                    new ReportCriteriaItemOption() { Value = "1", Text = "Monday/Friday" },
                    new ReportCriteriaItemOption() { Value = "2", Text = "1 Hour or Less" },
                    new ReportCriteriaItemOption() { Value = "3", Text = "Weekdays" }
                }
            });

            ReportCriteriaItem item = GetAbsenceReasonCriteria(user);
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);
            criteria.Filters.Add(GetWorkStateCriteria(user, criteria.StartDate, criteria.EndDate));
            criteria.Filters.AddRange(GetOrganizationTypeCriteria(user));
            criteria.Filters.Add(GetEmployeeCriteria(user));
            return criteria;
        }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override void RunReport(User user, ReportResult result)
        {
            DateTime startDate = result.Criteria.StartDate.ToMidnight();
            DateTime endDate = result.Criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>
            {
                user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "Employee._id")
            };
            
            ApplyEmployerFilter<Case>(ands, result.Criteria);
            ands.Add(Case.Query.NE(e => e.Status, CaseStatus.Cancelled));
            ands.Add(Case.Query.ElemMatch(c => c.Segments, q => q.And(
                q.EQ(s => s.Type, CaseType.Intermittent),
                q.ElemMatch(s => s.UserRequests, q2 => q2.And(
                    q2.GTE(e => e.RequestDate, new BsonDateTime(startDate)),
                    q2.LTE(e => e.RequestDate, new BsonDateTime(endDate)))))));

            string pattern = "0"; // All

            if (result.Criteria.Filters != null && result.Criteria.Filters.Any())
            {
                SetAbenseReasonFilter(result, ands);
                SetWorkStateFilter(result, ands);
                SetOfficeLocationFilter(result, ands);
                SetEmployeeNumberFilter(result, ands);
                pattern = GetPattern(result);
            }
            if (string.IsNullOrWhiteSpace(pattern))
            {
                pattern = "0";
            }

            var query = Case.Query.Find(Case.Query.And(ands)).GroupBy(r => r.Employee.Id).ToList();

            List<ResultData> resultItems = new List<ResultData>();

            // Loop through each employee group to build our results
            foreach (var group in query)
            {
                // Should never ever get this, but just in case, here's a little security measure.
                if (!group.Any())
                {
                    continue;
                }

                // Get a flat list of all time off requests for all cases for an employee 
                //  for all dates falling within the date range
                var absences = group
                    .SelectMany(g => g.Segments.Where(s => s.Type == CaseType.Intermittent))
                    .SelectMany(s => s.UserRequests.Where(r => r.RequestDate.DateInRange(result.Criteria.StartDate, result.Criteria.EndDate)))
                    .ToList();

                // If there are none, which is odd 'cause the query should have already excluded this 
                //  guy, then just move on to the next employee group
                if (!absences.Any())
                {
                    continue;
                }

                // Grab the employee from the first case, just need the name + employee number.
                Employee emp = group.First().Employee;
                SetAbsenceCount(pattern, resultItems, absences, emp);
            }

            result.Items = resultItems;
        }

        private void SetAbsenceCount(string pattern, List<ResultData> resultItems, List<IntermittentTimeRequest> absences, Employee emp)
        {
            SetMondayFridayAbsenceCount(pattern, resultItems, absences, emp);
            SetLessThanHourAbsenceCount(pattern, resultItems, absences, emp);
            SetTuesdayAbsenceCount(pattern, resultItems, absences, emp);
            SetWednesdayAbsenceCount(pattern, resultItems, absences, emp);
            SetThursdayAbsenceCount(pattern, resultItems, absences, emp);
        }

        private void SetLessThanHourAbsenceCount(string pattern, List<ResultData> resultItems, List<IntermittentTimeRequest> absences, Employee emp)
        {
            // Less than 1 Hour: Gets a count of all absences that total less than 1 hour for the day
            if (pattern == "0" || pattern == "2")
            {
                int pat = absences.Count(a => a.TotalMinutes <= 60);
                // Ensure there is more than a single occurrence and at a frequency of at least 30%
                if (pat > 1 && ((pat / (decimal)absences.Count) >= 0.3M))
                {
                    resultItems.Add(BuildResult(emp, "1 Hour or Less", absences.Count, pat));
                }
            }
        }

        private void SetMondayFridayAbsenceCount(string pattern, List<ResultData> resultItems, List<IntermittentTimeRequest> absences, Employee emp)
        {
            // Monday/Friday: Gets a count of all absences that occur on Monday or Friday
            if (pattern == "0" || pattern == "1")
            {
                int pat = absences.Count(a => a.RequestDate.DayOfWeek == DayOfWeek.Friday || a.RequestDate.DayOfWeek == DayOfWeek.Monday);
                // Ensure there is more than a single occurrence and at a frequency of at least 30%
                if (pat >= 1 && ((pat / (decimal)absences.Count) >= 0.3M))
                {
                    resultItems.Add(BuildResult(emp, "Monday/Friday", absences.Count, pat));
                }
            }
        }

        private void SetTuesdayAbsenceCount(string pattern, List<ResultData> resultItems, List<IntermittentTimeRequest> absences, Employee emp)
        {
            // Day of Week - Tuesday: Gets a count of all absences that occur on Tuesday
            if (pattern == "0" || pattern == "3")
            {
                int pat = absences.Count(a => a.RequestDate.DayOfWeek == DayOfWeek.Tuesday);
                // Ensure there is more than a single occurrence and at a frequency of at least 30%
                if (pat >= 1 && ((pat / (decimal)absences.Count) >= 0.3M))
                {
                    resultItems.Add(BuildResult(emp, "Weekday - Tuesday", absences.Count, pat));
                }
            }
        }

        private void SetWednesdayAbsenceCount(string pattern, List<ResultData> resultItems, List<IntermittentTimeRequest> absences, Employee emp)
        {
            // Day of Week - Wednesday: Gets a count of all absences that occur on Wednesday
            if (pattern == "0" || pattern == "3")
            {
                int pat = absences.Count(a => a.RequestDate.DayOfWeek == DayOfWeek.Wednesday);
                // Ensure there is more than a single occurrence and at a frequency of at least 30%
                if (pat >= 1 && ((pat / (decimal)absences.Count) >= 0.3M))
                {
                    resultItems.Add(BuildResult(emp, "Weekday - Wednesday", absences.Count, pat));
                }
            }
        }

        private void SetThursdayAbsenceCount(string pattern, List<ResultData> resultItems, List<IntermittentTimeRequest> absences, Employee emp)
        {
            // Day of Week - Thursday: Gets a count of all absences that occur on Thursday
            if (pattern == "0" || pattern == "3")
            {
                int pat = absences.Count(a => a.RequestDate.DayOfWeek == DayOfWeek.Thursday);
                // Ensure there is more than a single occurrence and at a frequency of at least 30%
                if (pat >= 1 && ((pat / (decimal)absences.Count) >= 0.3M))
                {
                    resultItems.Add(BuildResult(emp, "Weekday - Thursday", absences.Count, pat));
                }
            }
        }

        private static string GetPattern(ReportResult result)
        {
            string pattern = "0";
            var patternCriteria = result.Criteria.Filters.FirstOrDefault(m => m.Name == "Pattern");
            if (patternCriteria != null && patternCriteria.Value != null)
            {
                pattern = patternCriteria.ValueAs<string>();
            }
            return pattern;
        }

        private void SetEmployeeNumberFilter(ReportResult result, List<IMongoQuery> ands)
        {
            var employeeCriteria = result.Criteria.Filters.FirstOrDefault(m => m.Name == "EmployeeNumber");
            if (employeeCriteria != null && employeeCriteria.Value != null && !string.IsNullOrWhiteSpace(employeeCriteria.Value.ToString()))
            {
                ands.Add(Case.Query.EQ(e => e.Employee.EmployeeNumber, employeeCriteria.Value.ToString().Trim()));
            }
        }

        private void SetAbenseReasonFilter(ReportResult result, List<IMongoQuery> ands)
        {
            var absenceReasonCriteria = result.Criteria.Filters.FirstOrDefault(m => m.Name == "AbsenceReason");
            if (absenceReasonCriteria != null && absenceReasonCriteria.Value != null)
            {
                ands.Add(Case.Query.EQ(e => e.Reason.Id, absenceReasonCriteria.Value));
            }
        }

        private void SetWorkStateFilter(ReportResult result, List<IMongoQuery> ands)
        {
            var workStateCriteria = result.Criteria.Filters.FirstOrDefault(m => m.Name == "WorkState");
            if (workStateCriteria != null && workStateCriteria.Value != null)
            {
                ands.Add(Case.Query.EQ(e => e.Employee.WorkState, workStateCriteria.Value));
            }
        }

        private void SetOfficeLocationFilter(ReportResult result, List<IMongoQuery> ands)
        {
            var officeLocation = result.Criteria.Filters.FirstOrDefault(m => m.Name.ToUpper() == "OFFICELOCATION");
            if (officeLocation != null && (officeLocation.Value != null || officeLocation.Values.Any()))
            {
                if (officeLocation.Value != null)
                {
                    ands.Add(Case.Query.EQ(e => e.Employee.Info.OfficeLocation, officeLocation.Value));
                }
                else
                {
                    ands.Add(Case.Query.In(e => e.Employee.Info.OfficeLocation, officeLocation.Values));
                }
            }
        }

        /// <summary>
        /// Builds the result.
        /// </summary>
        /// <param name="emp">The emp.</param>
        /// <param name="pattern">The pattern.</param>
        /// <param name="absences">The absences.</param>
        /// <param name="occurs">The occurs.</param>
        /// <returns></returns>
        private ResultData BuildResult(Employee emp, string pattern, int absences, int occurs)
        {
            dynamic data = new ResultData();

            data.EmployeeId = new LinkItem() { Value = emp.EmployeeNumber, Link = string.Format("/Employees/{0}/View", emp.Id) };
            data.EmployeeName = new LinkItem() { Value = emp.FullName, Link = string.Format("/Employees/{0}/View", emp.Id) };
            data.UtilizationPattern = pattern;
            data.TotalIntAbsences = absences.ToString("N0");
            data.TotalPatternOccurences = occurs.ToString("N0");
            data.PatternRatio = absences == 0 ? "N/A" : (occurs / (decimal)absences).ToString("P0");

            return data;
        }
    }
}
