﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Reporting.Reports.AccommodationStatus
{
    [Serializable]
    public abstract class AccommodationStatusReport : BaseReport
    {

        /// <summary>
        /// Category for all Accommodation Status reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Accommodation Status Report".
        /// </summary>
        public override string Category { get { return "Accommodation Status Report"; } }

        /// <summary>
        /// Icon to set for Accommodation Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns>ADA Feature</returns>
        public override IEnumerable<Feature> RequiredFeatures()
        {
            yield return Feature.ADA;
        }

        /// <summary>
        /// Determines whether the specified user is visible.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public override bool IsVisible(User user)
        {
            // Ensure customer is not parrot.
            return user != null && user.CustomerId != "546e5097a32aa00d60e3210a";
        }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get { return null; } }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerId,
                StartDate = DateTime.UtcNow.Date.AddDays(-30),
                EndDate = DateTime.UtcNow.Date
            };
            ReportCriteriaItem item = GetCaseStatusCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetAccomTypeCriteria(user);
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetAdjudicationCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            criteria.Filters.Add(GetCaseAssigneeCriteria(user));
            criteria.Filters.Add(GetWorkStateCriteria(user, criteria.StartDate, criteria.EndDate));
            criteria.Filters.AddRange(GetOrganizationTypeCriteria(user));
            if (IncludeOrganizationGrouping) criteria.Filters.Add(GetOrganizationTypeGroupByCriteria(user));
            return criteria;
        }

        internal string FormatEmployeeName(Employee employee)
        {
            return string.Format("{0}, {1} {2}",
                    employee.LastName,
                    employee.FirstName,
                    employee.MiddleName != null ? employee.MiddleName.Substring(0, 1) : string.Empty
                );
        }

        /// <summary>
        /// Runs the query.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        protected virtual List<Case> RunQuery(User user, ReportCriteria criteria)
        {
            List<IMongoQuery> ands = BuildFilter(user, criteria);

            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null);
            return query.ToList();
        }

        /// <summary>
        /// To be used for ESS category
        /// </summary>
        public override string ESSCategoryTitle { get { return "ADA Report"; } }

        internal string GetCaseClosedDate(Case @case)
        {
            var closedDate = @case.GetClosedDate();
            return closedDate.HasValue ? closedDate.Value.ToShortDateString() : string.Empty;
        }

        internal string GetCaseApprovedBy(Case @case)
        {
            return @case.AssignedToName;
        }

        internal void ReportAdditionalQuestions(ResultData rd, ICollection<AccommodationInteractiveProcess.Step> steps)
        {
            foreach (var step in steps.Where(s => !string.IsNullOrWhiteSpace(s.ReportLabel)))
            {
                rd[step.ReportLabel] = step.SAnswer ?? String.Empty;
            }
        }

        protected virtual List<IMongoQuery> BuildFilter(User user, ReportCriteria criteria)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "Employee._id"));
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            ApplyEmployerFilter<Case>(ands, criteria);
            ands.Add(Case.Query.EQ(e => e.IsAccommodation, true));

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
            ));

            if (criteria.Filters.Count != 0)
            {
                if (criteria.Filters.Any(m => m.Name == "CaseStatus" && m.Value != null))
                {
                    CaseStatus enumCaseStatusFilter = criteria.ValueOf<CaseStatus>("CaseStatus");
                    ands.Add(Case.Query.EQ(c => c.Status, enumCaseStatusFilter));
                }

                List<IMongoQuery> elemMatchAnds = new List<IMongoQuery>();

                if (criteria.Filters.Any(m => m.Name == "Determination" && m.Value != null))
                {
                    AdjudicationStatus enumadjStatusFilter = criteria.ValueOf<AdjudicationStatus>("Determination");
                    elemMatchAnds.Add(Query<Accommodation>.EQ(a => a.Determination, enumadjStatusFilter));
                }

                if (criteria.Filters.Any(m => m.Name == "AccomType" && m.Value != null))
                {
                    elemMatchAnds.Add(Query<Accommodation>.EQ(a => a.Type.Code, criteria.Filters.FirstOrDefault(m => m.Name == "AccomType").Value));
                }

                if (elemMatchAnds.Any())
                {
                    ands.Add(Case.Query.ElemMatch(c => c.AccommodationRequest.Accommodations, q => q.And(elemMatchAnds)));
                }

                if (criteria.Filters.Any(m => m.Name == "WorkState" && m.Value != null))
                {
                    ands.Add(Case.Query.EQ(e => e.Employee.WorkState, criteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value));
                }

                AppendOrganizationFilterQuery(user, criteria, queries: ands, employeeIdField: "Employee._id");

                if (criteria.Filters.Any(m => m.Name == "CaseAssignee" && m.Value != null && !string.IsNullOrWhiteSpace(m.ValueAs<string>())))
                {
                    ands.Add(Case.Query.EQ(e => e.AssignedToId, criteria.Filters.FirstOrDefault(m => m.Name == "CaseAssignee").Value));
                }

                var reasonCode = criteria.Filters.FirstOrDefault(m => m.Name == "ReasonCode");
                if (reasonCode != null && reasonCode.Value != null)
                {
                    ands.Add(Case.Query.EQ(c => c.Reason.Code, reasonCode.Value));
                }
            }

            return ands;
        }

        protected virtual ResultData GetDetail(Case @case, Accommodation q, AccommodationUsage detail, ref string displayCaseNumber, ref string displayFullName)
        {
            dynamic data = new ResultData();
            DateTime? displayRTW = GetCaseEventDate(@case, CaseEventType.ReturnToWork);
            DateTime? displayERTW = GetCaseEventDate(@case, CaseEventType.EstimatedReturnToWork);

            data.CaseNumber = string.IsNullOrWhiteSpace(displayCaseNumber) ? null : new LinkItem() { Value = @case.CaseNumber, Link = string.Format("/Cases/{0}/View", @case.Id) };
            data.EmployeeNumber = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = @case.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", @case.Employee.Id) };
            data.Name = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = @case.Employee.FullName, Link = string.Format("/Employees/{0}/View", @case.Employee.Id) };
            data.CaseStatus = @case.Status.ToString().SplitCamelCaseString();
            data.Duration = q.Duration == 0 ? "Pending" : q.Duration.ToString().SplitCamelCaseString();
            data.Type = q.Type.Name;
            data.Created = q.CreatedDate.ToString("MM/dd/yyyy");
            data.StartDate = detail.StartDate == DateTime.MinValue ? "Pending" : detail.StartDate.ToString("MM/dd/yyyy");
            data.EndDate = detail.EndDate.ToString("MM/dd/yyyy");
            data.Accommodations = string.Concat(q.Type.Code, " - ", detail.Determination.ToString().SplitCamelCaseString());
            data.AccommStatus = q.Status.ToString().SplitCamelCaseString();
            data.AssignedTo = @case.AssignedToName;
            data.WorkState = @case.Employee.WorkState;
            data.Location = @case.Employee.Info.OfficeLocation;
            data.ReturnToWork = displayRTW != null ? displayRTW.ToString("MM/dd/yyyy") : string.Empty;
            data.EstimatedReturnToWork = displayERTW != null ? displayERTW.ToString("MM/dd/yyyy") : string.Empty;

            displayCaseNumber = string.Empty;
            displayFullName = string.Empty;

            return data;
        }

        protected virtual ResultData GetBlankDetail(Case @case, ref string displayCaseNumber, ref string displayFullName)
        {
            dynamic data = new ResultData();
            DateTime? displayRTW = GetCaseEventDate(@case, CaseEventType.ReturnToWork);
            DateTime? displayERTW = GetCaseEventDate(@case, CaseEventType.EstimatedReturnToWork);

            data.CaseNumber = string.IsNullOrWhiteSpace(displayCaseNumber) ? null : new LinkItem() { Value = @case.CaseNumber, Link = string.Format("/Cases/{0}/View", @case.Id) };
            data.EmployeeNumber = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = @case.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", @case.Employee.Id) };
            data.Name = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = @case.Employee.FullName, Link = string.Format("/Employees/{0}/View", @case.Employee.Id) };
            data.Duration = @case.AccommodationRequest.SummaryDurationType;
            data.Type = "N/A";
            data.CaseStatus = @case.Status.ToString().SplitCamelCaseString();
            var evt = @case.FindCaseEvent(CaseEventType.CaseCreated);
            data.Created = (evt == null ? @case.CreatedDate : evt.EventDate).ToString("MM/dd/yyyy");
            data.StartDate = @case.StartDate.ToString("MM/dd/yyyy");
            data.EndDate = @case.EndDate.ToString("MM/dd/yyyy");
            data.Accommodations = "N/A";
            data.AccommStatus = "N/A";
            data.AssignedTo = @case.AssignedToName;
            data.WorkState = @case.Employee.WorkState;
            data.Location = @case.Employee.Info.OfficeLocation;
            data.ReturnToWork = displayRTW != null ? displayRTW.ToString("MM/dd/yyyy") : string.Empty;
            data.EstimatedReturnToWork = displayERTW != null ? displayERTW.ToString("MM/dd/yyyy") : string.Empty;

            displayCaseNumber = string.Empty;
            displayFullName = string.Empty;

            return data;
        }
    }
}
