﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Security;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Reporting.Reports.AccommodationStatus
{
    [Serializable]
    public sealed class AccommodationStatusByOfficeLocationReport : AccommodationStatusReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "F0CBD881-14A5-47D5-8C3A-BDD4178A3265"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "by Office Location"; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 3; } }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override void RunReport(User user, ReportResult result)
        {
            var queryList = RunQuery(user, result.Criteria);

            var groupQuery = queryList.GroupBy(n => n.Employee.Info.OfficeLocation ?? "None")
            .Select(g => new
            {
                label = g.Key,
                data = g.Select(x => x).ToList()
            }).OrderBy(m => m.label);

            List<ResultData> resultItems = new List<ResultData>();
            
            result.Groups = new List<ResultDataGroup>();
            int i = 1;
            foreach (var t in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (var q in t.data.Where(c => c.AccommodationRequest != null))
                {
                    q.AccommodationRequest.Accommodations = q.AccommodationRequest.Accommodations ?? new List<Accommodation>(0);
                    string displayCaseNumber = q.CaseNumber;
                    string displayFullName = q.Employee.FullName;

                    foreach (var accom in q.AccommodationRequest.Accommodations.Where(a => a.Type != null).OrderBy(p => p.Type.Code))
                        foreach (var detail in accom.Usage.Where(u => u.StartDate.DateRangesOverLap(u.EndDate, result.Criteria.StartDate, result.Criteria.EndDate)).OrderBy(u => u.StartDate))
                            resultItems.Add(GetDetail(q, accom, detail, ref displayCaseNumber, ref displayFullName));

                    if (!q.AccommodationRequest.Accommodations.Where(a => a.Type != null).Any())
                        resultItems.Add(GetBlankDetail(q, ref displayCaseNumber, ref displayFullName));
                }

                result.Groups.Add(new ResultDataGroup()
                {
                    Key = i,
                    Label = t.label,
                    Items = new List<ResultData>().AddRangeFluid(resultItems)
                });

                i++;
            }
        }
    }
}
