﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Reporting.Reports.AccommodationStatus
{
    [Serializable]
    public sealed class AccommodationStatusByOrganizationReport : AccommodationStatusReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "E086BFC7-4F3E-4AC9-AC12-21F5BD63152C"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "by Organization"; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 5; } }

        /// <summary>
        /// Include the Organization Grouping Criteria
        /// </summary>
        public override bool IncludeOrganizationGrouping { get { return true; } }


        public override ESSReportDetail ESSReportDetails { get { return new ESSReportDetail() { Title = "Accommodation Report", Type = ESSReportType.OrganizationReport, ESSReportName = "Accommodation Report" }; } }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override void RunReport(User user, ReportResult result)
        {
            var queryList = RunQuery(user, result.Criteria);

            string grouptypeCode = null;
            var customer = user.Customer;            
            if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "OrganizationTypeGrouping") != null)
                grouptypeCode = result.Criteria.ValueOf<string>("OrganizationTypeGrouping");
            List<string> customerIds = queryList.Select(cs => cs.CustomerId).Distinct().ToList();
            List<string> employerIds = queryList.Select(cs => cs.EmployerId).Distinct().ToList();
            List<string> employeeNumbers = queryList.Select(cs => cs.Employee.EmployeeNumber).Distinct().ToList();
            List<EmployeeOrganization> employeeOrganizations = null;
            if (grouptypeCode != null)
                employeeOrganizations = EmployeeOrganization.AsQueryable().Where(org => org.TypeCode == grouptypeCode && customerIds.Contains(org.CustomerId) && employerIds.Contains(org.EmployerId) && employeeNumbers.Contains(org.EmployeeNumber)).OrderByDescending(o => o.Dates).ToList();
            else
                employeeOrganizations = EmployeeOrganization.AsQueryable().Where(org => customerIds.Contains(org.CustomerId) && employerIds.Contains(org.EmployerId) && employerIds.Contains(org.EmployerId) && employeeNumbers.Contains(org.EmployeeNumber)).OrderByDescending(o => o.Dates).ToList();
            List<EmployeeOrganization> employeeUniqueOrganizations = new List<EmployeeOrganization>();
            foreach (EmployeeOrganization empOrg in employeeOrganizations)
            {
                if (empOrg.TypeCode != OrganizationType.OfficeLocationTypeCode ||
                    ((empOrg.Dates == null || empOrg.Dates.IncludesToday(true))
                    && (!employeeUniqueOrganizations.Where(empUniqueOrg => empUniqueOrg.TypeCode == OrganizationType.OfficeLocationTypeCode && empUniqueOrg.EmployeeNumber == empOrg.EmployeeNumber && empUniqueOrg.CustomerId == empOrg.CustomerId && empUniqueOrg.EmployerId == empOrg.EmployerId).Any())))
                {
                    employeeUniqueOrganizations.Add(empOrg);
                }
            }
            var groupQuery = employeeUniqueOrganizations.GroupBy(org => new { CustomerId = org.CustomerId, EmployerId = org.EmployerId, Code = org.Code })
           .Select(g => new
           {
               label = employeeUniqueOrganizations.Where(empOrgs => empOrgs.Code == g.Key.Code).FirstOrDefault().Name,
               data = g.Select(x => x).ToList()
           }).OrderBy(m => m.label);

            List<ResultData> resultItems = new List<ResultData>();
            
            result.Groups = new List<ResultDataGroup>();
            int i = 1;
            //Avoid displaying cases under more than one office location code.
            List<string> empLocationCase = new List<string>();
            foreach (var t in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (var org in t.data)
                {
                    foreach (Case td in queryList.Where(cs => cs.AccommodationRequest != null && cs.Employee.EmployeeNumber == org.EmployeeNumber && cs.CustomerId == org.CustomerId && cs.EmployerId == org.EmployerId))
                    {
                        if (!string.IsNullOrWhiteSpace(org.TypeCode) 
                            && empLocationCase != null
                            && org.TypeCode.ToUpper().TrimEnd() == OrganizationType.OfficeLocationTypeCode.ToUpper().TrimEnd() 
                            && empLocationCase.Contains(td.Id.ToUpper()))
                        {
                            continue;
                        }
                        
                        td.AccommodationRequest.Accommodations = td.AccommodationRequest.Accommodations ?? new List<Accommodation>(0);
                        string displayCaseNumber = td.CaseNumber;
                        string displayFullName = td.Employee.FullName;

                        foreach (var accom in td.AccommodationRequest.Accommodations.Where(a => a.Type != null).OrderBy(p => p.Type.Code))
                            foreach (var detail in accom.Usage.Where(u => u.StartDate.DateRangesOverLap(u.EndDate, result.Criteria.StartDate, result.Criteria.EndDate)).OrderBy(u => u.StartDate))
                                resultItems.Add(GetDetail(td, accom, detail, ref displayCaseNumber, ref displayFullName));

                        if (!td.AccommodationRequest.Accommodations.Where(a => a.Type != null).Any())
                            resultItems.Add(GetBlankDetail(td, ref displayCaseNumber, ref displayFullName));

                        if (!string.IsNullOrWhiteSpace(org.TypeCode) 
                            && empLocationCase != null
                            && org.TypeCode.ToUpper().TrimEnd() == OrganizationType.OfficeLocationTypeCode.ToUpper().TrimEnd() 
                            && (!empLocationCase.Contains(td.Id.ToUpper())))
                        {
                            empLocationCase.Add(td.Id.ToUpper());
                        }
                    }
                }

                if (resultItems.Count > 0)
                {
                    result.Groups.Add(new ResultDataGroup()
                    {
                        Key = i,
                        Label = t.label,
                        Items = new List<ResultData>().AddRangeFluid(resultItems)
                    });
                }

                i++;
            }
        }
    }
}
