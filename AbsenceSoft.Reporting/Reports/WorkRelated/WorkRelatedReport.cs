﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace AbsenceSoft.Reporting.Reports.WorkRelated
{
    [Serializable]
    public abstract class WorkRelatedReport : BaseReport
    {
        /// <summary>
        /// When overridden in a derived class, gets the name of the report main category to bifurcate reports.
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Icon to set for intermittent pattern report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_intermittent.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report category for display.
        /// </summary>
        public override string Category { get { return "Work Related"; } }

        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<Feature> RequiredFeatures()
        {
            yield return Feature.WorkRelatedReporting;
        }

        /// <summary>
        /// Determines whether the specified user is visible.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public override bool IsVisible(User user)
        {
            // Ensure customer is not oh.
            return user != null && user.CustomerId != "56b90041c5f32f05088a9e8d";
        }

        #region Methods to help build out the Criteria.

        protected List<ReportCriteriaItemOption> GetEnumOptions<TEnum>() where TEnum : struct
        {
            if (typeof(TEnum).IsEnum)
            {
                return Enum.GetValues(typeof(TEnum)).OfType<TEnum>().Select(e => new ReportCriteriaItemOption()
                {
                    Text = e.ToString().SplitCamelCaseString(),
                    Value = Convert.ToInt32(e)
                }).ToList();
            }

            return null;
        }

        protected ReportCriteriaItem GetYesNoOCriteria(string name, string prompt, bool required)
        {
            ReportCriteriaItem toReturn = new ReportCriteriaItem
            {
                Name = name,
                Prompt = prompt,
                Required = required,
                ControlType = ControlType.RadioButtonList,
                Options = (new[] { "Yes", "No" }).Select(e => new ReportCriteriaItemOption(e)).ToList()
            };
            return toReturn;
        }

        protected ReportCriteriaItem GetReportCriteriaItem<T>(string name, string prompt, bool required, ControlType controlType, List<ReportCriteriaItemOption> options = null)
        {
            ReportCriteriaItem toReturn = new ReportCriteriaItem
            {
                Name = name,
                Prompt = prompt,
                Required = required,
                ControlType = controlType,
                Options = options
            };

            return toReturn;
        }

        protected virtual IEnumerable<ReportCriteriaItem> GetSharpsCriteria(User user)
        {
            yield return GetReportCriteriaItem<WorkRelatedSharpsInjuryLocation>("WorkRelatedSharpsInjuryLocation", "Inury Location", false, ControlType.CheckBoxList, GetEnumOptions<WorkRelatedSharpsInjuryLocation>());
            yield return GetReportCriteriaItem<WorkRelatedSharpsInjuryLocation>("WorkRelatedSharpsLocation", "Location/Department", false, ControlType.CheckBoxList, GetEnumOptions<WorkRelatedSharpsLocation>());
            yield return GetReportCriteriaItem<WorkRelatedSharpsInjuryLocation>("WorkRelatedSharpsProcedure", "Procedure", false, ControlType.CheckBoxList, GetEnumOptions<WorkRelatedSharpsProcedure>());
            yield return GetReportCriteriaItem<WorkRelatedSharpsJobClassification>("WorkRelatedSharpsJobClassification", "Job Classification", false, ControlType.CheckBoxList, GetEnumOptions<WorkRelatedSharpsJobClassification>());
            yield return GetLocationCriteria(user);
        }

        protected virtual IEnumerable<ReportCriteriaItem> GetWorkRelatedCriteria(User user)
        {
            yield return GetReportCriteriaItem<WorkRelatedCaseClassification>("WorkRelatedCaseClassification", "Case Classification", false, ControlType.CheckBoxList, GetEnumOptions<WorkRelatedCaseClassification>());
            yield return GetReportCriteriaItem<WorkRelatedCaseClassification>("WorkRelatedTypeOfInjury", "Type of Injury", false, ControlType.CheckBoxList, GetEnumOptions<WorkRelatedTypeOfInjury>());
            yield return GetLocationCriteria(user);
        }

        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerId,
                StartDate = DateTime.UtcNow.Date.AddDays(-30),
                EndDate = DateTime.UtcNow.Date
            };

            return criteria;
        }

        #endregion

        #region Retrieve the selected filters and apply to the Query

        protected void ApplySharpsFilters(ReportCriteria criteria, List<IMongoQuery> ands)
        {
            AddMultiFilter<IEnumerable<WorkRelatedSharpsInjuryLocation>>(c => c.WorkRelated.SharpsInfo.InjuryLocation, "WorkRelatedCaseClassification", criteria, ands);
            AddMultiFilter(c => c.WorkRelated.SharpsInfo.LocationAndDepartment, "WorkRelatedSharpsLocation", criteria, ands);
            AddMultiFilter(c => c.WorkRelated.SharpsInfo.Procedure, "WorkRelatedSharpsProcedure", criteria, ands);
            AddMultiFilter(c => c.WorkRelated.SharpsInfo.JobClassification, "WorkRelatedSharpsJobClassification", criteria, ands);
            AddMultiFilter<string>(e => e.Employee.Info.OfficeLocation, "Location", criteria, ands);
        }

        protected void ApplyWorkRelatedCriteria(ReportCriteria criteria, List<IMongoQuery> ands)
        {
            AddMultiFilter(e => e.WorkRelated.Classification, "WorkRelatedCaseClassification", criteria, ands);
            AddMultiFilter(e => e.WorkRelated.TypeOfInjury, "WorkRelatedTypeOfInjury", criteria, ands);
            AddMultiFilter<string>(e => e.Employee.Info.OfficeLocation, "Location", criteria, ands);
        }

        public void AddMultiFilter<TEnum>(Expression<Func<Case, TEnum>> memberExpression, string filterName, ReportCriteria criteria, List<IMongoQuery> ands)
        {
            var matchingFilter = criteria.Filters.FirstOrDefault(m => m.Name == filterName && m.Values.Any());
            if (matchingFilter != null)
            {
                var value = matchingFilter.ValuesAs<TEnum>();
                ands.Add(Case.Query.In(memberExpression, value));
            }
        }

        public void AddEQFilter<TEnum>(Expression<Func<Case, TEnum>> memberExpression, string filterName, ReportCriteria criteria, List<IMongoQuery> ands)
        {
            var matchingFilter = criteria.Filters.FirstOrDefault(m => m.Name == filterName && m.Value != null);
            if (matchingFilter != null)
            {
                var value = matchingFilter.ValueAs<TEnum>();
                ands.Add(Case.Query.EQ(memberExpression, value));
            }
        }

        public void AddYesNoBoolFilter(Expression<Func<Case, bool>> memberExpression, string filterName, ReportCriteria criteria, List<IMongoQuery> ands)
        {
            var matchingFilter = criteria.Filters.FirstOrDefault(m => m.Name == filterName && m.Value != null);
            if (matchingFilter != null)
            {
                var value = matchingFilter.Value.ToString() == "Yes";
                ands.Add(Case.Query.EQ(memberExpression, value));
            }
        }

        #endregion
    }
}

