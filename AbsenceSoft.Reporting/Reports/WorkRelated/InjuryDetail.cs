using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Reporting.Reports.WorkRelated
{
    [Serializable]
    public class InjuryDetail : WorkRelatedReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id
        {
            get { return "7222e047-5dd2-4e7d-8cd8-12ebf1a98153"; }
        }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name
        {
            get { return "Injury Detail"; }
        }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped
        {
            get { return true; }
        }

        /// <summary>
        /// Property to configure chart related options
        /// </summary>
        public override ReportChart Chart
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the criteria.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public override ReportCriteria GetCriteria(User user)
        {
            var criteria = base.GetCriteria(user);
            criteria.Filters.AddRange(GetWorkRelatedCriteria(user));
            return criteria;
        }

        /// <summary>
        /// Runs the query.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        protected virtual List<Case> RunQuery(User user, ReportCriteria criteria)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();
            List<IMongoQuery> ands = new List<IMongoQuery>();

            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "Employee._id"));
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));

            ApplyEmployerFilter<Case>(ands, criteria);

            ands.Add(Case.Query.And(
                    Case.Query.GTE(e => e.WorkRelated.IllnessOrInjuryDate, new BsonDateTime(startDate)),
                    Case.Query.LTE(e => e.WorkRelated.IllnessOrInjuryDate, new BsonDateTime(endDate))
            ));

            ands.Add(Case.Query.Exists(c => c.WorkRelated));
            ApplyWorkRelatedCriteria(criteria, ands);

            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null);
            return query.ToList();

        }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override void RunReport(User user, ReportResult result)
        {
            var queryResults = RunQuery(user, result.Criteria);
            List<ResultData> reportResultItems = new List<ResultData>();

            foreach (var c in queryResults)
            {
                dynamic row = new ResultData();
                DateTime? displayRTW = GetCaseEventDate(c, CaseEventType.ReturnToWork);
                DateTime? displayERTW = GetCaseEventDate(c, CaseEventType.EstimatedReturnToWork);

                row.CaseNumber = new LinkItem() { Value = c.CaseNumber, Link = string.Format("/Cases/{0}/View", c.Id) };
                row.EmployeeNumber = new LinkItem() { Value = c.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", c.Employee.Id) };
                row.FirstName = new LinkItem() { Value = c.Employee.FirstName, Link = string.Format("/Employees/{0}/View", c.Employee.Id) };
                row.LastName = new LinkItem() { Value = c.Employee.LastName, Link = string.Format("/Employees/{0}/View", c.Employee.Id) };
                row.StartDate = c.StartDate.ToUIString();
                row.EndDate = c.EndDate.HasValue ? c.EndDate.ToUIString() : string.Empty;
                row.LeaveType = c.CaseType.ToString();
                row.Status = c.GetCaseStatus().ToString().SplitCamelCaseString();
                row.Reason = c.Reason == null ? string.Empty : c.Reason.Name;
                row.Reportable = c.WorkRelated != null ? c.WorkRelated.Reportable ? "Yes" : "No" : string.Empty;
                row.Classification = (c.WorkRelated != null && c.WorkRelated.Classification.HasValue) ? c.WorkRelated.Classification.ToString().SplitCamelCaseString() : string.Empty;
                row.WhereOccurred = c.WorkRelated != null ? c.WorkRelated.WhereOccurred ?? string.Empty : string.Empty;
                row.TypeOfInjury = (c.WorkRelated != null && c.WorkRelated.TypeOfInjury.HasValue) ? c.WorkRelated.TypeOfInjury.ToString().SplitCamelCaseString() : string.Empty;
                row.InjuryOrIllness = (c.WorkRelated != null ? c.WorkRelated.InjuryOrIllness ?? string.Empty : string.Empty);
                row.WhatHappened = (c.WorkRelated != null ? c.WorkRelated.WhatHappened ?? string.Empty : string.Empty);
                row.DaysAwayFromWork = (c.WorkRelated != null ? c.WorkRelated.OverrideDaysAwayFromWork.HasValue ?c.WorkRelated.OverrideDaysAwayFromWork.Value.ToString() :c.WorkRelated.DaysAwayFromWork.HasValue ? c.WorkRelated.DaysAwayFromWork.Value.ToString() : "0" : string.Empty);
                row.DaysOnJobTransferOrRestriction = (c.WorkRelated != null ? c.WorkRelated.DaysOnJobTransferOrRestriction.HasValue ? c.WorkRelated.DaysOnJobTransferOrRestriction.Value.ToString() : c.WorkRelated.CalculatedDaysOnJobTransferOrRestriction.HasValue ? c.WorkRelated.CalculatedDaysOnJobTransferOrRestriction.Value.ToString() : string.Empty : string.Empty);
                row.ActivityBeforeIncident = (c.WorkRelated != null ? c.WorkRelated.ActivityBeforeIncident ?? string.Empty : string.Empty);
                row.WhatHarmedTheEmployee = (c.WorkRelated != null ? c.WorkRelated.WhatHarmedTheEmployee ?? string.Empty : string.Empty);
                row.JobDetailOccupation = c.Employee.JobTitle ?? string.Empty;
                row.TotalClaims = "";
                row.ReturnToWork = displayRTW != null ? displayRTW.ToString("MM/dd/yyyy") : string.Empty;
                row.EstimatedReturnToWork = displayERTW != null ? displayERTW.ToString("MM/dd/yyyy") : string.Empty;

                reportResultItems.Add(row);
            }

            ResultDataGroup group = new ResultDataGroup() { Key = "Details", Items = reportResultItems, Label = "Details" };

            // Report Totals
            var totalClaims = queryResults.Count;
            var totalDaysAwayFromWork = queryResults.Sum(c => c.WorkRelated != null ? c.WorkRelated.OverrideDaysAwayFromWork.HasValue ? c.WorkRelated.OverrideDaysAwayFromWork.GetValueOrDefault() : c.WorkRelated.DaysAwayFromWork.GetValueOrDefault() : 0);
            var totalDaysOnJobTransferOrRestriction = queryResults.Sum(c => c.WorkRelated != null ? c.WorkRelated.DaysOnJobTransferOrRestriction.HasValue ? c.WorkRelated.DaysOnJobTransferOrRestriction.GetValueOrDefault() : c.WorkRelated.CalculatedDaysOnJobTransferOrRestriction.GetValueOrDefault() : 0);
            
            List<ResultData> reportSummaryItems = new List<ResultData>();
            if (queryResults.Any())
            {
                dynamic rowTotals = new ResultData();

                rowTotals.CaseNumber = string.Empty;
                rowTotals.EmployeeNumber = string.Empty;
                rowTotals.FirstName = string.Empty;
                rowTotals.LastName = string.Empty;
                rowTotals.StartDate = string.Empty;
                rowTotals.EndDate = string.Empty;
                rowTotals.LeaveType = string.Empty;
                rowTotals.Status = string.Empty;
                rowTotals.Reason = string.Empty;
                rowTotals.Reportable = string.Empty;
                rowTotals.Classification = string.Empty;
                rowTotals.WhereOccurred = string.Empty;
                rowTotals.TypeOfInjury = string.Empty;
                rowTotals.InjuryOrIllness = string.Empty;
                rowTotals.WhatHappened = string.Empty;
                rowTotals.DaysAwayFromWork = totalDaysAwayFromWork;
                rowTotals.DaysOnJobTransferOrRestriction = totalDaysOnJobTransferOrRestriction;
                rowTotals.ActivityBeforeIncident = string.Empty;
                rowTotals.WhatHarmedTheEmployee = string.Empty;
                rowTotals.JobDetailOccupation = string.Empty;
                rowTotals.TotalClaims = totalClaims;
                rowTotals.ReturnToWork = string.Empty;
                rowTotals.EstimatedReturnToWork = string.Empty;

                reportSummaryItems.Add(rowTotals);
            }
            ResultDataGroup groupSummary = new ResultDataGroup() { Key = "ReportTotals", Items = reportSummaryItems, Label = "Report Totals;" };

            result.Groups = new List<ResultDataGroup>();
            if (queryResults.Any())
            {
                result.Groups.Add(group);
                result.Groups.Add(groupSummary);
            }
        }
    }
}