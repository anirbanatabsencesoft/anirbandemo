﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Reporting.Reports.WorkRelated
{
    [Serializable]
    public class NeedleStick : WorkRelatedReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id
        {
            get { return "bd74f80f-9597-465a-bf99-66d8039ff393"; }
        }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name
        {
            get { return "Needlestick"; }
        }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped
        {
            get { return false; }
        }

        /// <summary>
        /// Property to configure chart related options
        /// </summary>
        public override ReportChart Chart
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the criteria.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public override ReportCriteria GetCriteria(User user)
        {
            var criteria = base.GetCriteria(user);
            criteria.Filters.AddRange(GetSharpsCriteria(user));
            return criteria;
        }

        /// <summary>
        /// Runs the query.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        protected virtual List<Case> RunQuery(User user, ReportCriteria criteria)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();
            List<IMongoQuery> ands = new List<IMongoQuery>();

            if (user != null)
            {
                ands.Add(user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "Employee._id"));
            }
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
            {
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            }

            ApplyEmployerFilter<Case>(ands, criteria);

            ands.Add(Case.Query.And(
                    Case.Query.GTE(e => e.WorkRelated.IllnessOrInjuryDate, new BsonDateTime(startDate)),
                    Case.Query.LTE(e => e.WorkRelated.IllnessOrInjuryDate, new BsonDateTime(endDate))
            ));

            // This report requires the Sharps info
            ands.Add(Case.Query.EQ(e => e.WorkRelated.Sharps, true));
            ApplySharpsFilters(criteria, ands);

            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null);
            return query.ToList();

        }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override void RunReport(User user, ReportResult result)
        {
            var queryResults = RunQuery(user, result.Criteria);

            List<ResultData> reportResultItems = new List<ResultData>();

            foreach (var c in queryResults)
            {
                dynamic row = new ResultData();

                row.CaseNumber = new LinkItem() { Value = c.CaseNumber, Link = string.Format("/Cases/{0}/View", c.Id) };
                row.EmployeeNumber = new LinkItem() { Value = c.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", c.Employee.Id) };
                row.FirstName = new LinkItem() { Value = c.Employee.FirstName, Link = string.Format("/Employees/{0}/View", c.Employee.Id) };
                row.LastName = new LinkItem() { Value = c.Employee.LastName, Link = string.Format("/Employees/{0}/View", c.Employee.Id) };
                row.StartDate = c.StartDate.ToUIString();
                row.EndDate = c.EndDate.ToUIString();
                row.LeaveType = c.CaseType.ToString();
                row.Status = c.GetCaseStatus().ToString().SplitCamelCaseString();
                row.Reason = c.Reason.Name;
                row.Reportable = c.WorkRelated.Reportable ? "Yes" : "No";
                row.Classification = (c.WorkRelated.Classification.HasValue) ? c.WorkRelated.Classification.ToString().SplitCamelCaseString() : string.Empty;
                row.WhereOccurred = c.WorkRelated.WhereOccurred;
                row.TypeOfInjury = (c.WorkRelated.TypeOfInjury.HasValue) ? c.WorkRelated.TypeOfInjury.ToString().SplitCamelCaseString() : string.Empty;
                row.InjuryOrIllness = c.WorkRelated.InjuryOrIllness;
                row.WhatHappened = c.WorkRelated.WhatHappened;
                row.DaysAwayFromWork = c.WorkRelated.DaysAwayFromWork;
                row.DaysOnJobTransferOrRestriction = c.WorkRelated.DaysOnJobTransferOrRestriction;
                row.ActivityBeforeIncident = c.WorkRelated.ActivityBeforeIncident;
                row.WhatHarmedTheEmployee = c.WorkRelated.WhatHarmedTheEmployee;
                row.JobDetailOccupation = c.Employee.JobTitle;

                // Sharps Specifics
                row.JobClassification = c.WorkRelated != null && c.WorkRelated.SharpsInfo != null && c.WorkRelated.SharpsInfo.JobClassification.HasValue ? c.WorkRelated.SharpsInfo.JobClassification.ToString().SplitCamelCaseString() : string.Empty;
                row.LocationAndDepartment = c.WorkRelated != null && c.WorkRelated.SharpsInfo != null && c.WorkRelated.SharpsInfo.LocationAndDepartment.HasValue ? c.WorkRelated.SharpsInfo.LocationAndDepartment.ToString().SplitCamelCaseString() : string.Empty;
                row.SharpType = c.WorkRelated != null && c.WorkRelated.SharpsInfo != null ? c.WorkRelated.SharpsInfo.TypeOfSharp ?? string.Empty : string.Empty;
                row.SharpBrand = c.WorkRelated != null && c.WorkRelated.SharpsInfo != null ? c.WorkRelated.SharpsInfo.BrandOfSharp ?? string.Empty : string.Empty;
                row.Procedure = c.WorkRelated != null && c.WorkRelated.SharpsInfo != null && c.WorkRelated.SharpsInfo.Procedure.HasValue ? c.WorkRelated.SharpsInfo.Procedure.ToString().SplitCamelCaseString() : string.Empty;
                row.InjuryLocation = string.Join(", ", c.WorkRelated.SharpsInfo.InjuryLocation.Select(i => i.ToString().SplitCamelCaseString()));
                row.ExposureDetail = c.WorkRelated != null && c.WorkRelated.SharpsInfo != null ? c.WorkRelated.SharpsInfo.ExposureDetail ?? string.Empty : string.Empty;
                reportResultItems.Add(row);
            }

            result.Items = reportResultItems;
        }
    }

}