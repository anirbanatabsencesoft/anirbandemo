﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using MongoDB.Driver;
using MongoDB.Bson;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Security;
using MongoDB.Driver.Builders;
using AbsenceSoft.Data.Notes;

namespace AbsenceSoft.Reporting.Reports.Custom._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269
{
    [Serializable]
    public class HRTouchesReport : AmazonReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "20BAA0BB-CB4C-4FF7-88A9-24B2AA85F1F7"; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 99; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Accommodation Status Report".
        /// </summary>
        public override string Category { get { return "Accommodation Interactive Process Report"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "HR Contacts Count"; } }

        /// <summary>
        /// Icon to set for Accommodation Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether to show the auto
        /// generated total counts at the bottom of the group page.
        /// </summary>
        /// <value>
        ///   <c>true</c> if group auto totals; otherwise, <c>false</c>.
        /// </value>
        public override bool GroupAutoTotals { get { return false; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get { return null; } }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria();
            criteria.CustomerId = user.CustomerId;
            criteria.StartDate = DateTime.UtcNow.Date.GetFirstDayOfMonth();
            criteria.EndDate = DateTime.UtcNow.Date;
            return criteria;
        }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override void RunReport(User user, ReportResult result)
        {
            var queryList = RunQuery(user, result.Criteria);
            var caseIds = queryList.Select(c => c.Id).ToList();
            var notes = CaseNote.AsQueryable().Where(n => caseIds.Contains(n.CaseId) && (n.Category == NoteCategoryEnum.InteractiveProcess || n.Category == NoteCategoryEnum.HRConversation)).ToList();

            List<ResultData> resultItems = new List<ResultData>();

            foreach (var q in queryList)
                resultItems.Add(GetDetail(q, "000000000000000000000072", true, notes));

            result.Groups = new List<ResultDataGroup>(2)
            {
                new ResultDataGroup()
                {
                    Key = "Summary",
                    Label = "Summary",
                    Items = new List<ResultData>(1) { BuildSummary(resultItems) }
                },
                new ResultDataGroup()
                {
                    Key = "Detail",
                    Label = "Detail",
                    Items = resultItems
                }
            };
        }

        /// <summary>
        /// Builds the summary.
        /// </summary>
        /// <param name="results">The results.</param>
        /// <returns></returns>
        protected ResultData BuildSummary(List<ResultData> results)
        {
            dynamic data = new ResultData();
            data.Metric = "HR Contacts";
            data.Count = !results.Any() ? "0" : results.Sum(r => r["Contacts"] == null ? 0 : Convert.ToInt32(r["Contacts"].ToString())).ToString("N0");
            return data;
        }

        /// <summary>
        /// Runs the query.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        protected virtual List<Case> RunQuery(User user, ReportCriteria criteria)
        {
            List<IMongoQuery> ands = BuildFilter(user, criteria);

            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null);
            return query.ToList();
        }

        /// <summary>
        /// Builds the filter.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        protected virtual List<IMongoQuery> BuildFilter(User user, ReportCriteria criteria)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "Employee._id"));
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            ands.Add(Case.Query.EQ(e => e.IsAccommodation, true));

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
            ));

            return ands;
        }

        /// <summary>
        /// Gets the detail.
        /// </summary>
        /// <param name="case">The case.</param>
        /// <param name="questionId">The question identifier.</param>
        /// <param name="answer">The answer.</param>
        /// <param name="allNotes">All notes.</param>
        /// <returns></returns>
        protected virtual ResultData GetDetail(Case @case, string questionId, object answer = null, List<CaseNote> allNotes = null)
        {
            ResultData data = new ResultData();
            data["CaseNumber"] = new LinkItem() { Value = @case.CaseNumber, Link = string.Format("/Cases/{0}/View", @case.Id) };
            data["EmployeeNumber"] = new LinkItem() { Value = @case.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", @case.Employee.Id) };
            data["Name"] = new LinkItem() { Value = @case.Employee.FullName, Link = string.Format("/Employees/{0}/View", @case.Employee.Id) };

            int count = 0;
            var notes = (allNotes ?? new List<CaseNote>(0)).Where(n => n.CaseId == @case.Id).ToList();
            if (notes.Any())
            {
                count += notes.Count(n => n.Category == NoteCategoryEnum.HRConversation);
                count += notes.Count(n => n.Category == NoteCategoryEnum.InteractiveProcess && n.Metadata.GetRawValue<string>("AccommQuestionId") == "000000000000000000000072");
            }
            data["Contacts"] = count.ToString("N0");

            data["WorkState"] = @case.Employee.WorkState ?? "";
            data["Location"] = @case.CurrentOfficeLocation ?? "";

            return data;
        }
    }
}
