﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using MongoDB.Driver;
using MongoDB.Bson;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Security;
using MongoDB.Driver.Builders;

namespace AbsenceSoft.Reporting.Reports.Custom._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269
{
    [Serializable]
    public class EmployeeOnLeaveReport : AmazonReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "E8E5E616-557E-4064-BE44-20200F22D025"; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 99; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return false; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Accommodation Status Report".
        /// </summary>
        public override string Category { get { return "Employee on Leave Report"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "Detail"; } }

        /// <summary>
        /// Icon to set for Accommodation Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get { return null; } }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria();
            criteria.CustomerId = user.CustomerId;
            criteria.StartDate = DateTime.UtcNow.Date.GetFirstDayOfMonth();
            criteria.EndDate = DateTime.UtcNow.Date;
            return criteria;
        }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override void RunReport(User user, ReportResult result)
        {
            var queryList = RunQuery(user, result.Criteria);

            List<ResultData> resultItems = new List<ResultData>();

            foreach (var q in queryList)
                resultItems.Add(GetDetail(q));

            result.Items = resultItems;
        }

        /// <summary>
        /// Runs the query.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        protected virtual List<Case> RunQuery(User user, ReportCriteria criteria)
        {
            List<IMongoQuery> ands = BuildFilter(user, criteria);

            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null);
            return query.ToList();
        }

        /// <summary>
        /// Builds the filter.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        protected virtual List<IMongoQuery> BuildFilter(User user, ReportCriteria criteria)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "Employee._id"));
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            ands.Add(Case.Query.EQ(e => e.IsAccommodation, true));
            ands.Add(Query.EQ("IsEmployeeOnLeave", true));
            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
            ));

            return ands;
        }

        /// <summary>
        /// Gets the detail.
        /// </summary>
        /// <param name="case">The case.</param>
        /// <returns></returns>
        protected virtual ResultData GetDetail(Case @case)
        {
            ResultData data = new ResultData();

            data["CaseNumber"] = new LinkItem() { Value = @case.CaseNumber, Link = string.Format("/Cases/{0}/View", @case.Id) };
            data["Name"] = new LinkItem() { Value = @case.Employee.FullName, Link = string.Format("/Employees/{0}/View", @case.Employee.Id) };

            data["EEID"] = @case.Employee.EmployeeNumber;

            List<string> accommodationTypes = new List<string>();
            if (@case.AccommodationRequest != null && @case.AccommodationRequest.Accommodations != null)
                 accommodationTypes = @case.AccommodationRequest.Accommodations.Select(a => a.Type.Name).ToList();

            if (accommodationTypes.Count > 0)
                data["AccommodationType"] = string.Join(",", accommodationTypes);
            else
                data["AccommodationType"] = "";

            data["OtherLeaveCaseNumber"] = @case.Metadata.GetRawValue<string>("OtherLeaveCaseNumber") ?? "";
            
            return data;
        }
    }
}
