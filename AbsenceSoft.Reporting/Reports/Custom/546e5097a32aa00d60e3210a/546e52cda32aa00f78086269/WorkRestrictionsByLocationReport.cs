﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using MongoDB.Driver;
using MongoDB.Bson;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Security;
using MongoDB.Driver.Builders;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Jobs;

namespace AbsenceSoft.Reporting.Reports.Custom._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269
{
    [Serializable]
    public class WorkRestrictionsByLocationReport : AmazonReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "D1F73C2C-9313-4E45-90FB-4A39FABF0BF9"; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 98; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Accommodation Status Report".
        /// </summary>
        public override string Category { get { return "Work Restrictions Report"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "by Location"; } }

        /// <summary>
        /// Icon to set for Accommodation Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether to show the auto
        /// generated total counts at the bottom of the group page.
        /// </summary>
        /// <value>
        ///   <c>true</c> if group auto totals; otherwise, <c>false</c>.
        /// </value>
        public override bool GroupAutoTotals { get { return false; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get { return null; } }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria();
            criteria.CustomerId = user.CustomerId;
            criteria.StartDate = DateTime.UtcNow.Date.GetFirstDayOfMonth();
            criteria.EndDate = DateTime.UtcNow.Date;

            ReportCriteriaItem item = GetCaseStatusCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetAccomTypeCriteria(user);
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetAdjudicationCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            criteria.Filters.Add(GetCaseAssigneeCriteria(user));
            criteria.Filters.Add(GetWorkStateCriteria(user, criteria.StartDate, criteria.EndDate));
            criteria.Filters.Add(GetLocationCriteria(user));
            return criteria;
        }


        private string lastCaseNumber = null;

        /// <summary>
        /// Get an employee job name.
        /// </summary>
        /// <param name="customerJobs"></param>
        /// <param name="employerId"></param>
        /// <param name="employeeNumber"></param>
        /// <returns></returns>
        private string GetEmployeeJob(IEnumerable<EmployeeJob> customerJobs, string employerId, string employeeNumber, AdjudicationStatus? status)
        {
            var currentJobs = customerJobs.Where(e =>
                (e.Dates == null || e.Dates.IsNull || e.Dates.IncludesToday())
                && (status == null || e.Status == null || e.Status.Value == status)
                && e.EmployerId == employerId
                && e.EmployeeNumber == employeeNumber);

            if (!currentJobs.Any())
                return string.Empty;

            if (currentJobs.Count() == 1)
                return currentJobs.First().JobName;

            return currentJobs.OrderByDescending(ej => ej.ModifiedDate).First().JobName;
        }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override void RunReport(User user, ReportResult result)
        {
            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunCaseManagerReport));
            else if (!string.IsNullOrWhiteSpace(result.Criteria.CustomerId))
                ands.Add(EmployeeRestriction.Query.EQ(e => e.CustomerId, result.Criteria.CustomerId));

            DateTime startDate = result.Criteria.StartDate.ToMidnight();
            DateTime endDate = result.Criteria.EndDate.EndOfDay();

            ands.Add(EmployeeRestriction.Query.Or(
                EmployeeRestriction.Query.And(EmployeeRestriction.Query.GTE(e => e.Restriction.Dates.StartDate, new BsonDateTime(startDate)), EmployeeRestriction.Query.LTE(e => e.Restriction.Dates.StartDate, new BsonDateTime(endDate))),
                EmployeeRestriction.Query.And(EmployeeRestriction.Query.GTE(e => e.Restriction.Dates.EndDate, new BsonDateTime(startDate)), EmployeeRestriction.Query.LTE(e => e.Restriction.Dates.EndDate, new BsonDateTime(endDate)))
            ));
            ands.Add(EmployeeRestriction.Query.NE(e => e.CaseId, null));

            var employeeRestrictions = EmployeeRestriction.Query.Find(EmployeeRestriction.Query.And(ands)).ToList();
            var caseIds = employeeRestrictions.Select(r => r.CaseId).Distinct().ToList();

            var caseQuery = BuildFilter(user, result.Criteria);
            caseQuery.Add(Case.Query.In(c => c.Id, caseIds));

            var cases = Case.Query.Find(Case.Query.And(caseQuery)).ToList();
            caseIds.Clear();
            caseIds = new List<string>(cases.Count);
            List<string> empIds = new List<string>();
            List<string> empNums = new List<string>();
            List<string> employerIds = new List<string>();
            foreach (var c in cases)
            {
                caseIds.Add(c.Id);
                empIds.AddIfNotExists(c.Employee.Id);
                empNums.AddIfNotExists(c.Employee.EmployeeNumber);
                employerIds.AddIfNotExists(c.EmployerId);
            }

            employeeRestrictions = employeeRestrictions.Where(r => caseIds.Contains(r.CaseId)).ToList();

            var demandIds = employeeRestrictions.Select(r => r.Restriction.DemandId).Distinct().ToList();
            var demands = Demand.Query.Find(Demand.Query.In(d => d.Id, demandIds)).ToList();

            List<EmployeeContact> employeeContacts = EmployeeContact.AsQueryable().Where(ec => empIds.Contains(ec.EmployeeId) && ec.ContactTypeCode == "SUPERVISOR").ToList();
            var customerJobs = EmployeeJob.AsQueryable().Where(e => e.CustomerId == user.CustomerId && empNums.Contains(e.EmployeeNumber)).ToList();

            result.Groups = cases
                .GroupBy(c => c.Employee.Info.OfficeLocation ?? "")
                .Select(q => new ResultDataGroup()
                {
                    Key = q.Key,
                    Label = q.Key,
                    Items = q
                        .SelectMany(c => employeeRestrictions.Where(e => e.CaseId == c.Id).Select(p => p.Restriction).Select(r => GetDetail(c, r, employeeContacts, customerJobs, demands)))
                        .ToList()
                }).ToList();
        }

        /// <summary>
        /// Builds the filter.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        protected virtual List<IMongoQuery> BuildFilter(User user, ReportCriteria criteria)
        {
            DateTime startDate = criteria.StartDate;
            DateTime endDate = criteria.EndDate;

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "Employee._id"));
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            ands.Add(Case.Query.EQ(e => e.IsAccommodation, true));

            ApplyCaseFilters(ands, criteria);
            return ands;
        }

        /// <summary>
        /// Applies the case filters WITHOUT the date criteria.... that gets filtered elsewhere.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="criteria">The criteria.</param>
        protected override void ApplyCaseFilters(ICollection<IMongoQuery> query, ReportCriteria criteria)
        {
            if (!criteria.Filters.Any())
                return;

            ApplyEmployerFilter<Case>(query, criteria);
            var caseStatus = criteria.Filters.FirstOrDefault(m => m.Name == "CaseStatus");
            if (caseStatus != null && caseStatus.Value != null)
            {
                var enumCaseStatusFilter = criteria.ValueOf<CaseStatus>("CaseStatus");
                query.Add(Case.Query.EQ(c => c.Status, enumCaseStatusFilter));
            }

            var elemMatchAnds = new List<IMongoQuery>();

            var determination = criteria.Filters.FirstOrDefault(m => m.Name == "Determination");
            if (determination != null && determination.Value != null)
            {
                var enumadjStatusFilter = criteria.ValueOf<AdjudicationStatus>("Determination");
                elemMatchAnds.Add(Query<Accommodation>.EQ(a => a.Determination, enumadjStatusFilter));
            }

            var accomType = criteria.Filters.FirstOrDefault(m => m.Name == "AccomType");
            if (accomType != null && accomType.Value != null)
                elemMatchAnds.Add(Query<Accommodation>.EQ(a => a.Type.Code, accomType.Value));

            if (elemMatchAnds.Any())
                query.Add(Case.Query.ElemMatch(c => c.AccommodationRequest.Accommodations, q => q.And(elemMatchAnds)));

            var workState = criteria.Filters.FirstOrDefault(m => m.Name == "WorkState");
            if (workState != null && workState.Value != null)
                query.Add(Case.Query.EQ(e => e.Employee.WorkState, workState.Value));

            var locationFilter = criteria.Filters.FirstOrDefault(m => m.Name == "Location");
            if (locationFilter != null
                && locationFilter.Values != null
                && locationFilter.Values.Any())
            {
                var count = locationFilter.Values.Count;
                var values = locationFilter.Values.Where(p => !string.IsNullOrEmpty(Convert.ToString(p))).ToList();
                if (count != values.Count)
                    values.AddRange(new List<object> { null, string.Empty });
                query.Add(Case.Query.In(e => e.CurrentOfficeLocation, values));
            }

            var assignee = criteria.Filters.FirstOrDefault(m => m.Name == "CaseAssignee");
            if (assignee != null && assignee.Value != null && !string.IsNullOrWhiteSpace(assignee.ValueAs<string>()))
                query.Add(Case.Query.EQ(e => e.AssignedToId, assignee.Value));
        }

        /// <summary>
        /// Gets the detail.
        /// </summary>
        /// <param name="case">The case.</param>
        /// <returns></returns>
        protected virtual ResultData GetDetail(Case @case, AbsenceSoft.Data.Jobs.WorkRestriction restriction, List<EmployeeContact> employeeContacts, List<EmployeeJob> customerJobs, List<Demand> demands)
        {
            ResultData data = new ResultData();

            if (lastCaseNumber != @case.CaseNumber)
            {
                // Case Number - link
                data["CaseNumber"] = new LinkItem() { Value = @case.CaseNumber, Link = string.Format("/Cases/{0}/View", @case.Id) };
                // Employee Name - link
                data["Name"] = new LinkItem() { Value = @case.Employee.FullName, Link = string.Format("/Employees/{0}/View", @case.Employee.Id) };
                data["EEID"] = new LinkItem() { Value = @case.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", @case.Employee.Id) };
                data["CostCenterCode"] = @case.Employee.CostCenterCode ?? "";
                data["Department"] = @case.Employee.Department ?? "";
                data["CurrentProcessPath"] = GetEmployeeJob(customerJobs, @case.EmployerId, @case.Employee.EmployeeNumber, null) ?? "";
                data["Supervisor"] = employeeContacts
                    .Where(c => c.ContactTypeCode == "SUPERVISOR" && c.EmployeeId == @case.Employee.Id)
                    .Select(c => string.Format("{0}, {1}", c.Contact.LastName, c.Contact.FirstName))
                    .DefaultIfEmpty("")
                    .First();
                data["Class"] = @case.Employee.CustomFields
                    .Where(cf => cf.Name == "EmployeeClass")
                    .Select(cf => cf.SelectedValueText)
                    .DefaultIfEmpty("")
                    .First();

                data["Status"] = @case.Status.ToString().SplitCamelCaseString();
                // Case Begin date
                data["StartDate"] = @case.StartDate.ToString("MM/dd/yyyy");
                // Case End date
                data["EndDate"] = @case.EndDate.ToString("MM/dd/yyyy") ?? "";
                // Location
                data["Location"] = @case.CurrentOfficeLocation ?? "";
                // The accommodation type(s)
                data["AccommodationType"] = @case.AccommodationRequest == null ? "" : string.Join(", ", @case.AccommodationRequest.Accommodations.Select(p => p.Type == null ? "" : p.Type.Name));
                data["AccommodationStatus"] = @case.AccommodationRequest == null ? "" : @case.AccommodationRequest.Status.ToString().SplitCamelCaseString();
                string workRelated = null;
                string granted = null;
                string grantedDate = null;
                if (@case.AccommodationRequest != null && @case.AccommodationRequest.Accommodations != null)
                {
                    granted = @case.AccommodationRequest.Accommodations.Any(r => r.Granted) ? "Yes" : "No";
                    var grantedAccommodation = @case.AccommodationRequest.Accommodations.FirstOrDefault(r => r.Granted);
                    if (grantedAccommodation != null && grantedAccommodation.GrantedDate.HasValue)
                        grantedDate = grantedAccommodation.GrantedDate.ToString("MM/dd/yyyy");
                    if (@case.AccommodationRequest.Accommodations.Any(r => r.IsWorkRelated.HasValue))
                    {
                        bool isWorkRelated = @case.AccommodationRequest.Accommodations.Any(r => r.IsWorkRelated.HasValue && r.IsWorkRelated.Value);
                        workRelated = isWorkRelated ? "Yes" : "No";
                    }
                }
                data["Granted"] = granted ?? "No";
                data["GrantedDate"] = grantedDate ?? "";
                data["ApprovedProcessPath"] = GetEmployeeJob(customerJobs, @case.EmployerId, @case.Employee.EmployeeNumber, AdjudicationStatus.Approved) ?? "";
                data["WorkRelated"] = workRelated ?? "No";
            }
            else
            {
                data["CaseNumber"] = "";
                data["Name"] = "";
                data["EEID"] = "";
                data["CostCenterCode"] = "";
                data["Department"] = "";
                data["CurrentProcessPath"] = "";
                data["Supervisor"] = "";
                data["Class"] = "";
                data["Status"] = "";
                data["StartDate"] = "";
                data["EndDate"] = "";
                data["Location"] = "";
                data["AccommodationType"] = "";
                data["AccommodationStatus"] = "";
                data["Granted"] = "";
                data["GrantedDate"] = "";
                data["ApprovedProcessPath"] = "";
                data["WorkRelated"] = "";
            }

            if (restriction != null && !string.IsNullOrWhiteSpace(restriction.DemandId))
            {
                var demand = demands.FirstOrDefault(d => d.Id == restriction.DemandId);
                if (demand != null)
                    data["Restriction"] = demand.Name ?? "";
                else
                    data["Restriction"] = "";
                data["Limitation"] = restriction.ToString();
                data["From"] = (restriction.Dates == null || restriction.Dates.IsNull ? "" : restriction.Dates.StartDate.ToString("MM/dd/yyyy"));
                data["Thru"] = (restriction.Dates == null || restriction.Dates.IsNull || restriction.Dates.EndDate == null ? "" : restriction.Dates.EndDate.ToString("MM/dd/yyyy") ?? "");
                if (restriction.Dates == null || restriction.Dates.IsNull)
                    data["Duration"] = "";
                else
                    data["Duration"] = restriction.Dates.EndDate.HasValue ? string.Format("{0} days", restriction.Dates.Timeframe.TotalDays) : "";
            }
            else
            {
                data["Restriction"] = "";
                data["Limitation"] = "";
                data["From"] = "";
                data["Thru"] = "";
                data["Duration"] = "";
            }

            lastCaseNumber = @case.CaseNumber;

            return data;
        }
    }
}
