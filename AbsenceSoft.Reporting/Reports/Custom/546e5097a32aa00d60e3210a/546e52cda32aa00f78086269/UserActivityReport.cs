﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using MongoDB.Driver;
using MongoDB.Bson;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Security;
using MongoDB.Driver.Builders;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Communications;

namespace AbsenceSoft.Reporting.Reports.Custom._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269
{
    [Serializable]
    public class UserActivityReport : AmazonReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "20BAA0CD-CB4C-4FF7-88A9-24B2AA85F1F7"; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 99; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return false; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Accommodation Status Report".
        /// </summary>
        public override string Category { get { return "User Activity Report"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "by User Summary"; } }

        /// <summary>
        /// Icon to set for Accommodation Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether to show the auto
        /// generated total counts at the bottom of the group page.
        /// </summary>
        /// <value>
        ///   <c>true</c> if group auto totals; otherwise, <c>false</c>.
        /// </value>
        public override bool GroupAutoTotals { get { return false; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get { return null; } }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria();
            criteria.CustomerId = user.CustomerId;
            criteria.StartDate = DateTime.UtcNow.Date.GetFirstDayOfMonth();
            criteria.EndDate = DateTime.UtcNow.Date;
            return criteria;
        }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override void RunReport(User user, ReportResult result)
        {
            var startDate = new BsonDateTime(result.Criteria.StartDate.ToMidnight());
            var endDate = new BsonDateTime(result.Criteria.EndDate.EndOfDay());

            // Group results, captures userId + count of
            Dictionary<string, int> toDos = new Dictionary<string,int>(0);
            Dictionary<string, int> caseNotes = new Dictionary<string, int>(0);
            Dictionary<string, int> empNotes = new Dictionary<string, int>(0);
            Dictionary<string, int> attachments = new Dictionary<string, int>(0);

            var userFilter = user.BuildDataAccessFilters(Permission.RunCaseManagerReport);

            var caseIdQuery = Case.Query.And(user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "Employee._id"), Case.Query.IsNotDeleted());
            List<string> caseIds = Case.Query.Find(caseIdQuery).ToList().Select(c => c.Id).ToList();

            List<Action> operations = new List<Action>(4)
            {
                new Action(() =>
                {
                    toDos = DoAggregation(
                        ToDoItem.Query.And(userFilter,
                        ToDoItem.Query.GTE(e => e.ModifiedDate, startDate),
                        ToDoItem.Query.LTE(e => e.ModifiedDate, endDate),
                        ToDoItem.Query.EQ(e => e.Status, ToDoItemStatus.Complete),
                        ToDoItem.Query.In(e => e.CaseId, caseIds)),
                        ToDoItem.Repository.Collection);
                }),
                new Action(() =>
                {
                    caseNotes = DoAggregation(
                        CaseNote.Query.And(userFilter,
                        CaseNote.Query.GTE(e => e.ModifiedDate, startDate),
                        CaseNote.Query.LTE(e => e.ModifiedDate, endDate),
                        CaseNote.Query.In(e => e.CaseId, caseIds)), 
                        CaseNote.Repository.Collection);
                }),
                new Action(() =>
                {
                    empNotes = DoAggregation(
                        EmployeeNote.Query.And(userFilter,
                        EmployeeNote.Query.GTE(e => e.ModifiedDate, startDate),
                        EmployeeNote.Query.LTE(e => e.ModifiedDate, endDate)), 
                        EmployeeNote.Repository.Collection);
                }),
                new Action(() =>
                {
                    attachments = DoAggregation(
                        Attachment.Query.And(userFilter,
                        Attachment.Query.GTE(e => e.ModifiedDate, startDate),
                        Attachment.Query.LTE(e => e.ModifiedDate, endDate)), 
                        Attachment.Repository.Collection);
                })
            };
            operations.AsParallel().ForAll(a => a());

            var distinctUserIds = toDos.Keys.Union(caseNotes.Keys).Union(empNotes.Keys).Union(attachments.Keys).Distinct().ToList();
            var users = User.AsQueryable().Where(u => distinctUserIds.Contains(u.Id)).ToList() /// get all the users
                .OrderBy(u => u.LastName).ThenBy(u => u.FirstName); /// use linq to order them, not mongo

            // Compile the results
            List<ResultData> resultItems = new List<ResultData>(users.Count());

            foreach (var u in users)
            {
                ResultData data = new ResultData();
                //count.ToString("N0")
                data["LastName"] = u.LastName ?? "";
                data["FirstName"] = u.FirstName ?? "";
                data["Email"] = u.Email ?? "";
                data["CompletedToDos"] = (toDos.ContainsKey(u.Id) ? toDos[u.Id] : 0).ToString("N0");
                data["Notes"] = ((caseNotes.ContainsKey(u.Id) ? caseNotes[u.Id] : 0) + (empNotes.ContainsKey(u.Id) ? empNotes[u.Id] : 0)).ToString("N0");
                data["Attachments"] = (attachments.ContainsKey(u.Id) ? attachments[u.Id] : 0).ToString("N0");
                resultItems.Add(data);
            }
            result.Items = resultItems;
        }

        /// <summary>
        /// Does the aggregation.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query.</param>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        protected virtual Dictionary<string, int> DoAggregation<T>(IMongoQuery query, MongoCollection<T> collection)
        {
            var args = new AggregateArgs()
            {
                OutputMode = AggregateOutputMode.Inline,
                Pipeline = new List<BsonDocument>(2)
                {
                    BsonDocument.Parse("{ $match: " + query.ToString() + " }"),
                    BsonDocument.Parse("{ $group: { _id: \"$mby\", count: { $sum: 1 } } }")
                }
            };

            return collection
                .Aggregate(args).ToDictionary(
                    i => i.GetRawValue<string>("_id"), 
                    i => i.GetRawValue<int>("count")
                );
        }
    }
}
