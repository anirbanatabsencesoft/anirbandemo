﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using MongoDB.Driver;
using MongoDB.Bson;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Security;
using MongoDB.Driver.Builders;
using AbsenceSoft.Data.Customers;

namespace AbsenceSoft.Reporting.Reports.Custom._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269
{
    [Serializable]
    public class AccommodationStatusGrantedReport : AmazonReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "20BAA0BB-CB4C-4FF7-88A9-24B2AA85F2E8"; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 0; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return false; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Accommodation Status Report".
        /// </summary>
        public override string Category { get { return "Accommodation Status Report"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "Granted"; } }

        /// <summary>
        /// Icon to set for Accommodation Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get { return null; } }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria();
            criteria.CustomerId = user.CustomerId;
            criteria.StartDate = DateTime.UtcNow.Date.GetFirstDayOfMonth();
            criteria.EndDate = DateTime.UtcNow.Date;
            return criteria;
        }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override void RunReport(User user, ReportResult result)
        {
            var queryList = RunQuery(user, result.Criteria);
            var users = User.Repository.Collection.FindAs<BsonDocument>(User.Query.And(User.Query.IsNotDeleted(),
                User.Query.EQ(u => u.CustomerId, user.CustomerId))
                ).SetFields(Fields.Include("_id", "FirstName", "LastName"))
                .ToDictionary(k => k["_id"].ToString(), v => string.Concat(v.Contains("FirstName") ? v["FirstName"].ToString() : "", " ", v.Contains("LastName") ? v["LastName"].ToString() : "").Trim());

            List<ResultData> resultItems = new List<ResultData>();

            foreach (var q in queryList.Where(c => c.AccommodationRequest != null))
            {
                q.AccommodationRequest.Accommodations = q.AccommodationRequest.Accommodations ?? new List<Accommodation>(0);
                string displayCaseNumber = q.CaseNumber;
                string displayFullName = q.Employee.FullName;

                foreach (var accom in q.AccommodationRequest.Accommodations.Where(a => a.Type != null).OrderBy(p => p.Type.Code))
                    foreach (var detail in accom.Usage.OrderBy(u => u.StartDate))
                        resultItems.Add(GetDetail(q, accom, detail, ref displayCaseNumber, ref displayFullName, users));
            }

            result.Items = resultItems;
        }

        /// <summary>
        /// Runs the query.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        protected virtual List<Case> RunQuery(User user, ReportCriteria criteria)
        {
            List<IMongoQuery> ands = BuildFilter(user, criteria);

            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null);
            return query.ToList();
        }

        /// <summary>
        /// Builds the filter.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        protected virtual List<IMongoQuery> BuildFilter(User user, ReportCriteria criteria)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "Employee._id"));
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            ands.Add(Case.Query.EQ(e => e.IsAccommodation, true));
            ands.Add(Case.Query.Exists(e => e.AccommodationRequest));
            ands.Add(Case.Query.Exists(e => e.AccommodationRequest.Accommodations));

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
            ));

            return ands;
        }

        /// <summary>
        /// Gets the detail.
        /// </summary>
        /// <param name="case">The case.</param>
        /// <param name="q">The q.</param>
        /// <param name="detail">The detail.</param>
        /// <param name="displayCaseNumber">The display case number.</param>
        /// <param name="displayFullName">Display name of the full.</param>
        /// <param name="users">The users.</param>
        /// <returns></returns>
        protected virtual ResultData GetDetail(Case @case, Accommodation q, AccommodationUsage detail, ref string displayCaseNumber, ref string displayFullName, Dictionary<string, string> users)
        {
            dynamic data = new ResultData();

            data.CaseNumber = string.IsNullOrWhiteSpace(displayCaseNumber) ? null : new LinkItem() { Value = @case.CaseNumber, Link = string.Format("/Cases/{0}/View", @case.Id) };
            data.EmployeeNumber = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = @case.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", @case.Employee.Id) };
            data.Name = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = @case.Employee.FullName, Link = string.Format("/Employees/{0}/View", @case.Employee.Id) };
            CustomField employeeClass = @case.Employee.CustomFields.FirstOrDefault(cf => cf.Name == "EmployeeClass");
            data.Class = employeeClass == null ? "" : employeeClass.SelectedValueText;
            data.StartDate = detail.StartDate == DateTime.MinValue ? "Pending" : detail.StartDate.ToString("MM/dd/yyyy");
            data.EndDate = detail.EndDate.HasValue ? detail.EndDate.ToString("MM/dd/yyyy") : string.Empty;
            data.Accommodation = q.Type.Code;
            data.AccommStatus = q.Status.ToString().SplitCamelCaseString();
            data.Granted = q.Status == CaseStatus.Cancelled ? "Cancelled" : detail.Determination.ToString().SplitCamelCaseString();
            data.ReasonNotGranted = detail.Determination == AdjudicationStatus.Denied ? string.IsNullOrWhiteSpace(detail.DenialReasonOther) ? detail.DenialReasonName : detail.DenialReasonOther ?? "" : "";
            data.DecisionDate = detail.DeterminationDate.HasValue ? detail.DeterminationDate.ToString("MM/dd/yyyy") : "";
            data.DecisionBy = string.IsNullOrWhiteSpace(detail.DeterminationById) ? "" : users.ContainsKey(detail.DeterminationById) ? users[detail.DeterminationById] : "";

            data.WorkState = @case.Employee.WorkState;
            data.Location = string.IsNullOrWhiteSpace(@case.CurrentOfficeLocation) ? string.Empty : @case.CurrentOfficeLocation;
            data.WorkRelated = q.IsWorkRelated == null ? "" : q.IsWorkRelated.Value ? "1" : "0";

            displayCaseNumber = string.Empty;
            displayFullName = string.Empty;

            return data;
        }
    }
}
