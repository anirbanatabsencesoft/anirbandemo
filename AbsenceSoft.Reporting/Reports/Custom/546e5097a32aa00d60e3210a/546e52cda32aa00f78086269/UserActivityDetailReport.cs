﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbsenceSoft.Reporting.Reports.Custom._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269
{
    [Serializable]
    public class UserActivityDetailReport : AmazonReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "20BAA0FF-CB4C-4FF7-88A9-24B2AA85F1F7"; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 98; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Accommodation Status Report".
        /// </summary>
        public override string Category { get { return "User Activity Report"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "Detail"; } }

        /// <summary>
        /// Icon to set for Accommodation Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get { return null; } }

        /// <summary>
        /// Gets a value indicating whether this instance is viewable. This report is NOT viewable in the UI. Only exportable.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is viewable; otherwise, <c>false</c>.
        /// </value>
        public override bool IsViewable { get { return false; } }

        /// <summary>
        /// Gets a value indicating whether this instance is exportable to PDF. This report is NOT, only to CSV.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is exportable to PDF; otherwise, <c>false</c>.
        /// </value>
        public override bool IsExportableToPdf { get { return false; } }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria();
            criteria.CustomerId = user.CustomerId;
            criteria.StartDate = DateTime.UtcNow.Date.GetFirstDayOfMonth();
            criteria.EndDate = DateTime.UtcNow.Date;
            return criteria;
        }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override void RunReport(User user, ReportResult result)
        {
            var startDate = new BsonDateTime(result.Criteria.StartDate.ToMidnight());
            var endDate = new BsonDateTime(result.Criteria.EndDate.EndOfDay());

            // Group results, captures userId + count of
            List<CaseNote> caseNotes = new List<CaseNote>(0);
            List<EmployeeNote> empNotes = new List<EmployeeNote>(0);
            List<Attachment> attachments = new List<Attachment>(0);

            var userFilter = user.BuildDataAccessFilters(Permission.RunCaseManagerReport);
            var caseIds = Case.Repository.Collection
                .FindAs<BsonDocument>(Query.And(user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "Employee._id"), Case.Query.IsNotDeleted()))
                .SetFields(Fields.Include("_id", "CaseNumber", "Employee._id", "Employee.EmployeeNumber", "Employee.FirstName", "Employee.LastName", "CurrentOfficeLocation", "CustomerId", "EmployerId"))
                .ToList()
                .ToDictionary(k => k["_id"].ToString(), v => new
                {
                    CaseNumber = v["CaseNumber"].ToString(),
                    EmployeeNumber = v["Employee"]["EmployeeNumber"].ToString(),
                    EmployeeId = v["Employee"]["_id"].ToString(),
                    OfficeLocation = v.GetRawValue<string>("CurrentOfficeLocation"),
                    Name = string.Concat(v["Employee"]["FirstName"], " ", v["Employee"]["LastName"])
                });
            var users = User.Repository.Collection
                .FindAs<BsonDocument>(Query.And(User.Query.EQ(u => u.CustomerId, user.CustomerId), User.Query.IsNotDeleted()))
                .SetFields(Fields.Include("_id", "FirstName", "LastName"))
                .ToList()
                .ToDictionary(k => k["_id"].ToString(), v => string.Concat(v.Contains("FirstName") ? v["FirstName"] : "", " ", v.Contains("LastName") ? v["LastName"] : "").Trim());

            // Compile the results
            result.Groups = new List<ResultDataGroup>(2);
            var notesGroup = result.Groups.AddFluid(new ResultDataGroup()
            {
                Label = "Case and Employee Notes",
                Key = "Notes",
                Items = new List<ResultData>()
            });
            var attGroup = result.Groups.AddFluid(new ResultDataGroup()
            {
                Label = "Case Attachments",
                Key = "Attachments",
                Items = new List<ResultData>()
            });

            List<Action> operations = new List<Action>(2)
            {
                new Action(() =>
                {
                    notesGroup.Items.AddRange(CaseNote.Query.Find(
                        CaseNote.Query.And(userFilter,
                        CaseNote.Query.GTE(e => e.CreatedDate, startDate),
                        CaseNote.Query.LTE(e => e.CreatedDate, endDate),
                        CaseNote.Query.In(e => e.CaseId, caseIds.Keys)))
                        .ToList().Select(n =>
                        {
                            ResultData data = new ResultData();
                            var c = caseIds[n.CaseId];
                            data["CaseNumber"] = new LinkItem(c == null ? "Case" : c.CaseNumber, string.Format("/Cases/{0}/View", n.CaseId));
                            if (c != null)
                            {
                                data["EmployeeNumber"] = new LinkItem() { Value = c.EmployeeNumber, Link = string.Format("/Employees/{0}/View", c.EmployeeId) };
                            }
                            data["Name"] = new LinkItem(c == null ? "Employee" : c.Name, string.Format("/Employees/{0}/View", c.EmployeeId));
                            data["OfficeLocation"] = c == null ? "" : c.OfficeLocation ?? "";
                            data["CreatedOn"] = n.CreatedDate.ToString("MM/dd/yyyy");
                            data["CreatedBy"] = users.ContainsKey(n.CreatedById) ? users[n.CreatedById] ?? "System" : "System";
                            data["Type"] = n.Category.HasValue ? n.Category.Value.ToString().SplitCamelCaseString() : "";
                            data["Detail"] = n.Notes == null || n.Notes.Length < 50 ? n.Notes : n.Notes.Substring(0, 47) + "...";
                            return data;
                        }));
                    notesGroup.Items.AddRange(EmployeeNote.Query.Find(
                        EmployeeNote.Query.And(userFilter,
                        EmployeeNote.Query.GTE(e => e.CreatedDate, startDate),
                        EmployeeNote.Query.LTE(e => e.CreatedDate, endDate),
                        EmployeeNote.Query.In(e => e.EmployeeId, caseIds.Select(v => v.Value.EmployeeId))))
                        .ToList().Select(n =>
                        {
                            ResultData data = new ResultData();
                            var c = caseIds.Values.FirstOrDefault(k => k.EmployeeId == n.EmployeeId);
                            data["CaseNumber"] = "N/A";
                            if (c != null)
                            {
                                data["EmployeeNumber"] = new LinkItem() { Value = c.EmployeeNumber, Link = string.Format("/Employees/{0}/View", n.EmployeeId) };
                            }
                            data["Name"] = new LinkItem(c == null ? "Employee" : c.Name, string.Format("/Employees/{0}/View", c.EmployeeId));
                            data["OfficeLocation"] = c == null ? "" : c.OfficeLocation ?? "";
                            data["CreatedOn"] = n.CreatedDate.ToString("MM/dd/yyyy");
                            data["CreatedBy"] = users.ContainsKey(n.CreatedById) ? users[n.CreatedById] ?? "System" : "System";
                            data["Type"] = n.Category.HasValue ? n.Category.Value.ToString().SplitCamelCaseString() : "";
                            data["Detail"] = n.Notes == null || n.Notes.Length < 50 ? n.Notes : n.Notes.Substring(0, 47) + "...";
                            return data;
                        }));
                }),
                new Action(() =>
                {
                    attGroup.Items.AddRange(Attachment.Query.Find(
                        Attachment.Query.And(userFilter,
                        Attachment.Query.GTE(e => e.CreatedDate, startDate),
                        Attachment.Query.LTE(e => e.CreatedDate, endDate),
                        Attachment.Query.In(e => e.CaseId, caseIds.Keys)))
                        .ToList().Select(n =>
                        {
                            ResultData data = new ResultData();
                            var c = caseIds[n.CaseId];
                            data["CaseNumber"] = new LinkItem(c == null ? "Case" : c.CaseNumber, string.Format("/Cases/{0}/View", n.CaseId));
                            if (c != null)
                            {
                                data["EmployeeNumber"] = new LinkItem() { Value = c.EmployeeNumber, Link = string.Format("/Employees/{0}/View", n.EmployeeId) };
                            }
                            data["Name"] = new LinkItem(c == null ? "Employee" : c.Name, string.Format("/Employees/{0}/View", n.EmployeeId));
                            data["OfficeLocation"] = c == null ? "" : c.OfficeLocation ?? "";
                            data["CreatedOn"] = n.CreatedDate.ToString("MM/dd/yyyy");
                            data["CreatedBy"] = n.CreatedByName ?? "System";
                            data["Type"] = n.AttachmentType.ToString().SplitCamelCaseString();
                            data["Detail"] = n.FileName == null || n.FileName.Length < 50 ? n.FileName : n.FileName.Substring(0, 47) + "...";
                            return data;
                        }));
                })
            };
            operations.AsParallel().ForAll(a => a());
        }
    }
}
