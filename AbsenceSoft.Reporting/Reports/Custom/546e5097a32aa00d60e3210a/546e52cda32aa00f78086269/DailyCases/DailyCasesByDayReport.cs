﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Reporting.Reports;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;
using MongoDB.Driver.Builders;

namespace AbsenceSoft.Reporting.Reports.Custom._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269.DailyCases
{
    [Serializable]
    public class DailyCasesByDayReport : DailyCasesReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "088BDE87-48AD-4560-84A7-982D5DE8D72A"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "by Day"; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 2; } }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override void RunReport(User user, ReportResult result)
        {
            var query = RunQuery(user, result.Criteria);

            var groupQuery = query.GroupBy(n =>
            {
                var evt = n.FindCaseEvent(CaseEventType.CaseCreated);
                if (evt != null) return evt.EventDate.Date;
                return n.CreatedDate.Date;
            })
            .OrderByDescending(g => g.Key)
           .Select(g => new
           {
               label = g.Key.ToString("MM/dd/yyyy"),
               data = g.Select(x => x).ToList()
           });

            List<ResultData> resultItems = new List<ResultData>();

            result.Groups = new List<ResultDataGroup>();
            int i = 1;
            foreach (var t in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (var td in t.data)
                    resultItems.Add(GetData(td));

                result.Groups.Add(new ResultDataGroup()
                {
                    Key = i,
                    Label = t.label,
                    Items = new List<ResultData>().AddRangeFluid(resultItems.OrderBy(r => ((dynamic)r).CreatedOn))
                });

                i++;
            }
        }
    }
}
