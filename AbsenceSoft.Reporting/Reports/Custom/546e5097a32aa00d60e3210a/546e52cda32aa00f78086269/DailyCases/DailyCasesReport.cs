﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Reporting.Reports;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;
using MongoDB.Driver.Builders;
using AbsenceSoft.Data.Customers;

namespace AbsenceSoft.Reporting.Reports.Custom._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269.DailyCases
{
    [Serializable]
    public class DailyCasesReport : AmazonReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "06EE4676-EA21-41F6-9DCF-EDE65D8D3B3E"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "Detail"; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return false; } }

        /// <summary>
        /// Category for all Absence Status reports is "Absence Status".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all Daily Cases reports is "New Case Load".
        /// </summary>
        public override string Category { get { return "New Case Load"; } }

        /// <summary>
        /// Icon to set for Absence Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get { return null; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 1; } }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria();
            criteria.CustomerId = user.CustomerId;
            criteria.StartDate = DateTime.UtcNow.Date.AddDays(-7);
            criteria.EndDate = DateTime.UtcNow.Date;
            return criteria;
        }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override void RunReport(User user, ReportResult result)
        {
            var query = RunQuery(user, result.Criteria);
            
            List<ResultData> resultItems = new List<ResultData>();
            query.Select(c => c.Employee.Id).ToList();
            foreach (var td in query)
                resultItems.Add(GetData(td));

            result.Items = resultItems.ToList();
        }

        /// <summary>
        /// Runs the query.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        protected virtual List<Case> RunQuery(User user, ReportCriteria criteria)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "Employee._id"));
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.CreatedDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.CreatedDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.ElemMatch(c => c.CaseEvents, e => e.And(Query.EQ("EventType", new BsonInt32(0)), 
                    e.Or(Query.GTE("EventDate", new BsonDateTime(startDate)), Query.GTE("EventDate", new BsonDateTime(endDate)))
                    )))
            ));

            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null).SetSortOrder(SortBy.Ascending("cdt")).ToList();
            return query;
        }

        private Dictionary<string, Customer> _cust = new Dictionary<string, Customer>();

        /// <summary>
        /// Gets the data.
        /// </summary>
        /// <param name="td">The td.</param>
        /// <returns></returns>
        protected virtual ResultData GetData(Case td)
        {
            var cust = _cust[td.CustomerId] = _cust.ContainsKey(td.CustomerId) ? _cust[td.CustomerId] : td.Customer;

            string displayCaseNumber = td.CaseNumber;
            string displayFullName = td.Employee.FullName;
            string displayReason = td.Reason == null ? string.Empty : td.Reason.Name ?? string.Empty;
            string displayLeaveType = td.CaseType.ToString();
            string displayCreatedOn = td.CreatedDate.ToString("MM/dd/yyyy");
            var evt = td.FindCaseEvent(CaseEventType.CaseCreated);
            if (evt != null && evt.EventDate != DateTime.MinValue)
                displayCreatedOn = evt.EventDate.ToString("MM/dd/yyyy");

            dynamic data = new ResultData();
            data.CaseNumber = string.IsNullOrWhiteSpace(displayCaseNumber) ? null : new LinkItem() { Value = td.CaseNumber, Link = string.Format("/Cases/{0}/View", td.Id) };
            data.EmployeeNumber = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = td.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", td.Employee.Id) };
            data.Name = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = td.Employee.FullName, Link = string.Format("/Employees/{0}/View", td.Employee.Id) };
            CustomField employeeClass = td.Employee.CustomFields.FirstOrDefault(cf => cf.Name == "EmployeeClass");
            data.Class = employeeClass == null ? "" : employeeClass.SelectedValueText;
            if (cust.HasFeature(Feature.ADA) && td.IsAccommodation && td.AccommodationRequest != null && td.AccommodationRequest.Accommodations != null && td.AccommodationRequest.Accommodations.Any())
                data.Duration = string.Join(", ", td.AccommodationRequest.Accommodations.Select(a => a.Duration.ToString().SplitCamelCaseString()).Distinct());
            else
                data.Duration = string.Empty;
            if (cust.HasFeature(Feature.LOA))
                data.Reason = displayReason ?? string.Empty;
            else
                data.Reason = string.Empty;
            data.CaseType = displayLeaveType ?? string.Empty;
            data.CreatedOn = displayCreatedOn ?? string.Empty;
            data.StartDate = td.StartDate.ToString("MM/dd/yyyy");
            data.EndDate = td.EndDate.HasValue ? td.EndDate.ToString("MM/dd/yyyy") : string.Empty;
            data.Status = td.Status.ToString().SplitCamelCaseString();
            if (cust.HasFeature(Feature.ADA) && td.IsAccommodation && td.AccommodationRequest != null && td.AccommodationRequest.Accommodations != null && td.AccommodationRequest.Accommodations.Any())
                data.Accommodations = string.Join(", ", td.AccommodationRequest.Accommodations.Select(a => a.Type.Code).Distinct());
            else
            {
                data.Accommodations = string.Empty;
            }
            data.AssignedTo = td.AssignedToName ?? string.Empty;
            data.WorkState = td.Employee.WorkState ?? string.Empty;
            data.Location = td.CurrentOfficeLocation ?? "";
            data.Email = td.Employee.Info.Email ?? string.Empty;
            if (td.AccommodationRequest != null && td.AccommodationRequest.Accommodations != null)
                data.WorkRelated = td.AccommodationRequest.Accommodations.Any(r => r.IsWorkRelated == true) ? "1" : td.AccommodationRequest.Accommodations.Any(r => r.IsWorkRelated == false) ? "0" : "";
            else
                data.WorkRelated = "";
            data.CostCenter = td.Employee.CostCenterCode ?? "";
            
            return data;
        }
    }
}
