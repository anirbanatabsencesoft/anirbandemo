﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using MongoDB.Driver;
using MongoDB.Bson;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Security;
using MongoDB.Driver.Builders;

namespace AbsenceSoft.Reporting.Reports.Custom._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269
{
    [Serializable]
    public class InteractiveProcessReport : AmazonReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "20BAA0BB-CB4C-4FF7-88A9-24B2AA85F1E7"; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 99; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Accommodation Status Report".
        /// </summary>
        public override string Category { get { return "Accommodation Interactive Process Report"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "Question Count"; } }

        /// <summary>
        /// Icon to set for Accommodation Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether to show the auto
        /// generated total counts at the bottom of the group page.
        /// </summary>
        /// <value>
        ///   <c>true</c> if group auto totals; otherwise, <c>false</c>.
        /// </value>
        public override bool GroupAutoTotals { get { return false; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get { return null; } }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria();
            criteria.CustomerId = user.CustomerId;
            criteria.StartDate = DateTime.UtcNow.Date.GetFirstDayOfMonth();
            criteria.EndDate = DateTime.UtcNow.Date;

            var questions = AccommodationQuestion.AsQueryable().Where(q => q.CustomerId == user.CustomerId).ToList();

            ReportCriteriaItem question = new ReportCriteriaItem()
            {
                ControlType = ControlType.SelectList,
                KeepOptionAll = false,
                Name = "Question",
                OnChangeMethodName = "FilterChangeEvent",
                Prompt = "Question",
                Required = true
            };
            question.Options = questions.Select(q => new ReportCriteriaItemOption()
            {
                GroupText = q.AccommodationTypeCode ?? "All",
                Value = q.Id,
                Text = (q.AccommodationType == null || string.IsNullOrWhiteSpace(q.AccommodationType.Name)) ? q.Question : q.AccommodationType.Name.Trim() + " - " + q.Question
            }).ToList();
            criteria.Filters.Add(question);

            ReportCriteriaItem answers = new ReportCriteriaItem()
            {
                ControlType = ControlType.SelectList,
                KeepOptionAll = true,
                Name = "Answer",
                OnChangeMethodName = "FilterChangeEvent",
                Prompt = "Answer",
                Tweener = "is counted if"
            };
            answers.Options = questions.SelectMany(q => q.Choices ?? new List<string>(0)).Select(c => new ReportCriteriaItemOption() { Value = c, Text = c }).ToList();
            answers.Options.InsertRange(0, new List<ReportCriteriaItemOption>(2)
            {
                new ReportCriteriaItemOption() { Text = "Yes", Value = true },
                new ReportCriteriaItemOption() { Text = "No", Value = false }
            });
            criteria.Filters.Add(answers);

            return criteria;
        }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override void RunReport(User user, ReportResult result)
        {
            var queryList = RunQuery(user, result.Criteria);

            List<ResultData> resultItems = new List<ResultData>();

            string questionId = null;
            object answer = null;
            var qFilter = result.Criteria.Filters.FirstOrDefault(m => m.Name == "Question");
            if (qFilter != null && qFilter.Value != null)
                questionId = qFilter.Value.ToString();

            var aFilter = result.Criteria.Filters.FirstOrDefault(m => m.Name == "Answer");
            if (aFilter != null && aFilter.Value != null)
                answer = aFilter.Value;

            foreach (var q in queryList)
                resultItems.Add(GetDetail(q, questionId, answer));

            result.Groups = new List<ResultDataGroup>(2)
            {
                new ResultDataGroup()
                {
                    Key = "Summary",
                    Label = "Summary",
                    Items = new List<ResultData>(1) { BuildSummary(resultItems, answer) }
                },
                new ResultDataGroup()
                {
                    Key = "Detail",
                    Label = "Detail",
                    Items = resultItems
                }
            };
        }

        /// <summary>
        /// Builds the summary.
        /// </summary>
        /// <param name="results">The results.</param>
        /// <returns></returns>
        protected ResultData BuildSummary(List<ResultData> results, object answer)
        {
            var textAnswer = answer == null ? "" : answer is bool ? (bool)answer ? "Yes" : "No" : answer.ToString().Replace(" ", "").Trim();
            var col = "Answered" + textAnswer;

            dynamic data = new ResultData();
            data.Metric = col;
            data.Count = !results.Any() ? "0" : results.Sum(r => r[col] == null ? 0 : Convert.ToInt32(r[col].ToString())).ToString("N0");
            return data;
        }

        /// <summary>
        /// Runs the query.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        protected virtual List<Case> RunQuery(User user, ReportCriteria criteria)
        {
            List<IMongoQuery> ands = BuildFilter(user, criteria);

            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null);
            return query.ToList();
        }

        /// <summary>
        /// Builds the filter.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        protected virtual List<IMongoQuery> BuildFilter(User user, ReportCriteria criteria)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "Employee._id"));
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            ands.Add(Case.Query.EQ(e => e.IsAccommodation, true));

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
            ));

            return ands;
        }
        /// <summary>
        /// Gets the detail.
        /// </summary>
        /// <param name="case">The case.</param>
        /// <param name="questionId">The question identifier.</param>
        /// <param name="answer">The answer.</param>
        /// <returns></returns>
        protected virtual ResultData GetDetail(Case @case, string questionId, object answer = null)
        {
            ResultData data = new ResultData();
            data["CaseNumber"] = new LinkItem() { Value = @case.CaseNumber, Link = string.Format("/Cases/{0}/View", @case.Id) };
            data["EmployeeNumber"] = new LinkItem() { Value = @case.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", @case.Employee.Id) };
            data["Name"] = new LinkItem() { Value = @case.Employee.FullName, Link = string.Format("/Employees/{0}/View", @case.Employee.Id) };

            int count = 0;
            if (@case.AccommodationRequest != null)
            {
                var q = @case.AccommodationRequest.AdditionalInfo == null ? null 
                    : @case.AccommodationRequest.AdditionalInfo.Steps == null ? null 
                    : @case.AccommodationRequest.AdditionalInfo.Steps.FirstOrDefault(s => s.QuestionId == questionId);
                if (q != null && ((answer == null && q.Answer == true) || (answer != null && answer is bool && (bool)answer == q.Answer) || (answer != null && q.SAnswer == answer.ToString())))
                    count++;

                if (@case.AccommodationRequest.Accommodations != null)
                foreach (var accomm in @case.AccommodationRequest.Accommodations)
                {
                    if (accomm.InteractiveProcess != null && accomm.InteractiveProcess.Steps != null)
                    {
                        q = accomm.InteractiveProcess.Steps.FirstOrDefault(s => s.QuestionId == questionId);
                        if (q != null && ((answer == null && q.Answer == true) || (answer != null && answer is bool && (bool)answer == q.Answer) || (answer != null && q.SAnswer == answer.ToString())))
                            count++;
                    }
                }
            }
            var textAnswer = answer == null ? "" : answer is bool ? (bool)answer ? "Yes" : "No" : answer.ToString().Replace(" ", "").Trim();
            data["Answered" + textAnswer] = count.ToString("N0");

            data["WorkState"] = @case.Employee.WorkState ?? "";
            data["Location"] = @case.CurrentOfficeLocation ?? "";

            return data;
        }
    }
}
