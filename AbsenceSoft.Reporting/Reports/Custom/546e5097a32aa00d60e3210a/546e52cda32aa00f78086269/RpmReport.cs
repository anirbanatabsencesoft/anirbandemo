﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic.Cases;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Reporting.Reports.Custom._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269
{
    [Serializable]
    public class RpmReport : AmazonReport
    {
        public override string Id { get { return "8783893E-A3C9-4024-B044-6B1E3D680289"; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 0; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether to show the auto
        /// generated total counts at the bottom of the group page.
        /// </summary>
        /// <value>
        ///   <c>true</c> if group auto totals; otherwise, <c>false</c>.
        /// </value>
        public override bool GroupAutoTotals { get { return false; } }

        /// <summary>
        /// Category is "Operations Report".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Accommodation Status Report".
        /// </summary>
        public override string Category { get { return "Accommodations Report"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "RPM"; } }

        /// <summary>
        /// Icon to set for Business intelligence report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_intelligence-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get { return null; } }

        /// <summary>
        /// Determines whether the specified report is visible to the given user for any valid reason.
        /// </summary>
        /// <param name="user">The user to check against.</param>
        /// <returns>
        ///   <c>true</c> if the specified report is visible; otherwise, <c>false</c>.
        /// </returns>
        public override bool IsVisible(User user)
        {
            // Hard Coded to FALSE for AMZ
            return false;
        }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria();
            criteria.CustomerId = user.CustomerId;
            criteria.StartDate = DateTime.UtcNow.Date.AddMonths(-1).GetFirstDayOfMonth();
            criteria.EndDate = criteria.StartDate.GetLastDayOfMonth();
            return criteria;
        }

        /// <summary>
        /// The users for mapping users with their respective roles
        /// </summary>
        private Dictionary<string, string> users = null;

        // Runtime stuff, yikes.
        private DateTime startDate;
        private DateTime endDate;

        /// <summary>
        /// Gets a value indicating whether this instance is exportable to PDF.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is exportable to PDF; otherwise, <c>false</c>.
        /// </value>
        public override bool IsExportableToPdf { get { return false; } }

        /// <summary>
        /// Gets a value indicating whether this instance is viewable.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is viewable; otherwise, <c>false</c>.
        /// </value>
        public override bool IsViewable
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override void RunReport(User user, ReportResult result)
        {
            var cases = RunQuery(user, result.Criteria);

            startDate = result.Criteria.StartDate.ToMidnight();
            endDate = result.Criteria.EndDate.EndOfDay();

            // Get all Office Locations
            string[] locations = cases
                .Where(c => c.Employee != null && c.Employee.Info != null)
                .Select(c => c.CurrentOfficeLocation)
                .Where(l => l != null)
                .Distinct()
                .OrderBy(l => l)
                .ToArray();

            // Get call accommodation types
            string[] accommTypes =
                GetAccommodationsFromCases(cases)
                .Where(a => a.Type != null)
                .Select(a => a.Type)
                .Select(t => t.Code)
                .OrderBy(c => c)
                .Distinct()
                .ToArray();

            // Get all customer users
            users = User.AsQueryable()
                .Where(u => u.CustomerId == user.CustomerId)
                .Where(u => u.Roles != null)
                .ToList()
                .Where(r => r.Roles.Any())
                .ToDictionary(u => u.Id, u => u.Roles.First());

            // Get matching roles for those users
            var userRoles = Role.AsQueryable()
                .Where(r => r.CustomerId == user.CustomerId)
                .Where(r => users.Values.Contains(r.Id))
                .ToDictionary(r => r.Id, r => r.Name);

            // Add the system administrator role, 'cause it's not in the DB
            userRoles.Add(Role.SystemAdministrator.Id, Role.SystemAdministrator.Name);

            // GEt a hashtable of our grouped cases so we can quickly lookup collections of cases by location
            var groupedCases = cases.Where(c => !string.IsNullOrWhiteSpace(c.CurrentOfficeLocation))
                .GroupBy(c => c.CurrentOfficeLocation)
                .ToDictionary(g => g.Key, g => g.ToList());

            GetAllHeadcountData(result.Criteria);
            GetAllTouchData(result.Criteria, cases, users, userRoles);

            // Nifty function that builds a dynamic awesome result data for all locations given some description
            Func<string, Func<string, string, List<Case>, string>, ResultData> locationData = new Func<string, Func<string, string, List<Case>, object>, ResultData>((description, fetcher) =>
            {
                ResultData data = new ResultData();
                data["Description"] = description ?? "";
                foreach (var loc in locations)
                {
                    object val = 0;
                    if (fetcher != null && groupedCases.ContainsKey(loc) && groupedCases[loc] != null && groupedCases[loc].Any())
                        val = fetcher(user.CustomerId, loc, groupedCases[loc]);
                    data[loc] = val;
                }
                return data;
            });
            Func<string, Func<string, string, List<Case>, string, string>, string, ResultData> locationByCodeData = new Func<string, Func<string, string, List<Case>, string, object>, string, ResultData>((description, fetcher, accommTypeCode) =>
            {
                ResultData data = new ResultData();
                data["Description"] = description ?? "";
                foreach (var loc in locations)
                {
                    object val = 0;
                    if (fetcher != null && groupedCases.ContainsKey(loc) && groupedCases[loc] != null && groupedCases[loc].Any())
                        val = fetcher(user.CustomerId, loc, groupedCases[loc], accommTypeCode);
                    data[loc] = val;
                }
                return data;
            });

            using (new InstrumentationContext("RpmReport.RunReport.BuildingQuery"))
            {

                // Create our data set (wowwee zowwie!)
                List<ResultDataGroup> groups = new List<ResultDataGroup>()
            {
                // Population
                //  - Total Headcount (NOTE: includes "as of" date, as of most recent EL)
                //  - New Cases in Month (NOTE: allocated to period based on case create date)
                //  - Annualized Case Incidence Rate per 100  ( [New Cases] / [Headcount] * 12 * 100 )
                new ResultDataGroup()
                {
                    Key = "POP",
                    Label = "Population",
                    Items = new List<ResultData>()
                    {
                        locationData("Total Headcount", CalcHeadcount),
                        locationData("New Cases in Month", (c, l, list) => list.Count(r => r.CreatedDate.IsInRange(result.Criteria.StartDate, result.Criteria.EndDate)).ToString("N0")),
                        locationData("Annualized Case Incidence Rate per 100", (c, l, list) => 
                        {
                            // This probably needs to be optimized, as it may slow down the report running so many queries, but we'll see....
                            //  it will only grow as to the number of physical locations that are added in the future.
                            var headcount = CalcHeadcount(c, l, list);
                            var h = int.Parse(headcount, System.Globalization.NumberStyles.Number);
                            if (h == 0) return "0";
                            var n = list.Count(r => r.CreatedDate.IsInRange(result.Criteria.StartDate, result.Criteria.EndDate));
                            return (Convert.ToDecimal(n / h) * 12M * 100M).ToString("N1");
                        }),
#warning AT-1657 - RPM Reports - How many cases are open prior to month/date selection
                        //locationData("Cases prior to Month", (c, l, list) => 
                        //{
                        //    // Grab the total count of cases opened BEFORE the report criteria start date
                        //    return Case.AsQueryable().Where(r => r.CreatedDate < result.Criteria.StartDate).Count().ToString("N0");
                        //})
                    }
                },
                // Total Accommodation Cases Open at EOM (NOTE: Cases allocated to category based on the latest type start date; COFA = COFA, R4C & Other accomodation types)
                //  - by ACCOMM TYPE (count)
                new ResultDataGroup()
                {
                    Key = "OPEN",
                    Label = "Total Accommodation Cases Open at EOM",
                    Items = accommTypes.Select(t => locationByCodeData(t, CalcOpenAccomms, t)).ToList()
                        .AddRangeFluid(new List<ResultData>(1) { locationByCodeData("Total", CalcOpenAccomms, null) })
                },
                // Accommodations Requested (count)
                //  - by ACCOMM TYPE (count)
                new ResultDataGroup()
                {
                    Key = "REQUESTED",
                    Label = "Accommodations Requested",
                    Items = accommTypes.Select(t => locationByCodeData(t, CalcRequestedAccomms, t)).ToList()
                        .AddRangeFluid(new List<ResultData>(1) { locationByCodeData("Total", CalcRequestedAccomms, null) })
                },
                // Accommodations Granted (NOTE: Claim type level count - allocated to month based on decision date)
                //  - by ACCOMM TYPE (count)
                new ResultDataGroup()
                {
                    Key = "GRANTED",
                    Label = "Accommodations Granted",
                    Items = accommTypes.Select(t => locationByCodeData(t, CalcGrantedAccomms, t)).ToList()
                        .AddRangeFluid(new List<ResultData>(1) { locationByCodeData("Total", CalcGrantedAccomms, null) })
                },
                // Accommodations Denied (NOTE: Claim type level count - allocated to month based on decision date)
                //  - by ACCOMM TYPE (count)
                new ResultDataGroup()
                {
                    Key = "DENIED",
                    Label = "Accommodations Denied",
                    Items = accommTypes.Select(t => locationByCodeData(t, CalcDeniedAccomms, t)).ToList()
                        .AddRangeFluid(new List<ResultData>(1) { locationByCodeData("Total", CalcDeniedAccomms, null) })
                },
                // Time to decision for accommodations with decisions in month (NOTE: Calculated from the date requested to the date resolved.  Date requested = earlier of the create date and the start date.)
                //  - by ACCOMM TYPE (count)
                new ResultDataGroup()
                {
                    Key = "TTD",
                    Label = "Time to decision for accommodations with decisions in month",
                    Items = accommTypes.Select(t => locationByCodeData(t, CalcAccommTimeToDecision, t)).ToList()
                },
                // Closed Cases
                //  - by ACCOMM TYPE (count)
                new ResultDataGroup()
                {
                    Key = "CLOSED",
                    Label = "Closed Cases",
                    Items = accommTypes.Select(t => locationByCodeData(t, CalcClosedAccomms, t)).ToList()
                        .AddRangeFluid(new List<ResultData>(1) { locationByCodeData("Total", CalcClosedAccomms, null) })
                },
                // Time to Resolution (NOTE: Time to close:  Start date = earlier of the create date and the start date.  End time = date the case closed (timestamp))
                //  - Time to resolution for Simple cases closed in month (NOTE: Considered simple if not complex (COF and COFa, COF off Work, R4C))
                //  - Time to resolution for Complex cases closed in month (NOTE: Considered complex if case contains LTA, DA, TWA, TA, or LEA)
                new ResultDataGroup()
                {
                    Key = "TTR",
                    Label = "Time to Resolution",
                    Items = new List<ResultData>()
                    {
                        locationData("Time to resolution for Simple cases closed in month", TimeToResolutionSimple),
                        locationData("Time to resolution for Complex cases closed in month", TimeToResolutionComplex)
                    }
                },
                // Touches (count) (NOTE: touch = attachments, notes, manually sent letter, and completed todo)
                //  - by User Role
                new ResultDataGroup()
                {
                    Key = "TOUCH",
                    Label = "Touches",
                    Items = GroupTouchData(user.CustomerId, locations, userRoles, groupedCases)
                },
                // Tiered Work Count/% (NOTE: Count of Open Cases in each Category, See Table Tab for list of Simple & Complex Case types)
                //  - Simple (count)
                //  - Complex (count)
                //  - Complex Consult (count) (NOTE: Consults are estimated at 21% of open EOM cases; [Total Accommodation Cases Open at EOM] * 0.21)
                new ResultDataGroup()
                {
                    Key = "WORK",
                    Label = "Tiered Work Count/%",
                    Items = new List<ResultData>()
                    {
                        locationData("Simple", CountSimple),
                        locationData("Complex", CountComplex),
                        locationData("Complex Consult", CountConsult),
                    }
                }
            };

                // All that hard work for this one little bitty line of code, sheesh!
                result.Groups = groups;
            }

        }

        Dictionary<string, int> headcountByLocationCounts = new Dictionary<string, int>();
        private string CalcHeadcount(string customerId, string location, List<Case> cases)
        {
            using (new InstrumentationContext(string.Format("RpmReport.CalcHeadcount({0})", location)))
            {
                string loc = location ?? "TOTAL";

                if (headcountByLocationCounts.ContainsKey(loc))
                    return headcountByLocationCounts[loc].ToString("N0");
                int count = Employee.AsQueryable().Where(e => e.CustomerId == customerId && e.Info.OfficeLocation == location && e.Status != EmploymentStatus.Terminated).Count();
                headcountByLocationCounts.Add(loc, count);
                return count.ToString("N0");
            }
        }

        Dictionary<string, int> openAccomsByLocationCounts = new Dictionary<string, int>();
        private string CalcOpenAccomms(string customerId, string location, List<Case> cases, string accommTypeCode)
        {
            using (new InstrumentationContext(string.Format("RpmReport.CalcOpenAccomms({0}, {1})", location, accommTypeCode)))
            {
                string loc = location ?? "TOTAL";
                string key = loc + accommTypeCode;
                if (openAccomsByLocationCounts.ContainsKey(key))
                    return openAccomsByLocationCounts[key].ToString("N0");
                // Total Accommodation Cases Open at EOM (NOTE: Cases allocated to category based on the latest type start date; COFA = COFA, R4C & Other accomodation types)
                //  - by ACCOMM TYPE (count)
                int openAccommsCount = GetAccommodationsFromCases(cases, accommTypeCode, CaseStatus.Open, loc)
                    .Count();

                openAccomsByLocationCounts.Add(key, openAccommsCount);
                return openAccommsCount.ToString("N0");
            }
        }

        private string CalcOpenAccommsPercentage(string customerId, string location, List<Case> cases, string accommTypeCode)
        {
            using (new InstrumentationContext(string.Format("RpmReport.CalcOpenAccommsPercentage({0}, {1})", location, accommTypeCode)))
            {
                int me = int.Parse(CalcOpenAccomms(customerId, location, cases, accommTypeCode), NumberStyles.Number);
                int all = int.Parse(CalcOpenAccomms(customerId, location, cases, null), NumberStyles.Number);
                return string.Format("{0:N2}%", (me / all * 100M));
            }
        }

        private string CalcClosedAccomms(string customerId, string location, List<Case> cases, string accommTypeCode)
        {
            // Total Accommodation Cases Closed at EOM (NOTE: Cases allocated to category based on the latest type start date; COFA = COFA, R4C & Other accomodation types)
            //  - by ACCOMM TYPE (count)

            using (new InstrumentationContext(string.Format("RpmReport.CalcClosedAccomms({0}, {1})", location, accommTypeCode)))
            {
                int count = GetAccommodationsFromCases(cases, accommTypeCode, CaseStatus.Closed, location)
                    .Count();

                return count.ToString("N0");
            }
        }

        private string CalcRequestedAccomms(string customerId, string location, List<Case> cases, string accommTypeCode)
        {
            using (new InstrumentationContext(string.Format("RpmReport.CalcRequestedAccomms({0}, {1})", location, accommTypeCode)))
            {
                int count =
                    GetAccommodationsFromCases(cases, accommTypeCode)
                    .Where(a => a.CreatedDate.DateInRange(startDate, endDate))
                    .Where(a => a.Status != CaseStatus.Cancelled)
                    .Count();

                return count.ToString("N0");
            }

        }

        private string CalcGrantedAccomms(string customerId, string location, List<Case> cases, string accommTypeCode)
        {
            using (new InstrumentationContext(string.Format("RpmReport.CalcGrantedAccomms({0}, {1})", location, accommTypeCode)))
            {
                int count =
                    GetAccommodationsFromCases(cases, accommTypeCode)
                    .Where(a => a.Status != CaseStatus.Cancelled)
                    .Where(a => a.Granted && a.GrantedDate.HasValue && a.GrantedDate.IsInRange(startDate, endDate))
                    .Count();

                return count.ToString("N0");
            }
        }

        private string CalcDeniedAccomms(string customerId, string location, List<Case> cases, string accommTypeCode)
        {
            using (new InstrumentationContext(string.Format("RpmReport.CalcDeniedAccomms({0}, {1})", location, accommTypeCode)))
            {
                int count =
                    GetAccommodationsFromCases(cases, accommTypeCode)
                    .Where(a => a.Status != CaseStatus.Cancelled)
                    .Where(a => a.Determination == AdjudicationStatus.Denied)
                    .Count();
                return count.ToString("N0");
            }

        }

        private string CalcAccommTimeToDecision(string customerId, string location, List<Case> cases, string accommTypeCode)
        {
            using (new InstrumentationContext(string.Format("RpmReport.CalcAccommTimeToDecision({0}, {1})", location, accommTypeCode)))
            {
                var accommSequence = GetAccommodationsFromCases(cases, accommTypeCode)
                    .Where(a => a.GrantedDate.HasValue || a.ResolvedDate.HasValue)
                    .Where(a => (a.GrantedDate ?? a.ResolvedDate).IsInRange(startDate, endDate));

                if (!accommSequence.Any())
                    return "0";

                var resolutionTimes = accommSequence.Select(a =>
                {
                    var decisionDate = (a.GrantedDate ?? a.ResolvedDate);
                    if (decisionDate.HasValue)
                        return Convert.ToDecimal(decisionDate.Value.Subtract(a.CreatedDate).TotalDays);

                    return 0;
                });

                var avgDays = resolutionTimes.Average();
                return Math.Round(avgDays, 0).ToString("N0");
            }
        }

        private string TimeToResolutionSimple(string customerId, string location, List<Case> cases)
        {
            // Time to Resolution (NOTE: Time to close:  Start date = earlier of the create date and the start date.  End time = date the case closed (timestamp))
            //  - Time to resolution for Simple Accommodation created date to Granted Date
            return TimeToResolution(customerId, location, cases, false);
        }

        private string TimeToResolutionComplex(string customerId, string location, List<Case> cases)
        {
            // Time to Resolution (NOTE: Time to close:  Start date = earlier of the create date and the start date.  End time = date the case closed (timestamp))
            //  - Time to resolution for Complex cases closed in month (NOTE: Considered complex if case contains LTA, DA, TWA, TA, or LEA)
            return TimeToResolution(customerId, location, cases, true);
        }

        private string TimeToResolution(string customerId, string location, List<Case> cases, bool isComplex)
        {
            using (new InstrumentationContext(string.Format("RpmReport.TimeToResolution({0}, {1})", location, isComplex)))
            {
                var accommodationSequence = GetAccommodationsFromCases(cases, null, null, location)
                    .Where(a => a.Type != null && IsComplex(a.Type.Code) == isComplex)
                    .Where(a => a.Granted && a.GrantedDate.HasValue);

                if (!accommodationSequence.Any())
                    return "0";

                var avgDays = accommodationSequence.Average(a =>
                {
                    var from = a.StartDate ?? a.CreatedDate;
                    if (a.StartDate.HasValue && from > a.CreatedDate)
                        from = a.CreatedDate;
                    return (a.GrantedDate - from).HasValue ? (a.GrantedDate - from).Value.TotalDays : 0;
                });

                return Math.Round(avgDays, 0).ToString("N0");
            }

        }

        private string CountSimple(string customerId, string location, List<Case> cases)
        {
            using (new InstrumentationContext(string.Format("RpmReport.CountSimple({0})", location)))
            {
                int count =
                    GetAccommodationsFromCases(cases, null, null, location)
                    .Where(a => a.Status != CaseStatus.Cancelled)
                    .Where(a => a.Type != null && !IsComplex(a.Type.Code))
                    .Count();
                return count.ToString("N0");
            }
        }

        private Dictionary<string, int> locationComplexCounts = new Dictionary<string, int>();
        private string CountComplex(string customerId, string location, List<Case> cases)
        {
            using (new InstrumentationContext(string.Format("RpmReport.CountComplex({0})", location)))
            {
                if (locationComplexCounts.ContainsKey(location))
                    return locationComplexCounts[location].ToString("N0");

                int count =
                    GetAccommodationsFromCases(cases, null, null, location)
                    .Where(a => a.Status != CaseStatus.Cancelled)
                    .Where(a => a.Type != null && IsComplex(a.Type.Code))
                    .Count();

                locationComplexCounts.Add(location, count);
                return count.ToString("N0");
            }

        }

        private string CountConsult(string customerId, string location, List<Case> cases)
        {
            using (new InstrumentationContext(string.Format("RpmReport.CountConsult({0})", location)))
            {
                // Complex Consult (count) (NOTE: Consults are estimated at 21% of open EOM cases; [Total Accommodation Cases Open at EOM] * 0.21)
                int all = int.Parse(CalcOpenAccomms(customerId, location, cases, null), NumberStyles.Number);
                decimal count = Math.Round(all * 0.21M, 0);
                return count.ToString("N0");
            }

        }

        private List<ResultData> GroupTouchData(string customerId, string[] locations, Dictionary<string, string> roles, Dictionary<string, List<Case>> groupedCases)
        {
            List<ResultData> resultData = new List<ResultData>();

            foreach (string role in roles.Values)
            {
                ResultData roleData = new ResultData();
                roleData["Description"] = role;
                foreach (string loc in locations)
                {
                    roleData[loc] = TouchesForRole(customerId, loc, groupedCases[loc], role);
                }
                resultData.Add(roleData);
                
            }
            
            ResultData totalData = new ResultData();
            totalData["Description"] = "Total";
            foreach (var loc in locations)
            {
                totalData[loc] = TouchesForRole(customerId, loc, groupedCases[loc], "Total");    
            }
            resultData.Add(totalData);


            return resultData;
        }

        Dictionary<string, int?> mappedTouchData = new Dictionary<string, int?>();
        private string TouchesForRole(string customerId, string location, List<Case> cases, string role)
        {
            string key = BuildTouchKey(role, location);
            if (mappedTouchData.ContainsKey(key))
                return mappedTouchData[key].HasValue ? mappedTouchData[key].Value.ToString("N0") : "0";

            int count = BuildTouchData(cases, role);
            return count.ToString("N0");

        }

        /// <summary>
        /// The complex types
        /// </summary>
        private static readonly string[] complexTypes = new string[] { "DA", "FFD", "LEV", "LTA", "TA", "WPV", };
        /// <summary>
        /// Determines whether the specified accom code is complex.
        /// </summary>
        /// <param name="accomCode">The accom code.</param>
        /// <returns></returns>
        protected bool IsComplex(string accomCode)
        {
            if (string.IsNullOrWhiteSpace(accomCode))
                return false;
            return complexTypes.Contains(accomCode);
        }

        /// <summary>
        /// Separates all the accommodations from the cases
        /// </summary>
        /// <param name="cases"></param>
        /// <param name="accommTypeCode"></param>
        /// <param name="status"></param>
        /// <param name="location"></param>
        /// <returns></returns>
        private IEnumerable<Accommodation> GetAccommodationsFromCases(IEnumerable<Case> cases, string accommTypeCode = null, CaseStatus? status = null, string location = null)
        {
            using (new InstrumentationContext(string.Format("RpmReport.GetAccommodationsFromCases({0}, {1}, {2})", accommTypeCode, status, location)))
            {
                IEnumerable<Case> casesWithAccommodations = cases.Where(c => c.IsAccommodation)
                    .Where(c => c.AccommodationRequest != null)
                    .Where(c => c.AccommodationRequest.Accommodations != null);

                if (status.HasValue)
                    casesWithAccommodations = casesWithAccommodations.Where(c => c.Status == status || c.AccommodationRequest.Accommodations.Any(a => a.Status == status));

                if (!string.IsNullOrEmpty(location) && location != "TOTAL")
                    casesWithAccommodations = casesWithAccommodations.Where(c => c.CurrentOfficeLocation == location);


                // If AMZ kicks back and says they need case counts instead of accommodations
                // Modify here to count with the accommodation request of any
                IEnumerable<Accommodation> accommodations =
                    casesWithAccommodations.SelectMany(c => c.AccommodationRequest.Accommodations);

                if (!string.IsNullOrEmpty(accommTypeCode))
                    accommodations = accommodations.Where(a => a.Type != null && a.Type.Code == accommTypeCode);

                if (status.HasValue)
                    accommodations = accommodations.Where(a => a.Status == status);

                return accommodations;
            }

        }

        #region Query Stuff

        /// <summary>
        /// Runs the query.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        protected virtual List<Case> RunQuery(User user, ReportCriteria criteria)
        {
            using (new InstrumentationContext("RpmReport.RunQuery"))
            {
                List<IMongoQuery> ands = BuildFilter(user, criteria);
                var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null);
                return query.ToList();
            }
        }

        /// <summary>
        /// Builds the filter.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        protected virtual List<IMongoQuery> BuildFilter(User user, ReportCriteria criteria)
        {
            using (new InstrumentationContext("RpmReport.BuildFilter"))
            {
                DateTime startDate = criteria.StartDate.ToMidnight();
                DateTime endDate = criteria.EndDate.EndOfDay();

                List<IMongoQuery> ands = new List<IMongoQuery>();
                if (user != null)
                    ands.Add(user.BuildDataAccessFilters(Permission.RunBIReport, "Employee._id"));
                else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
                    ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));

                // Must match a range for dates, not just start and end within that range, it's
                //  any leave that intersects the date range selected by the user.
                ands.Add(Case.Query.Or(
                    Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                    Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate))),
                    // Also grab any cases that were created OR modified within the date parameters.
                    Case.Query.And(Case.Query.GTE(e => e.CreatedDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.CreatedDate, new BsonDateTime(endDate))),
                    Case.Query.And(Case.Query.GTE(e => e.ModifiedDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.ModifiedDate, new BsonDateTime(endDate))),
                    // Also grab any cases that were closed within the date parameters.
                    Case.Query.And(Case.Query.ElemMatch(e => e.CaseEvents, b => b.And(
                        b.EQ(e => e.EventType, CaseEventType.CaseClosed),
                        b.ElemMatch(e => e.AllEventDates, f => new QueryDocument(new List<BsonElement>() {
                        new BsonElement("$gte", new BsonDateTime(startDate)),
                        new BsonElement("$lte", new BsonDateTime(endDate))
                    }))
                    )))
                ));

                return ands;
            }

        }

        /// <summary>
        /// Builds the touch data.
        /// </summary>
        /// <param name="cases">The cases.</param>
        protected virtual int BuildTouchData(List<Case> cases, string roleName)
        {
            using (new InstrumentationContext(string.Format("RpmReport.BuildTouchData({0})", roleName)))
            {
                int count = 0;
                var caseIds = cases.Select(c => c.Id).ToList();
                if (!caseIds.Any())
                    return 0;
                var userIds = users.Where(u => string.IsNullOrWhiteSpace(roleName) || u.Value == roleName).Select(u => u.Key).ToList();
                if (!userIds.Any())
                    return 0;
                List<IMongoQuery> noteAnds = new List<IMongoQuery>();
                noteAnds.Add(CaseNote.Query.In(n => n.CaseId, caseIds));
                noteAnds.Add(CaseNote.Query.In(n => n.CreatedById, userIds));
                List<IMongoQuery> attachmentAnds = new List<IMongoQuery>();
                attachmentAnds.Add(Attachment.Query.In(n => n.CaseId, caseIds));
                attachmentAnds.Add(Attachment.Query.In(n => n.CreatedById, userIds));
                List<IMongoQuery> communicationAnds = new List<IMongoQuery>();
                communicationAnds.Add(Communication.Query.In(n => n.CaseId, caseIds));
                communicationAnds.Add(Communication.Query.In(n => n.CreatedById, userIds));
                List<IMongoQuery> todoAnds = new List<IMongoQuery>();
                todoAnds.Add(ToDoItem.Query.In(n => n.CaseId, caseIds));
                todoAnds.Add(ToDoItem.Query.In(n => n.CreatedById, userIds));


                var noteCount = CaseNote.Query.Find(Query.And(noteAnds)).Count();
                var attachmentCount = Attachment.Query.Find(Query.And(attachmentAnds)).Count();
                var communicationCount = Communication.Query.Find(Query.And(noteAnds)).Count();
                var todoCount = ToDoItem.Query.Find(Query.And(noteAnds)).Count();
                count = Convert.ToInt32(noteCount + attachmentCount + communicationCount + todoCount);
                return count;
            }
        }

        private void GetAllHeadcountData(ReportCriteria criteria)
        {
            AggregateArgs args = new AggregateArgs();
            args.OutputMode = AggregateOutputMode.Inline;
            args.Pipeline = new List<BsonDocument>()
                {
                    new BsonDocument(new Dictionary<string, object>()
                    {
                        {
                            "$match",
                            new BsonDocument(new Dictionary<string, object>()
                            {
                                { "CustomerId", new ObjectId(criteria.CustomerId) },
                                { "Status", new BsonDocument(new Dictionary<string, object>()
                                    {
                                        { "$ne", "T" }
                                    })
                                }
                            })
                        }
                    }),
                    new BsonDocument(new Dictionary<string, object>()
                    {
                        {
                            "$group",
                            new Dictionary<string, object>()
                            {
                                { "_id", "$Info.OfficeLocation" },
                                {
                                    "count",
                                    new BsonDocument(new Dictionary<string, object>()
                                    {
                                        { "$sum", 1 }
                                    })
                                }
                            }
                        }
                    }),
                };

            var headcounts = Employee.Repository.Collection.Aggregate(args).ToList();
            Dictionary<string, int> finalHeadcounts = new Dictionary<string, int>();
            foreach (var count in headcounts)
            {
                string k = count.GetRawValue<string>("_id");
                int v = count.GetRawValue<int>("count");

                if (!string.IsNullOrWhiteSpace(k) && !finalHeadcounts.ContainsKey(k))
                    finalHeadcounts.Add(k, v);
            }

            finalHeadcounts.Add("TOTAL", finalHeadcounts.Sum(s => s.Value));
            headcountByLocationCounts = finalHeadcounts;
        }

        private void GetAllTouchData(ReportCriteria criteria, List<Case> cases, Dictionary<string, string> users, Dictionary<string, string> userRoles)
        {
            string collectionName = RandomString.Generate(16, true, false, false, false, false);
            MongoCollection collection = Case.Repository.Collection.Database.GetCollection(collectionName);
            MapReduceTouchData(collectionName, cases);
            List<TouchData> touchData = collection.FindAs<TouchData>(null).ToList();
            MapTouchDataToRolesAndLocations(touchData, cases, users, userRoles);
            collection.Drop();
        }

        private void MapTouchDataToRolesAndLocations(List<TouchData> touchData, List<Case> cases, Dictionary<string, string> users, Dictionary<string, string> userRoles)
        {
            Dictionary<string, int?> finalMappedTouchData = new Dictionary<string, int?>();
            Dictionary<string, int?> roleTotalData = new Dictionary<string, int?>();
            foreach (var touch in touchData)
            {
                Case matchingCase = cases.FirstOrDefault(c => c.Id == touch._id.CaseId);
                if (matchingCase == null)
                    continue;

                if (!users.ContainsKey(touch._id.CreatedById))
                    continue;

                string userRoleId = users[touch._id.CreatedById];

                if (!userRoles.ContainsKey(userRoleId))
                    continue;

                string roleName = userRoles[userRoleId];
                string location = matchingCase.CurrentOfficeLocation;
                string key = BuildTouchKey(roleName, location);
                string roleKey = BuildTouchKey("Total", location);
                if (finalMappedTouchData.ContainsKey(key))
                    finalMappedTouchData[key] += touch.value;
                else
                    finalMappedTouchData.Add(key, touch.value);

                if (roleTotalData.ContainsKey(roleKey))
                    roleTotalData[roleKey] += touch.value;
                else
                    roleTotalData.Add(roleKey, touch.value);
            }

            foreach (var roleTotal in roleTotalData)
            {
                finalMappedTouchData.Add(roleTotal.Key, roleTotal.Value);
            }

            mappedTouchData = finalMappedTouchData;
        }


        /// <summary>
        /// Reduces all the touch data to an easily queried collection
        /// </summary>
        /// <param name="collectionName"></param>
        private void MapReduceTouchData(string collectionName, List<Case> cases)
        {
            StringBuilder caseMap = new StringBuilder();
            caseMap.AppendLine("function() {");
            caseMap.AppendLine("   emit({ CaseId: this.CaseId, CreatedById: this.cby }, 1);");
            caseMap.AppendLine("}");

            StringBuilder reduceToTouches = new StringBuilder();
            reduceToTouches.AppendLine("function(key, values) {");
            reduceToTouches.AppendLine("  return Array.sum(values);");
            reduceToTouches.AppendLine("}");


            List<IMongoQuery> caseAnds = new List<IMongoQuery>();
            caseAnds.Add(ToDoItem.Query.In(c => c.CaseId, cases.Select(c => c.Id)));
            IMongoQuery query = Case.Query.And(caseAnds);

            var mapReduce = new MapReduceArgs()
            {
                OutputMode = MapReduceOutputMode.Reduce,
                OutputCollectionName = collectionName,
                Query = query,
                MapFunction = new BsonJavaScript(caseMap.ToString()),
                ReduceFunction = new BsonJavaScript(reduceToTouches.ToString())
            };

            if (Case.Repository.Collection.Database.GetCollection("ToDoItem").Exists())
                ToDoItem.Repository.Collection.MapReduce(mapReduce);

            if (Case.Repository.Collection.Database.GetCollection("Attachment").Exists())
                Attachment.Repository.Collection.MapReduce(mapReduce);

            if (Case.Repository.Collection.Database.GetCollection("CaseNote").Exists())
                CaseNote.Repository.Collection.MapReduce(mapReduce);
        }



        private string BuildTouchKey(string role, string location)
        {
            return string.Format("{0}-{1}", role, location);
        }

        #endregion Query Stuff
    }

    public class TouchData
    {
        public TouchId _id { get; set; }
        public int value { get; set; }
    }

    public class TouchId
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string CaseId { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string CreatedById { get; set; }
    }
}
