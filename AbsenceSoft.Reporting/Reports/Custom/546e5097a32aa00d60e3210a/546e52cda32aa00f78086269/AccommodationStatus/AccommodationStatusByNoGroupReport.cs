﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Reporting.Reports.Custom._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269.AccommodationStatus
{
    [Serializable]
    public sealed class AccommodationStatusByNoGrroupReport : AccommodationStatusReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "0A3B77C2-374F-49ED-859F-54FDFE1066E4"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "Detail"; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return false; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 0; } }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override void RunReport(User user, ReportResult result)
        {
            var queryList = RunQuery(user, result.Criteria).ToList();

            AdjudicationStatus? accomStatus = null;
            string accomType = string.Empty;

            if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "Determination") != null &&
                  result.Criteria.Filters.FirstOrDefault(m => m.Name == "Determination").Value != null)
            {
                accomStatus = result.Criteria.ValueOf<AdjudicationStatus>("Determination");
            }

            if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "AccomType") != null && result.Criteria.Filters.FirstOrDefault(m => m.Name == "AccomType").Value != null)
            {
                accomType = result.Criteria.Filters.FirstOrDefault(m => m.Name == "AccomType").Value.ToString();
            }

            List<ResultData> resultItems = new List<ResultData>();
            
            foreach (var q in queryList.Where(c => c.AccommodationRequest != null))
            {
                q.AccommodationRequest.Accommodations = q.AccommodationRequest.Accommodations ?? new List<Accommodation>(0);
                string displayCaseNumber = q.CaseNumber;
                string displayFullName = q.Employee.FullName;

                foreach (var accom in q.AccommodationRequest.Accommodations.Where(a => a.Type != null).OrderBy(p => p.Type.Code))
                {
                    if (accomStatus.HasValue && accomStatus.Value != accom.Determination)
                        continue;

                    if (!string.IsNullOrWhiteSpace(accomType) && accom.Type.Code != accomType)
                        continue;

                    foreach (var detail in accom.Usage.Where(u => u.StartDate.DateRangesOverLap(u.EndDate, result.Criteria.StartDate, result.Criteria.EndDate)).OrderBy(u => u.StartDate))
                        resultItems.Add(GetDetail(q, accom, detail, ref displayCaseNumber, ref displayFullName));
                }

                if (!q.AccommodationRequest.Accommodations.Where(a => a.Type != null).Any())
                    resultItems.Add(GetBlankDetail(q, ref displayCaseNumber, ref displayFullName));
            }

            result.Items = resultItems;
        }
    }
}
