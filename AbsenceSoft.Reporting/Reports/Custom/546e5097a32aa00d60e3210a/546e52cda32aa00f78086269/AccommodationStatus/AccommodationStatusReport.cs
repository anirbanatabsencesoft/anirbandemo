﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Reporting.Reports.Custom._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269.AccommodationStatus
{
    [Serializable]
    public abstract class AccommodationStatusReport : AmazonReport
    {
        /// <summary>
        /// Category for all Accommodation Status reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all Accommodation Status reports is "Accommodation Status Report".
        /// </summary>
        public override string Category { get { return "Accommodation Status Report"; } }

        /// <summary>
        /// Icon to set for Accommodation Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get { return null; } }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria();
            criteria.CustomerId = user.CustomerId;
            criteria.StartDate = DateTime.UtcNow.Date.AddDays(-30);
            criteria.EndDate = DateTime.UtcNow.Date;
            ReportCriteriaItem item = GetCaseStatusCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetAccomTypeCriteria(user);
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetAdjudicationCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            criteria.Filters.Add(GetCaseAssigneeCriteria(user));
            criteria.Filters.Add(GetWorkStateCriteria(user, criteria.StartDate, criteria.EndDate));
            criteria.Filters.Add(GetLocationCriteria(user));
            return criteria;
        }

        /// <summary>
        /// Runs the query.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        protected virtual List<Case> RunQuery(User user, ReportCriteria criteria)
        {
            List<IMongoQuery> ands = BuildFilter(user, criteria);

            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null);
            return query.ToList();
        }

        /// <summary>
        /// Builds the filter.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        protected virtual List<IMongoQuery> BuildFilter(User user, ReportCriteria criteria)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = CreateBaseAccommodationQuery(user, criteria);

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, startDate), Case.Query.LTE(e => e.StartDate, endDate)),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, startDate), Case.Query.LTE(e => e.EndDate, endDate))
            ));

            AddFilterCriteria(user, criteria, ands);

            return ands;
        }

        protected List<IMongoQuery> CreateBaseAccommodationQuery(User user, ReportCriteria criteria)
        {
            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "Employee._id"));
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            ands.Add(Case.Query.EQ(e => e.IsAccommodation, true));

            return ands;
        }

        protected void AddFilterCriteria(User user, ReportCriteria criteria, List<IMongoQuery> ands)
        {
            if (criteria.Filters == null || criteria.Filters.Count == 0)
                return;

            if (criteria.Filters.FirstOrDefault(m => m.Name == "CaseStatus") != null &&
                criteria.Filters.FirstOrDefault(m => m.Name == "CaseStatus").Value != null)
            {
                CaseStatus enumCaseStatusFilter = criteria.ValueOf<CaseStatus>("CaseStatus");
                ands.Add(Case.Query.EQ(c => c.Status, enumCaseStatusFilter));
            }

            List<IMongoQuery> elemMatchAnds = new List<IMongoQuery>();

            if (criteria.Filters.FirstOrDefault(m => m.Name == "Determination") != null &&
                criteria.Filters.FirstOrDefault(m => m.Name == "Determination").Value != null)
            {
                AdjudicationStatus enumadjStatusFilter = criteria.ValueOf<AdjudicationStatus>("Determination");
                elemMatchAnds.Add(Query<Accommodation>.EQ(a => a.Determination, enumadjStatusFilter));
            }

            if (criteria.Filters.FirstOrDefault(m => m.Name == "AccomType") != null && criteria.Filters.FirstOrDefault(m => m.Name == "AccomType").Value != null)
                elemMatchAnds.Add(Query<Accommodation>.EQ(a => a.Type.Code, criteria.Filters.FirstOrDefault(m => m.Name == "AccomType").Value));

            if (elemMatchAnds.Any())
                ands.Add(Case.Query.ElemMatch(c => c.AccommodationRequest.Accommodations, q => q.And(elemMatchAnds)));

            if (criteria.Filters.FirstOrDefault(m => m.Name == "WorkState") != null &&
                criteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value != null)
                ands.Add(Case.Query.EQ(e => e.Employee.WorkState, criteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value));

            var locationFilter = criteria.Filters.FirstOrDefault(m => m.Name == "Location");
            if (locationFilter != null && locationFilter.Values != null && locationFilter.Values.Any())
            {
                var val = locationFilter.ValuesAs<string>();
                if (val != null && val.Any())
                    ands.Add(Case.Query.In(e => e.CurrentOfficeLocation, val));
            }

            if (criteria.Filters.FirstOrDefault(m => m.Name == "CaseAssignee") != null &&
               criteria.Filters.FirstOrDefault(m => m.Name == "CaseAssignee").Value != null &&
               !string.IsNullOrWhiteSpace(criteria.Filters.FirstOrDefault(m => m.Name == "CaseAssignee").ValueAs<string>()))
                ands.Add(Case.Query.EQ(e => e.AssignedToId, criteria.Filters.FirstOrDefault(m => m.Name == "CaseAssignee").Value));
        }

        /// <summary>
        /// Gets the detail.
        /// </summary>
        /// <param name="case">The case.</param>
        /// <param name="q">The q.</param>
        /// <param name="detail">The detail.</param>
        /// <param name="displayCaseNumber">The display case number.</param>
        /// <param name="displayFullName">Display name of the full.</param>
        /// <returns></returns>
        protected virtual ResultData GetDetail(Case @case, Accommodation q, AccommodationUsage detail, ref string displayCaseNumber, ref string displayFullName)
        {
            dynamic data = new ResultData();
            data.CaseNumber = string.IsNullOrWhiteSpace(displayCaseNumber) ? null : new LinkItem() { Value = @case.CaseNumber, Link = string.Format("/Cases/{0}/View", @case.Id) };
            data.EmployeeNumber = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = @case.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", @case.Employee.Id) };
            data.Name = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = @case.Employee.FullName, Link = string.Format("/Employees/{0}/View", @case.Employee.Id) };
            CustomField employeeClass = @case.Employee.CustomFields.FirstOrDefault(cf => cf.Name == "EmployeeClass");
            data.Class = employeeClass == null ? "" : employeeClass.SelectedValueText;
            data.CaseStatus = @case.Status.ToString().SplitCamelCaseString();
            data.Duration = q.Duration == 0 ? "Pending" : q.Duration.ToString().SplitCamelCaseString();
            data.Type = q.Type.Name;
            data.Created = q.CreatedDate.ToString("MM/dd/yyyy");
            data.StartDate = detail.StartDate == DateTime.MinValue ? "Pending" : detail.StartDate.ToString("MM/dd/yyyy");
            data.EndDate = detail.EndDate.HasValue ? detail.EndDate.ToString("MM/dd/yyyy") : string.Empty;
            data.Accommodations = string.Concat(q.Type.Code, " - ", detail.Determination.ToString().SplitCamelCaseString());
            data.AccommStatus = q.Status.ToString().SplitCamelCaseString();
            data.Resolved = q.Resolved ? "Yes" : "No";
            data.ResolvedDate = q.ResolvedDate.HasValue ? q.ResolvedDate.ToString("MM/dd/yyyy") : string.Empty;
            data.Granted = q.Granted ? "Yes" : "No";
            data.GrantedDate = q.GrantedDate.HasValue ? q.GrantedDate.ToString("MM/dd/yyyy") : string.Empty;
            data.AssignedTo = @case.AssignedToName;
            data.WorkState = @case.Employee.WorkState;
            data.Location = string.IsNullOrWhiteSpace(@case.CurrentOfficeLocation) ? string.Empty : @case.CurrentOfficeLocation;
            data.WorkRelated = q.IsWorkRelated == null ? "" : q.IsWorkRelated.Value ? "1" : "0";
            data.CostCenter = @case.Employee.CostCenterCode ?? "";
            data.ClosedDate = @case.GetClosedDate().HasValue ? string.Format("{0:MM/dd/yyyy}", @case.GetClosedDate()) : string.Empty;

            displayCaseNumber = string.Empty;
            displayFullName = string.Empty;

            return data;
        }

        /// <summary>
        /// Gets the blank detail.
        /// </summary>
        /// <param name="case">The case.</param>
        /// <param name="displayCaseNumber">The display case number.</param>
        /// <param name="displayFullName">Display name of the full.</param>
        /// <returns></returns>
        protected virtual ResultData GetBlankDetail(Case @case, ref string displayCaseNumber, ref string displayFullName)
        {
            dynamic data = new ResultData();
            data.CaseNumber = string.IsNullOrWhiteSpace(displayCaseNumber) ? null : new LinkItem() { Value = @case.CaseNumber, Link = string.Format("/Cases/{0}/View", @case.Id) };
            data.EmployeeNumber = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = @case.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", @case.Employee.Id) };
            data.Name = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = @case.Employee.FullName, Link = string.Format("/Employees/{0}/View", @case.Employee.Id) };
            data.Duration = @case.AccommodationRequest.SummaryDurationType;
            data.Type = "N/A";
            data.CaseStatus = @case.Status.ToString().SplitCamelCaseString();
            var evt = @case.FindCaseEvent(Data.Enums.CaseEventType.CaseCreated);
            data.Created = (evt == null ? @case.CreatedDate : evt.EventDate).ToString("MM/dd/yyyy");
            data.StartDate = @case.StartDate.ToString("MM/dd/yyyy");
            data.EndDate = @case.EndDate.ToString("MM/dd/yyyy");
            data.Accommodations = "N/A";
            data.AccommStatus = "N/A";
            data.Resolved = "N/A";
            data.ResolvedDate = "N/A";
            data.Granted = "N/A";
            data.GrantedDate = "N/A";
            data.AssignedTo = @case.AssignedToName;
            data.WorkState = @case.Employee.WorkState;
            data.Location = @case.CurrentOfficeLocation;
            if (@case.AccommodationRequest != null && @case.AccommodationRequest.Accommodations != null)
                data.WorkRelated = @case.AccommodationRequest.Accommodations.Any(r => r.IsWorkRelated == true) ? "1" : @case.AccommodationRequest.Accommodations.Any(r => r.IsWorkRelated == false) ? "0" : "";
            else
                data.WorkRelated = "";
            data.CostCenter = @case.Employee.CostCenterCode ?? "";

            displayCaseNumber = string.Empty;
            displayFullName = string.Empty;

            return data;
        }
    }
}
