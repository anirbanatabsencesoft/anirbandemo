﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Security;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Reporting.Reports.Custom._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269.AccommodationStatus
{
    [Serializable]
    public class AccommodationStatusByDurationReport : AccommodationStatusReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "03ABCDE4-6A77-42DC-AEC4-0C8337B65504"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "by Duration"; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 2; } }

        /// <summary>
        /// Determines whether the specified user is visible.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public override bool IsVisible(User user)
        {
            return false;
        }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override void RunReport(User user, ReportResult result)
        {
            var queryList = RunQuery(user, result.Criteria);

            var groupQuery = queryList.Where(c => c.AccommodationRequest != null).SelectMany(c => c.AccommodationRequest.Accommodations)
                .GroupBy(n => n.Duration == 0 ? "Pending" : n.Duration.ToString().SplitCamelCaseString())
                .Select(g => new
                {
                    label = g.Key,
                    data = g.Select(x => x).ToList()
                }).OrderBy(m => m.label);

            List<ResultData> resultItems = new List<ResultData>();

            result.Groups = new List<ResultDataGroup>();
            int i = 1;
            foreach (var t in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (var q in t.data)
                {
                    var @case = queryList.Where(c => c.AccommodationRequest != null &&
                        c.AccommodationRequest.Accommodations != null &&
                        c.AccommodationRequest.Accommodations.Any(a => a.Id == q.Id)).FirstOrDefault();

                    string displayCaseNumber = @case.CaseNumber;
                    string displayFullName = @case.Employee.FullName;

                    foreach (var detail in q.Usage.Where(u => u.StartDate.DateRangesOverLap(u.EndDate, result.Criteria.StartDate, result.Criteria.EndDate)).OrderBy(u => u.StartDate))
                        resultItems.Add(GetDetail(@case, q, detail, ref displayCaseNumber, ref displayFullName));
                }

                result.Groups.Add(new ResultDataGroup()
                {
                    Key = i,
                    Label = t.label,
                    Items = new List<ResultData>().AddRangeFluid(resultItems)
                });

                i++;
            }
        }
    }
}
