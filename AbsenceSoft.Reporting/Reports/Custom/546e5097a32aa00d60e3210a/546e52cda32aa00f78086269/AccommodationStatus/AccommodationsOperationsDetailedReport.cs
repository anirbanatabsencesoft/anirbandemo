﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;

namespace AbsenceSoft.Reporting.Reports.Custom._546e5097a32aa00d60e3210a._546e52cda32aa00f78086269.AccommodationStatus
{
    public class AccommodationsOperationsDetailedReport : AccommodationStatusReport
    {
        private Dictionary<string, List<EmployeeJob>> _employeeJobs = null;
        private Dictionary<string, Employee> _employees = null;
        private Dictionary<string, List<EmployeeContact>> _employeeContacts = null;
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id
        {
            get { return "28b4c98f-c321-4a1f-b928-e19886a06a3c"; }
        }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name
        {
            get { return "Accommodations Operations Detailed Report"; }
        }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped
        {
            get { return false; }
        }

        /// <summary>
        /// Data heavy report, exportable only.
        /// </summary>
        public override bool IsViewable
        {
            get { return false; }
        }

        protected override List<IMongoQuery> BuildFilter(User user, ReportCriteria criteria)
        {
            List<IMongoQuery> ands = CreateBaseAccommodationQuery(user, criteria);
            ands.Add(Case.Query.GTE(c => c.CreatedDate, criteria.StartDate.ToMidnight()));
            ands.Add(Case.Query.LTE(c => c.CreatedDate, criteria.EndDate.EndOfDay()));
            AddFilterCriteria(user, criteria, ands);
            return ands;
        }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override void RunReport(User user, ReportResult result)
        {
            result.Items = new List<ResultData>();
            var toReturn = new ReportResult();            
            var queryList = RunQuery(user, result.Criteria).Where(c => c.AccommodationRequest != null);
            var customers = queryList.Select(c => c.CustomerId).ToList();
            var employers = queryList.Select(c => c.EmployerId).ToList();
            var employees = queryList.Select(c => c.Employee.EmployeeNumber).ToList();
            var employeeIds = queryList.Select(c => c.Employee.Id).ToList();
            _employeeJobs = EmployeeJob.AsQueryable().Where(job => employees.Contains(job.EmployeeNumber))
                .ToList().GroupBy(ej => ej.EmployeeNumber).ToDictionary(e => e.Key, e => e.ToList());
            _employees = Employee.AsQueryable().Where(e => employeeIds.Contains(e.Id))
                .ToList().ToDictionary(e => e.Id, e => e);
            _employeeContacts = EmployeeContact.AsQueryable().Where(ec => employeeIds.Contains(ec.EmployeeId))
                .ToList().GroupBy(e => e.EmployeeId).ToDictionary(e => e.Key, e => e.Where(ec => ec.ContactTypeCode == "SUPERVISOR").ToList());

            string dummy = "WHYOVERRIDE";

            foreach (var @case in queryList)
            {
                @case.AccommodationRequest.Accommodations = @case.AccommodationRequest.Accommodations ?? new List<Accommodation>(0);
                foreach (var accom in @case.AccommodationRequest.Accommodations.Where(a => a.Type != null).OrderBy(p => p.Type.Code))
                {
                    foreach (var detail in accom.Usage.OrderBy(u => u.StartDate))
                    {
                        if (@case.StartDate != DateTime.MinValue)
                        {
                            result.Items.Add(GetDetail(@case, accom, detail, ref dummy, ref dummy));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the detail.
        /// </summary>
        /// <param name="case">The case.</param>
        /// <param name="q">The q.</param>
        /// <param name="detail">The detail.</param>
        /// <param name="displayCaseNumber">The display case number.</param>
        /// <param name="displayFullName">Display name of the full.</param>
        /// <returns></returns>
        /// 
        protected override ResultData GetDetail(Case @case, Accommodation q, AccommodationUsage detail, ref string displayCaseNumber, ref string displayFullName)
        {
            dynamic toReturn = new ResultData();

            string employeeId = @case.Employee.Id;
            string employeeNumber = @case.Employee.EmployeeNumber;
            Employee employee = _employees[employeeId];
            EmployeeJob employeeCurrentJob = null;
            EmployeeJob employeeAccommodatedJob = null;
            if (_employeeJobs.ContainsKey(employeeNumber))
            {
                List<EmployeeJob> employeeJobs = _employeeJobs[employeeNumber];
                if (employeeJobs != null && employeeJobs.Any())
                {
                    employeeCurrentJob = GetCurrentJob(@case.Employee, employeeJobs, @case, q);
                    employeeAccommodatedJob = GetAccommodatedJob(@case.Employee, employeeJobs, q, detail);
                }
            }
           
            EmployeeContact supervisor = null;
            if (_employeeContacts.ContainsKey(employeeId) && _employeeContacts[employeeId] != null)
                supervisor = _employeeContacts[employeeId].OrderByDescending(ec => ec.CreatedDate).FirstOrDefault();

            toReturn.CaseNumber = FormatCaseNumber(@case);
            toReturn.EmployeeNumber = FormatEmployeeName(employee);
            toReturn.FirstName = @case.Employee.FirstName;
            toReturn.LastName = @case.Employee.LastName;
            toReturn.EmployeeClass = GetCustomFieldValue(@case.Employee, "EMPLOYEECLASS");
            toReturn.CaseStatus = @case.Status.ToString().SplitCamelCaseString();
            toReturn.CaseCreatedDate = @case.CreatedDate.ToUIString();
            toReturn.CaseStartDate = @case.StartDate.ToUIString();
            toReturn.CaseEndDate = @case.EndDate.ToUIString();
            toReturn.Duration = q.Duration == 0 ? "Pending" : q.Duration.ToString().SplitCamelCaseString();
            toReturn.AccommodationType = q.Type.Name;
            toReturn.AccommodationCreatedDate = q.CreatedDate.ToUIString();
            toReturn.AccommodationStartDate = detail.StartDate == DateTime.MinValue ? "Pending" : detail.StartDate.ToUIString();
            toReturn.AccommodationEndDate = detail.EndDate.ToUIString();
            toReturn.AccommodationTypeCode = q.Type.Code;
            toReturn.AccommodationDetermination = detail.Determination.ToString().SplitCamelCaseString();
            toReturn.AccommodationStatus = q.Status.ToString().SplitCamelCaseString();
            toReturn.Resolved = q.Resolved ? "Yes" : "No";
            toReturn.ResolvedDate = q.ResolvedDate.ToUIString();
            toReturn.Granted = q.Granted ? "Yes" : "No";
            toReturn.GrantedDate = q.GrantedDate.ToUIString();
            toReturn.AssignedTo = @case.AssignedToName;
            toReturn.WorkState = @case.Employee.WorkState;
            toReturn.OfficeLocation = @case.CurrentOfficeLocation ?? "";
            toReturn.WorkRelated = q.IsWorkRelated == null ? "" : q.IsWorkRelated.Value ? "1" : "0";
            toReturn.CostCenter = @case.Employee.CostCenterCode ?? "";
            toReturn.ClosedDate = @case.GetClosedDate().ToUIString();
            toReturn.DenialReasonCode = detail.DenialReasonCode;
            toReturn.DenialReasonName = detail.DenialReasonName;
            toReturn.Supervisor = supervisor != null && supervisor.Contact != null ? string.Format("{0}, {1}", supervisor.Contact.LastName, supervisor.Contact.FirstName) : "";
            toReturn.FCLMArea = GetCustomFieldValue(@case.Employee, "FCLMAREA");
            toReturn.ShiftPatternDescription = GetCustomFieldValue(@case.Employee, "SHIFTPATTERNDESCRIPTION");
            toReturn.Department = GetCustomFieldValue(@case.Employee, "DEPARTMENT");
            toReturn.BusinessTitle = GetCustomFieldValue(@case.Employee, "BUSINESSTITLE");
            toReturn.CurrentFCLMArea = GetCustomFieldValue(employee, "FCLMAREA");
            toReturn.CurrentShiftPatternDesc = GetCustomFieldValue(employee, "SHIFTPATTERNDESCRIPTION");
            toReturn.CurrentDepartment = GetCustomFieldValue(employee, "DEPARTMENT");
            toReturn.CurrentBusinessTitle = GetCustomFieldValue(employee, "BUSINESSTITLE");
            return toReturn;
        }

        private LinkItem FormatCaseNumber(Case @case)
        {
            if (string.IsNullOrEmpty(@case.CaseNumber))
                return null;

            return new LinkItem() { Value = @case.CaseNumber, Link = string.Format("/Cases/{0}/View", @case.Id) };
        }

        private LinkItem FormatEmployeeName(Employee emp)
        {
            if (string.IsNullOrEmpty(emp.EmployeeNumber))
                return null;

            return new LinkItem() { Value = emp.EmployeeNumber, Link = string.Format("/Employees/{0}/View", emp.Id) };
        }

        private EmployeeJob GetCurrentJob(Employee emp, List<EmployeeJob> employeeJobs, Case @case, Accommodation accom)
        {
            if (employeeJobs == null || !employeeJobs.Any())
                return null;

            var currentJobs = employeeJobs.Where(j => j.CreatedDate <= accom.CreatedDate || (j.Dates.EndDate.HasValue && j.Dates.EndDate < @case.StartDate));
            if (!currentJobs.Any())
                return null;
            
            DateTime testDate = @case.StartDate.AddDays(-1);
            DateTime minDate = currentJobs.Min(j => j.Dates.StartDate);

            // Walk backwards by day from the day before the TWA/LTA accommodation start date to find the job in range as the current job.
            while (testDate >= minDate && testDate >= DateTime.MinValue.AddDays(1))
            {
                var job = @case.Employee.GetPeriodJob(testDate, currentJobs);
                if (job != null)
                    return job;
                testDate = testDate.AddDays(-1);
            }
            return null;
        }
        
        private EmployeeJob GetAccommodatedJob(Employee emp, List<EmployeeJob> employeeJobs, Accommodation accom, AccommodationUsage detail)
        {
            if (accom == null || accom.StartDate == null)
                return null;

            if (employeeJobs == null || !employeeJobs.Any())
                return null;

            // Only TWA or LTA accommodation will have accommodated job. 
            if (accom.Type == null || !(new[] { "LTA", "TWA" }.Contains(accom.Type.Code)))
                return null;

            // Only go based off of approved time, get the approved start date
            if (detail.Determination != AdjudicationStatus.Approved)
                return null;

            // The accommodated job in the report should be pulling in jobs that are for the TWA approved date range. 
            // In this case, the TWA was approved from 5/9 to 5/30 and the accommodated job was also approved for the same date
            // range, although the requested dates are 5/8 to 5/30. The report should show the accommodated job for the approved 
            // TWA date range.
            var date = detail.StartDate;
            IEnumerable<EmployeeJob> currentJobs = employeeJobs
                .Where(j => j.CreatedDate > accom.CreatedDate)
                .Where(e => e.Dates != null && !e.Dates.IsNull && (e.Dates.IncludesDate(date) ||
                    // Need to also account for a job how dates are encompassed by the date itself
                    (e.Dates.StartDate >= date && (!e.Dates.EndDate.HasValue || date < e.Dates.EndDate.Value))));

            // If there are no matching jobs then bail
            if (!currentJobs.Any())
                return null;

            // If there's only 1 matching job, then return that one
            if (currentJobs.Count() == 1)
                return currentJobs.First();

            // Get our best match where we have no perpetual job (i.e. is has an end date) and
            //  the ranges overlap at least to some degree for best match
            var bestMatch = currentJobs
                .Where(j => j.Dates.EndDate.HasValue)
                .Where(j => j.Dates.DateRangesOverLap(detail.StartDate, detail.EndDate));

            // If there are none of those best matches, then just get the oldest one that matches the original
            //  criteria and return that one, sigh.
            if (!bestMatch.Any())
                return currentJobs.OrderBy(ej => ej.Dates.StartDate).First();

            // If there is only 1 best match, even better, return it
            if (bestMatch.Count() == 1)
                return bestMatch.First();

            // Now we just need to get the job that starts the earliest that overlaps 'cause that's the 
            //  one we probably care about
            return bestMatch.OrderBy(ej => ej.Dates.StartDate).First();
        }

        private string GetCustomFieldValue(Employee emp, string customFieldCode)
        {
            string selectedValue = emp.CustomFields
                .Where(cf => cf.Code == customFieldCode)
                .Select(cf => cf.SelectedValueText).FirstOrDefault();

            if (string.IsNullOrWhiteSpace(selectedValue))
                return string.Empty;

            return selectedValue;
        }

        private string FormatJobValue(EmployeeJob job)
        {
            if (job == null)
                return string.Empty;

            return string.Format("{0} ({1})", job.JobName, job.JobCode);
        }

    }
}
