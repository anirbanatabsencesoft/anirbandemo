﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;
using AbsenceSoft.Data.Cases;
using MongoDB.Driver.Builders;
using MongoDB.Bson;

namespace AbsenceSoft.Reporting.Reports.Custom._546e5097a32aa00d60e3210a
{
    /// <summary>
    /// Base report for all Amazon custom reports.
    /// </summary>
    public abstract class AmazonReport : BaseReport
    {
        /// <summary>
        /// The customer identifier for Amazon in ALL environments
        /// </summary>
        internal const string CustomerId = "546e5097a32aa00d60e3210a";

        /// <summary>
        /// Determines whether the specified report is visible to the given user for any valid reason.
        /// </summary>
        /// <param name="user">The user to check against.</param>
        /// <returns>
        ///   <c>true</c> if the specified report is visible; otherwise, <c>false</c>.
        /// </returns>
        public override bool IsVisible(User user)
        {
            return user != null && user.CustomerId == CustomerId;
        }

        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns>ADA Feature</returns>
        public override IEnumerable<Feature> RequiredFeatures()
        {
            yield return Feature.ADA;
        }

        protected override void ApplyCaseFilters(ICollection<IMongoQuery> query, ReportCriteria criteria)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();
            query.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
            ));

            if (criteria.Filters.Any())
            {
                ApplyEmployerFilter<Case>(query, criteria);
                var caseStatus = criteria.Filters.FirstOrDefault(m => m.Name == "CaseStatus");
                if (caseStatus != null && caseStatus.Value != null)
                {
                    CaseStatus enumCaseStatusFilter = criteria.ValueOf<CaseStatus>("CaseStatus");
                    query.Add(Case.Query.EQ(c => c.Status, enumCaseStatusFilter));
                }

                List<IMongoQuery> elemMatchAnds = new List<IMongoQuery>();

                var determination = criteria.Filters.FirstOrDefault(m => m.Name == "Determination");
                if (determination != null && determination.Value != null)
                {
                    AdjudicationStatus enumadjStatusFilter = criteria.ValueOf<AdjudicationStatus>("Determination");
                    elemMatchAnds.Add(Query<Accommodation>.EQ(a => a.Determination, enumadjStatusFilter));
                }

                var accomType = criteria.Filters.FirstOrDefault(m => m.Name == "AccomType");
                if (accomType != null && accomType.Value != null)
                    elemMatchAnds.Add(Query<Accommodation>.EQ(a => a.Type.Code, criteria.Filters.FirstOrDefault(m => m.Name == "AccomType").Value));

                if (elemMatchAnds.Any())
                    query.Add(Case.Query.ElemMatch(c => c.AccommodationRequest.Accommodations, q => q.And(elemMatchAnds)));

                var workState = criteria.Filters.FirstOrDefault(m => m.Name == "WorkState");
                if (workState != null && workState.Value != null)
                    query.Add(Case.Query.EQ(e => e.Employee.WorkState, criteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value));

                // Notice we're going to use the new fancy office location property on Case directly instead of Employee.Info.OfficeLocation :-)
                if (criteria.Filters.FirstOrDefault(m => m.Name == "Location") != null &&
                    criteria.Filters.FirstOrDefault(m => m.Name == "Location").Value != null &&
                    !string.IsNullOrWhiteSpace(criteria.Filters.FirstOrDefault(m => m.Name == "Location").ValueAs<string>()))
                    query.Add(Case.Query.EQ(e => e.CurrentOfficeLocation, criteria.Filters.FirstOrDefault(m => m.Name == "Location").Value));

                var assignee = criteria.Filters.FirstOrDefault(m => m.Name == "CaseAssignee");
                if (assignee != null && assignee.Value != null && !string.IsNullOrWhiteSpace(assignee.ValueAs<string>()))
                    query.Add(Case.Query.EQ(e => e.AssignedToId, assignee.Value));
            }
        }
    }
}
