﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Reporting.Reports.Custom._56b90041c5f32f05088a9e8d
{
    /// <summary>
    /// Base report for all Amazon custom reports.
    /// </summary>
    public abstract class OhioReport : BaseReport
    {
        /// <summary>
        /// The customer identifier for Amazon in ALL environments
        /// </summary>
        internal const string CustomerId = "56b90041c5f32f05088a9e8d";

        /// <summary>
        /// Determines whether the specified report is visible to the given user for any valid reason.
        /// </summary>
        /// <param name="user">The user to check against.</param>
        /// <returns>
        ///   <c>true</c> if the specified report is visible; otherwise, <c>false</c>.
        /// </returns>
        public override bool IsVisible(User user)
        {
            return user != null && user.CustomerId == CustomerId;
        }

        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns>ADA Feature</returns>
        public override IEnumerable<Feature> RequiredFeatures()
        {
            yield return Feature.WorkRelatedReporting;
        }
    }
}
