﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Reporting.Reports.Custom._56b90041c5f32f05088a9e8d._56b90101c5f32f05088a9e97.WorkRelated
{
    [Serializable]
    public class NeedleStick : WorkRelatedReport
    {
        private List<Employee> _employees = null;
        public override string Id
        {
            get { return "0d74f80f-9597-465a-bf99-66d8039ff393"; }
        }

        public override string Name
        {
            get { return "Needlestick"; }
        }

        public override bool IsGrouped
        {
            get { return false; }
        }

        public override ReportChart Chart
        {
            get { return null; }
        }

        public override ReportCriteria GetCriteria(User user)
        {
            var criteria = base.GetCriteria(user);
            criteria.Filters.AddRange(GetSharpsCriteria(user));
            criteria.Filters.AddRange(GetCustomFieldCriteria(user));
            return criteria;
        }

        protected virtual List<Case> RunQuery(User user, ReportCriteria criteria)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();
            List<IMongoQuery> ands = new List<IMongoQuery>();

            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "Employee._id"));
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            ApplyEmployerFilter<Case>(ands, criteria);

            ands.Add(Case.Query.And(
                    Case.Query.GTE(e => e.WorkRelated.IllnessOrInjuryDate, new BsonDateTime(startDate)),
                    Case.Query.LTE(e => e.WorkRelated.IllnessOrInjuryDate, new BsonDateTime(endDate))
            ));

            // This report requires the Sharps info
            ands.Add(Case.Query.EQ<bool?>(e => e.WorkRelated.Sharps, true));
            ApplySharpsFilters(criteria, ands);
            ApplyCustomFieldFilter(criteria, ands);
            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null);
            return query.ToList();

        }

        protected override void RunReport(User user, ReportResult result)
        {
            var queryResults = RunQuery(user, result.Criteria);                        
            List<ResultData> reportResultItems = new List<ResultData>();            
            foreach (var c in queryResults)
            {
                dynamic row = new ResultData();

                row.CaseNumber = c.CaseNumber;
                row.FirstName = c.Employee.FirstName;
                row.LastName = c.Employee.LastName;
                row.StartDate = c.StartDate.ToUIString();
                row.EndDate = c.EndDate.HasValue ? c.EndDate.ToUIString() : string.Empty;
                row.LeaveType = c.CaseType.ToString();
                row.Status = c.GetCaseStatus().ToString().SplitCamelCaseString();
                row.Reason = c.Reason == null ? string.Empty : c.Reason.Name;
                row.Reportable = c.WorkRelated != null ? c.WorkRelated.Reportable ? "Yes" : "No" : string.Empty;
                row.Classification = (c.WorkRelated != null && c.WorkRelated.Classification.HasValue) ? c.WorkRelated.Classification.ToString().SplitCamelCaseString() : string.Empty;
                row.WhereOccurred = c.WorkRelated != null ? c.WorkRelated.WhereOccurred ?? string.Empty : string.Empty;
                row.TypeOfInjury = (c.WorkRelated != null && c.WorkRelated.TypeOfInjury.HasValue) ? c.WorkRelated.TypeOfInjury.ToString().SplitCamelCaseString() : string.Empty;
                row.InjuryOrIllness = (c.WorkRelated != null ? c.WorkRelated.InjuryOrIllness ?? string.Empty : string.Empty);
                row.WhatHappened = (c.WorkRelated != null ? c.WorkRelated.WhatHappened ?? string.Empty : string.Empty);
                row.DaysAwayFromWork = (c.WorkRelated != null ? c.WorkRelated.DaysAwayFromWork.HasValue ? c.WorkRelated.DaysAwayFromWork.Value.ToString() : string.Empty : string.Empty);
                row.DaysOnJobTransferOrRestriction = (c.WorkRelated != null ? c.WorkRelated.DaysOnJobTransferOrRestriction.HasValue ? c.WorkRelated.DaysOnJobTransferOrRestriction.Value.ToString() : string.Empty : string.Empty);
                row.ActivityBeforeIncident = (c.WorkRelated != null ? c.WorkRelated.ActivityBeforeIncident ?? string.Empty : string.Empty);
                row.WhatHarmedTheEmployee = (c.WorkRelated != null ? c.WorkRelated.WhatHarmedTheEmployee ?? string.Empty : string.Empty); 
                row.JobDetailOccupation = c.Employee.JobTitle ?? string.Empty;

                // Sharps Specifics
                row.JobClassification = c.WorkRelated != null && c.WorkRelated.SharpsInfo != null &&  c.WorkRelated.SharpsInfo.JobClassification.HasValue ? c.WorkRelated.SharpsInfo.JobClassification.ToString().SplitCamelCaseString() : string.Empty;
                row.LocationAndDepartment = c.WorkRelated != null && c.WorkRelated.SharpsInfo != null && c.WorkRelated.SharpsInfo.LocationAndDepartment.HasValue ? c.WorkRelated.SharpsInfo.LocationAndDepartment.ToString().SplitCamelCaseString() : string.Empty;
                row.SharpType = c.WorkRelated != null && c.WorkRelated.SharpsInfo != null ? c.WorkRelated.SharpsInfo.TypeOfSharp ?? string.Empty : string.Empty;
                row.SharpBrand = c.WorkRelated != null && c.WorkRelated.SharpsInfo != null ? c.WorkRelated.SharpsInfo.BrandOfSharp ?? string.Empty : string.Empty;
                row.Procedure = c.WorkRelated != null && c.WorkRelated.SharpsInfo != null && c.WorkRelated.SharpsInfo.Procedure.HasValue ? c.WorkRelated.SharpsInfo.Procedure.ToString().SplitCamelCaseString() : string.Empty;
                row.InjuryLocation = string.Join(", ", c.WorkRelated.SharpsInfo.InjuryLocation.Select(i => i.ToString().SplitCamelCaseString()));
                row.ExposureDetail = c.WorkRelated != null && c.WorkRelated.SharpsInfo != null ? c.WorkRelated.SharpsInfo.ExposureDetail ?? string.Empty : string.Empty;

                //Custom Fields
                row.BusinessUnit = GetCustomFieldValue(c, "BUSINESSUNIT");
                row.DepartmentID = GetCustomFieldValue(c, "DEPARTMENTID");
                reportResultItems.Add(row);
            }

            result.Items = reportResultItems;
        }

        private string GetCustomFieldValue(Case @case, string customFieldCode)
        {
            string selectedValue = @case.Employee.CustomFields
                .Where(cf => cf.Code == customFieldCode)
                .Select(cf => cf.SelectedValueText).FirstOrDefault();

            if (string.IsNullOrWhiteSpace(selectedValue))
                return string.Empty;

            return selectedValue;
        }
    }
}