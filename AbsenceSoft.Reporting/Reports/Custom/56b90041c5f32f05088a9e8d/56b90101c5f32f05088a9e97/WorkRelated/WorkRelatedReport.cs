﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoRepository;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace AbsenceSoft.Reporting.Reports.Custom._56b90041c5f32f05088a9e8d._56b90101c5f32f05088a9e97.WorkRelated
{
    [Serializable]
    public abstract class WorkRelatedReport : OhioReport
    {
        /// <summary>
        /// Category for all Work Related reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all Work Related reports is "Work Related Report".
        /// </summary>
        public override string Category { get { return "Work Related"; } }

        /// <summary>
        /// Icon to set for Work Related Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart { get { return null; } }


        #region Methods to help build out the Criteria.

        protected List<ReportCriteriaItemOption> GetEnumOptions<TEnum>() where TEnum : struct
        {
            if (typeof(TEnum).IsEnum)
            {
                return Enum.GetValues(typeof(TEnum)).OfType<TEnum>().Select(e => new ReportCriteriaItemOption()
                {
                    Text = e.ToString().SplitCamelCaseString(),
                    Value = Convert.ToInt32(e)
                }).ToList();
            }

            return null;
        }
        protected List<ReportCriteriaItemOption> GetCustomFieldOptions(List<CustomField> customFields)
        {

            return customFields.OrderBy(cf => cf.SelectedValueText).Select(cf => new { Text = cf.SelectedValueText, Value = cf.SelectedValueText }).Distinct().Select(cf => new ReportCriteriaItemOption()
            {
                Text = cf.Text.SplitCamelCaseString(),
                Value = cf.Value
            }).ToList();
        }

        protected ReportCriteriaItem GetYesNoOCriteria(string name, string prompt, bool required)
        {
            ReportCriteriaItem toReturn = new ReportCriteriaItem();
            toReturn.Name = name;
            toReturn.Prompt = prompt;
            toReturn.Required = required;
            toReturn.ControlType = ControlType.RadioButtonList;
            toReturn.Options = (new[] { "Yes", "No" }).Select(e => new ReportCriteriaItemOption(e)).ToList();
            return toReturn;
        }

        protected ReportCriteriaItem GetReportCriteriaItem<T>(string name, string prompt, bool required, ControlType controlType, List<ReportCriteriaItemOption> options = null)
        {
            ReportCriteriaItem toReturn = new ReportCriteriaItem();
            toReturn.Name = name;
            toReturn.Prompt = prompt;
            toReturn.Required = required;
            toReturn.ControlType = controlType;
            toReturn.Options = options;

            return toReturn;
        }

        protected virtual IEnumerable<ReportCriteriaItem> GetSharpsCriteria(User user)
        {
            yield return GetReportCriteriaItem<WorkRelatedSharpsInjuryLocation>("WorkRelatedSharpsInjuryLocation", "Inury Location", false, ControlType.CheckBoxList, GetEnumOptions<WorkRelatedSharpsInjuryLocation>());
            yield return GetReportCriteriaItem<WorkRelatedSharpsInjuryLocation>("WorkRelatedSharpsLocation", "Location/Department", false, ControlType.CheckBoxList, GetEnumOptions<WorkRelatedSharpsLocation>());
            yield return GetReportCriteriaItem<WorkRelatedSharpsInjuryLocation>("WorkRelatedSharpsProcedure", "Procedure", false, ControlType.CheckBoxList, GetEnumOptions<WorkRelatedSharpsProcedure>());
            yield return GetReportCriteriaItem<WorkRelatedSharpsJobClassification>("WorkRelatedSharpsJobClassification", "Job Classification", false, ControlType.CheckBoxList, GetEnumOptions<WorkRelatedSharpsJobClassification>());
            yield return GetLocationCriteria(user);
        }

        protected virtual IEnumerable<ReportCriteriaItem> GetWorkRelatedCriteria(User user)
        {
            yield return GetReportCriteriaItem<WorkRelatedCaseClassification>("WorkRelatedCaseClassification", "Case Classification", false, ControlType.CheckBoxList, GetEnumOptions<WorkRelatedCaseClassification>());
            yield return GetReportCriteriaItem<WorkRelatedCaseClassification>("WorkRelatedTypeOfInjury", "Type of Injury", false, ControlType.CheckBoxList, GetEnumOptions<WorkRelatedTypeOfInjury>());
            yield return GetLocationCriteria(user);
        }

        protected virtual IEnumerable<ReportCriteriaItem> GetCustomFieldCriteria(User user)
        {
            List<string> customFieldCodes = new List<string>() { "BUSINESSUNIT", "DEPARTMENTID" };

            List<IMongoQuery> query = new List<IMongoQuery>();
            foreach (string code in customFieldCodes)
            {
                query.Add(Query.ElemMatch("CustomFields", Query.EQ("Code", code)));
            }
            var result = Employee.Query.Find(Query.And(Query.EQ("CustomerId", new BsonObjectId(new ObjectId(user.CustomerId))), Query.Or(query))).Select(emp => emp.CustomFields).ToList();
            List<CustomField> customFields = new List<CustomField>();
            result.ForEach(r => customFields.AddRange(r.Where(cf => customFieldCodes.Contains(cf.Code))));
            yield return GetReportCriteriaItem<CustomField>("BusinessUnit", "Business Unit", false, ControlType.CheckBoxList, GetCustomFieldOptions(customFields.Where(cf => cf.Code.Trim() == "BUSINESSUNIT").ToList()));
            yield return GetReportCriteriaItem<CustomField>("DepartmentID", "Department ID", false, ControlType.CheckBoxList, GetCustomFieldOptions(customFields.Where(cf => cf.Code.Trim() == "DEPARTMENTID").ToList()));
        }

        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria();
            criteria.CustomerId = user.CustomerId;
            criteria.StartDate = DateTime.UtcNow.Date.AddDays(-30);
            criteria.EndDate = DateTime.UtcNow.Date;

            return criteria;
        }

        #endregion

        #region Retrieve the selected filters and apply to the Query

        protected void ApplySharpsFilters(ReportCriteria criteria, List<IMongoQuery> ands)
        {
            AddMultiFilter<IEnumerable<WorkRelatedSharpsInjuryLocation>>(c => c.WorkRelated.SharpsInfo.InjuryLocation, "WorkRelatedCaseClassification", criteria, ands);
            AddMultiFilter<WorkRelatedSharpsLocation?>(c => c.WorkRelated.SharpsInfo.LocationAndDepartment, "WorkRelatedSharpsLocation", criteria, ands);
            AddMultiFilter<WorkRelatedSharpsProcedure?>(c => c.WorkRelated.SharpsInfo.Procedure, "WorkRelatedSharpsProcedure", criteria, ands);
            AddMultiFilter<WorkRelatedSharpsJobClassification?>(c => c.WorkRelated.SharpsInfo.JobClassification, "WorkRelatedSharpsJobClassification", criteria, ands);
            AddMultiFilter<string>(e => e.Employee.Info.OfficeLocation, "Location", criteria, ands);
        }

        protected void ApplyWorkRelatedCriteria(ReportCriteria criteria, List<IMongoQuery> ands)
        {
            AddMultiFilter<WorkRelatedCaseClassification?>(e => e.WorkRelated.Classification, "WorkRelatedCaseClassification", criteria, ands);
            AddMultiFilter<WorkRelatedTypeOfInjury?>(e => e.WorkRelated.TypeOfInjury, "WorkRelatedTypeOfInjury", criteria, ands);
            AddMultiFilter<string>(e => e.Employee.Info.OfficeLocation, "Location", criteria, ands);
        }

        public void AddMultiFilter<TEnum>(Expression<Func<Case, TEnum>> memberExpression, string filterName, ReportCriteria criteria, List<IMongoQuery> ands)
        {
            var matchingFilter = criteria.Filters.FirstOrDefault(m => m.Name == filterName && m.Values.Any());
            if (matchingFilter != null)
            {
                var value = matchingFilter.ValuesAs<TEnum>();
                ands.Add(Case.Query.In<TEnum>(memberExpression, value));
            }
        }

        public void AddEQFilter<TEnum>(Expression<Func<Case, TEnum>> memberExpression, string filterName, ReportCriteria criteria, List<IMongoQuery> ands)
        {
            var matchingFilter = criteria.Filters.FirstOrDefault(m => m.Name == filterName && m.Value != null);
            if (matchingFilter != null)
            {
                var value = matchingFilter.ValueAs<TEnum>();
                ands.Add(Case.Query.EQ<TEnum>(memberExpression, value));
            }
        }

        public void AddYesNoBoolFilter(Expression<Func<Case, bool>> memberExpression, string filterName, ReportCriteria criteria, List<IMongoQuery> ands)
        {
            var matchingFilter = criteria.Filters.FirstOrDefault(m => m.Name == filterName && m.Value != null);
            if (matchingFilter != null)
            {
                var value = matchingFilter.Value.ToString() == "Yes";
                ands.Add(Case.Query.EQ(memberExpression, value));
            }
        }

        public void ApplyCustomFieldFilter(ReportCriteria criteria, ICollection<IMongoQuery> query)
        {
            var businessUnitFilter = criteria.Filters.FirstOrDefault(m => m.Name == "BusinessUnit");
            if (businessUnitFilter != null && businessUnitFilter.Values != null && businessUnitFilter.Values.Any())
            {
                List<BsonRegularExpression> exp = new List<BsonRegularExpression>();
                foreach (string val in businessUnitFilter.Values) exp.Add(ToCaseInsensitiveRegex(val));
                query.Add(Query.ElemMatch("Employee.CustomFields", Query.And(Query.EQ("Code", "BUSINESSUNIT"), Query.In("SelectedValue", exp))));
            }
            var departmentIDFilter = criteria.Filters.FirstOrDefault(m => m.Name == "DepartmentID");
            if (departmentIDFilter != null && departmentIDFilter.Values != null && departmentIDFilter.Values.Any())
            {
                List<BsonRegularExpression> exp = new List<BsonRegularExpression>();
                foreach (string val in departmentIDFilter.Values) exp.Add(ToCaseInsensitiveRegex(val));
                query.Add(Query.ElemMatch("Employee.CustomFields", Query.And(Query.EQ("Code", "DEPARTMENTID"), Query.In("SelectedValue", exp))));
            }
        }

        private BsonRegularExpression ToCaseInsensitiveRegex(string source)
        {
            return new BsonRegularExpression("^" + source, "i");
        }

        #endregion

    }
}
