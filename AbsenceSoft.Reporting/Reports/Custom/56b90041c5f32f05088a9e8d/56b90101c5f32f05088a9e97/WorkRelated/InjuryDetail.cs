using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Reporting.Reports.Custom._56b90041c5f32f05088a9e8d._56b90101c5f32f05088a9e97.WorkRelated
{
    [Serializable]
    public class InjuryDetail : WorkRelatedReport
    {
        public override string Id
        {
            get { return "0222e047-5dd2-4e7d-8cd8-12ebf1a98153"; }
        }

        public override string Name
        {
            get { return "Injury Detail"; }
        }

        public override bool IsGrouped
        {
            get { return true; }
        }

        public override ReportChart Chart
        {
            get { return null; }
        }

        public override ReportCriteria GetCriteria(User user)
        {
            var criteria = base.GetCriteria(user);
            criteria.Filters.AddRange(GetWorkRelatedCriteria(user));
            criteria.Filters.AddRange(GetCustomFieldCriteria(user));
            return criteria;
        }

        protected virtual List<Case> RunQuery(User user, ReportCriteria criteria)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();
            List<IMongoQuery> ands = new List<IMongoQuery>();

            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "Employee._id"));
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));

            ApplyEmployerFilter<Case>(ands, criteria);

            ands.Add(Case.Query.And(
                    Case.Query.GTE(e => e.WorkRelated.IllnessOrInjuryDate, new BsonDateTime(startDate)),
                    Case.Query.LTE(e => e.WorkRelated.IllnessOrInjuryDate, new BsonDateTime(endDate))
            ));

            ands.Add(Case.Query.Exists(c => c.WorkRelated));
            ApplyWorkRelatedCriteria(criteria, ands);
            ApplyCustomFieldFilter(criteria, ands);
            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null);
            return query.ToList();

        }

        protected override void RunReport(User user, ReportResult result)
        {
            var queryResults = RunQuery(user, result.Criteria);            
            List<ResultData> reportResultItems = new List<ResultData>();            
            foreach (var c in queryResults)
            {
                dynamic row = new ResultData();

                row.CaseNumber = c.CaseNumber;
                row.FirstName = c.Employee.FirstName;
                row.LastName = c.Employee.LastName;
                row.StartDate = c.StartDate.ToUIString();
                row.EndDate = c.EndDate.HasValue ? c.EndDate.ToUIString() : string.Empty;
                row.LeaveType = c.CaseType.ToString();
                row.Status = c.GetCaseStatus().ToString().SplitCamelCaseString();
                row.Reason = c.Reason == null ? string.Empty : c.Reason.Name;
                row.Reportable = c.WorkRelated != null ? c.WorkRelated.Reportable ? "Yes" : "No" : string.Empty;
                row.Classification = (c.WorkRelated != null && c.WorkRelated.Classification.HasValue) ? c.WorkRelated.Classification.ToString().SplitCamelCaseString() : string.Empty;
                row.WhereOccurred = c.WorkRelated != null ? c.WorkRelated.WhereOccurred ?? string.Empty : string.Empty;
                row.TypeOfInjury = (c.WorkRelated != null && c.WorkRelated.TypeOfInjury.HasValue) ? c.WorkRelated.TypeOfInjury.ToString().SplitCamelCaseString() : string.Empty;
                row.InjuryOrIllness = (c.WorkRelated != null ? c.WorkRelated.InjuryOrIllness ?? string.Empty : string.Empty);
                row.WhatHappened = (c.WorkRelated != null ? c.WorkRelated.WhatHappened ?? string.Empty : string.Empty);
                row.DaysAwayFromWork = (c.WorkRelated != null ? c.WorkRelated.DaysAwayFromWork.HasValue ? c.WorkRelated.DaysAwayFromWork.Value.ToString() : "0" : string.Empty);
                row.DaysOnJobTransferOrRestriction = (c.WorkRelated != null ? c.WorkRelated.DaysOnJobTransferOrRestriction.HasValue ? c.WorkRelated.DaysOnJobTransferOrRestriction.Value.ToString() : string.Empty : string.Empty);
                row.ActivityBeforeIncident = (c.WorkRelated != null ? c.WorkRelated.ActivityBeforeIncident ?? string.Empty : string.Empty);
                row.WhatHarmedTheEmployee = (c.WorkRelated != null ? c.WorkRelated.WhatHarmedTheEmployee ?? string.Empty : string.Empty);
                row.JobDetailOccupation = c.Employee.JobTitle ?? string.Empty;
                row.TotalClaims = "";

                // Custom Fields. Questions that need answers before showing this: Does Custom Fields on a report mean the report is a custom-employer only report?
                row.BusinessUnit = GetCustomFieldValue(c, "BUSINESSUNIT");
                row.DepartmentID = GetCustomFieldValue(c, "DEPARTMENTID");

                reportResultItems.Add(row);

            }

            ResultDataGroup group = new ResultDataGroup() { Key = "Details", Items = reportResultItems, Label = "Details" };

            // Report Totals
            var totalClaims = queryResults.Count;
            var totalDaysAwayFromWork = queryResults.Sum(c => c.WorkRelated.DaysAwayFromWork.GetValueOrDefault());
            var totalDaysOnJobTransferOrRestriction = queryResults.Sum(c => c.WorkRelated.DaysOnJobTransferOrRestriction.GetValueOrDefault());

            List<ResultData> reportSummaryItems = new List<ResultData>();
            if (queryResults != null && queryResults.Count > 0)
            {
                dynamic rowTotals = new ResultData();

                rowTotals.CaseNumber = string.Empty;
                rowTotals.FirstName = string.Empty;
                rowTotals.LastName = string.Empty;
                rowTotals.StartDate = string.Empty;
                rowTotals.EndDate = string.Empty;
                rowTotals.LeaveType = string.Empty;
                rowTotals.Status = string.Empty;
                rowTotals.Reason = string.Empty;
                rowTotals.Reportable = string.Empty;
                rowTotals.Classification = string.Empty;
                rowTotals.WhereOccurred = string.Empty;
                rowTotals.TypeOfInjury = string.Empty;
                rowTotals.InjuryOrIllness = string.Empty;
                rowTotals.WhatHappened = string.Empty;
                rowTotals.DaysAwayFromWork = totalDaysAwayFromWork;
                rowTotals.DaysOnJobTransferOrRestriction = totalDaysOnJobTransferOrRestriction;
                rowTotals.ActivityBeforeIncident = string.Empty;
                rowTotals.WhatHarmedTheEmployee = string.Empty;
                rowTotals.JobDetailOccupation = string.Empty;
                rowTotals.TotalClaims = totalClaims;
                reportSummaryItems.Add(rowTotals);
            }
            ResultDataGroup groupSummary = new ResultDataGroup() { Key = "ReportTotals", Items = reportSummaryItems, Label = "Report Totals;" };

            result.Groups = new List<ResultDataGroup>();
            if (queryResults != null && queryResults.Count > 0)
            {
                result.Groups.Add(group);
                result.Groups.Add(groupSummary);
            }
        }

        private string GetCustomFieldValue(Case @case, string customFieldCode)
        { 
            string selectedValue = @case.Employee.CustomFields
                .Where(cf => cf.Code == customFieldCode)
                .Select(cf => cf.SelectedValueText).FirstOrDefault();

            if (string.IsNullOrWhiteSpace(selectedValue))
                return string.Empty;

            return selectedValue;
        }
    }
}