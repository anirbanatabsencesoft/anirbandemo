﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Reporting.Reports.AbsenceStatus
{
    [Serializable]
    public sealed class AbsenceStatusByNoGroupReport : AbsenceStatusReport
    {
        public override string Id { get { return "955E6AC2-4C2C-4cb5-A6B7-DF4CCD3BC252"; } }
        public override string Name { get { return "Detail"; } }
        public override bool IsGrouped { get { return false; } }

        public override ESSReportDetail ESSReportDetails { get { return new ESSReportDetail() { Title = "Detail Report", Type = ESSReportType.TeamReport, ESSReportName = "Absence Status Detail Report" }; } }

        protected override void RunReport(User user, ReportResult result)
        {
            var queryList = RunQuery(user, result.Criteria);

            List<ResultData> resultItems = new List<ResultData>();

            var customer = user.Customer;
            var isTpa = customer.HasFeature(Feature.MultiEmployerAccess) && user.Employers.Count > 1;

            foreach (var q in queryList)
            {
                string displayCaseNumber = q.CaseNumber;
                string displayEmployeeNumber = q.Employee.EmployeeNumber;
                string displayFullName = q.Employee.FullName;
                string displayReason = q.Reason.Name;
                string displayLeaveType = q.CaseType.ToString();
                DateTime? displayRTW = GetCaseEventDate(q, CaseEventType.ReturnToWork);
                DateTime? displayERTW = GetCaseEventDate(q, CaseEventType.EstimatedReturnToWork);

                List<PolicySummary> ps = null;
                using (var service = new CaseService())
                {
                    ps = service.GetEmployeePolicySummaryByCaseId(q.Id, null, q.Summary.Policies.Select(p => p.PolicyCode).ToArray());
                }

                foreach (var policy in q.Summary.Policies.OrderBy(p => p.PolicyCode))
                {
                    foreach (var detail in policy.Detail.OrderBy(d => d.StartDate))
                    {
                        dynamic data = new ResultData();

                        if (isTpa)
                        {
                            data.Employer = q.EmployerName;
                        }
                        data.CaseNumber = string.IsNullOrWhiteSpace(displayCaseNumber) ? null : new LinkItem() { Value = q.CaseNumber, Link = string.Format("/Cases/{0}/View", q.Id) };
                        data.EmployeeNumber = string.IsNullOrWhiteSpace(displayEmployeeNumber) ? null : new LinkItem() { Value = q.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", q.Employee.Id) };
                        data.Name = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = q.Employee.FullName, Link = string.Format("/Employees/{0}/View", q.Employee.Id) };
                        data.Reason = displayReason;
                        data.LeaveType = displayLeaveType;
                        data.StartDate = detail.StartDate.ToString("MM/dd/yyyy");
                        data.EndDate = detail.EndDate.ToString("MM/dd/yyyy");
                        data.Status = q.Status.ToString().SplitCamelCaseString();
                        data.Policies = string.Concat(policy.PolicyCode, " - ", detail.Determination.ToString().SplitCamelCaseString());
                        var pd = ps.FirstOrDefault(m => m.PolicyCode == policy.PolicyCode);
                        data.TimeUsed = pd == null ? "N/A" : pd.TimeUsed < 0 ? "reasonable amount" : String.Format("{0:N2} {1}", pd.TimeUsed, pd.Units.ToString().ToLowerInvariant());
                        data.TimeAvailable = pd == null ? "N/A" : pd.TimeRemaining < 0 ? "reasonable amount" : String.Format("{0:N2} {1}", pd.TimeRemaining, pd.Units.ToString().ToLowerInvariant());
                        data.ReturnToWork = displayRTW != null ? displayRTW.ToString("MM/dd/yyyy") : string.Empty;
                        data.EstimatedReturnToWork = displayERTW != null ? displayERTW.ToString("MM/dd/yyyy") : string.Empty;

                        resultItems.Add(data);

                        displayCaseNumber = string.Empty;
                        displayEmployeeNumber = string.Empty;
                        displayFullName = string.Empty;
                        displayReason = string.Empty;
                        displayLeaveType = string.Empty;
                    }
                }

                if (q.Summary.Policies.Count == 0)
                {
                    dynamic data = new ResultData();
                    if (isTpa)
                    {
                        data.Employer = q.EmployerName;
                    }
                    data.CaseNumber = string.IsNullOrWhiteSpace(displayCaseNumber) ? null : new LinkItem() { Value = q.CaseNumber, Link = string.Format("/Cases/{0}/View", q.Id) };
                    data.EmployeeNumber = string.IsNullOrWhiteSpace(displayEmployeeNumber) ? null : new LinkItem() { Value = q.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", q.Employee.Id) };
                    data.Name = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = q.Employee.FullName, Link = string.Format("/Employees/{0}/View", q.Employee.Id) };
                    data.Reason = displayReason;
                    data.LeaveType = displayLeaveType;
                    data.StartDate = q.StartDate.ToString("MM/dd/yyyy");
                    data.EndDate = q.EndDate.ToString("MM/dd/yyyy");
                    data.Status = q.Status.ToString().SplitCamelCaseString();
                    data.Policies = "N/A";
                    data.TimeUsed = "N/A";
                    data.TimeAvailable = "N/A";
                    data.ReturnToWork = displayRTW != null ? displayRTW.ToString("MM/dd/yyyy") : string.Empty;
                    data.EstimatedReturnToWork = displayERTW != null ? displayERTW.ToString("MM/dd/yyyy") : string.Empty;

                    resultItems.Add(data);
                }

            }

            result.Items = resultItems;
        }
    }
}
