﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Reporting.Reports.AbsenceStatus
{
    [Serializable]
    public sealed class AbsenceStatusByLeaveTypeReport : AbsenceStatusReport
    {
        public override string Id { get { return "117AF888-E65E-44da-A043-61A5896FC316"; } }
        public override string Name { get { return "by Leave Type"; } }
        public override bool IsGrouped { get { return true; } }

        protected override void RunReport(User user, ReportResult result)
        {
            var query = RunQuery(user, result.Criteria);

            var groupQuery = query.GroupBy(n => n.CaseType)
           .Select(g => new
           {
               label = g.Key,
               data = g.Select(x => x).ToList()
           }).OrderBy(m => m.label);

            List<ResultData> resultItems;

            result.Groups = new List<ResultDataGroup>();
            int i = 1;
            foreach (var t in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (var td in t.data)
                {
                    string displayCaseNumber = td.CaseNumber;
                    string displayEmployeeNumber = td.Employee.EmployeeNumber;
                    string displayFullName = td.Employee.FullName;
                    string displayReason = td.Reason.Name;
                    string displayLeaveType = td.CaseType.ToString();
                    DateTime? displayRTW = GetCaseEventDate(td, CaseEventType.ReturnToWork);
                    DateTime? displayERTW = GetCaseEventDate(td, CaseEventType.EstimatedReturnToWork);

                    foreach (var policy in td.Summary.Policies.OrderBy(p => p.PolicyCode))
                    {
                        foreach (var detail in policy.Detail.OrderBy(d => d.StartDate))
                        {
                            dynamic data = new ResultData();
                            data.CaseNumber = string.IsNullOrWhiteSpace(displayCaseNumber) ? null : new LinkItem() { Value = displayCaseNumber, Link = string.Format("/Cases/{0}/View", td.Id) };
                            data.EmployeeNumber = string.IsNullOrWhiteSpace(displayEmployeeNumber) ? null : new LinkItem() { Value = td.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", td.Employee.Id) };
                            data.Name = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = td.Employee.FullName, Link = string.Format("/Employees/{0}/View", td.Employee.Id) };
                            data.Reason = displayReason;
                            data.LeaveType = displayLeaveType;
                            data.StartDate = detail.StartDate.ToString("MM/dd/yyyy");
                            data.EndDate = detail.EndDate.ToString("MM/dd/yyyy");
                            data.Status = td.Status.ToString().SplitCamelCaseString();
                            data.Policies = string.Concat(policy.PolicyCode, " - ", detail.Determination.ToString().SplitCamelCaseString()) ;
                            data.ReturnToWork = displayRTW != null ? displayRTW.ToString("MM/dd/yyyy") : string.Empty;
                            data.EstimatedReturnToWork = displayERTW != null ? displayERTW.ToString("MM/dd/yyyy") : string.Empty;

                            resultItems.Add(data);

                            displayCaseNumber = string.Empty;
                            displayEmployeeNumber = string.Empty;
                            displayFullName = string.Empty;
                            displayReason = string.Empty;
                            displayLeaveType = string.Empty;
                        }
                    }

                    if (td.Summary.Policies.Count == 0)
                    {
                        dynamic data = new ResultData();
                        data.CaseNumber = string.IsNullOrWhiteSpace(displayCaseNumber) ? null : new LinkItem() { Value = td.CaseNumber, Link = string.Format("/Cases/{0}/View", td.Id) };
                        data.EmployeeNumber = string.IsNullOrWhiteSpace(displayEmployeeNumber) ? null : new LinkItem() { Value = td.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", td.Employee.Id) };
                        data.Name = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = td.Employee.FullName, Link = string.Format("/Employees/{0}/View", td.Employee.Id) };
                        data.Reason = displayReason;
                        data.LeaveType = displayLeaveType;
                        data.StartDate = td.StartDate.ToString("MM/dd/yyyy");
                        data.EndDate = td.EndDate.ToString("MM/dd/yyyy");
                        data.Status = td.Status.ToString().SplitCamelCaseString();
                        data.Policies = "N/A";
                        data.ReturnToWork = displayRTW != null ? displayRTW.ToString("MM/dd/yyyy") : string.Empty;
                        data.EstimatedReturnToWork = displayERTW != null ? displayERTW.ToString("MM/dd/yyyy") : string.Empty;

                        resultItems.Add(data);
                    }
                }

                result.Groups.Add(new ResultDataGroup()
                {
                    Key = i,
                    Label = t.label,
                    Items = new List<ResultData>().AddRangeFluid(resultItems ?? new List<ResultData>(0))
                });

                i++;
            }
        }

    }
}
