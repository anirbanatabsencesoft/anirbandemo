﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Reporting.Reports.AbsenceStatus
{
    [Serializable]
    public sealed class AbsenceStatusByOrganizationReport : AbsenceStatusReport
    {
        public override string Id { get { return "3060662A-B102-4017-BD53-285BA03D2238"; } }
        public override string Name { get { return "by Organization"; } }
        public override bool IsGrouped { get { return true; } }
        public override int CategoryOrder { get { return 1; } }
        public override bool IncludeOrganizationGrouping { get { return true; } }

        public override ESSReportDetail ESSReportDetails { get { return new ESSReportDetail() { Title = "Absence Status Report", Type = ESSReportType.OrganizationReport, ESSReportName = "Absence Status Report" }; } }

        protected override void RunReport(User user, ReportResult result)
        {
            var query = RunQuery(user, result.Criteria);
            string grouptypeCode = null;
            var customer = user.Customer;
            var isTpa = customer.HasFeature(Feature.MultiEmployerAccess) && user.Employers.Count > 1;
            if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "OrganizationTypeGrouping") != null)
                grouptypeCode = result.Criteria.ValueOf<string>("OrganizationTypeGrouping");
            List<string> customerIds = query.Select(cs => cs.CustomerId).Distinct().ToList();
            List<string> employerIds = query.Select(cs => cs.EmployerId).Distinct().ToList();
            List<string> employeeNumbers = query.Select(cs => cs.Employee.EmployeeNumber).Distinct().ToList();
            List<EmployeeOrganization> employeeOrganizations = null;
            if (grouptypeCode != null)
                employeeOrganizations = EmployeeOrganization.AsQueryable().Where(org => org.TypeCode == grouptypeCode && customerIds.Contains(org.CustomerId) && employerIds.Contains(org.EmployerId) && employeeNumbers.Contains(org.EmployeeNumber)).OrderByDescending(o=>o.Dates).ToList();
            else
                employeeOrganizations = EmployeeOrganization.AsQueryable().Where(org => customerIds.Contains(org.CustomerId) && employerIds.Contains(org.EmployerId) && employerIds.Contains(org.EmployerId) && employeeNumbers.Contains(org.EmployeeNumber)).OrderByDescending(o => o.Dates).ToList();
            List<EmployeeOrganization> employeeUniqueOrganizations = new List<EmployeeOrganization>();
            foreach (EmployeeOrganization empOrg in employeeOrganizations)
            {
                if (empOrg.TypeCode != OrganizationType.OfficeLocationTypeCode || 
                    ((empOrg.Dates == null || empOrg.Dates.IncludesToday(true)) 
                    && (!employeeUniqueOrganizations.Any(empUniqueOrg => empUniqueOrg.TypeCode == OrganizationType.OfficeLocationTypeCode && empUniqueOrg.EmployeeNumber == empOrg.EmployeeNumber && empUniqueOrg.CustomerId == empOrg.CustomerId && empUniqueOrg.EmployerId == empOrg.EmployerId))))
                {
                    employeeUniqueOrganizations.Add(empOrg);
                }                
            }
            var groupQuery = employeeUniqueOrganizations.GroupBy(org => new { org.CustomerId, org.EmployerId, org.Code })
           .Select(g => new
           {
               label = employeeUniqueOrganizations.FirstOrDefault(empOrgs => empOrgs.Code == g.Key.Code)?.Name,
               data = g.Select(x => x).ToList()
           }).OrderBy(m => m.label);

            List<ResultData> resultItems;

            result.Groups = new List<ResultDataGroup>();
            int i = 1;
            //Avoid displaying cases under more than one office location code.
            List<string> empLocationCase = new List<string>();
            foreach (var t in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (var org in t.data)
                {
                    foreach (Case td in query.Where(cs => cs.Employee.EmployeeNumber == org.EmployeeNumber && cs.CustomerId == org.CustomerId && cs.EmployerId == org.EmployerId).OrderBy(c=>c.Employee.FullName))
                    {
                        if (!string.IsNullOrWhiteSpace(org.TypeCode) 
                            && empLocationCase != null
                            && org.TypeCode.ToUpper().TrimEnd() == OrganizationType.OfficeLocationTypeCode.ToUpper().TrimEnd() 
                            && empLocationCase.Contains(td.Id.ToUpper()))
                        {
                            continue;
                        }

                        string displayCaseNumber = td.CaseNumber;
                        string displayEmployeeNumber = td.Employee.EmployeeNumber;
                        string displayFullName = td.Employee.FullName;
                        string displayReason = td.Reason.Name;
                        string displayLeaveType = td.CaseType.ToString();
                        DateTime? displayRTW = GetCaseEventDate(td, CaseEventType.ReturnToWork);
                        DateTime? displayERTW = GetCaseEventDate(td, CaseEventType.EstimatedReturnToWork);

                        List<PolicySummary> ps = null;
                        using (var service = new CaseService())
                        {
                            ps = service.GetEmployeePolicySummaryByCaseId(td.Id, null, td.Summary.Policies.Select(p => p.PolicyCode).ToArray());
                        }

                        foreach (var policy in td.Summary.Policies.OrderBy(p => p.PolicyCode))
                        {
                            foreach (var detail in policy.Detail.OrderBy(d => d.StartDate))
                            {
                                dynamic data = new ResultData();

                                if (isTpa)
                                {
                                    data.Employer = td.EmployerName;
                                }
                                data.CaseNumber = string.IsNullOrWhiteSpace(displayCaseNumber) ? null : new LinkItem() { Value = td.CaseNumber, Link = string.Format("/Cases/{0}/View", td.Id) };
                                data.EmployeeNumber = string.IsNullOrWhiteSpace(displayEmployeeNumber) ? null : new LinkItem() { Value = td.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", td.Employee.Id) };
                                data.Name = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = td.Employee.FullName, Link = string.Format("/Employees/{0}/View", td.Employee.Id) };
                                data.Reason = displayReason;
                                data.LeaveType = displayLeaveType;
                                data.StartDate = detail.StartDate.ToString("MM/dd/yyyy");
                                data.EndDate = detail.EndDate.ToString("MM/dd/yyyy");
                                data.Status = td.Status.ToString().SplitCamelCaseString();
                                data.Policies = string.Concat(policy.PolicyCode, " - ", detail.Determination.ToString().SplitCamelCaseString());
                                var pd = ps.FirstOrDefault(m => m.PolicyCode == policy.PolicyCode);
                                data.TimeUsed = pd == null ? "N/A" : pd.TimeUsed < 0 ? "reasonable amount" : String.Format("{0:N2} {1}", pd.TimeUsed, pd.Units.ToString().ToLowerInvariant());
                                data.TimeAvailable = pd == null ? "N/A" : pd.TimeRemaining < 0 ? "reasonable amount" : String.Format("{0:N2} {1}", pd.TimeRemaining, pd.Units.ToString().ToLowerInvariant());

                                data.ReturnToWork = displayRTW != null ? displayRTW.ToString("MM/dd/yyyy") : string.Empty;
                                data.EstimatedReturnToWork = displayERTW != null ? displayERTW.ToString("MM/dd/yyyy") : string.Empty;

                                resultItems.Add(data);

                                if (!string.IsNullOrWhiteSpace(org.TypeCode) 
                                    && empLocationCase != null
                                    && org.TypeCode.ToUpper().TrimEnd() == OrganizationType.OfficeLocationTypeCode.ToUpper().TrimEnd() 
                                    && (!empLocationCase.Contains(td.Id.ToUpper())))
                                {
                                    empLocationCase.Add(td.Id.ToUpper());
                                }

                                displayCaseNumber = string.Empty;
                                displayEmployeeNumber = string.Empty;
                                displayFullName = string.Empty;
                                displayReason = string.Empty;
                                displayLeaveType = string.Empty;
                            }
                        }

                        if (td.Summary.Policies.Count == 0)
                        {
                            dynamic data = new ResultData();
                            if (isTpa)
                            {
                                data.Employer = td.EmployerName;
                            }
                            data.CaseNumber = string.IsNullOrWhiteSpace(displayCaseNumber) ? null : new LinkItem() { Value = td.CaseNumber, Link = string.Format("/Cases/{0}/View", td.Id) };
                            data.EmployeeNumber = string.IsNullOrWhiteSpace(displayEmployeeNumber) ? null : new LinkItem() { Value = td.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", td.Employee.Id) };
                            data.Name = string.IsNullOrWhiteSpace(displayFullName) ? null : new LinkItem() { Value = td.Employee.FullName, Link = string.Format("/Employees/{0}/View", td.Employee.Id) };
                            data.Reason = displayReason;
                            data.LeaveType = displayLeaveType;
                            data.StartDate = td.StartDate.ToString("MM/dd/yyyy");
                            data.EndDate = td.EndDate.ToString("MM/dd/yyyy");
                            data.Status = td.Status.ToString().SplitCamelCaseString();
                            data.Policies = "N/A";
                            data.TimeUsed = "N/A";
                            data.TimeAvailable = "N/A";

                            data.ReturnToWork = displayRTW != null ? displayRTW.ToString("MM/dd/yyyy") : string.Empty;
                            data.EstimatedReturnToWork = displayERTW != null ? displayERTW.ToString("MM/dd/yyyy") : string.Empty;

                            resultItems.Add(data);
                        }
                    }
                }
                if (resultItems.Count > 0)
                {
                    result.Groups.Add(new ResultDataGroup()
                    {
                        Key = i,
                        Label = t.label,
                        Items = new List<ResultData>().AddRangeFluid(resultItems ?? new List<ResultData>(0))
                    });
                    i++;
                }
            }         
        }
    }
}
