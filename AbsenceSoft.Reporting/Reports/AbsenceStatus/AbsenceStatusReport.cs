﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Reporting.Reports.AbsenceStatus
{
    [Serializable]
    public abstract class AbsenceStatusReport : BaseReport
    {

        /// <summary>
        /// Category for all Absence Status reports is "Operations Report".
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Category for all Absence Status reports is "Absence Status Report".
        /// </summary>
        public override string Category { get { return "Absence Status Report"; } }

        /// <summary>
        /// Icon to set for Absence Status Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns>LOA Feature</returns>
        public override IEnumerable<Feature> RequiredFeatures()
        {
            yield return Feature.LOA;
        }

        /// <summary>
        /// When overridden in a derived class, gets which type of chart been displayed.
        /// </summary>
        public override ReportChart Chart
        {
            get { return null; }
        }

        /// <summary>
        /// To be used for ESS category
        /// </summary>
        public override string ESSCategoryTitle { get { return "Absence Status Reports"; } }

        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria
            {
                CustomerId = user.CustomerId,
                StartDate = DateTime.UtcNow.Date.AddDays(-30),
                EndDate = DateTime.UtcNow.Date
            };
            ReportCriteriaItem item = GetCaseStatusCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetAbsenceReasonCriteria(user);
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetCaseTypeCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            item = GetAdjudicationCriteria();
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            criteria.Filters.Add(GetWorkStateCriteria(user, criteria.StartDate, criteria.EndDate));
            criteria.Filters.AddRange(GetOrganizationTypeCriteria(user));
            if (IncludeOrganizationGrouping) criteria.Filters.Add(GetOrganizationTypeGroupByCriteria(user));
            return criteria;
        }

        protected virtual List<Case> RunQuery(User user, ReportCriteria criteria)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "Employee._id"));
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            ApplyEmployerFilter<Case>(ands, criteria);

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.LTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.GTE(e => e.EndDate, new BsonDateTime(endDate)))
            ));

            if (criteria.Filters.Count != 0)
            {
                if (criteria.Filters.Any(m => m.Name == "CaseStatus" && m.Value != null))
                {
                    CaseStatus enumCaseStatusFilter = criteria.ValueOf<CaseStatus>("CaseStatus");
                    ands.Add(Case.Query.EQ(e => e.Status, enumCaseStatusFilter));
                }

                if (criteria.Filters.Any(m => m.Name == "CaseType" && m.Value != null))
                {
                    CaseType enumCaseTypeFilter = criteria.ValueOf<CaseType>("CaseType");
                    ands.Add(Case.Query.EQ(e => e.Segments[0].Type, enumCaseTypeFilter));
                }

                if (criteria.Filters.Any(m => m.Name == "Determination" && m.Value != null))
                {
                    AdjudicationStatus enumadjStatusFilter = criteria.ValueOf<AdjudicationStatus>("Determination");
                    ands.Add(Case.Query.EQ(e => e.Summary.Determination, enumadjStatusFilter));
                }

                if (criteria.Filters.Any(m => m.Name == "AbsenceReason" && m.Value != null))
                {
                    ands.Add(Case.Query.EQ(e => e.Reason.Id, criteria.Filters.FirstOrDefault(m => m.Name == "AbsenceReason").Value));
                }

                if (criteria.Filters.Any(m => m.Name == "WorkState" && m.Value != null))
                {
                    ands.Add(Case.Query.EQ(e => e.Employee.WorkState, criteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value));
                }

                var locationFilter = criteria.Filters.FirstOrDefault(m => m.Name == "OFFICELOCATION");
                if (locationFilter != null && locationFilter.Values != null && locationFilter.Values.Any())
                {
                    var val = locationFilter.ValuesAs<string>();
                    if (val != null && val.Any())
                    {
                        ands.Add(Case.Query.In(e => e.Employee.Info.OfficeLocation, val));
                    }
                }
                AppendOrganizationFilterQuery(user, criteria, queries: ands, employeeIdField: "Employee._id");
            }

            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null);
            return query.ToList();

        }
    }
}
