﻿using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Reporting.Reports.AbsenceStatus
{
    [Serializable]
    public class AbsenceStatusByPolicyReport : AbsenceStatusReport
    {
        public override string Id { get { return "EBE61ECA-5222-460e-B2D1-684069156EBC"; } }
        public override string Name { get { return "by Policy"; } }
        public override bool IsGrouped { get { return true; } }

        protected override void RunReport(User user, ReportResult result)
        {
            var query = RunQuery(user, result.Criteria);

            List<ReportByPolicyViewModel> lstVM = new List<ReportByPolicyViewModel>();

            foreach (var q in query)
            {
                List<PolicySummary> ps = new List<PolicySummary>();
                using (var service = new CaseService())
                {
                    ps = service.GetEmployeePolicySummaryByCaseId(q.Id, null);
                }

                var caseAppliedPolicies = (from p1 in q.Summary.Policies
                                           join p2 in ps on p1.PolicyCode equals p2.PolicyCode
                                           select p2);

                foreach (var p in caseAppliedPolicies)
                {
                    ReportByPolicyViewModel vm = new ReportByPolicyViewModel
                    {
                        CaseNumber = new LinkItem() { Value = q.CaseNumber, Link = string.Format("/Cases/{0}/View", q.Id) },
                        EmployeeNumber = new LinkItem() { Value = q.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", q.Employee.Id) },
                        EmployeeName = new LinkItem() { Value = q.Employee.FullName, Link = string.Format("/Employees/{0}/View", q.Employee.Id) },
                        Reason = q.Reason.Name,
                        LeaveType = q.CaseType.ToString(),
                        StartDate = q.StartDate,
                        EndDate = q.EndDate,
                        Status = q.Status.ToString().SplitCamelCaseString(),
                        PolicyName = p.PolicyName
                    };
                    if (q.Summary.Policies.Any(m => m.PolicyCode == p.PolicyCode))
                    {
                        if (q.Summary.Policies.FirstOrDefault(m => m.PolicyCode == p.PolicyCode)?.Detail.Any() ?? false)
                        {
                            vm.Determination = q.Summary.Policies.FirstOrDefault(m => m.PolicyCode == p.PolicyCode).Detail[0].Determination.ToString();
                        }
                        else
                        {
                            vm.Determination = string.Empty;
                        }
                    }
                    else
                    {
                        vm.Determination = string.Empty;
                    }
                    vm.TimeUsed = p.TimeUsed < 0 ? "reasonable amount" : String.Format("{0:N2} {1}", p.TimeUsed, p.Units.ToString().ToLowerInvariant());
                    vm.TimeAvailable = p.TimeRemaining < 0 ? "reasonable amount" : String.Format("{0:N2} {1}", p.TimeRemaining, p.Units.ToString().ToLowerInvariant());

                    lstVM.Add(vm);
                }

                if (caseAppliedPolicies == null || !caseAppliedPolicies.Any())
                {
                    ReportByPolicyViewModel vm = new ReportByPolicyViewModel
                    {
                        CaseNumber = new LinkItem() { Value = q.CaseNumber, Link = string.Format("/Cases/{0}/View", q.Id) },
                        EmployeeNumber = new LinkItem() { Value = q.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", q.Employee.Id) },
                        EmployeeName = new LinkItem() { Value = q.Employee.FullName, Link = string.Format("/Employees/{0}/View", q.Employee.Id) },
                        Reason = q.Reason.Name,
                        LeaveType = q.CaseType.ToString(),
                        StartDate = q.StartDate,
                        EndDate = q.EndDate,
                        Status = q.Status.ToString().SplitCamelCaseString(),
                        PolicyName = "N/A",
                        Determination = "N/A",
                        TimeUsed = "N/A",
                        TimeAvailable = "N/A"
                    };
                    lstVM.Add(vm);
                }
            }

            var groupQuery = lstVM.GroupBy(n => n.PolicyName)
           .Select(g => new
           {
               label = g.Key,
               data = g.Select(x => x).ToList()
           }).OrderBy(m => m.label);

            List<ResultData> resultItems;
            result.Groups = new List<ResultDataGroup>();
            int i = 1;


            foreach (var grp in groupQuery)
            {
                resultItems = new List<ResultData>();

                string prevCaseNumber = string.Empty;

                foreach (var td in grp.data)
                {
                    dynamic data = new ResultData();
                    if (prevCaseNumber != td.CaseNumber.Value.ToString())
                    {
                        data.CaseNumber = td.CaseNumber;
                        prevCaseNumber = td.CaseNumber.Value.ToString();
                    }

                    data.EmployeeNumber = td.EmployeeNumber;
                    data.Name = td.EmployeeName;
                    data.Reason = td.Reason;
                    data.LeaveType = td.LeaveType;
                    data.StartDate = td.StartDate.ToString("MM/dd/yyyy");
                    data.EndDate = td.EndDate.ToString("MM/dd/yyyy");
                    data.Status = td.Status;
                    data.Determination = td.Determination;
                    data.TimeUsed = td.TimeUsed;
                    data.TimeAvailable = td.TimeAvailable;
                    resultItems.Add(data);

                    if (prevCaseNumber == string.Empty)
                        prevCaseNumber = td.CaseNumber.Value.ToString();
                }

                result.Groups.Add(new ResultDataGroup()
                {
                    Key = i,
                    Label = grp.label,
                    Items = new List<ResultData>().AddRangeFluid(resultItems ?? new List<ResultData>(0))
                });

                i++;
            }
        }

        private class ReportByPolicyViewModel
        {
            public LinkItem EmployeeNumber { get; set; }
            public LinkItem EmployeeName { get; set; }
            public LinkItem CaseNumber { get; set; }
            public string Reason { get; set; }
            public string LeaveType { get; set; }
            public string Status { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public string Determination { get; set; }
            public string TimeUsed { get; set; }
            public string TimeAvailable { get; set; }
            public string PolicyName { get; set; }
        }
    }
}
