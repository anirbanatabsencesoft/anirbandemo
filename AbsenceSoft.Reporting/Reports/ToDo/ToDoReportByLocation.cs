﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Reporting.Reports.ToDo
{
    [Serializable]
    public class ToDoReportByLocation : ToDoReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "DEB288F4-3A9C-4CF9-8B71-8BCZD9DE44D"; } }

        public override bool IsVisible(User user)
        {
            return false;
        }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "by Location"; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 6; } }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void RunReport(User user, ReportResult result)
        {
            var query = BuildQuery(user, result.Criteria);
            var todos = GetToDos(query);
            result
                .Groups = todos.OrderBy(t =>
                {
                    return t.Metadata.GetRawValue<string>("OfficeLocation") ?? "";
                }).GroupBy(t =>
                {
                    return t.Metadata.GetRawValue<string>("OfficeLocation") ?? "";
                }).Select(g => new ResultDataGroup()
            {
                Key = g.Key,
                Label = g.Key,
                Items = g.Select(GetResult).ToList()
            }).ToList();
        }
    }
}
