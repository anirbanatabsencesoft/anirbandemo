﻿using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Reporting.Reports.ToDo
{
    [Serializable]
    public class ToDoReportByOrganization : ToDoReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "6637BC27-E32E-4193-9BFF-3B692D3CD62B"; } }        

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "by Organization"; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 7; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }

        /// <summary>
        /// Include the Organization Grouping Criteria
        /// </summary>
        public override bool IncludeOrganizationGrouping { get { return true; } }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void RunReport(User user, ReportResult result)
        {
            var query = BuildQuery(user, result.Criteria);
            var todos = GetToDos(query);
            string grouptypeCode = null;
            var customer = user.Customer;
            if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "OrganizationTypeGrouping") != null)
                grouptypeCode = result.Criteria.ValueOf<string>("OrganizationTypeGrouping");
            List<string> customerIds = todos.Select(todo => todo.CustomerId).Distinct().ToList();
            List<string> employerIds = todos.Select(todo => todo.EmployerId).Distinct().ToList();
            List<string> employeeIds = todos.Select(todo => todo.EmployeeId).Distinct().ToList();
            List<Employee> employees = Employee.AsQueryable().Where(emp => employeeIds.Contains(emp.Id)).ToList();
            List<string> employeeNumbers = employees.Select(emp => emp.EmployeeNumber).ToList();
            List<EmployeeOrganization> employeeOrganizations = null;
            if (grouptypeCode != null)
                employeeOrganizations = EmployeeOrganization.AsQueryable().Where(org => org.TypeCode == grouptypeCode && customerIds.Contains(org.CustomerId) && employerIds.Contains(org.EmployerId) && employeeNumbers.Contains(org.EmployeeNumber)).OrderByDescending(o => o.Dates).ToList();
            else
                employeeOrganizations = EmployeeOrganization.AsQueryable().Where(org => customerIds.Contains(org.CustomerId) && employerIds.Contains(org.EmployerId) && employerIds.Contains(org.EmployerId) && employeeNumbers.Contains(org.EmployeeNumber)).OrderByDescending(o => o.Dates).ToList();
            List<EmployeeOrganization> employeeUniqueOrganizations = new List<EmployeeOrganization>();
            foreach (EmployeeOrganization empOrg in employeeOrganizations)
            {
                if (empOrg.TypeCode != OrganizationType.OfficeLocationTypeCode ||
                    ((empOrg.Dates == null || empOrg.Dates.IncludesToday(true))
                    && (!employeeUniqueOrganizations.Where(empUniqueOrg => empUniqueOrg.TypeCode == OrganizationType.OfficeLocationTypeCode && empUniqueOrg.EmployeeNumber == empOrg.EmployeeNumber && empUniqueOrg.CustomerId == empOrg.CustomerId && empUniqueOrg.EmployerId == empOrg.EmployerId).Any())))
                {
                    employeeUniqueOrganizations.Add(empOrg);
                }
            }
            var groupQuery = employeeUniqueOrganizations.GroupBy(org => new { CustomerId = org.CustomerId, EmployerId = org.EmployerId, Code = org.Code })
           .Select(g => new
           {
               label = employeeUniqueOrganizations.Where(empOrgs => empOrgs.Code == g.Key.Code).FirstOrDefault().Name,
               data = g.Select(x => x).ToList()
           }).OrderBy(m => m.label);
            List<ResultData> resultItems = new List<ResultData>();
            result.Groups = new List<ResultDataGroup>();
            int i = 1;
            //Avoid displaying cases under more than one office location code.
            List<string> empLocationToDo = new List<string>();
            List<string> empOrganizationToDo = new List<string>();
            foreach (var t in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (var org in t.data)
                {
                    foreach (ToDoItem td in todos.Where(todo => todo.CustomerId == org.CustomerId && todo.EmployerId == org.EmployerId))
                    {
                        Employee employee = employees.FirstOrDefault(emp => td.EmployeeId == emp.Id && emp.EmployeeNumber == org.EmployeeNumber && emp.CustomerId == org.CustomerId && emp.EmployerId == org.EmployerId);
                        if (employee != null)
                        {
                            if (org.TypeCode != null && org.TypeCode.ToUpper().TrimEnd() == OrganizationType.OfficeLocationTypeCode.ToUpper().TrimEnd() && empLocationToDo.Contains(td.Id.ToUpper())) continue;
                            dynamic data = GetResult(td);
                            data.Location = org.Code;
                            resultItems.Add(data);                            
                            if (grouptypeCode == null) empOrganizationToDo.Add(td.Id);
                            if (org.TypeCode != null && org.TypeCode.ToUpper().TrimEnd() == OrganizationType.OfficeLocationTypeCode.ToUpper().TrimEnd() && (!empLocationToDo.Contains(td.Id.ToUpper()))) empLocationToDo.Add(td.Id.ToUpper());
                        }
                    }
                }
                if (resultItems != null && resultItems.Count > 0)
                {
                    result.Groups.Add(new ResultDataGroup()
                    {
                        Key = i,
                        Label = t.label,
                        Items = new List<ResultData>().AddRangeFluid(resultItems)
                    });
                    i++;
                }
            }
            if (grouptypeCode == null && todos != null && todos.Any())
            {
                var noOrgTodos = todos.Where(todo => !empOrganizationToDo.Contains(todo.Id));
                if (noOrgTodos.Any())
                {
                    resultItems = new List<ResultData>();
                    foreach (var todo in noOrgTodos)
                    {
                        dynamic data = GetResult(todo);                        
                        resultItems.Add(data);
                    }
                    result.Groups.Add(new ResultDataGroup()
                    {
                        Key = result.Groups.Count + 1,
                        Label = "None",
                        Items = new List<ResultData>().AddRangeFluid(resultItems)
                    });
                }
            }                    
        }
    }
}
