﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Reporting.Reports.ToDo
{
    [Serializable]
    public class TctReportByType : ToDoReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "DEB288F4-329C-4CF7-8B71-9BBA9D9DE42D"; } }

        public override bool IsVisible(User user)
        {
            return false;
        }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "Completion by Type"; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 5; } }

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether to show the auto 
        /// generated total counts at the bottom of the group page.
        /// </summary>
        /// <value>
        ///   <c>true</c> if group auto totals; otherwise, <c>false</c>.
        /// </value>
        public override bool GroupAutoTotals { get { return false; } }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            var criteria = base.GetCriteria(user);
            criteria.Filters.First(f => f.Name == "ToDoStatus").Value = (int)AbsenceSoft.Data.Enums.ToDoItemStatus.Complete;
            return criteria;
        }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void RunReport(User user, ReportResult result)
        {
            var query = BuildQuery(user, result.Criteria);
            var todos = ToDoItem.Query.Find(query).SetBatchSize(100).ToList();
            var groupQuery = todos.Where(t => t.Status == Data.Enums.ToDoItemStatus.Complete || t.Status == Data.Enums.ToDoItemStatus.Open || t.Status == Data.Enums.ToDoItemStatus.Pending)
                .OrderBy(t => t.ItemType)
                .GroupBy(t => t.ItemType.ToString().SplitCamelCaseString()).Select(g => new
            {
                key = g.Key,
                label = g.Key,
                data = g.ToList()
            });

            List<ResultData> resultItems = new List<ResultData>();
            result.Groups = new List<ResultDataGroup>(groupQuery.Count() + 1);
            List<Tuple<string, int, string, string>> summary = new List<Tuple<string, int, string, string>>(groupQuery.Count());
            foreach (var t in groupQuery)
            {
                int totalAverageHandleTimeDays = 0;
                int totalAverageHandleTimeHours = 0;
                int totalAverageTimelineDays = 0;
                int totalAverageTimelineHours = 0;

                resultItems = new List<ResultData>();

                foreach (var td in t.data)
                {
                    int avrageHandleTimeDays = 0;
                    int avrageHandleTimeHours = 0;
                    TimeSpan avrageHandleTime;
                    avrageHandleTime = td.Status == Data.Enums.ToDoItemStatus.Complete ?
                                        td.ModifiedDate != null && td.CreatedDate != null ?
                                        (td.ModifiedDate - td.CreatedDate)
                                        : new TimeSpan()
                                        : td.CreatedDate != null ?
                                        (DateTime.UtcNow - td.CreatedDate)
                                        : new TimeSpan();
                    avrageHandleTimeDays = Convert.ToInt32(Math.Floor((avrageHandleTime.TotalHours / 24)));
                    avrageHandleTimeHours = avrageHandleTime.Hours;

                    totalAverageHandleTimeDays += avrageHandleTimeDays;
                    totalAverageHandleTimeHours += avrageHandleTimeHours;

                    int timeLinesDays = 0;
                    int timeLinesHours = 0;
                    TimeSpan timeLines;
                    timeLines = td.Status == Data.Enums.ToDoItemStatus.Complete ?
                        td.ModifiedDate != null && td.DueDate != null ?
                        (td.DueDate - td.ModifiedDate)
                        : new TimeSpan()
                        : td.DueDate != null ?
                        (td.DueDate - DateTime.UtcNow)
                        : new TimeSpan();
                    timeLinesDays = Convert.ToInt32(Math.Floor((timeLines.TotalHours / 24)));
                    timeLinesHours = timeLines.Hours;

                    totalAverageTimelineDays = totalAverageTimelineDays + timeLinesDays;
                    totalAverageTimelineHours = totalAverageTimelineHours + timeLinesHours;

                    dynamic data = new ResultData();
                    data.CaseNumber = new LinkItem() { Value = td.CaseNumber, Link = string.Format("/Cases/{0}/View", td.CaseId) };
                    data.EmployeeName = new LinkItem() { Value = td.EmployeeName, Link = string.Format("/Employees/{0}/View", td.EmployeeId) };
                    data.AssignedTo = td.AssignedToName;
                    data.ToDo = td.Title ?? td.ItemType.ToString().SplitCamelCaseString();
                    data.CreatedOn = td.CreatedDate.ToString("MM/dd/yyyy");
                    data.DueBy = td.DueDate.ToString("MM/dd/yyyy");
                    data.CompletedOn = td.Status == Data.Enums.ToDoItemStatus.Complete ? td.ModifiedDate.ToString("MM/dd/yyyy") : DateTime.UtcNow.ToString("MM/dd/yyyy");
                    data.AverageHandleTime = string.Format("{0:N0} days {1:N0} hours", avrageHandleTimeDays, avrageHandleTimeHours);
                    data.Timelines = string.Format("{0:N0} days {1:N0} hours", timeLinesDays, timeLinesHours);
                    resultItems.Add(data);
                }

                result.Groups.Add(new ResultDataGroup()
                {
                    Key = t.key,
                    Label = t.label,
                    Items = new List<ResultData>().AddRangeFluid(resultItems)
                });

                summary.Add(new Tuple<string, int, string, string>(t.label, t.data.Count,
                    string.Format("{0:N0} days {1:N0} hours", 
                    Math.Floor((decimal)totalAverageHandleTimeDays / (decimal)t.data.Count), 
                    Math.Floor((decimal)totalAverageHandleTimeHours / (decimal)t.data.Count)),
                    string.Format("{0:N0} days {1:N0} hours", 
                    Math.Floor((decimal)totalAverageTimelineDays / (decimal)t.data.Count), 
                    Math.Floor((decimal)totalAverageTimelineHours / (decimal)t.data.Count))));
            }

            result.Groups.Insert(0, new ResultDataGroup()
            {
                Key = "Summary",
                Label = "Summary",
                Items = summary.Select(m =>
                {
                    var rd = new ResultData();
                    rd["Type"] = m.Item1;
                    rd["Count"] = m.Item2.ToString();
                    rd["Average Handle Time"] = m.Item3;
                    rd["Average Timelines"] = m.Item4;
                    return rd;
                }).ToList()
            });
        }
    }
}
