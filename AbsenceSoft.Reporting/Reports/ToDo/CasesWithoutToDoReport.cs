﻿using AbsenceSoft;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Reporting;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Reporting.Reports.ToDo
{
    [Serializable]
    public class CasesWithoutToDoReport : BaseReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "EA194CE4-7E4D-429D-9D19-CC092A4E307C"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report main category to bifurcate reports.
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Icon to set for intermittent pattern report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_intermittent.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report category for display.
        /// </summary>
        public override string Category { get { return "ToDo Reports"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "Cases Without"; } }

        /// <summary>
        /// Determines whether the specified report is visible to the given user for any valid reason.
        /// </summary>
        /// <param name="user">The user to check against.</param>
        /// <returns>
        ///   <c>true</c> if the specified report is visible; otherwise, <c>false</c>.
        /// </returns>
        public override bool IsVisible(User user)
        {
            if (user == null) return false;
            return true;
        }

        /// <summary>
        /// Property to configure chart related options
        /// </summary>
        public override ReportChart Chart { get { return null; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return false; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 4; } }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria();
            criteria.StartDate = DateTime.UtcNow.GetFirstDayOfYear().Date;
            criteria.EndDate = DateTime.UtcNow.Date;
            criteria.CustomerId = user.CustomerId;
            criteria.Filters.AddRange(GetOrganizationTypeCriteria(user));
            return criteria;
        }

        [Serializable]
        public class MapReduceResultItem
        {
            [BsonId, BsonRepresentation(BsonType.ObjectId)]
            public string _id { get; set; }

            public MapReduceResultItemItem value { get; set; }
        }
        [Serializable]
        public class MapReduceResultItemItem
        {
            [BsonDefaultValue(0)]
            public int ToDos { get; set; }

            [BsonIgnoreIfNull]
            public Case Case { get; set; }
        }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected override void RunReport(User user, ReportResult result)
        {
            var criteria = result.Criteria;
            var qh = new ReportQueryHandler("c", new ReportQueryHandler.MappedColumns("cust_id", "eplr_id", "empl_id", "public.view_todo"), criteria.CustomerId, user, true);
            

            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            //Dictionary<string, object> dict = new Dictionary<string, object>();
            qh.WhereConditions.Add("startDate", startDate);
            qh.WhereConditions.Add("endDate", endDate);

            //create instance handler
            

            //define select query
            var selectSql = @"SELECT 
                                c.case_id
                                , c.case_case_number
                                , c.empl_id
                                , CONCAT(c.empl_first_name, ' ', c.empl_last_name) as employee_name
                                , c.empl_employee_number
	                            , c.case_reason
                                , c.case_case_type
                                , c.case_case_created_date
                                , c.case_current_office_location
                                , c.case_case_status
                                , c.case_assigned_to";

            //define from SQL
            var fromSql = @"FROM public.view_case c
                                LEFT JOIN public.dim_todoitem td ON c.case_id = td.case_id AND td.ver_is_current=True AND td.is_deleted=False AND td.status IN (-1, 2)";

            //define qhere clause
            var whereSql = $@" WHERE c.case_case_created_date >= @startDate 
                                AND c.case_case_created_date <= @endDate 
                            AND c.case_case_status IN ('Open', 'Inquiry')
                                AND td.case_id IS NULL
                            ";

            //define order sql
            var orderSql = " ORDER BY c.eplr_id ASC, c.case_case_created_date DESC";

            //Add the SQL to handler
            qh.AppendSql(selectQuery: selectSql, fromQuery: fromSql, filterQuery: whereSql);



            //add data access filter
            qh.AppendDataAccessFilter(Permission.RunCaseManagerReport.Id, null, criteria);

            //add organization filters
            qh.AppendOrganizationFilter(criteria);

            //add additional filter. In this case location
            qh.AppendAdditionalFilters(AppendLocationFilter(criteria, qh.WhereConditions));

            //add order sql
            qh.AppendOrderSql(orderSql);

            //define result
            result.Items = new List<ResultData>();

            //get data
            qh.GetAndFormatData(qh.WhereConditions, FormatResult);

            //format and data to result
            void FormatResult(NpgsqlDataReader reader)
            {
                dynamic data = new ResultData();

                data.CaseNumber = new LinkItem() { Value = reader.GetString(1), Link = string.Format("/Cases/{0}/View", reader.GetValue(0)) };
                data.Name = new LinkItem() { Value = reader.GetString(3), Link = string.Format("/Employees/{0}/View", reader.GetString(2)) };
                data.EmployeeNumber = new LinkItem() { Value = reader.GetString(4), Link = string.Format("/Employees/{0}/View", reader.GetString(2)) };
                data.Reason = reader.GetStringOrEmpty(5);
                data.CaseType = reader.GetStringOrEmpty(6);

                var cd = reader.GetDateTimeOrNull(7);
                data.CreatedOn = cd != null ? cd.ToString("MM/dd/yyyy") : "";

                data.Status = reader.GetStringOrEmpty(9);
                data.AssignedTo = reader.GetStringOrEmpty(10);
                data.Location = reader.GetStringOrEmpty(8);

                result.Items.Add(data);
            }
        }

        /// <summary>
        /// This is a local method to append location filters
        /// </summary>
        /// <returns></returns>
        string AppendLocationFilter(ReportCriteria criteria, Dictionary<string, object> dict)
        {
            var officeLocationFilter = criteria.Filters.FirstOrDefault(m => m.Name == "OFFICELOCATION");
            if (officeLocationFilter != null && officeLocationFilter.Values != null && officeLocationFilter.Values.Any())
            {
                var locations = officeLocationFilter.ValuesAs<string>();
                if (locations != null && locations.Any())
                {
                    List<string> parameters = new List<string>();
                    for (var i = 0; i < locations.Count; i++)
                    {
                        var lf = $"@location_{i}";
                        parameters.Add(lf);
                        dict.Add(lf, locations[i]);
                    }

                    return $"c.case_current_office_location IN({string.Join(",", parameters)})";
                }
            }

            return "";
        }
    }
}
