﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Reporting;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Reporting.Reports.ToDo
{
    [Serializable]
    public class ToDoReportDetail : ToDoReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "C4CD5D21-58B3-47EB-BFA6-BEDADB479B7F"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "Detail"; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 1; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return false; } }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void RunReport(User user, ReportResult result)
        {
            var criteria = result.Criteria ?? GetCriteria(user);


            //create instance handler
            var qh = new ReportQueryHandler("t", new ReportQueryHandler.MappedColumns("cust_id", "eplr_id", "empl_id", "public.view_todo"), criteria.CustomerId, user, true);


            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();
 
            qh.WhereConditions.Add("startDate", startDate);
            qh.WhereConditions.Add("endDate", endDate);


            var selectSql = @"SELECT 
	                                t.case_id
	                                , t.case_case_number
	                                , t.empl_id
	                                , t.empl_employee_number
	                                , CONCAT(t.empl_first_name, ' ', t.empl_last_name) as employee_name
	                                , CONCAT(t.todo_assigned_to_first_name, ' ', t.todo_assigned_to_last_name) as assigned_to
	                                , t.todo_title
	                                , t.todo_todo_type
	                                , t.todo_due_date
	                                , t.todo_todo_last_modified_date
	                                , t.todo_todo_status
	                                , t.todo_modified_by_name
	                                , t.case_current_office_location
	                                , t.case_case_status
	                                , t.todo_todo_created_date";

            var fromSql = @"FROM public.view_todo t";

            var whereSql = $@" WHERE t.todo_todo_created_date >= @startDate 
                                AND t.todo_todo_created_date <= @endDate";

            //Add the SQL to handler
            qh.AppendSql(selectQuery: selectSql, fromQuery: fromSql, filterQuery: whereSql);

            //add data access filter
            qh.AppendDataAccessFilter(Permission.RunCaseManagerReport.Id, null, criteria);

            //add organization filters
            qh.AppendOrganizationFilter(criteria);

            //add additional filter. In this case location/assignee/todo type/status
            qh.AppendAdditionalFilters(AppendLocationFilter(criteria, qh.WhereConditions));
            qh.AppendAdditionalFilters(AppendAssigneeFilter(criteria, qh.WhereConditions));
            qh.AppendAdditionalFilters(AppendToDoItemTypeFilter(criteria, qh.WhereConditions));
            qh.AppendAdditionalFilters(AppendStatusFilter(criteria, qh.WhereConditions));

            //define result
            result.Items = new List<ResultData>();

            //get data
            qh.GetAndFormatData(qh.WhereConditions, FormatResult);

            //format result
            void FormatResult(NpgsqlDataReader reader)
            {
                var status = reader.GetStringOrEmpty(10);
                bool isClosed = status == "Complete" || status == "Cancelled";

                dynamic data = new ResultData();
                data.CaseNumber = string.IsNullOrWhiteSpace(reader.GetStringOrEmpty(1)) ? (object)"" : new LinkItem() { Value = reader.GetStringOrEmpty(1) ?? "View Case", Link = $"/Cases/{reader.GetStringOrEmpty(0)}/View" };
                data.EmployeeNumber = new LinkItem() { Value = reader.GetStringOrEmpty(3) ?? "View Employee", Link = $"/Employees/{reader.GetStringOrEmpty(2)}/View" };
                data.EmployeeName = new LinkItem() { Value = reader.GetStringOrEmpty(4) ?? "View Employee", Link = $"/Employees/{reader.GetStringOrEmpty(2)}/View" };
                data.AssignedTo = reader.GetStringOrEmpty(5);
                data.ToDo = reader.GetStringOrEmpty(6) ?? reader.GetStringOrEmpty(7).SplitCamelCaseString();
                data.DueBy = reader.GetDateTime(8).ToString("MM/dd/yyyy");
                data.ClosedOn = isClosed ? reader.GetDateTime(9).ToString("MM/dd/yyyy") : string.Empty;
                data.ClosedBy = isClosed ? reader.GetStringOrEmpty(11) ?? "" : "";
                data.Status = status;
                data.Location = reader.GetStringOrEmpty(12);
                data.CaseStatus = reader.GetStringOrEmpty(13);
                data.ToDoCreatedDate = reader.GetDateTime(14).ToString("MM/dd/yyyy");

                result.Items.Add(data);
            }
        }

        #region Postgres methods
        /// <summary>
        /// This is a local method to append location filters
        /// </summary>
        /// <returns></returns>
        private string AppendLocationFilter(ReportCriteria criteria, Dictionary<string, object> dict)
        {
            var officeLocationFilter = criteria.Filters.FirstOrDefault(m => m.Name == "OFFICELOCATION");
            if (officeLocationFilter != null && officeLocationFilter.Values != null && officeLocationFilter.Values.Any())
            {
                var locations = officeLocationFilter.ValuesAs<string>();
                if (locations != null && locations.Any())
                {
                    List<string> parameters = new List<string>();
                    for (var i = 0; i < locations.Count; i++)
                    {
                        var lf = $"@location_{i}";
                        parameters.Add(lf);
                        dict.Add(lf, locations[i]);
                    }

                    return $"t.case_current_office_location IN({string.Join(",", parameters)})";
                }
            }

            return "";
        }

        /// <summary>
        /// Add assignee filter.
        /// Remained single select of assignee as it's now
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="dict"></param>
        /// <returns></returns>
        private string AppendAssigneeFilter(ReportCriteria criteria, Dictionary<string, object> dict)
        {
            string assignee = criteria.ValueOf<string>("ToDoItemAssignee");
            if (!string.IsNullOrWhiteSpace(assignee))
            {
                dict.Add("@assignee", assignee);
                return "t.todo_assigned_to_id=@assignee";
            }
            return "";
        }

        /// <summary>
        /// Append todo item type
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="dict"></param>
        /// <returns></returns>
        private string AppendToDoItemTypeFilter(ReportCriteria criteria, Dictionary<string, object> dict)
        {
            ToDoItemType? type = criteria.ValueOf<ToDoItemType?>("ToDoItemType");
            if (type.HasValue)
            {
                dict.Add("@todotype", Convert.ToString(type));
                return "t.todo_todo_type=@todotype";
            }
            return "";
        }

        /// <summary>
        /// Filter against the status of to-do
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="dict"></param>
        /// <returns></returns>
        private string AppendStatusFilter(ReportCriteria criteria, Dictionary<string, object> dict)
        {
            ToDoItemStatus? statusFilter = criteria.ValueOf<ToDoItemStatus?>("ToDoStatus");
            if (statusFilter.HasValue)
            {
                dict.Add("@todostatus", Convert.ToString(statusFilter));
                return "t.todo_todo_status=@todostatus";
            }
            return "";
        }
        #endregion
    }
}
