﻿using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Reporting.Reports.ToDo
{
    [Serializable]
    public class ToDoReportByAssignee : ToDoReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "340520EA-5215-4B31-AA22-73AD127D7FD9"; } }

        public override bool IsVisible(User user)
        {
            return false;
        }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "by Assignee"; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 3; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void RunReport(User user, ReportResult result)
        {
            var query = BuildQuery(user, result.Criteria);
            var todos = GetToDos(query);
            result.Groups = todos.OrderBy(t => t.AssignedToName).GroupBy(t => t.AssignedToId).Select(g => new ResultDataGroup()
            {
                Key = g.Key,
                Label = g.First().AssignedToName,
                Items = g.Select(GetResult).ToList()
            }).ToList();
        }
    }
}
