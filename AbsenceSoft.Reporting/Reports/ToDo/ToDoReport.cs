﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Reporting.Reports.ToDo
{
    /// <summary>
    /// When overridden in a derived class, represents a ToDo report that shows
    /// todos, status, etc. for case managers and may be grouped.
    /// </summary>
    [Serializable]
    public abstract class ToDoReport : BaseReport
    {
        /// <summary>
        /// When overridden in a derived class, gets the name of the report main category to bifurcate reports.
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// Icon to set for intermittent pattern report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_intermittent.png"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report category for display.
        /// </summary>
        public override string Category { get { return "ToDo Reports"; } }

        /// <summary>
        /// Determines whether the specified report is visible to the given user for any valid reason.
        /// </summary>
        /// <param name="user">The user to check against.</param>
        /// <returns>
        ///   <c>true</c> if the specified report is visible; otherwise, <c>false</c>.
        /// </returns>
        public override bool IsVisible(User user)
        {
            if (user == null) return false;
            return true;
        }

        /// <summary>
        /// Property to configure chart related options
        /// </summary>
        public override ReportChart Chart { get { return null; } }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria();
            criteria.StartDate = DateTime.UtcNow.GetFirstDayOfWeek().Date;
            criteria.EndDate = DateTime.UtcNow.Date;
            criteria.CustomerId = user.CustomerId;
            criteria.Filters.Add(GetToDoAssigneeCriteria(user));
            criteria.Filters.Add(GetToDoTypeCriteria(user));
            criteria.Filters.Add(GetToDoStatusCriteria(user));
            criteria.Filters.AddRange(GetOrganizationTypeCriteria(user));
            if (IncludeOrganizationGrouping) criteria.Filters.Add(GetOrganizationTypeGroupByCriteria(user));
            return criteria;
        }

        protected override ReportCriteriaItem GetToDoAssigneeCriteria(User user)
        {
            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(ToDoItem.Query.IsNotDeleted());
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "EmployeeId"));
            ands.Add(ToDoItem.Query.NE(t => t.AssignedToId, null));

            ReportCriteriaItem item = new ReportCriteriaItem();
            item.ControlType = Data.Enums.ControlType.SelectList;
            item.KeepOptionAll = true;
            item.Name = "ToDoItemAssignee";
            item.Prompt = "Assigned To";
            item.Required = false;
            item.OnChangeMethodName = "FilterChangeEvent";

            var userDocs = ToDoItem.Repository.Collection.Aggregate(new AggregateArgs()
            {
                OutputMode = AggregateOutputMode.Cursor,
                Pipeline = new List<BsonDocument>(8)
                {
                    BsonDocument.Parse("{ $match: " + ToDoItem.Query.And(ands).ToString() + " }"),
                    BsonDocument.Parse("{ $project: { UserId: \"$AssignedToId\", _id: false } }"),
                    BsonDocument.Parse("{ $group: { _id: \"$UserId\" } }"),
                    BsonDocument.Parse("{ $lookup: { from: \"User\", localField: \"_id\", foreignField: \"_id\", as: \"User\" } }"),
                    BsonDocument.Parse("{ $project: { User: { $arrayElemAt: [\"$User\", 0] } } }"),
                    BsonDocument.Parse("{ $match: { User: { $ne: null } } }"),
                    BsonDocument.Parse("{ $project: { User: { $ifNull: [{ $concat: [\"$User.FirstName\", \" \", \"$User.LastName\"] }, \"$User.Email\"] } } }"),
                    BsonDocument.Parse("{ $sort: { User: 1 } }")
                }
            });

            item.Options = userDocs.Select(t => new ReportCriteriaItemOption()
            {
                Value = t["_id"].AsObjectId.ToString(),
                Text = t["User"].AsString
            }).ToList();
            return item;
        }

        /// <summary>
        /// Builds the query.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="criteria">The criteria.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        protected virtual IMongoQuery BuildQuery(User user, ReportCriteria criteria)
        {
            criteria = criteria ?? GetCriteria(user);

            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "EmployeeId"));
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
                ands.Add(ToDoItem.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            ApplyEmployerFilter<ToDoItem>(ands, criteria);

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(ToDoItem.Query.GTE(t => t.DueDate, new BsonDateTime(startDate)));
            ands.Add(ToDoItem.Query.LTE(t => t.DueDate, new BsonDateTime(endDate)));

            ands.Add(ToDoItem.Query.IsNotDeleted());

            string assignee = criteria.ValueOf<string>("ToDoItemAssignee");
            ToDoItemType? type = criteria.ValueOf<ToDoItemType?>("ToDoItemType");
            ToDoItemStatus? statusFilter = criteria.ValueOf<ToDoItemStatus?>("ToDoStatus");

            if (!string.IsNullOrWhiteSpace(assignee))
                ands.Add(ToDoItem.Query.EQ(t => t.AssignedToId, assignee));
            if (type.HasValue)
                ands.Add(ToDoItem.Query.EQ(t => t.ItemType, type.Value));
            if (statusFilter.HasValue)
            {
                if (statusFilter == ToDoItemStatus.Open)
                    ands.Add(ToDoItem.Query.In(e => e.Status, new List<ToDoItemStatus>() { ToDoItemStatus.Pending, ToDoItemStatus.Overdue }));
                else if (statusFilter == ToDoItemStatus.Overdue)//overdue
                    // Either it has an explicit status of overdue, OR, it is literally overdue with a different open/active status
                    ands.Add(ToDoItem.Query.Or(
                        ToDoItem.Query.And(ToDoItem.Query.EQ(e => e.Status, ToDoItemStatus.Pending), ToDoItem.Query.LT(e => e.DueDate, DateTime.UtcNow)),
                        ToDoItem.Query.And(ToDoItem.Query.EQ(e => e.Status, ToDoItemStatus.Open), ToDoItem.Query.LT(e => e.DueDate, DateTime.UtcNow)),
                        ToDoItem.Query.EQ(e => e.Status, ToDoItemStatus.Overdue))
                    );
                else
                    ands.Add(ToDoItem.Query.EQ(t => t.Status, statusFilter.Value));
            }            
            AppendOrganizationFilterQuery(user, criteria, queries:ands);       

            return ToDoItem.Query.And(ands);
        }

        /// <summary>
        /// Gets to dos.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns></returns>
        protected List<ToDoItem> GetToDos(IMongoQuery query)
        {
            var todoAggs = ToDoItem.Repository.Collection.Aggregate(new AggregateArgs()
            {
                OutputMode = AggregateOutputMode.Cursor,
                Pipeline = new List<BsonDocument>(7)
                {
                    BsonDocument.Parse("{ $match: " + query.ToString() + " }"),
                    BsonDocument.Parse("{ $project: { Priority:1, CustomerId:1, EmployerId:1, CaseId: 1, CaseNumber: 1, EmployeeId: 1, EmployeeNumber: 1, EmployeeName: 1, AssignedToName: 1, Title: 1, ItemType: 1, DueDate: 1, mdt: 1, mby: 1, cdt: 1, cby: 1, Status: 1, OfficeLocation: 1, AssignedToId: 1 } }"),
                    BsonDocument.Parse("{ $lookup: { from: \"Employee\", localField: \"EmployeeId\", foreignField: \"_id\", as: \"Employee\" } }"),
                    BsonDocument.Parse("{ $lookup: { from: \"User\", localField: \"mby\", foreignField: \"_id\", as: \"ModifiedBy\" } }"),
                    BsonDocument.Parse("{ $lookup: { from: \"Case\", localField: \"CaseId\", foreignField: \"_id\", as: \"Case\" } }"),
                    BsonDocument.Parse("{ $project: { Priority:1, CustomerId:1, EmployerId:1, Employee: { $arrayElemAt: [\"$Employee\", 0] }, Case: { $arrayElemAt: [\"$Case\", 0] }, ModifiedBy: { $arrayElemAt: [\"$ModifiedBy\", 0] }, CaseId: 1, CaseNumber: 1, EmployeeId: 1, EmployeeNumber: 1, EmployeeName: 1, AssignedToName: 1, AssignedToId: 1, Title: 1, ItemType: 1, DueDate: 1, mdt: 1, mby: 1, cdt: 1, cby: 1, Status: 1, OfficeLocation: 1 } }"),
                    BsonDocument.Parse("{ $project: { Priority:1, CustomerId:1, EmployerId:1, CaseId: 1, CaseNumber: 1, CaseStatus: { $ifNull: [\"$Case.Status\", 4] }, EmployeeId: 1, EmployeeNumber: { $ifNull: [\"$EmployeeNumber\", \"$Employee.EmployeeNumber\"] }, EmployeeName: { $ifNull: [\"$EmployeeName\", { $concat: [\"$Employee.LastName\", \", \", \"$Employee.FirstName\"] }] }, AssignedToName: 1, AssignedToId: 1, Title: 1, ItemType: 1, DueDate: 1, mdt: 1, mby: 1, cdt: 1, cby: 1, Status: 1, OfficeLocation: { $ifNull: [\"$OfficeLocation\", \"$Employee.Info.OfficeLocation\"] }, ModifiedByName: { $ifNull: [{ $concat: [\"$ModifiedBy.FirstName\", \" \", \"$ModifiedBy.LastName\"] }, \"$ModifiedBy.Email\"] } } }")
                }
            });
            return todoAggs
                    .AsParallel()
                    .AsOrdered()
                    .WithDegreeOfParallelism(_degreeOfParallelism)
                    .Select(t => BsonSerializer.Deserialize<ToDoItem>(t))
                    .ToList();
        }

        /// <summary>
        /// Gets the result.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        protected virtual ResultData GetResult(ToDoItem item)
        {
            bool isClosed = item.Status == ToDoItemStatus.Complete || item.Status == ToDoItemStatus.Cancelled;

            dynamic data = new ResultData();
            data.CaseNumber = item.CaseId == null ? (object)"" : new LinkItem() { Value = item.CaseNumber ?? "View Case", Link = $"/Cases/{item.CaseId}/View"};
            data.EmployeeNumber = new LinkItem() { Value = item.EmployeeNumber ?? "View Employee", Link = $"/Employees/{item.EmployeeId}/View"};
            data.EmployeeName = new LinkItem() { Value = item.EmployeeName ?? "View Employee", Link = $"/Employees/{item.EmployeeId}/View" };
            data.AssignedTo = item.AssignedToName ?? string.Empty;
            data.ToDo = item.Title ?? item.ItemType.ToString().SplitCamelCaseString();
            data.DueBy = item.DueDate.ToString("MM/dd/yyyy");
            data.ClosedOn = isClosed ? item.ModifiedDate.ToString("MM/dd/yyyy") : string.Empty;
            data.ClosedBy = isClosed ? item.Metadata.GetRawValue<string>("ModifiedByName") ?? "" : "";
            data.Status = item.DueDate.Date < DateTime.Now.Date && item.Status != ToDoItemStatus.Overdue && item.Status != ToDoItemStatus.Complete && item.Status != ToDoItemStatus.Cancelled ? ToDoItemStatus.Overdue.ToString() : item.DueDate.Date >= DateTime.Now.Date && item.Status == ToDoItemStatus.Overdue ? ToDoItemStatus.Pending.ToString() : item.Status.ToString();
            data.Location = item.Metadata.GetRawValue<string>("OfficeLocation") ?? "";
            data.CaseStatus = Convert.ToString(item.Metadata.GetRawValue<CaseStatus?>("CaseStatus"));
            data.ToDoCreatedDate = item.CreatedDate.ToString("MM/dd/yyyy");
            return data;
        }
    }
}
