﻿using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Reporting.Reports.Disability
{
    [Serializable]
    public class DisabilityDurationReportByCondition : DisabilityDurationReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "F2A78242-6507-4707-A79F-D8925D3BC309"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "Duration by Condition"; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 5; } }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void RunReport(User user, ReportResult result)
        {
            var cases = RunQuery(user, result.Criteria)
                .Where(c => c.IsSTD
                    && c.Disability != null
                    && c.Disability.PrimaryDiagnosis != null
                    && c.Disability.PrimaryDiagnosis != null)
                .OrderBy(c => c.Disability.PrimaryDiagnosis.Code)
                .GroupBy(c => c.Disability.PrimaryDiagnosis.Code)
                .ToList();

            var minExceedsDurationByCriteria = (result.Criteria.Filters ?? new List<ReportCriteriaItem>(0)).FirstOrDefault(c => c.Name == "ExceedsDurationBy");
            int? minExceedsDurationBy = minExceedsDurationByCriteria == null ? null : minExceedsDurationByCriteria.ValueAs<int?>();

            result.Groups = cases.Select(c =>
            {
                var items = c.Select(a => GetResultData(user, a, minExceedsDurationBy)).Where(a => a != null).ToList();
                if (!items.Any())
                    return null;
                return new ResultDataGroup()
                {
                    Key = c.Key,
                    Label = c.First().Disability.PrimaryDiagnosis.UIDescription,
                    Items = items
                };
            }).Where(g => g != null).ToList();

            var getInts = new Func<string, List<ResultData>, List<int>>((field, items) =>
            {
                if (string.IsNullOrWhiteSpace(field) || items == null || !items.Any())
                    return new List<int>(0);

                return items.Select(i =>
                {
                    string x = (!i.ContainsKey(field) || i[field] == null ? null : i[field].ToString());
                    int e = 0;
                    if (!string.IsNullOrWhiteSpace(x) && int.TryParse(x.Replace(" days", ""), out e))
                        return e;
                    return 0;
                }).ToList();
            });
            
            result.Groups.Insert(0, new ResultDataGroup()
            {
                Key = "Summary",
                Label = "Summary",
                Items = result.Groups.Select(g =>
                {
                    ResultData rd = new ResultData();
                    rd["Diagnosis"] = g.Label;
                    rd["TotalCases"] = g.Items.Count.ToString("N0");
                    List<int> durations = getInts("ActualDuration", g.Items);
                    rd["MinDuration"] = string.Format("{0:N0} days", durations.Min());
                    rd["MaxDuration"] = string.Format("{0:N0} days", durations.Max());
                    rd["AvgDuration"] = string.Format("{0:N2} days", durations.Average());
                    rd["AvgExceeded"] = string.Format("{0:N2} days", getInts("Exceeded", g.Items).Average());
                    return rd;
                }).ToList()
            });
        }
    }
}
