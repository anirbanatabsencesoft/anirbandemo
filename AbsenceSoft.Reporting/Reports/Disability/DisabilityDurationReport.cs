﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Reporting.Reports.Disability
{
    [Serializable]
    public class DisabilityDurationReport : DisabilityReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "4292753B-4786-4F45-9682-3F654161B2A1"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "Duration"; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return false; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 4; } }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            var criteria = base.GetCriteria(user);

            ReportCriteriaItem item = new ReportCriteriaItem();
            item.Name = "ExceedsDurationBy";
            item.Prompt = "Exceeds Duration By";
            item.Required = false;
            item.ControlType = ControlType.Numeric;
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Insert(1, item);

            return criteria;
        }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void RunReport(User user, ReportResult result)
        {
            var cases = RunQuery(user, result.Criteria);
            var minExceedsDurationByCriteria = (result.Criteria.Filters ?? new List<ReportCriteriaItem>(0)).FirstOrDefault(c => c.Name == "ExceedsDurationBy");
            int? minExceedsDurationBy = minExceedsDurationByCriteria == null ? null : minExceedsDurationByCriteria.ValueAs<int?>();
            result.Items = cases.Select(c => GetResultData(user, c, minExceedsDurationBy)).Where(c => c != null).ToList();
        }

        /// <summary>
        /// Gets the result data.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="c">The c.</param>
        /// <returns></returns>
        protected virtual ResultData GetResultData(User user, Case c, int? minExceedsDurationBy = null)
        {
            if (!c.IsSTD || c.Disability == null || c.Disability.PrimaryDiagnosis == null || c.Summary == null || c.Summary.Policies == null || !c.Summary.Policies.Any())
                return null;

            dynamic data = new ResultData();

            // Standard stuff here
            if (user.Customer.HasFeature(Feature.MultiEmployerAccess) && user.Employers.Count > 1)
                data.Employer = c.EmployerName;
            data.EmployeeID = string.IsNullOrWhiteSpace(c.Employee.EmployeeNumber) ? null : new LinkItem() { Value = c.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", c.Employee.Id) };
            data.Employee = string.IsNullOrWhiteSpace(c.Employee.FullName) ? null : new LinkItem() { Value = c.Employee.FullName, Link = string.Format("/Employees/{0}/View", c.Employee.Id) };
            data.CaseNumber = string.IsNullOrWhiteSpace(c.CaseNumber) ? null : new LinkItem() { Value = c.CaseNumber, Link = string.Format("/Cases/{0}/View", c.Id) };

            // Get all approved STD policies
            //  if at any point we don't have any data to use/show, then bail and return null
            // Get all of the STD policy IDs on the case so that we can grab the summary info for them
            var codes = c.Segments.SelectMany(s => s.AppliedPolicies.Where(p => p.Policy.PolicyType == PolicyType.STD)).Select(p => p.Policy.Code).Distinct().ToList();
            if (!codes.Any())
                return null;
            // Get the policy summary for all of the STD policies on the case
            var policies = c.Summary.Policies.Where(p => codes.Contains(p.PolicyCode)).ToList();
            if (!policies.Any())
                return null;
            // Get a merged collection of all policy detail where the status is approved
            var detail = policies.SelectMany(p => p.Detail.Where(d => d.Determination == AdjudicationSummaryStatus.Approved)).ToList();
            if (!detail.Any())
                return null;

            // Get the absolutely first approved STD date as our start date
            DateTime stdStartDate = detail.Min(d => d.StartDate);
            // Set the start date, end date, condition and description (standard STD stuff)
            data.StartDate = stdStartDate.ToUIString();
            data.EndDate = detail.Max(d => d.EndDate).ToUIString();
            data.Condition = c.Disability.PrimaryDiagnosis.Code ?? "";
            data.Description = c.Disability.PrimaryDiagnosis.Description ?? "";

            // We need to know what the expected duration in days was for our diagnosis
            int? diagDur = null;
            // First, determine if we even have a max duration value for this diagnosis code, if not, ignore it
            if (c.Disability.PrimaryPathMaxDays.HasValue)
            {
                // Get the total number of days by getting the current max duration + type and doing a unit conversion to days
                diagDur = c.Disability.PrimaryPathMaxDays.Value;
                data.ExpectedDuration = string.Format("{0:N0} days", diagDur);
            }
            else
                data.ExpectedDuration = "";

            // Get the actual duration of all approved STD policies' approval periods only, factoring overlap and gaps to get an accurate range of days
            //  If the leave is perpetual, then it will be zero
            int duration = Convert.ToInt32(Math.Ceiling(Date.GetCumulativeTimeframeMinusGapsAndOverlap(detail.Select(d => new DateRange(d.StartDate, d.EndDate)).ToArray()).TotalDays));
            data.ActualDuration = string.Format("{0:N0} days", duration);

            // Determine if we've exceeded the max duration and if so, note it, otherwise set this to zero
            int diff = 0;
            if (diagDur.HasValue)
            {
                diff = duration - diagDur.Value;
                if (diff <= 0)
                    data.Exceeded = "0 days";
                else
                    data.Exceeded = string.Format("{0:N0} days", diff);
            }
            else
                data.Exceeded = "";

            // Whoops, if they said AT LEAST this many days exceeding and we don't meet
            //  that criteria, this late binding criteria will filter out the result and we
            //  need to bail and return zero.
            if (minExceedsDurationBy.HasValue && minExceedsDurationBy.Value > diff)
                return null;

            // Calculate our expected end date based on the approved start date + total number of days
            //  from the max duration, if provided, otherwise blank
            if (diagDur.HasValue)
                data.ExpectedEndDate = stdStartDate.AddDays(diagDur.Value).ToUIString();
            else
                data.ExpectedEndDate = "";

            // More typical stuff
            data.Location = c.Employee.Info.OfficeLocation ?? "";
            data.State = c.Employee.WorkState ?? "";

            return data;
        }
    }
}
