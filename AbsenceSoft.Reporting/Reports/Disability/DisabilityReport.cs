﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Reporting.Reports.Disability
{
    [Serializable]
    public abstract class DisabilityReport : BaseReport
    {
        /// <summary>
        /// When overridden in a derived class, gets the name of the report main category to bifurcate reports.
        /// </summary>
        public override string MainCategory { get { return "Operations Report"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report category for display.
        /// </summary>
        public override string Category { get { return "Disability Report"; } }

        /// <summary>
        /// When overridden in a derived class, gets name of the icon image to set in front end
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_status-report.png"; } }

        /// <summary>
        /// Property to configure chart related options
        /// </summary>
        public override ReportChart Chart { get { return null; } }

        /// <summary>
        /// Gets a value indicating whether this instance is exportable to CSV.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is exportable to CSV; otherwise, <c>false</c>.
        /// </value>
        public override bool IsExportableToCsv { get { return true; } }

        /// <summary>
        /// Gets a value indicating whether this instance is exportable to PDF.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is exportable to PDF; otherwise, <c>false</c>.
        /// </value>
        public override bool IsExportableToPdf { get { return true; } }

        /// <summary>
        /// Gets a value indicating whether this instance is viewable.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is viewable; otherwise, <c>false</c>.
        /// </value>
        public override bool IsViewable { get { return true; } }

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether to show the auto
        /// generated total counts at the bottom of the group page.
        /// </summary>
        /// <value>
        ///   <c>true</c> if group auto totals; otherwise, <c>false</c>.
        /// </value>
        public override bool GroupAutoTotals { get { return false; } }

        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns>LOA Feature</returns>
        public override IEnumerable<Feature> RequiredFeatures()
        {
            yield return Feature.LOA;
            yield return Feature.ShortTermDisability;
            yield return Feature.GuidelinesData;
        }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria();
            criteria.CustomerId = user.CustomerId;
            criteria.StartDate = DateTime.UtcNow.Date.AddDays(-30);
            criteria.EndDate = DateTime.UtcNow.Date;

            ReportCriteriaItem item = GetDiagnosisCodeCriteria(user);
            item.OnChangeMethodName = "FilterChangeEvent";
            criteria.Filters.Add(item);

            criteria.Filters.Add(GetWorkStateCriteria(user, criteria.StartDate, criteria.EndDate));
            criteria.Filters.AddRange(GetOrganizationTypeCriteria(user));
            if (IncludeOrganizationGrouping) criteria.Filters.Add(GetOrganizationTypeGroupByCriteria(user));
            return criteria;
        }

        /// <summary>
        /// Runs the query and returns the list of cases that the query applies to.
        /// </summary>
        /// <param name="user">The user to run the query for.</param>
        /// <param name="criteria">The criteria to apply to the query.</param>
        /// <returns>A list of cases found by the query.</returns>
        protected virtual List<Case> RunQuery(User user, ReportCriteria criteria)
        {
            DateTime startDate = criteria.StartDate.ToMidnight();
            DateTime endDate = criteria.EndDate.EndOfDay();

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "Employee._id"));
            else if (!string.IsNullOrWhiteSpace(criteria.CustomerId))
                ands.Add(Case.Query.EQ(e => e.CustomerId, criteria.CustomerId));
            ApplyEmployerFilter<Case>(ands, criteria);

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(Date.BuildDatesOverlapRangeQuery(startDate, endDate));

            // Must have an approved period for an STD policy within the start and end date
            ands.Add(Case.Query.ElemMatch(c =>
                c.Segments, s => s.ElemMatch(e =>
                    e.AppliedPolicies, p => Query.And(
                        p.EQ(o => o.Policy.PolicyType, PolicyType.STD),
                        p.ElemMatch(r => r.Usage, u => Query.And(
                            u.EQ(h => h.Determination, AdjudicationStatus.Approved),
                            Query.And(u.GTE(e => e.DateUsed, new BsonDateTime(startDate)), u.LTE(e => e.DateUsed, new BsonDateTime(endDate)))
                        ))
                    )
                )
            ));

            // Do we have filters outside of the typical? Yes? ok, great!
            if (criteria.Filters.Any())
            {
                // Add the diagnosis code filter if any codes were selected
                var diag = criteria.Filters.FirstOrDefault(m => m.Name == "DiagnosisCode");
                if (diag != null && diag.Values != null && diag.Values.Any())
                {
                    var deez = diag.ValuesAs<string>().Where(d => !string.IsNullOrWhiteSpace(d)).ToList();
                    if (deez.Any())
                        ands.Add(Case.Query.In(e => e.Disability.PrimaryDiagnosis.Code, deez.Distinct()));
                }

                // Add the work state filter if provided
                var ws = criteria.Filters.FirstOrDefault(m => m.Name == "WorkState");
                if (ws != null && ws.Value != null)
                    ands.Add(Case.Query.EQ(e => e.Employee.WorkState, ws.Value));
                
                AppendOrganizationFilterQuery(user, criteria, queries: ands, employeeIdField: "Employee._id");
            }

            // Create the Query and execute the Find against the DB
            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null);
            return query.ToList();
        }
    }
}
