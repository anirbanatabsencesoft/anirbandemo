﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using System;
using System.Linq;

namespace AbsenceSoft.Reporting.Reports.Disability
{
    [Serializable]
    public class DisabilityConditionReport : DisabilityReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "D6DC483F-FD23-4244-8430-89E49ABE2579"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "Conditions"; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return false; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 1; } }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void RunReport(User user, ReportResult result)
        {
            var cases = RunQuery(user, result.Criteria);
            result.Items = cases.Select(c => GetResultData(user, c)).Where(c => c != null).ToList();
        }

        /// <summary>
        /// Gets the result data.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="c">The c.</param>
        /// <returns></returns>
        protected virtual ResultData GetResultData(User user, Case c)
        {
            if (!c.IsSTD || c.Disability == null || c.Disability.PrimaryDiagnosis == null)
                return null;

            dynamic data = new ResultData();

            if (user.Customer.HasFeature(Feature.MultiEmployerAccess) && user.Employers.Count > 1)
                data.Employer = c.EmployerName;
            data.EmployeeID = string.IsNullOrWhiteSpace(c.Employee.EmployeeNumber) ? null : new LinkItem() { Value = c.Employee.EmployeeNumber, Link = string.Format("/Employees/{0}/View", c.Employee.Id) };
            data.Employee = string.IsNullOrWhiteSpace(c.Employee.FullName) ? null : new LinkItem() { Value = c.Employee.FullName, Link = string.Format("/Employees/{0}/View", c.Employee.Id) };
            data.CaseNumber = string.IsNullOrWhiteSpace(c.CaseNumber) ? null : new LinkItem() { Value = c.CaseNumber, Link = string.Format("/Cases/{0}/View", c.Id) };
            data.StartDate = c.StartDate.ToString("MM/dd/yyyy");
            data.EndDate = c.EndDate.ToString("MM/dd/yyyy");
            data.Condition = c.Disability.PrimaryDiagnosis.Code ?? "";
            data.Description = c.Disability.PrimaryDiagnosis.Description ?? "";
            data.Location = c.Employee.Info.OfficeLocation ?? "";
            data.State = c.Employee.WorkState ?? "";

            return data;
        }
    }
}
