﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Reporting.Reports.Disability
{
    [Serializable]
    public class DisabilityDurationReportByOrganization : DisabilityDurationReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "7E87BB74-4BEF-4149-B524-7B0BFA3C6548"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "Duration by Organization"; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 6; } }
        /// <summary>
        /// Include the Organization Grouping Criteria
        /// </summary>
        public override bool IncludeOrganizationGrouping { get { return true; } }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void RunReport(User user, ReportResult result)
        {
            var cases = RunQuery(user, result.Criteria);
            var minExceedsDurationByCriteria = (result.Criteria.Filters ?? new List<ReportCriteriaItem>(0)).FirstOrDefault(c => c.Name == "ExceedsDurationBy");
            int? minExceedsDurationBy = minExceedsDurationByCriteria == null ? null : minExceedsDurationByCriteria.ValueAs<int?>();

            string grouptypeCode = null;
            var customer = user.Customer;
            var isTpa = customer.HasFeature(Feature.MultiEmployerAccess) && user.Employers.Count > 1;
            if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "OrganizationTypeGrouping") != null)
                grouptypeCode = result.Criteria.ValueOf<string>("OrganizationTypeGrouping");
            List<string> customerIds = cases.Select(cs => cs.CustomerId).Distinct().ToList();
            List<string> employerIds = cases.Select(cs => cs.EmployerId).Distinct().ToList();
            List<string> employeeNumbers = cases.Select(cs => cs.Employee.EmployeeNumber).Distinct().ToList();
            List<EmployeeOrganization> employeeOrganizations = null;
            if (grouptypeCode != null)
                employeeOrganizations = EmployeeOrganization.AsQueryable().Where(org => org.TypeCode == grouptypeCode && customerIds.Contains(org.CustomerId) && employerIds.Contains(org.EmployerId) && employeeNumbers.Contains(org.EmployeeNumber)).OrderByDescending(o => o.Dates).ToList();
            else
                employeeOrganizations = EmployeeOrganization.AsQueryable().Where(org => customerIds.Contains(org.CustomerId) && employerIds.Contains(org.EmployerId) && employerIds.Contains(org.EmployerId) && employeeNumbers.Contains(org.EmployeeNumber)).OrderByDescending(o => o.Dates).ToList();
            List<EmployeeOrganization> employeeUniqueOrganizations = new List<EmployeeOrganization>();
            foreach (EmployeeOrganization empOrg in employeeOrganizations)
            {
                if (empOrg.TypeCode != OrganizationType.OfficeLocationTypeCode ||
                    ((empOrg.Dates == null || empOrg.Dates.IncludesToday(true))
                    && (!employeeUniqueOrganizations.Where(empUniqueOrg => empUniqueOrg.TypeCode == OrganizationType.OfficeLocationTypeCode && empUniqueOrg.EmployeeNumber == empOrg.EmployeeNumber && empUniqueOrg.CustomerId == empOrg.CustomerId && empUniqueOrg.EmployerId == empOrg.EmployerId).Any())))
                {
                    employeeUniqueOrganizations.Add(empOrg);
                }
            }
            var groupQuery = employeeUniqueOrganizations.GroupBy(org => new { CustomerId = org.CustomerId, EmployerId = org.EmployerId, Code = org.Code })
           .Select(g => new
           {
               label = employeeUniqueOrganizations.Where(empOrgs => empOrgs.Code == g.Key.Code).FirstOrDefault().Name,
               data = g.Select(x => x).ToList()
           }).OrderBy(m => m.label);

            List<ResultData> resultItems = new List<ResultData>();

            result.Groups = new List<ResultDataGroup>();
            int i = 1;
            //Avoid displaying cases under more than one office location code.
            List<string> empLocationCase = new List<string>();
            foreach (var t in groupQuery)
            {
                resultItems = new List<ResultData>();
                foreach (var org in t.data)
                {
                    foreach (Case td in cases.Where(cs => cs.Employee.EmployeeNumber == org.EmployeeNumber && cs.CustomerId == org.CustomerId && cs.EmployerId == org.EmployerId))
                    {
                        if (!string.IsNullOrWhiteSpace(org.TypeCode) && org.TypeCode.ToUpper().TrimEnd() == OrganizationType.OfficeLocationTypeCode.ToUpper().TrimEnd() && empLocationCase.Contains(td.Id.ToUpper())) continue;
                            ResultData data = GetResultData(user, td, minExceedsDurationBy);
                        if (data != null) resultItems.Add(data);
                        if (!string.IsNullOrWhiteSpace(org.TypeCode) && org.TypeCode.ToUpper().TrimEnd() == OrganizationType.OfficeLocationTypeCode.ToUpper().TrimEnd() && (!empLocationCase.Contains(td.Id.ToUpper()))) empLocationCase.Add(td.Id.ToUpper());
                    }
                }
                if (resultItems != null && resultItems.Count > 0)
                {
                    result.Groups.Add(new ResultDataGroup()
                    {
                        Key = i,
                        Label = t.label,
                        Items = new List<ResultData>().AddRangeFluid(resultItems)
                    });
                    i++;
                }
            }                        
        }
    }
}
