﻿using AbsenceSoft.Data.Security;
using System;
using System.Linq;

namespace AbsenceSoft.Reporting.Reports.Disability
{
    [Serializable]
    public class DisabilityConditionReportByCondition : DisabilityConditionReport
    {
        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public override string Id { get { return "CF04178E-233A-4437-BCA8-C04E520840D4"; } }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public override string Name { get { return "Conditions by Condition"; } }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public override bool IsGrouped { get { return true; } }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public override int CategoryOrder { get { return 2; } }

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether to show the auto
        /// generated total counts at the bottom of the group page.
        /// </summary>
        /// <value>
        ///   <c>true</c> if group auto totals; otherwise, <c>false</c>.
        /// </value>
        public override bool GroupAutoTotals { get { return false; } }

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void RunReport(User user, ReportResult result)
        {
            var cases = RunQuery(user, result.Criteria)
                .Where(c => c.IsSTD
                    && c.Disability != null
                    && c.Disability.PrimaryDiagnosis != null
                    && c.Disability.PrimaryDiagnosis != null)
                .OrderBy(c => c.Disability.PrimaryDiagnosis.Code)
                .GroupBy(c => c.Disability.PrimaryDiagnosis.Code)
                .ToList();

            result.Groups = cases.Select(c =>
            {
                var items = c.Select(a => GetResultData(user, a)).Where(a => a != null).ToList();
                if (!items.Any())
                    return null;
                return new ResultDataGroup()
                {
                    Key = c.Key,
                    Label = c.First().Disability.PrimaryDiagnosis.UIDescription,
                    Items = items
                };
            }).Where(g => g != null).ToList();
        }
    }
}
