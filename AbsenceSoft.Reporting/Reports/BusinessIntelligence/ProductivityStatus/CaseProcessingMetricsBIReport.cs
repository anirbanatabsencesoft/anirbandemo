﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;

namespace AbsenceSoft.Reporting.Reports.BusinessIntelligence.ProductivityStatus
{
    public class CaseProcessingMetricsBIReport : BusinessIntelligenceReport
    {
        /// <summary>
        /// Category for this "Business Intelligence Report" is "Productivity Comparison"
        /// </summary>
        public override string Category { get { return "Productivity Comparison"; } }

        public override string Id { get { return "1DF8F65F-F9FE-409E-8786-0A07F064CB6C"; } }
        public override string Name { get { return "by Case Processing"; } }
        public override bool IsGrouped { get { return false; } }

        /// <summary>
        /// Icon to set for Productivity Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_productivity-comparison.png"; } }

        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns>
        /// An Empty list
        /// </returns>
        public override IEnumerable<Feature> RequiredFeatures()
        {
            return new List<Feature>(0);
        }


        /// <summary>
        /// Chart configurations
        /// </summary>
        public override ReportChart Chart
        {
            get
            {
                return new ReportChart()
                {
                    Type = "ColumnChart",
                    Title = "Case Management Processing Metrics",
                    is3D = false,
                    isStacked = true,
                    Height = 600
                };
            }
        }

        /// <summary>
        /// has any lined detailed report
        /// </summary>
        public override bool HasLinkedReport
        {
            get { return false; }
        }

        /// <summary>
        /// linked report details
        /// </summary>
        public override LinkedReport LinkedReport
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// has sub report
        /// </summary>
        public override bool HasSubReport
        {
            get { return false; }
        }

        /// <summary>
        /// sub report details
        /// </summary>
        public override LinkedReport SubReport
        {
            get { return null; }
        }

        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = base.GetCriteria(user);

            criteria.StartDate = DateTime.Now.GetFirstDayOfMonth();
            criteria.EndDate = DateTime.Now.GetLastDayOfMonth();
            criteria.Filters.RemoveAll(f => f.Name == "ReportType");

            return criteria;
        }

        protected override void RunReport(User user, ReportResult result)
        {
            // check if there is any TodoItem exists
            if (ToDoItem.AsQueryable().Count() <= 0)
                return;

            // Set our series data for the chart
            result.SeriesData = new List<List<object>>();

            // Define our first row, which contains series labels for the x (col 1) and y (col 2~n) axis.
            List<object> labels = new List<object>();
            // Our X-Axis will be Employee
            labels.Add("Month");
            // Our Y-Axis series data will be defined as all available ToDo item types (could be a busy chart)
            labels.AddRange(Enum.GetValues(typeof(ToDoItemType)).OfType<ToDoItemType>().OrderBy(t => (int)t).Select(t => t.ToString().SplitCamelCaseString()));
            // Add our series definition/labels row to the result series data collection.
            result.SeriesData.Add(labels);

            // Define our query
            DateTime startDate = result.Criteria.StartDate.ToMidnight();
            DateTime endDate = result.Criteria.EndDate.EndOfDay();
            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(ToDoItem.Query.IsNotDeleted());
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunBIReport, "EmployeeId"));
            else if (!string.IsNullOrWhiteSpace(result.Criteria.CustomerId))
                ands.Add(ToDoItem.Query.EQ(e => e.CustomerId, result.Criteria.CustomerId));
            ApplyEmployerFilter<ToDoItem>(ands, result.Criteria);

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(ToDoItem.Query.Or(
                ToDoItem.Query.And(ToDoItem.Query.GTE(e => e.CreatedDate, new BsonDateTime(startDate)), ToDoItem.Query.LTE(e => e.CreatedDate, new BsonDateTime(endDate)))
            ));

            IMongoQuery query = ands.Count > 0 ? ToDoItem.Query.And(ands) : null;

            // Define our Map function
            StringBuilder map = new StringBuilder();
            var todoTypes = Enum.GetValues(typeof(ToDoItemType)).OfType<ToDoItemType>().OrderBy(t => (int)t).ToList();
            map.AppendLine("function() {");
            map.AppendLine("    var todo = this;");
            map.AppendLine("    if(isDateWise) {");
            map.AppendLine("        for (var m=0; m<groupBy.length; m++) {");
            map.AppendLine("            var ret = [groupBy[m]];");
            map.AppendLine("            var dt = new Date(groupBy[m]);");
            map.AppendLine("            for (var i=0; i<types.length; i++) {");
            map.AppendLine("                if (types[i] === todo.ItemType && todo.cdt.getDate() == dt.getDate() && todo.cdt.getMonth() == dt.getMonth() && todo.cdt.getFullYear() == dt.getFullYear()) {");
            map.AppendLine("                    ret.push(1);");
            map.AppendLine("                } else {");
            map.AppendLine("                    ret.push(0);");
            map.AppendLine("                }");
            map.AppendLine("            }");
            map.AppendLine("            emit(groupBy[m], { val: ret });");
            map.AppendLine("        }");
            map.AppendLine("    }");
            map.AppendLine("    else {");
            map.AppendLine("        for (var m=0; m<groupBy.length; m++) {");
            map.AppendLine("            var mnth = groupBy[m].split(':')[0];");
            map.AppendLine("            var mnthname = groupBy[m].split(':')[1];");
            map.AppendLine("            var yr = groupBy[m].split(':')[2];");
            map.AppendLine("            var group = mnthname + \" '\" + yr.slice(-2);");
            map.AppendLine("            var ret = [group];");
            map.AppendLine("            for (var i=0; i<types.length; i++) {");
            map.AppendLine("                if (types[i] === todo.ItemType && todo.cdt.getMonth() == (mnth - 1) && todo.cdt.getFullYear() == yr) {");
            map.AppendLine("                    ret.push(1);");
            map.AppendLine("                } else {");
            map.AppendLine("                    ret.push(0);");
            map.AppendLine("                }");
            map.AppendLine("            }");
            map.AppendLine("            emit(m, { val: ret });");
            map.AppendLine("        }");
            map.AppendLine("    }");
            map.AppendLine("}");

            // Define our reduce function
            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(key, values) {");
            reduce.AppendLine(" var result = { val: [values[0].val[0]] };");
            reduce.AppendLine(" values.forEach(function(value) {");
            reduce.AppendLine("     for (var i=1; i<value.val.length; i++) {");
            reduce.AppendLine("         result.val[i] = result.val[i] || 0;");
            reduce.AppendLine("         result.val[i] += (value.val[i] || 0);");
            reduce.AppendLine("     }");
            reduce.AppendLine(" });");
            reduce.AppendLine(" return result;");
            reduce.AppendLine("}");

            // Define scope document for map reduce
            string[] groupBy = createGroupBy(result);

            Dictionary<string, object> scopeDoc = new Dictionary<string, object>();
            scopeDoc.Add("types", todoTypes.Select(t => (int)t).OrderBy(t => t).ToArray());
            scopeDoc.Add("groupBy", groupBy);
            scopeDoc.Add("isDateWise", IsDateWiseComparison(result));

            // Define our map reduce arguments
            MapReduceArgs mapReduce = new MapReduceArgs();
            mapReduce.OutputMode = MapReduceOutputMode.Inline;
            mapReduce.Query = query;
            mapReduce.Scope = new ScopeDocument(scopeDoc);
            //new ScopeDocument(new Dictionary<string, object>() { { "types", todoTypes.Select(t => (int)t).OrderBy(t => t).ToArray() } });
            mapReduce.MapFunction = new BsonJavaScript(map.ToString());
            mapReduce.ReduceFunction = new BsonJavaScript(reduce.ToString());

            // Execute our map reduce query operation against the ToDoItem collection
            var response = ToDoItem.Repository.Collection.MapReduce(mapReduce);
            // If not OK or we have an error, throw an exception
            if (!response.Ok || !string.IsNullOrWhiteSpace(response.ErrorMessage))
                result.Success = false;
            else
            {
                // Loop through our inline results and pump out the series data results to the series data collection
                //  on the result object
                foreach (var r in response.InlineResults)
                {
                    object[] rData = r.GetValue("value").AsBsonDocument.GetValue("val").AsBsonArray.Select(b => b.GetRawValue<object>()).ToArray();
                    if (rData != null)
                        result.SeriesData.Add(rData.ToList());
                }
            }
        }

        private bool IsDateWiseComparison(ReportResult result)
        {
            ReportCriteriaItem durationCriteria = result.Criteria.Filters.Where(m => m.Name == "Duration").FirstOrDefault();

            if (durationCriteria != null)
            {
                string duraction = durationCriteria.Value != null ? durationCriteria.Value.ToString() : "Monthly";
                switch (duraction)
                {
                    case "Monthly":
                    case "DateRange":
                        return true;

                    case "Quarterly":
                        return false;

                    case "Yearly":
                        return false;

                    case "Rolling":
                        return false;
                }
            }

            return false;
        }

        private string[] createGroupBy(ReportResult result)
        {
            ReportCriteriaItem durationCriteria = result.Criteria.Filters.Where(m => m.Name == "Duration").FirstOrDefault();
            string[] groupby = new string[] { };
            int i;
            DateTime nxtDate;

            if (durationCriteria != null)
            {
                var duration = durationCriteria.Value != null ? durationCriteria.Value.ToString() : "Monthly";
                switch (duration)
                {
                    case "Monthly":
                        int daysInMonth = DateTime.DaysInMonth(result.Criteria.StartDate.Year, result.Criteria.StartDate.Month);
                        groupby = new string[daysInMonth];

                        i = 0;
                        nxtDate = result.Criteria.StartDate;
                        do
                        {
                            groupby[i] = nxtDate.ToString("MM/dd/yyy");
                            i++;
                            if (i >= daysInMonth)
                                break;

                            nxtDate = nxtDate.AddDays(1);
                        } while (nxtDate <= result.Criteria.EndDate);

                        break;
                    case "DateRange":
                        int daysInRange = Convert.ToInt32(Math.Round(result.Criteria.EndDate.Subtract(result.Criteria.StartDate).TotalDays));
                        groupby = new string[daysInRange];

                        i = 0;
                        nxtDate = result.Criteria.StartDate;
                        do
                        {
                            groupby[i] = nxtDate.ToString("MM/dd/yyy");
                            i++;
                            if (i >= daysInRange)
                                break;

                            nxtDate = nxtDate.AddDays(1);
                        } while (nxtDate <= result.Criteria.EndDate);

                        break;

                    case "Quarterly":
                        groupby = new string[3];
                        i = 0;
                        nxtDate = result.Criteria.StartDate;
                        do
                        {
                            groupby[i] = nxtDate.Month.ToString() + ":" + nxtDate.ToString("MMM") + ":" + nxtDate.Year.ToString();
                            i++;
                            nxtDate = nxtDate.AddMonths(1);
                        } while (nxtDate <= result.Criteria.EndDate);

                        break;

                    case "Yearly":
                        //int diff = ((result.Criteria.EndDate.Year - result.Criteria.StartDate.Year) * 12) + result.Criteria.EndDate.Month - result.Criteria.StartDate.Month;
                        groupby = new string[12];
                        i = 0;
                        nxtDate = result.Criteria.StartDate;
                        do
                        {
                            groupby[i] = nxtDate.Month.ToString() + ":" + nxtDate.ToString("MMM") + ":" + nxtDate.Year.ToString();
                            i++;
                            if (i >= groupby.Length)
                                break;
                            nxtDate = nxtDate.AddMonths(1);
                        } while (nxtDate <= result.Criteria.EndDate);

                        break;

                    case "Rolling":
                        int diff = ((result.Criteria.EndDate.Year - result.Criteria.StartDate.Year) * 12) + result.Criteria.EndDate.Month - result.Criteria.StartDate.Month;
                        groupby = new string[diff];
                        i = 0;
                        nxtDate = result.Criteria.StartDate;
                        do
                        {
                            groupby[i] = nxtDate.Month.ToString() + ":" + nxtDate.ToString("MMM") + ":" + nxtDate.Year.ToString();
                            i++;
                            if (i >= groupby.Length)
                                break;
                            nxtDate = nxtDate.AddMonths(1);
                        } while (nxtDate < result.Criteria.EndDate);

                        break;
                }
            }

            return groupby;
        }
    }
}
