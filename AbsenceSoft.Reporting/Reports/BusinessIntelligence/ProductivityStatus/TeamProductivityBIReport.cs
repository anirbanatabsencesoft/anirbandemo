﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;

namespace AbsenceSoft.Reporting.Reports.BusinessIntelligence.ProductivityStatus
{
    public class TeamProductivityBIReport : BusinessIntelligenceReport
    {
        /// <summary>
        /// Category for this "Business Intelligence Report" is "Productivity Comparison"
        /// </summary>
        public override string Category { get { return "Productivity Comparison"; } }

        public override string Id { get { return "C29B0D37-B62D-418A-831B-1AB5BDA5F7A9"; } }
        public override string Name { get { return "by Team"; } }
        public override bool IsGrouped { get { return false; } }

        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns>
        /// An Empty list
        /// </returns>
        public override IEnumerable<Feature> RequiredFeatures()
        {
            return new List<Feature>(0);
        }


        /// <summary>
        /// Icon to set for Productivity Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_productivity-comparison.png"; } }

        /// <summary>
        /// Chart configurations
        /// </summary>
        public override ReportChart Chart
        {
            get
            {
                return new ReportChart()
                {
                    Type = "ColumnChart",
                    Title = "Team Productivity Report",
                    is3D = false,
                    isStacked = true,
                    Height = 600
                };
            }
        }

        /// <summary>
        /// has any lined detailed report
        /// </summary>
        public override bool HasLinkedReport
        {
            get { return false; }
        }

        /// <summary>
        /// linked report details
        /// </summary>
        public override LinkedReport LinkedReport
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// has sub report
        /// </summary>
        public override bool HasSubReport
        {
            get { return false; }
        }

        /// <summary>
        /// sub report details
        /// </summary>
        public override LinkedReport SubReport
        {
            get { return null; }
        }

        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = base.GetCriteria(user);

            criteria.Filters.RemoveAll(f => f.Name == "ReportType");

            return criteria;
        }

        protected override void RunReport(User user, ReportResult result)
        {
            // check if there is any TodoItem exists
            if (ToDoItem.AsQueryable(user).Count() <= 0)
                return;

            // This will create a Map-Reduce query that looks exactly like this:
            // db.runCommand({
            //     mapReduce: 'ToDoItem',
            //     out: { inline: 1 }, 
            //     scope: { types: [0,1,2,3,4,5,6,7,8,9,10,11,12,13] },
            //     map: function() {
            //         var todo = this;
            //         var ret = [todo.AssignedToName];
            //         for (var i=0; i<types.length; i++) {
            //             if (types[i] === todo.ItemType) {
            //                 ret.push(1);
            //             } else {
            //                 ret.push(0);
            //             }
            //         }
            //         emit(todo.AssignedToId, { val: ret });
            //     },
            //     reduce: function(key, values) {
            //      var result = { val: [values[0].val[0]] };
            //      values.forEach(function(value) {
            //          for (var i=1; i<value.val.length; i++) {
            //              result.val[i] = result.val[i] || 0;
            //              result.val[i] += (value.val[i] || 0);
            //          }
            //      });
            //      return result;
            //     }    
            // })
            //
            // And return a result set that looks like this:
            // { results: [
            //  { val: ["Developer", 50, 10, 0, 1, 4, 7, 12, 0, 2, 1, 2, 0, 0, 3] },
            //  { val: ["Demo", 10, 2, 0, 1, 4, 7, 12, 0, 2, 1, 2, 0, 0, 3] },
            //  { val: ["Chad", 16, 5, 0, 1, 4, 7, 12, 0, 2, 1, 2, 0, 0, 3] }
            // ] }

            // Set our series data for the chart
            result.SeriesData = new List<List<object>>();

            // Define our first row, which contains series labels for the x (col 1) and y (col 2~n) axis.
            List<object> labels = new List<object>();
            // Our X-Axis will be Employee
            labels.Add("Employee");
            // Our Y-Axis series data will be defined as all available ToDo item types (could be a busy chart)
            labels.AddRange(Enum.GetValues(typeof(ToDoItemType)).OfType<ToDoItemType>().OrderBy(t => (int)t).Select(t => t.ToString().SplitCamelCaseString()));
            // Add our series definition/labels row to the result series data collection.
            result.SeriesData.Add(labels);

            // Define our query
            DateTime startDate = result.Criteria.StartDate.ToMidnight();
            DateTime endDate = result.Criteria.EndDate.EndOfDay();
            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(ToDoItem.Query.IsNotDeleted());
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunBIReport, "EmployeeId"));
            else if (!string.IsNullOrWhiteSpace(result.Criteria.CustomerId))
                ands.Add(ToDoItem.Query.EQ(e => e.CustomerId, result.Criteria.CustomerId));
            ApplyEmployerFilter<ToDoItem>(ands, result.Criteria);
            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(ToDoItem.Query.Or(
                ToDoItem.Query.And(ToDoItem.Query.GTE(e => e.CreatedDate, new BsonDateTime(startDate)), ToDoItem.Query.LTE(e => e.CreatedDate, new BsonDateTime(endDate)))
            ));
            IMongoQuery query = ands.Count > 0 ? ToDoItem.Query.And(ands) : null;

            // Define our Map function
            StringBuilder map = new StringBuilder();
            var todoTypes = Enum.GetValues(typeof(ToDoItemType)).OfType<ToDoItemType>().OrderBy(t => (int)t).ToList();
            map.AppendLine("function() {");
            map.AppendLine("    var todo = this;");
            map.AppendLine("    var ret = [todo.AssignedToName];");
            map.AppendLine("    for (var i=0; i<types.length; i++) {");
            map.AppendLine("        if (types[i] === todo.ItemType) {");
            map.AppendLine("            ret.push(1);");
            map.AppendLine("        } else {");
            map.AppendLine("            ret.push(0);");
            map.AppendLine("        }");
            map.AppendLine("    }");
            map.AppendLine("    emit(todo.AssignedToId, { val: ret });");
            map.AppendLine("}");

            // Define our reduce function
            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(key, values) {");
            reduce.AppendLine(" var result = { val: [values[0].val[0]] };");
            reduce.AppendLine(" values.forEach(function(value) {");
            reduce.AppendLine("     for (var i=1; i<value.val.length; i++) {");
            reduce.AppendLine("         result.val[i] = result.val[i] || 0;");
            reduce.AppendLine("         result.val[i] += (value.val[i] || 0);");
            reduce.AppendLine("     }");
            reduce.AppendLine(" });");
            reduce.AppendLine(" return result;");
            reduce.AppendLine("}");

            // Define our map reduce arguments
            MapReduceArgs mapReduce = new MapReduceArgs();
            mapReduce.OutputMode = MapReduceOutputMode.Inline;
            mapReduce.Query = query;
            mapReduce.Scope = new ScopeDocument(new Dictionary<string, object>() { { "types", todoTypes.Select(t => (int)t).OrderBy(t => t).ToArray() } });
            mapReduce.MapFunction = new BsonJavaScript(map.ToString());
            mapReduce.ReduceFunction = new BsonJavaScript(reduce.ToString());

            // Execute our map reduce query operation against the ToDoItem collection
            var response = ToDoItem.Repository.Collection.MapReduce(mapReduce);
            // If not OK or we have an error, throw an exception
            if (!response.Ok || !string.IsNullOrWhiteSpace(response.ErrorMessage))
                result.Success = false;
            else
            {
                // Loop through our inline results and pump out the series data results to the series data collection
                //  on the result object
                foreach (var r in response.InlineResults)
                {
                    object[] rData = r.GetValue("value").AsBsonDocument.GetValue("val").AsBsonArray.Select(b => b.GetRawValue<object>()).ToArray();
                    if (rData != null)
                        result.SeriesData.Add(rData.ToList());
                }
            }
        }
    }
}
