﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;

namespace AbsenceSoft.Reporting.Reports.BusinessIntelligence.ProductivityStatus
{
    public class TeamComplianceBIReport : BusinessIntelligenceReport
    {
        /// <summary>
        /// Category for this "Business Intelligence Report" is "Productivity Comparison"
        /// </summary>
        public override string Category { get { return "Productivity Comparison"; } }

        public override string Id { get { return "2E96F17E-8AFE-4E1D-8D39-B39901383FE1"; } }
        public override string Name { get { return "by Team Compliance"; } }
        public override bool IsGrouped { get { return false; } }

        /// <summary>
        /// Icon to set for Productivity Report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_productivity-comparison.png"; } }

        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns>
        /// An Empty list
        /// </returns>
        public override IEnumerable<Feature> RequiredFeatures()
        {
            return new List<Feature>(0);
        }


        /// <summary>
        /// Chart configurations
        /// </summary>
        public override ReportChart Chart
        {
            get
            {
                return new ReportChart()
                {
                    Type = "ColumnChart",
                    Title = "Case Manager Compliance Report",
                    is3D = false,
                    isStacked = false,
                    Height = 600
                };
            }
        }

        /// <summary>
        /// has any lined detailed report
        /// </summary>
        public override bool HasLinkedReport
        {
            get { return false; }
        }

        /// <summary>
        /// linked report details
        /// </summary>
        public override LinkedReport LinkedReport
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// has sub report
        /// </summary>
        public override bool HasSubReport
        {
            get { return false; }
        }

        /// <summary>
        /// sub report details
        /// </summary>
        public override LinkedReport SubReport
        {
            get { return null; }
        }

        public override ReportCriteria GetCriteria(User user)
        {
            // Remove duration criteria as it is may not be important for this chart
            ReportCriteria criteria = base.GetCriteria(user);
            criteria.Filters.RemoveAll(f => f.Name == "Duration");
            criteria.Filters.RemoveAll(f => f.Name == "ReportType");

            return criteria;
        }

        protected override void RunReport(User user, ReportResult result)
        {
            // check if there is any TodoItem exists
            if (ToDoItem.AsQueryable().Count() <= 0)
                return;     

            // Set our series data for the chart
            result.SeriesData = new List<List<object>>();

            // Define our first row, which contains series labels for the x (col 1) and y (col 2~n) axis.
            List<object> labels = new List<object>();
            // Our X-Axis will be Employee
            labels.Add("Users");
            labels.AddRange(Enum.GetValues(typeof(ToDoItemStatus)).OfType<ToDoItemStatus>().Where(t => t.Equals(ToDoItemStatus.Overdue) || t.Equals(ToDoItemStatus.Complete)).OrderBy(t => (int)t).Select(t => t.ToString().SplitCamelCaseString()));

            // labels.Add("Overdue");
            //labels.Add("Completed");
            // Add our series definition/labels row to the result series data collection.
            result.SeriesData.Add(labels);

            // Define our query
            DateTime startDate = result.Criteria.StartDate.ToMidnight();
            DateTime endDate = result.Criteria.EndDate.EndOfDay();
            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(ToDoItem.Query.IsNotDeleted());
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunBIReport, "EmployeeId"));
            else if (!string.IsNullOrWhiteSpace(result.Criteria.CustomerId))
                ands.Add(ToDoItem.Query.EQ(e => e.CustomerId, result.Criteria.CustomerId));
            ands.Add(ToDoItem.Query.Exists(e => e.AssignedToName));
            ands.Add(ToDoItem.Query.NE(e => e.AssignedToName, null));
            ApplyEmployerFilter<ToDoItem>(ands, result.Criteria);

            IMongoQuery query = ands.Count > 0 ? ToDoItem.Query.And(ands) : null;

            var todoStatuses = Enum.GetValues(typeof(ToDoItemStatus)).OfType<ToDoItemStatus>().Where(t => t.Equals(ToDoItemStatus.Overdue) || t.Equals(ToDoItemStatus.Complete)).OrderBy(t => (int)t).ToList();

            StringBuilder map = new StringBuilder();
            map.AppendLine("function() {");
            map.AppendLine("    var todo = this;");
            map.AppendLine("    var ret = [todo.AssignedToName];");
            map.AppendLine("    for (var i=0; i<types.length; i++) {");
            map.AppendLine("        if (types[i] === todo.Status) {");
            map.AppendLine("            ret.push(1);");
            map.AppendLine("        } else {");
            map.AppendLine("            ret.push(0);");
            map.AppendLine("        }");
            map.AppendLine("    }");
            map.AppendLine("    emit(todo.AssignedToId, { val: ret });");
            map.AppendLine("}");

            // Define our reduce function
            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(key, values) {");
            reduce.AppendLine(" var result = { val: [values[0].val[0]] };");
            reduce.AppendLine(" values.forEach(function(value) {");
            reduce.AppendLine("     for (var i=1; i<value.val.length; i++) {");
            reduce.AppendLine("         result.val[i] = result.val[i] || 0;");
            reduce.AppendLine("         result.val[i] += (value.val[i] || 0);");
            reduce.AppendLine("     }");
            reduce.AppendLine(" });");
            reduce.AppendLine(" return result;");
            reduce.AppendLine("}");

            // Define our map reduce arguments
            MapReduceArgs mapReduce = new MapReduceArgs();
            mapReduce.OutputMode = MapReduceOutputMode.Inline;
            mapReduce.Query = query;
            mapReduce.Scope = new ScopeDocument(new Dictionary<string, object>() { { "types", todoStatuses.Select(t => (int)t).OrderBy(t => t).ToArray() } });
            mapReduce.MapFunction = new BsonJavaScript(map.ToString());
            mapReduce.ReduceFunction = new BsonJavaScript(reduce.ToString());

            // Execute our map reduce query operation against the ToDoItem collection
            var response = ToDoItem.Repository.Collection.MapReduce(mapReduce);
            // If not OK or we have an error, throw an exception
            if (!response.Ok || !string.IsNullOrWhiteSpace(response.ErrorMessage))
                result.Success = false;
            else
            {
                // Loop through our inline results and pump out the series data results to the series data collection
                //  on the result object
                foreach (var r in response.InlineResults)
                {
                    object[] rData = r.GetValue("value").AsBsonDocument.GetValue("val").AsBsonArray.Select(b => b.GetRawValue<object>()).ToArray();
                    if (rData != null)
                        result.SeriesData.Add(rData.ToList());
                }
            }
        }
    }
}
