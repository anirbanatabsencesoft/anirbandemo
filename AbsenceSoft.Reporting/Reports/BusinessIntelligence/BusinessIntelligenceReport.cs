﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Reporting.Reports.BusinessIntelligence
{
    public abstract class BusinessIntelligenceReport : BaseReport
    {
        /// <summary>
        /// Category for all Absence Status reports is "Business Intelligence".
        /// </summary>
        public override string MainCategory { get { return "Business Intelligence Reports"; } }

        /// <summary>
        /// Category for all Absence Status reports is "Business Intelligence".
        /// </summary>
        public override string Category { get { return "Business Intelligence Report"; } }

        /// <summary>
        /// Icon to set for Business intelligence report
        /// </summary>
        public override string IconImage { get { return "absencesoft-temporary-reports-icons_intelligence-report.png"; } }

        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns>LOA Feature</returns>
        public override IEnumerable<Feature> RequiredFeatures()
        {
            yield return Feature.LOA;
        }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>
        /// A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.
        /// </returns>
        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = new ReportCriteria();
            criteria.CustomerId = user.CustomerId;
            criteria.Filters.Add(GetProductivityCriteria());
            criteria.Filters.Add(GetReportTypeCriteria());
            return criteria;
        }
    }
}
