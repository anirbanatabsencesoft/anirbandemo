﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Logic.Cases;

namespace AbsenceSoft.Reporting.Reports.BusinessIntelligence.AbsenceStatus
{
    [Serializable]
    public sealed class BIReportByAccomDeterminationComparison : AccommodationReport
    {
        public override string Id { get { return "DC01E524-0B4D-4218-9656-3BF715C08397"; } }
        public override string Name { get { return "by Accommodation Decision"; } }
        public override bool IsGrouped { get { return false; } }

        /// <summary>
        /// Chart configurations
        /// </summary>
        public override ReportChart Chart
        {
            get
            {
                return new ReportChart()
                {
                    Type = "ColumnChart",
                    Title = "Accommodation Decision Report",
                    is3D = false,
                    isStacked = false,
                    Height = 600
                };
            }
        }

        /// <summary>
        /// has any lined detailed report
        /// </summary>
        public override bool HasLinkedReport
        {
            get { return false; }
        }

        /// <summary>
        /// has any sub report
        /// </summary>
        public override bool HasSubReport
        {
            get { return false; }
        }
        
        /// <summary>
        /// linked report details
        /// </summary>
        public override LinkedReport LinkedReport
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// sub report details
        /// </summary>
        public override LinkedReport SubReport
        {
            get { return null; }
        }

        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = base.GetCriteria(user);
            criteria.Filters.RemoveAll(f => f.Name == "Gender");
            return criteria;
        }

        protected override void RunReport(User user, ReportResult result)
        {
            // check if there is any case exists
            if (Case.AsQueryable(user).Count() <= 0)
                return;

            // Set our series data for the chart
            result.SeriesData = new List<List<object>>();

            // Define our first row, which contains series labels for the x (col 1) and y (col 2~n) axis.
            List<object> labels = new List<object>();

            labels.Add("AccommodationReq");
            
            // Our Y-Axis series data will be defined as all available ToDo item types (could be a busy chart)
            labels.AddRange(Enum.GetValues(typeof(AdjudicationStatus)).OfType<AdjudicationStatus>().OrderBy(t => (int)t).Select(t => t.ToString().SplitCamelCaseString()));

            // Add our series definition/labels row to the result series data collection.
            result.SeriesData.Add(labels);

            // var query = RunQuery(result.Criteria);
            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(Case.Query.IsNotDeleted());
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunBIReport, "Employee._id"));
            else if (!string.IsNullOrWhiteSpace(result.Criteria.CustomerId))
                ands.Add(Case.Query.EQ(e => e.CustomerId, result.Criteria.CustomerId));
            ApplyEmployerFilter<Case>(ands, result.Criteria);

            // Only those case should be get considered which has Accommodation Request
            ands.Add(Case.Query.EQ(e => e.IsAccommodation, true));

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            DateTime startDate = result.Criteria.StartDate.ToMidnight();
            DateTime endDate = result.Criteria.EndDate.EndOfDay();

            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
            ));

            if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "WorkState") != null)
            {
                if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value != null)
                    ands.Add(Case.Query.EQ(e => e.Employee.WorkState, result.Criteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value));
            }

            var locationFilter = result.Criteria.Filters.FirstOrDefault(m => m.Name == "Location");
            if (locationFilter != null && locationFilter.Values != null && locationFilter.Values.Any())
            {
                var val = locationFilter.ValuesAs<string>();
                if (val != null && val.Any())
                    ands.Add(Case.Query.In(e => e.Employee.Info.OfficeLocation, val));
            }

            if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "AccomType") != null)
            {
                if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "AccomType").Value != null)
                {
                    AccommodationType enumAccomTypeFilter = result.Criteria.ValueOf<AccommodationType>("AccomType");
                    ands.Add(Case.Query.ElemMatch(c => c.AccommodationRequest.Accommodations, q => q.EQ(s => s.Type, enumAccomTypeFilter)));
                }
            }

            IMongoQuery query = ands.Count > 0 ? Case.Query.And(ands) : null;

            // Define our Map function
            // Define our Map function
            StringBuilder map = new StringBuilder();
            map.AppendLine("function() {");
            map.AppendLine("    var c = this;");
            map.AppendLine("    if (c.Accom != null && c.Accom.Accommodations != null) {");
            map.AppendLine("    for (var i=0; i<c.Accom.Accommodations.length; i++) {");
            map.AppendLine("        var accom = c.Accom.Accommodations[i];");
            map.AppendLine("        for (var k=0; k<types.length; k++) {");
            map.AppendLine("            if (types[k] === accom.Type.Code) {");
            map.AppendLine("                var ret = [strTypes[k]];");
            map.AppendLine("                for (var j=0; j<adjStatus.length; j++) {");
            map.AppendLine("                    if (adjStatus[j] === accom.Determination) {");
            map.AppendLine("                        ret.push(1);");
            map.AppendLine("                    } else {");
            map.AppendLine("                        ret.push(0);");
            map.AppendLine("                    }");
            map.AppendLine("                }");
            map.AppendLine("            emit(accom.Type.Name, { val: ret });");
            map.AppendLine("            }");            
            map.AppendLine("        }");
            map.AppendLine("    }");
            map.AppendLine("    }");
            map.AppendLine("}");

            // Define our reduce function
            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(key, values) {");
            reduce.AppendLine(" var result = { val: [values[0].val[0]] };");
            reduce.AppendLine(" values.forEach(function(value) {");
            reduce.AppendLine("     for (var i=1; i<value.val.length; i++) {");
            reduce.AppendLine("         result.val[i] = result.val[i] || 0;");
            reduce.AppendLine("         result.val[i] += (value.val[i] || 0);");
            reduce.AppendLine("     }");
            reduce.AppendLine(" });");
            reduce.AppendLine(" return result;");
            reduce.AppendLine("}");

            var adjStatus = Enum.GetValues(typeof(AdjudicationStatus)).OfType<AdjudicationStatus>().OrderBy(t => (int)t).ToList();
			var accommodationTypes = new AccommodationService().Using(s => s.GetAccommodationTypes(result.Criteria.CustomerId, user.Employers[0].EmployerId));
			var strAccommodationTypes = accommodationTypes.Select(t => t.Name);

            Dictionary<String, object> lstScopeObjects = new Dictionary<string, object>();
            lstScopeObjects.Add("types", accommodationTypes.Select(t => t.Code).OrderBy(t => t).ToArray());
            lstScopeObjects.Add("strTypes", strAccommodationTypes.Select(t => t).OrderBy(t => t).ToArray());
            lstScopeObjects.Add("adjStatus", adjStatus.Select(t => t).ToArray());

            // Define our map reduce arguments
            MapReduceArgs mapReduce = new MapReduceArgs();
            mapReduce.OutputMode = MapReduceOutputMode.Inline;
            mapReduce.Query = query;
            mapReduce.Scope = new ScopeDocument(lstScopeObjects);
            mapReduce.MapFunction = new BsonJavaScript(map.ToString());
            mapReduce.ReduceFunction = new BsonJavaScript(reduce.ToString());

            // Execute our map reduce query operation against the ToDoItem collection
            var response = Case.Repository.Collection.MapReduce(mapReduce);
            // If not OK or we have an error, throw an exception
            if (!response.Ok || !string.IsNullOrWhiteSpace(response.ErrorMessage))
                result.Success = false;
            else
            {
                // Loop through our inline results and pump out the series data results to the series data collection
                //  on the result object
                foreach (var r in response.InlineResults)
                {
                    object[] rData = r.GetValue("value").AsBsonDocument.GetValue("val").AsBsonArray.Select(b => b.GetRawValue<object>()).ToArray();
                    //object[] rData = new object[] { r.GetValue("_id"), r.GetValue("value") };
                    if (rData != null)
                        result.SeriesData.Add(rData.ToList());
                }
            }
        }
    }
}

