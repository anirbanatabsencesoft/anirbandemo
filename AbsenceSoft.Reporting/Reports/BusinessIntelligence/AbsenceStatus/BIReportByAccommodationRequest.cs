﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;
using AbsenceSoft.Logic.Cases;

namespace AbsenceSoft.Reporting.Reports.BusinessIntelligence.AbsenceStatus
{
    [Serializable]
    public sealed class BIReportByAccommodationRequest : AccommodationReport
    {
        public override string Id { get { return "A3878C0F-99B8-41B2-AFDE-0BC666CAE2A5"; } }
        public override string Name { get { return "by Accommodation Summary"; } }
        public override bool IsGrouped { get { return false; } }

        /// <summary>
        /// Chart configurations
        /// </summary>
        public override ReportChart Chart
        {
            get
            {
                return new ReportChart()
                {
                    Type = "PieChart",
                    Title = "Accommodation Count",
                    is3D = true,
                    isStacked = false,
                    Height = 400
                };
            }
        }

        /// <summary>
        /// has any lined detailed report
        /// </summary>
        public override bool HasLinkedReport
        {
            get { return false; }
        }

        /// <summary>
        /// has any sub report
        /// </summary>
        public override bool HasSubReport
        {
            get { return true; }
        }

        /// <summary>
        /// linked report details
        /// </summary>
        public override LinkedReport LinkedReport
        {
            get
            {
                return null;
            }
        }


        /// <summary>
        /// sub report details
        /// </summary>
        public override LinkedReport SubReport
        {
            get
            {
                return new LinkedReport()
                {
                    ReportId = "8B8FC23C-C9F3-446D-9A59-C64682D65F07",
                    ReportCategory = "Business Intelligence Sub Report",
                    ReportMainCategory = "Business Intelligence Sub Report"
                };
            }
        }

        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = base.GetCriteria(user);
            criteria.Filters.RemoveAll(f => f.Name == "Gender");
            criteria.Filters.RemoveAll(f => f.Name == "WorkState");
            criteria.Filters.RemoveAll(f => f.Name == "Location");
            return criteria;
        }

        public string FilterCriteria { get { return "AccomType"; } }

        protected override void RunReport(User user, ReportResult result)
        {
            // check if there is any case exists
            if (Case.AsQueryable(user).Count() <= 0)
                return;

            // Set our series data for the chart
            result.SeriesData = new List<List<object>>();

            // Define our first row, which contains series labels for the x (col 1) and y (col 2~n) axis.
            List<object> labels = new List<object>();

            labels.Add("AccommodationReq");
            labels.Add("Count");
            labels.Add("ChartIndex");

            // Our Y-Axis series data will be defined as all available ToDo item types (could be a busy chart)
            //labels.AddRange(Enum.GetValues(typeof(ToDoItemType)).OfType<ToDoItemType>().OrderBy(t => (int)t).Select(t => t.ToString().SplitCamelCaseString()));

            // Add our series definition/labels row to the result series data collection.
            result.SeriesData.Add(labels);

            // var query = RunQuery(result.Criteria);
            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(Case.Query.IsNotDeleted());
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunBIReport, "Employee._id"));
            else if (!string.IsNullOrWhiteSpace(result.Criteria.CustomerId))
                ands.Add(Case.Query.EQ(e => e.CustomerId, result.Criteria.CustomerId));
            ApplyEmployerFilter<Case>(ands, result.Criteria);

            // Only those case should be get considered which has Accommodation Request
            ands.Add(Case.Query.EQ(e => e.IsAccommodation, true));

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            DateTime startDate = result.Criteria.StartDate.ToMidnight();
            DateTime endDate = result.Criteria.EndDate.EndOfDay();

            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
            ));

            if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "WorkState") != null)
            {
                if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value != null)
                    ands.Add(Case.Query.EQ(e => e.Employee.WorkState, result.Criteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value));
            }

            var locationFilter = result.Criteria.Filters.FirstOrDefault(m => m.Name == "Location");
            if (locationFilter != null && locationFilter.Values != null && locationFilter.Values.Any())
            {
                var val = locationFilter.ValuesAs<string>();
                if (val != null && val.Any())
                    ands.Add(Case.Query.In(e => e.Employee.Info.OfficeLocation, val));
            }

            if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "AccomType") != null)
            {
                if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "AccomType").Value != null)
                {
                    AccommodationType enumAccomTypeFilter = result.Criteria.ValueOf<AccommodationType>("AccomType");
                    ands.Add(Case.Query.ElemMatch(c => c.AccommodationRequest.Accommodations, q => q.EQ(s => s.Type, enumAccomTypeFilter)));
                }
            }

            IMongoQuery query = ands.Count > 0 ? Case.Query.And(ands) : null;

            // Define our Map function
            StringBuilder map = new StringBuilder();
            map.AppendLine("function() {");
            map.AppendLine("  if (this.Accom != null) {");
            map.AppendLine("  var acc = this.Accom.Accommodations ;");
            map.AppendLine("    for (var c=0; c<acc.length; c++) {");
            map.AppendLine("        for (var i=0; i<types.length; i++) {");
            map.AppendLine("              if (types[i] == acc[c].Type.Code) {");
            map.AppendLine("                    emit(strTypes[i], 1);");
            map.AppendLine("              }");
            map.AppendLine("        }");
            map.AppendLine("    }");
            map.AppendLine("  }");
            map.AppendLine("}");


            // Define our reduce function
            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(status, count) {");
            reduce.AppendLine(" return Array.sum(count); ");
            reduce.AppendLine("}");

			var accommodationTypes = new AccommodationService().Using(s => s.GetAccommodationTypes(result.Criteria.CustomerId, user.Employers[0].EmployerId));
			var strAccommodationTypes = accommodationTypes.Select(t=>t.Name);

            Dictionary<String, object> lstScopeObjects = new Dictionary<string, object>();
            lstScopeObjects.Add("types", accommodationTypes.Select(t => t.Code).OrderBy(t => t).ToArray());
            lstScopeObjects.Add("strTypes", strAccommodationTypes.Select(t => t).ToArray());

            // Define our map reduce arguments
            MapReduceArgs mapReduce = new MapReduceArgs();
            mapReduce.OutputMode = MapReduceOutputMode.Inline;
            mapReduce.Query = query;
            mapReduce.Scope = new ScopeDocument(lstScopeObjects);
            mapReduce.MapFunction = new BsonJavaScript(map.ToString());
            mapReduce.ReduceFunction = new BsonJavaScript(reduce.ToString());

            // Execute our map reduce query operation against the ToDoItem collection
            var response = Case.Repository.Collection.MapReduce(mapReduce);
            // If not OK or we have an error, throw an exception
            if (!response.Ok || !string.IsNullOrWhiteSpace(response.ErrorMessage))
                result.Success = false;
            else
            {
                // Report Index
                int currIndex = 0;
                if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "CurrReportIndex") != null)
                {
                    if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "CurrReportIndex").Value != null)
                    {
                        currIndex = Convert.ToInt16(result.Criteria.ValueOf<string>("CurrReportIndex"));
                    }
                }

                // Loop through our inline results and pump out the series data results to the series data collection
                //  on the result object
                foreach (var r in response.InlineResults)
                {
                    object[] rData = new object[] { r.GetValue("_id"), r.GetValue("value"), currIndex };
                    if (rData != null)
                        result.SeriesData.Add(rData.ToList());
                }
            }
        }
    }
}
