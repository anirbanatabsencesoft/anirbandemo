﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Reporting.Reports.BusinessIntelligence.AbsenceStatus
{
    [Serializable]
    public sealed class BIReportBySimilarlySituated : AccommodationReport
    {
        public override string Id { get { return "19E8D52B-8DD2-4EEE-8B37-4EA9F86EEF46"; } }
        public override string Name { get { return "of Similarly Situated Employees"; } }
        public override bool IsGrouped { get { return false; } }

        /// <summary>
        /// Chart configurations
        /// </summary>
        public override ReportChart Chart
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// has any lined detailed report
        /// </summary>
        public override bool HasLinkedReport
        {
            get { return false; }
        }

        /// <summary>
        /// has any sub report
        /// </summary>
        public override bool HasSubReport
        {
            get { return false; }
        }

        /// <summary>
        /// linked report details
        /// </summary>
        public override LinkedReport LinkedReport
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// sub report details
        /// </summary>
        public override LinkedReport SubReport
        {
            get { return null; }
        }


        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = base.GetCriteria(user);
            criteria.ShowAgeCriteria = true;
            //criteria.Filters.RemoveAll(f => f.Name == "Duration");
            criteria.Filters.RemoveAll(f => f.Name == "ReportType");           
            return criteria;
        }

        protected override void RunReport(User user, ReportResult result)
        {
            //var query = RunQuery(result.Criteria);
            // check if there is any case exists
            if (Case.AsQueryable(user).Count() <= 0)
                return;

            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(Case.Query.IsNotDeleted());
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunBIReport, "Employee._id"));
            else if (!string.IsNullOrWhiteSpace(result.Criteria.CustomerId))
                ands.Add(Case.Query.EQ(e => e.CustomerId, result.Criteria.CustomerId));
            ApplyEmployerFilter<Case>(ands, result.Criteria);

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            DateTime startDate = result.Criteria.StartDate.ToMidnight();
            DateTime endDate = result.Criteria.EndDate.EndOfDay();

            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
            ));


            if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "WorkState") != null)
            {
                if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value != null)
                    ands.Add(Case.Query.EQ(e => e.Employee.WorkState, result.Criteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value));
            }

            var locationFilter = result.Criteria.Filters.FirstOrDefault(m => m.Name == "Location");
            if (locationFilter != null && locationFilter.Values != null && locationFilter.Values.Any())
            {
                var val = locationFilter.ValuesAs<string>();
                if (val != null && val.Any())
                    ands.Add(Case.Query.In(e => e.Employee.Info.OfficeLocation, val));
            }

            if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "Gender") != null)
            {
                if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "Gender").Value != null)
                {
                    Gender enumGenderFilter = result.Criteria.ValueOf<Gender>("Gender");
                    ands.Add(Case.Query.EQ(e => e.Employee.Gender, enumGenderFilter));
                }
            }

            // Age criteria
            if (result.Criteria.AgeFrom.HasValue)
            {
                ands.Add(Case.Query.GTE(m => (DateTime.Now.Year - m.Employee.DoB.Value.Year), result.Criteria.AgeFrom.Value));
            }

            if (result.Criteria.AgeTo.HasValue)
            {
                ands.Add(Case.Query.LTE(m => (DateTime.Now.Year - m.Employee.DoB.Value.Year), result.Criteria.AgeTo.Value));
            }


            // take case which has accommodation request
            ands.Add(Case.Query.NE(e => e.AccommodationRequest, null));

            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null);

            List<ResultData> resultItems = new List<ResultData>();

            var accommodationTypes = new AccommodationService().Using(s => s.GetAccommodationTypes(result.Criteria.CustomerId, User.Current.Employers[0].EmployerId));

            foreach (var q in query)
            {
                dynamic data = new ResultData();
                var p = data as IDictionary<String, object>;
                p["CaseNumber"] = new LinkItem() { Value = q.CaseNumber, Link = string.Format("/Cases/{0}/View", q.Id) }; //new LinkItem() { Value = q.CaseNumber, Link = string.Format("/Cases/{0}/View", q.Id) }; 
                p["FirstName"] = q.Employee.FirstName;
                p["LastName"] = q.Employee.LastName;
                p["DateOfRequest"] = q.CreatedDate.ToString("MM/dd/yyy");

                foreach (var accomType in accommodationTypes)
                {
                    var cd = accomType.Code;
                    p[cd] = q.AccommodationRequest.Accommodations.Count(m => m.Type.Code == cd).ToString();
                }

                //data.ErgoReq = q.AccommodationRequest.Accommodations.Count(m => m.Type.Code == "ERGONOMICASSESSMENT").ToString();
                //data.EquipSoftwr = q.AccommodationRequest.Accommodations.Count(m => m.Type.Code == "EQUIPMENTORSOFTWARE").ToString();
                //data.SchedChng = q.AccommodationRequest.Accommodations.Count(m => m.Type.Code == "SCHEDULECHANGE").ToString();
                //data.Other = q.AccommodationRequest.Accommodations.Count(m => m.Type.Code == "OTHER").ToString();
                //data.Leave = q.AccommodationRequest.Accommodations.Count(m => m.Type.Code == "LEAVE").ToString();
                p["Determination"] = q.AccommodationRequest.Determination.ToString();
                p["Duration"] = q.AccommodationRequest.SummaryDurationType;
                // data.Resolution = q.AccommodationRequest.Accommodations.Any(m => m.Resolved == true) ? "Resolved" : "Unresolved";
                //data.Cost = q.AccommodationRequest.Accommodations.Sum(m => m.Cost ?? 0m).ToString("C");
                // Current currency symbol
                p["US"] = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol;

                // Set number as currency without currency symbol
                NumberFormatInfo nfi = CultureInfo.CurrentCulture.NumberFormat;
                nfi = (NumberFormatInfo)nfi.Clone();
                nfi.CurrencySymbol = string.Empty;
                //data.Cost = "<span style='text-align: right; display: block;'>" + string.Format(nfi, "{0:c}", q.AccommodationRequest.Accommodations.Sum(m => m.Cost ?? 0m)) + "</span>";
                p["Cost"] = string.Format(nfi, "{0:c}", q.AccommodationRequest.Accommodations.Sum(m => m.Cost ?? 0m));
                
                p["WorkState"] = q.Employee.WorkState ?? string.Empty ;
                p["Location"] = q.Employee.Info.OfficeLocation ?? string.Empty;
                p["JobTitle"] = string.IsNullOrWhiteSpace(q.Employee.JobTitle) ? string.Empty : q.Employee.JobTitle;
                p["Gender"] = q.Employee.Gender.ToString();
                p["Age"] = q.Employee.DoB.HasValue ? (DateTime.Now.Year - q.Employee.DoB.Value.Year).ToString() : string.Empty;

                resultItems.Add(data);
            }

            result.Items = resultItems;
        }

        private List<string> GetPropertyList(DynamicObject item)
        {
            return item.GetDynamicMemberNames().ToList();
        }
    }
}
