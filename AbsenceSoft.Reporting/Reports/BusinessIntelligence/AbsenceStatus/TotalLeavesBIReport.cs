﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;

namespace AbsenceSoft.Reporting.Reports.BusinessIntelligence.ProductivityStatus
{
    public class TotalLeavesBIReport : BusinessIntelligenceReport
    {
        public override string Id { get { return "FE966012-A293-418A-A3F7-8FD853F03ABA"; } }
        public override string Name { get { return "for Total Leaves"; } }
        public override bool IsGrouped { get { return false; } }


        /// <summary>
        /// Chart configurations
        /// </summary>
        public override ReportChart Chart
        {
            get
            {
                return new ReportChart()
                {
                    Type = "LineChart",
                    Title = "Total Leaves",
                    is3D = false,
                    isStacked = true,
                    Height = 600
                };
            }
        }

        /// <summary>
        /// has any lined detailed report
        /// </summary>
        public override bool HasLinkedReport
        {
            get { return false; }
        }

        /// <summary>
        /// linked report details
        /// </summary>
        public override LinkedReport LinkedReport
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// has sub report
        /// </summary>
        public override bool HasSubReport
        {
            get { return false; }
        }

        /// <summary>
        /// sub report details
        /// </summary>
        public override LinkedReport SubReport
        {
            get { return null; }
        }

        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = base.GetCriteria(user);
            return criteria;
        }

        protected override void RunReport(User user, ReportResult result)
        {
            // check if there is any case exists
            if (Case.AsQueryable(user).Count() <= 0)
                return;            

            string reportType = "ByReason";

            if (result.Criteria.Filters.Count != 0)
            {
                if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "ReportType") != null)
                {
                    if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "ReportType").Value != null)
                    {
                        reportType = result.Criteria.Filters.FirstOrDefault(m => m.Name == "ReportType").Value.ToString();
                    }
                }
            }

            if (reportType == "ByReason")
            {
                //Set chart title
                result.Chart.Title = "Total Leaves By Reason";

                RunReportByReason(user, result);
            }
            else
            {
                //Set chart title
                result.Chart.Title = "Total Leaves By Location";

                RunReportByLocation(user, result);
            }
        }

        private void RunReportByReason(User user, ReportResult result)
        {
            // Set our series data for the chart
            result.SeriesData = new List<List<object>>();

            // Define our first row, which contains series labels for the x (col 1) and y (col 2~n) axis.
            List<object> labels = new List<object>();

            labels.Add("Month");
            // Our Y-Axis series data will be defined as Case Absence reason added
            labels.AddRange(Case.AsQueryable().Where(m => m.Reason.Name.Contains("Accommodation").Equals(false)).Select(m => m.Reason.Name).Distinct().ToArray());

            // Add our series definition/labels row to the result series data collection.
            result.SeriesData.Add(labels);

            // Define query
            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(Case.Query.IsNotDeleted());
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunBIReport, "Employee._id"));
            else if (!string.IsNullOrWhiteSpace(result.Criteria.CustomerId))
                ands.Add(Case.Query.EQ(e => e.CustomerId, result.Criteria.CustomerId));
            ApplyEmployerFilter<Case>(ands, result.Criteria);

            DateTime startDate = result.Criteria.StartDate.ToMidnight();
            DateTime endDate = result.Criteria.EndDate.EndOfDay();

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
            ));

            IMongoQuery query = ands.Count > 0 ? Case.Query.And(ands) : null;

            // Define our Map function
            StringBuilder map = new StringBuilder();
            map.AppendLine("function() {");
            map.AppendLine("    if(isDateWise) {");
            map.AppendLine("        for (var m=0; m<groupBy.length; m++) {");
            map.AppendLine("            var ret = [groupBy[m]];");
            map.AppendLine("            var dt = new Date(groupBy[m]);");
            map.AppendLine("            for (var i=0; i<reasons.length; i++) {");
            //map.AppendLine("                if(this.Reason.Name.indexOf('Accommodation') < 0) {");
            map.AppendLine("                    if (reasons[i] === this.Reason.Name && this.StartDate.getDate() == dt.getDate() && this.StartDate.getMonth() == dt.getMonth() && this.StartDate.getFullYear() == dt.getFullYear()) {");
            map.AppendLine("                        ret.push(1);");
            map.AppendLine("                    } else {");
            map.AppendLine("                        ret.push(0);");
            map.AppendLine("                    }");
            //map.AppendLine("                }");
            map.AppendLine("            }");
            map.AppendLine("            emit(groupBy[m], { val: ret });");
            map.AppendLine("        }");
            map.AppendLine("    }");
            map.AppendLine("    else {");
            map.AppendLine("        for (var m=0; m<groupBy.length; m++) {");
            map.AppendLine("            var mnth = groupBy[m].split(':')[0];");
            map.AppendLine("            var mnthname = groupBy[m].split(':')[1];");
            map.AppendLine("            var yr = groupBy[m].split(':')[2];");
            map.AppendLine("            var group = mnthname + \" '\" + yr.slice(-2);");
            map.AppendLine("            var ret = [group];");
            map.AppendLine("            for (var i=0; i<reasons.length; i++) {");
            map.AppendLine("                if(this.Reason.Name.indexOf('Accommodation') < 0) {");
            map.AppendLine("                    if (reasons[i] === this.Reason.Name && this.StartDate.getMonth() == (mnth-1) && this.StartDate.getFullYear() == yr) {");
            map.AppendLine("                        ret.push(1);");
            map.AppendLine("                    } else {");
            map.AppendLine("                        ret.push(0);");
            map.AppendLine("                    }");
            map.AppendLine("                }");
            map.AppendLine("            }");
            map.AppendLine("            emit(m, { val: ret });");
            map.AppendLine("        }");
            map.AppendLine("    }");
            map.AppendLine("}");

            // Define our reduce function
            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(key, values) {");
            reduce.AppendLine(" var result = { val: [values[0].val[0]] };");
            reduce.AppendLine(" values.forEach(function(value) {");
            reduce.AppendLine("     for (var i=1; i<value.val.length; i++) {");
            reduce.AppendLine("         result.val[i] = result.val[i] || 0;");
            reduce.AppendLine("         result.val[i] += (value.val[i] || 0);");
            reduce.AppendLine("     }");
            reduce.AppendLine(" });");
            reduce.AppendLine(" return result;");
            reduce.AppendLine("}");

            // make month year array to group data
            int i = 0;

            List<string> arrMonthYear = new List<string>();
            DateTime nextDate = startDate;
            do
            {
                arrMonthYear.Add(nextDate.Month.ToString() + ":" + nextDate.ToString("MMM") + ":" + nextDate.Year.ToString());
                nextDate = nextDate.AddMonths(1);
                i++;

            } while (nextDate < endDate);

            // Define scope document for map reduce
            string[] groupBy = createGroupBy(result);
            
            Dictionary<string, object> scopeDoc = new Dictionary<string, object>();
            scopeDoc.Add("reasons", Case.AsQueryable().Where(m => m.Reason.Name.Contains("Accommodation").Equals(false)).Select(m => m.Reason.Name).Distinct().ToArray());
            // scopeDoc.Add("monthyear", arrMonthYear.ToArray());
            scopeDoc.Add("groupBy", groupBy);
            scopeDoc.Add("isDateWise", IsDateWiseComparison(result));

            // Define our map reduce arguments
            MapReduceArgs mapReduce = new MapReduceArgs();
            mapReduce.OutputMode = MapReduceOutputMode.Inline;
            mapReduce.Query = query;
            mapReduce.Scope = new ScopeDocument(scopeDoc);
            mapReduce.MapFunction = new BsonJavaScript(map.ToString());
            mapReduce.ReduceFunction = new BsonJavaScript(reduce.ToString());

            // Execute our map reduce query operation against the ToDoItem collection
            var response = Case.Repository.Collection.MapReduce(mapReduce);
            // If not OK or we have an error, throw an exception
            if (!response.Ok || !string.IsNullOrWhiteSpace(response.ErrorMessage))
                result.Success = false;
            else
            {
                // Loop through our inline results and pump out the series data results to the series data collection
                //  on the result object
                foreach (var r in response.InlineResults)
                {
                    object[] rData = r.GetValue("value").AsBsonDocument.GetValue("val").AsBsonArray.Select(b => b.GetRawValue<object>()).ToArray();
                    if (rData != null)
                        result.SeriesData.Add(rData.ToList());
                }
            }

        }

        private void RunReportByLocation(User user, ReportResult result)
        {
            // Set our series data for the chart
            result.SeriesData = new List<List<object>>();

            // Define our first row, which contains series labels for the x (col 1) and y (col 2~n) axis.
            List<object> labels = new List<object>();

            labels.Add("Month");
            // Our Y-Axis series data will be defined as Case Absence reason added
            labels.AddRange(Case.AsQueryable(user).Select(m => m.Employee.WorkState).Distinct().ToArray());

            // Add our series definition/labels row to the result series data collection.
            result.SeriesData.Add(labels);

            // Define query
            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunBIReport, "Employee._id"));
            else if (!string.IsNullOrWhiteSpace(result.Criteria.CustomerId))
                ands.Add(Case.Query.EQ(e => e.CustomerId, result.Criteria.CustomerId));

            DateTime startDate = result.Criteria.StartDate.ToMidnight();
            DateTime endDate = result.Criteria.EndDate.EndOfDay();

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
            ));

            IMongoQuery query = ands.Count > 0 ? Case.Query.And(ands) : null;

            // Define our Map function
            // Define our Map function
            StringBuilder map = new StringBuilder();
            map.AppendLine("function() {");
            map.AppendLine("    if(isDateWise) {");
            map.AppendLine("        for (var m=0; m<groupBy.length; m++) {");
            map.AppendLine("            var ret = [groupBy[m]];");
            map.AppendLine("            var dt = new Date(groupBy[m]);");
            map.AppendLine("            for (var i=0; i<locations.length; i++) {");
            map.AppendLine("                if (locations[i] === this.Employee.WorkState && this.StartDate.getDate() == dt.getDate() && this.StartDate.getMonth() == dt.getMonth() && this.StartDate.getFullYear() == dt.getFullYear()) {");
            map.AppendLine("                    ret.push(1);");
            map.AppendLine("                } else {");
            map.AppendLine("                    ret.push(0);");
            map.AppendLine("                }");
            map.AppendLine("            }");
            map.AppendLine("            emit(groupBy[m], { val: ret });");
            map.AppendLine("        }");
            map.AppendLine("    }");
            map.AppendLine("    else {");
            map.AppendLine("        for (var m=0; m<groupBy.length; m++) {");
            map.AppendLine("            var mnth = groupBy[m].split(':')[0];");
            map.AppendLine("            var mnthname = groupBy[m].split(':')[1];");
            map.AppendLine("            var yr = groupBy[m].split(':')[2];");
            map.AppendLine("            var group = mnthname + \" '\" + yr.slice(-2);");
            map.AppendLine("            var ret = [group];");
            map.AppendLine("            for (var i=0; i<locations.length; i++) {");
            map.AppendLine("                if (locations[i] === this.Employee.WorkState && this.StartDate.getMonth() == (mnth-1) && this.StartDate.getFullYear() == yr) {");
            map.AppendLine("                    ret.push(1);");
            map.AppendLine("                } else {");
            map.AppendLine("                    ret.push(0);");
            map.AppendLine("                }");
            map.AppendLine("            }");
            map.AppendLine("            emit(m, { val: ret });");
            map.AppendLine("        }");
            map.AppendLine("    }");
            map.AppendLine("}");


            // Define our reduce function
            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(key, values) {");
            reduce.AppendLine(" var result = { val: [values[0].val[0]] };");
            reduce.AppendLine(" values.forEach(function(value) {");
            reduce.AppendLine("     for (var i=1; i<value.val.length; i++) {");
            reduce.AppendLine("         result.val[i] = result.val[i] || 0;");
            reduce.AppendLine("         result.val[i] += (value.val[i] || 0);");
            reduce.AppendLine("     }");
            reduce.AppendLine(" });");
            reduce.AppendLine(" return result;");
            reduce.AppendLine("}");


            // make month year array to group data
            int i = 0;

            List<string> arrMonthYear = new List<string>();
            DateTime nextDate = startDate;
            do
            {
                arrMonthYear.Add(nextDate.Month.ToString() + ":" + nextDate.ToString("MMM") + ":" + nextDate.Year.ToString());
                nextDate = nextDate.AddMonths(1);
                i++;

            } while (nextDate < endDate);

            // Define scope document for map reduce
            string[] groupBy = createGroupBy(result);

            Dictionary<string, object> scopeDoc = new Dictionary<string, object>();
            scopeDoc.Add("locations", Case.AsQueryable().Select(m => m.Employee.WorkState).Distinct().ToArray());
            scopeDoc.Add("monthyear", arrMonthYear.ToArray());
            scopeDoc.Add("groupBy", groupBy);
            scopeDoc.Add("isDateWise", IsDateWiseComparison(result));

            // Define our map reduce arguments
            MapReduceArgs mapReduce = new MapReduceArgs();
            mapReduce.OutputMode = MapReduceOutputMode.Inline;
            mapReduce.Query = query;
            mapReduce.Scope = new ScopeDocument(scopeDoc);
            mapReduce.MapFunction = new BsonJavaScript(map.ToString());
            mapReduce.ReduceFunction = new BsonJavaScript(reduce.ToString());

            // Execute our map reduce query operation against the ToDoItem collection
            var response = Case.Repository.Collection.MapReduce(mapReduce);
            // If not OK or we have an error, throw an exception
            if (!response.Ok || !string.IsNullOrWhiteSpace(response.ErrorMessage))
                result.Success = false;
            else
            {
                // Loop through our inline results and pump out the series data results to the series data collection
                //  on the result object
                foreach (var r in response.InlineResults)
                {
                    object[] rData = r.GetValue("value").AsBsonDocument.GetValue("val").AsBsonArray.Select(b => b.GetRawValue<object>()).ToArray();
                    if (rData != null)
                        result.SeriesData.Add(rData.ToList());
                }
            }

        }

        private bool IsDateWiseComparison(ReportResult result)
        {
            ReportCriteriaItem durationCriteria = result.Criteria.Filters.Where(m => m.Name == "Duration").FirstOrDefault();

            if (durationCriteria != null)
            {
                switch (durationCriteria.Value.ToString())
                {
                    case "Monthly":
                    case "DateRange":
                        return true;

                    case "Quarterly":
                        return false;

                    case "Yearly":
                        return false;

                    case "Rolling":
                        return false;
                }
            }

            return false;
        }

        private string[] createGroupBy(ReportResult result)
        {
            ReportCriteriaItem durationCriteria = result.Criteria.Filters.Where(m => m.Name == "Duration").FirstOrDefault();
            string[] groupby = new string[] { };
            int i;
            DateTime nxtDate;

            if (durationCriteria != null)
            {
                switch (durationCriteria.Value.ToString())
                {
                    case "Monthly":
                    case "DateRange":
                        int daysInMonth = DateTime.DaysInMonth(result.Criteria.StartDate.Year, result.Criteria.StartDate.Month);
                        groupby = new string[daysInMonth];

                        i = 0;
                        nxtDate = result.Criteria.StartDate;
                        do
                        {
                            groupby[i] = nxtDate.ToString("MM/dd/yyy");
                            i++;
                            nxtDate = nxtDate.AddDays(1);
                        } while (nxtDate <= result.Criteria.EndDate);

                        break;

                    case "Quarterly":
                        groupby = new string[3];
                        i = 0;
                        nxtDate = result.Criteria.StartDate;
                        do
                        {
                            groupby[i] = nxtDate.Month.ToString() + ":" + nxtDate.ToString("MMM") + ":" + nxtDate.Year.ToString();
                            i++;
                            nxtDate = nxtDate.AddMonths(1);
                        } while (nxtDate <= result.Criteria.EndDate);

                        break;

                    case "Yearly":
                        //int diff = ((result.Criteria.EndDate.Year - result.Criteria.StartDate.Year) * 12) + result.Criteria.EndDate.Month - result.Criteria.StartDate.Month;
                        groupby = new string[12];
                        i = 0;
                        nxtDate = result.Criteria.StartDate;
                        do
                        {
                            groupby[i] = nxtDate.Month.ToString() + ":" + nxtDate.ToString("MMM") + ":" + nxtDate.Year.ToString();
                            i++;
                            nxtDate = nxtDate.AddMonths(1);
                        } while (nxtDate <= result.Criteria.EndDate);

                        break;

                    case "Rolling":
                        int diff = ((result.Criteria.EndDate.Year - result.Criteria.StartDate.Year) * 12) + result.Criteria.EndDate.Month - result.Criteria.StartDate.Month;
                        groupby = new string[diff];
                        i = 0;
                        nxtDate = result.Criteria.StartDate;
                        do
                        {
                            groupby[i] = nxtDate.Month.ToString() + ":" + nxtDate.ToString("MMM") + ":" + nxtDate.Year.ToString();
                            i++;
                            nxtDate = nxtDate.AddMonths(1);
                        } while (nxtDate < result.Criteria.EndDate);

                        break;
                }
            }

            return groupby;
        }
    }

}
