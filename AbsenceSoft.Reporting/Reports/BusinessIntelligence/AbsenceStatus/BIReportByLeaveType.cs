﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;

namespace AbsenceSoft.Reporting.Reports.BusinessIntelligence
{
    [Serializable]
    public sealed class BIReportByLeaveType : BusinessIntelligenceReport
    {
        public override string Id { get { return "117AF888-E65E-44da-A043-61A5896FC456"; } }
        public override string Name { get { return "by Leave Type"; } }
        public override bool IsGrouped { get { return false; } }

        /// <summary>
        /// Chart configurations
        /// </summary>
        public override ReportChart Chart
        {
            get
            {
                return new ReportChart()
                {
                    Type = "PieChart",
                    Title = "",
                    is3D = true,
                    isStacked = false,
                    Height = 400
                };
            }
        }

        /// <summary>
        /// has any lined detailed report
        /// </summary>
        public override bool HasLinkedReport
        {
            get { return true; }
        }

        /// <summary>
        /// linked report details
        /// </summary>
        public override LinkedReport LinkedReport
        {
            get
            {
                return new LinkedReport()
                {
                    ReportId = "117AF888-E65E-44da-A043-61A5896FC316",
                    ReportCategory = "Absence Status Report",
                    ReportMainCategory = "Operations Report"
                };
            }
        }

        /// <summary>
        /// has sub report
        /// </summary>
        public override bool HasSubReport
        {
            get { return true; }
        }

        /// <summary>
        /// sub report details
        /// </summary>
        public override LinkedReport SubReport
        {
            get
            {
                return new LinkedReport()
                {
                    ReportId = "2F39E035-55F2-4652-8292-E4C5721EF112",
                    ReportCategory = "Business Intelligence Sub Report",
                    ReportMainCategory = "Business Intelligence Sub Report"
                };
            }
        }

        public string FilterCriteria { get { return "LeaveType"; } }

        public override ReportCriteria GetCriteria(User user)
        {
            ReportCriteria criteria = base.GetCriteria(user);

            criteria.Filters.RemoveAll(f => f.Name == "Duration");
            criteria.Filters.RemoveAll(f => f.Name == "ReportType");
            return criteria;
        }
        
        protected override void RunReport(User user, ReportResult result)
        {
            // check if there is any case exists
            if (Case.AsQueryable().Count() <= 0)
                return;            

            // Set our series data for the chart
            result.SeriesData = new List<List<object>>();

            // Define our first row, which contains series labels for the x (col 1) and y (col 2~n) axis.
            List<object> labels = new List<object>();

            labels.Add("LeaveType");
            labels.Add("Count");
            labels.Add("ChartIndex");

            // Our Y-Axis series data will be defined as all available ToDo item types (could be a busy chart)
            //labels.AddRange(Enum.GetValues(typeof(ToDoItemType)).OfType<ToDoItemType>().OrderBy(t => (int)t).Select(t => t.ToString().SplitCamelCaseString()));

            // Add our series definition/labels row to the result series data collection.
            result.SeriesData.Add(labels);

            // var query = RunQuery(result.Criteria);
            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(Case.Query.IsNotDeleted());
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunBIReport, "Employee._id"));
            else if (!string.IsNullOrWhiteSpace(result.Criteria.CustomerId))
                ands.Add(Case.Query.EQ(e => e.CustomerId, result.Criteria.CustomerId));
            ApplyEmployerFilter<Case>(ands, result.Criteria);

            IMongoQuery query = ands.Count > 0 ? Case.Query.And(ands) : null;

            // Define our Map function
            StringBuilder map = new StringBuilder();
            map.AppendLine("function() {");
            map.AppendLine("    for (var i=0; i<caseTypes.length; i++) {");
            map.AppendLine("        if (caseTypes[i] === this.Summary.t) {");
            map.AppendLine("              emit(strCaseTypes[i], 1);");
            map.AppendLine("        }");
            map.AppendLine("    }");
            map.AppendLine("}");

            // Define our reduce function
            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(caseType, count) {");
            reduce.AppendLine(" return Array.sum(count); ");
            reduce.AppendLine("}");

            var caseTypes = Enum.GetValues(typeof(CaseType)).OfType<CaseType>().OrderBy(t => (int)t).ToList();
            var strCaseTypes = Enum.GetValues(typeof(CaseType)).OfType<CaseType>().OrderBy(t => (int)t).Select(t => t.ToString().SplitCamelCaseString()).ToList();

            Dictionary<String, object> lstScopeObjects = new Dictionary<string,object>();
            lstScopeObjects.Add("caseTypes", caseTypes.Select(t => (int)t).OrderBy(t => t).ToArray());
            lstScopeObjects.Add("strCaseTypes", strCaseTypes.Select(t => t).ToArray());

            // Define our map reduce arguments
            MapReduceArgs mapReduce = new MapReduceArgs();
            mapReduce.OutputMode = MapReduceOutputMode.Inline;
            mapReduce.Query = query;
            mapReduce.Scope = new ScopeDocument(lstScopeObjects);
            mapReduce.MapFunction = new BsonJavaScript(map.ToString());
            mapReduce.ReduceFunction = new BsonJavaScript(reduce.ToString());

            // Execute our map reduce query operation against the ToDoItem collection
            var response = Case.Repository.Collection.MapReduce(mapReduce);
            // If not OK or we have an error, throw an exception
            if (!response.Ok || !string.IsNullOrWhiteSpace(response.ErrorMessage))
                result.Success = false;
            else
            {
                // Report Index
                int currIndex = 0;
                if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "CurrReportIndex") != null)
                {
                    if (result.Criteria.Filters.FirstOrDefault(m => m.Name == "CurrReportIndex").Value != null)
                    {

                        currIndex = Convert.ToInt16(result.Criteria.ValueOf<string>("CurrReportIndex"));
                    }
                }

                // Loop through our inline results and pump out the series data results to the series data collection
                //  on the result object
                foreach (var r in response.InlineResults)
                {
                    // object[] rData = r.GetValue("value").AsBsonDocument.GetValue("val").AsBsonArray.Select(b => b.GetRawValue<object>()).ToArray();
                    object[] rData = new object[] { r.GetValue("_id"), r.GetValue("value"), currIndex };
                    if (rData != null)
                        result.SeriesData.Add(rData.ToList());
                }
            }

        }
    }
}
