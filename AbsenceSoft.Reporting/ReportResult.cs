﻿using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Reporting
{
    [Serializable]
    public class ReportResult
    {
        /// <summary>
        /// Gets or sets the report name for display.
        /// </summary>
        [BsonIgnoreIfNull]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the report category name for display.
        /// </summary>
        [BsonIgnoreIfNull]
        public string Category { get; set; }


        /// <summary>
        /// Gets or sets the icon name for display.
        /// </summary>
        [BsonIgnoreIfNull]
        public string IconImage { get; set; }

        /// <summary>
        /// Gets or sets the date and time the report was originally requested to be run.
        /// </summary>
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc, Representation = BsonType.DateTime), BsonIgnoreIfDefault]
        public DateTime RequestDate { get; set; }

        /// <summary>
        /// Gets or sets the user that requested the report, also used for data visibility
        /// functions.
        /// </summary>
        [BsonIgnoreIfNull]
        public User RequestedBy { get; set; }

        /// <summary>
        /// Gets or sets the report criteria (original or modified) that was supplied in order to
        /// run the report.
        /// </summary>
        [BsonIgnoreIfNull]
        public ReportCriteria Criteria { get; set; }

        /// <summary>
        /// Gets or sets the total time it took to complete in miliseconds.
        /// </summary>
        [BsonIgnoreIfNull]
        public double? CompletedIn { get; set; }

        /// <summary>
        /// Gets a value whether or not this report request was successfully run.
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Gets or sets an error message for the report run request.
        /// </summary>
        [BsonIgnoreIfNull]
        public string Error { get; set; }

        /// <summary>
        /// Gets or sets the list of result data items in the report results. If this is a grouped
        /// report then this collection will be <c>null</c>.
        /// </summary>
        [BsonIgnoreIfNull]
        public List<ResultData> Items { get; set; }

        /// <summary>
        /// Gets or sets the list of result data item groups in the report results. If this is not
        /// a grouped report, then this collection will be <c>null</c>.
        /// </summary>
        [BsonIgnoreIfNull]
        public List<ResultDataGroup> Groups { get; set; }

        /// <summary>
        /// Gets or set a 2-dimensional array of data items meant to represent series data used for charts
        /// and other data visualizations and summaries.
        /// </summary>
        [BsonIgnoreIfNull]
        public List<List<object>> SeriesData { get; set; }

        /// <summary>
        /// chart related options
        /// </summary>
        [BsonIgnoreIfNull]
        public ReportChart Chart { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to show the auto 
        /// generated total counts at the bottom of the group page.
        /// </summary>
        /// <value>
        ///   <c>true</c> if group auto totals; otherwise, <c>false</c>.
        /// </value>
        public virtual bool GroupAutoTotals { get; set; }
        
    }
}
