﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace AbsenceSoft.Reporting
{
    [Serializable]
    public class LinkItem : BsonDocument
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LinkItem"/> class.
        /// </summary>
        public LinkItem() : base()
        {
            this.Add("Value", new BsonString(""));
            this.Add("Link", new BsonString(""));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LinkItem"/> class.
        /// </summary>
        /// <param name="val">The value.</param>
        /// <param name="link">The link.</param>
        public LinkItem(string val, string link) : base()
        {
            this.Add("Value", new BsonString(val));
            this.Add("Link", new BsonString(link));
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        [BsonElement("Value"), JsonProperty("Value")]
        public BsonString Value
        {
            get { return (BsonString)this.GetElement("Value").Value; }
            set { this.Set("Value", value ?? ""); }
        }
        /// <summary>
        /// Gets or sets the link.
        /// </summary>
        /// <value>
        /// The link.
        /// </value>
        [BsonElement("Link"), JsonProperty("Link")]
        public BsonString Link
        {
            get { return (BsonString)this.GetElement("Link").Value; }
            set { this.Set("Link", value ?? ""); }
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            if (Value != null)
                return Value.ToString();
            return string.Empty;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/>, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (Value == null && obj == null) return true;
            if (obj == null) return false;
            if (Value == null) return false;
            return Value.Equals(obj);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        /// <summary>
        /// Compares to.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns></returns>
        public override int CompareTo(BsonValue other)
        {
            if (other.IsBsonNull) return -1;
            if (other.IsBsonDocument)
            {
                LinkItem item = BsonTypeMapper.MapToDotNetValue(other) as LinkItem;
                if (item == null)
                    return -1;

                if (item.Value == null && this.Value == null)
                    return 0;

                return item.Value.CompareTo(this.Value);
            }

            return -1;
        }
    }
}
