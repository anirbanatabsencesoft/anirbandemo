﻿using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Rendering;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace AbsenceSoft.Reporting
{
    public class ReportService
    {
        /// <summary>
        /// The current customer wtihin the context/scope of the service call.
        /// </summary>
        public Customer Customer { get; set; }

        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        /// <value>
        /// The user.
        /// </value>
        public User User { get; set; }

        /// <summary>
        /// The cache list of all reports which will be returned for all report lists
        /// or inquiries, post-filtered by customer features.
        /// </summary>
        private List<BaseReport> Cache = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportService"/> class.
        /// </summary>
        public ReportService()
        {
            User = User.Current;
            Customer = Customer.Current ?? new Customer();
        }

        /// <summary>
        /// Gets the reports.
        /// </summary>
        /// <returns></returns>
        public List<BaseReport> GetReports()
        {
            var filter = new Func<BaseReport, bool>(r => r.RequiredFeatures().All(f => (Customer ?? new Customer()).HasFeature(f)) && r.IsVisible(this.User));

            if (Cache != null)
            {
                return Cache.Where(filter).ToList();
            }
            var reports = Assembly.GetAssembly(this.GetType()).GetTypes()
                .Where(r => !r.IsAbstract && !string.IsNullOrWhiteSpace(r.Namespace) && r.Namespace.StartsWith("AbsenceSoft.Reporting.Reports") && r.IsSubclassOf(typeof(BaseReport)))
                .ToList();

            Cache = reports.Select(r => Activator.CreateInstance(r) as BaseReport)
                .OrderBy(r => r.Category)
                .ThenBy(r => r.CategoryOrder)
                .ToList();

            return Cache.Where(filter).ToList();
        }

        /// <summary>
        /// Gets all reports.
        /// </summary>
        /// <returns></returns>
        private List<BaseReport> GetAllReports()
        {
            var reports = Assembly.GetAssembly(this.GetType()).GetTypes()
                .Where(r => !r.IsAbstract && !string.IsNullOrWhiteSpace(r.Namespace) && (r.Namespace.StartsWith("AbsenceSoft.Reporting.Reports") || r.Namespace.StartsWith("AbsenceSoft.Reporting.SubReports")) && r.IsSubclassOf(typeof(BaseReport)))
                .ToList();

            return reports.Select(r => Activator.CreateInstance(r) as BaseReport).OrderBy(r => r.Category).ThenBy(r => r.CategoryOrder).ToList();
        }

        /// <summary>
        /// Gets the reports.
        /// </summary>
        /// <param name="category">The category.</param>
        /// <returns></returns>
        public List<BaseReport> GetReports(string category)
        {
            return GetReports().ToList(r => r.Category == category);
        }

        /// <summary>
        /// Gets the reports by main category.
        /// </summary>
        /// <param name="mainCategory">The main category.</param>
        /// <returns></returns>
        public List<BaseReport> GetReportsByMainCategory(string mainCategory)
        {
            return GetReports().ToList(r => r.MainCategory == mainCategory);
        }

        /// <summary>
        /// Gets the report.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public BaseReport GetReport(string id)
        {
            return GetAllReports().FirstOrDefault(r => r.Id.Equals(id, StringComparison.InvariantCultureIgnoreCase));
        }

        /// <summary>
        /// Gets the criteria.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public ReportCriteria GetCriteria(string id, User user)
        {
            var report = GetReport(id);
            return report.BuildEmployerCriteria(user, report.GetCriteria(user));
        }

        /// <summary>
        /// Runs the report.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="criteria">The criteria.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public ReportResult RunReport(string id, ReportCriteria criteria, User user)
        {
            Log.Info("REPORTS: Report {0} requested for user '{1}' with criteria {2}", id, user, criteria.ToJSON());
            var report = GetReport(id);
            MergeCriteria(criteria, report.BuildEmployerCriteria(user, report.GetCriteria(user)));
            return report.RunReport(user, criteria);
        }

        /// <summary>
        /// Generates the PDF.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="criteria">The criteria.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public byte[] GeneratePDF(string id, ReportCriteria criteria, User user)
        {
            Log.Info("REPORTS: Generate PDF {0} requested for user '{1}' with criteria {2}", id, user ?? AbsenceSoft.Data.Security.User.Current, criteria.ToJSON());
            var result = RunReport(id, criteria, AbsenceSoft.Data.Security.User.Current);

            BaseReport report = GetReport(id);
            MergeCriteria(criteria, report.BuildEmployerCriteria(user, report.GetCriteria(user)));

            string displayCriteria = criteria.PlainEnglish;

            StringBuilder strHTML = new StringBuilder();
            strHTML.Append("<table style=\"width:100%; background-color: rgba(0, 0, 0, 0);  margin-bottom: 10px; margin-top: 15px; color: #FF8300; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; text-align: center;\">");
            strHTML.Append("<tr style=\"width:100%; font-size: 14px;  font-weight: bold;\"><td>" + report.Category + " " + report.Name + "</td></tr>");
            strHTML.Append("</table>");

            strHTML.Append("<table style=\"width:100%; background-color: rgba(0, 0, 0, 0);  margin-bottom: 15px; margin-top: 10px; color: #FF8300; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; text-align: center;\">");
            strHTML.Append("<tr style=\"width:100%; font-size: 13px; \"><td>" + displayCriteria + "</td></tr>");
            strHTML.Append("</table>");

            if (result.Items != null && result.Items.Any())
            {
                List<string> tList = new List<string>();
                foreach (var item in result.Items)
                {
                    if (tList.Count <= 0)
                    {
                        tList = GetPropertyList(result.Items);

                        tList = tList.Select(s => s.Replace("Accommodations", "Accom")).ToList();
                        tList = tList.Select(s => s.Replace("EstimatedReturnToWork", "Est RTW")).ToList();
                        tList = tList.Select(s => s.Replace("ReturnToWork", "RTW")).ToList();

                        if (tList.Count > 0)
                        {
                            strHTML.Append("<div style=\"width:100%;  color: #333333; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; margin-top: 15px;\">");
                            strHTML.Append("<table style=\"width:100%; border: 0; border-color: #CCCCCC; border-width: 1px; border-style: solid; background-color: rgba(0, 0, 0, 0);  margin-bottom: 15px; border-collapse: collapse; font-size: 8px;\">");
                            // Header get printed
                            strHTML.Append("<tr>");
                            var isFirst = true;
                            foreach (var col in tList)
                            {
                                if (isFirst)
                                {
                                    isFirst = false;
                                    strHTML.Append("<td style=\"border: 0; border-color: #CCCCCC; border-bottom-width: 1px; border-bottom-style: solid; border-top-left-radius: 5px; line-height: 1.42857; padding: 5px; vertical-align: top; width:20% !important;\"><strong>" + col + "</strong></td>");

                                }
                                else
                                {
                                    strHTML.Append("<td style=\"border: 0; border-color: #CCCCCC; border-bottom-width: 1px; border-bottom-style: solid; line-height: 1.42857; padding: 5px; vertical-align: top; width:10% !important;\"><strong>" + col + "</strong></td>");
                                }
                            }
                            strHTML.Append("</tr>");
                        }
                    }

                    // Data get printed
                    strHTML.Append("<tr>");
                    foreach (var col in tList)
                    {
                        string colName = string.Empty;

                        if (col == "Accom")
                        {
                            colName = "Accommodations";
                        }
                        else if (col == "Est RTW")
                        {
                            colName = "EstimatedReturnToWork";
                        }
                        else if (col == "RTW")
                        {
                            colName = "ReturnToWork";
                        }
                        else
                        {
                            colName = col;
                        }

                        object o = item[colName];
                        if (o == null)
                            strHTML.Append("<td style=\"line-height: 1.42857; padding: 5px; vertical-align: top; width:20% !important;\"></td>");
                        else if (o is DateTime)
                            strHTML.Append("<td style=\"line-height: 1.42857; padding: 5px; vertical-align: top; width:20% !important;\">" + Convert.ToDateTime(o).ToString("MM/dd/yyyy") + "</td>");
                        else
                            strHTML.Append("<td style=\"line-height: 1.42857; padding: 5px; vertical-align: top; width:20% !important;\">" + o.ToString() + "</td>");
                    }
                    strHTML.Append("</tr>");
                }

                strHTML.Append("</table></div>");
            }
            else if (result.Groups != null && result.Groups.Any())
            {
                foreach (var grp in result.Groups)
                {
                    strHTML.Append("<table style=\"width:100%; margin-bottom: 5px; margin-top: 15px; color: #FF8300; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;\">");
                    strHTML.Append("<tr style=\"width:100%; font-size: 12px;  font-weight: bold;\"><td>" + grp.Label + "</td></tr>");
                    strHTML.Append("</table>");

                    if (grp.Items.Count() > 0)
                    {
                        strHTML.Append("<div style=\"width:100%;  color: #333333; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; margin-top: 15px;\">");
                        strHTML.Append("<table style=\"width:100%; border: 0; border-color: #CCCCCC; border-width: 1px; border-style: solid; background-color: rgba(0, 0, 0, 0);  margin-bottom: 15px; border-collapse: collapse; font-size: 10px;\">");
                    }


                    List<string> tList = new List<string>();
                    foreach (var item in grp.Items)
                    {
                        if (tList.Count <= 0)
                        {
                            tList = GetPropertyList(grp.Items);

                            if (tList.Count > 0)
                            {
                                // Header get printed
                                strHTML.Append("<tr>");
                                var isFirst = true;
                                foreach (var col in tList)
                                {
                                    if (isFirst)
                                    {
                                        isFirst = false;
                                        strHTML.Append("<td style=\"border: 0; border-color: #CCCCCC; border-bottom-width: 1px; border-bottom-style: solid; border-top-left-radius: 5px; line-height: 1.42857; padding: 5px; vertical-align: top; width:20% !important;\"><strong>" + col + "</strong></td>");

                                    }
                                    else
                                    {
                                        strHTML.Append("<td style=\"border: 0; border-color: #CCCCCC; border-bottom-width: 1px; border-bottom-style: solid; line-height: 1.42857; padding: 5px; vertical-align: top; width:10% !important;\"><strong>" + col + "</strong></td>");
                                    }
                                }
                                strHTML.Append("</tr>");
                            }
                        }

                        // Data get printed
                        strHTML.Append("<tr>");
                        foreach (var col in tList)
                        {
                            object o = item[col];
                            if (o == null)
                                strHTML.Append("<td style=\"line-height: 1.42857; padding: 5px; vertical-align: top; width:20% !important;\"></td>");
                            else if (o is DateTime)
                                strHTML.Append("<td style=\"line-height: 1.42857; padding: 5px; vertical-align: top; width:20% !important;\">" + Convert.ToDateTime(o).ToString("MM/dd/yyyy") + "</td>");
                            else
                                strHTML.Append("<td style=\"line-height: 1.42857; padding: 5px; vertical-align: top; width:20% !important;\">" + o.ToString() + "</td>");
                        }
                        strHTML.Append("</tr>");
                    }

                    if (grp.Items.Count() > 0)
                    {
                        strHTML.Append("</table></div>");
                    }
                }

                // Totals Group
                strHTML.Append("<table style=\"width:100%; margin-bottom: 5px; margin-top: 15px; color: #FF8300; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;\">");
                strHTML.Append("<tr style=\"width:100%; font-size: 12px;  font-weight: bold;\"><td>Totals</td></tr>");
                strHTML.Append("</table>");
                strHTML.Append("<div style=\"width:100%;  color: #333333; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; margin-top: 15px;\">");
                strHTML.Append("<table style=\"width:100%; border: 0; border-color: #CCCCCC; border-width: 1px; border-style: solid; background-color: rgba(0, 0, 0, 0);  margin-bottom: 15px; border-collapse: collapse; font-size: 10px;\">");
                foreach (var item in result.Groups)
                {
                    strHTML.Append("<tr>");
                    strHTML.Append("<td style=\"line-height: 1.42857; padding: 5px; vertical-align: top; width:20% !important;\">" + item.Label + ":</td>");
                    strHTML.Append("<td style=\"line-height: 1.42857; padding: 5px; vertical-align: top; width:20% !important;\">" + item.Items.Count().ToString() + "</td>");
                    strHTML.Append("</tr>");
                }
                strHTML.Append("</table></div>");
            }

            return Pdf.ConvertHtmlToPdf(strHTML.ToString(), null, PageOrientation.Landscape);
        }

        /// <summary>
        /// Gets the total row count for the report result for readability because Alex 
        /// doesn't like inter-nested ternary operators that span 4 printed pages for
        /// whatever reason.
        /// </summary>
        /// <param name="result">The report result to get the total count from.</param>
        /// <returns>Either the count of items, sum of grouped item counts or 0 (zero).</returns>
        private int GetTotalRowCount(ReportResult result)
        {
            if (result.Items != null && result.Items.Any())
                return result.Items.Count;
            if (result.Groups != null && result.Groups.Any())
                return result.Groups.Sum(g => g.Items != null ? g.Items.Count : 0);
            return 0;
        }

        /// <summary>
        /// Generates the CSV.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="criteria">The criteria.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public byte[] GenerateCSV(string id, ReportCriteria criteria, User user)
        {
            User theUser = user ?? User.Current;
            Log.Info("REPORTS: Generate CSV {0} requested for user '{1}' with criteria {2}", id, user, criteria.ToJSON());
            var result = RunReport(id, criteria, theUser);

            BaseReport report = GetReport(id);
            MergeCriteria(criteria, report.BuildEmployerCriteria(theUser, report.GetCriteria(theUser)));

            using (MemoryStream output = new MemoryStream())
            using (StreamWriter writer = new StreamWriter(output, Encoding.UTF8))
            {
                writer.Write(report.Category + " " + report.Name);
                writer.Write("\n\r");

                writer.Write("Start Date");
                writer.Write(",");
                writer.Write(criteria.StartDate.ToString("MM/dd/yyyy"));
                writer.Write("\n");

                writer.Write("End Date");
                writer.Write(",");
                writer.Write(criteria.EndDate.ToString("MM/dd/yyyy"));
                writer.Write("\n");

                foreach (var filter in criteria.Filters)
                {
                    if (filter.Value != null && (!(filter.Value is string) || !string.IsNullOrWhiteSpace(filter.ValueAs<string>())))
                    {
                        writer.Write(filter.Name);
                        writer.Write(",");
                        ReportCriteriaItemOption opt = null;
                        if (filter.Options != null)
                        {
                            opt = filter.Options.FirstOrDefault(o => filter.Value.Equals(o.Value));
                            if (opt == null)
                                opt = filter.Options.FirstOrDefault(o => filter.Value.ToString() == o.Value.ToString());
                        }
                        writer.Write(opt == null ? filter.Value : opt.Text);
                        writer.Write("\n");
                    }
                }

                writer.Write("Total Rows");
                writer.Write(",");
                writer.Write(GetTotalRowCount(result).ToString());
                writer.Write("\n");

                if (result.Items != null && result.Items.Any())
                {
                    writer.Write("\n\r");
                    List<string> tList = new List<string>();
                    foreach (var item in result.Items)
                    {
                        if (tList.Count <= 0)
                        {
                            tList = GetPropertyList(result.Items);
                            foreach (var col in tList)
                            {
                                writer.Write(col);
                                writer.Write(",");
                            }
                            writer.Write("\n");
                        }

                        foreach (var col in tList)
                        {
                            writer.Write(Escape((item[col] ?? "").ToString()));
                            writer.Write(",");
                        }

                        writer.Write("\n");
                    }
                }
                else if (result.Groups != null && result.Groups.Any(m => m.Items.Count() > 0))
                {
                    writer.Write("\n\r");
                    foreach (var grp in result.Groups)
                    {
                        writer.Write(grp.Label);
                        writer.Write("\n");
                        List<string> tList = new List<string>();
                        foreach (var item in grp.Items)
                        {
                            if (tList.Count <= 0)
                            {
                                tList = GetPropertyList(grp.Items);
                                foreach (var col in tList)
                                {
                                    writer.Write(col);
                                    writer.Write(",");
                                }
                                writer.Write("\n");
                            }

                            foreach (var col in tList)
                            {
                                writer.Write(Escape((item[col] ?? "").ToString()));
                                writer.Write(",");
                            }

                            writer.Write("\n");
                        }

                        writer.Write("\n\r");
                    }
                }

                writer.Flush();
                output.Position = 0;

                return output.ToArray();
            }
        }

        /// <summary>
        /// Gets the property list.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        private List<string> GetPropertyList(DynamicObject item)
        {
            return item.GetDynamicMemberNames().ToList();
        }

        /// <summary>
        /// Gets the property list.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <returns></returns>
        private List<string> GetPropertyList(IEnumerable<DynamicObject> items)
        {
            return items.SelectMany(item => GetPropertyList(item)).Distinct().ToList();
        }

        /// <summary>
        /// This method escap some chars from string
        /// </summary>
        /// <param name="s">string to be escaped from some chars</param>
        /// <returns>returns string with escaped chars</returns>
        private static string Escape(string s)
        {
            if (string.IsNullOrWhiteSpace(s))
                return string.Empty;

            if (s.Contains(QUOTE))
                s = s.Replace(QUOTE, ESCAPED_QUOTE);

            if (s.IndexOfAny(CHARACTERS_THAT_MUST_BE_QUOTED) > -1)
                s = string.Concat(QUOTE, s, QUOTE);

            return s;
        }

        private const string Comma = ",";
        private const string QUOTE = "\"";
        private const string ESCAPED_QUOTE = "\"\"";
        private static char[] CHARACTERS_THAT_MUST_BE_QUOTED = { ',', '"', '\n' };

        /// <summary>
        /// Merges the criteria.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <param name="original">The original.</param>
        private void MergeCriteria(ReportCriteria criteria, ReportCriteria original)
        {
            if (criteria.Filters != null && original.Filters != null)
            {
                foreach (var filter in criteria.Filters)
                {
                    var match = original.Filters.FirstOrDefault(f => f.Name == filter.Name);
                    if (match != null)
                    {
                        filter.Options = match.Options;
                        filter.Prompt = match.Prompt;
                        filter.ControlType = match.ControlType;
                    }
                }
            }
        }

        #region ESS Report Filtering
        /// <summary>
        /// Returns all ESS Reports based on it's type
        /// </summary>
        /// <param name="Type"></param>
        /// <returns></returns>
        public List<BaseReport> GetAllESSReportsByType(ESSReportType Type = ESSReportType.TeamReport)
        {
            return GetReports().Where(p => p.IsESSReport == true
                                            && p.ESSReportDetails != null
                                            && p.ESSReportDetails.Type == Type).ToList();
        }

        #endregion
    }
}
