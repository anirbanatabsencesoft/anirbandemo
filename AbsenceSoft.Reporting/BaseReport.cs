﻿using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Reporting
{
    [Serializable]
    public abstract class BaseReport
    {

        protected static class CriteriaName
        {
            public const string CaseStatus = "CaseStatus";
            public const string AbsenceReason = "AbsenceReason";
            public const string WorkState = "WorkState";
            public const string Location = "Location";
            public const string EmployeeNumber = "EmployeeNumber";
            public const string CaseAssignee = "CaseAssignee";
        }

        /// <summary>
        /// The employer identifier filter name
        /// </summary>
        public const string EmployerIdFilterName = "EmployerId";

        /// <summary>
        /// Set the degree of parallelism
        /// </summary>
        public readonly int _degreeOfParallelism = Environment.ProcessorCount == 1 ? 1 : Environment.ProcessorCount -1;

        /// <summary>
        /// Gets the unique identifier for this report so it can be uniquely identified
        /// among any list of reports. Typically a GUID
        /// </summary>
        public abstract string Id { get; }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report for display. Also represents
        /// the report title or a portion of the report title based on report requirements.
        /// </summary>
        public abstract string Name { get; }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report category for display.
        /// </summary>
        public abstract string Category { get; }

        /// <summary>
        /// When overridden in a derived class, gets the name of the report main category to bifurcate reports.
        /// </summary>
        public abstract string MainCategory { get; }

        /// <summary>
        /// When overridden in a derived class, gets the order for which this report should be displayed within it's category
        /// </summary>
        public virtual int CategoryOrder { get { return 0; } }

        /// <summary>
        /// When overridden in a derived class, gets name of the icon image to set in front end
        /// </summary>
        public abstract string IconImage { get; }

        /// <summary>
        /// When overridden in a derived class, gets whether or not this report provides grouped results.
        /// </summary>
        public abstract bool IsGrouped { get; }

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether to show the auto 
        /// generated total counts at the bottom of the group page.
        /// </summary>
        /// <value>
        ///   <c>true</c> if group auto totals; otherwise, <c>false</c>.
        /// </value>
        public virtual bool GroupAutoTotals { get { return true; } }

        /// <summary>
        /// Property to configure chart related options
        /// </summary>
        public abstract ReportChart Chart { get; }

        /// <summary>
        /// When overridden in a derived class, builds and returns the default populated report
        /// criteria necessary for this given report, populates any select list options, etc. ready
        /// to be filled out in the UI by an end user, or populated with default values for automated
        /// or assisted report running.
        /// </summary>
        /// <param name="user">The user to pull criteria for based on user employer access, permissions, etc.</param>
        /// <returns>A fully populated report criteria object with filled in defaults and select options
        /// where appropriate.</returns>
        public abstract ReportCriteria GetCriteria(User user);

        /// <summary>
        /// Returns any required features that must be subscribed to by the current customer in order for this
        /// report to be run/included in the reports list.
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<Feature> RequiredFeatures() { return new List<Feature>(0); }

        /// <summary>
        /// Determines whether the specified report is visible to the given user for any valid reason.
        /// </summary>
        /// <param name="user">The user to check against.</param>
        /// <returns>
        ///   <c>true</c> if the specified report is visible; otherwise, <c>false</c>.
        /// </returns>
        public virtual bool IsVisible(User user) { return true; }

        /// <summary>
        /// Gets a value indicating whether this instance is viewable in the UI (Run Button) after being
        /// generated, or if not viewable, is only exportable.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is viewable; otherwise, <c>false</c>.
        /// </value>
        public virtual bool IsViewable { get { return true; } }

        /// <summary>
        /// Gets a value indicating whether this instance is exportable to CSV.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is exportable to CSV; otherwise, <c>false</c>.
        /// </value>
        public virtual bool IsExportableToCsv { get { return true; } }

        /// <summary>
        /// Gets a value indicating whether this instance is exportable to PDF.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is exportable to PDF; otherwise, <c>false</c>.
        /// </value>
        public virtual bool IsExportableToPdf { get { return true; } }

        /// <summary>
        /// Gets a value indicating whether report should include Organization Grouping Criteria.
        /// </summary>
        public virtual bool IncludeOrganizationGrouping { get { return false; } }


        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user account that is requesting the report</param>
        /// <param name="criteria">The report criteria to use for building the report.</param>
        /// <returns>A populated report result.</returns>
        public ReportResult RunReport(User user, ReportCriteria criteria)
        {
            // Prepare our report result
            ReportResult result = prepareResult(user, criteria);

            try
            {
                result.Success = true;
                RunReport(user, result);
            }
            catch (Exception ex)
            {
                var errorKey = Guid.NewGuid().ToString();
                Log.Error(string.Format("Error running report, key {0}", errorKey), ex);
                result.Error = string.Format("{0}: {1}", errorKey, ex.Message);
                result.Success = false;
            }

            // Return the finished result product back to the caller, successful or not
            return finishResult(result);
        }//end: RunReport

        /// <summary>
        /// When overridden in a derived class, builds a query and executes the query, evaluating the result
        /// and formatting it into a report result and returns that to the UI for display binding, or
        /// to the rendering engine for PDF rendering.
        /// </summary>
        /// <param name="user">The user to run the report for</param>
        /// <param name="result">The report result to populate during and after the run and return back to the caller.</param>
        protected abstract void RunReport(User user, ReportResult result);

        /// <summary>
        /// Prepares a new report result to be called by the inheriting report implementation for all report results.
        /// </summary>
        /// <param name="user">The user account of the user requesting the report.</param>
        /// <param name="criteria">The report critera for the report request.</param>
        /// <returns>A working report result that gets passed to the report's implementation of RunReport.</returns>
        private ReportResult prepareResult(User user, ReportCriteria criteria)
        {
            if (user == null)
                user = User.Current;
            if (user == null)
                throw new ArgumentNullException("user", "Must provide a valid user account or a current logged in user in order to request a report run.");

            if (criteria == null)
                criteria = GetCriteria(user);

            if (string.IsNullOrWhiteSpace(criteria.CustomerId))
                criteria.CustomerId = user.CustomerId;
            if (criteria.StartDate == default(DateTime))
                criteria.StartDate = DateTime.UtcNow.AddDays(-1).Date;
            if (criteria.EndDate == default(DateTime))
                criteria.EndDate = DateTime.UtcNow.Date;
            if (criteria.Filters == null)
                criteria.Filters = new List<ReportCriteriaItem>(0);

            ReportResult result = new ReportResult();
            result.Name = Name;
            result.Category = Category;
            result.Criteria = criteria;
            result.Chart = Chart;
            result.IconImage = IconImage;
            result.RequestDate = DateTime.UtcNow;
            result.RequestedBy = user ?? User.Current;
            result.GroupAutoTotals = GroupAutoTotals;
            return result;
        }//end: prepareResult

        /// <summary>
        /// Prepares the report result to be returned with results populated or an error
        /// back to the caller of run report. Calculates the total time to complete, etc.
        /// </summary>
        /// <param name="result">The active working report result to complete.</param>
        /// <returns>The passed in ReportResult with altered properties.</returns>
        private ReportResult finishResult(ReportResult result)
        {
            // Set our time to complete milliseconds
            result.CompletedIn = (DateTime.UtcNow - result.RequestDate).TotalMilliseconds;

            // Clean up any mis-appropriation of object references depending on the report type
            if (IsGrouped) result.Items = null;
            else result.Groups = null;

            return result;
        }//end: finishResult

        #region Global/Common Criteria Item Builders

        /// <summary>
        /// Builds the employer criteria.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="criteria">The criteria.</param>
        internal virtual ReportCriteria BuildEmployerCriteria(User user, ReportCriteria criteria)
        {
            // If the criteria already has an employerId filter, then no need to add it if necessary
            if (criteria.HasCriterias && criteria.Filters.Any(f => f.Name == EmployerIdFilterName))
                return criteria;

            // Check to see if the custer has the multi-employer-access feature, and if so, if this particular user
            //  even has access to multiple employers (if they don't, no need to filter by it)
            if (user.Customer != null && user.Customer.HasFeature(Feature.MultiEmployerAccess) && user.Employers.Count > 1)
                criteria.Filters.Insert(0, GetEmployerCriteria(user));

            // Return our working criteria for Fluid style use
            return criteria;
        }

        /// <summary>
        /// Gets the employer criteria.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        protected virtual ReportCriteriaItem GetEmployerCriteria(User user)
        {
            ReportCriteriaItem item = new ReportCriteriaItem();
            item.Name = EmployerIdFilterName;
            item.Prompt = "Employer";
            item.Required = false;
            item.ControlType = ControlType.CheckBoxList;
            item.KeepOptionAll = true;
            var employerIds = user.Employers == null ? new string[0] : user.Employers.Select(e => e.EmployerId).ToArray();
            var employers = Employer.AsQueryable(user).Where(e => employerIds.Contains(e.Id)).ToList();
            item.Options = employers.Select(e => new ReportCriteriaItemOption()
            {
                Value = e.Id,
                Text = e.Name
            }).OrderBy(e => e.Text).ToList();
            return item;
        }

        /// <summary>
        /// Gets the employee criteria.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        protected virtual ReportCriteriaItem GetEmployeeCriteria(User user)
        {
            ReportCriteriaItem item = new ReportCriteriaItem();
            item.Name = "EmployeeNumber";
            item.Prompt = "Employee Number";
            item.Required = false;
            item.ControlType = ControlType.TextBox;
            return item;
        }

        /// <summary>
        /// Gets the case status criteria.
        /// </summary>
        /// <returns></returns>
        protected virtual ReportCriteriaItem GetCaseStatusCriteria()
        {
            ReportCriteriaItem item = new ReportCriteriaItem();
            item.Name = "CaseStatus";
            item.Prompt = "Case Status";
            item.Required = false;
            item.ControlType = ControlType.RadioButtonList;
            item.Value = (int)CaseStatus.Open;
            item.Options = Enum.GetValues(typeof(CaseStatus)).OfType<CaseStatus>().Select(e => new ReportCriteriaItemOption()
            {
                Text = e.ToString().SplitCamelCaseString(),
                Value = (int)e
            }).ToList();
            return item;
        }

        /// <summary>
        /// Gets the absence reason criteria.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        protected virtual ReportCriteriaItem GetAbsenceReasonCriteria(User user)
        {
            ReportCriteriaItem item = new ReportCriteriaItem();
            item.Name = "AbsenceReason";
            item.Prompt = "Absence Reason";
            item.Required = false;
            item.ControlType = ControlType.SelectList;
            item.KeepOptionAll = true;
            var employerList = new EmployerService().Using(s => s.GetEmployersForUser(user)).Select(e => e.Id).ToArray();
            item.Options = AbsenceReason.AsQueryable(user)
                .Where(a => a.EmployerId == null || employerList.Contains(a.EmployerId))
                .OrderBy(a => a.Category)
                .ThenBy(a => a.Name)
                .ToList()
                .Select(e => new ReportCriteriaItemOption()
                {
                    Text = e.Name,
                    Value = e.Id,
                    GroupText = e.Category
                }).ToList();
            return item;
        }

        /// <summary>
        /// Gets the case type criteria.
        /// </summary>
        /// <returns></returns>
        protected virtual ReportCriteriaItem GetCaseTypeCriteria()
        {
            ReportCriteriaItem item = new ReportCriteriaItem();
            item.Name = "CaseType";
            item.Prompt = "Case Type";
            item.Required = false;
            item.ControlType = ControlType.RadioButtonList;
            item.Options = Enum.GetValues(typeof(CaseType)).OfType<CaseType>().Where(t => t != CaseType.Administrative).Select(e => new ReportCriteriaItemOption()
            {
                Text = e.ToString().SplitCamelCaseString(),
                Value = (int)e
            }).ToList();
            return item;
        }

        protected virtual ReportCriteriaItem GetCommTypeCriteria()
        {
            ReportCriteriaItem item = new ReportCriteriaItem();
            item.Name = "Comm Type";
            item.Prompt = "Communication Type";
            item.Required = false;
            item.ControlType = ControlType.RadioButtonList;
            item.Options = Enum.GetValues(typeof(CommunicationType)).OfType<CommunicationType>().Select(e => new ReportCriteriaItemOption()
            {
                Text = e.ToString().SplitCamelCaseString(),
                Value = (int)e
            }).ToList();
            return item;
        }

        /// <summary>
        /// Gets the adjudication criteria.
        /// </summary>
        /// <returns></returns>
        protected virtual ReportCriteriaItem GetAdjudicationCriteria()
        {
            ReportCriteriaItem item = new ReportCriteriaItem();
            item.Name = "Determination";
            item.Prompt = "Approval Status";
            item.Required = false;
            item.ControlType = ControlType.RadioButtonList;
            // It's optional, no default value
            //item.Value = (int)AdjudicationStatus.Approved;
            item.Options.Add(new ReportCriteriaItemOption()
            {
                Text = AdjudicationStatus.Approved.ToString().SplitCamelCaseString(),
                Value = (int)AdjudicationStatus.Approved
            });
            item.Options.Add(new ReportCriteriaItemOption()
            {
                Text = AdjudicationStatus.Pending.ToString().SplitCamelCaseString(),
                Value = (int)AdjudicationStatus.Pending
            });
            item.Options.Add(new ReportCriteriaItemOption()
            {
                Text = AdjudicationStatus.Denied.ToString().SplitCamelCaseString(),
                Value = (int)AdjudicationStatus.Denied
            });
            return item;
        }

        /// <summary>
        /// Gets the location criteria.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        protected virtual ReportCriteriaItem GetLocationCriteria(User user)
        {
            ReportCriteriaItem item = new ReportCriteriaItem();
            item.Name = "Location";
            item.Prompt = "Location";
            item.Required = false;
            item.ControlType = ControlType.CheckBoxList;
            item.KeepOptionAll = true;
            item.Options =
                Organization.AsQueryable(user).Where(c => c.CustomerId == user.CustomerId
                    && c.TypeCode == OrganizationType.OfficeLocationTypeCode)
                .ToList()
                .Select(o => new ReportCriteriaItemOption() { Text = o.Name, Value = o.Code })
                .OrderBy(o => o.Value)
                .ToList();
            item.OnChangeMethodName = "FilterChangeEvent";
            return item;
        }

        /// <summary>
        /// Gets the work state criteria.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        protected virtual ReportCriteriaItem GetWorkStateCriteria(User user, DateTime startDate, DateTime endDate)
        {
            var filterStartDate = startDate.ToMidnight();
            var filterEndDate = endDate.EndOfDay();
            ReportCriteriaItem item = new ReportCriteriaItem();
            item.Name = "WorkState";
            item.Prompt = "Work State";
            item.Required = false;
            item.ControlType = ControlType.SelectList;
            item.KeepOptionAll = true;
            item.Options = Case.AsQueryable(user)
               .Where(e => e.Employee.WorkState != null && e.Employee.WorkState != "" &&
                   ((e.StartDate >= filterStartDate && e.StartDate <= filterEndDate) || (e.EndDate >= filterStartDate && e.EndDate <= filterEndDate)))
               .Select(e => e.Employee.WorkState)
               .Distinct()
               .ToList()
               .OrderBy(e => e)
               .Select(e => new ReportCriteriaItemOption()
               {
                   Text = e,
                   Value = e
               }).ToList();

            item.OnChangeMethodName = "FilterChangeEvent";

            return item;
        }

        /// <summary>
        /// Gets the productivity criteria.
        /// </summary>
        /// <returns></returns>
        protected virtual ReportCriteriaItem GetProductivityCriteria()
        {
            ReportCriteriaItem item = new ReportCriteriaItem();
            item.Name = "Duration";
            item.Prompt = "Duration";
            item.Required = false;
            item.ControlType = ControlType.Duration;
            item.Value = "Monthly";
            item.Options = new List<ReportCriteriaItemOption>(){
                new ReportCriteriaItemOption()
                {
                    Text = "Monthly",
                    Value = "Monthly"
                },
                new ReportCriteriaItemOption()
                {
                    Text = "Quarterly",
                    Value = "Quarterly"
                },
                new ReportCriteriaItemOption()
                {
                    Text = "Yearly",
                    Value = "Yearly"
                },
                new ReportCriteriaItemOption()
                {
                    Text = "Date Range",
                    Value = "DateRange"
                },
                 new ReportCriteriaItemOption()
                {
                    Text = "Rolling 12 Months",
                    Value = "Rolling"
                }
            };
            item.OnChangeMethodName = "FilterChangeEvent";
            return item;
        }

        /// <summary>
        /// Gets the report type criteria.
        /// </summary>
        /// <returns></returns>
        protected virtual ReportCriteriaItem GetReportTypeCriteria()
        {
            ReportCriteriaItem item = new ReportCriteriaItem();
            item.Name = "ReportType";
            item.Prompt = "Report Type";
            item.Required = false;
            item.ControlType = ControlType.SelectList;
            item.Value = "ByReason";
            item.Options = new List<ReportCriteriaItemOption>(){
                new ReportCriteriaItemOption()
                {
                    Text = "By Reason",
                    Value = "ByReason"
                },
                new ReportCriteriaItemOption()
                {
                    Text = "By Location",
                    Value = "ByLocation"
                }
            };
            item.OnChangeMethodName = "FilterChangeEvent";
            return item;
        }

        /// <summary>
        /// Gets the accom type criteria.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        protected virtual ReportCriteriaItem GetAccomTypeCriteria(User user)
        {
            ReportCriteriaItem item = new ReportCriteriaItem();
            item.Name = "AccomType";
            item.Prompt = "Accommodation Type";
            item.Required = false;
            item.ControlType = ControlType.SelectList;
            item.KeepOptionAll = true;
            item.Options = new AccommodationService(user).Using(s => s.GetAccommodationTypes(user.CustomerId)).Where(m => !string.IsNullOrWhiteSpace(m.Code)).Select(e => new ReportCriteriaItemOption()
            {
                Text = e.Name,
                Value = e.Code
            }).OrderBy(g => g.Text).ToList();
            item.OnChangeMethodName = "FilterChangeEvent";
            return item;
        }

        /// <summary>
        /// Gets the gender criteria.
        /// </summary>
        /// <returns></returns>
        protected virtual ReportCriteriaItem GetGenderCriteria()
        {
            ReportCriteriaItem item = new ReportCriteriaItem();
            item.Name = "Gender";
            item.Prompt = "Gender";
            item.Required = false;
            item.ControlType = ControlType.SelectList;
            item.KeepOptionAll = true;
            item.Options = Enum.GetValues(typeof(Gender)).OfType<Gender>().Select(e => new ReportCriteriaItemOption()
            {
                Text = e.ToString().SplitCamelCaseString(),
                Value = (int)e
            }).ToList();
            item.OnChangeMethodName = "FilterChangeEvent";
            return item;
        }

        /// <summary>
        /// Gets the custom fields criteria.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="fieldType">Type of the field.</param>
        /// <param name="target">The target.</param>
        /// <returns></returns>
        protected virtual ReportCriteriaItem GetCustomFieldsCriteria(User user, CustomFieldType? fieldType = null, EntityTarget? target = null)
        {
            ReportCriteriaItem item = new ReportCriteriaItem();
            item.ControlType = Data.Enums.ControlType.SelectList;
            item.KeepOptionAll = true;
            item.Name = "CustomField";
            item.Prompt = "Custom Field";
            item.Required = false;
            item.OnChangeMethodName = "FilterChangeEvent";
            var query = CustomField.AsQueryable(user).Where(c => c.CustomerId == user.CustomerId);
            if (fieldType.HasValue)
                query = query.Where(f => f.DataType == fieldType.Value);
            if (target.HasValue)
                query = query.Where(f => f.Target == target.Value);
            item.Options = query
                .ToList()
                .Distinct(c => c.Name)
                .Select(f => new ReportCriteriaItemOption()
                {
                    Value = f.Name,
                    Text = f.Label
                })
                .ToList();
            return item;
        }

        /// <summary>
        /// Gets to do type criteria.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        protected virtual ReportCriteriaItem GetToDoTypeCriteria(User user)
        {
            ReportCriteriaItem item = new ReportCriteriaItem();
            item.ControlType = Data.Enums.ControlType.SelectList;
            item.KeepOptionAll = true;
            item.Name = "ToDoItemType";
            item.Prompt = "ToDo Type";
            item.Required = false;
            item.OnChangeMethodName = "FilterChangeEvent";
            Dictionary<ToDoItemType, string> items = new Dictionary<ToDoItemType, string>();
            using (ToDoService svc = new ToDoService())
                foreach (var employer in user.Employers.Where(e => e.IsActive))
                {
                    var merge = svc.GetToDoTypes(employer.EmployerId, user);
                    foreach (var kvp in merge)
                        if (!items.ContainsKey(kvp.Key))
                            items.Add(kvp.Key, kvp.Value);
                }
            item.Options = items.OrderBy(t => t.Value).Select(t => new ReportCriteriaItemOption()
            {
                Value = (int)t.Key,
                Text = t.Value
            }).ToList();
            return item;
        }

        protected virtual ReportCriteriaItem GetCaseAssigneeCriteria(User user)
        {
            ReportCriteriaItem item = new ReportCriteriaItem();
            item.ControlType = Data.Enums.ControlType.SelectList;
            item.KeepOptionAll = true;
            item.Name = CriteriaName.CaseAssignee;
            item.Prompt = "Assigned To";
            item.Required = false;
            item.OnChangeMethodName = "FilterChangeEvent";

            var userIds = Case.Repository.Collection.Distinct<BsonObjectId>("AssignedToId",
                Case.Query.And(
                    Case.Query.IsNotDeleted(),
                    Case.Query.EQ(c => c.CustomerId, user.CustomerId),
                    Case.Query.NE(c => c.AssignedToId, null))).ToList();
            if (!userIds.Any())
                return item;
            var users = User.Query.Find(Query.In("_id", userIds)).ToList();

            item.Options = users.OrderBy(t => t.DisplayName).Select(t => new ReportCriteriaItemOption()
            {
                Value = t.Id,
                Text = t.DisplayName
            }).ToList();
            return item;
        }

        protected virtual ReportCriteriaItem GetToDoAssigneeCriteria(User user)
        {
            ReportCriteriaItem item = new ReportCriteriaItem();
            item.ControlType = Data.Enums.ControlType.SelectList;
            item.KeepOptionAll = true;
            item.Name = "ToDoItemAssignee";
            item.Prompt = "Assigned To";
            item.Required = false;
            item.OnChangeMethodName = "FilterChangeEvent";

            var userIds = Case.Repository.Collection.Distinct<BsonObjectId>("AssignedToId", ToDoItem.Query.And(ToDoItem.Query.IsNotDeleted(),
                ToDoItem.Query.EQ(c => c.CustomerId, user.CustomerId))).ToList();
            if (!userIds.Any())
                return item;
            var users = User.Query.Find(Query.In("_id", userIds)).ToList();

            item.Options = users.OrderBy(t => t.DisplayName).Select(t => new ReportCriteriaItemOption()
            {
                Value = t.Id,
                Text = t.DisplayName
            }).ToList();
            return item;
        }

        /// <summary>
        /// Gets to do status criteria.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        protected virtual ReportCriteriaItem GetToDoStatusCriteria(User user)
        {
            ReportCriteriaItem item = new ReportCriteriaItem();
            item.ControlType = Data.Enums.ControlType.SelectList;
            item.KeepOptionAll = true;
            item.Name = "ToDoStatus";
            item.Prompt = "ToDo Status";
            item.Required = false;
            item.OnChangeMethodName = "FilterChangeEvent";
            item.Options = Enum.GetValues(typeof(ToDoItemStatus)).OfType<ToDoItemStatus>().Select(t => new ReportCriteriaItemOption()
            {
                Value = (int)t,
                Text = t.ToString().SplitCamelCaseString()
            }).OrderBy(t => t.Text).ToList();
            return item;
        }

        protected virtual ReportCriteriaItem GetDiagnosisCodeCriteria(User user)
        {
            ReportCriteriaItem item = new ReportCriteriaItem();
            item.Name = "DiagnosisCode";
            item.Prompt = "Diagnosis";
            item.Required = false;
            item.ControlType = ControlType.CheckBoxList;
            item.KeepOptionAll = true;

            var employerList = new EmployerService().Using(s => s.GetEmployersForUser(user)).Select(e => e.Id).ToArray();
            var caseList = Case.AsQueryable().Where(c =>
                c.CustomerId == user.CustomerId
                && (c.EmployerId == null || employerList.Contains(c.EmployerId))
                && c.Disability.PrimaryDiagnosis.Code != null)
                .ToList();

            Dictionary<string, string> codes = new Dictionary<string, string>(caseList.Count);
            foreach (var c in caseList)
                if (!codes.ContainsKey(c.Disability.PrimaryDiagnosis.Code))
                    codes.Add(c.Disability.PrimaryDiagnosis.Code, c.Disability.PrimaryDiagnosis.UIDescription);

            item.Options = codes
                .OrderBy(a => a.Value)
                .ThenBy(a => a.Key)
                .ToList()
                .Select(e => new ReportCriteriaItemOption()
                {
                    Text = e.Value,
                    Value = e.Key
                }).ToList();

            return item;
        }
        /// <summary>
        /// Gets Organization Type Criteria
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        protected virtual List<ReportCriteriaItem> GetOrganizationTypeCriteria(User user)
        {
            List<string> employerIds = user.Employers.Where(e => e.IsActive).Select(emp => emp.EmployerId).ToList<string>();

            List<OrganizationType> orgTypes = OrganizationType.Query.Find(OrganizationType.Query.And(new List<IMongoQuery>()
            {
                OrganizationType.Query.EQ(o => o.CustomerId, user.CustomerId),
                OrganizationType.Query.In(o => o.EmployerId, employerIds)
            })).ToList();

            if (!orgTypes.Where(type => type.Code.ToUpper() == OrganizationType.OfficeLocationTypeCode.ToUpper()).Any())
                orgTypes.Insert(0, new OrganizationType() { Code = OrganizationType.OfficeLocationTypeCode, Name = "Location" });

            var items = orgTypes.Select(o => new ReportCriteriaItem()
            {
                Name = o.Code,
                Prompt = o.Name,
                Required = false,
                ControlType = ControlType.CheckBoxList,
                KeepOptionAll = true,
                Options = new List<ReportCriteriaItemOption>()
            }).ToList();

            var query = Organization.Query.And(new List<IMongoQuery>()
            {
                Organization.Query.EQ(o => o.CustomerId, user.CustomerId),
                Organization.Query.In(o => o.EmployerId, employerIds),
                Organization.Query.In(o => o.TypeCode, items.Select(i => i.Name))
            });

            var orgs = Organization.Query.Find(query).SetFields(Organization.Query.IncludeFields(o => o.TypeCode, o => o.Code, o => o.Name)).ToList();

            foreach (var o in items)
            {
                o.Options = orgs.Where(t => t.TypeCode == o.Name && !string.IsNullOrEmpty(t.Name)).OrderBy(t => t.Name).Select(t => new ReportCriteriaItemOption()
                {
                    Value = t.Code,
                    Text = t.Name
                }).Distinct(t => t.Value).ToList();
            }
            return items;
        }

        /// <summary>
        /// Gets the absence reason criteria.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        protected virtual ReportCriteriaItem GetOrganizationTypeGroupByCriteria(User user)
        {
            var item = new ReportCriteriaItem();
            item.Name = "OrganizationTypeGrouping";
            item.Prompt = "Group By";
            item.Required = false;
            item.ControlType = ControlType.SelectList;
            item.KeepOptionAll = true;

            List<string> employerIds = user.Employers.Where(e => e.IsActive).Select(emp => emp.EmployerId).ToList<string>();
            item.Options = OrganizationType.Query.Find(OrganizationType.Query.And(new List<IMongoQuery>()
            {
                OrganizationType.Query.EQ(o => o.CustomerId, user.CustomerId),
                OrganizationType.Query.In(o => o.EmployerId, employerIds)
            })).ToList().Select(o => new ReportCriteriaItemOption()
            {
                Text = o.Name,
                Value = o.Code
            }).ToList();
            if (!item.Options.Any())
                item.Options.Add(new ReportCriteriaItemOption() { Value = "OFFICELOCATION", Text = "Office Location" });

            return item;
        }

        #endregion Global/Common Criteria Item Builders

        #region Global/Common Query Builders
        /// <summary>
        /// Appends Organization Filters
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <param name="employeeIdField"></param>
        /// <returns></returns>
        protected virtual void AppendOrganizationFilterQuery(User user, ReportCriteria criteria, List<IMongoQuery> queries, string employeeIdField = null)
        {
            if (user == null || criteria == null || criteria.Filters == null || !criteria.Filters.Any() || queries == null)
                return;
            List<OrganizationType> organizationTypes = OrganizationType.AsQueryable(user).Where(type => type.CustomerId == user.CustomerId).ToList();
            List<IMongoQuery> ors = new List<IMongoQuery>();
            employeeIdField = employeeIdField ?? "EmployeeId";
            List<string> employerIds = null;
            var employerIdFilter = criteria.Filters.FirstOrDefault(m => m.Name == EmployerIdFilterName);
            if (employerIdFilter != null && employerIdFilter.Options != null && employerIdFilter.Options.Any())
            {
                if (employerIdFilter.Values != null && employerIdFilter.Values.Any())
                    employerIds = employerIdFilter.ValuesAs<string>();
                foreach (var employerIdOption in employerIdFilter.Options)
                {
                    organizationTypes.AddIfNotExists(new OrganizationType()
                    {
                        Code = OrganizationType.OfficeLocationTypeCode,
                        EmployerId = employerIdOption.Value.ToString(),
                        CustomerId = user.CustomerId
                    }, c => string.Equals(c.Code, OrganizationType.OfficeLocationTypeCode, StringComparison.InvariantCultureIgnoreCase));
                }
            }
            bool orgTypeSelected = false;
            foreach (OrganizationType orgType in organizationTypes)
            {
                ReportCriteriaItem item = criteria.Filters.FirstOrDefault(m => m.Name == orgType.Code);
                if (item != null && item.Values != null && item.Values.Count > 0) orgTypeSelected = true; //Org Option selected
            }
            if (!orgTypeSelected) return;
            if (organizationTypes != null && employerIds != null && employerIds.Count > 0)
                organizationTypes = organizationTypes.Where(type => type.CustomerId == user.CustomerId
                        && employerIds.Contains(type.EmployerId)).ToList();

            if (organizationTypes != null)
            {
                Dictionary<string, List<IMongoQuery>> employerEmployees = new Dictionary<string, List<IMongoQuery>>();
                foreach (string employerId in organizationTypes.Select(type => type.EmployerId).Distinct())
                {
                    List<IMongoQuery> ands = new List<IMongoQuery>();
                    List<IMongoQuery> or = new List<IMongoQuery>();
                    foreach (OrganizationType orgType in organizationTypes.Where(type => type.EmployerId == employerId))
                    {
                        // Add the organization filter if provided
                        var orgTypeCodeFilter = criteria.Filters.FirstOrDefault(m => m.Name == orgType.Code);
                        if (orgTypeCodeFilter != null && orgTypeCodeFilter.Values != null && orgTypeCodeFilter.Values.Any())
                        {
                            List<string> orgCodes = orgTypeCodeFilter.Values.Select(s => s.ToString()).ToList();
                            List<EmployeeOrganization> empOrgs = EmployeeOrganization.AsQueryable(user).Where(org => org.CustomerId == user.CustomerId && orgCodes.Contains(org.Code) && org.EmployerId == orgType.EmployerId && org.TypeCode == orgType.Code).ToList();
                            List<string> emps = empOrgs.Where(org => org.TypeCode.ToUpper().Trim() != OrganizationType.OfficeLocationTypeCode.ToUpper().Trim() || org.Dates == null || org.Dates.IncludesToday(true)).Select(org => org.EmployeeNumber).ToList();
                            //if (emps.Count > 0) 
                            ands.Add(Query.In(employeeIdField, Employee.AsQueryable(user).Where(emp => emp.CustomerId == user.CustomerId && emp.EmployerId == orgType.EmployerId && emps.Contains(emp.EmployeeNumber)).
                            Select(emp => new BsonObjectId(new ObjectId(emp.Id))).ToList()));
                        }
                        else
                        {
                            List<EmployeeOrganization> empOrgs = EmployeeOrganization.AsQueryable(user).Where(org => org.CustomerId == user.CustomerId && org.EmployerId == orgType.EmployerId && org.TypeCode == orgType.Code).ToList();
                            List<string> emps = empOrgs.Where(org => org.TypeCode.ToUpper().Trim() != OrganizationType.OfficeLocationTypeCode.ToUpper().Trim() || org.Dates == null || org.Dates.IncludesToday(true)).Select(org => org.EmployeeNumber).ToList();
                            or.Add(Query.In(employeeIdField, Employee.AsQueryable(user).Where(emp => emp.CustomerId == user.CustomerId && emp.EmployerId == orgType.EmployerId && emps.Contains(emp.EmployeeNumber)).Select(emp => new BsonObjectId(new ObjectId(emp.Id))).ToList()));
                        }
                    }
                    List<IMongoQuery> andOr = new List<IMongoQuery>();
                    if (ands.Count > 0) andOr.Add(Query.And(ands));
                    if (or.Count > 0) andOr.Add(Query.Or(or));
                    if (andOr.Count <= 0)
                        andOr.Add(Query.Or(Query.NotExists("EmployerId"),
                            Query.In("EmployerId", new BsonValue[] { BsonNull.Value, new BsonObjectId(new ObjectId(employerId)) })
                        ));
                    employerEmployees.Add(employerId, andOr);
                }
                foreach (string key in employerEmployees.Keys) { if (employerEmployees[key] != null && employerEmployees[key].Count > 0) ors.Add(Query.Or(employerEmployees[key])); }
            }
            if (ors != null && ors.Count > 0)
                queries.Add(Query.Or(ors));
        }
        #endregion

        /// <summary>
        /// Applies the employer filter.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="query">The query.</param>
        /// <param name="criteria">The criteria.</param>
        protected void ApplyEmployerFilter<TEntity>(ICollection<IMongoQuery> query, ReportCriteria criteria) where TEntity : BaseEmployerEntity<TEntity>, new()
        {
            var employerIdFilter = criteria.Filters.FirstOrDefault(m => m.Name == EmployerIdFilterName);
            if (employerIdFilter != null && employerIdFilter.Values != null && employerIdFilter.Values.Any())
            {
                var val = employerIdFilter.ValuesAs<string>();
                if (val != null && val.Any())
                    query.Add(BaseEmployerEntity<TEntity>.Query.In(c => c.EmployerId, val));
            }
        }

        /// <summary>
        /// Applies the employer filter.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="criteria">The criteria.</param>
        /// <param name="employerId">The employer identifier.</param>
        protected void ApplyEmployerFilter(ICollection<IMongoQuery> query, ReportCriteria criteria, Func<string> employerId)
        {
            var employerIdFilter = criteria.Filters.FirstOrDefault(m => m.Name == EmployerIdFilterName);
            if (employerIdFilter != null && employerIdFilter.Value != null && employerIdFilter.Values.Any())
            {
                var val = employerIdFilter.ValuesAs<string>();
                if (val != null && val.Any())
                    query.Add(Query.In(employerId(), val.Select(v => new BsonString(v))));
            }
        }

        /// <summary>
        /// Applies the case filters.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="criteria">The criteria.</param>
        protected virtual void ApplyCaseFilters(ICollection<IMongoQuery> query, ReportCriteria criteria)
        {
            DateTime startDate = criteria.StartDate;
            DateTime endDate = criteria.EndDate;
            query.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate)))
            ));

            if (criteria.Filters.Any())
            {
                ApplyEmployerFilter<Case>(query, criteria);
                var caseStatus = criteria.Filters.FirstOrDefault(m => m.Name == "CaseStatus");
                if (caseStatus != null && caseStatus.Value != null)
                {
                    CaseStatus enumCaseStatusFilter = criteria.ValueOf<CaseStatus>("CaseStatus");
                    query.Add(Case.Query.EQ(c => c.Status, enumCaseStatusFilter));
                }

                List<IMongoQuery> elemMatchAnds = new List<IMongoQuery>();

                var determination = criteria.Filters.FirstOrDefault(m => m.Name == "Determination");
                if (determination != null && determination.Value != null)
                {
                    AdjudicationStatus enumadjStatusFilter = criteria.ValueOf<AdjudicationStatus>("Determination");
                    elemMatchAnds.Add(Query<Accommodation>.EQ(a => a.Determination, enumadjStatusFilter));
                }

                var accomType = criteria.Filters.FirstOrDefault(m => m.Name == "AccomType");
                if (accomType != null && accomType.Value != null)
                    elemMatchAnds.Add(Query<Accommodation>.EQ(a => a.Type.Code, criteria.Filters.FirstOrDefault(m => m.Name == "AccomType").Value));

                if (elemMatchAnds.Any())
                    query.Add(Case.Query.ElemMatch(c => c.AccommodationRequest.Accommodations, q => q.And(elemMatchAnds)));

                var workState = criteria.Filters.FirstOrDefault(m => m.Name == "WorkState");
                if (workState != null && workState.Value != null)
                    query.Add(Case.Query.EQ(e => e.Employee.WorkState, criteria.Filters.FirstOrDefault(m => m.Name == "WorkState").Value));

                if (criteria.Filters.FirstOrDefault(m => m.Name == "Location") != null &&
                    criteria.Filters.FirstOrDefault(m => m.Name == "Location").Value != null &&
                    !string.IsNullOrWhiteSpace(criteria.Filters.FirstOrDefault(m => m.Name == "Location").ValueAs<string>()))
                    query.Add(Case.Query.EQ(e => e.Employee.Info.OfficeLocation, criteria.Filters.FirstOrDefault(m => m.Name == "Location").Value));

                var assignee = criteria.Filters.FirstOrDefault(m => m.Name == "CaseAssignee");
                if (assignee != null && assignee.Value != null && !string.IsNullOrWhiteSpace(assignee.ValueAs<string>()))
                    query.Add(Case.Query.EQ(e => e.AssignedToId, assignee.Value));
            }

        }

        /// <summary>
        /// Gets the return to work date.
        /// </summary>
        public DateTime? GetCaseEventDate(Case c, CaseEventType eventType)
        {
            CaseEvent ev = c.FindCaseEvent(eventType);
            if (ev != null)
                return ev.EventDate;
            return null;
        }

        /// <summary>
        /// Gets a value indicating whether this instance has linked report.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has linked report; otherwise, <c>false</c>.
        /// </value>
        public virtual bool HasLinkedReport { get { return false; } }

        /// <summary>
        /// Gets the linked report.
        /// </summary>
        /// <value>
        /// The linked report.
        /// </value>
        public virtual LinkedReport LinkedReport { get { return null; } }

        /// <summary>
        /// Gets a value indicating whether this instance has sub report.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has sub report; otherwise, <c>false</c>.
        /// </value>
        public virtual bool HasSubReport { get { return false; } }

        /// <summary>
        /// Gets the sub report.
        /// </summary>
        /// <value>
        /// The sub report.
        /// </value>
        public virtual LinkedReport SubReport { get { return null; } }


        /// <summary>
        /// Category title to be used in ESS Report List
        /// </summary>
        public virtual string ESSCategoryTitle { get { return string.Empty; } }

        /// <summary>
        /// ESS Report detail
        /// </summary>
        public virtual ESSReportDetail ESSReportDetails { get { return null; } }

        /// <summary>
        /// Derived property to be used for ESS report filtering
        /// </summary>
        public bool IsESSReport { get { return ESSReportDetails == null ? false : true; } }
    }
}
