﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Reporting
{
    public class LinkedReport
    {
        public string ReportId { get; set; }

        public string ReportMainCategory { get; set; }

        public string ReportCategory { get; set; }
    }
}
