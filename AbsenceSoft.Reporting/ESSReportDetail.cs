﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Reporting
{
    public class ESSReportDetail
    {
        public string Title { get; set; }
        public ESSReportType Type { get; set; }
        public string ESSReportName { get; set; }
    }
}
