﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Common;
using AbsenceSoft.Common.Config;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Reporting;
using MongoDB.Bson;
using Npgsql;

namespace AbsenceSoft.Data.Reporting
{
    /// <summary>
    /// Handles query for report operations
    /// Faciliate the common query integrations
    /// </summary>
    public class ReportQueryHandler : IDisposable
    {
        StringBuilder _sb = new StringBuilder();
        bool isFirst = true;
        readonly string _customerId;
        readonly string _pt;
        readonly User _user;
        bool _hasEmployeeAccessFilter = false;
        static object syncOrgLock = new object();
        MappedColumns _map = null;
        List<string> _emplids = null;
        private Dictionary<string, object> _whereconditions;

        public Dictionary<string, object> WhereConditions
        {
            get { return _whereconditions; }
            set { _whereconditions = value; }
        }



        /// <summary>
        /// Constructor to inject dependencies
        /// </summary>
        /// <param name="primaryTable"></param>
        /// <param name="map"></param>
        /// <param name="customerId"></param>
        /// <param name="user"></param>
        /// <param name="hasEmployeeAccessFilter"></param>
        public ReportQueryHandler(string primaryTable, MappedColumns map, string customerId = "", User user = null, bool hasEmployeeAccessFilter = false)
        {
            _pt = primaryTable;
            _customerId = customerId;
            _user = user;
            _hasEmployeeAccessFilter = hasEmployeeAccessFilter;
            _map = map;
            //if user has no employer
            if (_user.Employers != null || _user.Employers.Any(e => e.IsActive))
            {
                _emplids = _user.Employers.Where(e => e.IsActive).Select(e => e.EmployerId).Distinct().ToList();
            }
            _whereconditions = new Dictionary<string, object>();

        }

        #region Utilities
        /// <summary>
        /// Get the employers from the criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="EmployerIdFilterName"></param>
        /// <returns></returns>
        private List<string> GetEmployersFromCriteria(ReportCriteria criteria, string EmployerIdFilterName = "EmployerId")
        {
            List<string> result = null;
            var employerIdFilter = criteria.Filters.FirstOrDefault(m => m.Name == EmployerIdFilterName);
            if (employerIdFilter != null && employerIdFilter.Values != null && employerIdFilter.Values.Any())
            {
                result = employerIdFilter.ValuesAs<string>();
            }
            return result;
        }
        #endregion

        #region Filters

        /// <summary>
        /// Returns the customer query to append
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        private void AddCustomerFilter(string customerId = null)
        {
            var c = _customerId;
            var CustomerQuery = $"{GetAppender()} {_pt}.{_map.CustomerIdFieldName}  =" + AppendAtSymbol(_map.CustomerIdFieldName);
            if (!string.IsNullOrWhiteSpace(customerId))
            {
                _sb.AppendLine(CustomerQuery);
                c = customerId;
                WhereConditions.Add(_map.CustomerIdFieldName, c);
            }


        }

        /// <summary>
        /// Add employer filters
        /// </summary>
        /// <param name="employerIds"></param>
        private void AddEmployerFilter(List<string> employerIds = null)
        {
            if (employerIds != null && employerIds.Any())
            {
                //+ _map.EmployerIdFieldName +")";
                var index = 0;
                var eplrParameterList = new List<string>();
                foreach (var eplrid in employerIds)
                {
                    eplrParameterList.Add(AppendAtSymbol(_map.EmployerIdFieldName) + index.ToString());
                    WhereConditions.Add((AppendAtSymbol(_map.EmployerIdFieldName) + index.ToString()), eplrid);
                    index++;
                }
                _sb.Append($"{GetAppender()} {_pt}.{_map.EmployerIdFieldName} IN (").Append(string.Join(",", eplrParameterList)).AppendLine(")");


            }
        }



        /// <summary>
        /// Add filters for employees
        /// </summary>
        /// <param name="employerIds"></param>
        /// <param name="employeeIds"></param>
        private void AddEmployeesFilter(List<string> employerIds = null, List<string> employeeIds = null)
        {
            if (_hasEmployeeAccessFilter)
            {
                if (employerIds != null && employerIds.Any() && employeeIds != null && employeeIds.Any())
                {
                    _sb.AppendLine($"{GetAppender()} ({_pt}.{_map.EmployeeIdFieldName} IN (SELECT {_pt}.{_map.EmployeeIdFieldName} FROM {_map.TableName} {_pt} WHERE {_pt}.{_map.EmployerIdFieldName} IN ({string.Join(",", employerIds.Select(p => string.Format("'{0}'", p)))}) OR ");
                    _sb.AppendLine($"{_pt}.{_map.EmployeeIdFieldName} IN ({string.Join(",", employeeIds.Select(e => string.Format("'{0}'", e)))}) )");
                    return;
                }

                if (employerIds != null && employerIds.Any())
                {
                    _sb.AppendLine($"{GetAppender()} {_pt}.{_map.EmployeeIdFieldName} IN (SELECT {_pt}.{_map.EmployeeIdFieldName} FROM {_map.TableName} {_pt} WHERE {_pt}.{_map.EmployerIdFieldName} IN ({string.Join(",", employerIds.Select(p => string.Format("'{0}'", p)))}))");
                    return;
                }

                if (employeeIds != null && employeeIds.Any())
                {
                    _sb.AppendLine($"{GetAppender()} {_pt}.{_map.EmployeeIdFieldName} IN ({string.Join(",", employeeIds.Select(e => string.Format("'{0}'", e)))})");
                    return;
                }
            }
        }

        /// <summary>
        /// Returns the list of employees for a organization
        /// </summary>
        /// <param name="orgCode"></param>
        private List<string> GetEmployeesForOrganization(string orgCode)
        {
            List<string> employeesOfOrg = new List<string>();
            List<EmployeeOrganization> userEmployeeOrganizationsAssigned = null;
            List<Employee> listEmps = new List<Employee>();

            userEmployeeOrganizationsAssigned = EmployeeOrganization.AsQueryable().Where(org => org.Code == orgCode).ToList();
            foreach (EmployeeOrganization empOrgs in userEmployeeOrganizationsAssigned)
            {
                employeesOfOrg.Add(empOrgs.EmployeeNumber);
            }

            if (employeesOfOrg != null && employeesOfOrg.Any())
            {
                foreach (var emp in employeesOfOrg)
                {
                    var listEmp = Employee.AsQueryable().Where(empl => empl.EmployeeNumber == emp);
                    if (listEmp != null && listEmp.Any())
                    {
                        listEmps.AddRange(listEmp);
                    }
                }
            }

            return listEmps.Select(e => e.Id).ToList();
        }

        /// <summary>
        /// Filter the employee user has access
        /// </summary>
        private void AddOrganizationDataAccessFilters()
        {
            Dictionary<string, List<EmployeeOrganization>> userOrganizations = null;
            Dictionary<string, List<EmployeeOrganization>> userEmployeeOrganizations = null;

            Dictionary<string, bool> viewAllEmployeesPermissioned = null;

            List<string> resultEmpIds = new List<string>();
            List<string> resultEmployerIds = new List<string>();

            //get employees for organization assigned to user
            var orgAssignedToUser = _user.Employers.Where(e => e.EmployeeId != null)?.SelectMany(a => a.OrgCodes).Distinct().ToList();
            if (orgAssignedToUser != null && orgAssignedToUser.Any())
            {

                foreach (var orgCode in orgAssignedToUser)
                {
                    resultEmpIds.AddRange(GetEmployeesForOrganization(orgCode));
                }
            }

            if (_user.Customer.HasFeature(Feature.OrgDataVisibility))
            {
                List<string> empAccessibleList = new List<string>();
                viewAllEmployeesPermissioned = _user.Employers.ToDictionary(e => e.EmployerId, e => User.Permissions.GetPermissions(_user, e.EmployerId).Contains(Permission.ViewAllEmployees.Id));

                userEmployeeOrganizations = _user.Employers.Where(e => e.EmployeeId != null && viewAllEmployeesPermissioned[e.EmployerId] == false).ToDictionary(e => e.EmployerId, e => EmployeeOrganization.AsQueryable().Where(org => org.EmployerId == e.EmployerId).ToList());
                userOrganizations = _user.Employers.Where(e => e.EmployeeId != null && viewAllEmployeesPermissioned[e.EmployerId] == false).ToDictionary(e => e.EmployerId, e => (e.EmployeeId != null ? EmployeeOrganization.AsQueryable().Where(org => org.EmployerId == e.EmployerId && org.EmployeeNumber == Employee.GetById(e.EmployeeId).EmployeeNumber).ToList() : null));

                List<EmployeeOrganization> userOrgList = new List<EmployeeOrganization>();

                foreach (List<EmployeeOrganization> orgList in userOrganizations.Values)
                {
                    if (orgList != null && orgList.Count > 0 && (!viewAllEmployeesPermissioned[orgList[0].EmployerId]))

                    {
                        userOrgList.AddRange(orgList);
                    }
                }
                foreach (List<EmployeeOrganization> empOrgs in userEmployeeOrganizations.Values)
                {
                    foreach (EmployeeOrganization empOrg in empOrgs)
                    {
                        if (empOrg != null && userOrgList.Count > 0 && _user.HasEmployeeOrganizationAccess(empOrg, userOrgList))
                        {
                            empAccessibleList.Add(empOrg.EmployeeNumber);
                        }
                    }
                }

                foreach (EmployerAccess emp in _user.Employers)
                {
                    if (ApplyEmployeeSpecificFilters())
                        resultEmpIds.AddRange(_user.Employers.Where(e => !string.IsNullOrWhiteSpace(e.EmployeeId)).Select(e => e.EmployeeId));

                    if (!viewAllEmployeesPermissioned[emp.EmployerId])
                    {

                        resultEmpIds.AddRange(Employee.AsQueryable()
                                                .Where(empl => empl.CustomerId == _user.CustomerId && empl.EmployerId == emp.EmployerId && empAccessibleList.Contains(empl.EmployeeNumber))
                                                .Select(empl => empl.Id));

                    }
                    else
                    {
                        resultEmployerIds.Add(emp.EmployerId);
                    }
                }
            }

            //add to query
            AddEmployeesFilter(employerIds: resultEmployerIds, employeeIds: resultEmpIds);
        }

        /// <summary>
        /// Validates whether employee specific filter is required or not
        /// </summary>
        /// <returns></returns>
        private bool ApplyEmployeeSpecificFilters()
        {
            if (User.Permissions.GetProjectedPermissions(_user).Contains(Permission.ViewAllEmployees.Id))
                return false;

            if (User.IsEmployeeSelfServicePortal || _user.Customer.HasFeature(Feature.ESSDataVisibilityInPortal))
                return true;

            return false;
        }

        /// <summary>
        /// Add data access filter
        /// </summary>
        /// <param name="permission"></param>
        /// <param name="filteredEmployerIds"></param>
        private void AddDataAccessFilters(string permission = null, List<string> filteredEmployerIds = null)
        {
            var noCustomerQuery = $"{GetAppender()} {_pt}.{_map.CustomerIdFieldName} IS NULL";
            var noEmployerQuery = $"{GetAppender()} {_pt}.{_map.EmployerIdFieldName} IS NULL";

            //if user is null the add null query
            if (_user == null || string.IsNullOrWhiteSpace(_user.CustomerId))
            {
                _sb.AppendLine(noCustomerQuery);
                _sb.AppendLine(noEmployerQuery);
                return;
            }
            //if user has no employer
            else if (_user.Employers == null || !_user.Employers.Any(e => e.IsActive))
            {

                AddCustomerFilter(_user.CustomerId);
                _sb.AppendLine(noEmployerQuery);
                return;
            }
            else if (_user.Employers != null || _user.Employers.Any(e => e.IsActive))
            {

                AddCustomerFilter(_user.CustomerId);


                // Filter out any that are not active

                List<string> employerIds = null;

                if (_emplids != null && _emplids.Any())
                {
                    //filter with passed filtered ids
                    if (filteredEmployerIds != null && filteredEmployerIds.Any())
                        employerIds = _emplids.Where(e => filteredEmployerIds.Contains(e)).ToList();
                    else
                        employerIds = _emplids;
                }
                // Ensure the permissions passed in are empty (meaning just all active) or that 
                //  if we have explicit intent, that the user has the appropriate permissions for that function for that employer
                if (!string.IsNullOrWhiteSpace(permission) && employerIds != null)
                    employerIds = User.Permissions.GetEmpIdsWithPermission(_user, employerIds.ToList(), permission).ToList();

                AddEmployerFilter(employerIds);

            }

            if (_user.Customer.HasFeature(Feature.OrgDataVisibility)
                || _user.Employers.Where(e => e.EmployeeId != null).Select(a => a.OrgCodes).Any()
                || ApplyEmployeeSpecificFilters())
                AddOrganizationDataAccessFilters();
        }

        /// <summary>
        /// Add the organization filter
        /// This may not be for all reports so call it separately
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="employeeIdField"></param>
        /// <param name="EmployerIdFilterName"></param>
        public void AppendOrganizationFilter(ReportCriteria criteria, string EmployerIdFilterName = "EmployerId")
        {
            //do nothing for improper parameter
            if (_user == null || criteria == null || criteria.Filters == null || !criteria.Filters.Any())
                return;

            //get organization types
            List<OrganizationType> organizationTypes = OrganizationType.AsQueryable(_user).Where(type => type.CustomerId == _user.CustomerId).ToList();

            //define
            List<string> empFilters = new List<string>();

            List<string> employerIds = null;
            var employerIdFilter = criteria.Filters.FirstOrDefault(m => m.Name == EmployerIdFilterName);
            if (employerIdFilter != null && employerIdFilter.Options != null && employerIdFilter.Options.Any())
            {
                if (employerIdFilter.Values != null && employerIdFilter.Values.Any())
                    employerIds = employerIdFilter.ValuesAs<string>();
                foreach (var employerIdOption in employerIdFilter.Options)
                {
                    organizationTypes.AddIfNotExists(new OrganizationType()
                    {
                        Code = OrganizationType.OfficeLocationTypeCode,
                        EmployerId = employerIdOption.Value.ToString(),
                        CustomerId = _user.CustomerId
                    }, c => string.Equals(c.Code, OrganizationType.OfficeLocationTypeCode, StringComparison.InvariantCultureIgnoreCase));
                }
            }
            bool orgTypeSelected = false;
            foreach (OrganizationType orgType in organizationTypes)
            {
                ReportCriteriaItem item = criteria.Filters.FirstOrDefault(m => m.Name == orgType.Code);
                if (item != null && item.Values != null && item.Values.Count > 0) orgTypeSelected = true; //Org Option selected
            }
            if (!orgTypeSelected) return;
            if (organizationTypes != null && employerIds != null && employerIds.Count > 0)
                organizationTypes = organizationTypes.Where(type => type.CustomerId == _user.CustomerId
                        && employerIds.Contains(type.EmployerId)).ToList();

            if (organizationTypes != null)
            {
                foreach (string employerId in organizationTypes.Select(type => type.EmployerId).Distinct())
                {
                    foreach (OrganizationType orgType in organizationTypes.Where(type => type.EmployerId == employerId))
                    {
                        // Add the organization filter if provided
                        var orgTypeCodeFilter = criteria.Filters.FirstOrDefault(m => m.Name == orgType.Code);
                        if (orgTypeCodeFilter != null && orgTypeCodeFilter.Values != null && orgTypeCodeFilter.Values.Any())
                        {
                            List<string> orgCodes = orgTypeCodeFilter.Values.Select(s => s.ToString()).ToList();
                            List<EmployeeOrganization> empOrgs = EmployeeOrganization.AsQueryable(_user).Where(org => org.CustomerId == _user.CustomerId && orgCodes.Contains(org.Code) && org.EmployerId == orgType.EmployerId && org.TypeCode == orgType.Code).ToList();
                            List<string> emps = empOrgs.Where(org => org.TypeCode.ToUpper().Trim() != OrganizationType.OfficeLocationTypeCode.ToUpper().Trim() || org.Dates == null || org.Dates.IncludesToday(true)).Select(org => org.EmployeeNumber).ToList();

                            //add to filter
                            empFilters.AddRange(Employee
                                                     .AsQueryable(_user)
                                                     .Where(emp => emp.CustomerId == _user.CustomerId && emp.EmployerId == orgType.EmployerId && emps.Contains(emp.EmployeeNumber))
                                                     .Select(emp => emp.Id)
                                                     .ToList());
                        }
                        else
                        {
                            List<EmployeeOrganization> empOrgs = EmployeeOrganization
                                                                        .AsQueryable(_user)
                                                                        .Where(org => org.CustomerId == _user.CustomerId && org.EmployerId == orgType.EmployerId && org.TypeCode == orgType.Code)
                                                                        .ToList();
                            List<string> emps = empOrgs.Where(org => org.TypeCode.ToUpper().Trim() != OrganizationType.OfficeLocationTypeCode.ToUpper().Trim() || org.Dates == null || org.Dates.IncludesToday(true)).Select(org => org.EmployeeNumber).ToList();
                            empFilters.AddRange(Employee
                                                    .AsQueryable(_user)
                                                    .Where(emp => emp.CustomerId == _user.CustomerId && emp.EmployerId == orgType.EmployerId && emps.Contains(emp.EmployeeNumber))
                                                    .Select(emp => emp.Id).ToList());
                        }
                    }
                }
            }

            //append to query now
            if (empFilters != null && empFilters.Any())
                AddEmployeesFilter(employeeIds: empFilters);
        }

        #endregion

        /// <summary>
        /// Append the SQL
        /// </summary>
        /// <param name="selectQuery"></param>
        /// <param name="fromQuery"></param>
        /// <param name="filterQuery"></param>
        public void AppendSql(string selectQuery, string fromQuery, string filterQuery = "")
        {
            if (string.IsNullOrWhiteSpace(selectQuery) || string.IsNullOrWhiteSpace(fromQuery))
                throw new Exception("Query not found");

            _sb.Append(selectQuery);
            _sb.AppendLine($" {fromQuery}");

            if (!string.IsNullOrWhiteSpace(filterQuery))
            {
                _sb.AppendLine($" {filterQuery}");
                isFirst = false;
            }

        }

        /// <summary>
        /// Add the order sql
        /// </summary>
        /// <param name="orderSql"></param>
        public void AppendOrderSql(string orderSql)
        {
            if (!string.IsNullOrWhiteSpace(orderSql))
                _sb.AppendLine(orderSql);
        }

        /// <summary>
        /// Add data access filter based on condition
        /// </summary>
        /// <param name="permission"></param>
        /// <param name="filteredEmployerIds"></param>
        public void AppendDataAccessFilter(string permission = null, List<string> filteredEmployerIds = null, ReportCriteria criteria = null, string EmployerIdFilterName = "EmployerId")
        {
            if (_user != null)
                AddDataAccessFilters(permission, GetEmployersFromCriteria(criteria, EmployerIdFilterName));
            else if (!string.IsNullOrWhiteSpace(_customerId))
                AddCustomerFilter();


        }

        /// <summary>
        /// Use this for any additional filters
        /// </summary>
        /// <param name="sql"></param>
        public void AppendAdditionalFilters(string sql)
        {
            if (!string.IsNullOrWhiteSpace(sql))
                _sb.AppendLine($"{GetAppender()} {sql}");
        }

        /// <summary>
        /// Get data as list of dynamic
        /// </summary>
        /// <returns></returns>
        public IList<dynamic> GetData(object parameters = null)
        {
            ReportDataHandler dao = new ReportDataHandler();
            return dao.GetReportData(_sb.ToString(), parameters);
        }

        /// <summary>
        /// Get data as strongly typed list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IList<T> GetData<T>(object parameters = null)
        {
            ReportDataHandler dao = new ReportDataHandler();
            return dao.GetReportData<T>(_sb.ToString(), parameters);
        }

        /// <summary>
        /// Get data using action injection to handle data directly from IDataReader
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="action"></param>
        public void GetAndFormatData(IList<NpgsqlParameter> parameters = null, Action<NpgsqlDataReader> action = null)
        {
            ReportDataHandler dao = new ReportDataHandler();
            dao.GetAndProcessData(_sb.ToString(), parameters, action);
        }

        /// <summary>
        /// Get data using action injection to handle data directly from IDataReader
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="action"></param>
        public void GetAndFormatData(Dictionary<string, object> parameters = null, Action<NpgsqlDataReader> action = null)
        {
            ReportDataHandler dao = new ReportDataHandler();
            dao.GetAndProcessData(_sb.ToString(), parameters, action);
        }

        /// <summary>
        /// Get data using action injection to handle data directly from IDataReader
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="action"></param>
        public void GetAndFormatData(object parameters = null, Action<NpgsqlDataReader> action = null)
        {
            ReportDataHandler dao = new ReportDataHandler();
            dao.GetAndProcessData(_sb.ToString(), parameters, action);
        }

        /// <summary>
        /// Appender for sql
        /// </summary>
        /// <returns></returns>
        private string GetAppender()
        {
            var appender = isFirst ? " WHERE " : " AND ";
            if (isFirst)
                isFirst = false;
            return appender;
        }

        /// <summary>
        /// Appends @ symbol to input string  which is used while parameterizing queries
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private string AppendAtSymbol(string input)
        {
            if (!string.IsNullOrWhiteSpace(input)) return $"@{input}";
            else return string.Empty;
        }

        /// <summary>
        /// Dispose member variables
        /// </summary>
        public void Dispose()
        {
            _sb = null;
            _map = null;
        }


        public class MappedColumns
        {
            public string CustomerIdFieldName { get; set; }
            public string EmployerIdFieldName { get; set; }
            public string EmployeeIdFieldName { get; set; }
            public string TableName { get; set; }

            /// <summary>
            /// Set the mapping field names
            /// </summary>
            /// <param name="customerIdFieldName">The customer id field name in the query</param>
            /// <param name="employerIdFieldName">The employer id field name in the query</param>
            /// <param name="employeeIdFieldName">The employee id field name in the query</param>
            /// <param name="tableName">The Table name field in the query</param>
            public MappedColumns(string customerIdFieldName, string employerIdFieldName, string employeeIdFieldName, string tableName)
            {
                CustomerIdFieldName = customerIdFieldName;
                EmployerIdFieldName = employerIdFieldName;
                EmployeeIdFieldName = employeeIdFieldName;
                TableName = tableName;
            }
        }
    }
}
