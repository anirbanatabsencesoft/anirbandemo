﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Reporting
{
    [Serializable]
    public class ReportCriteria : BaseNonEntity
    {
        [BsonConstructor]
        public ReportCriteria()
        {
            Filters = new List<ReportCriteriaItem>();
            HasCriterias = true;
            ShowAgeCriteria = false;
        }

        /// <summary>
        /// Gets or sets the inclusive start date of the date range for reporting.
        /// </summary>
        [BsonRequired, BsonDateTimeOptions(Kind = DateTimeKind.Utc, Representation = BsonType.DateTime)]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the inclusive end date of the date range for reporting.
        /// </summary>
        [BsonRequired, BsonDateTimeOptions(Kind = DateTimeKind.Utc, Representation = BsonType.DateTime)]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Gets or sets the age from for reporting
        /// </summary>
        public int? AgeFrom { get; set; }

        /// <summary>
        /// Gets or sets age to for reporting
        /// </summary>        
        public int? AgeTo { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [show age criteria].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [show age criteria]; otherwise, <c>false</c>.
        /// </value>
        public bool ShowAgeCriteria { get; set; }

        /// <summary>
        /// Gets or sets the report's Customer Id.
        /// </summary>
        [BsonRequired, BsonRepresentation(BsonType.ObjectId)]
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the criteria items outside of the start/end date for the report that are defined
        /// by the report itself and served up to be prompted, populated and used by the UI and reporting engine
        /// to filter the report criteria.
        /// </summary>
        [BsonIgnoreIfNull]
        public List<ReportCriteriaItem> Filters { get; set; }

        /// <summary>
        /// Gets the plain english representation of the report criteria and it's values.
        /// </summary>
        public string PlainEnglish { get { return ToString(); } set {/* Ignore the set, just for serialization out */} }

        /// <summary>
        /// Gets the value of a provided criteria filter.
        /// </summary>
        /// <typeparam name="T">The type to cast the value as.</typeparam>
        /// <param name="criteriaName">The name of the criteria item.</param>
        /// <returns>The type-cast value of <c>criteriaName</c> or <c>default(T)</c>.</returns>
        public T ValueOf<T>(string criteriaName)
        {
            T val = default(T);
            if (Filters != null && Filters.Any())
            {
                ReportCriteriaItem item = Filters.FirstOrDefault(f => f.Name == criteriaName);
                if (item != null)
                    val = item.ValueAs<T>();
            }
            return val;
        }//end: ValueOf<T>

        public T ApplyForOptionalFilter<T>(string criteriaName, Action<T> action)
        {
            var value = ValueOf<T>(criteriaName);
            if (value!=null && !string.IsNullOrWhiteSpace(value.ToString()) && !EqualityComparer<T>.Default.Equals(value, default(T)))
            {
                action(value);
            }
            return value;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            StringBuilder english = new StringBuilder();

            english.AppendFormat("From '{0:MM/dd/yyyy}' through '{1:MM/dd/yyyy}'", StartDate, EndDate);
            if (ShowAgeCriteria)
            {
                if (AgeFrom.HasValue || AgeTo.HasValue)
                {
                    english.Append(" where");
                }

                if (AgeFrom.HasValue && AgeTo.HasValue)
                {
                    english.Append(" Age between " + AgeFrom.Value.ToString() + " and " + AgeTo.Value.ToString());
                }
                else
                {
                    if (AgeFrom.HasValue)
                    {
                        english.Append(" Age greater than " + AgeFrom.Value.ToString());
                    }

                    if (AgeTo.HasValue)
                    {
                        english.Append(" Age less than " + AgeTo.Value.ToString());
                    }

                }
            }
            if (Filters != null && Filters.Any(f => f.Value != null || f.Values != null))
            {
                var filteredFilters = Filters.Where(f => (f.Value != null && !string.IsNullOrWhiteSpace(f.ToString())) || (f.Values != null && f.Values.Any())).ToList();
                if (filteredFilters.Any())
                {
                    if (ShowAgeCriteria)
                    {
                        if (!(AgeFrom.HasValue || AgeTo.HasValue))
                            english.Append(" where");
                    }
                    else
                        english.Append(" where");

                    for (int i = 0; i < filteredFilters.Count; i++)
                    {
                        var filter = filteredFilters[i];
                        if (i > 0 && i < (filteredFilters.Count - 1))
                            english.Append(",");
                        english.Append(" ");
                        if (i > 0 && i + 1 == filteredFilters.Count)
                            english.Append("and ");

                        english.Append(filter.ToString());
                    }
                }
            }

            return english.ToString();
        } // ToString



        /// <summary>
        /// Gets or sets a value indicating whether this instance has criterias.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has criterias; otherwise, <c>false</c>.
        /// </value>
        public bool HasCriterias { get; set; }
    }
}
