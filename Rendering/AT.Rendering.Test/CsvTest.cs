﻿using AT.Common.Rendering.Helpers;
using AT.Rendering.Test.TestResources;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;

namespace AT.Rendering.Test
{
    [TestClass]
    public class CsvTest
    {
        private readonly IList<TestData> testData;
        private readonly ICollection<object> testCollectionData;
        private readonly IList<string> testFileList;
        private readonly string path;
        /// <summary>
        /// Constructor to set all default values
        /// </summary>
        public CsvTest()
        {
            testData = new List<TestData>
            {
              new TestData{ FirstName = "Jennifer", LastName = "Tilly", DoB = new DateTime(1970, 6, 5), HireDate = new DateTime(2011, 2, 1), Gender = "Female" },
              new TestData{ FirstName = "Alan", LastName = "Skillman", DoB = new DateTime(1979, 5, 12), HireDate = new DateTime(2007, 8, 16), Gender = "Male" },
              new TestData{ FirstName = "Tony", LastName = "gonzalez", DoB = new DateTime(1979, 2, 15), HireDate = new DateTime(2009, 8, 16), Gender = "Male" },
              new TestData{ FirstName = "Frank", LastName = "Dikinson", DoB = new DateTime(1977, 11, 28), HireDate = new DateTime(2002, 6, 17), Gender = "Male" },
              new TestData{ FirstName = "Joanna", LastName = "Grant", DoB = new DateTime(1974, 5, 28), HireDate = new DateTime(2010, 4, 13), Gender = "Female" },
              new TestData{ FirstName = "Kelly", LastName = "Tilly", DoB = new DateTime(1976, 8, 20), HireDate = new DateTime(2009, 8, 16), Gender = "Female" },
              new TestData{ FirstName = "Tinna", LastName = "Hall", DoB = new DateTime(1979, 12, 11), HireDate = new DateTime(2012, 8, 12), Gender = "Female" }
            };
            testCollectionData = new Collection<object>();
            testCollectionData = CreateTestCollectionData(testCollectionData);
            testFileList = new List<string>();
            path = Environment.CurrentDirectory;
        }


        /// <summary>
        /// Test the creation of CSV from json data
        /// </summary>
        [TestMethod]
        public void CreateCsvFromJsonData()
        {
            string fileName = Path.Combine(path, "CreateCsvFromJsonData.csv");
            var jsonData = TestResources.RenderingResources.data;
            string json = "";
            using (StreamReader r = new StreamReader(new MemoryStream(jsonData)))
            {
                json = r.ReadToEnd();
            }
            var convertedJson = JsonConvert.DeserializeObject(json) as JArray;
            File.WriteAllBytes(fileName, CsvHelper.GenerateCSVFromJson(convertedJson, "Employee Report", "All data"));
            testFileList.Add(fileName);
            Assert.IsTrue(File.Exists(fileName) && new FileInfo(fileName).Length != 0);
        }

        /// <summary>
        /// Test the creation of CSV from json Grouped data
        /// </summary>
        [TestMethod]
        public void CreateCsvFromJsonGroupedData()
        {
            string fileName = Path.Combine(path, "CreateCsvFromJsonGroupedData.csv");
            var jsonData = TestResources.RenderingResources.GroupData;
            string json = "";
            using (StreamReader r = new StreamReader(new MemoryStream(jsonData)))
            {
                json = r.ReadToEnd();
            }
            var convertedJson = JsonConvert.DeserializeObject(json) as JArray;
            File.WriteAllBytes(fileName, CsvHelper.GenerateCSVFromJson(convertedJson, "Employee Report with Grouping", "All data from specific date range"));
            testFileList.Add(fileName);
            Assert.IsTrue(File.Exists(fileName) && new FileInfo(fileName).Length != 0);
        }

        /// <summary>
        /// Create dummy list of collection
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private ICollection<object> CreateTestCollectionData(ICollection<object> data)
        {
            data.Add(new
            {
                FirstName = ",Jenn'ifer",
                LastName = "Ti" + "\n"
               + "lly",
                DoB = new DateTime(1970, 6, 5),
                HireDate = new DateTime(2011, 2, 1),
                Gender = "Female"
            });
            data.Add(new { FirstName = "Al,an", LastName = "Sk\"illman", DoB = new DateTime(1979, 5, 12), HireDate = new DateTime(2007, 8, 16), Gender = "Male" });
            data.Add(new { FirstName = "To,ny", LastName = "gonzal,''ez", DoB = new DateTime(1979, 2, 15), HireDate = new DateTime(2009, 8, 16), Gender = "Male" });
            data.Add(new { FirstName = "Fra\"nk", LastName = "Dikin,,son", DoB = new DateTime(1977, 11, 28), HireDate = new DateTime(2002, 6, 17), Gender = "Male" });
            data.Add(new { FirstName = ",Joanna", LastName = "Gr,ant", DoB = new DateTime(1974, 5, 28), HireDate = new DateTime(2010, 4, 13), Gender = "Female" });
            data.Add(new { FirstName = "Kelly", LastName = "Tilly", DoB = new DateTime(1976, 8, 20), HireDate = new DateTime(2009, 8, 16), Gender = "Female" });
            data.Add(new { FirstName = "Ti\"nna", LastName = "Hall", DoB = new DateTime(1979, 12, 11), HireDate = new DateTime(2012, 8, 12), Gender = "Female" });
            return data;
        }

        /// <summary>
        /// Test creation of Csv files with differnt parameters
        /// </summary>
        [TestMethod]
        public void CreateCsvFromPersonCsv()
        {
            string fileName = Path.Combine(path, "csv_test.csv");
            File.WriteAllBytes(fileName, CsvHelper.GenerateCSV<TestData>(testData, "Report Name", "Report Criteria"));
            testFileList.Add(fileName);
            Assert.IsTrue(File.Exists(fileName) && new FileInfo(fileName).Length != 0);

            fileName = Path.Combine(path, "csv1_test.csv");
            File.WriteAllBytes(fileName, CsvHelper.GenerateCSV<TestData>(testData));
            testFileList.Add(fileName);
            Assert.IsTrue(File.Exists(fileName) && new FileInfo(fileName).Length != 0);

            fileName = Path.Combine(path, "csv2_test.csv");
            File.WriteAllBytes(fileName, CsvHelper.GenerateCSV<TestData>(testData, "Report Name"));
            testFileList.Add(fileName);
            Assert.IsTrue(File.Exists(fileName) && new FileInfo(fileName).Length != 0);

            fileName = Path.Combine(path, "csv3_test.csv");
            File.WriteAllBytes(fileName, CsvHelper.GenerateCSV<TestData>(testData, null, "Report Criteria"));
            testFileList.Add(fileName);
            Assert.IsTrue(File.Exists(fileName) && new FileInfo(fileName).Length != 0);

            fileName = Path.Combine(path, "csv4_test.csv");
            File.WriteAllBytes(fileName, CsvHelper.GenerateCSV<TestData>(testData, null, null, false));
            testFileList.Add(fileName);
            Assert.IsTrue(File.Exists(fileName) && new FileInfo(fileName).Length != 0);
        }

        /// <summary>
        /// Test Csv creation from collection with data
        /// </summary>
        [TestMethod]
        public void CreateCsvFromCollectionWithData()
        {
            string fileName = Path.Combine(path, "csvwithdata.csv");
            File.WriteAllBytes(fileName, CsvHelper.GenerateCSV<object>(testCollectionData, "Employee Report", "Report Criteria"));
            testFileList.Add(fileName);
            Assert.IsTrue(File.Exists(fileName) && new FileInfo(fileName).Length != 0);
        }

        /// <summary>
        /// Test Csv creation from collection without data
        /// </summary>
        [TestMethod]
        public void CreateCsvFromCollectionWithoutData()
        {
            ICollection<object> blankObject = new Collection<object>();
            string fileName = Path.Combine(path, "csvwithoutdata.csv");
            File.WriteAllBytes(fileName, CsvHelper.GenerateCSV<object>(blankObject, "Employee Report", "Report Criteria"));
            testFileList.Add(fileName);
            Assert.IsTrue(File.Exists(fileName) && new FileInfo(fileName).Length != 0);
        }

        /// <summary>
        /// Clean up all the files generated in test
        /// </summary>
        [TestCleanup]
        public void DeleteAllCreatedFiles()
        {
            foreach (var item in testFileList)
            {
                if (File.Exists(item))
                {
                    File.Delete(item);
                }
            }
        }
    }
}
