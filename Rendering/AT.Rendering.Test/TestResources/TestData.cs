﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Rendering.Test.TestResources
{
    /// <summary>
    /// Test object 
    /// </summary>
    internal class TestData
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DoB { get; set; }
        public DateTime HireDate { get; set; }
        public string Gender { get; set; }
    }
}
