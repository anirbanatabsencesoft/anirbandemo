﻿using AbsenceSoft.Rendering;
using AT.Common.Rendering.Helpers;
using AT.Rendering.Test.TestResources;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;

namespace AT.Rendering.Test
{

    [TestClass]
    public class PdfTest
    {

        private readonly IList<TestData> testData;
        private readonly IList<string> testFileList;
        private readonly string path;
        /// <summary>
        /// Constructor to set all default values
        /// </summary>
        public PdfTest()
        {
            testFileList = new List<string>();
            path = Environment.CurrentDirectory;
            testData = new List<TestData> {
              new TestData{ FirstName = "Jennifer", LastName = "Tilly", DoB = new DateTime(1970, 6, 5), HireDate = new DateTime(2011, 2, 1), Gender = "Female" },
              new TestData{ FirstName = "Alan", LastName = "Skillman", DoB = new DateTime(1979, 5, 12), HireDate = new DateTime(2007, 8, 16), Gender = "Male" },
              new TestData{ FirstName = "Tony", LastName = "gonzalez", DoB = new DateTime(1979, 2, 15), HireDate = new DateTime(2009, 8, 16), Gender = "Male" },
              new TestData{ FirstName = "Frank", LastName = "Dikinson", DoB = new DateTime(1977, 11, 28), HireDate = new DateTime(2002, 6, 17), Gender = "Male" },
              new TestData{ FirstName = "Joanna", LastName = "Grant", DoB = new DateTime(1974, 5, 28), HireDate = new DateTime(2010, 4, 13), Gender = "Female" },
              new TestData{ FirstName = "Kelly", LastName = "Tilly", DoB = new DateTime(1976, 8, 20), HireDate = new DateTime(2009, 8, 16), Gender = "Female" },
              new TestData{ FirstName = "Tinna", LastName = "Hall", DoB = new DateTime(1979, 12, 11), HireDate = new DateTime(2012, 8, 12), Gender = "Female" } };
        }

        /// <summary>
        /// Test the creation of PDF from test data
        /// </summary>
        [TestMethod]
        public void CreatePdfFromTestData()
        {
            string fileName = Path.Combine(path, "CreatePdfFromTestData.pdf");
            File.WriteAllBytes(fileName, PdfHelper.ConvertCollectionToPdf<TestData>(testData, "Employee Report", "All data"));
            testFileList.Add(fileName);
            Assert.IsTrue(File.Exists(fileName) && new FileInfo(fileName).Length != 0);
        }


        /// <summary>
        /// Test the creation of PDF from json data
        /// </summary>
        [TestMethod]
        public void CreatePdfFromJsonData()
        {
            string fileName = Path.Combine(path, "CreatePdfFromJsonData.pdf");
            var jsonData = TestResources.RenderingResources.data;
            string json = "";
            using (StreamReader r = new StreamReader(new MemoryStream(jsonData)))
            {
                json = r.ReadToEnd();
            }
            var convertedJson = JsonConvert.DeserializeObject(json) as JArray;
            File.WriteAllBytes(fileName, PdfHelper.ConvertJsonToPdf(convertedJson, "Employee Report", "All data"));
            testFileList.Add(fileName);
            Assert.IsTrue(File.Exists(fileName) && new FileInfo(fileName).Length != 0);
        }

        /// <summary>
        /// Test the creation of PDF from json Grouped data
        /// </summary>
        [TestMethod]
        public void CreatePdfFromJsonGroupedData()
        {
            string fileName = Path.Combine(path, "CreatePdfFromJsonGroupedData.pdf");
            var jsonData = TestResources.RenderingResources.GroupData;
            string json = "";
            using (StreamReader r = new StreamReader(new MemoryStream(jsonData)))
            {
                json = r.ReadToEnd();
            }
            var convertedJson = JsonConvert.DeserializeObject(json) as JArray;
            File.WriteAllBytes(fileName, PdfHelper.ConvertJsonToPdf(convertedJson, "Employee Report with Grouping", "All data from specific date range"));
            testFileList.Add(fileName);
            Assert.IsTrue(File.Exists(fileName) && new FileInfo(fileName).Length != 0);
        }

        /// <summary>
        /// Test the creation of PDF from collection
        /// </summary>
        [TestMethod]
        public void CreatePdfFromCollectionWithData()
        {
            ICollection<object> testCollection = new Collection<object>();
            testCollection.Add(new { FirstName = "Jennifer", LastName = "Tilly", DoB = new DateTime(1970, 6, 5), HireDate = new DateTime(2011, 2, 1), Gender = "Female" });
            testCollection.Add(new { FirstName = "Alan", LastName = "Skillman", DoB = new DateTime(1979, 5, 12), HireDate = new DateTime(2007, 8, 16), Gender = "Male" });
            testCollection.Add(new { FirstName = "Tony", LastName = "gonzalez", DoB = new DateTime(1979, 2, 15), HireDate = new DateTime(2009, 8, 16), Gender = "Male" });
            testCollection.Add(new { FirstName = "Frank", LastName = "Dikinson", DoB = new DateTime(1977, 11, 28), HireDate = new DateTime(2002, 6, 17), Gender = "Male" });
            testCollection.Add(new { FirstName = "Joanna", LastName = "Grant", DoB = new DateTime(1974, 5, 28), HireDate = new DateTime(2010, 4, 13), Gender = "Female" });
            testCollection.Add(new { FirstName = "Kelly", LastName = "Tilly", DoB = new DateTime(1976, 8, 20), HireDate = new DateTime(2009, 8, 16), Gender = "Female" });
            testCollection.Add(new { FirstName = "Tinna", LastName = "Hall", DoB = new DateTime(1979, 12, 11), HireDate = new DateTime(2012, 8, 12), Gender = "Female" });

            string fileName = Path.Combine(path, "CreatePdfFromCollectionWithData.pdf");
            File.WriteAllBytes(fileName, PdfHelper.ConvertCollectionToPdf<object>(testCollection, "Employee Report", "All data"));
            testFileList.Add(fileName);
            Assert.IsTrue(File.Exists(fileName) && new FileInfo(fileName).Length != 0);
        }
        /// <summary>
        /// Test the creation of PDF from blank collection
        /// </summary>
        [TestMethod]
        public void CreatePdfFromCollectionWithoutData()
        {
            ICollection<object> blankCollection = new Collection<object>();
            string fileName = Path.Combine(path, "CreatePdfFromCollectionWithOutData.pdf");
            File.WriteAllBytes(fileName, PdfHelper.ConvertCollectionToPdf<object>(blankCollection, "Employee Report", "All data"));
            testFileList.Add(fileName);
            Assert.IsTrue(File.Exists(fileName) && new FileInfo(fileName).Length != 0);
        }

        /// <summary>
        /// Test to create PDF from HTML
        /// </summary>

        [TestMethod]
        public void RenderMultiplePdfDocumentsTest()
        {
            string fileName = Path.Combine(path, "pdf_1.pdf");
            File.WriteAllBytes(fileName, PdfHelper.ConvertHtmlToPdf("<p>Hi, this is some html!</p>"));
            testFileList.Add(fileName);
            Assert.IsTrue(File.Exists(fileName) && new FileInfo(fileName).Length != 0);

            fileName = Path.Combine(path, "pdf_2.pdf");
            File.WriteAllBytes(fileName, PdfHelper.ConvertHtmlToPdf("<html><head><style type='text/css'>body { font-size: 3em; }</style></head><body><p>Really Big Text!</p><p style=\"font-size:50%;\"><a href=\"rootMe.html\">Rooted Hyperlink</a></p></body></html>"));
            testFileList.Add(fileName);
            Assert.IsTrue(File.Exists(fileName) && new FileInfo(fileName).Length != 0);

            fileName = Path.Combine(path, "pdf_3.pdf");
            File.WriteAllBytes(fileName, PdfHelper.ConvertHtmlToPdf("<p>Wow, another Pdf document, go figure.</p>"));
            testFileList.Add(fileName);
            Assert.IsTrue(File.Exists(fileName) && new FileInfo(fileName).Length != 0);
        }

        /// <summary>
        /// Test to Join multiple Pdf Documents in one Pdf
        /// </summary>
        [TestMethod]
        public void JoinMultiplePdfDocumentsTest()
        {
            string fileName = Path.Combine(path, "pdf_4.pdf");
            var pdf1 = PdfHelper.ConvertHtmlToPdf("<p>Hi, this is some html!</p>");
            var pdf2 = PdfHelper.ConvertHtmlToPdf("<html><head><style type='text/css'>body { font-size: 3em; }</style></head><body><p>Really Big Text!</p><p style=\"font-size:50%;\"><a href=\"rootMe.html\">Rooted Hyperlink</a></p></body></html>");
            var pdf3 = PdfHelper.ConvertHtmlToPdf("<p>Wow, another Pdf document, go figure.</p>");
            File.WriteAllBytes(fileName, PdfHelper.MergePdfDocuments(pdf1, pdf2, pdf3));
            testFileList.Add(fileName);
            Assert.IsTrue(File.Exists(fileName) && new FileInfo(fileName).Length != 0);
        }

        /// <summary>
        /// Test OSHA PDF with stamp
        /// </summary>
        [TestMethod]
        public void GenerateOSHA301()
        {
            string pdfFileName = Path.Combine(path, "OSHA301.pdf");
            var pdf = PopulateOSHA301();
            File.WriteAllBytes(pdfFileName, pdf);
            testFileList.Add(pdfFileName);
            Assert.IsTrue(File.Exists(pdfFileName) && new FileInfo(pdfFileName).Length != 0);
        }

        /// <summary>
        /// Populates the OSHA Form 301 document PDF and returns the resulting document.
        /// </summary>
        /// <returns>Return byte array of the pdf</returns>
        private byte[] PopulateOSHA301()
        {
            var pdf = TestResources.RenderingResources.OSHA301;
            pdf = PdfHelper.StampPdf(pdf, BuildOSHAForm301Stamps());
            return pdf;
        }
        /// <summary>
        /// Create PDF stamps
        /// </summary>
        /// <returns>returns the List of PDFstamps</returns>
        private IEnumerable<PdfStamp> BuildOSHAForm301Stamps()
        {
            string WorkPhone = "9922933003";
            var myCase = new
            {
                Id = "999990000000000000000002",
                CaseNumber = "222222222",
                Employee = new
                {
                    Id = "999990000000000000000002",
                    FirstName = "Jennifer",
                    LastName = "Tilly",
                    FullName = "Jennifer Tilly",
                    DoB = new DateTime(1979, 2, 15),
                    HireDate = new DateTime(2009, 8, 16),
                    Gender = "Female",
                    Info = new
                    {
                        Address = new
                        {
                            Address1 = "123 Child's Play Ave.",
                            StreetAddress = "Apt 456",
                            City = "Denver",
                            State = "CO",
                            PostalCode = "80021",
                            Country = "US"
                        }
                    }
                },
                WorkRelated = new
                {
                    Classification = "Death",
                    DateOfDeath = new DateTime(2054, 8, 23),
                    DaysAwayFromWork = 8,
                    DaysOnJobTransferOrRestriction = 30,
                    EmergencyRoom = false,
                    HealthCarePrivate = false,
                    HospitalizedOvernight = false,
                    IllnessOrInjuryDate = new DateTime(2016, 1, 6),
                    Provider = new
                    {
                        Contact = new
                        {
                            CompanyName = "St. Mt. Helens Health Screwing Facility",
                            FirstName = "Bob",
                            LastName = "Misterfancypanceporsche",
                            FullName = "Bob Misterfancypanceporsche",
                            Title = "Dr.",
                            Address = new
                            {
                                StreetAddress = "8974 Orange Grove Street",
                                City = "Somewhere Else",
                                State = "GA",
                                PostalCode = "45698",
                                Country = "US"
                            }
                        },
                        ContactTypeCode = "PROVIDER",
                        ContactTypeName = "Provider"
                    },
                    Reportable = true,
                    Sharps = false,
                    TimeEmployeeBeganWork = "8:00 AM",
                    TimeOfEvent = "",
                    TypeOfInjury = "Injury",
                    InjuryOrIllness = "Busted head and lacerated eyeball, minor cuts and abraisions, missing brain pieces, a lobbed off ear, some broken toes, a broken pinky finger and of course death, then undeath, then some zombie like state of being.",
                    ActivityBeforeIncident = "They were carrying scissors and it was bad and they were running with those scissors and it was all pretty nightmarish you could hear their feet pounding in the warehouse and we all just knew the were going to die",
                    WhatHappened = "Well, the scissors came up and poked out thier eyeball and went into the dude's brain and it was a bloody mess but he was OK and we all laughed 'cause that goofy just always doing stuff like that and it's funny",
                    WhatHarmedTheEmployee = "Scissors mainly, but also a mop, the floor, his own ignorance, a washcloth some water that was left out on the floor, poison, chemicals and a host of other deadly things.",
                    WhereOccurred = "In the library, with the candlestick by Col. Mustard and Mrs. Peacock"
                }
            };

            yield return new PdfStamp() { Value = "Test User", FontSize = 12f, X = 80.34f, Y = 130f, MaxHeight = 24f, MaxWidth = 218f, AutoFit = true };

            yield return new PdfStamp() { Value = "Test Job Title", FontSize = 12f, X = 50f, Y = 105f, MaxHeight = 24f, MaxWidth = 218f, AutoFit = true };

            yield return new PdfStamp() { Value = String.Format(WorkPhone.Length == 9 ? "{0:  ###    ###     ####}" : "{0: ###   ###     ####}", Int64.Parse(WorkPhone)), FontSize = 12f, X = 50f, Y = 77f, MaxHeight = 24f, MaxWidth = 218f, AutoFit = true };

            yield return new PdfStamp() { Value = DateTime.Today, Format = "{0:MM}", FontSize = 12f, X = 202f, Y = 77f, MaxHeight = 24f, MaxWidth = 24f, AutoFit = true };
            yield return new PdfStamp() { Value = DateTime.Today, Format = "{0:dd}", FontSize = 12f, X = 228f, Y = 77f, MaxHeight = 24f, MaxWidth = 24f, AutoFit = true };
            yield return new PdfStamp() { Value = DateTime.Today, Format = "{0:yyyy}", FontSize = 12f, X = 249.16f, Y = 77f, MaxHeight = 24f, MaxWidth = 30f, AutoFit = true };

            if (myCase.Employee != null)
            {

                yield return new PdfStamp() { Value = myCase.Employee.FullName, FontSize = 12f, X = 338.34f, Y = 467f, MaxHeight = 24f, MaxWidth = 218f, AutoFit = true };

                yield return new PdfStamp() { Value = myCase.Employee.Info.Address.StreetAddress, FontSize = 12f, X = 325.47f, Y = 442.34f, MaxHeight = 24f, MaxWidth = 226.7f, AutoFit = true };
                yield return new PdfStamp() { Value = myCase.Employee.Info.Address.City, FontSize = 12f, X = 319.28f, Y = 418.85f, MaxHeight = 24f, MaxWidth = 130.03f, AutoFit = true };
                yield return new PdfStamp() { Value = myCase.Employee.Info.Address.State, FontSize = 12f, X = 471.09f, Y = 418.85f, MaxHeight = 24f, MaxWidth = 29.27f, AutoFit = true };
                yield return new PdfStamp() { Value = myCase.Employee.Info.Address.PostalCode, FontSize = 12f, X = 518.06f, Y = 418.85f, MaxHeight = 24f, MaxWidth = 45.61f, AutoFit = true };

                yield return new PdfStamp() { Value = myCase.Employee.DoB, Format = "{0:MM}", FontSize = 12f, X = 347.6f, Y = 396.77f, MaxHeight = 24f, MaxWidth = 24f, AutoFit = true };
                yield return new PdfStamp() { Value = myCase.Employee.DoB, Format = "{0:dd}", FontSize = 12f, X = 375.6f, Y = 396.77f, MaxHeight = 24f, MaxWidth = 24f, AutoFit = true };
                yield return new PdfStamp() { Value = myCase.Employee.DoB, Format = "{0:yyyy}", FontSize = 12f, X = 394.76f, Y = 396.77f, MaxHeight = 24f, MaxWidth = 30f, AutoFit = true };

                yield return new PdfStamp() { Value = myCase.Employee.HireDate, Format = "{0:MM}", FontSize = 12f, X = 346.21f, Y = 384.14f, MaxHeight = 24f, MaxWidth = 24f, AutoFit = true };
                yield return new PdfStamp() { Value = myCase.Employee.HireDate, Format = "{0:dd}", FontSize = 12f, X = 374.21f, Y = 384.14f, MaxHeight = 24f, MaxWidth = 24f, AutoFit = true };
                yield return new PdfStamp() { Value = myCase.Employee.HireDate, Format = "{0:yyyy}", FontSize = 12f, X = 393.37f, Y = 384.14f, MaxHeight = 24f, MaxWidth = 30f, AutoFit = true };

                yield return new PdfStamp() { Value = myCase.Employee.Gender == "Male", FontSize = 8f, X = 303.99f, Y = 374.78f, MaxHeight = 16f, MaxWidth = 16f, AutoFit = true };

                yield return new PdfStamp() { Value = myCase.Employee.Gender == "Female", FontSize = 8f, X = 303.99f, Y = 361.6f, MaxHeight = 16f, MaxWidth = 16f, AutoFit = true };
            }

            if (myCase.WorkRelated != null)
            {

                if (myCase.WorkRelated.Provider != null && myCase.WorkRelated.Provider.Contact != null)
                {

                    yield return new PdfStamp() { Value = myCase.WorkRelated.Provider.Contact.FullName, FontSize = 12f, X = 303.99f, Y = 251.9f, MaxHeight = 24f, MaxWidth = 250.77f, AutoFit = true };

                    yield return new PdfStamp() { Value = myCase.WorkRelated.Provider.Contact.CompanyName, FontSize = 12f, X = 338.34f, Y = 212.94f, MaxHeight = 24f, MaxWidth = 218f, AutoFit = true };

                    if (myCase.WorkRelated.Provider.Contact.Address != null)
                    {
                        yield return new PdfStamp() { Value = myCase.WorkRelated.Provider.Contact.Address.StreetAddress, FontSize = 12f, X = 325.47f, Y = 189.9f, MaxHeight = 24f, MaxWidth = 226.7f, AutoFit = true };
                        yield return new PdfStamp() { Value = myCase.WorkRelated.Provider.Contact.Address.City, FontSize = 12f, X = 319.28f, Y = 166.3f, MaxHeight = 24f, MaxWidth = 130.03f, AutoFit = true };
                        yield return new PdfStamp() { Value = myCase.WorkRelated.Provider.Contact.Address.State, FontSize = 12f, X = 471.09f, Y = 166.3f, MaxHeight = 24f, MaxWidth = 29.27f, AutoFit = true };
                        yield return new PdfStamp() { Value = myCase.WorkRelated.Provider.Contact.Address.PostalCode, FontSize = 12f, X = 518.06f, Y = 166.3f, MaxHeight = 24f, MaxWidth = 45.61f, AutoFit = true };
                    }
                }

                yield return new PdfStamp() { Value = myCase.WorkRelated.EmergencyRoom == true, FontSize = 8f, X = 303.99f, Y = 141.57f, MaxHeight = 16f, MaxWidth = 16f, AutoFit = true };
                yield return new PdfStamp() { Value = myCase.WorkRelated.EmergencyRoom == false, FontSize = 8f, X = 303.99f, Y = 130.39f, MaxHeight = 16f, MaxWidth = 16f, AutoFit = true };

                yield return new PdfStamp() { Value = myCase.WorkRelated.HospitalizedOvernight == true, FontSize = 8f, X = 303.99f, Y = 97.58f, MaxHeight = 16f, MaxWidth = 16f, AutoFit = true };
                yield return new PdfStamp() { Value = myCase.WorkRelated.HospitalizedOvernight == false, FontSize = 8f, X = 303.99f, Y = 85.4f, MaxHeight = 16f, MaxWidth = 16f, AutoFit = true };

                yield return new PdfStamp() { Value = myCase.CaseNumber, FontSize = 12f, X = 699.08f, Y = 466.45f, MaxHeight = 24f, MaxWidth = 90.27f, AutoFit = true };

                yield return new PdfStamp() { Value = myCase.WorkRelated.IllnessOrInjuryDate, Format = "{0:MM}", FontSize = 12f, X = 703.08f, Y = 451.09f, MaxHeight = 24f, MaxWidth = 24f, AutoFit = true };
                yield return new PdfStamp() { Value = myCase.WorkRelated.IllnessOrInjuryDate, Format = "{0:dd}", FontSize = 12f, X = 727.08f, Y = 451.09f, MaxHeight = 24f, MaxWidth = 24f, AutoFit = true };
                yield return new PdfStamp() { Value = myCase.WorkRelated.IllnessOrInjuryDate, Format = "{0:yyyy}", FontSize = 12f, X = 753.24f, Y = 451.09f, MaxHeight = 24f, MaxWidth = 30f, AutoFit = true };

                yield return new PdfStamp() { Value = myCase.WorkRelated.TimeEmployeeBeganWork, FontSize = 12f, X = 699.08f, Y = 436.27f, MaxHeight = 24f, MaxWidth = 76.27f, AutoFit = true };

                yield return new PdfStamp() { Value = myCase.WorkRelated.TimeOfEvent, FontSize = 12f, X = 699.08f, Y = 417.07f, MaxHeight = 24f, MaxWidth = 76.27f, AutoFit = true };

                yield return new PdfStamp() { Value = !myCase.WorkRelated.TimeOfEvent.Equals(string.Empty), FontSize = 8f, X = 815.21f, Y = 426.26f, MaxHeight = 16f, MaxWidth = 16f, AutoFit = true };

                yield return new PdfStamp() { Value = myCase.WorkRelated.ActivityBeforeIncident, FontSize = 10f, X = 593.17f, Y = 334.33f, MaxHeight = 50f, MaxWidth = 384f, AutoFit = true };

                yield return new PdfStamp() { Value = myCase.WorkRelated.WhatHappened, FontSize = 10f, X = 593.17f, Y = 255.32f, MaxHeight = 50f, MaxWidth = 384f, AutoFit = true };

                yield return new PdfStamp() { Value = myCase.WorkRelated.InjuryOrIllness, FontSize = 10f, X = 593.17f, Y = 175.75f, MaxHeight = 50f, MaxWidth = 384f, AutoFit = true };

                yield return new PdfStamp() { Value = myCase.WorkRelated.WhatHarmedTheEmployee, FontSize = 10f, X = 593.17f, Y = 92.34f, MaxHeight = 60f, MaxWidth = 384f, AutoFit = true };

                yield return new PdfStamp() { Value = myCase.WorkRelated.DateOfDeath, Format = "{0:MM}", FontSize = 12f, X = 829.67f, Y = 73.57f, MaxHeight = 24f, MaxWidth = 18f, AutoFit = true };
                yield return new PdfStamp() { Value = myCase.WorkRelated.DateOfDeath, Format = "{0:dd}", FontSize = 12f, X = 857.67f, Y = 73.57f, MaxHeight = 24f, MaxWidth = 18f, AutoFit = true };
                yield return new PdfStamp() { Value = myCase.WorkRelated.DateOfDeath, Format = "{0:yyyy}", FontSize = 12f, X = 876.83f, Y = 73.57f, MaxHeight = 24f, MaxWidth = 30f, AutoFit = true };
            }
        }
        /// <summary>
        /// Clean up all the files generated in test
        /// </summary>
        [TestCleanup]
        public void DeleteAllCreatedFiles()
        {
            foreach (var item in testFileList)
            {
                if (File.Exists(item))
                {
                    File.Delete(item);
                }
            }
        }
    }
}
