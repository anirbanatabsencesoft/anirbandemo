﻿using Aspose.Pdf;
using Aspose.Pdf.Facades;
using Aspose.Pdf.Forms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using AbsenceSoft.Common;
using AbsenceSoft;

namespace AT.Common.Rendering.Helper
{
    public class Form100PdfHelper
    {
        #region FORM 100 Fields
        public const string SSN = "1 Social Security Number";
        public const string DateOfInjury = "2 Date of injury";
        public const string EmployeeName = "3 Employee name Last First MI";
        public const string AddressStreet = "4 Address Number  Street";
        public const string City = "5 City";
        public const string State = "State";
        public const string PostalCode = "ZIP";
        public const string DOB = "8 Date of birth MMDDYYYY";
        public const string Gender = "Check Box";
        public const string Dependents = "Dependents";
        public const string Telephone = "TelephoneNumber";
        public const string TaxFilingStatus = "CheckBox1";
        public const string EmployerName = "13 Employer name";
        public const string EmployerFederalId = "14 Federal ID Number";
        public const string InjuryLocation = "15 Injury location code";
        public const string MailingLocation = "16 Mailing location code";
        public const string UINumber = "17 UI number";
        public const string TypeOfBusiness = "18 Type of business SICNAICS";
        public const string EmployerStreet = "19 Employer street address";
        public const string EmployerCity = "20 City";
        public const string EmployerState = "21.State";
        public const string EmployerPostalCode = "22.ZIP";
        public const string InsuranceCompany = "23.InsCo";
        public const string InsuranceCoTelephone = "24.InsCoTele";

        public const string LastDayWorked = "25 Last day worked";
        public const string DateEmployeeReturnedToWork = "26 Date employee returned to work if applicable";
        public const string DidEmployeeDie = "CheckBox2";
        public const string DateOfDeath = "28 If yes date of death";
        public const string InjuryCity = "InjuryCity";
        public const string InjuryState = "30.InjuryState";
        public const string InjuryCounty = "31 Injury county";
        public const string InjuryAtEmployerPremises = "CheckBox3";
        public const string CaseNumber = "33 Case number from OSHAMIOSHA log";
        public const string TimeEmployeeBeganWork = "Time";
        public const string TimeAMPM = "CheckBox4";
        public const string TimeOfEvent = "35.Time";
        public const string EvenTimeAMPM = "CheckBox5";
        public const string TimeCannotBeDetermined = "If time cannot be determined";
        public const string EmployeeReasonBeforeIncident = "36 What was the employee doing just before the incident occurred?";
        public const string HowDidInjuryOccur = "37 How did the injury occur?";
        public const string NatureOfInjury = "38 Describe the nature of injury or illness";
        public const string PartOfBodyAffected = "39 Part of body directly affected by the injury or illness";
        public const string WhatObjectHarmedEmployee = "40 What object or substance directly harmed the employee?";
        public const string NameOfPhysician = "41 Name of physician or other health care professional";
        public const string TreatedInEmergencyRoom = "CheckBox6";
        public const string EmployeeHospitalizedOvernight = "CheckBox7";
        public const string TreatmentLocation = "TreatmentLocation";

        public const string DateHired = "45 Date hired";
        public const string TotalGrossWeeklyWage = "46 Total gross weekly wage highest 39 of 52";
        public const string NoWeeks = "NoWeeks";
        public const string DiscountBenefits = "DiscBenefits";
        public const string Occupation = "49 Occupation Be specific";
        public const string EmployeeVolunterWorker = "Check Box8";
        public const string EmployeeHandicapped = "CheckBox9";
        public const string DateEmployerNotified = "52 Date employer notified by employee";
        public const string TempAgency = "TempAgency";
        public const string PreparersName = "54 Preparers name Please print or type";
        public const string TelephoneNumber = "56 Telephone number";
        public const string DatePrepared = "57 Date prepared";
        #endregion

        #region Private members
        private readonly string pdfFileName;
        private readonly Aspose.Pdf.Facades.Form form;
        private readonly Field[] fields;
        #endregion

        /// <summary>
        /// Form 100 Pdf Helper constructor which helps you initiate helper class
        /// that will help you create set the fields of Form 100 and then
        /// create PDF document once all the fields are set
        /// </summary>
        /// <param name="pdf"></param>
        public Form100PdfHelper(byte[] pdf)
        {
            try
            {
                pdfFileName = Path.GetTempPath() + "Form100-filled.pdf";

                form = new Aspose.Pdf.Facades.Form(pdf.ToStream());
                fields = form.Document.Form.Fields;
            }
            catch(Exception ex)
            {
                Log.Error(ex);
            }
        }

        /// <summary>
        /// Sets the specific fields mentioned by key string
        /// </summary>
        /// <param name="key">The String key that helps to locate and set the form field.</param>
        /// <param name="value">Value to be used to set the form field.</param>
        public void SetField(string key, string value)
        {
            try
            {
                var field = fields.FirstOrDefault(f => f.FullName == key);
                if (field != null)
                    field.Value = value;
            }
            catch(Exception ex)
            {
                Log.Error(ex);
            }
        }

        /// <summary>
        /// Closes all the open handle so that they can be reused later
        /// </summary>
        public void Close()
        {
            if(form != null && !string.IsNullOrWhiteSpace(pdfFileName) && fields != null)
                form.Save(pdfFileName);
        }

        /// <summary>
        /// Gets the pdf filename that is generated after setting the form fields
        /// </summary>
        /// <returns>
        /// Generated Pdf document file name.
        /// </returns>     
        public string GetFile()
        {
            Close();
            return pdfFileName;
        }
    }
}
