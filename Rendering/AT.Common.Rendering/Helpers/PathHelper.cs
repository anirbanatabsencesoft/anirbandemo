﻿using System;
using System.Web;

namespace AT.Common.Rendering.Helpers
{
    public static class PathHelper
    {
        public static string GetRootPath()
        {
            HttpContext context = HttpContext.Current;
            if (context != null && context.Request != null)
            {
                Uri u = context.Request.Url;
                string port = u.Port != 443 && u.Port != 80 ? string.Concat(":", u.Port) : "";
                return string.Concat(u.Scheme, "://", u.Host, port);
            }
            return "https://absencetracker.com";
        }
    }
}
