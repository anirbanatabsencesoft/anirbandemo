﻿using AbsenceSoft.Rendering;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace AT.Common.Rendering.Helpers
{
#pragma warning disable
    public static class PdfHelper
    {

        /// <summary>
        /// Converts Dynamic JSON to PDF 
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="reportName"></param>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public static byte[] ConvertJsonToPdf(JArray collection, string reportName, string criteria)
        {
            StringBuilder strHTML = new StringBuilder();
            strHTML.Append("<div style=\"width:100%;  color: #333333; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; margin-top: 15px;\">");
            strHTML.Append("<table style=\"width:100%; background-color: rgba(0, 0, 0, 0);  margin-bottom: 10px; margin-top: 15px; color: #FF8300; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; text-align: center;\">");
            strHTML.Append("<tr style=\"width:100%; font-size: 14px;  font-weight: bold;\"><td>" + reportName + "</td></tr>");
            strHTML.Append("</table>");
            if (!string.IsNullOrWhiteSpace(criteria))
            {
                strHTML.Append("<table style=\"width:100%; background-color: rgba(0, 0, 0, 0);  margin-bottom: 15px; margin-top: 10px; color: #FF8300; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; text-align: center;\">");
                strHTML.Append("<tr style=\"width:100%; font-size: 13px; \"><td>" + criteria + "</td></tr>");
                strHTML.Append("</table>");
            }
            var results = collection.OfType<JObject>().ToList();
            var anyData = results?.FirstOrDefault();
            if (results==null || anyData == null)
            {
                strHTML.Append("<table style=\"width:100%; background-color: rgba(0, 0, 0, 0);  margin-bottom: 15px; margin-top: 10px; color: #FF8300; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; text-align: center;\">");
                strHTML.Append("<tr style=\"width:100%; font-size: 13px; \"><td>No results found.</td></tr>");
                strHTML.Append("</table>");
                strHTML.Append("</div>");
                return ConvertHtmlToPdf(strHTML.ToString());
            }
            var properties = anyData.Properties().Select(x => x.Name).ToList();
            if (properties.Any())
            {
                if (properties.Contains("Key") && properties.Contains("Label"))
                {
                    foreach (JObject item in results)
                    {
                        foreach (var property in item)
                        {
                            if (property.Key.ToLower() == "label")
                            {
                                strHTML.Append("<table style=\"width:100%; margin-bottom: 5px; margin-top: 15px; color: #FF8300; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;\">");
                                strHTML.Append("<tr style=\"width:100%; font-size: 12px;  font-weight: bold;\"><td>" + property.Value.ToString() + "</td></tr>");
                                strHTML.Append("</table>");
                            }
                            else if (property.Key.ToLower() == "items")
                            {
                                strHTML.Append("<table style=\"width:100%; border: 0; border-color: #CCCCCC; border-width: 1px; border-style: solid; background-color: rgba(0, 0, 0, 0);  margin-bottom: 15px; border-collapse: collapse; font-size: 10px;\">");
                                strHTML.Append("<tr>");
                                JObject childHeader = property.Value.FirstOrDefault().ToObject<JObject>();
                                foreach (var propertyHeader in childHeader)
                                {
                                    strHTML.Append("<td style=\"border: 0; border-color: #CCCCCC; border-bottom-width: 1px; border-bottom-style: solid; line-height: 1.42857; padding: 5px; vertical-align: top;\"><strong>" + propertyHeader.Key + "</strong></td>");
                                }
                                strHTML.Append("</tr>");
                                foreach (var childProperty in property.Value.ToList())
                                {
                                    strHTML.Append("<tr>");
                                    JObject childItem = childProperty.ToObject<JObject>();

                                    foreach (var childData in childItem)
                                    {
                                        if (childData.Value.Type == JTokenType.Object)
                                        {
                                            var inlineData = ((JObject)childData.Value).Properties();
                                            var resultForObj = inlineData.Select(x => x.Value).ToList();
                                            string dataToDisplay = resultForObj.FirstOrDefault().ToString();
                                            strHTML.Append("<td style=\"line-height: 1.42857; padding: 5px; vertical-align: top;\">" + dataToDisplay + "</td>");
                                        }
                                        else if (childData.Value.Type == JTokenType.String || childData.Value.Type == JTokenType.Integer || childData.Value.Type == JTokenType.Float || childData.Value.Type == JTokenType.Date || childData.Value.Type == JTokenType.Guid || childData.Value.Type == JTokenType.Uri || childData.Value.Type == JTokenType.Boolean)
                                        {
                                            strHTML.Append("<td style=\"line-height: 1.42857; padding: 5px; vertical-align: top;\">" + childData.Value.ToString() + "</td>");

                                        }
                                    }

                                    strHTML.Append("</tr>");
                                }
                                strHTML.Append("</table>");
                            }
                        }

                    }

                }
                else
                {
                    strHTML.Append("<table style=\"width:100%; border: 0; border-color: #CCCCCC; border-width: 1px; border-style: solid; background-color: rgba(0, 0, 0, 0);  margin-bottom: 15px; border-collapse: collapse; font-size: 10px;\">");
                    // Header get printed
                    strHTML.Append("<tr>");
                    foreach (var property in properties)
                    {
                        strHTML.Append("<td style=\"border: 0; border-color: #CCCCCC; border-bottom-width: 1px; border-bottom-style: solid; line-height: 1.42857; padding: 5px; vertical-align: top;\"><strong>" + property + "</strong></td>");
                    }
                    strHTML.Append("</tr>");
                    strHTML.Append("<tr>");
                    foreach (JObject item in results)
                    {
                        foreach (var property in item)
                        {
                            if (property.Value.Type == JTokenType.Object)
                            {
                                var inlineClass = ((JObject)property.Value).Properties();
                                var resultado = inlineClass.Select(x => x.Value).ToList();
                                string resultadoUnido = resultado.FirstOrDefault().ToString();
                                strHTML.Append("<td style=\"line-height: 1.42857; padding: 5px; vertical-align: top;\">" + resultadoUnido + "</td>");
                            }
                            else if (property.Value.Type == JTokenType.String || property.Value.Type == JTokenType.Integer || property.Value.Type == JTokenType.Float || property.Value.Type == JTokenType.Date || property.Value.Type == JTokenType.Guid || property.Value.Type == JTokenType.Uri || property.Value.Type == JTokenType.Boolean)
                            {
                                strHTML.Append("<td style=\"line-height: 1.42857; padding: 5px; vertical-align: top;\">" + property.Value.ToString() + "</td>");

                            }
                        }
                        strHTML.Append("</tr>");
                    }
                    strHTML.Append("</table>");
                }
            }
            strHTML.Append("</div>");
            return ConvertHtmlToPdf(strHTML.ToString());
        }

        /// <summary>
        /// Converts list of Object to HTML and then PDF
        /// </summary>
        /// <param name="html">The html to convert to a PDF document.</param>
        /// <param name="reportName">Name of the report to display in header</param>
        /// <param name="criteria">Report criteria to display in Report</param>
        /// <returns>
        /// A Pdf document raw binary content as a byte array.
        /// </returns>     
        public static byte[] ConvertCollectionToPdf<T>(ICollection<T> collection, string reportName, string criteria)
        {
            StringBuilder strHTML = new StringBuilder();

            strHTML.Append("<table style=\"width:100%; background-color: rgba(0, 0, 0, 0);  margin-bottom: 10px; margin-top: 15px; color: #FF8300; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; text-align: center;\">");
            strHTML.Append("<tr style=\"width:100%; font-size: 14px;  font-weight: bold;\"><td>" + reportName + "</td></tr>");
            strHTML.Append("</table>");
            strHTML.Append("<table style=\"width:100%; background-color: rgba(0, 0, 0, 0);  margin-bottom: 15px; margin-top: 10px; color: #FF8300; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; text-align: center;\">");
            strHTML.Append("<tr style=\"width:100%; font-size: 13px; \"><td>" + criteria + "</td></tr>");
            strHTML.Append("</table>");
            strHTML.Append("<div style=\"width:100%;  color: #333333; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; margin-top: 15px;\">");
            strHTML.Append("<table style=\"width:100%; border: 0; border-color: #CCCCCC; border-width: 1px; border-style: solid; background-color: rgba(0, 0, 0, 0);  margin-bottom: 15px; border-collapse: collapse; font-size: 10px;\">");

            if (collection == null || !collection.Any())
            {
                strHTML.Append("<td style =\"border: 0; border-color: #CCCCCC; border-bottom-width: 1px; border-bottom-style: solid; border-top-left-radius: 5px; line-height: 1.42857; padding: 5px; vertical-align: top; width:20% !important;\"><strong>No Data</strong></td></ table >");
            }
            else
            {
                strHTML.Append(
                    collection.First()
                            .GetType()
                            .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                            .Select(p => p.Name).ToList()
                            .ToColumnHeaders()
                            );

                strHTML.Append(
                    collection.Aggregate(string.Empty,
                        (current, next) => current + next.ToHtmlTableRow())
                    );

                strHTML.Append("</table></div>");
            }
            return ConvertHtmlToPdf(strHTML.ToString());
        }

        /// <summary>
        /// This will create Table header HTML from property of the object
        /// </summary>
        /// <typeparam name="T">Generic Object</typeparam>
        /// <param name="propertyheaderlist"></param>
        /// <returns>HTML string for Table Header</returns>
        private static string ToColumnHeaders<T>(this List<T> propertyHeaderList)
        {
            if (propertyHeaderList == null || !propertyHeaderList.Any())
            {
                return string.Empty;
            }

            return string.Concat("<tr>",
                propertyHeaderList.Aggregate(string.Empty,
                      (current, propValue) => $"{current}<th style='font-size: 11pt; font-weight: bold; border: 1pt solid black'>{Convert.ToString(propValue)}</th>"
                      ),
                "</tr>");
        }
        /// <summary>
        /// This will create a Table row for HTML from object property value
        /// </summary>
        /// <typeparam name="T">Generic Object</typeparam>
        /// <param name="propertyvalue"></param>
        /// <returns>HTML string for Table row</returns>
        private static string ToHtmlTableRow<T>(this T propertyValue)
        {
            if (propertyValue == null)
            {
                return string.Empty;
            }

            return string.Concat("<tr>", propertyValue.GetType()
                       .GetProperties()
                       .Aggregate(string.Empty,
                           (current, prop) => $"{current}<td style='font-size: 11pt; font-weight: normal; border: 1pt solid black'>{Convert.ToString(prop.GetValue(propertyValue, null))}</td>"
                           ),
                        "</tr>");
        }

        /// <summary>
        /// cnverts MS Word Document to PDF
        /// </summary>
        /// <param name="currentDoc"></param>
        /// <returns>A Pdf document raw binary content as a byte array.</returns>
        public static byte[] ConvertMsWordToPdf(byte[] currentDoc)
        {
            using (MemoryStream output = new MemoryStream())
            {
                using (MemoryStream input = new MemoryStream(currentDoc))
                {
                    Aspose.Words.Document loadedFromBytes = new Aspose.Words.Document(input);
                    loadedFromBytes.Save(output, Aspose.Words.SaveFormat.Pdf);
                }
                return output.ToArray();
            }

        }

        /// <summary>
        /// Converts HTML (and CSS2 in the head or in-line) to a PDF document and returns the result. Also handles relative URLs
        /// and Image paths with the ones configured or appropriate for the site (hosted or physical file).
        /// </summary>
        /// <param name="html">The html to convert to a PDF document.</param>
        /// <returns>A Pdf document raw binary content as a byte array.</returns>
        public static byte[] ConvertHtmlToPdf(string html, Stream currentDoc = null)
        {
            int pages;
            return ConvertHtmlToPdf(html, out pages, currentDoc);
        }

        /// <summary>
        /// Converts HTML (and CSS2 in the head or in-line) to a PDF document and returns the result. Also handles relative URLs
        /// and Image paths with the ones configured or appropriate for the site (hosted or physical file).
        /// </summary>
        /// <param name="html">The html to convert to a PDF document.</param>
        /// <param name="pages">The pages.</param>
        /// <param name="orientation">The orientation.</param>
        /// <param name="size">The size.</param>
        /// <returns>
        /// A Pdf document raw binary content as a byte array.
        /// </returns>
        /// <exception cref="System.ArgumentException">Nothing provided to convert to PDF;html</exception>
        public static byte[] ConvertHtmlToPdf(string html, out int pages, Stream currentDoc = null)
        {
            new Aspose.Words.License().SetLicense("Aspose.Words.lic");
            if (string.IsNullOrWhiteSpace(html))
            {
                throw new ArgumentException("Nothing provided to convert to PDF", "html");
            }
            byte[] doc = null;

            using (MemoryStream output = new MemoryStream())
            {
                using (MemoryStream input = new MemoryStream())
                {
                    string htmlToLoad = html.StartsWith("<html>") ? html : $"<html><body>{html}</body></html>";
                    Aspose.Words.Document asposeDoc = new Aspose.Words.Document();
                    if (currentDoc != null)
                    {                        
                        asposeDoc = new Aspose.Words.Document(currentDoc);
                        asposeDoc.RemoveAllChildren();                       
                    }
                    Aspose.Words.DocumentBuilder builder = new Aspose.Words.DocumentBuilder(asposeDoc);
                    builder.InsertHtml(htmlToLoad);
                    Aspose.Words.PageSetup ps = builder.PageSetup;
                    ps.PageWidth= Aspose.Words.ConvertUtil.InchToPoint(15);
                    ps.Orientation = Aspose.Words.Orientation.Landscape;
                    ps.TopMargin = Aspose.Words.ConvertUtil.InchToPoint(0.5);
                    ps.BottomMargin = Aspose.Words.ConvertUtil.InchToPoint(0.5);
                    ps.LeftMargin = Aspose.Words.ConvertUtil.InchToPoint(0.5);
                    ps.RightMargin = Aspose.Words.ConvertUtil.InchToPoint(0.5);
                    ps.HeaderDistance = Aspose.Words.ConvertUtil.InchToPoint(0.2);
                    ps.FooterDistance = Aspose.Words.ConvertUtil.InchToPoint(0.2);
                    asposeDoc.UpdatePageLayout();
                    asposeDoc.Save(output, Aspose.Words.SaveFormat.Pdf);
                    pages = asposeDoc.PageCount;
                }

                doc = output.ToArray();
            }
            return doc;
        }

        /// <summary>
        /// Extracts a range of pages from a pdf document and returns a new pdf document.
        /// </summary>
        /// <returns>A byte array representing the resulting extracted pages merged into a pdf document.</returns>
        public static byte[] ExtractPages(byte[] sourcePDF, int startPage, int endPage)
        {
            return AbsenceSoft.Rendering.Pdf.ExtractPages(sourcePDF, startPage, endPage);
        }

        /// <summary>
        /// Takes an array of PDF document byte arrays and joins them together into a single PDF document. All documents
        /// passed in the array MUST be valid PDF documents or this method will more than likely throw an exception.
        /// </summary>
        /// <param name="documents">An array of PDF document binary as byte arrays.</param>
        /// <returns>A byte array representing the resulting combined PDF document joining each of the passed in PDF documents.</returns>
        public static byte[] MergePdfDocuments(params byte[][] documents)
        {
            return AbsenceSoft.Rendering.Pdf.MergePdfDocuments(documents);
        }

        /// <summary>
        /// Takes an array of PDF document byte arrays and joins them together into a single PDF document. All documents
        /// passed in the array MUST be valid PDF documents or this method will more than likely throw an exception.
        /// </summary>
        /// <param name="documents">An array of PDF document binary as byte arrays.</param>
        /// <returns>A byte array representing the resulting combined PDF document joining each of the passed in PDF documents.</returns>
        public static byte[] MergePdfDocumentsWithPageNumbers(params byte[][] documents)
        {
            return AbsenceSoft.Rendering.Pdf.MergePdfDocumentsWithPageNumbers(documents);
        }

        /// <summary>
        /// Adds metadata to a PDF
        /// </summary>
        /// <param name="sourceDocument"></param>
        /// <param name="metadataToAdd"></param>
        /// <returns>A byte array PDF document</returns>
        public static byte[] AddPdfMetadata(byte[] sourceDocument, Dictionary<string, string> metadataToAdd)
        {
            return AbsenceSoft.Rendering.Pdf.AddPdfMetadata(sourceDocument, metadataToAdd);
        }

        /// <summary>
        /// Stamps the PDF.
        /// </summary>
        /// <param name="sourceDocument">The source document PDF to stamp.</param>
        /// <param name="stamps">The stamps to apply.</param>
        /// <returns>A stamped PDF file as raw binary.</returns>
        /// <remarks>http://stackoverflow.com/questions/15165357/centered-multiline-text-using-itextsharp-columntext</remarks>
        public static byte[] StampPdf(byte[] sourceDocument, IEnumerable<PdfStamp> stamps)
        {
            return AbsenceSoft.Rendering.Pdf.StampPdf(sourceDocument, stamps);            
        }

    }
#pragma warning restore
}
