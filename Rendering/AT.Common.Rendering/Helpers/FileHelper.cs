﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace AT.Common.Rendering.Helpers
{
    /// <summary>
    /// File delivery Helper class
    /// </summary>
    public static class FileHelper
    {
        /// <summary>
        /// Upload the file by specific method
        /// </summary>
        /// <param name="file"></param>
        /// <param name="deliveryType"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public static bool UploadFile(byte[] file, DeliveryType deliveryType, DeliveryParams param)
        {
            switch (deliveryType)
            {
                case DeliveryType.FTP:
                    return SendFileToFtp(file, param);
                case DeliveryType.SFTP:
                    return UploadFileToSftp(file, param);
                case DeliveryType.S3:
                    return UploadFileToS3(file, param);
                case DeliveryType.EMAIL:
                    return SendFileByEmail(file, param);
                case DeliveryType.API:
                    return SendFileToApi(file, param);
            }
            return false;
        }

        /// <summary>
        /// Send the file on FTP and call API. Need to implement this once API code is ready
        /// </summary>
        /// <param name="file"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        private static bool SendFileToApi(byte[] file, DeliveryParams param)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Delivery the file on FTP
        /// </summary>
        /// <param name="file"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        private static bool SendFileToFtp(byte[] file, DeliveryParams param)
        {
            string ftpUrl = CreateFtpHostUrl(param);
            string fileUrl = CreateFilePath(param);
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpUrl +"/"+ fileUrl);
                request.Credentials = new NetworkCredential(param.UserName, param.Password);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                using (MemoryStream input = new MemoryStream(file))
                {
                    using (Stream ftpStream = request.GetRequestStream())
                    {
                        input.CopyTo(ftpStream);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(ex.ToString());
            }
            return true;
        }

        /// <summary>
        /// Upload the file on Amazon S3 location
        /// </summary>
        /// <param name="file"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        private static bool UploadFileToS3(byte[] file, DeliveryParams param)
        {
            try
            {               
                using (MemoryStream input = new MemoryStream(file))
                {
                    using (IAmazonS3 client = S3Client(param))
                    {
                        PutObjectRequest request = new PutObjectRequest
                        {
                            BucketName = param.DestinationPath,
                            Key = param.FileName,
                            CannedACL = S3CannedACL.Private,
                            InputStream = input
                        };
                        client.PutObject(request);
                    }
                }
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
              throw  new InvalidOperationException(amazonS3Exception.ToString()); ;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(ex.ToString()); ;
            }
            return true;
        }
       
        /// <summary>
        /// Upload the file on SFTP
        /// </summary>
        /// <param name="file"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        private static bool UploadFileToSftp(byte[] file, DeliveryParams param)
        {
            var methods = new List<AuthenticationMethod>
            {
                new PasswordAuthenticationMethod(param.UserName, param.Password)
            };
            if (!string.IsNullOrEmpty(param.PrivateKeyFileName))
            {
                PrivateKeyFile keyFile = new PrivateKeyFile(param.PrivateKeyFileName);
                var keyFiles = new[] { keyFile };
                methods.Add(new PrivateKeyAuthenticationMethod(param.UserName, keyFiles));
            }

            string fileUrl = CreateFilePath(param);
            var connectionInfo = new ConnectionInfo(param.Host,(int)param.Port, param.UserName, methods.ToArray());
            using (MemoryStream input = new MemoryStream(file))
            {
                using (SftpClient sftp = new SftpClient(connectionInfo))
                {
                    try
                    {
                        sftp.Connect();

                        sftp.UploadFile(input, fileUrl);

                        sftp.Disconnect();
                    }
                    catch (Exception ex)
                    {
                        throw new InvalidOperationException(ex.ToString());
                    }
                }
                return true;
            }
        }

        /// <summary>
        /// Upload the file on FTP and inform the user via email. Not required right now, but later we can implement.
        /// </summary>
        /// <param name="file"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        private static bool SendFileByEmail(byte[] file, DeliveryParams param)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Create the full path URL of the file 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        private static string CreateFilePath(DeliveryParams param)
        {
            string fileUrl = string.Empty;
            if (!string.IsNullOrEmpty(param.DestinationPath))
            {
                fileUrl = fileUrl + param.DestinationPath ;
            }
            return fileUrl + param.FileName;
        }

        /// <summary>
        /// Create the host URL with port
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        private static string CreateFtpHostUrl(DeliveryParams param)
        {
            string ftpUrl = param.Host;
            if (param.Port != null && param.Port != 0)
            {
                ftpUrl = ftpUrl + ":" + param.Port;
            }
            return ftpUrl;
        }

        /// <summary>
        /// Gets a new instance of an Amazon AWS S3 Client
        /// </summary>
        /// <returns>A new instance of an Amazon AWS S3 Client</returns>
        private static IAmazonS3 S3Client(DeliveryParams param)
        {
            AmazonS3Config config = new AmazonS3Config()
            {                
                ServiceURL = param.Host,
                ProxyHost = null
            };
            return AWSClientFactory.CreateAmazonS3Client(
                param.UserName,
                param.Password,
                config);
        } 
    }
}
