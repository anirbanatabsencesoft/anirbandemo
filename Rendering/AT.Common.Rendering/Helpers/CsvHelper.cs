﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace AT.Common.Rendering.Helpers
{
    public static class CsvHelper
    {
        /// <summary>
        /// This will Convert any generic object to CSV 
        /// </summary>
        /// <typeparam name="T">Generic Object</typeparam>
        /// <param name="collection">Generic Collection data</param>
        /// <param name="reportName">Name of the report</param>
        /// <param name="criteria">Report Criteria</param>
        /// <returns>A csv document raw binary content as a byte array.</returns>
        public static byte[] GenerateCSV<T>(ICollection<T> collection, string reportName = null, string criteria = null, bool includeHeader = true)
        {
            using (MemoryStream output = new MemoryStream())
            {
                using (StreamWriter writer = new StreamWriter(output, Encoding.UTF8))
                {
                    if (collection == null || !collection.Any())
                    {
                        writer.Write("No Data");
                    }
                    else
                    {
                        if (reportName != null)
                        {
                            writer.Write(reportName);
                            writer.WriteLine();
                        }
                        if (criteria != null)
                        {
                            writer.Write(criteria);
                            writer.WriteLine();
                        }
                        if (includeHeader)
                        {
                            writer.Write(collection.First()
                                    .GetType()
                                    .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                    .Select(p => p.Name).ToList()
                                    .ToCsvHeaders());
                            writer.WriteLine();
                        }

                        foreach (var item in collection.ToList())
                        {
                            writer.WriteLine(item.ToCsvRow());
                        }
                    }

                }
                return output.ToArray();
            }
        }

        /// <summary>
        /// Generate CSV from Json Data
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="reportName"></param>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public static byte[] GenerateCSVFromJson(JArray collection, string reportName, string criteria)
        {
            using (MemoryStream output = new MemoryStream())
            {
                using (StreamWriter writer = new StreamWriter(output, Encoding.UTF8))
                {
                    var results = collection.OfType<JObject>().ToList();
                    var anyData = results?.FirstOrDefault();
                    if (results == null || anyData == null)
                    {
                        writer.Write(Escape(reportName));
                        writer.WriteLine();
                        if (!string.IsNullOrWhiteSpace(criteria))
                        {
                            writer.Write(Escape(criteria));
                            writer.WriteLine();
                        }
                        writer.WriteLine("No results found.");
                    }
                    else
                    {
                        writer.Write(Escape(reportName));
                        writer.WriteLine();
                        writer.Write(Escape(criteria));
                        writer.WriteLine();
                        var properties = anyData.Properties().Select(x => x.Name).ToList();
                        if (properties.Any())
                        {
                            if (properties.Contains("Key") && properties.Contains("Label"))
                            {
                                CreateCsvForGroupedData(writer, results);
                            }
                            else
                            {
                                CreateCsvForSimpleData(writer, results, properties);
                            }
                        }
                    }
                }
                return output.ToArray();
            }
        }

        /// <summary>
        /// Create the CSV for no Grouped data
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="results"></param>
        /// <param name="properties"></param>
        private static void CreateCsvForSimpleData(StreamWriter writer, List<JObject> results, List<string> properties)
        {
            StringBuilder headerNoGroup = new StringBuilder();
            foreach (var property in properties)
            {
                headerNoGroup.Append(Escape(property) + ",");
            }
            headerNoGroup = headerNoGroup.Remove(headerNoGroup.Length - 1, 1);
            writer.Write(headerNoGroup.ToString());
            writer.WriteLine();
            foreach (JObject item in results)
            {
                StringBuilder columnsNoGroup = new StringBuilder();
                foreach (var property in item)
                {
                    if (property.Value.Type == JTokenType.Object)
                    {
                        var inlineClass = ((JObject)property.Value).Properties();
                        var resultado = inlineClass.Select(x => x.Value).ToList();
                        string resultadoUnido = resultado.FirstOrDefault().ToString();
                        columnsNoGroup.Append(Escape(resultadoUnido) + ",");

                    }
                    else if (property.Value.Type == JTokenType.String || property.Value.Type == JTokenType.Integer || property.Value.Type == JTokenType.Float || property.Value.Type == JTokenType.Date || property.Value.Type == JTokenType.Guid || property.Value.Type == JTokenType.Uri || property.Value.Type == JTokenType.Boolean)
                    {
                        columnsNoGroup.Append(Escape(property.Value.ToString()) + ",");

                    }
                }
                columnsNoGroup = columnsNoGroup.Remove(columnsNoGroup.Length - 1, 1);
                writer.Write(columnsNoGroup.ToString());
                writer.WriteLine();
            }
        }

        /// <summary>
        /// Create csv for Grouped Data
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="results"></param>
        private static void CreateCsvForGroupedData(StreamWriter writer, List<JObject> results)
        {
            foreach (JObject item in results)
            {
                foreach (var property in item)
                {
                    if (property.Key.ToLower() == "label")
                    {
                        writer.Write(property.Value.ToString());
                        writer.WriteLine();
                    }
                    else if (property.Key.ToLower() == "items")
                    {
                        StringBuilder header = new StringBuilder();
                        JObject childHeader = property.Value.FirstOrDefault().ToObject<JObject>();
                        foreach (var propertyHeader in childHeader)
                        {
                            header.Append(Escape(propertyHeader.Key) + ",");
                        }
                        header = header.Remove(header.Length - 1, 1);
                        writer.Write(header.ToString());
                        writer.WriteLine();
                        foreach (var childProperty in property.Value.ToList())
                        {
                            CreateCsvForChildData(writer, childProperty);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Create the CSV for Child data
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="childProperty"></param>
        private static void CreateCsvForChildData(StreamWriter writer, JToken childProperty)
        {
            JObject childItem = childProperty.ToObject<JObject>();
            StringBuilder columns = new StringBuilder();
            foreach (var childData in childItem)
            {
                if (childData.Value.Type == JTokenType.Object)
                {
                    var inlineData = ((JObject)childData.Value).Properties();
                    var resultForObj = inlineData.Select(x => x.Value).ToList();
                    string dataToDisplay = resultForObj.FirstOrDefault().ToString();
                    columns.Append(Escape(dataToDisplay) + ",");

                }
                else if (childData.Value.Type == JTokenType.String || childData.Value.Type == JTokenType.Integer || childData.Value.Type == JTokenType.Float || childData.Value.Type == JTokenType.Date || childData.Value.Type == JTokenType.Guid || childData.Value.Type == JTokenType.Uri || childData.Value.Type == JTokenType.Boolean)
                {
                    columns.Append(Escape(childData.Value.ToString()) + ",");

                }
            }
            columns = columns.Remove(columns.Length - 1, 1);
            writer.Write(columns.ToString());
            writer.WriteLine();
        }

        /// <summary>
        /// This will create Csv header from property of the object
        /// </summary>
        /// <typeparam name="T">Generic Object</typeparam>
        /// <param name="propertyheaderlist">List of report Header</param>
        /// <returns>comma seprated string for Table Header</returns>
        private static string ToCsvHeaders<T>(this List<T> propertyHeaderList)
        {
            if (propertyHeaderList == null || !propertyHeaderList.Any())
            {
                return string.Empty;
            }

            return propertyHeaderList.Aggregate("isFirst",
                      (current, propValue) =>
                      {
                          current = current == "isFirst" ? string.Empty : current + ",";
                          return current + Escape(Convert.ToString(propValue));
                      });

        }
        /// <summary>
        /// This will create a csv row from object property value
        /// </summary>
        /// <typeparam name="T">Generic Object</typeparam>
        /// <param name="propertyValue">list of values</param>
        /// <returns>Comma seperated string for table row</returns>
        private static string ToCsvRow<T>(this T propertyValue)
        {

            if (propertyValue == null)
            {
                return string.Empty;
            }

            return propertyValue.GetType()
                     .GetProperties()
                     .Aggregate("isFirst",
                         (current, prop) =>
                         {
                             current = current == "isFirst" ? string.Empty : current + ",";
                             return current + Escape(Convert.ToString(prop.GetValue(propertyValue, null)));
                         });
        }
        /// <summary>
        /// This method escap some chars from string
        /// </summary>
        /// <param name="s">string to be escaped from some chars</param>
        /// <returns>returns string with escaped chars</returns>
        private static string Escape(string escapedString)
        {
            if (string.IsNullOrWhiteSpace(escapedString))
            {
                return string.Empty;
            }
            if (escapedString.Contains(QUOTE))
            {
                escapedString = escapedString.Replace(QUOTE, ESCAPED_QUOTE);
            }
            if (escapedString.IndexOfAny(CHARACTERS_THAT_MUST_BE_QUOTED) > -1)
            {
                escapedString = string.Concat(QUOTE, escapedString, QUOTE);
            }
            return escapedString;
        }

        private const string Comma = ",";
        private const string QUOTE = "\"";
        private const string ESCAPED_QUOTE = "\"\"";
        private static char[] CHARACTERS_THAT_MUST_BE_QUOTED = { ',', '"', '\n' };
    }
}
