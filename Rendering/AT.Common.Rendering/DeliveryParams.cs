﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Common.Rendering
{
    /// <summary>
    /// File Delivery parameters
    /// </summary>
   public class DeliveryParams
    {
        /// <summary>
        /// Host name 
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// User name or Access Key
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Password or Secret Key
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// File name to set on delivery location 
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Any path after root or S3 Bucket name
        /// </summary>
        public string DestinationPath { get; set; }

        /// <summary>
        /// Port 
        /// </summary>
        public int? Port { get; set; }

        /// <summary>
        /// private key for SFTP if any
        /// </summary>
        public string PrivateKeyFileName { get; set; }
    }
}
