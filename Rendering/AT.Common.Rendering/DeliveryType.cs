﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Common.Rendering
{
    /// <summary>
    /// File Delivery type 
    /// </summary>
   public enum DeliveryType
    {
        FTP,
        SFTP,
        S3,
        EMAIL,
        API
    }
}
