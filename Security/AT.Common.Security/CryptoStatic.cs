﻿using AbsenceSoft.Common.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AT.Common.Security
{
    public static class CryptoStatic
    {
        static string ivAsBase64;
        static string keyAsBase64;

        static CryptoStatic()
        {
            ivAsBase64 = Settings.Default.SymmetricIV;
            keyAsBase64 = Settings.Default.SymmetricKey;
        }


        public static string Encrypt(string plainText)
        {
            using (AesCryptoServiceProvider aes = new AesCryptoServiceProvider())
            {
                aes.IV = Convert.FromBase64String(ivAsBase64);
                aes.Key = Convert.FromBase64String(keyAsBase64);
                // Encrypt the text
                byte[] textBytes = Encoding.UTF8.GetBytes(plainText);
                var cryptor = aes.CreateEncryptor();
                byte[] encryptedBytes = cryptor.TransformFinalBlock(textBytes, 0, textBytes.Length);
                return  Convert.ToBase64String(encryptedBytes);
            }
        }

        public static string Decrypt(byte[] encryptedBytes)
        {
            using (AesCryptoServiceProvider aes = new AesCryptoServiceProvider())
            {
                // Decrypt the text
                byte[] iv = Convert.FromBase64String(ivAsBase64);
                byte[] keyBytes = Convert.FromBase64String(keyAsBase64);
                byte[] textBytes = Convert.FromBase64String(Encoding.UTF8.GetString(encryptedBytes));
                var decryptor = aes.CreateDecryptor(keyBytes, iv);
                byte[] decryptedBytes = decryptor.TransformFinalBlock(textBytes, 0, textBytes.Length);
                var result = Encoding.UTF8.GetString(decryptedBytes);                
                return result;
            }
        }

    }
}
