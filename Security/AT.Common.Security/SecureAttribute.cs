﻿using AT.Common.Core;
using AT.Data.Authentication.Provider;
using AT.Entities.Authentication;
using AT.Logic.Authentication;
using AT.Logic.Cache;
using AT.Logic.Security;
using AT.Provider.Cache.Base;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.Results;

namespace AT.Common.Security
{
    /// <summary>
    /// Class to define attribute securing web api end points
    /// </summary>
    public class SecureAttribute : System.Web.Http.AuthorizeAttribute
    {
        /// <summary>
        /// Gets or sets the required permissions.
        /// </summary>
        public List<string> RequiredPermissions { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="SecureAttribute"/> is secure.
        /// </summary>
        public bool Secure { get; set; }

        /// <summary>
        /// Gets or sets the feature.
        /// </summary>
        public ApplicationEnums.AppFeature Feature { get; set; }

        /// <summary>
        /// Set the current employer id
        /// </summary>
        public string CurrentEmployerId { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SecureAttribute"/> class.
        /// </summary>
        public SecureAttribute()
        {
            RequiredPermissions = new List<string>();
            Feature = ApplicationEnums.AppFeature.None;
            Secure = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SecureAttribute"/> class.
        /// </summary>
        /// <param name="employerId">Set the current employer id</param>
        /// <param name="permissions">The permissions.</param>
        public SecureAttribute(string employerId, params string[] permissions) : this(employerId, ApplicationEnums.AppFeature.None, permissions) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SecureAttribute"/> class.
        /// </summary>
        /// <param name="employerId">Set the current employer id</param>
        /// <param name="feature">The feature.</param>
        public SecureAttribute(string employerId, ApplicationEnums.AppFeature feature) : this(employerId, feature, (string[])null) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SecureAttribute"/> class.
        /// </summary>
        /// <param name="employerId">Set the current employer id</param>
        /// <param name="feature">The feature.</param>
        /// <param name="permissions">The permissions.</param>
        public SecureAttribute(string employerId, ApplicationEnums.AppFeature feature, params string[] permissions) : this()
        {
            this.CurrentEmployerId = employerId;

            if (permissions == null)
            {
                RequiredPermissions = new List<string>();
            }
            else
            {
                RequiredPermissions = permissions.ToList();
            }

            Feature = feature;
            Secure = true;
        }


        /// <summary>
        /// The token to be retrieved
        /// </summary>
        public string Token { get; private set; }

        /// <summary>
        /// The user to be retrieved from token
        /// </summary>
        public User AuthenticatedUser { get; set; }

        /// <summary>
        /// Get the token from the context
        /// </summary>
        /// <param name="actionContext"></param>
        private void GetToken(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            if (actionContext.Request.Headers.GetValues(AppConstants.TokenIdentifier.HEADER_TOKEN_KEY) != null)
            {
                Token = actionContext.Request.Headers.GetValues(AppConstants.TokenIdentifier.HEADER_TOKEN_KEY).FirstOrDefault();
            }
            else if (actionContext.Request.GetQueryNameValuePairs() != null && actionContext.Request.GetQueryNameValuePairs().Any(p => p.Key == AppConstants.TokenIdentifier.URL_TOKEN_KEY))
            {
                Token = actionContext.Request.GetQueryNameValuePairs().FirstOrDefault(p => p.Key == AppConstants.TokenIdentifier.URL_TOKEN_KEY).Value;
            }
            else if (actionContext.Request.Headers.GetCookies(AppConstants.TokenIdentifier.COOKIE_TOKEN_KEY) != null && !string.IsNullOrWhiteSpace(actionContext.Request.Headers.GetCookies(AppConstants.TokenIdentifier.COOKIE_TOKEN_KEY).FirstOrDefault()[AppConstants.TokenIdentifier.COOKIE_TOKEN_KEY].Value))
            {
                Token = actionContext.Request.Headers.GetCookies(AppConstants.TokenIdentifier.COOKIE_TOKEN_KEY).FirstOrDefault()[AppConstants.TokenIdentifier.COOKIE_TOKEN_KEY].Value;
            }
        }

        /// <summary>
        /// Retrieve the user from token
        /// </summary>
        /// <param name="actionContext"></param>
        private void GetUser()
        {
            ApplicationUserManager userManager = new ApplicationUserManager(UserStoreProviderManager.Default.Repository);
            AuthenticationTicket authenticationTicket = userManager.ValidateTokenAsync(Token).Result;
            var userPrincipal = userManager.GetClaimsPrincipal(authenticationTicket).Result;

            //get the user data from cache
            this.AuthenticatedUser = CacheHelper.Get<AT.Entities.Authentication.User>(userPrincipal.Identity.Name);
            //if data is not available, get the data from database and cache for 30 mins
            if (AuthenticatedUser == null)
            {
                this.AuthenticatedUser = userManager.FindByNameAsync(userPrincipal.Identity.Name).Result;
                CacheProviderManager.Default.Set(userPrincipal.Identity.Name, this.AuthenticatedUser, new TimeSpan(0, 30, 0));
            }
        }

        /// <summary>
        /// Override the authorization now
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            //return if current employer id is not set
            if (CurrentEmployerId == null)
            {
                return;
            }

            //Get the Token from the context
            GetToken(actionContext);

            //Get the user
            if (!string.IsNullOrWhiteSpace(this.Token))
            {
                GetUser();
            }
            else
            {
                return;
            }

            if (this.AuthenticatedUser != null)
            {
                AuthorizationLogic logic = new AuthorizationLogic(CurrentEmployerId, AuthenticatedUser);
                var authorized = logic.IsAuthorized(this.Feature, this.RequiredPermissions).Result;

                if (!authorized)
                {
                    actionContext.Response = actionContext.Request.CreateResponse(
                                    HttpStatusCode.OK,
                                    new
                                    {
                                        success = false,
                                        error = new AccessViolationException("You do not have permission to make this change please consult with the System Administrator.")
                                    },
                                        actionContext.ControllerContext.Configuration.Formatters.JsonFormatter
                    );

                    return;
                }
            }
            else
            {
                return;
            }

            base.OnAuthorization(actionContext);
        }

    }
}
