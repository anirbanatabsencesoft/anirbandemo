using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace AT.Data.Security.Base
{
    public class DataProviderCollection : System.Configuration.Provider.ProviderCollection
    {

        /// <summary>
        /// Returns the data provider based on name
        /// </summary>
        /// <param name="name"></param>
        new public BaseDataProvider this[string name]
        {
            get { return (BaseDataProvider)base[name]; }
        }

    }
}