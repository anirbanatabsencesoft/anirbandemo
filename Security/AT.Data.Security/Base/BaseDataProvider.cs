﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Specialized;

using AT.Core.Entities;
using AT.Data.Security.Arguments;
using AT.Data.Security.Model;


namespace AT.Data.Security.Base
{
    public abstract class BaseDataProvider : System.Configuration.Provider.ProviderBase
    {
        #region Required Properties
        /// <summary>
        /// The User Mongo Object Id
        /// </summary>
        public string UserKey { get; set; }

        /// <summary>
        /// The Customer Mongo Object Id
        /// </summary>
        public string CustomerKey { get; set; }

        /// <summary>
        /// The Employer Mongo Object Id 
        /// </summary>
        public string EmployerKey { get; set; }

        /// <summary>
        /// The Employee Mongo Object Id
        /// </summary>
        public string EmployeeKey { get; set; }

        /// <summary>
        /// The User Postgres PK
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// The Customer Postgres PK
        /// </summary>
        public long CustomerId { get; set; }

        /// <summary>
        /// The EMployer Postgres PK
        /// </summary>
        public long EmployerId { get; set; }

        /// <summary>
        /// The Employee Postgres PK
        /// </summary>
        public long EmployeeId { get; set; }
        #endregion

        /// <summary>
        /// If we want to set a different connection string for this provider
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// This method to be overriden for setting up the config parameters
        /// </summary>
        /// <param name="collection"></param>
        public virtual void SetConfig(NameValueCollection collection)
        {

        }

        
        /// <summary>
        /// Get all the customer app features
        /// </summary>
        /// <returns></returns>
        public abstract Task<IList<CustomerAppFeature>> GetCustomerAppFeatures();


        /// <summary>
        /// Get all employer app features
        /// </summary>
        /// <returns></returns>
        public abstract Task<IList<EmployerAppFeature>> GetEmployerAppFeatures();
    }
}
