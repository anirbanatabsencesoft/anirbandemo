using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Configuration;
using System.Web.Configuration;

namespace AT.Data.Security.Base
{
    /// <summary>
    /// This is the starting point for a data provider as this is going to set all provider
    /// </summary>
    public class DataProviderManager
    {

        private static string DATA_PROVIDER_SECTION = "TemplateDataProviders";

        private static BaseDataProvider _default;
        private static DataProviderCollection _providerCollection;
        private static System.Configuration.ProviderSettingsCollection _providerSettings;


        /// <summary>
        /// Static constructor for getting all providers in a queue
        /// </summary>
        static DataProviderManager()
        {

            Initialize();
        }

        /// <summary>
        /// Initialize the configuration section and set all static private variables.
        /// </summary>
        private static void Initialize()
        {
            //get the configuration
            DataProviderConfiguration configSection = (DataProviderConfiguration)ConfigurationManager.GetSection(DATA_PROVIDER_SECTION);

            //throw exception if configuration section is not present
            if (configSection == null)
                throw new ConfigurationErrorsException("Data provider section is not set in the config.");

            //get provider collections
            _providerCollection = new DataProviderCollection();
            ProvidersHelper.InstantiateProviders(configSection.Providers, _providerCollection, typeof(BaseDataProvider));

            //get provider settings
            _providerSettings = configSection.Providers;

            //if there is no default provider, throw excpetion
            if (_providerCollection[configSection.DefaultProvider] == null)
                throw new ConfigurationErrorsException("Default provider is not set.");

            //get the default provider
            _default = _providerCollection[configSection.DefaultProvider];
            var defaultSettings = _providerSettings[configSection.DefaultProvider];


            _default.SetConfig(defaultSettings.Parameters);
        }

        /// <summary>
        /// Returns the Default Instance of the provider.
        /// </summary>
        public static BaseDataProvider Default { get { return _default; } }

        public static DataProviderCollection Providers { get { return _providerCollection; } }

        public System.Configuration.ProviderSettingsCollection ProviderSettings { get { return _providerSettings; } }

    }
}