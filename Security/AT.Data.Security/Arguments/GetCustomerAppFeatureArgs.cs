﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AT.Common.Core;
using AT.Core.Entities;

namespace AT.Data.Security.Arguments
{
    /// <summary>
    /// The arguments to get the object.
    /// Add all possible search criteria as property.
    /// </summary>
    public class GetCustomerAppFeatureArgs : BaseArgument
    {
        /// <summary>
        /// Set the identifier as unique constant Ex: EXAMPLE_TEMPLATE_IDENTIFIER
        /// </summary>
        public override string Identifier => throw new NotImplementedException();

        /// <summary>
        /// Set if data required for Id
        /// </summary>
        public long? Id { get; set; }

        /// <summary>
        /// Set if data required for Mongo Key
        /// </summary>
        public string Key { get; set; }
    }
}
