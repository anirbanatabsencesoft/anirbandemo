﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Insight.Database;
using Npgsql;

using AT.Data.Security.Base;
using AT.Core.Entities;
using AT.Data.Security.Arguments;
using AT.Data.Security.Model;
using System.Collections.Specialized;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AT.Common.Core;

namespace AT.Data.Security
{
    public class MongoDataProvider : BaseDataProvider
    {
        public override string Name => "SecurityMongoDataProvider";
        public override string Description => "Security Mongo Data Provider";

        /// <summary>
        /// Get the list of objects
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public override async Task<IList<CustomerAppFeature>> GetCustomerAppFeatures()
        {
            //Get the customer data
            var result = Customer.AsQueryable().Where(c => !c.IsDeleted).ToList();
            
            return await Task.FromResult(result.SelectMany(p => p.Features)
                                .Where(c => (c.EffectiveDate == null || c.EffectiveDate < DateTime.UtcNow) &&
                                                (c.IneffectiveDate == null || c.IneffectiveDate > DateTime.UtcNow) &&
                                                 c.Enabled)
                                .Select(c => new CustomerAppFeature()
                                {
                                    CustomerId = 0,
                                    CustomerKey = c.Id.ToString(),
                                    AppFeature = (ApplicationEnums.AppFeature)((int)c.Feature + 500),
                                    AppType = ApplicationEnums.AppType.WebApp,
                                    FeatureName =c.Name
                                })
                                .ToList());
        }

        /// <summary>
        /// Get all employers app features
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public override async Task<IList<EmployerAppFeature>> GetEmployerAppFeatures()
        {
            //get the employer data
            var result = Employer.AsQueryable().Where(e => !e.IsDeleted).ToList();
            
            return await Task.FromResult(result
                                .SelectMany(e => e.Features)
                                .Where(e => (e.EffectiveDate == null || e.EffectiveDate < DateTime.UtcNow) && (e.IneffectiveDate == null || e.IneffectiveDate > DateTime.UtcNow) && e.Enabled)
                                .Select(e => new EmployerAppFeature()
                                {
                                    EmployerId = 0,
                                    EmployerKey = e.Id.ToString(),
                                    AppFeature = (ApplicationEnums.AppFeature)((int)e.Feature + 500),
                                    AppType = ApplicationEnums.AppType.WebApp,
                                    FeatureName = e.Name
                                })
                                .ToList());
        }
    }
}
