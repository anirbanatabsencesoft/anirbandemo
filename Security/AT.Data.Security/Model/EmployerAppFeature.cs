﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AT.Common.Core;
using AT.Data.Core.Interfaces;


namespace AT.Data.Security.Model
{
    public class EmployerAppFeature : BaseEntity, IEmployerData
    {
        /// <summary>
        /// The Customer PK in Postgres
        /// </summary>
        public long EmployerId { get; set; }

        /// <summary>
        /// The Customer PK in Mongo
        /// </summary>
        public string EmployerKey { get; set; }

        /// <summary>
        /// Set the feature name
        /// </summary>
        public string FeatureName { get; set; }

        /// <summary>
        /// The app type of the access
        /// </summary>
        public ApplicationEnums.AppType AppType { get; set; }

        /// <summary>
        /// The app feature the user has permission
        /// </summary>
        public ApplicationEnums.AppFeature AppFeature { get; set; }
        
    }
}
