﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Insight.Database;
using Npgsql;

using AT.Data.Security.Base;
using AT.Core.Entities;
using AT.Data.Security.Arguments;
using AT.Data.Security.Model;
using System.Collections.Specialized;

namespace AT.Data.Security
{
    public class PostgresDataProvider : BaseDataProvider
    {

        public override string Name => "SecurityPostgresDataProvider";
        public override string Description => "Security Postgres Data Provider";

        /// <summary>
        /// Set the config parameters
        /// </summary>
        /// <param name="collection"></param>
        public override void SetConfig(NameValueCollection collection)
        {
            //if connection string is defined
            if (!string.IsNullOrWhiteSpace(collection["connectionString"]))
                this.ConnectionString = collection["connectionString"];
        }

        
        /// <summary>
        /// Get the list of objects
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public override async Task<IList<CustomerAppFeature>> GetCustomerAppFeatures()
        {
            return await Task.FromResult<IList<CustomerAppFeature>>(null);
        }

        /// <summary>
        /// Get all employer app features
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public override async Task<IList<EmployerAppFeature>> GetEmployerAppFeatures()
        {
            return await Task.FromResult<IList<EmployerAppFeature>>(null);
        }
        
    }
}
