﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Data.Security;
using AT.Data.Security.Model;
using AT.Data.Security.Arguments;
using AT.Data.Security.Base;
using AT.Common.Core;
using AT.Core.Entities;
using AT.Entities.Authentication;

using AbsenceSoft;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Security;
using AT.Provider.Cache.Base;
using AT.Logic.Cache;

namespace AT.Logic.Security
{
    public class AuthorizationLogic
    {
        //constants for cache key
        private const string CUSTOMER_FEATURES = "AUTHORIZATION_CUSTOMER_FEATURES";
        private const string EMPLOYER_FEATURES = "AUTHORIZATION_EMPLOYER_FEATURES";
                
        /// <summary>
        /// The Authenticated User
        /// </summary>
        public Entities.Authentication.User AuthenticatedUser { get; set; }

        /// <summary>
        /// The current employer Id
        /// </summary>
        public string CurrentEmployerId { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="employerId">Set the current employer id</param>
        public AuthorizationLogic(string employerId)
        {
            this.CurrentEmployerId = employerId;
        }

        /// <summary>
        /// Overloade constructor for injecting user
        /// </summary>
        /// <param name="employerId">The current employer id</param>
        /// <param name="user"></param>
        public AuthorizationLogic(string employerId, Entities.Authentication.User user) : this(employerId)
        {
            this.AuthenticatedUser = user;
        }

        /// <summary>
        /// Check the AT user is authorized for a permission or not
        /// </summary>
        /// <returns></returns>
        public async Task<bool> IsAuthorized(ApplicationEnums.AppFeature feature, List<string> permissions)
        {
            if (AuthenticatedUser != null)
            {
                if (feature != ApplicationEnums.AppFeature.None)
                {
                    //get whether the employer has feature
                    bool hasFeature = await EmployerHasFeature(feature);

                    //if not enabled, check whether the customer has feature enabled or not
                    if (!hasFeature)
                    {
                        hasFeature = await CustomerHasFeature(feature);
                    }

                    if (hasFeature)
                    {
                        return true;
                    }
                }

                if (permissions.Any())
                {
                    var perm = await GetPermissions();
                    bool havePermission = perm.Any(p => permissions.Contains(p.Name));
                    if (havePermission)
                    {
                        return true;
                    }
                }
            }

            //return default
            return false;
        }

        /// <summary>
        /// Check whether customer feature is enabled
        /// </summary>
        /// <param name="feature"></param>
        /// <returns></returns>
        private async Task<bool> CustomerHasFeature(ApplicationEnums.AppFeature feature)
        {
            //get data from cache
            IList<CustomerAppFeature> customerFeatures = CacheHelper.Get<List<CustomerAppFeature>>(CUSTOMER_FEATURES);

            //if there is no data in cache, get from database
            if (customerFeatures == null)
            {
                customerFeatures = await AT.Data.Security.Base.DataProviderManager.Default.GetCustomerAppFeatures();
                CacheProviderManager.Default.Set(CUSTOMER_FEATURES, customerFeatures, new TimeSpan(0, 30, 0));
            }

            //iterate & search
            var usedFeature = customerFeatures
                                .FirstOrDefault(p => (p.CustomerId == AuthenticatedUser.CustomerId || p.CustomerKey == AuthenticatedUser.CustomerKey) && p.AppFeature == feature);

            if (usedFeature != null)
            {
                return true;
            }

            //return default
            return false;
        }

        /// <summary>
        /// Check whether the employer has feature
        /// </summary>
        /// <param name="feature"></param>
        /// <returns></returns>
        private async Task<bool> EmployerHasFeature(ApplicationEnums.AppFeature feature)
        {
            //get data from cache
            IList<EmployerAppFeature> employerFeatures = CacheHelper.Get<List<EmployerAppFeature>>(EMPLOYER_FEATURES);

            //if there is no data in cache, get from database
            if (employerFeatures == null)
            {
                employerFeatures = await AT.Data.Security.Base.DataProviderManager.Default.GetEmployerAppFeatures();
                CacheProviderManager.Default.Set(EMPLOYER_FEATURES, employerFeatures, new TimeSpan(0, 30, 0));
            }

            //iterate & search
            var usedFeature = employerFeatures
                                .FirstOrDefault(p => (p.EmployerKey == CurrentEmployerId) && p.AppFeature == feature);

            if (usedFeature != null)
            {
                return true;
            }

            //return default
            return false;
        }


        /// <summary>
        /// Get a user permission
        /// </summary>
        /// <returns></returns>
        private async Task<List<Entities.Authentication.Permission>> GetPermissions()
        {
            // No user, guess what, no permissions, DUH
            if (AuthenticatedUser == null)
            {
                return Entities.Authentication.Permission.None;
            }

            // Build our list of applied roles that will actually create the final permission set
            List<Entities.Authentication.Role> appliedRoles = new List<Entities.Authentication.Role>();

            // If we have an Employer Id, let's check that first to see if we have roles set for that Employer override
            if (!string.IsNullOrWhiteSpace(CurrentEmployerId) && AuthenticatedUser.Employers != null && AuthenticatedUser.Employers.Any())
            {
                // Get the Employer Access objects associated with the user
                foreach (var access in AuthenticatedUser.Employers.Where(p => p.EmployerId == CurrentEmployerId).ToList())
                {
                    // For each one, if we have roles, then let's add them to our applied roles if not already in there.
                    if (access.Roles != null && access.Roles.Any())
                    {
                        access.Roles.ForEach(r => appliedRoles.AddIfNotExists(r));
                    }
                }
            }


            // Let's see if there are root/global roles
            if (AuthenticatedUser.Roles != null && AuthenticatedUser.Roles.Any())
            {
                //add these as well if not already in there
                AuthenticatedUser.Roles.ForEach(r => appliedRoles.AddIfNotExists(r));
            }

            // No roles at all for this user at the top level or for the employer, return None.
            if (!appliedRoles.Any())
            {
                return Entities.Authentication.Permission.None;
            }

            // Check to see if the applied roles contain the System Administrator role.
            var sysAdmin = Entities.Authentication.Role.SystemAdministrator;
            // If they do, then yeah, grant those permissions instead
            if (appliedRoles.Any(p=> p.Key == sysAdmin.Key))
            {
                if (sysAdmin.Permissions.Any(p => p.Id == AbsenceSoft.Data.Security.Permission.CreateCaseWizardOnly.Id))
                {
                    sysAdmin.Permissions.Remove(sysAdmin.Permissions.FirstOrDefault(p => p.Id == AbsenceSoft.Data.Security.Permission.CreateCaseWizardOnly.Id));
                }
                return sysAdmin.Permissions;
            }

            return await Task.FromResult(appliedRoles.SelectMany(r => r.Permissions ?? new List<Entities.Authentication.Permission>(0)).Distinct().ToList());
        }

        

    }
}
