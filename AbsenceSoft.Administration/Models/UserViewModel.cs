﻿using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Administration.Models
{
    public class UserViewModel: IValidatableObject
    {
        public UserViewModel()
        {

        }

        public UserViewModel(User user)
            :this()
        {
            if (user == null)
                return;

            Id = user.Id;
            Email = user.Email;
            FirstName = user.FirstName;
            LastName = user.LastName;
            Roles = user.Roles;
            IsLocked = user.IsLocked;
            EndDateLock = user.EndDate < DateTime.Now;
            var customer = user.Customer;
            if(customer != null)
            {
                CustomerName = customer.Name;
                CustomerId = customer.Id;
            }
        }

        public string Id { get; set; }

        public string CustomerId { get; set; }
        
        public string CustomerName { get; set; }

        [Required, EmailAddress]
        public string Email { get; set; }

        [Required, DisplayName("First Name")]
        public string FirstName { get; set; }

        [Required, DisplayName("Last Name")]
        public string LastName { get; set; }

        [Required]
        public List<string> Roles { get; set; }

        public bool IsLocked { get; set; }
        public bool EndDateLock { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1} ({2})", FirstName, LastName, Email);
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            using (var authenticationService = new AuthenticationService())
            {
                if (authenticationService.UserExists(Email))
                    results.Add(new ValidationResult("A user record with that email address already exists", new List<string>() { "Email" }));
            }

            return results;
        }

        internal User ApplyToDataModel(User user)
        {
            if (user == null)
                user = new User();

            user.Email = Email;
            user.FirstName = FirstName;
            user.LastName = LastName;
            user.Roles = Roles;
            return user;
        }
    }
}