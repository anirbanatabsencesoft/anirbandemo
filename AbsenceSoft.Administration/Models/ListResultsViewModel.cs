﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Administration.Models
{
    public class ListResultsViewModel<T>
    {
        public long Total { get; set; }
        public IEnumerable<T> Results { get; set; }
    }
}