﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Administration.Models.Settings
{
    public class PermissionCategoryViewModel
    {
        public string Name { get; set; }
        public List<PermissionViewModel> Permissions {get; set;}
        
    }
}