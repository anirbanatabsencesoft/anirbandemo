﻿using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Administration.Models.Settings
{
    public class RoleViewModel
    {
        public RoleViewModel()
        {

        }

        public RoleViewModel(Role role)
            :this()
        {
            if (role == null)
                return;

            Id = role.Id;
            Name = role.Name;
            Permissions = role.Permissions;
        }

        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public List<string> Permissions { get; set; }

        internal Role ApplyToDataModel(Role adminRole)
        {
            if (adminRole == null)
                adminRole = new Role();

            adminRole.Name = Name;
            adminRole.Permissions = Permissions;

            return adminRole;
        }
    }
}