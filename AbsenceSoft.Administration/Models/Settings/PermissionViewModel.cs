﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Administration.Models.Settings
{
    public class PermissionViewModel
    {
        public string Name { get; set; }
        public bool Checked { get; set; }
        public string Id { get; set; }
    }
}