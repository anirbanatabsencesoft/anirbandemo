﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Administration.Models
{
    public class EmployeeViewModel
    {
       
        public string EmployeeId { get; set; }
        public string EmployerId { get; set; }
        public string CustomerId { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? HireDate { get; set; }
        public string EmployerName { get; set; }        
        public string HoursWorkedPerWeek { get; set; }
        public string OfficeLocation { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? DOB { get; set; }
        public Gender? Gender { get; set; }
        public string SSN { get; set; }
        public EmployeeInfo EmpInfo { get; set; }       
        public string EmployeeNumber { get; set; }
        public string FullName { get; set; }
        public double? HoursWorked { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Status { get; internal set; }
    }
}