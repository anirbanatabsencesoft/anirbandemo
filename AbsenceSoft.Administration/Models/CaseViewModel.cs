﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.Administration.Models
{
    public class CaseViewModel
    {
        //Employer Case number
        public string caseNumber { get; set; }        
        public string employerId { get; set; }
        //Case requested start date
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? StartDate { get; set; }

        //Case requested end date
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? EndDate { get; set; }
        //Employee contact as case reporter
        public List<EmployeeContact> CaseReporter { get; set; }
        //Case status can be open,close,cancel
        public CaseStatus Status { get; set; }
        //Case description
        public string Description { get; set; }
        //Case reason like Emloyee health condition
        public string Reason { get; set; }
        //Case AssignTo-assigning the case to employee for review.
        public string AssignTo { get; set; }
        //CaseType can be consecutive,intermittent etc.
        public string CaseType { get; set; }
        //CaseId is unique identifier of case
        public string CaseId { get; set; }
        //ToDos specific to case can be assign in this property
        public IEnumerable<ToDosViewModel> todoitem { get; set; }
       
    }
}