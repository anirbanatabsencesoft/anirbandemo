﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Administration.Models
{
    public class EmployerViewModel:IHasFeatures
    {
        public EmployerViewModel()
        {
            Features = new List<FeatureViewModel>();
        }

        public EmployerViewModel(Employer employer)
            :this()
        {
            Id = employer.Id;
            Name = employer.Name;
            Customer customer = employer.Customer;
            CustomerName = customer.Name;
            CustomerId = customer.Id;
            List<FeatureViewModel> employerFeatures = new List<FeatureViewModel>();
            foreach (Feature feature in Enum.GetValues(typeof(Feature)))
            {
                if (feature == Feature.None)
                    continue;

                var employerFeature = new FeatureViewModel()
                {
                    Feature = feature,
                    Enabled = employer.HasFeature(feature)
                };

                employerFeatures.Add(employerFeature);
            }
            Features = employerFeatures.OrderBy(cf => cf.Feature.ToString()).ToList();
        }


        public string Id { get; set; }
        public string Name { get; set; }
        public string CustomerName { get; set; }
        public string CustomerId { get; set; }
        public string SaveRoute { get { return "SaveEmployerFeatures"; } }
        public List<FeatureViewModel> Features { get; set; }

        internal Employer UpdateEmployerFeatures(Employer employer)
        {
            if (employer == null)
                throw new ArgumentNullException("customer");

            var customer = employer.Customer;
            foreach (FeatureViewModel feature in Features.Where(f => f.Feature != Feature.None))
            {
                // Only add an employer specific entry if they don't match with what the customer has 
                bool empFeatureStatus = employer.Features.SingleOrDefault(f => f.Feature == feature.Feature)?.Enabled ?? customer.HasFeature(feature.Feature);
                if (customer.HasFeature(feature.Feature) && (empFeatureStatus != feature.Enabled))
                {
                    employer.AddFeature(feature.Feature, feature.Enabled);
                }
            }

            return employer;
        }
    }
}