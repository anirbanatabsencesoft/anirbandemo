﻿using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Web.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Administration.Models.Communication
{
    public class PaperworkTemplateViewModel
    {
        public PaperworkTemplateViewModel()
        {
            Criteria = new List<PaperworkTemplateCriteriaGroupViewModel>();
            Fields = new List<PaperworkFieldViewModel>();
        }

        public PaperworkTemplateViewModel(Paperwork paperwork)
            : this()
        {
            if (paperwork == null)
                return;

            Id = paperwork.Id;
            Name = paperwork.Name;
            Code = paperwork.Code;
            DocumentId = paperwork.FileId;
            DocumentName = paperwork.FileName;
            DocType = paperwork.DocType;
            Description = paperwork.Description;
            RequiresReview = paperwork.RequiresReview;
            IsForApprovalDecision = paperwork.IsForApprovalDecision;
            ReturnDateAdjustment = paperwork.ReturnDateAdjustmentDays;

            if (paperwork.Criteria != null)
                Criteria = paperwork.Criteria.Select(c => new PaperworkTemplateCriteriaGroupViewModel(c)).ToList();

            if (paperwork.Fields != null)
                Fields = paperwork.Fields.Select(f => new PaperworkFieldViewModel(f)).ToList();
        }

        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required, CodeFormat]
        public string Code { get; set; }

        public string DocumentId { get; set; }

        public string DocumentName { get; set; }

        [Required]
        public DocumentType? DocType { get; set; }

        public string Description { get; set; }

        [DisplayName("Enclosure Text")]
        public string EnclosureText { get; set; }

        [DisplayName("Requires Review")]
        public bool RequiresReview { get; set; }

        [DisplayName("Is For Approval Decision")]
        public bool IsForApprovalDecision { get; set; }

        [DisplayName("Return Date Adjustment")]
        public int? ReturnDateAdjustment { get; set; }

        public List<PaperworkTemplateCriteriaGroupViewModel> Criteria { get; set; }

        public List<PaperworkFieldViewModel> Fields { get; set; }

        public Paperwork ApplyToDataModel(Paperwork paperwork) {
            if (paperwork == null)
                paperwork = new Paperwork();

            paperwork.Name = Name;
            paperwork.Code = Code;
            paperwork.FileId = DocumentId;
            paperwork.FileName= DocumentName;
            paperwork.DocType = DocType.Value;
            paperwork.Description = Description;
            paperwork.EnclosureText = EnclosureText;
            paperwork.RequiresReview = RequiresReview;
            paperwork.IsForApprovalDecision = IsForApprovalDecision;
            if (ReturnDateAdjustment.HasValue)
            {
                paperwork.ReturnDateAdjustment = TimeSpan.FromDays(ReturnDateAdjustment.Value);
            }

            paperwork.Criteria = Criteria.Select(c => new PaperworkCriteriaGroup()
            {
                Name = c.Name,
                SuccessType = c.SuccessType.Value,
                Criteria = c.Group.Select(g => new PaperworkCriteria()
                {
                    Criteria = g.Criteria,
                    CriteriaType = g.CriteriaType.Value
                }).ToList()
            }).ToList();

            return paperwork;
        }
    }
}