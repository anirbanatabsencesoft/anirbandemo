﻿using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Enums;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.Administration.Models.Communication
{
    public class PaperworkTemplateCriteriaViewModel
    {
        public PaperworkTemplateCriteriaViewModel()
        {

        }

        public PaperworkTemplateCriteriaViewModel(PaperworkCriteria criteria)
            :this()
        {
            if (criteria == null)
                return;

            CriteriaType = criteria.CriteriaType;
            Criteria = criteria.Criteria;
        }

        [Required]
        public PaperworkCriteriaType? CriteriaType { get; set; }

        [Required]
        public string Criteria { get; set; }
    }
}