﻿using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace AbsenceSoft.Administration.Models.Communication
{
    public class PaperworkTemplateCriteriaGroupViewModel
    {
        public PaperworkTemplateCriteriaGroupViewModel()
        {
            Group = new List<PaperworkTemplateCriteriaViewModel>();
        }

        public PaperworkTemplateCriteriaGroupViewModel(PaperworkCriteriaGroup group)
            :this()
        {
            if (group == null)
                return;

            SuccessType = group.SuccessType;
            Name = group.Name;
            Description = group.Description;
            Group = group.Criteria.Select(c => new PaperworkTemplateCriteriaViewModel(c)).ToList();
        }

        [Required]
        public RuleGroupSuccessType? SuccessType { get; set; }

        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public List<PaperworkTemplateCriteriaViewModel> Group { get; set; }
    }
}