﻿using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Administration.Models.Communication
{
    public class PaperworkFieldViewModel
    {
        

        public PaperworkFieldViewModel(PaperworkField field)
        {
            if (field == null)
                return;

            Name = field.Name;
            Status = field.Status;
            Type = field.Type;
            Required = field.Required;
            Order = field.Order;
        }

        public string Name { get; set; }

        public PaperworkFieldStatus Status { get; set; }

        public PaperworkFieldType Type { get; set; }

        public bool Required { get; set; }

        public int? Order { get; set; }
    }
}