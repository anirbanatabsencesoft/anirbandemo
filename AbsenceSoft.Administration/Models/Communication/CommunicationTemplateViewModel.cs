﻿using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Web.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Administration.Models.Communication
{
    public class CommunicationTemplateViewModel
    {
        public CommunicationTemplateViewModel()
        {
            PaperworkCodes = new List<string>();
        }

        public CommunicationTemplateViewModel(Template template)
            :this()
        {
            if (template == null)
                return;

            Id = template.Id;
            Name = template.Name;
            Subject = template.Subject;
            Code = template.Code;
            AttachLatestIncompletePaperwork = template.AttachLatestIncompletePaperwork;
            IsLOASpecific = template.AttachLatestIncompletePaperwork;

            DocumentId = template.DocumentId;
            DocumentName = template.DocumentName;
            DocType = template.DocType;

            PaperworkCodes = template.PaperworkCodes;
        }

        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Subject { get; set; }

        [Required, CodeFormat]
        public string Code { get; set; }

        [DisplayName("Attach Latest Incomplete Paperwork?")]
        public bool AttachLatestIncompletePaperwork { get; set; }

        [DisplayName("Is Leave Of Absence Specific?")]
        public bool IsLOASpecific { get; set; }


        public string DocumentId { get; set; }

        public string DocumentName { get; set; }

        [Required, DisplayName("Document Type")]
        public DocumentType? DocType { get; set; }

        [DisplayName("Paperwork")]
        public List<string> PaperworkCodes { get; set; }

        public Template ApplyToDataModel(Template template)
        {
            if (template == null)
                template = new Template();

            template.Name = Name;
            template.Subject = Subject;
            template.Code = Code;
            template.AttachLatestIncompletePaperwork = AttachLatestIncompletePaperwork;
            template.IsLOASpecific = IsLOASpecific;
            template.DocType = DocType.Value;
            template.DocumentId = DocumentId;
            template.DocumentName = DocumentName;
            template.PaperworkCodes = PaperworkCodes;

            return template;
        }
    }
}