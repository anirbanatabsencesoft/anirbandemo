﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Administration.Models
{
    public class FeatureViewModel
    {
        public Feature Feature { get; set; }
        public bool Enabled { get; set; }

        public override string ToString()
        {
            return Feature.ToString().SplitCamelCaseString();
        }
    }
}