﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbsenceSoft.Data.Reporting;
using System.Web.Mvc;

namespace AbsenceSoft.Administration.Models
{
    public class CustomerDetailsViewModel
    {
        public CustomerDetailsViewModel(ReportCustomerDetails customerDetails)
        {
            CustomerId = customerDetails.CustomerId;
            Customer = customerDetails.Customer;
            Employer = customerDetails.Employer;
            EmployeeCount = customerDetails.EmployeeCount;
            EmployeeCount6month = customerDetails.EmployeeCount6month;
            EmployeeCount3month = customerDetails.EmployeeCount3month;
            EmployeeCount1month = customerDetails.EmployeeCount1month;
            CaseCount = customerDetails.CaseCount;
            TodoCount = customerDetails.TodoCount;
            AttachmentCount = customerDetails.AttachmentCount;
            NotesCount = customerDetails.NotesCount;
            ActiveUsers = customerDetails.ActiveUsers;
        }

        public long CustomerId { get; set; }
        public string Customer { get; set; }
        public string Employer { get; set; }
        public int EmployeeCount { get; set; }
        public int EmployeeCount6month { get; set; }
        public int EmployeeCount3month { get; set; }
        public int EmployeeCount1month { get; set; }
        public int CaseCount { get; set; }
        public int TodoCount { get; set; }
        public int AttachmentCount { get; set; }
        public int NotesCount { get; set; }
        public int ActiveUsers { get; set; }

        /// <summary>
        /// Gets a list of last 12 month's dates in yyyy-MM-dd format, where dd is the last day of month.
        /// </summary>
        public IEnumerable<SelectListItem> Last12Months
        {
            get
            {
                var dates = Enumerable.Range(0, 12).Reverse()
                                 .Select(i => DateTime.UtcNow.AddMonths(i - 12))
                                 .Select(date => date.ToString("yyyy-MM-" + DateTime.DaysInMonth(date.Year, date.Month).ToString()));
                return dates.Select(e => new SelectListItem { Text = e });
            }
        }
    }
}