﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Administration.Models
{
    public interface IHasFeatures
    {
        string Id { get; set; }
        string SaveRoute { get; }
        List<FeatureViewModel>  Features { get; set; }
    }
}
