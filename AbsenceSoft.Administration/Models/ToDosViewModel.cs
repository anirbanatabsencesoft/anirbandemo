﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.ToDo;
using System;
using System.Collections.Generic;

namespace AbsenceSoft.Administration.Models
{
    public class ToDosViewModel
    {
        public ToDosViewModel()
        {
            todoModel = new List<ToDosViewModel>();
        }
        public ToDosViewModel(ToDoItem ToDoItems)
        : this()
        {
            ToDosViewModel ToDosItem = new ToDosViewModel();
            List<ToDosViewModel> todo = new List<ToDosViewModel>();            
            ToDosItem.DueDate = ToDoItems!=null? Convert.ToDateTime(ToDoItems.DueDate).ToString("MM/dd/yyyy"):"";
            ToDosItem.Status = ToDoItems!=null?ToDoItems.Status:ToDoItemStatus.Pending;
            ToDosItem.TodoTitle = ToDoItems!=null? ToDoItems.Title:string.Empty;
            ToDosItem.Id = ToDoItems!=null? ToDoItems.Id:string.Empty;
            ToDosItem.CaseId = ToDoItems!=null?ToDoItems.CaseId:string.Empty;
            ToDosItem.CaseNumber = ToDoItems!=null? ToDoItems.CaseNumber:string.Empty;            
            ToDosItem.AssignedTo = ToDoItems!=null? ToDoItems.AssignedToName:string.Empty;
            todo.Add(ToDosItem);
            todoModel = todo;

        }
        //ToDos id of a case id
        public string Id { get; set; }
        //Unique case id for employer
        public string CaseId { get; set; }
        //Unique CaseNumber for employer
        public string CaseNumber { get; set; }
        //Employer name like Absencesoft
        public string EmployerName { get; set; }
        //Case ToDos like-Rewturn to work,HR communication etc.
        public string TodoTitle { get; set; }
        //Due date to complete the todos
        public string DueDate { get; set; }
        //To whome assigned to complete the todos
        public string AssignedTo { get; set; }
        //ToDo status can be Pending,Cancel,Completed,Overdue etc.
        public ToDoItemStatus Status { get; set; }
        //To hold the ToDos details
        public List<ToDosViewModel> todoModel { get; set; }
        
    }
}