﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Administration.Models
{
    public class CompleteButtonConfirmationViewModel
    {
        public CompleteButtonConfirmationViewModel()
        {

        }

        public CompleteButtonConfirmationViewModel(string url, string message)
        {
            Url = url;
            Message = message;
        }

        public string Url { get; set; }
        public string Message { get; set; }

    }
}