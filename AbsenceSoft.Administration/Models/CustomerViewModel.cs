﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Administration.Models
{
    public class CustomerViewModel:IHasFeatures
    {
        public CustomerViewModel()
        {
            Features = new List<FeatureViewModel>();
        }

        public CustomerViewModel(Customer customer)
            :this()
        {
            Id = customer.Id;
            Name = customer.Name;
            DefaultFromEmail = customer.DefaultEmailFromAddress;
            EmailDomain = customer.EmailDomain;
            List<FeatureViewModel> customerFeatures = new List<FeatureViewModel>();
            foreach (Feature feature in Enum.GetValues(typeof(Feature)))
            {
                if (feature == Feature.None)
                    continue;

                var customerFeature = new FeatureViewModel()
                {
                    Feature = feature,
                    Enabled = customer.HasFeature(feature)
                };

                customerFeatures.Add(customerFeature);
            }
            Features = customerFeatures.OrderBy(cf => cf.Feature.ToString()).ToList();
        }

        [Required]
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        public List<FeatureViewModel> Features { get; set; }

        [EmailAddress, DisplayName("Default From Email")]
        public string DefaultFromEmail { get; set; }

        [DisplayName("Email Domain")]
        public string EmailDomain { get; set; }

        public string SaveRoute { get { return "SaveCustomerFeatures"; } }

        internal Customer UpdateCustomerDetails(Customer customer)
        {
            if (customer == null)
                throw new ArgumentNullException("customer");

            customer.Name = Name;
            customer.DefaultEmailFromAddress = DefaultFromEmail;
            if (!string.IsNullOrEmpty(EmailDomain))
            {
                customer.EmailDomain = EmailDomain.ToLowerInvariant();
            }
            else
            {
                customer.EmailDomain = null;
            }
            

            return customer;
        }

        internal Customer UpdateCustomerFeatures(Customer customer)
        {
            if (customer == null)
                throw new ArgumentNullException("customer");

            foreach (FeatureViewModel feature in Features.Where(f => f.Feature != Feature.None))
            {
                customer.AddFeature(feature.Feature, feature.Enabled);
            }

            return customer;
        }
    }
}