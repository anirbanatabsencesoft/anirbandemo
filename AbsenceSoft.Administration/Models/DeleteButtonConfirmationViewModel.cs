﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Administration.Models
{
    public class DeleteButtonConfirmationViewModel
    {
        public DeleteButtonConfirmationViewModel()
        {

        }

        public DeleteButtonConfirmationViewModel(string url, string message)
        {
            Url = url;
            Message = message;
        }

        public string Url { get; set; }
        public string Message { get; set; }
    }
}