﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Administration.Models.Home
{
    public class ExpiredPasswordViewModel:IValidatableObject
    {
        [Required, DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required, DisplayName("Current Password"), DataType(DataType.Password)]
        public string CurrentPassword { get; set; }

        [Required, DisplayName("New Password"), DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Required, DisplayName("Confirm Password"), DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            if (NewPassword != ConfirmPassword)
            {
                results.Add(new ValidationResult("Password and Confirmation do not match"));
            }

            using (var authService = new AuthenticationService())
            using (var adminService = new AdministrationService())
            {
                var user = adminService.GetUserByEmail(Email);
                if(user != null)
                {
                    if (!adminService.CheckPassword(user, CurrentPassword))
                    {
                        results.Add(new ValidationResult("Current password is not correct", new List<string>() { "CurrentPassword" }));
                    }

                    ICollection<string> errors;
                    if (!authService.ValidatePasswordAgainstPolicy(new PasswordPolicy(), user, NewPassword, out errors))
                    {
                        results.Add(new ValidationResult(string.Join("\n", errors), new List<string>() { "Password" }));
                    }
                }
            }

            return results;
        }
    }
}