﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Administration.Models.Home
{
    public class ResetPasswordViewModel:IValidatableObject
    {
        public ResetPasswordViewModel()
        {
        }

        public ResetPasswordViewModel(string resetKey)
            :this()
        {
            ResetKey = resetKey;
        }

        [Required]
        public string ResetKey { get; set; }

        [Required, DisplayName("New Password"), DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Required, DisplayName("Confirm Password"), DataType(DataType.Password)]
        public string ConfirmNewPassword { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            if(NewPassword != ConfirmNewPassword)
            {
                results.Add(new ValidationResult("Password and Confirmation do not match"));
            }

            return results;
        }
    }
}