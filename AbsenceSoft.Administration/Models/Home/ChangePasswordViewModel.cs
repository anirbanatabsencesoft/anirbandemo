﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Administration.Models.Home
{
    public class ChangePasswordViewModel:IValidatableObject
    {
        [Required, DataType(DataType.Password)]
        public string Password { get; set; }

        [Required, DisplayName("Confirm"), DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();

            if (Password != ConfirmPassword) {
                results.Add(new ValidationResult("Password and Confirm Password do not match"));
            }

            using (var authenticationService = new AuthenticationService())
            {
                ICollection<string> errors = new List<string>();
                authenticationService.ValidatePasswordAgainstPolicy(PasswordPolicy.Default(), Current.User(), Password, out errors);
                if(errors.Count > 0)
                {
                    results.AddRange(errors.Select(e => new ValidationResult(e)));
                }
            }

            return results;
        }
    }
}