﻿using AbsenceSoft.Administration.Models;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Web.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Administration.Controllers
{
    [Secure]
    public class EmployerController : BaseAdministrationController
    {
        [Secure("EditEmployerInfo"), HttpGet, Route("Employer/Search", Name = "SearchEmployers")]
        public ActionResult SearchEmployer(string searchterm)
        {
            using(var customerService = new AdminCustomerService())
            {
                CustomerCriteria criteria = new CustomerCriteria()
                {
                    Name = searchterm
                };
                IEnumerable<EmployerViewModel> employers = customerService.GetEmployers(criteria).Results.Select(e => new EmployerViewModel(e));
                return PartialView("_EmployerList", employers);
            }
        }

        [Secure("EditEmployerInfo"), Route("Employer/{id}", Name ="EditEmployer")]
        public ActionResult EditEmployer(string id)
        {
            using (var customerService = new AdminCustomerService())
            {
                EmployerViewModel employer = new EmployerViewModel(customerService.GetEmployer(id));
                return View(employer);
            }
        }

        [Secure("EditEmployerInfo"), HttpPost, Route("Employer/SaveFeatures", Name = "SaveEmployerFeatures")]
        public ActionResult SaveEmployerFeatures(EmployerViewModel employer)
        {
            using (var customerService = new AdminCustomerService())
            {
                Employer toSave = customerService.GetEmployer(employer.Id);
                toSave = employer.UpdateEmployerFeatures(toSave);
                toSave = customerService.SaveEmployer(toSave);
                return PartialView("_Features", new EmployerViewModel(toSave));
            }
        }

        [Secure("ClearEmployerData"), HttpPost, Route("Employer/ClearEmployerData/{employerId}", Name = "ClearEmployerData")]
        public ActionResult ClearEmployerData(string employerId, bool removeConfig)
        {
            using (var customerService = new AdminCustomerService())
            {
                customerService.ClearEmployerData(employerId, removeConfig);
                return RedirectToRoute("EditEmployer", new { id = employerId });
            }
        }
    }
}