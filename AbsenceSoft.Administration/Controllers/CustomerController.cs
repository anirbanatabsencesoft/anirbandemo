﻿using AbsenceSoft.Administration.Models;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Web.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Administration.Controllers
{
    [Secure]
    public class CustomerController : BaseAdministrationController
    {
        [Secure("EditCustomerInfo"), HttpGet, Route("Customer/Search", Name = "SearchCustomers")]
        public ActionResult SearchCustomer(string searchterm)
        {
            using (var customerService = new AdminCustomerService())
            {
                CustomerCriteria criteria = new CustomerCriteria()
                {
                    Name = searchterm
                };
                IEnumerable<CustomerViewModel> customers = customerService.GetCustomers(criteria).Results.Select(c => new CustomerViewModel(c));
                return PartialView("_CustomerList", customers);
            }

        }

        [Secure("EditCustomerInfo"), HttpGet, Route("Customer/{customerId}", Name = "EditCustomer")]
        public ActionResult EditCustomer(string customerId)
        {
            if (customerId == null)
                return RedirectToAction("Index", "Home");

            using (var customerService = new AdminCustomerService())
            {
                CustomerViewModel customer = new CustomerViewModel(customerService.GetCustomer(customerId));
                return View(customer);
            }

        }

        [ChildActionOnly]
        public ActionResult CustomerUsers(string customerId)
        {
            UserCriteria criteria = new UserCriteria()
            {
                CustomerId = customerId
            };

            return PartialView(criteria);
        }

        [ChildActionOnly]
        public ActionResult Employers(string customerId)
        {
            using (var customerService = new AdminCustomerService())
            {
                List<EmployerViewModel> employers = customerService.GetEmployers(customerId).Select(e => new EmployerViewModel(e)).ToList();
                return PartialView("_Employers", employers);
            }
        }

        [Secure("EditCustomerInfo"), HttpPost, Route("Customer/SaveFeatures", Name = "SaveCustomerFeatures")]
        public ActionResult SaveCustomerFeatures(CustomerViewModel customer)
        {
            using (var customerService = new AdminCustomerService())
            {
                Customer toSave = customerService.GetCustomer(customer.Id);
                toSave = customer.UpdateCustomerFeatures(toSave);
                toSave = customerService.SaveCustomer(toSave);

                if (toSave.HasFeature(Feature.EmployeeSelfService))               
                { 
                    customerService.EnsureDefaultSelfServiceRolesForCustomer(toSave);
                }

                return PartialView("_Features", new CustomerViewModel(toSave));
                
            }
        }

        [Secure("EditCustomerInfo"), HttpPost, Route("Customer/SaveDetails", Name = "SaveCustomerDetails")]
        public ActionResult SaveCustomerDetails(CustomerViewModel customer)
        {
            if (!ModelState.IsValid)
                return PartialView("_CustomerDetails", customer);

            using (var customerService = new AdminCustomerService())
            {
                Customer toSave = customerService.GetCustomer(customer.Id);
                toSave = customer.UpdateCustomerDetails(toSave);
                toSave = customerService.SaveCustomer(toSave);

                return PartialView("_CustomerDetails", new CustomerViewModel(toSave));
            }
        }

        [Secure("CloseAccount"), HttpDelete, Route("Customer/Delete/{customerId}", Name = "DeleteCustomer")]
        public ActionResult DeleteCustomer(string customerId)
        {
            using (var customerService = new AdminCustomerService())
            {
                customerService.DeleteCustomer(customerId);
                return RedirectViaJavaScript(Url.RouteUrl("Home"));
            }
        }

    }
}