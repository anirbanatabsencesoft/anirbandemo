﻿using AbsenceSoft.Administration.Models;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Web.Attributes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Administration.Controllers
{
    [Secure]
    public class ReportController : BaseAdministrationController
    {
        [Secure("ViewAdminReport"), HttpGet]
        public ActionResult CustomerDetails()
        {
            using (AdminCustomerService service = new AdminCustomerService())
            {
                IEnumerable<CustomerDetailsViewModel> customerDetailsReport = service.GetCustomersDetail().Select(e => new CustomerDetailsViewModel(e));
                return View(customerDetailsReport);
            }
        }

        [Secure("ViewAdminReport"), HttpGet]
        public ActionResult CustomerBillingDetails(DateTime? startDate, DateTime? endDate)
        {
            using (AdminCustomerService service = new AdminCustomerService())
            {
                IEnumerable<CustomerDetailsViewModel> customerDetailsReport = service.GetCustomersDetailByCriteria(startDate, endDate).Select(e => new CustomerDetailsViewModel(e));
                if (Request.IsAjaxRequest())
                {
                    return PartialView("_CustomerBillingDetails", customerDetailsReport);
                }
                return View(customerDetailsReport);
            }
        }

        [Secure, HttpGet]
        public ActionResult Index()
        {
            return View();
        }
    }
}