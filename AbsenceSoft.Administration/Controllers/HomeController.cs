﻿using AbsenceSoft.Administration.Filters;
using AbsenceSoft.Administration.Models;
using AbsenceSoft.Administration.Models.Home;
using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Web;
using AbsenceSoft.Web.Attributes;
using AT.Entities.Authentication;
using AT.Logic.Authentication;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Administration.Controllers
{
    public class HomeController : BaseAdministrationController
    {
        private ApplicationSignInManager _signInManager;
        public HomeController()
        {

        }
        public HomeController(ApplicationSignInManager applicationSignInManager)
        {
            _signInManager = applicationSignInManager;
        }

        /// <summary>
        /// Gets the new object of ApplicationSignInManager.
        /// </summary>
        /// <value>
        /// The sign in manager.
        /// </value>
        protected ApplicationSignInManager SignInManager
        {
            get
            {
                if (_signInManager == null)
                    _signInManager = new ApplicationSignInManager(HttpContext.GetOwinContext().Authentication);
                return _signInManager;
            }
        }
        [Secure]
        [Route(Name = "Home")]
        public ActionResult Index()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult UserName()
        {
            UserViewModel user = new UserViewModel(Current.User());
            return PartialView(user);
        }

        [HttpGet, Route("Login", Name ="Login")]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost, Route("Login", Name ="LoginPost")]
        public async Task<ActionResult> Login(LoginViewModel login)
        {
            if (!ModelState.IsValid)
            {
                return View(login);
            }


            Data.Security.User loginUser = null;

            SignInStatus result = await SignInManager.SignInAsync(login.Email, login.Password, ApplicationType.Administration);
   
            using (var service = new AdministrationService())
            {
                loginUser = service.GetUserByEmail(login.Email);
            }
            
            switch (result)
            {
                case SignInStatus.Ok:
                    if (loginUser == null)
                    {
                        ModelState.AddModelError("", "Your login attempt has failed. The username and/or password may be incorrect. If you feel your account is properly registered with us, click on \"Forgot Password\" below to retrieve your account details.");
                        break;
                    }
                    return RedirectToRoute("Home");
                case SignInStatus.Error:
                    ModelState.AddModelError("", "Your login attempt has failed. The username and/or password may be incorrect. If you feel your account is properly registered with us, click on \"Forgot Password\" below to retrieve your account details.");
                    break;
                case SignInStatus.Locked:
                case SignInStatus.Disabled:
                    ModelState.AddModelError("", "Your account has been locked out for security reasons. You should receive an email with instructions how to unlock your account. Please contact AbsenceSoft team if you still having problem with your login.");
                    break;
                case SignInStatus.Expired:
                    return RedirectToRoute("ExpiredPassword");
            }

                ///If we get this far, something failed and a message should be populated
                return View(login);
            
        }

        [HttpGet, Route("Logout", Name = "Logout")]
        public async Task<ActionResult> Logout()
        {
            if (Request.IsAuthenticated)
            {
                await LogoutUser();
            }

            return RedirectToRoute("Login");

        }

        private async Task LogoutUser()
        {
            try
            {
                await SignInManager.SignOutAsync();               
            }
            catch (Exception ex)
            {
                Log.Error("cannot logout the user", ex);
            }

        }

        [Title("Forgot Password?")]
        [HttpGet, Route("ForgotPassword", Name = "ForgotPassword")]
        public ViewResult ForgotPassword()
        {
            ForgotPasswordViewModel model = new ForgotPasswordViewModel();
            return View(model);
        }

        [Title("Forgot Password?")]
        [HttpPost, Route("ForgotPassword", Name = "ForgotPasswordSubmit")]
        public ViewResult ForgotPasswordSubmit(ForgotPasswordViewModel forgotPassword)
        {
            if (!ModelState.IsValid)
                return View("ForgotPassword", forgotPassword);

            using (var authenticationService = new AuthenticationService())
            {
                authenticationService.ForgotPassword(forgotPassword.Email);
            }
            return View();
        }

        [Secure, HttpPost, Route("Home/ChangePassword", Name ="ChangePasswordPost")]
        public JsonResult ChangePassword(ChangePasswordViewModel changePassword)
        {
            if (!ModelState.IsValid)
            {
                List<string> errors = new List<string>();
                foreach (var value in ModelState.Values)
                {
                    foreach (var error in value.Errors)
                    {
                        if (!string.IsNullOrEmpty(error.ErrorMessage))
                        {
                            errors.Add(error.ErrorMessage);
                        }
                        
                    }
                }
                return Json(new { success = false, errors = errors });
            }
                

            using (var adminService = new AdministrationService())
            {
                adminService.ChangePassword(Current.User(), changePassword.Password);
            }

            return Json(new { success = true });
        }

        [Title("Reset Password")]
        [HttpGet, Route("ResetPassword/{resetKey}", Name = "ResetPassword")]
        public ActionResult ResetPassword(string resetKey)
        {
            if (string.IsNullOrWhiteSpace(resetKey))
                return RedirectToRoute("Login");

            return View(new ResetPasswordViewModel(resetKey));
        }

        [Title("Reset Password")]
        [HttpPost, Route("ResetPassword", Name = "ResetPasswordSubmit")]
        public ActionResult ResetPasswordSubmit(ResetPasswordViewModel resetPassword)
        {
            if (!ModelState.IsValid)
                return View("ResetPassword", resetPassword);

            using (var authService = new AuthenticationService())
            {
                authService.ResetPassword(resetPassword.ResetKey, resetPassword.NewPassword);
            }

            return View();
        }

        [Title("Expired Password")]
        [HttpGet, Route("ExpiredPassword", Name = "ExpiredPassword")]
        public ActionResult ExpiredPassword()
        {
            return View();
        }

        [Title("Expired Password")]
        [HttpPost, Route("ExpiredPassword", Name = "ExpiredPasswordSubmit")]
        public ActionResult ExpiredPasswordSubmit(ExpiredPasswordViewModel expiredPassword)
        {
            if (!ModelState.IsValid)
                return View("ExpiredPassword", expiredPassword);

            using (var adminService = new AdministrationService())
            {
                var user = adminService.GetUserByEmail(expiredPassword.Email);
                if (user != null)
                    adminService.ChangePassword(user, expiredPassword.NewPassword);
            }

            return View();
        }


        [Secure]
        [HttpGet, Route("Documents/Download/{documentId}", Name = "DownloadDocument")]
        public ActionResult Download(string documentId)
        {
            Document d = Document.GetById(documentId);
            if (d == null)
                return null;

            return Redirect(d.DownloadUrl());
        }

        [Secure, HttpPost]
        [Route("Administration/RenewSession", Name = "RenewSession")]
        public async Task<JsonResult> RenewSession()
        {
            if (User.Identity.IsAuthenticated && User.Identity is ClaimsIdentity identity)
            {
                var token = identity.FindFirstValue("token");
                if (token != null)
                {
                    await SignInManager.SignInRenewAysnc(token, ApplicationType.Administration);
                }
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Use to validate user, and retrieve the current user Identity value
        /// </summary>
        /// <returns>Auth details with time expiration</returns>
        [DoNotResetCookie]
        [HttpGet]
        [Route("Home/GetUserIsAuthenticatedOrNot", Name = "GetUserIsAuthenticatedOrNot")]
        public JsonResult IsUserAuthenticated()
        {
            var authDetails = new UserIsAuthenticatedViewModel();
            if (!(User.Identity is ClaimsIdentity identity))
            {
                return Json(authDetails, JsonRequestBehavior.AllowGet);
            };
            var token = identity.FindFirstValue("token");
            if (token == null)
            {
                return Json(authDetails, JsonRequestBehavior.AllowGet);
            };
            JwtSecurityToken jwtToken = new JwtSecurityToken(token);            
            authDetails.IsUserAuthenticated = identity.IsAuthenticated;
            authDetails.WillExpire = Data.Customers.SecuritySettings.ShowTimeoutDialog(HttpContext, jwtToken.ValidTo);
            authDetails.WillExpireSameDayOrWithinNextMeridiem = Data.Customers.SecuritySettings.WillExpireSameDayOrWithinNextMeridiem(HttpContext, jwtToken.ValidTo);
            authDetails.ExpirationTime = jwtToken.ValidTo.ToUniversalTime().ToUnixDate();
            return Json(authDetails, JsonRequestBehavior.AllowGet);
        }
    }
      
}