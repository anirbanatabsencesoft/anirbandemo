﻿using AbsenceSoft.Administration.Models;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Web.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Administration.Controllers
{
    [Secure]
    public class UserController : BaseAdministrationController
    {
        [Secure("EditCustomerUsers"), HttpPost, Route("Users", Name = "GetUsers")]
        public ActionResult CustomerUsers(UserCriteria criteria)
        {
            using(var userService = new AdminUserService())
            {
                var results = userService.GetUsers(criteria);
                ListResultsViewModel<UserViewModel> users = new ListResultsViewModel<UserViewModel>()
                {
                    Total = results.Total,
                    Results = results.Results.Select(u => new UserViewModel(u)).ToList()
                };

                return PartialView("_Users", users);
            }
        }

        [Secure("EditCustomerUsers"), HttpGet, Route("User/Edit/{userId}", Name = "EditUser")]
        public ActionResult EditUser(string userId)
        {
            using (var userService = new AdminUserService())
            {
                UserViewModel user = new UserViewModel(userService.GetUserById(userId));
                return View(user);
            }
        }

        [Secure("EditCustomerUsers"), HttpPost, Route("User/UnlockAccount/{userId}", Name = "UnlockAccount")]
        public JsonResult ResetPassword(string userId)
        {
            using (var userService = new AdminUserService())
            {
                userService.UnlockAccount(userId);
                return Json(new { accountUnlocked = true });
            }
        }

        
    }
}