﻿using AbsenceSoft.Administration.Models.Communication;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Web.Attributes;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Administration.Controllers
{
    [Secure]
    public class TemplateController : BaseAdministrationController
    {
        // GET: Communication
        [Secure("EditCommunicationTemplate"), HttpGet, Route("Templates", Name = "Templates")]
        public ActionResult ViewTemplates()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult CommunicationTemplates()
        {
            using (var commsService = new AdminCommunicationService())
            {
                IEnumerable<CommunicationTemplateViewModel> communications = commsService.GetDefaultCommunicationTemplates().Select(c => new CommunicationTemplateViewModel(c));
                return PartialView("_CommunicationTemplates", communications);
            }
        }

        [ChildActionOnly]
        public ActionResult PaperworkTemplates()
        {
            using (var commsService = new AdminCommunicationService())
            {
                IEnumerable<PaperworkTemplateViewModel> paperwork = commsService.GetDefaultPaperworkTemplates().Select(c => new PaperworkTemplateViewModel(c));
                return PartialView("_PaperworkTemplates", paperwork);
            }
        }

        [Secure("EditCommunicationTemplate"), HttpGet, Route("Templates/EditCommunication/{id?}", Name = "EditCommunicationTemplate")]
        public ActionResult EditCommunicationTemplate(string id)
        {
            using (var commsService = new AdminCommunicationService())
            {
                CommunicationTemplateViewModel template = new CommunicationTemplateViewModel(commsService.GetCoreTemplate(id));
                return View(template);
            }
        }

        [Secure("EditCommunicationTemplate"), HttpPost, Route("Templates/SaveCommunication", Name = "SaveCommunicationTemplate")]
        public ActionResult SaveCommunicationTemplate(CommunicationTemplateViewModel communicationTemplate, HttpPostedFileBase document)
        {
            if (!ModelState.IsValid)
                return View("EditCommunicationTemplate", communicationTemplate);

            using (var commsService = new AdminCommunicationService())
            {
                if (document != null && document.ContentLength > 0)
                {
                    Document doc = commsService.UploadCoreDocument(document);
                    communicationTemplate.DocumentId = doc.Id;
                    communicationTemplate.DocumentName = doc.FileName;
                }

                Template toSave = communicationTemplate.ApplyToDataModel(commsService.GetCoreTemplate(communicationTemplate.Id));
                toSave = commsService.SaveCoreTemplate(toSave);
                return RedirectToRoute("Templates");
            }
        }

        [Secure("DeleteCommunicationTemplate"), HttpDelete, Route("Templates/DeleteCommunication/{id}", Name ="DeleteCommunicationTemplate")]
        public ActionResult DeleteCommunicationTemplate(string id)
        {
            using (var commsService = new AdminCommunicationService())
            {
                commsService.DeleteCoreTemplate(id);
                return RedirectViaJavaScript(Url.RouteUrl("Templates"));
            }
        }

        [Secure("EditCommunicationTemplate"), HttpGet, Route("Templates/EditPaperwork/{id?}", Name = "EditPaperworkTemplate")]
        public ActionResult EditPaperworkTemplate(string id)
        {
            using (var commsService = new AdminCommunicationService())
            {
                PaperworkTemplateViewModel paperwork = new PaperworkTemplateViewModel(commsService.GetCorePaperwork(id));
                return View(paperwork);
            }
        }

        [Secure("EditCommunicationTemplate"), HttpPost, Route("Templates/SavePaperwork", Name = "SavePaperworkTemplate")]
        public ActionResult SavePaperworkTemplate(PaperworkTemplateViewModel paperworkTemplate, HttpPostedFileBase document)
        {
            if (!ModelState.IsValid)
                return View("EditPaperworkTemplate", paperworkTemplate);

            using (var commsService = new AdminCommunicationService())
            {
                if (document != null && document.ContentLength > 0)
                {
                    Document doc = commsService.UploadCoreDocument(document);
                    paperworkTemplate.DocumentId = doc.Id;
                    paperworkTemplate.DocumentName = doc.FileName;
                }

                Paperwork toSave = paperworkTemplate.ApplyToDataModel(commsService.GetCorePaperwork(paperworkTemplate.Id));
                toSave = commsService.SaveCorePaperwork(toSave);
                return RedirectToRoute("Templates");
            }
        }

        [Secure("DeleteCommunicationTemplate"), HttpDelete, Route("Templates/DeletePaperwork/{id}", Name = "DeletePaperworkTemplate")]
        public ActionResult DeletePaperworkTemplate(string id)
        {
            using (var commsService = new AdminCommunicationService())
            {
                commsService.DeleteCorePaperwork(id);
                return RedirectViaJavaScript(Url.RouteUrl("Templates"));
            }
        }
    }
}