﻿using AbsenceSoft.Administration.Models;
using AbsenceSoft.Administration.Models.Settings;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Web.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Administration.Controllers
{
    public class SettingsController : BaseAdministrationController
    {
        [Secure, Route("Settings", Name = "Settings")]
        public ActionResult Index()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult Users()
        {
            using (var userService = new AdminUserService())
            {
                var users = userService.GetAdminUsers().Select(u => new UserViewModel(u));
                return PartialView(users);
            }

        }

        [Secure("EditUser"), HttpGet, Route("Settings/EditAdminUser/{userId?}", Name = "EditAdminUser")]
        public ActionResult EditAdminUser(string userId)
        {
            using (var userService = new AdminUserService())
            {
                UserViewModel user = new UserViewModel(userService.GetUserById(userId));
                return View(user);
            }
        }

        [Secure("EditUser"), HttpPost, Route("Settings/SaveAdminUser", Name = "SaveAdminUser")]
        public ActionResult SaveAdminUser(UserViewModel user)
        {
            if (!ModelState.IsValid)
                return PartialView("EditAdminUser", user);

            using (var userService = new AdminUserService())
            {
                User adminUser = userService.GetUserById(user.Id);
                adminUser = user.ApplyToDataModel(adminUser);
                userService.SaveAdminUser(adminUser);
                return RedirectViaJavaScript(Url.RouteUrl("Settings"));
            }
        }

        [ChildActionOnly]
        public ActionResult Roles()
        {
            using (var userService = new AdminUserService())
            {
                var roles = userService.GetAdminRoles(false).Select(r => new RoleViewModel(r));
                return PartialView(roles);
            }

        }

        [Secure("EditRoleDefinition"), Route("Settings/EditAdminRole/{id?}", Name = "EditAdminRole")]
        public ActionResult EditAdminRole(string id)
        {
            using (var userService = new AdminUserService())
            {
                RoleViewModel role = new RoleViewModel(userService.GetAdminRole(id));
                return View(role);
            }
        }

        [Secure("EditRoleDefinition"), Route("Settings/SaveAdminRole", Name = "SaveAdminRole")]
        public ActionResult SaveAdminRole(RoleViewModel role)
        {
            if (!ModelState.IsValid)
                return PartialView("EditAdminRole", role);

            using (var userService = new AdminUserService())
            {
                Role adminRole = userService.GetAdminRole(role.Id);
                adminRole = role.ApplyToDataModel(adminRole);
                userService.SaveAdminRole(adminRole);
                return RedirectViaJavaScript(Url.RouteUrl("Settings"));
            }
        }

        [ChildActionOnly, Route("Settings/Permissions", Name = "PermissionsEditor")]
        public ActionResult PermissionsEditor(string roleId)
        {
            using (var administrationService = new AdministrationService())
            using (var adminUserService = new AdminUserService())
            {
                Role theRole = adminUserService.GetAdminRole(roleId);
                var allPermissions = administrationService.GetPermissionsForRoleType(RoleType.Administration);
                List<PermissionCategoryViewModel> categories = allPermissions.GroupBy(p => p.Category)
                    .Select(g => new PermissionCategoryViewModel()
                    {
                        Name = g.Key,
                        Permissions = g.Select(p => new PermissionViewModel()
                        {
                            Name = p.Name,
                            Id = p.Id,
                            Checked = theRole != null && theRole.Permissions.Contains(p.Id)
                        }).ToList()
                    }).ToList();

                return PartialView(categories);
            }
        }

        
    }
}