﻿using System.Web;
using System.Web.Optimization;

namespace AbsenceSoft.Administration
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-ui-{version}.js",
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/jquery.unobtrusive-ajax.js*",
                        "~/Scripts/jQuery.FileUpload/jquery.fileupload.js",
                        "~/Scripts/jQuery.FileUpload/jquery.iframe-transport.js",
                        "~/Scripts/select2.js*")
                        .IncludeDirectory("~/Scripts/noty", "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/site")
                .Include("~/Scripts/app.js")
                .IncludeDirectory("~/Scripts/Customer", "*.js", false)
                .IncludeDirectory("~/Scripts/Employer", "*.js", false)
                .IncludeDirectory("~/Scripts/Template", "*.js", false)
                .IncludeDirectory("~/Scripts/Settings", "*.js", false)
                .IncludeDirectory("~/Scripts/Home", "*.js", false)
                .IncludeDirectory("~/Scripts/User", "*.js", false)
                .IncludeDirectory("~/Scripts/Shared", "*.js", false)
                .IncludeDirectory("~/Scripts/Reports", "*.js", false));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/bootstrap-datepicker.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/sitecss").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/css/select2.css",
                      "~/Content/site.css"));
        }
    }
}
