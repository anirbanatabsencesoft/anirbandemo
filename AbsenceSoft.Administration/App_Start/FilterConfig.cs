﻿using AbsenceSoft.Web.Filters;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Administration
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new RenewSessionGlobalFilter( AT.Entities.Authentication.ApplicationType.Administration));
        }
    }
}
