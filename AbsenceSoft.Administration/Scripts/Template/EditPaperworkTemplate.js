﻿(function (AbsenceSoft, $) {
    AbsenceSoft.EditCommunicationTemplate = AbsenceSoft.EditCommunicationTemplate || {};
    var removeFileUpload = function () {
        $('#uploadedTemplate').addClass('hide');
        $('#currentPaperworkTemplate').removeClass('hide');
        $('#templateName').html('');
        $('#paperworkTemplateUploadFile').val('');
    }

    var fileUploaded = function (e, data) {
        if (data.files.length != 1)
            return;

        var uploadedFile = data.files[0];
        $('#uploadedTemplate').removeClass('hide');
        $('#currentCommunicationTemplate').addClass('hide');
        $('#templateName').html(uploadedFile.name);
    }

    var createHiddenInput = function (name, val) {
        return $('<input>')
            .attr('type', 'hidden')
            .attr('name', name)
            .val(val);
    }

    var appendRuleGroupData = function (i, elem) {
        var group = $(elem);
        var name = createHiddenInput('Criteria[' + i + '].Name', group.find('.criteria-name').val());
        var successType = createHiddenInput('Criteria[' + i + '].SuccessType', group.find('.criteria-success-type').val());
        group.find('.criteria-rule').each(appendRuleData(i));
        $('#editPaperworkTemplate').append(name).append(successType);
    }

    var appendRuleData = function (ruleGroupIndex) {
        return function (i, elem) {
            var rule = $(elem);
            var type = createHiddenInput('Criteria[' + ruleGroupIndex + '].Group[' + i + '].CriteriaType', rule.find('.rule-type').val());
            var criteria = createHiddenInput('Criteria[' + ruleGroupIndex + '].Group[' + i + '].Criteria', rule.find('.rule').val());
            $('#editPaperworkTemplate').append(type).append(criteria);
        }
    }

    var appendCriteriaData = function () {
        var isValid = criteriaValid();
        if (isValid) {
            $('#editPaperworkTemplate .criteria-group').each(appendRuleGroupData);
            $('#invalidRules').addClass('hide');
        } else {
            $('#invalidRules').removeClass('hide');
        }

        return isValid;
    }

    var criteriaValid = function () {
        var valid = true;
        $('#editPaperworkTemplate .criteria-group :input').each(function (i, elem) {
            var theElement = $(elem);
            if (!theElement.is(':button') && !$(elem).val()) {
                valid = false;
                return false;
            }
        });

        return valid;
    }

    var addNewGroup = function () {
        var newGroup = $('#groupTemplate').clone();
        newGroup.removeClass('hide');
        newGroup.find('.delete-criteria').click(removeGroup);
        newGroup.find('.new-rule').click(addNewRule);
        $('#paperworkCriteria').append(newGroup);
        return false;
    }

    var removeGroup = function () {
        $(this).closest('.criteria-group').remove();
        return false;
    }

    var addNewRule = function () {
        var newRule = $('#ruleTemplate').clone();
        newRule.removeClass('hide');
        newRule.find('.delete-rule').click(removeRule);
        $(this).closest('.criteria-group').find('.criteria-rules').append(newRule);
        return false;
    }

    var removeRule = function () {
        $(this).closest('.criteria-rule').remove();
        return false;
    }

    $(document).ready(function () {
        $('#paperworkTemplateUploadFile').fileupload({
            autoUpload: false,
            singleFileUploads: true,
            replaceFileInput: false
        }).on('fileuploadadd', fileUploaded);

        $('#cancelPaperworkTemplateUpload').click(removeFileUpload);
        $('#editPaperworkTemplate').submit(appendCriteriaData);
        $('#newCriteria', '#editPaperworkTemplate').click(addNewGroup);
        $('.delete-criteria', '#editPaperworkTemplate').click(removeGroup);
        $('.delete-rule', '#editPaperworkTemplate').click(removeRule);
        $('.new-rule', '#editPaperworkTemplate').click(addNewRule);
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);