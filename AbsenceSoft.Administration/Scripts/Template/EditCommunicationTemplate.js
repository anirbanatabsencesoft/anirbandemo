﻿(function (AbsenceSoft, $) {
    AbsenceSoft.EditCommunicationTemplate = AbsenceSoft.EditCommunicationTemplate || {};
    var removeFileUpload = function () {
        $('#uploadedTemplate').addClass('hide');
        $('#currentCommunicationTemplate').removeClass('hide');
        $('#templateName').html('');
        $('#communicationTemplateUploadFile').val('');
    }

    var fileUploaded = function (e, data) {
        if (data.files.length != 1)
            return;

        var uploadedFile = data.files[0];
        $('#uploadedTemplate').removeClass('hide');
        $('#currentCommunicationTemplate').addClass('hide');
        $('#templateName').html(uploadedFile.name);
    }

    $(document).ready(function () {
        $('#selectedPaperwork').select2();
        $('#communicationTemplateUploadFile').fileupload({
            autoUpload: false,
            singleFileUploads: true,
            replaceFileInput: false
        }).on('fileuploadadd', fileUploaded);

        $('#cancelCommunicationTemplateUpload').click(removeFileUpload);
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);