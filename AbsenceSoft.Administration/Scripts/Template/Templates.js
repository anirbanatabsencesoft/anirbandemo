﻿(function (AbsenceSoft, $) {
    AbsenceSoft.Templates = AbsenceSoft.Templates || {};
    $(document).ready(function () {
        $('#searchCommunicationTemplates').inPlaceFilter({fieldSelector: '.communication-template'});
        $('#searchPaperworkTemplates').inPlaceFilter({ fieldSelector: '.paperwork-template' });
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);