﻿(function (AbsenceSoft, $) {
    AbsenceSoft.EditEmployer = AbsenceSoft.EditEmployer || {};

    var clearEmployer = function () {
        $.ajax({
            url: '/Employer/ClearEmployerData/' + $('#employerId').val(),
            data: {
                removeConfig:  $('#removeConfig').is(':checked')
            },
            method: 'POST',
            success: function () {
                noty({ type: 'success', text: 'Employer Data Cleared.  Reloading page in 10 seconds.', timeout: 10000 });
                setTimeout(function () {
                    window.location.reload();
                }, 10000);
            }
        })
    };

    var checkClearEmployer = function () {
        if ($('#confirmEmployerClear').val() == 'confirm') {
            clearEmployer();
        }
    };

    var clearConfirm = function () {
        $('#confirmCustomerDelete').val('');
    };

    $(document).ready(function () {
        $('#clearConfirmed').click(checkClearEmployer);
        $('#clearConfirmation').on('hide.bs.modal', clearConfirm);
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);