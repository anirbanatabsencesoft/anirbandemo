﻿(function (AbsenceSoft, $) {
    AbsenceSoft.DeleteConfirmation = AbsenceSoft.DeleteConfirmation || {};
    var triggeringButton = undefined;

    var updateModal = function (event) {
        triggeringButton = $(event.relatedTarget);
        var message = triggeringButton.data('delete-confirmation-message');
        $('#confirmationMessage').html(message);
    };

    var doDelete = function () {
        $.ajax({
            url: triggeringButton.data('delete-confirmation-url'),
            method: 'DELETE'
        })
    };

    var checkDelete = function () {
        if ($('#confirmDelete').val() == 'confirm') {
            doDelete();
        }
    };

    var clearConfirm = function () {
        $('#confirmDelete').val('');
        $('#confirmationMessage').html('');
    };

    $(document).ready(function () {
        $('#deleteConfirmation').on('show.bs.modal', updateModal)
                                .on('hide.bs.modal', clearConfirm);
        $('#deleteConfirmed').click(checkDelete);
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);