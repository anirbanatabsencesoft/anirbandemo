﻿(function (AbsenceSoft, $) {
    var filter = function (searchTerm, fieldSelector) {
        searchTerm = searchTerm.toLowerCase();
        return function () {
            var toBeFiltered = $(this);
            var text = toBeFiltered.text().toLowerCase();
            if (text.indexOf(searchTerm) > -1) {
                toBeFiltered.closest(fieldSelector).removeClass('hide');
            } else {
                toBeFiltered.closest(fieldSelector).addClass('hide');
            }
        };
    };

    var checkFilter = function (options) {
        return function () {
            var searchTerm = $(this).val().trim();
            if (!searchTerm) {
                $(options.fieldSelector).removeClass('hide');
            } else {
                $(options.fieldSelector).each(filter(searchTerm, options.fieldSelector));
            }
        }

    };

    var bindFilter = function (options, context) {
        return context.keyup(checkFilter(options));
    }

    $.fn.inPlaceFilter = function (options) {
        var settings = $.extend({
            fieldSelector:  ''
        }, options);
        bindFilter(settings, this);
        return this;
    }

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);
