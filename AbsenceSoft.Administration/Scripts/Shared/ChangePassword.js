﻿(function (AbsenceSoft, $) {
    AbsenceSoft.ChangePassword = AbsenceSoft.ChangePassword || {};
    var changePassword = function () {
        var changePasswordInfo = {
            Password: $('#changePasswordPassword').val(),
            ConfirmPassword: $('#changePasswordConfirm').val()
        };

        if (changePasswordInfo.Password !== changePasswordInfo.ConfirmPassword) {
            $('#changePasswordErrors').html('Password and Confirm Password must match.');
        } else if($('#changePasswordForm').valid()) {
            $.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(changePasswordInfo),
                url: '/Home/ChangePassword',
                success: checkChangePasswordSuccess
            })
        }

        return false;
    }

    var checkChangePasswordSuccess = function (data, jqXHR, success) {
        if (data.success) {
            noty({ type: 'success', text: 'Your password has been successfully changed.' });
            $('#changePasswordModal').modal('hide');
            $('#changePasswordPassword').val('');
            $('#changePasswordConfirm').val('');
            $('#changePasswordErrors').html('');
        } else {
            var text = 'An error was encountered while attempting to change your password. ';
            for (var i = 0; i < data.errors.length; i++) {
                text += data.errors[i] + ' ';
            }
            $('#changePasswordErrors').html(text);
        }
    }

    $(document).ready(function () {
        $('#changePasswordForm').submit(changePassword);
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);