﻿(function (AbsenceSoft, $) {
    AbsenceSoft.List = AbsenceSoft.List || {};

    var initList = function () {
        var listContainer = $(this);
        var url = listContainer.data('url');
        if (!url)
            return;

        listContainer.on('keyup', updateFilter);
        listContainer.find('.list-sort').click(sortList);
        getListResults(listContainer, true);
    }

    var updateFilter = function () {
        var listContainer = $(this).closest('.list-container');
        getListResults(listContainer, true);
    }

    var getListResults = function (listContainer, reset) {
        $.ajax({
            url: listContainer.data('url'),
            method: 'POST',
            data: getListCriteria(listContainer),
            success: populateList(listContainer, reset)
        });
    }

    var getListCriteria = function (list) {
        var criteria = {};
        list.find('.list-criteria').each(function (i, specificCriteria) {
            var theCriteria = $(specificCriteria);
            var criteriaName = theCriteria.attr('name');
            var criteriaValue = theCriteria.val();
            if (criteriaName && criteriaValue)
                criteria[criteriaName] = criteriaValue;
        });
        var sortField = list.find('.list-sort .fa').not('.hide');
        if (sortField.hasClass('fa-chevron-up')) {
            criteria.Direction = 1;
        } else {
            criteria.Direction = -1;
        }
        criteria.SortBy = sortField.siblings('.sort-name').val();
        return criteria;
    }

    var populateList = function (list, reset) {
        return function (data, jqXHR, success) {
            if (reset) {
                list.find('.results-container').html(data);
            } else {
                list.find('.results-container').append(data);
            }
        }
    }

    var sortList = function () {
        var sortableHeader = $(this);
        var listContainer = sortableHeader.closest('.list-container');
        var sortChevrons = sortableHeader.find('.fa');
        if (sortChevrons.not('.hide').length > 0) {
            // sort field is showing a chevron
            // - hide the existing chevron and show the other one
            sortChevrons.toggleClass('hide');
        } else {
            // sort field is not showing any chevrons
            // - hide all existing chevrons, show chevron up in clicked list sort
            listContainer.find('.fa-chevron-down, .fa-chevron-up').addClass('hide');
            sortChevrons.filter('.fa-chevron-up').removeClass('hide');
        }
        getListResults(listContainer, true);
    }

    var listStart = function () {
        $('.list-container').each(initList);
    }

    $(document).ready(function () {
        listStart();
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);