﻿(function (AbsenceSoft, $) {
    AbsenceSoft.Features = AbsenceSoft.Features || {};
    var featuresSaved = function () {
        noty({ type: 'success', text: 'Features Saved' });
    }

    AbsenceSoft.Features.Saved = featuresSaved;
    $(document).ready(function () {
        $('#searchFeatures').inPlaceFilter({ fieldSelector: '.feature' });
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);