﻿(function (AbsenceSoft, $) {
    $.validator.setDefaults({
        ignore: ''
    });

    $(document).ajaxError(function (e, jqXHR, ajaxSettings, thrownError) {
        if (thrownError !== 'abort')
            noty({ type: 'error', text: thrownError || 'Unknown Error', timeout: 10000 });
    });

    $(document).ajaxSuccess(function (event, request, settings, data) {
        if (settings.dataType == "json" && data.Redirect && data.RedirectUrl) {
            window.location.href = data.RedirectUrl;
        }
    });

    $(document).ready(function () {
        $('.input-daterange').datepicker({
            orientation: 'top auto',
            autoclose: false
        });

        $('.datepicker').datepicker({
            orientation: 'top auto',
            autoclose: true
        });
    });

    $.noty.defaults.layout = 'topRight';
    $.noty.defaults.timeout = 5 * 1000; // Show notifications for 5 seconds

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);
