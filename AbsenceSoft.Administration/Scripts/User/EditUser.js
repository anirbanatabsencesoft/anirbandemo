﻿(function (AbsenceSoft, $) {
    AbsenceSoft.EditUser = AbsenceSoft.EditUser || {};
    var alertSuccess = function () {
        noty({ type: 'success', text: 'User Account Unlocked.  Page will refresh in three seconds' });
        setTimeout(function () {
            window.location.reload();
        }, 3000)
    }


    var unlockAccount = function () {
        var url = '/User/UnlockAccount/' + $('#userId').val();
        $.ajax({
            type: 'POST',
            url: url,
            success: alertSuccess
        })
    }

    $(document).ready(function () {
        $('#unlockAccount').click(unlockAccount);
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);