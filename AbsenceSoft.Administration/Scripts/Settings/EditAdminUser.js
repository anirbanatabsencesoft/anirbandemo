﻿(function (AbsenceSoft, $) {
    AbsenceSoft.EditAdminUser = AbsenceSoft.EditAdminUser || {};
    AbsenceSoft.EditAdminUser.Init = function () {
        $('#selectedRoles').select2();
    };
    $(document).ready(AbsenceSoft.EditAdminUser.Init);

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);