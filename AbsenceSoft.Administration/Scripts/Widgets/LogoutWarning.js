﻿//Validate the token with forms timeout
(function (AbsenceSoft, $) {
    var checkAuthentication = function () {
        $.ajax({
            url: '/Home/GetUserIsAuthenticatedOrNot',
            contentType: 'application/json; charset=utf-8',
            dataType: "text json",
            cache: false,
            success: checkAuthenticationResponse,
            error: redirectToLoginPage
        })
    }

    var originalExpirationTime = "";

    /// redirects to login page
    redirectToLoginPage = function (data) {
        originalExpirationTime = "";

        // Destroy onbeforeunload (IE Compliant way)
        window.onbeforeunload = undefined;

        // Redirect
        window.location.href = "/Login?ReturnUrl=''";
    }

    /// renews the session
    /// if the call fails, redirect to login page, otherwise we hide the modal
    var renewSession = function () {
        $.ajax({
            url: '/Administration/RenewSession',
            contentType: 'application/json; charset=utf-8',
            type: "POST",
            dataType: "text",
            cache: false,
            error: redirectToLoginPage,
            success: function () {
                originalExpirationTime = "";
                $('#formsAuthTimeout').modal('hide');
            }
        });

        return false;
    }


    /// if authenticated and will expire
    ///     pop modal, show when session will expire
    /// if not authenticated
    ///     redirect to login page and show the logout warning
    checkAuthenticationResponse = function (data) {
        if (originalExpirationTime === "") {
            originalExpirationTime = new Date(data.ExpirationTime);
        }
        if (data.IsUserAuthenticated && data.WillExpire) {
            if (data.WillExpireSameDayOrWithinNextMeridiem) {
                $('.session-timeout').html(originalExpirationTime.toLocaleTimeString());
            } else {
                $('.session-timeout').html(originalExpirationTime.toLocaleString());
            }
            $('#formsAuthTimeout').modal('show');
        } else if (!data.IsUserAuthenticated) {
            redirectToLoginPage();
        }
    }

    /// if they click continue session
    /// renew the session
    $('.continue-session').click(renewSession);
    $(document).ready(function () {
        setInterval(checkAuthentication, 60000);
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);