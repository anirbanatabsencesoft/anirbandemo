﻿(function (AbsenceSoft, $) {
    AbsenceSoft.CustomerBillingDetails = AbsenceSoft.CustomerBillingDetails || {};

    function downloadCSV(csv, filename) {
        var csvFile = new Blob([csv], {
            type: "text/csv"
        });
        var downloadLink = document.createElement("a");

        downloadLink.download = filename;
        downloadLink.href = window.URL.createObjectURL(csvFile);
        $(downloadLink).addClass("none");

        document.body.appendChild(downloadLink);
        downloadLink.click();
    };

    var appendColumn = function (row) {
        return function () {
            var $this = $(this);
            row.push($this.text());
        }
    };

    var appendRow = function (csv) {
        return function () {
            var $this = $(this);
            var row = [];
            var cols = $this.find("td, th");
            cols.each(appendColumn(row));
            csv.push(row.join(","));
        };
    };

    var buildCsv = function () {
        var filename = 'CustomerBillingReport.csv';
        var csv = [];
        var rows = $("table tr");
        rows.each(appendRow(csv));
        downloadCSV(csv.join("\n"), filename);
    };

    var getBillingReport = function () {
        var today = new Date();
        var currentDate = '';
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        currentDate = yyyy + '-' + mm + '-' + dd;;

        $("#billrpt_no_data").hide();
        $("#btnDownloadBillingReport").hide();
        $("#billrpt_loading_msg").show();
        $("#divBillingReport").empty();

        var startDate = '';
        var endDate = '';
        startDate = $.trim($("#txtRptStartDate").val());
        endDate = $.trim($("#txtRptEndDate").val());
        var validData = true;

        if (startDate == '' || endDate == '') {
            validData = false;
            noty({ type: 'error', text: 'Please enter both the dates.' });
        }
        else {
            if (Date.parse(startDate) > Date.parse(endDate)) {
                validData = false;
                noty({ type: 'error', text: 'Start date should be less than or equal to end date.' });
            }

            if (Date.parse(startDate) > Date.parse(currentDate) || Date.parse(endDate) > Date.parse(currentDate)) {
                validData = false;
                noty({ type: 'error', text: 'Dates cannot be a future date.' });
            }
        }
        
        if (validData) {
            $.ajax({
                url: '/report/customerbillingdetails',
                data: {
                    startDate: startDate,
                    endDate: endDate
                },
                success: function (data) {
                    $("#divBillingReport").empty();
                    $('#divBillingReport').append(data);
                    $("#billrpt_loading_msg").hide();
                    $("#btnDownloadBillingReport").show();
                },
                type: "GET",
            });
        }
    };

    var init = function () {
        $('#btnDownloadBillingReport').click(buildCsv);

        $("#btnRunBillingReport").click(getBillingReport);

        $("#billrpt_loading_msg").hide();
        $("#btnDownloadBillingReport").hide();
    }

    $(document).ready(init);

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);