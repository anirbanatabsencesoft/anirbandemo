﻿(function (AbsenceSoft, $) {
    AbsenceSoft.CustomerDetails = AbsenceSoft.CustomerDetails || {};

    function downloadCSV(csv, filename) {
        var csvFile = new Blob([csv], {
            type: "text/csv"
        });
        var downloadLink = document.createElement("a");

        downloadLink.download = filename;
        downloadLink.href = window.URL.createObjectURL(csvFile);
        downloadLink.style.display = "none";

        document.body.appendChild(downloadLink);
        downloadLink.click();
    };

    var appendColumn = function (row) {
        return function () {
            var $this = $(this);
            row.push($this.text());
        }
    };

    var appendRow = function (csv) {
        return function () {
            var $this = $(this);
            var row = [];
            var cols = $this.find("td, th");
            cols.each(appendColumn(row));
            csv.push(row.join(","));
        };
    };

    var buildCsv = function () {
        var filename = 'CustomerReport.csv';
        var csv = [];
        var rows = $("table tr");
        rows.each(appendRow(csv));
        downloadCSV(csv.join("\n"), filename);
    };

    var init = function() {
        $('#btnReport').click(buildCsv);
    }

    $(document).ready(init);

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);