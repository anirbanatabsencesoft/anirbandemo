﻿(function (AbsenceSoft, $) {
    AbsenceSoft.Index = AbsenceSoft.Index || {};
    var timer;
    var clearTimer = function () {
        if(timer)
        {
            clearTimeout(timer);
        }
    }

    var populateResults = function (data, jqXHR) {
        $('#customerSearchResults').html(data);
    }

    var clearResults = function () {
        $('#customerSearchResults').html('');
    }

    var issueSearch = function () {
        clearTimer();
        var searchTerm = $('#customerSearch').val().trim();
        if (!searchTerm) {
            clearResults();
        } else {
            var url = '/' + $('.search-type.active').text().trim() + '/Search';

            $.ajax({
                method: 'GET',
                url: url,
                data: {
                    searchTerm: searchTerm
                },
                success: populateResults
            })
        }
    };

    var queueSearch = function (e) {
        clearTimer();
        if (e.keyCode !== 13) {
            timer = setTimeout(issueSearch, 200);
        } else {
            issueSearch();
        }
    };

    var updateSearch = function () {
        clearResults();
        // hack to let bootstrap update the toggle before we update the other labels
        setTimeout(function () {
            var label = $('.search-type.active').text().trim();
            $('#customerSearch').val('').attr('placeholder', 'Search ' + label + 's...');
            $('.search-label').text(label);
        }, 10);
    }

    $(document).ready(function () {
        $('#customerSearch').keyup(queueSearch);
        $('.search-type').click(updateSearch);
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);