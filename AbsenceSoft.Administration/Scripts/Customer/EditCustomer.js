﻿(function (AbsenceSoft, $) {
    AbsenceSoft.EditCustomer = AbsenceSoft.EditCustomer || {};

    var deleteCustomer = function () {
        $.ajax({
            url: '/Customer/Delete/' + $('#customerId').val(),
            method: 'DELETE',
            success: function () {
                window.location.href = '/';
            }
        })
    };

    var checkDeleteCustomer = function () {
        if ($('#confirmCustomerDelete').val() == 'confirm') {
            deleteCustomer();
        }
    };

    var clearConfirm = function () {
        $('#confirmCustomerDelete').val('');
    };

    var detailsSaved = function () {
        noty({ type: 'success', text: 'Customer Details Saved' });
    }

    AbsenceSoft.EditCustomer.DetailsSaved = detailsSaved;

    $(document).ready(function () {
        $('#deleteConfirmed').click(checkDeleteCustomer);
        $('#deleteConfirmation').on('hide.bs.modal', clearConfirm);
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);