﻿using AbsenceSoft.Logic.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace AbsenceSoft.Administration.Helpers
{
    public static class HtmlHelpers
    {
        public static MvcHtmlString PaperworkListBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            return htmlHelper.PaperworkListBoxFor(expression, null);
        }

        public static MvcHtmlString PaperworkListBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes)
        {
            using (var commsService = new AdminCommunicationService())
            {
                var paperwork = commsService.GetDefaultPaperworkTemplates();
                IEnumerable<SelectListItem> selectList = paperwork.Select(p => new SelectListItem()
                {
                    Text = p.Name,
                    Value = p.Code
                });

                return htmlHelper.ListBoxFor(expression, selectList, htmlAttributes);
            }
        }

        public static MvcHtmlString RoleDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            return htmlHelper.RoleDropDownListFor(expression, null);
        }

        public static MvcHtmlString RoleDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes)
        {
            using (var adminUserService = new AdminUserService())
            {
                var roles = adminUserService.GetAdminRoles();
                IEnumerable<SelectListItem> selectList = roles.Select(p => new SelectListItem()
                {
                    Text = p.Name,
                    Value = p.Id
                });

                return htmlHelper.ListBoxFor(expression, selectList, htmlAttributes);
            }
        }
    }
}