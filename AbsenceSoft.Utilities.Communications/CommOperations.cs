﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using AT.Data.Authentication.Provider;
using AT.Logic.Authentication;
using CLAP;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Utilities.Communications
{
    public class CommOperations
    {
        protected CommOperations()
        {

        }
        [Verb(Description = "Re-send a single communication.  NOTE: If User.Current resolves to null (outside of httpcontext), the communication will be resent in the context of the user who last modified the communication.")]
        public static void Resend([Required]string customerId, [Required]string employerId, [Required]string commId)
        {
            if (string.IsNullOrWhiteSpace(commId)) throw new ArgumentNullException("commId");
            if (string.IsNullOrWhiteSpace(employerId)) throw new ArgumentNullException("employerId");
            if (string.IsNullOrWhiteSpace(customerId)) throw new ArgumentNullException("customerId");

            var comm = Communication.AsQueryable()
                .Where(c =>
                    c.Id == commId
                    && c.EmployerId == employerId
                    && c.CustomerId == customerId
                )
                .SingleOrDefault();
            if (comm == null)
            {
                throw new AbsenceSoftException("Communication not found. Verify inputs and try again.");
            }

            User user = User.GetById(comm.CreatedById) ?? User.AsQueryable().FirstOrDefault(c => c.Email == comm.CreatedByEmail) ?? User.GetById(comm.ModifiedById);
            if (user != null)
            {
                HttpContext context = new HttpContext(new HttpRequest(null, "http://tempuri.org", null), new HttpResponse(null));
                HttpContext.Current = context;
                ApplicationUserManager userManager = new ApplicationUserManager(UserStoreProviderManager.Default.Repository);
                var ticket = userManager.GetSystemTicketAsync(user.Email, AT.Entities.Authentication.ApplicationType.Portal).GetAwaiter().GetResult();
                if (ticket != null)
                {
                    context.Request.Browser = new HttpBrowserCapabilities() { Capabilities = new Dictionary<string, string> { { "supportsEmptyStringInCookieValue", "false" }, { "cookies", "false" } } };
                    context.Items[User.CURRENT_USER_CONTEXT_KEY] = user;
                    userManager.SetClaimsPrincipal(ticket, new HttpContextWrapper(context)).GetAwaiter().GetResult();

                    try
                    {
                        comm = new CommunicationService() { SuppressWorkflow = true }.Using(c => c.ResendCommunication(comm));
                    }
                    catch (Exception ex)
                    {
                        Console.Error.Write($"Communication '{commId} - {comm.Subject}' Failed with Error");
                        Console.Error.WriteLine(ex);
                    }
                    Trace.TraceInformation(
                        "Communication re-sent successfully: CommunicationId={0};Subject={1};Name={2};Sender={3}",
                        commId,
                        comm.Subject,
                        comm.Name,
                        comm.CreatedByName
                    );
                }

                else
                {
                    throw new AbsenceSoftException($"Unable to generate the system ticket for the user :{user.Email} ");
                }
            }
        }
    }
}
