﻿using Aspose.Words;
using CLAP;
using System.Diagnostics;

namespace AbsenceSoft.Utilities.Communications
{
    class Program
    {
        static void Main(string[] args)
        {
            Common.Serializers.DateTimeSerializer.Register();
            log4net.Config.XmlConfigurator.Configure();
            new License().SetLicense("Aspose.Words.lic");
            Trace.Listeners.Add(new ConsoleTraceListener());
            Parser.RunConsole<CommOperations>(args);
        }
    }

}
