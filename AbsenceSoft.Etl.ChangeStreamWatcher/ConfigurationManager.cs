﻿using System;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace AbsenceSoft.Etl.ChangeStreamWatcher
{
    /// <summary>
    /// Class to handle all configuration things
    /// </summary>
    public static class ConfigurationManager
    {
        /// <summary>
        /// Define the environment for the configuration manager
        /// </summary>
        public static Environment ConfigEnvironment { get; set; }
        
        /// <summary>
        /// Returns the configuration directory for the environment
        /// </summary>
        /// <returns></returns>
        private static string GetConfigDirectory()
        {
            var baseDir = Directory.GetCurrentDirectory();
            var env = ConfigEnvironment.ToString();
            var configDir = $"{baseDir}\\config\\{env}";
            if (Directory.Exists(configDir))
            {
                return configDir;
            }
            else
            {
                return $"{baseDir}\\config\\Development";
            }
        }

        /// <summary>
        /// Returns the Appsettings
        /// </summary>
        public static IConfiguration AppSettings
        {
            get
            {
                var builder = new ConfigurationBuilder()
                                .SetBasePath(GetConfigDirectory())
                                .AddJsonFile("appSettings.json");
                return builder.Build();
            }
        }

        /// <summary>
        /// Returns the connection strings
        /// </summary>
        public static IConfiguration ConnectionStrings
        {
            get
            {
                var builder = new ConfigurationBuilder()
                                .SetBasePath(GetConfigDirectory())
                                .AddJsonFile("connectionStrings.json");
                return builder.Build();
            }
        }

        /// <summary>
        /// Returns the Email related configurations
        /// </summary>
        public static IConfiguration MailSettings
        {
            get
            {
                var builder = new ConfigurationBuilder()
                                .SetBasePath(GetConfigDirectory())
                                .AddJsonFile("mailSettings.json");
                return builder.Build();
            }
        }


        /// <summary>
        /// The enum to define environment
        /// </summary>
        public enum Environment
        {
            Debug,
            Development,
            Demo,
            Lab,
            Preprod,
            QA,
            Release,
            Stage
        }
    }
}
