﻿// MONGODB COLLECTION CHANGE TRIGGERING SCRIPT

// - This script is designed to touch and change ALL records within the MongoDB collections that are synced to our datawarehouse.
// - By running this script and modifying all records, while ChangeStreamWatcher service is turned on, we can seed the change table for the intial DW load.
// - The isolated operator is IMPORTANT to make sure that while we touch all records in the collection, to populate the MongoDB oplog,
//  that no sites/services/apps are modifying records in-between. This would negative affect the initial ETL/DW load.

//   These collections will be locked when running this script. If running for UAT or Production ensure you know how long it will cause sites to possible be down!!!
//
// So we have a record of when this script was ran, it simply pushes a timestamp into an array as shown in the below example.
//
//       "EtlChangeTriggerScriptRun" : [ "Wed Jun 20 2018 16:42:53 GMT-0600 (MDT)", "Wed Jun 20 2018 16:43:00 GMT-0600 (MDT)", ...etc ]

var collections = [
  "Attachment",
  "Case",
  "CaseNote",
  "Communication",
  "Customer",
  "CustomerServiceOption",
  "Demand",
  "DemandType",
  "Employee",
  "EmployeeContact",
  "EmployeeJob",
  "EmployeeNecessity",
  "EmployeeNote",
  "EmployeeOrganization",
  "EmployeeRestriction",
  "Employer",
  "EmployerContact",
  "EmployerContactType",
  "EmployerServiceOption",
  "Event",
  "Organization",
  "OrganizationAnnualInfo",
  "Role",
  "Team",
  "TeamMember",
  "ToDoItem",
  "User",
  "VariableScheduleTime",
  "WorkflowInstance",
  "DenialReason"
]; var csv_results = []; var run_timestamp = Date(); collections.forEach(function(coll) {

  print("---------------------------------------------------------"); print("Collection: "
  + coll); //output collection name

  coll_count =
  db [ coll ]
  .find().count(); print("Number of Records to Update: "
  + coll_count);

  //sample update result: { "acknowledged" : true, "matchedCount" : 3, "modifiedCount" : 3 }
  results =
  db [ coll ]
  .updateMany( { "$isolated": "true" },
  { "$set": { "EtlChangeTriggerScriptRun": run_timestamp } },
  { upsert: false }
  ); printjson(results);
  csv_results.push( {
    "Collection": coll,
    "RecordCount": coll_count,
    "UpdateAcknowledge": results.acknowledged,
    "UpdateMatchedCount": results.matchedCount,
    "UpdateModifiedCount": results.modifiedCount
  }
  ); print("---------------------------------------------------------");
});

// Once we are done, lets print the results in CSV format
print('Change triggering script executed on ' + run_timestamp); print("Collection, Record Count, Update Acknowledge, Update Matched Count, Update Modified Count"); csv_results.forEach(function(result) {

  print(result.Collection +
  ',' +
  result.RecordCount +
  ',' +
  result.UpdateAcknowledge +
  ',' +
  result.UpdateMatchedCount +
  ',' +
  result.UpdateModifiedCount);

});
