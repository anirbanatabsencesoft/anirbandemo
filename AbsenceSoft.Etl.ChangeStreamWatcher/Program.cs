﻿using Microsoft.Extensions.Configuration;
using System;

namespace AbsenceSoft.Etl.ChangeStreamWatcher
{
    class Program
    {
        /// <summary>
        /// Expect a parameter for environment.
        /// If any parameter isn't available, it will pickup Debug one
        /// Options are:
        /// dev/lab/qa/stage/preprod/demo/release/prod
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            try
            {
                //set the configuration
                ConfigurationManager.ConfigEnvironment = SetEnvironment(args);

                //Watch
                WatchStream();
            }
            catch(Exception ex)
            {
                Console.Beep();
                Console.WriteLine(ex.Message);
                Notify(ex.ToString());
            }
        }

        /// <summary>
        /// Method to watch the stream
        /// </summary>
        static void WatchStream()
        {
            //get collections
            var p = ConfigurationManager.AppSettings.GetSection("watchForCollections").GetChildren();
            System.Threading.Tasks.Parallel.ForEach (p, (t) =>
            {
                var model = new MongoHandler.Model();
                try
                {
                    model.CollectionName = t.GetSection("collection").Value;
                    var children = t.GetSection("childCollections").GetChildren();
                    if(t.GetSection("partiallookup").Value.ToUpper() == "TRUE")
                    {
                        model.partiallkp = true;
                    }
                    else
                    {
                        model.partiallkp = false;
                    }
                    foreach (var child in children)
                    {
                        model.ChildCollections.Add(child.Value);
                    }

                    //process now
                    using (MongoHandler mh = new MongoHandler(model))
                    {
                        mh.DumpMongoBsonObject().GetAwaiter().GetResult();
                    }
                }
                catch (Exception ex)
                {
                    PostgresHandler.Log(model.CollectionName, ex.ToString(), "Error");
                    Notify(ex.ToString());
                    Environment.Exit(9);
                }
            });
        }

        /// <summary>
        /// Notify on error
        /// </summary>
        /// <param name="errMessage"></param>
        private static void Notify(string errMessage = "")
        {
            //send mail
            using (var eh = new EmailHandler())
            {
                eh.SendMail(errMessage);
            }
        }

        /// <summary>
        /// Returns the environment based on the argument
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        static ConfigurationManager.Environment SetEnvironment(string[] args)
        {
            if(args == null || args.Length == 0)
            {
                return ConfigurationManager.Environment.Debug;
            }

            switch (args[0].ToUpper())
            {
                case "DEV":
                case "DEVELOPMENT":
                    return ConfigurationManager.Environment.Development;
                case "LAB":
                    return ConfigurationManager.Environment.Lab;
                case "QA":
                    return ConfigurationManager.Environment.QA;
                case "STAGE":
                case "STAGING":
                    return ConfigurationManager.Environment.Stage;
                case "PREPROD":
                case "PRE-PROD":
                case "PRE_PROD":
                    return ConfigurationManager.Environment.Preprod;
                case "DEMO":
                    return ConfigurationManager.Environment.Demo;
                case "RELEASE":
                case "PROD":
                case "PRODUCTION":
                    return ConfigurationManager.Environment.Release;
                default:
                    return ConfigurationManager.Environment.Debug;
            }
        }
    }
}
