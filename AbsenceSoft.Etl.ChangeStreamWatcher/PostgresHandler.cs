﻿using System;
using Npgsql;
using Insight.Database;
using Insight.Database.Providers.PostgreSQL;
using System.Threading.Tasks;

namespace AbsenceSoft.Etl.ChangeStreamWatcher
{
    /// <summary>
    /// Class to handle postgres inserts
    /// </summary>
    public class PostgresHandler : IDisposable
    {
        /// <summary>
        /// private member to create a transaction scope for moving transaction to different scope
        /// </summary>
        private NpgsqlTransaction _tran = null;

        /// <summary>
        /// private memeber for connection object
        /// </summary>
        private NpgsqlConnection _conn = null;

        /// <summary>
        /// Counter to show how many records inserted
        /// </summary>
        private int _counter = 0;

        /// <summary>
        /// Constructor to create npgsql connection/transaction instance
        /// </summary>
        public PostgresHandler()
        {
            _conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["postgres"]);
            _conn.Open();
            _tran = _conn.BeginTransaction();
        }

        /// <summary>
        /// Get the last change stream id
        /// </summary>
        /// <param name="collectionName"></param>
        /// <returns></returns>
        public static string GetLastChangeStreamId(string collectionName)
        {
            using (var conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["postgres"]))
            {
                string sqlGetResumeToken = "select change_stream_id from mongo_changestream_log where id = (select max(id) from mongo_changestream_log where upper(mongo_collection_name)=@collName)";
                return conn.SingleSql<string>(sqlGetResumeToken, new { collName = collectionName.ToUpper() });
            }
        }

        /// <summary>
        /// Log the output into postgres
        /// </summary>
        /// <param name="collectionName"></param>
        /// <param name="logMessage"></param>
        /// <param name="logType"></param>
        public static void Log(string collectionName, string logMessage, string logType = "Info")
        {
            using (var conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["log"]))
            {
                string sql = "INSERT INTO watch_log(log_type, collection_name, log_message) VALUES(@logType, @collectionName, @logMessage);";
                conn.ExecuteSql(sql, new { logType = logType, collectionName = collectionName, logMessage = logMessage });
            }
        }

        /// <summary>
        /// Insert the mongo object JSON
        /// </summary>
        /// <param name="bsonObj"></param>
        /// <param name="collectionName"></param>
        /// <param name="changeStreamId"></param>
        public async Task Insert(string bsonObj, string collectionName, string changeStreamId, string documentKey = "", int operationType = 0, string operationTypeDescription = "")
        {
            var sql = "INSERT INTO mongo_changestream_log(objbson, mongo_collection_name, change_stream_id, document_key, operation_type, operation_type_description) VALUES (@bson, @coll, @changeStreamId, @documentKey, @operationType, @operationTypeDesc);";
            await _conn.ExecuteAsync(sql, new { bson = bsonObj, coll = collectionName, changeStreamId = changeStreamId, documentKey = documentKey, operationType = operationType, operationTypeDesc = operationTypeDescription }, System.Data.CommandType.Text, transaction: _tran);
            _counter += 1;
        }

        /// <summary>
        /// Commit everything
        /// </summary>
        /// <returns></returns>
        public int Commit()
        {
            _tran.Commit();
            _conn.Close();
            return _counter;
        }

        /// <summary>
        /// Rollback the transaction
        /// </summary>
        /// <returns></returns>
        public int Rollback()
        {
            _tran.Rollback();
            _conn.Close();
            return 0;
        }

        /// <summary>
        /// Dispose the object
        /// </summary>
        public void Dispose()
        {
            try
            {
                _conn.Close();
                _tran = null;
                _conn = null;
            }
            catch { }
        }
    }
}
