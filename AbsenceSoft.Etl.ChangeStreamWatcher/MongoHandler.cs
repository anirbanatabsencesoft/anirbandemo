﻿using System;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Bson.Serialization;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Threading;
using System.Diagnostics;

namespace AbsenceSoft.Etl.ChangeStreamWatcher
{
    /// <summary>
    /// Handles all Mongo Information
    /// </summary>
    public class MongoHandler : IDisposable
    {
        /// <summary>
        /// Private collection model
        /// </summary>
        private Model _model = null;

        /// <summary>
        /// Define the change stream Id
        /// </summary>
        private BsonDocument _lastDocument = null;

        /// <summary>
        /// Compose of all the log message
        /// </summary>
        private StringBuilder _log = new StringBuilder();

        /// <summary>
        /// No of retries for failures
        /// </summary>
        private int _retries = 0;
        /// <summary>
        /// Variable to store connection info of MongoDB
        /// </summary>
        private string _connectioninfo;
        /// <summary>
        /// Variable to store MongoDatabase Name
        /// </summary>
        private string _mongodbname;
        /// <summary>
        /// MongoClient variable
        /// </summary>
        private MongoClient _mclient;
        /// <summary>
        /// MongoDatabase 
        /// </summary>
        private IMongoDatabase _mongodBase ;
        /// <summary>
        /// mongo collection  
        /// </summary>
        private IMongoCollection<BsonDocument> _mongocollection;

      
        /// <summary>
        /// Constructor to inject dependency
        /// </summary>
        public MongoHandler(Model model)
        {
            _model = model;

            if (!string.IsNullOrWhiteSpace(model.CollectionName))
            {
                var changeStreamId = PostgresHandler.GetLastChangeStreamId(model.CollectionName);
                if (!string.IsNullOrWhiteSpace(changeStreamId))
                {
                    try
                    {
                        _lastDocument = BsonSerializer.Deserialize<BsonDocument>(changeStreamId);
                    }
                    catch { }
                }


                _connectioninfo = ConfigurationManager.ConnectionStrings["mongo"];
                _mongodbname = MongoUrl.Create(_connectioninfo).DatabaseName;
                _mclient = new MongoClient(_connectioninfo);
                _mongodBase = _mclient.GetDatabase(_mongodbname);
                _mongocollection = _mongodBase.GetCollection<BsonDocument>(_model.CollectionName);
            }
        }

        /// <summary>
        /// Iterate through the Mongo collection and insert into database
        /// </summary>
        /// <returns></returns>
        public async Task DumpMongoBsonObject()
        {
            if (!string.IsNullOrWhiteSpace(_model.CollectionName))
            {
                //get database
                var connInfo = ConfigurationManager.ConnectionStrings["mongo"];
                var dbName = MongoUrl.Create(connInfo).DatabaseName;
                MongoClient client = new MongoClient(connInfo);
                IMongoDatabase db = client.GetDatabase(dbName);

                //get collection
                IMongoCollection<BsonDocument> collection = db.GetCollection<BsonDocument>(_model.CollectionName);

                //Start log
                _log.AppendLine($"Starting watch on {_model.CollectionName}");

                //set bach size
                int batchSize = 0;
                int.TryParse(ConfigurationManager.AppSettings["batchSize"], out batchSize);

                //iterate
                var options = new ChangeStreamOptions { FullDocument = ChangeStreamFullDocumentOption.UpdateLookup };
                if (_lastDocument != null  && _model.partiallkp == true )
                {
                    options = new ChangeStreamOptions { ResumeAfter = _lastDocument };
                }
                else if (_lastDocument != null && _model.partiallkp == false)
                {
                    options = new ChangeStreamOptions { FullDocument = ChangeStreamFullDocumentOption.UpdateLookup, ResumeAfter = _lastDocument };
                }
                else if(_lastDocument == null && _model.partiallkp == false)
                {
                    options = new ChangeStreamOptions { FullDocument = ChangeStreamFullDocumentOption.UpdateLookup };
                }
                else if (_lastDocument == null && _model.partiallkp == true)
                {
                    options = new ChangeStreamOptions { };
                }
               



                //watch
                CancellationTokenSource cts = new CancellationTokenSource();
                var watcher = collection.Watch(options, cts.Token);

                try
                {
                    //iterate
                    await watcher.ForEachAsync(async (elem, recordCounter) =>
                    {
                        //check limit
                        if (batchSize == 0)
                        {
                            await InsertData(elem);
                        }
                        else if(batchSize != 0)
                        {
                            if (recordCounter >= batchSize)
                            {
                                if (!cts.IsCancellationRequested)
                                {
                                    cts.Cancel();
                                }
                            }
                            else
                            {
                                await InsertData(elem);
                            }
                        }
                    }, cts.Token);
                }
                catch(OperationCanceledException)
                {
                    watcher.Dispose();
                    watcher = null;
                    _log.AppendLine($"Batch size limit({batchSize}) reached. Resuming after next iteration.");
                }
                catch(Exception ex)
                {
                    watcher.Dispose();
                    watcher = null;
                    _log.AppendLine($"The application ended with error. The error: {ex.Message}. Check error log for details.");
                    PostgresHandler.Log(_model.CollectionName, ex.ToString(), "Error");
                    //Exit if an error occured on watch method and exit the service
                    ExitAndNotify(ex.ToString());
                }
                
                //Log output
                PostgresHandler.Log(_model.CollectionName, _log.ToString());
            }
        }


        /// <summary>
        /// Dump the Bson Document into database
        /// </summary>
        /// <param name="elem"></param>
        /// <returns></returns>
        private async Task<int> InsertData(ChangeStreamDocument<BsonDocument> elem)
        {
            if (elem != null && !string.IsNullOrWhiteSpace(_model.CollectionName))
            {
                using (var ph = new PostgresHandler())
                {
                    try
                    {

                        string fulldocument = string.Empty;
                        if (_model.partiallkp == true)
                        {
                            //if operation is of type delete then don't lookup for document in mongodb
                            if((int)elem.OperationType == 3)
                            {
                                fulldocument = null;

                            }
                            else
                            { 
                                var builder = Builders<BsonDocument>.Filter;
                                var filter = builder.Eq("_id", ObjectId.Parse(elem.DocumentKey[0]?.ToString()));
                                fulldocument = _mongocollection.Find(filter).ToList()?.First()?.ToString();
                            }

                        }
                        else if (_model.partiallkp == false)
                        {
                            fulldocument = elem.FullDocument?.ToString();
                        }
                        //insert the data into postgres 
                        await ph.Insert(fulldocument, _model.CollectionName, elem.ResumeToken?.ToString(), elem.DocumentKey?.ToString(), (int)elem.OperationType, elem.OperationType.ToString());

                        //iterate through child collections
                        if (_model.ChildCollections.Any())
                        {
                            foreach (var childCollection in _model.ChildCollections)
                            {
                                if (!string.IsNullOrWhiteSpace(childCollection))
                                {
                                    await ph.Insert(fulldocument, childCollection, elem.ResumeToken?.ToString(), elem.DocumentKey?.ToString(), (int)elem.OperationType, elem.OperationType.ToString());
                                }
                            }
                        }

                        var i = ph.Commit();

                        //Log output
                        PostgresHandler.Log(_model.CollectionName, $"Recorded {i} record(s)");

                        //reset retry counter
                        _retries = 0;

                        return i;
                    }
                    catch (Exception ex)
                    {
                        //increment retry count
                        _retries += 1;

                        //exit and notify if it exceeds
                        if(_retries >= 3)
                        {
                            ExitAndNotify(ex.ToString());
                        }

                        _log.AppendLine($"An error occurred. The exception details... {Environment.NewLine}{ex.ToString()}");
                        PostgresHandler.Log(_model.CollectionName, _log.ToString(), "Error");
                        ph.Rollback();
                    }
                }
            }
            
            return 0;
        }

        /// <summary>
        /// For failure to insert after certain numbers, exit the application and notify
        /// <paramref name="errMessage"/>
        /// </summary>
        private void ExitAndNotify(string errMessage = "")
        {
            //send mail
            using (var eh = new EmailHandler())
            {
                eh.SendMail(errMessage);
            }

            //stop service
            Environment.Exit(9);
        }


        /// <summary>
        /// Dispose off the object
        /// </summary>
        public void Dispose()
        {
            _model = null;
            _lastDocument = null;
            _connectioninfo = null;
            _mongodbname = null;
            _mclient = null;
            _mongodBase = null;
            _mongocollection = null;

        }


        /// <summary>
        /// Used for the Mongo collection iteration
        /// </summary>
        public class Model
        {
            public string CollectionName { get; set; }
            public IList<string> ChildCollections { get; set; }
            public Boolean partiallkp { get; set; }

            public Model()
            {
                ChildCollections = new List<string>();
            }
        }
    }
}
