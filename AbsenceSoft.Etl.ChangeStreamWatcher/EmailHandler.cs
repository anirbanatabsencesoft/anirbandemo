﻿using Amazon;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbsenceSoft.Etl.ChangeStreamWatcher
{
    /// <summary>
    /// Class to handle email
    /// </summary>
    public class EmailHandler : IDisposable
    {
        /// <summary>
        /// AWS Access Id
        /// </summary>
        private string AwsAccessId { get { return ConfigurationManager.MailSettings["AWSAccessId"]; } }

        /// <summary>
        /// AWS Secret Key
        /// </summary>
        private string AwsSecretKey { get { return ConfigurationManager.MailSettings["AWSSecretKey"]; } }

        /// <summary>
        /// AWS Region
        /// </summary>
        private string AwsRegion { get { return ConfigurationManager.MailSettings["AWSRegion"]; } }

        /// <summary>
        /// Send Email based on the configuration
        /// </summary>
        /// <param name="errMessage"></param>
        /// <returns></returns>
        public bool SendMail(string errMessage = "")
        {
            try
            {
                var config = new AmazonSimpleEmailServiceConfig
                {
                    RegionEndpoint = RegionEndpoint.GetBySystemName(AwsRegion)
                };

                using (var client = new AmazonSimpleEmailServiceClient(AwsAccessId, AwsSecretKey, config))
                {
                    client.SendEmailAsync(GetMailRequest(errMessage)).GetAwaiter().GetResult();
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Get Email Request
        /// </summary>
        /// <param name="errMessage"></param>
        /// <returns></returns>
        private SendEmailRequest GetMailRequest(string errMessage = "")
        {
            //get the to addresses
            var to = Array.ConvertAll(ConfigurationManager.MailSettings["recipientList"].Split(";"), p => p.Trim()).ToList();

            //get the content
            var body = ConfigurationManager.MailSettings["failureBody"];

            if (!string.IsNullOrWhiteSpace(errMessage))
            {
                body = $"{body}<br/><br/>The error details...<br/>{errMessage}";
            }

            return new SendEmailRequest()
            {
                Source = ConfigurationManager.MailSettings["senderAddress"],
                Destination = new Destination
                {
                    ToAddresses = to
                },
                Message = new Message
                {
                    Subject = new Content(ConfigurationManager.MailSettings["failureSubject"]),
                    Body = new Body
                    {
                        Html = new Content
                        {
                            Charset = "UTF-8",
                            Data = body
                        },
                        Text = new Content
                        {
                            Charset = "UTF-8",
                            Data = body
                        }
                    }
                }
            };
        }         


        /// <summary>
        /// Dispose the object
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
