/*
RATIONALE:
This table is created to archive data from changestream_collection table
Maintaining a slim version of table changestream_collectionlog will be 
import for performance of ETL code
The archive table will be created in existing schema with a different tablespace
The archive table can be stored in different database,however this would reduce the
performance of archiving as it has to go through dblink 
So to make sure the performance of archiving is maintained 
and also at the same time the  existing tables are not affected tablespace 
option is used.

TABLESPACE option allows table to be stored at specific location
Also a seperate schema is created for archival of all tables




*/


--CREATE TABLESPACE archivespace LOCATION '<location>';
CREATE SCHEMA IF NOT EXISTS archive;
CREATE TABLE IF NOT EXISTS  archive.mongo_changestream_log_archive
(
	id bigint ,
	created_date timestamp without time zone ,
	mongo_collection_name character varying(200) ,
	updated_date timestamp without time zone,
	is_processed boolean ,
	objbson text,
	change_stream_id varchar(1000),
	is_archived boolean,
	document_key text,
	operation_type integer,
	operation_type_description character varying(100)
) ;

ALTER TABLE archive.mongo_changestream_log_archive OWNER to postgres;
GRANT SELECT ON TABLE archive.mongo_changestream_log_archive TO absence_tracker;
GRANT ALL ON TABLE archive.mongo_changestream_log_archive TO etldw;
GRANT ALL ON TABLE archive.mongo_changestream_log_archive TO postgres;
 

-- Check if it is possible in AWS Aurora
-- add rationale for decisions

--Add processing status for error
ALTER TABLE archive.mongo_changestream_log_archive ADD COLUMN IF NOT EXISTS has_error boolean;

ALTER TABLE archive.mongo_changestream_log_archive ADD COLUMN IF NOT EXISTS err_message text;

ALTER TABLE archive.mongo_changestream_log_archive ADD COLUMN IF NOT EXISTS err_stack_trace text;


/*

This procedure archives the table changestream_collectionlog
The data is moved into archive.changestream_collectionlog_archive
The data is moved on hourly basis into archive table

The job runs at 15 mins past each hour using a scheduler like cron
For ex if the job runs at 1015hrs in the morning all rows from 
changestream_collectionlog which are processed(flag  = 'Y')
and inserted before 1000hrs are archived. The datetime is used 
in the sql query to make sure if the record is not processed in one iteration
the next scheduler will move the record moved into archive table

All moved records are deleted from main table using the same logic as above


*/
CREATE OR REPLACE FUNCTION UFN_CHANGESTREAM_ARCHIVE()
  RETURNS void AS
$BODY$
BEGIN
  create temp table collectionfilter  as
    select
      mongo_collection_name,
      min(id) as filterid
    from
      (
        select
          mongo_collection_name,
          id,
          rank()
          over (
            partition by mongo_collection_name
            order by id desc ) rnk
        from mongo_changestream_log
        where created_date < (now() - interval '15 minutes')
              and is_processed and err_message is null
      ) a
    where rnk <= 5
    group by mongo_collection_name;


  INSERT INTO archive.mongo_changestream_log_archive (
    id, created_date, mongo_collection_name, updated_date, is_processed, objbson, change_stream_id, is_archived,
    document_key, operation_type, operation_type_description, has_error, err_message, err_stack_trace)
    SELECT
      a.id,
      a.created_date,
      a.mongo_collection_name,
      a.updated_date,
      a.is_processed,
      a.objbson,
      a.change_stream_id,
      a.is_archived,
      a.document_key,
      a.operation_type,
      a.operation_type_description,
      a.has_error,
      a.err_message,
      a.err_stack_trace
    FROM
      mongo_changestream_log a inner join collectionfilter b
        on a.mongo_collection_name = b.mongo_collection_name

    WHERE a.is_processed is true and a.err_message is null
          and a.id < b.filterid;

  DELETE FROM mongo_changestream_log a
  using collectionfilter b
  WHERE a.mongo_collection_name = b.mongo_collection_name
        and a.is_processed is true and a.err_message is null
        and a.id < b.filterid;

  drop table collectionfilter;

END;
$BODY$
LANGUAGE 'plpgsql'
VOLATILE
COST 100;


CREATE OR REPLACE FUNCTION UFN_CHANGESTREAM_ARCHIVE(intBatchSize int)
  RETURNS void AS
$BODY$
DECLARE
  minid  INTEGER;
  maxid  INTEGER;
  loopid INTEGER;
BEGIN
  select
    min(id),
    max(id)
  into minid, maxid
  from mongo_changestream_log;
  loopid := minid;
  WHILE (loopid <= maxid)
  LOOP
    INSERT INTO archive.mongo_changestream_log_archive (
      id, created_date, mongo_collection_name, updated_date, is_processed, objbson, change_stream_id, is_archived,
      document_key, operation_type, operation_type_description, has_error, err_message, err_stack_trace)
      SELECT
        id,
        created_date,
        mongo_collection_name,
        updated_date,
        is_processed,
        objbson,
        change_stream_id,
        is_archived,
        document_key,
        operation_type,
        operation_type_description,
        has_error,
        err_message,
        err_stack_trace
      FROM
        mongo_changestream_log
      WHERE is_processed is true
            and created_date < (date_trunc('hour', now()) :: timestamp(0)) and id >= loopid and
            id < (loopid + intBatchSize);

    DELETE FROM mongo_changestream_log
    WHERE is_processed is true
          and created_date < (date_trunc('hour', now()) :: timestamp(0)) and id >= loopid and
          id < (loopid + intBatchSize);

    loopid := loopid + intBatchSize;
  END LOOP;


END;
$BODY$
LANGUAGE 'plpgsql'
VOLATILE
COST 100;
