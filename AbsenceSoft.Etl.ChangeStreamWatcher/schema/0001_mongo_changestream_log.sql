﻿-- Create Sequence
CREATE SEQUENCE IF NOT EXISTS public.mongo_changestream_log_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.mongo_changestream_log_id_seq OWNER TO postgres;



--Create Table
CREATE TABLE IF NOT EXISTS public.mongo_changestream_log
(
  id bigint NOT NULL DEFAULT nextval('mongo_changestream_log_id_seq'::regclass),
  created_date timestamp without time zone DEFAULT now(),
  mongo_collection_name character varying(30),
  updated_date timestamp without time zone,
  is_processed boolean DEFAULT false,
  objbson text,
  change_stream_id character varying(1000),
  is_archived boolean DEFAULT false
)
WITH (
  OIDS=FALSE
);

ALTER TABLE public.mongo_changestream_log OWNER TO postgres;

--create indexes
DROP INDEX IF EXISTS mongo_changestream_log_name;
DROP INDEX IF EXISTS mongo_changestream_log_processed ;
DROP INDEX IF EXISTS mongo_changestream_log_archived;



--Adding columns
ALTER TABLE public.mongo_changestream_log
	ADD COLUMN IF NOT EXISTS document_key text;

ALTER TABLE public.mongo_changestream_log
	ADD COLUMN IF NOT EXISTS operation_type INT;

ALTER TABLE public.mongo_changestream_log
	ADD COLUMN IF NOT EXISTS operation_type_description varchar(100);

--Add processing status for error
ALTER TABLE public.mongo_changestream_log
	ADD COLUMN IF NOT EXISTS has_error boolean;

ALTER TABLE public.mongo_changestream_log
	ADD COLUMN IF NOT EXISTS err_message text;

ALTER TABLE public.mongo_changestream_log
	ADD COLUMN IF NOT EXISTS err_stack_trace text;



-- Add indexes last	
CREATE INDEX IF NOT EXISTS mongo_changestream_log_etl_index ON public.mongo_changestream_log (id, mongo_collection_name, is_archived, is_processed);
CREATE INDEX IF NOT EXISTS mongo_changestream_log_date_index ON public.mongo_changestream_log (id,created_date, updated_date);
CREATE INDEX IF NOT EXISTS mongo_changestream_log_research ON mongo_changestream_log (document_key,err_message );