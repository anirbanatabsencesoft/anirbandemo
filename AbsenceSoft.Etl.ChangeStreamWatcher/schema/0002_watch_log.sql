﻿-- Create Sequence
CREATE SEQUENCE IF NOT EXISTS public.watch_log_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.watch_log_id_seq OWNER TO postgres;



--Create Table
CREATE TABLE IF NOT EXISTS public.watch_log
(
  id bigint NOT NULL DEFAULT nextval('watch_log_id_seq'::regclass),
  created_date timestamp without time zone DEFAULT now(),
  log_type varchar(10) NOT NULL DEFAULT 'Info',
  collection_name varchar(200),
  log_message text,
  is_archived boolean DEFAULT false
)
WITH (
  OIDS=FALSE
);

ALTER TABLE public.watch_log OWNER TO postgres;

CREATE INDEX IF NOT EXISTS watch_log_index ON public.watch_log (id, created_date, log_type, collection_name);

