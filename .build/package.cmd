rem NOTE: this process should be executed from the source root
rem Example: .build\package.cmd
@echo off
.build\7z a "artifacts\AbsenceSoft.Process.zip" "AbsenceSoft.Process\bin"
.build\7z a "artifacts\AbsenceSoft.Setup.zip" "AbsenceSoft.Setup\bin"
.build\7z a "artifacts\AbsenceSoft.Process.Console.zip" "AbsenceSoft.Process.Console\bin"
.build\7z a "artifacts\comm.zip" "AbsenceSoft.Utilities.Communications\bin"
.build\7z a "artifacts\AbsenceSoft.Process.zip" "AbsenceSoft.Process\deploy" "AbsenceSoft.Process\bin"
copy /y /b "AbsenceSoft\obj\%1\Package\*.zip" "artifacts\*.zip"
.build\7z a ftp-automation.tar -ttar -so "AbsenceSoft.FtpAutomation\app.js" -i!"AbsenceSoft.FtpAutomation\package.json" -i!"AbsenceSoft.FtpAutomation\README.md" -i!"AbsenceSoft.FtpAutomation\.config\*" -i!"AbsenceSoft.FtpAutomation\modules\*" -i!"AbsenceSoft.FtpAutomation\.test\*" | .build\7z a "artifacts\ftp-automation.tar.gz" -tgzip -siftp-automation.tar
.build\7z a "artifacts\AbsenceSoft.Configuration.zip" "AbsenceSoft.Configuration\bin"