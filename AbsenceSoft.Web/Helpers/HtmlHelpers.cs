﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;

namespace AbsenceSoft.Web.Helpers
{
    public static class HtmlHelpers
    {
        public static string RouteUrl(this UrlHelper urlHelper, string routeName, object routeValues, bool inheritRouteParams)
        {
            // replace urlhelper with a new one that has no inherited route data
            if (!inheritRouteParams)
                urlHelper = new UrlHelper(new RequestContext(urlHelper.RequestContext.HttpContext, new RouteData()));

            return urlHelper.RouteUrl(routeName, routeValues);
        }

        public static string RouteUrl(this UrlHelper urlHelper, string routeName, bool inheritRouteParams)
        {
            return urlHelper.RouteUrl(routeName, null, inheritRouteParams);
        }

    }
}
