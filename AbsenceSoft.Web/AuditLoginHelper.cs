﻿using AbsenceSoft.Data.Audit;
using AbsenceSoft.Data.Security;
using System;
using System.Web;

namespace AbsenceSoft.Web
{
    /// <summary>
    /// Used as a "mixin" class to implement a change pattern.
    /// The use of this class is not required for tracking changes
    /// and you are free to use whatever implementation you like.
    /// </summary>
    [Serializable]
    public static class AuditLoginHelper
    {
        /// <summary>
        /// Captures Login information
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="email">The email of user</param>
        /// <param name="ipAddress">The IP address.</param>
        /// <param name="browser">The browser information.</param>
        public static void AuditLogin(User user, string email, string ipAddress = null, string browser = null)
        {
            if (!AbsenceSoft.Common.Properties.Settings.Default.Audit)
                return;

            string clientIp = ipAddress, clientBrowser = browser;
            if ((string.IsNullOrWhiteSpace(clientIp) || string.IsNullOrWhiteSpace(clientBrowser))
                && HttpContext.Current != null && HttpContext.Current.Request != null)
            {
                clientIp = HttpContext.Current.Request.ClientIPAddress();
                clientBrowser = HttpContext.Current.Request.UserAgent;
            }

            new LoginHistory()
            {
                LoginStatus = LoginType.Login,
                CreatedBy = user,
                UserId = user.Id,
                CustomerId = user.CustomerId,
                IpAddress = clientIp,
                Browser = clientBrowser,
                Email = email,
                IsSuccess = true,
                IsSso = false,
                EmployerAccess = user.Employers,
                UserRoles = user.Roles
            }.Save();
        }

        /// <summary>
        /// Captures Logout information
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="email">The email of user</param>
        /// <param name="ipAddress">The IP address.</param>
        /// <param name="browser">The browser information.</param>
        public static void AuditLogout(User user, string email, string ipAddress = null, string browser = null)
        {
            if (!AbsenceSoft.Common.Properties.Settings.Default.Audit)
                return;

            string clientIp = ipAddress, clientBrowser = browser;
            if ((string.IsNullOrWhiteSpace(clientIp) || string.IsNullOrWhiteSpace(clientBrowser))
                && HttpContext.Current != null && HttpContext.Current.Request != null)
            {
                clientIp = HttpContext.Current.Request.ClientIPAddress();
                clientBrowser = HttpContext.Current.Request.UserAgent;
            }

            new LoginHistory()
            {
                LoginStatus = LoginType.Logout,
                CreatedBy = user,
                UserId = user.Id,
                CustomerId = user.CustomerId,
                IpAddress = clientIp,
                Browser = clientBrowser,
                Email = email,
                IsSuccess = true,
                IsSso = false,
                EmployerAccess = user.Employers,
                UserRoles = user.Roles
            }.Save();
        }

        /// <summary>
        /// Captures Login failure information
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="email">The email of user</param>
        /// <param name="status">The failure status.</param>
        /// <param name="ipAddress">The IP address.</param>
        /// <param name="browser">The browser information.</param>
        public static void AuditLoginFailure(User user, string email, LoginType status, string ipAddress = null, string browser = null)
        {
            if (!AbsenceSoft.Common.Properties.Settings.Default.Audit)
                return;

            string clientIp = ipAddress, clientBrowser = browser;
            if ((string.IsNullOrWhiteSpace(clientIp) || string.IsNullOrWhiteSpace(clientBrowser))
                && HttpContext.Current != null && HttpContext.Current.Request != null)
            {
                clientIp = HttpContext.Current.Request.ClientIPAddress();
                clientBrowser = HttpContext.Current.Request.UserAgent;
            }

            if (user == null)
            {
                Data.Customers.Customer customer = Current.Customer();
                new LoginHistory()
                {
                    LoginStatus = status,
                    CreatedBy = new User(),
                    UserId = null,
                    CustomerId = customer != null ? customer.Id : null,
                    IpAddress = clientIp,
                    Browser = clientBrowser,
                    Email = email,
                    IsSuccess = false,
                    IsSso = false,
                    EmployerAccess = null,
                    UserRoles = null
                }.Save();
            }
            else
            {
                new LoginHistory()
                {
                    LoginStatus = status,
                    CreatedBy = user,
                    UserId = user.Id,
                    CustomerId = user.CustomerId,
                    IpAddress = clientIp,
                    Browser = clientBrowser,
                    Email = email,
                    IsSuccess = true,
                    IsSso = false,
                    EmployerAccess = user.Employers,
                    UserRoles = user.Roles
                }.Save();
            }
        }

        /// <summary>
        /// Captures Sso Login information
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="email">The email of user</param>
        /// <param name="subject">The subject.</param>
        /// <param name="ipAddress">The IP address.</param>
        /// <param name="browser">The browser information.</param>
        public static void AuditSsoLogin(User user, string email, string subject, string ipAddress = null, string browser = null)
        {
            if (!AbsenceSoft.Common.Properties.Settings.Default.Audit)
                return;

            string clientIp = ipAddress, clientBrowser = browser;
            if ((string.IsNullOrWhiteSpace(clientIp) || string.IsNullOrWhiteSpace(clientBrowser))
                && HttpContext.Current != null && HttpContext.Current.Request != null)
            {
                clientIp = HttpContext.Current.Request.ClientIPAddress();
                clientBrowser = HttpContext.Current.Request.UserAgent;
            }

            new LoginHistory()
            {
                LoginStatus = LoginType.Login,
                CreatedBy = user,
                UserId = user.Id,
                CustomerId = user.CustomerId,
                IpAddress = clientIp,
                Browser = clientBrowser,
                Email = email,
                Subject = subject,
                IsSuccess = true,
                IsSso = true,
                EmployerAccess = user.Employers,
                UserRoles = user.Roles
            }.Save();
        }
    }
}
