﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Logic.SSO;
using AT.Entities.Authentication;
using AT.Logic.Authentication;
using ComponentSpace.SAML2;
using ComponentSpace.SAML2.Assertions;
using ComponentSpace.SAML2.Bindings;
using ComponentSpace.SAML2.Profiles.ArtifactResolution;
using ComponentSpace.SAML2.Profiles.SSOBrowser;
using ComponentSpace.SAML2.Protocols;
using System;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using System.Xml;

namespace AbsenceSoft.Web.Controllers
{
    public abstract class BaseSSOController : SharedBaseController
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationType _appType;
        public BaseSSOController(ApplicationType appType)
        {
            _appType = appType;
        }
        public BaseSSOController(ApplicationSignInManager applicationSignInManager, ApplicationType appType)
        {
            _signInManager = applicationSignInManager;
            _appType = appType;
        }

        /// <summary>
        /// Gets the new object of ApplicationSignInManager.
        /// </summary>
        /// <value>
        /// The sign in manager.
        /// </value>
        protected ApplicationSignInManager SignInManager
        {
            get
            {
                if (_signInManager == null)
                {
                    _signInManager = new ApplicationSignInManager(HttpContext.GetOwinContext().Authentication);
                }
                return _signInManager;
            }
        }
        /// <summary>
        /// Represents the SAML binding type to process from the request/endpoint
        /// </summary>
        private enum SamlBindingType
        {
            /// <summary>
            /// The POST SAML binding type.
            /// </summary>
            POST,
            /// <summary>
            /// The artifact SAML binding type.
            /// </summary>
            Artifact,
            /// <summary>
            /// The redirect SAML binding type.
            /// </summary>
            Redirect
        }

        // urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST
        [HttpPost]
        [Route("SSO/saml2/AssertionConsumerService")]
        public async virtual Task<ActionResult> AssertionConsumerService()
        {
            return await ReceiveSAMLResponse(SamlBindingType.POST);
        }
        
        // urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact
        [HttpPost]
        [Route("SSO/saml2/AssertionConsumerService.artifact")]
        public async virtual Task<ActionResult> AssertionConsumerServiceArtifact()
        {
            return await ReceiveSAMLResponse(SamlBindingType.Artifact);
        }

        // urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect
        [HttpGet]
        [Route("SSO/saml2/AssertionConsumerService.redirect")]
        public async virtual Task<ActionResult> AssertionConsumerServiceRedirect()
        {
            return await ReceiveSAMLResponse(SamlBindingType.Redirect);
        }

        [HttpGet]
        [Route("SSO/saml2/SLOService")]
        public virtual ActionResult SloService()
        {
            // Receive the single logout request or response.
            // If a request is received then single logout is being initiated by the identity provider.
            // If a response is received then this is in response to single logout having been initiated by the service provider.
            bool isRequest = false;
            string logoutReason = null;
            string partnerSP = null;

            SAMLServiceProvider.ReceiveSLO(Request, out isRequest, out logoutReason, out partnerSP);

            if (isRequest)
            {
                // Logout locally.
                new AuthenticationService().Using(s => s.Logout(HttpContext));

                // Respond to the IdP-initiated SLO request indicating successful logout.
                SAMLServiceProvider.SendSLO(Response, null);
            }
            else
            {
                // SP-initiated SLO has completed.
                FormsAuthentication.RedirectToLoginPage();
            }

            return new EmptyResult();
        }

        /// <summary>
        /// The SSO profile
        /// </summary>
        private SsoProfile profile = null;
        /// <summary>
        /// Gets the SSO profile and returns whether or not it was found.
        /// </summary>
        /// <returns></returns>
        private bool GetProfile()
        {
            var currentEmployer = CurrentEmployer;
            var currentCustomer = CurrentCustomer;

            if (currentEmployer != null)
            {
                profile = SsoProfile.AsQueryable().Where(p => p.CustomerId == currentCustomer.Id && p.EmployerId == currentEmployer.Id).FirstOrDefault();
            }
            else if (currentCustomer != null)
            {
                profile = SsoProfile.AsQueryable().Where(p => p.CustomerId == currentCustomer.Id && p.EmployerId == null).FirstOrDefault();
            }

            return profile != null;
        }

        // Receive the SAML response from the identity provider.
        private async Task<ActionResult> ReceiveSAMLResponse(SamlBindingType bindingType)
        {
            if (!GetProfile())
            {
                Log.Error("Errors.SSO.NotImplemented: SAML Configuration has not been implemented for this customer.");
                return SsoBadRequest(C("Errors.SSO.NotImplemented", "SAML Configuration has not been implemented for this customer. Please contact your implementation specialist or sales associate for assistance."));
            }

            // Receive the SAML response over the specified binding.
            XmlElement samlResponseXml = null;
            bool signed = false;
            SAMLResponse samlResponse;
            string relayState;

            switch (bindingType)
            {
                case SamlBindingType.POST:
                    ServiceProvider.ReceiveSAMLResponseByHTTPPost(Request, out samlResponseXml, out relayState);
                    break;

                case SamlBindingType.Artifact:
                    if (string.IsNullOrWhiteSpace(profile.ArtifactResolutionServiceUrl))
                    {
                        Log.Error("Errors.SSO.NoArtifactResolutionService: No Artifact Resolution Service URL is configured.");
                        return SsoBadRequest(C("Errors.SSO.NoArtifactResolutionService", "No Artifact Resolution Service URL is configured. Please contact your implementation specialist or sales associate for assistance."));
                    }

                    // Receive the artifact.
                    HTTPArtifact httpArtifact = null;

                    ServiceProvider.ReceiveArtifactByHTTPArtifact(Request, false, out httpArtifact, out relayState);

                    // Create an artifact resolve request.
                    ArtifactResolve artifactResolve = new ArtifactResolve();
                    artifactResolve.Issuer = new Issuer(new Uri(Request.Url, VirtualPathUtility.ToAbsolute("~/")).ToString());
                    artifactResolve.Artifact = new Artifact(httpArtifact.ToString());

                    XmlElement artifactResolveXml = artifactResolve.ToXml();

                    // Send the artifact resolve request and receive the artifact response.
                    XmlElement artifactResponseXml = ArtifactResolver.SendRequestReceiveResponse(profile.ArtifactResolutionServiceUrl, artifactResolveXml);

                    ArtifactResponse artifactResponse = new ArtifactResponse(artifactResponseXml);

                    // Extract the authentication request from the artifact response.
                    samlResponseXml = artifactResponse.SAMLMessage;
                    break;

                case SamlBindingType.Redirect:
                    AsymmetricAlgorithm key = null;
                    if (profile.PartnerCertificate != null)
                    {
                        key = new X509Certificate2(profile.PartnerCertificate, (string)null, X509KeyStorageFlags.MachineKeySet).PublicKey.Key;
                    }

                    HTTPRedirectBinding.ReceiveResponse(Request, out samlResponseXml, out relayState, out signed, key);
                    break;

                default:
                    Log.Error("Errors.SSO.UnsupportedSAMLBinding: An unknown or non-implemented SAML binding was requested.");
                    return SsoBadRequest(C("Errors.SSO.UnsupportedSAMLBinding", "An unknown or non-implemented SAML binding was requested. Please contact your implementation specialist or sales associate for assistance."));
            }

            // Verify the response's signature.
            if (signed || SAMLMessageSignature.IsSigned(samlResponseXml))
            {
                if (profile.PartnerCertificate == null)
                {
                    Log.Error("Errors.SSO.NoSigningKey: The SAML response signature was unable to be verified. No signing key on file.");
                    return SsoBadRequest(C("Errors.SSO.NoSigningKey: The SAML response signature was unable to be verified. No signing key on file."));
                }

                X509Certificate2 x509Certificate = new X509Certificate2(profile.PartnerCertificate, (string)null, X509KeyStorageFlags.MachineKeySet);
                if (!SAMLMessageSignature.Verify(samlResponseXml, x509Certificate))
                {
                    Log.Error("Errors.SSO.SignatureValidation: The SAML response signature was invalid or was unable to be validated.");
                    return SsoBadRequest(C("Errors.SSO.SignatureValidation", "The SAML response signature was invalid or was unable to be validated."));
                }
            }

            // Deserialize the XML.
            samlResponse = new SAMLResponse(samlResponseXml);

            // Check whether the SAML response indicates success or an error and process accordingly.
            if (samlResponse.IsSuccess())
            {
                return await ProcessSuccessSAMLResponse(samlResponse, relayState);
            }

            return ProcessErrorSAMLResponse(samlResponse);
        }

        // Process a successful SAML response.
        private async Task<ActionResult> ProcessSuccessSAMLResponse(SAMLResponse samlResponse, string relayState)
        {
            // Load the decryption key.
            X509Certificate2 x509Certificate = profile.LocalCertificate.ToCertificate();

            // Extract the asserted identity from the SAML response.
            SAMLAssertion samlAssertion = null;

            if (samlResponse.GetAssertions().Count > 0)
            {
                samlAssertion = samlResponse.GetAssertions()[0];
            }
            else if (samlResponse.GetEncryptedAssertions().Count > 0)
            {
                samlAssertion = samlResponse.GetEncryptedAssertions()[0].Decrypt(x509Certificate.PrivateKey, null, null);
            }
            else
            {
                var signedAssertions = samlResponse.GetSignedAssertions();
                if (signedAssertions != null && signedAssertions.Count > 0)
                {
                    samlAssertion = new SAMLAssertion(signedAssertions[0]);                    
                }
                else
                {
                    Log.Error("Errors.SSO.MissingAssertion: No assertions in response");
                    return SsoBadRequest(C("Errors.SSO.MissingAssertion", "No assertions in response"));
                }
            }


            Log.Info("SSO SAML ASSERTION: {0}", samlAssertion.ToString());

            // Get the subject name identifier.
            string userName = null;

            if (samlAssertion.Subject.NameID != null)
            {
                userName = samlAssertion.Subject.NameID.NameIdentifier;
            }
            else if (samlAssertion.Subject.EncryptedID != null)
            {
                NameID nameID = samlAssertion.Subject.EncryptedID.Decrypt(x509Certificate.PrivateKey, null, null);
                userName = nameID.NameIdentifier;
            }
            else
            {
                Log.Error("Errors.SSO.MissingAssertion: No assertions in response");
                return SsoBadRequest(C("Errors.SSO.MissingNameID", "No name in subject"));
            }

            string roleName = samlAssertion.GetAttributeValue(SsoConfiguration.RoleAttributeName) ?? samlAssertion.GetAttributeValueByFriendlyName(SsoConfiguration.RoleAttributeName);
            string employerReferenceCode = samlAssertion.GetAttributeValue(SsoConfiguration.EmployerReferenceCodeName) ?? samlAssertion.GetAttributeValueByFriendlyName(SsoConfiguration.EmployerReferenceCodeName);
            //HACK: For Pru, they can't send the employer reference code any other way but with the name "CONTROL_NUMBER", so that
            //  is hard-coded here :-(
            if (string.IsNullOrWhiteSpace(employerReferenceCode))
            {
                employerReferenceCode = samlAssertion.GetAttributeValue("CONTROL_NUMBER") ?? samlAssertion.GetAttributeValueByFriendlyName("CONTROL_NUMBER");
            }
            // Now, ensure the if we have an employer reference code in the assertion that it's a valid one.
            if (!string.IsNullOrWhiteSpace(employerReferenceCode))
            {
                Employer employer = Employer.AsQueryable().FirstOrDefault(e => e.ReferenceCode == employerReferenceCode);
                if (employer == null)
                {
                    Log.Error("Errors.SSO.UnknownEmployer: Invalid or unknown employer");
                    return SsoBadRequest(C("Errors.SSO.UnknownEmployer", "Invalid or unknown employer"));
                }

                Current.Employer(employer);
            }
            
            // Get the originally requested resource URL from the relay state.
            RelayState cachedRelayState = null;
            if (!string.IsNullOrWhiteSpace(relayState))
            {
                cachedRelayState = RelayStateCache.Remove(relayState);
            }

            Log.Info("SSO Requested - Attempt Authentication, Subject={0}, Role={1}, EmployerReferenceCode={2}", userName, roleName, employerReferenceCode);

            // Create a login context for the asserted identity.
            string errorMessage = null;                       
            using (AuthenticationService authService = new AuthenticationService())
            {
                Data.Security.User user = authService.AuthenticateSso(HttpContext, userName, out errorMessage, roleName, employerReferenceCode);
                bool resultSSO = false;
                if (user!=null)
                {
                    AbsenceSoft.Web.AuditLoginHelper.AuditSsoLogin(user, user.Email, userName); 
                    resultSSO = await SignInManager.SignInSSOAsync(user.Email, _appType);
                }

                if (!resultSSO)
                {
                    Log.Warn("SignInSSOAsync Failed, Subject={0}, Error={1}, Role={2}, EmployerReferenceCode={3}", userName, errorMessage, roleName, employerReferenceCode);
                    return SsoBadRequest(C("Errors.SSO.AuthFailed", errorMessage ?? "Unable to authenticate, authorize or process user credentials in SAML assertion"));
                }
                else
                {
                    Log.Info("SSO Auth Successful");
                }

                // Redirect to the originally requested resource URL.
                if (cachedRelayState == null && string.IsNullOrWhiteSpace(relayState) && !Uri.IsWellFormedUriString(relayState, UriKind.Relative))
                {
                    Log.Info("SSO Redirect to Index Home");
                    return RedirectToAction("Index", "Home");
                }

                var targetUrl = cachedRelayState?.ResourceURL ?? relayState;

                // If no target URL is provided, provide a default.
                if (string.IsNullOrWhiteSpace(targetUrl))
                {
                    targetUrl = "~/";
                }
                else
                {
                    targetUrl = AbsenceSoft.Web.RequestExtensions.GetRelativeUrl(targetUrl);
                }

                // Redirect to the target URL.
                if (Url.IsLocalUrl(targetUrl))
                {
                    Log.Info("SSO Redirect to {0}", targetUrl);
                    return Redirect(targetUrl);
                }

                Log.Info("SSO Redirect to Index Home");
                return RedirectToAction("Index", "Home");
            }

        }

        // Process an error SAML response.
        private ActionResult ProcessErrorSAMLResponse(SAMLResponse samlResponse)
        {
            string errorMessage = null;

            if (samlResponse.Status.StatusMessage != null)
                errorMessage = samlResponse.Status.StatusMessage.Message;

            Log.Warn("SSO Failed, Error={0}", errorMessage);
            return RedirectToAction("Login", "Home", new { error = C("Errors.SSO.SAMLFailed", errorMessage) });
        }

        private ActionResult SsoBadRequest(string message)
        {
            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            Response.StatusDescription = message;
            return View("Error");
        }

        [HttpGet]
        [Route("SSO/saml2/SingleSignOn")]
        public ActionResult AssertionInitiate()
        {
            return CreateSAMLRequest();
        }

        private ActionResult CreateSAMLRequest()
        {
            string ret = Request.QueryString["ReturnUrl"];

            if (!GetProfile())
            {
                return RedirectToAction("Login", "Account", new { ReturnUrl = ret });
            }

            RequestLoginAtIdentityProvider(profile, ret);
            
            return new EmptyResult();
        }

        // Initiate the SSO by sending an authentication request to the identity provider.
        private void RequestLoginAtIdentityProvider(SsoProfile config, string relayState)
        {
            // Create the authentication request.
            XmlElement authnRequestXml = CreateAuthnRequest(config);

            // Create and cache the relay state so we remember which SP resource the user wishes to access after SSO.
            relayState = RelayStateCache.Add(new RelayState(relayState, null));

            // Send the authentication request to the identity provider over the configured binding.
            switch (config.SingleSignOnServiceBinding)
            {
                case SAMLIdentifiers.BindingURIs.HTTPRedirect:
                    AsymmetricAlgorithm key = null;
                    if (config.SignAuthnRequest)
                    {
                        key = config.LocalCertificate.ToCertificate().PrivateKey;
                    }
                    ServiceProvider.SendAuthnRequestByHTTPRedirect(Response, config.SingleSignOnServiceUrl, authnRequestXml, relayState, key);
                    break;

                case SAMLIdentifiers.BindingURIs.HTTPPost:
                    ServiceProvider.SendAuthnRequestByHTTPPost(Response, config.SingleSignOnServiceUrl, authnRequestXml, relayState);

                    // Don't send this form.
                    Response.End();
                    break;

                case SAMLIdentifiers.BindingURIs.HTTPArtifact:
                    // Create the artifact.
                    string identificationURL = CreateAbsoluteUrl("~/");
                    HTTPArtifactType4 httpArtifact = new HTTPArtifactType4(HTTPArtifactType4.CreateSourceId(identificationURL), HTTPArtifactType4.CreateMessageHandle());

                    // Cache the authentication request for subsequent sending using the artifact resolution protocol.
                    HTTPArtifactState httpArtifactState = new HTTPArtifactState(authnRequestXml, null);
                    HTTPArtifactStateCache.Add(httpArtifact, httpArtifactState);

                    // Send the artifact.
                    ServiceProvider.SendArtifactByHTTPArtifact(Response, config.SingleSignOnServiceUrl, httpArtifact, relayState, false);
                    break;

                default:
                    throw new ArgumentException("Invalid binding type");
            }
        }

        // Create an authentication request.
        private XmlElement CreateAuthnRequest(SsoProfile config)
        {
            // Create the authentication request.
            AuthnRequest authnRequest = new AuthnRequest();
            authnRequest.Destination = config.SingleSignOnServiceUrl;
            authnRequest.Issuer = new Issuer(config.EntityId);
            authnRequest.ForceAuthn = config.ForceAuthn;
            authnRequest.NameIDPolicy = new NameIDPolicy(null, null, true);
            authnRequest.ProviderName = config.EntityId;
            //No providerId parameter given in Shibboleth SSO authentication request..

            // Serialize the authentication request to XML for transmission.
            XmlElement authnRequestXml = authnRequest.ToXml();

            // Don't sign if using HTTP redirect as the generated query string is too long for most browsers.        
            if (config.SingleSignOnServiceBinding != SAMLIdentifiers.BindingURIs.HTTPRedirect && config.SignAuthnRequest)
            {
                // Sign the authentication request.
                X509Certificate2 x509Certificate = config.LocalCertificate.ToCertificate();
                SAMLMessageSignature.Generate(authnRequestXml, x509Certificate.PrivateKey, x509Certificate);
            }

            return authnRequestXml;
        }

        /// <summary>
        /// Converts the provided app-relative path into an absolute Url containing the 
        /// full host name
        /// </summary>
        /// <param name="relativeUrl">App-Relative path</param>
        /// <returns>Provided relativeUrl parameter as fully qualified Url</returns>
        /// <example>~/path/to/foo to http://www.web.com/path/to/foo</example>
        public string CreateAbsoluteUrl(string relativeUrl)
        {
            if (string.IsNullOrEmpty(relativeUrl))
                return relativeUrl;

            if (HttpContext.CurrentHandler == null)
            {
                return relativeUrl;
            }

            if (relativeUrl.StartsWith("/"))
            {
                relativeUrl = relativeUrl.Insert(0, "~");
            }

            if (!relativeUrl.StartsWith("~/"))
            {
                relativeUrl = relativeUrl.Insert(0, "~/");
            }

            return VirtualPathUtility.ToAbsolute(relativeUrl);
        }

    }
}