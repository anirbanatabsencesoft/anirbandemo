﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Customers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AbsenceSoft.Web.Controllers
{
    public class SharedBaseController:Controller
    {
        private User _User { get; set; }
        /// <summary>
        /// Gets the current user.
        /// </summary>
        /// <value>
        /// The current user.
        /// </value>
        protected virtual User CurrentUser
        {
            get
            {
                if (_User == null)
                    _User = Current.User();

                return _User;
            }
        }

        private Customer _Customer { get; set; }
        /// <summary>
        /// Gets the current customer.
        /// </summary>
        /// <value>
        /// The current customer.
        /// </value>
        protected virtual Customer CurrentCustomer
        {
            get
            {
                if (_Customer == null)
                    _Customer = Current.Customer();
                return _Customer;
            }
        }

        private Employer _Employer { get; set; }
        /// <summary>
        /// Gets the current employer.
        /// </summary>
        /// <value>
        /// The current employer.
        /// </value>
        protected virtual Employer CurrentEmployer
        {
            get
            {
                if (_Employer == null)
                    _Employer = Current.Employer();

                return _Employer;
            }
        }

        /// <summary>
        /// Redirects the via java script.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <returns></returns>
        protected JavaScriptResult RedirectViaJavaScript(string url)
        {
            return JavaScript(string.Format("window.location = '{0}'", url));
        }

        /// <summary>
        /// Reloads the via java script.
        /// </summary>
        /// <returns></returns>
        protected JavaScriptResult ReloadViaJavaScript()
        {
            return JavaScript("location.reload(true)");
        }
        
        /// <summary>
        /// The content for the current customer/employer for text regions or use for replacement, etc.
        /// </summary>
        private Hashtable _content = null;

        /// <summary>
        /// Gets the custom content for the specified key and if not found, returns the default content.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="defaultContent">The default content.</param>
        /// <returns></returns>
        protected virtual string C(string key, string defaultContent = null)
        {
            return C(key, null, null, defaultContent);
        }

        /// <summary>
        /// Gets the custom content for the specified key and if not found, returns the default content.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="employee">The employee.</param>
        /// <param name="case">The case.</param>
        /// <param name="defaultContent">The default content.</param>
        /// <returns></returns>
        protected virtual string C(string key, Employee employee, Case @case, string defaultContent = null)
        {
            using (ContentService svc = new ContentService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                if (_content == null)
                    _content = svc.GetAllContent();
                var text = svc.Get(_content, key, null);
                text = svc.PerformTextMerge(text, @case, employee);
                return text ?? defaultContent;
            }
        }
    }
}
