﻿using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Processing;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Routing;

namespace AbsenceSoft.Web
{
    public static class Current
    {
        private const string CURRENT_USER_CONTEXT_KEY = "__current_user__";
        private const string CURRENT_USER_PORTAL_CONTEXT = "__portal_context";

        /// <summary>
        /// Gets the current user appropriately typed
        /// If the current user is not TUser, will return null
        /// </summary>
        /// <typeparam name="TUser"></typeparam>
        /// <returns></returns>
        public static User User()
        {
            User currentUser = null;
            try
            {
                HttpContext context = HttpContext.Current;
                if (context != null && context.Items != null && context.User != null && context.User.Identity != null && !string.IsNullOrWhiteSpace(context.User.Identity.Name))
                {
                    currentUser = context.Items[CURRENT_USER_CONTEXT_KEY] as User;
                    if (currentUser != null)
                        return currentUser;

                    /// We call the GetById here for the repository because if User.Current calls GetById it will setup an infinite loop
                    /// where User.Current is called
                    if (!(context.User.Identity is ClaimsIdentity identity))
                    {
                        return null;
                    };
                    var key = identity.FindFirstValue("Key");
                    if (key != null)
                    {
                        currentUser = AbsenceSoft.Data.Security.User.Repository.GetById(key);
                        context.Items[CURRENT_USER_CONTEXT_KEY] = currentUser;
                    }
                }
            }
            catch (NullReferenceException)
            {
                currentUser = null;
            }
            catch (Exception ex)
            {
                Log.Error("Error getting current user from context", ex);
                currentUser = null;
            }
            return currentUser;
        }

        private const string CURRENT_CUSTOMER_CONTEXT_KEY = "__customer";

        public static Customer Customer()
        {
            if (HttpContext.Current != null && HttpContext.Current.Items != null)
            {
                Customer customer = HttpContext.Current.Items[CURRENT_CUSTOMER_CONTEXT_KEY] as Customer;
                if (customer != null)
                    return customer;
            }

            User currentUser = Current.User();
            if (currentUser != null && currentUser.Customer != null)
            {
                Customer(currentUser.Customer);
                return currentUser.Customer;
            }

            return null;
        }

        public static void Customer(Customer customer)
        {
            if (HttpContext.Current != null && HttpContext.Current.Items != null)
            {
                HttpContext.Current.Items[CURRENT_CUSTOMER_CONTEXT_KEY] = customer;
            }
        }


        private const string CURRENT_EMPLOYER_CONTEXT_KEY = "__current_employer__";
        private const string CURRENT_EMPLOYER_ID_CONTEXT_KEY = "__current_employer_id__";

        /// <summary>
        /// Gets the current Employer from the context, if it is set or valid.
        /// </summary>
        public static Employer Employer()
        {
            Employer cur = null;
            try
            {
                HttpContext context = HttpContext.Current;
                if (context != null && context.Items != null && context.Request != null && context.Request.RequestContext != null && context.Request.RequestContext.RouteData != null)
                {
                    cur = context.Items[CURRENT_EMPLOYER_CONTEXT_KEY] as Employer;
                    if (cur != null)
                        return cur;

                    User currentUser = Current.User();
                    RouteValueDictionary routeValues = context.Request.RequestContext.RouteData.Values;
                    List<string> propertiesToCheck = new List<string>()
                    {
                        "employerId", "employeeId", "caseId", "communicationId", "todoItemId", "employeeNoteId", "caseNoteId", "fileId", "eligibilityUploadId", "contactId", "jobId"
                    };


                    if (context.Request.IsAjax())
                    {
                        var bodyStream = new StreamReader(context.Request.InputStream);
                        bodyStream.BaseStream.Seek(0, SeekOrigin.Begin);
                        string bodyText = bodyStream.ReadToEnd();
                        if (bodyText.StartsWith("{"))
                        {
                            ListCriteria criteria = JsonConvert.DeserializeObject<ListCriteria>(bodyText);
                            if (criteria["EmployerId"] != null)
                                cur = AbsenceSoft.Data.Customers.Employer.GetById((string)criteria["EmployerId"]);
                        }
                        else
                        {
                            var queryString = HttpUtility.ParseQueryString(bodyText);
                            var employerIds = queryString.GetValues("EmployerId");
                            if (employerIds != null && employerIds.Any())
                            {
                                cur = AbsenceSoft.Data.Customers.Employer.GetById(employerIds.First());
                            }
                        }
                    }
                    else if (routeValues.Count > 0)
                    {
                        foreach (var property in propertiesToCheck)
                        {
                            if (routeValues.ContainsKey(property))
                            {
                                cur = GetEmployerByRouteData(property, routeValues);
                            }
                        }
                    }
                    // Set the current employer if request is coming from ESS portal
                    if (cur == null && currentUser?.UserType == Data.Enums.UserType.SelfService && Data.Security.User.IsEmployeeSelfServicePortal)
                    {
                        cur = AbsenceSoft.Data.Customers.Employer.GetById(currentUser.EmployerId);
                    }

                    context.Items[CURRENT_EMPLOYER_CONTEXT_KEY] = cur;
                }
            }
            catch (NullReferenceException)
            {
                cur = null;
            }
            catch (Exception ex)
            {
                Log.Error("Error getting current employer from context", ex);
                cur = null;
            }
            return cur;
        }

        private static Employer GetEmployerByRouteData(string property, RouteValueDictionary routeValues)
        {
            var value = routeValues[property];
            if (value != null && value is string)
            {
                string id = value.ToString();
                switch (property)
                {
                    case "employerId":
                        return AbsenceSoft.Data.Customers.Employer.GetById(id);
                    case "employeeId":
                        var employee = Employee.GetById(id);
                        if (employee != null)
                            return employee.Employer;

                        break;
                    case "caseId":
                        var theCase = Case.GetById(id);
                        if (theCase != null)
                            return theCase.Employer;

                        break;
                    case "communicationId":
                        var communication = Communication.GetById(id);
                        if (communication != null)
                            return communication.Employer;

                        break;
                    case "todoItemId":
                        var todo = ToDoItem.GetById(id);
                        if (todo != null)
                            return todo.Employer;

                        break;
                    case "employeeNoteId":
                        var employeeNote = EmployeeNote.GetById(id);
                        if (employeeNote != null)
                            return employeeNote.Employer;

                        break;
                    case "caseNoteId":
                        var caseNote = CaseNote.GetById(id);
                        if (caseNote != null)
                            return caseNote.Employer;

                        break;
                    case "fileId":
                    case "eligibilityUploadId":
                        var file = EligibilityUpload.GetById(id);
                        if (file != null)
                            return file.Employer;

                        break;
                    case "contactId":
                        var employeeContact = EmployeeContact.GetById(id);
                        if (employeeContact != null)
                            return employeeContact.Employer;

                        break;
                    case "jobId":
                        var job = Job.GetById(id);
                        if (job != null)
                            return job.Employer;

                        break;
                    default:
                        break;
                }
            }

            return null;
        }

        /// <summary>
        /// Sets the current employer
        /// </summary>
        /// <param name="e"></param>
        public static void Employer(Employer e)
        {
            HttpContext context = HttpContext.Current;
            if (context != null && context.Items != null && context.Request != null && context.Request.RequestContext != null && context.Request.RequestContext.RouteData != null)
                context.Items[CURRENT_EMPLOYER_CONTEXT_KEY] = e;
        }

        public static void Employer(string employerId)
        {
            Current.Employer(Data.Customers.Employer.GetById(employerId));
        }

    }
}
