﻿using System;
using System.Web;

namespace AbsenceSoft.Web.HttpModules
{
    /// <summary>
    /// Removes the dirty little server response header from the pipeline.
    /// </summary>
    public class RemoveServerHeaderModule : IHttpModule
    {
        /// <summary>
        /// Initializes a module and prepares it to handle requests.
        /// </summary>
        /// <param name="context">An <see cref="T:System.Web.HttpApplication" /> that provides access to the methods, properties, and events common to all application objects within an ASP.NET application</param>
        public void Init(HttpApplication context)
        {
            if (context != null)
                context.BeginRequest += app_BeginRequest;
        }

        /// <summary>
        /// Handles the BeginRequest event of the app control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void app_BeginRequest(object sender, EventArgs e)
        {
            if (HttpContext.Current != null && HttpContext.Current.Response != null)
                HttpContext.Current.Response.Headers.Remove("Server");
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the module that implements <see cref="T:System.Web.IHttpModule" />.
        /// </summary>
        public void Dispose() { }
    }
}
