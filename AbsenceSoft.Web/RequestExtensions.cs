﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace AbsenceSoft.Web
{
    public static class RequestExtensions
    {
        /// <summary>
        /// Determines whether this request is an AJAX request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public static bool IsAjax(this HttpRequestBase request)
        {
            if (request == null) return false;

            if (request.Headers != null && request.Headers["X-Requested-With"] == "XMLHttpRequest")
                return true;
            if (request["X-Requested-With"] == "XMLHttpRequest")
                return true;
            if (request.AcceptTypes != null && request.AcceptTypes.Any(t => t.ToLowerInvariant().Contains("json")))
                return true;
            if (!string.IsNullOrWhiteSpace(request.ContentType) && request.ContentType.ToLowerInvariant().Contains("json"))
                return true;

            return false;
        }

        /// <summary>
        /// Determines whether this request is an AJAX request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public static bool IsAjax(this HttpRequest request)
        {
            if (request == null) return false;

            if (request.Headers != null && request.Headers.AllKeys.Contains("X-Requested-With") && request.Headers["X-Requested-With"] == "XMLHttpRequest")
                return true;
            if (request["X-Requested-With"] == "XMLHttpRequest")
                return true;
            if (request.AcceptTypes != null && request.AcceptTypes.Any(t => t != null && t.ToLowerInvariant().Contains("json")))
                return true;
            if (request.ContentType != null && request.ContentType.ToLowerInvariant().Contains("json"))
                return true;

            return false;
        }

        /// <summary>
        /// Determines whether this instance is secure/SSL/TLS.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public static bool IsSsl(this HttpRequest request)
        {
            if (request.IsSecureConnection)
                return true;
            if (request.Headers.HasKeys())
                if (request.Headers.AllKeys.Contains("X-Forwarded-Proto", StringComparer.OrdinalIgnoreCase))
                    return string.Equals(request.Headers["X-Forwarded-Proto"], "https", StringComparison.OrdinalIgnoreCase);
            return false;
        }

        /// <summary>
        /// Determines whether the specified request is a health-check request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public static bool IsHealthCheck(this HttpRequest request)
        {
            return request.Url.ToString().ToLower().EndsWith("&#47;healthcheck");
        }

        /// <summary>
        /// Gets the specified port of the request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public static int Port(this HttpRequest request)
        {
            string awsPort = null;
            if (request.Headers.HasKeys())
                if (request.Headers.AllKeys.Contains("X-Forwarded-Port", StringComparer.OrdinalIgnoreCase))
                    awsPort = request.Headers["X-Forwarded-Port"];
            int port;
            if (!string.IsNullOrWhiteSpace(awsPort) && int.TryParse(awsPort, out port))
                return port;
            return request.Url.Port;
        }

        /// <summary>
        /// Checks whether a Url is absolute or not
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static bool IsAbsoluteUrl(string url)
        {
            return Uri.IsWellFormedUriString(url, UriKind.Absolute);
        }

        /// <summary>
        /// Returns the relative url from absolute uri
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string GetRelativeUrl(string url)
        {
            if (!string.IsNullOrWhiteSpace(url))
            {
                try
                {
                    Uri result;
                    if (IsAbsoluteUrl(url))
                    {
                        Uri.TryCreate(url, UriKind.Absolute, out result);
                    }
                    else
                    {
                        Uri.TryCreate(url, UriKind.Relative, out result);
                    }
                    return result.PathAndQuery;
                }
                catch
                {
                    //just catching the exception for error
                    //Exception if for hacking attempt without proper uri format. example: google.com
                }
            }
            return "";
        }

        /// <summary>
        /// Extension method for request base to get relative url
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string GetRelativeReturnUrl(this HttpRequestBase request)
        {
            var url = request.QueryString["ReturnUrl"];
            return GetRelativeUrl(url);
        }
    }
}