﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Customers;
using System;
using System.Collections;
using System.Web;
using System.Web.Mvc;
using System.Linq;
using AbsenceSoft.Web;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using System.Collections.Generic;

namespace AbsenceSoft
{
    /// <summary>
    /// The AbsenceSoft base customer web view base view renderer for Razor syntax views.
    /// </summary>
    /// <typeparam name="TModel">The type of the model.</typeparam>
    public class CustomerWebViewBase<TModel> : WebViewPage<TModel>
    {
        /// <summary>
        /// The synchronize cache lock object instance
        /// </summary>
        private static object syncCache = new object();
        /// <summary>
        /// The synchronize cache add lock object instance
        /// </summary>
        private static object syncCacheAdd = new object();

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public string CustomerId
        {
            get
            {
                if (CurrentCustomer != null)
                    return CurrentCustomer.Id;

                return null;
            }
        }

        /// <summary>
        /// Gets or sets the employer identifier.
        /// </summary>
        /// <value>
        /// The employer identifier.
        /// </value>
        public string EmployerId
        {
            get
            {
                if (CurrentEmployer != null)
                    return CurrentEmployer.Id;

                return null;
            }
        }

        private string _CaseId { get; set; }
        private string CaseId
        {
            get
            {
                return _CaseId;
            }
            set
            {
                _CaseId = value;
                Case = null;
            }
        }
        private Case _Case { get; set; }
        private Case Case
        {
            set
            {
                if (value != null)
                    CaseId = value.Id;

                _Case = value;
            }
            get
            {
                if (_Case == null)
                    _Case = Case.GetById(CaseId);

                return _Case;
            }
        }

        private string _EmployeeId { get; set; }
        private string EmployeeId
        {
            get
            {
                return _EmployeeId;
            }
            set
            {
                Employee = null;
                _EmployeeId = value;
            }
        }
        private Employee _Employee { get; set; }
        private Employee Employee
        {
            set
            {
                if (value != null)
                    EmployeeId = value.Id;

                _Employee = value;
            }
            get
            {
                if (_Employee == null)
                    _Employee = Employee.GetById(EmployeeId);

                return _Employee;
            }
        }

        /// <summary>
        /// Gets or sets the current user.
        /// </summary>
        /// <value>
        /// The current user.
        /// </value>
        public AbsenceSoft.Data.Security.User CurrentUser { get; set; }

        /// <summary>
        /// The current customer
        /// </summary>
        public Customer CurrentCustomer { get; set; }

        /// <summary>
        /// The current employer
        /// </summary>
        public Employer CurrentEmployer { get; set; }

        /// <summary>
        /// Initializes the current page.
        /// </summary>
        protected override void InitializePage()
        {
            CurrentEmployer = Current.Employer();
            CurrentCustomer = Current.Customer();
            CurrentUser = Current.User() ?? AbsenceSoft.Data.Security.User.System;
            base.InitializePage();
        }

        private Hashtable LookupContent { get; set; }

        /// <summary>
        /// ts the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="defaultContent">The default content.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <returns></returns>
        public virtual HtmlString T(string key, string defaultContent, string employerId, string caseId, string employeeId)
        {
            employerId = employerId ?? EmployerId;
            if (caseId != CaseId)
                CaseId = caseId;

            if (employeeId != EmployeeId)
                EmployeeId = employeeId;


            using (ContentService svc = new ContentService(CustomerId, employerId, CurrentUser))
            {
                if (LookupContent == null)
                    LookupContent = svc.GetAllContent();

                string text = svc.Get(LookupContent, key, UICulture);
                text = svc.PerformTextMerge(text, Case, Employee);
                return new HtmlString(text ?? defaultContent ?? "");
            }
        }

        public virtual HtmlString T(string key)
        {
            return this.T(key, null, null, null, null);
        }

        public virtual HtmlString T(string key, string defaultContent)
        {
            return this.T(key, defaultContent, null, null, null);
        }

        public virtual HtmlString T(string key, string defaultContent, string employerId)
        {
            return this.T(key, defaultContent, employerId, null, null);
        }

        public virtual HtmlString T(string key, string defaultContent, string employerId, string caseId)
        {
            return this.T(key, defaultContent, employerId, caseId, null);
        }

        /// <summary>
        /// Currents the customer has feature.
        /// </summary>
        /// <param name="feature">The feature.</param>
        /// <returns></returns>
        public bool CurrentCustomerHasFeature(Feature feature)
        {
            using (var adminService = new AdministrationService(CurrentUser))
            {
                return adminService.CustomerHasFeature(feature);
            }
        }

        /// <summary>
        /// Checks if the current customer has a list of features
        /// </summary>
        /// <param name="features"></param>
        /// <returns></returns>
        public bool CurrentCustomerHasFeatures(params Feature[] features)
        {
            bool hasFeatures = true;
            using (var adminService = new AdministrationService(CurrentUser))
            {
                foreach (var feature in features)
                {
                    hasFeatures = hasFeatures && adminService.CustomerHasFeature(feature);

                    // If they don't have one feature, it doesn't matter if they have the rest
                    if (!hasFeatures)
                        break;
                }
            }

            return hasFeatures;
        }

        /// <summary>
        /// Checks that a customer has at least one feature in a passed in list
        /// </summary>
        /// <param name="features"></param>
        /// <returns></returns>
        public bool CurrentCustomerHasAnyFeature(params Feature[] features)
        {
            bool hasAFeature = false;
            using (var adminService = new AdministrationService(CurrentUser))
            {
                foreach (var feature in features)
                {
                    hasAFeature = adminService.CustomerHasFeature(feature);

                    // If we found a feature, it doesn't matter if they don't have any of the rest
                    if (hasAFeature)
                        break;
                }
            }

            return hasAFeature;
        }

        /// <summary>
        /// Returns whether a user has the specific permission
        /// Preferred way of calling it
        /// </summary>
        /// <param name="permission"></param>
        /// <returns></returns>
        public bool CurrentUserHasPermission(Permission permission)
        {
            using (var adminService = new AdministrationService(CurrentUser))
            {
                return adminService.UserHasPermission(permission);
            }
        }

        public List<string> GetCurrentUserPermissionList()
        {
            using (var adminService = new AdministrationService(CurrentUser))
            {
                return adminService.GetUserPermissionsList();
            }
        }

        public bool CurrentUserHasPermissionFromList(Permission permission, List<string> permissionLis)
        {
            using (var adminService = new AdministrationService(CurrentUser))
            {
                return adminService.UserHasPermissionFromList(permission, permissionLis);
            }
        }

        /// <summary>
        /// Returns whether the user has the permission.
        /// Prefer using the version that accepts a permission class, as that will avoid having issues with typos
        /// </summary>
        /// <param name="permission"></param>
        /// <returns></returns>
        public bool CurrentUserHasPermission(string permission)
        {
            return AbsenceSoft.Data.Security.User.Permissions.ProjectedPermissions.Contains(permission);
        }

        public bool HasFeatureAndPermission(Feature feature, string permission)
        {
            return CurrentCustomerHasFeature(feature) && CurrentUserHasPermission(permission);
        }

        public bool HasFeatureAndPermission(Feature feature, Permission permission)
        {
            return CurrentCustomerHasFeature(feature) && CurrentUserHasPermission(permission);
        }

        public bool HasFeatureAndPermissionFromList(Feature feature, Permission permission, List<string> permissions)
        {
            return CurrentCustomerHasFeature(feature) && CurrentUserHasPermissionFromList(permission, permissions);
        }


        /// <summary>
        /// Executes this instance.
        /// </summary>
        public override void Execute() { /*NoOp*/ }
    }

    /// <summary>
    /// The AbsenceSoft base customer web view base view renderer for Razor syntax views.
    /// </summary>
    public class CustomerWebViewBase : CustomerWebViewBase<object> { }
}