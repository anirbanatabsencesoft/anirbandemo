﻿using AbsenceSoft.Data.Customers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Web.ViewRendering
{
    public class CustomerViewEngine : RazorViewEngine
    {
        public CustomerViewEngine()
            : base()
        {
            AreaViewLocationFormats = new[] {
                "~/Areas/{2}/Customers/%customerId%/Views/{1}/{0}.cshtml",
	            "~/Areas/{2}/Views/{1}/{0}.cshtml",
	            "~/Areas/{2}/Views/{1}/{0}.vbhtml",
	            "~/Areas/{2}/Views/Shared/{0}.cshtml",
	            "~/Areas/{2}/Views/Shared/{0}.vbhtml"
	        };

            AreaMasterLocationFormats = new[] {
                "~/Areas/{2}/Customers/%customerId%/Views/{1}/{0}.cshtml",
	            "~/Areas/{2}/Views/{1}/{0}.cshtml",
	            "~/Areas/{2}/Views/{1}/{0}.vbhtml",
	            "~/Areas/{2}/Views/Shared/{0}.cshtml",
	            "~/Areas/{2}/Views/Shared/{0}.vbhtml"
	        };

            AreaPartialViewLocationFormats = new[] {
                "~/Areas/{2}/Customers/%customerId%/Views/{1}/{0}.cshtml",
	            "~/Areas/{2}/Views/{1}/{0}.cshtml",
	            "~/Areas/{2}/Views/{1}/{0}.vbhtml",
	            "~/Areas/{2}/Views/Shared/{0}.cshtml",
	            "~/Areas/{2}/Views/Shared/{0}.vbhtml"
	        };

            ViewLocationFormats = new[] {
                "~/Customers/%customerId%/Views/{1}/{0}.cshtml",
	            "~/Views/{1}/{0}.cshtml",
	            "~/Views/{1}/{0}.vbhtml",
	            "~/Views/Shared/{0}.cshtml",
	            "~/Views/Shared/{0}.vbhtml"
	        };

            MasterLocationFormats = new[] {
                "~/Customers/%customerId%/Views/{1}/{0}.cshtml",
	            "~/Views/{1}/{0}.cshtml",
	            "~/Views/{1}/{0}.vbhtml",
	            "~/Views/Shared/{0}.cshtml",
	            "~/Views/Shared/{0}.vbhtml"
	        };

            PartialViewLocationFormats = new[] {
                "~/Customers/%customerId%/Views/{1}/{0}.cshtml",
	            "~/Views/{1}/{0}.cshtml",
	            "~/Views/{1}/{0}.vbhtml",
	            "~/Views/Shared/{0}.cshtml",
	            "~/Views/Shared/{0}.vbhtml"
	        };
        }

        protected override IView CreatePartialView(ControllerContext controllerContext, string partialPath)
        {
            string customerId = Customer.Current != null ? Customer.Current.Id : null;
            return base.CreatePartialView(controllerContext, partialPath.Replace("%customerId%", customerId));
        }

        protected override IView CreateView(ControllerContext controllerContext, string viewPath, string masterPath)
        {
            string customerId = Customer.Current != null ? Customer.Current.Id : null;
            Debug.WriteLine(string.Format("customerId: {0}", customerId));
            return base.CreateView(controllerContext, viewPath.Replace("%customerId%", customerId), masterPath.Replace("%customerId%", customerId));
        }

        protected override bool FileExists(ControllerContext controllerContext, string virtualPath)
        {
            string customerId = Customer.Current != null ? Customer.Current.Id : null;
            return base.FileExists(controllerContext, virtualPath.Replace("%customerId%", customerId));
        }
    }
}