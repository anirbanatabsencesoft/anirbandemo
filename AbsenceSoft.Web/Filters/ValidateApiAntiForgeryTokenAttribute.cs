﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace AbsenceSoft.Web.Filters
{
    /// <summary>
    /// Validate the Antiforgery token passed from JS
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class ValidateApiAntiForgeryTokenAttribute : FilterAttribute, IAuthorizationFilter
    {
        /// <summary>
        /// Override the method to validate the token passed from JS to API
        /// </summary>
        /// <param name="actionContext"></param>
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }

            var httpContext = filterContext.HttpContext;
            var cookie = httpContext.Request.Cookies[AntiForgeryConfig.CookieName];
           //AntiForgery.Validate(cookie != null ? cookie.Value : null, httpContext.Request.Headers["X-XSRF-Token"]);
        }

    }
}