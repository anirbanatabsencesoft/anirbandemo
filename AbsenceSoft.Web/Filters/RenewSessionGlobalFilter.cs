﻿using AbsenceSoft.Web.Attributes;
using AT.Logic.Authentication;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Web.Filters
{
    public class RenewSessionGlobalFilter : IActionFilter
    {
        readonly AT.Entities.Authentication.ApplicationType  _appType = AT.Entities.Authentication.ApplicationType.Portal;
        public RenewSessionGlobalFilter(AT.Entities.Authentication.ApplicationType appType)
        {
            _appType = appType;
        }
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            // no action required.
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //checking for secure attribute
            bool hasSecureAttribute = filterContext.ActionDescriptor
                .GetCustomAttributes(typeof(SecureAttribute), false).Any();
            if (hasSecureAttribute && filterContext.ActionDescriptor.ActionName != "RenewSession")
            {
                if (filterContext.HttpContext.User.Identity.IsAuthenticated && filterContext.HttpContext.User.Identity is ClaimsIdentity identity)
                {
                    var token = identity.FindFirstValue("token");
                    if (token != null)
                    {
                        // checks the willExpire
                        RenewToken(token, filterContext.HttpContext).GetAwaiter();

                    }
                }
            }
        }

        private async Task RenewToken(string token, HttpContextBase context)
        {
            JwtSecurityToken jwtToken = new JwtSecurityToken(token);
            var duration = jwtToken.ValidTo.Subtract(jwtToken.ValidFrom);
            if (duration <= TimeSpan.Zero) return;
            var halfWay = duration.TotalMinutes / 2;
            var remaining = jwtToken.ValidTo.Subtract(DateTime.UtcNow);
            if (remaining <= TimeSpan.Zero) return;
            bool willExpire = remaining.TotalMinutes <= halfWay && remaining.TotalMinutes > 2;
            if (willExpire)
            {
                //renews the session
                var SignInManager = new ApplicationSignInManager(context.GetOwinContext().Authentication);
                await SignInManager.SignInRenewAysnc(token, _appType);
            }
        }
    }
}
