﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AbsenceSoft.Web;
using System.Web.Mvc;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Audit;

namespace AbsenceSoft.Web.Filters
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class AuditViewAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// Implements OnActionExecuting to capture routedata information
        /// </summary>
        /// <param name="filterContext">action context.</param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.HttpContext.Trace.Write("(AuditView Filter)Action Executing: " +
                filterContext.ActionDescriptor.ActionName);

            if (!AbsenceSoft.Common.Properties.Settings.Default.Audit)
            {
                base.OnActionExecuting(filterContext);
                return;
            }

            object[] customAttributes = filterContext.Controller.GetType().GetCustomAttributes(typeof(AuditClassNoAttribute), false);

            if (customAttributes.Length == 0)
            {
                if (filterContext.Controller is AbsenceSoft.Web.Controllers.SharedBaseController)
                {
                    User user = Current.User();

                    if (user != null && filterContext.HttpContext.Request.HttpMethod != "HEAD")
                    {
                        new PagesViewHistory()
                        {
                            CreatedBy = user,
                            CustomerId = user.CustomerId,
                            EmployerId = user.EmployerId,
                            UserName = user.DisplayName,
                            Url = filterContext.HttpContext.Request.Url.ToString(),
                            RouteData = filterContext.HttpContext.Request.RequestContext.RouteData.Values.ToDictionary(x => x.Key, x => x.Value),
                            HttpMethod = filterContext.HttpContext.Request.HttpMethod,
                            Email = user.Email
                        }.Save();
                    }
                }
            }

            base.OnActionExecuting(filterContext);
        }

        /// <summary>
        /// Overrides OnActionExecuted function
        /// </summary>
        /// <param name="filterContext">action context.</param>
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.Exception != null)
            {
                filterContext.HttpContext.Trace.Write("(AuditView Filter)Exception thrown");
            }

            base.OnActionExecuted(filterContext);
        }
    }
}
