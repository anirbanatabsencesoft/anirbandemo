﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AbsenceSoft.Web.Attributes
{
    public class RemoteRequireHttpsAttribute:RequireHttpsAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsLocal)
                return;

            base.OnAuthorization(filterContext);
        }
    }
}
