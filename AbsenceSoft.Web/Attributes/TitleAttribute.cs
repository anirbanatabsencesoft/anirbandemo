﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Web.Attributes
{
    public class TitleAttribute : ActionFilterAttribute
    {
        public string Title { get; set; }

        public TitleAttribute() { }
        public TitleAttribute(string title) { Title = title; }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            ViewResult result = filterContext.Result as ViewResult;
            if (result != null)
                result.ViewBag.Title = string.Concat(Title, string.IsNullOrWhiteSpace(Title) ? "" : " - ", "AbsenceTracker");
            base.OnResultExecuting(filterContext);
        }
    }
}