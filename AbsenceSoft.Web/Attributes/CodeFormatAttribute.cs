﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AbsenceSoft.Web.Attributes
{
    public class CodeFormatAttribute:RegularExpressionAttribute, IClientValidatable
    {
        public CodeFormatAttribute()
            :base(@"[A-Z0-9-_ ]{1,}")
        {
            this.ErrorMessage = "Allowed characters are: A-Z, 0-9, (space), (underscore), and (hyphen)";
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            yield return new ModelClientValidationRegexRule(ErrorMessage, Pattern);
        }
    }
}
