﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace AbsenceSoft.Web.Attributes
{
    public class SecureAttribute : ActionFilterAttribute, IAuthorizationFilter
    {
        /// <summary>
        /// Gets or sets the required permissions.
        /// </summary>
        /// <value>
        /// The required permissions.
        /// </value>
        public List<string> RequiredPermissions { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="SecureAttribute"/> is secure.
        /// </summary>
        /// <value>
        ///   <c>true</c> if secure; otherwise, <c>false</c>.
        /// </value>
        public bool Secure { get; set; }

        /// <summary>
        /// Gets or sets the feature.
        /// </summary>
        /// <value>
        /// The feature.
        /// </value>
        public Feature Feature { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SecureAttribute"/> class.
        /// </summary>
        public SecureAttribute()
        {
            RequiredPermissions = Permission.None;
            Feature = Feature.None;
            Secure = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SecureAttribute"/> class.
        /// </summary>
        /// <param name="permissions">The permissions.</param>
        public SecureAttribute(params string[] permissions) : 
            this(Feature.None, permissions)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SecureAttribute"/> class.
        /// </summary>
        /// <param name="feature">The feature.</param>
        public SecureAttribute(Feature feature) : 
            this(feature, (string[])null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SecureAttribute"/> class.
        /// </summary>
        /// <param name="feature">The feature.</param>
        /// <param name="permissions">The permissions.</param>
        public SecureAttribute(Feature feature, params string[] permissions):this()
        {
            if (permissions == null)
            {
                RequiredPermissions = Permission.None;
            }
            else
            {
                RequiredPermissions = permissions.ToList();
            }
            
            Feature = feature;
            Secure = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SecureAttribute"/> class.
        /// </summary>
        /// <param name="permissions">The permissions.</param>
        public SecureAttribute(params Permission[] permissions) : 
            this(Feature.None, permissions)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SecureAttribute"/> class.
        /// </summary>
        /// <param name="feature">The feature.</param>
        /// <param name="permissions">The permissions.</param>
        public SecureAttribute(Feature feature, params Permission[] permissions) : 
            this(feature, permissions.Select(p => p.Name).ToArray())
        {
            
        }

        /// <summary>
        /// Called when authorization is required.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!Secure)
            {
                return;
            }
                
            var usr = Current.User();
            if (usr == null || usr.IsDeleted)
            {
                filterContext.Result = new HttpUnauthorizedResult();
                return;
            }

            if (!RequiredPermissions.Any() && Feature == Feature.None)
            {
                return;
            }

            if (IsUnauthorized())
            {
                if (filterContext.RequestContext.HttpContext.Request.IsAjax())
                {
                    filterContext.Result = new JsonResult
                    {
                        Data = new { success = false, error = new Exception("You do not have permission to make this change. Please consult with the System Administrator.") },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
                else
                {
                    filterContext.Result = new ViewResult() { ViewName = "Unauthorized" };
                }
                    
            }
        }

        /// <summary>
        /// Determines whether this instance is unauthorized.
        /// </summary>
        /// <returns></returns>
        private bool IsUnauthorized()
        {
            Employer currentEmployer = Current.Employer();
            User currentUser = Current.User();
            if (Feature != Feature.None)
            {
                bool hasFeature = Current.Customer().HasFeature(Feature);
                if (!hasFeature && currentEmployer != null)
                {
                    hasFeature = currentEmployer.HasFeature(Feature);
                }
                    
                if (!hasFeature)
                {
                    return true;
                }
            }

            if (RequiredPermissions.Any())
            {
                var permissions = User.Permissions.GetPermissions(currentUser, currentEmployer?.Id);
                bool doesNotHavePermission = !RequiredPermissions.Any(p => permissions.Contains(p));
                if (doesNotHavePermission)
                {
                    return true;
                }
            }                

            return false;
        }
    }
}