﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Insight.Database;
using Npgsql;
using AT.Data.Notification.Arguments;
using AT.Data.Notification.Base;
using System.Collections.Specialized;
using System.Threading;
using AT.Common.Log;
using System.Transactions;

namespace AT.Data.Notification
{
    public class PostgresDataProvider : BaseDataProvider
    {
        private readonly string UserId;

        public override string Name => "PostgresDataProvider";
        public override string Description => "Postgres Data Provider";

        public PostgresDataProvider()
        {

        }

        /// <summary>
        /// Must share the user id who is accessing the provider
        /// </summary>
        /// <param name="userId"></param>
        public PostgresDataProvider(string userId)
        {
            UserId = userId;
        }

        /// <summary>
        /// Set all the configuration parameters
        /// </summary>
        /// <param name="collection"></param>
        public override void SetConfig(NameValueCollection collection)
        {

        }

        /// <summary>
        /// Save a notification
        /// If Notification Id is present, then this is an update else insert.
        /// On update, delete all recipients and attachments and re-insert them
        /// </summary>
        /// <param name="notification"></param>
        /// <returns></returns>
        public override async Task<AT.Entities.Notification.Notification> Save(SaveNotificationArgs args)
        {
            try
            {
                using (var connection = GetConnection())
                {
                    var notification = connection.Single<AT.Entities.Notification.Notification>("public.fn_save_notification",
                                                            new
                                                            {
                                                                noti_id = args.Notification.Id,
                                                                noti_customer_id = args.Notification.CustomerId,
                                                                noti_customer_object_id = args.Notification.CustomerKey,
                                                                noti_sender = args.Notification.From,
                                                                noti_lookup_notification_type_id = (long)args.Notification.NotificationType,
                                                                noti_created_by_id = args.Notification.CreatedById,
                                                                noti_created_by_object_id = args.Notification.CreatedByKey,
                                                                noti_modified_by_id = args.Notification.ModifiedById,
                                                                noti_modified_by_object_id = args.Notification.ModifiedByKey,
                                                                noti_lookup_message_format_id = (long)args.Notification.MessageFormat,
                                                                noti_lookup_notification_status_id = args.Notification.LookupNotificationStatusId ?? null,
                                                                noti_subject = args.Notification.Subject,
                                                                noti_message = args.Notification.Message,
                                                                noti_message_id = args.Notification.MessageId ?? null,
                                                                noti_notified_on = args.Notification.NotifiedOn ?? null,
                                                                noti_failure_reason = args.Notification.FailureReason ?? null
                                                            },
                                            System.Data.CommandType.StoredProcedure
                                                          );
                    if (args.Recipients != null && args.Recipients.Count > 0)
                    {
                        await SaveRecipients(args.Recipients, notification.Id ?? 0);
                    }
                    if (args.Attachments != null && args.Attachments.Count > 0)
                    {
                        await SaveAttachments(args.Attachments, notification.Id ?? 0);
                    }

                    return await Task.FromResult(notification);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Saves attachment of associated notification
        /// </summary>
        /// <param name="attachments"></param>
        /// <param name="notificationId"></param>
        /// <returns></returns>
        public override async Task<long> SaveAttachments(ICollection<AT.Entities.Notification.NotificationAttachment> attachments, long notificationId)
        {
            using (var connection = GetConnection())
            {
                long count = 0;

                foreach (var attachment in attachments)
                {
                    connection.ExecuteScalar<long>("public.fn_save_notification_attachment",
                                                    new
                                                    {
                                                        noti_id = notificationId,
                                                        file_name = attachment.FileName,
                                                        file_content = attachment.FileContent,
                                                        attachment_header = attachment.AttachmentHeader
                                                    },
                                    System.Data.CommandType.StoredProcedure
                                                    );
                    count++;
                }
                return await Task.FromResult(count);
            }
        }

        /// <summary>
        /// Saves recipient information of associate notification
        /// </summary>
        /// <param name="recipients"></param>
        /// <param name="notificationId"></param>
        /// <returns></returns>
        public override async Task<long> SaveRecipients(ICollection<AT.Entities.Notification.NotificationRecipient> recipients, long notificationId)
        {
            using (var connection = GetConnection())
            {
                long count = 0;

                foreach (var recipient in recipients)
                {
                    connection.ExecuteScalar<long>("public.fn_save_notification_recipient",
                                                    new
                                                    {
                                                        noti_id = notificationId,
                                                        noti_recipient = recipient.RecipientAddress,
                                                        noti_lookup_recipient_type_id = (long)recipient.RecipientType
                                                    },
                                    System.Data.CommandType.StoredProcedure
                                                  );
                    count++;
                }
                return await Task.FromResult(count);
            }
        }

        /// <summary>
        /// Delete a notification.
        /// If Id is null, delete for that customer
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public override async Task<int> DeleteNotification(DeleteNotificationArgs args)
        {
            try
            {
                using (var connection = GetConnection())
                {
                    return await connection.SingleAsync<int>("public.fn_delete_notification", new
                    {
                        noti_id = args.NotificationId,
                        noti_modified_by_id = args.ModifiedById
                    },
                    System.Data.CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                return 0;
            }
        }

        /// <summary>
        /// Search for Notifications based on different parameters which includes
        /// CustomerId, HasAttachment, SubjectContains, MessageContains, 
        /// LookupNotificationStatusId, IncludeArchive
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public override async Task<IList<AT.Entities.Notification.Notification>> GetNotificationList(GetNotificationListArgs arg)
        {
            using (var connection = GetConnection())
            {
                return await connection.QueryAsync<AT.Entities.Notification.Notification>("public.fn_search_notification",
                    new
                    {
                        notification_id = arg.Id,
                        noti_customer_id = arg.CustomerId,
                        noti_sender = arg.From,
                        noti_recipient = arg.To,
                        noti_hasattachment = arg.HasAttachment,
                        noti_subject_contains = arg.SubjectContains,
                        noti_message_contains = arg.MessageContains,
                        noti_include_archive = arg.IncludeArchive,
                        noti_lookup_notification_status_id = arg.LookupNotificationStatusId,
                        noti_customer_object_id = arg.CustomerKey
                    },
                    System.Data.CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Update the status of notification
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public override async Task<int> UpdateStatus(UpdateNotificationStatusArgs arg)
        {
            using (var connection = GetConnection())
            {
                return await connection.ExecuteAsync("public.fn_save_notification",
                                new
                                {
                                    noti_id = arg.NotificationId,
                                    noti_customer_id = arg.CustomerId,
                                    noti_modified_by_id = arg.ModifiedById,
                                    noti_notified_on = arg.NotifiedOn,
                                    noti_failure_reason = arg.FailureReason,
                                    noti_lookup_notification_status_id = arg.LookupNotificationStatusId
                                },
                                System.Data.CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Get the connection for a customer. If not available then default.
        /// </summary>
        /// <returns></returns>
        private NpgsqlConnection GetConnection()
        {
            return AT.Data.Core.PostgresConnection.CreateCustomerConnection(UserId);
        }
    }
}