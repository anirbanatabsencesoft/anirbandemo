﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Common.Core;
using AT.Core.Entities;
using System.Net.Mail;

namespace AT.Data.Notification.Arguments
{
    public class SaveNotificationArgs : BaseArgument
    {
        /// <summary>
        /// The identifier to be used under Message BUS
        /// </summary>
        public override string Identifier { get { return "SAVE_NOTIFICATION"; } }

        /// <summary>
        /// The notification object to be saved
        /// </summary>
        public AT.Entities.Notification.Notification Notification { get; set; }

        /// <summary>
        /// Attachment object to be associated
        /// </summary>
        public List<AT.Entities.Notification.NotificationAttachment> Attachments { get; set; }

        /// <summary>
        /// Recipient list who would receive associated notification
        /// </summary>
        public List<AT.Entities.Notification.NotificationRecipient> Recipients { get; set; }

        /// <summary>
        /// MailMessage can be used to send embeded images
        /// </summary>
        public MailMessage MailMessage { get; set; }
    }
}