﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Common.Core;

namespace AT.Data.Notification.Arguments
{
    public class DeleteNotificationArgs : BaseArgument
    {
        /// <summary>
        /// The identifier required if used under any BUS
        /// </summary>
        public override string Identifier { get { return "DELETE_NOTIFICATION"; } }

        /// <summary>
        /// The Postgres Key for Customer Id
        /// </summary>
        public long? CustomerId { get; set; }

        /// <summary>
        /// The customer object id
        /// </summary>
        public long? CustomerObjectId { get; set; }

        /// <summary>
        /// Set this if a  single notification is to be deleted.
        /// </summary>
        public long? NotificationId { get; set; }

        /// <summary>
        /// Set this with Id of user who is modifying the entry
        /// </summary>
        public long ModifiedById { get; set; }
    }
}
