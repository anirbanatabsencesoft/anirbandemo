﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using AT.Common.Core;
using AT.Core.Entities;
using AT.Common.Core.NotificationEnums;

namespace AT.Data.Notification.Arguments
{
    public class UpdateNotificationStatusArgs : BaseArgument
    {
        public override string Identifier { get { return "UPDATE_NOTIFICATION"; } }

        /// <summary>
        /// The Id of the notification which we want to update
        /// </summary>
        [Required]
        public long NotificationId { get; set; }

        /// <summary>
        /// The CustomerId attached to Notification
        /// </summary>
        [Required]
        public long CustomerId { get; set; }

        /// <summary>
        /// The user who Modified the Notification
        /// </summary>
        public long ModifiedById{ get; set; }

        /// <summary>
        /// The date time when notified.
        /// </summary>
        public DateTime? NotifiedOn { get; set; }

        /// <summary>
        /// The message/reference id from provider service
        /// </summary>
        public string MessageId { get; set; }

        /// <summary>
        /// Set this if failed.
        /// </summary>
        public DateTime? FailureOn { get; set; }

        /// <summary>
        /// Set this with failure reason 
        /// </summary>
        public string FailureReason { get; set; }

        /// <summary>
        /// The Status id of the current notification
        /// </summary>
        public int LookupNotificationStatusId { get; set; }

        /// <summary>
        /// Notification status of the notification
        /// </summary>
        public NotificationStatus? Status
        {
            get
            {
                if (NotifiedOn.HasValue)
                {
                    return NotificationStatus.Sent;
                }
                return null;
            }
        }
    }
}
