using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.ComponentModel.DataAnnotations;
using AT.Common.Core;

namespace AT.Data.Notification.Arguments {
	public class GetNotificationListArgs : BaseArgument {
        
		public override string Identifier{ get { return "SEARCH_NOTIFICATION"; } }

        /// <summary>
        /// Set this if only one message is required against it's Id
        /// </summary>
        public long? Id { get; set; }

        /// <summary>
        /// Search for the customer. This should be set always.
        /// </summary>
        public long? CustomerId { get; set; }

        /// <summary>
        /// Set the customer's Mongo Object Id here
        /// </summary>
        public string CustomerKey { get; set; }

        /// <summary>
        /// Find the notiifcations for a from address
        /// </summary>
        public string From { get; set; }

        /// <summary>
        /// Find notification that is to an address.
        /// </summary>
        public string To { get; set; }

        /// <summary>
        /// This is email only. Find notiifcations that has attachment
        /// </summary>
        public bool? HasAttachment { get; set; }

        /// <summary>
        /// Search for notification where subject contains the keyword
        /// </summary>
		public string SubjectContains{get;set;}

        /// <summary>
        /// Set the property to search the Notification for different status id
        /// </summary>
        public int? LookupNotificationStatusId { get; set; }

        /// <summary>
        /// Search notification for message that contains the keyword
        /// </summary>
		public string MessageContains{get;set;}

        /// <summary>
        /// Include archive notifications into the search criteria
        /// </summary>
		public bool? IncludeArchive{get;set;}
        
	}
}