using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Configuration;
using System.Web.Configuration;

namespace AT.Data.Notification.Base
{
	/// <summary>
	/// This is the starting point for a notification provider as this is going to set
	/// all providers.
	/// </summary>
	public class DataProviderManager {
        private static string DATA_PROVIDER_SECTION = "NotificationDataProviders";
        private static BaseDataProvider DefaultProvider;
		private static DataProviderCollection ProviderCollection;
		private static ProviderSettingsCollection ProviderSettingsColl;
        
		/// <summary>
		/// Static constructor for getting all providers in a queue
		/// </summary>
		static DataProviderManager(){

			Initialize();
		}

		/// <summary>
		/// Initialize the configuration section and set all static private variables.
		/// </summary>
		private static void Initialize()
        {
            //get the configuration
            DataProviderConfiguration configSection = (DataProviderConfiguration)ConfigurationManager.GetSection(DATA_PROVIDER_SECTION);

            //throw exception if configuration section is not present
            if (configSection == null)
                throw new ConfigurationErrorsException("Notification provider section is not set in the config.");

            //get provider collections
            ProviderCollection = new DataProviderCollection();
            ProvidersHelper.InstantiateProviders(configSection.Providers, ProviderCollection, typeof(BaseDataProvider));

            //get provider settings
            ProviderSettingsColl = configSection.Providers;

            //if there is no default provider, throw excpetion
            if (ProviderCollection[configSection.DefaultProvider] == null)
                throw new ConfigurationErrorsException("Default provider is not set.");

            //get the default provider
            DefaultProvider = ProviderCollection[configSection.DefaultProvider];
            var defaultSettings = ProviderSettingsColl[configSection.DefaultProvider];
            
            DefaultProvider.SetConfig(defaultSettings.Parameters);
        }

		/// <summary>
		/// Returns the Default Instance of the provider.
		/// </summary>
		public static BaseDataProvider Default{get { return DefaultProvider; }}

		public static DataProviderCollection Providers{ get { return ProviderCollection; } }

		public System.Configuration.ProviderSettingsCollection ProviderSettings{ get { return ProviderSettingsColl; } }

	}
}