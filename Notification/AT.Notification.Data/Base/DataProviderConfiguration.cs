using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Configuration;

namespace AT.Data.Notification.Base
{
	public class DataProviderConfiguration : System.Configuration.ConfigurationSection {

		[ConfigurationProperty("providers")]
		public System.Configuration.ProviderSettingsCollection Providers
        {
            get
            {
                return (ProviderSettingsCollection)base["providers"];
            }
        }

		[ConfigurationProperty("default", DefaultValue = "EmailProvider")]
		public string DefaultProvider
        {
            get
            {
                return base["default"] as string;
            }
        }
	}
}