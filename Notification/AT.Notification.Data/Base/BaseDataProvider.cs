using System.Collections.Generic;
using System.Collections.Specialized;
using AT.Data.Notification;
using AT.Data.Notification.Arguments;
using System.Threading.Tasks;

namespace AT.Data.Notification.Base
{
    public abstract class BaseDataProvider : System.Configuration.Provider.ProviderBase {

        /// <summary>
        /// This method to be overriden for setting up the config parameters
        /// </summary>
        /// <param name="collection"></param>
        public abstract void SetConfig(NameValueCollection collection);

        /// <summary>
        /// Insert a notification and it's related objects
        /// </summary>
        /// <param name="notification"></param>
        public abstract Task<AT.Entities.Notification.Notification> Save(SaveNotificationArgs args);

        /// <summary>
        /// Saves the attachment of associated notification
        /// </summary>
        /// <param name="attachment"></param>
        /// <returns></returns>
        public abstract Task<long> SaveAttachments(ICollection<AT.Entities.Notification.NotificationAttachment> args, long NotificationId);

        /// <summary>
        /// Saves the recipient information for associated notification
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public abstract Task<long> SaveRecipients(ICollection<AT.Entities.Notification.NotificationRecipient> args, long NotificationId);

        /// <summary>
        /// Search the notification and return data
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public abstract Task<IList<AT.Entities.Notification.Notification>> GetNotificationList(GetNotificationListArgs args);

        /// <summary>
        /// Deletes the notification based on the ID. If Id is null delete all notification for that customer id
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public abstract Task<int> DeleteNotification(DeleteNotificationArgs args);

        /// <summary>
        /// Update the status on notificaion to success if notified on is set 
        /// Else, increment the retries and set last retried.
        /// Message Id is the ID if can be retrieved from SMTP or other providers
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public abstract Task<int> UpdateStatus(UpdateNotificationStatusArgs args);

    }
}