using Amazon;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using AT.Common.Core;
using AT.Common.Core.NotificationEnums;
using AT.Common.Log;
using AT.Data.Notification.Arguments;
using AT.Logic.Notification;
using AT.Provider.Notification.Base;
using AT.Provider.Resource.Manager;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AT.Provider.Notification
{
    public class EmailProvider : BaseNotificationProvider
    {
        NotificationLogic notificationLogic;

        /// <summary>
        /// Gets or sets the aws access identifier.
        /// </summary>
        /// <value>
        /// The aws access identifier.
        /// </value>
        private string AwsAccessId { get; set; }

        /// <summary>
        /// Gets or sets the aws secret key.
        /// </summary>
        /// <value>
        /// The aws secret key.
        /// </value>
        private string AwsSecretKey { get; set; }

        /// <summary>
        /// Gets or sets the aws region.
        /// </summary>
        /// <value>
        /// The aws region.
        /// </value>
        private string AwsRegion { get; set; }

        /// <summary>
        /// Must have a user id to process
        /// </summary>
        public string UserObjectId { get; set; }

        /// <summary>
        /// The postgres Key User Id
        /// </summary>
        public long? UserId { get; set; }

        /// <summary>
        /// Set the Customer id for processing
        /// </summary>
        public long? CustomerObjectId { get; set; }

        /// <summary>
        /// The postgres key for customer id
        /// </summary>
        public long? CustomerId { get; set; }

        /// <summary>
        /// Set all the configuration parameters
        /// </summary>
        /// <param name="collection"></param>
        public override void SetConfig(NameValueCollection collection)
        {
            AwsAccessId = collection["AwsAccessId"];
            AwsSecretKey = collection["AwsSecretKey"];
            AwsRegion = collection["AwsRegion"];

            if (string.IsNullOrWhiteSpace(AwsAccessId))
            {
                throw new ConfigurationErrorsException("The configuration setting 'AwsAccessId' for the Email Provider was not found or was empty.");
            }

            if (string.IsNullOrWhiteSpace(AwsSecretKey))
            {
                throw new ConfigurationErrorsException("The configuration setting 'AwsSecretKey' for the Email Provider was not found or was empty.");
            }

            if (string.IsNullOrWhiteSpace(AwsRegion))
            {
                AwsRegion = RegionEndpoint.USWest2.SystemName;
            }
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public EmailProvider()
        {
            notificationLogic = new NotificationLogic();
        }

        /// <summary>
        /// Constructor to pass the user id
        /// </summary>
        /// <param name="userId"></param>
        public EmailProvider(string userId)
        {
            UserObjectId = userId;
        }

        /// <summary>
        /// Constructor to set the customer id and the user id
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        public EmailProvider(string userId, long customerId)
        {
            UserObjectId = userId;
            CustomerObjectId = customerId;
        }

        /// <summary>
        /// Constructor to set the User id from Postgres
        /// </summary>
        /// <param name="userId"></param>
        public EmailProvider(long userId)
        {
            UserId = userId;
        }

        /// <summary>
        /// Constructor to set the postgres key for user and customer
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        public EmailProvider(long userId, long customerId)
        {
            UserId = userId;
            CustomerId = customerId;
        }

        /// <summary>
        /// This method will be called to fire/ send all notifications from a background
        /// service.
        /// 1) Call Notification->SearchNotification and pass all arguments as null/default
        /// and get the list of unsent notification.
        /// 2) For each notification call SendMail method.
        /// </summary>
        public override async Task<int> Notify()
        {
            var arg = new GetNotificationListArgs() { CustomerId = CustomerObjectId, LookupNotificationStatusId = null };
            var unsentNotifications = await notificationLogic.GetNotificationList(arg);

            foreach (var notification in unsentNotifications)
            {
                await SendMailAsync(notification);
            }

            return unsentNotifications.Count;
        }

        /// <summary>
        /// Notify a notification using MailMessage
        /// </summary>
        /// <param name="notification"></param>
        /// <param name="mailMessage"></param>
        /// <returns></returns>
        public override Task<Entities.Notification.Notification> Notify(AT.Entities.Notification.Notification notification, MailMessage mailMessage)
        {
            return Task.FromResult(SendMail(notification, mailMessage));
        }

        /// <summary>
        /// Notify a single notification. First Save the notification and on success send
        /// the message using SendMail method.
        /// </summary>
        /// <param name="notification"></param>
        public override async Task<Entities.Notification.Notification> Notify(AT.Entities.Notification.Notification notification, List<LinkedResource> resources = null, MailAddressCollection replyToList = null)
        {
            return await Task.FromResult(SendMail(notification, resources, replyToList));
        }

        /// <summary>
        /// Get the notification from database and notify
        /// </summary>
        /// <param name="notificationId"></param>
        public override async Task<Entities.Notification.Notification> Notify(long notificationId)
        {
            var notification = await notificationLogic.GetNotificationList(new GetNotificationListArgs() { Id = notificationId });
            if (notification.Any())
            {
                return await SendMailAsync(notification[0]);
            }
            return null;
        }

        /// <summary>
        /// Gets an email template text directly from an embedded resource of the logic assembly. This is only
        /// for site-wide email templates such as forgot password, welcome, etc. All other emails (e.g. communications)
        /// should use the CommunicationService in order to get template text.
        /// </summary>
        /// <param name="name">The email template name</param>
        /// <returns>The specified email template text or <c>null</c> if the named resource wasn't found.</returns>
        public string GetEmailTemplate(string name)
        {
            return ResourceProviderManager.Default.Get("Notification", name);
        }

        /// <summary>
        /// Send mail based on a notification
        /// </summary>
        /// <param name="notification">The notification.</param>
        /// <returns></returns>
        private async Task<Entities.Notification.Notification> SendMailAsync(Entities.Notification.Notification notification)
        {
            try
            {
                var config = new AmazonSimpleEmailServiceConfig
                {
                    RegionEndpoint = RegionEndpoint.GetBySystemName(AwsRegion)
                };
                using (var client = AWSClientFactory.CreateAmazonSimpleEmailServiceClient(AwsAccessId, AwsSecretKey, config))
                {
                    var response = await client.SendRawEmailAsync(new SendRawEmailRequest
                    {
                        RawMessage = new RawMessage(GetMessageStream(notification))
                    });

                    notification.MessageId = response.MessageId;
                    notification.NotifiedOn = DateTime.Now;
                    notification.LookupNotificationStatusId = (int)NotificationStatus.Sent;
                }
            }
            catch (Exception ex)
            {
                notification.FailureReason = ex.Message;
                notification.Retries += 1;
                Logger.Error(ex.Message);
            }

            if (notification.Id != null)
            {
                return await new NotificationLogic().Update(new SaveNotificationArgs
                {
                    Notification = notification
                });
            }
            else
            {
                return notification;
            }
        }

        /// <summary>
        /// Send mail based on a notification
        /// </summary>
        /// <param name="notification">The notification.</param>
        /// <returns></returns>
        private Entities.Notification.Notification SendMail(Entities.Notification.Notification notification, List<LinkedResource> resources = null, MailAddressCollection replyToList = null)
        {
            var config = new AmazonSimpleEmailServiceConfig
            {
                RegionEndpoint = RegionEndpoint.GetBySystemName(AwsRegion)
            };

            SendRawEmailResponse response = null;

            using (var client = AWSClientFactory.CreateAmazonSimpleEmailServiceClient(AwsAccessId, AwsSecretKey, config))
            {
                response = client.SendRawEmail(new SendRawEmailRequest
                {
                    RawMessage = new RawMessage(GetMessageStream(notification, resources, replyToList))
                });

                    notification.MessageId = response.MessageId;
                    notification.NotifiedOn = DateTime.Now;
                    notification.LookupNotificationStatusId = (int)NotificationStatus.Sent;
                }

            if (notification.Id != null && response != null)
            {
                return new NotificationLogic().Update(new SaveNotificationArgs
                {
                    Notification = notification
                }).Result;
            }
            else
            {
                return notification;
            }
        }

        /// <summary>
        /// Send mail based on a notification
        /// </summary>
        /// <param name="notification">The notification.</param>
        /// <returns></returns>
        private Entities.Notification.Notification SendMail(Entities.Notification.Notification notification, MailMessage mailMessage)
        {
            try
            {
                var config = new AmazonSimpleEmailServiceConfig
                {
                    RegionEndpoint = RegionEndpoint.GetBySystemName(AwsRegion)
                };
                using (var client = AWSClientFactory.CreateAmazonSimpleEmailServiceClient(AwsAccessId, AwsSecretKey, config))
                {
                    var response = client.SendRawEmail(new SendRawEmailRequest
                    {
                        RawMessage = new RawMessage(GetMessageStream(mailMessage, notification))

                    });
                    notification.MessageId = response.MessageId;
                    notification.NotifiedOn = DateTime.Now;
                    notification.LookupNotificationStatusId = (int)NotificationStatus.Sent;
                }
            }
            catch (Exception ex)
            {
                notification.FailureReason = ex.Message;
                notification.Retries += 1;
                Logger.Error(ex.Message);
            }

            if (notification.Id != null)
            {
                return new NotificationLogic().Update(new SaveNotificationArgs
                {
                    Notification = notification
                }).Result;
            }
            else
            {
                return notification;
            }
        }

        /// <summary>
        /// Generates a message body
        /// </summary>
        /// <param name="notification"></param>
        /// <returns></returns>
        private BodyBuilder GetMessageBody(Entities.Notification.Notification notification, List<LinkedResource> resources = null)
        {
            var body = new BodyBuilder()
            {
                HtmlBody = notification.Message
            };

            if (resources != null && resources.Any())
            {
                foreach (var r in resources)
                {
                    body.LinkedResources.Add(r.ContentId, Utilities.ReadStreamFully(r.ContentStream));
                }
            }

            if (notification.AttachmentList != null && notification.AttachmentList.Count > 0)
            {
                foreach (var attachment in notification.AttachmentList)
                {
                    body.Attachments.Add(attachment.FileName, attachment.FileContent);
                }
            }

            return body;
        }

        /// <summary>
        /// Get the message mime
        /// </summary>
        /// <param name="notification"></param>
        /// <returns></returns>
        private MimeMessage GetMessage(Entities.Notification.Notification notification, List<LinkedResource> resources = null, MailAddressCollection replyToList = null)
        {
            var message = new MimeMessage();
            var delimiters = new char[] { ',', ';' };

            if (!string.IsNullOrWhiteSpace(notification.Recipients))
            {
                var mailAddresses = notification.Recipients.Split(delimiters);

                foreach (var mailAddress in mailAddresses)
                {
                    message.To.Add(new MailboxAddress(mailAddress.Trim(' ')));
                }

                if (notification.CCRecipients != null && notification.CCRecipients.Any())
                {
                    var ccMailAddresses = notification.CCRecipients.Split(delimiters);
                    foreach (var ccMailAddress in ccMailAddresses)
                    {
                        message.Cc.Add(new MailboxAddress(ccMailAddress.Trim(' ')));
                    }
                }
            }
            else if (notification.RecipientList != null && notification.RecipientList.Count > 0)
            {
                foreach (var recipient in notification.RecipientList)
                {
                    var mailAddresses = recipient.RecipientAddress.Split(delimiters);
                    foreach (var mailAddress in mailAddresses)
                    {
                        switch (recipient.RecipientType)
                        {
                            case RecipientType.To:
                                message.To.Add(new MailboxAddress(mailAddress.Trim(' ')));
                                break;
                            case RecipientType.CC:
                                message.Cc.Add(new MailboxAddress(mailAddress.Trim(' ')));
                                break;
                        }
                    }
                }
            }

            if (replyToList != null && replyToList.Any())
            {
                foreach (var replyTo in replyToList)
                {
                    message.ReplyTo.Add(new MailboxAddress(replyTo.DisplayName, replyTo.Address));
                }
            }

            System.Net.Mail.MailAddress a = new System.Net.Mail.MailAddress(notification.From);
            message.From.Add(new MailboxAddress(a.DisplayName, a.Address));

            message.Subject = notification.Subject;
            message.Body = GetMessageBody(notification, resources).ToMessageBody();
            return message;
        }

        /// <summary>
        /// Get the message stream
        /// </summary>
        /// <param name="notification"></param>
        /// <returns></returns>
        private MemoryStream GetMessageStream(Entities.Notification.Notification notification, List<LinkedResource> resources = null, MailAddressCollection replyToList = null)
        {
            var stream = new MemoryStream();
            GetMessage(notification, resources, replyToList).WriteTo(stream);
            return stream;
        }

        private BodyBuilder GetMessageBody(MailMessage mailMessage, Entities.Notification.Notification notification)
        {
            var body = new BodyBuilder()
            {
                HtmlBody = notification.Message
            };

            foreach (var v in mailMessage.AlternateViews)
            {
                foreach (var li in v.LinkedResources)
                    body.LinkedResources.Add(li.ContentId, Utilities.ReadStreamFully(li.ContentStream));
            }

            if (notification.AttachmentList != null && notification.AttachmentList.Count > 0)
            {
                foreach (var attachment in notification.AttachmentList)
                {
                    body.Attachments.Add(attachment.FileName, attachment.FileContent);
                }
            }

            return body;
        }

        private MimeMessage GetMessage(MailMessage mailMessage, Entities.Notification.Notification notification)
        {
            var message = new MimeMessage();

            if (!string.IsNullOrWhiteSpace(notification.Recipients))
            {
                var delimiters = new char[] { ',', ';' };
                var mailAddresses = notification.Recipients.Split(delimiters);

                foreach (var mailAddress in mailAddresses)
                {
                    message.To.Add(new MailboxAddress(mailAddress.Trim(' ')));
                }
            }
            else if (notification.RecipientList != null && notification.RecipientList.Count > 0)
            {
                foreach (var recipient in notification.RecipientList)
                {
                    message.To.Add(new MailboxAddress(recipient.RecipientAddress.Trim(' ')));
                }
            }

            System.Net.Mail.MailAddress a = new System.Net.Mail.MailAddress(notification.From);
            message.From.Add(new MailboxAddress(a.DisplayName, a.Address));

            message.Subject = notification.Subject;
            message.Body = GetMessageBody(mailMessage, notification).ToMessageBody();
            return message;
        }

        /// <summary>
        /// Converts MailMessage information to memory stream
        /// </summary>
        /// <param name="mailMessage"></param>
        /// <returns></returns>
        private MemoryStream GetMessageStream(MailMessage mailMessage, Entities.Notification.Notification notification)
        {
            var stream = new MemoryStream();
            GetMessage(mailMessage, notification).WriteTo(stream);
            return stream;
        }

    }
}