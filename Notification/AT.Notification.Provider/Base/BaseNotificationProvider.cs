using System.Collections.Generic;
using System.Collections.Specialized;
using AT.Data.Notification;
using AT.Data.Notification.Arguments;
using System.Threading.Tasks;
using System.Net.Mail;

namespace AT.Provider.Notification.Base
{
    public abstract class BaseNotificationProvider : System.Configuration.Provider.ProviderBase {
                                
        /// <summary>
        /// This is to set all config parameters
        /// </summary>
        /// <param name="collection"></param>
        public abstract void SetConfig(NameValueCollection collection);
        
        /// <summary>
        /// Fire to notify all notifications for a provider
        /// This is to be called from Services
        /// </summary>
		public abstract Task<int> Notify();
        
		/// <summary>
        /// Notify a notification after save
        /// </summary>
		/// <param name="notification"></param>
		public abstract Task<Entities.Notification.Notification> Notify(AT.Entities.Notification.Notification notification, List<LinkedResource> resources = null, MailAddressCollection replyToList = null);

        /// <summary>
        /// Notify a notification using MailMessage
        /// </summary>
        /// <param name="notification"></param>
        /// <param name="mailMessage"></param>
        /// <returns></returns>
        public abstract Task<Entities.Notification.Notification> Notify(AT.Entities.Notification.Notification notification, MailMessage mailMessage);
        /// <summary>
        /// Get the notification from database and notify
        /// </summary>
        /// <param name="notificationId"></param>
        public abstract Task<Entities.Notification.Notification> Notify(long notificationId);
	}//end BaseNotificationProvider

}//end namespace Notification