using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace AT.Provider.Notification.Base
{
	public class NotificationProviderCollection : System.Configuration.Provider.ProviderCollection {
        
		///// <summary>
		///// Must use override keyword as this is a base property. After the validation
		///// (null check and type of provider) call base.Add(provider);
		///// </summary>
		///// <param name="provider"></param>
		//public override void Add(System.Configuration.Provider.ProviderBase provider){

		//}

		/// <summary>
        /// Returns the notification provider based on name
        /// </summary>
		/// <param name="name"></param>
		new public BaseNotificationProvider this[string name]
        {
            get { return (BaseNotificationProvider)base[name]; }
        }

	}//end NotificationProviderCollection

}//end namespace Notification