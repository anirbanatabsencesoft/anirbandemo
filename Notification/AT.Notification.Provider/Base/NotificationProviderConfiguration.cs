using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Configuration;

namespace AT.Provider.Notification.Base
{
	public class NotificationProviderConfiguration : System.Configuration.ConfigurationSection {

		[ConfigurationProperty("providers")]
		public System.Configuration.ProviderSettingsCollection Providers
        {
            get
            {
                return (ProviderSettingsCollection)base["providers"];
            }
        }

		[ConfigurationProperty("default", DefaultValue = "EmailProvider")]
		public string DefaultProvider
        {
            get
            {
                return base["default"] as string;
            }
        }

	}//end NotificationProviderConfiguration

}//end namespace Notification