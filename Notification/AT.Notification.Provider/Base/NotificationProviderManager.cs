using System.Collections.Generic;
using System.Configuration;
using System.Web.Configuration;

namespace AT.Provider.Notification.Base
{
    /// <summary>
    /// This is the starting point for a notification provider as this is going to set
    /// all providers.
    /// </summary>
    public class NotificationProviderManager
    {
        private const string DATA_PROVIDER_SECTION = "NotificationImplementationProviders";

        private static BaseNotificationProvider DefaultLogic;
		private static NotificationProviderCollection ProviderCollection;
		private static ProviderSettingsCollection ProviderSettingsColl;
        
		/// <summary>
		/// Static constructor for getting all providers in a queue
		/// </summary>
		static NotificationProviderManager(){

			Initialize();
		}

		/// <summary>
		/// Initialize the configuration section and set all static private variables.
		/// </summary>
		private static void Initialize()
        {
            //get the configuration
            NotificationProviderConfiguration configSection = (NotificationProviderConfiguration)ConfigurationManager.GetSection(DATA_PROVIDER_SECTION);

            //throw exception if configuration section is not present
            if (configSection == null)
            {
                throw new ConfigurationErrorsException("Notification provider section is not set in the config.");
            }

            //get provider collections
            ProviderCollection = new NotificationProviderCollection();
            ProvidersHelper.InstantiateProviders(configSection.Providers, ProviderCollection, typeof(BaseNotificationProvider));

            //get provider settings
            ProviderSettingsColl = configSection.Providers;

            //if there is no default provider, throw excpetion
            if (ProviderCollection[configSection.DefaultProvider] == null)
            {
                throw new ConfigurationErrorsException("Default provider is not set.");
            }

            //get the default provider
            DefaultLogic = ProviderCollection[configSection.DefaultProvider];
            var defaultSettings = ProviderSettingsColl[configSection.DefaultProvider];
            
            DefaultLogic.SetConfig(defaultSettings.Parameters);
        }

		/// <summary>
		/// Returns the Default Instance of the provider.
		/// </summary>
		public static BaseNotificationProvider Default{get { return DefaultLogic; }}

		public static NotificationProviderCollection Providers{ get { return ProviderCollection; } }

		public System.Configuration.ProviderSettingsCollection ProviderSettings{ get { return ProviderSettingsColl; } }
        
        public static IList<BaseNotificationProvider> All
        {
            get
            {
                List<BaseNotificationProvider> lstProviders = new List<BaseNotificationProvider>();
                foreach (var item in Providers)
                {
                    lstProviders.Add(item as BaseNotificationProvider);
                }

                return lstProviders;
            }
        }

	}
}