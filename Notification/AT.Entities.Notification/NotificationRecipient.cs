using AT.Common.Core;
using AT.Common.Core.NotificationEnums;

namespace AT.Entities.Notification
{
    public class NotificationRecipient : BaseEntity
    {
        //The key
        public long? Id { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string RecipientAddress { get; set; }

        /// <summary>
        /// The foreign key for Notification
        /// </summary>
        public long NotificationId { get; set; }

        /// <summary>
        /// The type of the recipient i.e. SMS/CC/To
        /// </summary>
		public RecipientType RecipientType { get; set; }

    }
}