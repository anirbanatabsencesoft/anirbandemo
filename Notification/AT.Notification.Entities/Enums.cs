﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Notification.Entities
{
    public enum RecipientType : int
    {

        Phone = 6201,
        To = 6202,
        CC = 6203,
        BCC = 6204

    }//end RecipientType

    public enum NotificationType : int
    {

        Email = 6001,
        SMS = 6002,
        Fax = 6003

    }//end NotificationType


    public enum NotificationStatus : int
    {

        Sent = 6101,
        Discarded = 6102

    }//end NotificationStatus

    public enum MessageFormat : int
    {

        Text = 5901,
        FormattedText = 5902,
        HTML = 5903

    }//end MessageFormat
}
