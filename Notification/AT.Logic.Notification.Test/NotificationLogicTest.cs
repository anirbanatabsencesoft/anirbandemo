﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using AT.Entities.Notification;
using Moq;
using AT.Data.Notification.Base;
using System.Threading.Tasks;
using System.Linq;

namespace AT.Notificationic.Notification.Test
{
    [TestClass]
    public class NotificationLogicTest
    {
        public NotificationLogicTest()
        {
            IList<NotificationRecipient> lstNotificationRecipient = new List<NotificationRecipient>()
            {
                new NotificationRecipient { Id = 1010, RecipientType = Common.Core.NotificationEnums.RecipientType.To,  NotificationId = 1, RecipientAddress = "rshankar@absencesoft.com"},
                new NotificationRecipient { Id = 1020, RecipientType = Common.Core.NotificationEnums.RecipientType.To,  NotificationId = 1, RecipientAddress = "rshankar@absencesoft.com"}
            };

            IList<NotificationRecipient> lstNotificationRecipient2 = new List<NotificationRecipient>()
            {
                new NotificationRecipient { Id = 1015, RecipientType = Common.Core.NotificationEnums.RecipientType.To,  NotificationId = 2, RecipientAddress = "rshankar@absencesoft.com"},
                new NotificationRecipient { Id = 1025, RecipientType = Common.Core.NotificationEnums.RecipientType.To,  NotificationId = 2, RecipientAddress = "rshankar@absencesoft.com"}
            };

            IList<AT.Entities.Notification.Notification> lstNotification = new List<AT.Entities.Notification.Notification>
            {
                new Entities.Notification.Notification{ Id = 1, CustomerId = 140001, EmployerId = 120001, From = "rshankar@absencesoft.com", RecipientList = lstNotificationRecipient },
                new Entities.Notification.Notification{ Id = 2, CustomerId = 150010, EmployerId = 130005, From = "rshankar@absencesoft.com", RecipientList = lstNotificationRecipient2 }
            };

            // Mock the Notifications Repository using Moq
            Mock<BaseDataProvider> mockNotificationRepository = new Mock<BaseDataProvider>();

            mockNotificationRepository.Setup(mnr => mnr.Save(
                                    It.IsAny<AT.Data.Notification.Arguments.SaveNotificationArgs>())).Returns(
                                    async (AT.Data.Notification.Arguments.SaveNotificationArgs args) =>
                                    {
                                        if (args.Notification.Id == null || args.Notification.Id == 0)
                                        {
                                            args.Notification.Id = lstNotification.Count + 1;
                                            lstNotification.Add(args.Notification);
                                            return await Task.FromResult(args.Notification);
                                        }
                                        else
                                        {
                                            lstNotification.Remove(lstNotification.Where(x => x.Id == args.Notification.Id).First());
                                            lstNotification.Add(args.Notification);
                                            return await Task.FromResult(args.Notification);
                                        }
                                    }
                                    );

            mockNotificationRepository.Setup(mnr => mnr.GetNotificationList(
                                    It.IsAny<Data.Notification.Arguments.GetNotificationListArgs>())).Returns(
                                    async (Data.Notification.Arguments.GetNotificationListArgs args) =>
                                    {
                                        if (args.IsValid)
                                        {
                                            if (args.Id == null)
                                            {
                                                return await Task.FromResult(lstNotification);
                                            }
                                            else
                                            {
                                                var resultList = lstNotification.Where(x => x.Id == args.Id).ToList();
                                                return await Task.FromResult(resultList as IList<Entities.Notification.Notification>);
                                            }
                                        }
                                        else
                                        {
                                            return await Task.FromResult(lstNotification);
                                        }
                                    }
                                    );

            mockNotificationRepository.Setup(mnr => mnr.DeleteNotification(
                                    It.IsAny<Data.Notification.Arguments.DeleteNotificationArgs>())).Returns(
                                   async (Data.Notification.Arguments.DeleteNotificationArgs args) =>
                                   {
                                       if (args.IsValid)
                                       {
                                           lstNotification.Remove(lstNotification.FirstOrDefault(x => x.CustomerId == args.CustomerId));
                                           return await Task.FromResult(1);
                                       }
                                       else
                                       {
                                           return await Task.FromResult(0);
                                       }
                                   }
                                    );


            MockNotificationRepository = mockNotificationRepository.Object;
        }

        /// <summary>
        /// Our Mock Notifications Repository for use in testing.
        /// </summary>
        public readonly BaseDataProvider MockNotificationRepository;

        
        [TestMethod]
        public async Task CanSaveNotification()
        {
            // Check count before saving
            var dataResult = await MockNotificationRepository.GetNotificationList(new Data.Notification.Arguments.GetNotificationListArgs());
            int notificationCount = dataResult.Count;
            Assert.AreEqual(2, notificationCount);

            // Create a new Notification, here I do not supply an id and try saving our new Notification
            List<NotificationRecipient> lstNotificationRecipient = new List<NotificationRecipient>()
            {
                new NotificationRecipient { Id = 1030, RecipientType = Common.Core.NotificationEnums.RecipientType.To,  RecipientAddress = "superadmin@absencesoft.com"},
                new NotificationRecipient { Id = 1040, RecipientType = Common.Core.NotificationEnums.RecipientType.To,  RecipientAddress = "admin@absencesoft.com"}
            };

            AT.Data.Notification.Arguments.SaveNotificationArgs newNotification = new AT.Data.Notification.Arguments.SaveNotificationArgs()
            {
                Recipients = lstNotificationRecipient,
                Notification = new Entities.Notification.Notification { CustomerId = 140002, EmployerId = 130001, From = "rshankar@absencesoft.com", Message = "Demo Message", RecipientList = lstNotificationRecipient }
            };

            await MockNotificationRepository.Save(newNotification);

            // demand a recount
            var getAllResult = MockNotificationRepository.GetNotificationList(new Data.Notification.Arguments.GetNotificationListArgs());
            notificationCount = getAllResult.Result.Count;
            Assert.AreEqual(3, notificationCount);

            // verify that our new Notification has been saved
            var NotificationResult = MockNotificationRepository.GetNotificationList(new Data.Notification.Arguments.GetNotificationListArgs()).Result;
            Entities.Notification.Notification testNotification = NotificationResult.FirstOrDefault(p => p.EmployerId == 130001);
            Assert.IsNotNull(testNotification);
            Assert.IsInstanceOfType(testNotification, typeof(Entities.Notification.Notification));
            Assert.AreEqual(140002, testNotification.CustomerId);
        }

        [TestMethod]
        public void CanReturnNotificationById()
        {
            var dataResult = MockNotificationRepository.GetNotificationList(new Data.Notification.Arguments.GetNotificationListArgs() { Id = 1 });
            Entities.Notification.Notification testNotification = dataResult.Result.First();
            Assert.IsNotNull(testNotification);
            Assert.IsInstanceOfType(testNotification, typeof(Entities.Notification.Notification));
            Assert.AreEqual(140001, testNotification.CustomerId);
        }

        [TestMethod]
        public void CanDeleteNotification()
        {
            // Check count before deleting
            var dataResult = MockNotificationRepository.GetNotificationList(new Data.Notification.Arguments.GetNotificationListArgs());
            int NotificationCount = dataResult.Result.Count;
            Assert.AreEqual(2, NotificationCount);

            // Delete item
            var deletedItem = MockNotificationRepository.DeleteNotification(new Data.Notification.Arguments.DeleteNotificationArgs() { CustomerId = 150010 }).Result;
            Assert.IsTrue(deletedItem == 1);

            // verify that Notification has been deleted
            var NotificationResult = MockNotificationRepository.GetNotificationList(new Data.Notification.Arguments.GetNotificationListArgs()).Result;
            Entities.Notification.Notification testNotification = NotificationResult.FirstOrDefault(p => p.CustomerId == 150010);
            Assert.IsNull(testNotification);
        }

    }
}