﻿using AT.Api.Core;
using AT.Common.Core;
using AT.Common.Log;
using AT.Common.Security;
using AT.Data.Notification.Arguments;
using AT.Logic.Notification;
using AT.Provider.Notification.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace AT.Api.Notification.Controllers
{
    /// <summary>
    /// The controller for notification.
    /// </summary>
    [RoutePrefix("v1")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class NotificationController : BaseWebApiController<AT.Entities.Notification.Notification, GetNotificationListArgs, SaveNotificationArgs, DeleteNotificationArgs>
    {
        readonly NotificationLogic notificationLogic;
        private const string _userNotAuthorized = "User is not authenticated.";

        /// <summary>
        /// Constructor to set the provider as there is only one provider
        /// </summary>
        public NotificationController()
        {
            notificationLogic = new NotificationLogic();
        }

        /// <summary>
        /// Get the notification from Postgres using it's ID
        /// </summary>
        /// <param name="id">The ID/PK</param>
        /// <returns>The resultset with the notification</returns>
        [HttpGet, Secure]
        [Route("notification/get/id/{id}")]
        public async override Task<ResultSet<AT.Entities.Notification.Notification>> GetById(long id)
        {
            //define result
            var result = new ResultSet<AT.Entities.Notification.Notification>();
            try
            {
                if (AuthenticatedUser != null || Context?.Response?.StatusCode != 403)
                {
                    //get data
                    var data = await notificationLogic.GetNotificationList(new GetNotificationListArgs() { Id = id, CustomerId = null, CustomerKey = AuthenticatedUser.CustomerKey });
                    //set data and status/message
                    if (data != null && data.Count > 0)
                    {
                        result.RecordCount = data.Count();
                        result.Data = data[0];
                        result.Success = true;
                        result.Message = "Data available.";
                        result.StatusCode = HttpStatusCode.OK;
                    }
                    else
                    {
                        result.Message = "No data found for given Notification Id.";
                        result.Success = true;
                        result.StatusCode = HttpStatusCode.OK;
                    }
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                result.Success = false;
                result.Message = "Could not get Notification for given Id.";
                result.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return result;
        }

        /// <summary>
        /// This feature is not available for notification
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpGet, Secure]
        [Route("notification/get/key/{key}")]
        public async override Task<ResultSet<AT.Entities.Notification.Notification>> GetByKey(string key)
        {
            var result = new ResultSet<AT.Entities.Notification.Notification>();
            result.Message = "This feature is not available for notification.";
            result.Success = false;
            result.StatusCode = HttpStatusCode.NotImplemented;
            //return
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Returns all data for a customer key
        /// </summary>
        /// <returns>The data</returns>
        [HttpGet, Secure]
        [Route("notification/get/customer")]
        public async Task<ResultSet<List<AT.Entities.Notification.Notification>>> GetByCustomer()
        {
            var result = new ResultSet<List<AT.Entities.Notification.Notification>>();
            try
            {
                if (AuthenticatedUser != null || (Context?.Response?.StatusCode != 403))
                {
                    //get data
                    var data = await notificationLogic.GetNotificationList(new GetNotificationListArgs() { CustomerKey = AuthenticatedUser.CustomerKey });
                    //set data and status/message
                    if (data != null && data.Count > 0)
                    {
                        result.Data = new List<Entities.Notification.Notification>();
                        result.RecordCount = data.Count();
                        result.Data = data.ToList();

                        result.Success = true;
                        result.Message = "Data available.";
                        result.StatusCode = HttpStatusCode.OK;
                    }
                    else
                    {
                        result.Message = "No data found for the Customer.";
                        result.Success = true;
                        result.StatusCode = HttpStatusCode.OK;
                    }
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                result.Message = "Could not get data for the Customer.";
                result.Success = false;
                result.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            //return
            return result;
        }

        /// <summary>
        /// Save a notification into database.
        /// </summary>
        /// <param name="arg">Save notification argument including the notification object</param>
        /// <returns>The notification object with the database id.</returns>
        [HttpPut, Secure]
        public async override Task<ResultSet<AT.Entities.Notification.Notification>> Save(SaveNotificationArgs arg)
        {
            //define result
            var result = new ResultSet<AT.Entities.Notification.Notification>();
            try
            {
                if (AuthenticatedUser != null || (Context?.Response?.StatusCode != 403))
                {
                    //override with default                    
                    arg.Notification.CustomerKey = AuthenticatedUser.CustomerKey;
                    arg.Notification.CustomerId = AuthenticatedUser.CustomerId;
                    arg.Notification.CreatedById = AuthenticatedUser.Id;
                    arg.Notification.ModifiedById = AuthenticatedUser.Id;
                    arg.Notification.CreatedByKey = AuthenticatedUser.Key;
                    arg.Notification.ModifiedByKey = AuthenticatedUser.Key;
                    arg.Notification.ModifiedDate = DateTime.UtcNow;
                    arg.Notification.CreatedDate = DateTime.UtcNow;

                    //get data
                    var data = await notificationLogic.Save(arg);

                    //set data and status/message
                    if (data != null)
                    {
                        data.RecipientList = arg.Recipients;
                        data.AttachmentList = arg.Attachments;
                        await SendMail(arg, data);

                        string message = "Notification saved successfully.";

                        result.RecordCount = 1;
                        result.Data = data;
                        result.Success = true;
                        result.Message = message;
                        result.StatusCode = HttpStatusCode.OK;
                    }
                    else
                    {
                        result.Message = "Cannot save the Notification.";
                        result.Success = false;
                        result.StatusCode = HttpStatusCode.OK;
                    }
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                result.Message = ex.GetType() == typeof(OutOfMemoryException) ? "Message size was too large to process, Out of Memory" : ex.Message;
                result.Success = false;
                result.StatusCode = HttpStatusCode.ExpectationFailed;
            }

            return result;
        }

        private static async Task SendMail(SaveNotificationArgs arg, Entities.Notification.Notification data)
        {
            foreach (var provider in NotificationProviderManager.All)
            {
                if (arg.MailMessage == null)
                {
                    await provider.Notify(data);
                }
                else
                {
                    List<LinkedResource> resources = new List<LinkedResource>();
                    foreach (var view in arg.MailMessage.AlternateViews)
                    {
                        foreach (var resource in view.LinkedResources)
                        {
                            resources.Add(resource);
                        }
                    }
                    await provider.Notify(data, resources, arg.MailMessage.ReplyToList);
                }
            }
        }

        /// <summary>
        /// Search a notification against it's criteria.
        /// </summary>
        /// <param name="arg">The different search criteria</param>
        /// <returns>The notification object list as resultset.</returns>
        [HttpPost, Secure, Route("notification/search")]
        public async override Task<ResultSet<IEnumerable<AT.Entities.Notification.Notification>>> Search(GetNotificationListArgs arg)
        {
            //define result
            var result = new ResultSet<IEnumerable<AT.Entities.Notification.Notification>>();

            try
            {
                if (AuthenticatedUser != null || (Context?.Response?.StatusCode != 403))
                {
                    //set default
                    arg.CustomerId = AuthenticatedUser.CustomerId;
                    arg.CustomerKey = AuthenticatedUser.CustomerKey;
                    //get data
                    var data = await notificationLogic.GetNotificationList(arg);
                    //set data and status/message
                    if (data != null && data.Count > 0)
                    {
                        result.RecordCount = data.Count;
                        result.Data = data;
                        result.Success = true;
                        result.Message = "Search data available.";
                        result.StatusCode = HttpStatusCode.OK;
                    }
                    else
                    {
                        result.RecordCount = 0;
                        result.Message = "No data found for given search parameters.";
                        result.Success = true;
                        result.StatusCode = HttpStatusCode.OK;
                    }
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                result.Message = "Could not search.";
                result.Success = false;
                result.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            //return
            return result;
        }


        /// <summary>
        /// Search notification based on JSON passed as query string
        /// Example usage: /search/query?From=anirban@absencesoft.com&amp;IncludeArchive=true&amp;MessageContains=test
        /// Availavle keywords: From, To, HasAttachment, Id, IncludeArchive, LookupNotificationStatusId, Message, MessageContains, SubjectContains
        /// </summary>
        /// <returns></returns>
        [HttpGet, Secure]
        [Route("notification/query")]
        public override async Task<ResultSet<IEnumerable<Entities.Notification.Notification>>> SearchByQuery()
        {
            try
            {
                var collection = Context.Request.QueryString;
                var arg = new GetNotificationListArgs()
                {
                    From = collection.GetValue<string>("From"),
                    HasAttachment = collection.GetValue<bool?>("HasAttachment"),
                    Id = collection.GetValue<long?>("Id"),
                    IncludeArchive = collection.GetValue<bool?>("IncludeArchive"),
                    LookupNotificationStatusId = collection.GetValue<int?>("LookupNotificationStatusId"),
                    Message = collection.GetValue<string>("Message"),
                    MessageContains = collection.GetValue<string>("MessageContains"),
                    SubjectContains = collection.GetValue<string>("SubjectContains"),
                    To = collection.GetValue<string>("To")
                };

                return await Search(arg);

            }
            catch (Exception ex)
            {
                return await Task.FromResult(new ResultSet<IEnumerable<AT.Entities.Notification.Notification>>() { Message = ex.Message, Success = false, StatusCode = HttpStatusCode.ExpectationFailed });
            }
        }

        /// <summary>
        /// Delete a notification
        /// </summary>
        /// <param name="arg">The argument that satisfies a notiifcation search</param>
        /// <returns></returns>
        [HttpDelete, Secure]
        public async override Task<ResultSet<int>> Delete(DeleteNotificationArgs arg)
        {
            //define result
            var result = new ResultSet<int>();
            try
            {
                if (AuthenticatedUser != null || (Context?.Response?.StatusCode != 403))
                {
                    var notification = await GetById(arg.NotificationId.Value);
                    if (notification.Data != null)
                    {
                        //get data
                        var data = await notificationLogic.DeleteNotification(arg);
                        //set data and status/message
                        result.RecordCount = 1;
                        result.Data = data;
                        result.Success = true;
                        result.Message = "Deleted Notification.";
                    }
                    else
                    {
                        result.Data = 0;
                        result.Message = "Cannot delete the Notification.";
                        result.Success = true;
                    }
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                result.Success = false;
                result.Message = "Could not delete the Notification.";
                result.StatusCode = HttpStatusCode.ExpectationFailed;
            }

            return result;
        }

        /// <summary>
        /// Notify a notification immediately.
        /// </summary>
        /// <param name="notification"></param>
        /// <returns></returns>
        [HttpPost, Secure]
        public async Task<ResultSet<bool>> Notify(AT.Entities.Notification.Notification notification)
        {
            //define result
            var result = new ResultSet<bool>();
            try
            {
                if (AuthenticatedUser != null || (Context?.Response?.StatusCode != 403))
                {
                    if (notification.CustomerId == AuthenticatedUser.CustomerId || notification.CustomerKey == AuthenticatedUser.CustomerKey)
                    {
                        foreach (var provider in NotificationProviderManager.All)
                        {
                            await provider.Notify(notification);
                        }

                        result.RecordCount = 1;
                        result.Success = true;
                        result.Data = true;
                        result.Message = "Notified successfully.";
                        result.StatusCode = HttpStatusCode.OK;
                    }
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                result.Message = "Could not Notify for given Notification.";
                result.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Notify a notification immediately.
        /// </summary>
        /// <param name="notification"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("notification/notifyonly")]
        public async Task<ResultSet<AT.Entities.Notification.Notification>> NotifyOnly(AT.Entities.Notification.Notification notification)
        {
            //define result
            var result = new ResultSet<AT.Entities.Notification.Notification>();
            try
            {
                if (AuthenticatedUser != null || (Context?.Response?.StatusCode != 403))
                {
                    if (notification != null)
                    {
                        if (notification.RecipientList.Any())
                        {
                            notification.Recipients = String.Join(";", notification.RecipientList.Select(p => p.RecipientAddress));
                        }
                        foreach (var provider in NotificationProviderManager.All)
                        {
                            await provider.Notify(notification);
                        }

                        result.RecordCount = 1;
                        result.Success = true;
                        result.Data = notification;
                        result.Message = "Notified successfully.";
                    }
                    else
                    {
                        result.Message = "Cannot send notification.";
                        result.Success = false;
                    }
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                result.Message = "Could not Notify for given Notification.";
                result.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Notify all unsent messages to all notification engine.
        /// </summary>
        /// <returns></returns>
        [HttpPost, Secure]
        public async Task<ResultSet<bool>> NotifyAll()
        {
            var result = new ResultSet<bool>();
            try
            {
                if (AuthenticatedUser != null || (Context?.Response?.StatusCode != 403))
                {
                    Parallel.ForEach(NotificationProviderManager.All, async provider =>
                    {
                        await provider.Notify();
                    });
                    result.Success = true;
                    result.Message = "Notified successfully.";
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                result.Success = false;
                result.Message = "Could not Notify.";
                result.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Notify against a Id that's already saved.
        /// </summary>
        /// <param name="notificationId"></param>
        /// <returns></returns>
        [HttpPost, Secure]
        public async Task<ResultSet<bool>> NotifyNotificationId(int notificationId)
        {
            var result = new ResultSet<bool>();
            try
            {
                if (AuthenticatedUser != null || (Context?.Response?.StatusCode != 403))
                {
                    var notification = await GetById(notificationId);
                    if (notification.Data != null)
                    {
                        await Notify(notification.Data);

                        result.Data = true;
                        result.Success = true;
                        result.Message = "Notified successfully.";
                    }
                    else
                    {
                        result.Data = false;
                        result.Success = true;
                        result.Message = "No data found.";
                    }
                }
                else
                {
                    result.Message = _userNotAuthorized;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                result.Success = false;
                result.Message = "Could not Notify for given Notification Id.";
                result.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return await Task.FromResult(result);
        }

    }
}