﻿using AbsenceSoft.Rendering.Properties;
using AT.Common.Core;
using AT.Common.Core.NotificationEnums;
using AT.Common.Log;
using AT.Data.Notification.Arguments;
using AT.Entities.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace AT.Logic.Notification
{
    public static class NotificationHelper
    {
        #region Template constants
        /// <summary>
        /// The forgot password email template name.
        /// </summary>
        public const string EmailTemplate_ForgotPassword = "ForgotPassword";

        /// <summary>
        /// The forgot password email template name.
        /// </summary>
        public const string EmailTemplate_LockedOutEmail = "LockedOutEmail";

        /// <summary>
        /// The create account email template name.
        /// </summary>
        public const string EmailTemplate_CreateAccount = "CreateAccount";

        /// <summary>
        /// The change password email template name.
        /// </summary>
        public const string EmailTemplate_ChangePassword = "ChangePassword";

        /// <summary>
        /// The add new user template name
        /// </summary>
        public const string EmailTemplate_AddNewUser = "AddNewUser";

        /// <summary>
        /// User need access email template name
        /// </summary>
        public const string EmailTemplate_UserNeedAccess = "UserNeedAccessEmail";

        /// <summary>
        /// Welcome message to approved user
        /// </summary>
        public const string EmailTemplate_WelcomeEmail = "WelcomeEmail";

        public const string EmailTemplate_EmployeeValidationEmail = "EmployeeValidationEmail";
        public const string EmailTemplate_EmployeeAssociationEmail = "EmployeeAssociationEmail";

        public const string EmailTemplate_TodoNotification = "ToDoEmailNotification";

        public const string EmailTemplate_SelfAccountCreationEmail = "SelfAccountCreationEmail";

        public const string EmailTemplate_UserExistsEmailToUser = "UserExistsEmailToUser";

        public const string EmailTemplate_SelfRegistrationEmailToAdmin = "SelfRegistrationEmailToAdmin";

        public const string EmailTemplate_EmployeeNotUniqueEmailToAdmin = "EmployeeNotUniqueEmailToAdmin";

        #endregion

        /// <summary>
        /// Sends the mail after saving the notification
        /// </summary>
        /// <param name="message"></param>
        /// <param name="currentUser"></param>
        /// <param name="createdByUserId"></param>
        /// <returns></returns>
        public static bool SendEmail(this MailMessage message, User currentUser)
        {
            var saveNotificationArgs = SetNotificationArgument(message, currentUser);
            if (saveNotificationArgs != null)
            {
                return SendNotification(saveNotificationArgs);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Sends mail after saving notification in Async fashion
        /// </summary>
        /// <param name="message"></param>
        /// <param name="currentUser"></param>
        /// <param name="createdByUserId"></param>
        /// <returns></returns>
        public async static Task<bool> SendEmailAsync(this MailMessage message, User currentUser)
        {
            var saveNotificationArgs = SetNotificationArgument(message, currentUser);
            if (saveNotificationArgs != null)
            {
                return await SendNotificationAsync(saveNotificationArgs);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Sends Notification with given parameters
        /// </summary>
        /// <param name="message"></param>
        /// <param name="currentUser"></param>
        /// <param name="notificationTypeSelected"></param>
        /// <param name="fileNames"></param>
        /// <param name="lookupNotificationTypeId"></param>
        public static void Notify(this MailMessage message, User currentUser, NotificationType notificationTypeSelected, string fileNames)
        {
            var saveNotificationArgs = SetNotificationArgument(currentUser, message.From.ToString(), message.Subject, message.Body, (message.IsBodyHtml ? MessageFormat.HTML : MessageFormat.Text), notificationTypeSelected, message.To.ToString());
            SetAttachments(fileNames, saveNotificationArgs);

            SendNotification(saveNotificationArgs);
        }

        /// <summary>
        /// Sends Notification in Async fashion
        /// </summary>
        /// <param name="message"></param>
        /// <param name="currentUser"></param>
        /// <param name="notificationTypeSelected"></param>
        /// <param name="fileNames"></param>
        /// <param name="lookupNotificationTypeId"></param>
        /// <returns></returns>
        public async static Task<bool> NotifyAsync(this MailMessage message, User currentUser, NotificationType notificationTypeSelected, string fileNames)
        {
            var saveNotificationArgs = SetNotificationArgument(currentUser, message.From.ToString(), message.Subject, message.Body, (message.IsBodyHtml ? MessageFormat.HTML : MessageFormat.Text), notificationTypeSelected, message.To.ToString());
            SetAttachments(fileNames, saveNotificationArgs);

            return await SendNotificationAsync(saveNotificationArgs);
        }

        /// <summary>
        /// Set the attachments to a notification
        /// </summary>
        /// <param name="fileNames"></param>
        /// <param name="saveNotificationArgs"></param>
        private static void SetAttachments(string fileNames, SaveNotificationArgs saveNotificationArgs)
        {
            if (!string.IsNullOrWhiteSpace(fileNames))
            {
                return;
            }
            var nameList = fileNames.Split(new string[3] { ",", "|", ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();

            saveNotificationArgs.Attachments = new List<Entities.Notification.NotificationAttachment>();
            foreach (var name in nameList)
            {
                byte[] fileContentBytes = System.IO.File.ReadAllBytes(name);
                saveNotificationArgs.Attachments.Add(new Entities.Notification.NotificationAttachment()
                {
                    AttachmentHeader = $"CustomerKey - {saveNotificationArgs.Notification.CustomerKey}, EmployerId - {saveNotificationArgs.Notification.EmployerId}",
                    FileContent = fileContentBytes,
                    FileName = System.IO.Path.GetFileName(name)
                });
            }
        }

        /// <summary>
        /// Gets an email template text directly from an embedded resource of the logic assembly. This is only
        /// for site-wide email templates such as forgot password, welcome, etc. All other emails (e.g. communications)
        /// should use the CommunicationService in order to get template text.
        /// </summary>
        /// <param name="name">The email template name</param>
        /// <returns>The specified email template text or <c>null</c> if the named resource wasn't found.</returns>
        public static string GetEmailTemplate(string name)
        {
            return Resources.ResourceManager.GetString(name);
        }

        /// <summary>
        /// Sets the notification arguments
        /// </summary>
        /// <param name="currentUser"></param>
        /// <param name="mailSender"></param>
        /// <param name="mailSubject"></param>
        /// <param name="mailMessage"></param>
        /// <param name="mailMessageFormat"></param>
        /// <param name="notificationTypeSelected"></param>
        /// <param name="fileNamePassed"></param>
        /// <param name="lookupNotificationTypeId"></param>
        /// <param name="createdByUserId"></param>
        /// <param name="fileContentBytes"></param>
        /// <param name="mailRecipientAddress"></param>
        /// <returns></returns>
        private static SaveNotificationArgs SetNotificationArgument(User currentUser, string mailSender, string mailSubject, string mailMessage, MessageFormat mailMessageFormat, NotificationType notificationTypeSelected, string mailRecipientAddress)
        {
            //Build Notification   
            AT.Entities.Notification.Notification notification = new AT.Entities.Notification.Notification
            {
                CustomerKey = currentUser.CustomerKey,
                CustomerId = currentUser.CustomerId ?? 0,
                From = mailSender,
                NotificationType = notificationTypeSelected,
                CreatedById = currentUser.Id,
                CreatedByKey = currentUser.Key,
                ModifiedById = currentUser.Id,
                ModifiedByKey = currentUser.Key,
                MessageFormat = mailMessageFormat,
                Subject = mailSubject,
                Message = mailMessage
            };

            //Recipient List Recipient
            AT.Entities.Notification.NotificationRecipient recipient = new AT.Entities.Notification.NotificationRecipient()
            {
                RecipientAddress = mailRecipientAddress,
                RecipientType = RecipientType.To
            };

            // Create SaveNotificationArgs
            SaveNotificationArgs saveNotificationArgs = new SaveNotificationArgs()
            {
                Notification = notification,
                Recipients = new List<AT.Entities.Notification.NotificationRecipient>
                                {
                                    recipient
                                }
            };

            return saveNotificationArgs;
        }

        /// <summary>
        /// Sets the notification arguments
        /// </summary>
        /// <param name="message"></param>
        /// <param name="currentUser"></param>
        /// <param name="createdByUserId"></param>
        /// <returns></returns>
        private static SaveNotificationArgs SetNotificationArgument(MailMessage message, User currentUser)
        {
            //Build Notification
            AT.Entities.Notification.Notification notification = new AT.Entities.Notification.Notification
            {
                CustomerKey = currentUser.CustomerKey,
                CustomerId = currentUser.CustomerId ?? 0,
                From = message.From.ToString(),
                NotificationType = NotificationType.Email,
                CreatedById = currentUser.Id,
                CreatedByKey = currentUser.Key,
                ModifiedById = currentUser.Id,
                ModifiedByKey = currentUser.Key,
                MessageFormat = MessageFormat.HTML,
                Subject = message.Subject,
                Message = message.Body
            };

            List<AT.Entities.Notification.NotificationAttachment> notificationAttachments = new List<AT.Entities.Notification.NotificationAttachment>();

            //Build Attachment
            if (message.Attachments.Count > 0)
            {
                bool attachmentSuccess = true;
                Parallel.ForEach(message.Attachments, (attachment, state) =>
                    {
                        try
                        {
                            byte[] bytes = Utilities.ReadStreamFully(attachment.ContentStream);
                            if (bytes.Length > 0)
                            {
                                AT.Entities.Notification.NotificationAttachment notificationAttachment = new AT.Entities.Notification.NotificationAttachment()
                                {
                                    FileName = attachment.Name,
                                    FileContent = bytes
                                };
                                notificationAttachments.Add(notificationAttachment);
                            }
                            else
                            {
                                attachmentSuccess = false;
                                state.Break();
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex.Message);
                            attachmentSuccess = false;
                            state.Break();
                        }
                    }
                    );

                if (!attachmentSuccess)
                {
                    return null;
                }
            }

            List<AT.Entities.Notification.NotificationRecipient> NotificationRecipients = new List<AT.Entities.Notification.NotificationRecipient>();
            bool replyToListSet = true;
            //Build Recipient
            Parallel.ForEach(message.To, (replyTo, status) =>
                {
                    try
                    {
                        AT.Entities.Notification.NotificationRecipient notificationRecipient = new AT.Entities.Notification.NotificationRecipient()
                        {
                            RecipientAddress = replyTo.Address,
                            RecipientType = RecipientType.To
                        };
                        NotificationRecipients.Add(notificationRecipient);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex.Message);
                        replyToListSet = false;
                        status.Break();
                    }
                }
                );
            if (!replyToListSet)
            {
                return null;
            }

            // Create SaveNotificationArgs
            SaveNotificationArgs saveNotificationArgs = new SaveNotificationArgs()
            {
                Notification = notification,
                Recipients = NotificationRecipients
            };

            if (notificationAttachments.Count > 0)
            {
                saveNotificationArgs.Attachments = notificationAttachments;
            }
            return saveNotificationArgs;
        }

        /// <summary>
        /// Sends Notification 
        /// </summary>
        /// <param name="saveNotificationArgs"></param>
        /// <returns></returns>
        private static bool SendNotification(SaveNotificationArgs saveNotificationArgs)
        {
            NotificationLogic notificationLogic = new NotificationLogic();
            AT.Entities.Notification.Notification notification = notificationLogic.Save(saveNotificationArgs).GetAwaiter().GetResult();

            return notification?.Id > 0;
        }

        /// <summary>
        /// Sends Notification in Async fashion
        /// </summary>
        /// <param name="saveNotificationArgs"></param>
        /// <returns></returns>
        private async static Task<bool> SendNotificationAsync(SaveNotificationArgs saveNotificationArgs)
        {
            NotificationLogic notificationLogic = new NotificationLogic();
            AT.Entities.Notification.Notification notification = await notificationLogic.Save(saveNotificationArgs);

            return notification?.Id > 0;
        }
    }
}