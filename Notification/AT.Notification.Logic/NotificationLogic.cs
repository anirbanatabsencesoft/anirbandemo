﻿using AT.Data.Notification.Arguments;
using AT.Data.Notification.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Notification
{
    /// <summary>
    /// NotificationLogic has all the business logic defined for Notification
    /// </summary>
    public class NotificationLogic
    {
        /// <summary>
        /// The internal provider
        /// </summary>
        readonly BaseDataProvider notificationProvider;

        public NotificationLogic()
        {
            notificationProvider = DataProviderManager.Default;
        }

        /// <summary>
        /// Return the list of Notifications calling SearchNotifictaion method.
        /// </summary>
        /// <param name="args"></param>
        public async Task<IList<AT.Entities.Notification.Notification>> GetNotificationList(GetNotificationListArgs args)
        {
            return await notificationProvider.GetNotificationList(args);
        }

        /// <summary>
        /// Sets the notification as Archived and deletes it's associated Attachment and Recipient
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public async Task<int> DeleteNotification(DeleteNotificationArgs args)
        {
            return await notificationProvider.DeleteNotification(args);
        }

        /// <summary>
        /// Save a notification. On success call SendMail method.
        /// While updating we will not consider updating attachment and recipient
        /// </summary>
        /// <param name="arg"></param>
        public async Task<AT.Entities.Notification.Notification> Save(SaveNotificationArgs arg)
        {
            return await notificationProvider.Save(arg);
        }

        /// <summary>
        /// Save attachment of associated notification
        /// </summary>
        /// <param name="attachments"></param>
        /// <param name="NotificationId"></param>
        /// <returns></returns>
        public async Task<long> SaveAttachment(ICollection<AT.Entities.Notification.NotificationAttachment> attachments, long NotificationId)
        {
            return await notificationProvider.SaveAttachments(attachments, NotificationId);
        }

        /// <summary>
        /// Save recipient information of associated notification
        /// </summary>
        /// <param name="recipients"></param>
        /// <param name="NotificationId"></param>
        /// <returns></returns>
        public async Task<long> SaveRecipient(ICollection<AT.Entities.Notification.NotificationRecipient> recipients, long NotificationId)
        {
            return await notificationProvider.SaveRecipients(recipients, NotificationId);
        }

        /// <summary>
        /// Update notification
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public async Task<AT.Entities.Notification.Notification> Update(SaveNotificationArgs arg)
        {
            return await notificationProvider.Save(arg);
        }
    }
}