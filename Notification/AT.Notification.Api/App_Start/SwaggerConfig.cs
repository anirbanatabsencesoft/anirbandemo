using System.Web.Http;
using WebActivatorEx;
using AT.Api.Notification;
using Swashbuckle.Application;
using AT.Api.Core;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace AT.Api.Notification
{
    /// <summary>
    /// The swagger config file
    /// </summary>
    public static class SwaggerConfig
    {
        /// <summary>
        /// The registration process
        /// </summary>
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;
            BaseSwaggerConfig.Register("Notification", thisAssembly);
        }
    }
}
