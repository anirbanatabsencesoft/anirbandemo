﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace AT.Api.Notification
{
    /// <summary>
    /// Web Api config to register
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// The registration methodology
        /// </summary>
        /// <param name="config"></param>
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            config.EnableCors();
            
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new {controller = "notification", id = RouteParameter.Optional }
            );
        }
    }
}
