﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace AbsenceSoft.Process
{
    internal class ReverseEngineeredWorkSchedule
    {
        #region .ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="ReverseEngineeredWorkSchedule"/> class.
        /// </summary>
        public ReverseEngineeredWorkSchedule() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReverseEngineeredWorkSchedule"/> class.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <param name="employer">The employer.</param>
        public ReverseEngineeredWorkSchedule(Customer customer, Employer employer) : this()
        {
            Customer = customer;
            Employer = employer;
        }

        #endregion .ctor

        #region Public Properties

        /// <summary>
        /// Gets or sets the customer.
        /// </summary>
        /// <value>
        /// The customer.
        /// </value>
        public Customer Customer { get; set; }

        /// <summary>
        /// Gets or sets the employer.
        /// </summary>
        /// <value>
        /// The employer.
        /// </value>
        public Employer Employer { get; set; }

        #endregion Public Properties

        /// <summary>
        /// Processes the exceptions and corrections to the employee work schedule(s).
        /// </summary>
        /// <param name="employee">The employee.</param>
        public void CorrectSchedules(Employee employee = null)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            Console.WriteLine("Processing exceptions...");

            // Create the mongo query and apply the data access filters
            List<IMongoQuery> criteria = new List<IMongoQuery>()
            {
                Case.Query.EQ(c => c.CustomerId, Customer.Id),
                Case.Query.EQ(c => c.EmployerId, Employer.Id),
                Case.Query.NE(c => c.IsDeleted, true),
                Case.Query.EQ(c => c.Status, CaseStatus.Open),
                Case.Query.ElemMatch(c => c.Segments, s =>
                    s.And(
                        s.EQ(segment => segment.Type, CaseType.Intermittent),
                        s.ElemMatch(t => t.UserRequests, r => r.And(
                            // Only care where we have approved time over 0
                            r.ElemMatch(b => b.Detail, b => b.GT(f => f.Approved, 0))
                        ))
                    )
                )
            };

            if (employee != null)
            {
                criteria.Add(Case.Query.EQ(c => c.Employee.Id, employee.Id));
            }

            // Query cases
            var cases = Case.Query.Find(Case.Query.And(criteria)).ToList();
            var employeeIds = cases.Select(c => c.Employee.Id).Distinct().ToList();
            var employees = employee != null ? new List<Employee>(1) { employee } : Employee.AsQueryable().Where(e => employeeIds.Contains(e.Id)).ToList();

            Console.WriteLine($"Found {cases.Count} cases for {employees.Count} employees that have 'potential' exceptions");

            employees.AsParallel().ForAll(emp =>
            {
                try
                {
                    var employeeCases = cases.Where(c => c.Employee.Id == emp.Id);
                    Console.WriteLine($"Searching for exceptions for {emp.EmployeeNumber} - {emp.FullName} who has {employeeCases.Count()} cases");
                    var tors = GetAllTimeOff(employeeCases);
                    var usage = employeeCases.SelectMany(GetConsecutiveAndReducedUsage).ToList();
                    var exceptions = GetExceptions(employeeCases);
                    Console.WriteLine($"Found {tors.Count} time off requests with {exceptions.Count} exceptions for {emp.EmployeeNumber} - {emp.FullName}");

                    if (!exceptions.Any())
                    {
                        Console.WriteLine($"Skipping {emp.EmployeeNumber} - {emp.FullName} (no exceptions)");
                        return;
                    }

                    // Get this for logging
                    var currentScheduleCount = emp.WorkSchedules.Count;

                    // Merge the TORs (missing/exceptions) with the actual Consecutive/Reduced usage (or approved TOR usage)
                    tors.AddRangeIfNotExists(usage, (t, u) => t.DateUsed == u.DateUsed);

                    // Get our new schedules, all merged and ready to be set/updated
                    emp.WorkSchedules = FixSchedulesForUsage(emp, tors);

                    // Level work schedules
                    LevelWorkSchedules(emp);

                    // Merge contiguous duplicate schedules (it happens for sure)
                    MergeContiguousDuplicateSchedules(emp);

                    // Update the employee
                    Console.WriteLine($"Updating {currentScheduleCount} to {emp.WorkSchedules.Count} work schedules for {emp.EmployeeNumber} - {emp.FullName}");
                    emp.Save();
                    Console.WriteLine($"Updated work schedules for {emp.EmployeeNumber} - {emp.FullName}");

                    // Since we have schedule updates we need to re-calc/process that update
                    Console.WriteLine($"Processing updates/recalculations for {emp.EmployeeNumber} - {emp.FullName}");
                    ProcessUpdates(emp);
                    Console.WriteLine($"Processed updates/recalculations for {emp.EmployeeNumber} - {emp.FullName}");
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine($"ERROR processing {emp.EmployeeNumber} - {emp.FullName}");
                    Console.Error.WriteLine(ex);
                }
            });

            timer.Stop();
            Console.WriteLine($"Finished processing exceptions in {timer.Elapsed}");
        }

        /// <summary>
        /// Processes the updates for an employee's open cases (copy from Employee Service but different).
        /// </summary>
        /// <param name="emp">The emp.</param>
        public void ProcessUpdates(Employee emp)
        {
            var cases = Case.AsQueryable()
                .Where(c => c.CustomerId == emp.CustomerId && c.EmployerId == emp.EmployerId && c.Employee.Id == emp.Id && c.Status != CaseStatus.Cancelled && c.Status != CaseStatus.Closed)
                .OrderBy(c => c.StartDate)
                .ToList();
            if (!cases.Any())
                return;

            cases.SelectMany(c => c.Segments.Where(s => s.Type == CaseType.Intermittent))
                .SelectMany(s => s.AppliedPolicies)
                .SelectMany(p => p.Usage.Where(u => u.IntermittentDetermination == IntermittentStatus.Allowed))
                .ForEach(u => u.Determination = AdjudicationStatus.Approved);

            using (CaseService svc = new CaseService(false))
            {
                foreach (var c in cases)
                {
                    var calculatedCase = svc.RunCalcs(c);
                    calculatedCase.Save();
                }
            }
        }

        /// <summary>
        /// Gets the exceptions for the given collection of cases.
        /// </summary>
        /// <param name="cases">The cases.</param>
        /// <returns></returns>
        private List<MeasuredDate> GetExceptions(IEnumerable<Case> cases)
        {
            List<MeasuredDate> exceptions = new List<MeasuredDate>();

            foreach (var c in cases)
            {
                // Get time off requests for this case represented as measured data
                var tors = GetTimeOffRequests(c);

                // Determine the exceptions where zero days intersect with approved TORs (> 0 time approved)
                exceptions.AddRangeIfNotExists(tors.Where(t => t.Approved > t.Scheduled), (a, b) => a.DateUsed == b.DateUsed);
            }

            return exceptions;
        }

        /// <summary>
        /// Gets all time off.
        /// </summary>
        /// <param name="cases">The cases.</param>
        /// <returns></returns>
        private List<MeasuredDate> GetAllTimeOff(IEnumerable<Case> cases)
        {
            return cases.SelectMany(GetTimeOffRequests).ToList();
        }

        /// <summary>
        /// Gets the usage for the given case in summary form.
        /// </summary>
        /// <param name="case">The case.</param>
        /// <returns></returns>
        private List<MeasuredDate> GetUsage(Case @case)
        {
            // Boosted this from the Intermittent Schedule Exception Report
            return @case.Segments
                .Where(s => s.Type == CaseType.Intermittent)
                .SelectMany(s => s.AppliedPolicies
                // Ignore calendar type, only worry about "work" types
                .Where(p => p.PolicyReason.EntitlementType?.CalendarType() != true)
                // Get only where usage is zero or less
                .SelectMany(p => p.Usage))
                .GroupBy(r => r.DateUsed)
                // Get our container for comparison, minutes will always be zero, leave it default
                .Select(u => new MeasuredDate()
                {
                    DateUsed = u.Key,
                    Scheduled = Convert.ToInt32(u.Max(r => r.MinutesInDay))
                })
                .ToList();
        }

        /// <summary>
        /// Gets the time off requests in summary form.
        /// </summary>
        /// <param name="case">The case.</param>
        /// <returns></returns>
        private List<MeasuredDate> GetTimeOffRequests(Case @case)
        {
            var usage = GetUsage(@case);

            return @case.Segments
                .Where(s => s.Type == CaseType.Intermittent)
                .SelectMany(s => s.UserRequests)
                // Filter just to approved minutes, and get data containers
                .Select(r => new MeasuredDate()
                {
                    DateUsed = r.RequestDate,
                    // We only care about approved minutes
                    Approved = r.Detail.Max(d => d.Approved),
                    // Need to get the scheduled minutes for this date and policy
                    Scheduled = usage
                        .Where(u => u.DateUsed.Date == r.RequestDate.Date)
                        .OrderByDescending(u => u.Approved)
                        .FirstOrDefault()?
                        .Scheduled ?? 0
                })
                .ToList();
        }

        /// <summary>
        /// Gets the consecutive and reduced usage.
        /// </summary>
        /// <param name="case">The case.</param>
        /// <returns></returns>
        private List<MeasuredDate> GetConsecutiveAndReducedUsage(Case @case)
        {
            return @case.Segments
                .Where(s => s.Type == CaseType.Consecutive || s.Type == CaseType.Reduced)
                .SelectMany(s => s.AppliedPolicies
                // Ignore calendar type, only worry about "work" types
                .Where(p => p.PolicyReason.EntitlementType?.CalendarType() != true)
                // Get only where usage is zero or less
                .SelectMany(p => p.Usage)
                .Where(u => u.Determination == AdjudicationStatus.Approved))
                .GroupBy(r => r.DateUsed)
                // Get our container for comparison, minutes will always be zero, leave it default
                .Select(u => new MeasuredDate()
                {
                    DateUsed = u.Key,
                    Approved = Convert.ToInt32(u.Max(r => r.MinutesUsed)),
                    Scheduled = Convert.ToInt32(u.Max(r => r.MinutesInDay))
                })
                .ToList();
        }

        /// <summary>
        /// Levels the work schedules.
        /// </summary>
        /// <param name="emp">The emp.</param>
        private void LevelWorkSchedules(Employee emp)
        {
            // Level Work Schedules
            Schedule last = null;
            foreach (var sched in emp.WorkSchedules.OrderBy(s => s.StartDate))
            {
                if (last != null && last.EndDate?.ToMidnight() != sched.StartDate.AddDays(-1).ToMidnight())
                {
                    last.EndDate = sched.StartDate.AddDays(-1).ToMidnight();
                }
                if (sched.StartDate <= sched.EndDate)
                    last = sched;
            }
            emp.WorkSchedules.RemoveAll(s => s.StartDate > s.EndDate);
            emp.WorkSchedules = emp.WorkSchedules.GroupBy(x => x.StartDate).Select(g => g.First()).OrderBy(s => s.StartDate).ToList();

            // Set the end date of the last known schedule to null.
            //  This ensures it's perpetual
            emp.WorkSchedules.Last().EndDate = null;
        }

        /// <summary>
        /// Merges the contiguous duplicate schedules.
        /// </summary>
        /// <param name="emp">The emp.</param>
        private void MergeContiguousDuplicateSchedules(Employee emp)
        {
            // Merge Contiguous Duplicate Schedules
            List<Schedule> finalSchedules = new List<Schedule>(emp.WorkSchedules.Count);
            foreach (var sched in emp.WorkSchedules)
            {
                var last = finalSchedules.OrderBy(s => s.StartDate).LastOrDefault();
                if (last != null && last.Times.TrueForAll(t => sched.Times.Any(s => s.SampleDate.DayOfWeek == t.SampleDate.DayOfWeek && s.TotalMinutes == t.TotalMinutes)))
                {
                    last.EndDate = sched.EndDate;
                    continue;
                }
                finalSchedules.Add(sched);
            }
            emp.WorkSchedules = finalSchedules;
        }

        /// <summary>
        /// Use an initialized LOA (this) ojbect and generate a schedule that is between
        /// the passed start and end dates, inclusive of both dates
        /// </summary>
        /// <param name="employee">The employee.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="schedules">The schedules.</param>
        /// <returns>
        /// List of Time objects
        /// </returns>
        private List<Time> MaterializeSchedule(Employee employee, DateTime startDate, DateTime endDate, List<Schedule> schedules = null)
        {
            // Call our leave of absence instance to materialize the schedule
            return new LeaveOfAbsence()
            {
                Customer = Customer,
                Employer = Employer,
                Employee = employee,
                WorkSchedule = schedules
            }.MaterializeSchedule(startDate, endDate, schedules: schedules);
        }

#pragma warning disable S3776 // Cognitive Complexity of methods should not be too high
        /// <summary>
        /// Fixes the schedules from usage and returns a corrected list of schedules for
        /// the employee.
        /// </summary>
        /// <param name="employee">The employee.</param>
        /// <param name="schedules">The schedules.</param>
        /// <param name="tors">The time off requests we need to accommodat in the work schedules.</param>
        /// <returns></returns>
        private List<Schedule> FixSchedulesForUsage(Employee employee, List<MeasuredDate> tors)
#pragma warning restore S3776 // Cognitive Complexity of methods should not be too high
        {
            // If there's nothing to do, then duh, don't do anything, just return the scheudles passed in.
            if (!tors.Any() || employee.WorkSchedules == null || !employee.WorkSchedules.Any())
            {
                return employee.WorkSchedules;
            }

            List<Schedule> schedules = employee.WorkSchedules.Select(w => w.Clone()).OrderBy(w => w.StartDate).ToList();

            // Pre-Date the first work schedule back to the employee's hire date/service date OR at least start of all cases
            schedules.First().StartDate = Date.Min(
                employee.HireDate,
                employee.ServiceDate,
                schedules.First().StartDate,
                Case.AsQueryable().Where(c => c.Employee.Id == employee.Id).Select(c => c.StartDate).Min()
            ) ?? schedules.First().StartDate;

            // Materialize the current schedule passed in based on the extreme values of our exceptions' week start and end
            //  Even rotating and variable schedules are going to get changed to weekly in this routine.
            var scheduledTimes = MaterializeSchedule(
                employee,
                schedules.Min(w => w.StartDate).GetFirstDayOfWeek(employee.StartDayOfWeek),
                schedules.OrderBy(w => w.EndDate ?? DateTime.MaxValue).Last().EndDate ?? schedules.Max(w => w.StartDate).GetLastDayOfWeek(employee.StartDayOfWeek),
                schedules);

            // This will hold our new schedules that cover a week were we "missed" some time
            List<Schedule> newSchedules = new List<Schedule>();

            // Get a grouping of the entire schedule range by week
            var scheduleByWeek = scheduledTimes.GroupBy(k => new
            {
                StartDate = k.SampleDate.GetFirstDayOfWeek(),
                EndDate = k.SampleDate.GetLastDayOfWeek()
            }).OrderBy(g => g.Key.StartDate);

            // Rotate week by week of all days in all usage to check
            //  and then testing the schedule against those by scope of the week.
            foreach (var week in scheduleByWeek)
            {
                // Stage out a corrected schedule for this week, if we don't need it, we'll throw
                //  it to the GC to deal with once we leave the loop context
                Schedule correctedWeekSchedule = new Schedule()
                {
                    ScheduleType = ScheduleType.Weekly,
                    StartDate = week.Key.StartDate,
                    EndDate = week.Key.EndDate,
                    Times = new List<Time>(7)
                };
                // Track each type of date in this week, this is the fun stuff
                List<DateTime?> missingDates = new List<DateTime?>();
                List<DateTime?> goodDates = new List<DateTime?>();
                List<DateTime?> extraDates = new List<DateTime?>();

                // Loop through each date of this week and determine if we're covered, otherwise
                //  of if there are no "A" records for that date, then move on
                foreach (var date in week.Key.StartDate.AllDatesInRange(week.Key.EndDate))
                {
                    // Get the time used/lost for this day of this week
                    var usage = tors.Where(a => a.DateUsed == date).OrderByDescending(a => a.Approved).FirstOrDefault()?.Approved ?? 0;
                    // Get the work schedule that covers this date
                    var time = scheduledTimes.FirstOrDefault(t => t.SampleDate == date);
                    if (time == null)
                    {
                        // If there is no schedule that covers it, we need to create a date that covers it
                        missingDates.Add(date);
                        continue;
                    }
                    // Get the time that matches the same "day of the week" of the current date we're testing
                    if (time.TotalMinutes < usage)
                    {
                        // If the total minutes scheduled is less than the recorded time used, then we need to correct
                        //  that as well
                        missingDates.Add(date);
                        continue;
                    }
                    // If we're not using time but time is scheduled, add this to the extra dates
                    if (usage == 0 && time.TotalMinutes > 0)
                    {
                        extraDates.Add(date);
                        continue;
                    }
                    // If we're using time and have time scheduled and they "match(ish)", 
                    //  AWESOME, add this to our good dates collection.
                    if (usage <= time.TotalMinutes)
                    {
                        goodDates.Add(date);
                    }
                }

                // Loop through each date of this week and determine if we're going to pull
                //  from the good date, missing date OR extra date.
                foreach (var date in week.Key.StartDate.AllDatesInRange(week.Key.EndDate))
                {
                    // Get the work schedule that covers this date
                    var sched = schedules.OrderBy(s => s.StartDate).FirstOrDefault(s => date.DateInRange(s.StartDate, s.EndDate));
                    // Get the time that matches the same "day of the week" of the current date we're testing
                    var time = sched?.Times.FirstOrDefault(t => t.SampleDate.DayOfWeek == date.DayOfWeek);

                    // This was good, then keep it
                    if (goodDates.Contains(date))
                    {
                        // Add this to the corrected schedule
                        correctedWeekSchedule.Times.Add(new Time()
                        {
                            SampleDate = date,
                            TotalMinutes = time?.TotalMinutes
                        });
                        // continue to the next day
                        continue;
                    }

                    // This was missing, add it to the weekly schedule date
                    if (missingDates.Contains(date))
                    {
                        // Get the time used/lost for this day of this week
                        var usage = tors.Where(a => a.DateUsed == date).OrderByDescending(a => a.Approved).FirstOrDefault()?.Approved ?? 0;
                        // So usage was more than zero, however if our time scheduled was zero, we need to find
                        //  an "extra" day where time was sceduled but not used. This essentially "shifts" the
                        //  usage for that date to this day and leaves that day at zero
                        if (usage > 0 && (time?.TotalMinutes ?? 0) == 0)
                        {
                            // Get the first substitute date with enough time to cover this one
                            var substituteDate = extraDates
                                .FirstOrDefault(e => sched?.Times.Any(a => a.SampleDate.DayOfWeek == e.Value.DayOfWeek && a.TotalMinutes >= usage) == true);

                            // Check if we have one, if so we're good, if not we're adding an 
                            //  entirely new scheduled day of week (increase in schedule)
                            if (substituteDate.HasValue)
                            {
                                usage = sched.Times.FirstOrDefault(a => a.SampleDate.DayOfWeek == substituteDate.Value.DayOfWeek && a.TotalMinutes >= usage)?.TotalMinutes ?? 0;
                                // Remove this from our extra dates list so we don't re-use it
                                extraDates.Remove(substituteDate.Value);
                            }
                        }
                        // Add this to the corrected schedule
                        correctedWeekSchedule.Times.Add(new Time()
                        {
                            SampleDate = date,
                            TotalMinutes = usage
                        });

                        // continue to the next day
                    }
                }

                // One last loop through these to fill in any extra dates after accouting for filling
                //  in/shifting for missing dates and good dates
                foreach (var date in week.Key.StartDate.AllDatesInRange(week.Key.EndDate))
                {
                    // Get the day of the week of scheduled time for the new schedule
                    var time = correctedWeekSchedule.Times.FirstOrDefault(t => t.SampleDate.DayOfWeek == date.DayOfWeek);
                    if (time != null)
                    {
                        // We have one aleady, move on to the next day
                        continue;
                    }
                    var sampleDate = extraDates.FirstOrDefault();
                    if (sampleDate.HasValue)
                    {
                        // Remove this sample date so we don't use it again
                        extraDates.Remove(sampleDate);
                        // Get the work schedule that covers this date
                        var sched = schedules.OrderBy(s => s.StartDate).FirstOrDefault(s => sampleDate.Value.DateInRange(s.StartDate, s.EndDate));
                        // Get the time that matches the same "day of the week" of the current date we're testing
                        time = sched.Times.FirstOrDefault(t => t.SampleDate.DayOfWeek == sampleDate.Value.DayOfWeek);
                        // Determine if we have one
                        if (time != null)
                        {
                            // Add this to the corrected schedule
                            correctedWeekSchedule.Times.Add(new Time()
                            {
                                SampleDate = date,
                                TotalMinutes = time.TotalMinutes
                            });
                            // Go do the next day
                            continue;
                        }
                    }

                    // We're here, so add a zero day to the schedule.
                    // Add this to the corrected schedule
                    correctedWeekSchedule.Times.Add(new Time()
                    {
                        SampleDate = date,
                        TotalMinutes = 0
                    });
                }

                // Add the newly built schedule for this week
                correctedWeekSchedule.Times = correctedWeekSchedule.Times.OrderBy(t => t.SampleDate).ToList();
                newSchedules.Add(correctedWeekSchedule);
            }

            // Change the End Date of the schedules to null to make it perpetual.
            newSchedules.OrderBy(s => s.StartDate).Last().EndDate = null;

            // Return the list of new schedules
            return newSchedules;
        }

        #region MeasuredDate Class

        /// <summary>
        /// Schedule Exception and Measured Usage class represents a single instance of a date
        /// with approved time and schedule for that date. Can be used for recognizing exceptions,
        /// time off, or just plain usage records.
        /// </summary>
        /// <remarks>
        /// Used for reverse-engineering work schedules.
        /// </remarks>
        class MeasuredDate
        {
            /// <summary>
            /// Gets or sets the date used.
            /// </summary>
            /// <value>
            /// The date used.
            /// </value>
            public DateTime DateUsed { get; set; }

            /// <summary>
            /// Gets or sets the scheduled.
            /// </summary>
            /// <value>
            /// The scheduled.
            /// </value>
            public int Scheduled { get; set; }

            /// <summary>
            /// Gets or sets the approved.
            /// </summary>
            /// <value>
            /// The approved.
            /// </value>
            public int Approved { get; set; }
        }

        #endregion MeasuredDate
    }
}
