﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Logic.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Process
{
    [Serializable]
    public class PolicyReportCoverPage
    {
        public PolicyReportCoverPage()
        {
            ReportDate = DateTime.Today.ToString("MM/dd/yyyy");
            Year = DateTime.Today.Year.ToString();
            ToC = new List<Tuple<string, int, int>>();
        }
        public string ReportDate { get; set; }
        public string Year { get; set; }
        public List<Tuple<string, int, int>> ToC { get; set; }
    }

    [Serializable]
    public class PolicyReportPolicy
    {
        public PolicyReportPolicy()
        {
            SelectionRules = new List<PolicyReportRuleGroup>(0);
            EligibilityRules = new List<PolicyReportRuleGroup>(0);
        }
        public PolicyReportPolicy Populate(Policy policy, List<Policy> allPolicies)
        {
            if (policy == null)
                return this;
            Code = policy.Code;
            Name = policy.Name;
            Description = policy.Description;
            EffectiveDate = policy.EffectiveDate.ToString("MM/dd/yyyy");
            PolicyType = policy.PolicyType.ToString().SplitCamelCaseString();
            if (!string.IsNullOrWhiteSpace(policy.WorkState) || !string.IsNullOrWhiteSpace(policy.ResidenceState) || policy.Gender.HasValue)
                Restrictions = string.Format("This policy is constrained to only apply to those employees that{0}{1}{2}{3}{4}{5}{6}{7}.",
                    string.IsNullOrWhiteSpace(policy.WorkState) ? "" : " work in the state of ",
                    policy.WorkState,
                    !string.IsNullOrWhiteSpace(policy.WorkState) && !string.IsNullOrWhiteSpace(policy.ResidenceState) ? " or" : "",
                    string.IsNullOrWhiteSpace(policy.ResidenceState) ? "" : " reside in the state of ",
                    policy.ResidenceState,
                    (!string.IsNullOrWhiteSpace(policy.WorkState) || !string.IsNullOrWhiteSpace(policy.ResidenceState)) && policy.Gender.HasValue ? " and that" : "",
                    policy.Gender.HasValue ? " are " : "",
                    policy.Gender.ToString().SplitCamelCaseString());
            else
                Restrictions = "This policy has no constraints on work state, state of residence or gender.";
            IsCustom = policy.IsCustom;
            if (policy.ConsecutiveTo != null && policy.ConsecutiveTo.Any())
            {
                var cons = new List<string>(policy.ConsecutiveTo.Count);
                foreach (var c in policy.ConsecutiveTo)
                {
                    var p = allPolicies.FirstOrDefault(a => a.Code == c);
                    if (p == null)
                        cons.Add(c);
                    else
                        cons.Add(string.Format("{1} ({0})", p.Code, p.Name));
                }
                ConsecutiveTo = string.Join(", ", cons);
            }
            if (policy.AbsenceReasons != null && policy.AbsenceReasons.Any())
                Reasons = policy.AbsenceReasons.Select(r => new PolicyReportPolicyAbsenceReason().Populate(r)).ToList();
            if (policy.RuleGroups != null && policy.RuleGroups.Any())
            {
                SelectionRules = policy.RuleGroups.Where(r => r.RuleGroupType == PolicyRuleGroupType.Selection).Select(r => new PolicyReportRuleGroup().Populate(r)).ToList();
                EligibilityRules = policy.RuleGroups.Where(r => r.RuleGroupType == PolicyRuleGroupType.Eligibility).Select(r => new PolicyReportRuleGroup().Populate(r)).ToList();
            }

            return this;
        }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string EffectiveDate { get; set; }
        public string PolicyType { get; set; }
        public string Restrictions { get; set; }
        public bool IsCustom { get; set; }
        public string ConsecutiveTo { get; set; }
        public string SupplementalTo { get; set; }
        public List<PolicyReportRuleGroup> SelectionRules { get; set; }
        public List<PolicyReportRuleGroup> EligibilityRules { get; set; }
        public List<PolicyReportPolicyAbsenceReason> Reasons { get; set; }
        public bool HasSelectionRules { get { return SelectionRules != null && SelectionRules.Any(); } }
        public bool HasEligibilityRules { get { return EligibilityRules != null && EligibilityRules.Any(); } }
        public bool HasReasons { get { return Reasons != null && Reasons.Any(); } }

    }

    [Serializable]
    public class PolicyReportPolicyAbsenceReason
    {
        public PolicyReportPolicyAbsenceReason()
        {
            SelectionRules = new List<PolicyReportRuleGroup>(0);
            EligibilityRules = new List<PolicyReportRuleGroup>(0);
            TimeRules = new List<PolicyReportRuleGroup>(0);
            PaymentRules = new List<PolicyReportRuleGroup>(0);
        }

        public PolicyReportPolicyAbsenceReason Populate(PolicyAbsenceReason reason)
        {
            if (reason == null)
                return this;

            if (reason.Reason != null)
            {
                ReasonName = reason.Reason.Name;
                ReasonCode = reason.Reason.Code;
            }
            Summary = reason.ToString();
            if (reason.RuleGroups != null && reason.RuleGroups.Any())
            {
                SelectionRules = reason.RuleGroups.Where(r => r.RuleGroupType == PolicyRuleGroupType.Selection).Select(r => new PolicyReportRuleGroup().Populate(r)).ToList();
                EligibilityRules = reason.RuleGroups.Where(r => r.RuleGroupType == PolicyRuleGroupType.Eligibility).Select(r => new PolicyReportRuleGroup().Populate(r)).ToList();
                TimeRules = reason.RuleGroups.Where(r => r.RuleGroupType == PolicyRuleGroupType.Time).Select(r => new PolicyReportRuleGroup().Populate(r)).ToList();
                PaymentRules = reason.RuleGroups.Where(r => r.RuleGroupType == PolicyRuleGroupType.Payment).Select(r => new PolicyReportRuleGroup().Populate(r)).ToList();
            }

            return this;
        }

        public string ReasonName { get; set; }
        public string ReasonCode { get; set; }
        public string Summary { get; set; }

        public List<PolicyReportRuleGroup> SelectionRules { get; set; }
        public List<PolicyReportRuleGroup> EligibilityRules { get; set; }
        public List<PolicyReportRuleGroup> TimeRules { get; set; }
        public List<PolicyReportRuleGroup> PaymentRules { get; set; }
        public bool HasSelectionRules { get { return SelectionRules != null && SelectionRules.Any(); } }
        public bool HasEligibilityRules { get { return EligibilityRules != null && EligibilityRules.Any(); } }
        public bool HasTimeRules { get { return TimeRules != null && TimeRules.Any(); } }
        public bool HasPaymentRules { get { return PaymentRules != null && PaymentRules.Any(); } }
    }

    [Serializable]
    public class PolicyReportRuleGroup
    {
        public PolicyReportRuleGroup()
        {
            Rules = new List<PolicyReportRule>(0);
        }
        public PolicyReportRuleGroup Populate(PolicyRuleGroup group)
        {
            if (group == null)
                return this;

            Name = group.Name;
            Description = group.Description;
            Summary = group.ToString();

            using (PolicyService policyService = new PolicyService(null, null)) { 
                var expressions = policyService.GetRuleExpressions();
                Rules = group.Rules.Select(r =>
                {
                    var rule = new PolicyReportRule
                    {
                        Text = string.Format("{0}{3}{1}, {2}", r.Name, r.Description, r, string.IsNullOrWhiteSpace(r.Description) ? "" : " - "),
                        Expression = r.ToString()
                    };
                    try
                    {
                        rule = new PolicyReportRule
                        {
                            Text = policyService.GetExpression(r, expressions).ToString(),
                            Expression = r.ToString()
                        };
                    }
                    catch { }
                    return rule;
                }).ToList();
            }
            return this;
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Summary { get; set; }
        public List<PolicyReportRule> Rules { get; set; }
        public bool HasRules { get { return Rules != null && Rules.Any(); } }
    }

    public class PolicyReportRule
    {
        public string Text { get; set; }

        public string Expression { get; set; }
    }
}
