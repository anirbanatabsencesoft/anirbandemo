﻿using AbsenceSoft.Common.Properties;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Processing;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Processing;
using AbsenceSoft.Logic.Processing.EL;
using AbsenceSoft.Logic.SSO;
using AbsenceSoft.Process.MessageProcessing;
using AbsenceSoft.Rendering;
using AbsenceSoft.Rendering.Templating.MailMerge;
using AbsenceSoft.Reporting;
using Aspose.Words;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using AbsenceSoft.Data;
using System.Net.Mail;
using System.Web;
using AT.Logic.Authentication;
using AT.Data.Authentication.Provider;
using AbsenceSoft.Logic.Notification;
using System.Security.Claims;
using AbsenceSoft.Logic.Workflows;
using MongoDB.Driver;

namespace AbsenceSoft.Process
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            // Decent test of export: -c 000000000000000000000002 -e 000000000000000000000002
            AbsenceSoft.Common.Serializers.DateTimeSerializer.Register();
            log4net.Config.XmlConfigurator.Configure();
            new License().SetLicense("Aspose.Words.lic");

            // Initialize Aspose PDF license object
            Aspose.Pdf.License license = new Aspose.Pdf.License();
            // Set license
            license.SetLicense("Aspose.Pdf.lic");
            // Set the value to indicate that license will be embedded in the application
            license.Embedded = true;

            bool export = false, process = false, caseMigration = false, import = false, jobs = false, policyReport = false, sso = false, runReport = false, dates = false, ess = false, ignoreOverlappingCases = false, recalc = false, notifications = false, itor = false;
            string fileName = null, attachmentfilePath = null, customerId = null, employerId = null, report = null, codes = null, meta = null, cert = null, format = null, reportId = null, startDate = null, endDate = null, userEmail = null, id = null;
            bool isWorkflow = false;
            string workflowCode = null, caseNumber = null;
            Guid? activityId = null;
            int? count = null, minutes = null;
            Dictionary<string, string> reportParams = null;
            bool resetApiKeys = false;
            #region Parse Arguments

            for (var i = 0; i < args.Length; i++)
            {
                string a = args[i];
                if (a == "-f")
                {
                    fileName = GetSwitchValue(args, i);
                }
                else if (a == "-c")
                {
                    customerId = GetSwitchValue(args, i);
                }
                else if (a == "-e")
                {
                    employerId = GetSwitchValue(args, i);
                }
                else if (a == "-a")
                {
                    attachmentfilePath = GetSwitchValue(args, i);
                    if (string.IsNullOrWhiteSpace(attachmentfilePath) || !Directory.Exists(attachmentfilePath))
                    {
                        Console.Error.WriteLine("The directory specified does not exist.");
                        return;
                    }
                }
                else if (a == "-n")
                {
                    string n = GetSwitchValue(args, i);
                    if (!string.IsNullOrWhiteSpace(n) && int.TryParse(n, out int ct))
                    {
                        count = ct;
                    }
                }
                else if (a == "-proc")
                {
                    process = true;
                }
                else if (a == "-export")
                {
                    export = true;
                }
                else if (a == "-case")
                {
                    caseMigration = true;
                }
                else if (a == "-wf")
                {
                    isWorkflow = true;
                }
                else if (a == "-code")
                {
                    workflowCode = GetSwitchValue(args, i);
                }
                else if (a == "-activity")
                {
                    string ai = GetSwitchValue(args, i);
                    if (!string.IsNullOrWhiteSpace(ai) && Guid.TryParse(ai, out Guid actid))
                    {
                        activityId = actid;
                    }
                    else
                    {
                        Console.Error.WriteLine("Activity Id is not correct.");
                        return;
                    }
                }
                else if (a == "-casenumber")
                {
                    caseNumber = GetSwitchValue(args, i);
                }
                else if (a == "-import")
                {
                    import = true;
                }
                else if (a == "-r")
                {
                    report = GetSwitchValue(args, i);
                }
                else if (a == "-p")
                {
                    policyReport = true;
                    fileName = GetSwitchValue(args, i);
                    codes = args.Length > (i + 2) && !args[i + 2].StartsWith("-") ? args[i + 2] : null;
                }
                else if (a == "-format")
                {
                    format = GetSwitchValue(args, i);
                }
                else if (a == "-sso")
                {
                    sso = true;
                }
                else if (a == "-ess")
                {
                    ess = true;
                }
                else if (a == "-meta")
                {
                    meta = GetSwitchValue(args, i);
                }
                else if (a == "-cert")
                {
                    cert = GetSwitchValue(args, i);
                }
                else if (a == "-report")
                {
                    runReport = true;
                    reportId = GetSwitchValue(args, i);
                }
                else if (a == "-start")
                {
                    startDate = GetSwitchValue(args, i);
                }
                else if (a == "-end")
                {
                    endDate = GetSwitchValue(args, i);
                }
                else if (a == "-email")
                {
                    userEmail = GetSwitchValue(args, i);
                }
                else if (a == "-para")
                {
                    var para = GetSwitchValue(args, i);
                    if (!string.IsNullOrWhiteSpace(para))
                    {
                        var p = para.Split('=');
                        if (p.Length == 2 && !string.IsNullOrWhiteSpace(p[0]))
                        {
                            if (reportParams == null)
                            {
                                reportParams = new Dictionary<string, string>();
                            }

                            if (reportParams.ContainsKey(p[0]))
                            {
                                reportParams[p[0]] = string.IsNullOrWhiteSpace(p[1]) ? null : p[1];
                            }
                            else
                            {
                                reportParams.Add(p[0], string.IsNullOrWhiteSpace(p[1]) ? null : p[1]);
                            }
                        }
                    }
                }
                else if (a == "-dates")
                {
                    dates = true;
                    string n = GetSwitchValue(args, i);
                    if (!string.IsNullOrWhiteSpace(n) && int.TryParse(n, out int ct))
                    {
                        minutes = ct;
                    }
                }
                else if (a == "-jobs")
                {
                    jobs = true;
                }
                else if (a == "-ignoreOverlappingCases")
                {
                    ignoreOverlappingCases = true;
                }
                else if (a == "-recalc")
                {
                    recalc = true;
                }
                else if (a == "-notification")
                {
                    notifications = true;
                }
                else if (a == "-id")
                {
                    id = GetSwitchValue(args, i);
                }
                else if (a == "-apikey")
                {
                    resetApiKeys = true;
                }
                else if (a =="-itor")
                {
                    itor = true;
                }
                else if (a.Contains("?") || a.Contains("help"))
                {
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine("Command Syntax:");
                    Console.WriteLine("    AbsenceSoft.Process.Console.exe -(proc|case|import|export) [-c <customerId>] [-e <employerId>] [-f <file path>] [-n <max>] [-r <report path>]");
                    Console.WriteLine();
                    Console.WriteLine("    -proc: Runs the queue process (mimics the 'service')");
                    Console.WriteLine("    -case: Performs a case migration, -f and -e are required");
                    Console.WriteLine("    -export: Exports an eligibilty file, -f, -c and -e OR -n are required");
                    Console.WriteLine("    -wf: Run the workflow process, -e -code and -casenumber are required");
                    Console.WriteLine("    -code <workflowCode>: Specifies the workflow code");
                    Console.WriteLine("    -activity <activityId>: Specifies the activityId");
                    Console.WriteLine("    -casenumber <caseNumber>: Specifies the caseNumber");
                    Console.WriteLine("    -import: Imports an eligibilty file, -f, -c and -e are required");
                    Console.WriteLine("    -report <reportId>: Runs a report and outputs a csv file, -f, -c, -email, -start and -end are required");
                    Console.WriteLine("    -email <email>: specifices the user's email to run the report as");
                    Console.WriteLine("    -c <customerId>: Specifies the customerId");
                    Console.WriteLine("    -e <employerId>: Specifies the employerId");
                    Console.WriteLine("    -a <attachment files path>: Specifies the attachment files path ");
                    Console.WriteLine("    -f <file path>: Specifies the file path for the requested operation (export to or import from)");
                    Console.WriteLine("    -n <max>: Specifies the max number of employees to export");
                    Console.WriteLine("    -r <report path>: The report output file name (defaults to the console)");
                    Console.WriteLine("    -p <report path> [<code(s)>]: Generate policy config report to <report path> for [<codes>]");
                    Console.WriteLine("    -format (pdf|docx): Specifies the -p output format");
                    Console.WriteLine("    -sso: Provisions an SSO profile, -c is required, -e and -f are optional, -f specifies metadata export file");
                    Console.WriteLine("    -ess: Switches context for things like SSO to be for ESS instead of the portal");
                    Console.WriteLine("    -meta <file path>: Specifies SSO metadata to import, only used with -sso option");
                    Console.WriteLine("    -cert <certificate path>: Optionally specifies the customer's X.509 certificate path to import");
                    Console.WriteLine("    -report <report id>: Specifies to run a report to CSV on the back-end (requires -f, -c, -email, -start)");
                    Console.WriteLine("    -email <user email>: The user's email address to emulate for that report run");
                    Console.WriteLine("    -start <date>: The start date for the report");
                    Console.WriteLine("    -end <date>: The optional end date for the report, if none supplied, uses tomorrow");
                    Console.WriteLine("    -para <name>=<value>*: A report parameter by name and value; value may not contain an '=' sign");
                    Console.WriteLine("    -dates <minutes>: Generates a file with all dates between -start and -end to -f for -e (distribution)");
                    Console.WriteLine("    -format <string>: The format string to use with the -dates argument for final output (0=date, 1=minutes)");
                    Console.WriteLine("    -jobs: Performs a job definition import for -e (of -c) with the file -f");
                    Console.WriteLine("    -ignoreOverlappingCases: when doing case conversion, ignores case overlap (use with -case)");
                    Console.WriteLine("    -recalc: use to recalc all open cases, use with -c and -e to target a specific employer");
                    Console.WriteLine("    -id: the id to apply to a specific entity, whether a case, employee, etc. (use with -recalc, <future>)");
                    Console.WriteLine("    -notification: handle any notifications");
                    Console.WriteLine("    -apikey: resets the customer's api key & secret");
                    Console.WriteLine("    -itor: reverse engineers work schedules for employees from ITORs (-c, -e required; -id optional)");
                    Console.WriteLine();
                    return;
                }
            }

            #endregion

            if (import)
            {
                #region Import EL Record

                #region Validation (Important when it comes to touching customer's data, duh)

                bool good = true;
                if (string.IsNullOrWhiteSpace(customerId))
                {
                    Console.Error.WriteLine("Must provide the -c <customerId> option");
                    good = false;
                }
                if (string.IsNullOrWhiteSpace(employerId))
                {
                    Console.Error.WriteLine("Must provide the -e <employerId> option");
                    good = false;
                }
                Customer cust = null;
                if (good)
                {
                    cust = Customer.GetById(customerId);
                    if (cust == null)
                    {
                        Console.Error.WriteLine("The customerId '{0}' provided was not found in the database", customerId);
                        good = false;
                    }
                    else
                        Console.WriteLine("Found customer '{0}'", cust.Name);
                }
                Employer employer = null;
                if (good)
                {
                    employer = Employer.GetById(employerId);
                    if (employer == null)
                    {
                        Console.Error.WriteLine("The employerId '{0}' provided was not found in the database", employerId);
                        good = false;
                    }
                    else
                    {
                        Console.WriteLine("Found employer '{0}'", employer.Name);
                        if (employer.CustomerId != customerId)
                        {
                            Console.Error.WriteLine("The employer provided does not have customerId '{0}', but instead '{1}'", customerId, employer.CustomerId);
                            good = false;
                        }
                    }
                }
                if (string.IsNullOrWhiteSpace(fileName))
                {
                    Console.Error.WriteLine("Must provide the -f <file path> option");
                    good = false;
                }
                var info = new FileInfo(fileName);
                if (!info.Exists)
                {
                    Console.Error.WriteLine("The specified import file, '{0}', does not exist", fileName);
                    good = false;
                }
                if (!good)
                {
                    Environment.Exit(9);
                    return;
                }
                Console.WriteLine("Are you sure you want to upload file '{0}' for '{1}' > '{2}' (Y/n)?", fileName, cust.Name, employer.Name);
                Console.Write("Enter 'Y' or 'N' and press Enter to continue: ");
                if (Console.ReadLine().ToLowerInvariant() != "y")
                {
                    Environment.Exit(-1);
                    return;
                }

                #endregion

                string locator = null;
                EligibilityUpload upload = null;
                try
                {
                    Console.WriteLine("Uploading file to S3");
                    using (FileService svc = new FileService())
                        locator = svc.UploadFile(fileName, Settings.Default.S3BucketName_Processing, customerId, employerId);
                    Console.WriteLine("File uploaded to S3 in bucket '{0}' as '{1}'", Settings.Default.S3BucketName_Processing, locator);

                    upload = new EligibilityUpload()
                    {
                        // Interesting stuff
                        FileKey = locator,
                        FileName = Path.GetFileName(fileName),
                        Size = info.Length,
                        Status = ProcessingStatus.Pending,
                        MimeType = "text/plain",
                        // Boring audit fields and identifiers
                        CreatedById = User.System.Id,
                        CreatedBy = User.System,
                        ModifiedById = User.System.Id,
                        ModifiedBy = User.System,
                        CustomerId = customerId,
                        Customer = cust,
                        EmployerId = employerId,
                        Employer = employer
                    };

                    using (EligibilityUploadService svc = new EligibilityUploadService(User.System))
                        upload = svc.CreateEligibilityDataFileUpload(upload);
                    Console.WriteLine("Saved eligibility upload with _id: '{0}' and queued for processing", upload.Id);
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex.ToString());
                    try
                    {
                        if (!upload.IsNew)
                        {
                            upload.Delete();
                            Console.WriteLine("Deleted upload record due to error");
                        }
                    }
                    catch (Exception delFileEx)
                    {
                        Console.Error.WriteLine(delFileEx.ToString());
                        Console.Error.WriteLine("Unable to delete upload record, this must be done manually, id: {0}", upload.Id);
                    }
                    try
                    {
                        if (!string.IsNullOrWhiteSpace(locator))
                        {
                            using (FileService svc = new FileService())
                                svc.DeleteS3Object(locator, Settings.Default.S3BucketName_Processing);
                            Console.WriteLine("Deleted S3 due to error");
                        }
                    }
                    catch (Exception delS3Ex)
                    {
                        Console.Error.WriteLine(delS3Ex.ToString());
                        Console.Error.WriteLine("Unable to delete S3 object, this must be done manually, locator: {0}", locator);
                    }
                }

                #endregion
            }
            else if (export)
            {
                #region Export

                fileName = fileName ?? Path.Combine(Environment.CurrentDirectory, string.Format("eligibility_file_{0:yyyyMMddHHmmss}.csv", DateTime.Now));
                if (string.IsNullOrWhiteSpace(customerId) || string.IsNullOrWhiteSpace(employerId))
                {
                    if (count.HasValue && count.Value > 0)
                    {
                        Random rand = new Random();
                        var randString = new Func<int, int, string>((min, max) =>
                        {
                            int len = rand.Next(min, max);
                            return RandomString.Generate(len, true, false, false, true);
                        });
                        var randName = new Func<int, int, string>((min, max) =>
                        {
                            int len = rand.Next(min, max);
                            return RandomString.Generate(len, true, false, false, false);
                        });
                        var randPhone = new Func<string>(() =>
                        {
                            return RandomString.Generate(10, false, false, true, false, false);
                        });
                        var randDate = new Func<int, int, bool, DateTime>((minYear, maxYear, cap) =>
                        {
                            int year = rand.Next(minYear, maxYear);
                            int month = rand.Next(1, 12);
                            int day = rand.Next(1, 28);
                            if (cap && year >= DateTime.Now.Year)
                            {
                                year = DateTime.Now.Year;
                                month = rand.Next(1, DateTime.Now.Month);
                                if (month == DateTime.Now.Month)
                                    day = rand.Next(1, DateTime.Now.Day);
                            }
                            return new DateTime(year, month, day).ToMidnight();
                        });
                        var randBoundDate = new Func<DateTime, DateTime, DateTime>((min, max) =>
                        {
                            int year = rand.Next(min.Year, max.Year);
                            int month = rand.Next(1, 12);
                            int day = rand.Next(1, DateTime.DaysInMonth(year, month));
                            if (year >= max.Year)
                            {
                                year = max.Year;
                                month = rand.Next(1, max.Month);
                                if (month == max.Month)
                                    day = rand.Next(1, max.Day);
                            }
                            if (year <= min.Year)
                            {
                                year = min.Year;
                                month = rand.Next(min.Month, 12);
                                if (month == min.Month)
                                    day = rand.Next(min.Day, DateTime.DaysInMonth(year, month));
                            }
                            return new DateTime(year, month, day).ToMidnight();
                        });
                        var randChar = new Func<char[], char>(chars =>
                        {
                            int idx = rand.Next(0, chars.Length);
                            return chars[idx];
                        });
                        var randAlphaChar = new Func<char>(() =>
                        {
                            return (char)rand.Next((int)'A', (int)'Z');
                        });
                        var randByte = new Func<int, int, byte>((min, max) =>
                        {
                            return Convert.ToByte(rand.Next(min, max));
                        });
                        var randState = new Func<string>(() =>
                        {
                            List<string> states = new List<string>() { "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY" };
                            int idx = rand.Next(0, states.Count);
                            return states[idx];
                        });
                        using (var writer = new StreamWriter(fileName))
                        {
                            for (int i = 0; i < count.Value; i++)
                            {
                                EligibilityRow row = new EligibilityRow()
                                {
                                    CustomerId = customerId,
                                    EmployerId = employerId
                                };
                                row.RecType = EligibilityRow.RecordType.Eligibility;
                                row.Delimiter = ',';

                                row.Address = string.Concat(rand.Next(100, 9999), " ", randString(8, 50));
                                row.Address2 = i % 6 == 0 ? null : randString(8, 50);
                                row.AdjustedServiceDate = randDate(2007, DateTime.Now.Year, true);
                                row.City = randString(6, 30);
                                row.Country = "US";
                                row.DateOfBirth = randDate(1955, DateTime.Now.Year - 16, true);
                                row.Email = string.Concat(randName(3, 50), "@absencetracker.com");
                                row.EmployeeNumber = string.Concat(randAlphaChar(), randAlphaChar(), i.ToString().PadLeft(6, '0'));
                                row.EmploymentStatus = randChar(new char[] { (char)EmploymentStatus.Active, (char)EmploymentStatus.Inactive });
                                if (i % rand.Next(77, 1023) == 0)
                                    row.EmploymentStatus = (char)EmploymentStatus.Terminated;
                                row.HireDate = row.AdjustedServiceDate;
                                if (row.EmploymentStatus == (char)EmploymentStatus.Terminated)
                                    row.TerminationDate = randBoundDate(row.HireDate.Value.AddDays(1), DateTime.Now);
                                row.EmploymentType = string.Concat(randByte(1, 2));
                                row.ExemptionStatus = randChar(new char[] { 'E', 'N' });
                                row.FirstName = randName(4, 20);
                                row.Gender = randChar(new char[] { (char)Gender.Female, (char)Gender.Male });
                                row.HoursWorkedIn12Months = Convert.ToDecimal(rand.Next(0, 2080));
                                row.JobLocation = randString(6, 20);
                                row.JobTitle = randString(10, 30);
                                row.KeyEmployee = i % rand.Next(72, 1065) == 0 ? true : false;
                                row.LastName = randName(6, 30);
                                row.Meets50In75 = i % rand.Next(18, 97) == 0 ? false : true;
                                row.MiddleName = i % 4 == 0 ? null : randName(6, 30);
                                row.MilitaryStatus = randByte(0, 2);
                                row.MinutesPerWeek = rand.Next(1, 2400);
                                row.PayType = randByte(1, 2);
                                if (row.PayType == (byte)PayType.Salary)
                                    row.PayRate = rand.Next(14600, 750000);
                                else
                                    row.PayRate = Convert.ToDecimal(rand.Next(1000, 12000)) / 100;
                                row.PhoneHome = randPhone();
                                row.PhoneMobile = randPhone();
                                row.PhoneWork = randPhone();
                                row.PostalCode = randPhone().Substring(0, 5);
                                row.RehireDate = null;
                                row.State = randState();
                                row.VariableSchedule = false;
                                row.WorkCountry = "US";
                                row.WorkState = randState();

                                writer.WriteLine(row.ToString());
                            }
                        }
                    }
                }
                else
                {
                    if (caseMigration)
                    {
                        using (CaseMigrationService svc = new CaseMigrationService(employerId, report))
                        {
                            svc.ExportFile(fileName);
                        }
                    }
                    else
                    {
                        using (var writer = new StreamWriter(fileName))
                        {
                            var employees = Employee.AsQueryable().Where(e => e.CustomerId == customerId && e.EmployerId == employerId);
                            if (count.HasValue)
                                employees = employees.Take(count.Value);
                            foreach (var emp in employees)
                            {
                                EligibilityRow row = new EligibilityRow(emp);
                                writer.WriteLine(row.ToString());
                            }
                        }
                    }
                }

                #endregion
            }
            else if (caseMigration)
            {
                #region Case Migration

                if (string.IsNullOrWhiteSpace(fileName) || !File.Exists(fileName))
                {
                    Console.Error.WriteLine("The file specified does not exist.");
                    return;
                }
                if (ignoreOverlappingCases)
                {
                    Logic.Cases.CaseService.SetIgnoreOverlappingCases(true);
                }
                using (CaseMigrationService svc = new CaseMigrationService(employerId, report, attachmentfilePath))
                {
                    if (svc.ProcessFile(fileName))
                        Console.WriteLine("Finished Case Migration!");
                    else
                        Console.WriteLine("Uh oh, something went wrong with Case Migration, better check it out!");
                }

                #endregion
            }
            else if (isWorkflow)
            {
                #region Workflow Activity Run
                if (string.IsNullOrEmpty(workflowCode))
                {
                    Console.Error.WriteLine("Please provide Workflow code.");
                    return;
                }
                try
                {
                    Employer wfEmployer = Employer.GetById(employerId);
                    if (wfEmployer == null)
                    {
                        Console.Error.WriteLine(string.Format("Unable to find Employer for employerId \"{0}\".", employerId));
                        return;
                    }
                    Customer wfCustomer = Customer.GetById(wfEmployer.CustomerId);
                    if (wfCustomer == null)
                    {
                        Console.Error.WriteLine(string.Format("Unable to find Customer for employerId \"{0}\".", employerId));
                        return;
                    }

                    List<IMongoQuery> ands = new List<IMongoQuery>();
                    ands.Add(Case.Query.EQ(c => c.CustomerId, wfEmployer.CustomerId));
                    ands.Add(Case.Query.EQ(c => c.EmployerId, employerId));
                    ands.Add(Case.Query.EQ(c => c.CaseNumber, caseNumber));

                    Case theCase = Case.Query.Find(Case.Query.And(ands)).FirstOrDefault();

                    if (theCase == null)
                    {
                        Console.Error.WriteLine(string.Format("Unable to find Case Number \"{0}\".", caseNumber));
                        return;
                    }
                    using (WorkflowService wf = new WorkflowService(wfCustomer, wfEmployer, User.System))
                    {
                        wf.Run(workflowCode, theCase.Id, activityId);
                        Console.WriteLine(string.Format("Workflow \"{0}\" was successful ran for case # {1}.", workflowCode, caseNumber));
                    }
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex.ToString());
                    Console.Error.WriteLine("Unable to run workflow activity for given caseNumber {0}.", caseNumber);
                    return;
                }
                #endregion
            }
            else if (policyReport)
            {
                #region Policy Report

                if (string.IsNullOrWhiteSpace(fileName))
                {
                    Console.Error.WriteLine("The file to export the policy report to was not provided.");
                    return;
                }
                if (File.Exists(fileName))
                {
                    Console.Write("The file already exists... Overwrite? (Y/N): ");
                    if (!Regex.IsMatch(@"([Yy](es)?)", Console.ReadLine(), RegexOptions.CultureInvariant | RegexOptions.ExplicitCapture))
                        return;
                }
                if (string.IsNullOrWhiteSpace(format))
                    format = "pdf";
                if (format != "pdf" && format != "docx")
                {
                    Console.Error.WriteLine("The file format to export the policy report to was invalid, should be pdf or docx.");
                    return;
                }

                Console.WriteLine("Pulling policies to report on{0}{1}",
                    string.IsNullOrWhiteSpace(customerId) ? " from core configuration" : " for the specified customer",
                    string.IsNullOrWhiteSpace(employerId) ? "" : " and the specified employer");

                var allPolicies = Policy.AsQueryable().Where(p => p.CustomerId == customerId && p.EmployerId == employerId).OrderBy(p => p.Code).ToList();
                if (!string.IsNullOrWhiteSpace(codes))
                {
                    var polCodes = codes.Split(',');
                    allPolicies = allPolicies.Where(p => polCodes.Contains(p.Code)).ToList();
                }
                Console.WriteLine("Found {0} policies to report on", allPolicies.Count);
                if (format == "pdf")
                {
                    List<byte[]> documents = new List<byte[]>(allPolicies.Count);
                    int currentPage = 1;
                    PolicyReportCoverPage coverPage = new PolicyReportCoverPage();
                    foreach (var policy in allPolicies)
                    {
                        Console.WriteLine("Generating report for {0} ({1})", policy.Name, policy.Code);
                        var rpt = new PolicyReportPolicy().Populate(policy, allPolicies);
                        string html = Rendering.Template.RenderTemplate(AbsenceSoft.Process.Properties.Resources.PolicyReport, rpt);
                        int pages;
                        documents.Add(Rendering.Pdf.ConvertHtmlToPdf(html, out pages));
                        coverPage.ToC.Add(new Tuple<string, int, int>(policy.Name, currentPage, currentPage + pages - 1));
                        currentPage += pages;
                        Console.WriteLine("Successfully generated report for {0} ({1})", policy.Name, policy.Code);
                    }
                    Console.WriteLine("Stitching PDF reports together to final output for all policies...");
                    var mergedPdf = Rendering.Pdf.MergePdfDocuments(documents.ToArray());
                    Console.WriteLine("Finished stitching PDF reports together to final output for all policies");
                    Console.WriteLine("Generating Cover page and ToC...");
                    var coverHtml = Rendering.Template.RenderTemplate(AbsenceSoft.Process.Properties.Resources.PolicyReportCoverPage, coverPage);
                    int coverPages;
                    var coverPdf = Pdf.ConvertHtmlToPdf(coverHtml, out coverPages);
                    coverPage.ToC = coverPage.ToC.Select(t => new Tuple<string, int, int>(t.Item1, t.Item2 + coverPages, t.Item3 + coverPages)).ToList();
                    coverHtml = Rendering.Template.RenderTemplate(AbsenceSoft.Process.Properties.Resources.PolicyReportCoverPage, coverPage);
                    coverPdf = Pdf.ConvertHtmlToPdf(coverHtml);
                    Console.WriteLine("Finished generating Cover page and ToC; Creating final output...");
                    var finalPdf = Rendering.Pdf.MergePdfDocumentsWithPageNumbers(new byte[][] { coverPdf, mergedPdf });
                    File.WriteAllBytes(fileName, finalPdf);
                    Console.WriteLine("Successfully Exported Report to {0}", fileName);
                }
                else
                {
                    Aspose.Words.Document doc = new Aspose.Words.Document();
                    DocumentBuilder builder = new DocumentBuilder(doc);
                    builder.InsertHtml(@"
<table cellpadding=""0"" cellspacing=""0"" border=""0"" width=""100%"" style=""width:100%;"">
        <tr>
            <td>
                <h1>AbsenceTracker &trade;</h1>
                <i>&copy; " + DateTime.Today.Year.ToString() + @" - AbsenceSoft LLC</i>
                <hr />
            </td>
        </tr>
        <tr>
            <td>
                <p style=""font-size:12pt;padding:12px;"">Company Confidential Information - To Be Read By Authorized Parties Only: The information in this document is confidential, proprietary, or 
                privileged and may be subject to protection under the law, including the Electronic Communications Protection Act and the Health 
                Insurance Portability and Accountability Act. This document is intended for the sole use of the individual or entity to which explicit permission has been granted. 
                You are hereby notified that any unauthorized use, distribution, or copying of this document is strictly prohibited and may 
                subject you to criminal or civil penalties.</p>
                <hr />
            </td>
        </tr>
        <tr>
            <td valign=""middle"" align=""center"" style=""width:100%;"" width=""100%"">
                <span style=""font-size:16pt;font-weight:600;"">Policy Configuration Audit Report</span>
                <hr />
            </td>
        </tr>
        <tr>
            <td><div style=""height:8px;""></div></td>
        </tr>
        <tr>
            <td valign=""middle"" align=""center"" style=""width:100%;font-size:8pt;font-style:italic;"" width=""100%"">Generated on " + DateTime.Today.ToShortDateString() + @"</td>
        </tr>
        <tr>
            <td><div style=""height:20px;""></div></td>
        </tr>
</table>
<p></p>");
                    builder.InsertTableOfContents("\\o \"1-1\" \\h \\z \\u");
                    foreach (var policy in allPolicies)
                    {
                        Console.WriteLine("Generating report for {0} ({1})", policy.Name, policy.Code);
                        var rpt = new PolicyReportPolicy().Populate(policy, allPolicies);
                        string html = Rendering.Template.RenderTemplate(AbsenceSoft.Process.Properties.Resources.PolicyReport, rpt);
                        builder.InsertHtml(html);
                        Console.WriteLine("Successfully generated report for {0} ({1})", policy.Name, policy.Code);
                    }
                    doc.UpdateFields();
                    doc.Save(fileName, SaveFormat.Docx);
                    Console.WriteLine("Successfully Exported Report to {0}", fileName);
                }

                #endregion Policy Report
            }
            else if (sso)
            {
                #region SSO

                #region Validation

                bool good = true;
                if (string.IsNullOrWhiteSpace(customerId))
                {
                    Console.Error.WriteLine("Must provide the -c <customerId> option");
                    good = false;
                }
                Customer cust = null;
                if (good)
                {
                    cust = Customer.GetById(customerId);
                    if (cust == null)
                    {
                        Console.Error.WriteLine("The customerId '{0}' provided was not found in the database", customerId);
                        good = false;
                    }
                    else
                        Console.WriteLine("Found customer '{0}'", cust.Name);
                }
                Employer employer = null;
                if (good && !string.IsNullOrWhiteSpace(employerId))
                {
                    employer = Employer.GetById(employerId);
                    if (employer == null)
                    {
                        Console.Error.WriteLine("The employerId '{0}' provided was not found in the database", employerId);
                        good = false;
                    }
                    else
                    {
                        Console.WriteLine("Found employer '{0}'", employer.Name);
                        if (employer.CustomerId != customerId)
                        {
                            Console.Error.WriteLine("The employer provided does not have customerId '{0}', but instead '{1}'", customerId, employer.CustomerId);
                            good = false;
                        }
                    }
                }
                if (good && !string.IsNullOrWhiteSpace(cert))
                {
                    var info = new FileInfo(cert);
                    if (!info.Exists)
                    {
                        Console.Error.WriteLine("The specified X.509 certificate file, '{0}', does not exist", cert);
                        good = false;
                    }
                }
                if (good && string.IsNullOrWhiteSpace(fileName))
                {
                    string name = cust.Name;
                    Path.GetInvalidFileNameChars().ForEach(c => name = name.Replace(c, '_'));
                    string env = "";
#if RELEASE
                    env = "PROD";
#elif STAGE
                    env = "STAGE";
#else
                    env = "TEST";
#endif
                    fileName = string.Format("meta-{1}-{0}.txt", name, env);
                }
                if (good && !string.IsNullOrWhiteSpace(meta))
                {
                    var info = new FileInfo(meta);
                    if (!info.Exists)
                    {
                        Console.Error.WriteLine("The specified metadata file, '{0}', does not exist", meta);
                        good = false;
                    }
                }
                if (!good)
                {
                    Environment.Exit(9);
                    return;
                }
                if (employerId != null)
                    ess = true;
                Console.WriteLine("Are you sure you want to provision SSO, export metadata to file '{0}' for '{1}'{2}{3}{4}{5}{6}{7} (Y/n)?",
                    fileName,
                    cust.Name,
                    string.IsNullOrWhiteSpace(cert) ? "" : " and import X.509 cert '",
                    cert,
                    string.IsNullOrWhiteSpace(cert) ? "" : "'",
                    string.IsNullOrWhiteSpace(meta) ? "" : " and import metadata '",
                    meta,
                    string.IsNullOrWhiteSpace(meta) ? "" : "'");
                Console.Write("Enter 'Y' or 'N' and press Enter to continue: ");
                if (Console.ReadLine().ToLowerInvariant() != "y")
                {
                    Environment.Exit(-1);
                    return;
                }

                #endregion

                Console.WriteLine("Getting {1}, '{0}', SSO Profile", employer == null ? cust.Name : employer.Name, employer == null ? "Customer" : "Employer");

                SsoProfile profile = SsoProfile.AsQueryable().Where(p => p.CustomerId == cust.Id && p.EmployerId == (employer == null ? null : employer.Id)).FirstOrDefault();
                if (profile == null)
                {
                    Console.WriteLine("No existing profile found, creating a new one");
                    profile = new SsoProfile() { Customer = cust, Employer = employer };
                }
                else
                    Console.WriteLine("SSO Profile found with _id: ObjectId(\"{0}\")", profile.Id);
                if (profile.LocalCertificate == null)
                {
                    Console.WriteLine("Generating local X.509 Encryption certificate");
                    profile.LocalCertificate = cust.GeneratePfx();
                    Console.WriteLine("Generated local X.509 Encryption certificate with Thumbprint, {0}", profile.LocalCertificate.Thumbprint);
                }
                Console.WriteLine("Generating SP metadata");
                profile.SpMetadata = SsoConfiguration.ExportMetadata(profile, ess);
                Console.WriteLine("Successfully generated SP metadata");
                if (!string.IsNullOrWhiteSpace(meta))
                {
                    Console.WriteLine("Importing IdP metadata from '{0}'", meta);
                    SsoConfiguration.ImportMetadata(File.ReadAllText(meta), profile);
                    Console.WriteLine("Successfully imported IdP metadata from '{0}'", meta);
                }
                if (!string.IsNullOrWhiteSpace(cert))
                {
                    Console.WriteLine("Importing IdP signing certificate from '{0}'", cert);
                    profile.PartnerCertificate = File.ReadAllBytes(cert);
                    Console.WriteLine("Successfully imported IdP signing certificate from '{0}'", cert);
                }

                Console.WriteLine("Saving SSO Profile");
                profile.Save();
                Console.WriteLine("Successfully saved SSO Profile");

                if (!string.IsNullOrWhiteSpace(fileName))
                {
                    if (File.Exists(fileName))
                    {
                        Console.WriteLine("Are you sure you want to overwrite metadata file '{0}'? (Y/n)?", fileName);
                        Console.Write("Enter 'Y' or 'N' and press Enter to continue: ");
                        if (Console.ReadLine().ToLowerInvariant() != "y")
                        {
                            Environment.Exit(-1);
                            return;
                        }
                    }
                    Console.WriteLine("Exporting SP metadata to '{0}'", fileName);
                    File.WriteAllText(fileName, profile.SpMetadata, System.Text.Encoding.UTF8);
                    Console.WriteLine("Successfully exported SP metadata to '{0}'", fileName);
                }

                #endregion SSO
            }
            else if (runReport)
            {
                RunReport(customerId, reportId, userEmail, fileName, startDate, endDate, reportParams);
            }
            else if (dates)
            {
                DoDateDistributionExport(employerId, fileName, startDate, endDate, minutes, format);
            }
            else if (jobs)
            {
                #region Validation (Important when it comes to touching customer's data, duh)

                bool good = true;
                if (string.IsNullOrWhiteSpace(customerId))
                {
                    Console.Error.WriteLine("Must provide the -c <customerId> option");
                    good = false;
                }
                if (string.IsNullOrWhiteSpace(employerId))
                {
                    Console.Error.WriteLine("Must provide the -e <employerId> option");
                    good = false;
                }
                Customer cust = null;
                if (good)
                {
                    cust = Customer.GetById(customerId);
                    if (cust == null)
                    {
                        Console.Error.WriteLine("The customerId '{0}' provided was not found in the database", customerId);
                        good = false;
                    }
                    else
                        Console.WriteLine("Found customer '{0}'", cust.Name);
                }
                Employer employer = null;
                if (good)
                {
                    employer = Employer.GetById(employerId);
                    if (employer == null)
                    {
                        Console.Error.WriteLine("The employerId '{0}' provided was not found in the database", employerId);
                        good = false;
                    }
                    else
                    {
                        Console.WriteLine("Found employer '{0}'", employer.Name);
                        if (employer.CustomerId != customerId)
                        {
                            Console.Error.WriteLine("The employer provided does not have customerId '{0}', but instead '{1}'", customerId, employer.CustomerId);
                            good = false;
                        }
                    }
                }
                if (string.IsNullOrWhiteSpace(fileName))
                {
                    Console.Error.WriteLine("Must provide the -f <file path> option");
                    good = false;
                }
                var info = new FileInfo(fileName);
                if (!info.Exists)
                {
                    Console.Error.WriteLine("The specified import file, '{0}', does not exist", fileName);
                    good = false;
                }
                if (!good)
                {
                    Environment.Exit(9);
                    return;
                }
                Console.WriteLine("Are you sure you want to process jobs file '{0}' for '{1}' > '{2}' (Y/n)?", fileName, cust.Name, employer.Name);
                Console.Write("Enter 'Y' or 'N' and press Enter to continue: ");
                if (Console.ReadLine().ToLowerInvariant() != "y")
                {
                    Environment.Exit(-1);
                    return;
                }

                #endregion

                DoJobImport(cust, employer, fileName);
            }
            else if (recalc)
            {
                PerformRecalc(customerId, employerId, id);
            }

            if (notifications)
            {
                ProcessNotifications();
            }

            if (itor)
            {
                // **** Usage ****
                // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                // AbsenceSoft.Process.Console.exe -itor -c <customer_id> -e <employer_id> -id <[employee_id]>
                // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                // **** Sample ****
                // Select Medical
                // -c 59f0ab28c5f33013d06e40f4 -e 59f0b159c5f33013d06e43e6
                // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                // Athelda Whittington (EEID: 227350), Case # 227350-1;   -itor -c 59f0ab28c5f33013d06e40f4 -e 59f0b159c5f33013d06e43e6 -id 5a86dd503de096175c9ab4b4
                // Beverly Newman      (EEID: 130133), Case # 130133-1/2; -itor -c 59f0ab28c5f33013d06e40f4 -e 59f0b159c5f33013d06e43e6 -id 5a86dd423de096175c9a0190
                // Heather O’Malley    (EEID: 236503), Case # 236503-1;   -itor -c 59f0ab28c5f33013d06e40f4 -e 59f0b159c5f33013d06e43e6 -id 5a86dd433de096175c9a039c
                // Tammy Knight        (EEID: 239227), Case # 239227-4;   -itor -c 59f0ab28c5f33013d06e40f4 -e 59f0b159c5f33013d06e43e6 -id 5a86dd3b3de096175c99a6ab
                // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                ReverseEngineerEmployeeWorkSchedulesFromIntermittentTimeOff(customerId, employerId, id);
            }

            if (process)
            {
                #region Process Queue

                try
                {
                    Console.WriteLine("Processor Starting...");
                    ProcessTimer.StartProcessing();
                    Console.WriteLine("Processor Started. Press Enter to stop.");
                    Console.ReadLine();
                    Console.WriteLine("Processor Stopping...");
                    ProcessTimer.StopProcessing();
                    Console.WriteLine("Processor Stopped. Press Enter to exit.");
                    Console.ReadLine();
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex.ToString());
                    Console.ReadLine();
                }

                #endregion
            }

            if (resetApiKeys)
            {
                ResetCustomerApiKeys(customerId);
            }
        }

        /// <summary>
        /// Reverses engineers the employee work schedules from intermittent time off.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <param name="employeeId">The employee identifier.</param>
        /// <exception cref="NotImplementedException"></exception>
        private static void ReverseEngineerEmployeeWorkSchedulesFromIntermittentTimeOff(string customerId, string employerId, string employeeId = null)
        {
            #region Validation

            bool good = true;
            if (string.IsNullOrWhiteSpace(customerId))
            {
                Console.Error.WriteLine("Must provide the -c <customerId> option");
                good = false;
            }
            if (string.IsNullOrWhiteSpace(employerId))
            {
                Console.Error.WriteLine("Must provide the -e <employerId> option");
                good = false;
            }
            Customer cust = null;
            if (good)
            {
                cust = Customer.GetById(customerId);
                if (cust == null)
                {
                    Console.Error.WriteLine("The customerId '{0}' provided was not found in the database", customerId);
                    good = false;
                }
                else
                    Console.WriteLine("Found customer '{0}'", cust.Name);
            }
            Employer employer = null;
            if (good)
            {
                employer = Employer.GetById(employerId);
                if (employer == null)
                {
                    Console.Error.WriteLine("The employerId '{0}' provided was not found in the database", employerId);
                    good = false;
                }
                else
                {
                    Console.WriteLine("Found employer '{0}'", employer.Name);
                    if (employer.CustomerId != customerId)
                    {
                        Console.Error.WriteLine("The employer provided does not have customerId '{0}', but instead '{1}'", customerId, employer.CustomerId);
                        good = false;
                    }
                }
            }
            Employee employee = null;
            if (good && !string.IsNullOrWhiteSpace(employeeId))
            {
                employee = Employee.GetById(employeeId);
                if (employee == null)
                {
                    Console.Error.WriteLine("The employeeId '{0}' provided was not found in the database", employeeId);
                    good = false;
                }
                else
                {
                    Console.WriteLine("Found employee '{0}'", employee.FullName);
                    if (employee.EmployerId != employerId)
                    {
                        Console.Error.WriteLine("The employee provided does not have employerId '{0}', but instead '{1}'", employerId, employee.EmployerId);
                        good = false;
                    }
                }
            }
            if (!good)
            {
                Environment.Exit(9);
                return;
            }

            #endregion

            new ReverseEngineeredWorkSchedule()
            {
                Customer = cust,
                Employer = employer
            }.CorrectSchedules(employee);
        }

        static void PerformRecalc(string customerId, string employerId, string caseId)
        {
            #region Validation

            var good = true;
            Customer cust = null;
            if (!string.IsNullOrWhiteSpace(customerId))
            {
                cust = Customer.GetById(customerId);
                if (cust == null)
                {
                    Console.Error.WriteLine($"The customerId '{customerId}' provided was not found in the database");
                    good = false;
                }
                else
                {
                    Console.WriteLine("Found customer '{0}'", cust.Name);
                }
            }
            Employer employer = null;
            if (!string.IsNullOrWhiteSpace(employerId))
            {
                employer = Employer.GetById(employerId);
                if (employer == null)
                {
                    Console.Error.WriteLine($"The employerId '{employerId}' provided was not found in the database");
                    good = false;
                }
                else
                {
                    Console.WriteLine($"Found employer '{employer.Name}'");
                    if (employer.CustomerId != customerId)
                    {
                        Console.Error.WriteLine($"The employer provided does not have customerId '{customerId}', but instead '{employer.CustomerId}'");
                        good = false;
                    }
                }
            }
            if (!string.IsNullOrWhiteSpace(caseId) && !Case.AsQueryable().Any(c => c.Id == caseId))
            {
                Console.Error.WriteLine($"The case id '{caseId}' provided was not found in the database");
                good = false;
            }

            if (!good)
            {
                Environment.Exit(9);
                return;
            }

            #endregion Validation

            Console.WriteLine("BEGIN: Case Recalc");

            var affectedCases = Case.AsQueryable();
            if (cust != null)
            {
                affectedCases = affectedCases.Where(r => r.CustomerId == cust.Id);
            }
            if (employer != null)
            {
                affectedCases = affectedCases.Where(r => r.EmployerId == employer.Id);
            }
            if (!string.IsNullOrWhiteSpace(caseId))
            {
                affectedCases = affectedCases.Where(r => r.Id == caseId);
            }
            affectedCases = affectedCases.Where(r => r.Status == CaseStatus.Open);

            var casesToRecalc = affectedCases.ToList();
            casesToRecalc = casesToRecalc
                .OrderBy(c => c.CustomerId)
                .ThenBy(c => c.EmployerId)
                .ThenBy(c => c.Employee.Id)
                .ThenBy(c => c.StartDate)
                .ToList();

            var customerIds = casesToRecalc.Select(r => r.CustomerId).Distinct().ToList();
            foreach (var custId in customerIds)
            {
                var customer = cust?.Id == custId ? cust : Customer.GetById(custId);
                if (customer == null)
                {
                    Console.Error.WriteAsync($"ERROR: Could not find customer with id, ObjectId(\"{custId}\").");
                    continue;
                }

                Console.WriteLine($"BEGIN Recalculate cases: Customer={customer.Name}; Id={customer.Id}");

                var employerIds = casesToRecalc.Where(r => r.CustomerId == custId).Select(r => r.EmployerId).Distinct();
                foreach (var eplrId in employerIds)
                {
                    var eplr = employer?.Id == eplrId ? employer : Employer.GetById(eplrId);
                    if (eplr == null)
                    {
                        Console.Error.WriteLine($"ERROR: Could not find employer for customer, \"{customer.Name}\", with id, ObjectId(\"{custId}\").");
                        continue;
                    }

                    Console.WriteLine($"BEGIN Recalculate cases: Employer={eplr.Name}; Id={eplr.Id}");

                    var employerCases = casesToRecalc.Where(r => r.CustomerId == custId && r.EmployerId == eplrId).ToList();

                    var holidayYears = employerCases
                        .SelectMany(c => c.StartDate.AllYearsInRange(c.EndDate))
                        .Distinct()
                        .ToList();

                    var holidays = new HolidayService().GetHolidayList(holidayYears, eplr);

                    foreach (var @case in employerCases)
                    {
                        var segmentFilter = new Func<CaseSegment, bool>(segment =>
                            segment.Type != CaseType.Administrative
                            && segment.Type != CaseType.None
                            && segment.AppliedPolicies != null
                            && segment.AppliedPolicies.Any());

                        if (@case.Segments == null || !@case.Segments.Any(segmentFilter))
                        {
                            continue;
                        }

                        Console.WriteLine($"BEGIN Recalculate case: CaseNumber={@case.CaseNumber}; Id={@case.Id}");

                        foreach (var segment in @case.Segments.Where(segmentFilter))
                        {
                            foreach (var policy in segment.AppliedPolicies)
                            {
                                if (policy.Usage == null || !policy.Usage.Any())
                                {
                                    continue;
                                }

                                var holidayAndZeroDayUsageDeniedForExhaustion = policy.Usage.OrderBy(u => u.DateUsed)
                                    .Where(u => u.Determination == AdjudicationStatus.Denied
                                        && u.DenialReasonCode == AdjudicationDenialReason.Exhausted
                                        && (u.MinutesInDay <= 0D || holidays.Contains(u.DateUsed)))
                                    .ToList();

                                if (!holidayAndZeroDayUsageDeniedForExhaustion.Any())
                                {
                                    continue;
                                }

                                var getApprovedUsageDayBeforeZeroExhaustion = new Func<DateTime, AppliedPolicyUsage>(minDate =>
                                    policy.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Approved
                                        && u.DateUsed == minDate.AddDays(-1)));

                                foreach (var usage in holidayAndZeroDayUsageDeniedForExhaustion)
                                {
                                    // Continue to increment our test date so we can capture 
                                    //  contiguous holidays/zero days that should have been approved
                                    var approvedUsageDayBeforeZeroExhaustion = getApprovedUsageDayBeforeZeroExhaustion(usage.DateUsed);
                                    if (approvedUsageDayBeforeZeroExhaustion == null)
                                    {
                                        break;
                                    }

                                    Console.WriteLine($"ADJUST case: CaseNumber={@case.CaseNumber}; Id={@case.Id}; Zero time workday on '{usage.DateUsed:MM/dd/yyyy}' for policy '{policy.Policy.Code}' was exhausted when the day before, '{approvedUsageDayBeforeZeroExhaustion.DateUsed:MM/dd/yyyy}' was approved, extending approval through '{usage.DateUsed:MM/dd/yyyy}'.");

                                    usage.Determination = approvedUsageDayBeforeZeroExhaustion.Determination;
                                    usage.DenialReasonCode = null;
                                    usage.DenialReasonName = null;
                                    usage.DenialExplanation = null;
                                    usage.MinutesUsed = usage.MinutesInDay;
                                    usage.UserEntered = approvedUsageDayBeforeZeroExhaustion.UserEntered;
                                    usage.DeterminedById = approvedUsageDayBeforeZeroExhaustion.DeterminedById;
                                    usage.IntermittentDetermination = approvedUsageDayBeforeZeroExhaustion.IntermittentDetermination;
                                    usage.IsLocked = approvedUsageDayBeforeZeroExhaustion.IsLocked;
                                }
                            }
                        }

                        try
                        {
                            using (CaseService svc = new CaseService(true))
                            {
                                svc.RunCalcs(@case);
                                svc.UpdateCase(@case);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.Error.WriteLine($"ERROR: CaseNumber={@case.CaseNumber}, ex={ex}");
                        }

                        Console.WriteLine($"END Recalculate case: CaseNumber={@case.CaseNumber}; Id={@case.Id}");
                    }

                    Console.WriteLine($"END Recalculate cases: Employer={eplr.Name}; Id={eplr.Id}");
                }

                Console.WriteLine($"END Recalculate cases: Customer={customer.Name}; Id={customer.Id}");
            }

            Console.WriteLine("COMPLETE: Case Recalc");

        }


        static void DoDateDistributionExport(string employerId, string fileName, string startDate, string endDate, int? minutes, string format)
        {
            DateTime start = new DateTime();
            DateTime end = new DateTime();
            Employer emp = null;

            if (string.IsNullOrWhiteSpace(employerId))
            {
                Console.Error.WriteLine("You must provide an employer ID.");
                return;
            }

            emp = Employer.GetById(employerId);
            if (emp == null)
            {
                Console.Error.WriteLine("You must provide a valid employer ID. Employer not found.");
                return;
            }

            if (string.IsNullOrWhiteSpace(startDate) || !DateTime.TryParse(startDate, out start))
            {
                Console.Error.WriteLine("You must provide a valid start date.");
                return;
            }

            if (string.IsNullOrWhiteSpace(endDate) || !DateTime.TryParse(endDate, out end))
            {
                Console.Error.WriteLine("You must provide a valid start date.");
                return;
            }

            if (!minutes.HasValue)
            {
                Console.Error.WriteLine("You must provide the number of minutes to distribute.");
                return;
            }

            if (string.IsNullOrWhiteSpace(fileName))
            {
                Console.Error.WriteLine("You must provide a location to save the exported list to.");
                return;
            }

            if (File.Exists(fileName))
            {
                Console.Write("The file already exists... Overwrite? (Y/N): ");
                if (!Regex.IsMatch(@"([Yy](es)?)", Console.ReadLine(), RegexOptions.CultureInvariant | RegexOptions.ExplicitCapture))
                    return;
            }

            List<DateTime> holidays = new HolidayService().GetHolidayList(start, end, emp);
            List<DateTime> days = start.AllDatesInRange(end).Where(d => d.IsWeekday()).Except(holidays).ToList();

            int dayCount = days.Count;
            int minutesPerDay = Convert.ToInt32(Math.Floor((decimal)(minutes.Value / dayCount)));
            int diff = minutes.Value - (minutesPerDay * dayCount);

            using (StreamWriter writer = new StreamWriter(fileName, false))
                foreach (var dt in days.OrderBy(d => d))
                {
                    int m = minutesPerDay;
                    if (diff > 0)
                    {
                        m++;
                        diff--;
                    }
                    writer.WriteLine(string.Format(format ?? "{0:MM/dd/yyyy},{1}", dt, m));
                }
        }

        static void RunReport(string customerId, string reportId, string userEmail, string fileName, string startDate, string endDate, Dictionary<string, string> parameters)
        {

            DateTime start = new DateTime();
            DateTime end = new DateTime();
            User user = User.AsQueryable().FirstOrDefault(u => u.CustomerId == customerId && u.Email == userEmail);
            if (string.IsNullOrWhiteSpace(customerId))
            {
                Console.Error.WriteLine("You must provide a customerId.");
                return;
            }

            if (string.IsNullOrWhiteSpace(userEmail) || user == null)
            {
                Console.Error.WriteLine("You must provide a valid user.");
                return;
            }


            if (string.IsNullOrWhiteSpace(reportId))
            {
                Console.Error.WriteLine("You must provide a reportId.");
                return;
            }

            if (string.IsNullOrWhiteSpace(startDate) || !DateTime.TryParse(startDate, out start))
            {
                Console.Error.WriteLine("You must provide a valid start date.");
                return;
            }

            if (string.IsNullOrWhiteSpace(endDate) || !DateTime.TryParse(endDate, out end))
            {
                Console.Error.WriteLine("You must provide a valid start date.");
                return;
            }

            if (string.IsNullOrWhiteSpace(fileName))
            {
                Console.Error.WriteLine("You must provide a location to save the report to.");
                return;
            }

            if (File.Exists(fileName))
            {
                Console.Write("The file already exists... Overwrite? (Y/N): ");
                if (!Regex.IsMatch(@"([Yy](es)?)", Console.ReadLine(), RegexOptions.CultureInvariant | RegexOptions.ExplicitCapture))
                    return;
            }



            var reportService = new ReportService();
            var criteria = reportService.GetCriteria(reportId, user);
            criteria.CustomerId = customerId;
            criteria.StartDate = start;
            criteria.EndDate = end;

            if (parameters != null && parameters.Any())
            {
                foreach (var p in parameters)
                {
                    var filter = criteria.Filters.FirstOrDefault(f => f.Name == p.Key);
                    if (filter != null)
                    {
                        if (string.IsNullOrWhiteSpace(p.Value))
                            filter.Value = null;
                        else if (filter.Options != null && filter.Options.Count > 0)
                        {
                            var opt = filter.Options.FirstOrDefault(o => p.Value.Equals(o.Value));
                            if (opt == null)
                                opt = filter.Options.FirstOrDefault(o => p.Value.ToString() == o.Value.ToString());
                            if (opt != null)
                                filter.Value = opt.Value;
                        }
                        else
                        {
                            filter.Value = p.Value;
                        }
                    }
                }
            }

            byte[] output = reportService.GenerateCSV(reportId, criteria, user);
            File.WriteAllBytes(string.Format("{0}.csv", fileName), output);
        }

        static string GetSwitchValue(string[] args, int index)
        {
            return args.Length > index + 1 ? args[index + 1] : null;
        }

        static void DoJobImport(Customer cust, Employer employer, string fileName)
        {
            List<Job> jobs = Job.AsQueryable().Where(j => j.CustomerId == cust.Id && j.EmployerId == employer.Id).ToList();
            using (FileService fs = new FileService(cust, employer, User.System))
            {
                fs.ReadDelimitedZipFileByLine(fileName, (rowNumber, parts, delimiter, recordText) =>
                {
                    bool error = false;

                    string jobCode = parts.Length > 1 ? parts[1] : null;
                    string jobName = parts.Length > 2 ? parts[2] : null;
                    string jobDesc = parts.Length > 3 ? parts[3] : null;
                    string jobClassString = parts.Length > 4 ? parts[4] : null;
                    JobClassification? jobClass = null;
                    if (!string.IsNullOrWhiteSpace(jobClassString) && int.TryParse(jobClassString, out int jc) && jc > 0 && Enum.IsDefined(typeof(JobClassification), jc))
                    {
                        jobClass = (JobClassification)jc;
                    }

                    if (string.IsNullOrWhiteSpace(jobCode))
                    {
                        error = true;
                        Console.Error.WriteLine("Row # {0}: is missing Job Code", rowNumber);
                    }
                    else
                        jobCode = jobCode.ToUpperInvariant();
                    if (string.IsNullOrWhiteSpace(jobName))
                    {
                        error = true;
                        Console.Error.WriteLine("Row # {0}: is missing Job Name", rowNumber);
                    }

                    if (error)
                        return;

                    if (string.IsNullOrWhiteSpace(jobDesc))
                        jobDesc = null;

                    var job = jobs.FirstOrDefault(j => j.Code.ToUpperInvariant() == jobCode);
                    if (job == null)
                    {
                        Console.WriteLine("Row # {0}: Creating a new job {1} - {2}", rowNumber, jobCode, jobName);
                        job = new Job()
                        {
                            Code = jobCode,
                            CustomerId = cust.Id,
                            EmployerId = employer.Id
                        };
                    }
                    else
                        Console.WriteLine("Row # {0}: Updating existing job {1} - {2}; {3}", rowNumber, jobCode, jobName, job.Id);

                    job.Name = jobName ?? job.Name;
                    job.Description = jobDesc ?? job.Description;
                    job.Activity = jobClass ?? job.Activity;
                    job.Save();
                    jobs.AddIfNotExists(job, j => j.Code == jobCode);

                    Console.Write("Row # {0}: Saved job id {1}", rowNumber, job.Id);
                });
            }
        }

        /// <summary>
        /// Processes the notifications.
        /// </summary>
        static void ProcessNotifications()
        {
            // For now, we'll only be sending notifications for ToDos
            List<NotificationConfiguration> configuredNotifications = NotificationConfiguration.AsQueryable()
                .Where(nc => nc.IsActive && nc.Category == NotificationCategory.ToDo).ToList();

            // Nothing configured to send, just bounce
            if (!configuredNotifications.Any())
            {
                return;
            }

            // Going to retrieve the most recent history run
            NotificationHistory lastRun = NotificationHistory.AsQueryable().OrderByDescending(nh => nh.CreatedDate).FirstOrDefault();
            NotificationHistory history = new NotificationHistory();

            //We're always going to do the immediate
            SendToDoNotifications(configuredNotifications, NotificationFrequencyType.Immediately, history, lastRun);

            // Send out the daily notifications if we have a history record and that record was from the day before this run
            if (lastRun != null && lastRun.CreatedDate.Day == DateTime.Today.AddDays(-1).Day)
            {
                SendToDoNotifications(configuredNotifications, NotificationFrequencyType.Daily, history, lastRun);
            }

            // Send out the weekly notifications if we have a history record that record is not from Sunday and today is Sunday
            if (lastRun != null && lastRun.CreatedDate.DayOfWeek != DayOfWeek.Sunday && DateTime.Today.DayOfWeek == DayOfWeek.Sunday)
            {
                SendToDoNotifications(configuredNotifications, NotificationFrequencyType.Weekly, history, lastRun);
            }

            history.Save();
        }

        /// <summary>
        /// Sends to do notifications.
        /// </summary>
        /// <param name="configuredNotifications">The configured notifications.</param>
        /// <param name="frequencyType">Type of the frequency.</param>
        /// <param name="history">The history.</param>
        /// <param name="lastRun">The last run.</param>
        static void SendToDoNotifications(List<NotificationConfiguration> configuredNotifications, NotificationFrequencyType frequencyType, NotificationHistory history, NotificationHistory lastRun)
        {
            var notificationsToSend = configuredNotifications.Where(nc => nc.FrequencyType == frequencyType);
            foreach (var notification in notificationsToSend)
            {
                List<ToDoItem> todosToSend = GetToDosToSend(notification);

                // No todos that haven't been sent out for this configuration
                if (!todosToSend.Any())
                {
                    continue;
                }

                // at this point we can retrieve the template - the list of ToDos should belong to the same employer and customer
                var firstToDo = todosToSend.First();
                Customer currentCustomer = firstToDo.Customer;
                Employer currentEmployer = firstToDo.Employer;
                Data.Communications.Template template = GetToDoNotificationTemplate(currentCustomer, currentEmployer);

                // no template, nothing to send
                if (template == null)
                {
                    Console.WriteLine("Template not exist for Case '{0}' CustomerId '{1}' EmployerId '{2}'", firstToDo.CaseNumber, firstToDo.CustomerId, firstToDo.EmployerId);
                    continue;
                }

                if (frequencyType == NotificationFrequencyType.Immediately)
                {
                    foreach (var todo in todosToSend)
                    {
                        // todosToSend get multiple ToDos for one user with multiple case, so when Case is null to get it by Id
                        // And it's process based so no login detail here, so we used direct MongoQuery
                        if (todo.Case == null && !string.IsNullOrEmpty(todo.CaseNumber))
                        {
                            List<IMongoQuery> ands = new List<IMongoQuery>();
                            ands.Add(Case.Query.EQ(c => c.Id, todo.CaseId));
                            ands.Add(Case.Query.EQ(c => c.IsDeleted, false));
                            
                            todo.Case = Case.Query.Find(Case.Query.And(ands)).FirstOrDefault();
                        }

                        MailMergeData merge = new MailMergeData();
                        merge.CopyToDoData(todo);
                        SendNotification(merge, notification, template, currentCustomer, currentEmployer);
                        Console.WriteLine("UserId {0} TodoId {1}", notification.UserId, todo.Id);
                        history.AddNotificationHistory(notification.UserId, todo.Id);
                    }
                }
                else
                {
                    //send it as a batch
                    MailMergeData merge = new MailMergeData();
                    merge.CopyToDoData(todosToSend.ToArray());
                    SendNotification(merge, notification, template, currentCustomer, currentEmployer);
                    Console.WriteLine("UserId {0} TodoId {1}", notification.UserId, todosToSend.Select(tdi => tdi.Id).ToArray());
                    history.AddNotificationHistory(notification.UserId, todosToSend.Select(tdi => tdi.Id).ToArray());
                }

            }
        }

        /// <summary>
        /// Sends the notification.
        /// </summary>
        /// <param name="merge">The merge.</param>
        /// <param name="notification">The notification.</param>
        /// <param name="template">The template.</param>
        /// <param name="currentCustomer">The current customer.</param>
        /// <param name="currentEmployer">The current employer.</param>
        private static void SendNotification(MailMergeData merge, NotificationConfiguration notification, Data.Communications.Template template, Customer currentCustomer, Employer currentEmployer)
        {
            string toEmail;
            string subject = template.Subject;
            if (!string.IsNullOrWhiteSpace(notification.DeliveryAddress))
            {
                toEmail = notification.DeliveryAddress;
            }
            else if (!string.IsNullOrWhiteSpace(notification.User?.Email))
            {
                toEmail = notification.User.Email;
            }
            else
            {
                Console.WriteLine("No Recipient for this email notification for CustomerId '{0}' EmployerId '{1}', better check it out!", currentCustomer?.Id, currentEmployer?.Id);              
                return;
            }
            switch (notification.FrequencyType)
            {
                case NotificationFrequencyType.Immediately:
                    subject = subject ?? "AbsenceTracker ToDo Assignment";
                    break;
                case NotificationFrequencyType.Daily:
                    subject = subject ?? "AbsenceTracker Daily ToDo Assignment";
                    break;
                case NotificationFrequencyType.Weekly:
                    subject = subject ?? "AbsenceTracker Weekly ToDo Assignment";
                    break;
                default:
                    throw new AbsenceSoftException("Unknown Frequency Type");
            }
            //To be used for Notification API for authentication.
            User user = GetAuthenticatedSystemUser();

            using (MailMessage msg = new MailMessage())
            {
                AddReplyToEmail(msg, currentCustomer, currentEmployer);
                msg.To.Add(toEmail);
                msg.Subject = subject;
                msg.Body = Rendering.Template.RenderTemplateToHTML(template.Document.DownloadStream(), merge);
                msg.IsBodyHtml = true;

                NotificationApiHelper.SendEmailAsync(msg, user, GetTokenFromClaims()).GetAwaiter().GetResult();
            }
        }

        /// <summary>
        /// Gets the api token from the ClaimsIdentity.
        /// </summary>
        /// <returns></returns>
        public static string GetTokenFromClaims()
        {
            var identity = HttpContext.Current.User.Identity as ClaimsIdentity;
            var claims = new List<Claim>(identity.Claims);
            var tokenClaim = claims.Find(p => p.Type.Equals("token"));
            if (tokenClaim != null)
            {
                return tokenClaim.Value;
            }
            return null;
        }


        /// <summary>
        /// The method sets the system user with token in claims as the Principal Identity with-in the context.
        /// </summary>
        /// <returns></returns>
        private static User GetAuthenticatedSystemUser()
        {
            User user = User.System;
            HttpContext context = new HttpContext(new HttpRequest(null, "http://tempuri.org", null), new HttpResponse(null));
            HttpContext.Current = context;
            ApplicationUserManager userManager = new ApplicationUserManager(UserStoreProviderManager.Default.Repository);
            var ticket = userManager.GetSystemTicketAsync(user.Email, AT.Entities.Authentication.ApplicationType.Portal).GetAwaiter().GetResult();
            if (ticket != null)
            {
                context.Request.Browser = new HttpBrowserCapabilities() { Capabilities = new Dictionary<string, string> { { "supportsEmptyStringInCookieValue", "false" }, { "cookies", "false" } } };
                context.Items[User.CURRENT_USER_CONTEXT_KEY] = user;
                userManager.SetClaimsPrincipal(ticket, new HttpContextWrapper(context)).GetAwaiter().GetResult();
            }
            return user;
        }
        /// <summary>
        /// Gets from email.
        /// </summary>
        /// <param name="msg">The MSG.</param>
        /// <param name="currentCustomer">The current customer.</param>
        /// <param name="currentEmployer">The current employer.</param>
        private static void AddReplyToEmail(MailMessage msg, Customer currentCustomer, Employer currentEmployer)
        {
            EmailReplies? replyType = currentEmployer?.EmailRepliesType ?? currentCustomer.EmailRepliesType;
            // There is no current user, so we only care about the FromEmail configuration
            if (replyType != EmailReplies.FromEmail)
            {
                return;
            }

            using (CommunicationService communicationService = new CommunicationService())
            {
                string defaultFromAddress = communicationService.GetDefaultEmailFromAddress(currentEmployer);
                if (!string.IsNullOrWhiteSpace(defaultFromAddress))
                {
                    msg.ReplyToList.Add(defaultFromAddress);
                }
            }

        }

        /// <summary>
        /// Gets the to do notification template.
        /// </summary>
        /// <param name="currentCustomer">The customer.</param>
        /// <param name="currentEmployer">The employer.</param>
        /// <returns></returns>
        static Data.Communications.Template GetToDoNotificationTemplate(Customer currentCustomer, Employer currentEmployer)
        {
            if (currentEmployer != null && !string.IsNullOrEmpty(currentEmployer.ToDoTemplateCode))
            {
                return Data.Communications.Template.GetByCode(currentEmployer.ToDoTemplateCode, (currentCustomer == null ? null : currentCustomer.Id), currentEmployer.Id);
            }

            if (currentCustomer != null && !string.IsNullOrEmpty(currentCustomer.ToDoTemplateCode))
            {
                return Data.Communications.Template.GetByCode(currentCustomer.ToDoTemplateCode, currentCustomer.Id, (currentEmployer == null ? null : currentEmployer.Id));
            }

            return null;
        }

        /// <summary>
        /// Gets to dos to send.
        /// </summary>
        /// <param name="notification">The notification.</param>
        /// <returns></returns>
        static List<ToDoItem> GetToDosToSend(NotificationConfiguration notification)
        {
            List<string> alreadySentNotifications = GetAlreadySentNotifications(notification.UserId);

            return ToDoItem.AsQueryable()
                .Where(tdi => tdi.AssignedToId == notification.UserId           // Get the ToDos that have been assigned to them, 
                    && !alreadySentNotifications.Contains(tdi.Id)               // that we haven't sent out, 
                    && tdi.Status != ToDoItemStatus.Complete                    // That hasn't been completed
                    && tdi.Status != ToDoItemStatus.Cancelled                   // that hasn't been cancelled
                                                                                // Time to check the dates
                                                                                // it's not hidden and were created after the user most recently modified their notification preferences
                    && ((tdi.HideUntil == null && notification.ModifiedDate <= tdi.ModifiedDate)
                        // it's was hidden, but it should be visible now
                        || (tdi.HideUntil <= DateTime.Today && notification.ModifiedDate <= tdi.ModifiedDate)
                        // it was hidden, but it might have been modified (like reassigned to this user) since they elected receive notifications
                        || (tdi.HideUntil <= DateTime.Today && notification.ModifiedDate <= tdi.HideUntil)
                       )
                    ).ToList();
        }

        /// <summary>
        /// Gets the already sent notifications.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        static List<string> GetAlreadySentNotifications(string userId)
        {
            List<NotificationHistory> historyRecordsForUser = NotificationHistory.AsQueryable().Where(nh => nh.NotificationsSent.Any(u => u.UserId == userId)).ToList();
            List<string> alreadySentNotifications = new List<string>();
            foreach (var historyRecord in historyRecordsForUser)
            {
                alreadySentNotifications.AddRange(historyRecord.GetNotificationHistory(userId));
            }
            return alreadySentNotifications;
        }

        /// <summary>
        /// The method resets the customer's API key and API Secret
        /// </summary>
        /// <param name="CustomerId"></param>
        static void ResetCustomerApiKeys(string customerId)
        {
            //check for the specific customer if not null
            if (string.IsNullOrEmpty(customerId))
            {
                var customers = Customer.AsQueryable().Where(c => !c.IsDeleted).ToList();
                customers.ForEach(p =>
                    {
                        p.ApiKey = null;
                        p.ApiSecret = null;
                        p.Save();
                    }
                );
            }
            else
            {
                var customer = Customer.GetById(customerId);
                if (customer != null)
                {
                    customer.ApiKey = null;
                    customer.ApiSecret = null;
                    customer.Save();
                }
            }
        }
    }
}
