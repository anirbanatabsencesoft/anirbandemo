﻿namespace AbsenceSoft.Configuration
{
    partial class ImportExport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportExport));
            this.lblCustomer = new System.Windows.Forms.Label();
            this.cboCustomer = new System.Windows.Forms.ComboBox();
            this.tabs = new System.Windows.Forms.TabControl();
            this.tabEmployers = new System.Windows.Forms.TabPage();
            this.listEmployers = new System.Windows.Forms.ListView();
            this.colEmployerCB = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colEmployerId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colEmployerName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabReasons = new System.Windows.Forms.TabPage();
            this.listAbsenceReasons = new System.Windows.Forms.ListView();
            this.colReasonCB = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colARId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colARCode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colARName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabTemplates = new System.Windows.Forms.TabPage();
            this.listTemplates = new System.Windows.Forms.ListView();
            this.colTemplateCB = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPaperwork = new System.Windows.Forms.TabPage();
            this.listPaperwork = new System.Windows.Forms.ListView();
            this.colPaperworkCB = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabRoles = new System.Windows.Forms.TabPage();
            this.listRoles = new System.Windows.Forms.ListView();
            this.colRoleCB = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPolicies = new System.Windows.Forms.TabPage();
            this.listPolicies = new System.Windows.Forms.ListView();
            this.colPolicyCB = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabWorkflows = new System.Windows.Forms.TabPage();
            this.listWorkflows = new System.Windows.Forms.ListView();
            this.colWorkflowCB = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabContactTypes = new System.Windows.Forms.TabPage();
            this.listContactTypes = new System.Windows.Forms.ListView();
            this.colContactTypeCB = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader26 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader27 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader28 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader29 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabCustomFields = new System.Windows.Forms.TabPage();
            this.listCustomFields = new System.Windows.Forms.ListView();
            this.colCustomCB = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader31 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader32 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader33 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabAccomQuestions = new System.Windows.Forms.TabPage();
            this.listAccomQuestions = new System.Windows.Forms.ListView();
            this.colQuestionCB = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader35 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader36 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader37 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabAccomTypes = new System.Windows.Forms.TabPage();
            this.listAccomTypes = new System.Windows.Forms.ListView();
            this.colAccomCB = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader39 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader40 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader41 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader42 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPaySchedules = new System.Windows.Forms.TabPage();
            this.listPaySchedules = new System.Windows.Forms.ListView();
            this.columnHeader43 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader44 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader45 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader46 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnExport = new System.Windows.Forms.Button();
            this.btnImport = new System.Windows.Forms.Button();
            this.output = new System.Windows.Forms.TextBox();
            this.txtFile = new System.Windows.Forms.TextBox();
            this.btnOpenFile = new System.Windows.Forms.Button();
            this.groupImport = new System.Windows.Forms.GroupBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupExport = new System.Windows.Forms.GroupBox();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.tabs.SuspendLayout();
            this.tabEmployers.SuspendLayout();
            this.tabReasons.SuspendLayout();
            this.tabTemplates.SuspendLayout();
            this.tabPaperwork.SuspendLayout();
            this.tabRoles.SuspendLayout();
            this.tabPolicies.SuspendLayout();
            this.tabWorkflows.SuspendLayout();
            this.tabContactTypes.SuspendLayout();
            this.tabCustomFields.SuspendLayout();
            this.tabAccomQuestions.SuspendLayout();
            this.tabAccomTypes.SuspendLayout();
            this.tabPaySchedules.SuspendLayout();
            this.groupImport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupExport.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblCustomer
            // 
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomer.Location = new System.Drawing.Point(12, 24);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(51, 13);
            this.lblCustomer.TabIndex = 0;
            this.lblCustomer.Text = "Customer";
            // 
            // cboCustomer
            // 
            this.cboCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboCustomer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCustomer.Location = new System.Drawing.Point(69, 21);
            this.cboCustomer.Name = "cboCustomer";
            this.cboCustomer.Size = new System.Drawing.Size(850, 21);
            this.cboCustomer.TabIndex = 1;
            this.cboCustomer.SelectedIndexChanged += new System.EventHandler(this.cboCustomer_SelectedIndexChanged);
            // 
            // tabs
            // 
            this.tabs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabs.Controls.Add(this.tabEmployers);
            this.tabs.Controls.Add(this.tabReasons);
            this.tabs.Controls.Add(this.tabTemplates);
            this.tabs.Controls.Add(this.tabPaperwork);
            this.tabs.Controls.Add(this.tabRoles);
            this.tabs.Controls.Add(this.tabPolicies);
            this.tabs.Controls.Add(this.tabWorkflows);
            this.tabs.Controls.Add(this.tabContactTypes);
            this.tabs.Controls.Add(this.tabCustomFields);
            this.tabs.Controls.Add(this.tabAccomQuestions);
            this.tabs.Controls.Add(this.tabAccomTypes);
            this.tabs.Controls.Add(this.tabPaySchedules);
            this.tabs.Enabled = false;
            this.tabs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabs.Location = new System.Drawing.Point(6, 59);
            this.tabs.Margin = new System.Windows.Forms.Padding(0);
            this.tabs.Name = "tabs";
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(994, 350);
            this.tabs.TabIndex = 5;
            // 
            // tabEmployers
            // 
            this.tabEmployers.Controls.Add(this.listEmployers);
            this.tabEmployers.Location = new System.Drawing.Point(4, 22);
            this.tabEmployers.Name = "tabEmployers";
            this.tabEmployers.Padding = new System.Windows.Forms.Padding(3);
            this.tabEmployers.Size = new System.Drawing.Size(986, 324);
            this.tabEmployers.TabIndex = 0;
            this.tabEmployers.Text = "Employers";
            this.tabEmployers.UseVisualStyleBackColor = true;
            // 
            // listEmployers
            // 
            this.listEmployers.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listEmployers.CheckBoxes = true;
            this.listEmployers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colEmployerCB,
            this.colEmployerId,
            this.colEmployerName});
            this.listEmployers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listEmployers.Location = new System.Drawing.Point(3, 3);
            this.listEmployers.Name = "listEmployers";
            this.listEmployers.Size = new System.Drawing.Size(980, 318);
            this.listEmployers.TabIndex = 0;
            this.listEmployers.UseCompatibleStateImageBehavior = false;
            this.listEmployers.View = System.Windows.Forms.View.Details;
            this.listEmployers.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listEmployers_ColumnClick);
            this.listEmployers.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listEmployers_ItemChecked);
            // 
            // colEmployerCB
            // 
            this.colEmployerCB.Text = "";
            this.colEmployerCB.Width = 22;
            // 
            // colEmployerId
            // 
            this.colEmployerId.Tag = "";
            this.colEmployerId.Text = "ID";
            this.colEmployerId.Width = 164;
            // 
            // colEmployerName
            // 
            this.colEmployerName.Tag = "";
            this.colEmployerName.Text = "Employer Name";
            this.colEmployerName.Width = 777;
            // 
            // tabReasons
            // 
            this.tabReasons.Controls.Add(this.listAbsenceReasons);
            this.tabReasons.Location = new System.Drawing.Point(4, 22);
            this.tabReasons.Name = "tabReasons";
            this.tabReasons.Padding = new System.Windows.Forms.Padding(3);
            this.tabReasons.Size = new System.Drawing.Size(986, 324);
            this.tabReasons.TabIndex = 1;
            this.tabReasons.Text = "Absence Reasons";
            this.tabReasons.UseVisualStyleBackColor = true;
            // 
            // listAbsenceReasons
            // 
            this.listAbsenceReasons.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listAbsenceReasons.CheckBoxes = true;
            this.listAbsenceReasons.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colReasonCB,
            this.colARId,
            this.columnHeader8,
            this.colARCode,
            this.colARName});
            this.listAbsenceReasons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listAbsenceReasons.FullRowSelect = true;
            this.listAbsenceReasons.Location = new System.Drawing.Point(3, 3);
            this.listAbsenceReasons.Name = "listAbsenceReasons";
            this.listAbsenceReasons.Size = new System.Drawing.Size(980, 318);
            this.listAbsenceReasons.TabIndex = 1;
            this.listAbsenceReasons.UseCompatibleStateImageBehavior = false;
            this.listAbsenceReasons.View = System.Windows.Forms.View.Details;
            this.listAbsenceReasons.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.list_ColumnClick);
            // 
            // colReasonCB
            // 
            this.colReasonCB.Text = "";
            this.colReasonCB.Width = 22;
            // 
            // colARId
            // 
            this.colARId.Tag = "";
            this.colARId.Text = "ID";
            this.colARId.Width = 164;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Employer";
            this.columnHeader8.Width = 197;
            // 
            // colARCode
            // 
            this.colARCode.Text = "Code";
            this.colARCode.Width = 143;
            // 
            // colARName
            // 
            this.colARName.Tag = "";
            this.colARName.Text = "Name";
            this.colARName.Width = 437;
            // 
            // tabTemplates
            // 
            this.tabTemplates.Controls.Add(this.listTemplates);
            this.tabTemplates.Location = new System.Drawing.Point(4, 22);
            this.tabTemplates.Name = "tabTemplates";
            this.tabTemplates.Padding = new System.Windows.Forms.Padding(3);
            this.tabTemplates.Size = new System.Drawing.Size(986, 324);
            this.tabTemplates.TabIndex = 7;
            this.tabTemplates.Text = "Templates";
            this.tabTemplates.UseVisualStyleBackColor = true;
            // 
            // listTemplates
            // 
            this.listTemplates.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listTemplates.CheckBoxes = true;
            this.listTemplates.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTemplateCB,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader9});
            this.listTemplates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listTemplates.FullRowSelect = true;
            this.listTemplates.Location = new System.Drawing.Point(3, 3);
            this.listTemplates.Name = "listTemplates";
            this.listTemplates.Size = new System.Drawing.Size(980, 318);
            this.listTemplates.TabIndex = 2;
            this.listTemplates.UseCompatibleStateImageBehavior = false;
            this.listTemplates.View = System.Windows.Forms.View.Details;
            this.listTemplates.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.list_ColumnClick);
            // 
            // colTemplateCB
            // 
            this.colTemplateCB.Text = "";
            this.colTemplateCB.Width = 22;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Tag = "";
            this.columnHeader2.Text = "ID";
            this.columnHeader2.Width = 164;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Employer";
            this.columnHeader3.Width = 197;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Code";
            this.columnHeader4.Width = 143;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Tag = "";
            this.columnHeader9.Text = "Name";
            this.columnHeader9.Width = 437;
            // 
            // tabPaperwork
            // 
            this.tabPaperwork.Controls.Add(this.listPaperwork);
            this.tabPaperwork.Location = new System.Drawing.Point(4, 22);
            this.tabPaperwork.Name = "tabPaperwork";
            this.tabPaperwork.Padding = new System.Windows.Forms.Padding(3);
            this.tabPaperwork.Size = new System.Drawing.Size(986, 324);
            this.tabPaperwork.TabIndex = 6;
            this.tabPaperwork.Text = "Paperwork";
            this.tabPaperwork.UseVisualStyleBackColor = true;
            // 
            // listPaperwork
            // 
            this.listPaperwork.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listPaperwork.CheckBoxes = true;
            this.listPaperwork.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colPaperworkCB,
            this.columnHeader6,
            this.columnHeader10,
            this.columnHeader7});
            this.listPaperwork.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listPaperwork.FullRowSelect = true;
            this.listPaperwork.Location = new System.Drawing.Point(3, 3);
            this.listPaperwork.Name = "listPaperwork";
            this.listPaperwork.Size = new System.Drawing.Size(980, 318);
            this.listPaperwork.TabIndex = 1;
            this.listPaperwork.UseCompatibleStateImageBehavior = false;
            this.listPaperwork.View = System.Windows.Forms.View.Details;
            this.listPaperwork.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.list_ColumnClick);
            // 
            // colPaperworkCB
            // 
            this.colPaperworkCB.Text = "";
            this.colPaperworkCB.Width = 22;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Tag = "";
            this.columnHeader6.Text = "ID";
            this.columnHeader6.Width = 164;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Employer";
            this.columnHeader10.Width = 200;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Tag = "";
            this.columnHeader7.Text = "Name";
            this.columnHeader7.Width = 420;
            // 
            // tabRoles
            // 
            this.tabRoles.Controls.Add(this.listRoles);
            this.tabRoles.Location = new System.Drawing.Point(4, 22);
            this.tabRoles.Name = "tabRoles";
            this.tabRoles.Size = new System.Drawing.Size(986, 324);
            this.tabRoles.TabIndex = 10;
            this.tabRoles.Text = "Roles";
            this.tabRoles.UseVisualStyleBackColor = true;
            // 
            // listRoles
            // 
            this.listRoles.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listRoles.CheckBoxes = true;
            this.listRoles.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colRoleCB,
            this.columnHeader12,
            this.columnHeader14});
            this.listRoles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listRoles.FullRowSelect = true;
            this.listRoles.Location = new System.Drawing.Point(0, 0);
            this.listRoles.Name = "listRoles";
            this.listRoles.Size = new System.Drawing.Size(986, 324);
            this.listRoles.TabIndex = 2;
            this.listRoles.UseCompatibleStateImageBehavior = false;
            this.listRoles.View = System.Windows.Forms.View.Details;
            this.listRoles.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.list_ColumnClick);
            // 
            // colRoleCB
            // 
            this.colRoleCB.Text = "";
            this.colRoleCB.Width = 22;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Tag = "";
            this.columnHeader12.Text = "ID";
            this.columnHeader12.Width = 164;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Tag = "";
            this.columnHeader14.Text = "Name";
            this.columnHeader14.Width = 420;
            // 
            // tabPolicies
            // 
            this.tabPolicies.Controls.Add(this.listPolicies);
            this.tabPolicies.Location = new System.Drawing.Point(4, 22);
            this.tabPolicies.Name = "tabPolicies";
            this.tabPolicies.Size = new System.Drawing.Size(986, 324);
            this.tabPolicies.TabIndex = 9;
            this.tabPolicies.Text = "Policies";
            this.tabPolicies.UseVisualStyleBackColor = true;
            // 
            // listPolicies
            // 
            this.listPolicies.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listPolicies.CheckBoxes = true;
            this.listPolicies.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colPolicyCB,
            this.columnHeader16,
            this.columnHeader17,
            this.columnHeader18,
            this.columnHeader19});
            this.listPolicies.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listPolicies.FullRowSelect = true;
            this.listPolicies.Location = new System.Drawing.Point(0, 0);
            this.listPolicies.Name = "listPolicies";
            this.listPolicies.Size = new System.Drawing.Size(986, 324);
            this.listPolicies.TabIndex = 3;
            this.listPolicies.UseCompatibleStateImageBehavior = false;
            this.listPolicies.View = System.Windows.Forms.View.Details;
            this.listPolicies.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.list_ColumnClick);
            // 
            // colPolicyCB
            // 
            this.colPolicyCB.Text = "";
            this.colPolicyCB.Width = 22;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Tag = "";
            this.columnHeader16.Text = "ID";
            this.columnHeader16.Width = 164;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Employer";
            this.columnHeader17.Width = 197;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "Code";
            this.columnHeader18.Width = 143;
            // 
            // columnHeader19
            // 
            this.columnHeader19.Tag = "";
            this.columnHeader19.Text = "Name";
            this.columnHeader19.Width = 437;
            // 
            // tabWorkflows
            // 
            this.tabWorkflows.Controls.Add(this.listWorkflows);
            this.tabWorkflows.Location = new System.Drawing.Point(4, 22);
            this.tabWorkflows.Name = "tabWorkflows";
            this.tabWorkflows.Size = new System.Drawing.Size(986, 324);
            this.tabWorkflows.TabIndex = 11;
            this.tabWorkflows.Text = "Workflows";
            this.tabWorkflows.UseVisualStyleBackColor = true;
            // 
            // listWorkflows
            // 
            this.listWorkflows.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listWorkflows.CheckBoxes = true;
            this.listWorkflows.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colWorkflowCB,
            this.columnHeader21,
            this.columnHeader22,
            this.columnHeader23,
            this.columnHeader24});
            this.listWorkflows.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listWorkflows.FullRowSelect = true;
            this.listWorkflows.Location = new System.Drawing.Point(0, 0);
            this.listWorkflows.Name = "listWorkflows";
            this.listWorkflows.Size = new System.Drawing.Size(986, 324);
            this.listWorkflows.TabIndex = 4;
            this.listWorkflows.UseCompatibleStateImageBehavior = false;
            this.listWorkflows.View = System.Windows.Forms.View.Details;
            this.listWorkflows.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.list_ColumnClick);
            // 
            // colWorkflowCB
            // 
            this.colWorkflowCB.Text = "";
            this.colWorkflowCB.Width = 22;
            // 
            // columnHeader21
            // 
            this.columnHeader21.Tag = "";
            this.columnHeader21.Text = "ID";
            this.columnHeader21.Width = 164;
            // 
            // columnHeader22
            // 
            this.columnHeader22.Text = "Employer";
            this.columnHeader22.Width = 197;
            // 
            // columnHeader23
            // 
            this.columnHeader23.Text = "Code";
            this.columnHeader23.Width = 143;
            // 
            // columnHeader24
            // 
            this.columnHeader24.Tag = "";
            this.columnHeader24.Text = "Name";
            this.columnHeader24.Width = 437;
            // 
            // tabContactTypes
            // 
            this.tabContactTypes.Controls.Add(this.listContactTypes);
            this.tabContactTypes.Location = new System.Drawing.Point(4, 22);
            this.tabContactTypes.Name = "tabContactTypes";
            this.tabContactTypes.Padding = new System.Windows.Forms.Padding(3);
            this.tabContactTypes.Size = new System.Drawing.Size(986, 324);
            this.tabContactTypes.TabIndex = 4;
            this.tabContactTypes.Text = "Contact Types";
            this.tabContactTypes.UseVisualStyleBackColor = true;
            // 
            // listContactTypes
            // 
            this.listContactTypes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listContactTypes.CheckBoxes = true;
            this.listContactTypes.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colContactTypeCB,
            this.columnHeader26,
            this.columnHeader27,
            this.columnHeader28,
            this.columnHeader29});
            this.listContactTypes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listContactTypes.FullRowSelect = true;
            this.listContactTypes.Location = new System.Drawing.Point(3, 3);
            this.listContactTypes.Name = "listContactTypes";
            this.listContactTypes.Size = new System.Drawing.Size(980, 318);
            this.listContactTypes.TabIndex = 4;
            this.listContactTypes.UseCompatibleStateImageBehavior = false;
            this.listContactTypes.View = System.Windows.Forms.View.Details;
            this.listContactTypes.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.list_ColumnClick);
            // 
            // colContactTypeCB
            // 
            this.colContactTypeCB.Text = "";
            this.colContactTypeCB.Width = 22;
            // 
            // columnHeader26
            // 
            this.columnHeader26.Tag = "";
            this.columnHeader26.Text = "ID";
            this.columnHeader26.Width = 164;
            // 
            // columnHeader27
            // 
            this.columnHeader27.Text = "Employer";
            this.columnHeader27.Width = 197;
            // 
            // columnHeader28
            // 
            this.columnHeader28.Text = "Code";
            this.columnHeader28.Width = 143;
            // 
            // columnHeader29
            // 
            this.columnHeader29.Tag = "";
            this.columnHeader29.Text = "Name";
            this.columnHeader29.Width = 437;
            // 
            // tabCustomFields
            // 
            this.tabCustomFields.Controls.Add(this.listCustomFields);
            this.tabCustomFields.Location = new System.Drawing.Point(4, 22);
            this.tabCustomFields.Name = "tabCustomFields";
            this.tabCustomFields.Padding = new System.Windows.Forms.Padding(3);
            this.tabCustomFields.Size = new System.Drawing.Size(986, 324);
            this.tabCustomFields.TabIndex = 5;
            this.tabCustomFields.Text = "Custom Fields";
            this.tabCustomFields.UseVisualStyleBackColor = true;
            // 
            // listCustomFields
            // 
            this.listCustomFields.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listCustomFields.CheckBoxes = true;
            this.listCustomFields.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colCustomCB,
            this.columnHeader31,
            this.columnHeader32,
            this.columnHeader33});
            this.listCustomFields.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listCustomFields.FullRowSelect = true;
            this.listCustomFields.Location = new System.Drawing.Point(3, 3);
            this.listCustomFields.Name = "listCustomFields";
            this.listCustomFields.Size = new System.Drawing.Size(980, 318);
            this.listCustomFields.TabIndex = 2;
            this.listCustomFields.UseCompatibleStateImageBehavior = false;
            this.listCustomFields.View = System.Windows.Forms.View.Details;
            this.listCustomFields.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.list_ColumnClick);
            // 
            // colCustomCB
            // 
            this.colCustomCB.Text = "";
            this.colCustomCB.Width = 22;
            // 
            // columnHeader31
            // 
            this.columnHeader31.Tag = "";
            this.columnHeader31.Text = "ID";
            this.columnHeader31.Width = 164;
            // 
            // columnHeader32
            // 
            this.columnHeader32.Text = "Employer";
            this.columnHeader32.Width = 200;
            // 
            // columnHeader33
            // 
            this.columnHeader33.Tag = "";
            this.columnHeader33.Text = "Name";
            this.columnHeader33.Width = 420;
            // 
            // tabAccomQuestions
            // 
            this.tabAccomQuestions.Controls.Add(this.listAccomQuestions);
            this.tabAccomQuestions.Location = new System.Drawing.Point(4, 22);
            this.tabAccomQuestions.Name = "tabAccomQuestions";
            this.tabAccomQuestions.Padding = new System.Windows.Forms.Padding(3);
            this.tabAccomQuestions.Size = new System.Drawing.Size(986, 324);
            this.tabAccomQuestions.TabIndex = 2;
            this.tabAccomQuestions.Text = "Accom Questions";
            this.tabAccomQuestions.UseVisualStyleBackColor = true;
            // 
            // listAccomQuestions
            // 
            this.listAccomQuestions.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listAccomQuestions.CheckBoxes = true;
            this.listAccomQuestions.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colQuestionCB,
            this.columnHeader35,
            this.columnHeader36,
            this.columnHeader37});
            this.listAccomQuestions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listAccomQuestions.FullRowSelect = true;
            this.listAccomQuestions.Location = new System.Drawing.Point(3, 3);
            this.listAccomQuestions.Name = "listAccomQuestions";
            this.listAccomQuestions.Size = new System.Drawing.Size(980, 318);
            this.listAccomQuestions.TabIndex = 2;
            this.listAccomQuestions.UseCompatibleStateImageBehavior = false;
            this.listAccomQuestions.View = System.Windows.Forms.View.Details;
            this.listAccomQuestions.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.list_ColumnClick);
            // 
            // colQuestionCB
            // 
            this.colQuestionCB.Text = "";
            this.colQuestionCB.Width = 22;
            // 
            // columnHeader35
            // 
            this.columnHeader35.Tag = "";
            this.columnHeader35.Text = "ID";
            this.columnHeader35.Width = 164;
            // 
            // columnHeader36
            // 
            this.columnHeader36.Text = "Employer";
            this.columnHeader36.Width = 200;
            // 
            // columnHeader37
            // 
            this.columnHeader37.Tag = "";
            this.columnHeader37.Text = "Question";
            this.columnHeader37.Width = 420;
            // 
            // tabAccomTypes
            // 
            this.tabAccomTypes.Controls.Add(this.listAccomTypes);
            this.tabAccomTypes.Location = new System.Drawing.Point(4, 22);
            this.tabAccomTypes.Name = "tabAccomTypes";
            this.tabAccomTypes.Padding = new System.Windows.Forms.Padding(3);
            this.tabAccomTypes.Size = new System.Drawing.Size(986, 324);
            this.tabAccomTypes.TabIndex = 3;
            this.tabAccomTypes.Text = "Accom Types";
            this.tabAccomTypes.UseVisualStyleBackColor = true;
            // 
            // listAccomTypes
            // 
            this.listAccomTypes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listAccomTypes.CheckBoxes = true;
            this.listAccomTypes.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colAccomCB,
            this.columnHeader39,
            this.columnHeader40,
            this.columnHeader41,
            this.columnHeader42});
            this.listAccomTypes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listAccomTypes.FullRowSelect = true;
            this.listAccomTypes.Location = new System.Drawing.Point(3, 3);
            this.listAccomTypes.Name = "listAccomTypes";
            this.listAccomTypes.Size = new System.Drawing.Size(980, 318);
            this.listAccomTypes.TabIndex = 5;
            this.listAccomTypes.UseCompatibleStateImageBehavior = false;
            this.listAccomTypes.View = System.Windows.Forms.View.Details;
            this.listAccomTypes.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.list_ColumnClick);
            // 
            // colAccomCB
            // 
            this.colAccomCB.Text = "";
            this.colAccomCB.Width = 22;
            // 
            // columnHeader39
            // 
            this.columnHeader39.Tag = "";
            this.columnHeader39.Text = "ID";
            this.columnHeader39.Width = 164;
            // 
            // columnHeader40
            // 
            this.columnHeader40.Text = "Employer";
            this.columnHeader40.Width = 197;
            // 
            // columnHeader41
            // 
            this.columnHeader41.Text = "Code";
            this.columnHeader41.Width = 143;
            // 
            // columnHeader42
            // 
            this.columnHeader42.Tag = "";
            this.columnHeader42.Text = "Name";
            this.columnHeader42.Width = 437;
            // 
            // tabPaySchedules
            // 
            this.tabPaySchedules.Controls.Add(this.listPaySchedules);
            this.tabPaySchedules.Location = new System.Drawing.Point(4, 22);
            this.tabPaySchedules.Name = "tabPaySchedules";
            this.tabPaySchedules.Padding = new System.Windows.Forms.Padding(3);
            this.tabPaySchedules.Size = new System.Drawing.Size(986, 324);
            this.tabPaySchedules.TabIndex = 8;
            this.tabPaySchedules.Text = "Pay Schedules";
            this.tabPaySchedules.UseVisualStyleBackColor = true;
            // 
            // listPaySchedules
            // 
            this.listPaySchedules.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listPaySchedules.CheckBoxes = true;
            this.listPaySchedules.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader43,
            this.columnHeader44,
            this.columnHeader45,
            this.columnHeader46});
            this.listPaySchedules.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listPaySchedules.FullRowSelect = true;
            this.listPaySchedules.Location = new System.Drawing.Point(3, 3);
            this.listPaySchedules.Name = "listPaySchedules";
            this.listPaySchedules.Size = new System.Drawing.Size(980, 318);
            this.listPaySchedules.TabIndex = 2;
            this.listPaySchedules.UseCompatibleStateImageBehavior = false;
            this.listPaySchedules.View = System.Windows.Forms.View.Details;
            this.listPaySchedules.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.list_ColumnClick);
            // 
            // columnHeader43
            // 
            this.columnHeader43.Text = "";
            this.columnHeader43.Width = 22;
            // 
            // columnHeader44
            // 
            this.columnHeader44.Tag = "";
            this.columnHeader44.Text = "ID";
            this.columnHeader44.Width = 164;
            // 
            // columnHeader45
            // 
            this.columnHeader45.Text = "Employer";
            this.columnHeader45.Width = 200;
            // 
            // columnHeader46
            // 
            this.columnHeader46.Tag = "";
            this.columnHeader46.Text = "Name";
            this.columnHeader46.Width = 420;
            // 
            // btnExport
            // 
            this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExport.Enabled = false;
            this.btnExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.Location = new System.Drawing.Point(925, 19);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(75, 23);
            this.btnExport.TabIndex = 6;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnImport
            // 
            this.btnImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImport.Enabled = false;
            this.btnImport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImport.Location = new System.Drawing.Point(925, 21);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(75, 23);
            this.btnImport.TabIndex = 7;
            this.btnImport.Text = "Import";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // output
            // 
            this.output.AcceptsReturn = true;
            this.output.AcceptsTab = true;
            this.output.CausesValidation = false;
            this.output.Dock = System.Windows.Forms.DockStyle.Fill;
            this.output.HideSelection = false;
            this.output.Location = new System.Drawing.Point(0, 0);
            this.output.Multiline = true;
            this.output.Name = "output";
            this.output.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.output.Size = new System.Drawing.Size(1005, 159);
            this.output.TabIndex = 8;
            this.output.TabStop = false;
            this.output.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ImportExport_KeyDown);
            // 
            // txtFile
            // 
            this.txtFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFile.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFile.Location = new System.Drawing.Point(6, 23);
            this.txtFile.Name = "txtFile";
            this.txtFile.ReadOnly = true;
            this.txtFile.Size = new System.Drawing.Size(879, 20);
            this.txtFile.TabIndex = 11;
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenFile.Location = new System.Drawing.Point(891, 21);
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(28, 23);
            this.btnOpenFile.TabIndex = 12;
            this.btnOpenFile.Text = "...";
            this.btnOpenFile.UseVisualStyleBackColor = true;
            this.btnOpenFile.Click += new System.EventHandler(this.btnOpenFile_Click);
            // 
            // groupImport
            // 
            this.groupImport.Controls.Add(this.btnOpenFile);
            this.groupImport.Controls.Add(this.btnImport);
            this.groupImport.Controls.Add(this.txtFile);
            this.groupImport.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupImport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupImport.Location = new System.Drawing.Point(0, 0);
            this.groupImport.Name = "groupImport";
            this.groupImport.Size = new System.Drawing.Size(1005, 55);
            this.groupImport.TabIndex = 13;
            this.groupImport.TabStop = false;
            this.groupImport.Text = "Import";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupExport);
            this.splitContainer1.Panel1.Controls.Add(this.groupImport);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.output);
            this.splitContainer1.Size = new System.Drawing.Size(1005, 627);
            this.splitContainer1.SplitterDistance = 464;
            this.splitContainer1.TabIndex = 14;
            // 
            // groupExport
            // 
            this.groupExport.Controls.Add(this.btnSelectAll);
            this.groupExport.Controls.Add(this.lblCustomer);
            this.groupExport.Controls.Add(this.tabs);
            this.groupExport.Controls.Add(this.btnExport);
            this.groupExport.Controls.Add(this.cboCustomer);
            this.groupExport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupExport.Location = new System.Drawing.Point(0, 55);
            this.groupExport.Name = "groupExport";
            this.groupExport.Size = new System.Drawing.Size(1005, 409);
            this.groupExport.TabIndex = 14;
            this.groupExport.TabStop = false;
            this.groupExport.Text = "Export";
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectAll.Location = new System.Drawing.Point(884, 48);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(116, 27);
            this.btnSelectAll.TabIndex = 7;
            this.btnSelectAll.Text = "Select All Visible";
            this.btnSelectAll.UseVisualStyleBackColor = true;
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // ImportExport
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1005, 627);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ImportExport";
            this.Text = "AbsenceTracker Configuration Utility";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ImportExport_FormClosing);
            this.Load += new System.EventHandler(this.ImportExport_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.ImportExport_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.ImportExport_DragEnter);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ImportExport_KeyDown);
            this.tabs.ResumeLayout(false);
            this.tabEmployers.ResumeLayout(false);
            this.tabReasons.ResumeLayout(false);
            this.tabTemplates.ResumeLayout(false);
            this.tabPaperwork.ResumeLayout(false);
            this.tabRoles.ResumeLayout(false);
            this.tabPolicies.ResumeLayout(false);
            this.tabWorkflows.ResumeLayout(false);
            this.tabContactTypes.ResumeLayout(false);
            this.tabCustomFields.ResumeLayout(false);
            this.tabAccomQuestions.ResumeLayout(false);
            this.tabAccomTypes.ResumeLayout(false);
            this.tabPaySchedules.ResumeLayout(false);
            this.groupImport.ResumeLayout(false);
            this.groupImport.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupExport.ResumeLayout(false);
            this.groupExport.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblCustomer;
        private System.Windows.Forms.ComboBox cboCustomer;
        private System.Windows.Forms.TabControl tabs;
        private System.Windows.Forms.TabPage tabEmployers;
        private System.Windows.Forms.TabPage tabReasons;
        private System.Windows.Forms.TabPage tabAccomQuestions;
        private System.Windows.Forms.TabPage tabAccomTypes;
        private System.Windows.Forms.TabPage tabContactTypes;
        private System.Windows.Forms.TabPage tabCustomFields;
        private System.Windows.Forms.TabPage tabPaperwork;
        private System.Windows.Forms.TabPage tabTemplates;
        private System.Windows.Forms.TabPage tabPaySchedules;
        private System.Windows.Forms.TabPage tabRoles;
        private System.Windows.Forms.TabPage tabPolicies;
        private System.Windows.Forms.TabPage tabWorkflows;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.ListView listEmployers;
        private System.Windows.Forms.ColumnHeader colEmployerId;
        private System.Windows.Forms.ColumnHeader colEmployerName;
        private System.Windows.Forms.ColumnHeader colEmployerCB;
        private System.Windows.Forms.ListView listAbsenceReasons;
        private System.Windows.Forms.ColumnHeader colReasonCB;
        private System.Windows.Forms.ColumnHeader colARId;
        private System.Windows.Forms.ColumnHeader colARCode;
        private System.Windows.Forms.ColumnHeader colARName;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ListView listPaperwork;
        private System.Windows.Forms.ColumnHeader colPaperworkCB;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ListView listTemplates;
        private System.Windows.Forms.ColumnHeader colTemplateCB;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ListView listRoles;
        private System.Windows.Forms.ColumnHeader colRoleCB;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ListView listPolicies;
        private System.Windows.Forms.ColumnHeader colPolicyCB;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ListView listWorkflows;
        private System.Windows.Forms.ColumnHeader colWorkflowCB;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ListView listContactTypes;
        private System.Windows.Forms.ColumnHeader colContactTypeCB;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private System.Windows.Forms.ColumnHeader columnHeader27;
        private System.Windows.Forms.ColumnHeader columnHeader28;
        private System.Windows.Forms.ColumnHeader columnHeader29;
        private System.Windows.Forms.ListView listCustomFields;
        private System.Windows.Forms.ColumnHeader colCustomCB;
        private System.Windows.Forms.ColumnHeader columnHeader31;
        private System.Windows.Forms.ColumnHeader columnHeader32;
        private System.Windows.Forms.ColumnHeader columnHeader33;
        private System.Windows.Forms.ListView listAccomQuestions;
        private System.Windows.Forms.ColumnHeader colQuestionCB;
        private System.Windows.Forms.ColumnHeader columnHeader35;
        private System.Windows.Forms.ColumnHeader columnHeader36;
        private System.Windows.Forms.ColumnHeader columnHeader37;
        private System.Windows.Forms.ListView listAccomTypes;
        private System.Windows.Forms.ColumnHeader colAccomCB;
        private System.Windows.Forms.ColumnHeader columnHeader39;
        private System.Windows.Forms.ColumnHeader columnHeader40;
        private System.Windows.Forms.ColumnHeader columnHeader41;
        private System.Windows.Forms.ColumnHeader columnHeader42;
        private System.Windows.Forms.ListView listPaySchedules;
        private System.Windows.Forms.ColumnHeader columnHeader43;
        private System.Windows.Forms.ColumnHeader columnHeader44;
        private System.Windows.Forms.ColumnHeader columnHeader45;
        private System.Windows.Forms.ColumnHeader columnHeader46;
        private System.Windows.Forms.TextBox output;
        private System.Windows.Forms.TextBox txtFile;
        private System.Windows.Forms.Button btnOpenFile;
        private System.Windows.Forms.GroupBox groupImport;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupExport;
        private System.Windows.Forms.Button btnSelectAll;
    }
}

