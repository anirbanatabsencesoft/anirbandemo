﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Administration.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Threading;

namespace AbsenceSoft.Configuration
{
    public partial class ImportExport : Form
    {
        /// <summary>
        /// Thread safe background thread executor
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="error">The error.</param>
        public static void ThreadSafe(Action action, Action<Exception> error)
        {
            try
            {
                Dispatcher.CurrentDispatcher.Invoke(DispatcherPriority.Normal, new MethodInvoker(action));
            }
            catch (Exception ex)
            {
                if (error != null)
                    error(ex);
            }
        }

        /// <summary>
        /// Writes the output.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="args">The arguments.</param>
        private void WriteOutput(string format, params object[] args)
        {
            string message = string.Format("{0:yyyy-MM-dd HH:mm:ss.fff}: {1}{2}", DateTime.Now, string.Format(format, args), Environment.NewLine);
            output.Text += message;
            output.SelectionStart = output.TextLength - 1;
            output.ScrollToCaret();
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="ImportExport"/> class.
        /// </summary>
        public ImportExport()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the ThreadException event of the Application control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ThreadExceptionEventArgs"/> instance containing the event data.</param>
        private void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            WriteOutput("Unhandled Exception: {0}", e.Exception);
        }

        /// <summary>
        /// Handles the UnhandledException event of the CurrentDomain control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="UnhandledExceptionEventArgs"/> instance containing the event data.</param>
        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            WriteOutput("Unhandled Exception: {0}", e.ExceptionObject);
        }

        /// <summary>
        /// Handles the Load event of the ImportExport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ImportExport_Load(object sender, EventArgs e)
        {
            Disable();
            cboCustomer.Text = "(select a customer)";
            WriteOutput("Loading Customers...");
            ThreadSafe(() =>
            {
                var customers = Customer.Query.Find(Query.Null).OrderBy(c => c.Name).ToArray();
                cboCustomer.Items.AddRange(customers);
                WriteOutput("Ready...");
                Enable();
            }, ex => WriteOutput("ERROR Loading Customers: {0}", ex));
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the cboCustomer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void cboCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            WriteOutput("Loading Employers for Customer...");
            ThreadSafe(() =>
            {
                listEmployers.Items.Clear();
                List<Employer> employers = FetchEntities<Employer>();
                foreach (var emp in employers)
                    listEmployers.Items.Add(new ListViewItem(new string[] { emp.Id, emp.Id, emp.Name }));
                Enable();
                WriteOutput("Ready...");
                btnExport.Enabled = !string.IsNullOrWhiteSpace(CustomerId);
            }, ex => WriteOutput("ERROR Loading Employers: {0}", ex));
        }

        /// <summary>
        /// Binds the data.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="list">The list.</param>
        /// <param name="label">The label.</param>
        /// <param name="tab">The tab.</param>
        /// <param name="columns">The columns.</param>
        private void BindData<TEntity>(ListView list, string label, TabPage tab, Func<TEntity, string[]> columns) where TEntity : BaseEntity<TEntity>, new()
        {
            WriteOutput("Loading {0}", label);
            list.Items.Clear();
            FetchEntities<TEntity>().ForEach(r => list.Items.Add(new ListViewItem(columns(r))));
            WriteOutput("{0} {1} found", list.Items.Count, label);
        }

        /// <summary>
        /// Binds the configuration data.
        /// </summary>
        private void BindConfigurationData()
        {
            ThreadSafe(() =>
            {
                Disable();
                WriteOutput("Loading Employer mapper");
                Dictionary<string, string> employers = new Dictionary<string, string>();
                foreach (ListViewItem item in listEmployers.Items)
                    employers[item.Text] = item.SubItems[2].Text;
                var employerName = new Func<string, string>(id => string.IsNullOrWhiteSpace(id) || !employers.ContainsKey(id) ? "" : employers[id]);
                WriteOutput("Employer mapper loaded");

                BindData<AbsenceReason>(listAbsenceReasons, "Absence Reasons", tabReasons, r => new string[] { r.Id, r.Id, employerName(r.EmployerId), r.Code, r.Name });
                BindData<Template>(listTemplates, "Templates", tabTemplates, r => new string[] { r.Id, r.Id, employerName(r.EmployerId), r.Code, r.Name });
                BindData<Paperwork>(listPaperwork, "Paperwork", tabPaperwork, r => new string[] { r.Id, r.Id, employerName(r.EmployerId), r.Name });
                BindData<Role>(listRoles, "Roles", tabRoles, r => new string[] { r.Id, r.Id, r.Name });
                BindData<Policy>(listPolicies, "Policies", tabPolicies, r => new string[] { r.Id, r.Id, employerName(r.EmployerId), r.Code, r.Name });
                BindData<Workflow>(listWorkflows, "Workflows", tabWorkflows, r => new string[] { r.Id, r.Id, employerName(r.EmployerId), r.Code, r.Name });
                BindData<ContactType>(listContactTypes, "Contact Types", tabContactTypes, r => new string[] { r.Id, r.Id, employerName(r.EmployerId), r.Code, r.Name });
                BindData<CustomField>(listCustomFields, "Custom Fields", tabCustomFields, r => new string[] { r.Id, r.Id, employerName(r.EmployerId), r.Name });
                BindData<PaySchedule>(listPaySchedules, "Pay Schedules", tabPaySchedules, r => new string[] { r.Id, r.Id, employerName(r.EmployerId), r.Name });
                BindData<AccommodationType>(listAccomTypes, "Accommodation Types", tabAccomTypes, r => new string[] { r.Id, r.Id, employerName(r.EmployerId), r.Code, r.Name });
                BindData<AccommodationQuestion>(listAccomQuestions, "Accomodation Questions", tabAccomQuestions, r => new string[] { r.Id, r.Id, employerName(r.EmployerId), r.Question });

                WriteOutput("Ready...");
                Enable();
            }, ex => WriteOutput("ERROR Loading Customer + Employer Configuration Data: {0}", ex));
        }

        /// <summary>
        /// Handles the ItemChecked event of the listEmployers control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ItemCheckedEventArgs"/> instance containing the event data.</param>
        private void listEmployers_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            BindConfigurationData();
        }

        /// <summary>
        /// Gets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public string CustomerId
        {
            get
            {
                return (cboCustomer.SelectedItem == null || (cboCustomer.SelectedItem as Customer) == null || string.IsNullOrWhiteSpace((cboCustomer.SelectedItem as Customer).Id)) ? null :
                    (cboCustomer.SelectedItem as Customer).Id;
            }
        }

        /// <summary>
        /// Gets the selected employers.
        /// </summary>
        /// <value>
        /// The selected employers.
        /// </value>
        protected IEnumerable<string> SelectedEmployers
        {
            get
            {
                return listEmployers.CheckedItems.OfType<ListViewItem>()
                    .Where(i => !string.IsNullOrWhiteSpace(i.Text))
                    .Select(i => i.Text);
            }
        }

        /// <summary>
        /// Enables this instance.
        /// </summary>
        protected void Enable()
        {
            cboCustomer.Enabled = true;
            tabs.Enabled = true;
            btnExport.Enabled = true;
            btnImport.Enabled = true;
        }

        /// <summary>
        /// Disables this instance.
        /// </summary>
        protected void Disable()
        {
            cboCustomer.Enabled = false;
            tabs.Enabled = false;
            btnExport.Enabled = false;
            btnImport.Enabled = false;
        }

        /// <summary>
        /// Fetches the entities.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns></returns>
        private List<TEntity> FetchEntities<TEntity>() where TEntity : BaseEntity<TEntity>, new()
        {
            if (CustomerId == null)
                return new List<TEntity>(0);

            List<BsonValue> employers = SelectedEmployers.Select(e => (BsonValue)new ObjectId(e)).ToList();
            employers.Insert(0, BsonNull.Value);
            List<IMongoQuery> ands = new List<IMongoQuery>()
            {
                // Always ensure it's our customer's data, no mater what
                Query.EQ("CustomerId", new ObjectId(CustomerId)),
                // Ensure it meets the requirements of one of the passed in EmployerIds, OR, it's a core customer only item
                Query.Or(
                    Query.In("EmployerId", employers),
                    Query.NotExists("EmployerId")
                ),
                // Ensure not employee specific info, that would be bad
                Query.NotExists("EmployeeId"),
                Query.NotExists("CaseId")
            };
            var query = Query.And(ands);
            return BaseEntity<TEntity>.Query.Find(query).ToList();
        }

        /// <summary>
        /// Handles the Click event of the btnExport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnExport_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(CustomerId))
                return;
            ThreadSafe(() =>
            {
                ImportExportSettings settings = new ImportExportSettings()
                {
                    IncludeCustomer = true
                };
                settings.IncludeEmployers = listEmployers.CheckedItems.Count > 0;
                settings.IncludeAbsenceReasons = listAbsenceReasons.CheckedItems.Count > 0;
                settings.IncludeAccommodationQuestions = listAccomQuestions.CheckedItems.Count > 0;
                settings.IncludeAccommodationTypes = listAccomTypes.CheckedItems.Count > 0;
                settings.IncludeContactTypes = listContactTypes.CheckedItems.Count > 0;
                settings.IncludeCustomFields = listCustomFields.CheckedItems.Count > 0;
                settings.IncludePaperwork = listPaperwork.CheckedItems.Count > 0;
                settings.IncludePaySchedules = listPaySchedules.CheckedItems.Count > 0;
                settings.IncludePolicies = listPolicies.CheckedItems.Count > 0;
                settings.IncludeRoles = listRoles.CheckedItems.Count > 0;
                settings.IncludeTemplates = listTemplates.CheckedItems.Count > 0;
                settings.IncludeWorkflows = listWorkflows.CheckedItems.Count > 0;

                settings.EmployerIds = listEmployers.CheckedItems.OfType<ListViewItem>().Select(i => i.Text).ToList();
                settings.AbsenceReasons = listAbsenceReasons.CheckedItems.OfType<ListViewItem>().Select(i => i.Text).ToList();
                settings.AccommodationQuestions = listAccomQuestions.CheckedItems.OfType<ListViewItem>().Select(i => i.Text).ToList();
                settings.AccommodationTypes = listAccomTypes.CheckedItems.OfType<ListViewItem>().Select(i => i.Text).ToList();
                settings.ContactTypes = listContactTypes.CheckedItems.OfType<ListViewItem>().Select(i => i.Text).ToList();
                settings.CustomFields = listCustomFields.CheckedItems.OfType<ListViewItem>().Select(i => i.Text).ToList();
                settings.Paperwork = listPaperwork.CheckedItems.OfType<ListViewItem>().Select(i => i.Text).ToList();
                settings.PaySchedules = listPaySchedules.CheckedItems.OfType<ListViewItem>().Select(i => i.Text).ToList();
                settings.Policies = listPolicies.CheckedItems.OfType<ListViewItem>().Select(i => i.Text).ToList();
                settings.Roles = listRoles.CheckedItems.OfType<ListViewItem>().Select(i => i.Text).ToList();
                settings.Templates = listTemplates.CheckedItems.OfType<ListViewItem>().Select(i => i.Text).ToList();
                settings.Workflows = listWorkflows.CheckedItems.OfType<ListViewItem>().Select(i => i.Text).ToList();

                WriteOutput("Starting export of data for Customer...");
                ImportExportFile file = new ImportExportService(CustomerId, null) { Output = s => WriteOutput(s) }.Using(s => s.Export(settings));
                WriteOutput("Output complete for file '{0}', Prompting Save As...", file.FileName);
                var saveExport = new SaveFileDialog()
                {
                    DefaultExt = "json",
                    FileName = file.FileName,
                    OverwritePrompt = true,
                    Title = "Save Export JSON File"
                };
                var result = saveExport.ShowDialog(this);
                switch (result)
                {
                    case DialogResult.OK:
                    case DialogResult.Yes:
                        new BinaryWriter(saveExport.OpenFile()).Using(s => s.Write(file.File));
                        WriteOutput("Export file saved to '{0}'", saveExport.FileName);
                        break;
                    case DialogResult.Abort:
                    case DialogResult.Cancel:
                    case DialogResult.Ignore:
                    case DialogResult.No:
                    case DialogResult.None:
                    default:
                        WriteOutput("Export file cancelled");
                        break;
                }
            }, ex => WriteOutput("ERROR Building Export: {0}", ex));
        }

        /// <summary>
        /// Handles the Click event of the btnImport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnImport_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show(string.Format("Import the file \"{0}\"?", Path.GetFileName(txtFile.Text)), "Confirm Import", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result != System.Windows.Forms.DialogResult.Yes)
                return;
            ThreadSafe(() =>
            {
                if (string.IsNullOrWhiteSpace(txtFile.Text) || !File.Exists(txtFile.Text))
                    return;
                WriteOutput("Importing file '{0}'", txtFile.Text);
                var json = Encoding.UTF8.GetString(File.ReadAllBytes(txtFile.Text));
                WriteOutput("Reading Import File: {0}", json);
                ImportExportFile file = JsonConvert.DeserializeObject<ImportExportFile>(json);
                WriteOutput("Starting Import...");
                new ImportExportService(file.CustomerId, null) { Output = s => WriteOutput(s) }.Using(svc => svc.Import(file));
                WriteOutput("Import of file '{0}' was successful!", txtFile.Text);
                txtFile.Text = "";
            }, ex => WriteOutput("ERROR Performing Import: {0}", ex));
        }

        /// <summary>
        /// Handles the ColumnClick event of the listEmployers control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ColumnClickEventArgs"/> instance containing the event data.</param>
        private void listEmployers_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (e.Column > 0)
                return;
            bool check = listEmployers.Items.OfType<ListViewItem>().Any(i => !i.Checked);
            listEmployers.BeginUpdate();
            listEmployers.Items.OfType<ListViewItem>().Where(i => i.Checked != check).ForEach(i => i.Checked = check);
            listEmployers.EndUpdate();
            if (listEmployers.Items.Count > 0)
                listEmployers_ItemChecked(sender, new ItemCheckedEventArgs(listEmployers.Items.OfType<ListViewItem>().LastOrDefault()));
        }

        /// <summary>
        /// Handles the ColumnClick event of the list control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ColumnClickEventArgs"/> instance containing the event data.</param>
        private void list_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (e.Column > 0)
                return;
            ListView view = sender as ListView;
            if (view == null)
                return;
            bool check = view.Items.OfType<ListViewItem>().Any(i => !i.Checked);
            view.Items.OfType<ListViewItem>().Where(i => i.Checked != check).ForEach(i => i.Checked = check);
        }

        /// <summary>
        /// Handles the FormClosing event of the ImportExport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="FormClosingEventArgs"/> instance containing the event data.</param>
        private void ImportExport_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Application.ThreadException -= Application_ThreadException;
            //AppDomain.CurrentDomain.UnhandledException -= CurrentDomain_UnhandledException;
        }

        /// <summary>
        /// Handles the Click event of the btnOpenFile control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            var openImport = new OpenFileDialog()
            {
                DefaultExt = "json",
                CheckFileExists = true,
                CheckPathExists = true,
                Title = "Open Import JSON File"
            };
            var result = openImport.ShowDialog(this);
            switch (result)
            {
                case DialogResult.OK:
                case DialogResult.Yes:
                    txtFile.Text = openImport.FileName;
                    break;
                case DialogResult.Abort:
                case DialogResult.Cancel:
                case DialogResult.Ignore:
                case DialogResult.No:
                case DialogResult.None:
                default:
                    txtFile.Text = "";
                    break;
            }
            btnImport.Enabled = !string.IsNullOrWhiteSpace(txtFile.Text);
        }

        /// <summary>
        /// Handles the DragDrop event of the ImportExport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DragEventArgs"/> instance containing the event data.</param>
        private void ImportExport_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {

                var data = ((string[])e.Data.GetData(DataFormats.FileDrop)).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(data))
                {
                    txtFile.Text = data;
                    btnImport.Enabled = !string.IsNullOrWhiteSpace(txtFile.Text);
                }
            }
        }

        /// <summary>
        /// Handles the DragEnter event of the ImportExport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DragEventArgs"/> instance containing the event data.</param>
        private void ImportExport_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
        }

        private void ImportExport_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.S)
            {
                var saveExport = new SaveFileDialog()
                {
                    DefaultExt = "txt",
                    FileName = "output.txt",
                    OverwritePrompt = true,
                    Title = "Save Output Log"
                };
                var result = saveExport.ShowDialog(this);
                switch (result)
                {
                    case DialogResult.OK:
                    case DialogResult.Yes:
                        new BinaryWriter(saveExport.OpenFile()).Using(s => s.Write(output.Text));
                        break;
                }
            }
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            var selectedTab = tabs.SelectedTab;
            foreach (var control in selectedTab.Controls)
            {
                if(control is ListView)
                {
                    var listView = ((ListView)control);
                    for(int i = 0; i < listView.Items.Count; i++)
                    {
                        listView.Items[i].Checked = true;
                    }
                }
            }
        }
    }
}
