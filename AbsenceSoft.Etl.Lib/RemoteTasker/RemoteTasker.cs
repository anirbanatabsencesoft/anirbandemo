﻿using AbsenceSoft.Etl.Data.Sync;
using Npgsql;
using System;
using System.Linq;
using System.Reflection;

namespace AbsenceSoft.Etl.Lib
{
    public class RemoteTasker : BaseSynchronizer
    {
        /// <summary>
        /// When implemented in a parent class, gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return -1; } }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has error.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has error; otherwise, <c>false</c>.
        /// </value>
        public bool HasError { get; set; }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="RemoteTasker"/> class.
        /// </summary>
        public RemoteTasker() : base() { }

        /// <summary>
        /// Called when [syncronize].
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Registers the specified task client.
        /// </summary>
        /// <param name="taskClient">The task client.</param>
        public void Register(TaskClient taskClient)
        {
            Config config = null;
            DataManagerRemoteTasker dataManagerRemoteTasker = null;
            try
            {
                config = new Config();
                dataManagerRemoteTasker = new DataManagerRemoteTasker(SyncronizationConnectionString);
                dataManagerRemoteTasker.LoadTaskClientByProcessName(taskClient);
                if (!dataManagerRemoteTasker.HasError)
                {
                    if (taskClient.TaskClientId == 0)
                    {
                        //new service
                        dataManagerRemoteTasker.SaveTaskClient(taskClient);
                        if (dataManagerRemoteTasker.HasError)
                        {
                            HasError = true;
                            new SyncLogError(SyncronizationConnectionString)
                            {
                                MsgSource = "RemoteTasker.Register",
                                ErrorMessage = dataManagerRemoteTasker.ErrorMessage,
                                StackTrace = dataManagerRemoteTasker.StackTrace
                            }.SaveSyncLogError();
                        }
                    }
                    else
                    {
                        //update service info
                        dataManagerRemoteTasker.UpdateTaskClientInit(taskClient);
                        if (dataManagerRemoteTasker.HasError)
                        {
                            HasError = true;
                            new SyncLogError(SyncronizationConnectionString)
                            {
                                MsgSource = "RemoteTasker.Register",
                                ErrorMessage = dataManagerRemoteTasker.ErrorMessage,
                                StackTrace = dataManagerRemoteTasker.StackTrace
                            }.SaveSyncLogError();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                HasError = true;
                new SyncLogError(SyncronizationConnectionString) { MsgSource = "RemoteTasker.Register", ErrorMessage = ex.Message, StackTrace = ex.ToString() }.SaveSyncLogError();
            }
        }

        /// <summary>
        /// Gets the synchronize manager identifier.
        /// </summary>
        /// <param name="synchronizationCode">The synchronization code.</param>
        /// <returns></returns>
        public long? GetSyncManagerId(string synchronizationCode)
        {
            try
            {
                using (NpgsqlConnection conn = new NpgsqlConnection(SyncronizationConnectionString))
                using (NpgsqlCommand cmd = new NpgsqlCommand(string.Format("select dimension_id from operational.dimension where code = '{0}' limit 1", synchronizationCode), conn))
                {

                        conn.Open();

                    var retVal = cmd.ExecuteScalar();
                    if (retVal != null && retVal != DBNull.Value)
                        return (long)retVal;
                }
            }
            catch (Exception ex)
            {
                HasError = true;
                new SyncLogError(SyncronizationConnectionString) { MsgSource = "RemoteTasker.GetSyncManagerId", ErrorMessage = ex.Message, StackTrace = ex.ToString() }.SaveSyncLogError();
            }
            return null;
        }

        /// <summary>
        /// Requests the synchronize task.
        /// </summary>
        /// <param name="taskClient">The task client.</param>
        public void RequestSyncTask(TaskClient taskClient)
        {
            try
            {
                Config config = new Config();

                SyncManager syncManager = new SyncManager(SyncronizationConnectionString) { SyncManagerId = taskClient.SyncManagerId };
                syncManager.LoadSyncManager();

                if ((!syncManager.IsSuspended) && (TaskClient != null))
                {
                    // attempt to obtain a lock for gathering sync data
                    syncManager.LockTaskClientId = TaskClient.TaskClientId;
                    syncManager.IsLocked = true;
                    syncManager.SetSyncManagerLock();
                    // verify a lock was obtained by checking the task client id
                    syncManager.LoadSyncManager();

                    if ((!syncManager.HasError) && (syncManager.IsLocked) && (syncManager.LockTaskClientId > 0))
                    {
                        // service has obtained a lock and ready to sync
                        Sync(syncManager, taskClient);
                        // end release lock so other processes can do sync
                        syncManager.ReleaseSyncManagerLock();
                    }
                    else if (syncManager.HasError)
                    {
                        HasError = true;
                        LogError(syncManager.ErrorMessage, syncManager.StackTrace);
                    }
                }
            }
            catch (Exception ex)
            {
                HasError = true;
                new SyncLogError(SyncronizationConnectionString) { MsgSource = "RemoteTasker.RequestSyncTask", ErrorMessage = ex.Message, StackTrace = ex.ToString() }.SaveSyncLogError();
            }
        }

        /// <summary>
        /// Loads the task client.
        /// </summary>
        /// <param name="taskClient">The task client.</param>
        public void LoadTaskClient(TaskClient taskClient)
        {
            try
            {
                DataManagerRemoteTasker dataManagerRemoteTasker = null;
                Config config = new Config();
                dataManagerRemoteTasker = new DataManagerRemoteTasker(SyncronizationConnectionString);
                dataManagerRemoteTasker.LoadTaskClient(taskClient);
                TaskClient = taskClient;
            }
            catch (Exception ex)
            {
                HasError = true;
                new SyncLogError(SyncronizationConnectionString) { MsgSource = "RemoteTasker.LoadTaskClient", ErrorMessage = ex.Message, StackTrace = ex.ToString() }.SaveSyncLogError();
            }

        }

        /// <summary>
        /// Synchronizes the specified synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        /// <param name="taskClient">The task client.</param>
        public void Sync(SyncManager syncManager, TaskClient taskClient)
        {
            Config config = new Config();

            try
            {
                SyncLogInfo logInfo = null;
                logInfo = new SyncLogInfo(SyncronizationConnectionString) { DimensionId = syncManager.DimensionId, MsgSource = "SERVICE", MsgAction = "STARTING-TASKS", MsgText = "Starting main ETL tasks" };
                logInfo.SaveSyncLogInfo();
                
                var startTime = DateTime.UtcNow;

                Type pBaseType = typeof(BaseSynchronizer);
                BaseSynchronizer syncer = null;
                foreach (var syncerType in Assembly.GetAssembly(pBaseType).GetTypes().Where(t => t != pBaseType && t != typeof(RemoteTasker) && t.IsSubclassOf(pBaseType)))
                {
                    BaseSynchronizer pBase = Activator.CreateInstance(syncerType) as BaseSynchronizer;
                    if (pBase.DimensionId == syncManager.DimensionId)
                    {
                        syncer = pBase;
                        break;
                    }
                }
                if (syncer == null)
                    return;

                syncer.TaskSyncManager = TaskSyncManager = syncManager;
                syncer.TaskClient = TaskClient = taskClient;
                syncer.Syncronize();

                if (syncManager.HasError)
                {
                    taskClient.HasError = true;
                    taskClient.ErrorMessage = syncManager.ErrorMessage;
                    taskClient.StackTrace = syncManager.StackTrace;
                }
                
                string totalTime = "{totaltime=" + (DateTime.UtcNow - startTime) + ",pid=" + System.Threading.Thread.CurrentThread.ManagedThreadId + ",outcome=" + (syncManager.HasError ? "error" : "success") + "}";
                logInfo = new SyncLogInfo(SyncronizationConnectionString) { MsgSource = "SERVICE", MsgAction = "TOTAL-BATCH-TIME", MsgText = totalTime, DimensionId = syncManager.DimensionId };
                logInfo.SaveSyncLogInfo();
            }
            catch (Exception ex)
            {
                HasError = true;
                new SyncLogError(SyncronizationConnectionString) { MsgSource = "SERVICE-TASKS", ErrorMessage = ex.Message, StackTrace = ex.ToString() }.SaveSyncLogError();
            }
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            throw new NotImplementedException();
        }
    }
}
