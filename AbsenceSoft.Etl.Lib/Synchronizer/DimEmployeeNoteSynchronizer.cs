﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using Newtonsoft.Json;

namespace AbsenceSoft.Etl.Lib
{
    /// <summary>
    /// Employee Note Synchronizer
    /// </summary>
    public class DimEmployeeNoteSynchronizer : BaseSynchronizer
    {
        /// <summary>
        /// Dimension Id
        /// </summary>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_EMPLOYEE_NOTE; } }
        /// <summary>
        /// On Synchronize Event
        /// </summary>
        /// <param name="syncManager"></param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<EmployeeNote, DimEmployeeNote>(syncManager, PopulateDimEmployeeNoteFromEmployeeNote, null);
        }
        /// <summary>
        /// Convert Employee Note to Dim Employee Note
        /// </summary>
        /// <param name="employeeNote"></param>
        /// <returns></returns>
        private DimEmployeeNote PopulateDimEmployeeNoteFromEmployeeNote(EmployeeNote employeeNote)
        {
            DimEmployeeNote dimEmployeeNote = null;
            dimEmployeeNote = new DimEmployeeNote(DataWarehouseConnectionString)
            {
                ExternalReferenceId = employeeNote.Id,
                ExternalCreatedDate = employeeNote.CreatedDate,
                ExternalModifiedDate = employeeNote.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = 0,
                VersionPreviousId = 0,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                CreatedBy = employeeNote.CreatedById,
                ModifiedBy = employeeNote.ModifiedById,
                EmployeeId = employeeNote.EmployeeId,
                EmployerId = employeeNote.EmployerId,
                CustomerId = employeeNote.CustomerId,
                Note = employeeNote.Notes,
                IsPublic = employeeNote.Public
            };

            dimEmployeeNote.CategoryCode = employeeNote.Categories?.FirstOrDefault()?.CategoryCode;

            dimEmployeeNote.CategoryName = employeeNote.Categories?.FirstOrDefault()?.CategoryName;

            return dimEmployeeNote;
        }
        /// <summary>
        /// On Retry Event
        /// </summary>
        /// <param name="failure"></param>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<EmployeeNote, DimEmployeeNote>(failure, PopulateDimEmployeeNoteFromEmployeeNote);
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<EmployeeNote>();
        }
    }
}
