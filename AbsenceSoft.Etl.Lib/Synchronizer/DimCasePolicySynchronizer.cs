﻿using AbsenceSoft.Common.Security;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using System;
using System.Collections.Generic;
using System.Linq;


namespace AbsenceSoft.Etl.Lib
{
    public sealed class DimCasePolicySynchronizer : BaseSynchronizer
    {
        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_CASE_POLICY; } }

        /// <summary>
        /// Initializes a new instance of the DimCasePolicySynchronizer class.
        /// </summary>
        public DimCasePolicySynchronizer() : base() { }


        /// <summary>
        /// Does the synchronize work for Case Policies.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            // This base method mimics the "simple" perform sync method, however handles the
            //  scneario where we're syncing a CHILD class nested within an array, a BaseNonEntity,
            //  within our parent. The parent is the "controller" collection meaning it hosts the
            //  primary query collection, the modified date and entity id for sync coordination and is
            //  used by the sync coordinator.
            //
            // Secondly, this method takes some additional generic types to help it along the way
            //  where you're passinng in:
            //  - the PARENT entity type (TEntity)
            //  - the INTERMEDIARY type (this can be anything, if you don't need one, use bool OR byte for smallest memory footprint
            //      in our case we need the CaseSegment for the applied policy we're mapping, so we type that accordingly
            //  - the CHILD non-entity type we're actually going to map and save in the dimension table
            //  - the DIMENSION class type for that child's mapper to the dimension table itself
            PerformChildOfDimensionalSync<Case, CaseSegment, AppliedPolicy, DimCasePolicy>(
                // Pass in our sync manager
                syncManager,
                // This lambda passes you the parent entity and then allows the dimension mapper to load history for it
                //  This is necessary because the history is rectified against the new child collection for the parent
                //  entity and any not present any longer are marked as deleted in the data warehouse. This is because
                //  our MongoDB entities in arrays don't have "soft" delete enabled, so we have to detect it and take
                //  action on it by loading the dimensional table history here.
                (@case, dimPolicy) => dimPolicy.LoadHistory(@case.Id),
                // Here we're given the parent entity (case in this instance) and the goal is to return a collection of
                //  Tuples containing the intermediary type AND our child object. Intermediary can be null, or any
                //  type remember, as long as it's not used downstream you can make it whatever.
                @case =>
                {
                    // Get our initialized tuple list
                    List<Tuple<CaseSegment, AppliedPolicy>> children = new List<Tuple<CaseSegment, AppliedPolicy>>();
                    // Determine if we have any segments
                    if (@case.Segments != null)
                    { 
                        // Loop through each segment
                        foreach (var segment in @case.Segments)
                        {
                            // Determine if this segment has any policies applied to it
                            if (segment.AppliedPolicies != null)
                            {
                                // Add the applied policies to the collection passing the parent segment as the intermediary
                                //  of the tuple Item1.
                                children.AddRange(segment.AppliedPolicies.Select(ap => new Tuple<CaseSegment, AppliedPolicy>(segment, ap)));
                                // Return our populated or empty children collection of applied policy and segment tuples
                                
                            }
                        }
                        //delete the case policy and case policy usage records if segments are present.
                        DimCasePolicy dimCP = new DimCasePolicy(DataWarehouseConnectionString) { };
                        dimCP.CaseId = @case.Id;
                        dimCP.Delete();
                        DimCasePolicyUsage cpUsage = new DimCasePolicyUsage(DataWarehouseConnectionString) { };
                        cpUsage.CaseId = @case.Id;
                        cpUsage.Delete();
                    }
                    return children;
                },
                // This is our mapper delegate method
                PopulateDimCasePolicyFromAppliedPolicy,
                // We need to compare the new DimCasePolicy record with the old if an old one exists.
                //  This ensures we're not duplicating work or inserting more  history than we need to, especially
                //  if there were no changes to that record since last time the case was updated
                (dimPolicy, ap) => dimPolicy != null && dimPolicy.RecordHash != Crypto.Sha256Hash(ap.ToJSON()),
                null,
                true,
                // We can pass as many "additional" dimensions that we need to here, which will get called in order passed
                //  once the original dimension record version has been saved/inserted into the database and we have a
                //  populated dimension id for that. The populated/saved dimension class, original parent and child get
                //  passed to these methods for additional work that needs to be done
                (dimPolicy, @case, ap, conn) =>
                {
                    //check for policy usage and first delete if exists then save
                    if (ap.Usage != null && ap.Usage.Any())
                    {
                        
                        //save
                        foreach (AppliedPolicyUsage usage in ap.Usage)
                            PopulateDimCasePolicyUsage(usage, dimPolicy).Save(conn);
                    }
                }
                );
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformChildOfDimensionalRetry<Case, CaseSegment, AppliedPolicy, DimCasePolicy>(
                failure,
                (@case, dimPolicy) => dimPolicy.LoadHistory(@case.Id),
                @case =>
                {
                    // Get our initialized tuple list
                    List<Tuple<CaseSegment, AppliedPolicy>> children = new List<Tuple<CaseSegment, AppliedPolicy>>();
                    // Determine if we have any segments
                    if (@case.Segments != null)
                    { 
                        // Loop through each segment
                        foreach (var segment in @case.Segments)
                            // Determine if this segment has any policies applied to it
                            if (segment.AppliedPolicies != null)
                                // Add the applied policies to the collection passing the parent segment as the intermediary
                                //  of the tuple Item1.
                                children.AddRange(segment.AppliedPolicies.Select(ap => new Tuple<CaseSegment, AppliedPolicy>(segment, ap)));
                        // Return our populated or empty children collection of applied policy and segment tuples

                    }
                    //delete the case policy and case policy usage records if segments are present.
                    DimCasePolicy dimCP = new DimCasePolicy(DataWarehouseConnectionString) { };
                    dimCP.CaseId = @case.Id;
                    dimCP.Delete();
                    DimCasePolicyUsage cpUsage = new DimCasePolicyUsage(DataWarehouseConnectionString) { };
                    cpUsage.CaseId = @case.Id;
                    cpUsage.Delete();
                    return children;
                },
                PopulateDimCasePolicyFromAppliedPolicy,
                (dimPolicy, ap) => dimPolicy != null && dimPolicy.RecordHash != Crypto.Sha256Hash(ap.ToJSON()),
                (dimPolicy, @case, ap) =>
                {
                    //check for policy usage and first delete if exists then save
                    if (ap.Usage != null && ap.Usage.Any())
                    {
                        //save
                        foreach (AppliedPolicyUsage usage in ap.Usage)
                            PopulateDimCasePolicyUsage(usage, dimPolicy).Save();
                    }
                });
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<Case>();
        }

        

        /// <summary>
        /// Populates the dim case policy from case and segments.
        /// </summary>
        /// <param name="parentCase"></param>
        /// <param name="segment"></param>
        /// <param name="policy"></param>
        /// <param name="verRootId"></param>
        /// <param name="verPrevId"></param>
        /// <returns></returns>
        private DimCasePolicy PopulateDimCasePolicyFromAppliedPolicy(Case parentCase, CaseSegment segment, AppliedPolicy policy, IEnumerable<DimCasePolicy> history)
        {
            var lastHistory = history == null || !history.Any() ? null : history.OrderBy(h => h.DimVerId).LastOrDefault();
            var determination = policy.Status == EligibilityStatus.Ineligible ? (int)AdjudicationSummaryStatus.NotEligible : GetDetermnation(policy.Usage);
            var deniedPolicy = policy.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied);
            var actualDuration = determination == (int)AdjudicationSummaryStatus.Approved ? ((DateTime)policy.EndDate - policy.StartDate).Days : 0;
            var dimCP = new DimCasePolicy(DataWarehouseConnectionString)
            {
                //ExternalReferenceId = policy.Id.ToString(),
               ExternalReferenceId =  Guid.NewGuid().ToString(),
                ExternalCreatedDate = segment.CreatedDate,
                ExternalModifiedDate = parentCase.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = parentCase.IsDeleted ? Const.VER_SEQ_DELETED_VALUE : Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = lastHistory == null ? 0 : lastHistory.VersionRootId,
                VersionPreviousId = lastHistory == null ? 0 : lastHistory.DimVerId,
                VersionHistoryCount = lastHistory == null ? Const.DEFAULT_HISTORY_COUNT : lastHistory.VersionHistoryCount + 1,

                //set the case parameters
                ExternalCreatedBy = string.IsNullOrWhiteSpace(parentCase.ModifiedById) ? parentCase.CreatedById : parentCase.ModifiedById,
                ExternalModifiedBy = parentCase.ModifiedById,
                CaseId = parentCase.Id,
                EmployeeId = parentCase.Employee.Id,
                CustomerId = parentCase.CustomerId,
                EmployerId = parentCase.EmployerId,
                PolicyCode = policy.Policy.Code,
                PolicyName = policy.Policy.Name,
                StartDate = policy.StartDate,
                EndDate = policy.EndDate,
                CaseType = segment == null ? 0 : (int)segment.Type,
                Eligibility = (int)policy.Status,
                Determination = determination,
                DenialReasonCode = deniedPolicy != null ? deniedPolicy.DenialReasonCode : null,
                DenialReasonDescription = deniedPolicy != null ? deniedPolicy.DenialReasonName : null,
                PayoutPercentage = null,
                ActualDuration = actualDuration,
                ExceededDuration = actualDuration < parentCase.Disability?.PrimaryPathMaxDays ? 0 : (actualDuration - parentCase.Disability?.PrimaryPathMaxDays) ,
                SegmentId = segment.Id.ToString()
            };

            if (policy.PolicyCrosswalkCodes != null)
            {
                dimCP.CrosswalkCodes = policy.PolicyCrosswalkCodes.Distinct(pcc => pcc.TypeCode)
                    .ToDictionary(pcc => pcc.TypeCode, pcc => pcc.Code);
            }

            return dimCP;
        }

        /// <summary>
        /// Get the determination for applied policy usages
        /// </summary>
        /// <param name="usages"></param>
        /// <returns></returns>
        private int GetDetermnation(List<AppliedPolicyUsage> usages)
        {
            if(usages != null && usages.Count > 0)
            {
                // If any are approved, then the case is approved
                if (usages.Any(u => u.Determination == AdjudicationStatus.Approved))
                    return (int)AdjudicationSummaryStatus.Approved;

                // If any are allowed for intermittent, then leave is allowed (sibling to approved)
                if (usages.Any(u => u.IntermittentDetermination == IntermittentStatus.Allowed))
                    return (int)AdjudicationSummaryStatus.Allowed;

                // If the entire thing is denied, then it's denied; otherwise should be pending
                if (usages.All(u => u.Determination == AdjudicationStatus.Denied || u.IntermittentDetermination == IntermittentStatus.Denied))
                    return (int)AdjudicationSummaryStatus.Denied;
            }

            // If all else then it's pending
            return (int)AdjudicationSummaryStatus.Pending;
        }

        /// <summary>
        /// Returns a new class for policy usage
        /// </summary>
        /// <param name="usage"></param>
        /// <param name="policy"></param>
        /// <returns></returns>
        private DimCasePolicyUsage PopulateDimCasePolicyUsage(AppliedPolicyUsage usage, DimCasePolicy policy)
        {
            DimCasePolicyUsage dimCPU = new DimCasePolicyUsage(DataWarehouseConnectionString)
            {
                //set values here
                ExternalReferenceId = usage.Id.ToString(),
                ExternalCreatedDate = policy.ExternalCreatedDate,
                ExternalModifiedDate = policy.ExternalModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = policy.VersionSequence,
                VersionRootId = policy.VersionRootId,
                VersionPreviousId = policy.VersionPreviousId,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                DimCasePolicyId = policy.DimCasePolicyId,
                ExternalCreatedBy = policy.ExternalCreatedBy,
                ExternalModifiedBy = policy.ExternalModifiedBy,
                CaseId = policy.CaseId,
                EmployeeId = policy.EmployeeId,
                CustomerId = policy.CustomerId,
                EmployerId = policy.EmployerId,
                PolicyCode = policy.PolicyCode,
                DateUsed = usage.DateUsed,
                MinutesUsed = int.TryParse(usage.MinutesUsed.ToString(), out int minutesUsed) ? (int?)minutesUsed : null,
                MinutesInDay = int.TryParse(usage.MinutesInDay.ToString(), out int minutesInDay) ? (int?)minutesInDay : null,
                HoursUsed = decimal.TryParse(usage.MinutesUsed.ToString(), out decimal hoursUsed) ? (hoursUsed / 60) : 0,
                HoursInDay = decimal.TryParse(usage.MinutesInDay.ToString(), out decimal hoursInDayUsed) ? (hoursInDayUsed / 60) : 0,
                UserEntered = usage.UserEntered,
                Determination = (int)usage.Determination,
                DenialReasonCode = usage.DenialReasonCode,
                DenialReasonDescription = usage.DenialReasonName,
                DeterminedBy = usage.DeterminedById,
                IntermittentDetermination = usage.IntermittentDetermination == null ? default(int?) : (int)usage.IntermittentDetermination,
                IsLocked = usage.IsLocked,
                UsesTime = usage.UsesTime,
                IsIntermittentRestriction = usage.IsIntermittentRestriction,
                PercentOfDay = decimal.TryParse(usage.PercentOfDay.ToString(), out decimal percentOfDay) ? (decimal?)percentOfDay : null,
                Status = (int)usage.SummaryStatus                 
            };

            return dimCPU;
        }
    }
}
