﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using AbsenceSoft.Common.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

namespace AbsenceSoft.Etl.Lib
{
    public sealed class DimEmployeeSynchronizer : BaseSynchronizer
    {
        // Cache properties.
        private Dictionary<string, Customer> _customers = null;
        private Dictionary<string, Employer> _employers = null;
        private Dictionary<string, User> _users = null;

        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_EMPLOYEE; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployeeSynchronizer"/> class.
        /// </summary>
        public DimEmployeeSynchronizer() : base() { }

        /// <summary>
        /// Does the synchronize work.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            _customers = new Dictionary<string, Customer>();
            _employers = new Dictionary<string, Employer>();
            _users = new Dictionary<string, User>();
            BatchReadCompleted += results =>
            {
                _customers = Customer.Repository.Collection.Find(Customer.Query.In(c => c.Id, _customers.Keys)).ToDictionary(r => r.Id);
                _employers = Employer.Repository.Collection.Find(Employer.Query.In(c => c.Id, _employers.Keys)).ToDictionary(r => r.Id);
                _users = User.Repository.Collection.Find(User.Query.In(c => c.Id, _users.Keys)).ToDictionary(r => r.Id);
            };
            PerformDimensionalSync<Employee, DimEmployee>(syncManager, PopulateDimEmployeeFromEmployee, employee =>
            {
                _customers[employee.CustomerId] = null;
                _employers[employee.EmployerId] = null;
                _users[employee.CreatedById] = null;
            });
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<Employee, DimEmployee>(failure, PopulateDimEmployeeFromEmployee);
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<Employee>();
        }

        /// <summary>
        /// Populates the dim employee from employee.
        /// </summary>
        /// <param name="employee">The employee.</param>
        /// <returns></returns>
        private DimEmployee PopulateDimEmployeeFromEmployee(Employee employee)
        {
            var dimEmployee = new DimEmployee(DataWarehouseConnectionString)
            {
                ExternalReferenceId = employee.Id,
                ExternalCreatedDate = employee.CreatedDate,
                ExternalModifiedDate = employee.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = 0,
                VersionPreviousId = 0,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                EmployeeName = employee.EmployerName,
                FirstName = employee.FirstName,
                MiddleName = employee.MiddleName,
                LastName = employee.LastName,
                Status = (int)employee.Status,
                Salary = employee.Salary,
                PayType = (employee.PayType != null) ? (int)employee.PayType : (int?)null,
                Gender = employee.Gender == null ? (int?)null : (int)employee.Gender.Value,
                Dob = employee.DoB,
                EmployeeNumber = employee.EmployeeNumber,
                FullName =employee.FullName,
                EmployeeClassCode = employee.EmployeeClassCode,
                EmployeeClassName = employee.EmployeeClassName,
                CostCenterCode = employee.CostCenterCode,
                JobTitle = employee.JobTitle,
                Department = employee.Department,
                WorkCountry = employee.WorkCountry,
                WorkState = employee.WorkState,
                PayScheduleId = employee.PayScheduleId,
                Meets50In75MileRule = employee.Meets50In75MileRule,
                IsKeyEmployee = employee.IsKeyEmployee,
                IsExempt = employee.IsExempt,
                SpouseEmployeeNumber = employee.SpouseEmployeeNumber,
                MilitaryStatus = (int)employee.MilitaryStatus,
                MilitaryStatusName = employee.MilitaryStatus.ToString().SplitCamelCaseString(),
                EmployerName = employee.EmployerName,
                Title = employee.Title,
                Description = employee.Description,
                EmployerId = employee.EmployerId,
                CustomerId = employee.CustomerId,
                ServiceDate = employee.ServiceDate,
                HireDate = employee.HireDate,
                ReHireDate = employee.RehireDate,
                TerminationDate = employee.TerminationDate,
                JobClassification = employee.JobActivity != null ? (int)employee.JobActivity : (int?)null,
                RiskProfileCode = employee.RiskProfile != null ? employee.RiskProfile.Code : null,
                RiskProfileName = employee.RiskProfile != null ? employee.RiskProfile.Name : null,
                RiskProfileDescription = employee.RiskProfile != null ? employee.RiskProfile.Description : null,
                RiskProfileOrder = employee.RiskProfile != null ? employee.RiskProfile.Order : (int?)null,
                StartDayOfWeek = employee.StartDayOfWeek != null ? employee.StartDayOfWeek.ToString() : null
            };
            try
            {
                dimEmployee.SSN = employee.Ssn != null && !string.IsNullOrWhiteSpace(employee.Ssn.Encrypted) ? employee.Ssn.Encrypted.Decrypt() : null;
            }
            catch
            {
                //LogError("Error decrypting SSN from cipher", ex.ToString());
            }

            //Set Employee Info Attributes
            var ei = employee.Info;
            if (ei != null)
            {
                dimEmployee.InfoWorkPhone = ei.WorkPhone;
                dimEmployee.InfoHomePhone = ei.HomePhone;
                dimEmployee.InfoCellPhone = ei.CellPhone;
                dimEmployee.InfoAltPhone = ei.AltPhone;
                dimEmployee.Email = ei.Email;
                dimEmployee.InfoEmail = ei.Email;
                dimEmployee.InfoAltemail = ei.AltEmail;
                dimEmployee.AltEmail = ei.AltEmail;
                dimEmployee.WorkPhone = ei.WorkPhone;
                dimEmployee.HomePhone = ei.HomePhone;
                dimEmployee.CellPhone = ei.CellPhone;
                dimEmployee.AltPhone = ei.AltPhone;
                dimEmployee.OfficeLocation = ei.OfficeLocation;

                if (ei.Address != null)
                {
                    dimEmployee.AddressName = ei.Address.Address1;
                    dimEmployee.AddressAddress1 = ei.Address.Address1;
                    dimEmployee.AddressAddress2 = ei.Address.Address2;
                    dimEmployee.AddressCity = ei.Address.City;
                    dimEmployee.AddressState = ei.Address.State;
                    dimEmployee.AddressPotalcode = ei.Address.PostalCode;
                    dimEmployee.AddressCountry = ei.Address.Country;
                }

                if (ei.AltAddress != null)
                {
                    dimEmployee.AltAddress1 = ei.AltAddress.Address1;
                    dimEmployee.AltAddress2 = ei.AltAddress.Address2;
                    dimEmployee.AltCity = ei.AltAddress.City;
                    dimEmployee.AltState = ei.AltAddress.State;
                    dimEmployee.AltPotalcode = ei.AltAddress.PostalCode;
                    dimEmployee.AltCountry = ei.AltAddress.Country;
                }
            }

            dimEmployee.Dob = employee.DoB;

            try
            {
                if (employee.WorkSchedules != null && employee.WorkSchedules.Any(w => DateTime.UtcNow.ToMidnight().DateInRange(w.StartDate, w.EndDate)))
                {
                    if (_customers != null && _customers.ContainsKey(employee.CustomerId) && _customers[employee.CustomerId] != null)
                        employee.Customer = _customers[employee.CustomerId];
                    if (_employers != null && _employers.ContainsKey(employee.EmployerId) && _employers[employee.EmployerId] != null)
                        employee.Employer = _employers[employee.EmployerId];
                    dimEmployee.HoursWorked = Logic.Cases.LeaveOfAbsence.TotalHoursWorkedLast12Months(employee);
                    dimEmployee.HoursWorkedAsOfDate = DateTime.UtcNow;
                }
            }
            catch
            {
                //if (!ex.Message.Contains("Employee does not have a work schedule defined"))
                //    LogError(string.Concat("Error calculating hours worked in last 12 months for '", employee.Id, "': ", ex.Message), ex.ToString());
            }



            if (employee.MinutesWorkedPerWeek != null && employee.MinutesWorkedPerWeek.Any())
            {
                if (_customers != null && _customers.ContainsKey(employee.CustomerId) && _customers[employee.CustomerId] != null)
                    employee.Customer = _customers[employee.CustomerId];
                if (_employers != null && _employers.ContainsKey(employee.EmployerId) && _employers[employee.EmployerId] != null)
                    employee.Employer = _employers[employee.EmployerId];
                dimEmployee.AverageMinutesWorkedPerWeek = Logic.Cases.LeaveOfAbsence.TotalMinutesWorkedPerWeek(employee);
                dimEmployee.AverageMinutesWorkedPerWeekAsOfDate = DateTime.UtcNow;
            }

            if (employee.CustomFields != null && employee.CustomFields.Any())
                dimEmployee.CustomFields = employee.CustomFields.Distinct(k => k.Code).ToDictionary(
                    f => f.Code.ToUpperInvariant(),
                    f => f.ValueType == AbsenceSoft.Data.Enums.CustomFieldValueType.SelectList ? f.SelectedValueText : f.SelectedValue);

            User user = null;
            if (_users == null || !_users.TryGetValue(employee.CreatedById, out user))
                user = employee.CreatedBy;
            if (user != null)
            {
                dimEmployee.EmployeeCreatedByEmail = user.Email;
                dimEmployee.EmployeeCreatedByFirstName = user.FirstName;
                dimEmployee.EmployeeCreatedByLastName = user.LastName;
            }
            var job = employee.GetCurrentJob();
            if (job != null)
            {
                dimEmployee.JobStartDate = job.Dates.StartDate;
                dimEmployee.JobEndDate = job.Dates.EndDate;
                dimEmployee.JobCode = job.JobCode;
                dimEmployee.JobName = job.JobName;
                dimEmployee.JobOfficeLocationCode = job.OfficeLocationCode;
                // This is pretty rare, but when it happens, okay fine, 1 more query won't hurt per record.
                if (!string.IsNullOrWhiteSpace(job.OfficeLocationCode))
                {
                    var office = job.OfficeLocation;
                    if (office != null)
                    {
                        dimEmployee.JobOfficeLocationCode = office.Code;
                        dimEmployee.JobOfficeLocationName = office.Name;
                    }
                }
            }

            dimEmployee.IsDeleted = employee.IsDeleted;
            if (employee.IsDeleted)
                dimEmployee.VersionSequence = Const.VER_SEQ_DELETED_VALUE;

            //set audit data
            dimEmployee.CreatedBy = employee.CreatedById;
            dimEmployee.ModifiedBy = employee.ModifiedById;

            return dimEmployee;
        }
    }
}
