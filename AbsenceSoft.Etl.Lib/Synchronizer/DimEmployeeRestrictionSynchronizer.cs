﻿using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using System.Linq;

namespace AbsenceSoft.Etl.Lib
{
    public sealed class DimEmployeeRestrictionSynchronizer : BaseSynchronizer
    {
        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_EMPLOYEE_RESTRICTION; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployeeRestrictionSynchronizer"/> class.
        /// </summary>
        public DimEmployeeRestrictionSynchronizer() : base() { }

        /// <summary>
        /// Called when [syncronize].
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<EmployeeRestriction, DimEmployeeRestriction>(syncManager, 
                PopulateDimEmployeeRestrictionFromEmployeeRestriction, 
                null,
                null,
                SaveEmployeeResctrictionEntry);
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<EmployeeRestriction>();
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<EmployeeRestriction, DimEmployeeRestriction>(failure, 
                PopulateDimEmployeeRestrictionFromEmployeeRestriction,
                SaveEmployeeResctrictionEntry);
        }

        /// <summary>
        /// Populates the dim employee restriction from employee restriction.
        /// </summary>
        /// <param name="employeerestriction">The employeerestriction.</param>
        /// <param name="verRootID">The ver root identifier.</param>
        /// <param name="verPrevID">The ver previous identifier.</param>
        /// <returns></returns>
        private DimEmployeeRestriction PopulateDimEmployeeRestrictionFromEmployeeRestriction(EmployeeRestriction employeerestriction)
        {
            DimEmployeeRestriction DimEmployeeRestriction = null;
            Demand demand = employeerestriction.Restriction == null ? null : employeerestriction.Restriction.Demand;
            DimEmployeeRestriction = new DimEmployeeRestriction(DataWarehouseConnectionString)
            {
                ExternalReferenceId = employeerestriction.Id,
                ExternalCreatedDate = employeerestriction.CreatedDate,
                ExternalModifiedDate = employeerestriction.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = 0,
                VersionPreviousId = 0,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                CaseId = employeerestriction.CaseId,                
                RestrictionName = demand == null ? null : demand.Name,
                RestrictionCode = demand == null ? null : demand.Code
            };

            if (employeerestriction.Restriction != null && employeerestriction.Restriction.Dates != null)
            {
                DimEmployeeRestriction.StartDate = employeerestriction.Restriction.Dates.StartDate;
                DimEmployeeRestriction.EndDate = employeerestriction.Restriction.Dates.EndDate;
            }

            DimEmployeeRestriction.IsDeleted = employeerestriction.IsDeleted;
            if (employeerestriction.IsDeleted)
                DimEmployeeRestriction.VersionSequence = Const.VER_SEQ_DELETED_VALUE;

            //set audit data
            DimEmployeeRestriction.CreatedBy = employeerestriction.CreatedById;
            DimEmployeeRestriction.ModifiedBy = employeerestriction.ModifiedById;

            return DimEmployeeRestriction;
        }

        /// <summary>
        /// Saves the employee resctriction entry.
        /// </summary>
        /// <param name="dimEmployeeRestriction">The dim employee restriction.</param>
        /// <param name="restriction">The restriction.</param>
        private void SaveEmployeeResctrictionEntry(DimEmployeeRestriction dimEmployeeRestriction, EmployeeRestriction restriction, Npgsql.NpgsqlConnection conn)
        {
            if (restriction.Restriction != null && restriction.Restriction.Values != null && restriction.Restriction.Values.Any())
            {
                DimEmployeeRestrictionEntry dimEmployeeRestrictionEntryTemp = new DimEmployeeRestrictionEntry(DataWarehouseConnectionString) { DimEmployeeRestrictionId = dimEmployeeRestriction.DimEmployeeRestrictionId };
                dimEmployeeRestrictionEntryTemp.Delete(conn);                
                // iterate and save in dim_user_roles
                foreach (AppliedDemandValue<WorkRestrictionValue> demandValue in restriction.Restriction.Values)
                {
                    DimEmployeeRestrictionEntry dimEmployeeRestrictionEntry = new DimEmployeeRestrictionEntry();
                    dimEmployeeRestrictionEntry.DimEmployeeRestrictionId = dimEmployeeRestriction.DimEmployeeRestrictionId;
                    dimEmployeeRestrictionEntry.CaseId = restriction.CaseId;
                    dimEmployeeRestrictionEntry.WorkRestrictionType = demandValue.DemandType != null ? demandValue.DemandType.Name : null;
                    dimEmployeeRestrictionEntry.WorkRestriction = demandValue.ToString();
                    dimEmployeeRestrictionEntry.ExtRefDemandTypeId = demandValue.DemandTypeId;
                    dimEmployeeRestrictionEntry.ConnectionString = DataWarehouseConnectionString;
                    dimEmployeeRestrictionEntry.Save(conn);
                    if (dimEmployeeRestrictionEntry.HasError)
                    {
                        dimEmployeeRestriction.HasError = dimEmployeeRestrictionEntry.HasError;
                        dimEmployeeRestriction.ErrorMessage = dimEmployeeRestrictionEntry.ErrorMessage;
                        dimEmployeeRestriction.StackTrace = dimEmployeeRestrictionEntry.StackTrace;
                        break;
                    }
                }
            }
        }

        private void SaveEmployeeResctrictionEntry(DimEmployeeRestriction dimEmployeeRestriction, EmployeeRestriction restriction)
        {
            if (restriction.Restriction != null && restriction.Restriction.Values != null && restriction.Restriction.Values.Any())
            {
                DimEmployeeRestrictionEntry dimEmployeeRestrictionEntryTemp = new DimEmployeeRestrictionEntry(DataWarehouseConnectionString) { DimEmployeeRestrictionId = dimEmployeeRestriction.DimEmployeeRestrictionId };
                dimEmployeeRestrictionEntryTemp.Delete();
                // iterate and save in dim_user_roles
                foreach (AppliedDemandValue<WorkRestrictionValue> demandValue in restriction.Restriction.Values)
                {
                    DimEmployeeRestrictionEntry dimEmployeeRestrictionEntry = new DimEmployeeRestrictionEntry();
                    dimEmployeeRestrictionEntry.DimEmployeeRestrictionId = dimEmployeeRestriction.DimEmployeeRestrictionId;
                    dimEmployeeRestrictionEntry.CaseId = restriction.CaseId;
                    dimEmployeeRestrictionEntry.WorkRestrictionType = demandValue.DemandType != null ? demandValue.DemandType.Name : null;
                    dimEmployeeRestrictionEntry.WorkRestriction = demandValue.ToString();
                    dimEmployeeRestrictionEntry.ExtRefDemandTypeId = demandValue.DemandTypeId;
                    dimEmployeeRestrictionEntry.ConnectionString = DataWarehouseConnectionString;
                    dimEmployeeRestrictionEntry.Save();
                    if (dimEmployeeRestrictionEntry.HasError)
                    {
                        dimEmployeeRestriction.HasError = dimEmployeeRestrictionEntry.HasError;
                        dimEmployeeRestriction.ErrorMessage = dimEmployeeRestrictionEntry.ErrorMessage;
                        dimEmployeeRestriction.StackTrace = dimEmployeeRestrictionEntry.StackTrace;
                        break;
                    }
                }
            }
        }
    }
}
