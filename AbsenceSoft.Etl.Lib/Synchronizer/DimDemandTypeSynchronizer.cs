﻿using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using System;

namespace AbsenceSoft.Etl.Lib
{
    public class DimDemandTypeSynchronizer : BaseSynchronizer
    {
        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_DEMAND_TYPE; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimDemandTypeSynchronizer"/> class.
        /// </summary>
        public DimDemandTypeSynchronizer() : base() { }

        /// <summary>
        /// Called when [syncronize].
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<DemandType, DimDemandType>(syncManager, PopulateDimDemandTypeFromDemandType, null);
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<DemandType>();
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<DemandType, DimDemandType>(failure, PopulateDimDemandTypeFromDemandType);
        }

        /// <summary>
        /// Populates the type of the dim demand type from demand.
        /// </summary>
        /// <param name="demandType">Type of the demand.</param>
        /// <returns></returns>
        private DimDemandType PopulateDimDemandTypeFromDemandType(DemandType demandType)
        {
            DimDemandType dimDemandType = null;
            dimDemandType = new DimDemandType(DataWarehouseConnectionString)
            {
                ExternalReferenceId = demandType.Id,
                ExternalCreatedDate = demandType.CreatedDate,
                ExternalModifiedDate = demandType.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = 0,
                VersionPreviousId = 0,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                CustomerId = demandType.CustomerId, 
                Code = demandType.Code,
                Description = demandType.Description,
                TypeData = Convert.ToString(demandType.Type),
                RequirementLabel = demandType.RequirementLabel,
                OrderData = demandType.Order
            };

            dimDemandType.IsDeleted = demandType.IsDeleted;
            if (demandType.IsDeleted)
                dimDemandType.VersionSequence = Const.VER_SEQ_DELETED_VALUE;

            //set audit data
            dimDemandType.CreatedBy = demandType.CreatedById;
            dimDemandType.ModifiedBy = demandType.ModifiedById;

            return dimDemandType;
        }
    }
}

