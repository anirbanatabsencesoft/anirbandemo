﻿using AbsenceSoft.Etl.Data.Sync;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Reflection;

namespace AbsenceSoft.Etl.Lib.Synchronizer
{
    public class RetryQueueSyncronizer : BaseSynchronizer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RetryQueueSyncronizer"/> class.
        /// </summary>
        public RetryQueueSyncronizer() : base() { }

        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.RETRY_QUEUE; } }

        /// <summary>
        /// Called when [syncronize].
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            TaskSyncManager = syncManager;

            // Get our failures and mourn them
            var failures = new SyncFailure(SyncronizationConnectionString).GetAllUnresolvedFailures(TaskClient.FailureRetryCoolOffPeriod).ToList();
            // If there are no failures, that's not a bad thing, just bail out of here like nothing happened
            if (!failures.Any())
                return;
            
            // Initialize our dimension syncer collection based on a distinct list of dimension ids to consider
            var failureDimensions = failures.Select(f => f.DimensionId).Distinct().ToList();
            // Then initialize a dictionary to store them
            ConcurrentDictionary<long, BaseSynchronizer> syncronizers = new ConcurrentDictionary<long, BaseSynchronizer>(Config.MaxDegreesOfParallelism * 4, failureDimensions.Count);

            try
            {
                // Build our syncronizers based on the dimension ids that we need.
                Type pBaseType = typeof(BaseSynchronizer);
                Assembly.GetAssembly(pBaseType)
                    .GetTypes()
                    .AsParallel()
                    .Where(t => t != pBaseType)
                    .Where(t => t != typeof(RetryQueueSyncronizer))
                    .Where(t => t != typeof(RemoteTasker))
                    .Where(t => t.IsSubclassOf(pBaseType))
                    .ForAll(syncerType =>
                {
                    BaseSynchronizer pBase = Activator.CreateInstance(syncerType) as BaseSynchronizer;
                    if (failureDimensions.Contains(pBase.DimensionId))
                    {
                        pBase.TaskSyncManager = syncManager;
                        pBase.TaskClient = TaskClient;
                        syncronizers.TryAdd(pBase.DimensionId, pBase);
                    }
                });
            }
            catch (Exception ex)
            {
                // Log the error and bounce out... If there's an issue/error here there's not much
                //  we can do about it.
                LogError(ex.Message, ex.ToString());
                return;
            }

            // At this point, we don't need to do anything else special, all error handling
            //  logging, retry logic, etc. are all handled within the Retry method of each
            //  syncronizer, or at least that's the presumption.
            failures.AsParallel().ForAll(f =>
            {
                f.ConnectionString = SyncronizationConnectionString;
                syncronizers[f.DimensionId].Retry(f);
            });
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.SourceCount = new SyncFailure(SyncronizationConnectionString).GetAllUnresolvedFailures(TimeSpan.Zero).Count;
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            throw new NotImplementedException();
        }
    }
}
