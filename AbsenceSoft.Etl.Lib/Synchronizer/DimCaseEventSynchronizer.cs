﻿using AbsenceSoft.Common.Security;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Etl.Lib
{
    public class DimCaseEventSynchronizer : BaseSynchronizer
    {

        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_CASE_EVENT_DATE; } }

        /// <summary>
        /// Initializes a new instance of the DimCasePolicySynchronizer class.
        /// </summary>
        public DimCaseEventSynchronizer() : base() { }


        /// <summary>
        /// Does the synchronize work for Case Policies.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            // This base method mimics the "simple" perform sync method, however handles the
            //  scneario where we're syncing a CHILD class nested within an array, a BaseNonEntity,
            //  within our parent. The parent is the "controller" collection meaning it hosts the
            //  primary query collection, the modified date and entity id for sync coordination and is
            //  used by the sync coordinator.
            //
            // Secondly, this method takes some additional generic types to help it along the way
            //  where you're passinng in:
            //  - the PARENT entity type (TEntity)
            //  - the INTERMEDIARY type (this can be anything, if you don't need one, use bool OR byte for smallest memory footprint
            //      in our case we need the CaseSegment for the applied policy we're mapping, so we type that accordingly
            //  - the CHILD non-entity type we're actually going to map and save in the dimension table
            //  - the DIMENSION class type for that child's mapper to the dimension table itself
            PerformChildOfDimensionalSync<Case, bool, CaseEvent, DimCaseEventDate>(
                // Pass in our sync manager
                syncManager,
                // This lambda passes you the parent entity and then allows the dimension mapper to load history for it
                //  This is necessary because the history is rectified against the new child collection for the parent
                //  entity and any not present any longer are marked as deleted in the data warehouse. This is because
                //  our MongoDB entities in arrays don't have "soft" delete enabled, so we have to detect it and take
                //  action on it by loading the dimensional table history here.
                (@case, dimEvent) => dimEvent.LoadHistory(@case.Id),
                // Here we're given the parent entity (case in this instance) and the goal is to return a collection of
                //  Tuples containing the intermediary type AND our child object. Intermediary can be null, or any
                //  type remember, as long as it's not used downstream you can make it whatever.
                @case =>
                {
                    // Get our initialized tuple list
                    List<Tuple<bool, CaseEvent>> children = new List<Tuple<bool, CaseEvent>>();
                    // Determine if we have any segments
                    if (@case.CaseEvents != null)
                    {
                        // Loop through each segment
                        foreach (var caseEvent in @case.CaseEvents)
                        {
                            // Determine if this segment has any policies applied to it
                            if (caseEvent.AllEventDates != null)
                            {
                                children.AddRange(caseEvent.AllEventDates.Select(ap => new Tuple<bool, CaseEvent>(true, new CaseEvent()
                                {
                                    Id = caseEvent.Id,
                                    EventDate = ap.Date,
                                    EventType = caseEvent.EventType
                                })));
                            }
                        }
                    }
                    // Return our populated or empty children collection of case events
                    return children;
                },
                // This is our mapper delegate method
                PopulateDimCaseEventFromCase,
                // We need to compare the new DimCasePolicy record with the old if an old one exists.
                //  This ensures we're not duplicating work or inserting more  history than we need to, especially
                //  if there were no changes to that record since last time the case was updated
                (dimEvent, ap) => dimEvent != null && dimEvent.RecordHash != Crypto.Sha256Hash(ap.ToJSON()),
                null,
                true,
                // We can pass as many "additional" dimensions that we need to here, which will get called in order passed
                //  once the original dimension record version has been saved/inserted into the database and we have a
                //  populated dimension id for that. The populated/saved dimension class, original parent and child get
                //  passed to these methods for additional work that needs to be done
                null
                );
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformChildOfDimensionalRetry<Case, bool, CaseEvent, DimCaseEventDate>(
                failure,
                (@case, dimPolicy) => dimPolicy.LoadHistory(@case.Id),
                @case =>
                {
                    // Get our initialized tuple list
                    List<Tuple<bool, CaseEvent>> children = new List<Tuple<bool, CaseEvent>>();
                    // Determine if we have any segments
                    if (@case.CaseEvents != null)
                    {
                        // Loop through each segment
                        foreach (var caseEvent in @case.CaseEvents)
                        {
                            // Determine if this segment has any policies applied to it
                            if (caseEvent.AllEventDates != null)
                            {
                                children.AddRange(caseEvent.AllEventDates.Select(ap => new Tuple<bool, CaseEvent>(true, new CaseEvent()
                                {
                                    Id = caseEvent.Id,
                                    EventDate = ap.Date,
                                    EventType = caseEvent.EventType
                                })));
                            }
                        }
                    }
                    // Return our populated or empty children collection of applied policy and segment tuples
                    return children;
                },
                PopulateDimCaseEventFromCase,
                (dimCaseEvent, ap) => dimCaseEvent != null && dimCaseEvent.RecordHash != Crypto.Sha256Hash(ap.ToJSON()),
               null
               );
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<Case>();
        }

        /// <summary>
        ///  Defines the predicate that overrides the condition required for unique comparision from its history
        /// </summary>
        /// <param name="dim"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        protected override bool UniqueHistoryPredicate(DimBase dim, BaseNonEntity entity)            
        {
            if (dim != null && entity != null)
            {
                var dimCaseEvent = (DimCaseEventDate)dim;
                var caseEvent = (CaseEvent)entity;

                return (dimCaseEvent.ExternalReferenceId == entity.Id.ToString()) &&
                        (dimCaseEvent.EventType == (int)caseEvent.EventType) &&
                        (dimCaseEvent.EventDate == caseEvent.EventDate);
            }
            else
            {
                return false;
            }
        }

      /// <summary>
      /// Returns DimCaseEventDate object populated from case and CaseEvent objects.
      /// </summary>
      /// <param name="parentCase"></param>
      /// <param name="intermediary"></param>
      /// <param name="caseEvent"></param>
      /// <param name="history"></param>
      /// <returns></returns>
        private DimCaseEventDate PopulateDimCaseEventFromCase(Case parentCase, bool intermediary, CaseEvent caseEvent, IEnumerable<DimCaseEventDate> history)
        {
            var lastHistory = history == null || !history.Any() ? null : history.OrderBy(h => h.DimVerId).LastOrDefault();
            var dimCE = new DimCaseEventDate(DataWarehouseConnectionString)
            {
                ExternalReferenceId = caseEvent.Id.ToString(),
                ExternalCreatedDate = parentCase.CreatedDate,
                ExternalModifiedDate = parentCase.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = parentCase.IsDeleted ? Const.VER_SEQ_DELETED_VALUE : Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = lastHistory == null ? 0 : lastHistory.VersionRootId,
                VersionPreviousId = lastHistory == null ? 0 : lastHistory.DimVerId,
                VersionHistoryCount = lastHistory == null ? Const.DEFAULT_HISTORY_COUNT : lastHistory.VersionHistoryCount + 1,

                //set the case parameters
                CreatedBy = string.IsNullOrWhiteSpace(parentCase.ModifiedById) ? parentCase.CreatedById : parentCase.ModifiedById,
                ModifiedBy = parentCase.ModifiedById,
                CaseId = parentCase.Id,               
                EventType = (int)caseEvent.EventType,
                EventDate= caseEvent.EventDate.GetValueOrNullIfMinValue(),
            };            
            return dimCE;
        }

        
    }
}
