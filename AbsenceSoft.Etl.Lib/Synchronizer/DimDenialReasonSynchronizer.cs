﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;

namespace AbsenceSoft.Etl.Lib.Synchronizer
{
    public class DimDenialReasonSynchronizer : BaseSynchronizer
    {
        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_DENIAL_REASON; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimDenialReasonSynchronizer"/> class.
        /// </summary>
        public DimDenialReasonSynchronizer() : base() { }

        /// <summary>
        /// Called when [syncronize].
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<DenialReason, DimDenialReason>(syncManager, PopulateDimDenialReason, null);
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<DenialReason>();
        }

        /// <summary>
        /// Populates the dim contact from contact.
        /// </summary>
        /// <param name="contact">The contact.</param>
        /// <returns></returns>
        private DimDenialReason PopulateDimDenialReason(DenialReason denialReason)
        {
            SuppressedEntity supressedEntity = null;
            if (denialReason.Employer != null)
            {
                supressedEntity = Employer.Repository.GetById(denialReason.EmployerId).SuppressedEntities.Find(e => e.EntityName == "DenialReason" && e.EntityCode == denialReason.Code);
            }
            else if(denialReason.Customer != null)
            {
                supressedEntity = Customer.Repository.GetById(denialReason.CustomerId).SuppressedEntities.Find(e => e.EntityName == "DenialReason" && e.EntityCode == denialReason.Code);
            }
            DimDenialReason dimDenialReason = new DimDenialReason(DataWarehouseConnectionString)
            {
                ExternalReferenceId = denialReason.Id,
                ExternalCreatedDate = denialReason.CreatedDate,
                ExternalModifiedDate = denialReason.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = 0,
                VersionPreviousId = 0,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                CustomerId = denialReason.CustomerId,
                CreatedBy = denialReason.CreatedById,
                ModifiedBy = denialReason.ModifiedById,
                IsDeleted = denialReason.IsDeleted,
                Description = denialReason.Description,
                Code = denialReason.Code,
                DisabledDate = supressedEntity != null ? supressedEntity.EntityModifiedDate : null
            };

            return dimDenialReason;
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<DenialReason, DimDenialReason>(failure, PopulateDimDenialReason);
        }
    }
}
