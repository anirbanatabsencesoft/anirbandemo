﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Common.Security;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using AbsenceSoft.Data.Customers;

namespace AbsenceSoft.Etl.Lib
{
    public sealed class DimAuthorizedSubmitterSynchronizer : BaseSynchronizer
    {
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_CASE_AUTHORIZED_SUBMITTER; } }

        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<Case, DimCaseAuthorizedSubmitter>(failure, PopulateDimCaseAuthorizedSubmitterFromCase);
        }

        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<Case, DimCaseAuthorizedSubmitter>(syncManager, PopulateDimCaseAuthorizedSubmitterFromCase, null, "CaseAuthorizedSubmitter");
        }

        private DimCaseAuthorizedSubmitter PopulateDimCaseAuthorizedSubmitterFromCase(Case oCase)
        {
            if (oCase.AuthorizedSubmitter != null)
            {
                DimCaseAuthorizedSubmitter dimCaseAS = new DimCaseAuthorizedSubmitter(DataWarehouseConnectionString);
                dimCaseAS.ExternalReferenceId = oCase.Id;
                dimCaseAS.ExternalCreatedDate = oCase.CreatedDate;
                dimCaseAS.ExternalModifiedDate = oCase.ModifiedDate;
                dimCaseAS.VersionIsCurrent = true;
                dimCaseAS.VersionSequence = Const.VER_SEQ_NEWEST_VALUE;
                dimCaseAS.VersionRootId = 0;
                dimCaseAS.VersionPreviousId = 0;
                dimCaseAS.VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT;
                dimCaseAS.EmployeeId = oCase.Employee.Id;
                dimCaseAS.CustomerId = oCase.Customer.Id;

                dimCaseAS.AuthorizeSubmitterId = oCase.AuthorizedSubmitter.Id;
                dimCaseAS.ContactTypeCode = oCase.AuthorizedSubmitter.ContactTypeCode;
                dimCaseAS.ContactTypeName = oCase.AuthorizedSubmitter.ContactTypeName;
                dimCaseAS.ContactFirstName = oCase.AuthorizedSubmitter.Contact.FirstName;
                dimCaseAS.ContactLastName = oCase.AuthorizedSubmitter.Contact.LastName;
                dimCaseAS.ContactEmail = oCase.AuthorizedSubmitter.Contact.Email;
                dimCaseAS.ContactAltEmail = oCase.AuthorizedSubmitter.Contact.AltEmail;
                dimCaseAS.ContactWorkPhone = oCase.AuthorizedSubmitter.Contact.WorkPhone;
                dimCaseAS.ContactHomePhone = oCase.AuthorizedSubmitter.Contact.HomePhone;
                dimCaseAS.ContactCellPhone = oCase.AuthorizedSubmitter.Contact.CellPhone;
                dimCaseAS.ContactFax = oCase.AuthorizedSubmitter.Contact.Fax;
                dimCaseAS.ContactStreet = oCase.AuthorizedSubmitter.Contact.Address.StreetAddress;
                dimCaseAS.ContactAddress1 = oCase.AuthorizedSubmitter.Contact.Address.Address1;
                dimCaseAS.ContactAddress2 = oCase.AuthorizedSubmitter.Contact.Address.Address2;
                dimCaseAS.ContactCity = oCase.AuthorizedSubmitter.Contact.Address.City;
                dimCaseAS.ContactState = oCase.AuthorizedSubmitter.Contact.Address.State;
                dimCaseAS.ContactPostalCode = oCase.AuthorizedSubmitter.Contact.Address.PostalCode;
                dimCaseAS.ContactCountry = oCase.AuthorizedSubmitter.Contact.Address.Country;
                dimCaseAS.IsDeleted = oCase.IsDeleted;
                dimCaseAS.CreatedBy = oCase.CreatedById;
                dimCaseAS.ModifiedBy = oCase.ModifiedById;

                return dimCaseAS;
            }
            else
                return null;
        }
    }
}
