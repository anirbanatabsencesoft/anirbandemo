﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;

namespace AbsenceSoft.Etl.Lib
{
    /// <summary>
    /// Dim Employee Necessity Synchronizer
    /// </summary>
    public class DimEmployeeNecessitySynchronizer : BaseSynchronizer
    {
        /// <summary>
        /// Dimension Id
        /// </summary>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_EMPLOYEE_NECESSITY; } }
        /// <summary>
        /// On Synchronize Event
        /// </summary>
        /// <param name="syncManager"></param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<EmployeeNecessity, DimEmployeeNecessity>(syncManager, PopulateDimEmployeeNecessityFromEmployeeNecessity, null);
        }
        /// <summary>
        /// Convert the EmployeeNecessity to DimEmployeeNecessity
        /// </summary>
        /// <param name="employeeNecessity"></param>
        /// <returns></returns>
        private DimEmployeeNecessity PopulateDimEmployeeNecessityFromEmployeeNecessity(EmployeeNecessity employeeNecessity)
        {
            DimEmployeeNecessity dimEmployeeNecessity = null;
            dimEmployeeNecessity = new DimEmployeeNecessity(DataWarehouseConnectionString)
            {
                ExternalReferenceId = employeeNecessity.Id,
                ExternalCreatedDate = employeeNecessity.CreatedDate,
                ExternalModifiedDate = employeeNecessity.ModifiedDate,
                CreatedBy = employeeNecessity.CreatedById,
                ModifiedBy = employeeNecessity.ModifiedById,
                EmployeeId = employeeNecessity.EmployeeId,
                EmployerId = employeeNecessity.EmployerId,
                CustomerId = employeeNecessity.CustomerId,
                CaseId = employeeNecessity.CaseId,
                CaseNumber = employeeNecessity.CaseNumber,
                StartDate = employeeNecessity.Dates?.StartDate,
                EndDate = employeeNecessity.Dates?.EndDate,
                Type = (int?)employeeNecessity.Type,
                Name = employeeNecessity.Name,
                Description = employeeNecessity.Description,
                VersionIsCurrent = true
            };

            return dimEmployeeNecessity;
        }
        /// <summary>
        /// On Retry Event
        /// </summary>
        /// <param name="failure"></param>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<EmployeeNecessity, DimEmployeeNecessity>(failure, PopulateDimEmployeeNecessityFromEmployeeNecessity);
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<EmployeeNecessity>();
        }
    }
}
