﻿using AbsenceSoft.Common.Security;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Etl.Lib
{
    /// <summary>
    /// This is a special case syncronizer in that it doesn't perform a single dimension sync, it
    /// is multi-faceted and multi-dimensional and there is no easily repeatable pattern to abstract
    /// in OO terms.
    /// </summary>
    public sealed class DimAccommodationSynchronizer : BaseSynchronizer
    {
        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_ACCOMMODATION; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimAccommodationSynchronizer"/> class.
        /// </summary>
        public DimAccommodationSynchronizer() : base() { }

        /// <summary>
        /// Does the synchronize work.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            // This base method mimics the "simple" perform sync method, however handles the
            //  scneario where we're syncing a CHILD class nested within an array, a BaseNonEntity,
            //  within our parent. The parent is the "controller" collection meaning it hosts the
            //  primary query collection, the modified date and entity id for sync coordination and is
            //  used by the sync coordinator.
            //
            // Secondly, this method takes some additional generic types to help it along the way
            //  where you're passinng in:
            //  - the PARENT entity type (TEntity)
            //  - the INTERMEDIARY type (this can be anything, if you don't need one, use bool OR byte for smallest memory footprint
            //      in our case we need the CaseSegment for the accommodation we're mapping, so we type that accordingly
            //  - the CHILD non-entity type we're actually going to map and save in the dimension table
            //  - the DIMENSION class type for that child's mapper to the dimension table itself
            PerformChildOfDimensionalSync<Case, AccommodationRequest, Accommodation, DimAccommodation>(
                // Pass in our sync manager
                syncManager,
                // This lambda passes you the parent entity and then allows the dimension mapper to load history for it
                //  This is necessary because the history is rectified against the new child collection for the parent
                //  entity and any not present any longer are marked as deleted in the data warehouse. This is because
                //  our MongoDB entities in arrays don't have "soft" delete enabled, so we have to detect it and take
                //  action on it by loading the dimensional table history here.
                (@case, dimAccom) => dimAccom.LoadHistory(@case.Id),
                // Here we're given the parent entity (case in this instance) and the goal is to return a collection of
                //  Tuples containing the intermediary type AND our child object. Intermediary can be null, or any
                //  type remember, as long as it's not used downstream you can make it whatever.
                // We're giong to use the Accommodation Request as the intermediary.
                GetChildren,
                // This is our mapper delegate method
                PopulateDimAccommodationFromAccommodation,
                // We need to compare the new DimAccommodation record with the old if an old one exists.
                //  This ensures we're not duplicating work or inserting more  history than we need to, especially
                //  if there were no changes to that record since last time the case was updated
                (dimAccom, accom) => dimAccom.RecordHash != Crypto.Sha256Hash(accom.ToJSON()),
                null,
                true,
                // We can pass as many "additional" dimensions that we need to here, which will get called in order passed
                //  once the original dimension record version has been saved/inserted into the database and we have a
                //  populated dimension id for that. The populated/saved dimension class, original parent and child get
                //  passed to these methods for additional work that needs to be done
                (dimAccom, @case, accom, conn) =>
                {
                    // Check for accommodation usage and first delete if exists then save
                    if (accom.Usage != null && accom.Usage.Any())
                    {
                        var dimUsage = new DimAccommodation(DataWarehouseConnectionString) { DimAccommodationId = dimAccom.DimAccommodationId };
                        dimUsage.AccommodationUsage.DimAccommodationId = dimAccom.DimAccommodationId;
                        dimUsage.AccommodationUsage.Delete(conn);
                        foreach (AccommodationUsage usage in accom.Usage)
                        {
                            PopulateDimAccommodationUsageFromAccommodationUsage(usage, dimAccom.DimAccommodationId).AccommodationUsage.Save(conn);
                        }
                    }
                });
        }

        /// <summary>
        /// Returns the child objects
        /// </summary>
        /// <param name="theCase"></param>
        /// <returns></returns>
        public List<Tuple<AccommodationRequest, Accommodation>> GetChildren(Case theCase)
        {
            if (theCase != null && theCase.AccommodationRequest != null && theCase.AccommodationRequest.Accommodations != null)
            {
                return theCase.AccommodationRequest.Accommodations.Select(accom => new Tuple<AccommodationRequest, Accommodation>(theCase.AccommodationRequest, accom)).ToList();
            }

            return null;
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformChildOfDimensionalRetry<Case, AccommodationRequest, Accommodation, DimAccommodation>(
                failure,
                (@case, dimAccom) => dimAccom.LoadHistory(@case.Id),
                @case => @case.AccommodationRequest == null || @case.AccommodationRequest.Accommodations == null ? null :
                    @case.AccommodationRequest.Accommodations.Select(accom => new Tuple<AccommodationRequest, Accommodation>(@case.AccommodationRequest, accom)),
                PopulateDimAccommodationFromAccommodation,
                (dimAccom, accom) => dimAccom.RecordHash != Crypto.Sha256Hash(accom.ToJSON()),
                (dimAccom, @case, accom) =>
                {
                    // Check for accommodation usage and first delete if exists then save
                    if (accom.Usage != null && accom.Usage.Any())
                    {
                        var dimUsage = new DimAccommodation(DataWarehouseConnectionString) { DimAccommodationId = dimAccom.DimAccommodationId };
                        dimUsage.AccommodationUsage.DimAccommodationId = dimAccom.DimAccommodationId;
                        dimUsage.AccommodationUsage.Delete();
                        foreach (AccommodationUsage usage in accom.Usage)
                            PopulateDimAccommodationUsageFromAccommodationUsage(usage, dimAccom.DimAccommodationId).AccommodationUsage.Save();
                    }
                });
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<Case>();
        }

        /// <summary>
        /// Populates the dim accommodation from accommodation.
        /// </summary>
        /// <param name="parentCase">The parent case.</param>
        /// <param name="request">The request.</param>
        /// <param name="accom">The accom.</param>
        /// <param name="accomHistory">The accom history.</param>
        /// <returns></returns>
        private DimAccommodation PopulateDimAccommodationFromAccommodation(Case parentCase, AccommodationRequest request, Accommodation accom, IEnumerable<DimAccommodation> accomHistory)
        {
            string denialReasonCode = string.Empty;
            var accomodationUsage = accom.Usage.FirstOrDefault(a => a.DenialReasonCode != null);
            if (accomodationUsage != null)
            {
                denialReasonCode = accomodationUsage.DenialReasonCode;
            }
            var lastHistory = accomHistory == null ? null : accomHistory.OrderBy(r => r.DimVerId).LastOrDefault();
            var dimAccommodation = new DimAccommodation(DataWarehouseConnectionString)
            {
                ExternalReferenceId = accom.Id.ToString(),
                ExternalCreatedDate = accom.CreatedDate,
                ExternalModifiedDate = parentCase.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = parentCase.IsDeleted ? Const.VER_SEQ_DELETED_VALUE : Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = lastHistory == null ? 0 : lastHistory.VersionRootId,
                VersionPreviousId = lastHistory == null ? 0 : lastHistory.DimVerId,
                VersionHistoryCount = lastHistory == null ? Const.DEFAULT_HISTORY_COUNT : lastHistory.VersionHistoryCount + 1,
                EmployeeId = parentCase.Employee.Id,
                EmployerId = parentCase.EmployerId,
                CustomerId = parentCase.CustomerId,
                CaseId = parentCase.Id,
                AccommodationType = accom.Type.Name,
                AccommodationTypeCode = accom.Type.Code,
                Duration = (int)accom.Duration,
                Resolution = accom.Resolution,
                Resolved = accom.Resolved,
                ResolvedDate = accom.ResolvedDate,
                Granted = accom.Granted,
                GrantedDate = accom.GrantedDate,
                Cost = accom.Cost,
                AccomStartDate = accom.StartDate,
                AccomEndDate = accom.EndDate,
                Determination = (int)accom.Determination,
                Status = (int)accom.Status,
                CancelReasonOtherDesc = accom.OtherReasonDesc,
                CancelReason = accom.CancelReason.HasValue ? (int)accom.CancelReason.Value : (int?)null,
                DenialReasonCode = denialReasonCode,
                IsWorkRelated = accom.IsWorkRelated,
                IsDeleted = parentCase.IsDeleted,
                MinApprovedDate = accom.MinApprovedFromDate,
                MaxApprovedDate = accom.MaxApprovedThruDate,
                RecordHash = CryptoString.GetHash(accom.ToJSON()).Hashed
            };

            //set audit data
            if(lastHistory != null)
                dimAccommodation.CreatedBy = lastHistory.CreatedBy;
            else
                dimAccommodation.CreatedBy = parentCase.ModifiedById;
            dimAccommodation.ModifiedBy = parentCase.ModifiedById;

            return dimAccommodation;
        }

        /// <summary>
        /// Populates the dim accommodation usage from accommodation usage.
        /// </summary>
        /// <param name="usage">The usage.</param>
        /// <param name="dimAccommodationId">The dim accommodation identifier.</param>
        /// <returns></returns>
        private DimAccommodation PopulateDimAccommodationUsageFromAccommodationUsage(AccommodationUsage usage, long dimAccommodationId = 0)
        {
            DimAccommodation dimAccommodation = new DimAccommodation(DataWarehouseConnectionString);
            dimAccommodation.AccommodationUsage.StartDate = usage.StartDate;
            dimAccommodation.AccommodationUsage.EndDate = usage.EndDate;
            dimAccommodation.AccommodationUsage.ExternalReferenceId = usage.Id.ToString();
            dimAccommodation.AccommodationUsage.Determination = (int)usage.Determination;
            dimAccommodation.AccommodationUsage.DimAccommodationId = dimAccommodationId;
            return dimAccommodation;
        }
    }
}