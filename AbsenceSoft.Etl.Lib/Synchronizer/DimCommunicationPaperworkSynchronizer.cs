﻿using AbsenceSoft.Common.Security;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Pay;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Etl.Lib
{
    public class DimCommunicationPaperworkSynchronizer : BaseSynchronizer
    {

        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_COMMUNICATION_PAPERWORK; } }

        /// <summary>
        /// Initializes a new instance of the DimCommunicationPaperworkSynchronizer class.
        /// </summary>
        public DimCommunicationPaperworkSynchronizer() : base() { }


        /// <summary>
        /// Does the synchronize work.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            // This base method mimics the "simple" perform sync method, however handles the
            //  scneario where we're syncing a CHILD class nested within an array, a BaseNonEntity,
            //  within our parent. The parent is the "controller" collection meaning it hosts the
            //  primary query collection, the modified date and entity id for sync coordination and is
            //  used by the sync coordinator.
            //
            // Secondly, this method takes some additional generic types to help it along the way
            //  where you're passinng in:
            //  - the PARENT entity type (TEntity)
            //  - the INTERMEDIARY type (this can be anything, if you don't need one, use bool OR byte for smallest memory footprint
            //      in our case we need the CaseSegment for the accommodation we're mapping, so we type that accordingly
            //  - the CHILD non-entity type we're actually going to map and save in the dimension table
            //  - the DIMENSION class type for that child's mapper to the dimension table itself
            PerformChildOfDimensionalSync<Communication, bool, CommunicationPaperwork, DimCommunicationPaperwork>(
                // Pass in our sync manager
                syncManager,
                // This lambda passes you the parent entity and then allows the dimension mapper to load history for it
                //  This is necessary because the history is rectified against the new child collection for the parent
                //  entity and any not present any longer are marked as deleted in the data warehouse. This is because
                //  our MongoDB entities in arrays don't have "soft" delete enabled, so we have to detect it and take
                //  action on it by loading the dimensional table history here.
                (@case, dimComm) => dimComm.LoadHistory(@case.Id),
                // Here we're given the parent entity (case in this instance) and the goal is to return a collection of
                //  Tuples containing the intermediary type AND our child object. Intermediary can be null, or any
                //  type remember, as long as it's not used downstream you can make it whatever.
                // We're giong to use the Accommodation Request as the intermediary.
                (@Comm) =>
                {
                    if (@Comm.Paperwork == null)
                    {
                        return null;
                    }
                    else
                    {
                        return @Comm.Paperwork.Select(paperwork => new Tuple<bool, CommunicationPaperwork>(true, paperwork));
                    }                   
                }
               ,
                // This is our mapper delegate method
                PopulateDimPaperworkFromCommunication,
                // We need to compare the new DimAccommodation record with the old if an old one exists.
                //  This ensures we're not duplicating work or inserting more  history than we need to, especially
                //  if there were no changes to that record since last time the case was updated
                null,
                null,
                true,
                // We can pass as many "additional" dimensions that we need to here, which will get called in order passed
                //  once the original dimension record version has been saved/inserted into the database and we have a
                //  populated dimension id for that. The populated/saved dimension class, original parent and child get
                //  passed to these methods for additional work that needs to be done
                (dimCommPaperwork, @comm, Paperwork, conn) =>
                {
                    // Check for pay period details and first delete if exists then save
                    if (Paperwork.Fields != null && Paperwork.Fields.Any())
                    {
                        var dimPaperworkField = new DimCommunicationPaperworkField(DataWarehouseConnectionString)
                        {
                            DimCommunicationPaperworkId = dimCommPaperwork.DimCommunicationPaperworkId
                        };
                        dimPaperworkField.Delete(conn);
                        foreach (PaperworkField field in Paperwork.Fields)
                        {
                            var dimCommPaperworkField = PopulateDimPaperworkFieldFromPaperwork(field, dimCommPaperwork.DimCommunicationPaperworkId);
                            dimCommPaperworkField.Save(conn);
                        }
                    }
                });
        }

        private DimCommunicationPaperworkField PopulateDimPaperworkFieldFromPaperwork(PaperworkField field, long dimCommunicationPaperworkId)
        {
            DimCommunicationPaperworkField dimPaperworkField = new DimCommunicationPaperworkField(DataWarehouseConnectionString)
            {
                ExternalReferenceId = field.Id.ToString(),
                DimCommunicationPaperworkId = dimCommunicationPaperworkId,
                Name = field.Name,
                Status = (int)field.Status,
                Type = (int)field.Type,
                Required = field.Required,
                FieldOrder = field.Order,
                Value = field.Value
            };
            return dimPaperworkField;
        }

        private DimCommunicationPaperwork PopulateDimPaperworkFromCommunication(Communication communication,bool intermediary, CommunicationPaperwork communicationPaperwork, IEnumerable<DimCommunicationPaperwork> communicationPaperworkHistory)
        {
            var lastHistory = communicationPaperworkHistory == null ? null : communicationPaperworkHistory.OrderBy(r => r.DimVerId).LastOrDefault();
            var dimCommunicationPaperwork = new DimCommunicationPaperwork(DataWarehouseConnectionString)
            {
                ExternalReferenceId = communicationPaperwork.Id.ToString(),
                ExternalCreatedDate = communication.CreatedDate,
                ExternalModifiedDate = communication.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = communication.IsDeleted ? Const.VER_SEQ_DELETED_VALUE : Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = lastHistory == null ? 0 : lastHistory.VersionRootId,
                VersionPreviousId = lastHistory == null ? 0 : lastHistory.DimVerId,
                VersionHistoryCount = lastHistory == null ? Const.DEFAULT_HISTORY_COUNT : lastHistory.VersionHistoryCount + 1,
                CommunicationId = communication.Id,
                EmployeeId = communication.EmployeeId,
                EmployerId = communication.EmployerId,
                CustomerId = communication.CustomerId,
                AttachmentId = communicationPaperwork.AttachmentId,                
                DueDate = communicationPaperwork.DueDate.GetValueOrNullIfMinValue(),
                ReturnAttachmentId = communicationPaperwork.ReturnAttachmentId,
                Status = (int)communicationPaperwork.Status,
                StatusDate = communicationPaperwork.StatusDate.GetValueOrNullIfMinValue(),
                PaperworkId = communicationPaperwork.Paperwork.Id,
                PaperworkCreatedBy = communicationPaperwork.Paperwork.CreatedById,
                PaperworkCreatedDate = communicationPaperwork.Paperwork.CreatedDate.GetValueOrNullIfMinValue(),
                PaperworkCode = communicationPaperwork.Paperwork.Code,
                PaperworkCustomerId = communicationPaperwork.Paperwork.CustomerId,
                PaperworkDescription = communicationPaperwork.Paperwork.Description,
                PaperworkDocType = (int)communicationPaperwork.Paperwork.DocType,
                PaperworkEmployerId = communicationPaperwork.Paperwork.EmployerId,
                PaperworkEnclosure = communicationPaperwork.Paperwork.EnclosureText,
                PaperworkFileId = communicationPaperwork.Paperwork.FileId,
                PaperworkFileName = communicationPaperwork.Paperwork.FileName,
                PaperworkIsDeleted = communicationPaperwork.Paperwork.IsDeleted,
                PaperworkModifiedBy = communicationPaperwork.Paperwork.ModifiedById,
                PaperworkModifiedDate = communicationPaperwork.Paperwork.ModifiedDate.GetValueOrNullIfMinValue(),
                PaperworkName = communicationPaperwork.Paperwork.Name,
                PaperworkRequiresReview = communicationPaperwork.Paperwork.RequiresReview,
                PaperworkReturnDateAdjustment = communicationPaperwork.Paperwork.ReturnDateAdjustment,
                PaperworkReturnDateAdjustmentDays = communicationPaperwork.Paperwork.ReturnDateAdjustmentDays,
                IsDeleted= communication.IsDeleted          
            };

            //set audit data
            if (lastHistory != null)
            {
                dimCommunicationPaperwork.CreatedBy = lastHistory.CreatedBy;
            }
            else
            {
                dimCommunicationPaperwork.CreatedBy = communication.ModifiedById;
            }
            dimCommunicationPaperwork.ModifiedBy = communication.ModifiedById;

            return dimCommunicationPaperwork;
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformChildOfDimensionalRetry<Communication, bool, CommunicationPaperwork, DimCommunicationPaperwork>(
                failure,
                (Comm, dimCommPaperwork) => dimCommPaperwork.LoadHistory(Comm.Id),
                @Comm =>
                {
                    if (@Comm.Paperwork == null)
                    {
                        return null;
                    }
                    else
                    {
                        return @Comm.Paperwork.Select(paperwork => new Tuple<bool, CommunicationPaperwork>(true, paperwork));
                    }
                },
                PopulateDimPaperworkFromCommunication,
                (dimCommPaperwork, accom) => dimCommPaperwork.RecordHash != Crypto.Sha256Hash(accom.ToJSON()),
                 (dimCommPaperwork, @comm, Paperwork) =>
                 {
                     // Check for pay period details and first delete if exists then save
                     if (Paperwork.Fields != null && Paperwork.Fields.Any())
                     {
                         var dimPaperworkField = new DimCommunicationPaperworkField(DataWarehouseConnectionString)
                         {
                             DimCommunicationPaperworkId = dimCommPaperwork.DimCommunicationPaperworkId
                         };
                         dimPaperworkField.Delete();
                         foreach (PaperworkField field in Paperwork.Fields)
                         {
                             var dimCommPaperworkField = PopulateDimPaperworkFieldFromPaperwork(field, dimCommPaperwork.DimCommunicationPaperworkId);
                             dimCommPaperworkField.Save();
                         }
                     }
                 });
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<Case>();
        }
        
      
    }
}
