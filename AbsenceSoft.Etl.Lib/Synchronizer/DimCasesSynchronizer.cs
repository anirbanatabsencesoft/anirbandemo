﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using AbsenceSoft.Logic.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using AbsenceSoft.Common;

namespace AbsenceSoft.Etl.Lib
{
    public sealed class DimCasesSynchronizer : BaseSynchronizer
    {
        // Cache properties.
        private Dictionary<string, User> _users = null;

        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_CASES; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimCasesSynchronizer"/> class.
        /// </summary>
        public DimCasesSynchronizer() : base() { }

        /// <summary>
        /// Called when [syncronize].
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            _users = new Dictionary<string, User>();
            BatchReadCompleted += (results) => _users = User.Repository.Collection.Find(User.Query.In(c => c.Id, _users.Keys)).ToDictionary(r => r.Id);
            PerformDimensionalSync<Case, DimCases>(syncManager, PopulateDimCaseFromCase, @case =>
            {
                _users[@case.CreatedById] = null;
            });
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<Case>();
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<Case, DimCases>(failure, PopulateDimCaseFromCase);
        }

        /// <summary>
        /// Populates the dim case from case.
        /// </summary>
        /// <param name="oCase">The o case.</param>
        /// <returns></returns>
        private DimCases PopulateDimCaseFromCase(Case oCase)
        {
            var dimCase = new DimCases(DataWarehouseConnectionString);
            dimCase.ExternalReferenceId = oCase.Id;
            dimCase.ExternalCreatedDate = oCase.CreatedDate;
            dimCase.ExternalModifiedDate = oCase.ModifiedDate;
            dimCase.VersionIsCurrent = true;
            dimCase.VersionSequence = Const.VER_SEQ_NEWEST_VALUE;
            dimCase.VersionRootId = 0;
            dimCase.VersionPreviousId = 0;
            dimCase.VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT;
            dimCase.EmployeeId = oCase.Employee.Id;
            dimCase.EmployerId = oCase.EmployerId;
            var reporter = oCase.CaseReporter;
            if (reporter == null || string.IsNullOrWhiteSpace(reporter.Id))
                dimCase.CaseReporterId = null;
            else
            {
                dimCase.CaseReporterId = reporter.Id;
                dimCase.CaseReporterTypeCode = reporter.ContactTypeCode;
                dimCase.CaseReporterName = reporter.ContactTypeName;
                if (reporter.Contact != null)
                {
                    dimCase.CaseReporterFirstName = reporter.Contact.FirstName ?? string.Empty;
                    dimCase.CaseReporterLastName = reporter.Contact.LastName ?? string.Empty;
                }
            }
            dimCase.CaseNumber = oCase.CaseNumber;
            dimCase.CustomerId = oCase.CustomerId;
            dimCase.StartDate = oCase.StartDate;
            dimCase.EndDate = oCase.EndDate;
            dimCase.Status = (int)oCase.Status;
            dimCase.CreatedDate = oCase.CreatedDate;
            dimCase.SpouseCase = oCase.SpouseCaseId;
            dimCase.Type = oCase.Segments == null || (!oCase.Segments.Any()) ? 0 : (int)oCase.Segments[0].Type;
            dimCase.CaseReason = (oCase.Reason == null) ? null : oCase.Reason.Name;
            dimCase.CancelReason = (int?)oCase.CancelReason;
            dimCase.AssignedToName = oCase.AssignedToName;
            dimCase.AssignedToId = oCase.AssignedToId;
            dimCase.CaseClosureReason = (int?)oCase.ClosureReason;
            dimCase.CaseClosureReasonOther = oCase.ClosureReasonDetails;
            dimCase.CaseClosureOtherReasonDetails = oCase.ClosureOtherReasonDetails;
            dimCase.RelatedCaseNumber = oCase.RelatedCaseNumber;
            dimCase.RelatedCaseType = (int?)oCase.RelatedCaseType;
            var curJob = oCase.Employee.GetCurrentJob();
            var offLoc = curJob == null ? null : curJob.OfficeLocation;
            dimCase.JobOfficeLocationCode = offLoc == null ? null : offLoc.Code;
            dimCase.JobOfficeLocationName = offLoc == null ? null : offLoc.Name;

            var cert = oCase.Certifications == null ? null : oCase.Certifications.LastOrDefault();
            if (cert != null)
            {
                dimCase.Occurances = cert.Occurances;
                dimCase.Frequency = cert.Frequency;
                dimCase.FrequencyType = (int)cert.FrequencyType;
                dimCase.FrequencyUnitType = (int)cert.FrequencyUnitType;
                dimCase.Duration = cert.Duration;
                dimCase.DurationType = (int)cert.DurationType;
                dimCase.CaseCertStartDate = cert.StartDate;
                dimCase.CaseCertEndDate = cert.EndDate;
                dimCase.IntermittentType = (int?)cert.IntermittentType;
            }

            dimCase.IsAccommodation = oCase.IsAccommodation;
            dimCase.IsWorkRelated = oCase.Metadata.GetRawValue<bool?>("IsWorkRelated");

            var determination = oCase.GetSummaryDetermination();
            dimCase.CaseIsDenied = determination == AdjudicationSummaryStatus.Denied;
            dimCase.CaseIsApproved = determination == AdjudicationSummaryStatus.Approved || determination == AdjudicationSummaryStatus.Allowed;
            dimCase.CaseDetermination = (int)determination;
            var summary = oCase.Summary;
            if (summary == null)
                summary = new CaseService().Using(s => s.CalcCaseSummary(oCase)).Summary;
            if (summary != null)
            {
                dimCase.CaseIsEligible = (int)oCase.Summary.Eligibility;
                dimCase.CaseFmlaIsApproved = oCase.Summary.FmlaApproved;
                dimCase.CaseFmlaIsDenied = oCase.Summary.FmlaDenied;
                dimCase.CaseFmlaProjectedUsage = oCase.Summary.FmlaProjectedUsageText;
                dimCase.CaseFmlaExhaustDate = oCase.Summary.FmlaExhaustDate;
                dimCase.CaseMaxApprovedThruDate = oCase.Summary.MaxApprovedThruDate;
                dimCase.CaseMinApprovedThruDate = oCase.Summary.MinApprovedThruDate;
            }
            if (oCase.AccommodationRequest != null)
            {
                dimCase.CaseAccomStatus = (int)oCase.AccommodationRequest.Status;
                dimCase.CaseAccomStartDate = oCase.AccommodationRequest.StartDate;
                dimCase.CaseAccomEndDate = oCase.AccommodationRequest.EndDate;
                dimCase.CaseAccomMinApprovedDate = oCase.AccommodationRequest.MinApprovedDate;
                dimCase.CaseAccomMaxApprovedDate = oCase.AccommodationRequest.MaxApprovedDate;
                dimCase.CaseAccomDetermination = (int)oCase.AccommodationRequest.Determination;
                dimCase.CaseAccomCancelReason = (int?)oCase.AccommodationRequest.CancelReason;
                dimCase.CaseAccomCancelReasonOther = oCase.AccommodationRequest.OtherReasonDesc;
                dimCase.CaseAccomDuration = oCase.AccommodationRequest.SummaryDuration;
                dimCase.CaseAccomDurationType = oCase.AccommodationRequest.SummaryDurationType;
            }

            if (oCase.Disability != null)
            {
                dimCase.PrimaryPathText = oCase.Disability.PrimaryPathText;
                dimCase.PrimaryPathMinDays = oCase.Disability.PrimaryPathMinDays;
                dimCase.PrimaryPathMaxDays = oCase.Disability.PrimaryPathMaxDays;
                dimCase.Hospitalization = oCase.Disability.Hospitalization;
                dimCase.MedicalComplications = oCase.Disability.MedicalComplications;
                dimCase.GeneralHealthCondition = oCase.Disability.GeneralHealthCondition;
                dimCase.ConditionStartDate = oCase.Disability.ConditionStartDate;
                if (oCase.Disability.PrimaryDiagnosis != null)
                {
                    dimCase.CasePrimaryDiagnosisCode = oCase.Disability.PrimaryDiagnosis.Code;
                    dimCase.CasePrimaryDiagnosisName = oCase.Disability.PrimaryDiagnosis.Name;
                }
                if (oCase.Disability.SecondaryDiagnosis != null && oCase.Disability.SecondaryDiagnosis.Any())
                {
                    var diag = oCase.Disability.SecondaryDiagnosis.First();
                    dimCase.CaseSecondaryDiagnosisCode = diag.Code;
                    dimCase.CaseSecondaryDiagnosisName = diag.Name;
                }
            }

            try
            {
                User user = null;
                if (_users == null || !_users.TryGetValue(oCase.CreatedById, out user))
                    user = oCase.CreatedBy;
                if (user != null)
                {
                    dimCase.CreatedBy = user.Id;
                    dimCase.CaseCreatedByEmail = user.Email;
                    dimCase.CaseCreatedByFirstName = user.FirstName;
                    dimCase.CaseCreatedByLastName = user.LastName;
                }
            }
            catch (Exception ex)
            {
                LogError("Error setting created by details", ex.ToString(), oCase.Id);
            }

            //set modified by id value
            dimCase.ModifiedBy = oCase.ModifiedById;

            if (oCase.CustomFields != null && oCase.CustomFields.Count > 0)
                dimCase.CustomFields = oCase.CustomFields.ToDictionary(
                    f => f.Code.ToUpperInvariant(),
                    f => f.ValueType == CustomFieldValueType.SelectList ? f.SelectedValueText : f.SelectedValue);

            dimCase.EmployerCaseNumber = oCase.EmployerCaseNumber;

            dimCase.IsIntermittent = oCase.IsIntermittent;
            if (oCase.Contact != null)
            {
                dimCase.RelationshipTypeCode = oCase.Contact.ContactTypeCode;
                dimCase.RelationshipTypeName = oCase.Contact.ContactTypeName;
            }

            // case event dates
            dimCase.ReturnToWork = oCase.GetCaseEventDate(CaseEventType.ReturnToWork);
            dimCase.DeliveryDate = oCase.GetCaseEventDate(CaseEventType.DeliveryDate);
            dimCase.BondingStartDate = oCase.GetCaseEventDate(CaseEventType.BondingStartDate);
            dimCase.BondingEndDate = oCase.GetCaseEventDate(CaseEventType.BondingEndDate);
            dimCase.IllnessOrInjuryDate = oCase.GetCaseEventDate(CaseEventType.IllnessOrInjuryDate);
            dimCase.HospitalAdmissionDate = oCase.GetCaseEventDate(CaseEventType.HospitalAdmissionDate);
            dimCase.HospitalReleaseDate = oCase.GetCaseEventDate(CaseEventType.HospitalReleaseDate);
            dimCase.ReleaseReceived = oCase.GetCaseEventDate(CaseEventType.ReleaseReceived);
            dimCase.PaperworkReceived = oCase.GetCaseEventDate(CaseEventType.PaperworkReceived);
            dimCase.AccommodationAdded = oCase.GetCaseEventDate(CaseEventType.AccommodationAdded);
            dimCase.CaseClosed = oCase.GetCaseEventDate(CaseEventType.CaseClosed);
            dimCase.CaseCancelled = oCase.GetCaseEventDate(CaseEventType.CaseCancelled);
            dimCase.AdoptionDate = oCase.GetCaseEventDate(CaseEventType.AdoptionDate);
            dimCase.EstimatedReturnToWork = oCase.GetCaseEventDate(CaseEventType.EstimatedReturnToWork);
            dimCase.EmployerHolidayScheduleChanged = oCase.GetCaseEventDate(CaseEventType.EmployerHolidayScheduleChanged);

            dimCase.IsDeleted = oCase.IsDeleted;
            if (oCase.IsDeleted)
                dimCase.VersionSequence = Const.VER_SEQ_DELETED_VALUE;


            #region Alternate Contact Info 

            dimCase.AltPhoneNumber = oCase.Employee.Info.AltPhone;
            dimCase.AltEmail = oCase.Employee.Info.AltEmail;
            if (oCase.Employee.Info.AltAddress != null)
            {
                dimCase.AltAddress1 = oCase.Employee.Info.AltAddress.Address1;
                dimCase.AltAddress2 = oCase.Employee.Info.AltAddress.Address2;
                dimCase.AltCity = oCase.Employee.Info.AltAddress.City;
                dimCase.AltState = oCase.Employee.Info.AltAddress.State;
                dimCase.AltPostalCode = oCase.Employee.Info.AltAddress.PostalCode;
                dimCase.AltCountry = oCase.Employee.Info.AltAddress.Country;
            }
            else
            {
                dimCase.AltAddress1 = null;
                dimCase.AltAddress2 = null;
                dimCase.AltCity = null;
                dimCase.AltState = null;
                dimCase.AltPostalCode = null;
                dimCase.AltCountry = null;
            }


            #endregion

            #region Work Related
            //get the work related info from a case
            var wr = oCase.WorkRelated;
            //fill the properties if not null
            if (wr != null)
            {
                dimCase.WorkRelatedDateOfInjury = wr.IllnessOrInjuryDate;
                dimCase.WorkRelatedIsHealthCarePrivate = wr.HealthCarePrivate;
                dimCase.WorkRelatedIsReportable = wr.Reportable;
                dimCase.WorkRelatedPlaceOfOccurance = wr.WhereOccurred;
                dimCase.WorkRelatedWorkClassification = wr.Classification != null ? (int)wr.Classification : (int?)null;
                dimCase.WorkRelatedTypeOfInjury = wr.TypeOfInjury != null ? (int)wr.TypeOfInjury : (int?)null;
                dimCase.WorkRelatedDaysAwayFromWork = wr.DaysAwayFromWork;
                dimCase.WorkRelatedRestrictedDays = wr.DaysOnJobTransferOrRestriction;

                //provider
                var provider = wr.Provider;
                if (provider != null && provider.Contact != null)
                {
                    var contact = provider.Contact;
                    dimCase.WorkRelatedProviderFirstName = contact.FirstName;
                    dimCase.WorkRelatedProviderLastName = contact.LastName;
                    dimCase.WorkRelatedProviderCompanyName = contact.CompanyName;
                    if (contact.Address != null)
                    {
                        dimCase.WorkRelatedProviderAddress1 = contact.Address.Address1;
                        dimCase.WorkRelatedProviderAddress2 = contact.Address.Address2;
                        dimCase.WorkRelatedProviderState = contact.Address.State;
                        dimCase.WorkRelatedProviderCity = contact.Address.City;
                        dimCase.WorkRelatedProviderCountry = contact.Address.Country;
                        dimCase.WorkRelatedProviderPostalCode = contact.Address.PostalCode;
                    }
                }

                dimCase.WorkRelatedIsTreatedInEmergencyRoom = wr.EmergencyRoom;
                dimCase.WorkRelatedIsHospitalized = wr.HospitalizedOvernight;
                dimCase.WorkRelatedEmployeeStartedWorking = wr.TimeEmployeeBeganWork != null ? wr.TimeEmployeeBeganWork.ToString() : null;
                dimCase.WorkRelatedTimeOfInjury = wr.TimeOfEvent != null ? wr.TimeOfEvent.ToString() : null;
                dimCase.WorkRelatedActivityBeforeInjury = wr.ActivityBeforeIncident;
                dimCase.WorkRelatedWhatHappened = wr.WhatHappened;
                dimCase.WorkRelatedInjuryDetails = wr.InjuryOrIllness;
                dimCase.WorkRelatedWhatHarmedEmployee = wr.WhatHarmedTheEmployee;
                dimCase.WorkRelatedDateOfDeath = wr.DateOfDeath;

                //sharps
                dimCase.WorkRelatedIsSharp = wr.Sharps;
                if (wr.SharpsInfo != null)
                {
                    var sharp = wr.SharpsInfo;
                    if (sharp.InjuryLocation.Count > 0)
                        dimCase.WorkRelatedSharpBodyPart = string.Join(",", sharp.InjuryLocation);

                    dimCase.WorkRelatedTypeOfSharp = sharp.TypeOfSharp;
                    dimCase.WorkRelatedBrandOfSharp = sharp.BrandOfSharp;
                    dimCase.WorkRelatedModelOfSharp = sharp.ModelOfSharp;
                    dimCase.WorkRelatedBodyFluid = sharp.BodyFluidInvolved;
                    dimCase.WorkRelatedIsEngineeredInjuryProtection = sharp.HaveEngineeredInjuryProtection;
                    dimCase.WorkRelatedIsProtectiveMechanismActivated = sharp.WasProtectiveMechanismActivated;
                    dimCase.WorkRelatedWhenSharpIncidentHappened = sharp.WhenDidTheInjuryOccur != null ? (int)sharp.WhenDidTheInjuryOccur : (int?)null;
                    dimCase.WorkRelatedSharpJobClassification = sharp.JobClassification != null ? (int)sharp.JobClassification : (int?)null;
                    dimCase.WorkRelatedSharpJobClassificationOther = sharp.JobClassificationOther;
                    dimCase.WorkRelatedSharpLocationOrDepartment = sharp.LocationAndDepartment != null ? (int)sharp.LocationAndDepartment : (int?)null;
                    dimCase.WorkRelatedSharpLocationOrDepartmentOther = sharp.LocationAndDepartmentOther;
                    dimCase.WorkRelatedSharpProcedure = sharp.Procedure != null ? (int)sharp.Procedure : (int?)null;
                    dimCase.WorkRelatedSharpProcedureOther = sharp.ProcedureOther;
                    dimCase.WorkRelatedSharpExposureDetail = sharp.ExposureDetail;
                }
            }
            #endregion

            #region Comorbidity Guidelines data
            if (oCase.Disability != null && oCase.Disability.CoMorbidityGuidelineDetail != null)
            {
                var cg = oCase.Disability.CoMorbidityGuidelineDetail;
                dimCase.CGRiskAssessmentScore = cg.RiskAssessmentScore;
                dimCase.CGRiskAssessmentStatus = cg.RiskAssessmentStatus;
                dimCase.CGActualDaysLost = cg.ActualDaysLost;
                dimCase.CGBestPracticeDays = cg.BestPracticesDays;
                dimCase.CGMidRangeAllAbsence = cg.MidrangeAllAbsence;
                dimCase.CGAtRiskAllAbsence = cg.AtRiskAllAbsence;
            }

            #endregion


            #region Missing Attributes
            dimCase.ClosureReason = oCase.ClosureReason != null ? (int)oCase.ClosureReason : (int?)null;
            dimCase.ClosureReasonDetails = oCase.ClosureReasonDetails;
            dimCase.Description = oCase.Description;
            dimCase.Narrative = oCase.Narrative;
            dimCase.IsReportedByAuthorizedSubmitter = oCase.IsReportedByAuthorizedSubmitter;
            dimCase.SendECommunication = oCase.SendECommunication;
            dimCase.ECommunicationRequestDate = oCase.ECommunicationRequestDate;

            var rp = oCase.RiskProfile;
            if (rp != null)
            {
                dimCase.RiskProfileCode = rp.Code;
                dimCase.RiskProfileName = rp.Name;
            }

            dimCase.CurrentOfficeLocation = oCase.CurrentOfficeLocation;

            var pay = oCase.Pay;
            if (pay != null)
            {
                dimCase.TotalPaid = pay.TotalPaid;
                dimCase.TotalOffset = pay.TotalOffset;
                dimCase.ApplyOffsetsByDefault = pay.ApplyOffsetsByDefault;
                dimCase.WaiveWaitingPeriod = pay.WaiveWaitingPeriod.Value;
                dimCase.PayScheduleId = pay.PayScheduleId;
            }

            #endregion

            if (dimCase.CaseIsApproved)
            {
                DateRange[] actualDurationForEachPolicy = oCase.Summary.Policies.SelectMany(m => m.Detail)
                                                            .Where(p => p.Determination == AdjudicationSummaryStatus.Approved)
                                                            .Select(
                                                                g => new DateRange(
                                                                g.StartDate, g.EndDate
                                                                )).Distinct().ToArray();
                dimCase.ActualDuration = Date.GetCumulativeTimeframeMinusGapsAndOverlap(actualDurationForEachPolicy).Days;
                dimCase.ExceededDuration = dimCase.ActualDuration < oCase.Disability?.PrimaryPathMaxDays ? 0 : (dimCase.ActualDuration - oCase.Disability?.PrimaryPathMaxDays);
            }
            dimCase.HasRelapse = oCase.HasRelapse();
            return dimCase;
        }
    }
}