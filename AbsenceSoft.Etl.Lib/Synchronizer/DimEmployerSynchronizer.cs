﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using System.Linq;

namespace AbsenceSoft.Etl.Lib
{
    public sealed class DimEmployerSynchronizer : BaseSynchronizer
    {
        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_EMPLOYER; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployerSynchronizer"/> class.
        /// </summary>
        public DimEmployerSynchronizer() : base() { }

        /// <summary>
        /// Called when [syncronize].
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<Employer, DimEmployer>(syncManager, PopulateDimEmployerFromEmployer, null);
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<Employer>();
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<Employer, DimEmployer>(failure, PopulateDimEmployerFromEmployer);
        }

        private DimEmployer PopulateDimEmployerFromEmployer(Employer employer)
        {
            DimEmployer dimEmployer = null;
            dimEmployer = new DimEmployer(DataWarehouseConnectionString)
            {
                ExternalReferenceId = employer.Id,
                CustomerId = employer.CustomerId,
                ExternalCreatedDate = employer.CreatedDate,
                ExternalModifiedDate = employer.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = 0,
                VersionPreviousId = 0,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                Name = employer.Name,
                ReferenceCode = employer.ReferenceCode,
                EmployerUrl = employer.Url,
                ResetMonth = employer.ResetMonth.HasValue ? (int)employer.ResetMonth.Value : (int?)null,
                ResetDayOfMonth = employer.ResetDayOfMonth,
                StartDate = employer.StartDate,
                EndDate = employer.EndDate,
                FMLPeriodType = (int)employer.FMLPeriodType,
                AllowIntermittentForBirthAdoptionOrFosterCare = employer.AllowIntermittentForBirthAdoptionOrFosterCare,
                IgnoreScheduleForPriorHoursWorked = employer.IgnoreScheduleForPriorHoursWorked,
                IgnoreAverageMinutesWorkedPerWeek = employer.IgnoreAverageMinutesWorkedPerWeek,
                Enable50in75mileRule = employer.Enable50In75MileRule,
                EnableSpouseAtSameEmployerRule = employer.EnableSpouseAtSameEmployerRule,
                FTWeeklyWorkHours = employer.FTWeeklyWorkHours,
                PubliclyTradedCompany = employer.IsPubliclyTradedCompany,
                ContactFirstName = (employer.Contact == null) ? null : employer.Contact.FirstName,
                ContactLastName = (employer.Contact == null) ? null : employer.Contact.LastName,
                ContactEmail = (employer.Contact == null) ? null : employer.Contact.Email,
                ContactAddress1 = (employer.Contact == null) ? null : employer.Contact.Address.Address1,
                ContactAddress2 = (employer.Contact == null) ? null : employer.Contact.Address.Address2,
                ContactCity = (employer.Contact == null) ? null : employer.Contact.Address.City,
                ContactState = (employer.Contact == null) ? null : employer.Contact.Address.State,
                ContactPostalCode = (employer.Contact == null) ? null : employer.Contact.Address.PostalCode,
                ContactCountry = (employer.Contact == null) ? null : employer.Contact.Address.Country,
                IsDeleted = employer.IsDeleted
            };

            dimEmployer.IsDeleted = employer.IsDeleted;
            if (employer.IsDeleted)
                dimEmployer.VersionSequence = Const.VER_SEQ_DELETED_VALUE;

            EmployerServiceOption employerServiceOption = EmployerServiceOption.AsQueryable()
                    .FirstOrDefault(p => p.EmployerId == employer.Id && p.Key == "ServiceOptionIndicator");
            if (employerServiceOption != null)
            {
                CustomerServiceOption customerServiceOption = employerServiceOption.CusomerServiceOption;
                dimEmployer.ServiceType = customerServiceOption != null ? customerServiceOption.Value : null; 
            }

            //set audit data
            dimEmployer.CreatedBy = employer.CreatedById;
            dimEmployer.ModifiedBy = employer.ModifiedById;

            return dimEmployer;
        }
    }
}
