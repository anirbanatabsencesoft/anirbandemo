﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Etl.Lib.Synchronizer
{
    /// <summary>
    /// Synchronizer for Relaspe
    /// </summary>
    public class DimRelpaseSynchronizer : BaseSynchronizer
    {
        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMEMSION_RELAPSE; } }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<Relapse, DimRelapse>(failure, PopulateDimRelapse);
        }

        /// <summary>
        /// Called when [syncronize].
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<Relapse, DimRelapse>(syncManager, PopulateDimRelapse, null);
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<Relapse>();
        }

        /// <summary>
        /// Populates the dim relpase from Relapse.
        /// </summary>
        /// <param name="relapse">The relapse of case.</param>
        /// <returns></returns>
        private DimRelapse PopulateDimRelapse(Relapse relapse)
        {
            if (relapse != null)
            {
                DimRelapse dimRelapse = new DimRelapse(DataWarehouseConnectionString)
                {
                    ExternalReferenceId = relapse.Id,
                    ExternalCreatedDate = relapse.CreatedDate,
                    ExternalModifiedDate = relapse.ModifiedDate,
                    VersionIsCurrent = true,
                    VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                    VersionRootId = 0,
                    VersionPreviousId = 0,
                    VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                    EmployeeId = relapse.Employee?.Id,
                    CustomerId = relapse.Customer?.Id,
                    EmployerId = relapse.EmployerId,
                    CaseId = relapse.CaseId,
                    CreatedBy = relapse.CreatedById,
                    ModifiedBy = relapse.ModifiedById,
                    StartDate = relapse.StartDate,
                    EndDate = relapse.EndDate
                };
                dimRelapse.IsDeleted = relapse.IsDeleted;
                if (dimRelapse.IsDeleted)
                {
                    dimRelapse.VersionSequence = Const.VER_SEQ_DELETED_VALUE;
                }
                Case thisCase = Case.Repository.GetById(relapse.CaseId);
                if (thisCase != null)
                {
                    var eventq = thisCase.CaseEvents.FirstOrDefault(e => e.EventType == AbsenceSoft.Data.Enums.CaseEventType.ReturnToWork) ?? thisCase.CaseEvents.FirstOrDefault(e => e.EventType == AbsenceSoft.Data.Enums.CaseEventType.EstimatedReturnToWork);
                    dimRelapse.PriorReturnToWorkDate = eventq?.AllEventDates?.OrderByDescending(c => c).FirstOrDefault(c => c < relapse.StartDate);
                    //relapse segment start date minus preceding gap start date
                    dimRelapse.PriorReturnToWorkDays = dimRelapse.PriorReturnToWorkDate.HasValue ? dimRelapse.PriorReturnToWorkDate.Value.Subtract(thisCase.StartDate).Days : new int?();
                }
                return dimRelapse;
            }
            else
            {
                return null;
            }
        }
    }
}
