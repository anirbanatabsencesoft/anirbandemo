﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;

namespace AbsenceSoft.Etl.Lib
{
    public class DimCustomerSynchronizer : BaseSynchronizer
    {
        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_CUSTOMER; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimCustomerSynchronizer"/> class.
        /// </summary>
        public DimCustomerSynchronizer() : base() { }

        /// <summary>
        /// Called when [syncronize].
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<Customer, DimCustomer>(syncManager, PopulateDimCustomerFromCustomer, null);
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<Customer>();
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<Customer, DimCustomer>(failure, PopulateDimCustomerFromCustomer);
        }

        /// <summary>
        /// Populates the dim customer from customer.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <returns></returns>
        private DimCustomer PopulateDimCustomerFromCustomer(Customer customer)
        {
            var dimCustomer = new DimCustomer(DataWarehouseConnectionString)
            {
                ExternalReferenceId = customer.Id,
                ExternalCreatedDate = customer.CreatedDate,
                ExternalModifiedDate = customer.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = 0,
                VersionPreviousId = 0,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                CustomerCreatedDate = customer.CreatedDate,
                CustomerName = customer.Name,
                CustomerUrl = customer.Url
            };
            if ((customer.BillingContact != null) && (customer.BillingContact.Address != null))
            {
                dimCustomer.CustomerBillingAddress1 = customer.BillingContact.Address.Address1;
                dimCustomer.CustomerBillingAddress2 = customer.BillingContact.Address.Address2;
                dimCustomer.CustomerBillingCity = customer.BillingContact.Address.City;
                dimCustomer.CustomerBillingCountry = customer.BillingContact.Address.Country;
                dimCustomer.CustomerBillingEmail = customer.BillingContact.Email;
                dimCustomer.CustomerBillingFirstName = customer.BillingContact.FirstName;
                dimCustomer.CustomerBillingLastName = customer.BillingContact.LastName;
                dimCustomer.CustomerBillingPostalCode = customer.BillingContact.Address.PostalCode;
                dimCustomer.CustomerBillingState = customer.BillingContact.Address.State;
            }
            if ((customer.Contact != null) && (customer.Contact.Address != null))
            {
                dimCustomer.CustomerContactAddress1 = customer.Contact.Address.Address1;
                dimCustomer.CustomerContactAddress2 = customer.Contact.Address.Address2;
                dimCustomer.CustomerContactCity = customer.Contact.Address.City;
                dimCustomer.CustomerContactCountry = customer.Contact.Address.Country;
                dimCustomer.CustomerContactEmail = customer.Contact.Email;
                dimCustomer.CustomerContactFirstName = customer.Contact.FirstName;
                dimCustomer.CustomerContactLastName = customer.Contact.LastName;
                dimCustomer.CustomerContactPostalCode = customer.Contact.Address.PostalCode;
                dimCustomer.CustomerContactState = customer.Contact.Address.State;
            }

            dimCustomer.IsDeleted = customer.IsDeleted;
            if (customer.IsDeleted)
                dimCustomer.VersionSequence = Const.VER_SEQ_DELETED_VALUE;

            //set audit data
            dimCustomer.CreatedBy = customer.CreatedById;
            dimCustomer.ModifiedBy = customer.ModifiedById;

            return dimCustomer;
        }
    }
}
