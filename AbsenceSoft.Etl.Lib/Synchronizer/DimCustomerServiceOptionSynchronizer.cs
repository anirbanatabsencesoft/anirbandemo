﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;

namespace AbsenceSoft.Etl.Lib.Synchronizer
{
    public class DimCustomerServiceOptionSynchronizer : BaseSynchronizer
    {
        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMEMSION_CUSTOMER_SERVICE_OPTION; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimCustomerServiceOptionSynchronizer"/> class.
        /// </summary>
        public DimCustomerServiceOptionSynchronizer() : base() { }

        /// <summary>
        /// Called when [syncronize].
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<CustomerServiceOption, DimCustomerServiceOption>(syncManager, PopulateDimCustomerServiceOptionFromCustomerServiceOption, null);
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<CustomerServiceOption>();
        }

        /// <summary>
        /// Populates the dim customer service option from customer service option.
        /// </summary>
        /// <param name="contact">The customer service option.</param>
        /// <returns></returns>
        private DimCustomerServiceOption PopulateDimCustomerServiceOptionFromCustomerServiceOption(CustomerServiceOption customerServiceOption)
        {
            DimCustomerServiceOption dimCustomerServiceOption = new DimCustomerServiceOption(DataWarehouseConnectionString)
            {
                ExternalReferenceId = customerServiceOption.Id,
                ExternalCreatedDate = customerServiceOption.CreatedDate,
                ExternalModifiedDate = customerServiceOption.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = 0,
                VersionPreviousId = 0,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                CustomerId = customerServiceOption.CustomerId,
                CreatedBy = customerServiceOption.CreatedById,
                ModifiedBy = customerServiceOption.ModifiedById,
                IsDeleted = customerServiceOption.IsDeleted,
                Key = customerServiceOption.Key,
                Value = customerServiceOption.Value,
                Order = customerServiceOption.Order
            };

            return dimCustomerServiceOption;
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<CustomerServiceOption, DimCustomerServiceOption>(failure, PopulateDimCustomerServiceOptionFromCustomerServiceOption);
        }
    }
}
