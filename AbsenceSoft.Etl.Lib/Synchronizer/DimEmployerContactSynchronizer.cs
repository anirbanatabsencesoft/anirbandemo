﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;

namespace AbsenceSoft.Etl.Lib.Synchronizer
{
    public class DimEmployerContactSynchronizer : BaseSynchronizer
    {
        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMEMSION_EMPLOYER_CONTACT; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployeeContactSynchronizer"/> class.
        /// </summary>
        public DimEmployerContactSynchronizer() : base() { }

        /// <summary>
        /// Called when [syncronize].
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<EmployerContact, DimEmployerContact>(syncManager, PopulateDimEmployerContactFromEmployerContact, null);
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<EmployerContact>();
        }

        /// <summary>
        /// Populates the dim contact from contact.
        /// </summary>
        /// <param name="contact">The contact.</param>
        /// <returns></returns>
        private DimEmployerContact PopulateDimEmployerContactFromEmployerContact(EmployerContact contact)
        {
            DimEmployerContact dimContact = new DimEmployerContact(DataWarehouseConnectionString)
            {
                ExternalReferenceId = contact.Id,
                ExternalCreatedDate = contact.CreatedDate,
                ExternalModifiedDate = contact.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = 0,
                VersionPreviousId = 0,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                CustomerId = contact.CustomerId,
                EmployerId = contact.EmployerId,
                CreatedBy = contact.CreatedById,
                ModifiedBy = contact.ModifiedById,
                IsDeleted = contact.IsDeleted,
                ContactFirstName = contact.Contact?.FirstName,
                ContactLastName = contact.Contact?.LastName,
                ContactEmail = contact.Contact?.Email,
                ContactAltEmail = contact.Contact?.AltEmail,
                ContactWorkPhone = contact.Contact?.WorkPhone,
                ContactFax = contact.Contact?.Fax,
                ContactAddress1 = contact.Contact?.Address?.Address1,
                ContactCity = contact.Contact?.Address?.City,
                ContactState = contact.Contact?.Address?.State,
                ContactCountry = contact.Contact?.Address?.Country,
                ContactPostalCode = contact.Contact?.Address?.PostalCode,
                ContactTypeCode = contact.ContactTypeCode,
                ContactTypeName = contact.ContactTypeName
            };

            return dimContact;
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<EmployerContact, DimEmployerContact>(failure, PopulateDimEmployerContactFromEmployerContact);
        }
    }
}
