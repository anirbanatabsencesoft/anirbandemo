﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Common.Security;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Workflows;

namespace AbsenceSoft.Etl.Lib
{
    public class DimWorflowInstanceSynchronizer : BaseSynchronizer
    {
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_WORKFLOW_INSTANCE; } }
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<WorkflowInstance, DimWorkflowInstance>(failure, PopulateDimWorkflowInstanceFromWorkflowInstance);
        }

        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<WorkflowInstance, DimWorkflowInstance>(syncManager, PopulateDimWorkflowInstanceFromWorkflowInstance, null);
        }

        private DimWorkflowInstance PopulateDimWorkflowInstanceFromWorkflowInstance(WorkflowInstance workflowInstance)
        {
            if (workflowInstance != null)
            {
                DimWorkflowInstance dimWorkflowInstance = new DimWorkflowInstance(DataWarehouseConnectionString);

                dimWorkflowInstance.ExternalReferenceId = workflowInstance.Id;
                dimWorkflowInstance.ExternalCreatedDate = workflowInstance.CreatedDate;
                dimWorkflowInstance.ExternalModifiedDate = workflowInstance.ModifiedDate;
                dimWorkflowInstance.VersionIsCurrent = true;
                dimWorkflowInstance.VersionSequence = Const.VER_SEQ_NEWEST_VALUE;
                dimWorkflowInstance.VersionRootId = 0;
                dimWorkflowInstance.VersionPreviousId = 0;
                dimWorkflowInstance.VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT;
                dimWorkflowInstance.EmployeeId = workflowInstance.EmployeeId;
                dimWorkflowInstance.CustomerId = workflowInstance.CustomerId;
                dimWorkflowInstance.EmployerId = workflowInstance.EmployerId;
                dimWorkflowInstance.CaseId = workflowInstance.CaseId;
                dimWorkflowInstance.EventId = workflowInstance.EventId;
                dimWorkflowInstance.EventType = (int?)workflowInstance.EventType;
                dimWorkflowInstance.WorkflowId = workflowInstance.WorkflowId;
                dimWorkflowInstance.WorkflowName = workflowInstance.WorkflowName;
                dimWorkflowInstance.Status = (int?)workflowInstance.Status;
                dimWorkflowInstance.AccommodationEndDate = workflowInstance.Metadata.GetRawValue<DateTime?>("AccommodationEndDate");
                dimWorkflowInstance.AccommodationId = workflowInstance.Metadata.GetRawValue<string>("AccommodationId");
                dimWorkflowInstance.AccommodationTypeCode = workflowInstance.Metadata.GetRawValue<string>("AccommodationTypeCode");
                dimWorkflowInstance.AccommodationTypeId = workflowInstance.Metadata.GetRawValue<string>("AccommodationTypeId");
                dimWorkflowInstance.ActiveAccommodationId = workflowInstance.Metadata.GetRawValue<string>("ActiveAccommodationId");
                dimWorkflowInstance.AppliedPolicyId = workflowInstance.Metadata.GetRawValue<string>("AppliedPolicyId");
                dimWorkflowInstance.AttachmentId = workflowInstance.Metadata.GetRawValue<string>("AttachmentId");
                dimWorkflowInstance.CloseCase = workflowInstance.Metadata.GetRawValue<bool?>("CloseCase");
                dimWorkflowInstance.CommunicationId = workflowInstance.Metadata.GetRawValue<string>("CommunicationId");
                dimWorkflowInstance.CommunicationType = workflowInstance.Metadata.GetRawValue<int?>("CommunicationType");
                dimWorkflowInstance.ConditionStartDate = workflowInstance.Metadata.GetRawValue<DateTime?>("ConditionStartDate").GetValueOrNullIfMinValue();
                dimWorkflowInstance.ContactEmployeeDueDate = workflowInstance.Metadata.GetRawValue<DateTime?>("ContactEmployeeDueDate").GetValueOrNullIfMinValue();
                dimWorkflowInstance.ContactHcpDueDate = workflowInstance.Metadata.GetRawValue<DateTime?>("ContactHCPDueDate").GetValueOrNullIfMinValue();
                dimWorkflowInstance.DenialExplanation = workflowInstance.Metadata.GetRawValue<string>("DenialExplanation");
                dimWorkflowInstance.DenialReason = workflowInstance.Metadata.GetRawValue<int?>("DenialReason");
                dimWorkflowInstance.Description = workflowInstance.Metadata.GetRawValue<string>("Description");
                dimWorkflowInstance.Determination = workflowInstance.Metadata.GetRawValue<int?>("Determination");
                dimWorkflowInstance.DueDate = workflowInstance.Metadata.GetRawValue<DateTime?>("DueDate").GetValueOrNullIfMinValue();
                dimWorkflowInstance.ErgonomicAssessmentDate = workflowInstance.Metadata.GetRawValue<DateTime?>("ErgonomicAssessmentDate").GetValueOrNullIfMinValue();
                dimWorkflowInstance.ExtendGracePeriod = workflowInstance.Metadata.GetRawValue<bool?>("ExtendGracePeriod");
                dimWorkflowInstance.FirstExhaustionDate = workflowInstance.Metadata.GetRawValue<DateTime?>("FirstExhaustionDate").GetValueOrNullIfMinValue();
                dimWorkflowInstance.GeneralHealthCondition = workflowInstance.Metadata.GetRawValue<string>("GeneralHealthCondition");
                dimWorkflowInstance.HasWorkRestrictions = workflowInstance.Metadata.GetRawValue<bool?>("HasWorkRestrictions");
                dimWorkflowInstance.IllnessOrInjuryDate = workflowInstance.Metadata.GetRawValue<DateTime?>("IllnessOrInjuryDate").GetValueOrNullIfMinValue();
                dimWorkflowInstance.NewDueDate = workflowInstance.Metadata.GetRawValue<DateTime?>("NewDueDate").GetValueOrNullIfMinValue();
                dimWorkflowInstance.PaperworkAttachmentId = workflowInstance.Metadata.GetRawValue<string>("PaperworkAttachmentId");
                dimWorkflowInstance.PaperworkDueDate = workflowInstance.Metadata.GetRawValue<DateTime?>("PaperworkDueDate").GetValueOrNullIfMinValue();
                dimWorkflowInstance.PaperworkId = workflowInstance.Metadata.GetRawValue<string>("PaperworkId");
                dimWorkflowInstance.PaperworkName = workflowInstance.Metadata.GetRawValue<string>("PaperworkName");
                dimWorkflowInstance.PaperworkReceived = workflowInstance.Metadata.GetRawValue<bool?>("PaperworkReceived");
                dimWorkflowInstance.PolicyCode = workflowInstance.Metadata.GetRawValue<string>("PolicyCode");
                dimWorkflowInstance.PolicyId = workflowInstance.Metadata.GetRawValue<string>("PolicyId");
                dimWorkflowInstance.Reason = workflowInstance.Metadata.GetRawValue<int?>("Reason");
                dimWorkflowInstance.RequiresReview = workflowInstance.Metadata.GetRawValue<bool?>("RequiresReview");
                dimWorkflowInstance.ReturnAttachmentId = workflowInstance.Metadata.GetRawValue<string>("ReturnAttachmentId");
                dimWorkflowInstance.ReturnToWorkDate = workflowInstance.Metadata.GetRawValue<DateTime?>("ReturnToWorkDate").GetValueOrNullIfMinValue();
                dimWorkflowInstance.RTW = workflowInstance.Metadata.GetRawValue<bool?>("RTW");
                dimWorkflowInstance.SourceWorkflowActivityActivityId = workflowInstance.Metadata.GetRawValue<string>("SourceWorkflowActivityActivityId");
                dimWorkflowInstance.SourceWorkflowActivityId = workflowInstance.Metadata.GetRawValue<string>("SourceWorkflowActivityId");
                dimWorkflowInstance.SourceWorkflowInstanceId = workflowInstance.Metadata.GetRawValue<string>("SourceWorkflowInstanceId");
                dimWorkflowInstance.Template = workflowInstance.Metadata.GetRawValue<string>("Template");
                dimWorkflowInstance.WorkflowActivityId = workflowInstance.Metadata.GetRawValue<string>("WorkflowActivityId");
                dimWorkflowInstance.IsDeleted = workflowInstance.IsDeleted;
                dimWorkflowInstance.CreatedBy = workflowInstance.CreatedById;
                dimWorkflowInstance.ModifiedBy = workflowInstance.ModifiedById;

                return dimWorkflowInstance;
            }
            else
                return null;
        }
    }
}
