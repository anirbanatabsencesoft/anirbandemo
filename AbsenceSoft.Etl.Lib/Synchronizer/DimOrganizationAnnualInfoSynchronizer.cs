﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Etl.Lib
{
    public sealed class DimOrganizationAnnualInfoSynchronizer : BaseSynchronizer
    {
        // Cache properties.
        private Dictionary<string, User> _users = null;


        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_ORGANIZATION_ANNUAL_INFO; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimOrganizationAnnualInfoSynchronizer"/> class.
        /// </summary>
        public DimOrganizationAnnualInfoSynchronizer() : base() { }

        /// <summary>
        /// Called when [syncronize].
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            _users = new Dictionary<string, User>();
            BatchReadCompleted += (results) => _users = User.Repository.Collection.Find(User.Query.In(c => c.Id, _users.Keys)).ToDictionary(r => r.Id);
            PerformDimensionalSync<OrganizationAnnualInfo, DimOrganizationAnnualInfo>(syncManager, PopulateDimOrganizationAnnualInfoFromOrganizationAnnualInfo, org =>
            {
                _users[org.CreatedById] = null;
            });
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<Organization>();
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<OrganizationAnnualInfo, DimOrganizationAnnualInfo>(failure, PopulateDimOrganizationAnnualInfoFromOrganizationAnnualInfo);
        }

        /// <summary>
        /// Populates the dim organization annual information from organization annual information.
        /// </summary>
        /// <param name="organizationAnnualInfo">The organization annual information.</param>
        /// <returns></returns>
        private DimOrganizationAnnualInfo PopulateDimOrganizationAnnualInfoFromOrganizationAnnualInfo(OrganizationAnnualInfo organizationAnnualInfo)
        {
            var dimOrganizationAnnualInfo = new DimOrganizationAnnualInfo(DataWarehouseConnectionString)
            {
                ExternalReferenceId = organizationAnnualInfo.Id,
                CustomerId = organizationAnnualInfo.CustomerId,
                EmployerId = organizationAnnualInfo.EmployerId,
                ExternalCreatedDate = organizationAnnualInfo.CreatedDate,
                ExternalModifiedDate = organizationAnnualInfo.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = 0,
                VersionPreviousId = 0,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                OrganizationCode = organizationAnnualInfo.OrganizationCode,
                OrganizationYear = organizationAnnualInfo.Year,
                AverageEmployeeCount = organizationAnnualInfo.AverageEmployeeCount,
                TotalHoursWorked = organizationAnnualInfo.TotalHoursWorked
            };

           
            dimOrganizationAnnualInfo.IsDeleted = organizationAnnualInfo.IsDeleted;
            if (organizationAnnualInfo.IsDeleted)
                dimOrganizationAnnualInfo.VersionSequence = Const.VER_SEQ_DELETED_VALUE;

            //set audit data
            dimOrganizationAnnualInfo.CreatedBy = organizationAnnualInfo.CreatedById;
            dimOrganizationAnnualInfo.ModifiedBy = organizationAnnualInfo.ModifiedById;

            return dimOrganizationAnnualInfo;
        }
    }
}
