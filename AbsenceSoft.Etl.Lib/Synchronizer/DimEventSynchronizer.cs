﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;

namespace AbsenceSoft.Etl.Lib.Synchronizer
{
    public class DimEventSynchronizer : BaseSynchronizer
    {
        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMEMSION_EVENT; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimEventSynchronizer"/> class.
        /// </summary>
        public DimEventSynchronizer() : base() { }

        /// <summary>
        /// Called when [syncronize].
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<Event, DimEvent>(syncManager, PopulateDimEventFromEvent, null);
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<Event>();
        }

        /// <summary>
        /// Populates the dimevent from event.
        /// </summary>
        /// <returns></returns>
        private DimEvent PopulateDimEventFromEvent(Event evt)
        {
            DimEvent dimEvent = new DimEvent(DataWarehouseConnectionString)
            {
                ExternalReferenceId = evt.Id,
                ExternalCreatedDate = evt.CreatedDate,
                ExternalModifiedDate = evt.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = 0,
                VersionPreviousId = 0,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                CustomerId = evt.CustomerId,
                EmployerId = evt.EmployerId,
                EmployeeId = evt.EmployeeId,
                ToDoItemId = evt.Metadata.GetRawValue<string>("ToDoItemId"),
                CreatedBy = evt.CreatedById,
                ModifiedBy = evt.ModifiedById,
                IsDeleted = evt.IsDeleted,
                Name = evt.Name,
                CaseId = evt.CaseId,
                AccomodationEndDate = evt.Metadata.GetRawValue<DateTime?>("AccommodationEndDate"),
                AccomodationId = evt.Metadata.GetRawValue<string>("AccommodationId"),
                AccomodationTypeCode = evt.Metadata.GetRawValue<string>("AccommodationTypeCode"),
                AccomodationTypeId = evt.Metadata.GetRawValue<string>("AccommodationTypeId"),
                ActiveAccomodationId = evt.Metadata.GetRawValue<string>("ActiveAccommodationId"),
                AppliedPolicyId = evt.Metadata.GetRawValue<string>("AppliedPolicyId"),
                AttachmentId = evt.Metadata.GetRawValue<string>("AttachmentId"),
                CloseCase = evt.Metadata.GetRawValue<bool?>("CloseCase"),                
                CommunicationId = evt.Metadata.GetRawValue<string>("CommunicationId"),
                CommunicationType = evt.Metadata.GetRawValue<int?>("CommunicationType"),
                ConditionStartDate = evt.Metadata.GetRawValue<DateTime?>("ConditionStartDate").GetValueOrNullIfMinValue(),
                ContactEmployeeDueDate = evt.Metadata.GetRawValue<DateTime?>("ContactEmployeeDueDate").GetValueOrNullIfMinValue(),
                ContactHcpDueDate = evt.Metadata.GetRawValue<DateTime?>("ContactHCPDueDate").GetValueOrNullIfMinValue(),
                DenialExplanation = evt.Metadata.GetRawValue<string>("DenialExplanation"),
                DenialReason = evt.Metadata.GetRawValue<int?>("DenialReason"),
                Description = evt.Metadata.GetRawValue<string>("Description"),
                Determination = evt.Metadata.GetRawValue<int?>("Determination"),
                DueDate = evt.Metadata.GetRawValue<DateTime?>("DueDate").GetValueOrNullIfMinValue(),
                EventType = Convert.ToString(evt.EventType),
                ErgonomicAssessmentDate = evt.Metadata.GetRawValue<DateTime?>("ErgonomicAssessmentDate").GetValueOrNullIfMinValue(),
                ExtendGracePeriod = evt.Metadata.GetRawValue<bool?>("ExtendGracePeriod"),
                FirstExhaustionDate = evt.Metadata.GetRawValue<DateTime?>("FirstExhaustionDate").GetValueOrNullIfMinValue(),
                GeneralHealthCondition = evt.Metadata.GetRawValue<string>("GeneralHealthCondition"),
                HasWorkRestrictions = evt.Metadata.GetRawValue<bool?>("HasWorkRestrictions"),
                IllnessOrInjuryDate = evt.Metadata.GetRawValue<DateTime?>("IllnessOrInjuryDate").GetValueOrNullIfMinValue(),
                NewDueDate = evt.Metadata.GetRawValue<DateTime?>("NewDueDate").GetValueOrNullIfMinValue(),
                PaperworkAttachmentId = evt.Metadata.GetRawValue<string>("PaperworkAttachmentId"),
                PaperworkDueDate = evt.Metadata.GetRawValue<DateTime?>("PaperworkDueDate").GetValueOrNullIfMinValue(),
                PaperworkId = evt.Metadata.GetRawValue<string>("PaperworkId"),
                PaperworkName = evt.Metadata.GetRawValue<string>("PaperworkName"),
                PaperworkReceived = evt.Metadata.GetRawValue<bool?>("PaperworkReceived"),
                PolicyCode = evt.Metadata.GetRawValue<string>("PolicyCode"),
                PolicyId = evt.Metadata.GetRawValue<string>("PolicyId"),
                Reason = evt.Metadata.GetRawValue<int?>("Reason"),
                RequiresReview = evt.Metadata.GetRawValue<bool?>("RequiresReview"),
                ReturnAttachmentId = evt.Metadata.GetRawValue<string>("ReturnAttachmentId"),
                ReturnToWorkDate = evt.Metadata.GetRawValue<DateTime?>("ReturnToWorkDate").GetValueOrNullIfMinValue(),
                RTW = evt.Metadata.GetRawValue<bool?>("RTW"),
                SourceWorkflowActivityActivityId = evt.Metadata.GetRawValue<string>("SourceWorkflowActivityActivityId"),
                SourceWorkflowActivityId = evt.Metadata.GetRawValue<string>("SourceWorkflowActivityId"),
                SourceWorkflowInstanceId = evt.Metadata.GetRawValue<string>("SourceWorkflowInstanceId"),
                Template = evt.Metadata.GetRawValue<string>("Template"),
                WorkFlowActivityId = evt.Metadata.GetRawValue<string>("WorkflowActivityId")
            };

            return dimEvent;
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<Event, DimEvent>(failure, PopulateDimEventFromEvent);
        }
    }
}
