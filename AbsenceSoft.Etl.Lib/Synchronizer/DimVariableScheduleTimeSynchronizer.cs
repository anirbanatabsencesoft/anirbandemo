﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Common.Security;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Communications;

namespace AbsenceSoft.Etl.Lib
{
    public class DimVariableScheduleTimeSynchronizer : BaseSynchronizer
    {
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_VARIABLE_SCHEDULE_TIME; } }
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<VariableScheduleTime, DimVariableScheduleTime>(failure, PopulateDimVariableScheduleTimeFromVariableScheduleTime);
        }

        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<VariableScheduleTime, DimVariableScheduleTime>(syncManager, PopulateDimVariableScheduleTimeFromVariableScheduleTime, null);
        }

        private DimVariableScheduleTime PopulateDimVariableScheduleTimeFromVariableScheduleTime(VariableScheduleTime variableScheduleTime)
        {            
            if (variableScheduleTime != null)
            {
                DimVariableScheduleTime dimVariableScheduleTime = new DimVariableScheduleTime(DataWarehouseConnectionString)
                {
                    ExternalReferenceId = variableScheduleTime.Id,
                    ExternalCreatedDate = variableScheduleTime.CreatedDate,
                    ExternalModifiedDate = variableScheduleTime.ModifiedDate,
                    VersionIsCurrent = true,
                    VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                    VersionRootId = 0,
                    VersionPreviousId = 0,
                    VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                    EmployeeId = variableScheduleTime.Employee?.Id,
                    CustomerId = variableScheduleTime.Customer?.Id,
                    EmployerId = variableScheduleTime.EmployerId,
                    TimeSampleDate = variableScheduleTime.Time.SampleDate.GetValueOrNullIfMinValue(),
                    TimeTotalMinutes = variableScheduleTime.Time.TotalMinutes,
                    EmployeeNumber = variableScheduleTime.EmployeeNumber,                
                    IsDeleted = variableScheduleTime.IsDeleted,
                    CreatedBy = variableScheduleTime.CreatedById,
                    ModifiedBy = variableScheduleTime.ModifiedById                    
                };

                return dimVariableScheduleTime;
            }
            else
                return null;
        }
    }
}
