﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Common.Security;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Notes;

namespace AbsenceSoft.Etl.Lib
{
    public class DimCaseNoteSynchronizer : BaseSynchronizer
    {
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_CASE_NOTE; } }
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<CaseNote, DimCaseNote>(failure, PopulateDimCaseNoteFromCaseNote);
        }

        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<CaseNote, DimCaseNote>(syncManager, PopulateDimCaseNoteFromCaseNote, null);
        }

        private DimCaseNote PopulateDimCaseNoteFromCaseNote(CaseNote caseNote)
        {            
            if (caseNote != null)
            {
                DimCaseNote DimCaseNote = new DimCaseNote(DataWarehouseConnectionString)
                {
                    ExternalReferenceId = caseNote.Id,
                    ExternalCreatedDate = caseNote.CreatedDate,
                    ExternalModifiedDate = caseNote.ModifiedDate,
                    VersionIsCurrent = true,
                    VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                    VersionRootId = 0,
                    VersionPreviousId = 0,
                    VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                    EmployeeId = caseNote.Metadata.GetRawValue<string>("EmployeeId"),
                    CustomerId = caseNote.CustomerId,
                    EmployerId = caseNote.EmployerId,
                    CaseId = caseNote.CaseId,
                    AccommId = caseNote.Metadata.GetRawValue<string>("AccommId"),
                    AccommQuestionId = caseNote.Metadata.GetRawValue<string>("AccommQuestionId"),
                    Answer = caseNote.Metadata.GetRawValue<bool?>("Answer"),
                    CertId = caseNote.CertId.ToString(),
                    Contact = caseNote.Metadata.GetRawValue<string>("Contact"),
                    CopiedFrom = caseNote.Metadata.GetRawValue<string>("CopiedFrom"),
                    CopiedFromEmployee = caseNote.Metadata.GetRawValue<string>("CopiedFromEmployee"),
                    EmployeeNumber = caseNote.EnteredByEmployeeNumber,
                    EnteredByName = caseNote.EnteredByName,
                    Notes = caseNote.Notes,
                    ProcessPath = caseNote.Metadata.GetRawValue<string>("ProcessPath"),
                    RequestDate = caseNote.Metadata.GetRawValue<DateTime?>("RequestDate").GetValueOrNullIfMinValue(),
                    RequestId = caseNote.Metadata.GetRawValue<string>("RequestId"),
                    SAnswer = caseNote.Metadata.GetRawValue<string>("SAnswer"),
                    WorkRestrictionId = caseNote.Metadata.GetRawValue<string>("WorkRestrictionId"),
                    CategoryCode = caseNote.Categories.FirstOrDefault()?.CategoryCode,
                    CategoryName = caseNote.Categories.FirstOrDefault()?.CategoryName,
                    Category = (int?)caseNote.Category,
                    Public = caseNote.Public,
                    IsDeleted = caseNote.IsDeleted,
                    CreatedBy = caseNote.CreatedById,
                    ModifiedBy = caseNote.ModifiedById
                };
                return DimCaseNote;
            }
            else
                return null;
        }
    }
}
