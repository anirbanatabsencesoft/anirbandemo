﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;

namespace AbsenceSoft.Etl.Lib.Synchronizer
{
    public class DimEmployerContactTypesSynchronizer : BaseSynchronizer
    {
        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMEMSION_EMPLOYER_CONTACT_TYPES; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployerContactTypesSynchronizer"/> class.
        /// </summary>
        public DimEmployerContactTypesSynchronizer() : base() { }

        /// <summary>
        /// Called when [syncronize].
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<EmployerContactType, DimEmployerContactType>(syncManager, PopulateDimEmployerContactTypesFromEmployerContactType, null);
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<EmployerContactType>();
        }

        /// <summary>
        /// Populates the dim contact from contact.
        /// </summary>
        /// <param name="contact">The contact.</param>
        /// <returns></returns>
        private DimEmployerContactType PopulateDimEmployerContactTypesFromEmployerContactType(EmployerContactType contactType)
        {
            DimEmployerContactType dimContactType = new DimEmployerContactType(DataWarehouseConnectionString)
            {
                ExternalReferenceId = contactType.Id,
                ExternalCreatedDate = contactType.CreatedDate,
                ExternalModifiedDate = contactType.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = 0,
                VersionPreviousId = 0,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                CustomerId = contactType.CustomerId,
                CreatedBy = contactType.CreatedById,
                ModifiedBy = contactType.ModifiedById,
                IsDeleted = contactType.IsDeleted,
                Name = contactType.Name,
                Code = contactType.Code
            };

            return dimContactType;
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<EmployerContactType, DimEmployerContactType>(failure, PopulateDimEmployerContactTypesFromEmployerContactType);
        }
    }
}
