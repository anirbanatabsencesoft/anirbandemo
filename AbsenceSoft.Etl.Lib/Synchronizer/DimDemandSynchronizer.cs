﻿using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;

namespace AbsenceSoft.Etl.Lib
{
    public class DimDemandSynchronizer : BaseSynchronizer
    {
        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_DEMAND; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimDemandSynchronizer"/> class.
        /// </summary>
        public DimDemandSynchronizer() : base() { }

        /// <summary>
        /// Called when [syncronize].
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<Demand, DimDemand>(syncManager, PopulateDimDemandFromDemand, null);
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<Demand>();
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<Demand, DimDemand>(failure, PopulateDimDemandFromDemand);
        }

        /// <summary>
        /// Populates the dim demand from demand.
        /// </summary>
        /// <param name="demand">The demand.</param>
        /// <returns></returns>
        private DimDemand PopulateDimDemandFromDemand(Demand demand)
        {
            DimDemand dimDemand = null;
            dimDemand = new DimDemand(DataWarehouseConnectionString)
            {
                ExternalReferenceId = demand.Id,
                ExternalCreatedDate = demand.CreatedDate,
                ExternalModifiedDate = demand.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = 0,
                VersionPreviousId = 0,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                CustomerId = demand.CustomerId,
                Name = demand.Name,
                Code = demand.Code,
                Description = demand.Description,
                OrderData = demand.Order,
                Category = demand.Category
            };

            dimDemand.IsDeleted = demand.IsDeleted;
            if (demand.IsDeleted)
                dimDemand.VersionSequence = Const.VER_SEQ_DELETED_VALUE;

            //set audit data
            dimDemand.CreatedBy = demand.CreatedById;
            dimDemand.ModifiedBy = demand.ModifiedById;

            return dimDemand;
        }
    }
}
