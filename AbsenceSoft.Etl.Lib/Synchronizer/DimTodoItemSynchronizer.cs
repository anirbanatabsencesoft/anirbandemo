﻿using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Etl.Lib
{
    public sealed class DimTodoItemSynchronizer : BaseSynchronizer
    {
        // Cache properties.
        private Dictionary<string, User> _users = null;
        private Dictionary<string, string> _workflows = null;

        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_TODOITEM; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimTodoItemSynchronizer"/> class.
        /// </summary>
        public DimTodoItemSynchronizer() : base() { }

        /// <summary>
        /// Called when [syncronize].
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            _users = new Dictionary<string, User>();
            _workflows = new Dictionary<string, string>();
            BatchReadCompleted += (results) =>
            {
                _users = User.Repository.Collection.Find(User.Query.In(c => c.Id, _users.Keys)).ToDictionary(r => r.Id);
                _workflows = WorkflowInstance.Repository.Collection
                    .Find(User.Query.In(c => c.Id, _workflows.Keys))
                    .Select(w => new { Id = w.Id, WorkflowName = w.WorkflowName })
                    .ToDictionary(r => r.Id, r => r.WorkflowName);
            };
            PerformDimensionalSync<ToDoItem, DimTodoItem>(syncManager, PopulateDimTodoItemFromTodoItem, todo =>
            {
                if (!string.IsNullOrWhiteSpace(todo.AssignedToId))
                    _users[todo.AssignedToId] = null;
                if (!string.IsNullOrWhiteSpace(todo.WorkflowInstanceId))
                    _workflows[todo.WorkflowInstanceId] = null;
            });
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<ToDoItem>();
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<ToDoItem, DimTodoItem>(failure, PopulateDimTodoItemFromTodoItem);
        }

        /// <summary>
        /// Populates the dim todo item from todo item.
        /// </summary>
        /// <param name="oTodoItem">The o todo item.</param>
        /// <returns></returns>
        private DimTodoItem PopulateDimTodoItemFromTodoItem(ToDoItem oTodoItem)
        {
            var dimTodoItem = new DimTodoItem(DataWarehouseConnectionString)
            {
                ExternalReferenceId = oTodoItem.Id,
                ExternalCreatedDate = oTodoItem.CreatedDate,
                ExternalModifiedDate = oTodoItem.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = 0,
                VersionPreviousId = 0,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                ItemType = (int)oTodoItem.ItemType,
                AssignedToId = oTodoItem.AssignedToId,
                AssignedToName = oTodoItem.AssignedToName,
                EmployerId = oTodoItem.EmployerId,
                EmployeeId = oTodoItem.EmployeeId,
                CaseNumber = oTodoItem.CaseNumber,
                CustomerId = oTodoItem.CustomerId,
                CaseId = oTodoItem.CaseId,
                DueDate = oTodoItem.DueDate,
                Status = (int)oTodoItem.Status,
                Priority = (int)oTodoItem.Priority,
                Title = oTodoItem.Title,
                DueDateChangeReason = oTodoItem.DueDateChangeReason,
                Weight = oTodoItem.Weight,
                ResultText = oTodoItem.ResultText,
                Optional = oTodoItem.Optional,
                HideUntil = oTodoItem.HideUntil,
                Template = oTodoItem.Metadata.GetRawValue<string>("Template")
            };

            if (!string.IsNullOrWhiteSpace(oTodoItem.AssignedToId))
            {
                User user = null;
                if (_users == null || !_users.TryGetValue(oTodoItem.AssignedToId, out user))
                    user = oTodoItem.AssignedTo;
                if (user != null)
                {
                    dimTodoItem.AssignedToEmail = user.Email;
                    dimTodoItem.AssignedToFirstName = user.FirstName;
                    dimTodoItem.AssignedToLastName = user.LastName;
                }
            }

            if (!string.IsNullOrWhiteSpace(oTodoItem.WorkflowInstanceId))
            {
                string name = null;
                if (_workflows == null || !_workflows.TryGetValue(oTodoItem.WorkflowInstanceId, out name))
                    if (oTodoItem.WorkflowInstance != null)
                        name = oTodoItem.WorkflowInstance.WorkflowName;
                dimTodoItem.WorkFlowName = name;
            }

            dimTodoItem.IsDeleted = oTodoItem.IsDeleted;
            if (oTodoItem.IsDeleted)
                dimTodoItem.VersionSequence = Const.VER_SEQ_DELETED_VALUE;

            //set audit data
            dimTodoItem.CreatedBy = oTodoItem.CreatedById;
            dimTodoItem.ModifiedBy = oTodoItem.ModifiedById;

            return dimTodoItem;
        }
    }
}
