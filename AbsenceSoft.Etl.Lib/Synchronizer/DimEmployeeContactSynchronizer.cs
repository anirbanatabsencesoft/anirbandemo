﻿using System;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;

namespace AbsenceSoft.Etl.Lib
{
    public class DimEmployeeContactSynchronizer : BaseSynchronizer
    {
        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_EMPLOYEE_CONTACT; } }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployeeContactSynchronizer"/> class.
        /// </summary>
        public DimEmployeeContactSynchronizer() : base() { }

        /// <summary>
        /// Called when [syncronize].
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<EmployeeContact, DimEmployeeContact>(syncManager, PopulateDimContactFromContact, null);
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<EmployeeContact>();
        }

        /// <summary>
        /// Populates the dim contact from contact.
        /// </summary>
        /// <param name="contact">The contact.</param>
        /// <returns></returns>
        private DimEmployeeContact PopulateDimContactFromContact(EmployeeContact contact)
        {
            DimEmployeeContact dimContact = new DimEmployeeContact(DataWarehouseConnectionString)
            {
                ExternalReferenceId = contact.Id,
                ExternalCreatedDate = contact.CreatedDate,
                ExternalModifiedDate = contact.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = 0,
                VersionPreviousId = 0,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                EmployeeId = contact.EmployeeId,
                CustomerId = contact.CustomerId,
                EmployerId = contact.EmployerId,
                CreatedBy = contact.CreatedById,
                EmployeeNumber = contact.RelatedEmployeeNumber,
                ContactPosition = contact.ContactPosition,
                ContactTypeCode = contact.ContactTypeCode,
                ContactTypeName = contact.ContactTypeName,
                YearsOfAge = contact.YearsOfAge,
                MilitaryStatus = (int)contact.MilitaryStatus,
                Title = contact.Contact == null ? null : contact.Contact.Title,
                FirstName = contact.Contact == null ? null : contact.Contact.FirstName,
                MiddleName = contact.Contact == null ? null : contact.Contact.MiddleName,
                LastName = contact.Contact == null ? null : contact.Contact.LastName,
                IsPrimary = contact.Contact == null ? null : contact.Contact.IsPrimary,
                DateOfBirth = contact.Contact == null ? null : contact.Contact.DateOfBirth,
                AltEmail = contact.Contact == null ? null : contact.Contact.AltEmail,
                Email = contact.Contact == null ? null : contact.Contact.Email,
                WorkPhone = contact.Contact == null ? null : contact.Contact.WorkPhone,
                HomePhone = contact.Contact == null ? null : contact.Contact.HomePhone,
                CellPhone = contact.Contact == null ? null : contact.Contact.CellPhone,
                Fax = contact.Contact == null ? null : contact.Contact.Fax,
                CompanyName = contact.Contact == null ? null : contact.Contact.CompanyName,
                ContactPersonName = contact.Contact == null ? null : contact.Contact.ContactPersonName,
                Street = contact.Contact == null || contact.Contact.Address == null ? null : contact.Contact.Address.StreetAddress,
                Address1 = contact.Contact == null || contact.Contact.Address == null ? null : contact.Contact.Address.Address1,
                Address2 = contact.Contact == null || contact.Contact.Address == null ? null : contact.Contact.Address.Address2,
                City = contact.Contact == null || contact.Contact.Address == null ? null : contact.Contact.Address.City,
                State = contact.Contact == null || contact.Contact.Address == null ? null : contact.Contact.Address.State,
                PostalCode = contact.Contact == null || contact.Contact.Address == null ? null : contact.Contact.Address.PostalCode,
                Country = contact.Contact == null || contact.Contact.Address == null ? null : contact.Contact.Address.Country,
                IsDeleted = contact.IsDeleted
            };
            
            if (dimContact.IsDeleted)
                dimContact.VersionSequence = Const.VER_SEQ_DELETED_VALUE;

            return dimContact;
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<EmployeeContact, DimEmployeeContact>(failure, PopulateDimContactFromContact);
        }
    }
}
