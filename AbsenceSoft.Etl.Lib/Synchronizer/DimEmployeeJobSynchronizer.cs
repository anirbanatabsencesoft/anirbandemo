﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;

namespace AbsenceSoft.Etl.Lib.Synchronizer
{
    /// <summary>
    /// Employee Job Synchronizer
    /// </summary>
    public class DimEmployeeJobSynchronizer : BaseSynchronizer
    {
        /// <summary>
        /// Dimension Id from the Warehouse database
        /// </summary>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_EMPLOYEE_JOB; } }
        /// <summary>
        /// On Synchronize Event
        /// </summary>
        /// <param name="syncManager"></param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<EmployeeJob, DimEmployeeJob>(syncManager, PopulateDimEmployeeJobFromEmployeeJob, null);
        }
        /// <summary>
        /// Convert the EmployeeJob to DimEmployeeJob
        /// </summary>
        /// <param name="employeeJob"></param>
        /// <returns></returns>
        private DimEmployeeJob PopulateDimEmployeeJobFromEmployeeJob(EmployeeJob employeeJob)
        {
            DimEmployeeJob dimEmployeeNote = null;
            dimEmployeeNote = new DimEmployeeJob(DataWarehouseConnectionString)
            {
                ExternalReferenceId = employeeJob.Id,
                ExternalCreatedDate = employeeJob.CreatedDate,
                ExternalModifiedDate = employeeJob.ModifiedDate,
                CreatedBy = employeeJob.CreatedById,
                ModifiedBy = employeeJob.ModifiedById,
                EmployeeId = employeeJob.Employee?.Id,
                EmployerId = employeeJob.EmployerId,
                CustomerId = employeeJob.CustomerId,
                JobName = employeeJob.JobName,
                JobCode = employeeJob.JobCode,
                StatusValue = (int?)employeeJob.Status?.Value,
                StatusReason = (int?)employeeJob.Status?.Reason,
                StatusComments = employeeJob.Status?.Comments,
                StatusUserName = employeeJob.Status?.Username,
                StatusUserId = employeeJob.Status?.UserId,
                StatusDate = employeeJob.Status?.Date,
                StartDate = employeeJob.Dates?.StartDate,
                EndDate = employeeJob.Dates?.EndDate,
                IsDeleted = employeeJob.IsDeleted,
                VersionIsCurrent = true
            };

            return dimEmployeeNote;
        }
        /// <summary>
        /// On Retry Event
        /// </summary>
        /// <param name="failure"></param>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<EmployeeJob, DimEmployeeJob>(failure, PopulateDimEmployeeJobFromEmployeeJob);
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<EmployeeJob>();
        }
    }
}
