﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;

namespace AbsenceSoft.Etl.Lib
{
    /// <summary>
    /// Dim Employee Necessity Synchronizer
    /// </summary>
    public class DimEmployerServiceOptionSynchronizer : BaseSynchronizer
    {
        /// <summary>
        /// Dimension Id
        /// </summary>
        public override int DimensionId { get { return Const.Dimensions.DIMEMSION_EMPLOYER_SERVICE_OPTIONS; } }
        /// <summary>
        /// On Synchronize Event
        /// </summary>
        /// <param name="syncManager"></param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<EmployerServiceOption, DimEmployerServiceOption>(syncManager, PopulateDimEmployerServiceOptionFromEmployerServiceOption, null);
        }
        /// <summary>
        /// Convert the EmployeeNecessity to DimEmployeeNecessity
        /// </summary>
        /// <param name="employeeNecessity"></param>
        /// <returns></returns>
        private DimEmployerServiceOption PopulateDimEmployerServiceOptionFromEmployerServiceOption(EmployerServiceOption employerServiceOption)
        {
            DimEmployerServiceOption dimEmployerServiceOption = null;
            dimEmployerServiceOption = new DimEmployerServiceOption(DataWarehouseConnectionString)
            {
                ExternalReferenceId = employerServiceOption.Id,
                ExternalCreatedDate = employerServiceOption.CreatedDate,
                ExternalModifiedDate = employerServiceOption.ModifiedDate,
                CreatedBy = employerServiceOption.CreatedById,
                ModifiedBy = employerServiceOption.ModifiedById,
                VersionIsCurrent = true,
                VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = 0,
                VersionPreviousId = 0,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                EmployerId = employerServiceOption.EmployerId,
                CustomerId = employerServiceOption.CustomerId,
                Key = employerServiceOption.Key,
                CustomerServiceOptionId = employerServiceOption.CusomerServiceOptionId
            };

            return dimEmployerServiceOption;
        }
        /// <summary>
        /// On Retry Event
        /// </summary>
        /// <param name="failure"></param>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<EmployerServiceOption, DimEmployerServiceOption>(failure, PopulateDimEmployerServiceOptionFromEmployerServiceOption);
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<EmployerServiceOption>();
        }
    }
}
