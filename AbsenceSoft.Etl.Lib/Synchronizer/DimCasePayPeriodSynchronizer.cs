﻿using AbsenceSoft.Common.Security;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Pay;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Etl.Lib
{
    public class DimCasePayPeriodSynchronizer : BaseSynchronizer
    {

        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_CASE_PAY_PERIOD; } }

        /// <summary>
        /// Initializes a new instance of the DimCasePolicySynchronizer class.
        /// </summary>
        public DimCasePayPeriodSynchronizer() : base() { }


        /// <summary>
        /// Does the synchronize work.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            // This base method mimics the "simple" perform sync method, however handles the
            //  scneario where we're syncing a CHILD class nested within an array, a BaseNonEntity,
            //  within our parent. The parent is the "controller" collection meaning it hosts the
            //  primary query collection, the modified date and entity id for sync coordination and is
            //  used by the sync coordinator.
            //
            // Secondly, this method takes some additional generic types to help it along the way
            //  where you're passinng in:
            //  - the PARENT entity type (TEntity)
            //  - the INTERMEDIARY type (this can be anything, if you don't need one, use bool OR byte for smallest memory footprint
            //      in our case we need the CaseSegment for the accommodation we're mapping, so we type that accordingly
            //  - the CHILD non-entity type we're actually going to map and save in the dimension table
            //  - the DIMENSION class type for that child's mapper to the dimension table itself
            PerformChildOfDimensionalSync<Case, PayInfo, PayPeriod, DimCasePayPeriod>(
                // Pass in our sync manager
                syncManager,
                // This lambda passes you the parent entity and then allows the dimension mapper to load history for it
                //  This is necessary because the history is rectified against the new child collection for the parent
                //  entity and any not present any longer are marked as deleted in the data warehouse. This is because
                //  our MongoDB entities in arrays don't have "soft" delete enabled, so we have to detect it and take
                //  action on it by loading the dimensional table history here.
                (@case, dimAccom) => dimAccom.LoadHistory(@case.Id),
                // Here we're given the parent entity (case in this instance) and the goal is to return a collection of
                //  Tuples containing the intermediary type AND our child object. Intermediary can be null, or any
                //  type remember, as long as it's not used downstream you can make it whatever.
                // We're giong to use the Accommodation Request as the intermediary.
                (@case) =>
                {
                    if (@case.Pay == null || @case.Pay.PayPeriods == null)
                    {
                        return null;
                    }
                    else
                    {
                        return @case.Pay.PayPeriods.Select(period => new Tuple<PayInfo, PayPeriod>(@case.Pay, period));
                    }                   
                }
               ,
                // This is our mapper delegate method
                PopulateDimPayPeriodFromCase,
                // We need to compare the new DimAccommodation record with the old if an old one exists.
                //  This ensures we're not duplicating work or inserting more  history than we need to, especially
                //  if there were no changes to that record since last time the case was updated
                (dimCasePayPeriod, accom) => dimCasePayPeriod.RecordHash != Crypto.Sha256Hash(accom.ToJSON()),
                null,
                true,
                // We can pass as many "additional" dimensions that we need to here, which will get called in order passed
                //  once the original dimension record version has been saved/inserted into the database and we have a
                //  populated dimension id for that. The populated/saved dimension class, original parent and child get
                //  passed to these methods for additional work that needs to be done
                (dimCasePayPeriod, @case, payPeriod, conn) =>
                {
                    // Check for pay period details and first delete if exists then save
                    if (payPeriod.Detail != null && payPeriod.Detail.Any())
                    {
                        var dimDetail = new DimCasePayPeriodDetail(DataWarehouseConnectionString)
                        {
                            DimCasePayPeriodId = dimCasePayPeriod.DimCasePayPeriodId
                        };
                        dimDetail.Delete(conn);
                        foreach (PayPeriodDetail detail in payPeriod.Detail)
                        {
                            var dimPayPeriodDetail = PopulateDimPayPeriodDetailFromPayPeriod(detail, @case.Id, dimCasePayPeriod.DimCasePayPeriodId);
                            dimPayPeriodDetail.Save(conn);
                        }
                    }
                });
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformChildOfDimensionalRetry<Case, PayInfo, PayPeriod, DimCasePayPeriod>(
                failure,
                (@case, dimAccom) => dimAccom.LoadHistory(@case.Id),
                @case =>
                {
                    if (@case.Pay == null || @case.Pay.PayPeriods == null)
                    {
                        return null;
                    }
                    else
                    {
                        return @case.Pay.PayPeriods.Select(period => new Tuple<PayInfo, PayPeriod>(@case.Pay, period));
                    }
                },
                PopulateDimPayPeriodFromCase,
                (dimCasePayPeriod, accom) => dimCasePayPeriod.RecordHash != Crypto.Sha256Hash(accom.ToJSON()),
                (dimCasePayPeriod, @case, payPeriod) =>
                {
                    // Check for pay period details and first delete if exists then save
                    if (payPeriod.Detail != null && payPeriod.Detail.Any())
                    {
                        var dimDetail = new DimCasePayPeriodDetail(DataWarehouseConnectionString)
                        {
                            DimCasePayPeriodId = dimCasePayPeriod.DimCasePayPeriodId
                        };
                        dimDetail.Delete();
                        foreach (PayPeriodDetail detail in payPeriod.Detail)
                        {
                            var dimPayPeriodDetail = PopulateDimPayPeriodDetailFromPayPeriod(detail, @case.Id, dimCasePayPeriod.DimCasePayPeriodId);
                            dimPayPeriodDetail.Save();
                        }
                    }
                });
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<Case>();
        }
        
        /// <summary>
        /// Populate PayPeriod from parent case 
        /// </summary>
        /// <param name="parentCase"></param>
        /// <param name="payInfo"></param>
        /// <param name="payPeriod"></param>
        /// <param name="payPeriodHistory"></param>
        /// <returns></returns>
        private DimCasePayPeriod PopulateDimPayPeriodFromCase(Case parentCase, PayInfo payInfo, PayPeriod payPeriod, IEnumerable<DimCasePayPeriod> payPeriodHistory)
        {
            var lastHistory = payPeriodHistory == null ? null : payPeriodHistory.OrderBy(r => r.DimVerId).LastOrDefault();
            var dimCasePayPeriod = new DimCasePayPeriod(DataWarehouseConnectionString)
            {
                ExternalReferenceId = payPeriod.Id.ToString(),
                ExternalCreatedDate = parentCase.CreatedDate,
                ExternalModifiedDate = parentCase.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = parentCase.IsDeleted ? Const.VER_SEQ_DELETED_VALUE : Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = lastHistory == null ? 0 : lastHistory.VersionRootId,
                VersionPreviousId = lastHistory == null ? 0 : lastHistory.DimVerId,
                VersionHistoryCount = lastHistory == null ? Const.DEFAULT_HISTORY_COUNT : lastHistory.VersionHistoryCount + 1,
                CaseId = parentCase.Id,
                StartDate = payPeriod.StartDate,
                EndDate = payPeriod.EndDate,
                LockedDate = payPeriod.LockedDate,
                LockedBy = payPeriod.LockedById,
                Offset = payPeriod.Offset,
                Total = payPeriod.Total,
                SubTotal = payPeriod.SubTotal,
                MaxWeeklyPayAmount = payPeriod.MaxWeeklyPayAmount,
                PayrollDateOverride = payPeriod.PayRollDateOverride,
                PayrollDate = payPeriod.PayrollDate.SystemValue,
                EndDateOverride = payPeriod.EndDateOverride,
                PayrollDateOverrideBy = payPeriod.PayrollDateOverrideById,
                EndDateOverrideBy = payPeriod.EndDateOverrideById,
                Status = (int)payPeriod.Status,
                IsDeleted = parentCase.IsDeleted,
                RecordHash = CryptoString.GetHash(payPeriod.ToJSON()).Hashed
            };

            //set audit data
            if (lastHistory != null)
            {
                dimCasePayPeriod.CreatedBy = lastHistory.CreatedBy;
            }
            else
            {
                dimCasePayPeriod.CreatedBy = parentCase.ModifiedById;
            }
            dimCasePayPeriod.ModifiedBy = parentCase.ModifiedById;

            return dimCasePayPeriod;
        }

        /// <summary>
        /// Populates the dim accommodation usage from accommodation usage.
        /// </summary>
        /// <param name="detail">The usage.</param>
        /// <param name="dimAccommodationId">The dim accommodation identifier.</param>
        /// <returns></returns>
        private DimCasePayPeriodDetail PopulateDimPayPeriodDetailFromPayPeriod(PayPeriodDetail detail, string dimCaseId, long dimCasePayPeriodId = 0)
        {
            DimCasePayPeriodDetail dimCasePayPeriodDetail = new DimCasePayPeriodDetail(DataWarehouseConnectionString)
            {
                StartDate = detail.StartDate.GetValueOrNullIfMinValue(),
                EndDate = detail.EndDate.GetValueOrNullIfMinValue(),
                ExternalReferenceId = detail.Id.ToString(),
                CaseId = dimCaseId,
                IsOffset = detail.IsOffset,
                MaxPaymentAmount = detail.MaxPaymentAmount,
                PayAmountOverrideValue = detail.PayAmount.OverrideValue,
                PayAmountSystemValue = detail.PayAmount.SystemValue,
                PaymentTierPercentage = detail.PaymentTierPercentage,
                PayPercentageOverrideBy = detail.PayPercentage.OverrideById,
                PayPercentageOverrideDate = detail.PayPercentage.OverrideDate.GetValueOrNullIfMinValue(),
                PayPercentageOverrideValue = detail.PayPercentage.OverrideValue,
                PayPercentageSystemValue = detail.PayPercentage.SystemValue,
                PolicyCode = detail.PolicyCode,
                PolicyName = detail.PolicyName,
                SalaryTotal = detail.SalaryTotal,
                DimCasePayPeriodId = dimCasePayPeriodId                
            };          
            return dimCasePayPeriodDetail;
        }

    }
}
