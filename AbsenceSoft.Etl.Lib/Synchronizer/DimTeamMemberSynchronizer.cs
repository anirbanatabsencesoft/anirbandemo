﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;

namespace AbsenceSoft.Etl.Lib.Synchronizer
{
    public class DimTeamMemberSynchronizer : BaseSynchronizer
    {
        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMEMSION_TEAM_MEMBER; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimTeamMemberSynchronizer"/> class.
        /// </summary>
        public DimTeamMemberSynchronizer() : base() { }

        /// <summary>
        /// Called when [syncronize].
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<TeamMember, DimTeamMember>(syncManager, PopulateDimTeamMemberFromTeamMember, null);
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<TeamMember>();
        }

        /// <summary>
        /// Populates the dim team member from team member.
        /// </summary>
        /// <param name="contact">The team member.</param>
        /// <returns></returns>
        private DimTeamMember PopulateDimTeamMemberFromTeamMember(TeamMember teamMember)
        {
            DimTeamMember dimTeamMember = new DimTeamMember(DataWarehouseConnectionString)
            {
                ExternalReferenceId = teamMember.Id,
                ExternalCreatedDate = teamMember.CreatedDate,
                ExternalModifiedDate = teamMember.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = 0,
                VersionPreviousId = 0,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                CustomerId = teamMember.CustomerId,
                CreatedBy = teamMember.CreatedById,
                ModifiedBy = teamMember.ModifiedById,
                IsDeleted = teamMember.IsDeleted,
                UserId = teamMember.UserId,
                TeamId = teamMember.TeamId,
                IsTeamLead = teamMember.IsTeamLead,
                AssignedById = teamMember.AssignedById,
                AssignedOn = teamMember.AssignedOn,
                ModifiedById = teamMember.ModifiedById,
                ModifiedOn = teamMember.ModifiedOn
            };

            return dimTeamMember;
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<TeamMember, DimTeamMember>(failure, PopulateDimTeamMemberFromTeamMember);
        }
    }
}
