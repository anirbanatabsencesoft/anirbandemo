﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Etl.Lib
{
    public class DimUsersSynchronizer : BaseSynchronizer
    {
        // Cache properties.
        private Dictionary<string, string> _employees = null;
        private Dictionary<string, string> _roles = null;

        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_USERS; } }

        /// <summary>
        /// The _admin
        /// </summary>
        private Role _admin;

        /// <summary>
        /// Initializes a new instance of the <see cref="DimUsersSynchronizer"/> class.
        /// </summary>
        public DimUsersSynchronizer() : base()
        {
            _admin = Role.SystemAdministrator;
        }

        /// <summary>
        /// Called when [syncronize].
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            _employees = new Dictionary<string, string>();
            _roles = new Dictionary<string, string>();
            BatchReadCompleted += (results) =>
            {
                _employees = Employee.Repository.Collection
                    .Find(User.Query.In(c => c.Id, _employees.Keys))
                    .Select(e => new { Id = e.Id, EmployeeNumber = e.EmployeeNumber })
                    .ToDictionary(r => r.Id, r => r.EmployeeNumber);
                _roles = Role.Repository.Collection
                    .Find(Role.Query.In(r => r.Id, _roles.Keys))
                    .Select(r => new { Id = r.Id, Name = r.Name })
                    .ToDictionary(r => r.Id, r => r.Name);
            };
            PerformDimensionalSync<User, DimUsers>(syncManager, PopulateDimUserFromUser, user =>
            {
                if (!string.IsNullOrWhiteSpace(user.EmployeeId))
                    _employees[user.EmployeeId] = null;
                if (user.Roles != null && user.Roles.Any())
                    user.Roles.ForEach(r => _roles[r] = null);
            });
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<User>();
        }

        /// <summary>
        /// Populates the dim user from user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        private DimUsers PopulateDimUserFromUser(User user)
        {
            DimUsers dimUser = new DimUsers(DataWarehouseConnectionString)
            {
                ExternalReferenceId = user.Id,
                ExternalCreatedDate = user.CreatedDate,
                ExternalModifiedDate = user.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = 0,
                VersionPreviousId = 0,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                CustomerId = user.CustomerId,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                FailedLoginAttempts = user.FailedLoginAttempts,
                LastFailedLoginAttempt = user.LastFailedLoginAttempt,
                Locked = user.IsLocked,
                LockedDate = user.LockedDate,
                Disabled = user.IsDisabled,
                DisabledDate = user.DisabledDate,
                LastActivityDate = user.LastActivityDate,
                IsDeleted = user.IsDeleted,
                UserFlags = (int)user.UserFlags,
                UserType = (int)user.UserType,
                IsSelfServiceUser = user.UserType == UserType.SelfService,
                DisplayName = user.DisplayName,
                IsPortalUser = user.UserType == UserType.Portal,
                MustChangePassword = user.MustChangePassword,
                UserStatus = user.IsLocked ? 3 : user.IsDisabled ? 2 : 1,
                EmployeeRefId = user.EmployeeId
            };
            if (!string.IsNullOrWhiteSpace(user.EmployeeId))
            {

                if (!string.IsNullOrWhiteSpace(user.EmployeeId))
                {
                    string eeNumber = null;
                    if (_employees == null || !_employees.TryGetValue(user.EmployeeId, out eeNumber))
                    {
                        var employee = Employee.Repository.GetById(user.EmployeeId);
                        if (employee != null)
                            eeNumber = employee.EmployeeNumber;
                    }
                    dimUser.EmployeeNumber = eeNumber;
                }
            }
            if (user.Employers != null && user.Employers.Any(e => e.IsActive))
                dimUser.EmployerIds = user.Employers.Where(e => e.IsActive).Select(e => e.EmployerId).Distinct().ToArray();
            
            if (user.Roles != null && user.Roles.Any())
            {
                dimUser.RoleIds = user.Roles.ToArray();
                if (user.Roles.Contains(_admin.Id))
                    dimUser.RoleName = _admin.Name;
                else
                {
                    string roleName;
                    foreach (var id in user.Roles)
                        if (_roles.TryGetValue(id, out roleName))
                        {
                            dimUser.RoleName = roleName;
                            break;
                        }
                }
            }

            dimUser.IsDeleted = user.IsDeleted;
            if (user.IsDeleted)
                dimUser.VersionSequence = Const.VER_SEQ_DELETED_VALUE;

            //set audit data
            dimUser.CreatedBy = user.CreatedById;
            dimUser.ModifiedBy = user.ModifiedById;

            return dimUser;
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<User, DimUsers>(failure, PopulateDimUserFromUser);
        }
    }
}

