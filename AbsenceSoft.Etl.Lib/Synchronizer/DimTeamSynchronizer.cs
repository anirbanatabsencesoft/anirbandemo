﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;

namespace AbsenceSoft.Etl.Lib.Synchronizer
{
    public class DimTeamSynchronizer : BaseSynchronizer
    {
        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMEMSION_TEAM; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimTeamSynchronizer"/> class.
        /// </summary>
        public DimTeamSynchronizer() : base() { }

        /// <summary>
        /// Called when [syncronize].
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<Team, DimTeam>(syncManager, PopulateDimTeamFromTeam, null);
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<Team>();
        }

        /// <summary>
        /// Populates the dim contact from contact.
        /// </summary>
        /// <param name="contact">The contact.</param>
        /// <returns></returns>
        private DimTeam PopulateDimTeamFromTeam(Team team)
        {
            DimTeam dimTeam = new DimTeam(DataWarehouseConnectionString)
            {
                ExternalReferenceId = team.Id,
                ExternalCreatedDate = team.CreatedDate,
                ExternalModifiedDate = team.ModifiedDate,
                CreatedBy = team.CreatedById,
                ModifiedBy = team.ModifiedById,
                VersionIsCurrent = true,
                VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = 0,
                VersionPreviousId = 0,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                Name = team.Name,
                CustomerId = team.CustomerId,
                IsDeleted = team.IsDeleted,
                Description = team.Description
            };

            return dimTeam;
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<Team, DimTeam>(failure, PopulateDimTeamFromTeam);
        }
    }
}
