﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Common.Security;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Communications;

namespace AbsenceSoft.Etl.Lib
{
    public class DimAttachmentSynchronizer : BaseSynchronizer
    {
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_ATTACHMENT; } }
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<Attachment, DimAttachment>(failure, PopulateDimAttachmentFromAttachment);
        }

        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<Attachment, DimAttachment>(syncManager, PopulateDimAttachmentFromAttachment, null);
        }

        private DimAttachment PopulateDimAttachmentFromAttachment(Attachment attachment)
        {            
            if (attachment != null)
            {
                DimAttachment dimAttachment = new DimAttachment(DataWarehouseConnectionString)
                {
                    ExternalReferenceId = attachment.Id,
                    ExternalCreatedDate = attachment.CreatedDate,
                    ExternalModifiedDate = attachment.ModifiedDate,
                    VersionIsCurrent = true,
                    VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                    VersionRootId = 0,
                    VersionPreviousId = 0,
                    VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                    EmployeeId = attachment.EmployeeId,
                    CustomerId = attachment.CustomerId,
                    EmployerId = attachment.EmployerId,
                    CaseId = attachment.CaseId,
                    ContentLength = attachment.ContentLength,
                    ContentType = attachment.ContentType,
                    AttachmentType = (int)attachment.AttachmentType,
                    CreatedByName = attachment.CreatedByName,
                    Description = attachment.Description,
                    FileId = attachment.FileId,
                    FileName = attachment.FileName,
                    Public = attachment.Public,
                    Checked= attachment.Metadata.GetRawValue<bool?>("Checked"),
                    IsDeleted = attachment.IsDeleted,
                    CreatedBy = attachment.CreatedById,
                    ModifiedBy = attachment.ModifiedById,
                    TimeSampleDate = attachment.CreatedDate,
                    TimeTotalMinutes = 0,
                    EmployeeNumber = attachment.EmployeeNumber,
                    DimVariableScheduleTimeId = 0
                };


                return dimAttachment;
            }
            else
                return null;
        }
    }
}
