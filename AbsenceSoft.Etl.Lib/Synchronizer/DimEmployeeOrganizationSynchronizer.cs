﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;

namespace AbsenceSoft.Etl.Lib
{
    public sealed class DimEmployeeOrganizationSynchronizer : BaseSynchronizer
    {
        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_EMPLOYEE_ORGANIZATION; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployeeOrganizationSynchronizer"/> class.
        /// </summary>
        public DimEmployeeOrganizationSynchronizer() : base() { }

        /// <summary>
        /// Called when [syncronize].
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<EmployeeOrganization, DimEmployeeOrganization>(syncManager, PopulateDimEmployeeOrganizationFromOrganization, null);
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<EmployeeOrganization>();
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<EmployeeOrganization, DimEmployeeOrganization>(failure, PopulateDimEmployeeOrganizationFromOrganization);
        }

        /// <summary>
        /// Populates the dim employee organization from organization.
        /// </summary>
        /// <param name="employeeOrganization">The employee organization.</param>
        /// <returns></returns>
        private DimEmployeeOrganization PopulateDimEmployeeOrganizationFromOrganization(EmployeeOrganization employeeOrganization)
        {
            DimEmployeeOrganization dimEmployeeOrganization = null;
            dimEmployeeOrganization = new DimEmployeeOrganization(DataWarehouseConnectionString)
            {
                ExternalReferenceId = employeeOrganization.Id, 
                CustomerId = employeeOrganization.CustomerId,
                EmployerId = employeeOrganization.EmployerId,
                ExternalCreatedDate = employeeOrganization.CreatedDate,
                ExternalModifiedDate = employeeOrganization.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = 0,
                VersionPreviousId = 0,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                Name = employeeOrganization.Name,
                Code = employeeOrganization.Code,
                Path =  employeeOrganization.Path.Path,
                TypeCode  = employeeOrganization.TypeCode,
                EmployeeNumber = employeeOrganization.EmployeeNumber
            };
            if (employeeOrganization.Organization != null)
                dimEmployeeOrganization.Description = employeeOrganization.Organization.Description;

            dimEmployeeOrganization.IsDeleted = employeeOrganization.IsDeleted;
            if (employeeOrganization.IsDeleted)
                dimEmployeeOrganization.VersionSequence = Const.VER_SEQ_DELETED_VALUE;

            //set audit data
            dimEmployeeOrganization.CreatedBy = employeeOrganization.CreatedById;
            dimEmployeeOrganization.ModifiedBy = employeeOrganization.ModifiedById;

            return dimEmployeeOrganization;
        }
    }
}
