﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;

namespace AbsenceSoft.Etl.Lib.Synchronizer
{
    public class DimRoleSynchronizer : BaseSynchronizer
    {
        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMEMSION_ROLE; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimRoleSynchronizer"/> class.
        /// </summary>
        public DimRoleSynchronizer() : base() { }

        /// <summary>
        /// Called when [syncronize].
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<Role, DimRole>(syncManager, PopulateDimRoleFromRole, null);
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<Role>();
        }

        /// <summary>
        /// Populates the dim contact from contact.
        /// </summary>
        /// <param name="contact">The contact.</param>
        /// <returns></returns>
        private DimRole PopulateDimRoleFromRole(Role role)
        {
            DimRole dimRole = new DimRole(DataWarehouseConnectionString)
            {
                ExternalReferenceId = role.Id,
                ExternalCreatedDate = role.CreatedDate,
                ExternalModifiedDate = role.ModifiedDate,
                CreatedBy = role.CreatedById,
                ModifiedBy = role.ModifiedById,
                VersionIsCurrent = true,
                VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = 0,
                VersionPreviousId = 0,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                Name = role.Name,
                CustomerId = role.CustomerId,
                RoleType = (int?)role.Type,
                IsDeleted = role.IsDeleted,
                Permissions = role.Permissions?.ToArray()
            };

            return dimRole;
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<Role, DimRole>(failure, PopulateDimRoleFromRole);
        }
    }
}
