﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Common.Security;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Enums;
using System.Web;

namespace AbsenceSoft.Etl.Lib
{
    public sealed class DimCommunicationSynchronizer : BaseSynchronizer
    {
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_COMMUNICATION; } }

        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<Communication, DimCommunication>(failure, PopulateDimCommunicationFromCommunication, null, 
                (dimCommunication, comm) =>
                {
                    SyncAdditionalDimensions(dimCommunication, comm);
                }
                );
        }

        protected override void OnSyncronize(SyncManager syncManager)
        {
            PerformDimensionalSync<Communication, DimCommunication>(syncManager, PopulateDimCommunicationFromCommunication, null, null, SyncAdditionalDimensions);
        }

        private void SyncAdditionalDimensions(DimCommunication dimCommunication, Communication comm, Npgsql.NpgsqlConnection conn=null)
        {
            //checking for the CCRecipient and Recipients                    
            if ((comm.Recipients != null && comm.Recipients.Any()) || (comm.CCRecipients != null && comm.CCRecipients.Any()))
            {
                var dimRecps = new DimCommunicationRecipient(DataWarehouseConnectionString)
                {
                    DimCommunicationId = dimCommunication.DimCommunicationId
                };
                if (conn != null)
                {
                    dimRecps.Delete(conn);
                }
                else
                {
                    dimRecps.Delete();
                }
                PopulateRecipients(dimCommunication.DimCommunicationId, 0, comm.Recipients, conn);
                PopulateRecipients(dimCommunication.DimCommunicationId, 1, comm.CCRecipients, conn);
            }
        }

        private void PopulateRecipients(long dimCommunicationId, int recipientType, List<Contact> recipients, Npgsql.NpgsqlConnection conn = null)
        {
            foreach (Contact recp in recipients)
            {
                //0 for To Recipient type
                var dimCommRecp = PopulateDimCommunicationRecipientFromCommunication(recp, dimCommunicationId, recipientType);
                if (conn != null)
                {
                    dimCommRecp.Save(conn);
                }
                else
                {
                    dimCommRecp.Save();
                }
            }
        }
        private DimCommunication PopulateDimCommunicationFromCommunication(Communication oComm)
        {
            if (oComm != null)
            {
                DimCommunication dimComm = new DimCommunication(DataWarehouseConnectionString)
                {
                    ExternalReferenceId = oComm.Id,
                    ExternalCreatedDate = oComm.CreatedDate,
                    ExternalModifiedDate = oComm.ModifiedDate,
                    VersionIsCurrent = true,
                    VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                    VersionRootId = 0,
                    VersionPreviousId = 0,
                    VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                    EmployeeId = oComm.EmployeeId,
                    CustomerId = oComm.CustomerId,
                    EmployerId = oComm.EmployerId,
                    CaseId = oComm.CaseId,
                    AttachmentId = oComm.AttachmentId,
                    Body = RemoveHtmlTags(oComm.Body),
                    CommunicationType = (int)oComm.CommunicationType,
                    CreatedByEmail = oComm.CreatedByEmail,
                    CreatedByName = oComm.CreatedByName,
                    EmailRepliesType = GetEmailRepliesType(oComm.EmailRepliesType),
                    IsDraft = oComm.IsDraft,
                    Name = oComm.Name,
                    Public = oComm.Public,
                    FirstSendDate = oComm.SentDates.FirstOrDefault().GetValueOrNullIfMinValue(),
                    LastSendDate = oComm.SentDates.LastOrDefault().GetValueOrNullIfMinValue(),
                    Subject = RemoveHtmlTags(oComm.Subject),
                    Template = oComm.Template,
                    ToDoItemId = oComm.ToDoItemId,
                    IsDeleted = oComm.IsDeleted,
                    CreatedBy = oComm.CreatedById,
                    ModifiedBy = oComm.ModifiedById
                };

                return dimComm;
            }
            else
            {
                return null;
            }
        }
        private DimCommunicationRecipient PopulateDimCommunicationRecipientFromCommunication(Contact recp, long dimCommunicationId, int recipientType)
        {
            if (recp != null)
            {
                DimCommunicationRecipient dimCommRecp = new DimCommunicationRecipient(DataWarehouseConnectionString)
                {
                    ExternalReferenceId = recp.Id.ToString(),
                    DimCommunicationId = dimCommunicationId,
                    RecipientType = recipientType,
                    AddressId = recp.Address.Id.ToString(),
                    AddressAddress1 = recp.Address.Address1,
                    AddressCity = recp.Address.City,
                    AddressCountry = recp.Address.Country,
                    AddressPostalCode = recp.Address.PostalCode,
                    AddressState = recp.Address.State,
                    AltEmail = recp.AltEmail,
                    Email = recp.Email,
                    Fax = recp.Fax,
                    Title = recp.Title,
                    FirstName = recp.FirstName,
                    MiddleName = recp.MiddleName,
                    LastName = recp.LastName,
                    CompanyName = recp.CompanyName,
                    ContactPersonName = recp.ContactPersonName,
                    DateOfBirth = recp.DateOfBirth,
                    HomePhone = recp.HomePhone,
                    CellPhone = recp.CellPhone,
                    IsPrimary = recp.IsPrimary
                };

                return dimCommRecp;
            }
            else
            {
                return null;
            }
        }

        private int? GetEmailRepliesType(EmailReplies? emailReplies)
        {
            if (emailReplies != null)
            {
                return (int)emailReplies;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Removes the html tags
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private string RemoveHtmlTags(string input)
        {
            if(input != null)
            {
                var str = System.Text.RegularExpressions.Regex.Replace(input, "<.*?>", String.Empty);
                str = str.Replace("\n\n\n", Environment.NewLine);
                return HttpUtility.HtmlDecode(str);
            }
            else
            {
                return input;
            }
            
        }
    }
}
