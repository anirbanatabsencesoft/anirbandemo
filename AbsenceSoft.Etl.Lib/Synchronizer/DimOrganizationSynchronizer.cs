﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Etl.Lib
{
    public sealed class DimOrganizationSynchronizer : BaseSynchronizer
    {
        // Cache properties.
        private Dictionary<string, User> _users = null;

        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_ORGANIZATION; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimOrganizationSynchronizer"/> class.
        /// </summary>
        public DimOrganizationSynchronizer() : base() { }

        /// <summary>
        /// Called when [syncronize].
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            _users = new Dictionary<string, User>();
            BatchReadCompleted += (results) => _users = User.Repository.Collection.Find(User.Query.In(c => c.Id, _users.Keys)).ToDictionary(r => r.Id);
            PerformDimensionalSync<Organization, DimOrganization>(syncManager, PopulateDimOrganizationFromOrganization, org =>
            {
                _users[org.CreatedById] = null;
            });
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<Organization>();
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformDimensionalRetry<Organization, DimOrganization>(failure, PopulateDimOrganizationFromOrganization);
        }

        /// <summary>
        /// Populates the dim organization from organization.
        /// </summary>
        /// <param name="organization">The organization.</param>
        /// <returns></returns>
        private DimOrganization PopulateDimOrganizationFromOrganization(Organization organization)
        {
            var dimOrganization = new DimOrganization(DataWarehouseConnectionString)
            {
                ExternalReferenceId = organization.Id,
                CustomerId = organization.CustomerId,
                EmployerId = organization.EmployerId,
                ExternalCreatedDate = organization.CreatedDate,
                ExternalModifiedDate = organization.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = 0,
                VersionPreviousId = 0,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                Name = organization.Name,
                Code = organization.Code,
                Description = organization.Description,
                Path =  organization.Path.Path,
                TypeCode = organization.TypeCode,
                TypeName = organization.TypeName,
                SicCode = organization.SicCode,
                NaicsCode =organization.NaicsCode,
                OrgAddress1 = organization.Address.Address1,
                OrgAddress2 = organization.Address.Address2,
                OrgCity = organization.Address.City,
                OrgState = organization.Address.State,
                OrgCountry = organization.Address.Country,
                OrgPostalCode = organization.Address.PostalCode
            };

            try
            {
                User user = null;
                if (_users == null || !_users.TryGetValue(organization.CreatedById, out user))
                    user = organization.CreatedBy;
                if (user != null)
                {
                    dimOrganization.OrgCreatedByEmail = user.Email;
                    dimOrganization.OrgCreatedByFirstName = user.FirstName;
                    dimOrganization.OrgCreatedByLastName = user.LastName;
                }
            }
            catch (Exception ex)
            {
                LogError("Error setting created by details", ex.ToString(), organization.Id);
            }

            dimOrganization.IsDeleted = organization.IsDeleted;
            if (organization.IsDeleted)
                dimOrganization.VersionSequence = Const.VER_SEQ_DELETED_VALUE;

            //set audit data
            dimOrganization.CreatedBy = organization.CreatedById;
            dimOrganization.ModifiedBy = organization.ModifiedById;

            return dimOrganization;
        }
    }
}
