﻿using AbsenceSoft.Common.Security;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using System;
using System.Collections.Generic;
using System.Linq;


namespace AbsenceSoft.Etl.Lib
{
    public sealed class DimCaseSegmentSynchronizer : BaseSynchronizer
    {
        /// <summary>
        /// Gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_CASE_SEGMENT; } }

        /// <summary>
        /// Initializes a new instance of the DimCaseSegmentSynchronizer class.
        /// </summary>
        public DimCaseSegmentSynchronizer() : base() { }


        /// <summary>
        /// Does the synchronize work for Case Policies.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void OnSyncronize(SyncManager syncManager)
        {
            // This base method mimics the "simple" perform sync method, however handles the
            //  scneario where we're syncing a CHILD class nested within an array, a BaseNonEntity,
            //  within our parent. The parent is the "controller" collection meaning it hosts the
            //  primary query collection, the modified date and entity id for sync coordination and is
            //  used by the sync coordinator.
            //
            // Secondly, this method takes some additional generic types to help it along the way
            //  where you're passinng in:
            //  - the PARENT entity type (TEntity)
            //  - the INTERMEDIARY type (this can be anything, if you don't need one, use bool OR byte for smallest memory footprint
            //      in our case we need the CaseSegment for the applied policy we're mapping, so we type that accordingly
            //  - the CHILD non-entity type we're actually going to map and save in the dimension table
            //  - the DIMENSION class type for that child's mapper to the dimension table itself
            PerformChildOfDimensionalSync<Case, bool, CaseSegment, DimCaseSegment>(
                // Pass in our sync manager
                syncManager,
                // This lambda passes you the parent entity and then allows the dimension mapper to load history for it
                //  This is necessary because the history is rectified against the new child collection for the parent
                //  entity and any not present any longer are marked as deleted in the data warehouse. This is because
                //  our MongoDB entities in arrays don't have "soft" delete enabled, so we have to detect it and take
                //  action on it by loading the dimensional table history here.
                (@case, dimSegment) => dimSegment.LoadHistory(@case.Id),
                // Here we're given the parent entity (case in this instance) and the goal is to return a collection of
                //  Tuples containing the intermediary type AND our child object. Intermediary can be null, or any
                //  type remember, as long as it's not used downstream you can make it whatever.
                @case =>
                {
                    // Get our initialized tuple list
                    List<Tuple<bool, CaseSegment>> children = new List<Tuple<bool, CaseSegment>>();
                    children.AddRange(@case.Segments.Select(ap => new Tuple<bool, CaseSegment>(true, ap)));
                    // Return our populated or empty children collection of applied policy and segment tuples
                    return children;
                },
                // This is our mapper delegate method
                PopulateDimCaseSegment,
                // We need to compare the new DimCaseSegment record with the old if an old one exists.
                //  This ensures we're not duplicating work or inserting more  history than we need to, especially
                //  if there were no changes to that record since last time the case was updated
                (dimSegment, ap) => dimSegment != null && dimSegment.RecordHash != Crypto.Sha256Hash(ap.ToJSON()),
                null,
                true,
                // We can pass as many "additional" dimensions that we need to here, which will get called in order passed
                //  once the original dimension record version has been saved/inserted into the database and we have a
                //  populated dimension id for that. The populated/saved dimension class, original parent and child get
                //  passed to these methods for additional work that needs to be done
                (dimSegment, @case, ap, conn) =>
                {
                    //check for policy usage and first delete if exists then save
                    if (ap.UserRequests != null && ap.UserRequests.Any())
                    {
                        //delete
                        var dimCSRequest = new DimCaseSegment(DataWarehouseConnectionString) { DimCaseSegmentId = dimSegment.DimCaseSegmentId };
                        dimCSRequest.CaseIntermittentTimeRequest.DimCaseSegmentId = dimSegment.DimCaseSegmentId;
                        dimCSRequest.CaseIntermittentTimeRequest.Delete(conn);

                        //save
                        foreach (IntermittentTimeRequest request in ap.UserRequests)
                            PopulateDimRequests(request, dimSegment).Save(conn);
                    }
                });
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected override void OnRetry(SyncFailure failure)
        {
            PerformChildOfDimensionalRetry<Case, bool, CaseSegment, DimCaseSegment>(
                failure,
                (@case, dimSegment) => dimSegment.LoadHistory(@case.Id),
                @case =>
                {
                    // Get our initialized tuple list
                    List<Tuple<bool, CaseSegment>> children = new List<Tuple<bool, CaseSegment>>();
                    children.AddRange(@case.Segments.Select(ap => new Tuple<bool, CaseSegment>(true, ap)));
                    // Return our populated or empty children collection of applied policy and segment tuples
                    return children;
                },
                PopulateDimCaseSegment,
                (dimSegment, ap) => dimSegment != null && dimSegment.RecordHash != Crypto.Sha256Hash(ap.ToJSON()),
                (dimSegment, @case, ap) =>
                {
                    //check for policy usage and first delete if exists then save
                    if (ap.UserRequests != null && ap.UserRequests.Any())
                    {
                        //delete
                        var dimCSRequest = new DimCaseSegment(DataWarehouseConnectionString) { DimCaseSegmentId = dimSegment.DimCaseSegmentId };
                        dimCSRequest.CaseIntermittentTimeRequest.DimCaseSegmentId = dimSegment.DimCaseSegmentId;
                        dimCSRequest.CaseIntermittentTimeRequest.Delete();

                        //save
                        foreach (IntermittentTimeRequest request in ap.UserRequests)
                            PopulateDimRequests(request, dimSegment).Save();
                    }
                });
        }

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected override void UpdateSourceCount(SyncManager syncManager)
        {
            syncManager.UpdateSyncManagerSourceCount<Case>();
        }

        

        /// <summary>
        /// Populates the dim case policy from case and segments.
        /// </summary>
        /// <param name="parentCase"></param>
        /// <param name="segment"></param>
        /// <param name="policy"></param>
        /// <param name="verRootId"></param>
        /// <param name="verPrevId"></param>
        /// <returns></returns>
        private DimCaseSegment PopulateDimCaseSegment(Case parentCase, bool intermediary, CaseSegment segment, IEnumerable<DimCaseSegment> history)
        {
            var lastHistory = history == null || !history.Any() ? null : history.OrderBy(h => h.DimVerId).LastOrDefault();
            var dimCS = new DimCaseSegment(DataWarehouseConnectionString)
            {
                ExternalReferenceId = segment.Id.ToString(),
                ExternalCreatedDate = segment.CreatedDate,
                ExternalModifiedDate = parentCase.ModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = parentCase.IsDeleted ? Const.VER_SEQ_DELETED_VALUE : Const.VER_SEQ_NEWEST_VALUE,
                VersionRootId = lastHistory == null ? 0 : lastHistory.VersionRootId,
                VersionPreviousId = lastHistory == null ? 0 : lastHistory.DimVerId,
                VersionHistoryCount = lastHistory == null ? Const.DEFAULT_HISTORY_COUNT : lastHistory.VersionHistoryCount + 1,

                //set the case parameters
                ExternalCreatedBy = string.IsNullOrWhiteSpace(parentCase.ModifiedById) ? parentCase.CreatedById : parentCase.ModifiedById,
                ExternalModifiedBy = parentCase.ModifiedById,
                CaseId = parentCase.Id,
                EmployeeId = parentCase.Employee.Id,
                CustomerId = parentCase.CustomerId,
                EmployerId = parentCase.EmployerId,
                
                StartDate = segment.StartDate,
                EndDate = segment.EndDate,
                CaseType = (int)segment.Type,
                CaseStatus = (int)segment.Status
            };

            return dimCS;
        }

        /// <summary>
        /// Returns a new class for policy usage
        /// </summary>
        /// <param name="usage"></param>
        /// <param name="policy"></param>
        /// <returns></returns>
        private DimIntermittentTimeRequest PopulateDimRequests(IntermittentTimeRequest request, DimCaseSegment segment)
        {
            DimIntermittentTimeRequest dimCPU = new DimIntermittentTimeRequest(DataWarehouseConnectionString)
            {
                //set values here
                ExternalReferenceId = request.Id.ToString(),
                ExternalCreatedDate = segment.ExternalCreatedDate,
                ExternalModifiedDate = segment.ExternalModifiedDate,
                VersionIsCurrent = true,
                VersionSequence = segment.VersionSequence,
                VersionRootId = segment.VersionRootId,
                VersionPreviousId = segment.VersionPreviousId,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                DimCaseSegmentId = segment.DimCaseSegmentId,
                ExternalCreatedBy = segment.ExternalCreatedBy,
                ExternalModifiedBy = segment.ExternalModifiedBy,
                CaseId = segment.CaseId,
                EmployeeId = segment.EmployeeId,
                CustomerId = segment.CustomerId,
                EmployerId = segment.EmployerId,
                
                StartTimeOfLeave = request.StartTimeForLeave?.ToString(),
                EndTimeOfLeave = request.EndTimeForLeave?.ToString(),
                TotalMinutes = request.TotalMinutes,
                Notes = request.Notes,
                RequestDate = request.RequestDate
            };

            return dimCPU;
        }
    }
}
