﻿using AbsenceSoft.Data;
using AbsenceSoft.Etl.Data;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AbsenceSoft.Etl.Lib
{
    public abstract class BaseSynchronizer
    {
        /// <summary>
        /// The configuration
        /// </summary>
        private Config config = new Config();

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseSynchronizer"/> class.
        /// </summary>
        public BaseSynchronizer() : base() { }

        /// <summary>
        /// When implemented in a parent class, gets the dimension identifier for this syncronizer.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public abstract int DimensionId { get; }

        /// <summary>
        /// Gets or sets the task synchronize manager.
        /// </summary>
        /// <value>
        /// The task synchronize manager.
        /// </value>
        public SyncManager TaskSyncManager { get; set; }

        /// <summary>
        /// Gets the size of the batch.
        /// </summary>
        /// <value>
        /// The size of the batch.
        /// </value>
        protected int BatchSize { get { return TaskSyncManager == null ? 0 : TaskSyncManager.BatchSize; } }

        /// <summary>
        /// Gets or sets the task client.
        /// </summary>
        /// <value>
        /// The task client.
        /// </value>
        public TaskClient TaskClient { get; set; }

        /// <summary>
        /// Gets the data warehouse connection string.
        /// </summary>
        /// <value>
        /// The data warehouse connection string.
        /// </value>
        protected string DataWarehouseConnectionString { get { return config.DataWarehouseConnectionString; } }

        /// <summary>
        /// Gets the syncronization connection string.
        /// </summary>
        /// <value>
        /// The syncronization connection string.
        /// </value>
        protected string SyncronizationConnectionString { get { return config.SynchronizerConnectionString; } }

        #region Syncronize

        /// <summary>
        /// Syncronizes this instance.
        /// </summary>
        public void Syncronize()
        {
            SyncManager syncManager = TaskSyncManager;

            try
            {
                if (syncManager.HasError)
                {
                    LogError(syncManager);
                    return;
                }

                if (syncManager.SyncManagerId != 0)
                {
                    LogInfo("doSync", "ENTITY-START", syncManager.ToString());
                    OnSyncronize(syncManager);
                    if (syncManager.HasError)
                    {
                        LogError(syncManager);
                    }
                    
                    //log
                    LogInfo("doSync", "ENTITY-END", syncManager.ToString());

                    LogInfo("doSync", "UPDATE-SOURCE-CT-START", syncManager.ToString());

                    //Update source count if there is a requirement of update
                    if (BsonObjectHandler.UpdateList.Count > 0)
                    {
                        UpdateSourceCount(syncManager);
                    }

                    if (syncManager.HasError)
                    {
                        LogError(syncManager);
                    }

                    LogInfo("doSync", "UPDATE-SOURCE-CT-END", syncManager.ToString());

                    //update object list
                    var strUpdate = BsonObjectHandler.UpdateObject();
                    LogInfo("BsonObjectUpdate", "Update", $"BsonObjectHandler for {syncManager.DimensionId}. Message: {strUpdate}");

                    //archive
                    BsonObjectHandler.Archive();
                }
            }
            catch (Exception ex)
            {
                SyncLogError logError = new SyncLogError(SyncronizationConnectionString)
                {
                    DimensionId = syncManager.DimensionId,
                    MsgSource = "{processname=" + TaskClient.ProcessName + ",processid=" + TaskClient.ProcessId + "}" + "{" + "doSync}",
                    ErrorMessage = ex.Message,
                    StackTrace = ex.ToString()
                };
                logError.SaveSyncLogError();
                if (logError.HasError)
                    throw;
            }
        }


        /// <summary>
        /// Called when [syncronize].
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected abstract void OnSyncronize(SyncManager syncManager);

        /// <summary>
        /// Occurs when reading results from the database is complete. This is called once after the full result
        /// set is obtained into memory for the batch to be processed.
        /// </summary>
        protected event Action<IEnumerable> BatchReadCompleted;

        /// <summary>
        /// Performs the single OR multi-dimensional synchronize operation for a single entity of type <typeparamref name="TEntity" /> which
        /// is mapped by calling the action that returns a base dim type of <typeparamref name="TDim" />.
        /// </summary>
        /// <typeparam name="TEntity">The type of the parent entity (top level collection) that data is being pulled from and
        /// maintains the last modified date being pulled via batch logic.</typeparam>
        /// <typeparam name="TDim">The type of the dimension that is being synced.</typeparam>
        /// <param name="syncManager">The synchronize manager.</param>
        /// <param name="fetch">The fetch.</param>
        /// <param name="onResult">The on result.</param>
        /// <param name="bsonCollectionName"></param>
        /// <param name="extraSyncDimensions">The extra synchronize dimension delegate actions.</param>
        /// <exception cref="System.ArgumentNullException">syncManager
        /// or
        /// fetch</exception>
        /// <remarks>
        /// When extra sync dimension delegates
        /// are passed in, those are executed against the entity and resulting dimension for additional sync opeations and errors should bubble
        /// up using the parent dim's HasError and error properties accordingly.
        /// </remarks>
        protected virtual void PerformDimensionalSync<TEntity, TDim>(SyncManager syncManager
            , Func<TEntity, TDim> fetch
            , Action<TEntity> onResult
            , string bsonCollectionName = ""
            , params Action<TDim, TEntity, Npgsql.NpgsqlConnection>[] extraSyncDimensions)
            where TEntity : BaseEntity<TEntity>, new()
            where TDim : DimBase, new()
        {
            if (syncManager == null)
            {
                throw new ArgumentNullException("syncManager");
            }
            if (fetch == null)
            {
                throw new ArgumentNullException("fetch");
            }

            // Read the entities from MongoDB that we need to sync. This method automatically filters and sorts them, as well
            //  as limits the batch size, so all we need to worry about is iterating the list.
            var entities = syncManager.FetchEntityUpdateList(BatchSize, onResult, bsonCollectionName);

            // Log our start and determine if we should continue.
            LogInfo("OnSyncronize", "ENTITY-UPDATE-BEGIN", "{count=" + entities.Count + "}");
            if (entities.Count == 0)
            {
                // Nope, end it here and bail out of the method
                LogInfo("OnSyncronize", "ENTITY-UPDATE-END", "{nothing to do}");
                return;
            }
            LogInfo("OnSyncronize", "ENTITY-UPDATE-BEGIN", "{firstsyncid=" + entities[0].Id + "}{firstsyncdate=" + entities[0].ModifiedDate + "}");
            LogInfo("OnSyncronize", "ENTITY-UPDATE-BEGIN", "{lastsyncid=" + entities[entities.Count - 1].Id + "}{lastsyncdate=" + entities[entities.Count - 1].ModifiedDate + "}");
            // Notify any event handlers that we have results.
            BatchReadCompleted?.Invoke(entities);

            int updated = 0;
            int errors = 0;
            List<DimensionObjectAndEntity<TDim, TEntity>> dims = new List<DimensionObjectAndEntity<TDim, TEntity>>();
            
            foreach (var entity in entities)
            {
                // Use a delegate for our return value because there's some complex stuff and we
                //  don't want a bunch of duplicate code floating around.
                var returnValue = new Action<bool, DimBase>((error, dimObj) =>
                {
                        // If it's an error, return the context error result
                        if (error)
                    {
                        LogError(dimObj, entity.Id);
                        syncManager.RecordFailure(entity, dimObj);
                        Interlocked.Increment(ref errors);
                        return;
                    }
                    else
                    {
                        Interlocked.Increment(ref updated);
                    }
                });

                // Map the dim object
                TDim dimObject = null;
                try
                {
                    dimObject = fetch(entity);
                }
                catch (Exception ex)
                {
                    dimObject = dimObject ?? new TDim();
                    dimObject.HasError = true;
                    dimObject.ErrorMessage = ex.Message;
                    dimObject.StackTrace = ex.ToString();
                    returnValue(true, dimObject);
                    return;
                }

                // Check if the employee dim has an error
                if (dimObject != null && dimObject.HasError)
                {
                    returnValue(true, dimObject);
                }


                if (dimObject != null)
                {
                    // Add records to list for saving in single transaction
                    dims.Add(new DimensionObjectAndEntity<TDim, TEntity> { Dimension = dimObject, Entity = entity });

                    // It was a success, return our returnValue function result with error = false.
                    returnValue(false, dimObject);
                }
            }

            //Update the database now in a single transaction
            //save now
            PerformSingleTransactionOperationForSingleDim(dims, syncManager, extraSyncDimensionsAction: extraSyncDimensions);

        }

        /// <summary>
        /// Performs the single OR multi-dimensional synchronize operation for all children of a single entity of type <typeparamref name="TEntity" /> which
        /// is mapped by calling the action that returns a base dim type of <typeparamref name="TDim" />.
        /// This method specifically segments each child with an intermediary parent container which is not the entity itself nor is it the target dimension
        /// class we're after, just an interesting intermediary parent that contains the child and is needed in other methods to map correctly.
        /// </summary>
        /// <typeparam name="TEntity">The type of the parent entity (top level collection) that data is being pulled from and
        /// maintains the last modified date being pulled via batch logic.</typeparam>
        /// <typeparam name="TItermediary">The type of the itermediary. This can be anything, a number, a string another entity, class,
        /// non-entity, GUID, etc. Make this whatever your mapper needs to do it's job correctly.</typeparam>
        /// <typeparam name="TChild">The type of the child entity.</typeparam>
        /// <typeparam name="TDim">The type of the dimension that is being synced.</typeparam>
        /// <param name="syncManager">The synchronization manager.</param>
        /// <param name="loadHistory">The load history delegate that fetches the historical dimension collection using some method.</param>
        /// <param name="getChildren">The get children delegate that returns the collection of children to use as the actual final dimension targets.
        /// If the value returned from getChildren is null or empty, this action will clear any prior children and mark them as deleted.</param>
        /// <param name="fetch">The fetch deletgate that maps the proeprties and returns a dimension object based on the entity. Also an annoying
        /// trend word the character Gretchen on "Mean Girls" constantly tries to make 'happen' but fails.</param>
        /// <param name="hasChanged">The has changed.</param>
        /// <param name="onResult">The on result.</param>
        /// <param name="useChildCollectionNameForEntity"></param>
        /// <param name="extraSyncDimensions">The extra synchronize dimension delegate actions.</param>
        /// <exception cref="System.ArgumentNullException">syncManager
        /// or
        /// getChildren
        /// or
        /// fetch</exception>
        /// <remarks>
        /// When extra sync dimension delegates
        /// are passed in, those are executed against the entity and resulting dimension for additional sync opeations and errors should bubble
        /// up using the parent dim's HasError and error properties accordingly.
        /// </remarks>
        protected virtual void PerformChildOfDimensionalSync<TEntity, TItermediary, TChild, TDim>(
            SyncManager syncManager,
            Func<TEntity, TDim, IEnumerable<TDim>> loadHistory,
            Func<TEntity, IEnumerable<Tuple<TItermediary, TChild>>> getChildren,
            Func<TEntity, TItermediary, TChild, IEnumerable<TDim>, TDim> fetch,
            Func<TDim, TChild, bool> hasChanged,
            Action<TEntity> onResult,
            bool useChildCollectionNameForEntity = true,
            params Action<TDim, TEntity, TChild, Npgsql.NpgsqlConnection>[] extraSyncDimensions)
            where TEntity : BaseEntity<TEntity>, new()
            where TChild : BaseNonEntity, new()
            where TDim : DimBase, new()
        {
            if (syncManager == null)
                throw new ArgumentNullException("syncManager");
            if (getChildren == null)
                throw new ArgumentNullException("getChildren");
            if (fetch == null)
                throw new ArgumentNullException("fetch");

            // Read the entities from MongoDB that we need to sync. This method automatically filters and sorts them, as well
            //  as limits the batch size, so all we need to worry about is iterating the list.

            List<TEntity> entities = null;

            if (useChildCollectionNameForEntity)
            {
                entities = syncManager.FetchEntityUpdateList(BatchSize, onResult, collectionName: typeof(TChild).Name);
            }
            else
            {
                entities = syncManager.FetchEntityUpdateList(BatchSize, onResult);
            }

            // Log our start and determine if we should continue.
            LogInfo("OnSyncronize", "ENTITY-UPDATE-BEGIN", "{count=" + entities.Count + "}");
            if (entities.Count == 0)
            {
                // Nope, end it here and bail out of the method
                LogInfo("OnSyncronize", "ENTITY-UPDATE-END", "{nothing to do}");
                return;
            }
            LogInfo("OnSyncronize", "ENTITY-UPDATE-BEGIN", "{firstsyncid=" + entities[0].Id + "}{firstsyncdate=" + entities[0].ModifiedDate + "}");
            LogInfo("OnSyncronize", "ENTITY-UPDATE-BEGIN", "{lastsyncid=" + entities[entities.Count - 1].Id + "}{lastsyncdate=" + entities[entities.Count - 1].ModifiedDate + "}");
            // Notify any event handlers that we have results.
            BatchReadCompleted?.Invoke(entities);

            int updated = 0;
            int errors = 0;

            //declare the list of dimention object
            List<DimensionObjectEntityAndChild<TDim, TEntity, TChild>> dims = new List<DimensionObjectEntityAndChild<TDim, TEntity, TChild>>();
            List<TDim> deletableDims = new List<TDim>();


            foreach (var entity in entities)
            {
                // Use a delegate for our return value because there's some complex stuff and we
                //  don't want a bunch of duplicate code floating around.
                var error = new Action<DimBase>(dimObj =>
                {
                        // If it's an error, return the context error result
                        LogError(dimObj, entity.Id);
                    syncManager.RecordFailure(entity, dimObj);
                    Interlocked.Increment(ref errors);
                });

                // Load the historical dimension records given the method passed in, this function will probably
                //  simply filter based on the parent entity's external ref id, but we can't just assume.
                // If the loadHistory parameter is null, we just assume there is no history to load for this type and make this always empty.
                var history = loadHistory == null ? new List<TDim>(0) : loadHistory(entity, new TDim() { ConnectionString = DataWarehouseConnectionString });

                // We need to load the children, aren't they sooooo cute! This should not  be null, it's a required thingy
                var children = getChildren(entity);
                //history return null in case of an exception so taking a precaution.
                if (history != null)
                {
                    if (children == null || !children.Any())
                    {
                        // So here's the deal, there are no children for this parent, but we have history of there being some,
                        //  this means that they were removed from the entity (deleted) and this is the only way we can detect that
                        //  since BaseNonEntity types don't use soft deletes like actual entities do (they're just sub-entities in an array).
                        if (history.Any())
                        {
                            history.ForEach(p => deletableDims.Add(p));
                        }

                        Interlocked.Add(ref updated, history.Count());

                        // Add a dummy pointer to use for updating the sync manager at least for this item.
                        dims.Add(new DimensionObjectEntityAndChild<TDim, TEntity, TChild>()
                        {
                            Dimension = null,
                            Child = null,
                            Entity = entity
                        });

                    }

                    // These are accommodations that no longer  exist in the case, but there are still some accommodations on the case so we
                    //  still need to do further processing.

                    var toDelete = history.Where(h => children != null && !children.Any(a => a.Item2 != null && UniqueHistoryPredicate(h, a.Item2))).ToList();
                    if (toDelete.Any())
                    {
                        toDelete.ForEach(p => deletableDims.Add(p));
                        Interlocked.Add(ref updated, toDelete.Count);
                    }
                }

                // Loop through each of our children to map them to dimensions and do the actual sync part (the meat)
                if (children != null)
                {
                    foreach (var child in children.Where(c => c.Item2 != null))
                    {
                        // Only care about this specific non-entity id (GUID) we should only add to dimension if there is an actual update.
                        //  This ensures dual records are not recorded during first time load AND we don't duplicate versions unecessarily.
                        // Also, if the hasChanged delegate is null, then that means we're only comparing by ID, if ID exists then don't update it.                        
                        if (history != null && history.Any(h => h.VersionIsCurrent && UniqueHistoryPredicate(h, child.Item2) && (hasChanged == null || !hasChanged(h, child.Item2))))
                        {
                            continue;
                        }
                        // Map the dim object
                        TDim dimObject = null;
                        try
                        {
                            dimObject = fetch(entity, child.Item1, child.Item2, history);
                        }
                        catch (Exception ex)
                        {
                            dimObject = dimObject ?? new TDim();
                            dimObject.HasError = true;
                            dimObject.ErrorMessage = ex.Message;
                            dimObject.StackTrace = ex.ToString();
                            error(dimObject);
                            return;
                        }

                        // Check if the employee dim has an error
                        if (dimObject.HasError || dimObject == null)
                        {
                            error(dimObject);
                        }

                        if (dimObject != null)
                        {
                            // Add object to list to save in single transaction
                            dims.Add(new DimensionObjectEntityAndChild<TDim, TEntity, TChild>() { Dimension = dimObject, Entity = entity, Child = child.Item2 });

                            // Record the fact we updated one of the children to cover the total updates made.
                            Interlocked.Increment(ref updated);
                        }
                    }
                }
            }

            //save now
            PerformSingleTransactionOperationForMultiDim(dims, syncManager, deletableDims, extraSyncDimensionsAction: extraSyncDimensions);

        }

        /// <summary>
        /// Defines the predicate that allows condition required for unique comparision from its history
        /// </summary>
        /// <param name="dim"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        protected  virtual bool UniqueHistoryPredicate(DimBase dim, BaseNonEntity entity) 
        {
            if (dim != null && entity != null)
            {
                return dim.ExternalReferenceId == entity.Id.ToString();
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Perform a single transaction operation for all enetities provided
        /// </summary>
        /// <typeparam name="TDim"></typeparam>
        /// <param name="dimensionObjects"></param>
        /// <param name="syncManager"></param>
        private void PerformSingleTransactionOperationForMultiDim<TDim, TEntity, TChild>(
            List<DimensionObjectEntityAndChild<TDim, TEntity, TChild>> dimensionObjects
            , SyncManager syncManager
            , List<TDim> deletableObjects = null
            , Action<TDim, TEntity, TChild, Npgsql.NpgsqlConnection>[] extraSyncDimensionsAction = null
            )
            where TDim : DimBase, new()
            where TEntity : BaseEntity<TEntity>, new()
            where TChild : BaseNonEntity, new()
        {
            //Update the database now in a single transaction
            if (dimensionObjects != null && dimensionObjects.Count > 0)
            {
                using (var conn = new Npgsql.NpgsqlConnection(DataWarehouseConnectionString))
                {
                    conn.Open();
                    using (var tran = conn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted))
                    {
                        try
                        {
                            //first delete all objects if present
                            if (deletableObjects != null && deletableObjects.Count > 0)
                            {
                                foreach (var delObj in deletableObjects)
                                {
                                    DeleteMissingChild(delObj, conn);
                                }
                            }

                            //save objects now
                            foreach (var obj in dimensionObjects.Where(d => d.Dimension != null))
                            {
                                UpdateVersion(obj.Dimension, conn);
                                obj.Dimension.Save(conn);

                                // Extra Sync
                                if (extraSyncDimensionsAction != null && extraSyncDimensionsAction.Length > 0)
                                {
                                    extraSyncDimensionsAction.ForEach(s => s(obj.Dimension, obj.Entity, obj.Child, conn));
                                }
                            }

                            // Sync Manager Update
                            var lastDim = dimensionObjects.Last();
                            var lastModifiedDate = lastDim.Dimension?.ExternalModifiedDate ?? lastDim.Entity.ModifiedDate;
                            UpdateSynManager(syncManager, lastModifiedDate, dimensionObjects.Count, conn);
                            
                            //commit transaction now after all success
                            tran.Commit();
                        }
                        catch (Exception ex)
                        {
                            LogError(ex.Message, ex.ToString());
                            tran.Rollback();
                            throw;
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Perform a single transaction for single dimension
        /// </summary>
        /// <typeparam name="TDim"></typeparam>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="dimensionObjects"></param>
        /// <param name="syncManager"></param>
        /// <param name="deletableObjects"></param>
        /// <param name="extraSyncDimensionsAction"></param>
        private void PerformSingleTransactionOperationForSingleDim<TDim, TEntity>(
            List<DimensionObjectAndEntity<TDim, TEntity>> dimensionObjects
            , SyncManager syncManager
            , List<TDim> deletableObjects = null
            , Action<TDim, TEntity, Npgsql.NpgsqlConnection>[] extraSyncDimensionsAction = null
            )
            where TDim : DimBase, new()
            where TEntity : BaseEntity<TEntity>, new()
        {
            //Update the database now in a single transaction
            if (dimensionObjects != null && dimensionObjects.Count > 0)
            {
                //order by date first
                dimensionObjects = dimensionObjects
                                    .Where(p => p != null)
                                    .Where(p => p.Dimension != null)
                                    .ToList();

                using (var conn = new Npgsql.NpgsqlConnection(DataWarehouseConnectionString))
                {
                    conn.Open();
                    using (var tran = conn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted))
                    {
                        try
                        {
                            //first delete all objects if present
                            if (deletableObjects != null && deletableObjects.Count > 0)
                            {
                                foreach (var delObj in deletableObjects)
                                    DeleteMissingChild<TDim>(delObj, conn);
                            }

                            //save objects now
                            foreach (var obj in dimensionObjects)
                            {
                                UpdateVersion(obj.Dimension, conn);
                                obj.Dimension.Save(conn);

                                #region Extra Sync
                                if (extraSyncDimensionsAction != null && extraSyncDimensionsAction.Length > 0)
                                {
                                    extraSyncDimensionsAction.ForEach(s => s(obj.Dimension, obj.Entity, conn));
                                }

                                #endregion
                            }

                            #region Sync Manager Update
                                UpdateSynManager(syncManager, dimensionObjects.Last().Dimension.ExternalModifiedDate, dimensionObjects.Count, conn);      
                            #endregion

                            //commit transaction now after all success
                            tran.Commit();

                        }
                        catch (Exception ex)
                        {
                            LogError(ex.Message, ex.ToString());
                            tran.Rollback();
                            throw;
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }

                }
            }
        }



        /// <summary>
        /// Update the sync records
        /// </summary>
        /// <param name="syncManager"></param>
        /// <param name="lmdt"></param>
        /// <param name="count"></param>
        /// <param name="conn"></param>
        private void UpdateSynManager(SyncManager syncManager, DateTime lmdt, int count, Npgsql.NpgsqlConnection conn)
        {
            try
            {
                // Update the last sync info
                syncManager.UpdateSyncUpdate(conn, lmdt);
                if (syncManager.HasError)
                    LogError(syncManager);
            }
            finally
            {
                LogInfo("OnSyncronize", "ENTITY-UPDATE-END", "{countupdated=" + count + "}");
            }
        }


        /// <summary>
        /// Update the version on same transaction
        /// </summary>
        /// <typeparam name="TDim"></typeparam>
        /// <param name="dimObject"></param>
        /// <param name="conn"></param>
        /// <returns></returns>
        private long UpdateVersion<TDim>(TDim dimObject, Npgsql.NpgsqlConnection conn) where TDim : DimBase, new()
        {
            var dimVer = dimObject.NewVersion(conn);
            dimVer.VersionSequence = Const.VER_SEQ_DELETED_VALUE;
            if (dimVer.HasError)
                dimVer.ThrowException();

            return Const.VER_SEQ_NEWEST_VALUE;
        }


        /// <summary>
        /// Deletes the missing child from the data warehouse (or at least marks it as deleted).
        /// </summary>
        /// <typeparam name="TDim">The type of the dimension class.</typeparam>
        /// <param name="lastHistory">The last history dimension to mark as deleted.</param>
        private void DeleteMissingChild<TDim>(TDim lastHistory) where TDim : DimBase, new()
        {
            var dimDeleted = lastHistory.Clone();
            dimDeleted.IsDeleted = true;
            dimDeleted.VersionSequence = Const.VER_SEQ_DELETED_VALUE;

            var dimVer = dimDeleted.NewVersion();
            dimVer.VersionSequence = Const.VER_SEQ_DELETED_VALUE;
            if (dimVer.HasError)
                dimVer.ThrowException();
            if (dimDeleted.HasError)
                dimDeleted.ThrowException();

            dimDeleted.DimVerId = dimVer.DimVerId;

            if (dimDeleted.HasError)
                dimDeleted.ThrowException();

            dimDeleted.Save();
            if (dimDeleted.HasError)
                // error in saving, throw exception
                dimDeleted.ThrowException();

            // change the version info of the previous recent row IF this is an update
            lastHistory.VersionIsCurrent = false;
        }

        private void DeleteMissingChild<TDim>(TDim lastHistory, Npgsql.NpgsqlConnection conn) where TDim : DimBase, new()
        {
            var dimDeleted = lastHistory.Clone();
            dimDeleted.IsDeleted = true;
            dimDeleted.VersionSequence = Const.VER_SEQ_DELETED_VALUE;
            dimDeleted.VersionIsCurrent = false;


            if (dimDeleted.HasError)
                dimDeleted.ThrowException();

            dimDeleted.DimVerId = UpdateVersion<TDim>(dimDeleted, conn);

            if (dimDeleted.HasError)
                dimDeleted.ThrowException();

            dimDeleted.Save(conn);
            if (dimDeleted.HasError)
                // error in saving, throw exception
                dimDeleted.ThrowException();

            // change the version info of the previous recent row IF this is an update
            lastHistory.VersionIsCurrent = false;
        }

        #endregion Syncronize

        /// <summary>
        /// Updates the source count for the synchronize manager.
        /// </summary>
        /// <param name="syncManager">The synchronize manager.</param>
        protected virtual void UpdateSourceCount(SyncManager syncManager) { }

        #region Retry Failed

        /// <summary>
        /// Syncronizes this instance.
        /// </summary>
        public void Retry(SyncFailure failure)
        {
            SyncManager syncManager = TaskSyncManager;

            try
            {
                if (syncManager.HasError)
                {
                    LogError(syncManager);
                    return;
                }

                if (syncManager.SyncManagerId != 0)
                {
                    LogInfo("doSync", "RETRY-START", syncManager.ToString());
                    OnRetry(failure);
                    if (syncManager.HasError)
                        LogError(syncManager);
                    LogInfo("doSync", "RETRY-END", syncManager.ToString());
                }
            }
            catch (Exception ex)
            {
                SyncLogError logError = new SyncLogError(SyncronizationConnectionString)
                {
                    DimensionId = syncManager.DimensionId,
                    MsgSource = "{processname=" + TaskClient.ProcessName + ",processid=" + TaskClient.ProcessId + "}" + "{" + "retry}",
                    ErrorMessage = ex.Message,
                    StackTrace = ex.ToString()
                };
                logError.SaveSyncLogError();
                if (logError.HasError)
                    throw;
            }
        }

        /// <summary>
        /// Called when [retry].
        /// </summary>
        /// <param name="failure">The failure.</param>
        protected abstract void OnRetry(SyncFailure failure);

        /// <summary>
        /// Retries the single OR multi-dimensional synchronize operation for a single entity of type <typeparamref name="TEntity" /> which
        /// is mapped by calling the action that returns a base dim type of <typeparamref name="TDim" />.
        /// </summary>
        /// <typeparam name="TEntity">The type of the parent entity (top level collection) that data is being pulled from and
        /// maintains the last modified date being pulled via batch logic.</typeparam>
        /// <typeparam name="TDim">The type of the dimension that is being synced.</typeparam>
        /// <param name="failure">The failure.</param>
        /// <param name="fetch">The fetch.</param>
        /// <param name="extraSyncDimensions">The extra synchronize dimension delegate actions.</param>
        /// <exception cref="System.ArgumentNullException">syncManager
        /// or
        /// fetch</exception>
        /// <exception cref="System.ArgumentException">Failure record must have a SyncFailureId;failure</exception>
        /// <remarks>
        /// When extra sync dimension delegates
        /// are passed in, those are executed against the entity and resulting dimension for additional sync opeations and errors should bubble
        /// up using the parent dim's HasError and error properties accordingly.
        /// </remarks>
        protected virtual void PerformDimensionalRetry<TEntity, TDim>(SyncFailure failure, Func<TEntity, TDim> fetch, params Action<TDim, TEntity>[] extraSyncDimensions)
            where TEntity : BaseEntity<TEntity>, new()
            where TDim : DimBase, new()
        {
            if (failure == null)
                throw new ArgumentNullException("failure");
            if (!failure.SyncFailureId.HasValue)
                throw new ArgumentException("Failure record must have a SyncFailureId", "failure");
            if (string.IsNullOrWhiteSpace(failure.EntityJson))
                throw new ArgumentException("Failure record must have the entity JSON", "failure");
            if (fetch == null)
                throw new ArgumentNullException("fetch");

            TEntity entity;
            TEntity latest;

            try
            {
                // Deserialize the entity state from the failure record's entity JSON to retry
                entity = failure.EntityJson.DeserializeJson<TEntity>();
                // Read the entity from MongoDB that we need to compare
                latest = BaseEntity<TEntity>.Repository.Collection.FindOneById(failure.ExternalReferenceId);
                if (latest == null)
                    // If we don't have a latest, then someone hard-deleted it in MongoDB, just go with it anyway.
                    latest = entity;
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.ToString());
                failure.RecordFailure(TaskSyncManager, ex.Message, ex.ToString());
                LogInfo("OnRetry", "ENTITY-RETRY-FAIL", failure.ToString());
                return;
            }
            LogInfo("OnRetry", "ENTITY-RETRY-BEGIN", "{id=" + entity.Id + ",retrymdt=" + failure.ExternalModifiedDate + ",latestmdt=" + latest.ModifiedDate.ToUnixDate() + "}");

            // If this is an old record and what's in MongoDB is already newer, then skip it, mark this as expired.
            if (failure.ExternalModifiedDate < latest.ModifiedDate.ToUnixDate())
            {
                failure.RecordExpired();
                LogInfo("OnRetry", "ENTITY-RETRY-END", failure.ToString());
                return;
            }

            // We still need to call this in case there's any setup the sync'er needs in order
            //  to property map things or fill in blanks, build cache, etc.
            BatchReadCompleted?.Invoke(new List<TEntity>(1) { entity });

            TDim dimObject = null;
            try
            {
                dimObject = fetch(entity);
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.ToString());
                failure.RecordFailure(TaskSyncManager, ex.Message, ex.ToString());
                LogInfo("OnRetry", "ENTITY-RETRY-END", failure.ToString());
                return;
            }

            // Create a new version record for this dim object
            var dimVer = dimObject.NewVersion();

            // Check if the version dim has an error
            if (dimVer.HasError)
            {
                failure.RecordFailure(TaskSyncManager, dimVer.ErrorMessage, dimVer.StackTrace);
                LogInfo("OnRetry", "ENTITY-RETRY-END", failure.ToString());
                return;
            }

            // Check if the employee dim has an error
            if (dimObject.HasError)
            {
                failure.RecordFailure(TaskSyncManager, dimObject.ErrorMessage, dimObject.StackTrace);
                LogInfo("OnRetry", "ENTITY-RETRY-END", failure.ToString());
                return;
            }

            // Save our employee dimension record
            dimObject.Save();

            // Ensure we note and log our error if the save failed
            if (dimObject.HasError)
            {
                LogError(dimObject);
                failure.RecordFailure(TaskSyncManager, dimObject.ErrorMessage, dimObject.StackTrace);
                LogInfo("OnRetry", "ENTITY-RETRY-END", failure.ToString());
                return;
            }

            // Sync the extra dimensions, one at a time using this dimension and entity pair as parameters
            if (extraSyncDimensions != null && extraSyncDimensions.Any())
            {
                extraSyncDimensions.ForEach(s => s(dimObject, entity));
                if (dimObject.HasError)
                {
                    LogError(dimObject);
                    failure.RecordFailure(TaskSyncManager, dimObject.ErrorMessage, dimObject.StackTrace);
                    LogInfo("OnRetry", "ENTITY-RETRY-END", failure.ToString());
                    return;
                }
            }

            // It was a success
            failure.RecordSuccess();

            LogInfo("OnRetry", "ENTITY-RETRY-END", failure.ToString());
        }

        /// <summary>
        /// Retries the single OR multi-dimensional synchronize operation for all children of a single entity of type <typeparamref name="TEntity" /> which
        /// is mapped by calling the action that returns a base dim type of <typeparamref name="TDim" />.
        /// This method specifically segments each child with an intermediary parent container which is not the entity itself nor is it the target dimension
        /// class we're after, just an interesting intermediary parent that contains the child and is needed in other methods to map correctly.
        /// </summary>
        /// <typeparam name="TEntity">The type of the parent entity (top level collection) that data is being pulled from and
        /// maintains the last modified date being pulled via batch logic.</typeparam>
        /// <typeparam name="TItermediary">The type of the itermediary. This can be anything, a number, a string another entity, class,
        /// non-entity, GUID, etc. Make this whatever your mapper needs to do it's job correctly.</typeparam>
        /// <typeparam name="TChild">The type of the child entity.</typeparam>
        /// <typeparam name="TDim">The type of the dimension that is being synced.</typeparam>
        /// <param name="failure">The failure.</param>
        /// <param name="loadHistory">The load history delegate that fetches the historical dimension collection using some method.</param>
        /// <param name="getChildren">The get children delegate that returns the collection of children to use as the actual final dimension targets.
        /// If the value returned from getChildren is null or empty, this action will clear any prior children and mark them as deleted.</param>
        /// <param name="fetch">The fetch deletgate that maps the proeprties and returns a dimension object based on the entity. Also an annoying
        /// trend word the character Gretchen on "Mean Girls" constantly tries to make 'happen' but fails.</param>
        /// <param name="hasChanged">The has changed.</param>
        /// <param name="extraSyncDimensions">The extra synchronize dimension delegate actions.</param>
        /// <exception cref="System.ArgumentNullException">syncManager
        /// or
        /// getChildren
        /// or
        /// fetch</exception>
        /// <exception cref="System.ArgumentException">Failure record must have a SyncFailureId;failure</exception>
        /// <remarks>
        /// When extra sync dimension delegates
        /// are passed in, those are executed against the entity and resulting dimension for additional sync opeations and errors should bubble
        /// up using the parent dim's HasError and error properties accordingly.
        /// </remarks>
        protected virtual void PerformChildOfDimensionalRetry<TEntity, TItermediary, TChild, TDim>(
            SyncFailure failure,
            Func<TEntity, TDim, IEnumerable<TDim>> loadHistory,
            Func<TEntity, IEnumerable<Tuple<TItermediary, TChild>>> getChildren,
            Func<TEntity, TItermediary, TChild, IEnumerable<TDim>, TDim> fetch,
            Func<TDim, TChild, bool> hasChanged,
            params Action<TDim, TEntity, TChild>[] extraSyncDimensions)
            where TEntity : BaseEntity<TEntity>, new()
            where TChild : BaseNonEntity, new()
            where TDim : DimBase, new()
        {
            if (failure == null)
                throw new ArgumentNullException("syncManager");
            if (!failure.SyncFailureId.HasValue)
                throw new ArgumentException("Failure record must have a SyncFailureId", "failure");
            if (string.IsNullOrWhiteSpace(failure.EntityJson))
                throw new ArgumentException("Failure record must have the entity JSON", "failure");
            if (getChildren == null)
                throw new ArgumentNullException("getChildren");
            if (fetch == null)
                throw new ArgumentNullException("fetch");

            TEntity entity;
            TEntity latest;

            try
            {
                // Deserialize the entity state from the failure record's entity JSON to retry
                entity = failure.EntityJson.DeserializeJson<TEntity>();
                // Read the entity from MongoDB that we need to compare
                latest = BaseEntity<TEntity>.Repository.Collection.FindOneById(failure.ExternalReferenceId);
                if (latest == null)
                    // If we don't have a latest, then someone hard-deleted it in MongoDB, just go with it anyway.
                    latest = entity;
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.ToString());
                failure.RecordFailure(TaskSyncManager, ex.Message, ex.ToString());
                LogInfo("OnRetry", "ENTITY-RETRY-FAIL", failure.ToString());
                return;
            }
            LogInfo("OnRetry", "ENTITY-RETRY-BEGIN", "{id=" + entity.Id + ",retrymdt=" + failure.ExternalModifiedDate + ",latestmdt=" + latest.ModifiedDate.ToUnixDate() + "}");

            // If this is an old record and what's in MongoDB is already newer, then skip it, mark this as expired.
            if (failure.ExternalModifiedDate < latest.ModifiedDate.ToUnixDate())
            {
                failure.RecordExpired();
                LogInfo("OnRetry", "ENTITY-RETRY-END", failure.ToString());
                return;
            }

            // We still need to call this in case there's any setup the sync'er needs in order
            //  to property map things or fill in blanks, build cache, etc.
            BatchReadCompleted?.Invoke(new List<TEntity>(1) { entity });

            // Load the historical dimension records given the method passed in, this function will probably
            //  simply filter based on the parent entity's external ref id, but we can't just assume.
            // If the loadHistory parameter is null, we just assume there is no history to load for this type and make this always empty.
            var history = loadHistory == null ? new List<TDim>(0) : loadHistory(entity, new TDim() { ConnectionString = DataWarehouseConnectionString });

            // We need to load the children, aren't they sooooo cute! This should not  be null, it's a required thingy
            var children = getChildren(entity);

            if (children == null || !children.Any())
            {
                // So here's the deal, there are no children for this parent, but we have history of there being some,
                //  this means that they were removed from the entity (deleted) and this is the only way we can detect that
                //  since BaseNonEntity types don't use soft deletes like actual entities do (they're just sub-entities in an array).
                if (history.Any())
                    history.ForEach(DeleteMissingChild);

                // This is the end of line, we had no children to map so we've marked any DW records as deleted
                //  and now it's time to bail out of the method.
                failure.RecordSuccess();
                LogInfo("OnRetry", "ENTITY-RETRY-END", failure.ToString());
                return;
            }

            // These are accommodations that no longer  exist in the case, but there are still some accommodations on the case so we
            //  still need to do further processing.
            var toDelete = history.Where(h => !children.Any(a => a.Item2 != null && a.Item2.Id.ToString() == h.ExternalReferenceId)).ToList();
            if (toDelete.Any())
                toDelete.ForEach(DeleteMissingChild);

            // Loop through each of our children to map them to dimensions and do the actual sync part (the meat)
            foreach (var child in children.Where(c => c.Item2 != null))
            {
                // Only care about this specific non-entity id (GUID) we should only add to dimension if there is an actual update.
                //  This ensures dual records are not recorded during first time load AND we don't duplicate versions unecessarily.
                // Also, if the hasChanged delegate is null, then that means we're only comparing by ID, if ID exists then don't update it.
                if (history.Any(h => h.VersionIsCurrent && h.ExternalReferenceId == child.Item2.Id.ToString() && (hasChanged == null || !hasChanged(h, child.Item2))))
                    continue;

                // Map the dim object
                TDim dimObject = null;
                try
                {
                    dimObject = fetch(entity, child.Item1, child.Item2, history);
                }
                catch (Exception ex)
                {
                    dimObject = dimObject ?? new TDim();
                    dimObject.HasError = true;
                    dimObject.ErrorMessage = ex.Message;
                    dimObject.StackTrace = ex.ToString();
                    LogError(dimObject);
                    failure.RecordFailure(TaskSyncManager, ex.Message, ex.ToString());
                    return;
                }

                // Create a new version record for this dim object
                var dimVer = dimObject.NewVersion();

                // Check if the version dim has an error
                if (dimVer.HasError)
                {
                    failure.RecordFailure(TaskSyncManager, dimVer.ErrorMessage, dimVer.StackTrace);
                    LogInfo("OnRetry", "ENTITY-RETRY-END", failure.ToString());
                    return;
                }


                // Check if the employee dim has an error
                if (dimObject.HasError)
                {
                    LogError(dimObject);
                    failure.RecordFailure(TaskSyncManager, dimObject.ErrorMessage, dimObject.StackTrace);
                    LogInfo("OnRetry", "ENTITY-RETRY-END", failure.ToString());
                    return;
                }


                // Save our employee dimension record
                dimObject.Save();

                // Ensure we note and log our error if the save failed
                if (dimObject.HasError)
                {
                    LogError(dimObject);
                    failure.RecordFailure(TaskSyncManager, dimObject.ErrorMessage, dimObject.StackTrace);
                    LogInfo("OnRetry", "ENTITY-RETRY-END", failure.ToString());
                    return;
                }

                // Sync the extra dimensions, one at a time using this dimension and entity pair as parameters
                if (extraSyncDimensions != null && extraSyncDimensions.Any())
                {
                    extraSyncDimensions.ForEach(s => s(dimObject, entity, child.Item2));
                    if (dimObject.HasError)
                    {
                        LogError(dimObject);
                        failure.RecordFailure(TaskSyncManager, dimObject.ErrorMessage, dimObject.StackTrace);
                        LogInfo("OnRetry", "ENTITY-RETRY-END", failure.ToString());
                        return;
                    }
                }
            }

            // It was a success
            failure.RecordSuccess();

            LogInfo("OnRetry", "ENTITY-RETRY-END", failure.ToString());
        }

        #endregion Retry Failed

        #region Logging

        /// <summary>
        /// Logs the information.
        /// </summary>
        /// <param name="msgSource">The MSG source.</param>
        /// <param name="msgAction">The MSG action.</param>
        /// <param name="msgText">The MSG text.</param>
        protected void LogInfo(string msgSource, string msgAction, string msgText)
        {
            if (config.LogDebugInfo)
            {
                try
                {
                    SyncLogInfo logInfo = new SyncLogInfo(SyncronizationConnectionString) { MsgSource = msgSource, MsgAction = msgAction, MsgText = msgText, DimensionId = DimensionId };
                    logInfo.SaveSyncLogInfo();
                }
                catch (Exception ex)
                {
                    try
                    {
                        string info = string.Format("msgSource={0}, msgAction={1}, msgText={2}", msgSource, msgAction, msgText);
                        LogError("Error logging info: " + info, ex.ToString());
                    }
                    catch { }
                }
            }
        }

        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <param name="entityId">The entity identifier.</param>
        public void LogError(BaseBase instance, string entityId = null)
        {
            if (instance.HasError)
            {
                LogError(instance.ErrorMessage, instance.StackTrace, entityId);

                //update change stream log
                if (!string.IsNullOrWhiteSpace(entityId))
                {
                    BsonObjectHandler.MarkChangeStreamLogAsError(entityId, instance.ErrorMessage, instance.StackTrace);
                }
            }
        }

        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="stackTrace">The stack trace.</param>
        protected virtual void LogError(string message, string stackTrace)
        {
            new SyncLogError(SyncronizationConnectionString)
            {
                DimensionId = DimensionId,
                MsgSource = TaskClient == null ? "BaseSyncronizer" : "{processname=" + (TaskClient.ProcessName ?? "") + ",processid=" + (TaskClient.ProcessId ?? "") + "}",
                ErrorMessage = message,
                StackTrace = stackTrace
            }.SaveSyncLogError();
        }

        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="stackTrace">The stack trace.</param>
        /// <param name="entityId">The entity identifier.</param>
        public void LogError(string message, string stackTrace, string entityId = null)
        {
            new SyncLogError(SyncronizationConnectionString)
            {
                DimensionId = DimensionId,
                MsgSource = string.Format("processname={0},processid={1},entityid={2}",
                    TaskClient == null ? null : TaskClient.ProcessName,
                    TaskClient == null ? null : TaskClient.ProcessId,
                    entityId),
                ErrorMessage = message,
                StackTrace = stackTrace
            }.SaveSyncLogError();
        }

        #endregion Logging


        #region Subclass for dimension arguments
        private class DimensionObjectAndEntity<TDim, TEntity>
            where TDim : DimBase, new()
            where TEntity : BaseEntity<TEntity>, new()
        {
            public TDim Dimension { get; set; }
            public TEntity Entity { get; set; }
        }

        private class DimensionObjectEntityAndChild<TDim, TEntity, TChild>
            where TDim : DimBase, new()
            where TEntity : BaseEntity<TEntity>, new()
            where TChild : BaseNonEntity, new()
        {
            public TDim Dimension { get; set; }
            public TEntity Entity { get; set; }
            public TChild Child { get; set; }
        }
        #endregion
    }
}
