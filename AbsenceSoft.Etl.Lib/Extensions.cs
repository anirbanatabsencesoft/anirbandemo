﻿using AbsenceSoft.Data;
using AbsenceSoft.Etl.Data.Sync;
using AbsenceSoft.Etl.Data.Warehouse;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;

namespace AbsenceSoft.Etl.Lib
{
    public static class Extensions
    {
        /// <summary>
        /// Fetches the entity update list from MongoDB.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity to fetch; determines the source data collection and repository.</typeparam>
        /// <param name="syncManager">The synchronize manager which contains the last update information to pull from.</param>
        /// <param name="batchSize">Size of the batch, if any, to override the setting in the <paramref name="syncManager" /> passed in.</param>
        /// <param name="onResult">The on result.</param>
        /// <returns></returns>
        public static List<TEntity> FetchEntityUpdateList<TEntity>(this SyncManager syncManager, int? batchSize = null, Action<TEntity> onResult = null, string collectionName = "") 
            where TEntity : BaseEntity<TEntity>, new()
        {
            if (syncManager == null)
                return new List<TEntity>(0);

            //set batch size
            if(batchSize == null)
            {
                batchSize = 1000;
            }

            //set collection name
            if (string.IsNullOrWhiteSpace(collectionName))
            {
                collectionName = typeof(TEntity).Name;
            }

            //get data
            var boh = new BsonObjectHandler();
            var entityList = boh.GetEntityList<TEntity>(collectionName, batchSize.Value, syncManager.DimensionId);

            //do the action
            if(onResult != null)
            {
                foreach (var e in entityList)
                    onResult(e);
            }
            
            //return
            return entityList;
        }

        /// <summary>
        /// Updates the synchronize manager source count.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="syncManager">The synchronize manager.</param>
        public static void UpdateSyncManagerSourceCount<TEntity>(this SyncManager syncManager) where TEntity : BaseEntity<TEntity>, new()
        {
            var count = BsonObjectHandler.GetPendingItems(typeof(TEntity).Name);

            syncManager.SourceCount = count;

            if (count > 0)
            {
                syncManager.UpdateSourceCount();
            }
        }
                

        /// <summary>
        /// Records the failure.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="syncManager">The synchronize manager.</param>
        /// <param name="entity">The entity.</param>
        /// <param name="dimObject">The dim object.</param>
        /// <returns></returns>
        public static SyncFailure RecordFailure<TEntity>(this SyncManager syncManager, TEntity entity, DimBase dimObject)
            where TEntity : BaseEntity<TEntity>, new()
        {
            if (entity == null)
                return null;

            SyncFailure failure = new SyncFailure(syncManager.ConnectionString)
            {
                DimensionId = syncManager.DimensionId,
                DoAction = syncManager.DefaultFailureAction,
                ExternalReferenceId = entity.Id,
                ExternalModifiedDate = entity.ModifiedDate.ToUnixDate(),
                EntityJson = entity.ToJSON()
            };
            if (dimObject != null)
            {
                failure.HasError = dimObject.HasError;
                failure.ErrorMessage = dimObject.ErrorMessage;
                failure.StackTrace = dimObject.StackTrace;
            }
            failure.Save();

            return failure;
        }

        /// <summary>
        /// Records the failure.
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <param name="syncManager">The synchronize manager.</param>
        /// <param name="message">The message.</param>
        /// <param name="stackTrace">The stack trace.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Failure must have a valid SyncFailureId;failure</exception>
        public static SyncFailure RecordFailure(this SyncFailure failure, SyncManager syncManager, string message, string stackTrace)
        {
            if (!failure.SyncFailureId.HasValue)
                throw new ArgumentException("Failure must have a valid SyncFailureId", "failure");

            failure.Attempts++;
            if (!string.IsNullOrWhiteSpace(message))
                failure.ErrorMessage = message;
            if (!string.IsNullOrWhiteSpace(stackTrace))
                failure.StackTrace = stackTrace;
            if (syncManager.MaxFailureAttempts <= failure.Attempts)
                failure.DoAction = SyncFailureAction.Nothing;
            failure.Save();

            return failure;
        }

        /// <summary>
        /// Records the success.
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Failure must have a valid SyncFailureId;failure</exception>
        public static SyncFailure RecordExpired(this SyncFailure failure)
        {
            if (!failure.SyncFailureId.HasValue)
                throw new ArgumentException("Failure must have a valid SyncFailureId", "failure");

            failure.Attempts++;
            failure.DoAction = SyncFailureAction.Expired;
            failure.IsResolved = true;
            failure.ResolvedDate = DateTime.UtcNow;
            failure.Save();

            return failure;
        }

        /// <summary>
        /// Records the success.
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Failure must have a valid SyncFailureId;failure</exception>
        public static SyncFailure RecordSuccess(this SyncFailure failure)
        {
            if (!failure.SyncFailureId.HasValue)
                throw new ArgumentException("Failure must have a valid SyncFailureId", "failure");

            failure.Attempts++;
            failure.DoAction = SyncFailureAction.Success;
            failure.IsResolved = true;
            failure.ResolvedDate = DateTime.UtcNow;
            failure.Save();

            return failure;
        }
    }
}
