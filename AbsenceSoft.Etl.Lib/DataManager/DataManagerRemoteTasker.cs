﻿using AbsenceSoft.Etl.Data.Sync;
using Npgsql;

namespace AbsenceSoft.Etl.Lib
{
    public class DataManagerRemoteTasker : BaseDataManager
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataManagerRemoteTasker" /> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DataManagerRemoteTasker(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Saves the task client.
        /// </summary>
        /// <param name="o">The o.</param>
        public void SaveTaskClient(TaskClient o)
        {
            if (o == null)
                return;

            using (var conn = new NpgsqlConnection(ConnectionString))
            {
                conn.Open();
                o.Save(conn);
                if (o.HasError)
                {
                    HasError = true;
                    ErrorMessage = o.ErrorMessage;
                    StackTrace = o.StackTrace;
                }
            }
        }

        /// <summary>
        /// Loads the name of the task client by process.
        /// </summary>
        /// <param name="o">The o.</param>
        public void LoadTaskClientByProcessName(TaskClient o)
        {
            if (o == null)
                return;

            using (var conn = new NpgsqlConnection(ConnectionString))
            {
                conn.Open();
                o.LoadByProcessName(conn);
                if (o.HasError)
                {
                    HasError = true;
                    ErrorMessage = o.ErrorMessage;
                    StackTrace = o.StackTrace;
                }
            }
        }

        /// <summary>
        /// Loads the task client.
        /// </summary>
        /// <param name="o">The o.</param>
        public void LoadTaskClient(TaskClient o)
        {
            if (o == null)
                return;
            using (var conn = new NpgsqlConnection(ConnectionString))
            {
                conn.Open();
                o.Load(conn);
                if (o.HasError)
                {
                    HasError = true;
                    ErrorMessage = o.ErrorMessage;
                    StackTrace = o.StackTrace;
                }
            }
        }

        /// <summary>
        /// Updates the task client initialize.
        /// </summary>
        /// <param name="o">The o.</param>
        public void UpdateTaskClientInit(TaskClient o)
        {
            if (o == null)
                return;

            using (var conn = new NpgsqlConnection(ConnectionString))
            {
                conn.Open();
                o.UpdateTaskClientInit(conn);
                if (o.HasError)
                {
                    HasError = true;
                    ErrorMessage = o.ErrorMessage;
                    StackTrace = o.StackTrace;
                }
            }
        }
    }
}
