﻿using Npgsql;
using Insight.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using MongoDB.Bson.Serialization;

namespace AbsenceSoft.Etl.Lib
{

    /// <summary>
    /// Retrieve and update the postgres raw data
    /// </summary>
    public class BsonObjectHandler
    {
        private static string _connectionString = Convert.ToString(System.Configuration.ConfigurationManager.ConnectionStrings["PostgresObjectSource"]);
        private static List<UpdateObject> _updateList = new List<UpdateObject>();
        private static StringBuilder _log = new StringBuilder();

        public static List<UpdateObject> UpdateList { get { return _updateList; } }

        /// <summary>
        /// Constructor to inject
        /// </summary>
        public BsonObjectHandler()
        {

        }

        /// <summary>
        /// Get the object data
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entityName"></param>
        /// <param name="limit"></param>
        /// <param name="dimensionId"></param>
        /// <returns></returns>
        public List<T> GetEntityList<T>(string entityName, int limit = 1000, long? dimensionId = null) where T : new()
        {
            //define
            IList<ObjectModel> bsonObjects = null;
            List<T> result = new List<T>();

            //get
            using (var conn = new NpgsqlConnection(_connectionString))
            {
                string getObjectSql = $"SELECT id, mongo_collection_name, objbson, operation_type, document_key FROM mongo_changestream_log WHERE mongo_collection_name=@coll AND is_archived = False AND is_processed = False ORDER BY id ASC LIMIT {limit};";
                bsonObjects = conn.QuerySql<ObjectModel>(getObjectSql, new { coll = entityName });
            }

            //cast
            if (bsonObjects != null && bsonObjects.Count > 0)
            {

                foreach (var model in bsonObjects)
                {
                    try
                    {
                        //check if it's deleted, then update our DW as well
                        if (model.OperationType == 3)
                        {
                            try
                            {
                                dynamic deletedObj = BsonSerializer.Deserialize<System.Dynamic.ExpandoObject>(model.DocumentKey);
                                MarkAsDeletedForMongoHardDelete(dimensionId, deletedObj._id.ToString());
                            }
                            catch (Exception ex)
                            {
                                _log.AppendLine($"An error occurred for {model.DocumentKey}. Error: {ex.Message}");
                                MarkChangeStreamLogAsError(model.Id, "Unable to parse document Key", ex.ToString());
                                model.IsError = true;
                            }
                        }
                        else
                        {
                            //deserialize & add
                            var obj = BsonSerializer.Deserialize<T>(model.BsonObject);
                            result.Add(obj);
                        }

                    }
                    catch (Exception ex)
                    {
                        _log.AppendLine($"Skipping row id: {model.Id} for error. Error message: {ex.Message}");
                        MarkChangeStreamLogAsError(model.Id, "Unable to deserialize object", ex.ToString());
                        model.IsError = true;
                    }
                    finally
                    {
                        _updateList.Add(new Lib.UpdateObject { Id = model.Id, ObjectId = model.DocumentKey, IsError = model.IsError });
                    }
                }
            }

            //return
            return result;
        }

        /// <summary>
        /// Mark the change stream log as error and log messages
        /// </summary>
        /// <param name="id"></param>
        /// <param name="message"></param>
        /// <param name="trace"></param>
        public static void MarkChangeStreamLogAsError(long id, string message, string trace)
        {
            using (var conn = new NpgsqlConnection(_connectionString))
            {
                var updateSql = $"UPDATE public.mongo_changestream_log SET has_error = True, err_message = @message, err_stack_trace = @trace, is_archived = False WHERE id=@id";
                conn.ExecuteSql(updateSql, new { id = id, message = message, trace = trace });
            }
        }

        /// <summary>
        /// Mark the change stream log as error and log messages
        /// </summary>
        /// <param name="objectId"></param>
        /// <param name="message"></param>
        /// <param name="trace"></param>
        public static void MarkChangeStreamLogAsError(string objectId, string message, string trace)
        {
            foreach(var obj in _updateList)
            {
                if (obj.ObjectId.Contains(objectId))
                {
                    MarkChangeStreamLogAsError(obj.Id, message, trace);
                    break;
                }
            }
        }


        /// <summary>
        /// Update 
        /// </summary>
        /// <param name="dimensionId"></param>
        /// <param name="objectId"></param>
        private void MarkAsDeletedForMongoHardDelete(long? dimensionId = null, string objectId = null)
        {
            try
            {
                if (dimensionId != null && !string.IsNullOrWhiteSpace(objectId))
                {
                    using (var conn = new NpgsqlConnection(_connectionString))
                    {
                        var tableNameSql = "SELECT table_name FROM operational.dimension WHERE dimension_id = @dimensionid";
                        var tableName = conn.SingleSql<string>(tableNameSql, new { dimensionid = dimensionId });

                        var updateSql = $"UPDATE {tableName} SET is_deleted = True, ver_is_current = False WHERE ext_ref_id=@ext_ref_id";
                        conn.ExecuteSql(updateSql, new { ext_ref_id = objectId });
                    }
                }
            }
            catch(Exception ex)
            {
                _log.AppendLine($"Unable to mark as deleted for dimention id {dimensionId}. Mongo object id: {objectId}. The error: {ex.Message}");
            }
        }

        /// <summary>
        /// Update all rows retrieved
        /// </summary>
        /// <returns></returns>
        public static string UpdateObject()
        {
            if (_updateList.Count > 0)
            {
                //update
                using (var conn = new NpgsqlConnection(_connectionString))
                {
                    string sql = $"UPDATE mongo_changestream_log SET updated_date=now(), is_processed=True WHERE id IN ({string.Join(",", _updateList.Select(p=>p.Id).ToArray())});";
                    conn.ExecuteSql(sql);
                }

                //update log
                _log.AppendLine($"Total {_updateList.Count} row(s) processed");
            }
            else
            {
                _log.AppendLine("No record to be processed");
            }

            //return
            return _log.ToString();               
        }

        /// <summary>
        /// Archive the processed objects for future deletion
        /// </summary>
        public static void Archive()
        {
            var archiveList = _updateList.Where(p => p.IsError == false).Select(p=>p.Id).ToList();
            if (archiveList.Count > 0)
            {
                //update
                using (var conn = new NpgsqlConnection(_connectionString))
                {
                    string sql = $"UPDATE mongo_changestream_log SET updated_date=now(), is_archived=True WHERE id IN ({string.Join(",", archiveList.ToArray())});";
                    conn.ExecuteSql(sql);
                }
            }
            //clear
            _updateList.Clear();
        }

        /// <summary>
        /// Returns the sum of pending items
        /// </summary>
        /// <param name="entityName"></param>
        /// <returns></returns>
        public static long GetPendingItems(string entityName)
        {
            using (var conn = new NpgsqlConnection(_connectionString))
            {
                string sql = $"SELECT COUNT(1) FROM mongo_changestream_log WHERE mongo_collection_name='{entityName}' AND is_archived = False AND is_processed = False;";
                return conn.SingleSql<long>(sql);
            }
        }
    }

    /// <summary>
    /// Model to bind the object json data
    /// </summary>
    public class ObjectModel
    {
        [Column("id")]
        public long Id { get; set; }

        [Column("mongo_collection_name")]
        public string CollectionName { get; set; }

        [Column("objbson")]
        public string BsonObject { get; set; }

        [Column("operation_type")]
        public int OperationType { get; set; }

        [Column("document_key")]
        public string DocumentKey { get; set; }
        
        public bool IsError { get; set; } = false;
    }

    /// <summary>
    /// Class to hold to object key and id
    /// </summary>
    public class UpdateObject
    {
        public string ObjectId { get; set; }
        public long Id { get; set; }
        public bool IsError { get; set; } = false;
    }

}
